* regenerated at 05.05.2016 16:36:52 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_GEN_UOM_TTOP.                " Global Data
  INCLUDE LZARN_GEN_UOM_TUXX.                " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_GEN_UOM_TF...                " Subroutines
* INCLUDE LZARN_GEN_UOM_TO...                " PBO-Modules
* INCLUDE LZARN_GEN_UOM_TI...                " PAI-Modules
* INCLUDE LZARN_GEN_UOM_TE...                " Events
* INCLUDE LZARN_GEN_UOM_TP...                " Local class implement.
* INCLUDE LZARN_GEN_UOM_TT99.                " ABAP Unit tests
  INCLUDE LZARN_GEN_UOM_TF00                      . " subprograms
  INCLUDE LZARN_GEN_UOM_TI00                      . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
