* regenerated at 05.05.2016 15:45:08 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_PRICEREAS_TTOP.              " Global Data
  INCLUDE LZARN_PRICEREAS_TUXX.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_PRICEREAS_TF...              " Subroutines
* INCLUDE LZARN_PRICEREAS_TO...              " PBO-Modules
* INCLUDE LZARN_PRICEREAS_TI...              " PAI-Modules
* INCLUDE LZARN_PRICEREAS_TE...              " Events
* INCLUDE LZARN_PRICEREAS_TP...              " Local class implement.
* INCLUDE LZARN_PRICEREAS_TT99.              " ABAP Unit tests
  INCLUDE LZARN_PRICEREAS_TF00                    . " subprograms
  INCLUDE LZARN_PRICEREAS_TI00                    . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
