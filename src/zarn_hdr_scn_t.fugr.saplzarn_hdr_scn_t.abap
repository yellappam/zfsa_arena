* regenerated at 18.11.2016 14:19:18
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_HDR_SCN_TTOP.                " Global Data
  INCLUDE LZARN_HDR_SCN_TUXX.                " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_HDR_SCN_TF...                " Subroutines
* INCLUDE LZARN_HDR_SCN_TO...                " PBO-Modules
* INCLUDE LZARN_HDR_SCN_TI...                " PAI-Modules
* INCLUDE LZARN_HDR_SCN_TE...                " Events
* INCLUDE LZARN_HDR_SCN_TP...                " Local class implement.
* INCLUDE LZARN_HDR_SCN_TT99.                " ABAP Unit tests
  INCLUDE LZARN_HDR_SCN_TF00                      . " subprograms
  INCLUDE LZARN_HDR_SCN_TI00                      . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
