*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 25.05.2016 at 11:50:38 by user C90001363
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_HANDLINST_T................................*
DATA:  BEGIN OF STATUS_ZARN_HANDLINST_T              .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_HANDLINST_T              .
CONTROLS: TCTRL_ZARN_HANDLINST_T
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_HANDLINST_T              .
TABLES: ZARN_HANDLINST_T               .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
