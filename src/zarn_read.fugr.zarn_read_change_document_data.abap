*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Functional Specification 3113
* DATE WRITTEN     # 23112015
* SAP VERSION      # 731
* TYPE             # Function Module
* AUTHOR           # C90001363, Jitin Kharbanda
*-----------------------------------------------------------------------
* TITLE            # AReNa: Read Change Document Data
* PURPOSE          # AReNa: Read Change Document Data for Control,
*                  # Version and Regional Tables
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------
FUNCTION zarn_read_change_document_data.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(IV_REG) TYPE  XFELD DEFAULT SPACE
*"     REFERENCE(IV_CHG) TYPE  XFELD DEFAULT SPACE
*"     REFERENCE(IR_IDNO_ID) TYPE  ZTARN_CDOBJECTV_RANGE OPTIONAL
*"     REFERENCE(IR_CHGID_ID) TYPE  ZTARN_CDOBJECTV_RANGE OPTIONAL
*"     REFERENCE(IR_IDNO) TYPE  ZTARN_IDNO_RANGE OPTIONAL
*"     REFERENCE(IR_CHANGENR) TYPE  ZTARN_CHANGENR_RANGE OPTIONAL
*"     REFERENCE(IR_CHG_CATEGORY) TYPE  ZTARN_CHG_CATEGORY_RANGE
*"       OPTIONAL
*"     REFERENCE(IR_UDATE) TYPE  ZTARN_UDATE_RANGE OPTIONAL
*"     REFERENCE(IR_USERNAME) TYPE  ZTARN_USERNAME_RANGE OPTIONAL
*"  EXPORTING
*"     REFERENCE(ET_DATA) TYPE  ZTARN_CHANGE_DOCUMENT_DISPLAY
*"  EXCEPTIONS
*"      INPUT_REQUIRED
*"      NO_DATA
*"----------------------------------------------------------------------

  DATA: lt_chg_doc         TYPE ty_t_chg_doc,
        ls_chg_doc         TYPE ty_s_chg_doc,
        lt_cdpos_uid       TYPE ty_t_cdpos_uid,
        ls_cdpos_uid       TYPE ty_s_cdpos_uid,
        lt_data            TYPE ztarn_change_document_display,
        ls_data            TYPE zsarn_change_document_display,
        ls_reg_banner_key  TYPE ty_s_reg_banner_key,
        ls_prd_version_key TYPE ty_s_prd_version_key,
        ls_control_key     TYPE ty_s_control_key,
        ls_reg_uom_key     TYPE ty_s_reg_uom_key,
        ls_reg_ean_key     TYPE ty_s_reg_ean_key,
        ls_reg_pir_key     TYPE ty_s_reg_pir_key,
        ls_reg_prfam_key   TYPE ty_s_reg_prfam_key,
        ls_reg_lst_prc_key TYPE ty_s_reg_lst_prc_key,
        ls_reg_std_ter_key TYPE ty_s_reg_std_ter_key,
        ls_reg_dc_sell_key TYPE ty_s_reg_dc_sell_key,
        ls_reg_rrp_key     TYPE ty_s_reg_rrp_key,
        ls_reg_onlcat_key  TYPE ty_s_reg_onlcat_key,                  "++ONLD-835 JKH 30.05.2017

        lt_cc_hdr          TYPE ztarn_cc_hdr,
        ls_cc_hdr          TYPE zarn_cc_hdr,
        lt_cc_det          TYPE ztarn_cc_det,
        ls_cc_det          TYPE zarn_cc_det,

        ls_cc_hdr_key      TYPE ty_s_cc_hdr_key,
        ls_cc_det_key      TYPE ty_s_cc_det_key,
        ls_cc_team_key     TYPE ty_s_cc_team_key,
        lt_dfies           TYPE dfies_tab,
        ls_dfies           TYPE dfies,
        lt_field_confg     TYPE STANDARD TABLE OF zarn_field_confg,
        ls_field_confg     TYPE zarn_field_confg,

        lr_idno_id         TYPE ztarn_cdobjectv_range,
        lr_chgid_id        TYPE ztarn_cdobjectv_range,
        lr_changenr        TYPE ztarn_changenr_range,
        lr_chg_category    TYPE ztarn_chg_category_range,
        lr_udate           TYPE ztarn_udate_range,
        lr_username        TYPE ztarn_username_range.


  IF iv_reg IS INITIAL AND
     iv_chg IS INITIAL.
    RAISE input_required.
  ENDIF.


  lr_idno_id[]      = ir_idno_id[].
  lr_chgid_id[]     = ir_chgid_id[].
  lr_changenr[]     = ir_changenr[].
  lr_chg_category[] = ir_chg_category[].
  lr_udate[]        = ir_udate[].
  lr_username[]     = ir_username[].


* Get Change Document Data
  PERFORM get_change_document_data USING iv_reg
                                         iv_chg
                                         lr_idno_id[]
                                         lr_chgid_id[]
                                         lr_changenr[]
                                         lr_udate[]
                                         lr_username[]
                                CHANGING lt_chg_doc[]
                                         lt_cdpos_uid[]
                                         lt_cc_hdr[]
                                         lt_cc_det[].

  IF lt_chg_doc[] IS INITIAL.
    RAISE no_data.
    EXIT.
  ENDIF.


  CLEAR: lt_field_confg[].
  SELECT * FROM zarn_field_confg
    INTO CORRESPONDING FIELDS OF TABLE lt_field_confg[].

  LOOP AT lt_chg_doc[] INTO ls_chg_doc.
    CLEAR ls_data.

    ls_data-tabname    = ls_chg_doc-tabname.
    ls_data-fieldname  = ls_chg_doc-fname.
    ls_data-fielddesc  = ls_chg_doc-fname.
    ls_data-tabkey     = ls_chg_doc-tabkey.
    ls_data-changenr   = ls_chg_doc-changenr.
    ls_data-change_ind = ls_chg_doc-chngind.

    IF ls_chg_doc-objectclas = 'ZARN_CTRL_REGNL'.

      ls_data-idno = ls_chg_doc-objectid.

      CLEAR ls_field_confg.
      READ TABLE lt_field_confg[] INTO ls_field_confg
      WITH KEY tabname = ls_chg_doc-tabname
      fldname = ls_chg_doc-fname.
      IF sy-subrc = 0.
        ls_data-chg_category = ls_field_confg-chg_category.
      ENDIF.
    ENDIF.

    IF ls_chg_doc-objectclas = 'ZARN_CHG_CAT'.
      ls_data-chgid = ls_chg_doc-objectid.
    ENDIF.

    REFRESH: lt_dfies[].
    CALL FUNCTION 'DDIF_FIELDINFO_GET'
      EXPORTING
        tabname        = ls_chg_doc-tabname
        fieldname      = ls_chg_doc-fname
        langu          = sy-langu
      TABLES
        dfies_tab      = lt_dfies[]
      EXCEPTIONS
        not_found      = 1
        internal_error = 2
        OTHERS         = 3.
    IF sy-subrc = 0.
      CLEAR ls_dfies.
      READ TABLE lt_dfies[] INTO ls_dfies INDEX 1.
      IF sy-subrc = 0.
        ls_data-fielddesc = ls_dfies-scrtext_l.
      ENDIF.
    ENDIF.

    ls_data-value_new = ls_chg_doc-value_new.
    ls_data-value_old = ls_chg_doc-value_old.
    ls_data-userid    = ls_chg_doc-userid.
    ls_data-username  = ls_chg_doc-username.
    ls_data-udate     = ls_chg_doc-udate.
    ls_data-utime     = ls_chg_doc-utime.

    CASE ls_chg_doc-tabname.
      WHEN 'ZARN_REG_BANNER'.
        CLEAR ls_reg_banner_key.
        ls_reg_banner_key = ls_chg_doc-tabkey.

        ls_data-banner = ls_reg_banner_key-banner.

      WHEN 'ZARN_PRD_VERSION'.
        CLEAR ls_prd_version_key.
        ls_prd_version_key = ls_chg_doc-tabkey.

        ls_data-version = ls_prd_version_key-version.

      WHEN 'ZARN_CONTROL'.
        CLEAR ls_cdpos_uid.
        READ TABLE lt_cdpos_uid[] INTO ls_cdpos_uid
        WITH TABLE KEY keyguid = ls_chg_doc-tabkey.
        IF sy-subrc = 0.
          CLEAR ls_control_key.
          ls_control_key = ls_cdpos_uid-tabkey.

          ls_data-tabkey = ls_cdpos_uid-tabkey.
        ENDIF.

      WHEN 'ZARN_REG_UOM'.
        CLEAR ls_cdpos_uid.
        READ TABLE lt_cdpos_uid[] INTO ls_cdpos_uid
        WITH TABLE KEY keyguid = ls_chg_doc-tabkey.
        IF sy-subrc = 0.
          CLEAR ls_reg_uom_key.
          ls_reg_uom_key = ls_cdpos_uid-tabkey.

          ls_data-pim_uom_code = ls_reg_uom_key-pim_uom_code.

          ls_data-tabkey    = ls_cdpos_uid-tabkey.
        ENDIF.

      WHEN 'ZARN_REG_EAN'.
        CLEAR ls_reg_ean_key.
        ls_reg_ean_key = ls_chg_doc-tabkey.

        ls_data-meinh = ls_reg_ean_key-meinh.
        ls_data-ean11 = ls_reg_ean_key-ean11.

      WHEN 'ZARN_REG_PIR'.
        CLEAR ls_cdpos_uid.
        READ TABLE lt_cdpos_uid[] INTO ls_cdpos_uid
        WITH TABLE KEY keyguid = ls_chg_doc-tabkey.
        IF sy-subrc = 0.
          CLEAR ls_reg_pir_key.
          ls_reg_pir_key = ls_cdpos_uid-tabkey.

          ls_data-order_uom_pim        = ls_reg_pir_key-order_uom_pim.
          ls_data-hybris_internal_code = ls_reg_pir_key-hybris_internal_code.

          ls_data-tabkey         = ls_reg_pir_key.
        ENDIF.


      WHEN 'ZARN_REG_PRFAM'.
        CLEAR ls_reg_prfam_key.
        ls_reg_prfam_key = ls_chg_doc-tabkey.

        ls_data-prfam_uom = ls_reg_prfam_key-prfam_uom.


      WHEN 'ZARN_REG_LST_PRC'.
        CLEAR ls_reg_lst_prc_key.
        ls_reg_lst_prc_key = ls_chg_doc-tabkey.

        ls_data-condition_type = ls_reg_lst_prc_key-condition_type.
        ls_data-ekorg          = ls_reg_lst_prc_key-ekorg.
        ls_data-lifnr          = ls_reg_lst_prc_key-lifnr.
        ls_data-cond_unit      = ls_reg_lst_prc_key-cond_unit.

      WHEN 'ZARN_REG_STD_TER'.
        CLEAR ls_reg_std_ter_key.
        ls_reg_std_ter_key = ls_chg_doc-tabkey.

        ls_data-vkorg = ls_reg_std_ter_key-vkorg.

      WHEN 'ZARN_REG_DC_SELL'.
        CLEAR ls_reg_dc_sell_key.
        ls_reg_dc_sell_key = ls_chg_doc-tabkey.

        ls_data-condition_type = ls_reg_dc_sell_key-condition_type.
        ls_data-vkorg          = ls_reg_dc_sell_key-vkorg.
        ls_data-lifnr          = ls_reg_dc_sell_key-lifnr.
        ls_data-cond_unit      = ls_reg_dc_sell_key-cond_unit.

      WHEN 'ZARN_REG_RRP'.
        CLEAR ls_reg_rrp_key.
        ls_reg_rrp_key = ls_chg_doc-tabkey.

        ls_data-vkorg     = ls_reg_rrp_key-vkorg.
        ls_data-pltyp     = ls_reg_rrp_key-pltyp.
        ls_data-cond_unit = ls_reg_rrp_key-cond_unit.


* INS Begin of Change ONLD-835 JKH 30.05.2017
      WHEN 'ZARN_REG_ONLCAT'.
        CLEAR ls_reg_onlcat_key.
        ls_reg_onlcat_key = ls_chg_doc-tabkey.

        ls_data-bunit        = ls_reg_onlcat_key-bunit.
        ls_data-catalog_type = ls_reg_onlcat_key-catalog_type.
* INS End of Change ONLD-835 JKH 30.05.2017



      WHEN 'ZARN_CC_HDR'.
        CLEAR ls_cc_hdr_key.
        ls_cc_hdr_key = ls_chg_doc-tabkey.

        ls_data-idno         = ls_cc_hdr_key-idno.
        ls_data-chgid        = ls_cc_hdr_key-chgid.
        ls_data-chg_category = ls_cc_hdr_key-chg_category.

        CLEAR ls_cc_hdr.
        READ TABLE lt_cc_hdr[] INTO ls_cc_hdr
        WITH KEY chgid        = ls_cc_hdr_key-chgid
                 idno         = ls_cc_hdr_key-idno
                 chg_category = ls_cc_hdr_key-chg_category
                 chg_area     = ls_cc_hdr_key-chg_area.
        IF sy-subrc = 0.
          ls_data-version       = ls_cc_hdr-national_ver_id.
          ls_data-ver_comp_with = ls_cc_hdr-ver_comp_with.
        ENDIF.


      WHEN 'ZARN_CC_DET'.
        CLEAR ls_cdpos_uid.
        READ TABLE lt_cdpos_uid[] INTO ls_cdpos_uid
        WITH TABLE KEY keyguid = ls_chg_doc-tabkey.
        IF sy-subrc = 0.
          CLEAR ls_cc_det_key.
          ls_cc_det_key = ls_cdpos_uid-tabkey.

          ls_data-idno           = ls_cc_det_key-idno.
          ls_data-chgid          = ls_cc_det_key-chgid.
          ls_data-chg_category   = ls_cc_det_key-chg_category.
          ls_data-chg_table      = ls_cc_det_key-chg_table.
          ls_data-chg_field      = ls_cc_det_key-chg_field.
          ls_data-reference_data = ls_cc_det_key-reference_data.


          CLEAR ls_cc_det.
          READ TABLE lt_cc_det[] INTO ls_cc_det
          WITH KEY chgid          = ls_cc_det_key-chgid
                   idno           = ls_cc_det_key-idno
                   chg_category   = ls_cc_det_key-chg_category
                   chg_table      = ls_cc_det_key-chg_table
                   chg_field      = ls_cc_det_key-chg_field
                   reference_data = ls_cc_det_key-reference_data.
          IF sy-subrc = 0.
            ls_data-version       = ls_cc_det-national_ver_id.
            ls_data-ver_comp_with = ls_cc_det-ver_comp_with.
            ls_data-value_old     = ls_cc_det-value_old.
            ls_data-value_new     = ls_cc_det-value_new.
          ENDIF.


          CLEAR: ls_cc_det_key-reference_data.
          ls_data-tabkey         = ls_cc_det_key.
        ENDIF.

      WHEN 'ZARN_CC_TEAM'.
        CLEAR ls_cc_team_key.
        ls_cc_team_key = ls_chg_doc-tabkey.

        ls_data-idno         = ls_cc_team_key-idno.
        ls_data-chgid        = ls_cc_team_key-chgid.
        ls_data-chg_category = ls_cc_team_key-chg_category.
        ls_data-team_code    = ls_cc_team_key-team_code.


    ENDCASE.

    APPEND ls_data TO lt_data[].
  ENDLOOP.  " LOOP AT lt_chg_doc[] INTO ls_chg_doc.


  IF lr_chg_category[] IS NOT INITIAL.
    DELETE lt_data[] WHERE chg_category NOT IN lr_chg_category[].
  ENDIF.

  IF iv_chg = abap_true AND ir_idno[] IS NOT INITIAL.
    DELETE lt_data[] WHERE idno NOT IN ir_idno[].
  ENDIF.


  IF lt_data[] IS INITIAL.
    RAISE no_data.
  ELSE.
    et_data[] = lt_data[].
  ENDIF.

ENDFUNCTION.
