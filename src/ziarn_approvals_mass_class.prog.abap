*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_APPROVALS_MASS_CLASS
*&---------------------------------------------------------------------*


*----------------------------------------------------------------------*
*   CLASS DECLARATIONS
*----------------------------------------------------------------------*

CLASS lcx_exception DEFINITION FINAL
  INHERITING FROM zcx_bapi_exception.
ENDCLASS.


INCLUDE ziarn_approvals_mass_cd3. " Model
INCLUDE ziarn_approvals_mass_cd2. " View
INCLUDE ziarn_approvals_mass_cd1. " Controller
INCLUDE ziarn_approvals_mass_cd4. " Tree

*----------------------------------------------------------------------*
*   CLASS IMPLEMENTATIONS
*----------------------------------------------------------------------*

INCLUDE ziarn_approvals_mass_ci3. " Model
INCLUDE ziarn_approvals_mass_ci2. " View
INCLUDE ziarn_approvals_mass_ci1. " Controller
INCLUDE ziarn_approvals_mass_ci4. " Tree
