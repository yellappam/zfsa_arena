*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_DATA_RECONCILIATION_PAI
*&---------------------------------------------------------------------*


*&---------------------------------------------------------------------*
*&      Module  EXIT_9000  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE exit_9000 INPUT.


* Initialize
  PERFORM initialize.




  CALL METHOD cl_gui_cfw=>flush.

  CASE sy-ucomm.
*   Back
    WHEN zcl_constants=>gc_ucomm_back.
      LEAVE TO SCREEN 0.

*   Exit
    WHEN zcl_constants=>gc_ucomm_exit.
      LEAVE TO SCREEN 0.

*   Cancel
    WHEN zcl_constants=>gc_ucomm_cancel.
      LEAVE PROGRAM.

  ENDCASE.


ENDMODULE.                 " EXIT_8000  INPUT


*&SPWIZARD: INPUT MODULE FOR TS 'TS_RECON'. DO NOT CHANGE THIS LINE!
*&SPWIZARD: GETS ACTIVE TAB
MODULE ts_recon_active_tab_get INPUT.
  ok_code = sy-ucomm.
  CASE ok_code.
    WHEN c_ts_recon-tab1.
      g_ts_recon-pressed_tab = c_ts_recon-tab1.
    WHEN c_ts_recon-tab2.
      g_ts_recon-pressed_tab = c_ts_recon-tab2.
    WHEN c_ts_recon-tab3.
      g_ts_recon-pressed_tab = c_ts_recon-tab3.
    WHEN c_ts_recon-tab4.
      g_ts_recon-pressed_tab = c_ts_recon-tab4.
    WHEN c_ts_recon-tab5.
      g_ts_recon-pressed_tab = c_ts_recon-tab5.
    WHEN c_ts_recon-tab6.
      g_ts_recon-pressed_tab = c_ts_recon-tab6.
    WHEN c_ts_recon-tab7.
      g_ts_recon-pressed_tab = c_ts_recon-tab7.
    WHEN c_ts_recon-tab8.
      g_ts_recon-pressed_tab = c_ts_recon-tab8.
    WHEN OTHERS.
      g_ts_recon-pressed_tab = ts_recon-activetab.
*&SPWIZARD:      DO NOTHING
  ENDCASE.
ENDMODULE.                    "TS_RECON_ACTIVE_TAB_GET INPUT
