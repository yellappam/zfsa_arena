*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 16.05.2016 at 09:39:14 by user C90001363
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_INB_ERRMAIL................................*
DATA:  BEGIN OF STATUS_ZARN_INB_ERRMAIL              .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_INB_ERRMAIL              .
CONTROLS: TCTRL_ZARN_INB_ERRMAIL
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_INB_ERRMAIL              .
TABLES: ZARN_INB_ERRMAIL               .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
