*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 19.04.2016 at 16:14:44 by user C90002769
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_SLA........................................*
DATA:  BEGIN OF STATUS_ZARN_SLA                      .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_SLA                      .
CONTROLS: TCTRL_ZARN_SLA
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_SLA                      .
TABLES: ZARN_SLA                       .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
