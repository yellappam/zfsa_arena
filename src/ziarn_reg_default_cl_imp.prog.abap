*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_REG_DEFAULT_CL_IMP
*&---------------------------------------------------------------------*

CLASS lcx_error IMPLEMENTATION.

  METHOD constructor.

    super->constructor(
      EXPORTING previous = iv_previous ).

    me->mv_text = iv_text.

  ENDMETHOD.  " constructor

ENDCLASS.  " lcx_error



CLASS lcl_main IMPLEMENTATION.

  METHOD constructor.

    me->ms_sel_params = is_sel_params.


  ENDMETHOD.  " constructor


  METHOD process.

* Get IDNO and Approvals data
    me->get_idno_data( ).

* Validate IDNO and Locking
    me->validate_idno_and_locking( ).

* Get National and Regional data
    me->get_nat_reg_data( ).

* Default and Save Regional data
    me->default_and_save_reg_data( ).

  ENDMETHOD.  " process

  METHOD get_idno_data.

    IF me->ms_sel_params-s_idno[] IS INITIAL.
      RETURN.
    ENDIF.

* Get Product Details for entered IDNOs
    SELECT a~idno a~fan_id
           b~current_ver b~previous_ver b~latest_ver b~article_ver
      INTO CORRESPONDING FIELDS OF TABLE me->mt_idno_data[]
      FROM zarn_products AS a
      JOIN zarn_ver_status AS b
        ON a~idno    = b~idno
       AND a~version = b~current_ver
     WHERE a~idno IN me->ms_sel_params-s_idno[].
    IF sy-subrc = 0.
* Get Approval data
      SELECT *
        FROM zarn_approval
        INTO CORRESPONDING FIELDS OF TABLE me->mt_approval[]
        FOR ALL ENTRIES IN me->mt_idno_data[]
        WHERE idno          EQ me->mt_idno_data-idno
          AND approval_area EQ 'NAT'
          AND nat_version   EQ me->mt_idno_data-current_ver
          AND status        IN ('AR', 'AQ', 'RJ').
    ENDIF.

  ENDMETHOD.  " get_idno_data

  METHOD validate_idno_and_locking.

    DATA: ls_log TYPE ty_s_log.

    LOOP AT me->mt_idno_data INTO DATA(ls_idno_data).

      DATA(lv_tabix) = sy-tabix.

      READ TABLE me->mt_approval[] TRANSPORTING NO FIELDS
      WITH KEY idno = ls_idno_data-idno.
      IF sy-subrc = 0.
* Unable to proceed due to outstanding NAT approval
        CLEAR ls_log.
        ls_log-idno    = ls_idno_data-idno.
        ls_log-msgty   = 'E'.
        ls_log-message = text-001.
        APPEND ls_log TO me->mt_log[].

        DELETE me->mt_idno_data[] INDEX lv_tabix.
      ENDIF.

    ENDLOOP.  " LOOP AT me->mt_idno_data INTO DATA(ls_idno_data)


  ENDMETHOD.  " validate_idno_and_locking

  METHOD get_nat_reg_data.


    DATA: lt_key     TYPE ztarn_key,
          ls_key     TYPE zsarn_key,
          lt_reg_key TYPE ztarn_reg_key,
          ls_reg_key TYPE zsarn_reg_key.

    IF me->mt_idno_data[] IS INITIAL.
      RETURN.
    ENDIF.

    LOOP AT me->mt_idno_data[] INTO DATA(ls_idno_data).

      CLEAR ls_key.
      ls_key-idno    = ls_idno_data-idno.
      ls_key-version = ls_idno_data-current_ver.
      APPEND ls_key TO lt_key[].

      CLEAR ls_reg_key.
      ls_reg_key-idno = ls_idno_data-idno.
      APPEND ls_reg_key TO lt_reg_key[].

    ENDLOOP.


* Get National Data
    CLEAR me->mt_prod_data[].
    CALL FUNCTION 'ZARN_READ_NATIONAL_DATA'
      EXPORTING
        it_key  = lt_key[]
      IMPORTING
        et_data = me->mt_prod_data[].


* Get Regional Data
    CLEAR me->mt_reg_data[].
    CALL FUNCTION 'ZARN_READ_REGIONAL_DATA'
      EXPORTING
        it_key  = lt_reg_key[]
      IMPORTING
        et_data = me->mt_reg_data[].


  ENDMETHOD.  " get_nat_reg_data

  METHOD default_and_save_reg_data.

    DATA: lo_mass_update  TYPE REF TO zcl_arn_mass_update,
          lv_locked       TYPE flag,
          lv_user         TYPE sy-uname,
          lv_username     TYPE ad_namtext,
          lv_error        TYPE flag,
          lt_msg          TYPE mass_msgs,
          ls_msg          TYPE mass_msg,

          lo_gui_load     TYPE REF TO zcl_arn_gui_load,
          ls_reg_data_def	TYPE zsarn_reg_data,
          lt_reg_data     TYPE ztarn_reg_data,
          lt_reg_data_def TYPE ztarn_reg_data,
          ls_log          TYPE ty_s_log.

    IF me->mt_idno_data[] IS INITIAL.
      RETURN.
    ENDIF.

    CREATE OBJECT lo_mass_update.
    CREATE OBJECT lo_gui_load.

    LOOP AT me->mt_idno_data[] INTO DATA(ls_idno_data).

* Check if IDNO is locked
      CLEAR : lv_locked, lv_user, lv_username, lt_msg[].
      CALL METHOD lo_mass_update->check_idno_lock
        EXPORTING
          iv_idno     = ls_idno_data-idno
        IMPORTING
          ev_locked   = lv_locked
          ev_user     = lv_user
          ev_username = lv_username
        CHANGING
          ct_msg      = lt_msg[].
      IF lv_locked = abap_true.
* IDNO & is Locked by & - &
        CLEAR ls_log.
        ls_log-idno    = ls_idno_data-idno.
        ls_log-msgty   = 'E'.

        CLEAR ls_msg.
        READ TABLE lt_msg[] INTO ls_msg WITH KEY msgty = 'E'.
        IF sy-subrc = 0.
          MESSAGE ID ls_msg-msgid TYPE ls_msg-msgty NUMBER ls_msg-msgno
                  INTO ls_log-message
                  WITH ls_msg-msgv1 ls_msg-msgv2 ls_msg-msgv3 ls_msg-msgv4.
        ELSE.
          ls_log-message = text-002.
          REPLACE FIRST OCCURRENCE OF '&' IN ls_log-message WITH ls_idno_data-idno.
          REPLACE FIRST OCCURRENCE OF '&' IN ls_log-message WITH lv_user.
          REPLACE FIRST OCCURRENCE OF '&' IN ls_log-message WITH lv_username.
        ENDIF.

        APPEND ls_log TO me->mt_log[].

        CONTINUE.
      ENDIF.  " IF lv_locked = abap_true


* Lock IDNO
      CLEAR: lt_msg[], lv_error.
      CALL METHOD lo_mass_update->lock_idno_regional
        EXPORTING
          iv_idno  = ls_idno_data-idno
        CHANGING
          ct_msg   = lt_msg[]
          cv_error = lv_error.
      IF lv_error IS NOT INITIAL.
* IDNO & is Locked by & - &
        CLEAR ls_log.
        ls_log-idno    = ls_idno_data-idno.
        ls_log-msgty   = 'E'.

        CLEAR ls_msg.
        READ TABLE lt_msg[] INTO ls_msg WITH KEY msgty = 'E'.
        IF sy-subrc = 0.
          MESSAGE ID ls_msg-msgid TYPE ls_msg-msgty NUMBER ls_msg-msgno
                  INTO ls_log-message
                  WITH ls_msg-msgv1 ls_msg-msgv2 ls_msg-msgv3 ls_msg-msgv4.
        ELSE.
* IDNO can't be Locked for some technical
          ls_log-message = text-003.
        ENDIF.

        APPEND ls_log TO me->mt_log[].

        CONTINUE.
      ENDIF.

* Read regional data
      READ TABLE me->mt_reg_data[] INTO DATA(ls_reg_data)
      WITH KEY idno = ls_idno_data-idno.
      IF sy-subrc NE 0.
        CLEAR ls_log.
        ls_log-idno    = ls_idno_data-idno.
        ls_log-msgty   = 'E'.
        ls_log-message = text-004.
        APPEND ls_log TO me->mt_log[].

* Unlock IDNO
        CALL METHOD lo_mass_update->unlock_idno_regional
          EXPORTING
            iv_idno = ls_idno_data-idno.

        CONTINUE.
      ENDIF.

* Read national data
      READ TABLE me->mt_prod_data[] INTO DATA(ls_prod_data)
      WITH KEY idno = ls_idno_data-idno
               version = ls_idno_data-current_ver.
      IF sy-subrc NE 0.
        CLEAR ls_log.
        ls_log-idno    = ls_idno_data-idno.
        ls_log-msgty   = 'E'.
        ls_log-message = text-004.
        APPEND ls_log TO me->mt_log[].

* Unlock IDNO
        CALL METHOD lo_mass_update->unlock_idno_regional
          EXPORTING
            iv_idno = ls_idno_data-idno.

        CONTINUE.
      ENDIF.


* Get Regional Defaulted data
      CLEAR ls_reg_data_def.
      CALL METHOD lo_gui_load->set_regional_default
        EXPORTING
          is_reg_data     = ls_reg_data
          is_prod_data    = ls_prod_data
        IMPORTING
          es_reg_data_def = ls_reg_data_def.
      IF ls_reg_data_def IS INITIAL.
* Some error occured during defaulting
        CLEAR ls_log.
        ls_log-idno    = ls_idno_data-idno.
        ls_log-msgty   = 'E'.
        ls_log-message = text-004.
        APPEND ls_log TO me->mt_log[].

* Unlock IDNO
        CALL METHOD lo_mass_update->unlock_idno_regional
          EXPORTING
            iv_idno = ls_idno_data-idno.

        CONTINUE.
      ENDIF.

      APPEND ls_reg_data_def TO me->mt_reg_data_def[].

      CLEAR: lt_reg_data[], lt_reg_data_def[].
      APPEND ls_reg_data     TO lt_reg_data[].
      APPEND ls_reg_data_def TO lt_reg_data_def[].

      IF me->ms_sel_params-p_test IS INITIAL.
        CALL FUNCTION 'ZARN_REGIONAL_DB_UPDATE'
          EXPORTING
            it_reg_data     = lt_reg_data_def[]
            it_reg_data_old = lt_reg_data[].
      ENDIF.


* Regional data defaulted successfully
      CLEAR ls_log.
      ls_log-idno    = ls_idno_data-idno.
      ls_log-msgty   = 'S'.
      ls_log-message = text-005.
      APPEND ls_log TO me->mt_log[].


* Unlock IDNO
      CALL METHOD lo_mass_update->unlock_idno_regional
        EXPORTING
          iv_idno = ls_idno_data-idno.

    ENDLOOP.  " LOOP AT me->mt_idno_data[] INTO DATA(ls_idno_data)

  ENDMETHOD.  " default_and_save_reg_data


ENDCLASS.  " lcl_main
