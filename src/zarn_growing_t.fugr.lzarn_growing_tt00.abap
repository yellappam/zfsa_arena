*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 25.05.2016 at 11:44:54 by user C90001363
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_GROWING_T..................................*
DATA:  BEGIN OF STATUS_ZARN_GROWING_T                .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_GROWING_T                .
CONTROLS: TCTRL_ZARN_GROWING_T
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_GROWING_T                .
TABLES: ZARN_GROWING_T                 .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
