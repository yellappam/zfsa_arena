*-----------------------------------------------------------------------
* PROJECT          # AReNA
* SPECIFICATION    #
* DATE WRITTEN     # 29.03.2017
* SAP VERSION      # 740
* TYPE             # Program
* AUTHOR           # C90001363, Jitin Kharbanda
*-----------------------------------------------------------------------
* TITLE            # National Data Reconciliation
* PURPOSE          # Get data from National and compare with current
*                  # version to see any changes
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      #
*                  #
*-----------------------------------------------------------------------
* CALLS TO         #
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 26.08.2019
* CHANGE No.       # Patching
* DESCRIPTION      # When Hybris system not available, instead of giving dump
*                  # give message to user
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
REPORT zrarn_nat_reconciliation MESSAGE-ID zarena_msg.


* Top Include for Declarations
INCLUDE ziarn_nat_reconciliation_top.

* Selection Screen
INCLUDE ziarn_nat_reconciliation_sel.

* Main Processing Logic
INCLUDE ziarn_nat_reconciliation_main.

* Subroutines Main
INCLUDE ziarn_nat_reconciliation_sub.

* PBO
INCLUDE ziarn_nat_reconciliation_pbo.

* PAI
INCLUDE ziarn_nat_reconciliation_pai.
