* regenerated at 11.12.2018 09:16:30
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_LAUNCHPTOP.                  " Global Declarations
  INCLUDE LZARN_LAUNCHPUXX.                  " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_LAUNCHPF...                  " Subroutines
* INCLUDE LZARN_LAUNCHPO...                  " PBO-Modules
* INCLUDE LZARN_LAUNCHPI...                  " PAI-Modules
* INCLUDE LZARN_LAUNCHPE...                  " Events
* INCLUDE LZARN_LAUNCHPP...                  " Local class implement.
* INCLUDE LZARN_LAUNCHPT99.                  " ABAP Unit tests
  INCLUDE LZARN_LAUNCHPF00                        . " subprograms
  INCLUDE LZARN_LAUNCHPI00                        . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
