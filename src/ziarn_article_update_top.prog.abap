*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_ARTICLE_UPDATE_TOP
*&---------------------------------------------------------------------*


DATA: gt_key          TYPE ztarn_key,
      gt_message      TYPE bal_t_msg,

      gv_log_no       TYPE balognr.
