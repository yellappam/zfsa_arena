*&---------------------------------------------------------------------*
*&  Include           ZIARN_CLEANUP_SCR
*&---------------------------------------------------------------------*

PARAMETERS: p_dir TYPE string OBLIGATORY,
            p_hdr TYPE flag,
            p_del TYPE flag.
SELECT-OPTIONS: s_idno   FOR zarn_products-idno,
                s_fan_id FOR zarn_products-fan_id.

AT SELECTION-SCREEN ON VALUE-REQUEST FOR p_dir.
  CALL METHOD cl_gui_frontend_services=>directory_browse
    CHANGING
      selected_folder      = p_dir
    EXCEPTIONS
      cntl_error           = 1
      error_no_gui         = 2
      not_supported_by_gui = 3
      OTHERS               = 4.
