*----------------------------------------------------------------------*
***INCLUDE ZARN_NATIONAL_UPDATES_O01.
*----------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*&      Module  display_alv  OUTPUT
*&---------------------------------------------------------------------*
MODULE display_alv_9001 OUTPUT.

  PERFORM get_data_9001.
  PERFORM display_alv_9001.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  EXIT_9000  INPUT
*&---------------------------------------------------------------------*
MODULE exit_9000 INPUT.
* Initialize
*  PERFORM initialize.


  CALL METHOD cl_gui_cfw=>flush.

  CASE sy-ucomm.
*   Back
    WHEN zcl_constants=>gc_ucomm_back.
      LEAVE TO SCREEN 0.

*   Exit
    WHEN zcl_constants=>gc_ucomm_exit.
      LEAVE TO SCREEN 0.

*   Cancel
    WHEN zcl_constants=>gc_ucomm_cancel.
      LEAVE PROGRAM.

  ENDCASE.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  STATUS_9000  OUTPUT
*&---------------------------------------------------------------------*
MODULE status_9000 OUTPUT.
*  SET PF-STATUS '* Set PF status and title bar
  SET PF-STATUS 'ZPF_ARN_NAT_UPD'.

  SET TITLEBAR 'ZTB_ARN_NAT_UPD'.
ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  TS_RECON_ACTIVE_TAB_GET  INPUT
*&---------------------------------------------------------------------*
MODULE ts_recon_active_tab_get INPUT.
  DATA lv_nodata TYPE abap_bool.
  gv_okcode = sy-ucomm.
  CASE gv_okcode.
    WHEN c_ts_recon-tab1.
      g_ts_recon-pressed_tab = c_ts_recon-tab1.
    WHEN c_ts_recon-tab2.
      g_ts_recon-pressed_tab = c_ts_recon-tab2.
*-- Stay on the same tab if no data selected
      CLEAR lv_nodata.
      PERFORM get_data_9002 CHANGING lv_nodata.
      IF lv_nodata EQ abap_true.
        g_ts_recon-pressed_tab = ts_recon-activetab.
      ENDIF.

    WHEN OTHERS.
      g_ts_recon-pressed_tab = ts_recon-activetab.
*&SPWIZARD:      DO NOTHING
  ENDCASE.
ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  TS_RECON_ACTIVE_TAB_SET  OUTPUT
*&---------------------------------------------------------------------*
MODULE ts_recon_active_tab_set OUTPUT.
  ts_recon-activetab = g_ts_recon-pressed_tab.
  CASE g_ts_recon-pressed_tab.
    WHEN c_ts_recon-tab1.
      g_ts_recon-subscreen = '9001'.
    WHEN c_ts_recon-tab2.
      g_ts_recon-subscreen = '9002'.

    WHEN OTHERS.
*&SPWIZARD:      DO NOTHING
  ENDCASE.
ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  DISPLAY_ALV_9002  OUTPUT
*&---------------------------------------------------------------------*
MODULE display_alv_9002 OUTPUT.

  PERFORM display_alv_9002.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  PAI_9001  INPUT
*&---------------------------------------------------------------------*
MODULE pai_9001 INPUT.

*-- Get the selected rows
  CALL METHOD go_alvgrid_9001->get_selected_rows
    IMPORTING
      et_index_rows = gt_rows_9001.
*    et_row_no     =


ENDMODULE.
