*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Functional Specification 3131
* DATE WRITTEN     # 01 03 2016
* SAP VERSION      # 731
* TYPE             # Function
* AUTHOR           # C90001448, Greg van der Loeff
*-----------------------------------------------------------------------
* TITLE            # AReNa: Send I-Care flag to Hybris
* PURPOSE          # AReNa: Send I-Care flag to Hybris
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 2019/05
* CHANGE No.       # CIP-621
* DESCRIPTION      # Enabling no commit - however in ASYNC we keep it empty
*                  # because we want the commit to happen
* WHO              # I90018154, Ivo Skolek
*-----------------------------------------------------------------------
FUNCTION z_hybris_set_async.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(ICARE) TYPE  ZARN_HYBRIS_SET_FIELDS_TAB
*"----------------------------------------------------------------------


  "IS 2019/05 Async Icaring of Online catalogue articles
  "is not used anywhere at the moment

  "If you want to use it, please make sure that following will work:

"- Z_HYBRIS_SET_ASYNC will do the inbound of national data (ZARN_INBOUND_DATA_PROCESSING)
" and register update FM to save the data (ZARN_INBOUND_DATA_UPDATE)
" potentially if everything is autoapproved it also registers defaulting of regional data
" in "background task" ZARN_REG_DATA_ENRICH_UPDATE - which will be executed after the update functions
" this needs the regional header to be available => make sure that ZARN_REG_HDR is saved and committed
" before calling this function



  PERFORM hybris_set USING icare
                           abap_true. "do commit

ENDFUNCTION.
