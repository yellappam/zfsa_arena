*&---------------------------------------------------------------------*
*&  Include           ZRMD_I_PRODUCT_INQUIRY_C01
*&---------------------------------------------------------------------*


*---------------------------------------------------------------------*
*       CLASS lcl_eventhandler DEFINITION
*---------------------------------------------------------------------*
*
*---------------------------------------------------------------------*
CLASS lcl_event_handler_edit DEFINITION.

  PUBLIC SECTION.
    METHODS:
      handle_hotspot_click
                    FOR EVENT hotspot_click OF cl_gui_alv_grid
        IMPORTING e_row_id e_column_id es_row_no sender,

      handle_close
                    FOR EVENT close OF cl_gui_dialogbox_container
        IMPORTING sender.


ENDCLASS.                    "lcl_eventhandler DEFINITION


*---------------------------------------------------------------------*
*       CLASS lcl_eventhandler IMPLEMENTATION
*---------------------------------------------------------------------*
*
*---------------------------------------------------------------------*
CLASS lcl_event_handler_edit IMPLEMENTATION.

  METHOD handle_hotspot_click.

    DATA: lo_grid TYPE REF TO cl_gui_alv_grid,
          ls_alv  TYPE zsmd_product_inquiry_details.

    lo_grid ?= sender.


*   Handle Checkbox like a radiobutton
    CASE sender.

      WHEN go_alvgrid_9001.

        CLEAR ls_alv.
        READ TABLE gt_alv INTO ls_alv INDEX e_row_id.
        IF sy-subrc = 0 AND ls_alv-matnr IS NOT INITIAL.
          PERFORM build_alink_data_9002 USING ls_alv-matnr.
          PERFORM display_alv_9002.
        ENDIF.
    ENDCASE.


  ENDMETHOD.                    "handle_hotspot_click

  METHOD handle_close.
* ^Handle the CLOSE-button of the dialogbox
* (the dialogbox is destroyed outomatically when the user
* switches to another dynpro).

    DATA: lo_root TYPE REF TO cx_root.

    CLEAR gt_alv_9002[].

    TRY.
        IF go_dialogbox_cont_9002 IS NOT INITIAL.
          CALL METHOD go_dialogbox_cont_9002->free
            EXCEPTIONS
              cntl_error        = 1
              cntl_system_error = 2
              OTHERS            = 3.
        ENDIF.
        FREE: go_dialogbox_cont_9002.
        FREE: go_alvgrid_9002.
      CATCH cx_root INTO lo_root.
    ENDTRY.
    SET USER-COMMAND 'ENTE'.



*    CALL METHOD go_dialogbox_cont_9002->free.
*    FREE go_dialogbox_cont_9002.
*
*    CALL METHOD go_alvgrid_9002->free.
*    FREE go_alvgrid_9002.
* closing the dialogbox leads to make it invisible.
* It is also conceivable to destroy it
* and recreate it if the user doubleclicks a line again.
* Displaying a great amount of data has a greater impact on performance.
  ENDMETHOD.                    "handle_close
ENDCLASS.                    "lcl_eventhandler IMPLEMENTATION


*---------------------------------------------------------------------*
*       Class relevant global declarations
*---------------------------------------------------------------------*
*
*---------------------------------------------------------------------*
DATA: go_event_handler_9001 TYPE REF TO lcl_event_handler_edit.
