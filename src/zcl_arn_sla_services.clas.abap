class ZCL_ARN_SLA_SERVICES definition
  public
  create public .

public section.

  class-methods CALC_DATE_DIFF
    importing
      !IV_FROM type DATUM
      !IV_TO type DATUM
      !IV_FACTORY_CALENDAR type WFCID
    returning
      value(RV_DAYS) type INT4 .
  class-methods GET_FACT_CAL
    returning
      value(RV_FACTORY_CALENDAR) type WFCID .
  class-methods GET_DEFAULT_POSTING_DATE
    returning
      value(RV_DATE) type D .
protected section.
private section.
ENDCLASS.



CLASS ZCL_ARN_SLA_SERVICES IMPLEMENTATION.


  METHOD calc_date_diff.
* Calculate date difference

    DATA: lt_dates TYPE TABLE OF rke_dat.

    CALL FUNCTION 'RKE_SELECT_FACTDAYS_FOR_PERIOD'
      EXPORTING
        i_datab               = iv_from
        i_datbi               = iv_to
        i_factid              = iv_factory_calendar
      TABLES
        eth_dats              = lt_dates
      EXCEPTIONS
        date_conversion_error = 1
        OTHERS                = 2.
    IF sy-subrc = 0.
      rv_days = lines( lt_dates ).
    ENDIF.

  ENDMETHOD.


  METHOD get_default_posting_date.
* Return default posting date
    TRY.
        data(lv_date) = zcl_arn_param_prov=>get( zif_arn_param_const=>c_sla_default_posting_date ).
        CALL FUNCTION 'CONVERT_DATE_TO_INTERNAL'
          EXPORTING
            date_external                  = lv_date
         IMPORTING
           DATE_INTERNAL                  = rv_date
         EXCEPTIONS
           DATE_EXTERNAL_IS_INVALID       = 1
           OTHERS                         = 2.
        IF sy-subrc <> 0.
* Implement suitable error handling here
        ENDIF.

      CATCH zcx_arn_param_err ##NO_HANDLER.
    ENDTRY.

  ENDMETHOD.


  METHOD get_fact_cal.
* Returns the ARENA factory calendar

    TRY.
        rv_factory_calendar = zcl_arn_param_prov=>get( zif_arn_param_const=>c_sla_fact_cal ).
      CATCH zcx_arn_param_err ##NO_HANDLER.
    ENDTRY.

  ENDMETHOD.
ENDCLASS.
