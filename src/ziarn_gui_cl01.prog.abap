*&---------------------------------------------------------------------*
*&  Include           ZIARN_GUI_CL01
*&---------------------------------------------------------------------*


*---------------------------------------------------------------------*
*       CLASS lcl_eventhandler DEFINITION
*---------------------------------------------------------------------*
*
*---------------------------------------------------------------------*
CLASS lcl_event_handler DEFINITION.

  PUBLIC SECTION.
    CLASS-METHODS:
      handle_toolbar
                  FOR EVENT toolbar OF cl_gui_alv_grid
        IMPORTING e_object e_interactive sender,

      handle_user_command
                  FOR EVENT user_command OF cl_gui_alv_grid
        IMPORTING e_ucomm sender,

      handle_hotspot_click FOR EVENT hotspot_click OF cl_gui_alv_grid
        IMPORTING e_row_id e_column_id es_row_no sender,

      handle_close
                  FOR EVENT close OF cl_gui_dialogbox_container
        IMPORTING sender,

      close_dialogbox
                  FOR EVENT close OF cl_gui_dialogbox_container                   "++3174 JKH 16.03.2017
        IMPORTING sender,

      handle_double_click
                  FOR EVENT double_click OF cl_gui_alv_grid
        IMPORTING e_row,

      on_toolbar
                  FOR EVENT toolbar OF cl_gui_alv_grid
        IMPORTING e_object,


      handle_data_change                                                            "++3174 JKH 21.03.2017
                  FOR EVENT data_changed OF cl_gui_alv_grid
        IMPORTING er_data_changed e_onf4 e_onf4_before e_onf4_after e_ucomm sender.

ENDCLASS.                    "lcl_eventhandler DEFINITION

*---------------------------------------------------------------------*
*       CLASS lcl_eventhandler IMPLEMENTATION
*---------------------------------------------------------------------*
*
*---------------------------------------------------------------------*
CLASS lcl_event_handler IMPLEMENTATION.

  METHOD handle_toolbar.
*   In event handler method for event TOOLBAR: Append own functions
*   by using event parameter E_OBJECT.
    DATA: ls_toolbar TYPE stb_button,
          lo_grid    TYPE REF TO cl_gui_alv_grid.

    lo_grid ?= sender.

    CASE sender.
      WHEN go_worklist_alv.

* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*   ICare Related Functions

        IF gv_sel_tab EQ gc_icare_tab.
*           gv_display IS INITIAL.
          CLEAR ls_toolbar.
          MOVE 0 TO ls_toolbar-butn_type.
          MOVE '[' TO ls_toolbar-text.
          MOVE abap_true TO ls_toolbar-disabled.
          APPEND ls_toolbar TO e_object->mt_toolbar.

          CLEAR ls_toolbar.
          MOVE 'SELECT' TO ls_toolbar-function.
          MOVE icon_select_all TO ls_toolbar-icon.
          MOVE 'Select all'(017) TO ls_toolbar-quickinfo.
          MOVE ' ' TO ls_toolbar-disabled.
          APPEND ls_toolbar TO e_object->mt_toolbar.

          CLEAR ls_toolbar.
          MOVE 'DESELECT' TO ls_toolbar-function.
          MOVE icon_deselect_all TO ls_toolbar-icon.
          MOVE 'De Select all'(018) TO ls_toolbar-quickinfo.
          MOVE ' ' TO ls_toolbar-disabled.
          APPEND ls_toolbar TO e_object->mt_toolbar.

          CLEAR ls_toolbar.
          MOVE 'ICARE' TO ls_toolbar-function.
          MOVE icon_complete TO ls_toolbar-icon.
          MOVE 'I Care Select'(019) TO ls_toolbar-quickinfo.
          MOVE '  I Care Flag'(021) TO ls_toolbar-text.
          MOVE ' ' TO ls_toolbar-disabled.
          APPEND ls_toolbar TO e_object->mt_toolbar.

* INS Begin of Change 3174 JKH 22.03.2017
          IF gv_cr_r3_switch = abap_true.
            CLEAR ls_toolbar.
            MOVE 'REREQ' TO ls_toolbar-function.
            MOVE icon_system_redo TO ls_toolbar-icon.
            MOVE 'Repeat Enrichment Request' TO ls_toolbar-quickinfo.
            MOVE 'Re-Request' TO ls_toolbar-text.

            IF gv_rereq_auth = abap_true.
              MOVE ' ' TO ls_toolbar-disabled.
            ELSE.
              MOVE abap_true TO ls_toolbar-disabled.
            ENDIF.

            APPEND ls_toolbar TO e_object->mt_toolbar.
          ENDIF.  " IF gv_cr_r3_switch = abap_true
* INS End of Change 3174 JKH 22.03.2017

* INS Begin of Change 3174 JKH 13.03.2017
          IF gv_cr_r1_switch = abap_true.
            CLEAR ls_toolbar.
            MOVE 'SMORE' TO ls_toolbar-function.
            MOVE icon_next_page TO ls_toolbar-icon.
            MOVE 'Search more National Products' TO ls_toolbar-quickinfo.
            MOVE 'Search More' TO ls_toolbar-text.

            IF gv_smore_disable = abap_true.
              MOVE abap_true TO ls_toolbar-disabled.
            ELSE.
              MOVE ' ' TO ls_toolbar-disabled.
            ENDIF.
            APPEND ls_toolbar TO e_object->mt_toolbar.

            CLEAR ls_toolbar.
            MOVE 'VLOG' TO ls_toolbar-function.
            MOVE icon_display_text TO ls_toolbar-icon.
            MOVE 'View Error Log' TO ls_toolbar-quickinfo.
            MOVE 'Log' TO ls_toolbar-text.

*            IF gv_valerr_cnt GT 0 OR gv_error_cnt GT 0.
*            IF gv_vlog_enable = abap_true.
            IF gt_vlog[] IS NOT INITIAL.
              MOVE ' ' TO ls_toolbar-disabled.
            ELSE.
              MOVE abap_true TO ls_toolbar-disabled.
            ENDIF.
            APPEND ls_toolbar TO e_object->mt_toolbar.
          ENDIF.
* INS End of Change 3174 JKH 13.03.2017

          CLEAR ls_toolbar.
          MOVE 0 TO ls_toolbar-butn_type.
          MOVE ']' TO ls_toolbar-text.
          MOVE abap_true TO ls_toolbar-disabled.
          APPEND ls_toolbar TO e_object->mt_toolbar.
        ELSE.

*     AReNa Related Functions
          IF  gv_sel_tab EQ gc_arena_tab." AND
            CLEAR ls_toolbar.
            MOVE 3 TO ls_toolbar-butn_type.
            APPEND ls_toolbar TO e_object->mt_toolbar.
          ENDIF.

        ENDIF.

**   only for AReNa Enrich or Approvals
        CLEAR ls_toolbar.
        MOVE 3 TO ls_toolbar-butn_type.
        APPEND ls_toolbar TO e_object->mt_toolbar.
*
*     display/change
        IF ( gv_sel_tab EQ gc_arena_tab
        OR gv_sel_tab EQ gc_appro_tab )
       "IS: Display Only Authorisations
        AND gv_auth_display_only IS INITIAL.

          CLEAR ls_toolbar.
          MOVE 'EXECUTE' TO ls_toolbar-function.
          MOVE icon_wizard TO ls_toolbar-icon.
          MOVE 'Enrich Regional Data'(027) TO ls_toolbar-quickinfo.
          MOVE 'Enrich Data'(028) TO ls_toolbar-text.
          MOVE ' ' TO ls_toolbar-disabled.
          IF gv_approval_data EQ abap_true.
            MOVE icon_wizard TO ls_toolbar-icon.
            MOVE 'Approve Data'(a27) TO ls_toolbar-quickinfo.
            MOVE 'Approve Data'(a28) TO ls_toolbar-text.
          ENDIF.
          APPEND ls_toolbar TO e_object->mt_toolbar.
        ENDIF.

        IF gv_sel_tab EQ gc_arena_tab
        OR gv_sel_tab EQ gc_icare_tab
        "IS: Display Only Authorisations
        " - give the option to display only on Approval Tab as well
        OR gv_sel_tab EQ gc_appro_tab.
          CLEAR ls_toolbar.
          MOVE 'DISPLAY' TO ls_toolbar-function.
          MOVE icon_display TO ls_toolbar-icon.
          MOVE 'Display Regional Data' TO ls_toolbar-quickinfo.
          MOVE 'Display Data' TO ls_toolbar-text.
          MOVE ' ' TO ls_toolbar-disabled.
          APPEND ls_toolbar TO e_object->mt_toolbar.
        ENDIF.


* Mass Update Regional Data
        IF gv_sel_tab EQ gc_arena_tab
       "IS: Display Only Authorisations
        AND gv_auth_display_only IS INITIAL.
          CLEAR ls_toolbar.
          MOVE 'MASS_UPD' TO ls_toolbar-function.
          MOVE icon_mass_change TO ls_toolbar-icon.
          MOVE 'Mass Update Regional Data' TO ls_toolbar-quickinfo.
          MOVE 'Mass Update' TO ls_toolbar-text.
          MOVE ' ' TO ls_toolbar-disabled.
          APPEND ls_toolbar TO e_object->mt_toolbar.
        ENDIF.


* Mass Approval
        IF ( gv_sel_tab EQ gc_arena_tab OR gv_sel_tab EQ gc_appro_tab )
       "IS: Display Only Authorisations
        AND gv_auth_display_only IS INITIAL.
          CLEAR ls_toolbar.
          MOVE 'MASS_APPR' TO ls_toolbar-function.
          MOVE icon_allow TO ls_toolbar-icon.
          MOVE 'Mass Approvals' TO ls_toolbar-quickinfo.
          MOVE 'Mass Approvals' TO ls_toolbar-text.
          MOVE ' ' TO ls_toolbar-disabled.
          APPEND ls_toolbar TO e_object->mt_toolbar.
        ENDIF.


      WHEN go_reg_rb_alv.
        CLEAR ls_toolbar.
        MOVE 3 TO ls_toolbar-butn_type.
        APPEND ls_toolbar TO e_object->mt_toolbar.
*
        CLEAR ls_toolbar.
        MOVE 'DELETE' TO ls_toolbar-function.
        MOVE icon_delete TO ls_toolbar-icon.
        MOVE 'Delete' TO ls_toolbar-quickinfo.
        MOVE 'Delete' TO ls_toolbar-text.

*     display/change
        IF gv_display IS INITIAL.
          MOVE ' ' TO ls_toolbar-disabled.
        ELSE.
          MOVE 'X' TO ls_toolbar-disabled.
        ENDIF.

        APPEND ls_toolbar TO e_object->mt_toolbar.


      WHEN go_reg_con_rrp_alv.
        CLEAR ls_toolbar.
        MOVE 3 TO ls_toolbar-butn_type.
        APPEND ls_toolbar TO e_object->mt_toolbar.
*
        CLEAR ls_toolbar.
        MOVE 'ADD_RRP' TO ls_toolbar-function.
        MOVE icon_add_row TO ls_toolbar-icon.
        MOVE 'Add' TO ls_toolbar-quickinfo.
        MOVE 'Add' TO ls_toolbar-text.

*     display/change
        IF gv_display IS INITIAL.
          MOVE ' ' TO ls_toolbar-disabled.
        ELSE.
          MOVE 'X' TO ls_toolbar-disabled.
        ENDIF.

        APPEND ls_toolbar TO e_object->mt_toolbar.

      WHEN go_reg_con_dis_alv.
        CLEAR ls_toolbar.
        MOVE 3 TO ls_toolbar-butn_type.
        APPEND ls_toolbar TO e_object->mt_toolbar.
*
        CLEAR ls_toolbar.
        MOVE 'ADD_TER' TO ls_toolbar-function.
        MOVE icon_add_row TO ls_toolbar-icon.
        MOVE 'Add' TO ls_toolbar-quickinfo.
        MOVE 'Add' TO ls_toolbar-text.

*     display/change
        IF gv_display IS INITIAL.
          MOVE ' ' TO ls_toolbar-disabled.
        ELSE.
          MOVE 'X' TO ls_toolbar-disabled.
        ENDIF.

        APPEND ls_toolbar TO e_object->mt_toolbar.

      WHEN OTHERS.

    ENDCASE.


  ENDMETHOD.                    "handle_toolbar
*---------------------------------------------------------------------
  METHOD handle_user_command.
* At event USER_COMMAND query the function code of each function

    DATA:
      lt_rows TYPE lvc_t_row,
      ls_rows LIKE LINE OF lt_rows,
      lo_grid TYPE REF TO cl_gui_alv_grid.

    gv_toolbar_ucomm = e_ucomm.

*   to get the sender for identification
    lo_grid ?= sender.

    CASE sender.
      WHEN go_worklist_alv.

        CLEAR: lt_rows[].
*   get selected row
        CALL METHOD go_worklist_alv->get_selected_rows
          IMPORTING
            et_index_rows = lt_rows.

        CALL METHOD cl_gui_cfw=>flush.

*   used in worklist selection later on
        gt_rows = lt_rows.

*   ALV Worklist Processing
        CASE e_ucomm.
          WHEN 'SELECT'.
            PERFORM select_all_entries CHANGING gt_worklist[].
*___________________________________________________________________
          WHEN 'DESELECT'.
            PERFORM deselect_all_entries CHANGING gt_worklist[].
*___________________________________________________________________
          WHEN 'COPY'.
*       Check if the selection contains locked entries ?
            LOOP AT lt_rows INTO ls_rows.
              READ TABLE gt_worklist INTO gs_worklist INDEX ls_rows-index.

              IF sy-subrc             IS INITIAL      AND
                 gs_worklist-rowcolor EQ gc_alv_locked.
*           message
                MESSAGE i000 WITH TEXT-032.
                CLEAR gt_rows.
                EXIT.
              ENDIF.
            ENDLOOP.

*       read selected articles from worklist
            IF gt_rows[] IS NOT INITIAL.

              PERFORM alv_copy_article_details  USING    gt_rows
                                            CHANGING gs_reg_data_copy.
              IF gs_reg_data_copy IS NOT INITIAL.
                PERFORM process_worklist_selection
                  USING gt_rows.
*           resize the docking container
                CALL METHOD go_docking_alv->set_extension
                  EXPORTING
                    extension = gc_extension.
                gv_extension =  gc_extension.
                gv_text_size-text      = TEXT-i02.
                gv_text_size-icon_text = TEXT-i02.
*           This is to refresh dynpro
                cl_gui_cfw=>set_new_ok_code( new_code = 'DUMMY').
              ENDIF.
            ELSE.
              MESSAGE i012.
              RETURN.
            ENDIF.
*___________________________________________________________________
          WHEN 'LOGS'.
*       Call change document report with selection
            PERFORM change_documents_display
              USING gt_rows zarn_products-idno sy-ucomm.
*___________________________________________________________________
          WHEN 'EXECUTE'.
            gv_set_default_cursor = abap_true.
            gv_display_default = abap_false.
            gv_display = gv_display_default.
*        get selected values from table
            IF lt_rows[] IS NOT INITIAL.
              g_ts_main-pressed_tab = 'TS_MAIN_FC2'.
              PERFORM lock_regional_data  USING gt_rows abap_true.
              PERFORM process_worklist_selection USING gt_rows.
*         resize the docking container
              CALL METHOD go_docking_alv->set_extension
                EXPORTING
                  extension = gc_extension.
              gv_extension = gc_extension.
              gv_execute = abap_true.
              gv_text_size-text      = TEXT-i02.
              gv_text_size-icon_text = TEXT-i02.
*         Refresh display
              CALL METHOD go_worklist_alv->refresh_table_display.
*         This is to refresh dynpro
              cl_gui_cfw=>set_new_ok_code( new_code = 'DUMMY').
              lcl_controller=>get_instance( )->set_display( iv_display = gv_display ).
            ELSE.
              MESSAGE i012.
              RETURN.
            ENDIF.
*___________________________________________________________________
          WHEN 'DISPLAY'.
            gv_set_default_cursor = abap_true.
            gv_display_default = abap_true.
            gv_display = gv_display_default.
*        get selected values from table
            IF lt_rows[] IS NOT INITIAL.
              ">>>IS190213 CHARM 9000004888 - default tab "regional" for display mode
              "g_ts_main-pressed_tab = 'TS_MAIN_FC1'.
              g_ts_main-pressed_tab = 'TS_MAIN_FC2'.
              "<<<IS190213 CHARM 9000004888 - default tab "regional" for display mode
*              PERFORM lock_regional_data  USING gt_rows.
              PERFORM process_worklist_selection USING gt_rows.
*         resize the docking container
              CALL METHOD go_docking_alv->set_extension
                EXPORTING
                  extension = gc_extension.
              gv_extension = gc_extension.
              gv_execute = abap_true.
              gv_text_size-text      = TEXT-i02.
              gv_text_size-icon_text = TEXT-i02.
*         Refresh display
              CALL METHOD go_worklist_alv->refresh_table_display.
*         This is to refresh dynpro
              cl_gui_cfw=>set_new_ok_code( new_code = 'DUMMY').
            ELSE.
              MESSAGE i012.
              RETURN.
            ENDIF.
*___________________________________________________________________
          WHEN 'ICARE'.
            PERFORM icare_activation.

          WHEN 'REREQ'.
            PERFORM repeat_enrichment_process_r3 USING gt_rows.                "++3174 JKH 22.03.2017

          WHEN 'SMORE'.
            PERFORM search_more_r1_lean_paging.                                "++3174 JKH 13.03.2017

          WHEN 'VLOG'.
            PERFORM display_vlog_r1_lean_search.                               "++3174 JKH 13.03.2017

          WHEN 'MASS_UPD'.
* Mass Regional Update
            PERFORM mass_regional_update USING gt_rows.

**          WHEN 'MASS_CAT_ASS'.
*** Mass Category Assign
**            PERFORM mass_category_assign.

          WHEN 'MASS_APPR'.

            IF gt_rows[] IS INITIAL.
              MESSAGE i012.
              RETURN.
            ELSE.
* Mass Approvals
              PERFORM mass_approvals USING gt_rows.
            ENDIF.




        ENDCASE.

      WHEN go_reg_rb_alv.
*   get selected row
        CALL METHOD go_reg_rb_alv->get_selected_rows
          IMPORTING
            et_index_rows = lt_rows.

        CHECK lt_rows[] IS NOT INITIAL.

        CASE e_ucomm.
          WHEN 'DELETE'.
            PERFORM delete_banner USING lt_rows.

          WHEN OTHERS.

        ENDCASE.


      WHEN go_reg_con_rrp_alv.
        CASE e_ucomm.
          WHEN 'ADD_RRP'.
*   get selected row in banner table
            CALL METHOD go_reg_rb_alv->get_selected_rows
              IMPORTING
                et_index_rows = lt_rows.
            IF lt_rows[] IS INITIAL.
              MESSAGE s058 DISPLAY LIKE 'E'.
              RETURN.
            ENDIF.

            PERFORM alv_add_banner_info USING lt_rows go_reg_con_rrp_alv.

          WHEN OTHERS.

        ENDCASE.

      WHEN go_reg_con_dis_alv.
        CASE e_ucomm.
          WHEN 'ADD_TER'.
*   get selected row in banner table
            CALL METHOD go_reg_rb_alv->get_selected_rows
              IMPORTING
                et_index_rows = lt_rows.

            IF lt_rows[] IS INITIAL.
              MESSAGE s058 DISPLAY LIKE 'E'.
              RETURN.
            ENDIF.

            PERFORM alv_add_banner_info USING lt_rows go_reg_con_dis_alv.
          WHEN OTHERS.

        ENDCASE.

      WHEN OTHERS.

    ENDCASE.

    CALL METHOD cl_gui_cfw=>flush.

  ENDMETHOD.                           "handle_user_command
*---------------------------------------------------------------------
  METHOD handle_hotspot_click.

    PERFORM show_multi_gtin.

  ENDMETHOD.                    "handle_hotspot_click
*---------------------------------------------------------------------
  METHOD handle_close.
* ^Handle the CLOSE-button of the dialogbox
* (the dialogbox is destroyed outomatically when the user
* switches to another dynpro).
    CALL METHOD go_dialogbox_cont_gtin->free.
    FREE go_dialogbox_cont_gtin.
* closing the dialogbox leads to make it invisible.
* It is also conceivable to destroy it
* and recreate it if the user doubleclicks a line again.
* Displaying a great amount of data has a greater impact on performance.
  ENDMETHOD.                    "handle_close
*---------------------------------------------------------------------

  METHOD close_dialogbox.                             "++3174 JKH 16.03.2017
    IF sender IS NOT INITIAL.
      CALL METHOD sender->free.
    ENDIF.
  ENDMETHOD.                    "close_dialogbox


  METHOD handle_double_click.

    CHECK e_row-index GT 0.

    DATA ls_zarn_pir LIKE LINE OF gt_zarn_pir.
    DATA ls_zarn_pir_comm_ch LIKE LINE OF gt_zarn_pir_comm_ch.

    READ TABLE gt_zarn_pir INTO ls_zarn_pir INDEX e_row-index.
    IF sy-subrc = 0.
      FREE gt_zarn_pir_comm_ui[].
      LOOP AT gt_zarn_pir_comm_ch INTO ls_zarn_pir_comm_ch
        WHERE idno = ls_zarn_pir-idno AND uom_code = ls_zarn_pir-uom_code
        AND hybris_internal_code = ls_zarn_pir-hybris_internal_code.

        INSERT ls_zarn_pir_comm_ch INTO TABLE gt_zarn_pir_comm_ui.

      ENDLOOP.
      CALL METHOD go_nat_comm_ch_alv->refresh_table_display.

    ENDIF.

  ENDMETHOD.                    "handle_close


  METHOD on_toolbar.

    FIELD-SYMBOLS:
      <fs_toolbar> TYPE LINE OF ttb_button.

    IF gv_toolbar_ucomm EQ 'EXECUTE' OR sy-ucomm EQ 'DISPLAY'
      OR gv_toolbar_ucomm EQ 'COPY'.

      READ TABLE e_object->mt_toolbar ASSIGNING <fs_toolbar>
        WITH KEY function = 'EXECUTE'.
      IF sy-subrc EQ 0.
        <fs_toolbar>-disabled = abap_true.
      ENDIF.

      UNASSIGN <fs_toolbar>.

      READ TABLE e_object->mt_toolbar ASSIGNING <fs_toolbar>
        WITH KEY function = 'DISPLAY'.
      IF sy-subrc EQ 0.
        <fs_toolbar>-disabled = abap_true.
      ENDIF.

      READ TABLE e_object->mt_toolbar ASSIGNING <fs_toolbar>
        WITH KEY function = 'COPY'.
      IF sy-subrc EQ 0.
        <fs_toolbar>-disabled = abap_true.
      ENDIF.

*
*      CLEAR gv_toolbar_ucomm.
    ENDIF.


  ENDMETHOD.                    "handle_close


  METHOD handle_data_change.

    DATA: lo_grid TYPE REF TO cl_gui_alv_grid.
    lo_grid ?= sender.

    CASE sender.
      WHEN go_worklist_alv.
        PERFORM manage_worklist_data_change CHANGING er_data_changed.

    ENDCASE.
  ENDMETHOD.                    "handle_data_change

ENDCLASS.                    "lcl_eventhandler IMPLEMENTATION

DATA:
  go_event_receiver TYPE REF TO lcl_event_handler.

*---------------------------------------------------------------------
*&---------------------------------------------------------------------*
*&      Form  SELECT_ALL_ENTRIES
*&---------------------------------------------------------------------*
FORM select_all_entries CHANGING pt_outtab TYPE STANDARD TABLE.
  DATA: ls_outtab TYPE ty_outtab,
        l_valid   TYPE c,
        l_locked  TYPE c.


  CALL METHOD go_worklist_alv->check_changed_data
    IMPORTING
      e_valid = l_valid.

  IF l_valid IS NOT INITIAL.

    LOOP AT pt_outtab INTO ls_outtab.
      PERFORM check_lock USING    ls_outtab
                         CHANGING l_locked.
      IF l_locked IS INITIAL AND
         NOT ls_outtab-checkbox EQ abap_undefined.
        ls_outtab-checkbox = abap_true.
      ENDIF.
      MODIFY pt_outtab FROM ls_outtab.
    ENDLOOP.

    CALL METHOD go_worklist_alv->refresh_table_display.

  ENDIF.

ENDFORM.                    " SELECT_ALL_ENTRIES
*&---------------------------------------------------------------------*
*&      Form  CHECK_LOCK
*&---------------------------------------------------------------------*
FORM check_lock USING    ps_outtab TYPE ty_outtab
                CHANGING p_locked.
  DATA ls_celltab TYPE lvc_s_styl.

  LOOP AT ps_outtab-celltab INTO ls_celltab.
    IF ls_celltab-fieldname = gc_field_chkbox. " 'CHECKBOX'.
      IF ls_celltab-style EQ cl_gui_alv_grid=>mc_style_disabled.
        p_locked = abap_true.
      ELSE.
        p_locked = space.
      ENDIF.
    ENDIF.
  ENDLOOP.
ENDFORM.                    " CHECK_LOCK
*&---------------------------------------------------------------------*
*&      Form  DESELECT_ALL_ENTRIES
*&---------------------------------------------------------------------*
FORM deselect_all_entries CHANGING pt_outtab TYPE STANDARD TABLE.
  DATA: ls_outtab TYPE ty_outtab,
        l_valid   TYPE c,
        l_locked  TYPE c.


  CALL METHOD go_worklist_alv->check_changed_data
    IMPORTING
      e_valid = l_valid.

  IF l_valid IS NOT INITIAL.

    LOOP AT pt_outtab INTO ls_outtab.
      PERFORM check_lock USING    ls_outtab
                       CHANGING l_locked.
      IF l_locked IS INITIAL
         AND NOT ls_outtab-checkbox EQ abap_undefined.
        ls_outtab-checkbox = abap_false.
      ENDIF.

      MODIFY pt_outtab FROM ls_outtab.
    ENDLOOP.

    CALL METHOD go_worklist_alv->refresh_table_display.

  ENDIF.



ENDFORM.                    " DESELECT_ALL_ENTRIES
*&---------------------------------------------------------------------*
*&      Form  icare_activation
*&---------------------------------------------------------------------*
FORM icare_activation.

  DATA:
    ls_worklist        TYPE ty_outtab,
*    ls_rows     LIKE LINE OF pt_rows,
    lv_valid           TYPE c,
    ls_reg_hdr         TYPE zarn_reg_hdr,
    lt_reg_data        TYPE ztarn_reg_data,
    ls_reg_data        TYPE zsarn_reg_data,
    lt_hybris          TYPE zarn_hybris_set_fields_tab,
    ls_hybris          LIKE LINE OF lt_hybris,
    lv_release_status  TYPE zarn_rel_status,
*>>>IS1609ins Feature 3162 - AReNa Approval Stabilisation
    lt_idno_noautoappr TYPE ztarn_idno_key,
*<<<IS1609ins Feature 3162 - AReNa Approval Stabilisation
    lt_messages        TYPE bal_t_msg,                                             "++3174 JKH 24.03.2017
    lt_vlog            TYPE ty_t_vlog,                                             "++3174 JKH 24.03.2017
    lv_r3_sucs         TYPE flag,                                                  "++3174 JKH 24.03.2017
    lv_tabix           TYPE sy-tabix.

* check for changes on ALv
  CALL METHOD go_worklist_alv->check_changed_data
    IMPORTING
      e_valid = lv_valid.

* if changed
  IF lv_valid IS NOT INITIAL.

* INS Begin of Change 3174 JKH 17.03.2017
* If CR Switch is ON then I-Care through R3 Service
    IF gv_cr_r3_switch = abap_true.

      READ TABLE gt_worklist[] TRANSPORTING NO FIELDS
          WITH KEY fsni_icare_value = space   " ICare not done
                   checkbox         = abap_true   " ICare requested on ALV
                   catalog_version  = gc_staged.
      IF sy-subrc = 0.
        CALL FUNCTION 'SAPGUI_PROGRESS_INDICATOR'
          EXPORTING
            percentage = 25
            text       = 'Requesting Staged data for Enrichment'.
      ENDIF.

      CLEAR lv_r3_sucs.
      PERFORM icare_activation_r3 USING gt_md_category[]
                               CHANGING gt_worklist[]
                                        gt_reg_data[]
                                        lt_messages[]
                                        lt_vlog[]
                                        lv_r3_sucs.
    ENDIF.
* INS End of Change 3174 JKH 17.03.2017





*   at rows where
    LOOP AT gt_worklist INTO ls_worklist
      WHERE fsni_icare_value IS     INITIAL AND   " ICare not done
            checkbox         IS NOT INITIAL. "AND   " ICare requested on ALV
*            catalog_version  NE gc_staged.                                    "++3174 JKH 21.03.2017

      IF sy-tabix = 1.
        CALL FUNCTION 'SAPGUI_PROGRESS_INDICATOR'
          EXPORTING
            percentage = 50
            text       = 'I-Care Online data for Enrichment'.
      ENDIF.

*     Is this a new I-Care or selected ?
      lv_tabix = sy-tabix.
      READ TABLE gt_worklist_orig ASSIGNING FIELD-SYMBOL(<ls_worklist_orig>) WITH KEY idno = ls_worklist-idno.
      IF sy-subrc EQ 0.
        IF <ls_worklist_orig>-checkbox IS NOT INITIAL.
*         This was selected as already I-Care so do not process unless it was selected
          READ TABLE gt_rows TRANSPORTING NO FIELDS WITH KEY index = lv_tabix.
          IF sy-subrc NE 0.
*           Not a new I-Care and not selected
            CONTINUE.
          ENDIF.
        ENDIF.
      ENDIF.


*     Frist check that a category manager has been assigned
      IF ( ls_worklist-gil_zzcatman IS INITIAL   AND
           ls_worklist-ret_zzcatman IS INITIAL ) OR
           ls_worklist-initiation IS INITIAL.
        MESSAGE i000 WITH TEXT-035  ls_worklist-fan_id.
        EXIT.
      ENDIF.

*     collect rows that require ICare
*     get ICare
      ls_hybris-idno  =  ls_worklist-idno.
      ls_hybris-gtin  =  ls_worklist-gtin_code.
      ls_hybris-icare =  ls_worklist-checkbox.
      APPEND ls_hybris TO lt_hybris.
      CLEAR ls_hybris.

*     build Regional header table
      ls_reg_hdr-mandt        = sy-mandt.
      ls_reg_hdr-idno         = ls_worklist-idno.
      ls_reg_hdr-version      = ls_worklist-version.
      ls_reg_hdr-status       = gc_status_new.
      ls_reg_hdr-gil_zzcatman = ls_worklist-gil_zzcatman.
      ls_reg_hdr-ret_zzcatman = ls_worklist-ret_zzcatman.
      ls_reg_hdr-initiation   = ls_worklist-initiation.
      ls_reg_hdr-ersda        = sy-datum.
      ls_reg_hdr-erzet        = sy-uzeit.
      ls_reg_hdr-ernam        = sy-uname.



      CLEAR lv_release_status.


      ls_reg_data-idno        = ls_worklist-idno.

      APPEND:
         ls_reg_hdr  TO ls_reg_data-zarn_reg_hdr,
         ls_reg_data TO lt_reg_data.

      CLEAR:
         ls_reg_hdr,
         ls_reg_data.
    ENDLOOP.


    IF lt_hybris IS NOT INITIAL.

      "IS 2019/05 Fixing the LUW logic for synchronous I-caring

      "- Z_HYBRIS_SET will do the inbound of national data (ZARN_INBOUND_DATA_PROCESSING)
      " and register update FM to save the data (ZARN_INBOUND_DATA_UPDATE)
      " potentially if everything is autoapproved it also registers defaulting of regional data
      " in "background task" ZARN_REG_DATA_ENRICH_UPDATE - which will be executed after the update functions
      " this needs the regional header to be available => we register save of regional data ZARN_REGIONAL_DB_UPDATE
      " in update task as well. So that after we finally do the COMMIT WORK the sequence will be
      " 1) ZARN_INBOUND_DATA_UPDATE (update task) - saves national data
      " 2) ZARN_REGIONAL_DB_UPDATE (update task) - saves regional header
      " 3) ZARN_REG_DATA_ENRICH_UPDATE (background task) - defaults regional data from national data

      CALL FUNCTION 'Z_HYBRIS_SET'
        EXPORTING
          icare                = lt_hybris
          iv_commit            = abap_false   "IS 2019/05 enable no commit
        EXCEPTIONS
          hybris_not_available = 1
          OTHERS               = 2.
      IF sy-subrc IS NOT INITIAL.
*       Implement suitable error handling here
        MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                   WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
      ENDIF.

      CLEAR lt_idno_noautoappr[].
      lt_idno_noautoappr[] = CORRESPONDING #( lt_reg_data ).

*     and update ZARN_REG_HDR and ZARN_REG_BANNER
      CALL FUNCTION 'ZARN_REGIONAL_DB_UPDATE'
        IN UPDATE TASK                      "IS 2019/05 call in update task (before it wasnt)
        EXPORTING
          it_reg_data       = lt_reg_data
          "explicitly say, don't do regional autoapprovals at this stage
          it_idno_val_error = lt_idno_noautoappr[].

      "IS 2019/05 commit work and trigger sequence described above
      COMMIT WORK AND WAIT.

      MESSAGE s000 WITH TEXT-037 TEXT-038.
*     and EXIT to Selection screen
      SET SCREEN 0. LEAVE SCREEN.

    ENDIF.

* INS Begin of Change 3174 JKH 24.03.2017
    IF lt_vlog[] IS NOT INITIAL.
      PERFORM display_vlog_r1_lean_search.
    ENDIF.


    IF lv_r3_sucs = abap_true.
      MESSAGE s000 WITH TEXT-037 TEXT-038.
*     and EXIT to Selection screen
      SET SCREEN 0. LEAVE SCREEN.
    ENDIF.
* INS End of Change 3174 JKH 24.03.2017


**
    CALL METHOD go_worklist_alv->refresh_table_display.

  ENDIF.


ENDFORM.                               " switch_activation
