*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 05.05.2016 at 15:45:08 by user C90001363
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_PRICEREAS_T................................*
DATA:  BEGIN OF STATUS_ZARN_PRICEREAS_T              .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_PRICEREAS_T              .
CONTROLS: TCTRL_ZARN_PRICEREAS_T
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_PRICEREAS_T              .
TABLES: ZARN_PRICEREAS_T               .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
