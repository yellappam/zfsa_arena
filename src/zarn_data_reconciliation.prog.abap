*&---------------------------------------------------------------------*
*& Report  ZARN_DATA_RECONCILIATION
*&
*&---------------------------------------------------------------------*
*&---------------------------------------------------------------------*
* PROJECT          # FSA
* SPECIFICATION    # 3151
* DATE WRITTEN     # 01.06.2016
* SYSTEM           # DE1
* SAP VERSION      # ECC6.0
* TYPE             # Report
* AUTHOR           # C90001363
*-----------------------------------------------------------------------
* TITLE            # Data Reconciliation
* PURPOSE          # To analyze data for AReNa articles if they existed
*                  # in National/Regional Data.
*                  # Analyse National/Regional Data after Regional sync up
*                  # Analyse National/Regional Data after first edit
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      #
*-----------------------------------------------------------------------
* CALLS TO         #
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 24.04.2018
* CHANGE No.       # ChaRM DE0K917906; JIRA CI18-51 Pay By Scan Changes
* DESCRIPTION      # Pay by Scan should be set not only at the banner
*                  # level but also at the individual store level. The
*                  # previous implementation was to allow it to be set
*                  # only at the banner level on MVKE-MVGR3. The new
*                  # implementation is to add custom field ZZ_PBS to MARC.
*                  # 1. Removed *MATL_GRP_3 related logic
*                  # 2. Added logic for *ZZ_PBS.
* WHO              # I00600343, Fengyu Li
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

REPORT zarn_data_reconciliation MESSAGE-ID zarena_msg.



* Constant declarations
INCLUDE ziarn_data_reconciliation_cons.

* Top Include for Declarations
INCLUDE ziarn_data_reconciliation_top.

* Selection Screen
INCLUDE ziarn_data_reconciliation_sel.

* Main Processing Logic
INCLUDE ziarn_data_reconciliation_main.

* Subroutines Main
INCLUDE ziarn_data_reconciliation_sub.

* Subroutines Screens
INCLUDE ziarn_data_reconciliation_sub2.

* PBO
INCLUDE ziarn_data_reconciliation_pbo.

* PAI
INCLUDE ziarn_data_reconciliation_pai.
