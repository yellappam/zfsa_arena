*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_REG_DEFAULT_CL_DEF
*&---------------------------------------------------------------------*

*&---------------------------------------------------------------------*
*&  Include           ZIMD_ARENA_TO_ADC_CL_DEF
*&---------------------------------------------------------------------*

CLASS lcx_error DEFINITION FINAL
                INHERITING FROM cx_static_check.

  PUBLIC SECTION.
    DATA: mv_text TYPE string    ##NEEDED.

    METHODS: constructor IMPORTING iv_text     TYPE string OPTIONAL
                                   iv_previous LIKE previous OPTIONAL.

ENDCLASS.  " lcx_error


CLASS lcl_main DEFINITION FINAL.
  PUBLIC SECTION.

    TYPES:
* Selection Params
      BEGIN OF ty_sel_params,
        s_idno TYPE ztarn_idno_range,
        p_test TYPE flag,
      END OF ty_sel_params,

* Log
      BEGIN OF ty_s_log,
        idno    TYPE zarn_idno,
        msgty   TYPE sy-msgty,
        message TYPE char50,
      END OF ty_s_log,
      ty_t_log       TYPE STANDARD TABLE OF ty_s_log,

* IDNO Data
      ty_s_idno_data TYPE zsarn_idno_data,
      ty_t_idno_data TYPE SORTED TABLE OF ty_s_idno_data WITH UNIQUE KEY idno,

* National Data
      ty_s_prod_data TYPE zsarn_prod_data,
      ty_t_prod_data TYPE ztarn_prod_data,

* Regional Data
      ty_s_reg_data  TYPE zsarn_reg_data,
      ty_t_reg_data  TYPE ztarn_reg_data.

    DATA:       mt_log          TYPE ty_t_log.

    METHODS constructor IMPORTING is_sel_params TYPE ty_sel_params OPTIONAL.
    METHODS process RAISING lcx_error.


  PRIVATE SECTION.


    DATA:
      ms_sel_params   TYPE ty_sel_params,
      mt_idno_data    TYPE ty_t_idno_data,
      mt_approval     TYPE zarn_t_approval,
      mt_prod_data    TYPE ty_t_prod_data,
      mt_reg_data     TYPE ty_t_reg_data,
      mt_reg_data_def TYPE ty_t_reg_data.


    METHODS get_idno_data.
    METHODS validate_idno_and_locking.
    METHODS get_nat_reg_data.
    METHODS default_and_save_reg_data.


ENDCLASS.  " lcl_main
