FUNCTION-POOL zarn_mass.                    "MESSAGE-ID ..

* INCLUDE LZARN_MASSD...                     " Local class definition


CONSTANTS: gc_user_command_ref TYPE string VALUE '(SAPLMASSINTERFACE)G_USER_COMMAND',
           gc_tabledef         TYPE string VALUE '(SAPLMASSINTERFACE)<TABLEDEF>',
           gc_flag_save        TYPE string VALUE '(SAPLMASSINTERFACE)E_FLG_SAVE',
           gc_flag_test        TYPE string VALUE '(SAPLMASSINTERFACE)E_FLG_TEST'.


TYPES:

  BEGIN OF ty_s_idno,
    idno TYPE zarn_idno,
  END OF ty_s_idno,
  ty_t_idno        TYPE STANDARD TABLE OF ty_s_idno,

  ty_s_field_confg TYPE zarn_field_confg,
  ty_t_field_confg TYPE SORTED TABLE OF ty_s_field_confg
                        WITH UNIQUE KEY tabname fldname,


  BEGIN OF ty_s_mass_fields,
    table TYPE tabname,
    field TYPE fieldname,
  END OF ty_s_mass_fields ,

  ty_t_mass_fields TYPE SORTED TABLE OF ty_s_mass_fields WITH NON-UNIQUE KEY table field,

  ty_t_reg_key     TYPE SORTED TABLE OF zsarn_reg_key WITH UNIQUE KEY idno,

  ty_t_key         TYPE SORTED TABLE OF zsarn_key     WITH UNIQUE KEY idno version,

  ty_s_idno_data   TYPE zsarn_idno_data,

  ty_t_idno_data   TYPE SORTED TABLE OF ty_s_idno_data WITH UNIQUE KEY idno,

  ty_t_reg_hdr     TYPE SORTED TABLE OF zarn_reg_hdr WITH UNIQUE KEY idno.

TYPES: BEGIN OF ty_s_act_fields.
        INCLUDE STRUCTURE massdfies.
TYPES:   sel_flag,
         index    LIKE sy-tabix.
TYPES: END   OF ty_s_act_fields.

TYPES: ty_t_act_fields TYPE STANDARD TABLE OF ty_s_act_fields.



*>>>structure copied from LMASSINTERFACETOP to be able to assign and access data from
"main MASS programme

*     Kopfleisten zu Untertabellen von TABLEDEF
DATA: key_fields LIKE massdfies, "APO no MCS01
      all_fields LIKE massdfies, "APO no MCS01
      flt_fields LIKE massdfies. "APO no MCS01
DATA: act_fields TYPE ty_s_act_fields.

*     Informationen zu den zu ändernden Tabellen
DATA: BEGIN OF tabledef OCCURS 0.
*        INCLUDE STRUCTURE cnmass_tabinfos.
        INCLUDE STRUCTURE masstabinfos.
*       number of key fields including MANDT (see FORM FILL_TABLEDEF)
DATA:   key_num_m  LIKE sy-tabix,
        act_num    LIKE sy-tabix,
        key_fields LIKE key_fields OCCURS 0,
        act_fields LIKE act_fields OCCURS 0,
        all_fields LIKE all_fields OCCURS 0,
        flt_fields LIKE flt_fields OCCURS 0,
        sel_flags  TYPE xfeld OCCURS 0,
        top_line   TYPE i,             "CXTAB_CONTROL-TOP_LINE
        left_col   TYPE i,             "CXTAB_CONTROL-LEFT_COL
        END   OF tabledef.

*<<<structure copied from LMASSINTERFACETOP to be able to assign and access data from



DATA: gt_t141       TYPE SORTED TABLE OF t141 WITH UNIQUE KEY mmsta,
      gt_tvms       TYPE SORTED TABLE OF tvms WITH UNIQUE KEY vmsta,
      gs_log_handle TYPE balloghndl.
