class ZCX_ARN_NOTIF_ERR definition
  public
  inheriting from ZCX_T100_MESSAGE_BASE
  create public .

public section.

  constants:
    begin of GENERATION_ERROR,
      msgid type symsgid value 'ZARENA_MSG',
      msgno type symsgno value '113',
      attr1 type scx_attrname value 'MV_ERROR_TEXT',
      attr2 type scx_attrname value '',
      attr3 type scx_attrname value '',
      attr4 type scx_attrname value '',
    end of GENERATION_ERROR .
  data MV_ERROR_TEXT type STRING read-only .

  methods CONSTRUCTOR
    importing
      !TEXTID like IF_T100_MESSAGE=>T100KEY optional
      !PREVIOUS like PREVIOUS optional
      !MV_VAR1 type STRING optional
      !MV_VAR2 type STRING optional
      !MV_VAR3 type STRING optional
      !MV_VAR4 type STRING optional
      !MV_ERROR_TEXT type STRING optional .
protected section.
private section.
ENDCLASS.



CLASS ZCX_ARN_NOTIF_ERR IMPLEMENTATION.


  method CONSTRUCTOR.
CALL METHOD SUPER->CONSTRUCTOR
EXPORTING
PREVIOUS = PREVIOUS
MV_VAR1 = MV_VAR1
MV_VAR2 = MV_VAR2
MV_VAR3 = MV_VAR3
MV_VAR4 = MV_VAR4
.
me->MV_ERROR_TEXT = MV_ERROR_TEXT .
clear me->textid.
if textid is initial.
  IF_T100_MESSAGE~T100KEY = IF_T100_MESSAGE=>DEFAULT_TEXTID.
else.
  IF_T100_MESSAGE~T100KEY = TEXTID.
endif.
  endmethod.
ENDCLASS.
