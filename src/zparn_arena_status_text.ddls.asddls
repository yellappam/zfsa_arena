@AbapCatalog.sqlViewName: 'zparnstatust'
@AbapCatalog.compiler.compareFilter: true
@AccessControl.authorizationCheck: #CHECK
@EndUserText.label: 'Arena Status Text'
define view zparn_arena_status_text as select from ziarn_arena_status_text {    
  key Arena_Status,
  Arena_Status_Text
}
  where Language = $session.system_language
