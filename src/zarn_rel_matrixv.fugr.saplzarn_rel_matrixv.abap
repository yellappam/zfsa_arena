* regenerated at 24.02.2017 11:24:02
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_REL_MATRIXVTOP.              " Global Data
  INCLUDE LZARN_REL_MATRIXVUXX.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_REL_MATRIXVF...              " Subroutines
* INCLUDE LZARN_REL_MATRIXVO...              " PBO-Modules
* INCLUDE LZARN_REL_MATRIXVI...              " PAI-Modules
* INCLUDE LZARN_REL_MATRIXVE...              " Events
* INCLUDE LZARN_REL_MATRIXVP...              " Local class implement.
* INCLUDE LZARN_REL_MATRIXVT99.              " ABAP Unit tests
  INCLUDE LZARN_REL_MATRIXVF00                    . " subprograms
  INCLUDE LZARN_REL_MATRIXVI00                    . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
