* regenerated at 29.04.2016 13:50:13 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_ORGANISM_TTOP.               " Global Data
  INCLUDE LZARN_ORGANISM_TUXX.               " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_ORGANISM_TF...               " Subroutines
* INCLUDE LZARN_ORGANISM_TO...               " PBO-Modules
* INCLUDE LZARN_ORGANISM_TI...               " PAI-Modules
* INCLUDE LZARN_ORGANISM_TE...               " Events
* INCLUDE LZARN_ORGANISM_TP...               " Local class implement.
* INCLUDE LZARN_ORGANISM_TT99.               " ABAP Unit tests
  INCLUDE LZARN_ORGANISM_TF00                     . " subprograms
  INCLUDE LZARN_ORGANISM_TI00                     . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
