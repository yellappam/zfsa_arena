*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 05.05.2016 at 15:39:57 by user C90001363
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_PACKTYPE_T.................................*
DATA:  BEGIN OF STATUS_ZARN_PACKTYPE_T               .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_PACKTYPE_T               .
CONTROLS: TCTRL_ZARN_PACKTYPE_T
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_PACKTYPE_T               .
TABLES: ZARN_PACKTYPE_T                .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
