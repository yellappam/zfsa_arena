*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 29.04.2016 at 13:50:13 by user C90001363
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_ORGANISM_T.................................*
DATA:  BEGIN OF STATUS_ZARN_ORGANISM_T               .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_ORGANISM_T               .
CONTROLS: TCTRL_ZARN_ORGANISM_T
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_ORGANISM_T               .
TABLES: ZARN_ORGANISM_T                .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
