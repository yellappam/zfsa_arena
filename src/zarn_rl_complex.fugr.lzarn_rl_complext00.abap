*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 25.05.2017 at 13:49:18
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_RL_COMPLEX.................................*
DATA:  BEGIN OF STATUS_ZARN_RL_COMPLEX               .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_RL_COMPLEX               .
CONTROLS: TCTRL_ZARN_RL_COMPLEX
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_RL_COMPLEX               .
TABLES: ZARN_RL_COMPLEX                .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
