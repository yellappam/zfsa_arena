FUNCTION z_arn_approvals_get_matrix.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(IDNO) TYPE  ZARN_IDNO OPTIONAL
*"     REFERENCE(VERSION) TYPE  ZARN_VERSION OPTIONAL
*"  TABLES
*"      RELEASE_MATRIX STRUCTURE  ZARN_REL_MATRIX
*"----------------------------------------------------------------------

  STATICS: lt_matrix_buffer TYPE STANDARD TABLE OF zarn_rel_matrix,
           ls_reg_hdr       TYPE zarn_reg_hdr.

  FIELD-SYMBOLS: <ls_matrix_buffer> LIKE LINE OF lt_matrix_buffer.

  REFRESH release_matrix.

  IF lt_matrix_buffer[] IS INITIAL.
    SELECT *
      INTO TABLE lt_matrix_buffer
      FROM zarn_rel_matrix.
  ENDIF.

  LOOP AT lt_matrix_buffer ASSIGNING <ls_matrix_buffer>.
    IF <ls_matrix_buffer>-source_of_supply IS INITIAL.
      APPEND <ls_matrix_buffer> TO release_matrix.
    ELSE.
      IF ls_reg_hdr-idno NE idno.
        SELECT SINGLE idno bwscl      "#EC CI_SEL_NESTED
          INTO CORRESPONDING FIELDS OF ls_reg_hdr
          FROM zarn_reg_hdr
          WHERE idno EQ idno.
      ENDIF.
      IF ls_reg_hdr-bwscl EQ <ls_matrix_buffer>-source_of_supply.
        APPEND <ls_matrix_buffer> TO release_matrix.
      ENDIF.
    ENDIF.
  ENDLOOP.

ENDFUNCTION.
