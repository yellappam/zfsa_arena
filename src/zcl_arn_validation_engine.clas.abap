class ZCL_ARN_VALIDATION_ENGINE definition
  public
  final
  create public .

public section.

  data GT_OUTPUT type ZARN_T_RL_OUTPUT .
  data GT_VALIDATION type ZARN_T_RL_VALID .
  constants GC_DUMMY_EVENT_ALL_VALIDATIONS type ZARN_E_RL_EVENT_ID value '*' ##NO_TEXT.
  constants GC_EVENT_CHECK_REPORT type ZARN_E_RL_EVENT_ID value 'CHECK_REP' ##NO_TEXT.
  constants GC_EVENT_ARENA_UI type ZARN_E_RL_EVENT_ID value 'REG_UI' ##NO_TEXT.
  constants GC_EVENT_PIM_INBOUND type ZARN_E_RL_EVENT_ID value 'INBOUND' ##NO_TEXT.
  constants GC_EVENT_APPROVAL type ZARN_E_RL_EVENT_ID value 'APPROVAL' ##NO_TEXT.
  constants GC_EVENT_MASS_UPLOAD type ZARN_E_RL_EVENT_ID value 'MASS_UPLD' ##NO_TEXT.
  constants GC_EVENT_SYNC_BACK type ZARN_E_RL_EVENT_ID value 'SYNC_BACK' ##NO_TEXT.
  constants GC_EVENT_AUTO_APPROVAL type ZARN_E_RL_EVENT_ID value 'AUTO_APPR' ##NO_TEXT.

  methods APPLY_VALIDATION_FIELD_FILTER
    importing
      !IV_EVENT type ZARN_E_RL_EVENT_ID
      !IT_FIELD_FILTER type ZTARN_TABLE_FIELD
    returning
      value(RT_VALIDATIONS) type ZARN_T_RL_VALID
    raising
      ZCX_ARN_VALIDATION_ERR .
  methods CONSTRUCTOR
    importing
      !IV_EVENT type ZARN_E_RL_EVENT_ID
      !IV_BAPIRET_OUTPUT type XFELD optional
      !IT_FIELD_FILTER type ZTARN_TABLE_FIELD optional
    raising
      ZCX_ARN_VALIDATION_ERR .
  methods VALIDATE_ARN_BULK
    importing
      !IS_ZSARN_PROD_DATA type ZSARN_PROD_DATA optional
      !IS_ZSARN_REG_DATA type ZSARN_REG_DATA optional
    exporting
      !ET_OUTPUT type ZARN_T_RL_OUTPUT
    raising
      ZCX_ARN_VALIDATION_ERR .
  methods VALIDATE_ARN_SINGLE
    importing
      !IS_ZSARN_PROD_DATA type ZSARN_PROD_DATA optional
      !IS_ZSARN_REG_DATA type ZSARN_REG_DATA optional
      !IV_REFRESH_MSTRDATA_BUFFERS type XFELD optional
    exporting
      !ET_OUTPUT type ZARN_T_RL_OUTPUT
    raising
      ZCX_ARN_VALIDATION_ERR .
protected section.
PRIVATE SECTION.

  TYPES:
    BEGIN OF ty_s_vendor_data,
      lifnr      TYPE lfa1-lifnr,
      bbbnr      TYPE lfa1-bbbnr,
      bbsnr      TYPE lfa1-bbsnr,
      bubkz      TYPE lfa1-bubkz,
      plifz      TYPE lfm1-plifz,
      sptype     TYPE zfsvend-sptype,
      lfa1_sperm TYPE lfa1-sperm,
      lfm1_sperm TYPE lfm1-sperm,
      lfa1_loevm TYPE lfa1-loevm,
      lfm1_loevm TYPE lfm1-loevm,
      waers      TYPE lfm1-waers,
      gln_no     TYPE zarn_gln,
    END OF ty_s_vendor_data .
  TYPES:
    ty_t_vendor_data TYPE SORTED TABLE OF ty_s_vendor_data
    WITH UNIQUE KEY lifnr .

  DATA gv_bapiret_output TYPE xfeld .
  DATA gt_rule_and_range TYPE zarn_t_rl_rule_and_range .
  DATA gt_tables TYPE zarn_t_rl_table .
  DATA gt TYPE zsarn_all_table_data .
  DATA gs TYPE zsarn_all_table_wa .
  CLASS-DATA:
    BEGIN OF gt_buf,
      t023_matkl                TYPE zarn_t_matkl,
      t179_prodh                TYPE zarn_t_prodh,
      t143_tempb                TYPE zarn_t_tempb,
      t006_msehi                TYPE zarn_t_msehi,
      t006_isocode              TYPE zarn_t_isocode,
      t6wsp_saiso               TYPE zarn_t_saiso,
      wrf_brands_brand_id       TYPE zarn_t_brand_id,
      zmd_strd_zzstrd           TYPE zarn_t_zzstrd,
      tntp_numtp                TYPE zarn_t_numtp,
      t024_ekgrp                TYPE zarn_t_ekgrp,
      t007a_mwskz               TYPE zarn_t_mwskz,
      zmd_store_msg_t_zzstrg    TYPE zarn_t_zzstrg,
      zmd_label_code_t_zzlabnum TYPE zarn_t_zzlabnum,
      zmd_mandst_t_zzmandst     TYPE zarn_t_zzmandst,
      t141_mmsta                TYPE zarn_t_mmsta,
      tvms_vmsta                TYPE zarn_t_vmsta,
      zmd_prdtyp_zzprdtype      TYPE zarn_t_prdtype,
      zmd_host_prdtyp_wwgha     TYPE zarn_t_wwgha,
      vendor_data               TYPE ty_t_vendor_data,
      zmd_category_role_id      TYPE zarn_t_role_id,
      zarn_uom_cat              TYPE zarn_t_uom_cat,
      zarn_uomcategory          TYPE zarn_t_uomcategory,
      pur_ekorg                 TYPE tvarvc_t,
      greeting_codes            TYPE tvarvc_t,
    END OF gt_buf .
  DATA gv_add_msg_keys TYPE xfeld VALUE 'X' ##NO_TEXT.
  CONSTANTS:
    BEGIN OF gc,
      precond_link_or     TYPE zarn_e_precond_link_type VALUE '',
      precond_link_and    TYPE zarn_e_precond_link_type VALUE 'A',
      arn_msg_type_err    TYPE zarn_e_valid_msg_type VALUE '', "#EC NOTEXT
      arn_msg_type_ok     TYPE zarn_e_valid_msg_type VALUE 'I', "#EC NOTEXT
      arn_msg_type_warn   TYPE zarn_e_valid_msg_type VALUE 'W', "#EC NOTEXT
      arn_msg_icon_err    TYPE zarn_e_msg_icon VALUE '@5C@', "#EC NOTEXT
      arn_msg_icon_ok     TYPE zarn_e_msg_icon VALUE '@5B@', "#EC NOTEXT
      arn_msg_icon_warn   TYPE zarn_e_msg_icon VALUE '@5D@', "#EC NOTEXT
      tb_zarn_reg_hdr     TYPE zarn_e_rl_table VALUE 'ZARN_REG_HDR', "#EC NOTEXT
      tb_zarn_reg_ean     TYPE zarn_e_rl_table VALUE 'ZARN_REG_EAN', "#EC NOTEXT
      tb_zarn_reg_pir     TYPE zarn_e_rl_table VALUE 'ZARN_REG_PIR', "#EC NOTEXT
      tb_zarn_reg_uom     TYPE zarn_e_rl_table VALUE 'ZARN_REG_UOM', "#EC NOTEXT
      tb_zarn_reg_banner  TYPE zarn_e_rl_table VALUE 'ZARN_REG_BANNER', "#EC NOTEXT
      tb_zarn_reg_allerg  TYPE zarn_e_rl_table VALUE 'ZARN_REG_ALLERG', "#EC NOTEXT
      tb_zarn_reg_str     TYPE zarn_e_rl_table VALUE 'ZARN_REG_STR', "#EC NOTEXT
      tb_zarn_products    TYPE zarn_e_rl_table VALUE 'ZARN_PRODUCTS', "#EC NOTEXT
      tb_zarn_prod_str    TYPE zarn_e_rl_table VALUE 'ZARN_PROD_STR', "#EC NOTEXT
      tb_zarn_allergen    TYPE zarn_e_rl_table VALUE 'ZARN_ALLERGEN', "#EC NOTEXT
      tb_zarn_gtin_var    TYPE zarn_e_rl_table VALUE 'ZARN_GTIN_VAR', "#EC NOTEXT
      tb_zarn_reg_cluster TYPE zarn_e_rl_table VALUE 'ZARN_REG_CLUSTER', "#EC NOTEXT
      max_valid_sequence  TYPE zarn_e_valid_sequence VALUE '99999', "#EC NOTEXT
      access_tab          TYPE string VALUE 'GT-',          "#EC NOTEXT
      access_wa           TYPE string VALUE 'GS-',          "#EC NOTEXT
      access_buffers      TYPE string VALUE 'GT_BUF-',      "#EC NOTEXT
      access_complex_m    TYPE string VALUE 'COMPLEX_RL',   "#EC NOTEXT
      pur_ekorg_tvarv     TYPE string VALUE 'ZARN_ARTICLE_POST_EINE_EKORG',
      BEGIN OF field_name,
        banner TYPE zarn_e_rl_field VALUE 'BANNER',
      END OF field_name,
    END OF gc .

  METHODS build_output_record
    IMPORTING
      !is_check_rule   TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
      !iv_field_val    TYPE char50
      !iv_addit_inf    TYPE char100
      !is_validation   TYPE zarn_rl_valid
    EXPORTING
      !es_output       TYPE zarn_s_rl_output
    RAISING
      zcx_arn_validation_err .
  METHODS check_allowed_table
    IMPORTING
      !is_rule      TYPE zarn_s_rl_rule_and_range
      !iv_allwd_tb1 TYPE zarn_e_rl_table
      !iv_allwd_fld TYPE zarn_e_rl_field OPTIONAL
      !iv_allwd_tb2 TYPE zarn_e_rl_table OPTIONAL
      !iv_allwd_tb3 TYPE zarn_e_rl_table OPTIONAL
      !iv_allwd_tb4 TYPE zarn_e_rl_table OPTIONAL
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl00tmpl
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl01
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl02
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl03
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl04
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl05
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl58
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl06
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl07
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl08
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl09
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl10
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl11
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl12
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl13
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl14
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl15
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl16
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl17
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl18
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl19
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl20
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl21
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl22
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl23
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl24
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl25
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl26
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl27
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl28
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl29
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl30
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl31
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl32
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl33
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl34
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl35
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl36
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl37
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl38
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl39
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl40
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl41
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl42
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl43
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl44
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl45
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl46
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl47
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl48
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl49
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl50
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl51
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl52
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl53
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl54
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl55
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl56
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl57
    IMPORTING
      !iv_field_value  TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
      !is_check_record TYPE any
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
  METHODS complex_rl_util_get_vnd_data
    IMPORTING
      !iv_lifnr             TYPE lifnr
    RETURNING
      VALUE(rs_vendor_data) TYPE ty_s_vendor_data .
  METHODS get_record_precond
    IMPORTING
      !is_check_record   TYPE any
      !is_rule_check     TYPE zarn_s_rl_rule_and_range
      !is_rule_precond   TYPE zarn_s_rl_rule_and_range
      !is_validation     TYPE zarn_rl_valid
    EXPORTING
      !es_precond_record TYPE any
    RAISING
      zcx_arn_validation_err .
  METHODS process_addit_key
    IMPORTING
      !iv_addit_key_field_name TYPE zarn_e_rl_kf
      !is_check_record         TYPE any
      !iv_addit_key_desc       TYPE zarn_e_rl_kfd
    EXPORTING
      !ev_addit_keys_txt       TYPE string
      !ev_addit_key            TYPE zarn_e_addit_key_val
    RAISING
      zcx_arn_validation_err .
  METHODS process_rule_single
    IMPORTING
      !is_check_record TYPE any
      !is_rule         TYPE zarn_s_rl_rule_and_range
    EXPORTING
      !ev_result_ok    TYPE xfeld
      !ev_field_val    TYPE char50
      !ev_addit_inf    TYPE char100
    RAISING
      zcx_arn_validation_err .
ENDCLASS.



CLASS ZCL_ARN_VALIDATION_ENGINE IMPLEMENTATION.


  METHOD apply_validation_field_filter.
* For tools such as MASS, we provide a new facility to filter
* validations by table name+field name(A field filter).
* The validations determined must always be a subset of the
* Validations assigned to the event

    CHECK it_field_filter IS NOT INITIAL.

* Get any rules that are applicable to the field filter
    SELECT rule_id
      FROM zarn_rl_rule
      INTO TABLE @DATA(lt_filtered_rules)
      FOR ALL ENTRIES IN @it_field_filter
      WHERE table_name = @it_field_filter-table_name AND
            field_name = @it_field_filter-field_name.

* Get any complex logic that is applicable to the field filter
*    SELECT DISTINCT complex_logic "+Make sure we hit the table buffer
    SELECT complex_logic
      FROM zarn_rl_clog_fld
      INTO TABLE @DATA(lt_filtered_complex)
      FOR ALL ENTRIES IN @it_field_filter
      WHERE table_name = @it_field_filter-table_name AND
            field_name = @it_field_filter-field_name.
    IF sy-subrc = 0.
      SORT lt_filtered_complex BY complex_logic."+Make sure we hit the table buffer
      DELETE ADJACENT DUPLICATES FROM lt_filtered_complex COMPARING complex_logic."+Make sure we hit the table buffer
* Get the rules associated with the complex logic filter
      SELECT rule_id
        FROM zarn_rl_rule
        APPENDING TABLE lt_filtered_rules
        FOR ALL ENTRIES IN lt_filtered_complex
        WHERE complex_logic = lt_filtered_complex-complex_logic.
      IF sy-subrc = 0.
* Scrub duplicates
        SORT lt_filtered_rules BY rule_id.
        DELETE ADJACENT DUPLICATES FROM lt_filtered_rules COMPARING rule_id.
      ENDIF.
    ENDIF.
    IF lt_filtered_rules IS NOT INITIAL.
* Now we(should) have our final list of filtered rules. We can now restrict our validations
      SELECT b~validation_id b~rule_id_check b~rule_id_precond1 b~rule_id_precond2 b~rule_id_precond3
             b~precond_link_type b~validation_type b~validation_sequence b~validation_group
             b~message b~message_type
        FROM zarn_rl_ev_val AS a INNER JOIN
             zarn_rl_valid  AS b ON b~validation_id = a~validation_id
        INTO CORRESPONDING FIELDS OF TABLE rt_validations
        FOR ALL ENTRIES IN lt_filtered_rules
        WHERE a~event_id         = iv_event AND
              b~rule_id_check    = lt_filtered_rules-rule_id OR
              b~rule_id_precond1 = lt_filtered_rules-rule_id OR
              b~rule_id_precond2 = lt_filtered_rules-rule_id OR
              b~rule_id_precond3 = lt_filtered_rules-rule_id.
    ENDIF.
** Exception condition: We found no validations. "Not now an exception condition
*    IF rt_validations IS INITIAL.
*      RAISE EXCEPTION TYPE zcx_arn_validation_err
*        EXPORTING
*          msgv1 = text-e26.
*    ENDIF.

  ENDMETHOD.


METHOD build_output_record.

  FIELD-SYMBOLS: <lv_idno>      TYPE zarn_idno,
                 <lv_version>   TYPE zarn_version.

  DATA: ls_table LIKE LINE OF gt_tables,
        lv_addit_keys_txt   TYPE string,
        lv_string_4_bapiret TYPE char300.

  CLEAR es_output.

  es_output-message_txt = is_validation-message.
  "value of checked field in message
  REPLACE FIRST OCCURRENCE OF '&' IN es_output-message_txt
   WITH shift_left( val = iv_field_val sub = '0' ).

  ASSIGN COMPONENT 'IDNO' OF STRUCTURE is_check_record
    TO <lv_idno>.
  IF sy-subrc EQ 0.
    es_output-idno = <lv_idno>.
  ENDIF.

  ASSIGN COMPONENT 'VERSION' OF STRUCTURE is_check_record
    TO <lv_version>.
  IF sy-subrc EQ 0.
    es_output-version = <lv_version>.
  ENDIF.

  CASE is_validation-message_type.
    WHEN gc-arn_msg_type_err.
      es_output-message_icon = gc-arn_msg_icon_err.
    WHEN gc-arn_msg_type_ok.
      es_output-message_icon = gc-arn_msg_icon_ok.
    WHEN gc-arn_msg_type_warn.
      es_output-message_icon = gc-arn_msg_icon_warn.
  ENDCASE.

  es_output-validation_id = is_validation-validation_id.
  es_output-table_name    = is_check_rule-table_name.
  es_output-field_name    = is_check_rule-field_name.
  es_output-validation_type   = is_validation-validation_type.
  es_output-validation_group  = is_validation-validation_group.
  es_output-check_field_val   = iv_field_val.

  IF gv_add_msg_keys EQ abap_true.
    READ TABLE gt_tables INTO ls_table
      WITH KEY table_name = is_check_rule-table_name.
    IF sy-subrc NE 0.
      RAISE EXCEPTION TYPE zcx_arn_validation_err
        EXPORTING
          msgv1 = text-erc
          msgv2 = |{ is_check_rule-rule_id WIDTH = 50 }|
          msgv3 = text-e07.
    ENDIF.

    CLEAR lv_addit_keys_txt.
    IF  ls_table-key_field1 IS NOT INITIAL.
      process_addit_key( EXPORTING is_check_record   = is_check_record
                                 iv_addit_key_desc = ls_table-key_field1_desc
                                 iv_addit_key_field_name = ls_table-key_field1
                       IMPORTING ev_addit_key = es_output-key1
                                 ev_addit_keys_txt = lv_addit_keys_txt ).
    ENDIF.

    IF  ls_table-key_field2 IS NOT INITIAL.
      process_addit_key( EXPORTING is_check_record   = is_check_record
                                   iv_addit_key_desc = ls_table-key_field2_desc
                                   iv_addit_key_field_name = ls_table-key_field2
                         IMPORTING ev_addit_key = es_output-key2
                                   ev_addit_keys_txt = lv_addit_keys_txt ).
    ENDIF.

    IF  ls_table-key_field3 IS NOT INITIAL.
      process_addit_key( EXPORTING is_check_record   = is_check_record
                                   iv_addit_key_desc = ls_table-key_field3_desc
                                   iv_addit_key_field_name = ls_table-key_field3
                         IMPORTING ev_addit_key = es_output-key3
                                   ev_addit_keys_txt = lv_addit_keys_txt ).
    ENDIF.

    IF lv_addit_keys_txt IS NOT INITIAL.
      CONDENSE lv_addit_keys_txt.
      "add additional key information (banners etc.) to the beginning of message
      es_output-message_txt = lv_addit_keys_txt && `: ` && es_output-message_txt.
    ENDIF.
  ENDIF.
  "if additional information provided from complex check
  "append it to the end of message
  IF iv_addit_inf IS NOT INITIAL.
    es_output-addit_info = iv_addit_inf.
    es_output-message_txt = es_output-message_txt && ` ` && es_output-addit_info.
  ENDIF.
  CONDENSE es_output-message_txt.

  "fill in additional bapiret fields.
  IF gv_bapiret_output EQ abap_true.
    es_output-msgid = 'ZARENA_MSG'.
    es_output-msgno = '004'.

    CASE is_validation-message_type.
      WHEN gc-arn_msg_type_err.
        es_output-msgty = 'E'.
      WHEN gc-arn_msg_type_ok.
        es_output-msgty = 'I'.
      WHEN gc-arn_msg_type_warn.
        es_output-msgty = 'W'.
    ENDCASE.

    lv_string_4_bapiret = es_output-validation_id && ` ` &&
                          es_output-message_txt.
    "message might not be complete - only first 200 characters...
    "technical limit
    es_output-msgv1 = lv_string_4_bapiret(50).
    es_output-msgv2 = lv_string_4_bapiret+50(50).
    es_output-msgv3 = lv_string_4_bapiret+100(50).
    es_output-msgv4 = lv_string_4_bapiret+150(50).
  ENDIF.

ENDMETHOD.


METHOD check_allowed_table.

  DATA lv_allowed TYPE xfeld.
  CLEAR lv_allowed.

  IF is_rule-table_name = iv_allwd_tb1.
    lv_allowed = abap_true.
  ENDIF.

  IF iv_allwd_tb2 IS SUPPLIED AND is_rule-table_name = iv_allwd_tb2.
    lv_allowed = abap_true.
  ENDIF.

  IF iv_allwd_tb3 IS SUPPLIED AND is_rule-table_name = iv_allwd_tb3.
    lv_allowed = abap_true.
  ENDIF.

  IF iv_allwd_tb4 IS SUPPLIED AND is_rule-table_name = iv_allwd_tb4.
    lv_allowed = abap_true.
  ENDIF.

  "possibility to restrict to particular field
  IF iv_allwd_fld IS SUPPLIED AND is_rule-field_name NE iv_allwd_fld.
    CLEAR lv_allowed.
  ENDIF.

  IF lv_allowed NE abap_true.
    RAISE EXCEPTION TYPE zcx_arn_validation_err
      EXPORTING
        msgv1 = text-erc
        msgv2 = |{ is_rule-rule_id WIDTH = 50 }|
        msgv3 = text-e08.
  ENDIF.

ENDMETHOD.


METHOD complex_rl00tmpl.

  "template for complex logic method
  "not called - TEMPLATE ONLY, DONT CHANGE

* Note: also need to add new rule as entry in table ZARN_RL_COMPLEX

  IF 1 = 2. message i035(ZARENA_BUSINESS_RULE). ENDIF.

* optional check for allowed tables
*
*  check_allowed_table( is_rule      = is_rule
*                       iv_allwd_tb1 = gc-tb_zarn_reg_hdr ).

  ev_result_ok = abap_false.
  ev_addit_inf = text-e04.

ENDMETHOD.


METHOD complex_rl01.

  "Check value against possible values in DB table
  "buffered

  "PAR 1 = table name
  "PAR 2 = field name
  "PAR 3 = dont allowe empty value

  "this rule expect static attribute - table with buffered values
  "name of table GS_BUFFERS-<TABLE>_<FIELD> for example GS_BUFFERS-T023_MATKL
  "type standard table with only 1 column (check field)


  DATA: lv_table_name   TYPE string.

  FIELD-SYMBOLS: <lt_buffer> TYPE STANDARD TABLE.

  IF 1 = 2. MESSAGE i033(zarena_business_rule). ENDIF.


  IF is_rule-complex_l_par3 IS INITIAL AND iv_field_value IS INITIAL.
    "allowed empty value, dont proceed, OK
    ev_result_ok = abap_true.
    RETURN.
  ENDIF.

  "construct name of buffer table variable
  lv_table_name = gc-access_buffers &&
                  is_rule-complex_l_par1 && `_` && is_rule-complex_l_par2.

  ASSIGN (lv_table_name) TO <lt_buffer>.
  IF sy-subrc NE 0.
    RAISE EXCEPTION TYPE zcx_arn_validation_err
      EXPORTING
        msgv1 = text-erc
        msgv2 = |{ is_rule-rule_id WIDTH = 50 }|
        msgv3 = text-e13.
  ENDIF.

  IF <lt_buffer>[] IS INITIAL.

    TRY.
        "select values from DB
        SELECT (is_rule-complex_l_par2) FROM (is_rule-complex_l_par1)
          INTO TABLE <lt_buffer>.

      CATCH cx_sy_open_sql_db.
        RAISE EXCEPTION TYPE zcx_arn_validation_err
          EXPORTING
            msgv1 = text-erc
            msgv2 = |{ is_rule-rule_id WIDTH = 50 }|
            msgv3 = text-e15.
      CATCH cx_sy_dynamic_osql_semantics.
        RAISE EXCEPTION TYPE zcx_arn_validation_err
          EXPORTING
            msgv1 = text-erc
            msgv2 = |{ is_rule-rule_id WIDTH = 50 }|
            msgv3 = text-e15.
      CATCH cx_sy_dynamic_osql_syntax.
        RAISE EXCEPTION TYPE zcx_arn_validation_err
          EXPORTING
            msgv1 = text-erc
            msgv2 = |{ is_rule-rule_id WIDTH = 50 }|
            msgv3 = text-e15.
    ENDTRY.

    SORT <lt_buffer>.
  ENDIF.

  READ TABLE <lt_buffer> WITH KEY table_line = iv_field_value
      TRANSPORTING NO FIELDS BINARY SEARCH.
  IF sy-subrc EQ 0.
    ev_result_ok = abap_true.
  ELSE.
    ev_result_ok = abap_false.
  ENDIF.


ENDMETHOD.


METHOD complex_rl02.


  DATA: lv_value        TYPE string.
  FIELD-SYMBOLS: <lt_buffer> TYPE STANDARD TABLE.

  IF 1 = 2. MESSAGE i034(zarena_business_rule). ENDIF.

  "PAR1 = string with allowed characters

  lv_value = iv_field_value.
  TRANSLATE lv_value TO UPPER CASE.

  IF lv_value CO is_rule-complex_l_par1.
    ev_result_ok = abap_true.
  ELSE.
    ev_result_ok = abap_false.
  ENDIF.

ENDMETHOD.


METHOD complex_rl03.

  DATA lv_domname TYPE domname.

  "PAR1 = domain name
  lv_domname = is_rule-complex_l_par1.


  CALL FUNCTION 'CHECK_DOMAIN_VALUES'
    EXPORTING
      domname       = lv_domname
      value         = iv_field_value
    EXCEPTIONS
      no_domname    = 1
      wrong_value   = 2
      dom_not_found = 3
      OTHERS        = 4.
  IF sy-subrc <> 0.
    ev_result_ok = abap_false.
  ELSE.
    ev_result_ok = abap_true.
  ENDIF.


ENDMETHOD.


METHOD complex_rl04.

  data ls_rule LIKE is_rule.
  ls_rule = is_rule.

  FIELD-SYMBOLS: <ls_range> TYPE zarn_s_rl_rule_rng,
                 <lv_value>    TYPE any.

  LOOP AT ls_rule-ranges ASSIGNING <ls_range>.

    ASSIGN (<ls_range>-low) TO <lv_value>.
    IF sy-subrc EQ 0.
      "replace the variable name with actual value in it
      <ls_range>-low = <lv_value>.
    ENDIF.
    UNASSIGN <lv_value>.

    ASSIGN (<ls_range>-high) TO <lv_value>.
    IF sy-subrc EQ 0.
      "replace the variable name with actual value in it
      <ls_range>-high = <lv_value>.
    ENDIF.
    UNASSIGN <lv_value>.

  ENDLOOP.

  IF iv_field_value NOT IN ls_rule-ranges.
    ev_result_ok = abap_false.
  ELSE.
    ev_result_ok = abap_true.
  ENDIF.


ENDMETHOD.


METHOD complex_rl05.

  DATA: lv_fan_id       TYPE mara-zzfan,
        lv_matnr_fan    TYPE matnr,
        lv_matnr_reg    TYPE matnr,
        lt_matnr        TYPE STANDARD TABLE OF matnr,
        lv_matnr        TYPE matnr,
        ls_zarn_products TYPE zarn_products,
        lv_idno         TYPE zarn_idno.

  check_allowed_table( is_rule      = is_rule
                       iv_allwd_tb1 = gc-tb_zarn_products ).

  ls_zarn_products = is_check_record.

  "check for FAN ID usage
  lv_fan_id     = iv_field_value.
  lv_matnr_fan  = iv_field_value.

  ev_result_ok = abap_true.

  IF lv_fan_id IS INITIAL.
    "dont check if FAN id is initial.
    RETURN.
  ENDIF.

  CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'
    EXPORTING
      input  = lv_matnr_fan
    IMPORTING
      output = lv_matnr_fan.

  "uses always selects because it doesn't make sense to buffer
  "here - 1 check per Article should be called

  "first check in zarn_products whether
  "same FAN is not already used
  SELECT SINGLE idno FROM zarn_products
    INTO lv_idno
    WHERE fan_id EQ lv_fan_id
     AND idno NE ls_zarn_products-idno.
  IF sy-subrc EQ 0.
    ev_result_ok = abap_false.
    ev_addit_inf = `Already used in AReNa ID ` && lv_idno.
    RETURN.
  ENDIF.

  "dont check existence in MARA in here - as per requirement

*  "check existence in MARA
*  SELECT  matnr FROM mara INTO TABLE lt_matnr
*    WHERE matnr = lv_matnr_fan
*       OR zzfan = lv_fan_id.
*  IF sy-subrc EQ 0.
*    LOOP AT lt_matnr INTO lv_matnr.
*      "if something found, we need to check whether this is
*      "the Article we are going to update or whether it is error
*      SELECT SINGLE sap_article FROM zarn_reg_hdr INTO lv_matnr_reg
*        WHERE idno        = ls_zarn_products-idno
*          AND sap_article = lv_matnr.
*      IF sy-subrc NE 0.
*        "error - found Article for FAN ID and it is different Article
*        "than the currently processed Article for IDNO
*         ev_result_ok = abap_false.
*         ev_addit_inf = `Already used in Article `
*                        && shift_left( val = lv_matnr sub = '0' ).
*         RETURN.
*      ENDIF.
*    ENDLOOP.
*  ENDIF.

ENDMETHOD.


METHOD complex_rl06.

  CONSTANTS lc_ea TYPE zarn_sap_uom VALUE 'EA'.

  DATA: lv_idno          TYPE zarn_idno,
        lv_matnr         TYPE matnr,
        lv_meinh         TYPE meinh,
        lv_barcode       TYPE ean11,
        lv_uom_code      TYPE zarn_uom_code,
        lv_idno_db       TYPE zarn_idno,
        lv_meinh_db      TYPE meinh,
        lv_uom_code_db   TYPE zarn_uom_code,
        ls_zarn_reg_ean  TYPE zarn_reg_ean,
        ls_zarn_gtin_var TYPE zarn_gtin_var,
        ls_zarn_reg_hdr  TYPE zarn_reg_hdr,
        ls_zarn_products TYPE zarn_products,
        ls_zarn_reg_uom  TYPE zarn_reg_uom,
        lv_matnr_for_fan TYPE matnr,
        lv_matnr_reg_hdr TYPE matnr,
        lt_mean          TYPE STANDARD TABLE OF mean,
        ls_mean          TYPE mean,
        lv_count         TYPE i.

  check_allowed_table( is_rule      = is_rule
                       iv_allwd_tb1 = gc-tb_zarn_reg_ean
                       iv_allwd_tb2 = gc-tb_zarn_gtin_var ).

  ev_result_ok = abap_true.

  IF gt_buf-greeting_codes[] IS INITIAL.
    SELECT low high INTO CORRESPONDING FIELDS OF TABLE gt_buf-greeting_codes[]
      FROM tvarvc
      WHERE name = 'ZARN_GREETING_UOM_LIMIT'.
  ENDIF.

  "Checks usage of Barcode in
  "ZARN_GTIN_VAR
  "ZARN_REG_EAN
  "MEAN
  "if found, must be matching Article + Unit of Measure

  "get barcode, IDNO, unom and SAP article
  CASE is_rule-table_name.
    WHEN gc-tb_zarn_reg_ean.

      ls_zarn_reg_ean = is_check_record.


      CLEAR lv_count.
      LOOP AT gt-zarn_reg_ean INTO DATA(ls_reg_ean_temp)
          WHERE ean11 = ls_zarn_reg_ean-ean11.
        ADD 1 TO lv_count.
      ENDLOOP.
      IF lv_count > 1.
        "duplicates within internal table
        ev_result_ok = abap_false.
        ev_addit_inf = text-a01.
        RETURN.
      ENDIF.


      lv_barcode      = ls_zarn_reg_ean-ean11.
      lv_idno         = ls_zarn_reg_ean-idno.
      lv_meinh        = ls_zarn_reg_ean-meinh.
      READ TABLE gt-zarn_reg_uom
        INTO ls_zarn_reg_uom
        TRANSPORTING pim_uom_code hybris_internal_code
        WITH KEY idno = lv_idno
                 meinh = lv_meinh.
      IF sy-subrc EQ 0.
        lv_uom_code = ls_zarn_reg_uom-pim_uom_code.
        "special handling for greetings!
        "greeting will have special uom code which is not coming
        "from national pim
        "we need to substitute this with proper PIM UOM CODE for UOM EA
        IF line_exists( gt_buf-greeting_codes[ high = ls_zarn_reg_uom-hybris_internal_code ] ).
          READ TABLE gt-zarn_reg_uom
          INTO ls_zarn_reg_uom
          TRANSPORTING pim_uom_code
          WITH KEY idno = lv_idno
           meinh = lc_ea.
          IF sy-subrc EQ 0.
            lv_uom_code = ls_zarn_reg_uom-pim_uom_code.
          ELSE.
            CLEAR lv_uom_code.
          ENDIF.
        ENDIF.
      ELSE.
        CLEAR lv_uom_code.
      ENDIF.
*      SELECT SINGLE pim_uom_code FROM zarn_reg_uom
*        INTO lv_uom_code
*        WHERE idno         = lv_idno
*          AND meinh        = lv_meinh.
*      IF sy-subrc NE 0.
*        CLEAR lv_uom_code.
*      ENDIF.
    WHEN gc-tb_zarn_gtin_var.
      ls_zarn_gtin_var = is_check_record.

      CLEAR lv_count.
      LOOP AT gt-zarn_gtin_var INTO DATA(ls_gtin_var_temp)
          WHERE gtin_code = ls_zarn_gtin_var-gtin_code.
        ADD 1 TO lv_count.
      ENDLOOP.
      IF lv_count > 1.
        "duplicates within internal table
        ev_result_ok = abap_false.
        ev_addit_inf = text-a01.
        RETURN.
      ENDIF.

      lv_barcode       = ls_zarn_gtin_var-gtin_code.
      lv_idno          = ls_zarn_gtin_var-idno.
      lv_uom_code      = ls_zarn_gtin_var-uom_code.
      SELECT SINGLE meinh FROM zarn_reg_uom
        INTO lv_meinh
        WHERE idno         = lv_idno
          AND pim_uom_code = lv_uom_code .
      IF sy-subrc NE 0.
        CLEAR lv_meinh.
      ENDIF.
  ENDCASE.

  "check in ZARN_REG_EAN first
  IF lv_meinh IS NOT INITIAL.
    "that means we are currently checking ZARN_REG_EAN
    "or ZARN_GTIN_VAR and succeeded to convert UOM_CODE to UOM
    SELECT SINGLE idno meinh FROM zarn_reg_ean INTO (lv_idno_db, lv_meinh_db)
      WHERE  ean11    = lv_barcode
      AND NOT ( idno  = lv_idno
       AND      meinh = lv_meinh ).
    IF sy-subrc EQ 0.
      "barcode already exists on some other ZARN_REG_EAN
      ev_result_ok = abap_false.
      ev_addit_inf = `AReNa ID ` && lv_idno_db
                  && ` UOM ` && lv_meinh_db.
      RETURN. "error leave
    ENDIF.
  ELSE.
    "that means that we are checking ZARN_GTIN_VAR but failed
    "to convert UOM CODE to UOM

    "whatever record we find in ZARN_REG_EAN we will mark it as error
    "might be OK if same IDNO and UOM, but impossible to compare UOMs
    "because of missing link in ZARN_REG_UOM

    SELECT SINGLE idno meinh FROM zarn_reg_ean INTO (lv_idno_db, lv_meinh_db)
      WHERE ean11 = lv_barcode.
    IF sy-subrc EQ 0.
      ev_result_ok = abap_false.
      IF lv_idno_db EQ  lv_idno.
        "under same IDNO
        ev_addit_inf = `Reg. EAN of the same AReNa and impossible to match UOM`
        && ` (ZARN_REG_UOM not found for code ` && lv_uom_code && `)`.
      ELSE.
        "under different IDNO
        ev_addit_inf = `AReNa ID ` && lv_idno_db
                 && ` UOM ` && lv_meinh_db.
      ENDIF.
      RETURN. "error, leave
    ENDIF.
  ENDIF.

  "check in ZARN_GTIN_VAR
  IF lv_uom_code IS NOT INITIAL.
    "that means we are currently checking ZARN_GTIN_VAR
    "or ZARN_REG_EAN and succeeded to convert UOM to UOM_CODE

    "we want to search only for current versions of national data
    "and ignore historical versions - thats the reason for join on ZARN_VER_STATUS
    SELECT SINGLE g~idno g~uom_code FROM zarn_gtin_var AS g
      INNER JOIN zarn_ver_status AS s
      ON  g~idno    = s~idno
      AND g~version = s~current_ver
      INTO (lv_idno_db,  lv_uom_code_db)
      WHERE  gtin_code = lv_barcode
      AND NOT ( g~idno  = lv_idno
      AND      g~uom_code = lv_uom_code ).
    IF sy-subrc EQ 0.
      "barcode already exists on some other ZARN_GTIN_VAR
      ev_result_ok = abap_false.
      ev_addit_inf = `AReNa ID ` && lv_idno_db
                  && ` UOM ` && lv_uom_code_db.
      RETURN. "error leave
    ENDIF.
  ELSE.
    "that means that we are checking ZARN_REG_EAN but failed
    "to convert UOM to UOM CODE

    "whatever record we find in ZARN_GTIN_VAR we will mark it as error
    "might be OK if same IDNO and UOM, but impossible to compare UOMs
    "because of missing link in ZARN_REG_UOM
    SELECT SINGLE g~idno g~uom_code FROM zarn_gtin_var AS g
      INNER JOIN zarn_ver_status AS s
      ON  g~idno    = s~idno
      AND g~version = s~current_ver
       INTO (lv_idno_db,  lv_uom_code_db)
      WHERE  g~gtin_code = lv_barcode.
    IF sy-subrc EQ 0.
      ev_result_ok = abap_false.
      IF lv_idno_db EQ  lv_idno.
        "under same IDNO
        ev_addit_inf = `Nat. EAN of the same AReNa and impossible to match UOM`
        && ` (ZARN_REG_UOM not found for code ` && lv_meinh && `)`.
      ELSE.
        "under different IDNO
        ev_addit_inf = `AReNa ID number ` && lv_idno_db
                 && ` UOM ` && lv_uom_code_db.
      ENDIF.
      RETURN. "error leave
    ENDIF.
  ENDIF.

  "check in MEAN
  "get FAN ID first
  READ TABLE gt-zarn_products INDEX 1 INTO ls_zarn_products
  TRANSPORTING idno fan_id.
  IF sy-subrc NE 0
    OR ls_zarn_products-idno NE lv_idno.
    RAISE EXCEPTION TYPE zcx_arn_validation_err
      EXPORTING
        msgv1 = text-eri
        msgv2 = |{ is_rule-rule_id WIDTH = 50 }|
        msgv3 = text-e14
        msgv4 = |{ lv_idno WIDTH = 50 }|.
  ENDIF.

  CLEAR lv_matnr_for_fan.

  IF ls_zarn_products-fan_id IS NOT INITIAL.
    "find article number based on FAN ID
    SELECT SINGLE matnr FROM mara INTO lv_matnr_for_fan
      WHERE zzfan = ls_zarn_products-fan_id.
    IF sy-subrc NE 0.
      CLEAR lv_matnr_for_fan.
    ENDIF.

    IF lv_matnr_for_fan IS INITIAL.
      CALL FUNCTION 'CONVERSION_EXIT_MATN1_INPUT'
        EXPORTING
          input  = ls_zarn_products-fan_id
        IMPORTING
          output = lv_matnr.
      IF sy-subrc <> 0.
* Implement suitable error handling here
      ENDIF.
      SELECT SINGLE matnr FROM mara INTO lv_matnr_for_fan
        WHERE matnr = lv_matnr.
      IF sy-subrc NE 0.
        CLEAR lv_matnr_for_fan.
      ENDIF.

    ENDIF.
  ENDIF.

* Dont Check Reg. Hdr material number against FAN ID material number
  " as per requirement (material number on reg. header might not be updated yet)

*  CLEAR lv_matnr_reg_hdr.
*  IF is_rule-table_name = gc-tb_zarn_reg_ean.
*    "if checking reg. EAN than get Article Number from reg. HDR
*    READ TABLE gt-zarn_reg_hdr INDEX 1 INTO ls_zarn_reg_hdr
*      TRANSPORTING idno matnr.
*    IF sy-subrc NE 0
*      OR ls_zarn_reg_hdr-idno NE lv_idno.
*      RAISE EXCEPTION TYPE zcx_arn_validation_err
*        EXPORTING
*          msgv1 = text-eri
*          msgv2 = |{ is_rule-rule_id WIDTH = 50 }|
*          msgv3 = text-e14
*          msgv4 = |{ lv_idno WIDTH = 50 }|.
*    ENDIF.
*    lv_matnr_reg_hdr = ls_zarn_reg_hdr-matnr.
*  ENDIF.
*
*  "Article that we found for FAN should be matching with SAP Article on header
*  IF lv_matnr_for_fan NE ls_zarn_reg_hdr-matnr.
*    "if not matching, error
*    ev_result_ok = abap_false.
*    ev_addit_inf = `Not possible to check Barcodes in MEAN - `
*                    && `national FAN ID not matching SAP Article Nr.`.
*    RETURN. "error leave
*  ENDIF.

  SELECT matnr meinh FROM mean INTO CORRESPONDING FIELDS OF TABLE lt_mean
    WHERE ean11 = lv_barcode.
  IF sy-subrc EQ 0.
    LOOP AT lt_mean INTO ls_mean.
      IF lv_matnr_for_fan IS NOT INITIAL
        AND lv_matnr_for_fan = ls_mean-matnr.
        "Article Matching
        "Check UOM matching
        IF lv_meinh IS NOT INITIAL
          AND lv_meinh NE ls_mean-meinh.
          ev_result_ok = abap_false.
          ev_addit_inf = `SAP Article ` && ls_mean-matnr
                   && ` and UOM ` && ls_mean-meinh.
          RETURN.
        ELSEIF lv_meinh IS INITIAL.
          ev_result_ok = abap_false.
          ev_addit_inf = `SAP Article ` && ls_mean-matnr
                   && ` and UOM ` && ls_mean-meinh &&
                   ` (impossible to match UOMs, missing ZARN_REG_UOM)`.
          RETURN.
        ENDIF.
      ELSE.
        ev_result_ok = abap_false.
        ev_addit_inf = `SAP Article ` && ls_mean-matnr
                   && ` and UOM ` && ls_mean-meinh.
        RETURN.
      ENDIF.
    ENDLOOP.
  ENDIF.


ENDMETHOD.


METHOD COMPLEX_RL07.

  data: ls_zarn_reg_ean       type zarn_reg_ean,
        ls_zarn_reg_ean_other TYPE zarn_reg_ean.

"only 1 main barcode allowed per UOM

  check_allowed_table( is_rule      = is_rule
                       iv_allwd_tb1 = gc-tb_zarn_reg_ean ).

  ev_result_ok = abap_true.

  ls_zarn_reg_ean = is_check_Record.

  if ls_zarn_reg_ean-hpean EQ abap_true.
    LOOP AT gt-zarn_reg_ean INTO ls_zarn_reg_ean_other
      where meinh EQ ls_zarn_reg_ean-meinh
        AND hpean EQ abap_true
        and ean11 NE ls_zarn_reg_ean-ean11.
      "found another main barcode for same UOM
      ev_result_ok = abap_false.
    ENDLOOP.
  ENDIF.

ENDMETHOD.


METHOD complex_rl08.

  "entered vendor's GLN is present in national ZARN_PIR

  DATA: ls_vendor_data  TYPE ty_s_vendor_data,
        ls_zarn_reg_pir TYPE zarn_reg_pir.
        "lv_gln          TYPE zarn_gln.

  check_allowed_table( is_rule      = is_rule
                       iv_allwd_tb1 = gc-tb_zarn_reg_pir ).

  "start with assumption data is invalid and only if everything is OK
  "make the check result OK
  ev_result_ok = abap_false.

  ls_zarn_reg_pir = is_check_record.

  ls_vendor_data = complex_rl_util_get_vnd_data( ls_zarn_reg_pir-lifnr ).

  "GLN number is now constructed withing method complex_rl_util_get_vnd_data
  "lv_gln = ls_vendor_data-bbbnr && ls_vendor_data-bbsnr && ls_vendor_data-bubkz.

  READ TABLE gt-zarn_pir
     WITH KEY hybris_internal_code = ls_zarn_reg_pir-hybris_internal_code
              gln                  = ls_vendor_data-gln_no TRANSPORTING NO FIELDS.
  IF sy-subrc EQ 0.
    ev_result_ok = abap_true.
  ENDIF.

ENDMETHOD.


METHOD COMPLEX_RL09.

  "check only 1 regular vendor
  DATA: ls_zarn_reg_pir TYPE zarn_reg_pir.

  check_allowed_table( is_rule      = is_rule
                       iv_allwd_tb1 = gc-tb_zarn_reg_pir ).

  ev_result_ok = abap_true.
  ls_zarn_reg_pir = is_check_record.

  IF ls_zarn_reg_pir-relif EQ abap_true.
    LOOP AT gt-zarn_reg_pir TRANSPORTING NO FIELDS
                            where relif eq abap_true
                              AND lifnr NE ls_zarn_reg_pir-lifnr.
    ENDLOOP.
    IF sy-subrc EQ 0.
      "more than 1 regular vendor enter => error
      ev_result_ok = abap_false.
    ENDIF.
  ENDIF.

ENDMETHOD.


METHOD complex_rl10.

  "check field against vendor master
  "PAR1 is having field name

  "works on multiple tables LFA1, LFM1 (ekorg 9999), ZFSVEND (first record)
  "functionality is restricted only for what is implemented in method
  "complex_rl_util_get_vnd_data - for new fields this method must be extended

  DATA: ls_zarn_reg_pir TYPE zarn_reg_pir,
        ls_vendor_data  TYPE ty_s_vendor_data.

  FIELD-SYMBOLS: <lv_vm_field> TYPE any. "field of vendor master data

  check_allowed_table( is_rule      = is_rule
                       iv_allwd_tb1 = gc-tb_zarn_reg_pir ).

  ev_result_ok = abap_true.
  ls_zarn_reg_pir = is_check_record.

  ls_vendor_data = complex_rl_util_get_vnd_data( ls_zarn_reg_pir-lifnr ).

  ASSIGN COMPONENT is_rule-complex_l_par1 OF STRUCTURE  ls_vendor_data
  TO <lv_vm_field>.
  IF sy-subrc NE 0.
    RAISE EXCEPTION TYPE zcx_arn_validation_err
      EXPORTING
        msgv1 = text-erc
        msgv2 = |{ is_rule-rule_id WIDTH = 50 }|
        msgv3 = text-e17.
  ENDIF.

  IF iv_field_value NE <lv_vm_field>.
    ev_result_ok = abap_false.
  ENDIF.



ENDMETHOD.


METHOD complex_rl11.

  "at least 1 banner record from selected ranges must exist
  "assigned ranges must have list of allowed banners
  ev_result_ok = abap_true.

  LOOP AT gt-zarn_reg_banner TRANSPORTING NO FIELDS
      WHERE banner IN is_rule-ranges.
    EXIT.                     " Aruna Mackenjee, 16/06/2016
  ENDLOOP.
  IF sy-subrc NE 0.
    ev_result_ok = abap_false.
  ENDIF.

ENDMETHOD.


METHOD complex_rl12.

  "check if at least 1 EAN record exists for specified type
  "uses assigned ranges

  ev_result_ok = abap_true.

  LOOP AT gt-zarn_reg_ean TRANSPORTING NO FIELDS
    WHERE eantp IN is_rule-ranges.
    EXIT.                     " Aruna Mackenjee, 16/06/2016
  ENDLOOP.
  IF sy-subrc NE 0.
    ev_result_ok = abap_false.
  ENDIF.

ENDMETHOD.


METHOD complex_rl13.


  DATA: ls_zarn_reg_pir TYPE zarn_reg_pir,
        lv_count        TYPE i.

  "check consistent EINA / EINE ticks

  check_allowed_table( is_rule      = is_rule
                       iv_allwd_tb1 = gc-tb_zarn_reg_pir ).

  ls_zarn_reg_pir = is_check_record.
  ev_result_ok = abap_true.

  "there is multiple simple check we need to do for each vendor

  "1) if EINA rel = X then for same vendor 1 record need to have also EINE rel = X
  IF ls_zarn_reg_pir-pir_rel_eina = abap_true.
    IF NOT line_exists( gt-zarn_reg_pir[ lifnr = ls_zarn_reg_pir-lifnr
                                         pir_rel_eine = abap_true ] ).

      ev_result_ok = abap_false.
      ev_addit_inf = 'Relevant for PIR (purch. org) is not set'(e21).
    ENDIF.
  ENDIF.

  "2) if EINE rel = X then for same vendor 1 record need to have also EINA rel = X
  IF ls_zarn_reg_pir-pir_rel_eine = abap_true.
    IF NOT line_exists( gt-zarn_reg_pir[ lifnr = ls_zarn_reg_pir-lifnr
                                         pir_rel_eina = abap_true ] ).

      ev_result_ok = abap_false.
      ev_addit_inf = 'Relevant for PIR (general) is not set'(e22).
    ENDIF.
  ENDIF.


  "3) max. 1 record per vendor can be set as EINA rel = X
  IF ls_zarn_reg_pir-pir_rel_eina = abap_true.
    CLEAR lv_count.
    LOOP AT gt-zarn_reg_pir TRANSPORTING NO FIELDS
                            WHERE lifnr = ls_zarn_reg_pir-lifnr
                              AND pir_rel_eina = abap_true.
      ADD 1 TO lv_count.
      IF lv_count > 1.
        ev_result_ok = abap_false.
        ev_addit_inf = 'More than 1 record per vendor set as relevant for PIR (general)'(e23).
        EXIT.
      ENDIF.
    ENDLOOP.
  ENDIF.


  "4) max. 1 record per vendor can be set as EINE rel = X
  IF ls_zarn_reg_pir-pir_rel_eine = abap_true.
    CLEAR lv_count.
    LOOP AT gt-zarn_reg_pir TRANSPORTING NO FIELDS
                            WHERE lifnr = ls_zarn_reg_pir-lifnr
                              AND pir_rel_eine = abap_true.
      ADD 1 TO lv_count.
      IF lv_count > 1.
        ev_result_ok = abap_false.
        ev_addit_inf = 'More than 1 record per vendor set as relevant for PIR (purch. org)'(e24).
        EXIT.
      ENDIF.
    ENDLOOP.
  ENDIF.


  "5) if regular vendor is ticked, then at least EINA or EINE flag
  "must be ticked as well for the same vendor
  IF ls_zarn_reg_pir-relif = abap_true.

    IF NOT ( line_exists( gt-zarn_reg_pir[ lifnr = ls_zarn_reg_pir-lifnr
                                        pir_rel_eina = abap_true ] )
        OR line_exists( gt-zarn_reg_pir[ lifnr = ls_zarn_reg_pir-lifnr
                                        pir_rel_eine = abap_true ] ) ).

      ev_result_ok = abap_false.
      ev_addit_inf = 'Regular Vendor must be set as relevant for PIR'(e25).
      EXIT.

    ENDIF.

  ENDIF.



ENDMETHOD.


METHOD complex_rl14.

  "check value in ranges and no decimals (whole number)
  ev_result_ok = abap_true.

  IF iv_field_value IN is_rule-ranges.
    "is in ranges, now check decimals
    IF frac( iv_field_value ) > 0.
      ev_result_ok = abap_false.
    ENDIF.
    "tbd catch exception if not numeric?
  ELSE.
    ev_result_ok = abap_false.
  ENDIF.


ENDMETHOD.


METHOD complex_rl15.

  "rem. shelf life must be max. 5/6 of lifespan production

  CONSTANTS lc_rem_shelf_life TYPE zarn_e_rl_field VALUE 'MHDRZ'.
  DATA: ls_zarn_reg_hdr       TYPE zarn_reg_hdr,
        ls_zarn_products      TYPE zarn_products,
        lv_max_rem_shelf_life LIKE ls_zarn_reg_hdr-mhdrz.

  check_allowed_table( is_rule      = is_rule
                       iv_allwd_tb1 = gc-tb_zarn_reg_hdr
                       iv_allwd_fld = lc_rem_shelf_life ).

  ev_result_ok = abap_true.
  ls_zarn_reg_hdr = is_check_record.

  READ TABLE gt-zarn_products INTO ls_zarn_products INDEX 1.
  IF sy-subrc NE 0.
    RAISE EXCEPTION TYPE zcx_arn_validation_err
      EXPORTING
        msgv1 = text-eri
        msgv2 = |{ is_rule-rule_id WIDTH = 50 }|
        msgv3 = text-e18
        msgv4 = |{ ls_zarn_reg_hdr-idno WIDTH = 50 }|.
  ENDIF.

  "TBD check decimal calculation
  lv_max_rem_shelf_life = ls_zarn_products-lifespan_production * 5 / 6.
  IF ls_zarn_reg_hdr-mhdrz > lv_max_rem_shelf_life.
    ev_result_ok = abap_false.
  ENDIF.

ENDMETHOD.


METHOD complex_rl16.

  DATA: lv_meinh        TYPE meinh,
        lv_meinh_marm   TYPE meinh,
        ls_zarn_reg_hdr TYPE zarn_reg_hdr.

  "check whether entered UOM exists in ZARN_REG_UOM or MARM
  "for this Article
  "PAR1 - optional, can contained specific value of UOM
  "in that case ignores the check field and use the value in parameter instead
  "used in case we want to check for example "has EA"?

  ev_result_ok = abap_true.
  IF is_rule-complex_l_par1 IS INITIAL.
    lv_meinh = iv_field_value.
  ELSE.
    lv_meinh = is_rule-complex_l_par1.
  ENDIF.

  "first check whether UOM exists in ZARN_REG_UOM
  READ TABLE gt-zarn_reg_uom WITH KEY meinh = lv_meinh
    TRANSPORTING NO FIELDS.
  IF sy-subrc NE 0.
    "else try in MARM with the Article Number
    READ TABLE gt-zarn_reg_hdr INDEX 1 INTO ls_zarn_reg_hdr
      TRANSPORTING matnr.
    IF sy-subrc NE 0 OR ls_zarn_reg_hdr-matnr IS INITIAL.
      CLEAR ev_result_ok.
    ELSE.
      SELECT SINGLE meinh FROM marm INTO lv_meinh_marm
        WHERE matnr = ls_zarn_reg_hdr-matnr
          AND meinh = lv_meinh.
      IF sy-subrc NE 0.
        CLEAR ev_result_ok.
      ENDIF.
    ENDIF.
  ENDIF.


ENDMETHOD.


METHOD complex_rl17.

  DATA: lv_meinh        TYPE meinh,
        ls_zarn_reg_uom TYPE zarn_reg_uom.

  "check whether UNIT is the same / different as Base/Order/Sales/Issue Unit

  "PAR 1 has fieldname of 1 of the "ticks" in ZARN_REG_UOM
  "UNIT_BASE, UNIT_PURORD, SALES_UNIT, ISSUE_UNIT
  "PAR 2 optional - can have specific UOM value (for example EA).
  "If populated then check field is ignored and parameter value is used instead

  FIELD-SYMBOLS <lv_uom_tick> TYPE xfeld.

  IF is_rule-complex_l_par2 IS INITIAL.
    lv_meinh = iv_field_value.
  ELSE.
    lv_meinh = is_rule-complex_l_par2.
  ENDIF.

  "assume the condition is not met
  CLEAR ev_result_ok.

  LOOP AT gt-zarn_reg_uom INTO ls_zarn_reg_uom
     WHERE meinh = lv_meinh.

    ASSIGN COMPONENT is_rule-complex_l_par1 OF STRUCTURE ls_zarn_reg_uom
     TO <lv_uom_tick>.
    IF sy-subrc NE 0.
      RAISE EXCEPTION TYPE zcx_arn_validation_err
        EXPORTING
          msgv1 = text-erc
          msgv2 = |{ is_rule-rule_id WIDTH = 50 }|
          msgv3 = text-e19.
    ENDIF.

    IF <lv_uom_tick> EQ abap_true.
      "the corresponding UOM has required tick
      "then return OK, otherwise error
      ev_result_ok = abap_true.
      EXIT.
    ENDIF.
  ENDLOOP.


ENDMETHOD.


METHOD complex_rl18.

  "Check if value exists in DB, not buffered

  "PAR 1 = table name
  "PAR 2 = field name
  "PAR 3 = dont allow empty value

  DATA: lv_table_name   TYPE string,
        lv_where        TYPE string,
        dref            TYPE REF TO data,
        lv_data_type    TYPE string.

  FIELD-SYMBOLS <lv_target> TYPE any.

  CLEAR ev_result_ok.

  IF is_rule-complex_l_par3 IS INITIAL AND iv_field_value IS INITIAL.
    "allowed empty value, dont proceed, OK
    ev_result_ok = abap_true.
    RETURN.
  ENDIF.

  lv_where = is_rule-complex_l_par2 && ` = iv_field_value`.

  "create target for selection - just a single field
  lv_data_type = is_rule-complex_l_par1 && `-` && is_rule-complex_l_par2.
  TRY.
      CREATE DATA dref TYPE (lv_data_type).
    CATCH cx_sy_create_data_error.
      RAISE EXCEPTION TYPE zcx_arn_validation_err
        EXPORTING
          msgv1 = text-erc
          msgv2 = |{ is_rule-rule_id WIDTH = 50 }|
          msgv3 = text-e15.
  ENDTRY.

  ASSIGN dref->* TO <lv_target>.
  IF sy-subrc NE 0.
      RAISE EXCEPTION TYPE zcx_arn_validation_err
        EXPORTING
          msgv1 = text-erc
          msgv2 = |{ is_rule-rule_id WIDTH = 50 }|
          msgv3 = text-e15.
  ENDIF.

  TRY.
      "select values from DB
      SELECT SINGLE (is_rule-complex_l_par2) FROM (is_rule-complex_l_par1)
        INTO <lv_target>
        WHERE (lv_where).
      IF sy-subrc EQ 0.
        ev_result_ok  = abap_true.
      ENDIF.

    CATCH cx_sy_open_sql_db.
      RAISE EXCEPTION TYPE zcx_arn_validation_err
        EXPORTING
          msgv1 = text-erc
          msgv2 = |{ is_rule-rule_id WIDTH = 50 }|
          msgv3 = text-e15.
    CATCH cx_sy_dynamic_osql_semantics.
      RAISE EXCEPTION TYPE zcx_arn_validation_err
        EXPORTING
          msgv1 = text-erc
          msgv2 = |{ is_rule-rule_id WIDTH = 50 }|
          msgv3 = text-e15.
    CATCH cx_sy_dynamic_osql_syntax.
      RAISE EXCEPTION TYPE zcx_arn_validation_err
        EXPORTING
          msgv1 = text-erc
          msgv2 = |{ is_rule-rule_id WIDTH = 50 }|
          msgv3 = text-e15.
  ENDTRY.

ENDMETHOD.


METHOD complex_rl19.

  DATA: lv_meinh TYPE meinh,
        ls_zarn_reg_uom TYPE zarn_reg_uom,
        lv_category     TYPE zarn_uom_category.

  "check whether UOM is in category

  ev_result_ok = abap_true.

  lv_meinh = iv_field_value.

  CLEAR lv_category.
  IF is_rule-table_name EQ gc-tb_zarn_reg_uom.
    ls_zarn_reg_uom = is_check_record.
    lv_category = ls_zarn_reg_uom-uom_category.
  ENDIF.

  IF gt_buf-zarn_uom_cat[] IS INITIAL.
    SELECT * FROM zarn_uom_cat INTO TABLE gt_buf-zarn_uom_cat.
  ENDIF.

  IF lv_category IS NOT INITIAL.
    "check UOM + category
    READ TABLE  gt_buf-zarn_uom_cat WITH TABLE KEY  uom = lv_meinh
                                                    uom_category = lv_category
                            TRANSPORTING NO FIELDS.
    IF sy-subrc NE 0.
      CLEAR ev_result_ok.
    ENDIF.
  ELSE.
    "check just UOM
    READ TABLE  gt_buf-zarn_uom_cat WITH KEY  uom = lv_meinh
                                 TRANSPORTING NO FIELDS.
    IF sy-subrc NE 0.
      CLEAR ev_result_ok.
    ENDIF.
  ENDIF.


ENDMETHOD.


METHOD complex_rl20.


  "this check is comparing some of
  "UNIT_BASE, UNIT_PURORD, SALES_UNIT, ISSUE_UNIT
  "PAR1 = one of the list above
  "PAR2 = one of the list above

  DATA: lv_where        TYPE string,
        ls_zarn_reg_uom TYPE zarn_reg_uom.

  FIELD-SYMBOLS <lv_uom_tick> TYPE xfeld.

  lv_where = is_rule-complex_l_par1 && ` = abap_true`.

  TRY.
      LOOP AT gt-zarn_reg_uom INTO ls_zarn_reg_uom
        WHERE (lv_where).
        "get the uom the record for first specified uom (for example UNIT_BASE)

        "then check whether it also 2nd uom (for example SALES_UNIT)
        ASSIGN COMPONENT is_rule-complex_l_par2 OF STRUCTURE ls_zarn_reg_uom TO <lv_uom_tick>.
        IF sy-subrc NE 0.
          "incorrect configuration
          RAISE EXCEPTION TYPE cx_sy_itab_dyn_loop.
        ENDIF.

        IF <lv_uom_tick> = abap_true.
          ev_result_ok = abap_true.
        ELSE.
          ev_result_ok = abap_false.
        ENDIF.
      ENDLOOP.
      IF sy-subrc NE 0.
        "not found required UOM = error
        ev_result_ok = abap_false.
      ENDIF.

    CATCH cx_sy_itab_dyn_loop.
      RAISE EXCEPTION TYPE zcx_arn_validation_err
        EXPORTING
          msgv1 = text-erc
          msgv2 = |{ is_rule-rule_id WIDTH = 50 }|
          msgv3 = text-e20.

  ENDTRY.

ENDMETHOD.


METHOD complex_rl21.

  "first UOM category is contained in check field
  "second UOM category has to be within the field of same check record
  "field name is configured as PAR1
  "PAR2 can have value EQ (must be equal),
  "GT (must be greater than), GE (must be greater or equal)

  DATA : lv_result TYPE char005,
         ls_uom_category_check_field TYPE zarn_uomcategory,
         ls_uom_category_par_field   TYPE zarn_uomcategory.

  FIELD-SYMBOLS <lv_category> TYPE zarn_uom_category.

  ASSIGN COMPONENT is_rule-complex_l_par1
      OF STRUCTURE is_check_record TO <lv_category>.
  IF sy-subrc NE 0.
    RAISE EXCEPTION TYPE zcx_arn_validation_err
      EXPORTING
        msgv1 = text-erc
        msgv2 = |{ is_rule-rule_id WIDTH = 50 }|
        msgv3 = text-e20.
  ENDIF.

  IF gt_buf-zarn_uomcategory[] IS INITIAL.
    SELECT * FROM zarn_uomcategory INTO TABLE gt_buf-zarn_uomcategory.
  ENDIF.

  READ TABLE gt_buf-zarn_uomcategory INTO ls_uom_category_check_field
  WITH TABLE KEY uom_category = iv_field_value.
  IF sy-subrc NE 0.
    ev_result_ok = abap_false.
    RETURN.
  ENDIF.


  READ TABLE gt_buf-zarn_uomcategory INTO ls_uom_category_par_field
      WITH TABLE KEY uom_category = <lv_category>.
  IF sy-subrc NE 0.
    ev_result_ok = abap_false.
    RETURN.
  ENDIF.

  "start with error, only if condition is fulfilled
  "change to OK
  ev_result_ok = abap_false.

  CASE is_rule-complex_l_par2.
    WHEN 'EQ'.
      IF ls_uom_category_check_field-cat_seqno =
         ls_uom_category_par_field-cat_seqno.
        ev_result_ok = abap_true.
      ENDIF.
    WHEN 'GT'.
      IF ls_uom_category_check_field-cat_seqno >
         ls_uom_category_par_field-cat_seqno.
        ev_result_ok = abap_true.
      ENDIF.
    WHEN 'GE'.
      IF ls_uom_category_check_field-cat_seqno >=
         ls_uom_category_par_field-cat_seqno.
        ev_result_ok = abap_true.
      ENDIF.
    WHEN OTHERS.
      RAISE EXCEPTION TYPE zcx_arn_validation_err
        EXPORTING
          msgv1 = text-erc
          msgv2 = |{ is_rule-rule_id WIDTH = 50 }|
          msgv3 = text-e20.
  ENDCASE.

ENDMETHOD.


METHOD complex_rl22.

  "check maximum length of field
  "PAR1 having maximum allowed length
  "PAR2 having "additional description" - for example "digits"

  ev_result_ok = abap_true.

  IF strlen( iv_field_value ) > is_rule-complex_l_par1.
    ev_result_ok = abap_false.
    ev_addit_inf  = is_rule-complex_l_par1 && ` ` && is_rule-complex_l_par2.
    TRANSLATE ev_addit_inf TO LOWER CASE.
  ENDIF.

  "tbd catch exception if value in PAR1 is not numeric!!

ENDMETHOD.


METHOD complex_rl23.

  "check whether vendor data is maintained in LFM1
  "for specified Purch. Organisations

  "allow only ZARN_REG_PIR
  check_allowed_table( is_rule      = is_rule
                       iv_allwd_tb1 = gc-tb_zarn_reg_pir ).

  DATA: ls_pur_ekorg    LIKE LINE OF gt_buf-pur_ekorg,
        lv_lifnr        TYPE lfm1-lifnr,
        ls_zarn_reg_pir TYPE zarn_reg_pir.

  ls_zarn_reg_pir = is_check_record.

  ev_result_ok = abap_true.
  "check only if vendor populated
  CHECK ls_zarn_reg_pir-lifnr IS NOT INITIAL.

  IF gt_buf-pur_ekorg[] IS INITIAL.
    SELECT * FROM tvarvc INTO TABLE gt_buf-pur_ekorg
       WHERE name = gc-pur_ekorg_tvarv.
  ENDIF.

  "tvarv is having all required purch. organisations check if
  "we have existing records in LFM1 for all of them
  LOOP AT gt_buf-pur_ekorg INTO ls_pur_ekorg.
    SELECT SINGLE lifnr FROM lfm1 INTO lv_lifnr
      WHERE lifnr = ls_zarn_reg_pir-lifnr
        AND ekorg = ls_pur_ekorg-low.
    IF sy-subrc NE 0.
      ev_result_ok = abap_false.
      ev_addit_inf = ls_pur_ekorg-low.
      EXIT.
    ENDIF.
  ENDLOOP.


ENDMETHOD.


METHOD COMPLEX_RL24.
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE           # 16/06/2016
* CHANGE No.     # Feature 3128
* DESCRIPTION    # New complex rule to verify EAN check digit
* WHO            # C90001929, Aruna Mackenjee
*-----------------------------------------------------------------------

  " EAN check digit validation

  DATA: ls_zarn_reg_ean   TYPE zarn_reg_ean,
        lv_chk_digit_algo TYPE prfza,
        lv_ok             TYPE boolean.

  " Set to valid by default
  ev_result_ok = abap_true.

  ls_zarn_reg_ean = is_check_record.

  CALL FUNCTION 'EAN_GET_PRFZA'
    EXPORTING
      i_numtp       = ls_zarn_reg_ean-eantp
    IMPORTING
      E_PRFZA       = lv_chk_digit_algo
            .

  IF lv_chk_digit_algo IS NOT INITIAL.
    " Check digit algorithm exists so verify check digit
    CALL FUNCTION 'EAN_VERIFY_CHECKDIGIT'
      IMPORTING
        CHECKDIGIT_OK       = lv_ok
      CHANGING
        i_ean               = ls_zarn_reg_ean-ean11
              .
    IF NOT lv_ok = abap_true.
      ev_result_ok = abap_false.
    ENDIF.
  ENDIF.

ENDMETHOD.


  METHOD complex_rl25.
*-----------------------------------------------------------------------
* DATE           # 23/06/2016
* CHANGE No.     # Feature 3128
* DESCRIPTION    # New complex rule to verify if a MC has MC Ref.Artilcle
* WHO            # I90003180, Ponn Govindasamy
*-----------------------------------------------------------------------

    "Check if value exists in DB table configured, not buffered
    " with just one condition while selecting record

    "PAR 1 = table name to be checked
    "PAR 2 = field name
    "PAR 3 = another field name on which further condition is defined
    "PAR 4 = actual condition on PAR 3 E.g: NE SPACE; EQ 'X'; IS INITIAL; EQ '123'.

    DATA: lv_table_name TYPE string,
          lv_where1     TYPE string,
          lv_where2     TYPE string,
          dref          TYPE REF TO data,
          lv_data_type  TYPE string.

    FIELD-SYMBOLS <lv_target> TYPE any.

    CLEAR ev_result_ok.

    IF iv_field_value IS INITIAL.
      "allowed empty value, dont proceed, OK
      ev_result_ok = abap_true.
      RETURN.
    ENDIF.

    lv_where1 = is_rule-complex_l_par2 && ` = iv_field_value`.
    CONCATENATE is_rule-complex_l_par3 is_rule-complex_l_par4 INTO lv_where2 SEPARATED BY SPACE.

    "create target for selection - just a single field
    lv_data_type = is_rule-complex_l_par1 && `-` && is_rule-complex_l_par2.
    TRY.
        CREATE DATA dref TYPE (lv_data_type).
      CATCH cx_sy_create_data_error.
        RAISE EXCEPTION TYPE zcx_arn_validation_err
          EXPORTING
            msgv1 = text-erc
            msgv2 = |{ is_rule-rule_id WIDTH = 50 }|
            msgv3 = text-e15.
    ENDTRY.

    ASSIGN dref->* TO <lv_target>.
    IF sy-subrc NE 0.
      RAISE EXCEPTION TYPE zcx_arn_validation_err
        EXPORTING
          msgv1 = text-erc
          msgv2 = |{ is_rule-rule_id WIDTH = 50 }|
          msgv3 = text-e15.
    ENDIF.

    TRY.
        "select values from DB
        SELECT SINGLE (is_rule-complex_l_par2) FROM (is_rule-complex_l_par1)
          INTO <lv_target>
          WHERE (lv_where1) AND
                (lv_where2).
        IF sy-subrc EQ 0.
          ev_result_ok  = abap_true.
        ENDIF.

      CATCH cx_sy_open_sql_db.
        RAISE EXCEPTION TYPE zcx_arn_validation_err
          EXPORTING
            msgv1 = text-erc
            msgv2 = |{ is_rule-rule_id WIDTH = 50 }|
            msgv3 = text-e15.
      CATCH cx_sy_dynamic_osql_semantics.
        RAISE EXCEPTION TYPE zcx_arn_validation_err
          EXPORTING
            msgv1 = text-erc
            msgv2 = |{ is_rule-rule_id WIDTH = 50 }|
            msgv3 = text-e15.
      CATCH cx_sy_dynamic_osql_syntax.
        RAISE EXCEPTION TYPE zcx_arn_validation_err
          EXPORTING
            msgv1 = text-erc
            msgv2 = |{ is_rule-rule_id WIDTH = 50 }|
            msgv3 = text-e15.
    ENDTRY.
  ENDMETHOD.


METHOD complex_rl26.

  "check value of vendor master field (LFA1 + LFM1 etc...)
  "against configured ranges
  "vendor master field has to be present in structure TY_S_VENDOR_DATA
  "IV_FIELD_VALUE is ignored here

  "for comparison between arena DB field name and Vendor Master Field
  "use complex logic 10

  "PAR1 - vendor master field

  DATA: ls_zarn_reg_pir TYPE zarn_reg_pir,
        ls_vendor_data  TYPE ty_s_vendor_data.

  FIELD-SYMBOLS: <lv_vm_field> TYPE any. "field of vendor master data

  check_allowed_table( is_rule      = is_rule
                       iv_allwd_tb1 = gc-tb_zarn_reg_pir ).

  ev_result_ok = abap_true.
  ls_zarn_reg_pir = is_check_record.

  ls_vendor_data = complex_rl_util_get_vnd_data( ls_zarn_reg_pir-lifnr ).


  ASSIGN COMPONENT is_rule-complex_l_par1 OF STRUCTURE  ls_vendor_data
  TO <lv_vm_field>.
  IF sy-subrc NE 0.
    RAISE EXCEPTION TYPE zcx_arn_validation_err
      EXPORTING
        msgv1 = text-erc
        msgv2 = |{ is_rule-rule_id WIDTH = 50 }|
        msgv3 = text-e17.
  ENDIF.

  IF <lv_vm_field> NOT IN is_rule-ranges.
    ev_result_ok = abap_false.
  ENDIF.

ENDMETHOD.


METHOD complex_rl27.
*-----------------------------------------------------------------------
* PROJECT          # ARENA
* DATE WRITTEN     # 18.07.2016
* SYSTEM           # DE0
* SAP VERSION      # ECC6
* TYPE             # Class method
* AUTHOR           # C90003202, Howard Chow
*-----------------------------------------------------------------------
* TITLE            # Complex check 27
* PURPOSE          # To check table ZARN_REG_EAN for each combination of
*                    ID and UoM does not exceed the limit
* ASSUMPTION       #
*-----------------------------------------------------------------------
* CALLED FROM      #
*-----------------------------------------------------------------------
* CALLS TO         #
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  DATA:
    ls_reg_ean      TYPE zarn_reg_ean,
    lt_reg_ean      TYPE ztarn_reg_ean,
    lt_reg_ean_t    TYPE ztarn_reg_ean,
    lv_limit        TYPE i.


* check for header table, to run check once per document
  me->check_allowed_table( is_rule      = is_rule
                           iv_allwd_tb1 = gc-tb_zarn_reg_hdr ).

* get the limit from the rule PARAMETER 1
  lv_limit = is_rule-complex_l_par1.

* if the current total number of record is less than limit, pass check
  IF lines( me->gt-zarn_reg_ean ) LE lv_limit.
    ev_result_ok = abap_true.
    RETURN.
  ENDIF.

* get all unique combination of ID and UoM
  lt_reg_ean = me->gt-zarn_reg_ean.
  SORT lt_reg_ean BY idno meinh.
  DELETE ADJACENT DUPLICATES FROM lt_reg_ean
    COMPARING idno meinh.

* per each unique combination of ID and UoM, check if the number of
* record is more than limit
  LOOP AT lt_reg_ean INTO ls_reg_ean.

    lt_reg_ean_t = me->gt-zarn_reg_ean.
    DELETE lt_reg_ean_t
      WHERE idno  NE ls_reg_ean-idno
        OR  meinh NE ls_reg_ean-meinh.

    IF lines( lt_reg_ean_t ) GT lv_limit.
      ev_result_ok = abap_false.
      RETURN.
    ENDIF.
  ENDLOOP.

  ev_result_ok = abap_true.


ENDMETHOD.


METHOD complex_rl28.
*-----------------------------------------------------------------------
* PROJECT          # ARENA
* DATE WRITTEN     # 18.07.2016
* SYSTEM           # DE0
* SAP VERSION      # ECC6
* TYPE             # Class method
* AUTHOR           # C90003202, Howard Chow
*-----------------------------------------------------------------------
* TITLE            # Complex check 28
* PURPOSE          # To check for GTIN category ZD has corresponding
*                    RETAIL barcode, eg IE, ZP, AR
* ASSUMPTION       #
*-----------------------------------------------------------------------
* CALLED FROM      #
*-----------------------------------------------------------------------
* CALLS TO         #
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  DATA:
    ls_reg_header TYPE zarn_reg_hdr,
    lt_reg_uom    TYPE ztarn_reg_uom,
    ls_reg_uom    LIKE LINE OF lt_reg_uom,
    lt_reg_ean    TYPE ztarn_reg_ean,
    lt_reg_ean_t  TYPE ztarn_reg_ean.


* check for header table, to run check once per document
  me->check_allowed_table( is_rule      = is_rule
                           iv_allwd_tb1 = gc-tb_zarn_reg_hdr ).

* get legacy retail category from header, which should only be one record
  READ TABLE me->gt-zarn_reg_hdr INTO ls_reg_header INDEX 1
    TRANSPORTING leg_retail.
* if article has no legacy retail category, not relevant so check pass
  IF ls_reg_header-leg_retail IS INITIAL.
    ev_result_ok = abap_true.
    RETURN.
  ENDIF.

* only check UoM of same category as legacy retail category
  lt_reg_uom = me->gt-zarn_reg_uom.
  DELETE lt_reg_uom WHERE uom_category NE ls_reg_header-leg_retail.
* if none found, check pass
  IF lt_reg_uom IS INITIAL.
    ev_result_ok = abap_true.
    RETURN.
  ENDIF.

* collect all EAN records of the UoM
  LOOP AT lt_reg_uom INTO ls_reg_uom.
    lt_reg_ean_t = me->gt-zarn_reg_ean.
    DELETE lt_reg_ean_t
      WHERE idno  NE ls_reg_uom-idno
        OR  meinh NE ls_reg_uom-meinh.
    APPEND LINES OF lt_reg_ean_t TO lt_reg_ean.
  ENDLOOP.
* if none found, check pass
  IF lt_reg_ean IS INITIAL.
    ev_result_ok = abap_true.
    RETURN.
  ENDIF.

* check if any of the EAN has GTIN category ZD
  READ TABLE lt_reg_ean
    WITH KEY eantp = zif_gtin_category_c=>gc_duncode_shipper_barcode
    TRANSPORTING NO FIELDS.
* if none found, check pass
  IF sy-subrc NE 0.
    ev_result_ok = abap_true.
    RETURN.
  ENDIF.

* if GTIN category ZD found in any of the UoM, then check if any of the
* UoM also has the GTIN category in the rule range, eg. IE, ZP, ZR
  LOOP AT lt_reg_ean TRANSPORTING NO FIELDS
    WHERE eantp IN is_rule-ranges.

    ev_result_ok = abap_true.
    RETURN.
  ENDLOOP.

* if none found, check fail
  IF sy-subrc NE 0.
    ev_result_ok = abap_false.
    RETURN.
  ENDIF.


ENDMETHOD.


METHOD complex_rl29.

  "check whether value of configured field
  "changed against what is in database

  "Even though the logic is generic database selects
  "are implement statically for each table
  "currently supported only for ZARN_REG_HDR and ZARN_REG_BANNER
  "can be easily extended for any table
  "naming conventions LS_*_DB has to be followed for data declarations.

  DATA: lv_struc_name         TYPE char30,
        ls_zarn_reg_hdr       TYPE zarn_reg_hdr,
        ls_zarn_reg_hdr_db    TYPE zarn_reg_hdr,
        ls_zarn_reg_banner    TYPE zarn_reg_banner,
        ls_zarn_reg_banner_db TYPE zarn_reg_banner.

  check_allowed_table( is_rule      = is_rule
                       iv_allwd_tb1 = gc-tb_zarn_reg_hdr
                       iv_allwd_tb2 = gc-tb_zarn_reg_banner ).

  "start with assumption we are entering a new value
  ev_result_ok = abap_true.

  CASE is_rule-table_name.
    WHEN gc-tb_zarn_reg_hdr.
      ls_zarn_reg_hdr = is_check_record.
      SELECT SINGLE * FROM zarn_reg_hdr INTO  ls_zarn_reg_hdr_db
        WHERE idno = ls_zarn_reg_hdr-idno.
      IF sy-subrc NE 0.
        "record not even found - value is changing
        RETURN.
      ENDIF.

    WHEN gc-tb_zarn_reg_banner.
      ls_zarn_reg_banner = is_check_record.
      SELECT SINGLE * FROM zarn_reg_banner INTO ls_zarn_reg_banner_db
        WHERE idno   = ls_zarn_reg_banner-idno
          AND banner = ls_zarn_reg_banner-banner.
      IF sy-subrc NE 0.
        "record not even found - value is changing
        RETURN.
      ENDIF.

    WHEN OTHERS.
      "other tables currently not supported, feel free to extend
      "exception will be raised directly from check_allowed_table

  ENDCASE.

  lv_struc_name = `LS_` && is_rule-table_name && `_DB`.
  ASSIGN (lv_struc_name) TO FIELD-SYMBOL(<ls_db>).

  ASSIGN COMPONENT is_rule-field_name OF STRUCTURE <ls_db> TO FIELD-SYMBOL(<lv_val_db>).

  IF <lv_val_db> = iv_field_value.
    "only if the value is the same (no change), return false
    ev_result_ok = abap_false.
  ENDIF.


ENDMETHOD.


METHOD complex_rl30.

  "combines check EINE relevant or EINA relevant

  DATA ls_zarn_reg_pir TYPE zarn_reg_pir.

  check_allowed_table( is_rule      = is_rule
                       iv_allwd_tb1 = gc-tb_zarn_reg_pir ).

  ls_zarn_reg_pir = is_check_record.

  IF ls_zarn_reg_pir-pir_rel_eina = abap_true
    OR ls_zarn_reg_pir-pir_rel_eine = abap_true.
    ev_result_ok = abap_true.
  ELSE.
    ev_result_ok = abap_false.
  ENDIF.

ENDMETHOD.


  METHOD complex_rl31.

    DATA lv_retail_ranging TYPE flag.

    CLEAR lv_retail_ranging.

    "Check whether Ranging flag for Retail is ticked

    CALL FUNCTION 'ZARN_DETERMINE_RET_RANGING'
      EXPORTING
        it_zarn_reg_banner = gt-zarn_reg_banner
      CHANGING
        cv_ret_ranging     = lv_retail_ranging.

    ev_result_ok = lv_retail_ranging.

  ENDMETHOD.


METHOD COMPLEX_RL32.

   DATA lv_wholesale_ranging TYPE flag.

   CLEAR lv_wholesale_ranging.

   "check ranging flag for Wholesale is ticked

    CALL FUNCTION 'ZARN_DETERMINE_WHL_RANGING'
      EXPORTING
        it_zarn_reg_banner = gt-zarn_reg_banner
      CHANGING
        cv_whl_ranging     = lv_wholesale_ranging.

    ev_result_ok = lv_wholesale_ranging.


ENDMETHOD.


  METHOD complex_rl33.
    TYPES: BEGIN OF ty_eankey,
             idno  TYPE zarn_idno,
             meinh TYPE meinh,
           END OF ty_eankey.

    DATA: lt_eankey             TYPE TABLE OF ty_eankey,
          ls_eankey             LIKE LINE OF lt_eankey,
          ls_zarn_reg_ean       TYPE zarn_reg_ean,
          ls_zarn_reg_ean_other TYPE zarn_reg_ean.

    "At-least 1 main barcode per UoM.

    check_allowed_table( is_rule      = is_rule
                         iv_allwd_tb1 = gc-tb_zarn_reg_hdr ).

    ev_result_ok = abap_true.

    MOVE-CORRESPONDING gt-zarn_reg_ean TO lt_eankey.
    SORT lt_eankey ASCENDING BY idno meinh.
    DELETE ADJACENT DUPLICATES FROM lt_eankey COMPARING ALL FIELDS.


    LOOP AT lt_eankey INTO ls_eankey.
      IF NOT line_exists( gt-zarn_reg_ean[
               idno  = ls_eankey-idno
               meinh = ls_eankey-meinh
               hpean = 'X' ] ).
        ev_result_ok = abap_false.
        ev_addit_inf = ls_eankey-meinh.
      ENDIF.
      CLEAR: ls_eankey.
    ENDLOOP.

  ENDMETHOD.


  METHOD complex_rl34.
    "this check is comparing some of
    "PAR1 = EG: UNIT_BASE, UNIT_PURORD, SALES_UNIT, ISSUE_UNIT (Comma separated field list)
    "PAR2 = EG: X (Value to be checked)
    "PAR3 = EG: 1 (Max cnt for the column+value)
    "PAR4 = EG: ZARN_REG_UOM (The table to check)


    DATA: lv_where        TYPE string.

    DATA: lv_field_cnt TYPE i,
          lv_max_val   TYPE i VALUE 1. "Default to 1

    FIELD-SYMBOLS <lv_uom_tick> TYPE xfeld.

    FIELD-SYMBOLS: <lt_dyn> TYPE ANY TABLE.


* Assume success
    ev_result_ok = abap_true.

* Get the fields
    SPLIT is_rule-complex_l_par1 AT ',' INTO TABLE DATA(lt_fields).

* Get where value part
    IF is_rule-complex_l_par2 IS NOT INITIAL.
      DATA(lv_check_val) = | = '{ is_rule-complex_l_par2 }' |.
    ELSE.
      lv_check_val = | IS NOT INITIAL|.
    ENDIF.

* Get the max values
    IF is_rule-complex_l_par3 IS NOT INITIAL.
      TRY.
          lv_max_val = is_rule-complex_l_par3.
        CATCH cx_sy_conversion_no_number.
          RAISE EXCEPTION TYPE zcx_arn_validation_err
            EXPORTING
              msgv1 = text-erc
              msgv2 = |{ is_rule-rule_id WIDTH = 50 }|
              msgv3 = text-e20.
      ENDTRY.
    ENDIF.

* Get the table
    ASSIGN COMPONENT is_rule-complex_l_par4 OF STRUCTURE me->gt TO <lt_dyn>.
    IF sy-subrc <> 0. " Error
      RAISE EXCEPTION TYPE zcx_arn_validation_err
        EXPORTING
          msgv1 = text-erc
          msgv2 = |{ is_rule-rule_id WIDTH = 50 }|
          msgv3 = text-e20.
    ENDIF.


    TRY.
* For each field passed, check if the value condition exists across multiple records
        LOOP AT lt_fields ASSIGNING FIELD-SYMBOL(<lv_field>).
          CLEAR lv_field_cnt.
          lv_where = <lv_field> && lv_check_val.
          LOOP AT <lt_dyn> ASSIGNING FIELD-SYMBOL(<ls_dyn>)
            WHERE (lv_where).
            ADD 1 TO lv_field_cnt.
          ENDLOOP.
          IF lv_field_cnt > lv_max_val.
            ev_addit_inf = <lv_field>.
            ev_result_ok = abap_false.
            EXIT.
          ENDIF.
        ENDLOOP.
      CATCH cx_sy_itab_dyn_loop.
        RAISE EXCEPTION TYPE zcx_arn_validation_err
          EXPORTING
            msgv1 = text-erc
            msgv2 = |{ is_rule-rule_id WIDTH = 50 }|
            msgv3 = text-e20.
    ENDTRY.

  ENDMETHOD.


  METHOD complex_rl35.
    DATA: lo_pbs_data_check TYPE REF TO zcl_arn_pbs_data_check,
          lv_error          TYPE flag,
          lv_vtweg          TYPE vtweg,
          lt_marc           TYPE TABLE OF marc,
          ls_marc           TYPE marc,
          ls_marc_pbs       TYPE marc,
          ls_reg_hdr        TYPE zarn_reg_hdr,
          ls_reg_banner     TYPE zarn_reg_banner,
          lt_refsite        TYPE TABLE OF tvarvc,
          ls_refsite        TYPE tvarvc.

    ev_result_ok = abap_true.

    CLEAR: lt_refsite[], lt_marc[], ls_marc, ls_refsite.
    SELECT * FROM tvarvc INTO TABLE lt_refsite
      WHERE ( name = 'ZC_REF_VKORG_VTREFS' AND low = iv_field_value ) OR
            ( name = 'ZMD_VKORG_REF_SITES_LNI' AND low = iv_field_value ) OR
            ( name = 'ZC_DEF_VTWEG_VKORG' AND low <> '1000' ).

    READ TABLE gt-zarn_reg_hdr INTO ls_reg_hdr INDEX 1.
    ls_reg_banner = is_check_record.

    IF lt_refsite[] IS NOT INITIAL.
      LOOP AT lt_refsite INTO ls_refsite.
        ls_marc-matnr = ls_reg_hdr-matnr.
        ls_marc-werks = ls_refsite-high.
        IF ls_refsite-name = 'ZC_REF_VKORG_VTREFS'.
          ls_marc-bwscl    = ls_reg_banner-source_uni.
          APPEND ls_marc TO lt_marc.
        ELSEIF ls_refsite-name = 'ZMD_VKORG_REF_SITES_LNI'.
          ls_marc-bwscl    = ls_reg_banner-source_lni.
          APPEND ls_marc TO lt_marc.
        ENDIF.
        CLEAR: ls_marc, ls_refsite.
      ENDLOOP.
    ENDIF.

    CREATE OBJECT lo_pbs_data_check.

    READ TABLE lt_refsite INTO ls_refsite WITH KEY name = 'ZC_DEF_VTWEG_VKORG'.
    lv_vtweg = ls_refsite-high.

    CLEAR: lv_error.
    lv_error = lo_pbs_data_check->get_marc_pbs_error(
                  EXPORTING iv_matnr     = ls_reg_hdr-matnr
                            iv_vkorg     = iv_field_value
                            iv_vtweg     = lv_vtweg
                            iv_bwscl     = zcl_arn_pbs_data_check=>mc_bwscl_1   "'1'
                            iv_pbs_code  = ls_reg_banner-pbs_code
                            it_marc_data = lt_marc[]
                  IMPORTING es_marc      = ls_marc_pbs     ).

    IF lv_error = abap_true.
      ev_result_ok = abap_false.
      ev_addit_inf = ls_marc_pbs-werks.
    ENDIF.

  ENDMETHOD.


  METHOD complex_rl36.

    DATA: lv_excluded   TYPE char1,
          ls_reg_banner TYPE zarn_reg_banner.

    ev_result_ok = abap_true.

    CLEAR: lv_excluded.

    ls_reg_banner = is_check_record.

    TRY.
        CALL METHOD zcl_onl_art_range_exclusion=>is_article_excluded
          EXPORTING
            iv_arena_id_num = ls_reg_banner-idno
            iv_banner       = ls_reg_banner-banner
          RECEIVING
            rv_value        = lv_excluded.
      CATCH zcx_invalid_input.
    ENDTRY.

    IF lv_excluded IS NOT INITIAL.
      ev_result_ok = abap_false.
    ENDIF.

  ENDMETHOD.


  METHOD complex_rl37.

    DATA: ls_reg_banner   TYPE zarn_reg_banner,
          ls_reg_hdr      TYPE zarn_reg_hdr,
          lt_marc         TYPE marc_tt,
          lt_onl_stat_str TYPE ztmd_onl_stat_str,
          ls_onl_stat_str TYPE zmd_onl_stat_str.

    ev_result_ok = abap_true.

    CLEAR: ls_reg_hdr, ls_reg_banner.
    READ TABLE gt-zarn_reg_hdr INTO ls_reg_hdr INDEX 1.
    ls_reg_banner = is_check_record.

    IF ls_reg_banner IS NOT INITIAL OR
       ls_reg_hdr IS NOT INITIAL.

* DEL Begin of Change NCDRE-2 JKH 14.06.2017
*      CLEAR lt_marc[].
*      SELECT * FROM marc AS a
*      INNER JOIN zsaw_sites_ctrl AS b ON b~werks = a~werks
*      INNER JOIN t001w AS c ON c~werks = a~werks
*      WHERE a~matnr           = @ls_reg_hdr-matnr AND
*            a~zzonline_status <> ' ' AND
*            b~external_system = '04' AND
*            c~vkorg           = @ls_reg_banner-banner
*      INTO CORRESPONDING FIELDS OF TABLE @lt_marc[].
* DEL End of Change NCDRE-2 JKH 14.06.2017

* INS Begin of Change NCDRE-2 JKH 14.06.2017
* Get MARCs for article
      CLEAR lt_marc[].
      SELECT * FROM marc
      INTO CORRESPONDING FIELDS OF TABLE @lt_marc[]
      WHERE matnr           EQ @ls_reg_hdr-matnr
        AND zzonline_status NE ' '.
      IF sy-subrc = 0.
* Get all Online Live Sites
        DATA(lt_live_site_banner) = zcl_onl_md_interfaces=>get_live_site_banner( ).
        LOOP AT lt_marc[] INTO DATA(ls_marc).
          DATA(lv_tabix) = sy-tabix.

* If site is not online live for given banner then ignore MARC
          READ TABLE lt_live_site_banner[] TRANSPORTING NO FIELDS
          WITH KEY site   = ls_marc-werks
                   banner = ls_reg_banner-banner.
          IF sy-subrc NE 0.
            DELETE lt_marc[] INDEX lv_tabix.
          ENDIF.
        ENDLOOP.  " LOOP AT lt_marc[] INTO DATA(ls_marc)
      ENDIF.
* INS End of Change NCDRE-2 JKH 14.06.2017

      CLEAR lt_onl_stat_str[].
      SELECT * FROM zmd_onl_stat_str INTO TABLE lt_onl_stat_str[]
        WHERE online_status_banner = ls_reg_banner-online_status.
    ENDIF.

    IF lt_marc[] IS INITIAL OR lt_onl_stat_str[] IS INITIAL.
      EXIT.
    ENDIF.

    LOOP AT lt_onl_stat_str[] INTO ls_onl_stat_str.
      DELETE lt_marc WHERE zzonline_status = ls_onl_stat_str-online_status_store.
    ENDLOOP.
*
    IF NOT lt_marc[] IS INITIAL.
      ev_result_ok = abap_false.
      ev_addit_inf = lt_marc[ 1 ]-werks.
    ENDIF.

  ENDMETHOD.


  METHOD complex_rl38.

*** When user ticks a new regular vendor flag, check that it has the
*** latest PIR change date amongst all PIRs - warning message
*** if it's not the latest.


    DATA: lt_zarn_reg_pir       TYPE ztarn_reg_pir_ui,
          ls_zarn_reg_pir       TYPE zsarn_reg_pir_ui,
          ls_zarn_reg_pir_relif TYPE zarn_reg_pir,
          lt_reg_pir            TYPE ztarn_reg_pir,
          ls_reg_pir            TYPE zarn_reg_pir,
          lt_pir                TYPE ztarn_pir,
          ls_pir                TYPE zarn_pir,
          lv_relif_pir_chg_date TYPE zarn_pir_last_changed_dats.

    FIELD-SYMBOLS: <ls_zarn_reg_pir> TYPE zsarn_reg_pir_ui.


    check_allowed_table( is_rule      = is_rule
                         iv_allwd_tb1 = gc-tb_zarn_reg_pir ).

    ev_result_ok = abap_true.




    lt_reg_pir[] = gt-zarn_reg_pir[].
    lt_pir[]     = gt-zarn_pir[].


    MOVE-CORRESPONDING lt_reg_pir[] TO lt_zarn_reg_pir[].

* Get PIR Last Changed Date from National PIR
    LOOP AT lt_zarn_reg_pir[] ASSIGNING <ls_zarn_reg_pir>.
      CLEAR ls_pir.
      READ TABLE lt_pir[] INTO ls_pir
      WITH KEY idno                 = <ls_zarn_reg_pir>-idno
               uom_code             = <ls_zarn_reg_pir>-order_uom_pim
               hybris_internal_code = <ls_zarn_reg_pir>-hybris_internal_code.
      IF sy-subrc = 0.
        <ls_zarn_reg_pir>-pir_last_changed_date = ls_pir-last_changed_date.
      ENDIF.
    ENDLOOP.


* Get Regular Vendor PIR
    CLEAR ls_zarn_reg_pir_relif.
    READ TABLE lt_zarn_reg_pir[] INTO ls_zarn_reg_pir_relif
    WITH KEY relif = abap_true.
    IF sy-subrc NE 0.
      RETURN.
    ENDIF.


* Step1.  System will select the “PIR Change date” for the PIR record marked as Regular vendor.
    CLEAR: ls_zarn_reg_pir, lv_relif_pir_chg_date.
    READ TABLE lt_zarn_reg_pir[] INTO ls_zarn_reg_pir
    WITH KEY idno         = ls_zarn_reg_pir_relif-idno
             lifnr        = ls_zarn_reg_pir_relif-lifnr
             pir_rel_eina = abap_true.
    IF sy-subrc EQ 0.
      lv_relif_pir_chg_date = ls_zarn_reg_pir-pir_last_changed_date.
      CLEAR: ls_zarn_reg_pir.
    ELSE.
      RETURN.
    ENDIF.



    DELETE lt_zarn_reg_pir WHERE pir_rel_eina IS INITIAL.
    IF lt_zarn_reg_pir[] IS INITIAL.
      ev_result_ok = abap_false.
    ELSE.

* Step2. Then system will select the “PIR Change date” from all the other PIR records marked as “EINA Flag”.
* Get Latest PIR Chaned date where EINA is ticked
      SORT lt_zarn_reg_pir[] BY pir_last_changed_date DESCENDING.
      CLEAR ls_zarn_reg_pir.
      ls_zarn_reg_pir = lt_zarn_reg_pir[ 1 ].

* Step3. Compare the date in step1 and the dates found in Step2.
* If the date in Step1 is not the latest date then system will show the confirmation popup
      IF lv_relif_pir_chg_date LT ls_zarn_reg_pir-pir_last_changed_date.
        ev_result_ok = abap_false.
      ENDIF.

    ENDIF.  " IF lt_zarn_reg_pir[] IS INITIAL

  ENDMETHOD.


  METHOD complex_rl39.

*** Atleast 1 vendor in AReNa is ticked as regular vendor
*** when both Purch and Gen PIR are ticked



    DATA: ls_check_record       TYPE zarn_reg_pir,
          ls_zarn_reg_pir_relif TYPE zarn_reg_pir,
          lt_reg_pir            TYPE ztarn_reg_pir,
          ls_reg_pir            TYPE zarn_reg_pir,
          lt_eina               TYPE STANDARD TABLE OF eina,
          ls_eina               TYPE eina,
          ls_reg_hdr            TYPE zarn_reg_hdr,
          lv_tabix              TYPE sy-tabix.

    FIELD-SYMBOLS: <ls_zarn_reg_pir> TYPE zsarn_reg_pir_ui.


    check_allowed_table( is_rule      = is_rule
                         iv_allwd_tb1 = gc-tb_zarn_reg_pir ).

    ev_result_ok = abap_true.
    ls_check_record = is_check_record.

* Check if same Message is already generated then don't validate again.
    READ TABLE gt_output[] TRANSPORTING NO FIELDS
    WITH KEY validation_id = 'P127'
             table_name     = is_rule-table_name
             field_name     = is_rule-field_name
             idno           = ls_check_record-idno.
    IF sy-subrc = 0.
      RETURN.
    ENDIF.

    lt_reg_pir[] = gt-zarn_reg_pir[].


    SORT lt_reg_pir[] BY lifnr.

* Step1. System will select all the regional PIRs which are having the Purch PIR and Gen PIR is ticked
    LOOP AT lt_reg_pir[] INTO ls_reg_pir.

      lv_tabix = sy-tabix.

      READ TABLE lt_reg_pir[] TRANSPORTING NO FIELDS
      WITH KEY lifnr        = ls_reg_pir-lifnr
               pir_rel_eina = abap_true.
      IF sy-subrc NE 0.
        DELETE lt_reg_pir[] WHERE lifnr = ls_reg_pir-lifnr.
        CONTINUE.
      ENDIF.

      READ TABLE lt_reg_pir[] TRANSPORTING NO FIELDS
      WITH KEY lifnr        = ls_reg_pir-lifnr
               pir_rel_eine = abap_true.
      IF sy-subrc NE 0.
        DELETE lt_reg_pir[] WHERE lifnr = ls_reg_pir-lifnr.
        CONTINUE.
      ENDIF.

    ENDLOOP.

    IF lt_reg_pir[] IS INITIAL.
      RETURN.
    ENDIF.

* Step2. Among the records selected in step1, select record with regular vendor ZARN_REG_PIR-RELIF=’X’
* Step3. If record found in step2, then no error message Else go to step4.
    READ TABLE lt_reg_pir[] TRANSPORTING NO FIELDS
    WITH KEY relif = abap_true.
    IF sy-subrc EQ 0.
      RETURN.
    ENDIF.




* Step4. If no record found in step2, then select the EINA where EINA-RELIF =’X’ and EINA-LIFNR not in
*        ZARN_REG_PIR-LIFNR found in step1.
      READ TABLE gt-zarn_reg_hdr INTO ls_reg_hdr INDEX 1.
      IF ls_reg_hdr-matnr IS NOT INITIAL.
        SELECT infnr matnr lifnr relif
          INTO CORRESPONDING FIELDS OF TABLE lt_eina[]
          FROM eina
          FOR ALL ENTRIES IN lt_reg_pir[]
          WHERE matnr EQ ls_reg_hdr-matnr
            AND lifnr NE lt_reg_pir-lifnr
            AND relif EQ abap_true.
        IF sy-subrc NE 0.
          ev_result_ok = abap_false.  " if RELIF not maintained for any other local pir/vendor
        ENDIF.
      ELSE.
        ev_result_ok = abap_false.  " if article is not posted
      ENDIF.



  ENDMETHOD.


  METHOD complex_rl40.
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             # 06.12.2018
* CHANGE No.       # 9000004644: SAPTR-147 Defect649 ZARN_MASS PIR Update
* DESCRIPTION      # Check record is valid as of today
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------

*** If user enters  any other condition group which is apart from the
*** possible values then warning  message will appear for information.

    DATA: ls_zarn_reg_pir TYPE zarn_reg_pir.

    FIELD-SYMBOLS: <ls_zarn_reg_pir> TYPE zsarn_reg_pir_ui.


    check_allowed_table( is_rule      = is_rule
                         iv_allwd_tb1 = gc-tb_zarn_reg_pir ).

    ev_result_ok    = abap_true.
    ls_zarn_reg_pir = is_check_record.


    IF ls_zarn_reg_pir-lifnr IS INITIAL OR ls_zarn_reg_pir-lifnr EQ 'MULTIPLE'.
      RETURN.
    ENDIF.

    " Validity check
    SELECT @abap_true
      FROM a718
      UP TO 1 ROWS
      INTO @DATA(lv_valid)
     WHERE lifnr = @ls_zarn_reg_pir-lifnr
       AND ekkol = @ls_zarn_reg_pir-ekkol
       AND datab <= @sy-datum
       AND datbi >= @sy-datum.
    ENDSELECT.
    ev_result_ok = lv_valid.

  ENDMETHOD.


  METHOD complex_rl41.

*** A compex validation rule need to be added in the validation rule engine,
*** which will cross check the Order UOM from regional UOM screen with the
*** Order UOM in the regional PIR records.
*** It should always match as per the business requirement.

*** For all the regional PIR records marked PIR_REL_EINA=’X’ ,
*** system will match their ZARN_REG_PIR- BPRME with the ZARN_REG_UOM-MEINH
*** where ZARN_REG_UOM -UNIT_PURORD =’X’.


    DATA: ls_zarn_reg_pir TYPE zarn_reg_pir,
          ls_reg_uom      TYPE zarn_reg_uom.


    check_allowed_table( is_rule      = is_rule
                         iv_allwd_tb1 = gc-tb_zarn_reg_pir ).

    ev_result_ok = abap_true.
    ls_zarn_reg_pir = is_check_record.



    CLEAR ls_reg_uom.
    READ TABLE gt-zarn_reg_uom[] INTO ls_reg_uom
    WITH KEY unit_purord = abap_true.
    IF sy-subrc NE 0.
      RETURN.
    ENDIF.

    IF ls_zarn_reg_pir-bprme NE ls_reg_uom-meinh.
      ev_result_ok = abap_false.
    ENDIF.


  ENDMETHOD.


  METHOD complex_rl42.

*** System will check whether the condition group of the new PIR is null
*** If it is null
***  Then system will find the EINA-INFNR where EINA-RELIF=’X’
***    If the EINA-EKKOL of the above PIR is populated
***      Then system will give warning message to maintain the condition group in the new PIR as well.



    DATA: ls_zarn_reg_pir TYPE zarn_reg_pir,
          ls_reg_hdr      TYPE zarn_reg_hdr.



    check_allowed_table( is_rule      = is_rule
                         iv_allwd_tb1 = gc-tb_zarn_reg_pir ).

    ev_result_ok = abap_true.
    ls_zarn_reg_pir = is_check_record.

    READ TABLE gt-zarn_reg_hdr INTO ls_reg_hdr INDEX 1.
    IF ls_reg_hdr-matnr IS NOT INITIAL.

      SELECT SINGLE eina~infnr, eina~matnr, eina~lifnr, eina~relif,
             eine~bprme, eine~ekkol
                    INTO @DATA(ls_eina)
             FROM eina AS eina
             JOIN eine AS eine
               ON eina~infnr = eine~infnr
            WHERE eina~matnr = @ls_reg_hdr-matnr
              AND eina~lifnr = @ls_zarn_reg_pir-lifnr
              AND eina~relif = 'X'
              AND eine~ekorg = '9999'.
      IF sy-subrc = 0.
        IF ls_eina-ekkol NE ls_zarn_reg_pir-ekkol.
          ev_result_ok = abap_false.
        ENDIF.
      ENDIF.  " if PIR not maintained
    ENDIF. " if article is not posted



  ENDMETHOD.


  METHOD complex_rl43.

*** When the Retail banner level SOS is 2 for UNI,
*** but the ranging is sitting as Optional (OR) for all retail banners,



    DATA: ls_zarn_rb TYPE zarn_reg_banner.



    check_allowed_table( is_rule      = is_rule
                         iv_allwd_tb1 = gc-tb_zarn_reg_banner ).

    ev_result_ok = abap_true.
    ls_zarn_rb   = is_check_record.

    LOOP AT gt-zarn_reg_banner[] INTO DATA(ls_reg_banner)
      WHERE ( banner = '4000' OR banner = '5000' OR banner = '6000' )
        AND sstuf NE 'OR'.
      EXIT.
    ENDLOOP.
    IF sy-subrc NE 0.
      ev_result_ok = abap_false.
    ENDIF.




  ENDMETHOD.


  METHOD complex_rl44.

*** When none of the banner level SOS is 2 for UNI or LNI ,
*** but the reference level is 2 and available from UNI DC Flag is turned on



    DATA: ls_zarn_rb TYPE zarn_reg_banner.



    check_allowed_table( is_rule      = is_rule
                         iv_allwd_tb1 = gc-tb_zarn_reg_banner ).

    ev_result_ok = abap_true.
    ls_zarn_rb   = is_check_record.

    LOOP AT gt-zarn_reg_banner[] INTO DATA(ls_reg_banner)
      WHERE ( banner = '4000' OR banner = '5000' OR banner = '6000' )
        AND ( source_uni = '2' ).
      EXIT.
    ENDLOOP.
    IF sy-subrc NE 0.
      ev_result_ok = abap_false.
    ENDIF.



  ENDMETHOD.


  METHOD complex_rl45.


*** Compare total shelf life and remaining shelf life in AReNa
*** (error if total shelf life > 0 and remaining shelf life = 0).



    DATA: ls_zarn_rh TYPE zarn_reg_hdr.



    check_allowed_table( is_rule      = is_rule
                         iv_allwd_tb1 = gc-tb_zarn_reg_hdr ).

    ev_result_ok = abap_true.
    ls_zarn_rh   = is_check_record.

    READ TABLE gt-zarn_products[] INTO DATA(ls_products)
    WITH KEY idno = ls_zarn_rh-idno.
    IF sy-subrc = 0.
      IF ls_products-lifespan_production GT 0 AND ls_zarn_rh-mhdrz IS INITIAL.
        ev_result_ok = abap_false.
      ENDIF.
    ENDIF.


  ENDMETHOD.


  METHOD complex_rl46.

* If an article is flagged as “ingredient” (SCAGR = ING),
* add a validation (warning) if there’s no allergen that
* has level of containment = “CONTAINS” (i.e. BP # exist
* in ZARN_ALLERGEN table but NO entries with level of containment = “CONTAINS”).
* pre-conditions: ZARN_REG_HDR.SCAGR = ING and ZARN_ALLERGEN.IDNO < > null


    DATA: ls_zarn_rh TYPE zarn_reg_hdr,
          ls_zarn_rb TYPE zarn_reg_banner.



    check_allowed_table( is_rule      = is_rule
                         iv_allwd_tb1 = gc-tb_zarn_reg_hdr ).

    ev_result_ok = abap_true.
    ls_zarn_rh   = gt-zarn_reg_hdr[ 1 ].

* Check if same Message is already generated then don't validate again.
    READ TABLE gt_output[] TRANSPORTING NO FIELDS
    WITH KEY validation_id = 'C106'
             table_name     = is_rule-table_name
             field_name     = is_rule-field_name
             idno           = ls_zarn_rh-idno.
    IF sy-subrc = 0.
      RETURN.
    ENDIF.

    IF ls_zarn_rh-scagr NE 'ING'.
      RETURN.
    ENDIF.

    IF gt-zarn_allergen[] IS INITIAL OR ( NOT line_exists( gt-zarn_allergen[ idno = ls_zarn_rh-idno
                                                                             lvl_containment = 'CONTAINS' ] ) ).
      ev_result_ok = abap_false.
    ENDIF.




  ENDMETHOD.


  METHOD complex_rl47.


* If an article is flagged as “ingredient” (SCAGR = ING),
* add a validation (warning) if there’s no allergen that
* has level of containment = “CONTAINS” (i.e. BP # exist
* in ZARN_ALLERGEN table but NO entries with level of containment = “CONTAINS”).
* pre-conditions: ZARN_REG_BANNER.SCAGR = ING and ZARN_ALLERGEN.IDNO < > null


    DATA: ls_zarn_rh TYPE zarn_reg_hdr,
          ls_zarn_rb TYPE zarn_reg_banner.



    check_allowed_table( is_rule      = is_rule
                         iv_allwd_tb1 = gc-tb_zarn_reg_banner ).

    ev_result_ok = abap_true.
    ls_zarn_rh   = gt-zarn_reg_hdr[ 1 ].
    ls_zarn_rb   = is_check_record.

** Check if same Message is already generated then don't validate again.
*    READ TABLE gt_output[] TRANSPORTING NO FIELDS
*    WITH KEY validation_id = 'O104'
*             table_name     = is_rule-table_name
*             field_name     = is_rule-field_name
*             idno           = ls_zarn_rh-idno.
*    IF sy-subrc = 0.
*      RETURN.
*    ENDIF.

*    IF gt-zarn_reg_banner[] IS NOT INITIAL AND ( NOT line_exists( gt-zarn_reg_banner[ idno = ls_zarn_rh-idno
*                                                                                      scagr = 'ING' ] ) ).
*      RETURN.
*    ENDIF.


    IF ls_zarn_rb-scagr NE 'ING'.
      RETURN.
    ENDIF.

    IF gt-zarn_allergen[] IS INITIAL OR ( NOT line_exists( gt-zarn_allergen[ idno = ls_zarn_rh-idno
                                                                             lvl_containment = 'CONTAINS' ] ) ).
      ev_result_ok = abap_false.
    ENDIF.




  ENDMETHOD.


  METHOD complex_rl48.

* If an article is flagged as “ingredient” (SCAGR = ING),
* add a validation (error) if there’s no ingredient statement
* (i.e. ZARN_PROD_STR.INGREDIENT_STATEMENT is blank)
* pre-conditions: ZARN_REG_HDR.SCAGR = ING

    DATA: ls_zarn_rh TYPE zarn_reg_hdr.



    check_allowed_table( is_rule      = is_rule
                         iv_allwd_tb1 = gc-tb_zarn_prod_str ).

    ev_result_ok = abap_true.
    ls_zarn_rh   = gt-zarn_reg_hdr[ 1 ].

    IF ls_zarn_rh-scagr NE 'ING'.
      RETURN.
    ENDIF.

    IF gt-zarn_prod_str[] IS INITIAL OR ( gt-zarn_prod_str[ idno = ls_zarn_rh-idno ]-ingredient_statement IS INITIAL ).
      ev_result_ok = abap_false.
    ENDIF.


  ENDMETHOD.


  METHOD complex_rl49.

* If an article is flagged as “ingredient” (SCAGR = ING),
* add a validation (error) if there’s no ingredient statement
* (i.e. ZARN_PROD_STR.INGREDIENT_STATEMENT is blank)
* pre-conditions: ZARN_REG_BANNER.SCAGR = ING

    DATA: ls_zarn_rh TYPE zarn_reg_hdr.



    check_allowed_table( is_rule      = is_rule
                         iv_allwd_tb1 = gc-tb_zarn_prod_str ).

    ev_result_ok = abap_true.
    ls_zarn_rh   = gt-zarn_reg_hdr[ 1 ].

    IF gt-zarn_reg_banner[] IS INITIAL OR
       ( gt-zarn_reg_banner[] IS NOT INITIAL AND ( NOT line_exists( gt-zarn_reg_banner[ idno = ls_zarn_rh-idno
                                                                                        scagr = 'ING' ] ) ) ).
      RETURN.
    ENDIF.

    IF gt-zarn_prod_str[] IS INITIAL OR ( gt-zarn_prod_str[ idno = ls_zarn_rh-idno ]-ingredient_statement IS INITIAL ).
      ev_result_ok = abap_false.
    ENDIF.




  ENDMETHOD.


  METHOD complex_rl50.

* Validate that SCAGR header is the same as Retail banners SCAGR
* precondition: Banner is in 4000, 5000, 6000

    DATA: ls_zarn_rh TYPE zarn_reg_hdr.



    check_allowed_table( is_rule      = is_rule
                         iv_allwd_tb1 = gc-tb_zarn_reg_hdr ).

    ev_result_ok = abap_true.
    ls_zarn_rh   = gt-zarn_reg_hdr[ 1 ].

    LOOP AT gt-zarn_reg_banner[] INTO DATA(ls_reg_banner)
      WHERE idno = ls_zarn_rh-idno
        AND ( banner = '4000' OR banner = '5000' OR banner = '6000' )
        AND scagr NE ls_zarn_rh-scagr.
      ev_result_ok = abap_false.
      EXIT.
    ENDLOOP.



  ENDMETHOD.


  METHOD complex_rl51.

* Validate that SCAGR in any Retail banners is the same as header SCAGR
* precondition: Banner is in 4000, 5000, 6000

    DATA: ls_zarn_rh TYPE zarn_reg_hdr,
          ls_zarn_rb TYPE zarn_reg_banner.



    check_allowed_table( is_rule      = is_rule
                         iv_allwd_tb1 = gc-tb_zarn_reg_banner ).

    ev_result_ok = abap_true.
    ls_zarn_rh   = gt-zarn_reg_hdr[ 1 ].
    ls_zarn_rb   = is_check_record.

    IF ls_zarn_rb-scagr NE ls_zarn_rh-scagr.
      ev_result_ok = abap_false.
    ENDIF.


  ENDMETHOD.


  METHOD complex_rl52.

* Validate that SCAGR in all retail banners are the same across 3 retail banners
* precondition: Banner is in 4000, 5000, 6000

    DATA: ls_zarn_rh TYPE zarn_reg_hdr,
          lt_zarn_rb TYPE ztarn_reg_banner,
          ls_zarn_rb TYPE zarn_reg_banner.



    check_allowed_table( is_rule      = is_rule
                         iv_allwd_tb1 = gc-tb_zarn_reg_banner ).

    ev_result_ok = abap_true.
    ls_zarn_rh   = gt-zarn_reg_hdr[ 1 ].
    ls_zarn_rb   = is_check_record.


    lt_zarn_rb[] = gt-zarn_reg_banner[].

* Keep Retail banners
    DELETE lt_zarn_rb[] WHERE banner NE '4000'
                          AND banner NE '5000'
                          AND banner NE '6000'.

* Keep any other SCAGR
    DELETE lt_zarn_rb[] WHERE scagr = ls_zarn_rb-scagr.

    IF lt_zarn_rb[] IS NOT INITIAL.
      ev_result_ok = abap_false.
    ENDIF.


  ENDMETHOD.


  METHOD complex_rl53.
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 13.04.2018
* CHANGE No.       # Charm 9000003482; Jira CI18-554
* DESCRIPTION      # CI18-554 Recipe Management - Ingredient Changes
*                  # New regional table for Allergen Type overrides and
*                  # fields for Ingredients statement overrides in Arena.
*                  # New Ingredients Type field in the Scales block in
*                  # Arena and NONSAP tab in material master.
*                  # New complex rule to check Ingredient Statements
* WHO              # C90005557, Tessa Newman
*-----------------------------------------------------------------------

    IF 1 = 2. MESSAGE i035(zarena_business_rule). ENDIF.

* optional check for allowed tables
    check_allowed_table( is_rule      = is_rule
                         iv_allwd_tb1 = gc-tb_zarn_reg_str
                         iv_allwd_tb2 = gc-tb_zarn_reg_hdr ).
*   initialise check ok
    ev_result_ok = abap_true.

*   if Ingredient Type is MFG then either the regional or national level
*   Ingredient Statement must be filled
    IF VALUE #( gt-zarn_reg_hdr[ 1 ]-zzingretype OPTIONAL ) = 'MFG' AND
       VALUE #( gt-zarn_reg_str[ 1 ]-ingredient_statement OPTIONAL ) IS INITIAL AND
       VALUE #( gt-zarn_prod_str[ 1 ]-ingredient_statement OPTIONAL ) IS INITIAL.
*     check fail
      ev_result_ok = abap_false.
    ENDIF.

*  ev_addit_inf = text-e04.

  ENDMETHOD.


  METHOD complex_rl54.
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 13.04.2018
* CHANGE No.       # Charm 9000003482; Jira CI18-554
* DESCRIPTION      # CI18-554 Recipe Management - Ingredient Changes
*                  # New regional table for Allergen Type overrides and
*                  # fields for Ingredients statement overrides in Arena.
*                  # New Ingredients Type field in the Scales block in
*                  # Arena and NONSAP tab in material master.
*                  # New complex rule to check Ingredient Statements
* WHO              # C90005557, Tessa Newman
*-----------------------------------------------------------------------
    DATA: ls_reg_allerg TYPE zarn_reg_allerg.

    IF 1 = 2. MESSAGE i035(zarena_business_rule). ENDIF.

*   optional check for allowed tables
    check_allowed_table( is_rule      = is_rule
                         iv_allwd_tb1 = gc-tb_zarn_reg_hdr
                         iv_allwd_tb2 = gc-tb_zarn_reg_allerg ).
*   initialise check ok
    ev_result_ok = abap_true.

    ls_reg_allerg = is_check_record.

*   if there are NO corresponding National Allergen values but a Regional Allergen Override exists
*   then ensure that the level of containment override has been filled

    IF NOT line_exists( gt-zarn_allergen[ idno = ls_reg_allerg-idno
                                          allergen_type = ls_reg_allerg-allergen_type  ] ) AND
            ls_reg_allerg-lvl_containment_ovr IS INITIAL.
      ev_result_ok = abap_false.
    ENDIF.

*  ev_addit_inf = text-e04.

  ENDMETHOD.


  METHOD complex_rl55.
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 13.04.2018
* CHANGE No.       # Charm 9000004661
* DESCRIPTION      # CI19-294 Add Supply Chain Tab
*                  # New rules required for all the supply chain date
*                  # fields. If the field is changed and the date is
*                  # set in the past this should produce an error
* WHO              # C90012814, Brad Gorlicki
*-----------------------------------------------------------------------
    DATA: ls_reg_sc    TYPE zarn_reg_sc,
          ls_sel_list  TYPE edpline,
          lt_sel_list  TYPE TABLE OF edpline,
          lv_old_value TYPE datum.

    FIELD-SYMBOLS: <fieldname> TYPE any.

    IF 1 = 2. MESSAGE i035(zarena_business_rule). ENDIF.

*   optional check for allowed tables
*    check_allowed_table( is_rule      = is_rule
*                         iv_allwd_tb1 = gc-tb_zarn_reg_hdr
*                         iv_allwd_tb2 = gc-tb_zarn_reg_sc ).
*   initialise check ok
    ev_result_ok = abap_true.

    ls_reg_sc = is_check_record.

    ls_sel_list = is_rule-field_name.

* Check should only occur when user enters a new value and not try and
* validate an existing value that has been previously saved
    SELECT SINGLE (ls_sel_list)
        FROM zarn_reg_sc
        INTO lv_old_value
        WHERE idno = ls_reg_sc-idno.

    IF lv_old_value NE iv_field_value     "Value has changed
      AND iv_field_value < sy-datum       "Less than todays date (actual check)
      AND iv_field_value IS NOT INITIAL.  "Not Null
      ev_result_ok = abap_false.
    ENDIF.

  ENDMETHOD.


METHOD complex_rl56.

  DATA ls_reg_banner TYPE zarn_reg_banner.

  "allowed only for reg. banner table
  check_allowed_table( is_rule      = is_rule
                       iv_allwd_tb1 = gc-tb_zarn_reg_banner ).

  ls_reg_banner = is_check_record.

  ev_result_ok = abap_true.

  IF NOT zcl_onl_md_interfaces=>is_banner_live_online( iv_banner = ls_reg_banner-banner ).
    ev_result_ok = abap_false.
  ENDIF.

ENDMETHOD.


METHOD complex_rl57.
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 20/11/2019
* CHANGE No.       # Charm 9000006356; Jira CIP-148
* DESCRIPTION      # Check that for retail banners article is locally
*                  # ranged and either not ranged for Gilmours or if
*                  # ranged then locally ranged.
*                  # New complex rule.
* WHO              # C90001929, Aruna Mackenjee
*-----------------------------------------------------------------------

  DATA:
    lt_retail_banner_r TYPE RANGE OF vkorg.

  CONSTANTS:
    lc_local_range TYPE sstuf VALUE 'LR'.

  " Set check as failed by default
  ev_result_ok = abap_false.

  lt_retail_banner_r = VALUE #( sign = 'I' option = 'EQ' ( low = '4000' ) ( low = '5000' ) ( low = '6000' ) ).

  " Condition 1: At least 1 retail banner has local range (LR)
  LOOP AT gt-zarn_reg_banner INTO DATA(ls_reg_banner)
    WHERE banner IN lt_retail_banner_r.

    IF ls_reg_banner-sstuf = lc_local_range.
      DATA(lv_cond1) = abap_true.
      EXIT.
    ENDIF.

  ENDLOOP.
  IF sy-subrc IS NOT INITIAL.
    DATA(lv_no_retail_banner) = abap_true.
  ENDIF.

  " Condition 2: Banner 3000 doesn't exist or if it does then either
  "              grade is LR or both grade and category manager are
  "              blank (i.e. not ranged).
  CLEAR ls_reg_banner.
  ls_reg_banner = VALUE #( gt-zarn_reg_banner[ banner = '3000' ] OPTIONAL ).
  IF ls_reg_banner IS INITIAL
  OR ( ls_reg_banner-sstuf IS INITIAL
   AND ls_reg_banner-zzcatman IS INITIAL )
  OR ls_reg_banner-sstuf = lc_local_range.
    DATA(lv_cond2) = abap_true.
  ENDIF.

  " Condition 3: If there are no retail banners then check that banner 3000
  "              has grade LR.
  IF lv_no_retail_banner = abap_true
 AND ls_reg_banner-sstuf = lc_local_range.
    DATA(lv_cond3) = abap_true.
  ENDIF.

  " Now set final result of check
  IF lv_cond1 = abap_true
 AND lv_cond2 = abap_true.
    ev_result_ok = abap_true.
  ELSEIF lv_cond1 = abap_false
     AND lv_cond3 = abap_true.
    ev_result_ok = abap_true.
  ENDIF.

*  LOOP AT gt-zarn_reg_banner INTO DATA(ls_reg_banner)
*    WHERE banner IN lt_wholesale_banner_r.
*
*    IF NOT ( ls_reg_banner-sstuf IS INITIAL
*         AND ls_reg_banner-zzcatman IS INITIAL ).
*      DATA(lv_ranged) = abap_true.
*      EXIT.
*    ENDIF.
*  ENDLOOP.
*  IF sy-subrc NE 0
*  OR lv_ranged = abap_false.
*    " Implies not ranged for the banner range
*    ev_result_ok = abap_true.
*  ENDIF.

ENDMETHOD.


METHOD complex_rl58.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 24.06.2020
* SYSTEM           # DE0
* SPECIFICATION    # SSM-1 NW Range Policy ZARN_GUI
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Checks whether Banner is relevant to Cluster Range (Layout).
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  check_allowed_table( is_rule      = is_rule
                       iv_allwd_tb1 = gc-tb_zarn_reg_cluster
                       iv_allwd_fld = gc-field_name-banner ).

  ASSIGN COMPONENT gc-field_name-banner OF STRUCTURE is_check_record TO FIELD-SYMBOL(<lv_banner>).
  IF sy-subrc = 0.
    SELECT SINGLE cluster_relevant
      FROM zarn_cluster_cfg
      INTO @ev_result_ok
      WHERE banner = @<lv_banner>.
  ENDIF.

ENDMETHOD.


METHOD COMPLEX_RL_UTIL_GET_VND_DATA.

  DATA ls_ekorg_tvarvc TYPE tvarvc.

  IF gt_buf-pur_ekorg[] IS INITIAL.
    SELECT * FROM tvarvc INTO TABLE gt_buf-pur_ekorg[]
       WHERE name = gc-pur_ekorg_tvarv.
  ENDIF.

  "first read from buffer
  READ TABLE gt_buf-vendor_data INTO rs_vendor_data
   WITH TABLE KEY lifnr = iv_lifnr.
  IF sy-subrc NE 0.
    CLEAR rs_vendor_data.

    SELECT SINGLE lifnr bbbnr bbsnr bubkz sperm loevm FROM lfa1
      INTO (rs_vendor_data-lifnr,
            rs_vendor_data-bbbnr,
            rs_vendor_data-bbsnr,
            rs_vendor_data-bubkz,
            rs_vendor_data-lfa1_sperm,
            rs_vendor_data-lfa1_loevm)
      WHERE lifnr = iv_lifnr.
    IF sy-subrc NE 0.
      "incorrect vendor, insert dummy record into buffer
      rs_vendor_data-lifnr = iv_lifnr.
    ELSE.

      SELECT SINGLE sptype FROM zfsvend
        INTO rs_vendor_data-sptype
        WHERE lifnr = iv_lifnr.

      "get default EKORG for comparison between LFM1
      READ TABLE gt_buf-pur_ekorg WITH KEY high = 'X'
        INTO ls_ekorg_tvarvc.
      IF sy-subrc EQ 0.
        SELECT SINGLE plifz sperm loevm waers FROM lfm1
          INTO (rs_vendor_data-plifz,
                rs_vendor_data-lfm1_sperm,
                rs_vendor_data-lfm1_loevm,
                rs_vendor_data-waers)
          WHERE lifnr = iv_lifnr
            AND ekorg = ls_ekorg_tvarvc-low.
      ENDIF.
    ENDIF.

    "prepare GLN no so that it can be used as a single field
    rs_vendor_data-gln_no = rs_vendor_data-bbbnr && rs_vendor_data-bbsnr && rs_vendor_data-bubkz.

    "insert into buffer
    INSERT rs_vendor_data INTO TABLE gt_buf-vendor_data.
  ENDIF.

ENDMETHOD.


METHOD constructor.
*-----------------------------------------------------------------------
* PROJECT        # One Data
* SPECIFICATION  # Feature 3126 - AReNa Validation Engine
* DATE WRITTEN   # 2016-02
* SAP VERSION    # SAPK-606 05 (SAP Enterprise Extension Retail)
* TYPE           # Method
* AUTHOR         # Ivo Skolek, C90001587
*-----------------------------------------------------------------------
* TITLE          # Constructor for Validation Engine
* PURPOSE        # Instantiate Validation Engine and fetch
*                # validations for Event
*-----------------------------------------------------------------------
* CALLED FROM    # where used
*                # validations configured in tx. ZARN_CONSISTENCY_CHK
*-----------------------------------------------------------------------
* CALLS TO       #
*-----------------------------------------------------------------------
* RESTRICTIONS
* Expect EVENT as an input. Use constants defined for EVENT
* (example ZCL_ARN_VALIDATION_ENGINE=>GC_EVENT_ARENA_UI)
*
* always catch exception ZCX_ARN_VALIDATION_ERR
* (raised in case of incorrect / missing configuration or
* or inconsistent set of input data)
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE           # 27/09/16
* CHANGE No.     # 1
* DESCRIPTION    # Add facility to filter validations based on a field list
*                  New table ZARN_RL_CLOG_FLD supports complex rules for
*                  this scenario
* WHO            # Paul Collins
*-----------------------------------------------------------------------

  DATA: lt_validation_id  TYPE STANDARD TABLE OF zarn_e_rl_valid_id,
        lt_rule           TYPE STANDARD TABLE OF zarn_rl_rule,
        ls_rule           LIKE LINE OF lt_rule,
        lt_range          TYPE SORTED   TABLE OF zarn_rl_rule_rng
                          WITH NON-UNIQUE KEY rule_id,
        ls_range          LIKE LINE OF lt_range,
        ls_value_rng      TYPE zarn_s_rl_rule_rng,
        ls_rule_and_range LIKE LINE OF gt_rule_and_range,
        ls_validation     LIKE LINE OF gt_validation.

  IF iv_event IS INITIAL.
    "no event provided - error
    RAISE EXCEPTION TYPE zcx_arn_validation_err
      EXPORTING
        msgv1 = text-e01.
  ENDIF.



  "bapiret output option - used in BUILD_OUTPUT_RECORD method
  IF iv_bapiret_output IS SUPPLIED.
    gv_bapiret_output = iv_bapiret_output.
  ELSE.
    CLEAR gv_bapiret_output.
  ENDIF.

  "fetch AReNa tables enabled for Validation Engine
  SELECT * FROM zarn_rl_table INTO TABLE gt_tables.

  IF iv_event EQ gc_dummy_event_all_validations.
    "dummy event - process all validations
    SELECT * FROM zarn_rl_valid INTO TABLE gt_validation.
  ELSE.

* { +3140 - Mass update
* New determination of validations based on field filter
    IF it_field_filter IS NOT INITIAL.
      me->gt_validation =
        me->apply_validation_field_filter(
            iv_event               = iv_event
            it_field_filter        = it_field_filter
        ).
      IF me->gt_validation IS INITIAL.
        RETURN.
      ENDIF.
    ELSE.
* } +3140 - Mass update
      "fetch validation IDs to process based on given event
      SELECT validation_id FROM zarn_rl_ev_val INTO TABLE lt_validation_id
        WHERE event_id = iv_event.
      IF sy-subrc NE 0.
        "no validation assigned to event - OK, do nothing
        RETURN.
      ENDIF.

      SELECT * FROM zarn_rl_valid INTO TABLE gt_validation
        FOR ALL ENTRIES IN lt_validation_id
        WHERE validation_id = lt_validation_id-table_line.
      IF sy-subrc NE 0.
        "error - some validations found assigned on event
        "but missing validation record itself
        RAISE EXCEPTION TYPE zcx_arn_validation_err
          EXPORTING
            msgv1 = text-eec
            msgv2 = text-e16.
      ENDIF.
    ENDIF.
  ENDIF.

  "sort validations using sequencing
  "validations with entered sequence have priority
  CLEAR ls_validation.
  ls_validation-validation_sequence = gc-max_valid_sequence.
  MODIFY gt_validation FROM ls_validation TRANSPORTING validation_sequence
       WHERE validation_sequence IS INITIAL.
  SORT gt_validation STABLE BY validation_group validation_sequence.

  "select all rules for validations
  SELECT * FROM zarn_rl_rule INTO TABLE lt_rule
    FOR ALL ENTRIES IN gt_validation
    WHERE rule_id = gt_validation-rule_id_check
      OR  rule_id = gt_validation-rule_id_precond1
      OR  rule_id = gt_validation-rule_id_precond2
      OR  rule_id = gt_validation-rule_id_precond3.

  "select all ranges for rules
  IF lt_rule[] IS NOT INITIAL.
    SELECT * FROM zarn_rl_rule_rng INTO TABLE lt_range
      FOR ALL ENTRIES IN lt_rule
      WHERE rule_id = lt_rule-rule_id.
  ENDIF.

  "build rule-range table
  LOOP AT lt_rule INTO ls_rule.
    CLEAR ls_rule_and_range.
    MOVE-CORRESPONDING ls_rule TO ls_rule_and_range.
    LOOP AT lt_range INTO ls_range
                    WHERE rule_id = ls_rule-rule_id.
      ls_value_rng-sign   = ls_range-rng_sign.
      ls_value_rng-option = ls_range-rng_option.
      ls_value_rng-low    = ls_range-rng_low.
      ls_value_rng-high   = ls_range-rng_high.
      APPEND ls_value_rng TO ls_rule_and_range-ranges.
    ENDLOOP.
    IF sy-subrc NE 0 AND ls_rule-complex_logic IS INITIAL.
      "no ranges assigned and no complex logic
      "=>error
      RAISE EXCEPTION TYPE zcx_arn_validation_err
        EXPORTING
          msgv1 = text-erc
          msgv2 = |{ ls_rule-rule_id WIDTH = 50 }|
          msgv3 = text-e02.
    ENDIF.
    INSERT ls_rule_and_range INTO TABLE gt_rule_and_range.
  ENDLOOP.

ENDMETHOD.


METHOD get_record_precond.

  DATA: ls_table      LIKE LINE OF gt_tables,
        lv_table_name TYPE string,
        lv_idno       TYPE zarn_idno.

  FIELD-SYMBOLS: <check_table> TYPE ANY TABLE,
                 <lv_idno>     TYPE zarn_idno.

  IF is_rule_check-table_name = is_rule_precond-table_name.
    "precondition works on the same table
    es_precond_record = is_check_record.
  ELSE.
    "precondition can be defined on header table
    READ TABLE gt_tables INTO ls_table
       TRANSPORTING header_table_name
       WITH TABLE KEY table_name = is_rule_check-table_name.
    IF sy-subrc NE 0.
      RAISE EXCEPTION TYPE zcx_arn_validation_err
        EXPORTING
          msgv1 = text-erc
          msgv2 = |{ is_rule_check-rule_id WIDTH = 50 }|
          msgv3 = text-e07.
    ENDIF.
    IF is_rule_precond-table_name NE ls_table-header_table_name.
      "precondition defined on not allowed table - error
      RAISE EXCEPTION TYPE zcx_arn_validation_err
        EXPORTING
          msgv1 = text-erv
          msgv2 = |{ is_validation-validation_id WIDTH = 50 }|
          msgv3 = text-e09
          msgv4 = text-e10.
    ENDIF.

    "now get the header record
    lv_table_name = gc-access_tab && is_rule_precond-table_name.
    ASSIGN (lv_table_name) TO <check_table>.
    IF sy-subrc NE 0.
      RAISE EXCEPTION TYPE zcx_arn_validation_err
        EXPORTING
          msgv1 = text-erc
          msgv2 = |{ is_rule_precond-rule_id WIDTH = 50 }|
          msgv3 = text-e05.
    ENDIF.

    LOOP AT <check_table> INTO es_precond_record.
      EXIT.
    ENDLOOP.
    IF sy-subrc NE 0.
      "no header record found to check precondition
      "raise error - incomplete data on input
      ASSIGN COMPONENT 'IDNO' OF STRUCTURE is_check_record
        TO <lv_idno>.
      IF sy-subrc EQ 0.
        lv_idno = <lv_idno>.
      ENDIF.

      RAISE EXCEPTION TYPE zcx_arn_validation_err
        EXPORTING
          msgv1 = text-eri
          msgv2 = |{ is_rule_precond-rule_id WIDTH = 50 }|
          msgv3 = text-e14
          msgv4 = |{ lv_idno WIDTH = 50 }|.
    ENDIF.
  ENDIF.

ENDMETHOD.


METHOD process_addit_key.

  FIELD-SYMBOLS  <lv_addit_key> TYPE any.

  ASSIGN COMPONENT iv_addit_key_field_name OF STRUCTURE is_check_record
    TO <lv_addit_key>.
  IF sy-subrc NE 0.
    RAISE EXCEPTION TYPE zcx_arn_validation_err
      EXPORTING
        msgv1 = text-etc
        msgv2 = |{ iv_addit_key_field_name WIDTH = 50 }|
        msgv3 = text-e11.
  ENDIF.
  ev_addit_key = <lv_addit_key>.

  ev_addit_keys_txt = ev_addit_keys_txt && ` ` && "concatenate string
                     iv_addit_key_desc && ` ` &&
                     shift_left( val = ev_addit_key sub = '0' ).

ENDMETHOD.


METHOD process_rule_single.

  FIELD-SYMBOLS <lv_field_value> TYPE any.
  DATA lv_complex_rule_meth      TYPE string.

  CLEAR: ev_result_ok, ev_addit_inf.

  ASSIGN COMPONENT is_rule-field_name OF STRUCTURE is_check_record
   TO <lv_field_value>.
  IF sy-subrc NE 0.
    RAISE EXCEPTION TYPE zcx_arn_validation_err
      EXPORTING
        msgv1 = text-erc
        msgv2 = |{ is_rule-rule_id WIDTH = 50 }|
        msgv3 = text-e12
        msgv4 = |{ is_rule-field_name WIDTH = 50 }|.
  ENDIF.

  "return value for message
  WRITE <lv_field_value> TO ev_field_val.

  IF is_rule-complex_logic IS NOT INITIAL.

    "call method - returns OK/error in EV_RESULT_OK
    "but can calso populate additional info for message in EV_ADDIT_INF
    lv_complex_rule_meth = gc-access_complex_m && is_rule-complex_logic.
    TRY.
        CALL METHOD me->(lv_complex_rule_meth)
          EXPORTING
            iv_field_value  = <lv_field_value>
            is_rule         = is_rule
            is_check_record = is_check_record
          IMPORTING
            ev_result_ok    = ev_result_ok
            ev_addit_inf    = ev_addit_inf.
      CATCH cx_sy_dyn_call_error.
        RAISE EXCEPTION TYPE zcx_arn_validation_err
          EXPORTING
            msgv1 = text-erc
            msgv2 = |{ is_rule-rule_id WIDTH = 50 }|
            msgv3 = text-e13
            msgv4 = |{ is_rule-complex_logic WIDTH = 50 }|.
    ENDTRY.

  ELSEIF is_rule-ranges[] IS NOT INITIAL.
    "just simple check of value against configured ranges
    IF <lv_field_value> IN is_rule-ranges[].
      ev_result_ok = abap_true.
    ENDIF.
  ENDIF.

  "if inverse result is ticked, switch true and false
  IF is_rule-inverse = abap_true.
    IF ev_result_ok = abap_true.
      ev_result_ok = abap_false.
    ELSEIF ev_result_ok = abap_false.
      ev_result_ok = abap_true.
    ENDIF.
  ENDIF.


ENDMETHOD.


METHOD validate_arn_bulk.
*-----------------------------------------------------------------------
* PROJECT        # One Data
* SPECIFICATION  # Feature 3126 - AReNa Validation Engine
* DATE WRITTEN   # 2016-02
* SAP VERSION    # SAPK-606 05 (SAP Enterprise Extension Retail)
* TYPE           # Method
* AUTHOR         # Ivo Skolek, C90001587
*-----------------------------------------------------------------------
* TITLE          # Validate bulk ARENA data
* PURPOSE        # Make bulks of data per Article and do validations
*                #
*-----------------------------------------------------------------------
* CALLED FROM    # where used (UI, check report etc.)
*                #
*-----------------------------------------------------------------------
* CALLS TO       # method VALIDATE_ARN_SINGLE (1 Article)
*-----------------------------------------------------------------------
* RESTRICTIONS
* "2 possible scenarios of calling
* A) IF ZSARN_REG_DATA supplied => checking reg. AND nat. data together
* B) ELSE IF ZSARN_PROD_DATA supplied => checking ONLY national data
*
* for NATIONAL DATA ONLY 1 VERSION IS ALWAYS EXPECTED PER ARTICLE!
*
* always catch exception ZCX_ARN_VALIDATION_ERR
* (raised in case of incorrect / missing configuration or
* or inconsistent set of input data)
*
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE           #
* CHANGE No.     #
* DESCRIPTION    #
* WHO            #
*-----------------------------------------------------------------------

  CONSTANTS: lc_idno_where_reg TYPE string
             VALUE 'IDNO = LS_ZARN_REG_HDR-IDNO',
             lc_idno_where_nat TYPE string
             VALUE 'IDNO = LS_ZARN_PRODUCTS-IDNO'.

  DATA: ls_tables          LIKE LINE OF gt_tables,
        ls_zsarn_prod_data LIKE is_zsarn_prod_data,
        ls_zsarn_reg_data  LIKE is_zsarn_reg_data,
        ls_zarn_reg_hdr    TYPE zarn_reg_hdr,
        ls_zarn_products   TYPE zarn_products,
        lt_output          TYPE zarn_t_rl_output.

  FIELD-SYMBOLS : <lt_table_input> TYPE STANDARD TABLE,
                  <lt_table_single> TYPE STANDARD TABLE,
                  <ls_record_input> TYPE any.

  REFRESH et_output.

  IF is_zsarn_reg_data IS SUPPLIED.

    "having regional data - master entry is in ZARN_REG_HDR
    "make bulks of regional + national data for 1 ZARN_REG_HDR

    LOOP AT is_zsarn_reg_data-zarn_reg_hdr
      INTO ls_zarn_reg_hdr.

      CLEAR: ls_zsarn_reg_data, ls_zsarn_prod_data.
      REFRESH lt_output .

      "prepare all tables
      LOOP AT gt_tables INTO ls_tables.
        ASSIGN COMPONENT ls_tables-table_name OF STRUCTURE
         is_zsarn_reg_data-reg_tables TO <lt_table_input>.
        IF sy-subrc EQ 0.
          ASSIGN COMPONENT ls_tables-table_name OF STRUCTURE
          ls_zsarn_reg_data TO <lt_table_single>.
          IF sy-subrc NE 0.
            CONTINUE.
          ENDIF.
        ELSE.

          IF sy-subrc NE 0.
            ASSIGN COMPONENT ls_tables-table_name OF STRUCTURE
                   is_zsarn_prod_data TO <lt_table_input>.
            IF sy-subrc NE 0.
              CONTINUE.
            ENDIF.
            ASSIGN COMPONENT ls_tables-table_name OF STRUCTURE
            ls_zsarn_prod_data TO <lt_table_single>.
            IF sy-subrc NE 0.
              CONTINUE.
            ENDIF.
          ENDIF.
        ENDIF.

        LOOP AT <lt_table_input> ASSIGNING <ls_record_input>
          WHERE (lc_idno_where_reg).
          APPEND <ls_record_input> TO <lt_table_single>.
        ENDLOOP.
      ENDLOOP.

      CALL METHOD validate_arn_single
        EXPORTING
          is_zsarn_prod_data = ls_zsarn_prod_data
          is_zsarn_reg_data  = ls_zsarn_reg_data
        IMPORTING
          et_output          = lt_output.

      APPEND LINES OF lt_output TO et_output.
    ENDLOOP.

  ELSEIF is_zsarn_prod_data IS SUPPLIED.

*    "only national data provided - for example PIM inbound
*    " master entry is in ZARN_PRODUCTS
*    " make bulks per 1 entry in ZARN_PRODUCTS

    LOOP AT is_zsarn_prod_data-zarn_products
      INTO ls_zarn_products.

      CLEAR: ls_zsarn_reg_data, ls_zsarn_prod_data.
      REFRESH lt_output .

      "prepare all tables
      LOOP AT gt_tables INTO ls_tables.
        ASSIGN COMPONENT ls_tables-table_name OF STRUCTURE
               is_zsarn_prod_data TO <lt_table_input>.
        IF sy-subrc NE 0.
          CONTINUE.
        ENDIF.
        ASSIGN COMPONENT ls_tables-table_name OF STRUCTURE
          ls_zsarn_prod_data TO <lt_table_single>.
        IF sy-subrc NE 0.
          CONTINUE.
        ENDIF.

        LOOP AT <lt_table_input> ASSIGNING <ls_record_input>
          WHERE (lc_idno_where_nat).
          APPEND <ls_record_input> TO <lt_table_single>.
        ENDLOOP.
      ENDLOOP.

      CALL METHOD validate_arn_single
        EXPORTING
          is_zsarn_prod_data = ls_zsarn_prod_data
        IMPORTING
          et_output          = lt_output.

      APPEND LINES OF lt_output TO et_output.
    ENDLOOP.

  ENDIF.


ENDMETHOD.


METHOD validate_arn_single.
*-----------------------------------------------------------------------
* PROJECT        # One Data
* SPECIFICATION  # Feature 3126 - AReNa Validation Engine
* DATE WRITTEN   # 2016-02
* SAP VERSION    # SAPK-606 05 (SAP Enterprise Extension Retail)
* TYPE           # Method
* AUTHOR         # Ivo Skolek, C90001587
*-----------------------------------------------------------------------
* TITLE          # Validate single Article ARENA data
* PURPOSE        # Run configured validations on AReNa Article Data
*                #
*-----------------------------------------------------------------------
* CALLED FROM    # where used
*                # validations configured in tx. ZARN_CONSISTENCY_CHK
*-----------------------------------------------------------------------
* CALLS TO       #
*-----------------------------------------------------------------------
* RESTRICTIONS
*  Always expect data related to single AReNa IDNO + 1 only 1 version of
*  National Data.
*
* always catch exception ZCX_ARN_VALIDATION_ERR
* (raised in case of incorrect / missing configuration or
* or inconsistent set of input data)
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE           #
* CHANGE No.     #
* DESCRIPTION    #
* WHO            #
*-----------------------------------------------------------------------

  DATA: ls_validation      LIKE LINE OF gt_validation,
        ls_check_rule      LIKE LINE OF gt_rule_and_range,
        ls_precond_rule    LIKE LINE OF gt_rule_and_range,
        lv_table_name      TYPE string,
        lv_wa_name         TYPE string,
        lv_field_val       TYPE char50,
        lv_addit_inf       TYPE char100,
        lt_precond_rule_id TYPE STANDARD TABLE OF zarn_e_rl_rule_id,
        lv_precond_rule_id TYPE zarn_e_rl_rule_id,
        lv_result_ok       TYPE xfeld,
        ls_output          LIKE LINE OF et_output.


  FIELD-SYMBOLS: <check_table>    TYPE ANY TABLE,
                 <check_record>   TYPE any,
                 <precond_record> TYPE any.

  "crossreference only to business rule repository
  IF 1 = 2. MESSAGE s031(zarena_business_rule). ENDIF.

  "set input tables into attributes of class
  CLEAR gt.
  CLEAR me->gt_output[].
  REFRESH et_output.
  MOVE-CORRESPONDING is_zsarn_prod_data-prod_tables TO gt.
  MOVE-CORRESPONDING is_zsarn_reg_data-reg_tables  TO gt.

  "optional refresh of masterdata buffers
  IF iv_refresh_mstrdata_buffers = abap_true.
    "if there is more such buffered data in future
    "would be good to have separate gt_buf_mstrdata
    "and clear the whole deep structure
    CLEAR gt_buf-vendor_data[].
  ENDIF.

  "run all validation on given data
  LOOP AT gt_validation INTO ls_validation.

    "gather preconditions into table
    REFRESH lt_precond_rule_id.
    IF ls_validation-rule_id_precond1 IS NOT INITIAL.
      APPEND ls_validation-rule_id_precond1 TO lt_precond_rule_id.
    ENDIF.
    IF ls_validation-rule_id_precond2 IS NOT INITIAL.
      APPEND ls_validation-rule_id_precond2 TO lt_precond_rule_id.
    ENDIF.
    IF ls_validation-rule_id_precond3 IS NOT INITIAL.
      APPEND ls_validation-rule_id_precond3 TO lt_precond_rule_id.
    ENDIF.

    "first obtain check rule
    READ TABLE gt_rule_and_range INTO ls_check_rule
         WITH TABLE KEY rule_id = ls_validation-rule_id_check.
    IF sy-subrc NE 0.
      RAISE EXCEPTION TYPE zcx_arn_validation_err
        EXPORTING
          msgv1 = text-evc
          msgv2 = |{ ls_validation-validation_id WIDTH = 50 }|
          msgv3 = text-e03.
    ENDIF.

    "assign target check table
    lv_table_name = gc-access_tab && ls_check_rule-table_name.
    ASSIGN (lv_table_name) TO <check_table>.
    IF sy-subrc NE 0.
      RAISE EXCEPTION TYPE zcx_arn_validation_err
        EXPORTING
          msgv1 = text-erc
          msgv2 = |{ ls_check_rule-rule_id WIDTH = 50 }|
          msgv3 = text-e05.
    ENDIF.


    LOOP AT <check_table> ASSIGNING <check_record>.

      lv_result_ok = abap_true.

      "processing particular record
      "check the preconditions first

      LOOP AT lt_precond_rule_id INTO lv_precond_rule_id.
        READ TABLE gt_rule_and_range INTO ls_precond_rule
            WITH TABLE KEY rule_id = lv_precond_rule_id.
        IF sy-subrc NE 0.
          RAISE EXCEPTION TYPE zcx_arn_validation_err
            EXPORTING
              msgv1 = text-evc
              msgv2 = |{ ls_validation-validation_id WIDTH = 50 }|
              msgv3 = text-e06.
        ENDIF.

        lv_wa_name = gc-access_wa && ls_precond_rule-table_name.
        ASSIGN (lv_wa_name) TO <precond_record>.
        IF sy-subrc NE 0.
          RAISE EXCEPTION TYPE zcx_arn_validation_err
            EXPORTING
              msgv1 = text-erc
              msgv2 = |{ ls_precond_rule-rule_id WIDTH = 50 }|
              msgv3 = text-e05.
        ENDIF.

        "********process precondition********
        get_record_precond( EXPORTING is_check_record   = <check_record>
                                      is_rule_check     = ls_check_rule
                                      is_rule_precond   = ls_precond_rule
                                      is_validation     = ls_validation
                            IMPORTING es_precond_record = <precond_record>  ).

        process_rule_single( EXPORTING is_rule         = ls_precond_rule
                                       is_check_record = <precond_record>
                             IMPORTING ev_result_ok    = lv_result_ok  ).


        CASE ls_validation-precond_link_type.
          WHEN gc-precond_link_or.
            "at least 1 is OK => all good
            IF lv_result_ok = abap_true.
              EXIT.
            ENDIF.
          WHEN gc-precond_link_and.
            "at least 1 failed => all bad
            IF lv_result_ok = abap_false.
              EXIT.
            ENDIF.
        ENDCASE.
      ENDLOOP.

      "if precondition(s) OK => continue to check rule itself
      IF lv_result_ok = abap_true.
        CLEAR lv_addit_inf.

        "********process check********
        process_rule_single( EXPORTING is_rule         = ls_check_rule
                                       is_check_record = <check_record>
                             IMPORTING ev_result_ok    = lv_result_ok
                                       ev_field_val    = lv_field_val
                                       ev_addit_inf    = lv_addit_inf ).

        IF lv_result_ok NE abap_true.
          build_output_record( EXPORTING is_validation = ls_validation
                                         is_check_rule = ls_check_rule
                                         is_check_record = <check_record>
                                         iv_field_val  = lv_field_val
                                         iv_addit_inf  = lv_addit_inf
                               IMPORTING es_output     = ls_output ).

          APPEND ls_output TO et_output.
          APPEND ls_output TO me->gt_output.
        ENDIF.
      ENDIF.

    ENDLOOP.

  ENDLOOP.

ENDMETHOD.
ENDCLASS.
