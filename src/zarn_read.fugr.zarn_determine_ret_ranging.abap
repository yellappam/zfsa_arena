FUNCTION zarn_determine_ret_ranging.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(IT_ZARN_REG_BANNER) TYPE  ZTARN_REG_BANNER
*"  CHANGING
*"     REFERENCE(CV_RET_RANGING) TYPE  FLAG
*"----------------------------------------------------------------------

  "determines Retail Ranging Flag

  "this function is used both by GUI and validation engine

  LOOP AT it_zarn_reg_banner INTO DATA(ls_zarn_reg_banner)
    WHERE ( banner EQ zcl_constants=>gc_banner_4000
    OR banner EQ zcl_constants=>gc_banner_5000
    OR banner EQ zcl_constants=>gc_banner_6000 )
    AND zzcatman IS NOT INITIAL.

    cv_ret_ranging = abap_true.
    EXIT.
  ENDLOOP.

ENDFUNCTION.
