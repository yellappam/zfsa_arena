*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_ARTICLE_LINK_PAI
*&---------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*&      Module  EXIT_9000  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE exit_9000 INPUT.

  CASE sy-ucomm.
*   Back
    WHEN zcl_constants=>gc_ucomm_back.
      LEAVE TO SCREEN 0.

*   Exit
    WHEN zcl_constants=>gc_ucomm_exit.
      LEAVE TO SCREEN 0.

*   Cancel
    WHEN zcl_constants=>gc_ucomm_cancel.
      LEAVE PROGRAM.

  ENDCASE.

ENDMODULE.
