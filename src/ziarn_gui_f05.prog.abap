*----------------------------------------------------------------------*
***INCLUDE ZIARN_GUI_F05.
*----------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*&      Form  CHANGE_REG_CLUSTER_ICON
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM change_reg_cluster_icon_0112.

  DATA: lv_icon_name TYPE icon_d.

  IF gv_reg_pb_cluster_expanded = abap_false.
    gv_reg_pb_cluster_expanded = abap_true.
    lv_icon_name = icon_collapse.
  ELSE.
    gv_reg_pb_cluster_expanded = abap_false.
    lv_icon_name = icon_expand.
  ENDIF.

  CALL FUNCTION 'ICON_CREATE'
    EXPORTING
      name                  = lv_icon_name
      text                  = 'Cluster'(072)
    IMPORTING
      result                = gv_reg_pb_cluster
    EXCEPTIONS
      icon_not_found        = 1
      outputfield_too_short = 2
      OTHERS                = 3.
  IF sy-subrc <> 0.
    RETURN.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  DETERMINE_REG_CLUSTER_SCREEN
*&---------------------------------------------------------------------*
* Determines subscreen for Regional Cluster
*----------------------------------------------------------------------*
FORM determine_reg_cluster_screen .

  IF lcl_reg_cluster_ui=>get_instance( )->is_toggle_framework_active( ) = abap_false.
    gv_reg_cluster_scr = gv_empty_screen_no.
    RETURN.
  ENDIF.

  IF gv_reg_pb_cluster_expanded = abap_true.
    gv_reg_cluster_scr = gv_reg_cluster_scr_no.
  ELSE.
    gv_reg_cluster_scr = gv_empty_screen_no.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  BUILD_REG_CLUSTER_FIELDCAT
*&---------------------------------------------------------------------*
* Build Regional Cluster ALV Catalog
*----------------------------------------------------------------------*
FORM build_reg_cluster_fieldcat  CHANGING ct_fieldcat TYPE lvc_t_fcat.


  PERFORM fieldcatalog_merge USING    'ZSARN_REG_CLUSTER_UI'
                             CHANGING ct_fieldcat.

  LOOP AT ct_fieldcat ASSIGNING FIELD-SYMBOL(<ls_fieldcat>).

    CASE <ls_fieldcat>-fieldname.
      WHEN 'CLUSTER1_RANGE' OR
           'CLUSTER2_RANGE' OR
           'CLUSTER3_RANGE' OR
           'CLUSTER4_RANGE'.
        <ls_fieldcat>-outputlen = 15.
        <ls_fieldcat>-edit      = abap_true.
      WHEN 'CLUSTER1_RANGE_DESC' OR
           'CLUSTER2_RANGE_DESC' OR
           'CLUSTER3_RANGE_DESC' OR
           'CLUSTER4_RANGE_DESC'.
        <ls_fieldcat>-outputlen = 15.
      WHEN 'IDNO'.
        <ls_fieldcat>-tech = abap_true.
        <ls_fieldcat>-key  = abap_true.
      WHEN 'BANNER'.
        <ls_fieldcat>-key  = abap_true.
      WHEN OTHERS.
        CONTINUE.
    ENDCASE.

  ENDLOOP.

ENDFORM.

FORM fieldcatalog_merge USING    iv_structure TYPE tabname
                        CHANGING ct_fieldcat  TYPE lvc_t_fcat.

  CALL FUNCTION 'LVC_FIELDCATALOG_MERGE'
    EXPORTING
      i_structure_name       = iv_structure
    CHANGING
      ct_fieldcat            = ct_fieldcat
    EXCEPTIONS
      inconsistent_interface = 1
      program_error          = 2
      OTHERS                 = 3.
  IF sy-subrc <> 0.
    zcl_message_services=>issue_message_to_screen( iv_error_only = abap_true ).
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  ALIGN_REGIONAL_CLUSTER
*&---------------------------------------------------------------------*
* Regional cluster range must be in sync with Banner range. Align the values
* where necessary
*----------------------------------------------------------------------*
FORM align_regional_cluster .

  CHECK zarn_reg_hdr-matnr IS INITIAL.

  LOOP AT gt_zarn_reg_banner ASSIGNING FIELD-SYMBOL(<ls_reg_banner>).
    lcl_reg_cluster_ui=>get_instance( )->align_regional_cluster( EXPORTING is_reg_banner_ui  = <ls_reg_banner>
                                                                 CHANGING  ct_reg_cluster_ui = gt_zarn_reg_cluster ).
  ENDLOOP.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  BANNER_AND_CLUSTER_ALV
*&---------------------------------------------------------------------*
* Initialise Banner and Cluster ALV
*----------------------------------------------------------------------*
FORM banner_and_cluster_alv .
  PERFORM reg_rb_alv.
  PERFORM reg_cluster_alv.
ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  INIT_REG_CLUSTER_ALV
*&---------------------------------------------------------------------*
* Initialise Reginal Cluster ALV
*----------------------------------------------------------------------*
FORM init_reg_cluster_alv .

  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo,
    ls_variant  TYPE disvariant.

  CREATE OBJECT go_reg_cluster_container
    EXPORTING
      container_name = gc_reg_cluster_container.

  CREATE OBJECT go_reg_cluster_alv
    EXPORTING
      i_parent = go_reg_cluster_container.

  go_reg_cluster_alv->register_edit_event( i_event_id = cl_gui_alv_grid=>mc_evt_enter ).
  go_reg_cluster_alv->register_edit_event( i_event_id = cl_gui_alv_grid=>mc_evt_modified ).

  " Register events
  DATA(lo_cluster_ui) = lcl_reg_cluster_ui=>get_instance( ).
  DATA(lo_controller) = lcl_controller=>get_instance( ).
  lo_cluster_ui->set_alv_instance( io_alv = go_reg_cluster_alv ).
  SET HANDLER lo_cluster_ui->handle_display_mode_changed FOR lo_controller.
  SET HANDLER lo_cluster_ui->handle_data_changed         FOR go_reg_cluster_alv.
  SET HANDLER lo_cluster_ui->handle_data_changed         FOR go_reg_cluster_alv.
  SET HANDLER go_event_receiver->handle_user_command     FOR go_reg_cluster_alv.
  SET HANDLER go_event_receiver->handle_toolbar          FOR go_reg_cluster_alv.

  ls_layout-sel_mode   = abap_true.
  ls_layout-zebra      = abap_true.

  PERFORM build_reg_cluster_fieldcat CHANGING lt_fieldcat.
  PERFORM exclude_tb_functions_edit USING gc_alv_reg_cluster CHANGING lt_exclude.

  ls_variant-handle   = 'RCLU'.
  PERFORM alv_variant_default CHANGING ls_variant.

  CALL METHOD go_reg_cluster_alv->set_table_for_first_display
    EXPORTING
      is_variant                    = ls_variant
      i_save                        = gc_save
      is_layout                     = ls_layout
      it_toolbar_excluding          = lt_exclude
    CHANGING
      it_outtab                     = gt_zarn_reg_cluster
      it_fieldcatalog               = lt_fieldcat
    EXCEPTIONS
      invalid_parameter_combination = 1
      program_error                 = 2
      too_many_lines                = 3
      OTHERS                        = 4.
  IF sy-subrc IS NOT INITIAL.
    zcl_message_services=>issue_message_to_screen( ).
  ENDIF.

  " Cluster ALV is displayed on demand hence, raise display mode change event to sync the display mode
  lo_controller->raise_display_mode_changed( ).

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  ENRICH_DATA
*&---------------------------------------------------------------------*
* Enrich additional information
*----------------------------------------------------------------------*
FORM enrich_data .

  PERFORM enrich_reg_cluster.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  ENRICH_REG_CLUSTER
*&---------------------------------------------------------------------*
* Enrich Regional Cluster ALV data
*----------------------------------------------------------------------*
FORM enrich_reg_cluster .
  lcl_reg_cluster_ui=>get_instance( )->enrich_reg_cluster( CHANGING ct_reg_cluster_ui = gt_zarn_reg_cluster ).
ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  ENRICH_REG_BANNER_FIELDCAT
*&---------------------------------------------------------------------*
* Enrich regional banner field catalog
*----------------------------------------------------------------------*
FORM enrich_reg_banner_fieldcat CHANGING ct_fieldcat TYPE lvc_t_fcat.

  LOOP AT ct_fieldcat ASSIGNING FIELD-SYMBOL(<ls_fieldcat>).

    CASE <ls_fieldcat>-fieldname.
      WHEN 'IDNO' OR
           'BANNER'.
        <ls_fieldcat>-key  = abap_true.
      WHEN OTHERS.
        CONTINUE.
    ENDCASE.

  ENDLOOP.

ENDFORM.

FORM enrich_reg_allerg_ovr_fieldcat CHANGING ct_fieldcat TYPE lvc_t_fcat.

  LOOP AT ct_fieldcat ASSIGNING FIELD-SYMBOL(<ls_fieldcat>).

    CASE <ls_fieldcat>-fieldname.
      WHEN gc_alg_typ_ovr         OR
           gc_alg_typ_ovr_src     OR
           gc_alg_typ_ovr_date    OR
           gc_lvl_cont_ovr        OR
           gc_lvl_cont_ovr_src    OR
           gc_lvl_cont_ovr_date.

        <ls_fieldcat>-edit = abap_true.

      WHEN gc_allergen_type.
        <ls_fieldcat>-scrtext_s = 'NatAlrgTyp'(091).
        <ls_fieldcat>-scrtext_m = 'Nat Allergen Type'(092).
        <ls_fieldcat>-outputlen = 10.

      WHEN gc_allergen_type_reg.
        <ls_fieldcat>-scrtext_s = 'RegAlrgTyp'(093).
        <ls_fieldcat>-scrtext_m = 'Reg Allergen Type'(094).
        <ls_fieldcat>-outputlen = 10.

      WHEN gc_lvl_containment.
        <ls_fieldcat>-scrtext_s = 'NatLvlCont'(095).
        <ls_fieldcat>-scrtext_m = 'Nat Lvl of Cont'(096).
        <ls_fieldcat>-scrtext_l = 'National Level of Containment'(097).
        <ls_fieldcat>-outputlen = 10.

      WHEN OTHERS.
        CONTINUE.
    ENDCASE.

  ENDLOOP.

ENDFORM.
