*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_APPROVALS_MASS_MAIN
*&---------------------------------------------------------------------*
START-OF-SELECTION.

* Initialize Data
  PERFORM initialize.

  TRY.
      lcl_controller=>get( )->main( ).
    CATCH lcx_exception.
      WRITE 'error'.
      RETURN.
  ENDTRY.

* Get National and Regional Data
  PERFORM get_nat_reg_data USING gt_ver_data[]
                        CHANGING gt_prod_data[]
                                 gt_reg_data[]
                                 gt_prod_data_o[]
                                 gt_reg_data_o[].


*----------------------------------------------------------------------*
*   END OF SELECTION
*----------------------------------------------------------------------*
END-OF-SELECTION.
  lcl_controller=>get( )->call_screen_0100( abap_true ).
