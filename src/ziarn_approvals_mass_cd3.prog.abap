*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_APPROVALS_MASS_CD3
*&---------------------------------------------------------------------*

*&---------------------------------------------------------------------*
*&       Class lcl_model
*&---------------------------------------------------------------------*
*        Text
*----------------------------------------------------------------------*
CLASS lcl_model DEFINITION FRIENDS lcl_controller.

  PUBLIC SECTION.
    METHODS constructor
      IMPORTING ir_idno   TYPE ty_r_idno
                ir_chgcat TYPE ty_r_chgcat OPTIONAL
                ir_chgare TYPE ty_r_chgare OPTIONAL
                ir_nstat  TYPE ty_r_nstat  OPTIONAL
                ir_rstat  TYPE ty_r_rstat  OPTIONAL
      RAISING   lcx_exception.

    METHODS initialize.

  PRIVATE SECTION.
    DATA: mt_cc_hdr      TYPE ty_t_cc_hdr,
          mt_cc_desc     TYPE ty_t_cc_desc,
          mt_reg_hdr     TYPE ty_t_reg_hdr,
          mt_prd_version TYPE ty_t_prd_version,
          mt_ver_data    TYPE ty_t_ver_data,
          mt_approval    TYPE ty_t_approval,
          mt_appr_ch     TYPE ty_t_appr_ch,
          mt_rel_matrix  TYPE ty_t_rel_matrix,
          mt_table_mastr type ty_t_table_mastr,
          mt_field_confg type ty_t_field_confg,

          mr_idno        TYPE ty_r_idno,
          mr_chgcat      TYPE ty_r_chgcat,
          mr_chgare      TYPE ty_r_chgare,
          mr_nstat       TYPE ty_r_nstat,
          mr_rstat       TYPE ty_r_rstat.



    METHODS get_data.

ENDCLASS.
