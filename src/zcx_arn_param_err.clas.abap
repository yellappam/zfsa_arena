class ZCX_ARN_PARAM_ERR definition
  public
  inheriting from CX_STATIC_CHECK
  create public .

public section.

  constants NOT_FOUND type SOTR_CONC value '001DD8B71C341ED6A59554C229F360C5' ##NO_TEXT.
  data MV_PARAM type ZARN_E_PARAM_NAME read-only .

  methods CONSTRUCTOR
    importing
      !TEXTID like TEXTID optional
      !PREVIOUS like PREVIOUS optional
      !MV_PARAM type ZARN_E_PARAM_NAME optional .
protected section.
private section.
ENDCLASS.



CLASS ZCX_ARN_PARAM_ERR IMPLEMENTATION.


  method CONSTRUCTOR.
CALL METHOD SUPER->CONSTRUCTOR
EXPORTING
TEXTID = TEXTID
PREVIOUS = PREVIOUS
.
me->MV_PARAM = MV_PARAM .
  endmethod.
ENDCLASS.
