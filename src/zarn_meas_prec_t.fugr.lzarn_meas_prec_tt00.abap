*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 05.05.2016 at 15:00:58 by user C90001363
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_MEAS_PREC_T................................*
DATA:  BEGIN OF STATUS_ZARN_MEAS_PREC_T              .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_MEAS_PREC_T              .
CONTROLS: TCTRL_ZARN_MEAS_PREC_T
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_MEAS_PREC_T              .
TABLES: ZARN_MEAS_PREC_T               .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
