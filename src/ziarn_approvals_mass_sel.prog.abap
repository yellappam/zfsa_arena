*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_APPROVALS_MASS_SEL
*&---------------------------------------------------------------------*


*----------------------------------------------------------------------*
*   SELECTION SCREEN
*----------------------------------------------------------------------*

SELECTION-SCREEN BEGIN OF BLOCK b1 WITH FRAME TITLE text-001.
SELECT-OPTIONS: s_idno   FOR zarn_reg_hdr-idno OBLIGATORY,
                s_nstat  FOR zarn_prd_version-version_status,
                s_rstat  FOR zarn_reg_hdr-status.
SELECTION-SCREEN END OF BLOCK b1.

SELECTION-SCREEN BEGIN OF BLOCK b2 WITH FRAME TITLE text-002.
SELECT-OPTIONS: s_chgcat FOR zarn_cc_hdr-chg_category,
                s_chgare FOR zarn_cc_hdr-chg_area.
SELECTION-SCREEN END OF BLOCK b2.



AT SELECTION-SCREEN ON s_nstat.

  LOOP AT s_nstat.
    TRANSLATE s_nstat-low  TO UPPER CASE.
    TRANSLATE s_nstat-high TO UPPER CASE.
    MODIFY s_nstat.
  ENDLOOP.

AT SELECTION-SCREEN ON s_rstat.
  LOOP AT s_rstat.
    TRANSLATE s_rstat-low  TO UPPER CASE.
    TRANSLATE s_rstat-high TO UPPER CASE.
    MODIFY s_rstat.
  ENDLOOP.
