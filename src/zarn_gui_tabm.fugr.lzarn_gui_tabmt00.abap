*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 12.05.2016 at 13:27:03 by user C90002769
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZVARN_GUI_DEF...................................*
TABLES: ZVARN_GUI_DEF, *ZVARN_GUI_DEF. "view work areas
CONTROLS: TCTRL_ZVARN_GUI_DEF
TYPE TABLEVIEW USING SCREEN '0003'.
DATA: BEGIN OF STATUS_ZVARN_GUI_DEF. "state vector
          INCLUDE STRUCTURE VIMSTATUS.
DATA: END OF STATUS_ZVARN_GUI_DEF.
* Table for entries selected to show on screen
DATA: BEGIN OF ZVARN_GUI_DEF_EXTRACT OCCURS 0010.
INCLUDE STRUCTURE ZVARN_GUI_DEF.
          INCLUDE STRUCTURE VIMFLAGTAB.
DATA: END OF ZVARN_GUI_DEF_EXTRACT.
* Table for all entries loaded from database
DATA: BEGIN OF ZVARN_GUI_DEF_TOTAL OCCURS 0010.
INCLUDE STRUCTURE ZVARN_GUI_DEF.
          INCLUDE STRUCTURE VIMFLAGTAB.
DATA: END OF ZVARN_GUI_DEF_TOTAL.

*...processing: ZVARN_GUI_SEL...................................*
TABLES: ZVARN_GUI_SEL, *ZVARN_GUI_SEL. "view work areas
CONTROLS: TCTRL_ZVARN_GUI_SEL
TYPE TABLEVIEW USING SCREEN '0002'.
DATA: BEGIN OF STATUS_ZVARN_GUI_SEL. "state vector
          INCLUDE STRUCTURE VIMSTATUS.
DATA: END OF STATUS_ZVARN_GUI_SEL.
* Table for entries selected to show on screen
DATA: BEGIN OF ZVARN_GUI_SEL_EXTRACT OCCURS 0010.
INCLUDE STRUCTURE ZVARN_GUI_SEL.
          INCLUDE STRUCTURE VIMFLAGTAB.
DATA: END OF ZVARN_GUI_SEL_EXTRACT.
* Table for all entries loaded from database
DATA: BEGIN OF ZVARN_GUI_SEL_TOTAL OCCURS 0010.
INCLUDE STRUCTURE ZVARN_GUI_SEL.
          INCLUDE STRUCTURE VIMFLAGTAB.
DATA: END OF ZVARN_GUI_SEL_TOTAL.

*...processing: ZVARN_GUI_SET...................................*
TABLES: ZVARN_GUI_SET, *ZVARN_GUI_SET. "view work areas
CONTROLS: TCTRL_ZVARN_GUI_SET
TYPE TABLEVIEW USING SCREEN '0001'.
DATA: BEGIN OF STATUS_ZVARN_GUI_SET. "state vector
          INCLUDE STRUCTURE VIMSTATUS.
DATA: END OF STATUS_ZVARN_GUI_SET.
* Table for entries selected to show on screen
DATA: BEGIN OF ZVARN_GUI_SET_EXTRACT OCCURS 0010.
INCLUDE STRUCTURE ZVARN_GUI_SET.
          INCLUDE STRUCTURE VIMFLAGTAB.
DATA: END OF ZVARN_GUI_SET_EXTRACT.
* Table for all entries loaded from database
DATA: BEGIN OF ZVARN_GUI_SET_TOTAL OCCURS 0010.
INCLUDE STRUCTURE ZVARN_GUI_SET.
          INCLUDE STRUCTURE VIMFLAGTAB.
DATA: END OF ZVARN_GUI_SET_TOTAL.

*.........table declarations:.................................*
TABLES: ZARN_GUI_DEF                   .
TABLES: ZARN_GUI_SEL                   .
TABLES: ZARN_GUI_SET                   .
