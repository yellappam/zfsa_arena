class ZCL_ARN_NOTIF_REJECT definition
  public
  inheriting from ZCL_ARN_NOTIF
  create public .

public section.

  methods CONSTRUCTOR
    importing
      !IV_IDNO type ZARN_IDNO
      !IV_NOTIF_TEXT type ZARN_E_APPR_COMMENT optional
      !IT_APPROVALS type ZARN_T_APPROVAL optional
      !IS_NOTIF_PARAM type ZSARN_NOTIF_PARAM optional .
protected section.

  methods CREATE_REJECT_ATTACH
    exporting
      !ES_ATTACHMENT type ZSARN_BCS_ATTACH .
  methods BUILD_FILE_HEADER
    returning
      value(RV_HEADER) type STRING .

  methods CREATE_ATTACH
    redefinition .
  methods GET_BODY
    redefinition .
  methods GET_SUBJECT
    redefinition .
  methods RECIPIENT_DETERMINATION
    redefinition .
private section.
ENDCLASS.



CLASS ZCL_ARN_NOTIF_REJECT IMPLEMENTATION.


  method BUILD_FILE_HEADER.

    CONCATENATE
      'Base Unit GTIN'
      'Hybris Article Number'
      'Description'
      'Vendor Name'
      'Vendor GLN / Number'
      'Notes'
      INTO rv_header SEPARATED BY cl_abap_char_utilities=>horizontal_tab.

  endmethod.


  METHOD constructor.
    super->constructor(
      EXPORTING
        iv_idno       = iv_idno
        iv_notif_text = iv_notif_text
        it_approvals  = it_approvals
        is_notif_param = is_notif_param
      ).
  ENDMETHOD.


  METHOD create_attach.
* Create the attachment for the rejection

    CLEAR et_attachments.

* Create the attachment
    me->create_reject_attach(
      IMPORTING
        es_attachment = DATA(ls_attachment)
    ).

    APPEND ls_attachment TO et_attachments.

  ENDMETHOD.


  METHOD create_reject_attach.
* Create the rejection attachment

    CONSTANTS: lc_file_ext TYPE char3 VALUE 'XLS'.

    DATA: lv_attachment TYPE string.

* Initialise
    CLEAR: es_attachment.

* Determine the version for the approval block(as regional doesn't know a version)
    DATA(lv_version) = zcl_arn_notif_service=>get_latest_version( me->mt_approvals ).

* Gather data we need for building our attachment
    zcl_arn_read_db=>read_national(
      EXPORTING
        iv_idno           = me->mv_idno
        iv_version        = lv_version
      IMPORTING
        es_product        = DATA(ls_product)
        et_pir            = DATA(lt_pir)
        et_uom_variant    = DATA(lt_uom_variant)
        et_gtin_variant   = DATA(lt_gtin_variant)
    ).

* Identify the base unit gtin
    TRY.
        IF lt_uom_variant IS NOT INITIAL.
          DATA(lv_base_uom_code) =
            lt_uom_variant[ idno = me->mv_idno version = lv_version base_unit = abap_true ]-uom_code.
          DATA(lv_base_gtin_code) =
            lt_gtin_variant[ idno = me->mv_idno version = lv_version uom_code = lv_base_uom_code ]-gtin_code.
        ENDIF.
      CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
    ENDTRY.

* Get unique list of vendor GLNs
    DELETE lt_pir WHERE uom_code <> lv_base_uom_code.
    SORT lt_pir BY gln.
    DELETE ADJACENT DUPLICATES FROM lt_pir COMPARING gln.

* Build the header for the attachment
    DATA(lv_file_string) = me->build_file_header( ).

* Make sure we get rid of control characters in the existing comments
    DATA(lv_comment) = me->replace_ctl_char( me->mv_notif_text ).

* For each unique vendor GLN we create a record
    LOOP AT lt_pir ASSIGNING FIELD-SYMBOL(<ls_pir>).
* Build the record
      CONCATENATE
       lv_file_string
       cl_abap_char_utilities=>cr_lf
       lv_base_gtin_code "Base GTIN
       cl_abap_char_utilities=>horizontal_tab
       me->mv_idno "Hybris number
       cl_abap_char_utilities=>horizontal_tab
       ls_product-fs_short_descr_upper "Description
       cl_abap_char_utilities=>horizontal_tab
       <ls_pir>-gln_name "Vendor name
       cl_abap_char_utilities=>horizontal_tab
       <ls_pir>-gln "Vendor GLN
       cl_abap_char_utilities=>horizontal_tab
       lv_comment "Notifcation text
       INTO lv_file_string.
    ENDLOOP.

* Get into attachment format
    TRY.
        cl_bcs_convert=>string_to_solix(
          EXPORTING
            iv_string   = lv_file_string
          IMPORTING
            et_solix    = es_attachment-content
            ev_size     = es_attachment-size
        ).
        es_attachment-name = zcl_arn_notif_service=>get_attach_reject_filename( ).
        es_attachment-type = lc_file_ext.
      CATCH cx_bcs ##NO_HANDLER.
    ENDTRY.

  ENDMETHOD.


  METHOD get_body.
* Return rejection email body

    DATA: lt_lines TYPE TABLE OF tline.

    TRY.
        DATA(lv_text) = zcl_arn_param_prov=>get( zif_arn_param_const=>c_notify_reject_email_body ).
      CATCH zcx_arn_param_err.
        RETURN.
    ENDTRY.

* Read out the email body text
    CALL FUNCTION 'READ_TEXT'
      EXPORTING
        id                      = 'ST'
        language                = sy-langu
        name                    = CONV tdobname( lv_text )
        object                  = 'TEXT'
      TABLES
        lines                   = lt_lines
      EXCEPTIONS
        id                      = 1
        language                = 2
        name                    = 3
        not_found               = 4
        object                  = 5
        reference_check         = 6
        wrong_access_to_archive = 7
        OTHERS                  = 8.
    IF sy-subrc = 0.
      rt_body = CORRESPONDING #( lt_lines MAPPING line = tdline  ).
    ENDIF.

  ENDMETHOD.


  METHOD get_subject.
* Return the email subject

    DATA: lt_lines TYPE TABLE OF tline.

    TRY.
        DATA(lv_text) = zcl_arn_param_prov=>get( zif_arn_param_const=>c_notify_reject_email_subj ).
* Read out the email body text
        CALL FUNCTION 'READ_TEXT'
          EXPORTING
            id                      = 'ST'
            language                = sy-langu
            name                    = CONV tdobname( lv_text )
            object                  = 'TEXT'
          TABLES
            lines                   = lt_lines
          EXCEPTIONS
            id                      = 1
            language                = 2
            name                    = 3
            not_found               = 4
            object                  = 5
            reference_check         = 6
            wrong_access_to_archive = 7
            OTHERS                  = 8.
        IF sy-subrc = 0.
          rv_subject = lt_lines[ 1 ]-tdline.
        ENDIF.

      CATCH zcx_arn_param_err  cx_sy_itab_line_not_found.
        rv_subject = 'Change(s) rejected from AReNa'(001).
    ENDTRY.

  ENDMETHOD.


  METHOD recipient_determination.
* Determine recipients for rejection mail

    DATA: lv_catman TYPE zmd_e_catman.

* Include default behaviours
    rt_recipients = super->recipient_determination( ).

* Get the rejection DL
        DATA(lt_dl) = zcl_arn_notif_service=>get_rejection_dls( ).
        LOOP AT lt_dl INTO DATA(lv_dl).
* Add it as recipient email
          IF lv_dl CS '@'.
            APPEND cl_cam_address_bcs=>create_internet_address( CONV #( lv_dl ) ) TO rt_recipients.
* Or as a real DL in SAP
          ELSE.
            APPEND cl_distributionlist_bcs=>getu_persistent(
               EXPORTING
                 i_dliname      = CONV #( lv_dl )
                 i_private      = abap_false
             ) TO rt_recipients.

          ENDIF.
        ENDLOOP.

* Read out the regional header data to get the category manager info
        DATA(ls_reg_hdr) = zcl_arn_read_db=>read_hdr( me->mv_idno ).

* Retail category manager
        IF ls_reg_hdr-ret_zzcatman IS NOT INITIAL.
          DATA(lv_catman_user) = zcl_arn_notif_service=>get_catman_user( ls_reg_hdr-ret_zzcatman ).
          IF lv_catman_user IS NOT INITIAL.
            APPEND
              cl_cam_address_bcs=>create_user_home_address(
                  i_user         = lv_catman_user
                  i_commtype     = cl_cam_address_bcs=>c_address_type_smtp
              ) TO rt_recipients.
          ENDIF.
        ENDIF.

* Wholesale category manager
        IF ls_reg_hdr-gil_zzcatman IS NOT INITIAL.
          lv_catman_user = zcl_arn_notif_service=>get_catman_user( ls_reg_hdr-gil_zzcatman ).
          IF lv_catman_user IS NOT INITIAL.
            APPEND
              cl_cam_address_bcs=>create_user_home_address(
                  i_user         = lv_catman_user
                  i_commtype     = cl_cam_address_bcs=>c_address_type_smtp
              ) TO rt_recipients.
          ENDIF.
        ENDIF.



  ENDMETHOD.
ENDCLASS.
