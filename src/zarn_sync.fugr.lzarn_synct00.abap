*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 16.03.2016 at 15:04:55 by user C89991021
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_SYNC.......................................*
DATA:  BEGIN OF STATUS_ZARN_SYNC                     .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_SYNC                     .
CONTROLS: TCTRL_ZARN_SYNC
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_SYNC                     .
TABLES: ZARN_SYNC                      .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
