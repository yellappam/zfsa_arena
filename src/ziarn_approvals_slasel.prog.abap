*&---------------------------------------------------------------------*
*&  Include           ZIARN_APPROVALS_SLASEL
*&---------------------------------------------------------------------*

**********************************************************************
** Itemised Report Tab
**********************************************************************
* Scenario Selection
SELECTION-SCREEN BEGIN OF SCREEN 1100 AS SUBSCREEN.
SELECTION-SCREEN BEGIN OF BLOCK bl1 WITH FRAME TITLE text-001.
PARAMETERS: p_pwa RADIOBUTTON GROUP rd1 DEFAULT 'X',
            p_awa RADIOBUTTON GROUP rd1.
SELECTION-SCREEN END OF BLOCK bl1.

* Product Selection
SELECTION-SCREEN BEGIN OF BLOCK bl2 WITH FRAME TITLE text-002.
SELECT-OPTIONS: s_fanid FOR zarn_products-fan_id MATCHCODE OBJECT zmd_zzfan,
                s_hybid FOR zarn_products-idno,
                s_cman  FOR zarn_reg_hdr-gil_zzcatman NO INTERVALS,
                s_wfca  FOR zarn_s_approval_sel-wf_created_at OBLIGATORY.
SELECTION-SCREEN END OF BLOCK bl2.

* Status Selection
SELECTION-SCREEN BEGIN OF BLOCK bl3 WITH FRAME TITLE text-003.
* New or Change article
SELECTION-SCREEN BEGIN OF LINE.
PARAMETERS: p_apnew TYPE xfeld RADIOBUTTON GROUP ag1.
SELECTION-SCREEN COMMENT 3(15) text-005.
PARAMETERS: p_apchg TYPE xfeld RADIOBUTTON GROUP ag1.
SELECTION-SCREEN COMMENT 21(15) text-006.
PARAMETERS: p_apncb TYPE xfeld RADIOBUTTON GROUP ag1 DEFAULT 'X'.
SELECTION-SCREEN COMMENT 39(10) text-009.
SELECTION-SCREEN END OF LINE.
* National or regional change
SELECTION-SCREEN BEGIN OF LINE.
PARAMETERS: p_apnat TYPE xfeld RADIOBUTTON GROUP ag2.
SELECTION-SCREEN COMMENT 3(15) text-007.
PARAMETERS: p_apreg TYPE xfeld RADIOBUTTON GROUP ag2.
SELECTION-SCREEN COMMENT 21(15) text-008.
PARAMETERS: p_apnrb TYPE xfeld RADIOBUTTON GROUP ag2 DEFAULT 'X'.
SELECTION-SCREEN COMMENT 39(10) text-009.
SELECTION-SCREEN END OF LINE.

SELECT-OPTIONS: s_ateam FOR zarn_s_approval_sel-team_code NO INTERVALS,
                s_ccat  FOR zarn_s_approval_sel-chg_category NO INTERVALS.
SELECTION-SCREEN END OF BLOCK bl3.
SELECTION-SCREEN END OF SCREEN 1100.

**********************************************************************
** Product Overview tab
**********************************************************************
SELECTION-SCREEN BEGIN OF SCREEN 1200 AS SUBSCREEN.
SELECTION-SCREEN BEGIN OF BLOCK bl4 WITH FRAME TITLE text-010.
SELECT-OPTIONS: spo_per FOR zarn_s_approval_sel-wf_created_at.
SELECTION-SCREEN END OF BLOCK bl4.
SELECTION-SCREEN BEGIN OF BLOCK bl5 WITH FRAME TITLE text-011.
SELECT-OPTIONS: spo_init FOR zarn_reg_hdr-initiation NO INTERVALS,
                spo_cman FOR zarn_reg_hdr-gil_zzcatman NO INTERVALS.
SELECTION-SCREEN END OF BLOCK bl5.
SELECTION-SCREEN END OF SCREEN 1200.

**********************************************************************
** Vendor Overview
**********************************************************************
SELECTION-SCREEN BEGIN OF SCREEN 1300 AS SUBSCREEN.
SELECTION-SCREEN BEGIN OF BLOCK bl6 WITH FRAME TITLE text-010.
SELECT-OPTIONS: svo_per FOR zarn_s_approval_sel-wf_created_at NO-EXTENSION.
SELECTION-SCREEN END OF BLOCK bl6.
SELECTION-SCREEN BEGIN OF BLOCK bl7 WITH FRAME TITLE text-011.

SELECTION-SCREEN BEGIN OF LINE.
PARAMETERS r1 RADIOBUTTON GROUP rad1 USER-COMMAND rad1 DEFAULT 'X'.
SELECTION-SCREEN COMMENT 5(24) text-c01 FOR FIELD svo_gln.
SELECT-OPTIONS: svo_gln FOR zarn_reg_pir-gln_no NO INTERVALS.
SELECTION-SCREEN END OF LINE.

SELECTION-SCREEN BEGIN OF LINE.
PARAMETERS r2 RADIOBUTTON GROUP rad1.
SELECTION-SCREEN COMMENT 5(24) text-c02 FOR FIELD svo_lifn.
SELECT-OPTIONS: svo_lifn FOR zarn_reg_pir-lifnr NO INTERVALS.
SELECTION-SCREEN END OF LINE.

SELECT-OPTIONS: "svo_lifn FOR zarn_reg_pir-lifnr NO INTERVALS,
                svo_cmrl FOR zmd_category-role_id NO INTERVALS.
SELECTION-SCREEN END OF BLOCK bl7.
SELECTION-SCREEN END OF SCREEN 1300.


**********************************************************************
** Tabstrip
**********************************************************************
SELECTION-SCREEN: BEGIN OF TABBED BLOCK mytab FOR 16 LINES,
                  TAB (20) tab_itm USER-COMMAND tab_itm DEFAULT SCREEN 1100, "Itemised Report
                  TAB (20) tab_pov USER-COMMAND tab_pov, "Product Overview
                  TAB (20) tab_vov USER-COMMAND tab_vov,  "Vendor Overview
**                                   DEFAULT SCREEN 1100,
                  END OF BLOCK mytab.











**********************************************************************
** Screen Events
**********************************************************************

INITIALIZATION.
**********************************************************************
** Defaults
**********************************************************************
* Default last month for approval workflow date
  s_wfca-low = lcl_approvals_sla=>get_default_wf_created_date( ).
  s_wfca-sign = 'I'.
  s_wfca-option = 'GE'.
  APPEND s_wfca TO s_wfca[].

**********************************************************************
** Tabstrip
**********************************************************************
  tab_itm = 'Itemised Report'(014).
  tab_pov = 'Product Overview'(012).
  tab_vov = 'Vendor Overview'(013).
  mytab-prog = sy-repid.

  GET PARAMETER ID 'ZARN_APPROVALS_SLA' FIELD gv_sel_scr_tab.

  IF gv_sel_scr_tab IS INITIAL.
    mytab-dynnr = 1100.
    mytab-activetab = gc_tab-tab_itm.
  ELSE.
    mytab-activetab = gv_sel_scr_tab.
    CASE mytab-activetab.
      WHEN gc_tab-tab_itm.
        mytab-dynnr = 1100.
      WHEN gc_tab-tab_pov.
        mytab-dynnr = 1200.
      WHEN gc_tab-tab_vov.
        mytab-dynnr = 1300.
    ENDCASE.
  ENDIF.




AT SELECTION-SCREEN.
**********************************************************************
** Tabstrip
**********************************************************************
*  IF sy-dynnr = 1000.
  CASE sy-ucomm.
    WHEN gc_ucomm-tab_itm.
      mytab-dynnr = 1100.
      mytab-activetab = gc_tab-tab_itm.
    WHEN gc_ucomm-tab_pov.
      mytab-dynnr = 1200.
      mytab-activetab = gc_tab-tab_pov.
    WHEN gc_ucomm-tab_vov.
      mytab-dynnr = 1300.
      mytab-activetab = gc_tab-tab_vov.
  ENDCASE.
*  ENDIF.




AT SELECTION-SCREEN ON s_cman.
  IF sy-ucomm = 'ONLI'.
    CASE mytab-activetab.
* Itemised report
      WHEN gc_tab-tab_itm.
        IF s_cman[] IS INITIAL.
          MESSAGE e126.
        ENDIF.
    ENDCASE.
  ENDIF.


AT SELECTION-SCREEN ON spo_cman.
  IF sy-ucomm = 'ONLI'.
    CASE mytab-activetab.
* Product overview
      WHEN gc_tab-tab_pov.
        IF spo_cman[] IS INITIAL.
          MESSAGE e126.
        ENDIF.
    ENDCASE.
  ENDIF.


AT SELECTION-SCREEN ON svo_cmrl.
  IF sy-ucomm = 'ONLI'.
    CASE mytab-activetab.
* Vendor overview
      WHEN gc_tab-tab_vov.
        IF svo_cmrl[] IS INITIAL.
          MESSAGE e126.
        ENDIF.
    ENDCASE.
  ENDIF.



AT SELECTION-SCREEN ON svo_gln.
  IF sy-ucomm = 'ONLI'.
* Vendor overview
    IF mytab-activetab = gc_tab-tab_vov.
      IF svo_gln[] IS INITIAL AND r1 = abap_true.
        MESSAGE e128.
      ENDIF.
    ENDIF.
  ENDIF.

AT SELECTION-SCREEN ON svo_lifn.
  IF sy-ucomm = 'ONLI'.
* Vendor overview
    IF mytab-activetab = gc_tab-tab_vov.
      IF svo_lifn[] IS INITIAL AND r2 = abap_true.
        MESSAGE e129.
      ENDIF.
    ENDIF.
  ENDIF.

AT SELECTION-SCREEN ON svo_per.
  IF sy-ucomm = 'ONLI'.
* Vendor overview
    IF mytab-activetab = gc_tab-tab_vov.
      IF svo_per[] IS INITIAL.
        MESSAGE e131.
      ENDIF.
    ENDIF.
  ENDIF.



AT SELECTION-SCREEN ON VALUE-REQUEST FOR svo_gln-low.
  PERFORM get_lifnr_gln CHANGING svo_gln-low.












FORM get_lifnr_gln CHANGING pc_gln TYPE zarn_gln.

  DATA: lt_return TYPE STANDARD TABLE OF ddshretval,
        ls_return LIKE LINE OF lt_return,
        ls_lfa1   TYPE lfa1.

  REFRESH lt_return.
  CALL FUNCTION 'F4IF_FIELD_VALUE_REQUEST'
    EXPORTING
      tabname           = 'ZARN_PIR'
      fieldname         = 'GLN'
      searchhelp        = 'ZHARN_VENDOR'
      shlpparam         = 'LIFNR'
    TABLES
      return_tab        = lt_return
    EXCEPTIONS
      field_not_found   = 1
      no_help_for_field = 2
      inconsistent_help = 3
      no_values_found   = 4
      OTHERS            = 5.
  READ TABLE lt_return INTO ls_return WITH KEY fieldname = 'LIFNR'.
  IF sy-subrc EQ 0.
    ls_lfa1-lifnr = ls_return-fieldval.
    CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'
      EXPORTING
        input  = ls_lfa1-lifnr
      IMPORTING
        output = ls_lfa1-lifnr.
    SELECT SINGLE lifnr bbbnr bbsnr bubkz
      INTO CORRESPONDING FIELDS OF ls_lfa1
      FROM lfa1
      WHERE lifnr EQ ls_lfa1-lifnr.
    IF sy-subrc EQ 0.
      pc_gln+0(7)  = ls_lfa1-bbbnr.
      pc_gln+7(5)  = ls_lfa1-bbsnr.
      pc_gln+12(1) = ls_lfa1-bubkz.
    ENDIF.
  ENDIF.

ENDFORM.
