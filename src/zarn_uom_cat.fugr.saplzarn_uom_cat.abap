* regenerated at 24.05.2016 11:32:34 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_UOM_CATTOP.                  " Global Data
  INCLUDE LZARN_UOM_CATUXX.                  " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_UOM_CATF...                  " Subroutines
* INCLUDE LZARN_UOM_CATO...                  " PBO-Modules
* INCLUDE LZARN_UOM_CATI...                  " PAI-Modules
* INCLUDE LZARN_UOM_CATE...                  " Events
* INCLUDE LZARN_UOM_CATP...                  " Local class implement.
* INCLUDE LZARN_UOM_CATT99.                  " ABAP Unit tests
  INCLUDE LZARN_UOM_CATF00                        . " subprograms
  INCLUDE LZARN_UOM_CATI00                        . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
