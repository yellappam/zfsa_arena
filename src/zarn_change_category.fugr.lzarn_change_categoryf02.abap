*----------------------------------------------------------------------*
***INCLUDE LZARN_CHANGE_CATEGORYF02.
*----------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*&      Form  GET_LIST_PRICE_GLN_NR
*&---------------------------------------------------------------------*
* Get GLN to be set to NR for LIST_PRICE
*----------------------------------------------------------------------*
FORM get_list_price_gln_nr USING fu_t_list_price_n TYPE ztarn_list_price
                                 fu_t_list_price_o TYPE ztarn_list_price
                                 fu_t_uom_variant  TYPE ztarn_uom_variant
                                 fu_t_gtin_var     TYPE ztarn_gtin_var
                                 fu_v_mode         TYPE char3
                        CHANGING fc_t_gln_nr       TYPE ty_t_gln_nr.


  DATA: lt_list_price      TYPE ztarn_list_price,
        ls_list_price      TYPE zarn_list_price,
        lt_pir_vendor      TYPE ty_t_pir_vendor,
        ls_pir_vendor      TYPE ty_s_pir_vendor,
        lt_vendor          TYPE ty_t_pir_vendor,
        ls_vendor          TYPE ty_s_pir_vendor,
        lt_eina            TYPE STANDARD TABLE OF eina,
        ls_eina            TYPE eina,
        lt_eine            TYPE STANDARD TABLE OF eine,
        ls_eine            TYPE eine,
        ls_gln_nr          TYPE ty_s_gln_nr,

        lt_reg_uom         TYPE ztarn_reg_uom,
        ls_reg_uom         TYPE zarn_reg_uom,
        lt_marm            TYPE marm_tab,
        ls_marm            TYPE marm,

        lv_ekorg_main      TYPE ekorg,

        lv_matnr           TYPE matnr,
        lv_lower_uom       TYPE zarn_lower_uom,
        lv_lower_child_uom TYPE zarn_lower_child_uom.

  CLEAR fc_t_gln_nr[].

  IF fu_v_mode = 'DEL'.
    APPEND LINES OF fu_t_list_price_o[] TO lt_list_price[].
  ELSE.
    APPEND LINES OF fu_t_list_price_n[] TO lt_list_price[].
  ENDIF.


  IF lt_list_price[] IS INITIAL.
    EXIT.
  ENDIF.


  SORT lt_list_price[] BY idno version uom_code gln.
  DELETE lt_list_price[] WHERE active_lp NE 'X'.
  DELETE ADJACENT DUPLICATES FROM lt_list_price[] COMPARING idno version uom_code gln.

* Break GLN to get Vendor
  CLEAR: lt_pir_vendor[].
  LOOP AT lt_list_price[] INTO ls_list_price.
    CLEAR ls_pir_vendor.
    ls_pir_vendor-bbbnr = ls_list_price-gln+0(7).
    ls_pir_vendor-bbsnr = ls_list_price-gln+7(5).
    ls_pir_vendor-bubkz = ls_list_price-gln+12(1).
    INSERT ls_pir_vendor INTO TABLE lt_pir_vendor[].
  ENDLOOP.

  DELETE ADJACENT DUPLICATES FROM lt_pir_vendor[] COMPARING bbbnr bbsnr bubkz.


  CLEAR lt_vendor[].
  IF lt_pir_vendor[] IS NOT INITIAL.

* Get Vendor from GLN
    SELECT lifnr bbbnr bbsnr bubkz      "#EC CI_SEL_NESTED
      INTO CORRESPONDING FIELDS OF TABLE lt_vendor[]
       FROM lfa1
      FOR ALL ENTRIES IN lt_pir_vendor[]
              WHERE bbbnr EQ lt_pir_vendor-bbbnr   "#EC CI_NOFIELD
                AND bbsnr EQ lt_pir_vendor-bbsnr
                AND bubkz EQ lt_pir_vendor-bubkz.
    IF sy-subrc = 0.
      DELETE lt_vendor[] WHERE lifnr EQ space.
    ENDIF.
  ENDIF.

  IF lt_vendor[] IS INITIAL.
    EXIT.
  ENDIF.


* Get IDNO
  CLEAR ls_list_price.
  READ TABLE lt_list_price[] INTO ls_list_price INDEX 1.

* Get Article from IDNO and Current Version from MARA
  CLEAR: lv_matnr.
  SELECT SINGLE c~matnr INTO lv_matnr   "#EC CI_SEL_NESTED
    FROM zarn_ver_status AS a
    JOIN zarn_products AS b
    ON   a~idno = b~idno
    AND  a~current_ver = b~version
    JOIN mara AS c
    ON   b~fan_id = c~zzfan
    WHERE a~idno = ls_list_price-idno.

  IF lv_matnr IS INITIAL.
    EXIT.
  ENDIF.


* Get Main EKORG
  CLEAR lv_ekorg_main.
  SELECT SINGLE low FROM tvarvc
    INTO lv_ekorg_main
    WHERE name = 'ZARN_ARTICLE_POST_EINE_EKORG'
      AND type = 'S'
      AND high = abap_true.
  IF sy-subrc NE 0.
    lv_ekorg_main = '9999'.
  ENDIF.


  CLEAR: lt_eina[], lt_eine[].
  SELECT infnr matnr lifnr    "#EC CI_SEL_NESTED "#EC CI_NO_TRANSFORM
    FROM eina
    INTO CORRESPONDING FIELDS OF TABLE lt_eina[]
     FOR ALL ENTRIES IN lt_vendor[]
   WHERE matnr = lv_matnr
     AND lifnr = lt_vendor-lifnr.
  IF sy-subrc = 0.
    SELECT infnr ekorg esokz werks bprme    "#EC CI_SEL_NESTED "#EC CI_NO_TRANSFORM
      FROM eine
      INTO CORRESPONDING FIELDS OF TABLE lt_eine[]
       FOR ALL ENTRIES IN lt_eina[]
     WHERE infnr = lt_eina-infnr
       AND ekorg = lv_ekorg_main
       AND werks = space.
  ENDIF.


  IF lt_list_price[] IS NOT INITIAL.
    CLEAR lt_reg_uom[].
    SELECT idno uom_category pim_uom_code hybris_internal_code   "#EC CI_SEL_NESTED
           lower_uom lower_child_uom meinh lower_meinh
      FROM zarn_reg_uom
      INTO CORRESPONDING FIELDS OF TABLE lt_reg_uom[]
      FOR ALL ENTRIES IN lt_list_price[]
      WHERE idno                 = lt_list_price-idno
        AND pim_uom_code         = lt_list_price-uom_code
        AND hybris_internal_code = space.
  ENDIF.

  CLEAR lt_marm[].
  SELECT matnr meinh     "#EC CI_SEL_NESTED
    FROM marm
    INTO CORRESPONDING FIELDS OF TABLE lt_marm[]
    WHERE matnr = lv_matnr.



* Get GLNs to be set to NR from LP
  LOOP AT lt_list_price[] INTO ls_list_price.
    CLEAR ls_pir_vendor.
    ls_pir_vendor-bbbnr = ls_list_price-gln+0(7).
    ls_pir_vendor-bbsnr = ls_list_price-gln+7(5).
    ls_pir_vendor-bubkz = ls_list_price-gln+12(1).

** Get Vendor from GLN
*    CLEAR ls_vendor.
*    READ TABLE lt_vendor[] INTO ls_vendor
*    WITH TABLE KEY bbbnr = ls_pir_vendor-bbbnr
*                   bbsnr = ls_pir_vendor-bbsnr
*                   bubkz = ls_pir_vendor-bubkz.
*    IF sy-subrc = 0.

* Get Vendor from GLN
    LOOP AT lt_vendor[] INTO ls_vendor
      WHERE bbbnr = ls_pir_vendor-bbbnr
        AND bbsnr = ls_pir_vendor-bbsnr
        AND bubkz = ls_pir_vendor-bubkz.


* If PIR already exist then get this GLN to be set to NR from LP
      CLEAR ls_eina.
      READ TABLE lt_eina[] INTO ls_eina
      WITH KEY matnr = lv_matnr
               lifnr = ls_vendor-lifnr.
      IF sy-subrc EQ 0.
        CLEAR ls_eine.
        READ TABLE lt_eine[] INTO ls_eine
        WITH KEY infnr = ls_eina-infnr.
        IF sy-subrc EQ 0.


          CLEAR: lv_lower_uom, lv_lower_child_uom.
          CALL FUNCTION 'ZARN_GET_NAT_LOWER_CHILD_UOM'
            EXPORTING
              iv_idno            = ls_list_price-idno
              iv_version         = ls_list_price-version
              iv_uom_category    = 'RETAIL'
              iv_uom_code        = ls_list_price-uom_code
              iv_hybris_code     = space
              it_uom_variant     = fu_t_uom_variant[]
              it_gtin_var        = fu_t_gtin_var[]
            IMPORTING
              ev_lower_uom       = lv_lower_uom
              ev_lower_child_uom = lv_lower_child_uom
            EXCEPTIONS
              input_reqd         = 1
              OTHERS             = 2.

          CLEAR ls_reg_uom.
          READ TABLE lt_reg_uom[] INTO ls_reg_uom
          WITH KEY idno                 = ls_list_price-idno
                   pim_uom_code         = ls_list_price-uom_code
                   hybris_internal_code = space
                   lower_uom            = lv_lower_uom
                   lower_child_uom      = lv_lower_child_uom.
          IF sy-subrc = 0.

            IF ls_reg_uom-meinh IS INITIAL.

              CLEAR ls_gln_nr.
              ls_gln_nr-idno                 = ls_list_price-idno.
              ls_gln_nr-version              = ls_list_price-version.
              ls_gln_nr-gln                  = ls_list_price-gln.
              ls_gln_nr-uom_code             = ls_list_price-uom_code.
              ls_gln_nr-hybris_internal_code = space.
              ls_gln_nr-lower_uom            = lv_lower_uom.
              ls_gln_nr-lower_child_uom      = lv_lower_child_uom.
              ls_gln_nr-lifnr                = ls_vendor-lifnr.
              APPEND ls_gln_nr TO fc_t_gln_nr[].

            ELSE.
              CLEAR ls_marm.
              READ TABLE lt_marm[] TRANSPORTING NO FIELDS
              WITH KEY matnr = lv_matnr
                       meinh = ls_reg_uom-meinh.
              IF sy-subrc = 0.

                CLEAR ls_gln_nr.
                ls_gln_nr-idno                 = ls_list_price-idno.
                ls_gln_nr-version              = ls_list_price-version.
                ls_gln_nr-gln                  = ls_list_price-gln.
                ls_gln_nr-uom_code             = ls_list_price-uom_code.
                ls_gln_nr-hybris_internal_code = space.
                ls_gln_nr-lower_uom            = lv_lower_uom.
                ls_gln_nr-lower_child_uom      = lv_lower_child_uom.
                ls_gln_nr-lifnr                = ls_vendor-lifnr.
                APPEND ls_gln_nr TO fc_t_gln_nr[].

              ENDIF.  " READ TABLE lt_marm[] TRANSPORTING NO FIELDS
            ENDIF.  " IF ls_reg_uom-meinh IS INITIAL
          ENDIF.  "  READ TABLE lt_reg_uom[] INTO ls_reg_uom
        ENDIF.  " READ TABLE lt_eine[] INDEX 1 TRANSPORTING NO FIELDS
      ENDIF.  " READ TABLE lt_eina[] TRANSPORTING NO FIELDS
    ENDLOOP.  " LOOP AT lt_vendor[] INTO ls_vendor
*    ENDIF.  " READ TABLE lt_vendor[] INTO ls_vendor

  ENDLOOP.  " LOOP AT lt_list_price[] INTO ls_list_price

  SORT fc_t_gln_nr[] BY idno version gln uom_code.
  DELETE ADJACENT DUPLICATES FROM fc_t_gln_nr[] COMPARING idno version gln uom_code.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  GET_REG_PIR_GLN_NR
*&---------------------------------------------------------------------*
* Get GLN to be set to NR for REG_PIR
*----------------------------------------------------------------------*
FORM get_reg_pir_gln_nr USING fu_t_reg_pir_n TYPE ztarn_reg_pir
                              fu_t_reg_pir_o TYPE ztarn_reg_pir
                              fu_v_mode         TYPE char3
                     CHANGING fc_t_gln_nr    TYPE ty_t_gln_nr
                              fc_t_vendor_nr TYPE ty_t_vendor_nr.

  DATA: lt_reg_pir    TYPE ztarn_reg_pir,
        ls_reg_pir    TYPE zarn_reg_pir,
        lt_pir_vendor TYPE ty_t_pir_vendor,
        ls_pir_vendor TYPE ty_s_pir_vendor,
        lt_vendor     TYPE ty_t_pir_vendor,
        ls_vendor     TYPE ty_s_pir_vendor,
        lt_eina       TYPE STANDARD TABLE OF eina,
        ls_eina       TYPE eina,
        lt_eine       TYPE STANDARD TABLE OF eine,
        ls_eine       TYPE eine,
        ls_gln_nr     TYPE ty_s_gln_nr,
        ls_vendor_nr  TYPE ty_s_vendor_nr,

        lt_marm       TYPE marm_tab,
        ls_marm       TYPE marm,

        lv_ekorg_main TYPE ekorg,
        lv_matnr      TYPE matnr.


  CLEAR: fc_t_gln_nr[], fc_t_vendor_nr[].


  IF fu_v_mode = 'DEL'.
    APPEND LINES OF fu_t_reg_pir_o[] TO lt_reg_pir[].
  ELSE.
    APPEND LINES OF fu_t_reg_pir_n[] TO lt_reg_pir[].
  ENDIF.


  DELETE lt_reg_pir[] WHERE lifnr EQ space OR lifnr IS INITIAL.

  IF lt_reg_pir[] IS INITIAL.
    EXIT.
  ENDIF.



*  SORT lt_reg_pir[] BY idno order_uom_pim hybris_internal_code.
*  DELETE ADJACENT DUPLICATES FROM lt_reg_pir[]
*                COMPARING idno order_uom_pim hybris_internal_code.

* Get Vendor of GLNs where Vendor is MULTIPLE
* Break gln_no to get Vendor
  CLEAR lt_pir_vendor[].
  LOOP AT lt_reg_pir[] INTO ls_reg_pir
    WHERE lifnr EQ 'MULTIPLE'
       OR lifnr EQ space.
    CLEAR ls_pir_vendor.
    ls_pir_vendor-bbbnr = ls_reg_pir-gln_no+0(7).
    ls_pir_vendor-bbsnr = ls_reg_pir-gln_no+7(5).
    ls_pir_vendor-bubkz = ls_reg_pir-gln_no+12(1).
    INSERT ls_pir_vendor INTO TABLE lt_pir_vendor[].
  ENDLOOP.

  DELETE ADJACENT DUPLICATES FROM lt_pir_vendor[] COMPARING bbbnr bbsnr bubkz.




  CLEAR lt_vendor[].
  IF lt_pir_vendor[] IS NOT INITIAL.

* Get Vendor from gln_no
    SELECT lifnr bbbnr bbsnr bubkz   "#EC CI_SEL_NESTED
      INTO CORRESPONDING FIELDS OF TABLE lt_vendor[]
       FROM lfa1
      FOR ALL ENTRIES IN lt_pir_vendor[]
              WHERE bbbnr EQ lt_pir_vendor-bbbnr   "#EC CI_NOFIELD
                AND bbsnr EQ lt_pir_vendor-bbsnr
                AND bubkz EQ lt_pir_vendor-bubkz.
    IF sy-subrc = 0.
      DELETE lt_vendor[] WHERE lifnr EQ space.
    ENDIF.
  ENDIF.


* Get Vendor of GLNs where not MULTIPLE
  LOOP AT lt_reg_pir[] INTO ls_reg_pir
    WHERE lifnr NE 'MULTIPLE'
      AND lifnr NE space.
    CLEAR ls_vendor.
    ls_vendor-bbbnr = ls_reg_pir-gln_no+0(7).
    ls_vendor-bbsnr = ls_reg_pir-gln_no+7(5).
    ls_vendor-bubkz = ls_reg_pir-gln_no+12(1).
    ls_vendor-lifnr = ls_reg_pir-lifnr.
    INSERT ls_vendor INTO TABLE lt_vendor[].
  ENDLOOP.

  IF lt_vendor[] IS INITIAL.
    EXIT.
  ENDIF.


* Get IDNO
  CLEAR ls_reg_pir.
  READ TABLE lt_reg_pir[] INTO ls_reg_pir INDEX 1.

* Get Article from IDNO and Current Version from MARA
  CLEAR lv_matnr.
  SELECT SINGLE c~matnr INTO lv_matnr    "#EC CI_SEL_NESTED
    FROM zarn_ver_status AS a
    JOIN zarn_products AS b
    ON   a~idno = b~idno
    AND  a~current_ver = b~version
    JOIN mara AS c
    ON   b~fan_id = c~zzfan
    WHERE a~idno = ls_reg_pir-idno.

  IF lv_matnr IS INITIAL.
    EXIT.
  ENDIF.

* Get Main EKORG
  CLEAR lv_ekorg_main.
  SELECT SINGLE low FROM tvarvc
    INTO lv_ekorg_main
    WHERE name = 'ZARN_ARTICLE_POST_EINE_EKORG'
      AND type = 'S'
      AND high = abap_true.
  IF sy-subrc NE 0.
    lv_ekorg_main = '9999'.
  ENDIF.


  CLEAR: lt_eina[], lt_eine[].
  SELECT infnr matnr lifnr    "#EC CI_SEL_NESTED
    FROM eina
    INTO CORRESPONDING FIELDS OF TABLE lt_eina[]
    FOR ALL ENTRIES IN lt_vendor[]
  WHERE matnr = lv_matnr
    AND lifnr = lt_vendor-lifnr.
  IF sy-subrc = 0.
    SELECT infnr ekorg esokz werks bprme    "#EC CI_SEL_NESTED
      FROM eine
      INTO CORRESPONDING FIELDS OF TABLE lt_eine[]
       FOR ALL ENTRIES IN lt_eina[]
     WHERE infnr = lt_eina-infnr
       AND ekorg = lv_ekorg_main
       AND werks = space.
  ENDIF.


  CLEAR lt_marm[].
  SELECT matnr meinh    "#EC CI_SEL_NESTED
    FROM marm
    INTO CORRESPONDING FIELDS OF TABLE lt_marm[]
    WHERE matnr = lv_matnr.


* Get GLN_NOs to be set to NR from LP
  LOOP AT lt_reg_pir[] INTO ls_reg_pir
    WHERE lifnr EQ 'MULTIPLE'
       OR lifnr EQ space.
    CLEAR ls_pir_vendor.
    ls_pir_vendor-bbbnr = ls_reg_pir-gln_no+0(7).
    ls_pir_vendor-bbsnr = ls_reg_pir-gln_no+7(5).
    ls_pir_vendor-bubkz = ls_reg_pir-gln_no+12(1).

** Get Vendor from gln_no
*    CLEAR ls_vendor.
*    READ TABLE lt_vendor[] INTO ls_vendor
*    WITH TABLE KEY bbbnr = ls_pir_vendor-bbbnr
*                   bbsnr = ls_pir_vendor-bbsnr
*                   bubkz = ls_pir_vendor-bubkz.
*    IF sy-subrc = 0.

* Get Vendor from gln_no
    LOOP AT lt_vendor[] INTO ls_vendor
      WHERE bbbnr = ls_pir_vendor-bbbnr
        AND bbsnr = ls_pir_vendor-bbsnr
        AND bubkz = ls_pir_vendor-bubkz.


* If PIR already exist then get this gln_no to be set to NR from LP
      CLEAR ls_eina.
      READ TABLE lt_eina[] INTO ls_eina
      WITH KEY matnr = lv_matnr
               lifnr = ls_vendor-lifnr.
      IF sy-subrc EQ 0.
        CLEAR ls_eine.
        READ TABLE lt_eine[] INTO ls_eine
        WITH KEY infnr = ls_eina-infnr.
        IF sy-subrc EQ 0.

          IF ls_reg_pir-bprme IS INITIAL.

            CLEAR ls_gln_nr.
            ls_gln_nr-idno                 = ls_reg_pir-idno.
            ls_gln_nr-version              = space.
            ls_gln_nr-gln                  = ls_reg_pir-gln_no.
            ls_gln_nr-uom_code             = ls_reg_pir-order_uom_pim.
            ls_gln_nr-hybris_internal_code = ls_reg_pir-hybris_internal_code.
            ls_gln_nr-lower_uom            = ls_reg_pir-lower_uom.
            ls_gln_nr-lower_child_uom      = ls_reg_pir-lower_child_uom.
            ls_gln_nr-lifnr                = ls_vendor-lifnr.
            APPEND ls_gln_nr TO fc_t_gln_nr[].

          ELSE.

            CLEAR ls_marm.
            READ TABLE lt_marm[] TRANSPORTING NO FIELDS
            WITH KEY matnr = lv_matnr
                     meinh = ls_reg_pir-bprme.
            IF sy-subrc EQ 0.

              CLEAR ls_gln_nr.
              ls_gln_nr-idno                 = ls_reg_pir-idno.
              ls_gln_nr-version              = space.
              ls_gln_nr-gln                  = ls_reg_pir-gln_no.
              ls_gln_nr-uom_code             = ls_reg_pir-order_uom_pim.
              ls_gln_nr-hybris_internal_code = ls_reg_pir-hybris_internal_code.
              ls_gln_nr-lower_uom            = ls_reg_pir-lower_uom.
              ls_gln_nr-lower_child_uom      = ls_reg_pir-lower_child_uom.
              ls_gln_nr-lifnr                = ls_vendor-lifnr.
              APPEND ls_gln_nr TO fc_t_gln_nr[].

            ENDIF.  " READ TABLE lt_marm[] TRANSPORTING NO FIELDS
          ENDIF.  " IF ls_reg_pir-bprme IS INITIAL
        ENDIF.  " READ TABLE lt_eine[] INDEX 1 TRANSPORTING NO FIELDS
      ENDIF.  " READ TABLE lt_eina[] TRANSPORTING NO FIELDS
    ENDLOOP. " LOOP AT lt_vendor[] INTO ls_vendor
*    ENDIF.  " READ TABLE lt_vendor[] INTO ls_vendor
  ENDLOOP.  " LOOP AT lt_reg_pir[] INTO ls_reg_pir


* Get VENDORs to be set to NR from LP
  LOOP AT lt_reg_pir[] INTO ls_reg_pir
    WHERE lifnr NE 'MULTIPLE'
      AND lifnr NE space.

* If PIR already exist then get this gln_no to be set to NR from LP
    CLEAR ls_eina.
    READ TABLE lt_eina[] INTO ls_eina
    WITH KEY matnr = lv_matnr
             lifnr = ls_reg_pir-lifnr.
    IF sy-subrc EQ 0.
      CLEAR ls_eine.
      READ TABLE lt_eine[] INTO ls_eine
      WITH KEY infnr = ls_eina-infnr.
      IF sy-subrc EQ 0.

        IF ls_reg_pir-bprme IS INITIAL.

          CLEAR ls_vendor_nr.
          ls_vendor_nr-idno                 = ls_reg_pir-idno.
          ls_vendor_nr-version              = space.
          ls_vendor_nr-gln                  = ls_reg_pir-gln_no.
          ls_vendor_nr-uom_code             = ls_reg_pir-order_uom_pim.
          ls_vendor_nr-hybris_internal_code = ls_reg_pir-hybris_internal_code.
          ls_vendor_nr-lower_uom            = ls_reg_pir-lower_uom.
          ls_vendor_nr-lower_child_uom      = ls_reg_pir-lower_child_uom.
          ls_vendor_nr-lifnr                = ls_reg_pir-lifnr.
          APPEND ls_vendor_nr TO fc_t_vendor_nr[].

        ELSE.

          CLEAR ls_marm.
          READ TABLE lt_marm[] TRANSPORTING NO FIELDS
          WITH KEY matnr = lv_matnr
                   meinh = ls_reg_pir-bprme.
          IF sy-subrc EQ 0.

            CLEAR ls_vendor_nr.
            ls_vendor_nr-idno                 = ls_reg_pir-idno.
            ls_vendor_nr-version              = space.
            ls_vendor_nr-gln                  = ls_reg_pir-gln_no.
            ls_vendor_nr-uom_code             = ls_reg_pir-order_uom_pim.
            ls_vendor_nr-hybris_internal_code = ls_reg_pir-hybris_internal_code.
            ls_vendor_nr-lower_uom            = ls_reg_pir-lower_uom.
            ls_vendor_nr-lower_child_uom      = ls_reg_pir-lower_child_uom.
            ls_vendor_nr-lifnr                = ls_reg_pir-lifnr.
            APPEND ls_vendor_nr TO fc_t_vendor_nr[].

          ENDIF.  " READ TABLE lt_marm[] TRANSPORTING NO FIELDS
        ENDIF.  " IF ls_reg_pir-bprme IS INITIAL
      ENDIF.  " READ TABLE lt_eine[] INDEX 1 TRANSPORTING NO FIELDS
    ENDIF.  " READ TABLE lt_eina[] TRANSPORTING NO FIELDS

  ENDLOOP.  " LOOP AT lt_reg_pir[] INTO ls_reg_pir

  SORT fc_t_gln_nr[] BY idno version gln uom_code hybris_internal_code.
  DELETE ADJACENT DUPLICATES FROM fc_t_gln_nr[] COMPARING idno version gln uom_code hybris_internal_code.

  SORT fc_t_vendor_nr[] BY idno version gln uom_code hybris_internal_code.
  DELETE ADJACENT DUPLICATES FROM fc_t_vendor_nr[] COMPARING idno version gln uom_code hybris_internal_code.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  CONSTRUCT_WHERE_CLAUSE
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->IS_DATA  text
*      -->IT_EXCL_FIELD[]  text
*      <--EV_WHERE_CLAUSE  text
*----------------------------------------------------------------------*
FORM construct_where_clause  USING    is_data         TYPE any
                                      it_excl_field   TYPE ztmd_fname_range
                             CHANGING ev_where_clause TYPE string.


  DATA:
    lo_struct_descr TYPE REF TO cl_abap_structdescr.

  " Initialising because treating as export parameter
  CLEAR ev_where_clause.

  " Ensure flat structure provided as input
  DESCRIBE FIELD is_data TYPE DATA(lv_type) COMPONENTS DATA(lv_components).
  IF lv_components = 0
  OR lv_type      NE 'u'.
    RETURN.
  ENDIF.

  " Get the components of the structure
  lo_struct_descr ?= cl_abap_structdescr=>describe_by_data( is_data ).
  DATA(lt_component) = lo_struct_descr->components.

  " Construct clause
  DO.
    DATA(lv_index) = sy-index.

    ASSIGN COMPONENT lv_index
      OF STRUCTURE is_data TO FIELD-SYMBOL(<lv_fval>).
    IF sy-subrc IS NOT INITIAL.
      EXIT.
    ENDIF.
    READ TABLE lt_component INDEX lv_index
      INTO DATA(ls_component).
    IF sy-subrc IS NOT INITIAL.
      " Shouldn't really happen
      CONTINUE.
    ENDIF.

    " Exclude specified fields from clause
    IF ls_component-name IN it_excl_field
   AND it_excl_field[] IS NOT INITIAL.
      CONTINUE.
    ENDIF.

    IF ev_where_clause IS NOT INITIAL.
      ev_where_clause = |{ ev_where_clause } and |.
    ENDIF.
    ev_where_clause = |{ ev_where_clause }{ ls_component-name } = '{ <lv_fval> }'|.
  ENDDO.

ENDFORM.
