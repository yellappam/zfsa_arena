*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Functional Specification 3132
* DATE WRITTEN     # 07 03 2016
* SAP VERSION      # 731
* TYPE             # Report Program
* AUTHOR           # C90001448, Greg van der Loeff
*-----------------------------------------------------------------------
* TITLE            # AReNa: Send notifications for approvals
* PURPOSE          # AReNa: Send notifications for approvals
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------
*&---------------------------------------------------------------------*
*&  Include           ZRARN_APPROVALS_NOTIFY_DEF
*&---------------------------------------------------------------------*

TYPES: BEGIN OF ty_cpident,
         cpident TYPE cpident,
       END OF ty_cpident,
       ty_tab_cpident type sorted table of ty_cpident with unique key cpident,
       BEGIN OF ty_change,
         idno     TYPE zarn_cc_hdr-idno,
         chgid    TYPE zarn_cc_hdr-chgid,
         cpidents TYPE ty_tab_cpident,
       END OF ty_change,
       ty_tab_changes TYPE SORTED TABLE OF ty_change WITH UNIQUE KEY idno chgid,
       BEGIN OF ty_idno,
         idno    TYPE zarn_cc_hdr-idno,
         version TYPE zarn_cc_hdr-national_ver_id,
       END OF ty_idno,
       BEGIN OF ty_teamuser,
         team_code TYPE zarn_team_code,
         user      TYPE xubname,
       END OF ty_teamuser,
       ty_tab_team_users TYPE SORTED TABLE OF ty_teamuser WITH UNIQUE KEY team_code user,
       BEGIN OF ty_notif,
         team_code    TYPE zarn_team_code,
         user         TYPE xubname,
         idno         TYPE zarn_idno,
         chg_area     TYPE zarn_chg_area,
         chg_category TYPE zarn_chg_category,
       END OF ty_notif,
       ty_tab_notif TYPE SORTED TABLE OF ty_notif WITH UNIQUE KEY team_code user idno chg_area chg_category.
