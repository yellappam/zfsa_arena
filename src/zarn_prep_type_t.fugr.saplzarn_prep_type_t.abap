* regenerated at 25.05.2016 11:23:54 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_PREP_TYPE_TTOP.              " Global Data
  INCLUDE LZARN_PREP_TYPE_TUXX.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_PREP_TYPE_TF...              " Subroutines
* INCLUDE LZARN_PREP_TYPE_TO...              " PBO-Modules
* INCLUDE LZARN_PREP_TYPE_TI...              " PAI-Modules
* INCLUDE LZARN_PREP_TYPE_TE...              " Events
* INCLUDE LZARN_PREP_TYPE_TP...              " Local class implement.
* INCLUDE LZARN_PREP_TYPE_TT99.              " ABAP Unit tests
  INCLUDE LZARN_PREP_TYPE_TF00                    . " subprograms
  INCLUDE LZARN_PREP_TYPE_TI00                    . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
