class ZCL_IM_ARN_ARENA_POST definition
  public
  final
  create public .

public section.

  interfaces IF_EX_WRF_MAT_MAINTAINDATA .
protected section.
private section.

  class-data GV_BADI_NEW_MATERIAL type FLAG .
  constants GC_RETURN type STRING value '(SAPLWRF_MATERIAL_POSTING)G_T_RETURN'. "#EC NOTEXT
ENDCLASS.



CLASS ZCL_IM_ARN_ARENA_POST IMPLEMENTATION.


METHOD if_ex_wrf_mat_maintaindata~create_additional_data.
*-----------------------------------------------------------------------
* PROJECT          # PROGRAMME LIGHTNING - ONE Data Project
* SPECIFICATION    # AReNa Update ECC Article Tables
* DATE WRITTEN     # 18/05/2016
* TYPE             # Enhancement
* AUTHOR           # Jitin Kharbanda
*-----------------------------------------------------------------------
* TITLE            # AReNa Article Posting
* PURPOSE          # Create/Update Host SAP Product
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      # FM WRF_MATERIAL_MAINTAINDATA_RT
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------
  CONSTANTS: lc_via_arena  TYPE wrfcallmode  VALUE '14'.


  DATA: ls_import_ext    TYPE bapiparmatex,
        ls_host_sap_new  TYPE zarn_legacy_struct,
        lv_import_string TYPE string,
        lv_matnr         TYPE matnr.



  DATA:     gt_zmd_host_sap_itab TYPE  ztmd_host_sap,
            gs_zmd_host_sap_wa_n TYPE  zmd_host_sap.



*-Add entries to Table. As its always insert and unique record thus lockign is not required.
  CHECK flt_val EQ lc_via_arena.


* Set Flag for Article Sucessfully Posted - used in Article Posting FM
  CALL FUNCTION 'ZARN_SET_GET_ARTICLE_POST'
    EXPORTING
      iv_mode          = 'W'
      iv_arena         = abap_true
      iv_arena_success = abap_true.



  CLEAR: gt_zmd_host_sap_itab[].

  "Secondary check to check whether Article is changed using custom development
  LOOP AT it_importextension INTO ls_import_ext
    WHERE structure = 'ZARN_LEGACY_STRUCT'.


    lv_matnr = is_headdata-material.

    CLEAR ls_host_sap_new.

    ls_host_sap_new-structure = ls_import_ext-structure.
    ls_host_sap_new-material  = ls_import_ext-material.

*      lv_import_string = ls_import_ext.

    ls_host_sap_new-data      = ls_import_ext-field1+30.


    IF ls_host_sap_new-product IS INITIAL.
*--Get new product Code only if required otherwise skip.
      CALL FUNCTION 'ZMD_GET_HOST_PRODUCT_CODE'
        EXPORTING
          iv_matnr    = ls_import_ext-material
        IMPORTING
          ev_product  = ls_host_sap_new-product
        EXCEPTIONS
          error_found = 1
          OTHERS      = 2.
    ENDIF.

    IF ls_host_sap_new-product IS NOT INITIAL.

      CLEAR: gs_zmd_host_sap_wa_n.

      MOVE-CORRESPONDING ls_host_sap_new-data TO gs_zmd_host_sap_wa_n.
      gs_zmd_host_sap_wa_n-matnr = ls_host_sap_new-material.

      APPEND gs_zmd_host_sap_wa_n TO gt_zmd_host_sap_itab[].

    ENDIF.

  ENDLOOP.  " LOOP AT it_importextension INTO ls_import_ext



  IF gt_zmd_host_sap_itab[] IS NOT INITIAL.
*     Update mara change date information to kick-start SAVE
    CALL METHOD zcl_non_sap_article_master=>set_host_data
      EXPORTING
        it_host_sap_tab = gt_zmd_host_sap_itab[].

    CALL METHOD zcl_non_sap_article_master=>save_host_data.
  ENDIF.


ENDMETHOD.


method IF_EX_WRF_MAT_MAINTAINDATA~GET_EXCLUDED_FIELDS.
endmethod.


METHOD IF_EX_WRF_MAT_MAINTAINDATA~MODIFY_ADDITIONAL_DATA.
ENDMETHOD.


method IF_EX_WRF_MAT_MAINTAINDATA~MODIFY_BOM_DATA.
endmethod.


method IF_EX_WRF_MAT_MAINTAINDATA~MODIFY_CALCULATION_DATA.
endmethod.


method IF_EX_WRF_MAT_MAINTAINDATA~MODIFY_CONDITION_DATA.
endmethod.


method IF_EX_WRF_MAT_MAINTAINDATA~MODIFY_GENERAL_DATA.
endmethod.


method IF_EX_WRF_MAT_MAINTAINDATA~MODIFY_HIERARCHY_DATA.
endmethod.


METHOD if_ex_wrf_mat_maintaindata~modify_inforecord_data.
*-----------------------------------------------------------------------
* PROJECT          # PROGRAMME LIGHTNING - ONE Data Project
* SPECIFICATION    # AReNa Update ECC Article Tables
* DATE WRITTEN     # 18/05/2016
* TYPE             # Enhancement
* AUTHOR           # Jitin Kharbanda
*-----------------------------------------------------------------------
* TITLE            # AReNa Article Posting
* PURPOSE          # Maintain PIR Records
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      # FM WRF_MATERIAL_MAINTAINDATA_RT
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 13.12.2018
* CHANGE No.       # WSIWJ-143, CHARM: 9000004665
* DESCRIPTION      # LNI DC CUTOVER - LNI Stores DC moving from external vendor
*                  # to DC. Required to support interim state where only some
*                  # Merchandising Categories have moved over
* WHO              # C90005557, Tessa Newman
*-----------------------------------------------------------------------

  CONSTANTS: lc_via_arena         TYPE wrfcallmode  VALUE '14'.

  DATA: ls_import_ext  TYPE bapiparmatex,
        lt_matnr       TYPE ztmd_matnr_str,
        ls_matnr       LIKE LINE OF lt_matnr,
        lt_return      TYPE bapiret2_t,
        lv_bolt_switch TYPE xfeld,
        ls_eina_new    TYPE eina,
        ls_eine_new    TYPE eine,
        lv_field_name  TYPE string,
        ls_ret2        LIKE LINE OF lt_return,
        ls_return      TYPE bapireturn1,

        lt_meinh       TYPE STANDARD TABLE OF smeinh_f,
        ls_meinh       TYPE smeinh_f,
        lv_f1          TYPE f,
        lv_arena       TYPE char1.


  FIELD-SYMBOLS: <return_msg_tab> TYPE wrfmb_t_return.



*-Add entries to Table. As its always insert and unique crecord thus lockign is not required.
  CHECK flt_val EQ lc_via_arena.



  CLEAR: ls_eina_new, ls_eine_new.


  ls_eina_new = ct_eina_new.
  ls_eine_new = ct_eine_new.

  IF ct_eine_new-infnr IS INITIAL.
    "do clear only when creating new Inforecord (= OLD is initial)
    CLEAR ct_eine_new-xersn.
    CLEAR ct_eine_new-mhdrz.
    CLEAR ct_eine_new-uebtk.   "clear unlimited delivery tolerance
  ENDIF.

* Get Flag for Article Posting for AReNa
  CLEAR lv_arena.
  CALL FUNCTION 'ZARN_SET_GET_ARTICLE_POST'
    EXPORTING
      iv_mode  = 'R'
    IMPORTING
      ev_arena = lv_arena.


  IF lv_arena EQ abap_true.

**** Update new EINA
* Read MARM for al units maintained
    CALL METHOD cl_material_read_all=>read_marm
      EXPORTING
        i_material = ct_eina_new-matnr
      IMPORTING
        et_smeinh  = lt_meinh.

* Update Numerator and Denominator for Conversion of Order Unit to Base Unit
    READ TABLE lt_meinh INTO ls_meinh WITH KEY meinh =  ct_eina_new-meins.
    IF sy-subrc = 0.
      ct_eina_new-umrez = ls_meinh-umrez.
      ct_eina_new-umren = ls_meinh-umren.
    ENDIF.


    IF it_eina_old IS NOT INITIAL.
      ct_eina_new-erdat = it_eina_old-erdat.
      ct_eina_new-ernam = it_eina_old-ernam.
    ENDIF.

    IF it_eine_old IS NOT INITIAL.
      ct_eine_new-erdat = it_eine_old-erdat.
      ct_eine_new-ernam = it_eine_old-ernam.
    ENDIF.

* Set Flag for Article Posting as TRUE
    CALL FUNCTION 'ZARN_SET_GET_ARTICLE_POST'
      EXPORTING
        iv_mode     = 'W'
        iv_arena    = abap_true
        is_eina_new = ct_eina_new
        is_eine_new = ct_eine_new
        is_eina_old = it_eina_old
        is_eine_old = it_eine_old.


  ENDIF.  " IF lv_arena EQ abap_true

  CLEAR:  ls_ret2, ls_return.
  CLEAR lv_bolt_switch.
  CALL FUNCTION 'Z_MM_GET_LNI_SWITCH'
    IMPORTING
      ev_lni_switch = lv_bolt_switch.

  IF lv_bolt_switch EQ abap_true.

    "call creation/deletion of PIR for dummy vendor
    REFRESH lt_matnr.
    ls_matnr-matnr = ct_eina_new-matnr.
    APPEND ls_matnr TO lt_matnr.

    CALL FUNCTION 'ZMD_MAINT_STORE_PIR_LNI'
      EXPORTING
        it_matnr      = lt_matnr
      IMPORTING
        et_return     = lt_return
      EXCEPTIONS
        error_message = 1.
    IF sy-subrc NE 0.
      ls_return-id     = sy-msgid.
      ls_return-number = sy-msgno.
      ls_return-type   = sy-msgty.
      ls_return-message_v1 = sy-msgv1.
      ls_return-message_v2 = sy-msgv2.
      ls_return-message_v3 = sy-msgv3.
      ls_return-message_v4 = sy-msgv4.
    ELSE.
      READ TABLE lt_return INDEX 1 INTO ls_ret2.
      MOVE-CORRESPONDING ls_ret2 TO ls_return.
    ENDIF.

    "append message to log of Pricat Termination
    IF ls_return-number IS NOT INITIAL.
      lv_field_name = gc_return.
      ASSIGN (lv_field_name) TO <return_msg_tab>.
      IF sy-subrc EQ 0.
        APPEND ls_return TO <return_msg_tab>.
        UNASSIGN <return_msg_tab>.
      ENDIF.
    ENDIF.

  ENDIF.  " IF lv_bolt_switch EQ abap_true



ENDMETHOD.


method IF_EX_WRF_MAT_MAINTAINDATA~MODIFY_LISTING_DATA.
endmethod.


method IF_EX_WRF_MAT_MAINTAINDATA~MODIFY_REPLENISHMENT_DATA.
endmethod.


method IF_EX_WRF_MAT_MAINTAINDATA~MODIFY_REQUIREMENT_GROUP_DATA.
endmethod.


method IF_EX_WRF_MAT_MAINTAINDATA~MODIFY_SOURCE_LIST_DATA.
endmethod.


method IF_EX_WRF_MAT_MAINTAINDATA~MODIFY_VENDOR_CHARACTERISTICS.
endmethod.
ENDCLASS.
