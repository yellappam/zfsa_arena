* regenerated at 03.03.2017 09:57:14
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_FIELD_CONFGTOP.              " Global Data
  INCLUDE LZARN_FIELD_CONFGUXX.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_FIELD_CONFGF...              " Subroutines
* INCLUDE LZARN_FIELD_CONFGO...              " PBO-Modules
* INCLUDE LZARN_FIELD_CONFGI...              " PAI-Modules
* INCLUDE LZARN_FIELD_CONFGE...              " Events
* INCLUDE LZARN_FIELD_CONFGP...              " Local class implement.
* INCLUDE LZARN_FIELD_CONFGT99.              " ABAP Unit tests
  INCLUDE LZARN_FIELD_CONFGF00                    . " subprograms
  INCLUDE LZARN_FIELD_CONFGI00                    . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
