*-----------------------------------------------------------------------
* PROJECT          # AReNa
* SPECIFICATION    #
* DATE WRITTEN     # 26.07.2016
* SYSTEM           # DE0
* SAP VERSION      # ECC6
* TYPE             # Conversion Exit
* AUTHOR           # C90003202, Howard Chow
*-----------------------------------------------------------------------
* PURPOSE          # Conversion exit output for domain ZARN_DEC21_14_D
*                    - remove not meaningful zero after last none zero
*                      in the fractional part
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------
FUNCTION CONVERSION_EXIT_ZDC21_OUTPUT.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(INPUT)
*"  EXPORTING
*"     REFERENCE(OUTPUT)
*"----------------------------------------------------------------------

  DATA:
    lv_str      TYPE string,
    lv_char14   TYPE c LENGTH 14.


  lv_str = input.

* split the integer part and fractional part at the decimal point
  SPLIT lv_str AT '.' INTO lv_str lv_char14.
* remove not meaningful zero after last none zero
  SHIFT lv_char14 RIGHT DELETING TRAILING '0'.
* shift value to left again
  SHIFT lv_char14 LEFT DELETING LEADING space.
* make sure there are at least 3 decimal places
  OVERLAY lv_char14 WITH gc_zero_decimal.
* concatenate back with interger part
  lv_str = |{ lv_str }.{ lv_char14 }|.

  output = lv_str.


ENDFUNCTION.
