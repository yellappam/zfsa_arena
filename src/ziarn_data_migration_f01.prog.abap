*&---------------------------------------------------------------------*
*&  Include          ZIARN_DATA_MIGRATION_F01
*&---------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*&      Form  GET_OLD_DATA
*&---------------------------------------------------------------------*
FORM get_old_data.

  REFRESH: lt_zarn_net_qty,
           lt_zarn_serve_size,
           lt_zarn_bioorganism,
           lt_zarn_products,
           lt_zarn_nutrients,
           lt_zarn_organism,
           lt_log.
  "Counts records that already exist
  lv_existcount = 0.

  IF r_qty = abap_true.
    SELECT idno version
  INTO CORRESPONDING FIELDS OF  TABLE lt_zarn_net_qty
  FROM zarn_net_qty
  WHERE idno IN s_idno
  ORDER BY idno ASCENDING version DESCENDING.

    SELECT idno version net_quantity net_quantity_uom
      INTO CORRESPONDING FIELDS OF TABLE lt_zarn_products " PACKAGE SIZE 20000
      FROM zarn_products
      WHERE idno  IN s_idno
      ORDER BY idno ASCENDING version DESCENDING.

    "Filter data already existing and latest version
    LOOP AT lt_zarn_products INTO ls_zarn_products.
      lv_index = sy-tabix.
      READ TABLE lt_zarn_net_qty TRANSPORTING NO FIELDS WITH KEY idno    = ls_zarn_products-idno
                                                                 version = ls_zarn_products-version.
      IF sy-subrc = 0.
        ADD 1 TO lv_existcount.
        ls_log-msgty = 'W'.
        lv_idno_char = |{ ls_zarn_products-idno ALPHA = IN } |.
        CONCATENATE lv_idno_char  TEXT-011 INTO ls_log-message SEPARATED BY space.
        APPEND ls_log TO lt_log.
        DELETE lt_zarn_products[] WHERE idno = ls_zarn_products-idno.
      ELSE.
        DELETE lt_zarn_products[] WHERE idno = ls_zarn_products-idno AND version NE ls_zarn_products-version.
      ENDIF.
    ENDLOOP.

  ELSEIF r_srv = abap_true.

    SELECT idno version
      INTO CORRESPONDING FIELDS OF TABLE lt_zarn_serve_size
      FROM zarn_serve_size
      WHERE idno IN s_idno
      ORDER BY idno ASCENDING version DESCENDING.

    SELECT idno version nutrient_type ni_serving_size ni_serving_size_uom
      INTO CORRESPONDING FIELDS OF TABLE lt_zarn_nutrients
      FROM zarn_nutrients
      WHERE idno IN s_idno
      ORDER BY idno ASCENDING version DESCENDING.

    "Filter data already existing and latest version
    LOOP AT lt_zarn_nutrients INTO ls_zarn_nutrients.
      lv_index = sy-tabix.
      READ TABLE lt_zarn_serve_size TRANSPORTING NO FIELDS WITH KEY idno = ls_zarn_nutrients-idno
                                                                 version = ls_zarn_nutrients-version.
      IF sy-subrc = 0.
        ADD 1 TO lv_existcount.
        ls_log-msgty = 'W'.
        lv_idno_char = |{ ls_zarn_nutrients-idno ALPHA = IN } |.
        CONCATENATE lv_idno_char  TEXT-011  INTO ls_log-message SEPARATED BY space.
        APPEND ls_log TO lt_log.
        DELETE lt_zarn_nutrients[] WHERE idno = ls_zarn_nutrients-idno. " INDEX lv_index.
      ELSE.
        DELETE lt_zarn_nutrients[] WHERE idno = ls_zarn_nutrients-idno AND version NE ls_zarn_nutrients-version.
      ENDIF.
    ENDLOOP.

  ELSEIF r_bio = abap_true.
    SELECT idno version
      INTO CORRESPONDING FIELDS OF TABLE lt_zarn_bioorganism
      FROM zarn_bioorganism
      WHERE idno IN s_idno
    ORDER BY idno ASCENDING version DESCENDING.

    SELECT idno version organism_type max_value organism_uom
      INTO CORRESPONDING FIELDS OF TABLE lt_zarn_organism
      FROM zarn_organism
      WHERE idno IN s_idno
      ORDER BY idno ASCENDING version DESCENDING.

    "Filter data already existing and latest version
    LOOP AT lt_zarn_organism INTO ls_zarn_organism.
      lv_index = sy-tabix.
      READ TABLE lt_zarn_bioorganism TRANSPORTING NO FIELDS WITH KEY idno = ls_zarn_organism-idno
                                                                  version = ls_zarn_organism-version.
      IF sy-subrc = 0.
        ADD 1 TO lv_existcount.
        ls_log-msgty = 'W'.
        lv_idno_char =  |{ ls_zarn_organism-idno ALPHA = IN } |.
        CONCATENATE lv_idno_char TEXT-011 INTO ls_log-message SEPARATED BY space.
        APPEND ls_log TO lt_log.
        DELETE lt_zarn_organism[] WHERE idno = ls_zarn_organism-idno. "  INDEX lv_index.
      ELSE.
        DELETE lt_zarn_organism[] WHERE idno = ls_zarn_organism-idno AND version NE ls_zarn_organism-version.
      ENDIF.
    ENDLOOP.
  ENDIF.

ENDFORM.                    " GET_OLD_DATA

*&---------------------------------------------------------------------*
*&      Form  BUILD_UPDATE_DATA
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
FORM build_update_data .

  REFRESH: lt_zarn_net_qty,
           lt_zarn_serve_size,
           lt_zarn_bioorganism.

  "All tables have latest version only
  IF lt_zarn_products[] IS NOT INITIAL.
    LOOP AT lt_zarn_products INTO ls_zarn_products.
      TRY.
          ls_zarn_net_qty-net_qty_id = cl_system_uuid=>create_uuid_x16_static( ).
        CATCH cx_uuid_error INTO DATA(lv_uuid_error).
      ENDTRY.
      ls_zarn_net_qty-idno = ls_zarn_products-idno.
      ls_zarn_net_qty-version = ls_zarn_products-version.
      ls_zarn_net_qty-quantity = ls_zarn_products-net_quantity.
      ls_zarn_net_qty-uom = ls_zarn_products-net_quantity_uom.
      IF lv_uuid_error IS INITIAL.
        APPEND ls_zarn_net_qty TO lt_zarn_net_qty.
        CLEAR ls_zarn_net_qty.
      ENDIF.
      CLEAR lv_uuid_error.
    ENDLOOP.
  ENDIF.

  IF lt_zarn_nutrients[] IS NOT INITIAL.
    LOOP AT lt_zarn_nutrients INTO ls_zarn_nutrients.

      TRY.
          ls_zarn_serve_size-serve_size_id = cl_system_uuid=>create_uuid_x16_static( ).
        CATCH cx_uuid_error INTO lv_uuid_error.
      ENDTRY.
      ls_zarn_serve_size-idno = ls_zarn_nutrients-idno.
      ls_zarn_serve_size-version = ls_zarn_nutrients-version.
      ls_zarn_serve_size-nutrient_type = ls_zarn_nutrients-nutrient_type.
      ls_zarn_serve_size-serve_size = ls_zarn_nutrients-ni_serving_size.
      ls_zarn_serve_size-uom = ls_zarn_nutrients-ni_serving_size_uom.
      IF lv_uuid_error IS INITIAL.
        APPEND ls_zarn_serve_size TO lt_zarn_serve_size.
        CLEAR ls_zarn_serve_size.
      ENDIF.
      CLEAR lv_uuid_error.
    ENDLOOP.
  ENDIF.

  IF lt_zarn_organism[] IS NOT INITIAL.
    LOOP AT lt_zarn_organism INTO ls_zarn_organism.
      TRY.
          ls_zarn_bioorganism-bioorganism_id = cl_system_uuid=>create_uuid_x16_static( ).
        CATCH cx_uuid_error INTO lv_uuid_error.
      ENDTRY.
      ls_zarn_bioorganism-idno = ls_zarn_organism-idno.
      ls_zarn_bioorganism-version = ls_zarn_organism-version.
      ls_zarn_bioorganism-type = ls_zarn_organism-organism_type.
      ls_zarn_bioorganism-max_value = ls_zarn_organism-max_value.
      ls_zarn_bioorganism-uom = ls_zarn_organism-organism_uom.
      IF lv_uuid_error IS INITIAL.
        APPEND ls_zarn_bioorganism TO lt_zarn_bioorganism.
        CLEAR ls_zarn_bioorganism.
      ENDIF.
      CLEAR lv_uuid_error.
    ENDLOOP.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  UPDATE_DB
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
FORM update_db .

  lv_updatecount = 0.
  lv_failcount   = 0.
  CLEAR lv_idno.
  IF r_qty = abap_true.
    IF lt_zarn_net_qty[] IS NOT INITIAL.
      REFRESH lt_zarn_net_qty_tmp.
      DESCRIBE TABLE lt_zarn_net_qty LINES DATA(lv_total_lines).
      lv_packsize = 0.
      lv_line_counter = 0.
      LOOP AT lt_zarn_net_qty INTO ls_zarn_net_qty.
        ADD 1 TO lv_packsize. ADD 1 TO lv_line_counter.
        lv_index = sy-tabix.
        APPEND ls_zarn_net_qty TO lt_zarn_net_qty_tmp.
        IF ( lv_packsize = p_pksize OR lv_line_counter = lv_total_lines ).
          INSERT zarn_net_qty FROM TABLE lt_zarn_net_qty_tmp[] ACCEPTING DUPLICATE KEYS."#EC CI_IMUD_NESTED
          IF sy-subrc = 0.
            ADD sy-dbcnt TO lv_updatecount.
            lv_failcount = lv_failcount + ( lv_packsize - lv_updatecount ).
            IF p_test = abap_false.
              CALL FUNCTION 'DB_COMMIT'
                EXPORTING
                  iv_default = abap_false.
            ENDIF.
          ENDIF.
          lv_packsize = 0.
          REFRESH lt_zarn_net_qty_tmp.
        ENDIF.
      ENDLOOP.
    ELSE.
      ls_log-msgty = 'E'.
      ls_log-message = TEXT-012.
      APPEND ls_log TO lt_log.
    ENDIF.
  ELSEIF r_srv = abap_true.
    IF lt_zarn_serve_size[] IS NOT INITIAL.
      DESCRIBE TABLE lt_zarn_serve_size LINES lv_total_lines.
      REFRESH lt_zarn_serve_size_tmp.
      lv_packsize = 0.
      lv_line_counter = 0.
      LOOP AT lt_zarn_serve_size INTO ls_zarn_serve_size.
        ADD 1 TO lv_packsize. ADD 1 TO lv_line_counter.
        lv_index = sy-tabix.
        APPEND ls_zarn_serve_size TO lt_zarn_serve_size_tmp.
        IF ( lv_packsize = p_pksize OR lv_line_counter = lv_total_lines ).
          INSERT zarn_serve_size FROM TABLE lt_zarn_serve_size_tmp[] ACCEPTING DUPLICATE KEYS."#EC CI_IMUD_NESTED
          IF sy-subrc = 0.
            ADD sy-dbcnt TO lv_updatecount.
            lv_failcount = lv_failcount + ( lv_packsize - lv_updatecount ).
            IF p_test = abap_false.
              CALL FUNCTION 'DB_COMMIT'
                EXPORTING
                  iv_default = abap_false.
            ENDIF.
          ENDIF.
          lv_packsize = 0.
          REFRESH lt_zarn_serve_size_tmp.
        ENDIF.
      ENDLOOP.
    ELSE.
      ls_log-msgty = 'E'.
      ls_log-message = TEXT-012.
      APPEND ls_log TO lt_log.
    ENDIF.
  ELSEIF r_bio = abap_true.
    IF lt_zarn_bioorganism[] IS NOT INITIAL.
      DESCRIBE TABLE lt_zarn_bioorganism LINES lv_total_lines.
      REFRESH lt_zarn_bioorganism_tmp.
      lv_packsize = 0.
      lv_line_counter = 0.
      LOOP AT lt_zarn_bioorganism INTO ls_zarn_bioorganism.
        ADD 1 TO lv_packsize. ADD 1 TO lv_line_counter.
        lv_index = sy-tabix.
        APPEND ls_zarn_bioorganism TO lt_zarn_bioorganism_tmp.
        IF ( lv_packsize = p_pksize OR lv_line_counter = lv_total_lines ).
          INSERT zarn_bioorganism FROM TABLE lt_zarn_bioorganism_tmp[] ACCEPTING DUPLICATE KEYS."#EC CI_IMUD_NESTED
          IF sy-subrc = 0.
            ADD sy-dbcnt TO lv_updatecount.
            lv_failcount = lv_failcount + ( lv_packsize - lv_updatecount ).
            IF p_test = abap_false.
              CALL FUNCTION 'DB_COMMIT'
                EXPORTING
                  iv_default = abap_false.
            ENDIF.
          ENDIF.
          lv_packsize = 0.
          REFRESH lt_zarn_bioorganism_tmp.
        ENDIF.
      ENDLOOP.
    ELSE.
      ls_log-msgty   = 'E'.
      ls_log-message = TEXT-012.
      APPEND ls_log TO lt_log.
    ENDIF.
  ENDIF.

  IF p_test = abap_true.
    ROLLBACK WORK.
  ENDIF.

ENDFORM.

*&---------------------------------------------------------------------*
*&      Form  WRITE_LOG
*&---------------------------------------------------------------------*
FORM write_log .

  WRITE: 5 sy-datum, sy-uname.
  WRITE: / , sy-uline+1(114).
  WRITE: /5 'Updated Records:',          32 lv_updatecount.
  WRITE: /5 'Failed Records:',           32 lv_failcount.
  WRITE: /5 'Records Already Existing:', 32 lv_existcount.
  WRITE: / , sy-uline+1(114).

  LOOP AT lt_log[] INTO ls_log.
    IF ls_log-msgty = 'E'.
      WRITE: /5 ls_log-msgty COLOR 6, 15 ls_log-message COLOR 6.
    ELSEIF ls_log-msgty = 'S'.
      WRITE: /5 ls_log-msgty COLOR 5, 15 ls_log-message COLOR 5.
    ELSEIF ls_log-msgty = 'W'.
      WRITE: /5 ls_log-msgty COLOR 3, 15 ls_log-message COLOR 3.
    ENDIF.
  ENDLOOP.

ENDFORM.
