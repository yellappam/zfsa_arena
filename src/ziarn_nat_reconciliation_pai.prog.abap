*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_NAT_RECONCILIATION_PAI
*&---------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*&      Module  USER_COMMAND_9000  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE user_command_9000 INPUT.

  CALL METHOD cl_gui_cfw=>flush.

  CASE sy-ucomm.
*   Back
    WHEN zcl_constants=>gc_ucomm_back.
      LEAVE TO SCREEN 0.

*   Exit
    WHEN zcl_constants=>gc_ucomm_exit.
      LEAVE TO SCREEN 0.

*   Cancel
    WHEN zcl_constants=>gc_ucomm_cancel.
      LEAVE PROGRAM.

  ENDCASE.
ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  TS_NAT_RECO_ACTIVE_TAB_GET  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE ts_nat_reco_active_tab_get INPUT.
  ok_code = sy-ucomm.
  CASE ok_code.
    WHEN c_ts_nat_reco-tab1.
      g_ts_nat_reco-pressed_tab = c_ts_nat_reco-tab1.
    WHEN c_ts_nat_reco-tab2.
      g_ts_nat_reco-pressed_tab = c_ts_nat_reco-tab2.
    WHEN OTHERS.
      g_ts_nat_reco-pressed_tab = ts_nat_reco-activetab.
  ENDCASE.
ENDMODULE.
