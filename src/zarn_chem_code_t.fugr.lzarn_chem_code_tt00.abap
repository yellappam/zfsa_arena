*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 02.05.2016 at 14:18:56 by user C90001363
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_CHEM_CODE_T................................*
DATA:  BEGIN OF STATUS_ZARN_CHEM_CODE_T              .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_CHEM_CODE_T              .
CONTROLS: TCTRL_ZARN_CHEM_CODE_T
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_CHEM_CODE_T              .
TABLES: ZARN_CHEM_CODE_T               .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
