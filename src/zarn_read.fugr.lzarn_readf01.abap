*----------------------------------------------------------------------*
***INCLUDE LZARN_READF01 .
*----------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*&      Form  GET_CHANGE_DOCUMENT_DATA
*&---------------------------------------------------------------------*
* Get Change Document Data
*----------------------------------------------------------------------*
FORM get_change_document_data USING fu_v_reg        TYPE xfeld
                                    fu_v_chg        TYPE xfeld
                                    fu_r_idno_id    TYPE ztarn_cdobjectv_range
                                    fu_r_chgid_id   TYPE ztarn_cdobjectv_range
                                    fu_r_changenr	  TYPE ztarn_changenr_range
                                    fu_r_udate      TYPE ztarn_udate_range
                                    fu_r_username	  TYPE ztarn_username_range
                           CHANGING fc_t_chg_doc    TYPE ty_t_chg_doc
                                    fc_t_cdpos_uid  TYPE ty_t_cdpos_uid
                                    fc_t_cc_hdr     TYPE ztarn_cc_hdr
                                    fc_t_cc_det     TYPE ztarn_cc_det.

  DATA: lt_cdhdr        TYPE SORTED TABLE OF cdhdr
                                WITH UNIQUE KEY objectclas objectid changenr,
        ls_cdhdr        TYPE cdhdr,
        lt_cdpos        TYPE SORTED TABLE OF cdpos
                                WITH UNIQUE KEY objectclas objectid changenr tabname tabkey fname chngind,
        ls_cdpos        TYPE cdpos,
        lt_chg_doc      TYPE ty_t_chg_doc,
        ls_chg_doc      TYPE ty_s_chg_doc,

        lt_keyguid      TYPE ty_t_keyguid,
        ls_keyguid      TYPE ty_s_keyguid,

        ls_cc_hdr_key   TYPE ty_s_cc_hdr_key,
        ls_cc_det_key   TYPE ty_s_cc_det_key,
        ls_cdpos_uid    TYPE ty_s_cdpos_uid,
        lt_cc_hdr       TYPE ztarn_cc_hdr,
        ls_cc_hdr       TYPE zarn_cc_hdr,
        lt_cc_det       TYPE ztarn_cc_det,
        ls_cc_det       TYPE zarn_cc_det,
        ls_user_address TYPE addr3_val.


  CLEAR: fc_t_chg_doc[], lt_chg_doc[], fc_t_cdpos_uid[], fc_t_cc_hdr[], fc_t_cc_det[].


  CLEAR lt_cdhdr[].
  IF fu_v_reg = abap_true.
* Get Change Document Header
    SELECT objectclas objectid changenr username udate utime tcode
      INTO CORRESPONDING FIELDS OF TABLE lt_cdhdr[]
      FROM cdhdr
      WHERE objectclas EQ 'ZARN_CTRL_REGNL'
        AND objectid   IN fu_r_idno_id[]
        AND changenr   IN fu_r_changenr[]
        AND udate      IN fu_r_udate[]
        AND username   IN fu_r_username[].
  ELSEIF fu_v_chg = abap_true.
* Get Change Document Header
    SELECT objectclas objectid changenr username udate utime tcode
      INTO CORRESPONDING FIELDS OF TABLE lt_cdhdr[]
      FROM cdhdr
      WHERE objectclas EQ 'ZARN_CHG_CAT'
        AND objectid   IN fu_r_chgid_id[]
        AND changenr   IN fu_r_changenr[]
        AND udate      IN fu_r_udate[]
        AND username   IN fu_r_username[].
  ENDIF.




  IF lt_cdhdr[] IS NOT INITIAL.
* Get Change Document Details
    CLEAR: lt_cdpos[].
    SELECT objectclas objectid changenr tabname tabkey fname chngind
           value_new value_old
      INTO CORRESPONDING FIELDS OF TABLE lt_cdpos[]
    FROM cdpos
      FOR ALL ENTRIES IN lt_cdhdr[]
      WHERE objectclas EQ lt_cdhdr-objectclas
        AND objectid   EQ lt_cdhdr-objectid
        AND changenr   EQ lt_cdhdr-changenr.
    IF sy-subrc = 0.

      CLEAR: lt_keyguid[], lt_cc_hdr[], lt_cc_det[].

      LOOP AT lt_cdpos INTO ls_cdpos.
        CLEAR ls_chg_doc.
        ls_chg_doc-objectclas = ls_cdpos-objectclas.
        ls_chg_doc-objectid   = ls_cdpos-objectid.
        ls_chg_doc-changenr   = ls_cdpos-changenr.
        ls_chg_doc-tabname    = ls_cdpos-tabname.
        ls_chg_doc-tabkey     = ls_cdpos-tabkey.
        ls_chg_doc-fname      = ls_cdpos-fname.
        ls_chg_doc-chngind    = ls_cdpos-chngind.
        ls_chg_doc-value_new  = ls_cdpos-value_new.
        ls_chg_doc-value_old  = ls_cdpos-value_old.

        CLEAR ls_cdhdr.
        READ TABLE lt_cdhdr INTO ls_cdhdr
        WITH TABLE KEY objectclas = ls_cdpos-objectclas
                       objectid   = ls_cdpos-objectid
                       changenr   = ls_cdpos-changenr.
        IF sy-subrc = 0.
          ls_chg_doc-userid     = ls_cdhdr-username.
          ls_chg_doc-udate      = ls_cdhdr-udate.
          ls_chg_doc-utime      = ls_cdhdr-utime.
          ls_chg_doc-tcode      = ls_cdhdr-tcode.

          CLEAR ls_user_address.
          CALL FUNCTION 'SUSR_USER_ADDRESS_READ'
            EXPORTING
              user_name              = ls_cdhdr-username
            IMPORTING
              user_address           = ls_user_address
            EXCEPTIONS
              user_address_not_found = 1
              OTHERS                 = 2.
          IF sy-subrc EQ 0.
            ls_chg_doc-username = ls_user_address-name_text.
          ELSE.
            CLEAR ls_chg_doc-username.
          ENDIF.

        ENDIF.

        INSERT ls_chg_doc INTO TABLE lt_chg_doc[].


* GUID for longer tabkeys
        IF ls_cdpos-tabname = 'ZARN_REG_UOM' OR
           ls_cdpos-tabname = 'ZARN_CONTROL' OR
           ls_cdpos-tabname = 'ZARN_CC_DET'  OR
           ls_cdpos-tabname = 'ZARN_REG_PIR'.
          CLEAR ls_keyguid.
          ls_keyguid-keyguid = ls_cdpos-tabkey.
          APPEND ls_keyguid TO lt_keyguid[].
        ENDIF.


* For Getting Versions for CC_HDR table
        IF ls_cdpos-tabname = 'ZARN_CC_HDR'.
          CLEAR ls_cc_hdr_key.
          ls_cc_hdr_key = ls_cdpos-tabkey.

          CLEAR ls_cc_hdr.
          MOVE-CORRESPONDING ls_cc_hdr_key TO ls_cc_hdr.
          APPEND ls_cc_hdr TO lt_cc_hdr[].
        ENDIF.


      ENDLOOP.  " LOOP AT lt_cdpos INTO ls_cdpos


    ENDIF.  " IF sy-subrc = 0  - CDPOS
  ENDIF.  " IF sy-subrc = 0  - CDHDR

  SORT lt_chg_doc[] BY objectclas objectid changenr.

  fc_t_chg_doc[] = lt_chg_doc[].


  IF lt_keyguid[] IS NOT INITIAL.
    SELECT keyguid objectclas objectid changenr tabname tabkey
      INTO CORRESPONDING FIELDS OF TABLE fc_t_cdpos_uid[]
      FROM cdpos_uid
      FOR ALL ENTRIES IN lt_keyguid[]
      WHERE keyguid = lt_keyguid-keyguid.
    IF sy-subrc = 0.

      LOOP AT fc_t_cdpos_uid[] INTO ls_cdpos_uid.
* For Getting Versions and Old/New Values for CC_DET table
        IF ls_cdpos_uid-tabname = 'ZARN_CC_DET'.
          CLEAR ls_cc_det_key.
          ls_cc_det_key = ls_cdpos_uid-tabkey.

          CLEAR ls_cc_det.
          MOVE-CORRESPONDING ls_cc_det_key TO ls_cc_det.
          APPEND ls_cc_det TO lt_cc_det[].
        ENDIF.
      ENDLOOP.  " LOOP AT fc_t_cdpos_uid[] INTO ls_cdpos_uid
    ENDIF.  " IF sy-subrc = 0

  ENDIF.



  IF lt_cc_hdr[] IS NOT INITIAL.
    SELECT * FROM zarn_cc_hdr
      INTO CORRESPONDING FIELDS OF TABLE fc_t_cc_hdr[]
      FOR ALL ENTRIES IN lt_cc_hdr[]
      WHERE chgid        = lt_cc_hdr-chgid
        AND idno         = lt_cc_hdr-idno
        AND chg_category = lt_cc_hdr-chg_category
        AND chg_area     = lt_cc_hdr-chg_area.
  ENDIF.

  IF lt_cc_det[] IS NOT INITIAL.
    SELECT * FROM zarn_cc_det
      INTO CORRESPONDING FIELDS OF TABLE fc_t_cc_det[]
      FOR ALL ENTRIES IN lt_cc_det[]
      WHERE chgid          = lt_cc_det-chgid
        AND idno           = lt_cc_det-idno
        AND chg_category   = lt_cc_det-chg_category
        AND chg_table      = lt_cc_det-chg_table
        AND chg_field      = lt_cc_det-chg_field
        AND reference_data = lt_cc_det-reference_data.
  ENDIF.


ENDFORM.                    " GET_CHANGE_DOCUMENT_DATA
