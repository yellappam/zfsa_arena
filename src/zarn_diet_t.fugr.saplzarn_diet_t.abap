* regenerated at 05.05.2016 14:31:24 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_DIET_TTOP.                   " Global Data
  INCLUDE LZARN_DIET_TUXX.                   " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_DIET_TF...                   " Subroutines
* INCLUDE LZARN_DIET_TO...                   " PBO-Modules
* INCLUDE LZARN_DIET_TI...                   " PAI-Modules
* INCLUDE LZARN_DIET_TE...                   " Events
* INCLUDE LZARN_DIET_TP...                   " Local class implement.
* INCLUDE LZARN_DIET_TT99.                   " ABAP Unit tests
  INCLUDE LZARN_DIET_TF00                         . " subprograms
  INCLUDE LZARN_DIET_TI00                         . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
