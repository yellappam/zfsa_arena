*&---------------------------------------------------------------------*
*&  Include           ZIARN_APPROVALS_SLAMAIN
*&---------------------------------------------------------------------*



START-OF-SELECTION.

  SET PARAMETER ID 'ZARN_APPROVALS_SLA' FIELD mytab-activetab.

  CASE mytab-activetab.
* Itemised report
    WHEN gc_tab-tab_itm.
      go_approval_sla =
        NEW #(
          is_control =
            VALUE lcl_approvals_sla=>ty_control(
              pwa = p_pwa
              awa = p_awa
              report = COND #( WHEN p_pwa = abap_true THEN lcl_approvals_sla=>c_report-pending ELSE lcl_approvals_sla=>c_report-approved  )
          )
          is_selections =
            VALUE lcl_approvals_sla=>ty_selections(
              fanid = s_fanid[]
              idno = s_hybid[]
              catman = s_cman[]
              wfca =  s_wfca[]
              new  = p_apnew
              chg  = p_apchg
              nat = p_apnat
              reg = p_apreg
              team_code = s_ateam[]
              ccat = s_ccat[]
            )
        ).

* Product overview
    WHEN gc_tab-tab_pov.
      go_approval_sla =
        NEW #(
          is_control =
            VALUE lcl_approvals_sla=>ty_control(
              report =  lcl_approvals_sla=>c_report-product
          )
          is_selections =
            VALUE lcl_approvals_sla=>ty_selections(
              catman = spo_cman[]
              wfca = spo_per[]
              initiation = spo_init[]
            )
        ).

* Vendor overview
    WHEN gc_tab-tab_vov.

      IF r1 = abap_true.

        SELECT lifnr
          INTO TABLE @DATA(lt_lifnr)
          FROM zarn_reg_pir
          WHERE gln_no IN @svo_gln[]
            AND lifnr NE @space.
        IF sy-subrc = 0.
          LOOP AT lt_lifnr INTO DATA(ls_lifnr).
            gs_lifnr-sign = 'I'.
            gs_lifnr-option = 'EQ'.
            gs_lifnr-low = ls_lifnr-lifnr.
            APPEND gs_lifnr TO gr_lifnr[].
          ENDLOOP.
        ELSE.
          RETURN.
        ENDIF.

      ELSEIF r2 = abap_true.
        gr_lifnr[] = svo_lifn[].
      ENDIF.

      go_approval_sla =
        NEW #(
          is_control =
            VALUE lcl_approvals_sla=>ty_control(
              report =  lcl_approvals_sla=>c_report-vendor
          )
          is_selections =
            VALUE lcl_approvals_sla=>ty_selections(
              catman = svo_cmrl[]
              wfca = svo_per[]
              lifnr = gr_lifnr[]
            )
        ).

  ENDCASE.


* Execute the report
  DATA(lo_output) = go_approval_sla->run( ).


END-OF-SELECTION.

  SET PARAMETER ID 'ZARN_APPROVALS_SLA' FIELD mytab-activetab.

  IF go_approval_sla IS BOUND AND lo_output IS BOUND.
    go_approval_sla->display( lo_output ).
  ENDIF.
