*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_ARTICLE_LINK_MAIN
*&---------------------------------------------------------------------*

START-OF-SELECTION.

  CLEAR: gt_reg_artlink[], gt_mara[], gt_output[],
         gt_hdr_scn_t[], gt_link_scn_t[].

* Retreive Article Linking Data
  PERFORM get_data.


END-OF-SELECTION.

  IF gt_mara[] IS INITIAL.
* No data found, change selection criteria
    MESSAGE s123 DISPLAY LIKE 'E'.
    RETURN.
  ELSE.
* Build Output Data
    PERFORM build_output_data.
  ENDIF.

  IF gt_output[] IS INITIAL.
* No data found, change selection criteria
    MESSAGE s123 DISPLAY LIKE 'E'.
    RETURN.
  ELSE.
* Display Output Data
    PERFORM display_output_data.
  ENDIF.
