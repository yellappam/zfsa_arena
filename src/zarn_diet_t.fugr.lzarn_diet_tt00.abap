*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 05.05.2016 at 14:31:24 by user C90001363
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_DIET_T.....................................*
DATA:  BEGIN OF STATUS_ZARN_DIET_T                   .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_DIET_T                   .
CONTROLS: TCTRL_ZARN_DIET_T
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_DIET_T                   .
TABLES: ZARN_DIET_T                    .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
