*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 22.06.2020 at 15:50:23
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZVARN_CLUSTR_CFG................................*
TABLES: ZVARN_CLUSTR_CFG, *ZVARN_CLUSTR_CFG. "view work areas
CONTROLS: TCTRL_ZVARN_CLUSTR_CFG
TYPE TABLEVIEW USING SCREEN '9000'.
DATA: BEGIN OF STATUS_ZVARN_CLUSTR_CFG. "state vector
          INCLUDE STRUCTURE VIMSTATUS.
DATA: END OF STATUS_ZVARN_CLUSTR_CFG.
* Table for entries selected to show on screen
DATA: BEGIN OF ZVARN_CLUSTR_CFG_EXTRACT OCCURS 0010.
INCLUDE STRUCTURE ZVARN_CLUSTR_CFG.
          INCLUDE STRUCTURE VIMFLAGTAB.
DATA: END OF ZVARN_CLUSTR_CFG_EXTRACT.
* Table for all entries loaded from database
DATA: BEGIN OF ZVARN_CLUSTR_CFG_TOTAL OCCURS 0010.
INCLUDE STRUCTURE ZVARN_CLUSTR_CFG.
          INCLUDE STRUCTURE VIMFLAGTAB.
DATA: END OF ZVARN_CLUSTR_CFG_TOTAL.

*.........table declarations:.................................*
TABLES: ZARN_CLUSTER_CFG               .
