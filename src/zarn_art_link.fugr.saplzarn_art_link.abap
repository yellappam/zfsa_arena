* regenerated at 21.11.2016 14:10:30
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_ART_LINKTOP.                 " Global Data
  INCLUDE LZARN_ART_LINKUXX.                 " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_ART_LINKF...                 " Subroutines
* INCLUDE LZARN_ART_LINKO...                 " PBO-Modules
* INCLUDE LZARN_ART_LINKI...                 " PAI-Modules
* INCLUDE LZARN_ART_LINKE...                 " Events
* INCLUDE LZARN_ART_LINKP...                 " Local class implement.
* INCLUDE LZARN_ART_LINKT99.                 " ABAP Unit tests
  INCLUDE LZARN_ART_LINKF00                       . " subprograms
  INCLUDE LZARN_ART_LINKI00                       . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
