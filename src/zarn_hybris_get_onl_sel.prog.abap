*&---------------------------------------------------------------------*
*&  Include           ZARN_HYBRIS_GET_SEL
*&---------------------------------------------------------------------*
TABLES: zarn_products.
DATA: gv_run_id TYPE zarn_hybris-run_id,
      gv_cr_dat TYPE zarn_hybris-create_date.

SELECTION-SCREEN BEGIN OF BLOCK mod WITH FRAME TITLE text-mod.
PARAMETER: p_hyb RADIOBUTTON GROUP zrad DEFAULT 'X' USER-COMMAND zrad,
*           p_arn RADIOBUTTON GROUP zrad,                                                  "--ONLD-712 JKH 08.11.2016
*           p_ard RADIOBUTTON GROUP zrad,                                                  "--ONLD-712 JKH 08.11.2016
           p_rep RADIOBUTTON GROUP zrad,
           p_del RADIOBUTTON GROUP zrad.

PARAMETER:  p_arn TYPE char1 DEFAULT abap_false NO-DISPLAY,                                "--ONLD-712 JKH 08.11.2016
            p_ard TYPE char1 DEFAULT abap_false NO-DISPLAY.                                "--ONLD-712 JKH 08.11.2016

SELECTION-SCREEN END OF BLOCK mod.
SELECTION-SCREEN BEGIN OF BLOCK sel WITH FRAME TITLE text-sel.
SELECT-OPTIONS: s_fanid FOR zarn_products-fan_id NO INTERVALS.
PARAMETERS: p_file TYPE string.
SELECTION-SCREEN END OF BLOCK sel.
SELECTION-SCREEN BEGIN OF BLOCK arn WITH FRAME TITLE text-arn.
PARAMETERS: p_run_id TYPE zarn_run_id.
SELECTION-SCREEN END OF BLOCK arn.
SELECTION-SCREEN BEGIN OF BLOCK rep WITH FRAME TITLE text-rep.
SELECT-OPTIONS: s_run_id FOR gv_run_id,
                s_cr_dat FOR gv_cr_dat.
SELECTION-SCREEN END OF BLOCK rep.
SELECTION-SCREEN BEGIN OF BLOCK opt WITH FRAME TITLE text-opt.
PARAMETERS: p_block  TYPE i DEFAULT 50 OBLIGATORY,
            p_wait   TYPE i DEFAULT 1,
            p_arena  TYPE flag DEFAULT abap_false NO-DISPLAY,                               "ONLD-712 JKH 08.11.2016
            p_darena TYPE flag DEFAULT abap_false NO-DISPLAY,                               "ONLD-712 JKH 08.11.2016
            p_indiv  TYPE flag DEFAULT space,
            p_calles TYPE flag DEFAULT space.
SELECTION-SCREEN END OF BLOCK opt.

AT SELECTION-SCREEN ON VALUE-REQUEST FOR p_file.
  PERFORM get_file CHANGING p_file.

AT SELECTION-SCREEN OUTPUT.
  LOOP AT SCREEN.
    IF p_hyb NE abap_true.
      IF screen-name CS 'S_FANID'
      OR screen-name CS 'P_FILE'
      OR screen-name CS 'P_BLOCK'
      OR screen-name CS 'P_ARENA'
      OR screen-name CS 'P_CALLES'.
        screen-input     = 0.
        screen-output    = 0.
        screen-invisible = 1.
        MODIFY SCREEN.
      ENDIF.
    ENDIF.
    IF  p_arn NE abap_true
    AND p_ard NE abap_true.
      IF screen-name CS 'P_RUN_ID'.
        screen-input     = 0.
        screen-output    = 0.
        screen-invisible = 1.
        MODIFY SCREEN.
      ENDIF.
    ENDIF.
    IF  p_arn NE abap_true.
      IF screen-name CS 'P_INDIV'.
        screen-input     = 0.
        screen-output    = 0.
        screen-invisible = 1.
        MODIFY SCREEN.
      ENDIF.
    ENDIF.
    IF p_del NE abap_true.
      IF screen-name CS 'P_DARENA'.
        screen-input     = 0.
        screen-output    = 0.
        screen-invisible = 1.
        MODIFY SCREEN.
      ENDIF.
    ENDIF.
    IF p_rep EQ abap_true
    OR p_del EQ abap_true.
    ELSE.
      IF screen-name CS 'S_RUN_ID'.
        screen-input     = 0.
        screen-output    = 0.
        screen-invisible = 1.
        MODIFY SCREEN.
      ENDIF.
    ENDIF.
    IF p_rep NE abap_true.
      IF screen-name CS 'S_CR_DAT'.
        screen-input     = 0.
        screen-output    = 0.
        screen-invisible = 1.
        MODIFY SCREEN.
      ENDIF.
    ENDIF.
    IF p_del EQ abap_true
    OR p_rep EQ abap_true
    OR p_ard EQ abap_true.
      IF screen-name CS 'P_WAIT'.
        screen-input     = 0.
        screen-output    = 0.
        screen-invisible = 1.
        MODIFY SCREEN.
      ENDIF.
    ENDIF.
  ENDLOOP.
