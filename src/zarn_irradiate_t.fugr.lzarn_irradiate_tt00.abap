*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 02.05.2016 at 17:08:45 by user C90001363
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_IRRADIATE_T................................*
DATA:  BEGIN OF STATUS_ZARN_IRRADIATE_T              .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_IRRADIATE_T              .
CONTROLS: TCTRL_ZARN_IRRADIATE_T
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_IRRADIATE_T              .
TABLES: ZARN_IRRADIATE_T               .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
