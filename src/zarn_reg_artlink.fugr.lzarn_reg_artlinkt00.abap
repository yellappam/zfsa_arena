*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 08.04.2020 at 11:21:20
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_REG_ARTLINK................................*
DATA:  BEGIN OF STATUS_ZARN_REG_ARTLINK              .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_REG_ARTLINK              .
CONTROLS: TCTRL_ZARN_REG_ARTLINK
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_REG_ARTLINK              .
TABLES: ZARN_REG_ARTLINK               .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
