* regenerated at 16.05.2016 10:31:03 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_CC_DESCTOP.                  " Global Data
  INCLUDE LZARN_CC_DESCUXX.                  " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_CC_DESCF...                  " Subroutines
* INCLUDE LZARN_CC_DESCO...                  " PBO-Modules
* INCLUDE LZARN_CC_DESCI...                  " PAI-Modules
* INCLUDE LZARN_CC_DESCE...                  " Events
* INCLUDE LZARN_CC_DESCP...                  " Local class implement.
* INCLUDE LZARN_CC_DESCT99.                  " ABAP Unit tests
  INCLUDE LZARN_CC_DESCF00                        . " subprograms
  INCLUDE LZARN_CC_DESCI00                        . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
