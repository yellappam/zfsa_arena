* regenerated at 25.05.2016 12:09:27 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_COMM_CH_TTOP.                " Global Data
  INCLUDE LZARN_COMM_CH_TUXX.                " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_COMM_CH_TF...                " Subroutines
* INCLUDE LZARN_COMM_CH_TO...                " PBO-Modules
* INCLUDE LZARN_COMM_CH_TI...                " PAI-Modules
* INCLUDE LZARN_COMM_CH_TE...                " Events
* INCLUDE LZARN_COMM_CH_TP...                " Local class implement.
* INCLUDE LZARN_COMM_CH_TT99.                " ABAP Unit tests
  INCLUDE LZARN_COMM_CH_TF00                      . " subprograms
  INCLUDE LZARN_COMM_CH_TI00                      . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
