FUNCTION zarn_set_get_mass_flag.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(IV_MODE) TYPE  CHAR1
*"     REFERENCE(IV_ARENA_MASS) TYPE  CHAR1 OPTIONAL
*"     REFERENCE(IT_IDNO_MASS_RANGE) TYPE  ZTARN_IDNO_RANGE OPTIONAL
*"  EXPORTING
*"     REFERENCE(EV_ARENA_MASS) TYPE  CHAR1
*"     REFERENCE(ET_IDNO_MASS_RANGE) TYPE  ZTARN_IDNO_RANGE
*"----------------------------------------------------------------------

  STATICS: sv_arena_mass_static TYPE char1,
           st_idno_mass_range   TYPE ztarn_idno_range.





* default = nothing to return
  CLEAR ev_arena_mass.

  CASE iv_mode.
    WHEN 'W'. "write
      sv_arena_mass_static = iv_arena_mass.

      IF iv_arena_mass = abap_true.

        IF it_idno_mass_range[] IS SUPPLIED.
          st_idno_mass_range[] = it_idno_mass_range[].
        ENDIF.


      ELSE.
        CLEAR: st_idno_mass_range[].
      ENDIF.

    WHEN 'R'. "Read

      ev_arena_mass        = sv_arena_mass_static.
      et_idno_mass_range[] = st_idno_mass_range[].


    WHEN OTHERS.

  ENDCASE.


ENDFUNCTION.
