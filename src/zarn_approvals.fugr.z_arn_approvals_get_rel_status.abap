FUNCTION z_arn_approvals_get_rel_status.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(PROD_DATA_N) TYPE  ZTARN_PROD_DATA
*"     REFERENCE(CC_HDR) TYPE  ZTARN_CC_HDR
*"  CHANGING
*"     REFERENCE(PRD_VERSION_N) TYPE  ZTARN_PRD_VERSION
*"     REFERENCE(REG_DATA_N) TYPE  ZTARN_REG_DATA
*"----------------------------------------------------------------------


*>>>DELETED - Ivo Skolek, 3162 approval stabilisation clean up - code not USED anymore *<<<



*  DATA: lv_release_status TYPE zarn_rel_status,
*        lv_data_level     TYPE zarn_data_level,
*        lv_process_type   TYPE zarn_process_type.
*
*  FIELD-SYMBOLS: <ls_prd_version> LIKE LINE OF prd_version_n,
*                 <ls_reg_data>    LIKE LINE OF reg_data_n,
*                 <ls_reg_hdr>     LIKE LINE OF <ls_reg_data>-zarn_reg_hdr,
*                 <ls_prod_data>   LIKE LINE OF prod_data_n,
*                 <ls_arn_product> LIKE LINE OF <ls_prod_data>-zarn_products.
*
**>>>IS1609mod Feature 3162 - AReNa Approval Stabilisation
*  IF zcl_arn_approval_backend=>temp_is_active( ).
*
*    "TBD this function module not to be used after Approval Stabilisation
*    "Release status fields not to be used anymore
*
*  ENDIF.
**<<<IS1609mod Feature 3162 - AReNa Approval Stabilisation
*
*
*  LOOP AT prd_version_n ASSIGNING <ls_prd_version>.
*    IF <ls_prd_version>-release_status IS INITIAL.
*      CLEAR: lv_data_level,
*             lv_process_type,
*             lv_release_status.
*      lv_data_level = co_appr_called_from_nat.
*      PERFORM get_process_type USING <ls_prd_version>-idno CHANGING lv_process_type.
*      CALL FUNCTION 'Z_ARN_APPROVALS_GET_LIST'
*        EXPORTING
*          idno            = <ls_prd_version>-idno
*          version         = <ls_prd_version>-version
*          data_level      = lv_data_level
*          process_type    = lv_process_type
*          ZARN_CC_HDR_TAB = cc_hdr
*        IMPORTING
*          release_status  = lv_release_status.
*      <ls_prd_version>-release_status = lv_release_status.
*    ENDIF.
*  ENDLOOP.
*
*  LOOP AT reg_data_n ASSIGNING <ls_reg_data>.
*    LOOP AT <ls_reg_data>-zarn_reg_hdr ASSIGNING <ls_reg_hdr>.
*      IF <ls_reg_hdr>-release_status IS INITIAL.
*        CLEAR: lv_data_level,
*               lv_process_type,
*               lv_release_status.
*        lv_data_level = co_appr_called_from_reg.
*        PERFORM get_process_type USING <ls_prd_version>-idno CHANGING lv_process_type.
*        CALL FUNCTION 'Z_ARN_APPROVALS_GET_LIST'
*          EXPORTING
*            idno            = <ls_reg_hdr>-idno
*            version         = <ls_reg_hdr>-version
*            data_level      = lv_data_level
*            process_type    = lv_process_type
*            ZARN_CC_HDR_TAB = cc_hdr
*          IMPORTING
*            release_status  = lv_release_status.
*        <ls_reg_hdr>-release_status = lv_release_status.
*      ENDIF.
*    ENDLOOP.
*  ENDLOOP.

ENDFUNCTION.
