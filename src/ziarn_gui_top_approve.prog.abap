*&---------------------------------------------------------------------*
*&  Include           ZIARN_GUI_TOP_APPROVE
*&---------------------------------------------------------------------*
CONSTANTS:
  co_appr_called_from_nat TYPE char3 VALUE 'NAT',
  co_appr_called_from_reg TYPE char3 VALUE 'REG',
  co_status_wip           TYPE char3 VALUE 'WIP',
  co_status_rej           TYPE char3 VALUE 'REJ',
  co_status_apr           TYPE char3 VALUE 'APR',
  co_status_cre           TYPE char3 VALUE 'CRE'.

*>>>IS1609ins Feature 3162 - AReNa Approval Stabilisation

DATA go_approval TYPE REF TO zcl_arn_approval_backend.

TYPES: ty_t_cc_log_sorted TYPE SORTED TABLE OF zarn_cc_log
                                WITH NON-UNIQUE KEY team_code,
       ty_t_appr_output   TYPE STANDARD TABLE OF zarn_approvals_fields.

DATA: gt_art_ver_ra   TYPE RANGE OF zarn_article_ver,
      gt_appr_area_ra TYPE RANGE OF zarn_e_appr_area.

*<<<IS1609ins Feature 3162 - AReNa Approval Stabilisation
