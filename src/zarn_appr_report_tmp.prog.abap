*&---------------------------------------------------------------------*
*& Report  ZARN_APPR_REPORT_TMP
*&
*&---------------------------------------------------------------------*
* PROJECT        # One Data
* SPECIFICATION  # Temporary report to be used until AReNa Core Report
* AUTHOR         # Ponnaiyan Govindasamy
*-----------------------------------------------------------------------
* TITLE          # Report for Pending AReNa approval lines
* PURPOSE        # Temporary report to be used until AReNa Core Report
*                #
*-----------------------------------------------------------------------
* RESTRICTIONS
*
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE           #
* CHANGE No.     #
* DESCRIPTION    #
* WHO            #
*-----------------------------------------------------------------------
REPORT zarn_appr_report_tmp.

*----------------------------------------------------------------------*
*       Data Declarations
*----------------------------------------------------------------------*

TYPES: BEGIN OF ty_catman,
         role_id   TYPE zmd_category-role_id,
         role_name TYPE zmd_category-role_name,
       END OF ty_catman.

TYPES: BEGIN OF ty_matkl,
         matkl TYPE t023t-matkl,
         wgbez TYPE t023t-wgbez,
       END OF ty_matkl.

DATA: zsarn_appr_report TYPE zsarn_appr_report,
      zsarn_approval    TYPE zarn_approval,
      lt_output         TYPE zarn_t_appr_report,
      ls_output         LIKE LINE OF lt_output,
      lt_charea         TYPE RANGE OF zarn_e_appr_area,
      ls_charea         LIKE LINE OF lt_charea,
      lt_artver         TYPE RANGE OF zarn_article_ver,
      ls_artver         LIKE LINE OF lt_artver,
      it_catman         TYPE STANDARD TABLE OF ty_catman WITH HEADER LINE,
      ls_catman         LIKE LINE OF it_catman,
      it_matkl          TYPE STANDARD TABLE OF ty_matkl WITH HEADER LINE,
      ls_matkl          LIKE LINE OF it_matkl,
      lv_artver         TYPE zarn_article_ver.

FIELD-SYMBOLS: <fs_output> LIKE LINE OF lt_output.

*----------------------------------------------------------------------*
*       Selection screen definition
*----------------------------------------------------------------------*
SELECTION-SCREEN BEGIN OF BLOCK b1 WITH FRAME TITLE text-art.
SELECT-OPTIONS:
   s_fan    FOR zsarn_appr_report-fan_id MATCHCODE OBJECT zmd_zzfan,
   s_hid    FOR zsarn_appr_report-idno,
   s_credat FOR zsarn_approval-updated_timestamp NO-EXTENSION,
   s_catman FOR zsarn_appr_report-ret_zzcatman NO INTERVALS MATCHCODE OBJECT zhmd_role_id.
SELECTION-SCREEN END OF BLOCK b1.

SELECTION-SCREEN BEGIN OF BLOCK b2 WITH FRAME TITLE text-sta.
* New or Change article
SELECTION-SCREEN BEGIN OF LINE.
PARAMETERS: p_apnew TYPE xfeld RADIOBUTTON GROUP ag1.
SELECTION-SCREEN COMMENT 3(15) text-ar4.
PARAMETERS: p_apchg TYPE xfeld RADIOBUTTON GROUP ag1.
SELECTION-SCREEN COMMENT 21(15) text-ar5.
PARAMETERS: p_apncb TYPE xfeld RADIOBUTTON GROUP ag1 DEFAULT 'X'.
SELECTION-SCREEN COMMENT 39(10) text-ar8.
SELECTION-SCREEN END OF LINE.

* National or regional change
SELECTION-SCREEN BEGIN OF LINE.
PARAMETERS: p_apnat TYPE xfeld RADIOBUTTON GROUP ag2.
SELECTION-SCREEN COMMENT 3(15) text-ar6.
PARAMETERS: p_apreg TYPE xfeld RADIOBUTTON GROUP ag2.
SELECTION-SCREEN COMMENT 21(15) text-ar7.
PARAMETERS: p_apnrb TYPE xfeld RADIOBUTTON GROUP ag2 DEFAULT 'X'.
SELECTION-SCREEN COMMENT 39(10) text-ar8.
SELECTION-SCREEN END OF LINE.

SELECT-OPTIONS: s_aptem FOR zsarn_approval-team_code,
                s_apcat FOR zsarn_approval-chg_category.
SELECTION-SCREEN END OF BLOCK b2.

SELECTION-SCREEN BEGIN OF BLOCK b3 WITH FRAME TITLE text-ven.
 SELECT-OPTIONS: s_gln1   FOR zsarn_appr_report-gln NO INTERVALS NO-EXTENSION.
SELECTION-SCREEN END OF BLOCK b3.

AT SELECTION-SCREEN ON VALUE-REQUEST FOR s_gln1-low.
  PERFORM get_lifnr_gln CHANGING s_gln1-low.
*----------------------------------------------------------------------*
*       Records Processing
*----------------------------------------------------------------------*
START-OF-SELECTION.

  CLEAR: lv_artver.
  IF p_apnew IS NOT INITIAL.
    ls_artver-sign   = 'I'.
    ls_artver-option = 'EQ'.
    ls_artver-low    = lv_artver.
    APPEND ls_artver TO lt_artver[].
  ELSEIF p_apchg IS NOT INITIAL.
    ls_artver-sign   = 'I'.
    ls_artver-option = 'NE'.
    ls_artver-low    = lv_artver.
    APPEND ls_artver TO lt_artver[].
  ENDIF.


  IF p_apnat IS NOT INITIAL.
    ls_charea-sign   = 'I'.
    ls_charea-option = 'EQ'.
    ls_charea-low    = 'NAT'.
    APPEND ls_charea TO lt_charea[].
  ELSEIF p_apreg IS NOT INITIAL.
    ls_charea-sign   = 'I'.
    ls_charea-option = 'EQ'.
    ls_charea-low    = 'REG'.
    APPEND ls_charea TO lt_charea[].
  ENDIF.

  PERFORM fetch_records.
  PERFORM add_data.
  PERFORM display_alv.


FORM fetch_records.
  SELECT p~idno p~fan_id p~description p~gln p~gln_descr m~nat_mc_code AS matkl r~ret_zzcatman r~gil_zzcatman r~initiation
         a~updated_timestamp AS date_in a~chg_category a~approval_area a~team_code s~article_ver
            INTO CORRESPONDING FIELDS OF TABLE lt_output
            FROM zarn_approval AS a
            INNER JOIN zarn_ver_status AS s
              ON  s~idno         EQ a~idno
              AND s~current_ver  EQ a~nat_version
            INNER JOIN zarn_products AS p
              ON  p~idno        EQ s~idno
              AND p~version     EQ s~current_ver
            INNER JOIN zarn_nat_mc_hier AS m
              ON  m~idno        EQ a~idno
             AND  m~version     EQ a~nat_version
            INNER JOIN zarn_reg_hdr AS r
              ON  r~idno        EQ a~idno
           WHERE a~updated_timestamp IN s_credat[]
              AND p~fan_id           IN s_fan[]
              AND p~fan_id           NE space
              AND p~idno             IN s_hid[]
              AND p~gln              IN s_gln1[]
              AND s~article_ver      IN lt_artver[]
              AND ( r~ret_zzcatman   IN s_catman
              OR    r~gil_zzcatman   IN s_catman )
              AND a~status           EQ 'AR'
              AND a~approval_area    IN lt_charea[]
              AND a~team_code        IN s_aptem[]
              AND a~chg_category     IN s_apcat[].
ENDFORM.


FORM add_data.

  IF lt_output[] IS NOT INITIAL.
    SORT lt_output ASCENDING BY idno chg_category approval_area team_code.

    SELECT role_id role_name FROM zmd_category
      INTO TABLE it_catman.

    SELECT * FROM t023t
      INTO CORRESPONDING FIELDS OF TABLE it_matkl
      FOR ALL ENTRIES IN lt_output WHERE
      matkl = lt_output-matkl AND
      spras = sy-langu.

    ls_output-process_type = 'CHG'.
    MODIFY lt_output FROM ls_output
    TRANSPORTING process_type
    WHERE article_ver IS NOT INITIAL.
    CLEAR: ls_output.

    ls_output-process_type = 'NEW'.
    MODIFY lt_output FROM ls_output
    TRANSPORTING process_type
    WHERE article_ver IS INITIAL.
    CLEAR: ls_output.

    LOOP AT it_catman INTO ls_catman.

      ls_output-role_name_ret = ls_catman-role_name.

      MODIFY lt_output FROM ls_output
      TRANSPORTING role_name_ret
      WHERE ret_zzcatman = ls_catman-role_id.

      ls_output-role_name_gil = ls_catman-role_name.

      MODIFY lt_output FROM ls_output
      TRANSPORTING role_name_gil
      WHERE gil_zzcatman = ls_catman-role_id.

      CLEAR: ls_output, ls_catman.

    ENDLOOP.

    LOOP AT it_matkl INTO ls_matkl.

      ls_output-wgbez = ls_matkl-wgbez.

      MODIFY lt_output FROM ls_output
      TRANSPORTING wgbez
      WHERE matkl = ls_matkl-matkl.

      CLEAR: ls_matkl, ls_output.
    ENDLOOP.
  ENDIF.
ENDFORM.

FORM display_alv.
  TYPES: BEGIN OF ty_idno,
           idno TYPE zarn_idno,
         END OF ty_idno.
  DATA: lr_table   TYPE REF TO cl_salv_table,
        lr_layout  TYPE REF TO cl_salv_layout,
        ls_lay_key TYPE salv_s_layout_key,
        lv_header  TYPE lvc_title,
        lt_idno    TYPE TABLE OF ty_idno.

  MOVE-CORRESPONDING lt_output[] TO lt_idno[].
  SORT lt_idno ASCENDING.
  DELETE ADJACENT DUPLICATES FROM lt_idno COMPARING idno.

  TRY.

      cl_salv_table=>factory( IMPORTING r_salv_table = lr_table
                              CHANGING t_table = lt_output ).

      lr_table->get_columns( )->set_optimize( 'X' ).

      lr_layout = lr_table->get_layout( ).
      lr_layout->set_default( 'X' ).
      ls_lay_key-report = sy-repid.
      lr_layout->set_key( ls_lay_key ).
      lr_layout->set_save_restriction( if_salv_c_layout=>restrict_none ).

      lv_header = |No.of Pending lines { lines( lt_output ) } ;No.of Products { lines( lt_idno ) }  |.
      lr_table->get_display_settings( )->set_list_header( lv_header ).
      lr_table->get_functions( )->set_all( ).


      lr_table->display( ).

    CATCH cx_salv_msg.
      MESSAGE e009(zarena_msg).
      LEAVE LIST-PROCESSING.
  ENDTRY.

ENDFORM.

FORM get_lifnr_gln CHANGING pc_gln TYPE zarn_gln.

  DATA: lt_return TYPE STANDARD TABLE OF ddshretval,
        ls_return LIKE LINE OF lt_return,
        ls_lfa1   TYPE lfa1.

  REFRESH lt_return.
  CALL FUNCTION 'F4IF_FIELD_VALUE_REQUEST'
    EXPORTING
      tabname           = 'ZARN_PIR'
      fieldname         = 'GLN'
      searchhelp        = 'ZHARN_VENDOR'
      shlpparam         = 'LIFNR'
    TABLES
      return_tab        = lt_return
    EXCEPTIONS
      field_not_found   = 1
      no_help_for_field = 2
      inconsistent_help = 3
      no_values_found   = 4
      OTHERS            = 5.
  READ TABLE lt_return INTO ls_return WITH KEY fieldname = 'LIFNR'.
  IF sy-subrc EQ 0.
    ls_lfa1-lifnr = ls_return-fieldval.
    CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'
      EXPORTING
        input  = ls_lfa1-lifnr
      IMPORTING
        output = ls_lfa1-lifnr.
    SELECT SINGLE lifnr bbbnr bbsnr bubkz
      INTO CORRESPONDING FIELDS OF ls_lfa1
      FROM lfa1
      WHERE lifnr EQ ls_lfa1-lifnr.
    IF sy-subrc EQ 0.
      pc_gln+0(7)  = ls_lfa1-bbbnr.
      pc_gln+7(5)  = ls_lfa1-bbsnr.
      pc_gln+12(1) = ls_lfa1-bubkz.
    ENDIF.
  ENDIF.

ENDFORM.
