*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_DELTA_ANALYSIS_MAIN
*&---------------------------------------------------------------------*


START-OF-SELECTION.

  IF p_multi EQ abap_true.
* Get Initial Data for Multiple FAN
    PERFORM get_initial_data_multiple.

* Validate Multiple FAN ID Versions
    PERFORM validate_multiple_fan_versions.

  ENDIF.

  CHECK gv_error IS INITIAL.

  CLEAR: gt_delta_alv[].


* Get Initial Data
  PERFORM get_initial_data.


  IF gv_national = abap_true.
* Build National Data
    PERFORM build_national_data.
  ENDIF.


  IF gv_regional = abap_true.
* Build Regional Data
    PERFORM build_regional_data.
  ENDIF.

* Apply Filters on ALV
  PERFORM apply_filters_on_alv.

  IF gt_delta_alv[] IS INITIAL.
* No FAN ID to Display
    MESSAGE s055 DISPLAY LIKE 'E'.
    gv_error = abap_true.
  ENDIF.


END-OF-SELECTION.

  IF gt_delta_alv[] IS NOT INITIAL.
* Call the screen 9000 for displaying ALV grid.
    CALL SCREEN 9000.
  ENDIF.
