* regenerated at 11.05.2017 14:15:46
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_BUTOP.                       " Global Data
  INCLUDE LZARN_BUUXX.                       " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_BUF...                       " Subroutines
* INCLUDE LZARN_BUO...                       " PBO-Modules
* INCLUDE LZARN_BUI...                       " PAI-Modules
* INCLUDE LZARN_BUE...                       " Events
* INCLUDE LZARN_BUP...                       " Local class implement.
* INCLUDE LZARN_BUT99.                       " ABAP Unit tests
  INCLUDE LZARN_BUF00                             . " subprograms
  INCLUDE LZARN_BUI00                             . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
