*----------------------------------------------------------------------*
***INCLUDE LZARN_MASSF01.
*----------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*&      Form  INIT_LOG
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
FORM init_log.

  IF gs_log_handle IS INITIAL.
    DATA ls_s_log TYPE bal_s_log.

    ls_s_log-aldate = sy-datum.
    ls_s_log-altime = sy-uzeit.
    ls_s_log-aluser = sy-uname.

    CALL FUNCTION 'BAL_LOG_CREATE'
      EXPORTING
        i_s_log                 = ls_s_log
      IMPORTING
        e_log_handle            = gs_log_handle
      EXCEPTIONS
        log_header_inconsistent = 1
        OTHERS                  = 2.
    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
              WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.
  ELSE.
    CALL FUNCTION 'BAL_LOG_MSG_DELETE_ALL'
      EXPORTING
        i_log_handle = gs_log_handle
      EXCEPTIONS
        OTHERS       = 2.
    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
              WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  DISPLAY_LOG
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
FORM display_log.

  DATA lt_log_handle TYPE bal_t_logh.

  APPEND gs_log_handle TO lt_log_handle.

  CALL FUNCTION 'BAL_DSP_LOG_DISPLAY'
    EXPORTING
      i_t_log_handle       = lt_log_handle
      i_amodal             = abap_true
    EXCEPTIONS
      profile_inconsistent = 1
      internal_error       = 2
      no_data_available    = 3
      no_authority         = 4
      OTHERS               = 5.
  IF sy-subrc <> 0.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
            WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  write_msg
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->PIV_TYPE     text
*      -->PIV_MSG_NUM  text
*      -->PIV_VAR1     text
*      -->PIV_VAR2     text
*      -->PIV_VAR3     text
*      -->PIV_VAR4     text
*----------------------------------------------------------------------*
FORM write_msg USING piv_type    TYPE char1
                     piv_msg_num TYPE char3
                     piv_var1
                     piv_var2
                     piv_var3
                     piv_var4.

  DATA ls_msg TYPE bal_s_msg.

  ls_msg-msgid = 'ZARENA_MSG'.
  ls_msg-msgno = piv_msg_num.
  ls_msg-msgty = piv_type.
  ls_msg-msgv1 = piv_var1.
  ls_msg-msgv2 = piv_var2.
  ls_msg-msgv3 = piv_var3.
  ls_msg-msgv4 = piv_var4.


  CALL FUNCTION 'BAL_LOG_MSG_ADD'
    EXPORTING
      i_log_handle     = gs_log_handle
      i_s_msg          = ls_msg
    EXCEPTIONS
      log_not_found    = 1
      msg_inconsistent = 2
      log_is_full      = 3
      OTHERS           = 4.

ENDFORM.                    " WRITE_MSG
*&---------------------------------------------------------------------*
*&      Form  REG_HDR_CHECK
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
FORM reg_hdr_check USING pit_fields     TYPE mass_allfields
                         piv_user_cmd   TYPE sy-ucomm
                         pit_reg_hdr    TYPE ty_t_reg_hdr
                         pit_act_fields TYPE ty_t_act_fields
                CHANGING piv_flag_err   TYPE xfeld.


  DATA: ls_reg_hdr    TYPE zarn_reg_hdr,
        ls_fields     TYPE massdfies,
        lv_field      TYPE string,
        ls_act_fields TYPE ty_s_act_fields.

  LOOP AT pit_reg_hdr INTO ls_reg_hdr.
    LOOP AT pit_act_fields INTO ls_fields.

      CASE ls_fields-fieldname.
        WHEN 'MSTAE'.
          PERFORM x_site_status_check USING ls_reg_hdr-mstae
                                            ls_reg_hdr-idno
                                   CHANGING piv_flag_err.

        WHEN 'MSTAV'.
          PERFORM x_dc_status_check USING ls_reg_hdr-mstav
                                          ls_reg_hdr-idno
                                 CHANGING piv_flag_err.

      ENDCASE.

    ENDLOOP.  " LOOP AT pit_act_fields INTO ls_fields
  ENDLOOP.  " LOOP AT pit_reg_hdr INTO ls_reg_hdr

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  X_SITE_STATUS_CHECK
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
FORM x_site_status_check USING piv_mstae   TYPE mstae
                               piv_idno    TYPE zarn_idno
                      CHANGING pcv_err     TYPE xfeld.

  IF piv_mstae IS INITIAL.
    RETURN.
  ENDIF.

  IF gt_t141 IS INITIAL.
    SELECT mmsta FROM t141
    INTO CORRESPONDING FIELDS OF TABLE gt_t141
    WHERE zz_restricted_in_basic EQ space.
  ENDIF.

  READ TABLE gt_t141 TRANSPORTING NO FIELDS
  WITH TABLE KEY mmsta = piv_mstae.
  IF sy-subrc NE 0.
* IDNO &: X-site status '&' is not allowed in basic view.
    PERFORM write_msg USING 'E' '114' piv_idno
                              piv_mstae ''  ''.
    IF 1 = 2. MESSAGE e114(zarena_msg). ENDIF.
    pcv_err = 'X'.
    RETURN.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  X_DC_STATUS_CHECK
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
FORM x_dc_status_check USING piv_mstav   TYPE mstav
                             piv_idno    TYPE zarn_idno
                    CHANGING pcv_err     TYPE xfeld.

  IF piv_mstav IS INITIAL.
    RETURN.
  ENDIF.

  IF gt_tvms IS INITIAL.
    SELECT vmsta FROM tvms
    INTO CORRESPONDING FIELDS OF TABLE gt_tvms
    WHERE zz_restricted_in_basic EQ space.
  ENDIF.

  READ TABLE gt_tvms TRANSPORTING NO FIELDS
  WITH TABLE KEY vmsta = piv_mstav.
  IF sy-subrc NE 0.
* IDNO &: X-DChain status '&' is not allowed in basic view.
    PERFORM write_msg USING 'E' '115' piv_idno
                              piv_mstav ''  ''.
    IF 1 = 2. MESSAGE e115(zarena_msg). ENDIF.
    pcv_err = 'X'.
    RETURN.
  ENDIF.

ENDFORM.
