*&---------------------------------------------------------------------*
*& Report  ZRARN_CHANGE_DOCUMENT_DISPLAY
*&
*&---------------------------------------------------------------------*
*&---------------------------------------------------------------------*
* PROJECT          # FSA
* SPECIFICATION    # 3113
* DATE WRITTEN     # 29.12.2016
* SYSTEM           # DE1
* SAP VERSION      # ECC6.0
* TYPE             # Report
* AUTHOR           # C90001363
*-----------------------------------------------------------------------
* TITLE            # Display for Change Document Data
* PURPOSE          # Display for Change Document Data
*                  #
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      #
*-----------------------------------------------------------------------
* CALLS TO         #
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

REPORT zrarn_change_document_display.

CONSTANTS: lc_object        TYPE char15     VALUE 'ZARN_CTRL_REGNL'.

DATA: lt_alv      TYPE ztarn_change_document_display,
      ls_alv      TYPE zsarn_change_document_display,

      lr_idno_id  TYPE ztarn_cdobjectv_range,
      ls_idno_id  TYPE zsarn_cdobjectv,
      lr_chgid_id TYPE ztarn_cdobjectv_range,
      ls_chgid_id TYPE zsarn_cdobjectv.



INCLUDE zrarn_change_document_disps01.   " Selection screen
INCLUDE zrarn_change_document_dispo01.   " PBO Modules
INCLUDE zrarn_change_document_dispi01.   " PAI Modules
INCLUDE zrarn_change_document_dispf01.   " Forms



START-OF-SELECTION.

  DELETE s_idno[]  WHERE low IS INITIAL AND high IS INITIAL.
  DELETE s_chgid[] WHERE low IS INITIAL AND high IS INITIAL.
  DELETE s_chgnr[] WHERE low IS INITIAL AND high IS INITIAL.
  DELETE s_udate[] WHERE low IS INITIAL AND high IS INITIAL.
  DELETE s_uname[] WHERE low IS INITIAL.

  CLEAR: lr_idno_id[], lr_chgid_id[].

  LOOP AT s_idno.
    CLEAR ls_idno_id.
    ls_idno_id-sign   = s_idno-sign.
    ls_idno_id-option = s_idno-option.
    ls_idno_id-low    = s_idno-low.
    ls_idno_id-high   = s_idno-high.
    APPEND ls_idno_id TO lr_idno_id[].
  ENDLOOP.

  LOOP AT s_chgid.
    CLEAR ls_chgid_id.
    ls_chgid_id-sign   = s_chgid-sign.
    ls_chgid_id-option = s_chgid-option.
    ls_chgid_id-low    = s_chgid-low.
    ls_chgid_id-high   = s_chgid-high.
    APPEND ls_chgid_id TO lr_chgid_id[].
  ENDLOOP.


  REFRESH lt_alv[].
  CALL FUNCTION 'ZARN_READ_CHANGE_DOCUMENT_DATA'
    EXPORTING
      iv_reg          = p_reg
      iv_chg          = p_chg
      ir_idno_id      = lr_idno_id[]
      ir_chgid_id     = lr_chgid_id[]
      ir_idno         = s_idnocc[]
      ir_changenr     = s_chgnr[]
      ir_chg_category = s_chg[]
      ir_udate        = s_udate[]
      ir_username     = s_uname[]
    IMPORTING
      et_data         = lt_alv[]
    EXCEPTIONS
      input_required  = 1
      no_data         = 2
      OTHERS          = 3.


END-OF-SELECTION.

  IF lt_alv[] IS INITIAL.
    MESSAGE 'No data to display' TYPE 'S' DISPLAY LIKE 'E'.
  ELSE.

    SORT lt_alv[] BY udate DESCENDING
                     utime DESCENDING
                     idno ASCENDING.

* Call the screen 9000 for displaying ALV grid.
    CALL SCREEN 9000.
  ENDIF.
