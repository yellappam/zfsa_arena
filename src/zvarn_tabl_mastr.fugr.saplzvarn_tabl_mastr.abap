* regenerated at 25.02.2016 15:08:40 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZVARN_TABL_MASTRTOP.              " Global Data
  INCLUDE LZVARN_TABL_MASTRUXX.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZVARN_TABL_MASTRF...              " Subroutines
* INCLUDE LZVARN_TABL_MASTRO...              " PBO-Modules
* INCLUDE LZVARN_TABL_MASTRI...              " PAI-Modules
* INCLUDE LZVARN_TABL_MASTRE...              " Events
* INCLUDE LZVARN_TABL_MASTRP...              " Local class implement.
* INCLUDE LZVARN_TABL_MASTRT99.              " ABAP Unit tests
  INCLUDE LZVARN_TABL_MASTRF00                    . " subprograms
  INCLUDE LZVARN_TABL_MASTRI00                    . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
