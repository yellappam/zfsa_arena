FUNCTION zarn_article_posting.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(IT_KEY) TYPE  ZTARN_KEY OPTIONAL
*"     VALUE(IR_STATUS) TYPE  ZTARN_STATUS_RANGE OPTIONAL
*"     VALUE(IR_FRMDAT) TYPE  ZTARN_DATE_RANGE OPTIONAL
*"     VALUE(IV_BATCH) TYPE  FLAG DEFAULT SPACE
*"     VALUE(IV_DISPLAY_LOG) TYPE  BOOLE_D DEFAULT SPACE
*"     VALUE(IV_NO_CHANGE_DOC) TYPE  FLAG DEFAULT SPACE
*"     VALUE(IV_COMMIT) TYPE  BOOLE_D DEFAULT 'X'
*"  EXPORTING
*"     VALUE(ET_MESSAGE) TYPE  BAL_T_MSG
*"----------------------------------------------------------------------
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 27/11/2019
* CHANGE No.       # Charm 9000006356; Jira CIP-148
* DESCRIPTION      # Added parameter to control commit work.
* WHO              # C90001929, Aruna Mackenjee
*-----------------------------------------------------------------------

  DATA: lt_key             TYPE ty_t_key,
        "ls_key            TYPE zsarn_key,
        lr_status          TYPE ztarn_status_range,
        lr_frmdat          TYPE ztarn_date_range,
        ls_frmdat          TYPE zsarn_date_range,
        ls_status          TYPE zsarn_status_range,
        lv_batch           TYPE flag,

        lt_fan_id          TYPE ty_t_fan_id,
        lt_ver_status      TYPE ty_t_ver_status,
        "ls_ver_status     TYPE ty_s_ver_status,
        lt_key_valid       TYPE ty_t_key,
        "ls_key_valid      TYPE zsarn_key,
        lt_message         TYPE bal_t_msg,
        "ls_message        TYPE bal_s_msg,

        lt_prd_version     TYPE ty_t_prd_version,
        "ls_prd_version    TYPE ty_t_prd_version,
        lt_reg_hdr         TYPE ty_t_reg_hdr,
        "ls_reg_hdr        TYPE ty_t_reg_hdr,
        lt_products        TYPE ty_t_products,
*        ls_products       type ty_s_products,

        lt_idno_data       TYPE ty_t_idno_data,
        ls_idno_data       TYPE ty_s_idno_data,
        lt_idno_data_err   TYPE ty_t_idno_data,
        lt_idno_data_suc   TYPE ty_t_idno_data,

        ls_prod_data_all   TYPE zsarn_prod_data,
        ls_reg_data_all    TYPE zsarn_reg_data,

        lt_prod_data       TYPE ztarn_prod_data,
        ls_prod_data       TYPE zsarn_prod_data,
        lt_reg_data        TYPE ztarn_reg_data,
        ls_reg_data        TYPE zsarn_reg_data,

        lt_mara            TYPE ty_t_mara,
        lt_t006            TYPE ty_t_t006,
        lt_t005            TYPE ty_t_t005,
        lt_tcurc           TYPE ty_t_tcurc,
        lt_coo_t           TYPE ty_t_coo_t,
        lt_gen_uom_t       TYPE ty_t_gen_uom_t,
        lt_t134            TYPE ty_t_t134,
        "lt_tvarvc_ekorg    TYPE tvarvc_t,            "IS 2019/06
        lt_pir_ekorg       TYPE zarn_t_pir_pur_cnf,         "IS 2019/06
        lt_tvarvc          TYPE tvarvc_t,
        lt_tvarvc_hyb_code TYPE tvarvc_t,
        lt_tvta            TYPE tvta_tt,
        lt_marm            TYPE ty_t_marm,
        lt_mean            TYPE ty_t_mean,
        lt_eina            TYPE ty_t_eina,
        lt_eine            TYPE ty_t_eine,
        lt_eine_site       TYPE ty_t_eine,       "++3181 JKH 19.05.2017
        lt_maw1            TYPE ty_t_maw1,
        lt_host_sap        TYPE ty_t_host_sap,
        lt_uom_cat         TYPE ty_t_uom_cat,
        ls_matgrp_hier     TYPE ty_s_matgrp_hier,
        lt_matgrp_prod     TYPE ty_t_matgrp_prod,
        lt_art_hier        TYPE ty_t_art_hier,
        lt_rsparams        TYPE rsparams_tt,
        ls_rsparams        TYPE rsparams,
        lv_variant_def     TYPE raldb_vari,
        ls_log_params      TYPE zsarn_slg1_log_params_art_post,
        lv_ekorg_main      TYPE ekorg,
        lv_error           TYPE flag.

*>>>IS1609ins Feature 3162 - AReNa Approval Stabilisation

  DATA: lo_approval TYPE REF TO zcl_arn_approval_backend,
        lt_idno     TYPE ztarn_idno_key.

*<<<IS1609ins Feature 3162 - AReNa Approval Stabilisation


  lt_key[]    = it_key[].
  lr_status[] = ir_status[].
  lr_frmdat[] = ir_frmdat[].
  lv_batch    = iv_batch.



  IF iv_batch IS INITIAL AND lt_key[] IS INITIAL.

* Build Return Message

    CLEAR: ls_log_params.

    ls_log_params-batch_mode = iv_batch.

* IDNO and Current Version are required when posting online
    PERFORM build_message USING gc_message-error
                                gc_message-id
                                '020'
                                space
                                space
                                space
                                space
                                ls_log_params
                       CHANGING lt_message[].

    IF 1 = 2. MESSAGE e020(zarena_msg). ENDIF.
    lv_error = abap_true.

    et_message[] = lt_message[].
    EXIT.
  ENDIF.



  IF lr_status[] IS INITIAL.

    CLEAR lv_variant_def.
    SELECT SINGLE low
      INTO lv_variant_def
      FROM tvarvc
     WHERE name = 'ZARN_ARTICLE_POST_DEF_VARIANT'
       AND type = 'P'.
    IF sy-subrc EQ 0.
      CALL FUNCTION 'RS_VARIANT_CONTENTS'
        EXPORTING
          report               = 'ZRARN_ARTICLE_UPDATE'
          variant              = lv_variant_def
        TABLES
          valutab              = lt_rsparams[]
        EXCEPTIONS
          variant_non_existent = 1
          variant_obsolete     = 2
          OTHERS               = 3.
      IF sy-subrc EQ 0.
        LOOP AT lt_rsparams INTO ls_rsparams
         WHERE selname = 'S_STATUS'.

          CLEAR ls_status.
          ls_status-sign   = ls_rsparams-sign.
          ls_status-option = ls_rsparams-option.
          ls_status-low    = ls_rsparams-low.
          ls_status-high   = ls_rsparams-high.
          APPEND ls_status TO lr_status[].
        ENDLOOP.
      ENDIF.
    ENDIF.

    IF lr_status[] IS INITIAL.
* Default Status to 'APR' and 'CRE' if not provided
      CLEAR ls_status.
      ls_status-sign   = gc_range-sign_i.
      ls_status-option = gc_range-option_eq.
      ls_status-low    = gc_status-apr.   " 'APR'
      APPEND ls_status TO lr_status[].
    ENDIF.
  ENDIF.  " IF lr_status[] IS INITIAL



  IF lr_frmdat[] IS INITIAL.
    CLEAR ls_frmdat.
    ls_frmdat-sign   = 'I'.
    ls_frmdat-option = 'BT'.
    ls_frmdat-low    = sy-datum - 1.
    ls_frmdat-high   = sy-datum.
    APPEND ls_frmdat TO lr_frmdat[].
  ENDIF.  " IF lr_frmdat[] IS INITIAL.



  CLEAR: lt_message[], lv_error, lt_key_valid[], lt_ver_status[],
         lt_prd_version[], lt_reg_hdr[].

  IF lt_key[] IS NOT INITIAL.

    DELETE ADJACENT DUPLICATES FROM lt_key[] COMPARING ALL FIELDS.

* Validate Provided IDNO and Version
    PERFORM validate_key USING lt_key[]
                               lv_batch
                      CHANGING lt_ver_status[]
                               lt_key_valid[]
                               lt_fan_id[]
                               lt_message[]
                               lv_error.
    IF lt_key_valid[] IS INITIAL.
      et_message[] = lt_message[].
      EXIT.
    ENDIF.


    CLEAR: lt_key[]. FREE: lt_key[].

    lt_key[] = lt_key_valid[].

    CLEAR: lt_key_valid[]. FREE: lt_key_valid[].

* Check Approval Status - if status is 'APR' in National /Regional Tables
    PERFORM check_approval_status USING lt_key[]
                                        lr_status[]
                                        lt_fan_id[]
                                        lv_batch
                               CHANGING lt_prd_version[]
                                        lt_reg_hdr[]
                                        lt_fan_id[]
                                        lt_key_valid[]
                                        lt_message[]
                                        lv_error.

  ELSE.

* Get Approved Keys - if status is 'APR' in National /Regional Tables
    PERFORM get_approved_keys USING lr_status[]
                                    lr_frmdat[]
                                    lv_batch
                           CHANGING lt_ver_status[]
                                    lt_prd_version[]
                                    lt_reg_hdr[]
                                    lt_fan_id[]
                                    lt_key_valid[]
                                    lt_message[]
                                    lv_error.

  ENDIF.  " IF lt_key[] IS NOT INITIAL


  IF lt_key_valid[] IS INITIAL.
    et_message[] = lt_message[].
    EXIT.
  ENDIF.

* Build IDNO Data
  PERFORM build_idno_data USING lt_key_valid[]
                                lt_ver_status[]
                                lt_prd_version[]
                                lt_fan_id[]
                                lt_reg_hdr[]
                       CHANGING lt_idno_data[]
                                lt_products[]
                                lt_mara[].

* Get National and Regional Data
  CLEAR: lt_prod_data[], lt_reg_data[].
  PERFORM get_nat_reg_data USING lt_idno_data[]
                        CHANGING lt_prod_data[]
                                 lt_reg_data[]
                                 ls_prod_data_all
                                 ls_reg_data_all.


* Get SAP data from DB
  PERFORM get_sap_data USING lt_idno_data[]
                             ls_prod_data_all
                             ls_reg_data_all
                             lt_mara[]
                    CHANGING lt_marm[]
                             lt_mean[]
                             lt_eina[]
                             lt_eine[]
                             lt_eine_site[]                             "++3181 JKH 19.05.2017
                             lt_maw1[]
                             lt_host_sap[]
                             lt_t006[]
                             lt_t005[]
                             lt_tcurc[]
                             lt_coo_t[]
                             lt_gen_uom_t[]
                             lt_t134[]
                             lt_pir_ekorg                   "IS 2019/06
                             lv_ekorg_main
                             lt_tvarvc[]
                             lt_tvarvc_hyb_code[]
                             lt_tvta[]
                             lt_uom_cat[]
                             ls_matgrp_hier
                             lt_matgrp_prod[]
                             lt_art_hier[].

* Build Mapping Data and Post Article
  PERFORM build_mapping_data_and_post USING lt_idno_data[]
                                            lt_prod_data[]
                                            lt_reg_data[]
                                            lr_status[]
                                            lt_mara[]
                                            lt_marm[]
                                            lt_mean[]
                                            lt_eina[]
                                            lt_eine[]
                                            lt_eine_site[]                             "++3181 JKH 19.05.2017
                                            lt_maw1[]
                                            lt_host_sap[]
                                            lt_t006[]
                                            lt_t005[]
                                            lt_tcurc[]
                                            lt_coo_t[]
                                            lt_gen_uom_t[]
                                            lt_t134[]
                                            "lt_tvarvc_ekorg[]                        "IS 2019/06
                                            lt_pir_ekorg    "IS 2019/06
                                            lt_tvarvc[]
                                            lt_tvarvc_hyb_code[]
                                            lt_tvta[]
                                            lt_uom_cat[]
                                            ls_matgrp_hier
                                            lt_matgrp_prod[]
                                            lt_art_hier[]
                                            lv_ekorg_main
                                            lv_batch
                                   CHANGING lt_message[]
                                            lv_error
                                            lt_idno_data_err
                                            lt_idno_data_suc.


  et_message[] = lt_message[].

* If no article is sucessfully posted, then don't update AReNa tables
  IF lt_idno_data_suc[] IS NOT INITIAL.
*   Update National/Regional status and Version
    PERFORM update_nat_reg_status_version USING lt_prd_version[]
                                                lt_ver_status[]
                                                ls_reg_data_all-zarn_reg_hdr[]
                                                lt_idno_data_suc[]
                                                lr_status[]
                                                lv_error
                                                iv_no_change_doc.

*>>> IS 2019/02 LRA 3a - automatic ZN blocking / unblocking for DC supplied articles

    "this one can create records in ZMM_SUPPLY_BLOCK table for DC sourced articles
    DATA(lt_msg_zn) = zcl_arn_automatic_dc_block=>get_instance( )->automatic_supply_block_create(
                                                     it_reg_data = lt_reg_data
                                                     it_proc_idno = CONV #( lt_idno_data_suc ) ).
    APPEND LINES OF lt_msg_zn TO et_message.
*<<< IS 2019/02 LRA 3a - automatic ZN blocking / unblocking for DC supplied articles


*>>>IS1609ins Feature 3162 - AReNa Approval Stabilisation
    lt_idno = CORRESPONDING #( lt_idno_data_suc  ).


    lo_approval = NEW #( ).
    TRY.
        lo_approval->change_appr_from_artpost_bulk( EXPORTING it_idno = lt_idno
                                                    IMPORTING et_zarn_approval_ch_del = DATA(lt_approval_ch_del)
                                                              et_zarn_approval = DATA(lt_approval)
                                                              et_zarn_appr_hist = DATA(lt_appr_hist) ).



        lo_approval->post_approvals( EXPORTING it_approval = lt_approval
                                               it_approval_ch_del = lt_approval_ch_del
                                               it_appr_hist = lt_appr_hist ).


      CATCH zcx_excep_cls.
        PERFORM build_message USING gc_message-error
                            gc_message-id
                            '099'
                            space
                            space
                            space
                            space
                            ls_log_params
                   CHANGING et_message[].
    ENDTRY.

*<<<IS1609ins Feature 3162 - AReNa Approval Stabilisation

  ENDIF.

  IF iv_display_log EQ abap_true.
    PERFORM popup_appl_log USING et_message.
  ENDIF.

  IF iv_commit = abap_true.
*>>>IS1609ins Feature 3162 - AReNa Approval Stabilisation
    COMMIT WORK AND WAIT.
*<<<IS1609ins Feature 3162 - AReNa Approval Stabilisation
  ENDIF.


ENDFUNCTION.
