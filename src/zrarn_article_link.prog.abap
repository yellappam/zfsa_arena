*&---------------------------------------------------------------------*
*& Report  ZARN_DATA_RECONCILIATION
*&
*&---------------------------------------------------------------------*
*&---------------------------------------------------------------------*
* PROJECT          # FSA
* SPECIFICATION    # ONED-217
* DATE WRITTEN     # 23.11.2016
* SYSTEM           # DE0
* SAP VERSION      # ECC6.0
* TYPE             # Report
* AUTHOR           # C90001363
*-----------------------------------------------------------------------
* TITLE            # Article Link Display
* PURPOSE          # To show the linking of an article
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      #
*-----------------------------------------------------------------------
* CALLS TO         #
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

REPORT zrarn_article_link MESSAGE-ID zarena_msg.

* Top Include for Declarations
INCLUDE ziarn_article_link_top.

* Selection Screen
INCLUDE ziarn_article_link_sel.

* Main Processing Logic
INCLUDE ziarn_article_link_main.

* Subroutines
INCLUDE ziarn_article_link_sub.

* PBO
INCLUDE ziarn_article_link_pbo.

* PAI
INCLUDE ziarn_article_link_pai.
