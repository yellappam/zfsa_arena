*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 18.10.2016 at 09:11:46
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_PARAM......................................*
DATA:  BEGIN OF STATUS_ZARN_PARAM                    .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_PARAM                    .
CONTROLS: TCTRL_ZARN_PARAM
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_PARAM                    .
TABLES: ZARN_PARAM                     .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
