*----------------------------------------------------------------------*
***INCLUDE LZARN_INBOUNDF01 .
*----------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*&      Form  BUILD_LOG_PARAMETERS
*&---------------------------------------------------------------------*
* Build Log Parameters
*----------------------------------------------------------------------*
FORM build_log_parameters USING fu_t_dfies          TYPE dfies_tab
                                fu_s_master_product TYPE zmaster_products_product
                                fu_s_admin          TYPE zmaster_products_admin
                                fu_v_guid           TYPE zarn_guid
                                fu_t_prod_uom_t     TYPE ty_t_prod_uom_t
                       CHANGING fc_s_params         TYPE bal_s_parm
                                fc_s_log_params     TYPE zsarn_slg1_log_params_inbound.


  DATA: ls_dfies      TYPE dfies,
        lt_par        TYPE bal_t_par,
        ls_par        TYPE bal_s_par,
        ls_log_params TYPE zsarn_slg1_log_params_inbound,
        ls_prod_uom_t TYPE ty_s_prod_uom_t,

        lt_nat_mchier TYPE zmaster_products_national__tab,
        ls_nat_mchier TYPE zmaster_products_national_merc,
        lt_uom_var    TYPE zmaster_products_uomvarian_tab,
        ls_uom_var    TYPE zmaster_products_uomvariant,
        lt_pir        TYPE zmaster_products_purchasin_tab,
        ls_pir        TYPE zmaster_products_purchasing_in,
        lt_gtin       TYPE zmaster_products_gtinvaria_tab,
        ls_gtin       TYPE zmaster_products_gtinvariant.

  FIELD-SYMBOLS: <param>    TYPE any.

  CLEAR: ls_log_params, fc_s_log_params.
  ls_log_params-guid       = fu_v_guid.
  ls_log_params-created_on = sy-datum.
  ls_log_params-created_at = sy-uzeit.
  ls_log_params-created_by = sy-uname.
  ls_log_params-fan_id     = fu_s_master_product-fan.
  ls_log_params-arn_mode   = fu_s_admin-operation_mode.
  ls_log_params-code       = fu_s_master_product-code.

* MATKL
  lt_nat_mchier[] = fu_s_master_product-nat_merchandise_hierarchies-national_merchandise_hierarchy[].
  CLEAR ls_nat_mchier.
  READ TABLE lt_nat_mchier[] INTO ls_nat_mchier INDEX 1.
  IF sy-subrc = 0.
    ls_log_params-matkl = ls_nat_mchier-code.
  ENDIF.



  CLEAR: lt_pir[], lt_uom_var[].
  lt_uom_var[] = fu_s_master_product-uomvariants-uomvariant[].

  LOOP AT lt_uom_var INTO ls_uom_var.
    APPEND LINES OF ls_uom_var-purch_info_records-purchasing_info_record[] TO lt_pir[].
  ENDLOOP.

  SORT lt_pir[] BY gln_pir-gln.
  DELETE ADJACENT DUPLICATES FROM lt_pir[] COMPARING gln_pir-gln.



  ls_log_params-name       = fu_s_master_product-name.
  ls_log_params-descr      = fu_s_master_product-description.
  ls_log_params-fs_descr_s = fu_s_master_product-fsshort_description.
  ls_log_params-brandid    = fu_s_master_product-brand.
  ls_log_params-sub_brndid = fu_s_master_product-sub_brand.
  ls_log_params-fs_brandid = fu_s_master_product-fsbrand.
  ls_log_params-fs_brn_dsc = fu_s_master_product-fsbrand_desc.
  ls_log_params-fs_sbbrnid = fu_s_master_product-fssub_brand.
  ls_log_params-fs_sbbrnds = fu_s_master_product-fssub_brand_desc.

  TRANSLATE ls_log_params-name       TO UPPER CASE.
  TRANSLATE ls_log_params-descr      TO UPPER CASE.
  TRANSLATE ls_log_params-fs_descr_s TO UPPER CASE.
  TRANSLATE ls_log_params-brandid    TO UPPER CASE.
  TRANSLATE ls_log_params-sub_brndid TO UPPER CASE.
  TRANSLATE ls_log_params-fs_brandid TO UPPER CASE.
  TRANSLATE ls_log_params-fs_brn_dsc TO UPPER CASE.
  TRANSLATE ls_log_params-fs_sbbrnid TO UPPER CASE.
  TRANSLATE ls_log_params-fs_sbbrnds TO UPPER CASE.


* VENDOR NAME
  LOOP AT lt_pir[] INTO ls_pir.

    TRANSLATE ls_pir-gln_pir-description TO UPPER CASE.

    IF ls_log_params-vend_name IS INITIAL.
      ls_log_params-vend_name = ls_pir-gln_pir-description.
    ELSE.
      CONCATENATE ls_log_params-vend_name ls_pir-gln_pir-description
             INTO ls_log_params-vend_name SEPARATED BY '/'.
    ENDIF.
  ENDLOOP.

  IF ls_log_params-vend_name CS '/'.
    CONCATENATE ls_log_params-vend_name '/' INTO ls_log_params-vend_name.
  ENDIF.


* BARCODES
  LOOP AT fu_t_prod_uom_t INTO ls_prod_uom_t.

    TRANSLATE ls_prod_uom_t-sap_uom_cat TO UPPER CASE.

    CASE ls_prod_uom_t-sap_uom_cat.

      WHEN 'RETAIL'.
        CLEAR ls_uom_var.
        LOOP AT lt_uom_var[] INTO ls_uom_var WHERE uom = ls_prod_uom_t-product_uom.

          CLEAR lt_gtin[].
          lt_gtin[] = ls_uom_var-gtinvariants-gtinvariant[].

          LOOP AT lt_gtin[] INTO ls_gtin.
            IF ls_log_params-ret_bcode IS INITIAL.
              ls_log_params-ret_bcode = ls_gtin-code.
            ELSE.
              CONCATENATE ls_log_params-ret_bcode ls_gtin-code
                     INTO ls_log_params-ret_bcode SEPARATED BY '/'.
            ENDIF.
          ENDLOOP.  " LOOP AT lt_gtin[] INTO ls_gtin.
        ENDLOOP.  " LOOP AT lt_uom_var[] INTO ls_uom_var

      WHEN 'INNER'.
        CLEAR ls_uom_var.
        LOOP AT lt_uom_var[] INTO ls_uom_var WHERE uom = ls_prod_uom_t-product_uom.

          CLEAR lt_gtin[].
          lt_gtin[] = ls_uom_var-gtinvariants-gtinvariant[].

          LOOP AT lt_gtin[] INTO ls_gtin.
            IF ls_log_params-inn_bcode IS INITIAL.
              ls_log_params-inn_bcode = ls_gtin-code.
            ELSE.
              CONCATENATE ls_log_params-inn_bcode ls_gtin-code
                     INTO ls_log_params-inn_bcode SEPARATED BY '/'.
            ENDIF.
          ENDLOOP.  " LOOP AT lt_gtin[] INTO ls_gtin.
        ENDLOOP.  " LOOP AT lt_uom_var[] INTO ls_uom_var

      WHEN 'SHIPPER'.
        CLEAR ls_uom_var.
        LOOP AT lt_uom_var[] INTO ls_uom_var WHERE uom = ls_prod_uom_t-product_uom.

          CLEAR lt_gtin[].
          lt_gtin[] = ls_uom_var-gtinvariants-gtinvariant[].

          LOOP AT lt_gtin[] INTO ls_gtin.
            IF ls_log_params-ship_bcode IS INITIAL.
              ls_log_params-ship_bcode = ls_gtin-code.
            ELSE.
              CONCATENATE ls_log_params-ship_bcode ls_gtin-code
                     INTO ls_log_params-ship_bcode SEPARATED BY '/'.
            ENDIF.
          ENDLOOP.  " LOOP AT lt_gtin[] INTO ls_gtin.
        ENDLOOP.  " LOOP AT lt_uom_var[] INTO ls_uom_var

    ENDCASE.
  ENDLOOP.  " LOOP AT fu_t_prod_uom_t INTO ls_prod_uom_t


  IF ls_log_params-ret_bcode CS '/'.
    CONCATENATE ls_log_params-ret_bcode  '/' INTO ls_log_params-ret_bcode.
  ENDIF.

  IF ls_log_params-inn_bcode CS '/'.
    CONCATENATE ls_log_params-inn_bcode   '/' INTO ls_log_params-inn_bcode.
  ENDIF.

  IF ls_log_params-ship_bcode CS '/'.
    CONCATENATE ls_log_params-ship_bcode  '/' INTO ls_log_params-ship_bcode.
  ENDIF.




  fc_s_log_params = ls_log_params.



  REFRESH: lt_par[].
  CLEAR: fc_s_params.

  LOOP AT fu_t_dfies INTO ls_dfies.

    ASSIGN COMPONENT ls_dfies-fieldname OF STRUCTURE fc_s_log_params TO <param>.

*    IF <param> IS NOT INITIAL.
    CLEAR ls_par.
    ls_par-parname  = ls_dfies-fieldname.
    ls_par-parvalue = <param>.
    APPEND ls_par TO lt_par[].
*    ENDIF.
  ENDLOOP.

  fc_s_params-t_par[] = lt_par[].
  fc_s_params-altext = 'ZARN_INBOUND'.

ENDFORM.                    " BUILD_LOG_PARAMETERS
*&---------------------------------------------------------------------*
*&      Form  BUILD_MESSAGE
*&---------------------------------------------------------------------*
* Build Return Message
*----------------------------------------------------------------------*
FORM build_message USING fu_v_type       TYPE sy-msgty
                         fu_v_id         TYPE sy-msgid
                         fu_v_number     TYPE sy-msgno
                         fu_v_message_v1 TYPE symsgv
                         fu_v_message_v2 TYPE symsgv
                         fu_v_message_v3 TYPE symsgv
                         fu_v_message_v4 TYPE symsgv
                         fu_v_params     TYPE bal_s_parm
                CHANGING fc_t_message    TYPE bal_t_msg.

  DATA: ls_message      TYPE bal_s_msg.

  CLEAR ls_message.
  ls_message-msgty  = fu_v_type.
  ls_message-msgid  = fu_v_id.
  ls_message-msgno  = fu_v_number.
  ls_message-msgv1  = fu_v_message_v1.
  ls_message-msgv2  = fu_v_message_v2.
  ls_message-msgv3  = fu_v_message_v3.
  ls_message-msgv4  = fu_v_message_v4.
  ls_message-params = fu_v_params.

  APPEND ls_message TO fc_t_message[].

ENDFORM.                    " BUILD_MESSAGE
*&---------------------------------------------------------------------*
*&      Form  CHECK_OVR_MODE_VERSION
*&---------------------------------------------------------------------*
* Check 'OVR' Mode Version existence - overwrite mode
*----------------------------------------------------------------------*
FORM check_ovr_mode_version USING fu_v_idno           TYPE zarn_idno
                                  fu_t_ovr_mode_range TYPE ty_t_ovr_mode_range
                         CHANGING fc_v_continue       TYPE flag
                                  fc_t_ovr_version    TYPE ty_t_ovr_version.

  DATA: ls_ovr_version    TYPE ty_s_ovr_version.

  CLEAR fc_v_continue.

* Check if a version already exist for given 'OVR' mode IDNO
* If version exist and is not 'OVR' mode then dont create national data fo this IDNO
  CLEAR: ls_ovr_version.
  SELECT SINGLE b~idno a~previous_ver a~current_ver a~latest_ver a~article_ver b~id_type
     FROM zarn_ver_status  AS a
     JOIN zarn_prd_version AS b
      ON a~idno       = b~idno
     AND a~latest_ver = b~version
    INTO CORRESPONDING FIELDS OF ls_ovr_version
   WHERE a~idno = fu_v_idno.                         "#EC CI_SEL_NESTED
  IF sy-subrc = 0.
    IF ls_ovr_version-id_type NOT IN fu_t_ovr_mode_range[].
      fc_v_continue = abap_true.
*    ELSE.
    ENDIF.
    INSERT ls_ovr_version INTO TABLE fc_t_ovr_version[].
*    ENDIF.
  ENDIF.


ENDFORM.                    " CHECK_OVR_MODE_VERSION
*&---------------------------------------------------------------------*
*&      Form  RETREIVE_IDNO_DATA
*&---------------------------------------------------------------------*
* Retreive IDNO data for curent version, if any
*----------------------------------------------------------------------*
FORM retreive_idno_data USING fu_t_idno            TYPE ztarn_national_lock
                     CHANGING fc_t_field_confg     TYPE ty_t_field_confg
                              fc_t_table_mastr     TYPE ty_t_table_mastr
                              fc_t_tabname         TYPE ty_t_tabname
                              fc_t_ver_status_all  TYPE ty_t_ver_status
                              fc_t_ver_status_o    TYPE ty_t_ver_status
                              fc_t_prd_version_o   TYPE ty_t_prd_version
                              fc_t_control_o       TYPE ty_t_control
                              fc_t_prod_data_o     TYPE ty_t_prod_data
                              fc_t_prod_data_db    TYPE ty_t_prod_data
                              fc_t_prod_data_curr  TYPE ty_t_prod_data.

  DATA: ls_tabname      TYPE ty_s_tabname,
        ls_prod_data    TYPE ty_s_prod_data,
        ls_prod_data_o  TYPE ty_s_prod_data,
        ls_prod_data_db TYPE ty_s_prod_data,
        ls_ver_status   TYPE ty_s_ver_status,
        ls_ver_status_o TYPE ty_s_ver_status,
        lv_itabname     TYPE char50,
        lv_where_str    TYPE string.

  FIELD-SYMBOLS: <ls_prod_data_o> TYPE ty_s_prod_data,
                 <tabname>        TYPE tabname,
                 <itabname>       TYPE table,
                 <table_o>        TYPE table,
                 <table_db>       TYPE table,
                 <table>          TYPE table,
                 <table_str>      TYPE any,
                 <table_val>      TYPE any.

  REFRESH: fc_t_field_confg[], fc_t_table_mastr[], fc_t_tabname[], fc_t_prod_data_o[],
           fc_t_prod_data_db[], fc_t_ver_status_o[], fc_t_prd_version_o[], fc_t_control_o[],
           fc_t_ver_status_all[], fc_t_prod_data_curr[].

  SELECT b~*                                           "#EC CI_BUFFJOIN
    INTO CORRESPONDING FIELDS OF TABLE @fc_t_field_confg[]
    FROM zarn_table_mastr AS a
    JOIN zarn_field_confg AS b
      ON a~arena_table  = b~tabname
   WHERE a~arena_table_type EQ @gc_arn_tab_typ-nat.  " 'N' National Table

  IF gv_feature_a4_enabled EQ abap_false.
    DELETE fc_t_field_confg WHERE tabname EQ 'ZARN_ONLCAT'.
    DELETE fc_t_field_confg WHERE tabname EQ 'ZARN_UOMVARIANT' AND fldname EQ 'NET_CONTENT_SHORT_DESC'.
    DELETE fc_t_field_confg WHERE tabname EQ 'ZARN_UOMVARIANT' AND fldname EQ 'FSCUST_PROD_NAME40'.
    DELETE fc_t_field_confg WHERE tabname EQ 'ZARN_UOMVARIANT' AND fldname EQ 'FSCUST_PROD_NAME80'.
    DELETE fc_t_field_confg WHERE tabname EQ 'ZARN_UOMVARIANT' AND fldname EQ 'FSCUST_PROD_NAME255'.
    DELETE fc_t_field_confg WHERE tabname EQ 'ZARN_UOMVARIANT' AND fldname EQ 'PKGING_RCYCLING_SCHEME_CODE'.
    DELETE fc_t_field_confg WHERE tabname EQ 'ZARN_UOMVARIANT' AND fldname EQ 'PKGING_RCYCLING_PROCESS_TYPE_C'.
    DELETE fc_t_field_confg WHERE tabname EQ 'ZARN_UOMVARIANT' AND fldname EQ 'SUSTAINABILITY_FEATURE_CODE'.
    DELETE fc_t_field_confg WHERE tabname EQ 'ZARN_BENEFITS'.
  ENDIF.

* Get Table Master Data
  SELECT * FROM zarn_table_mastr
    INTO CORRESPONDING FIELDS OF TABLE fc_t_table_mastr[]
    WHERE arena_table_type EQ gc_arn_tab_typ-nat.  " 'N' National Table

* Get Tables to be checked for Version Management
  SELECT DISTINCT a~arena_table AS tabname
    INTO CORRESPONDING FIELDS OF TABLE fc_t_tabname[]
    FROM zarn_table_mastr AS a
    JOIN zarn_field_confg AS b
      ON a~arena_table  = b~tabname
   WHERE a~arena_table_type EQ gc_arn_tab_typ-nat  " 'N' National Table
     AND b~version_compare  EQ abap_true.              "#EC CI_BUFFJOIN


* Get Product Existing Version info for all versions, if any
  SELECT * FROM zarn_ver_status
    INTO CORRESPONDING FIELDS OF TABLE fc_t_ver_status_all[]
    FOR ALL ENTRIES IN fu_t_idno[]
    WHERE idno EQ fu_t_idno-idno.

  IF fc_t_ver_status_all[] IS NOT INITIAL.
* Get Product Article Version Details
    SELECT * FROM zarn_prd_version
      INTO CORRESPONDING FIELDS OF TABLE fc_t_prd_version_o[]
      FOR ALL ENTRIES IN fc_t_ver_status_all[]
      WHERE idno    EQ fc_t_ver_status_all-idno.
*        AND version EQ fc_t_ver_status_all-article_ver.
    IF fc_t_prd_version_o[] IS NOT INITIAL.

* Get Control Records for each Product and Article Versions
      SELECT * FROM zarn_control
        INTO CORRESPONDING FIELDS OF TABLE fc_t_control_o[]
        FOR ALL ENTRIES IN fc_t_prd_version_o[]
        WHERE guid EQ fc_t_prd_version_o-guid.
    ENDIF.
  ENDIF.

* INS Begin of Change JKH 13.09.2016
* Compare with current version if article version doesn't exist
* with status APR only

* Get National Data for Current Version
  PERFORM get_current_version_data USING fc_t_ver_status_all[]
                                         fc_t_prd_version_o[]
                                         fc_t_tabname[]
                                         fc_t_field_confg[]
                                CHANGING fc_t_prod_data_curr[]
                                         fc_t_prod_data_db[].


* INS End of Change JKH 13.09.2016



* Get Product Existing Version info, if any
  SELECT * FROM zarn_ver_status
    INTO CORRESPONDING FIELDS OF TABLE fc_t_ver_status_o[]
    FOR ALL ENTRIES IN fu_t_idno[]
    WHERE idno        EQ fu_t_idno-idno
      AND article_ver NE space.
  IF fc_t_ver_status_o[] IS INITIAL.
* If all the Products are comming for first time, then there won't be any existing version data
* Version ONE will be created for all the Products
    EXIT.
  ENDIF.

* Retreive data for Product Article Version - All tables
  CLEAR : ls_prod_data.
  LOOP AT fc_t_tabname INTO ls_tabname.

    IF <tabname>  IS ASSIGNED. UNASSIGN <tabname>.  ENDIF.
    IF <itabname> IS ASSIGNED. UNASSIGN <itabname>. ENDIF.

    ASSIGN ls_tabname-tabname TO <tabname>.


** Select Data for all the Product tables
    CLEAR: lv_itabname.
    CONCATENATE 'LS_PROD_DATA-' ls_tabname-tabname INTO lv_itabname.
    ASSIGN (lv_itabname) TO <itabname>.

    SELECT * FROM (<tabname>)
      INTO CORRESPONDING FIELDS OF TABLE <itabname>
        FOR ALL ENTRIES IN fc_t_ver_status_o[]
        WHERE idno    EQ fc_t_ver_status_o-idno
          AND version EQ fc_t_ver_status_o-article_ver. "#EC CI_SEL_NESTED

  ENDLOOP. " LOOP AT fc_t_tabname INTO ls_tabname.




** For all the data retreived for Product
* Build data as per IDNO and Version
* Fill fields in internal tables for each IDNO and Version which are configured
*   to be compared (version_compare is TRUE)
* Primary Keys will not be compared even if they are configured - as this is just
* to know if any attribute is changed or not. Actual change determination is done in
* change category determination
  LOOP AT fc_t_ver_status_o[] INTO ls_ver_status_o.

* Build data as per IDNO and Version
    CLEAR: ls_prod_data_o.
    ls_prod_data_o-idno    = ls_ver_status_o-idno.
    ls_prod_data_o-version = ls_ver_status_o-article_ver.


    CLEAR: ls_prod_data_db.
    ls_prod_data_db-idno    = ls_ver_status_o-idno.
    ls_prod_data_db-version = ls_ver_status_o-article_ver.

    LOOP AT fc_t_tabname INTO ls_tabname.

      IF <table_o>    IS ASSIGNED. UNASSIGN <table_o>.    ENDIF.
      IF <table_db>   IS ASSIGNED. UNASSIGN <table_db>.    ENDIF.
      IF <table>      IS ASSIGNED. UNASSIGN <table>.      ENDIF.
      IF <table_str>  IS ASSIGNED. UNASSIGN <table_str>.  ENDIF.
      IF <table_val>  IS ASSIGNED. UNASSIGN <table_val>.  ENDIF.

      CLEAR: lv_itabname.
      CONCATENATE 'LS_PROD_DATA_O-' ls_tabname-tabname INTO lv_itabname.
      ASSIGN (lv_itabname) TO <table_o>.

      CLEAR: lv_itabname.
      CONCATENATE 'LS_PROD_DATA_DB-' ls_tabname-tabname INTO lv_itabname.
      ASSIGN (lv_itabname) TO <table_db>.

      CLEAR: lv_itabname.
      CONCATENATE 'LS_PROD_DATA-' ls_tabname-tabname INTO lv_itabname.
      ASSIGN (lv_itabname) TO <table>.

* Build WHERE clause string as data has to be retreived for IDNO and VERSION only
      CLEAR lv_where_str.
*      CONCATENATE 'IDNO EQ ' ls_ver_status_o-idno 'AND version EQ ' ls_ver_status_o-current_ver
*      INTO lv_where_str SEPARATED BY space.

      CONCATENATE 'IDNO EQ ' '''' INTO lv_where_str SEPARATED BY space.
      CONCATENATE lv_where_str ls_ver_status_o-idno '''' INTO lv_where_str.
      CONCATENATE lv_where_str 'AND version EQ ' ls_ver_status_o-article_ver
             INTO lv_where_str SEPARATED BY space.

      LOOP AT <table> ASSIGNING <table_str>
        WHERE (lv_where_str).

* Collect data to be compared
        APPEND <table_str> TO <table_db>.

        go_inbound_utils = zcl_arn_inbound_utils=>get_instance( ).
        go_inbound_utils->set_field_config( fc_t_field_confg ).
        go_inbound_utils->prepare_struc_for_compare( EXPORTING iv_strucname = ls_tabname-tabname
                                                     CHANGING cs_structure = <table_str> ).

* Collect data to be compared
        APPEND <table_str> TO <table_o>.
      ENDLOOP.  " LOOP AT <table> ASSIGNING <table_str>

      IF <table_o> IS ASSIGNED AND <table_o> IS NOT INITIAL.
        SORT <table_o>.
      ENDIF.

      IF <table_db> IS ASSIGNED AND <table_db> IS NOT INITIAL.
        SORT <table_db>.
      ENDIF.

    ENDLOOP. " LOOP AT fc_t_tabname INTO ls_tabname.

    INSERT ls_prod_data_o  INTO TABLE fc_t_prod_data_o[].
    INSERT ls_prod_data_db INTO TABLE fc_t_prod_data_db[].

  ENDLOOP. " LOOP AT fc_t_ver_status_O[] INTO ls_ver_status_O.






ENDFORM.                    " RETREIVE_IDNO_DATA

* INS Begin of Change JKH 13.09.2016
*&---------------------------------------------------------------------*
*&      Form  GET_CURRENT_VERSION_DATA
*&---------------------------------------------------------------------*
* Get National Data for Current Version
*----------------------------------------------------------------------*
FORM get_current_version_data USING fu_t_ver_status_all TYPE ty_t_ver_status
                                    fu_t_prd_version_o  TYPE ty_t_prd_version
                                    fu_t_tabname        TYPE ty_t_tabname
                                    fu_t_field_confg    TYPE ty_t_field_confg
                           CHANGING fc_t_prod_data_curr TYPE ty_t_prod_data
                                    fc_t_prod_data_db   TYPE ty_t_prod_data.

  DATA: ls_prod_data    TYPE ty_s_prod_data,
        ls_prod_data_o  TYPE ty_s_prod_data,
        ls_prod_data_db TYPE ty_s_prod_data,
        ls_ver_status   TYPE ty_s_ver_status,
        ls_ver_status_o TYPE ty_s_ver_status,
        ls_field_confg  TYPE ty_s_field_confg,
        ls_tabname      TYPE ty_s_tabname,
        lv_itabname     TYPE char50,
        lv_where_str    TYPE string,
        lt_key          TYPE ztarn_key,
        ls_key          TYPE zsarn_key.

  FIELD-SYMBOLS: <table_o>   TYPE table,
                 <table_db>  TYPE table,
                 <table>     TYPE table,
                 <table_str> TYPE any,
                 <table_val> TYPE any.


  CLEAR: lt_key[].
* Get IDNO and Current version where Status is APR
  LOOP AT fu_t_ver_status_all[] INTO ls_ver_status.
*    WHERE article_ver IS INITIAL.          "--3171 JKH 05.05.2017 -

* Check Status of Current version must be APR
    READ TABLE fu_t_prd_version_o[] TRANSPORTING NO FIELDS
    WITH KEY idno           = ls_ver_status-idno
             version        = ls_ver_status-current_ver.
*             version_status = 'APR'.          "--3171 JKH 05.05.2017 - get data for all current versions - APR check later
    IF sy-subrc = 0.
      CLEAR ls_key.
      ls_key-idno    = ls_ver_status-idno.
      ls_key-version = ls_ver_status-current_ver.
      APPEND ls_key TO lt_key[].
    ENDIF.
  ENDLOOP.

  CLEAR: ls_prod_data.
  IF lt_key[] IS NOT INITIAL.
* Get National Data for Current Version
    CALL FUNCTION 'ZARN_READ_NATIONAL_DATA'
      EXPORTING
        it_key      = lt_key[]
      IMPORTING
        es_data_all = ls_prod_data.
  ENDIF.

** For all the data retreived for Product
* Build data as per IDNO and Version
* Fill fields in internal tables for each IDNO and Version which are configured
*   to be compared (version_compare is TRUE)
* Primary Keys will not be compared even if they are configured - as this is just
* to know if any attribute is changed or not. Actual change determination is done in
* change category determination
  LOOP AT lt_key[] INTO ls_key.

* Build data as per IDNO and Version
    CLEAR: ls_prod_data_o.
    ls_prod_data_o-idno    = ls_key-idno.
    ls_prod_data_o-version = ls_key-version.

    CLEAR: ls_prod_data_db.
    ls_prod_data_db-idno    = ls_key-idno.
    ls_prod_data_db-version = ls_key-version.

    LOOP AT fu_t_tabname INTO ls_tabname.

      IF <table_o>    IS ASSIGNED. UNASSIGN <table_o>.    ENDIF.
      IF <table_db>   IS ASSIGNED. UNASSIGN <table_db>.   ENDIF.
      IF <table>      IS ASSIGNED. UNASSIGN <table>.      ENDIF.
      IF <table_str>  IS ASSIGNED. UNASSIGN <table_str>.  ENDIF.
      IF <table_val>  IS ASSIGNED. UNASSIGN <table_val>.  ENDIF.

      CLEAR: lv_itabname.
      CONCATENATE 'LS_PROD_DATA_O-' ls_tabname-tabname INTO lv_itabname.
      ASSIGN (lv_itabname) TO <table_o>.

      CLEAR: lv_itabname.
      CONCATENATE 'LS_PROD_DATA_DB-' ls_tabname-tabname INTO lv_itabname.
      ASSIGN (lv_itabname) TO <table_db>.

      CLEAR: lv_itabname.
      CONCATENATE 'LS_PROD_DATA-' ls_tabname-tabname INTO lv_itabname.
      ASSIGN (lv_itabname) TO <table>.

* Build WHERE clause string as data has to be retreived for IDNO and VERSION only
      CLEAR lv_where_str.

      CONCATENATE 'IDNO EQ ' '''' INTO lv_where_str SEPARATED BY space.
      CONCATENATE lv_where_str ls_key-idno '''' INTO lv_where_str.
      CONCATENATE lv_where_str 'AND version EQ ' ls_key-version
             INTO lv_where_str SEPARATED BY space.

      LOOP AT <table> ASSIGNING <table_str>
        WHERE (lv_where_str).

* Collect data to be compared
        APPEND <table_str> TO <table_db>.

        go_inbound_utils = zcl_arn_inbound_utils=>get_instance( ).
        go_inbound_utils->set_field_config( fu_t_field_confg ).
        go_inbound_utils->prepare_struc_for_compare( EXPORTING iv_strucname = ls_tabname-tabname
                                                     CHANGING cs_structure = <table_str> ).

* Collect data to be compared
        APPEND <table_str> TO <table_o>.
      ENDLOOP.  " LOOP AT <table> ASSIGNING <table_str>

      IF <table_o> IS ASSIGNED AND <table_o> IS NOT INITIAL.
        SORT <table_o>.
      ENDIF.

      IF <table_db> IS ASSIGNED AND <table_db> IS NOT INITIAL.
        SORT <table_db>.
      ENDIF.

    ENDLOOP. " LOOP AT fu_t_tabname INTO ls_tabname.

    INSERT ls_prod_data_o  INTO TABLE fc_t_prod_data_curr[].
    INSERT ls_prod_data_db INTO TABLE fc_t_prod_data_db[].

  ENDLOOP. " LOOP AT lt_key[] INTO ls_key.



ENDFORM.
* INS End of Change JKH 13.09.2016

*&---------------------------------------------------------------------*
*&      Form  VALIDATE_BUSINESS_RULES
*&---------------------------------------------------------------------*
* To have Business rule frame work during inbound processing
*----------------------------------------------------------------------*
FORM validate_business_rules USING fu_s_master_product       TYPE zmaster_products_product
                                   fu_v_idno                 TYPE zarn_idno
                                   fu_t_errmail              TYPE ztarn_inb_errmail
                                   fu_v_guid                 TYPE zarn_guid
                                   fu_t_dfies                TYPE dfies_tab
                                   fu_s_admin                TYPE zmaster_products_admin
                                   fu_o_validation_engine    TYPE REF TO zcl_arn_validation_engine
                                   fu_t_prod_uom_t           TYPE ty_t_prod_uom_t
                          CHANGING fc_t_message              TYPE bal_t_msg
                                   fc_t_master_product_error TYPE zmaster_products_product_tab
                                   fc_s_prod_data_n          TYPE ty_s_prod_data
                                   fc_v_msgtyp               TYPE sy-msgty
                                   fc_v_error_cnt            TYPE i
                                   fc_v_warning_cnt          TYPE i
                                   fc_v_success_cnt          TYPE i
                                   fc_t_errmail_data         TYPE ty_t_errmail_data.


  DATA: lt_output       TYPE zarn_t_rl_output,
        ls_output       TYPE zarn_s_rl_output,
        ls_params       TYPE bal_s_parm,
        ls_log_params   TYPE zsarn_slg1_log_params_inbound,
        ls_errmail_data TYPE ty_s_errmail_data.

  IF fu_o_validation_engine IS INITIAL.
    EXIT.
  ENDIF.


* Validation Rule Engine
  CLEAR: lt_output[].
  TRY.
      CALL METHOD fu_o_validation_engine->validate_arn_single
        EXPORTING
          is_zsarn_prod_data = fc_s_prod_data_n
        IMPORTING
          et_output          = lt_output.
    CATCH zcx_arn_validation_err INTO DATA(lo_excp)."++SUP-20 JKH 02.10.2017
  ENDTRY.

  LOOP AT lt_output[] INTO ls_output.

* Build Return Message
    CLEAR: ls_log_params, ls_params.
* Build Log Parameters
    PERFORM build_log_parameters USING fu_t_dfies[]
                                       fu_s_master_product
                                       fu_s_admin
                                       fu_v_guid
                                       fu_t_prod_uom_t[]
                              CHANGING ls_params
                                       ls_log_params.


    PERFORM build_message USING ls_output-msgty
                                ls_output-msgid
                                ls_output-msgno
                                ls_output-msgv1
                                ls_output-msgv2
                                ls_output-msgv3
                                ls_output-msgv4
                                ls_params
                       CHANGING fc_t_message[].


    IF ls_output-msgty = gc_message-error.


* Build Email Data for Error
      PERFORM build_error_email_data USING ls_log_params
                                           fu_s_master_product
                                           fu_t_errmail[]
                                  CHANGING fc_t_errmail_data[].


      APPEND fu_s_master_product TO fc_t_master_product_error[].
    ENDIF.

  ENDLOOP.

  CLEAR fc_v_msgtyp.
  CLEAR ls_output.
* Check for Error
  READ TABLE lt_output[] INTO ls_output
  WITH KEY msgty = gc_message-error.
  IF sy-subrc = 0.
    fc_v_msgtyp = ls_output-msgty.
  ELSE.
* Check for Warning
    READ TABLE lt_output[] INTO ls_output
    WITH KEY msgty = gc_message-warning.
    IF sy-subrc = 0.
      fc_v_msgtyp = ls_output-msgty.
    ELSE.
* Check for Info
      READ TABLE lt_output[] INTO ls_output
      WITH KEY msgty = gc_message-info.
      IF sy-subrc = 0.
        fc_v_msgtyp = ls_output-msgty.
      ENDIF.
    ENDIF.
  ENDIF.


* Error
  IF fc_v_msgtyp = gc_message-error.

    fc_v_error_cnt = fc_v_error_cnt + 1.
    APPEND fu_s_master_product TO fc_t_master_product_error[].

* Warning
  ELSEIF fc_v_msgtyp = gc_message-warning.

    fc_v_warning_cnt = fc_v_warning_cnt + 1.

* Information or nothing
  ELSEIF fc_v_msgtyp = gc_message-info OR fc_v_msgtyp IS INITIAL.

    fc_v_success_cnt = fc_v_success_cnt + 1.

  ENDIF.




ENDFORM.                    " VALIDATE_BUSINESS_RULES
*&---------------------------------------------------------------------*
*&      Form  VERSION_MANAGEMENT
*&---------------------------------------------------------------------*
* Version Management
*----------------------------------------------------------------------*
FORM version_management USING fu_s_master_product TYPE zmaster_products_product
                              fu_v_idno           TYPE zarn_idno
                              fu_t_field_confg    TYPE ty_t_field_confg
                              fu_t_tabname        TYPE ty_t_tabname
                              fu_v_id_type        TYPE zarn_id_type    "++3171 JKH 05.05.2017
                     CHANGING fc_t_message        TYPE bal_t_msg
                              fc_t_ver_status_n   TYPE ty_t_ver_status
                              fc_t_ver_status_all TYPE ty_t_ver_status
                              fc_s_prod_data_o    TYPE ty_s_prod_data
                              fc_s_prod_data_n    TYPE ty_s_prod_data
                              fc_s_prod_data_c    TYPE ty_s_prod_data
                              fc_v_change         TYPE flag.

* Build New Product Data
  PERFORM build_new_prod_data USING fu_s_master_product
                                    fu_v_idno
                           CHANGING fc_s_prod_data_n.

* Build Product Version to be compared
  PERFORM build_prod_version_to_compare USING fc_s_prod_data_n
                                              fu_t_field_confg
                                              fu_t_tabname
                                              fc_t_ver_status_n
                                              fc_t_ver_status_all
                                              fu_v_idno
                                              fu_v_id_type          "++3171 JKH 05.05.2017
                                     CHANGING fc_s_prod_data_c.

* Check Data Changes
  PERFORM check_data_changes USING fc_s_prod_data_o
                                   fc_s_prod_data_c
                                   fu_v_idno
                          CHANGING fc_v_change.



ENDFORM.                    " VERSION_MANAGEMENT
*&---------------------------------------------------------------------*
*&      Form  BUILD_NEW_PROD_DATA
*&---------------------------------------------------------------------*
* Build New Product Data
*----------------------------------------------------------------------*
FORM build_new_prod_data USING fu_s_master_product TYPE zmaster_products_product
                               fu_v_idno           TYPE zarn_idno
                      CHANGING fc_s_prod_data_n    TYPE ty_s_prod_data.


  DATA: ls_tabname        TYPE ty_s_tabname.

  CLEAR fc_s_prod_data_n.

  fc_s_prod_data_n-idno    = fu_v_idno.
*fc_s_prod_data_n-version =

* Build Products
  PERFORM build_products_table    USING fu_s_master_product
                                        fu_v_idno
                               CHANGING fc_s_prod_data_n-zarn_products[].

* Build Products Extension
  PERFORM build_products_ex_table USING fu_s_master_product
                                        fu_v_idno
                               CHANGING fc_s_prod_data_n-zarn_products_ex[].

* Build Products Feature Benefits
  IF gv_feature_a4_enabled EQ abap_true.
    PERFORM build_benefits_table    USING fu_s_master_product
                                          fu_v_idno
                                 CHANGING fc_s_prod_data_n-zarn_benefits[].
  ENDIF.

* Build Products String
  PERFORM build_prod_str_table    USING fu_s_master_product
                                        fu_v_idno
                               CHANGING fc_s_prod_data_n-zarn_prod_str[].

* Build Products Extension 2
  PERFORM build_products_2_table USING fu_s_master_product
                                       fu_v_idno
                              CHANGING fc_s_prod_data_n-zarn_products_2[].

  PERFORM build_net_quantity_table USING fu_s_master_product
                                         fu_v_idno
                                CHANGING fc_s_prod_data_n-zarn_net_qty[].

* Build Nutrients and serving size tables
  PERFORM build_nutrients_table   USING fu_s_master_product
                                        fu_v_idno
                               CHANGING fc_s_prod_data_n-zarn_nutrients[]
                                        fc_s_prod_data_n-zarn_serve_size[].

* Build Nutrients Health Star Values
  PERFORM build_ntr_health_table  USING fu_s_master_product
                                        fu_v_idno
                               CHANGING fc_s_prod_data_n-zarn_ntr_health[].

* Build Nutritional Claim Free Text
  PERFORM build_ntr_claimtx_table USING fu_s_master_product
                                        fu_v_idno
                               CHANGING fc_s_prod_data_n-zarn_ntr_claimtx[].

* Build Nutritional Claims List
  PERFORM build_ntr_claims_table  USING fu_s_master_product
                                        fu_v_idno
                               CHANGING fc_s_prod_data_n-zarn_ntr_claims[].

* Build Growing List
  PERFORM build_growing_table     USING fu_s_master_product
                                        fu_v_idno
                               CHANGING fc_s_prod_data_n-zarn_growing[].

* Build Allergen Type and Level of Containments
  PERFORM build_allergen_table    USING fu_s_master_product
                                        fu_v_idno
                               CHANGING fc_s_prod_data_n-zarn_allergen[].

* Build HSNO Classification Codes
  PERFORM build_hsno_table        USING fu_s_master_product
                                        fu_v_idno
                               CHANGING fc_s_prod_data_n-zarn_hsno[].



** Build Dangerous Goods Margin Numbers
*  PERFORM build_dgi_margin_table  USING fu_s_master_product
*                                        fu_v_idno
*                               CHANGING fc_s_prod_data_n-zarn_dgi_margin[].

* Build Preparation Type
  PERFORM build_prep_type_table   USING fu_s_master_product
                                        fu_v_idno
                               CHANGING fc_s_prod_data_n-zarn_prep_type[].

* Build Handling Instructions Code
  PERFORM build_ins_code_table    USING fu_s_master_product
                                        fu_v_idno
                               CHANGING fc_s_prod_data_n-zarn_ins_code[].

* Build Dietary Information
  PERFORM build_diet_info_table   USING fu_s_master_product
                                        fu_v_idno
                               CHANGING fc_s_prod_data_n-zarn_diet_info[].

* Build Chemical Characteristic
  PERFORM build_chem_char_table   USING fu_s_master_product
                                        fu_v_idno
                               CHANGING fc_s_prod_data_n-zarn_chem_char[].

* Build Additives
  PERFORM build_additives_table   USING fu_s_master_product
                                        fu_v_idno
                               CHANGING fc_s_prod_data_n-zarn_additives[].

* Build Ingredients List
  PERFORM build_fb_ingre_table    USING fu_s_master_product
                                        fu_v_idno
                               CHANGING fc_s_prod_data_n-zarn_fb_ingre[].

* Build Organism
*  PERFORM build_organism_table    USING fu_s_master_product
*                                        fu_v_idno
*                               CHANGING fc_s_prod_data_n-zarn_organism[].
  PERFORM build_bioorganism_table USING fu_s_master_product
                                        fu_v_idno
                               CHANGING fc_s_prod_data_n-zarn_bioorganism[].

* Build National Merchandise Hierarchies
  PERFORM build_nat_mc_hier_table USING fu_s_master_product
                                        fu_v_idno
                               CHANGING fc_s_prod_data_n-zarn_nat_mc_hier[].

* Build CDT Level
  PERFORM build_cdt_level_table   USING fu_s_master_product
                                        fu_v_idno
                               CHANGING fc_s_prod_data_n-zarn_cdt_level[].

* Build UOM Variants
  PERFORM build_uom_variant_table USING fu_s_master_product
                                        fu_v_idno
                               CHANGING fc_s_prod_data_n.

* Build Additional Information
  PERFORM build_addit_info_table  USING fu_s_master_product
                                        fu_v_idno
                               CHANGING fc_s_prod_data_n-zarn_addit_info[].

* Build Online Categories
  PERFORM build_online_categories USING fu_s_master_product
                                        fu_v_idno
                               CHANGING fc_s_prod_data_n-zarn_onlcat[].


ENDFORM.                    " BUILD_NEW_PROD_DATA
*&---------------------------------------------------------------------*
*&      Form  BUILD_PRODUCTS_TABLE
*&---------------------------------------------------------------------*
* Build Products Table
*----------------------------------------------------------------------*
FORM build_products_table USING fu_s_master_product TYPE zmaster_products_product
                                fu_v_idno           TYPE zarn_idno
                       CHANGING fc_t_products       TYPE ztarn_products.

  DATA: ls_products          TYPE zarn_products.

  REFRESH: fc_t_products[].
  ls_products-mandt                      = sy-mandt.
  ls_products-idno                       = fu_v_idno.

  ls_products-saptransmission_time       = fu_s_master_product-saptransmission_time.
  ls_products-code                       = fu_s_master_product-code.
  ls_products-name                       = fu_s_master_product-name.
  ls_products-description                = fu_s_master_product-description.
  ls_products-fan_id                     = fu_s_master_product-fan.
  ls_products-matnr_ni                   = fu_s_master_product-article_number_ni.
  ls_products-matnr_si                   = fu_s_master_product-article_number_si.

  ls_products-brand_id                   = fu_s_master_product-brand.
  ls_products-brand_id_upper             = fu_s_master_product-brand.
  ls_products-sub_brand_id               = fu_s_master_product-sub_brand.
  ls_products-fs_short_descr             = fu_s_master_product-fsshort_description.
  ls_products-fs_short_descr_upper       = fu_s_master_product-fsshort_description.
  ls_products-fs_brand_id                = fu_s_master_product-fsbrand.
  ls_products-fs_brand_desc              = fu_s_master_product-fsbrand_desc.
  ls_products-fs_sub_brand_id            = fu_s_master_product-fssub_brand.
  ls_products-fs_sub_brand_desc          = fu_s_master_product-fssub_brand_desc.
*  ls_products-brand_owner_lbl            = fu_s_master_product-brand_owner_label_enq.

  ls_products-variant                    = fu_s_master_product-variant.

  ls_products-gpc_code                   = fu_s_master_product-gpc-code.
  ls_products-gpc_description            = fu_s_master_product-gpc-description.

  ls_products-functional_name            = fu_s_master_product-functional_name.

*  ls_products-net_quantity               = fu_s_master_product-net_quantity.
*  ls_products-net_quantity_uom           = fu_s_master_product-net_quantity_uom.

  ls_products-cpu_uom                    = fu_s_master_product-comparative_price_unit-uom.
  ls_products-cpu_content                = fu_s_master_product-comparative_price_unit-content.
  ls_products-cpu_comp_unit              = fu_s_master_product-comparative_price_unit-comparison_unit.

  ls_products-plu                        = fu_s_master_product-plu.
  ls_products-target_market_country      = fu_s_master_product-target_market_country.
  ls_products-market_segment_level       = fu_s_master_product-market_segment_level.

  ls_products-ni_num_serves_per_pack     = fu_s_master_product-nutritional_info-number_of_serves_per_pack.
  ls_products-health_star_rating         = fu_s_master_product-health_star_rating.
*  ls_products-growing_method             = fu_s_master_product-growing_method.

  ls_products-dgi_packing_group          = fu_s_master_product-dangerous_goods_info-packing_group.
  ls_products-dgi_regulation_code        = fu_s_master_product-dangerous_goods_info-regulation_code.
  ls_products-dgi_technical_name         = fu_s_master_product-dangerous_goods_info-technical_name.
  ls_products-dgi_hazardous_code         = fu_s_master_product-dangerous_goods_info-hazardous_code.
  ls_products-dgi_unn_code               = fu_s_master_product-dangerous_goods_info-unnumber-code.
  ls_products-dgi_unn_shipping_name      = fu_s_master_product-dangerous_goods_info-unnumber-shipping_name.
  ls_products-dgi_class                  = fu_s_master_product-dangerous_goods_info-dg_class.
  ls_products-hsno_app                   = fu_s_master_product-dangerous_goods_info-hsnoapproval_number.
  ls_products-hsno_hsr                   = fu_s_master_product-dangerous_goods_info-ghscode.
  ls_products-dgi_hazard_unit_vol        = fu_s_master_product-dangerous_goods_info-hazardous_unit_size.
  ls_products-dgi_hazard_unit_vol_uom    = fu_s_master_product-dangerous_goods_info-hazardous_unit_size_uom.
  ls_products-sds_issue_date             = fu_s_master_product-dangerous_goods_info-sdsissue_date.

  ls_products-flash_pt_temp_val          = fu_s_master_product-flash_point_temperature_value.
  ls_products-flash_pt_temp_uom          = fu_s_master_product-flash_point_temperature_uom.

  ls_products-gln                        = fu_s_master_product-last_information_provider-gln_lip-gln.
  ls_products-gln_descr                  = fu_s_master_product-last_information_provider-gln_lip-description.
  ls_products-gln_descr_upper            = fu_s_master_product-last_information_provider-gln_lip-description.
  ls_products-last_info_provider_date    = fu_s_master_product-last_information_provider-date.

  ls_products-dangerous_good             = fu_s_master_product-dangerous_good.
  ls_products-hazardous_good             = fu_s_master_product-hazardous_good.
  ls_products-price_on_pack              = fu_s_master_product-price_on_pack.
*  ls_products-preparation_state          = fu_s_master_product-preparation_info-preparation_state.
  ls_products-expiration_date_type       = fu_s_master_product-preparation_info-expiration_date_type.
  ls_products-genetically_modified       = fu_s_master_product-preparation_info-genetically_modified.
  ls_products-irradiated                 = fu_s_master_product-preparation_info-irradiated.
  ls_products-target_consumer_gender     = fu_s_master_product-target_consumer-gender.
  ls_products-target_consumer_age        = fu_s_master_product-target_consumer-age.
  ls_products-season_name                = fu_s_master_product-season_info-name.
  ls_products-avail_end_date             = fu_s_master_product-season_info-availability_end_date.
  ls_products-avail_start_date           = fu_s_master_product-season_info-availability_start_date.
  ls_products-seling_uom                 = fu_s_master_product-selling_unit_of_measure.

  ls_products-lifespan_arrival           = fu_s_master_product-trade_item_handling_info-lifespan_arrival.
  ls_products-lifespan_production        = fu_s_master_product-trade_item_handling_info-lifespan_production.

  ls_products-stor_handl_max_temp_val    = fu_s_master_product-stor_handl_max_temp_value.
  ls_products-stor_handl_max_temp_uom    = fu_s_master_product-stor_handl_max_temp_uom.
  ls_products-stor_handl_min_temp_val    = fu_s_master_product-stor_handl_min_temp_value.
  ls_products-stor_handl_min_temp_uom    = fu_s_master_product-stor_handl_min_temp_uom.

  ls_products-tax_rate                   = fu_s_master_product-tax-rate.
  ls_products-tax_type                   = fu_s_master_product-tax-type.
  ls_products-tax_descr                  = fu_s_master_product-tax-description.

  ls_products-fssi_catman_value          = fu_s_master_product-fssiranging-sicat_man-value.
  ls_products-fssi_catman_datetime       = fu_s_master_product-fssiranging-sicat_man-date.
  ls_products-fssi_trents_value          = fu_s_master_product-fssiranging-trents-value.
  ls_products-fssi_trents_datetime       = fu_s_master_product-fssiranging-trents-date.
  ls_products-fssi_fresh_value           = fu_s_master_product-fssiranging-fresh-value.
  ls_products-fssi_fresh_datetime        = fu_s_master_product-fssiranging-fresh-date.
  ls_products-fssi_gtt_value             = fu_s_master_product-fssiranging-gtt-value.
  ls_products-fssi_gtt_datetime          = fu_s_master_product-fssiranging-gtt-date.
  ls_products-fssi_instore_value         = fu_s_master_product-fssiranging-in_store_fssi-value.
  ls_products-fssi_instore_datetime      = fu_s_master_product-fssiranging-in_store_fssi-date.
  ls_products-fssi_ranging_meeting_date  = fu_s_master_product-fssiranging-ranging_meeting_date.
  ls_products-fssi_ranging_decision_date = fu_s_master_product-fssiranging-ranging_decision_date.
  ls_products-fssi_ranging_priority      = fu_s_master_product-fssiranging-ranging_priority.

  ls_products-fsni_icare_value           = fu_s_master_product-fsniranging-fsni_icare-value.
  ls_products-fsni_icare_datetime        = fu_s_master_product-fsniranging-fsni_icare-date.
*  ls_products-fsni_catman_value          = fu_s_master_product-fsniranging-fsni_icare-value.
*  ls_products-fsni_catman_datetime       = fu_s_master_product-fsniranging-fsni_icare-date.
  ls_products-fsni_gilmours_value        = fu_s_master_product-fsniranging-gilmours-value.
  ls_products-fsni_gilmours_datetime     = fu_s_master_product-fsniranging-gilmours-date.
  ls_products-fsni_instore_value         = fu_s_master_product-fsniranging-in_store_fsni-value.
  ls_products-fsni_instore_datetime      = fu_s_master_product-fsniranging-in_store_fsni-date.
  ls_products-fsni_ranging_meeting_date  = fu_s_master_product-fsniranging-ranging_meeting_date.
  ls_products-fsni_ranging_decision_date = fu_s_master_product-fsniranging-ranging_decision_date.
  ls_products-fsni_ranging_priority      = fu_s_master_product-fsniranging-ranging_priority.

  ls_products-status                     = fu_s_master_product-status.
  ls_products-fssi_last_trans_date       = fu_s_master_product-fssilast_transmission_date.
  ls_products-fsni_last_trans_date       = fu_s_master_product-fsnilast_transmission_date.
  ls_products-task_id                    = fu_s_master_product-task_id.
  ls_products-fish_catch_zone            = fu_s_master_product-fish_catch_zone.
  ls_products-fsgln                      = fu_s_master_product-fsglin.

*  ls_products-direction_use_1            = fu_s_master_product-directions_for_use1.
*  ls_products-direction_use_2            = fu_s_master_product-directions_for_use2.
*  ls_products-bo_facebook                = fu_s_master_product-brand_owner_facebook.
*  ls_products-bo_twitter                 = fu_s_master_product-brand_owner_twitter.
*  ls_products-bo_label_website           = fu_s_master_product-brand_owner_label_website.
*  ls_products-bo_label_company_name      = fu_s_master_product-brand_owner_label_company_name.
*  ls_products-bo_label_address           = fu_s_master_product-brand_owner_label_address.

  " Serving size description should be the same for all nutrient type - take value from 1st record
  ls_products-serve_size_descr            = VALUE #( fu_s_master_product-nutritional_info-nutrients-nutrient[ 1 ]-serving_size_description OPTIONAL ).

  TRANSLATE ls_products-fs_short_descr_upper TO UPPER CASE.
  TRANSLATE ls_products-gln_descr_upper TO UPPER CASE.
  TRANSLATE ls_products-brand_id_upper TO UPPER CASE.

  APPEND ls_products TO fc_t_products[].

ENDFORM.                    " BUILD_PRODUCTS_TABLE
*&---------------------------------------------------------------------*
*&      Form  BUILD_PRODUCTS_EX_TABLE
*&---------------------------------------------------------------------*
* Build Products Extension Table
*----------------------------------------------------------------------*
FORM build_products_ex_table USING fu_s_master_product TYPE zmaster_products_product
                                   fu_v_idno           TYPE zarn_idno
                          CHANGING fc_t_products_ex    TYPE ztarn_products_ex.

  DATA: ls_products_ex       TYPE zarn_products_ex.

  REFRESH: fc_t_products_ex[].
  ls_products_ex-mandt                      = sy-mandt.
  ls_products_ex-idno                       = fu_v_idno.


  ls_products_ex-full_description           = fu_s_master_product-full_trade_item_description.
*  ls_products_ex-feature_benefit            = fu_s_master_product-trade_item_feature_benefit.
  IF gv_feature_a4_enabled EQ abap_false.
*   If feature is enabled these go into table ZARN_BENEFITS
    IF lines( fu_s_master_product-trade_item_feature_benefits-trade_item_feature_benefit ) GT 0.
      READ TABLE fu_s_master_product-trade_item_feature_benefits-trade_item_feature_benefit
        INTO ls_products_ex-feature_benefit
        INDEX 1.
    ENDIF.
  ENDIF.
  ls_products_ex-fs_seasonality             = fu_s_master_product-fsseasonality.
  ls_products_ex-fs_cust_short_descr        = fu_s_master_product-fscustomer_short_description.
  ls_products_ex-liquor_type                = fu_s_master_product-alcoholic_info-type_of_liquor.
  ls_products_ex-strength_code              = fu_s_master_product-alcoholic_info-alcoholic_strength-code.
  ls_products_ex-strength_descr             = fu_s_master_product-alcoholic_info-alcoholic_strength-description.
  ls_products_ex-vintage                    = fu_s_master_product-alcoholic_info-vintage.
  ls_products_ex-alcohol_pct                = fu_s_master_product-alcoholic_info-percentage_of_alcohol.
  ls_products_ex-market_segment             = fu_s_master_product-alcoholic_info-market_segment.
  ls_products_ex-origin_region              = fu_s_master_product-alcoholic_info-regions_of_origin.
  ls_products_ex-labelling_claims           = fu_s_master_product-labelling_claims.
  ls_products_ex-promotional_groups         = fu_s_master_product-promotional_groups.

  ls_products_ex-fs_brand_desc_upper        = fu_s_master_product-fsbrand_desc.

  ls_products_ex-dgi_class_descr            = fu_s_master_product-dangerous_goods_info-dg_class_description.

  ls_products_ex-prio_req_by_date_ni        = fu_s_master_product-ni_prioritisation_by_date.
  ls_products_ex-prio_req_by_ni             = fu_s_master_product-ni_prioritisation_requested_by.
  ls_products_ex-requestor_ni               = fu_s_master_product-ni_requestor.
  ls_products_ex-prio_req_by_date_si        = fu_s_master_product-si_prioritisation_by_date.
  ls_products_ex-prio_req_by_si             = fu_s_master_product-si_prioritisation_requested_by.
  ls_products_ex-requestor_si               = fu_s_master_product-si_requestor.

  TRANSLATE ls_products_ex-fs_brand_desc_upper TO UPPER CASE.

  APPEND ls_products_ex TO fc_t_products_ex[].
ENDFORM.                    " BUILD_PRODUCTS_TABLE_EX
*&---------------------------------------------------------------------*
*&      Form  BUILD_PROD_STR_TABLE
*&---------------------------------------------------------------------*
* Build Products String
*----------------------------------------------------------------------*
FORM build_prod_str_table    USING fu_s_master_product TYPE zmaster_products_product
                                   fu_v_idno           TYPE zarn_idno
                          CHANGING fc_t_prod_str       TYPE ztarn_prod_str.

  DATA: ls_prod_str       TYPE zarn_prod_str.

  REFRESH: fc_t_prod_str[].
  ls_prod_str-mandt                      = sy-mandt.
  ls_prod_str-idno                       = fu_v_idno.

  ls_prod_str-allergen_statement         = fu_s_master_product-allergen_statement.
  ls_prod_str-ingredient_statement       = fu_s_master_product-ingredient_statement.
  ls_prod_str-precautions                = fu_s_master_product-preparation_info-precautions.
  ls_prod_str-preparation_instructions   = fu_s_master_product-preparation_info-preparation_instructions.
  ls_prod_str-cons_usage_ins             = fu_s_master_product-preparation_info-consumer_usage_instructions.
  ls_prod_str-cons_storage_ins           = fu_s_master_product-preparation_info-consumer_storage_instructions.
  ls_prod_str-serving_suggestion         = fu_s_master_product-serving_suggestion.
  ls_prod_str-marketing_message          = fu_s_master_product-trade_item_marketing_message.
  ls_prod_str-additional_description     = fu_s_master_product-additional_trade_item_descript.
  ls_prod_str-fs_cooking_ins             = fu_s_master_product-fscooking_instructions.
  ls_prod_str-fs_storage_ins             = fu_s_master_product-fsstorage_instructions.
  ls_prod_str-fs_preparation_ins         = fu_s_master_product-fspreparation_instructions.
  ls_prod_str-fs_tips_and_advice         = fu_s_master_product-fstips_and_advice.
  ls_prod_str-fs_cust_long_descr         = fu_s_master_product-fscustomer_long_description.
  ls_prod_str-fs_cust_medium_descr       = fu_s_master_product-fscustomer_medium_description.

  APPEND ls_prod_str TO fc_t_prod_str[].

ENDFORM.                    " BUILD_PROD_STR_TABLE
*&---------------------------------------------------------------------*
*&      Form  BUILD_PRODUCTS_2_TABLE
*&---------------------------------------------------------------------*
* Build Products Extension Table
*----------------------------------------------------------------------*
FORM build_products_2_table USING fu_s_master_product TYPE zmaster_products_product
                                  fu_v_idno           TYPE zarn_idno
                         CHANGING fc_t_products_2     TYPE ztarn_products_2.


  DATA: ls_products_2       TYPE zarn_products_2.

  REFRESH: fc_t_products_2[].
  ls_products_2-mandt                      = sy-mandt.
  ls_products_2-idno                       = fu_v_idno.

  ls_products_2-name_upper                 = fu_s_master_product-name.
  ls_products_2-description_upper          = fu_s_master_product-description.
  ls_products_2-fs_brand_id_upper          = fu_s_master_product-fsbrand.


  TRANSLATE ls_products_2-name_upper        TO UPPER CASE.
  TRANSLATE ls_products_2-description_upper TO UPPER CASE.
  TRANSLATE ls_products_2-fs_brand_id_upper TO UPPER CASE.

  APPEND ls_products_2 TO fc_t_products_2[].

ENDFORM.                    " BUILD_PRODUCTS_2_TABLE
*&---------------------------------------------------------------------*
*&      Form  BUILD_NUTRIENTS_TABLE
*&---------------------------------------------------------------------*
* Build Nutrients
*----------------------------------------------------------------------*
FORM build_nutrients_table USING fu_s_master_product TYPE zmaster_products_product
                                 fu_v_idno           TYPE zarn_idno
                        CHANGING fc_t_nutrients      TYPE ztarn_nutrients
                                 fc_t_serve_size     TYPE ztarn_serve_size.

  DATA: ls_prod_nutrients TYPE zmaster_products_nutrient,
        ls_nutrients      TYPE zarn_nutrients.

  REFRESH: fc_t_nutrients[],
           fc_t_serve_size[].

  LOOP AT fu_s_master_product-nutritional_info-nutrients-nutrient[] INTO ls_prod_nutrients.
    CLEAR: ls_nutrients.
    ls_nutrients-mandt                        = sy-mandt.
    ls_nutrients-idno                         = fu_v_idno.
**    ls_nutrients-seqno                        = sy-tabix.
    ls_nutrients-nutrient_type                = ls_prod_nutrients-nutrient_type.
    ls_nutrients-nutrient_type_descr          = ls_prod_nutrients-nutrient_type_description.

    ls_nutrients-quantity_contained           = ls_prod_nutrients-qty_contained.
    ls_nutrients-daily_intake                 = ls_prod_nutrients-daily_intake.
    ls_nutrients-nutrient_uom                 = ls_prod_nutrients-nutrient_uom.
    ls_nutrients-nutrient_base_quantity_type  = ls_prod_nutrients-nutrient_bqty_ty.
    ls_nutrients-nutrient_base_qty            = ls_prod_nutrients-nutrient_bqty.
    ls_nutrients-nutrient_base_qty_uom        = ls_prod_nutrients-nutrient_bqty_uom.
*    ls_nutrients-ni_serving_size              = ls_prod_nutrients-serving_size.
*    ls_nutrients-ni_serving_size_uom          = ls_prod_nutrients-serving_size_uom.
    ls_nutrients-ni_measurement_precision     = ls_prod_nutrients-measurement_precision.
    ls_nutrients-preparation_state            = ls_prod_nutrients-preparation_state.

    APPEND ls_nutrients TO fc_t_nutrients[].

    " Build serving size table - note even though within Nutrient info in xsd/proxy,
    " the values are per product, hence stored at product level.
    PERFORM build_prod_serving_size_table USING ls_prod_nutrients-serving_sizes
                                                fu_v_idno
                                                ls_nutrients-nutrient_type
                                       CHANGING fc_t_serve_size[].
  ENDLOOP.

  SORT fc_t_nutrients[] BY idno version nutrient_type.
  DELETE ADJACENT DUPLICATES FROM fc_t_nutrients[] COMPARING idno version nutrient_type.

  " Because there was no proper unique identifier for the table key, all the fields
  " specified as the secondary key will be used to prevent duplicate records semantically.
  " Because the GUID is used as primary key there should never be duplicates.
  " Currently the secondary key are all other fields of the table excluding the GUID.
  SORT fc_t_serve_size BY sk.
  DELETE ADJACENT DUPLICATES FROM fc_t_serve_size COMPARING sk.

ENDFORM.                    " BUILD_NUTRIENTS_TABLE
*&---------------------------------------------------------------------*
*&      Form  BUILD_NTR_HEALTH_TABLE
*&---------------------------------------------------------------------*
* Build Nutrients Health Star Values
*----------------------------------------------------------------------*
FORM build_ntr_health_table USING fu_s_master_product TYPE zmaster_products_product
                                  fu_v_idno           TYPE zarn_idno
                         CHANGING fc_t_ntr_health     TYPE ztarn_ntr_health.

  DATA: ls_prod_ntr_health TYPE zmaster_products_nutrient_heal,
        ls_ntr_health      TYPE zarn_ntr_health.

  REFRESH: fc_t_ntr_health[].

  LOOP AT fu_s_master_product-nutritional_info-nutr_health_star_values-nutrient_health_star_value[] INTO ls_prod_ntr_health.
    CLEAR: ls_ntr_health.
    ls_ntr_health-mandt              = sy-mandt.
    ls_ntr_health-idno               = fu_v_idno.
*    ls_ntr_health-seqno              = sy-tabix.
    ls_ntr_health-nutrient           = ls_prod_ntr_health-nutrient.

    ls_ntr_health-health_star_values = ls_prod_ntr_health-health_star_value.
    APPEND ls_ntr_health TO fc_t_ntr_health[].
  ENDLOOP.

  SORT fc_t_ntr_health[] BY idno version nutrient.
  DELETE ADJACENT DUPLICATES FROM fc_t_ntr_health[] COMPARING idno version nutrient.

ENDFORM.                    " BUILD_NTR_HEALTH_TABLE
*&---------------------------------------------------------------------*
*&      Form  BUILD_NTR_CLAIMTX_TABLE
*&---------------------------------------------------------------------*
* Build Nutritional Claim Free Text
*----------------------------------------------------------------------*
FORM build_ntr_claimtx_table USING fu_s_master_product TYPE zmaster_products_product
                                   fu_v_idno           TYPE zarn_idno
                          CHANGING fc_t_ntr_claimtx    TYPE ztarn_ntr_claimtx.

  DATA: ls_ntr_claimtx         TYPE zarn_ntr_claimtx.

  REFRESH: fc_t_ntr_claimtx[].


  CLEAR: ls_ntr_claimtx.
  ls_ntr_claimtx-mandt                = sy-mandt.
  ls_ntr_claimtx-idno                 = fu_v_idno.

  ls_ntr_claimtx-nutr_claim_free_text = fu_s_master_product-nutr_claims-nutritional_claim_free_text.
  APPEND ls_ntr_claimtx TO fc_t_ntr_claimtx[].



ENDFORM.                    " BUILD_NTR_CLAIMTX_TABLE
*&---------------------------------------------------------------------*
*&      Form  BUILD_NTR_CLAIMS_TABLE
*&---------------------------------------------------------------------*
* Build Nutritional Claims List
*----------------------------------------------------------------------*
FORM build_ntr_claims_table USING fu_s_master_product TYPE zmaster_products_product
                                  fu_v_idno           TYPE zarn_idno
                         CHANGING fc_t_ntr_claims     TYPE ztarn_ntr_claims.

  DATA: ls_prod_ntr_claims TYPE zmaster_products_nutritional_c, "string,
        ls_ntr_claims      TYPE zarn_ntr_claims.

  REFRESH: fc_t_ntr_claims[].

  LOOP AT fu_s_master_product-nutr_claims-list_nutr_claims-nutritional_claim[] INTO ls_prod_ntr_claims.
    CLEAR: ls_ntr_claims.
    ls_ntr_claims-mandt              = sy-mandt.
    ls_ntr_claims-idno               = fu_v_idno.
*    ls_ntr_claims-seqno              = sy-tabix.
    ls_ntr_claims-nutritional_claims      = ls_prod_ntr_claims-nutritional_claim_code.
    ls_ntr_claims-nutritional_claims_elem = ls_prod_ntr_claims-nutritional_claim_ele.

    APPEND ls_ntr_claims TO fc_t_ntr_claims[].
  ENDLOOP.

  SORT fc_t_ntr_claims[] BY idno version nutritional_claims nutritional_claims_elem.
  DELETE ADJACENT DUPLICATES FROM fc_t_ntr_claims[] COMPARING idno version nutritional_claims nutritional_claims_elem.

ENDFORM.                    " BUILD_NTR_CLAIMS_TABLE
*&---------------------------------------------------------------------*
*&      Form  BUILD_GROWING_TABLE
*&---------------------------------------------------------------------*
* Build Growing List
*----------------------------------------------------------------------*
FORM build_growing_table USING fu_s_master_product TYPE zmaster_products_product
                               fu_v_idno           TYPE zarn_idno
                      CHANGING fc_t_growing        TYPE ztarn_growing.

  DATA: ls_prod_growing TYPE char35,
        ls_growing      TYPE zarn_growing.

  REFRESH: fc_t_growing[].

  LOOP AT fu_s_master_product-growing_method_codes-growing_method[] INTO ls_prod_growing.
    CLEAR: ls_growing.
    ls_growing-mandt                = sy-mandt.
    ls_growing-idno                 = fu_v_idno.
    ls_growing-growing_method       = ls_prod_growing.

    APPEND ls_growing TO fc_t_growing[].
  ENDLOOP.

  SORT fc_t_growing[] BY idno version growing_method.
  DELETE ADJACENT DUPLICATES FROM fc_t_growing[] COMPARING idno version growing_method.

ENDFORM.                    " BUILD_GROWING_TABLE
*&---------------------------------------------------------------------*
*&      Form  BUILD_ALLERGEN_TABLE
*&---------------------------------------------------------------------*
* Build Allergen Type and Level of Containments
*----------------------------------------------------------------------*
FORM build_allergen_table USING fu_s_master_product TYPE zmaster_products_product
                                fu_v_idno           TYPE zarn_idno
                       CHANGING fc_t_allergen       TYPE ztarn_allergen.

  DATA: ls_prod_allergen TYPE zmaster_products_type_allergen,
        ls_allergen      TYPE zarn_allergen.

  REFRESH: fc_t_allergen[].

  LOOP AT fu_s_master_product-allergen_type_and_level_of_con-type_allergen_and_level_of_con[] INTO ls_prod_allergen.
    CLEAR: ls_allergen.
    ls_allergen-mandt                = sy-mandt.
    ls_allergen-idno                 = fu_v_idno.
*    ls_allergen-seqno                = sy-tabix.
    ls_allergen-allergen_type        = ls_prod_allergen-allergen_type.
    ls_allergen-allergen_spec_agency = ls_prod_allergen-allergen_agency-allergen_specification_agency.
    ls_allergen-allergen_type_descr  = ls_prod_allergen-allergen_type_description.

    ls_allergen-agency_name          = ls_prod_allergen-allergen_agency-name.
    ls_allergen-lvl_containment      = ls_prod_allergen-level_of_containment.
    APPEND ls_allergen TO fc_t_allergen[].
  ENDLOOP.

  SORT fc_t_allergen[] BY idno version allergen_type allergen_spec_agency.
  DELETE ADJACENT DUPLICATES FROM fc_t_allergen[] COMPARING idno version allergen_type allergen_spec_agency.

ENDFORM.                    " BUILD_ALLERGEN_TABLE
*&---------------------------------------------------------------------*
*&      Form  BUILD_HSNO_TABLE
*&---------------------------------------------------------------------*
* * Build HSNO Classification Codes
*----------------------------------------------------------------------*
FORM build_hsno_table     USING fu_s_master_product TYPE zmaster_products_product
                                fu_v_idno           TYPE zarn_idno
                       CHANGING fc_t_hsno           TYPE ztarn_hsno.

  DATA: ls_prod_hsno TYPE zmaster_products_hsnoclassifi1,
        ls_hsno      TYPE zarn_hsno.

  REFRESH: fc_t_hsno[].

  LOOP AT fu_s_master_product-dangerous_goods_info-hsnoclassifications-hsnoclassification[]
                                                               INTO ls_prod_hsno.

    CLEAR: ls_hsno.
    ls_hsno-mandt                = sy-mandt.
    ls_hsno-idno                 = fu_v_idno.
    ls_hsno-hsno_code            = ls_prod_hsno-code.
    ls_hsno-hsno_code_descr      = ls_prod_hsno-hsnoclassification_code_descri.

    APPEND ls_hsno TO fc_t_hsno[].
  ENDLOOP.

  SORT fc_t_hsno[] BY idno version hsno_code.
  DELETE ADJACENT DUPLICATES FROM fc_t_hsno[] COMPARING idno version hsno_code.

ENDFORM.                    " BUILD_HSNO_TABLE
*&---------------------------------------------------------------------*
*&      Form  BUILD_PREP_TYPE_TABLE
*&---------------------------------------------------------------------*
* Build Preparation Type
*----------------------------------------------------------------------*
FORM build_prep_type_table USING fu_s_master_product TYPE zmaster_products_product
                                 fu_v_idno           TYPE zarn_idno
                        CHANGING fc_t_prep_type      TYPE ztarn_prep_type.

  DATA: ls_prep_type         TYPE zarn_prep_type.

  REFRESH: fc_t_prep_type[].

  LOOP AT fu_s_master_product-preparation_info-types_preparation-preparation_type[] INTO DATA(ls_prep_type_in).
    CLEAR: ls_prep_type.
    ls_prep_type-mandt            = sy-mandt.
    ls_prep_type-idno             = fu_v_idno.
*    ls_prep_type-seqno           = sy-tabix.
    ls_prep_type-preparation_type = ls_prep_type_in.
    APPEND ls_prep_type TO fc_t_prep_type[].
  ENDLOOP.

  SORT fc_t_prep_type[] BY idno version preparation_type.
  DELETE ADJACENT DUPLICATES FROM fc_t_prep_type[] COMPARING idno version preparation_type.

ENDFORM.                    " BUILD_PREP_TYPE_TABLE
*&---------------------------------------------------------------------*
*&      Form  BUILD_INS_CODE_TABLE
*&---------------------------------------------------------------------*
* Build Handling Instructions Code
*----------------------------------------------------------------------*
FORM build_ins_code_table USING fu_s_master_product TYPE zmaster_products_product
                                fu_v_idno           TYPE zarn_idno
                       CHANGING fc_t_ins_code       TYPE ztarn_ins_code.

  DATA: ls_prod_ins_code TYPE zmaster_products_handling_inst,
        ls_ins_code      TYPE zarn_ins_code.

  REFRESH: fc_t_ins_code[].

  LOOP AT fu_s_master_product-handl_instructions_codes-handling_instructions_code[] INTO ls_prod_ins_code.
    CLEAR: ls_ins_code.
    ls_ins_code-mandt              = sy-mandt.
    ls_ins_code-idno               = fu_v_idno.
*    ls_ins_code-seqno              = sy-tabix.
    ls_ins_code-handling_ins_code  = ls_prod_ins_code-code.

    ls_ins_code-handling_ins_descr = ls_prod_ins_code-description.
    APPEND ls_ins_code TO fc_t_ins_code[].
  ENDLOOP.

  SORT fc_t_ins_code[] BY idno version handling_ins_code.
  DELETE ADJACENT DUPLICATES FROM fc_t_ins_code[] COMPARING idno version handling_ins_code.

ENDFORM.                    " BUILD_INS_CODE_TABLE
*&---------------------------------------------------------------------*
*&      Form  BUILD_DIET_INFO_TABLE
*&---------------------------------------------------------------------*
* Build Dietary Information
*----------------------------------------------------------------------*
FORM build_diet_info_table USING fu_s_master_product TYPE zmaster_products_product
                                 fu_v_idno           TYPE zarn_idno
                        CHANGING fc_t_diet_info      TYPE ztarn_diet_info.

  DATA: ls_prod_diet_info TYPE zmaster_products_dietary_infor,
        ls_diet_info      TYPE zarn_diet_info.

  REFRESH: fc_t_diet_info[].

  LOOP AT fu_s_master_product-list_dietary_information-dietary_information[] INTO ls_prod_diet_info.
    CLEAR: ls_diet_info.
    ls_diet_info-mandt           = sy-mandt.
    ls_diet_info-idno            = fu_v_idno.
*    ls_diet_info-seqno           = sy-tabix.
    ls_diet_info-diet_type       = ls_prod_diet_info-diet_type.

    ls_diet_info-cert_agency     = ls_prod_diet_info-certification_agency.
    ls_diet_info-cert_no         = ls_prod_diet_info-certification_number.
*    ls_diet_info-cert_url        =
    APPEND ls_diet_info TO fc_t_diet_info[].
  ENDLOOP.

  SORT fc_t_diet_info[] BY idno version diet_type.
  DELETE ADJACENT DUPLICATES FROM fc_t_diet_info[] COMPARING idno version diet_type.

ENDFORM.                    " BUILD_DIET_INFO_TABLE
*&---------------------------------------------------------------------*
*&      Form  BUILD_CHEM_CHAR_TABLE
*&---------------------------------------------------------------------*
* Build Chemical Characteristic
*----------------------------------------------------------------------*
FORM build_chem_char_table USING fu_s_master_product TYPE zmaster_products_product
                                 fu_v_idno           TYPE zarn_idno
                        CHANGING fc_t_chem_char      TYPE ztarn_chem_char.

  DATA: ls_prod_chem_char TYPE zmaster_products_chemical_char,
        ls_chem_char      TYPE zarn_chem_char.

  REFRESH: fc_t_chem_char[].

  LOOP AT fu_s_master_product-chem_characteristics-chemical_characteristic[] INTO ls_prod_chem_char.
    CLEAR: ls_chem_char.
    ls_chem_char-mandt           = sy-mandt.
    ls_chem_char-idno            = fu_v_idno.
*    ls_chem_char-seqno           = sy-tabix.
    ls_chem_char-chem_code       = ls_prod_chem_char-code.

    ls_chem_char-chem_value      = ls_prod_chem_char-value.
    ls_chem_char-chem_uom        = ls_prod_chem_char-uom.
    APPEND ls_chem_char TO fc_t_chem_char[].
  ENDLOOP.

  SORT fc_t_chem_char[] BY idno version chem_code.
  DELETE ADJACENT DUPLICATES FROM fc_t_chem_char[] COMPARING idno version chem_code.

ENDFORM.                    " BUILD_CHEM_CHAR_TABLE
*&---------------------------------------------------------------------*
*&      Form  BUILD_ADDITIVES_TABLE
*&---------------------------------------------------------------------*
* Build Additives
*----------------------------------------------------------------------*
FORM build_additives_table USING fu_s_master_product TYPE zmaster_products_product
                                 fu_v_idno           TYPE zarn_idno
                        CHANGING fc_t_additives      TYPE ztarn_additives.

  DATA: ls_prod_additives TYPE zmaster_products_additive,
        ls_additives      TYPE zarn_additives.

  REFRESH: fc_t_additives[].

  LOOP AT fu_s_master_product-additives_list-additive[] INTO ls_prod_additives.
    CLEAR: ls_additives.
    ls_additives-mandt             = sy-mandt.
    ls_additives-idno              = fu_v_idno.
    ls_additives-additive_name     = ls_prod_additives-name.

    ls_additives-additive_loc      = ls_prod_additives-level_of_containment.

    APPEND ls_additives TO fc_t_additives[].
  ENDLOOP.

  SORT fc_t_additives[] BY idno version additive_name.
  DELETE ADJACENT DUPLICATES FROM fc_t_additives[] COMPARING idno version additive_name.

ENDFORM.                    " BUILD_ADDITIVES_TABLE
*&---------------------------------------------------------------------*
*&      Form  BUILD_FB_INGRE_TABLE
*&---------------------------------------------------------------------*
* Build Ingredients List
*----------------------------------------------------------------------*
FORM build_fb_ingre_table  USING fu_s_master_product TYPE zmaster_products_product
                                 fu_v_idno           TYPE zarn_idno
                        CHANGING fc_t_fb_ingre       TYPE ztarn_fb_ingre .

  DATA: ls_prod_fb_ingre TYPE zmaster_products_ingredient,
        ls_fb_ingre      TYPE zarn_fb_ingre.

  REFRESH: fc_t_fb_ingre[].

  LOOP AT fu_s_master_product-ingredients_list-ingredient[] INTO ls_prod_fb_ingre.
    CLEAR: ls_fb_ingre.
    ls_fb_ingre-mandt             = sy-mandt.
    ls_fb_ingre-idno              = fu_v_idno.
    ls_fb_ingre-sequence          = ls_prod_fb_ingre-sequence.

    ls_fb_ingre-name              = ls_prod_fb_ingre-name.
    ls_fb_ingre-content_perc      = ls_prod_fb_ingre-content_percentage.
    ls_fb_ingre-place_of_activity = ls_prod_fb_ingre-place_of_activity.
    APPEND ls_fb_ingre TO fc_t_fb_ingre[].
  ENDLOOP.

  SORT fc_t_fb_ingre[] BY idno version sequence.
  DELETE ADJACENT DUPLICATES FROM fc_t_fb_ingre[] COMPARING idno version sequence.

ENDFORM.                    " BUILD_FB_INGRE_TABLE
*&---------------------------------------------------------------------*
*&      Form  BUILD_NAT_MC_HIER_TABLE
*&---------------------------------------------------------------------*
* Build National Merchandise Hierarchies
*----------------------------------------------------------------------*
FORM build_nat_mc_hier_table USING fu_s_master_product TYPE zmaster_products_product
                                   fu_v_idno           TYPE zarn_idno
                          CHANGING fc_t_nat_mc_hier    TYPE ztarn_nat_mc_hier.

  DATA: ls_prod_nat_mc_hier TYPE zmaster_products_national_merc,
        ls_nat_mc_hier      TYPE zarn_nat_mc_hier.

  REFRESH: fc_t_nat_mc_hier[].

  LOOP AT fu_s_master_product-nat_merchandise_hierarchies-national_merchandise_hierarchy[] INTO ls_prod_nat_mc_hier.
    CLEAR: ls_nat_mc_hier.
    ls_nat_mc_hier-mandt           = sy-mandt.
    ls_nat_mc_hier-idno            = fu_v_idno.
*    ls_nat_mc_hier-seqno           = sy-tabix.
    ls_nat_mc_hier-nat_mc_code     = ls_prod_nat_mc_hier-code.
    APPEND ls_nat_mc_hier TO fc_t_nat_mc_hier[].
  ENDLOOP.

  SORT fc_t_nat_mc_hier[] BY idno version nat_mc_code.
  DELETE ADJACENT DUPLICATES FROM fc_t_nat_mc_hier[] COMPARING idno version nat_mc_code.

ENDFORM.                    " BUILD_NAT_MC_HIER_TABLE
*&---------------------------------------------------------------------*
*&      Form  BUILD_CDT_LEVEL_TABLE
*&---------------------------------------------------------------------*
* Build CDT Level
*----------------------------------------------------------------------*
FORM build_cdt_level_table USING fu_s_master_product TYPE zmaster_products_product
                                 fu_v_idno           TYPE zarn_idno
                        CHANGING fc_t_cdt_level      TYPE ztarn_cdt_level.

  DATA: ls_prod_cdtlevel TYPE zmaster_products_cdtlevel,
        ls_cdt_level     TYPE zarn_cdt_level.

  REFRESH: fc_t_cdt_level[].

  LOOP AT fu_s_master_product-cdt-cdtlevel[] INTO ls_prod_cdtlevel.
    CLEAR: ls_cdt_level.
    ls_cdt_level-mandt    = sy-mandt.
    ls_cdt_level-idno     = fu_v_idno.
*    ls_cdt_level-seqno    = sy-tabix.
    ls_cdt_level-cdt_code = ls_prod_cdtlevel-code.
    APPEND ls_cdt_level TO fc_t_cdt_level[].
  ENDLOOP.

  SORT fc_t_cdt_level[] BY idno version cdt_code.
  DELETE ADJACENT DUPLICATES FROM fc_t_cdt_level[] COMPARING idno version cdt_code.

ENDFORM.                    " BUILD_CDT_LEVEL_TABLE
*&---------------------------------------------------------------------*
*&      Form  BUILD_ADDIT_INFO_TABLE
*&---------------------------------------------------------------------*
* Build Additional Information
*----------------------------------------------------------------------*
FORM build_addit_info_table USING fu_s_master_product TYPE zmaster_products_product
                                  fu_v_idno           TYPE zarn_idno
                         CHANGING fc_t_addit_info     TYPE ztarn_addit_info.

  DATA: ls_prod_addit_info TYPE zmaster_products_information,
        ls_addit_info      TYPE zarn_addit_info.

  REFRESH: fc_t_addit_info[].

  LOOP AT fu_s_master_product-addi_info-information[] INTO ls_prod_addit_info.
    CLEAR: ls_addit_info.
    ls_addit_info-mandt                   = sy-mandt.
    ls_addit_info-idno                    = fu_v_idno.
*    ls_addit_info-seqno                   = sy-tabix.
    ls_addit_info-addit_type              = ls_prod_addit_info-type.

    ls_addit_info-uniform_resource_ident1  = ls_prod_addit_info-uniform_resource_identifier.
    ls_addit_info-uniform_resource_ident2  = ls_prod_addit_info-uniform_resource_identifier+1333.
    ls_addit_info-file_eff_start_datetime  = ls_prod_addit_info-file_effective_start_date_time.
    APPEND ls_addit_info TO fc_t_addit_info[].
  ENDLOOP.

  SORT fc_t_addit_info[] BY idno version addit_type.
  DELETE ADJACENT DUPLICATES FROM fc_t_addit_info[] COMPARING idno version addit_type.

ENDFORM.                    " BUILD_ADDIT_INFO_TABLE
*&---------------------------------------------------------------------*
*&      Form  BUILD_ONLINE_CATEGORIES
*&---------------------------------------------------------------------*
* Build Online Categories
*----------------------------------------------------------------------*
FORM build_online_categories USING fu_s_master_product TYPE zmaster_products_product
                                   fu_v_idno           TYPE zarn_idno
                          CHANGING fc_t_onlcat         TYPE ztarn_onlcat.

  DATA: ls_prod_onl_cat LIKE LINE OF fu_s_master_product-non_merchandise_hierarchies-non_merchandise_hierarchy,
        ls_onl_cat      LIKE LINE OF fc_t_onlcat.

  REFRESH: fc_t_onlcat[].

  CHECK gv_feature_a4_enabled EQ abap_true.

  LOOP AT fu_s_master_product-non_merchandise_hierarchies-non_merchandise_hierarchy INTO ls_prod_onl_cat.
    CLEAR: ls_onl_cat.
    ls_onl_cat-mandt           = sy-mandt.
    ls_onl_cat-idno            = fu_v_idno.
    ls_onl_cat-bunit           = ls_prod_onl_cat-business_unit.
    ls_onl_cat-catalog_type    = ls_prod_onl_cat-catalogue_type.
    ls_onl_cat-seqno           = ls_prod_onl_cat-sequence.
    ls_onl_cat-onl_catalog     = |{ ls_prod_onl_cat-catalogue(6) }_{ ls_prod_onl_cat-catalogue_type }|.
    ls_onl_cat-category_1      = ls_prod_onl_cat-category1-code.
    ls_onl_cat-category_desc_1 = ls_prod_onl_cat-category1-description.
    ls_onl_cat-category_2      = ls_prod_onl_cat-category2-code.
    ls_onl_cat-category_desc_2 = ls_prod_onl_cat-category2-description.
    ls_onl_cat-category_3      = ls_prod_onl_cat-category3-code.
    ls_onl_cat-category_desc_3 = ls_prod_onl_cat-category3-description.
    ls_onl_cat-category_4      = ls_prod_onl_cat-category4-code.
    ls_onl_cat-category_desc_4 = ls_prod_onl_cat-category4-description.
    ls_onl_cat-category_5      = ls_prod_onl_cat-category5-code.
    ls_onl_cat-category_desc_5 = ls_prod_onl_cat-category5-description.
    ls_onl_cat-category_6      = ls_prod_onl_cat-category6-code.
    ls_onl_cat-category_desc_6 = ls_prod_onl_cat-category6-description.
    APPEND ls_onl_cat TO fc_t_onlcat[].
  ENDLOOP.

  SORT fc_t_onlcat[] BY idno bunit catalog_type seqno.
  DELETE ADJACENT DUPLICATES FROM fc_t_onlcat[] COMPARING idno bunit catalog_type seqno.

ENDFORM.                    " BUILD_ADDIT_INFO_TABLE
*&---------------------------------------------------------------------*
*&      Form  BUILD_UOM_VARIANT_TABLE
*&---------------------------------------------------------------------*
* Build UOM Variants
*----------------------------------------------------------------------*
FORM build_uom_variant_table USING fu_s_master_product TYPE zmaster_products_product
                                   fu_v_idno           TYPE zarn_idno
                          CHANGING fc_s_prod_data_n    TYPE ty_s_prod_data.

  DATA: ls_prod_uom_variant TYPE zmaster_products_uomvariant,
        ls_uom_variant      TYPE zarn_uom_variant.

  REFRESH: fc_s_prod_data_n-zarn_uom_variant[],
           fc_s_prod_data_n-zarn_gtin_var[],
           fc_s_prod_data_n-zarn_pir[],
           fc_s_prod_data_n-zarn_pir_comm_ch[],
           fc_s_prod_data_n-zarn_list_price[].

  LOOP AT fu_s_master_product-uomvariants-uomvariant[] INTO ls_prod_uom_variant.
    CLEAR: ls_uom_variant.
    ls_uom_variant-mandt                          = sy-mandt.
    ls_uom_variant-idno                           = fu_v_idno.
*    ls_uom_variant-uom_seqno                = sy-tabix.
    ls_uom_variant-uom_code                       = ls_prod_uom_variant-code.

    ls_uom_variant-num_base_units                 = ls_prod_uom_variant-number_of_base_units.
    ls_uom_variant-factor_of_base_units           = ls_prod_uom_variant-factor_of_base_units.
    ls_uom_variant-base_unit                      = ls_prod_uom_variant-base_unit.
    ls_uom_variant-child_gtin                     = ls_prod_uom_variant-child_gtin.
    ls_uom_variant-product_uom                    = ls_prod_uom_variant-uom.
    ls_uom_variant-consumer_unit                  = ls_prod_uom_variant-consumer_unit.
    ls_uom_variant-despatch_unit                  = ls_prod_uom_variant-despatch_unit.
    ls_uom_variant-an_invoice_unit                = ls_prod_uom_variant-an_invoice_unit.
    ls_uom_variant-nesting                        = ls_prod_uom_variant-nesting.
    ls_uom_variant-net_weight_uom                 = ls_prod_uom_variant-net_weight_uom.
    ls_uom_variant-net_weight_value               = ls_prod_uom_variant-net_weight_value.
    ls_uom_variant-gross_weight_uom               = ls_prod_uom_variant-gross_weight_uom.
    ls_uom_variant-gross_weight_value             = ls_prod_uom_variant-gross_weight_value.
    ls_uom_variant-height_uom                     = ls_prod_uom_variant-height_uom.
    ls_uom_variant-height_value                   = ls_prod_uom_variant-height_value.
    ls_uom_variant-width_uom                      = ls_prod_uom_variant-width_uom.
    ls_uom_variant-width_value                    = ls_prod_uom_variant-width_value.
    ls_uom_variant-depth_uom                      = ls_prod_uom_variant-depth_uom.
    ls_uom_variant-depth_value                    = ls_prod_uom_variant-depth_value.
    ls_uom_variant-packaging_type                 = ls_prod_uom_variant-packaging_type  .
    ls_uom_variant-packaging_material             = ls_prod_uom_variant-packaging_material.
    ls_uom_variant-status                         = ls_prod_uom_variant-status.
    ls_uom_variant-display_ready_packaging        = ls_prod_uom_variant-display_ready_packaging.
    ls_uom_variant-front_face_type                = ls_prod_uom_variant-front_face_type_code.
    ls_uom_variant-pkging_rcycling_scheme_code    = ls_prod_uom_variant-pkging_sustainability-pkging_rcycling_scheme_code.
    ls_uom_variant-pkging_rcycling_process_type_c = ls_prod_uom_variant-pkging_sustainability-pkging_rcycling_process_type_c.
    ls_uom_variant-sustainability_feature_code    = ls_prod_uom_variant-pkging_sustainability-sustainability_feature_code.
    IF gv_feature_a4_enabled EQ abap_true.
      ls_uom_variant-net_content_short_desc         = ls_prod_uom_variant-net_content_short_desc.
      ls_uom_variant-fscust_prod_name40             = ls_prod_uom_variant-fscust_prod_name40.
      ls_uom_variant-fscust_prod_name80             = ls_prod_uom_variant-fscust_prod_name80.
      ls_uom_variant-fscust_prod_name255            = ls_prod_uom_variant-fscust_prod_name255.
    ENDIF.

    APPEND ls_uom_variant TO fc_s_prod_data_n-zarn_uom_variant[].

* Build GTIN Variants
    PERFORM build_gtin_var_table USING ls_prod_uom_variant-gtinvariants-gtinvariant[]
                                       ls_uom_variant
                              CHANGING fc_s_prod_data_n.

* Build Purchasing Info Records
    PERFORM build_pir_table USING ls_prod_uom_variant-purch_info_records-purchasing_info_record[]
                                  ls_uom_variant
                         CHANGING fc_s_prod_data_n.

* Build List Price Info Record
    PERFORM build_list_price_table USING ls_prod_uom_variant-list_pr_info_records-list_price_info_record[]
                                         ls_uom_variant
                                CHANGING fc_s_prod_data_n.


  ENDLOOP.

* Special Logic - Default Sequence Number and Active LP flag
  PERFORM list_price_active_default CHANGING fc_s_prod_data_n-zarn_list_price[].


  SORT fc_s_prod_data_n-zarn_uom_variant[] BY idno version uom_code.
  DELETE ADJACENT DUPLICATES FROM fc_s_prod_data_n-zarn_uom_variant[] COMPARING idno version uom_code.

ENDFORM.                    " BUILD_UOM_VARIANT_TABLE
*&---------------------------------------------------------------------*
*&      Form  BUILD_GTIN_VAR_TABLE
*&---------------------------------------------------------------------*
* Build GTIN Variants
*----------------------------------------------------------------------*
FORM build_gtin_var_table USING fu_t_gtinvarian  TYPE zmaster_products_gtinvaria_tab
                                fu_s_uom_variant TYPE zarn_uom_variant
                       CHANGING fc_s_prod_data_n TYPE ty_s_prod_data.

  DATA: ls_prod_gtin_var TYPE zmaster_products_gtinvariant,
        ls_gtin_var      TYPE zarn_gtin_var.

  LOOP AT fu_t_gtinvarian[] INTO ls_prod_gtin_var.
    CLEAR ls_gtin_var.
    ls_gtin_var-mandt        = sy-mandt.
    ls_gtin_var-idno         = fu_s_uom_variant-idno.
    ls_gtin_var-uom_code     = fu_s_uom_variant-uom_code.
*    ls_gtin_var-gtin_seqno   = sy-tabix.
    ls_gtin_var-gtin_code    = ls_prod_gtin_var-code.

    ls_gtin_var-gtin_current = ls_prod_gtin_var-current.
    ls_gtin_var-gtin_status  = ls_prod_gtin_var-status.
    APPEND ls_gtin_var TO fc_s_prod_data_n-zarn_gtin_var[].
  ENDLOOP.

  SORT fc_s_prod_data_n-zarn_gtin_var[] BY idno version uom_code gtin_code.
  DELETE ADJACENT DUPLICATES FROM fc_s_prod_data_n-zarn_gtin_var[] COMPARING idno version uom_code gtin_code.

ENDFORM.                    " BUILD_GTIN_VAR_TABLE
*&---------------------------------------------------------------------*
*&      Form  BUILD_PIR_TABLE
*&---------------------------------------------------------------------*
* Build Purchasing Info Records
*----------------------------------------------------------------------*
FORM build_pir_table USING fu_t_pir         TYPE zmaster_products_purchasin_tab
                           fu_s_uom_variant TYPE zarn_uom_variant
                  CHANGING fc_s_prod_data_n TYPE ty_s_prod_data.

  DATA: ls_prod_pir TYPE zmaster_products_purchasing_in,
        ls_pir      TYPE zarn_pir.

  LOOP AT fu_t_pir[] INTO ls_prod_pir.
    CLEAR ls_pir.
    ls_pir-mandt        = sy-mandt.
    ls_pir-idno         = fu_s_uom_variant-idno.
    ls_pir-uom_code     = fu_s_uom_variant-uom_code.
    ls_pir-hybris_internal_code   = ls_prod_pir-hybris_internal_code.

    ls_pir-gln                    = ls_prod_pir-gln_pir-gln.
    ls_pir-gln_description        = ls_prod_pir-gln_pir-description.
    ls_pir-gln_description_upper  = ls_prod_pir-gln_pir-description.
    ls_pir-source_of_info         = ls_prod_pir-source_of_info.
    ls_pir-gln_name               = ls_prod_pir-glnname.
    ls_pir-vendor_article_number  = ls_prod_pir-vendor_article_number.
    ls_pir-manufacturer_gln       = ls_prod_pir-manufacturer_gln-gln.
    ls_pir-manufacturer_gln_descr = ls_prod_pir-manufacturer_gln-description.
    ls_pir-manufacturer_gln_descr_upper = ls_prod_pir-manufacturer_gln-description.
    ls_pir-manufacturer_gln_name  = ls_prod_pir-manufacturer_glnname.
    ls_pir-country_of_origin      = ls_prod_pir-country_of_origin.
    ls_pir-country_of_origin_stm  = ls_prod_pir-country_of_origin_statement.
    ls_pir-variable_unit          = ls_prod_pir-variable_unit.
    ls_pir-avail_start_date       = ls_prod_pir-start_availability_date.
    ls_pir-avail_end_date         = ls_prod_pir-end_availability_date.
    ls_pir-publication_date       = ls_prod_pir-publication_date.
    ls_pir-effective_date         = ls_prod_pir-effective_date.
    ls_pir-first_order_date       = ls_prod_pir-first_order_date.
    ls_pir-last_changed_date      = ls_prod_pir-last_changed_date.
    ls_pir-retail_price           = ls_prod_pir-retail_price.
    ls_pir-base_units_per_pallet  = ls_prod_pir-pallet_information-base_units_per_pallet.
    ls_pir-factor_of_buom_per_pallet = ls_prod_pir-pallet_information-factor_of_base_units_per_palle.
    ls_pir-gross_weight           = ls_prod_pir-pallet_information-gross_weight.
    ls_pir-gross_height           = ls_prod_pir-pallet_information-gross_height.
    ls_pir-qty_per_pallet_layer   = ls_prod_pir-pallet_information-qty_per_pallet_layer.
    ls_pir-qty_layers_per_pallet  = ls_prod_pir-pallet_information-qty_of_layers_per_pallet.
    ls_pir-qty_units_per_pallet   = ls_prod_pir-pallet_information-qty_units_per_pallet.


    TRANSLATE ls_pir-gln_description_upper TO UPPER CASE.
    TRANSLATE ls_pir-manufacturer_gln_descr_upper TO UPPER CASE.

    APPEND ls_pir TO fc_s_prod_data_n-zarn_pir[].

* Build PIR Communication Channel
    PERFORM build_pir_comm_ch_table USING ls_prod_pir-commmunication_channels-communication_channel[]
                                          ls_pir
                                 CHANGING fc_s_prod_data_n.


  ENDLOOP.

  SORT fc_s_prod_data_n-zarn_pir[] BY idno version uom_code hybris_internal_code.
  DELETE ADJACENT DUPLICATES FROM fc_s_prod_data_n-zarn_pir[] COMPARING idno version uom_code hybris_internal_code.

ENDFORM.                    " BUILD_PIR_TABLE
*&---------------------------------------------------------------------*
*&      Form  BUILD_PIR_COMM_CH_TABLE
*&---------------------------------------------------------------------*
* Build PIR Communication Channel
*----------------------------------------------------------------------*
FORM build_pir_comm_ch_table USING fu_t_pir_comm_ch TYPE zmaster_products_communica_tab
                                   fu_s_pir         TYPE zarn_pir
                          CHANGING fc_s_prod_data_n TYPE ty_s_prod_data.

  DATA: ls_prod_pir_comm_ch TYPE zmaster_products_communication,
        ls_pir_comm_ch      TYPE zarn_pir_comm_ch.

  LOOP AT fu_t_pir_comm_ch[] INTO ls_prod_pir_comm_ch.
    CLEAR ls_pir_comm_ch.
    ls_pir_comm_ch-mandt        = sy-mandt.
    ls_pir_comm_ch-idno         = fu_s_pir-idno.
    ls_pir_comm_ch-uom_code     = fu_s_pir-uom_code.
    ls_pir_comm_ch-hybris_internal_code = fu_s_pir-hybris_internal_code.
    ls_pir_comm_ch-comm_ch_type    = ls_prod_pir_comm_ch-type.

    ls_pir_comm_ch-comm_ch_value   = ls_prod_pir_comm_ch-value.
    ls_pir_comm_ch-comm_ch_number  = ls_prod_pir_comm_ch-number.
    APPEND ls_pir_comm_ch TO fc_s_prod_data_n-zarn_pir_comm_ch[].

  ENDLOOP.

  SORT fc_s_prod_data_n-zarn_pir_comm_ch[] BY idno version uom_code hybris_internal_code comm_ch_type.
  DELETE ADJACENT DUPLICATES FROM fc_s_prod_data_n-zarn_pir_comm_ch[]
                                    COMPARING idno version uom_code hybris_internal_code comm_ch_type.

ENDFORM.                    " BUILD_PIR_COMM_CH_TABLE
*&---------------------------------------------------------------------*
*&      Form  BUILD_LIST_PRICE_TABLE
*&---------------------------------------------------------------------*
* Build List Price Info Record
*----------------------------------------------------------------------*
FORM build_list_price_table USING fu_t_list_price  TYPE zmaster_products_list_pric_tab
                                  fu_s_uom_variant TYPE zarn_uom_variant
                         CHANGING fc_s_prod_data_n TYPE ty_s_prod_data.

  DATA: ls_prod_list_price TYPE zmaster_products_list_price_in,
        ls_list_price      TYPE zarn_list_price.

  LOOP AT fu_t_list_price[] INTO ls_prod_list_price.
    CLEAR ls_list_price.
    ls_list_price-mandt                 = sy-mandt.
    ls_list_price-idno                  = fu_s_uom_variant-idno.
    ls_list_price-uom_code              = fu_s_uom_variant-uom_code.
*    ls_list_price-list_seqno            = sy-tabix.
    ls_list_price-gln                   = ls_prod_list_price-gln_list_price-gln.

    ls_list_price-gln_description       = ls_prod_list_price-gln_list_price-description.
*    ls_list_price-vendor_article_number = ls_prod_list_price-vendor_article_number.
    ls_list_price-start_date_context    = ls_prod_list_price-start_date_context.
    ls_list_price-end_date_context      = ls_prod_list_price-end_date_context.
    ls_list_price-eff_start_date        = ls_prod_list_price-effective_start_date.
    ls_list_price-eff_end_date          = ls_prod_list_price-effective_end_date.
    ls_list_price-application_sequence  = ls_prod_list_price-application_sequence.
    ls_list_price-quantity              = ls_prod_list_price-quantity.
*    ls_list_price-product_uom           = ls_prod_list_price-uom.
    ls_list_price-price_type            = ls_prod_list_price-type.
    ls_list_price-price_type_descr      = ls_prod_list_price-type_description.
    ls_list_price-price_type_id         = ls_prod_list_price-type_id.
    ls_list_price-price_value           = ls_prod_list_price-price_value.
    ls_list_price-price_value_type      = ls_prod_list_price-price_value_type.
    ls_list_price-currency              = ls_prod_list_price-currency.
    ls_list_price-action_reason         = ls_prod_list_price-action_reason.
    ls_list_price-source_of_info        = ls_prod_list_price-source_of_info.
    ls_list_price-last_changed_date     = ls_prod_list_price-last_changed_date.


    CONDENSE ls_list_price-last_changed_date.
    IF ls_list_price-last_changed_date = '0'.
      CLEAR ls_list_price-last_changed_date.
    ENDIF.


    APPEND ls_list_price TO fc_s_prod_data_n-zarn_list_price[].


  ENDLOOP.

*  SORT fc_s_prod_data_n-zarn_list_price[] BY idno version uom_code gln.
*  DELETE ADJACENT DUPLICATES FROM fc_s_prod_data_n-zarn_list_price[] COMPARING idno version uom_code gln.

ENDFORM.                    " BUILD_LIST_PRICE_TABLE
*&---------------------------------------------------------------------*
*&      Form  LIST_PRICE_ACTIVE_DEFAULT
*&---------------------------------------------------------------------*
* Special Logic - Default Sequence Number and Active LP flag
*----------------------------------------------------------------------*
FORM list_price_active_default  CHANGING fc_t_list_price TYPE ztarn_list_price.

  DATA: lt_list_price   TYPE ztarn_list_price,
        ls_list_price   TYPE zarn_list_price,

        lt_active_past  TYPE ztarn_list_price,
        lt_active_curr  TYPE ztarn_list_price,
        lt_active_futr  TYPE ztarn_list_price,

        lt_active_lp    TYPE ztarn_list_price,
        ls_active_lp    TYPE zarn_list_price,
        lt_active_waers TYPE ztarn_list_price,
        ls_active_waers TYPE zarn_list_price,

        lt_pir_vendor   TYPE ty_t_pir_vendor,
        ls_pir_vendor   TYPE ty_s_pir_vendor,
        lt_vendor       TYPE ty_t_pir_vendor,
        ls_vendor       TYPE ty_s_pir_vendor,

        lv_seqno        TYPE zarn_seqno,
        lv_datetime     TYPE tzonref-tstamps,
        lv_lines        TYPE sy-tabix.

  FIELD-SYMBOLS: <ls_list_price> TYPE zarn_list_price,
                 <ls_lp_active>  TYPE zarn_list_price.

  lt_list_price[] = fc_t_list_price[].

  SORT lt_list_price[] BY idno version uom_code gln.

  CLEAR lv_datetime.
*  GET TIME STAMP FIELD lv_datetime.

  lv_datetime = sy-datum && sy-uzeit.

  lv_seqno = 0.

  CLEAR lt_pir_vendor[].
  LOOP AT lt_list_price[] INTO ls_list_price.
    CLEAR ls_pir_vendor.
    ls_pir_vendor-gln   = ls_list_price-gln.
    ls_pir_vendor-bbbnr = ls_list_price-gln+0(7).
    ls_pir_vendor-bbsnr = ls_list_price-gln+7(5).
    ls_pir_vendor-bubkz = ls_list_price-gln+12(1).
    APPEND ls_pir_vendor TO lt_pir_vendor[].
  ENDLOOP.

* Get Vendors and Currency associated with GLNs
  IF lt_pir_vendor[] IS NOT INITIAL.
    CLEAR lt_vendor[].
    SELECT lfm1~lifnr lfm1~ekorg lfm1~waers
           lfa1~bbbnr lfa1~bbsnr lfa1~bubkz
      INTO CORRESPONDING FIELDS OF TABLE lt_vendor[]
      FROM lfa1 AS lfa1
      JOIN lfm1 AS lfm1
      ON lfa1~lifnr = lfm1~lifnr
      FOR ALL ENTRIES IN lt_pir_vendor[]
      WHERE lfm1~ekorg = '9999'
        AND lfa1~bbbnr = lt_pir_vendor-bbbnr
        AND lfa1~bbsnr = lt_pir_vendor-bbsnr
        AND lfa1~bubkz = lt_pir_vendor-bubkz.        "#EC CI_SEL_NESTED
  ENDIF.






  CLEAR: lt_active_lp[], lt_active_past[], lt_active_curr[], lt_active_futr[].

  LOOP AT lt_list_price[] ASSIGNING <ls_list_price>.

    <ls_list_price>-seqno = lv_seqno.
    lv_seqno = lv_seqno + 1.

    IF <ls_list_price>-eff_end_date LT lv_datetime.
* Get the list price which were valid in past
      APPEND <ls_list_price> TO lt_active_past[].
    ELSEIF <ls_list_price>-eff_start_date GT lv_datetime.
* Get the list price which are valid in future
      APPEND <ls_list_price> TO lt_active_futr[].
    ELSE.
* Get the list price which are valid as on current date and time
      APPEND <ls_list_price> TO lt_active_curr[].
    ENDIF.


    AT END OF gln.

      lv_seqno = 0.

      IF lt_active_curr[] IS NOT INITIAL.

        lt_active_lp[] = lt_active_curr[].

        CLEAR lv_lines.
        DESCRIBE TABLE lt_active_lp[] LINES lv_lines.
        IF lv_lines GT 1.

* Get all the common vendor and list price currencies
          CLEAR: lt_active_waers[].
          LOOP AT lt_vendor[] INTO ls_vendor.
            LOOP AT lt_active_lp[] INTO ls_active_waers
            WHERE currency = ls_vendor-waers.

              APPEND ls_active_waers TO lt_active_waers[].
            ENDLOOP.  " LOOP AT lt_active_lp[] INTO ls_active_waers
          ENDLOOP.  " LOOP AT lt_vendor[] INTO ls_vendor

* If there is any common currency, then determine list price from common currencies
          IF lt_active_waers[] IS NOT INITIAL.
            lt_active_lp[] = lt_active_waers[].
          ENDIF.


* If any price is available in NZD then prefer those prices
          READ TABLE lt_active_lp[] TRANSPORTING NO FIELDS
          WITH KEY currency = 'NZD'.
          IF sy-subrc = 0.
            DELETE lt_active_lp[] WHERE currency NE 'NZD'.
          ENDIF.



          CLEAR lv_lines.
          DESCRIBE TABLE lt_active_lp[] LINES lv_lines.
          IF lv_lines GT 1.

* If still multiple list price still exist
* then, consider list price latest last changed date
            SORT lt_active_lp[] BY last_changed_date DESCENDING.
            CLEAR ls_active_lp.
            READ TABLE lt_active_lp[] INTO ls_active_lp INDEX 1.
            IF sy-subrc = 0.

              DELETE lt_active_lp[] WHERE last_changed_date LT ls_active_lp-last_changed_date
                                      AND last_changed_date IS NOT INITIAL.

              CLEAR lv_lines.
              DESCRIBE TABLE lt_active_lp[] LINES lv_lines.
              IF lv_lines GT 1.
* If there are multiple list price valid as on current date and time
* then get the list price as on highest start date
                SORT lt_active_lp[] BY eff_start_date DESCENDING.
                CLEAR ls_active_lp.
                READ TABLE lt_active_lp[] INTO ls_active_lp INDEX 1.
                IF sy-subrc = 0.
* Remove List Price which are not latest
                  DELETE lt_active_lp[] WHERE eff_start_date LT ls_active_lp-eff_start_date.

                  CLEAR lv_lines.
                  DESCRIBE TABLE lt_active_lp[] LINES lv_lines.
                  IF lv_lines GT 1.
* If still multiple list price still exist
* then, consider list price for lowest price
                    SORT lt_active_lp[] BY price_value.
                  ENDIF.  " eff_start_date - IF lv_lines GT 1.
                ENDIF.  "  eff_start_date - READ TABLE lt_active_lp[] INTO ls_active_lp INDEX 1
              ENDIF.  " last_changed_date - IF lv_lines GT 1
            ENDIF.  " last_changed_date - READ TABLE lt_active_lp[] INTO ls_active_lp INDEX 1
          ENDIF.  " currency - IF lv_lines GT 1
        ENDIF.  " main - IF lv_lines GT 1

      ELSE.  " IF lt_active_curr[] IS NOT INITIAL.

        IF lt_active_futr[] IS NOT INITIAL.


          lt_active_lp[] = lt_active_futr[].

          CLEAR lv_lines.
          DESCRIBE TABLE lt_active_lp[] LINES lv_lines.
          IF lv_lines GT 1.


* Get all the common vendor and list price currencies
            CLEAR: lt_active_waers[].
            LOOP AT lt_vendor[] INTO ls_vendor.
              LOOP AT lt_active_lp[] INTO ls_active_waers
              WHERE currency = ls_vendor-waers.

                APPEND ls_active_waers TO lt_active_waers[].
              ENDLOOP.  " LOOP AT lt_active_lp[] INTO ls_active_waers
            ENDLOOP.  " LOOP AT lt_vendor[] INTO ls_vendor

* If there is any common currency, then determine list price from common currencies
            IF lt_active_waers[] IS NOT INITIAL.
              lt_active_lp[] = lt_active_waers[].
            ENDIF.


* If any price is available in NZD then prefer those prices
            READ TABLE lt_active_lp[] TRANSPORTING NO FIELDS
            WITH KEY currency = 'NZD'.
            IF sy-subrc = 0.
              DELETE lt_active_lp[] WHERE currency NE 'NZD'.
            ENDIF.

            CLEAR lv_lines.
            DESCRIBE TABLE lt_active_lp[] LINES lv_lines.
            IF lv_lines GT 1.

* If there are multiple list price valid in future
* then get the list price as on nearest start date to current date
              SORT lt_active_lp[] BY eff_start_date.
              CLEAR ls_active_lp.
              READ TABLE lt_active_lp[] INTO ls_active_lp INDEX 1.
              IF sy-subrc = 0.
* Remove List Price which are not nearest
                DELETE lt_active_lp[] WHERE eff_start_date GT ls_active_lp-eff_start_date.

                CLEAR lv_lines.
                DESCRIBE TABLE lt_active_lp[] LINES lv_lines.
                IF lv_lines GT 1.
* If still multiple list price still exist
* then, consider list price for lowest price
                  SORT lt_active_lp[] BY price_value.

* INS Begin of Change JKH 12.06.2017
                  CLEAR ls_active_lp.
                  READ TABLE lt_active_lp[] INTO ls_active_lp INDEX 1.
                  IF sy-subrc = 0.
* Remove List Price which are not lowest
                    DELETE lt_active_lp[] WHERE price_value GT ls_active_lp-price_value.
                    CLEAR lv_lines.
                    DESCRIBE TABLE lt_active_lp[] LINES lv_lines.
                    IF lv_lines GT 1.

* If still multiple list price still exist
* then, consider list price latest last changed date
                      SORT lt_active_lp[] BY last_changed_date DESCENDING.
                    ENDIF.  " price_value - IF lv_lines GT 1
                  ENDIF.  " price_value - READ TABLE lt_active_lp[] INTO ls_active_lp INDEX 1
* INS End of Change JKH 12.06.2017

                ENDIF.  " eff_start_date - IF lv_lines GT 1.
              ENDIF.  "  eff_start_date - READ TABLE lt_active_lp[] INTO ls_active_lp INDEX 1
            ENDIF.  " currency - IF lv_lines GT 1
          ENDIF.  " main - IF lv_lines GT 1



        ELSE.  " IF lt_active_futr[] IS NOT INITIAL.

          IF lt_active_past[] IS NOT INITIAL.

            lt_active_lp[] = lt_active_past[].

            CLEAR lv_lines.
            DESCRIBE TABLE lt_active_lp[] LINES lv_lines.
            IF lv_lines GT 1.


* Get all the common vendor and list price currencies
              CLEAR: lt_active_waers[].
              LOOP AT lt_vendor[] INTO ls_vendor.
                LOOP AT lt_active_lp[] INTO ls_active_waers
                WHERE currency = ls_vendor-waers.

                  APPEND ls_active_waers TO lt_active_waers[].
                ENDLOOP.  " LOOP AT lt_active_lp[] INTO ls_active_waers
              ENDLOOP.  " LOOP AT lt_vendor[] INTO ls_vendor

* If there is any common currency, then determine list price from common currencies
              IF lt_active_waers[] IS NOT INITIAL.
                lt_active_lp[] = lt_active_waers[].
              ENDIF.


* If any price is available in NZD then prefer those prices
              READ TABLE lt_active_lp[] TRANSPORTING NO FIELDS
              WITH KEY currency = 'NZD'.
              IF sy-subrc = 0.
                DELETE lt_active_lp[] WHERE currency NE 'NZD'.
              ENDIF.


              CLEAR lv_lines.
              DESCRIBE TABLE lt_active_lp[] LINES lv_lines.
              IF lv_lines GT 1.

* If there are multiple list price valid in past
* then get the list price as on nearest end date to current date
                SORT lt_active_lp[] BY eff_end_date DESCENDING.
                CLEAR ls_active_lp.
                READ TABLE lt_active_lp[] INTO ls_active_lp INDEX 1.
                IF sy-subrc = 0.
* Remove List Price which are not nearest
                  DELETE lt_active_lp[] WHERE eff_end_date LT ls_active_lp-eff_end_date.

                  CLEAR lv_lines.
                  DESCRIBE TABLE lt_active_lp[] LINES lv_lines.
                  IF lv_lines GT 1.
* If still multiple list price still exist
* then, consider list price for lowest price
                    SORT lt_active_lp[] BY price_value.

* INS Begin of Change JKH 12.06.2017
                    CLEAR ls_active_lp.
                    READ TABLE lt_active_lp[] INTO ls_active_lp INDEX 1.
                    IF sy-subrc = 0.
* Remove List Price which are not lowest
                      DELETE lt_active_lp[] WHERE price_value GT ls_active_lp-price_value.
                      CLEAR lv_lines.
                      DESCRIBE TABLE lt_active_lp[] LINES lv_lines.
                      IF lv_lines GT 1.

* If still multiple list price still exist
* then, consider list price latest last changed date
                        SORT lt_active_lp[] BY last_changed_date DESCENDING.
                      ENDIF.  " price_value - IF lv_lines GT 1
                    ENDIF.  " price_value - READ TABLE lt_active_lp[] INTO ls_active_lp INDEX 1
* INS End of Change JKH 12.06.2017

                  ENDIF.  " eff_start_date - IF lv_lines GT 1.
                ENDIF.  "  eff_start_date - READ TABLE lt_active_lp[] INTO ls_active_lp INDEX 1

              ENDIF.  " currency - IF lv_lines GT 1
            ENDIF.  " main - IF lv_lines GT 1

          ELSE.  " IF lt_active_past[] IS NOT INITIAL.

            CLEAR lt_active_lp[].

          ENDIF.  " IF lt_active_past[] IS NOT INITIAL.
        ENDIF.  " IF lt_active_futr[] IS NOT INITIAL.
      ENDIF.  " IF lt_active_curr[] IS NOT INITIAL.





      IF lt_active_lp[] IS INITIAL.
        <ls_list_price>-active_lp = abap_true.
      ELSE.
        CLEAR ls_active_lp.
        READ TABLE lt_active_lp[] INTO ls_active_lp INDEX 1.
        IF sy-subrc = 0.

          IF <ls_lp_active> IS ASSIGNED. UNASSIGN <ls_lp_active>. ENDIF.
          READ TABLE lt_list_price[] ASSIGNING <ls_lp_active>
          WITH KEY idno     = ls_active_lp-idno
                   version  = ls_active_lp-version
                   uom_code = ls_active_lp-uom_code
                   gln      = ls_active_lp-gln
                   seqno    = ls_active_lp-seqno.
          IF sy-subrc = 0.
            <ls_lp_active>-active_lp = abap_true.
          ELSE.
            <ls_list_price>-active_lp = abap_true.
          ENDIF.
        ENDIF.
      ENDIF.

      CLEAR: lt_active_lp[], lt_active_past[], lt_active_curr[], lt_active_futr[].
    ENDAT.

  ENDLOOP.  " LOOP AT lt_list_price[] ASSIGNING <ls_list_price>



  fc_t_list_price[] = lt_list_price[].

ENDFORM.                    " LIST_PRICE_ACTIVE_DEFAULT

*&---------------------------------------------------------------------*
*&      Form  BUILD_PROD_VERSION_TO_COMPARE
*&---------------------------------------------------------------------*
* Build Product Version to be compared
*----------------------------------------------------------------------*
FORM build_prod_version_to_compare  USING    fu_s_prod_data_n    TYPE ty_s_prod_data
                                             fu_t_field_confg    TYPE ty_t_field_confg
                                             fu_t_tabname        TYPE ty_t_tabname
                                             fu_t_ver_status_n   TYPE ty_t_ver_status
                                             fu_t_ver_status_all TYPE ty_t_ver_status
                                             fu_v_idno           TYPE zarn_idno
                                             fu_v_id_type        TYPE zarn_id_type    "++3171 JKH 05.05.2017
                                    CHANGING fc_s_prod_data_c    TYPE ty_s_prod_data.


  DATA: ls_tabname     TYPE ty_s_tabname,
        ls_prod_data   TYPE ty_s_prod_data,
        ls_prod_data_o TYPE ty_s_prod_data,
        ls_ver_status  TYPE ty_s_ver_status,
        lv_itabname    TYPE char50,
        lv_where_str   TYPE string.


  FIELD-SYMBOLS: <ls_prod_data_o> TYPE ty_s_prod_data,
                 <tabname>        TYPE tabname,
                 <itabname>       TYPE table,
                 <table_o>        TYPE table,
                 <table>          TYPE table,
                 <table_str>      TYPE any,
                 <table_val>      TYPE any.

  CLEAR fc_s_prod_data_c.

* INS Begin of Change JKH 13.09.2016
  CLEAR ls_ver_status.

  IF fu_v_id_type NE 'C'.   "++3171 JKH 05.05.2017

* Check if Article Version Exist
    READ TABLE fu_t_ver_status_n[] INTO ls_ver_status
      WITH KEY idno = fu_s_prod_data_n-idno.
    IF sy-subrc = 0.
* Build data as per IDNO and Article Version
      CLEAR: ls_prod_data_o.
      ls_prod_data_o-idno    = ls_ver_status-idno.
      ls_prod_data_o-version = ls_ver_status-article_ver.
    ELSE.

* If Article Version doesn't exist and Current Version exist with APR status
      READ TABLE fu_t_ver_status_all[] INTO ls_ver_status
        WITH KEY idno = fu_s_prod_data_n-idno.
      IF sy-subrc = 0.
* Build data as per IDNO and Current Version
        CLEAR: ls_prod_data_o.
        ls_prod_data_o-idno    = ls_ver_status-idno.
        ls_prod_data_o-version = ls_ver_status-current_ver.
      ENDIF.  " READ TABLE fu_t_ver_status_all[] INTO ls_ver_status
    ENDIF.  " READ TABLE fu_t_ver_status_n[] INTO ls_ver_status
* INS End of Change JKH 13.09.2016

* INS Begin of Change 3171 JKH 05.05.2017
  ELSEIF fu_v_id_type EQ 'C'.

* If Current Version exist
    READ TABLE fu_t_ver_status_all[] INTO ls_ver_status
      WITH KEY idno = fu_s_prod_data_n-idno.
    IF sy-subrc = 0.
* Build data as per IDNO and Current Version
      CLEAR: ls_prod_data_o.
      ls_prod_data_o-idno    = ls_ver_status-idno.
      ls_prod_data_o-version = ls_ver_status-current_ver.
    ENDIF.  " READ TABLE fu_t_ver_status_all[] INTO ls_ver_status
  ENDIF.
* INS End of Change 3171 JKH 05.05.2017

  IF ls_ver_status IS NOT INITIAL.

** For all the data retreived for Product
* Build data as per IDNO and Version
* Fill fields in internal tables for each IDNO and Version which are configured
*   to be compared (version_compare is TRUE)
* Primary Keys will not be compared even if they are configured - as this is just
* to know if any attribute is changed or not. Actual change determination is done in
* change category determination
    LOOP AT fu_t_tabname INTO ls_tabname.

      IF <table_o>    IS ASSIGNED. UNASSIGN <table_o>.    ENDIF.
      IF <table>      IS ASSIGNED. UNASSIGN <table>.      ENDIF.
      IF <table_str>  IS ASSIGNED. UNASSIGN <table_str>.  ENDIF.
      IF <table_val>  IS ASSIGNED. UNASSIGN <table_val>.  ENDIF.

      CLEAR: lv_itabname.
      CONCATENATE 'LS_PROD_DATA_O-' ls_tabname-tabname INTO lv_itabname.
      ASSIGN (lv_itabname) TO <table_o>.


      CLEAR: lv_itabname.
      CONCATENATE 'FU_S_PROD_DATA_N-' ls_tabname-tabname INTO lv_itabname.
      ASSIGN (lv_itabname) TO <table>.

      <table_o> = <table>.

* Build WHERE clause string as data has to be retreived for IDNO and VERSION only
      CLEAR lv_where_str.
*      CONCATENATE 'IDNO EQ ' ls_ver_status-idno "'AND version EQ ' ls_ver_status_O-current_ver
*      INTO lv_where_str SEPARATED BY space.

      CONCATENATE 'IDNO EQ ' '''' INTO lv_where_str SEPARATED BY space.
      CONCATENATE lv_where_str ls_ver_status-idno '''' INTO lv_where_str.


      LOOP AT <table_o> ASSIGNING <table_str>
        WHERE (lv_where_str).

        go_inbound_utils = zcl_arn_inbound_utils=>get_instance( ).
        go_inbound_utils->set_field_config( fu_t_field_confg ).
        go_inbound_utils->prepare_struc_for_compare( EXPORTING iv_strucname = ls_tabname-tabname
                                                     CHANGING cs_structure = <table_str> ).


** Collect data to be compared
*        APPEND <table_str> TO <table_O>.
      ENDLOOP.  " LOOP AT <table> ASSIGNING <table_str>

      IF <table_o> IS ASSIGNED AND <table_o> IS NOT INITIAL.
        SORT <table_o>.
      ENDIF.

    ENDLOOP. " LOOP AT fc_t_tabname INTO ls_tabname.

    fc_s_prod_data_c = ls_prod_data_o.

*  ENDLOOP. " LOOP AT fc_t_ver_status_O[] INTO ls_ver_status_O.
  ENDIF.



ENDFORM.                    " BUILD_PROD_VERSION_TO_COMPARE
*&---------------------------------------------------------------------*
*&      Form  CHECK_DATA_CHANGES
*&---------------------------------------------------------------------*
* Check Data Changes
*----------------------------------------------------------------------*
FORM check_data_changes USING fu_s_prod_data_o    TYPE ty_s_prod_data
                              fu_s_prod_data_c    TYPE ty_s_prod_data
                              fu_v_idno           TYPE zarn_idno
                     CHANGING fc_v_change         TYPE flag.

  CLEAR fc_v_change.

* If Latest and Comaparing Versions are different, then set change flag
  IF fu_s_prod_data_o NE fu_s_prod_data_c.
    fc_v_change = abap_true.
  ENDIF.


ENDFORM.                    " CHECK_DATA_CHANGES
*&---------------------------------------------------------------------*
*&      Form  GENERATE_VERSION
*&---------------------------------------------------------------------*
* Generate Version
*----------------------------------------------------------------------*
FORM generate_version USING fu_s_master_product  TYPE zmaster_products_product
                            fu_s_prod_data_o     TYPE ty_s_prod_data
                            fu_v_change          TYPE flag
                            fu_t_tabname         TYPE ty_t_tabname
                            fu_v_idno            TYPE zarn_idno
                            fu_v_guid            TYPE zarn_guid
                            fu_v_id_type         TYPE zarn_id_type
                            fu_t_ovr_version     TYPE ty_t_ovr_version
                            fu_t_ver_status_all  TYPE ty_t_ver_status
                            fu_t_ovr_mode_range  TYPE ty_t_ovr_mode_range
                   CHANGING fc_s_prod_data_n     TYPE ty_s_prod_data
                            fc_t_ver_status      TYPE ty_t_ver_status
                            fc_t_prd_version     TYPE ty_t_prd_version
                            fc_t_prd_version_ovr TYPE ty_t_prd_version
                            fc_v_ver_status      TYPE zarn_version_status
                   RAISING  cx_uuid_error.


  DATA : ls_ver_status     TYPE ty_s_ver_status,
         ls_ver_status_all TYPE ty_s_ver_status,
         ls_ovr_version    TYPE ty_s_ovr_version.

  CLEAR fc_v_ver_status.

  FIELD-SYMBOLS : <ls_ver_status>     TYPE ty_s_ver_status.

  CLEAR ls_ver_status_all.
  READ TABLE fu_t_ver_status_all[] INTO ls_ver_status_all
  WITH KEY idno = fu_v_idno.
*  IF sy-subrc NE 0.

  IF fu_s_prod_data_o IS INITIAL.

* Generate First Version

    CLEAR: ls_ver_status.
    ls_ver_status-mandt          = sy-mandt.
    ls_ver_status-idno           = fu_v_idno.

    IF ls_ver_status_all IS INITIAL.
      ls_ver_status-current_ver    = 1.
      ls_ver_status-latest_ver     = 1.
    ELSE.
      ls_ver_status-previous_ver   = ls_ver_status_all-current_ver.
      ls_ver_status-current_ver    = ls_ver_status_all-latest_ver + 1.
      ls_ver_status-latest_ver     = ls_ver_status_all-latest_ver + 1.

      ls_ver_status-changed_on     = sy-datum.
      ls_ver_status-changed_at     = sy-uzeit.
      ls_ver_status-changed_by     = sy-uname.
    ENDIF.


* For 'OVR' mode, if IDNO already exist with 'OVR' mode then
* National Data should be updated with the new data but version should remain same
* no version increment
    IF fu_v_id_type IN fu_t_ovr_mode_range[].
      CLEAR ls_ovr_version.
      READ TABLE fu_t_ovr_version[] INTO ls_ovr_version
      WITH KEY idno = fu_v_idno.
      IF sy-subrc = 0.
        ls_ver_status-previous_ver = ls_ovr_version-previous_ver. "+PC 6/12
        ls_ver_status-current_ver  = ls_ovr_version-current_ver.
        ls_ver_status-latest_ver   = ls_ovr_version-latest_ver.
      ENDIF.
    ENDIF.  " IF fu_v_id_type IN fu_t_ovr_mode_range[]


*        ls_ver_status-changed_on    = sy-datum.
*        ls_ver_status-changed_at    = sy-uzeit.
*        ls_ver_status-changed_by    = sy-uname.


    IF fu_v_id_type EQ 'M'.
      fc_v_ver_status = 'MIG'.
    ELSE.
      fc_v_ver_status = 'NEW'.
    ENDIF.

* Build Product Version Data
    PERFORM build_prod_version_data USING fc_v_ver_status
                                          fu_v_guid
                                          fu_v_id_type
                                          fu_t_ovr_mode_range[]
                                 CHANGING ls_ver_status
                                          fc_t_prd_version[]
                                          fc_t_prd_version_ovr[].

* Build National Data
    PERFORM build_national_data CHANGING ls_ver_status
                                         fc_s_prod_data_n.


    IF <ls_ver_status> IS ASSIGNED. UNASSIGN <ls_ver_status>. ENDIF.
    READ TABLE fc_t_ver_status[] ASSIGNING <ls_ver_status>
    WITH KEY idno = fu_v_idno.
    IF sy-subrc = 0.
      <ls_ver_status> = ls_ver_status.
    ELSE.
      APPEND ls_ver_status TO fc_t_ver_status[].
    ENDIF.



  ELSE.

* Get Latest Version
    IF <ls_ver_status> IS ASSIGNED. UNASSIGN <ls_ver_status>. ENDIF.
    READ TABLE fc_t_ver_status[] ASSIGNING <ls_ver_status>
    WITH KEY idno = fu_v_idno.
    IF sy-subrc NE 0.
      APPEND ls_ver_status_all TO fc_t_ver_status[].

* Get Latest Version
      IF <ls_ver_status> IS ASSIGNED. UNASSIGN <ls_ver_status>. ENDIF.
      READ TABLE fc_t_ver_status[] ASSIGNING <ls_ver_status>
      WITH KEY idno = fu_v_idno.
    ENDIF.  " IF sy-subrc = 0, ver_status

    IF <ls_ver_status> IS ASSIGNED.
      IF fu_v_change = abap_true.
* If there is any change in Data then, generate new Version
* and maintain Product with new version

        <ls_ver_status>-previous_ver   = <ls_ver_status>-current_ver.
        <ls_ver_status>-current_ver    = <ls_ver_status>-latest_ver + 1.
        <ls_ver_status>-latest_ver     = <ls_ver_status>-latest_ver + 1.

        <ls_ver_status>-changed_on     = sy-datum.
        <ls_ver_status>-changed_at     = sy-uzeit.
        <ls_ver_status>-changed_by     = sy-uname.


        fc_v_ver_status = 'CHG'.

* Build Product Version Data
        PERFORM build_prod_version_data USING fc_v_ver_status
                                              fu_v_guid
                                              fu_v_id_type
                                              fu_t_ovr_mode_range[]
                                     CHANGING <ls_ver_status>
                                              fc_t_prd_version[]
                                              fc_t_prd_version_ovr[].

* Build National Data
        PERFORM build_national_data CHANGING <ls_ver_status>
                                             fc_s_prod_data_n.

*        APPEND <ls_ver_status> TO fc_t_ver_status[].
      ELSE.
* If there is no change in Data then, generate new Version
* and maintain Product with existing version

        <ls_ver_status>-latest_ver     = <ls_ver_status>-latest_ver + 1.

        IF <ls_ver_status>-article_ver IS NOT INITIAL.
          <ls_ver_status>-current_ver    = <ls_ver_status>-article_ver.
        ENDIF.

        <ls_ver_status>-changed_on     = sy-datum.
        <ls_ver_status>-changed_at     = sy-uzeit.
        <ls_ver_status>-changed_by     = sy-uname.


        fc_v_ver_status = 'NCH'.

* Build Product Version Data
        PERFORM build_prod_version_data USING fc_v_ver_status
                                              fu_v_guid
                                              fu_v_id_type
                                              fu_t_ovr_mode_range[]
                                     CHANGING <ls_ver_status>
                                              fc_t_prd_version[]
                                              fc_t_prd_version_ovr[].
* No National Data is required to be created/updated
        CLEAR fc_s_prod_data_n.

*        APPEND <ls_ver_status> TO fc_t_ver_status[].

      ENDIF.  " IF fu_v_change = abap_true
    ENDIF.  " IF <ls_ver_status> IS ASSIGNED
  ENDIF.  " IF fu_s_prod_data_O IS INITIAL


ENDFORM.                    " GENERATE_VERSION
*&---------------------------------------------------------------------*
*&      Form  BUILD_PROD_VERSION_DATA
*&---------------------------------------------------------------------*
* Build Product Version Data
*----------------------------------------------------------------------*
FORM build_prod_version_data USING fu_v_ver_status      TYPE zarn_version_status
                                   fu_v_guid            TYPE zarn_guid
                                   fu_v_id_type         TYPE zarn_id_type
                                   fu_t_ovr_mode_range  TYPE ty_t_ovr_mode_range
                          CHANGING fc_s_ver_status      TYPE ty_s_ver_status
                                   fc_t_prd_version     TYPE ty_t_prd_version
                                   fc_t_prd_version_ovr TYPE ty_t_prd_version.

  DATA: ls_prd_version    TYPE ty_s_prd_version.


  FIELD-SYMBOLS: <ls_prd_version>   TYPE ty_s_prd_version.


  CLEAR: ls_prd_version.


* AReNa: Product Version Master
  ls_prd_version-mandt          = sy-mandt.
  ls_prd_version-idno           = fc_s_ver_status-idno.
  ls_prd_version-version        = fc_s_ver_status-latest_ver.
  ls_prd_version-id_type        = fu_v_id_type.
  ls_prd_version-guid           = fu_v_guid.
  ls_prd_version-version_status = fu_v_ver_status.


  READ TABLE fc_t_prd_version[] ASSIGNING <ls_prd_version>
  WITH KEY idno    = fc_s_ver_status-idno
           version = fc_s_ver_status-latest_ver.
  IF sy-subrc = 0.
    <ls_prd_version> = ls_prd_version.
    <ls_prd_version>-changed_on = sy-datum.
    <ls_prd_version>-changed_at = sy-uzeit.
    <ls_prd_version>-changed_by = sy-uname.
  ELSE.
    APPEND ls_prd_version TO fc_t_prd_version[].
  ENDIF.




  IF fu_v_id_type IN fu_t_ovr_mode_range[] AND fu_v_ver_status = 'CHG'.
    APPEND ls_prd_version TO fc_t_prd_version_ovr[].
  ENDIF.


ENDFORM.                    " BUILD_PROD_VERSION_DATA
*&---------------------------------------------------------------------*
*&      Form  BUILD_NATIONAL_DATA
*&---------------------------------------------------------------------*
* Build National Data
*----------------------------------------------------------------------*
FORM build_national_data CHANGING fc_s_ver_status     TYPE ty_s_ver_status
                                  fc_s_prod_data_n    TYPE ty_s_prod_data
                         RAISING  cx_uuid_error.

  DATA : lv_itabname       TYPE char50,
         lv_where_str      TYPE string,
         lt_dfies          TYPE dfies_tab,
         ls_dfies          TYPE dfies,
         lt_dfies_guid_key TYPE dfies_tab.

  FIELD-SYMBOLS: <table>     TYPE table,
                 <table_str> TYPE any,
                 <table_val> TYPE any.

  fc_s_prod_data_n-version = fc_s_ver_status-latest_ver.

* Get Fields of the table
  REFRESH: lt_dfies[].
  CALL FUNCTION 'DDIF_FIELDINFO_GET'
    EXPORTING
      tabname        = 'ZSARN_PROD_DATA'
      langu          = sy-langu
      all_types      = abap_true
    TABLES
      dfies_tab      = lt_dfies[]
    EXCEPTIONS
      not_found      = 1
      internal_error = 2
      OTHERS         = 3.
  IF sy-subrc <> 0.
    EXIT.
  ENDIF.


  LOOP AT lt_dfies[] INTO ls_dfies.

    IF ls_dfies-fieldname = 'IDNO' OR ls_dfies-fieldname = 'VERSION'.
      CONTINUE.
    ENDIF.

    IF <table>      IS ASSIGNED. UNASSIGN <table>.      ENDIF.

    CLEAR: lv_itabname.
    CONCATENATE 'FC_S_PROD_DATA_N-' ls_dfies-fieldname INTO lv_itabname.
    ASSIGN (lv_itabname) TO <table>.

* Build WHERE clause string as data has to be retreived for IDNO
    CLEAR lv_where_str.
*    CONCATENATE 'IDNO EQ ' space '''' fc_s_ver_status-idno ''''
*    INTO lv_where_str." SEPARATED BY space.

    CONCATENATE 'IDNO EQ '  '''' INTO lv_where_str  SEPARATED BY space.
    CONCATENATE lv_where_str fc_s_ver_status-idno '''' INTO lv_where_str.

    " Identify the key fields of the tables that are GUIDs - required to
    " generate GUID
    CLEAR lt_dfies_guid_key[].
    CALL FUNCTION 'DDIF_FIELDINFO_GET'
      EXPORTING
        tabname        = ls_dfies-fieldname
        langu          = sy-langu
        all_types      = abap_true
      TABLES
        dfies_tab      = lt_dfies_guid_key[]
      EXCEPTIONS
        not_found      = 1
        internal_error = 2
        OTHERS         = 3.
    IF sy-subrc IS INITIAL.
      DELETE lt_dfies_guid_key WHERE NOT ( keyflag = abap_true
                                       AND domname = 'SYSUUID' ).
    ELSE.
      CLEAR lt_dfies_guid_key[].
    ENDIF.

    LOOP AT <table> ASSIGNING <table_str>
      WHERE (lv_where_str).

      ASSIGN COMPONENT 'VERSION' OF STRUCTURE <table_str> TO <table_val>.
      <table_val> = fc_s_ver_status-latest_ver.

      " Generate the GUID where relevant
      LOOP AT lt_dfies_guid_key INTO DATA(ls_dfies_guid).
        ASSIGN COMPONENT ls_dfies_guid-fieldname OF STRUCTURE <table_str> TO <table_val>.
        IF sy-subrc IS INITIAL
       AND <table_val> IS INITIAL.
          <table_val> = cl_system_uuid=>create_uuid_x16_static( ).
        ENDIF.
      ENDLOOP.

    ENDLOOP.  " LOOP AT <table> ASSIGNING <table_str>.

  ENDLOOP.  " LOOP AT lt_dfies[] INTO ls_dfies.


ENDFORM.                    " BUILD_NATIONAL_DATA
*&---------------------------------------------------------------------*
*&      Form  MAINTAIN_SLG1_LOG
*&---------------------------------------------------------------------*
* Maintain SLG1 Log
*----------------------------------------------------------------------*
FORM maintain_slg1_log USING fu_t_message TYPE bal_t_msg
                    CHANGING fc_v_log_no  TYPE balognr.

  DATA: ls_s_log      TYPE bal_s_log,
        ls_log_handle TYPE balloghndl,
        lt_message    TYPE bal_t_msg,
        ls_message    TYPE bal_s_msg.

  lt_message[] = fu_t_message[].

  ls_s_log-object    = 'ZARN'.
  ls_s_log-subobject = 'ZARN_INBOUND'.
  ls_s_log-aldate    = sy-datum.
  ls_s_log-altime    = sy-uzeit.
  ls_s_log-aluser    = sy-uname.
  ls_s_log-altcode   = sy-tcode.

* Create Log
  PERFORM create_log USING ls_s_log
                  CHANGING ls_log_handle.


  LOOP AT lt_message INTO ls_message.
* Write Message to Log
    PERFORM write_msg USING ls_log_handle
                   CHANGING ls_message.

  ENDLOOP.

* Save and Display the log
  PERFORM save_log USING ls_log_handle
                         abap_true  " Save
                         space      " Display
                CHANGING fc_v_log_no.


ENDFORM.                    " MAINTAIN_SLG1_LOG
*&---------------------------------------------------------------------*
*&      Form  CREATE_LOG
*&---------------------------------------------------------------------*
* Create Log
*----------------------------------------------------------------------*
FORM create_log USING fu_s_log        TYPE bal_s_log
             CHANGING fc_s_log_handle TYPE balloghndl.

  CLEAR fc_s_log_handle.
  CALL FUNCTION 'BAL_LOG_CREATE'
    EXPORTING
      i_s_log                 = fu_s_log
    IMPORTING
      e_log_handle            = fc_s_log_handle
    EXCEPTIONS
      log_header_inconsistent = 1
      OTHERS                  = 2.
  IF sy-subrc <> 0.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
            WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.


ENDFORM.                    " CREATE_LOG
*&---------------------------------------------------------------------*
*&      Form  WRITE_MSG
*&---------------------------------------------------------------------*
* Write Message to Log
*----------------------------------------------------------------------*
FORM write_msg  USING fu_s_log_handle TYPE balloghndl
             CHANGING fc_s_message    TYPE bal_s_msg.


  CALL FUNCTION 'BAL_LOG_MSG_ADD'
    EXPORTING
      i_log_handle     = fu_s_log_handle
      i_s_msg          = fc_s_message
    EXCEPTIONS
      log_not_found    = 1
      msg_inconsistent = 2
      log_is_full      = 3
      OTHERS           = 4.

  IF sy-subrc <> 0.
    WRITE : 'Write to Log Failed'(001).
  ENDIF.

ENDFORM.                    " WRITE_MSG
*&---------------------------------------------------------------------*
*&      Form  SAVE_LOG
*&---------------------------------------------------------------------*
* Save and Display the log
*----------------------------------------------------------------------*
FORM save_log  USING fu_s_log_handle TYPE balloghndl
                     fi_v_save       TYPE boole_d
                     fi_v_display    TYPE boole_d
            CHANGING fc_v_log_no  TYPE balognr.

  DATA: lt_log_handle TYPE bal_t_logh,
        lt_lognumbers TYPE bal_t_lgnm,
        ls_lognumbers TYPE bal_s_lgnm.

  APPEND fu_s_log_handle TO lt_log_handle.

  IF fi_v_save = abap_true.

    REFRESH: lt_lognumbers[].
    CALL FUNCTION 'BAL_DB_SAVE'
      EXPORTING
        i_t_log_handle   = lt_log_handle
      IMPORTING
        e_new_lognumbers = lt_lognumbers[]
      EXCEPTIONS
        log_not_found    = 1
        save_not_allowed = 2
        numbering_error  = 3
        OTHERS           = 4.
    IF sy-subrc <> 0.

      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
              WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ELSE.

      CLEAR ls_lognumbers.
      READ TABLE lt_lognumbers[] INTO ls_lognumbers
      WITH KEY log_handle = fu_s_log_handle.
      IF sy-subrc = 0.
        fc_v_log_no = ls_lognumbers-lognumber.
      ENDIF.

    ENDIF.

  ENDIF.

  IF fi_v_display = abap_true.

    CALL FUNCTION 'BAL_DSP_LOG_DISPLAY'
      EXPORTING
        i_t_log_handle       = lt_log_handle
      EXCEPTIONS
        profile_inconsistent = 1
        internal_error       = 2
        no_data_available    = 3
        no_authority         = 4
        OTHERS               = 5.
    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
              WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

  ENDIF.

ENDFORM.                    " SAVE_LOG
*&---------------------------------------------------------------------*
*&      Form  SEND_ERROR_EMAIL
*&---------------------------------------------------------------------*
* Send Email for Products in Error
*----------------------------------------------------------------------*
FORM send_error_email USING fu_t_errmail_data TYPE ty_t_errmail_data
                   CHANGING fc_v_error        TYPE flag.


  DATA : lt_errmail_data      TYPE ty_t_errmail_data,
         ls_errmail_data      TYPE ty_s_errmail_data,
         lt_errmail_data_tmp  TYPE ty_t_errmail_data,
         ls_errmail_data_tmp  TYPE ty_s_errmail_data,

         ls_head              TYPE thead,
         lv_string_cre        TYPE string,
         lv_string_guid       TYPE string,
         lv_string_cre_data   TYPE string,
*         lv_string_cha        TYPE string,
*         lv_catman_email      TYPE ad_smtpadr,
         lt_excel_art_created TYPE solix_tab,
         ls_excel_art_created TYPE solix,
*         lt_excel_art_changed TYPE solix_tab,
*         ls_excel_art_changed TYPE solix,
         lv_cre_size          TYPE so_obj_len,
*         lv_cha_size          TYPE so_obj_len,
*         lv_art_changed       TYPE flag,
         lv_art_created       TYPE flag,
         ls_lines             TYPE tline,
         lt_lines             TYPE tline_t,
         lt_body              TYPE bcsy_text,
         ls_body              TYPE soli,
*         ls_pricat_email      TYPE zsmd_pricat_email,
*         ls_email             TYPE zsmd_pricat_email_add.

         lo_send_request      TYPE REF TO cl_bcs,
         lo_recipient         TYPE REF TO if_recipient_bcs,
         lv_xstring           TYPE xstring,
         lv_len               TYPE char5,
         lo_excp              TYPE REF TO cx_send_req_bcs.   "++SUP-20 JKH 02.10.2017



  CLEAR fc_v_error.

  lt_errmail_data[] = fu_t_errmail_data[].

  IF lt_errmail_data[] IS INITIAL.
    EXIT.
  ELSE.
    SORT lt_errmail_data[] BY email_id.
  ENDIF.



* Read Email Content
  CLEAR: ls_head.
  REFRESH: lt_lines[].
  CALL FUNCTION 'READ_TEXT'
    EXPORTING
      client                  = sy-mandt
      id                      = gc_errmail-std_text_id
      language                = sy-langu
      name                    = gc_errmail-std_text_name
      object                  = gc_errmail-std_text_obj
    IMPORTING
      header                  = ls_head
    TABLES
      lines                   = lt_lines
    EXCEPTIONS
      id                      = 1
      language                = 2
      name                    = 3
      not_found               = 4
      object                  = 5
      reference_check         = 6
      wrong_access_to_archive = 7
      OTHERS                  = 8.
  IF sy-subrc NE 0.
*     Implement suitable error handling here - Read Text failed.
    CLEAR ls_head.
    REFRESH lt_lines.
    fc_v_error = abap_true.
    EXIT.
  ENDIF.


  "Create send request
  CLEAR lo_send_request.

  TRY.
      lo_send_request = cl_bcs=>create_persistent( ).
    CATCH cx_send_req_bcs INTO lo_excp.   "++SUP-20 JKH 02.10.2017
  ENDTRY.

  REFRESH: lt_errmail_data_tmp[].

  LOOP AT lt_errmail_data[] INTO ls_errmail_data.

    APPEND ls_errmail_data TO lt_errmail_data_tmp[].

    AT END OF email_id.

* Prepare the body of email from the standard text
      LOOP AT lt_lines INTO ls_lines.
        ls_body-line = ls_lines-tdline.

        CLEAR lv_len.
        DESCRIBE TABLE lt_errmail_data_tmp[] LINES lv_len.
        CONDENSE lv_len NO-GAPS.
        REPLACE '&error_cnt&' IN ls_body-line WITH lv_len.

        APPEND ls_body TO lt_body.
      ENDLOOP.

      CLEAR lo_recipient.
      TRY.
          lo_recipient = cl_cam_address_bcs=>create_internet_address( ls_errmail_data-email_id ).
        CATCH cx_address_bcs.
          "Ignore
      ENDTRY.
      IF NOT lo_recipient IS INITIAL.
        TRY.
            "Add recipient to send request
            CALL METHOD lo_send_request->add_recipient
              EXPORTING
                i_recipient = lo_recipient
                i_express   = abap_true.
          CATCH cx_send_req_bcs INTO lo_excp.   "++SUP-20 JKH 02.10.2017 " MC not received as a change from PIM
        ENDTRY.
      ENDIF.



* Unique to Recipient
      REFRESH: lt_excel_art_created.
      CLEAR:   ls_excel_art_created,
               lv_cre_size,
               lv_art_created,
               lv_string_cre.

* Fill Excel Header
      CONCATENATE gc_errmail-hdr1
                  gc_errmail-hdr2
                  gc_errmail-hdr3
                  gc_errmail-hdr4
                  gc_errmail-hdr5
                  gc_errmail-hdr6
                  gc_errmail-hdr7
                  gc_errmail-hdr8
                  gc_errmail-hdr9
                  gc_errmail-hdr10
                  gc_errmail-hdr11
                  gc_errmail-hdr12
                  gc_errmail-hdr13
                  gc_errmail-hdr14
                  gc_errmail-hdr15
                  gc_errmail-hdr16
                  gc_errmail-hdr17
                  gc_errmail-hdr18
                  gc_errmail-hdr19
             INTO lv_string_cre
             SEPARATED BY gc_errmail-tab.

      CONCATENATE lv_string_cre gc_errmail-crlf INTO lv_string_cre.

* Fill Excel Data
      LOOP AT lt_errmail_data_tmp[] INTO ls_errmail_data_tmp.
        CLEAR: lv_string_guid, lv_string_cre_data.

        lv_string_guid = ls_errmail_data_tmp-email_data-guid.

        CONCATENATE lv_string_guid
                    ls_errmail_data_tmp-email_data-created_on
                    ls_errmail_data_tmp-email_data-created_at
                    ls_errmail_data_tmp-email_data-created_by
                    ls_errmail_data_tmp-email_data-fan_id
                    ls_errmail_data_tmp-email_data-arn_mode
                    ls_errmail_data_tmp-email_data-code
                    ls_errmail_data_tmp-email_data-matkl
                    ls_errmail_data_tmp-email_data-ret_bcode
                    ls_errmail_data_tmp-email_data-inn_bcode
                    ls_errmail_data_tmp-email_data-ship_bcode
                    ls_errmail_data_tmp-email_data-vend_name
                    ls_errmail_data_tmp-email_data-name
                    ls_errmail_data_tmp-email_data-descr
                    ls_errmail_data_tmp-email_data-fs_descr_s
                    ls_errmail_data_tmp-email_data-brandid
                    ls_errmail_data_tmp-email_data-sub_brndid
                    ls_errmail_data_tmp-email_data-fs_brandid
                    ls_errmail_data_tmp-email_data-fs_sbbrnid
               INTO: lv_string_cre_data
               SEPARATED BY gc_errmail-tab.

        CONCATENATE lv_string_cre_data gc_errmail-crlf INTO lv_string_cre_data.

        CONCATENATE lv_string_cre lv_string_cre_data INTO lv_string_cre.
      ENDLOOP.  " LOOP AT lt_errmail_data_tmp[] INTO ls_errmail_data_tmp



      IF NOT lv_string_cre IS INITIAL.
        CLEAR lv_xstring.
        CALL FUNCTION 'LXE_COMMON_STRING_TO_XSTRING'
          EXPORTING
            in_string   = lv_string_cre
            in_codepage = '0000'
            ex_codepage = '4103'
          IMPORTING
            ex_xstring  = lv_xstring
          EXCEPTIONS
            error       = 1
            OTHERS      = 2.
        IF sy-subrc NE 0.
          CLEAR lv_xstring.
        ENDIF.

        REFRESH: lt_excel_art_created[].
        CALL METHOD cl_bcs_convert=>xstring_to_solix
          EXPORTING
            iv_xstring = lv_xstring
          RECEIVING
            et_solix   = lt_excel_art_created[].

        lv_cre_size  = xstrlen( lv_xstring ).
        lv_art_created = abap_true.
      ENDIF.


      IF lv_art_created IS NOT INITIAL.
* Send Email with Excel Attachment
        PERFORM send_email USING    lo_send_request
                                    lv_cre_size
                           CHANGING fc_v_error
                                    lt_body
                                    lt_excel_art_created.
      ENDIF.


      REFRESH: lt_errmail_data_tmp[].
    ENDAT.
  ENDLOOP.  " LOOP AT lt_errmail_data[] INTO ls_errmail_data


ENDFORM.                    " SEND_ERROR_EMAIL
*&---------------------------------------------------------------------*
*&      Form  BUILD_ERROR_EMAIL_DATA
*&---------------------------------------------------------------------*
* Build Email Data for Error
*----------------------------------------------------------------------*
FORM build_error_email_data USING fu_s_log_params     TYPE zsarn_slg1_log_params_inbound
                                  fu_s_master_product TYPE zmaster_products_product
                                  fu_t_errmail        TYPE ztarn_inb_errmail
                         CHANGING fc_t_errmail_data   TYPE ty_t_errmail_data.

  DATA: ls_errmail_data  TYPE ty_s_errmail_data,
        ls_errmail       TYPE zarn_inb_errmail,
        lv_matkl         TYPE matkl,
        lv_dept_code     TYPE zarn_dept_code,
        lv_sub_dept_code TYPE zarn_sub_dept_code.



  CLEAR lv_matkl.
  lv_matkl         = fu_s_log_params-matkl.
  lv_dept_code     = fu_s_log_params-matkl.
  lv_sub_dept_code = fu_s_log_params-matkl.

  CLEAR ls_errmail.
  READ TABLE fu_t_errmail[] INTO ls_errmail
  WITH KEY dept_code     = lv_dept_code
           sub_dept_code = lv_sub_dept_code
           matkl         = lv_matkl.
  IF sy-subrc NE 0.
    READ TABLE fu_t_errmail[] INTO ls_errmail
    WITH KEY dept_code     = lv_dept_code
             sub_dept_code = lv_sub_dept_code
             matkl         = space.
    IF sy-subrc NE 0.
      READ TABLE fu_t_errmail[] INTO ls_errmail
      WITH KEY dept_code     = lv_dept_code
               sub_dept_code = space
               matkl         = space.
      IF sy-subrc EQ 0.
* Prepare Email Data
        CLEAR ls_errmail_data.
        ls_errmail_data-email_id       = ls_errmail-email_id.
        ls_errmail_data-email_data     = fu_s_log_params.
        ls_errmail_data-master_product = fu_s_master_product.
        APPEND ls_errmail_data TO fc_t_errmail_data[].
      ENDIF.
    ELSE.
* Prepare Email Data
      CLEAR ls_errmail_data.
      ls_errmail_data-email_id       = ls_errmail-email_id.
      ls_errmail_data-email_data     = fu_s_log_params.
      ls_errmail_data-master_product = fu_s_master_product.
      APPEND ls_errmail_data TO fc_t_errmail_data[].
    ENDIF.
  ELSE.
* Prepare Email Data
    CLEAR ls_errmail_data.
    ls_errmail_data-email_id       = ls_errmail-email_id.
    ls_errmail_data-email_data     = fu_s_log_params.
    ls_errmail_data-master_product = fu_s_master_product.
    APPEND ls_errmail_data TO fc_t_errmail_data[].
  ENDIF.



ENDFORM.                    " BUILD_ERROR_EMAIL_DATA
*&---------------------------------------------------------------------*
*&      Form  SEND_EMAIL
*&---------------------------------------------------------------------*
* Send Email with Excel Attachment
*----------------------------------------------------------------------*
FORM send_email  USING fu_o_send_request      TYPE REF TO cl_bcs
                       fu_v_cre_size          TYPE so_obj_len
              CHANGING fc_v_error             TYPE flag
                       fc_t_body              TYPE soli_tab
                       fc_t_excel_art_created TYPE solix_tab.


  CONSTANTS: lc_xls TYPE so_obj_tp    VALUE 'xlsx',
             lc_raw TYPE char03       VALUE 'RAW'.


  DATA: "lv_mlrec         TYPE so_obj_nam,
    lv_sent_to_all   TYPE os_boolean,
    lv_email_subject TYPE so_obj_des,
    lv_xsl_cre_sub   TYPE so_obj_des,
*        lv_error_msg     TYPE string,
    lo_bcs_exception TYPE REF TO cx_bcs,
    lo_sender        TYPE REF TO cl_sapuser_bcs,
    lo_document      TYPE REF TO cl_document_bcs.


  CLEAR lv_email_subject.
  lv_email_subject      = 'Products in Error'.
  lv_xsl_cre_sub        = 'Error'.

  TRY.
      "Email FROM...
      lo_sender = cl_sapuser_bcs=>create( sy-uname ).
      "Add sender to send request
      CALL METHOD fu_o_send_request->set_sender
        EXPORTING
          i_sender = lo_sender.

      "Email BODY
      lo_document = cl_document_bcs=>create_document(
                      i_type    = lc_raw
                      i_text    = fc_t_body
                      i_subject = lv_email_subject ).

      IF lines( fc_t_excel_art_created ) GT 0.
        "add create spread sheet as attachment to document object
        lo_document->add_attachment(
          i_attachment_type    = lc_xls
          i_attachment_subject = lv_xsl_cre_sub
          i_attachment_size    = fu_v_cre_size
          i_att_content_hex    = fc_t_excel_art_created ).

        "Add create XSL document to send request
        CALL METHOD fu_o_send_request->set_document( lo_document ).
      ENDIF.

      "Send email
      CALL METHOD fu_o_send_request->send(
        RECEIVING
          result = lv_sent_to_all ).

** Commit to send email
*      COMMIT WORK.

      "Exception handling
    CATCH cx_bcs INTO lo_bcs_exception.
      CALL METHOD lo_bcs_exception->if_message~get_text
        RECEIVING
          result = fc_v_error.
  ENDTRY.


ENDFORM.                    " SEND_EMAIL
*&---------------------------------------------------------------------*
*&      Form  CHANGE_CATEGORY
*&---------------------------------------------------------------------*
* Change Category
*----------------------------------------------------------------------*
FORM change_category USING fu_t_table_mastr   TYPE ty_t_table_mastr
                           fu_t_prod_data_n   TYPE ty_t_prod_data
                           fu_t_prod_data_db  TYPE ty_t_prod_data
                  CHANGING fc_t_cc_hdr        TYPE ztarn_cc_hdr
                           fc_t_cc_det        TYPE ztarn_cc_det
                           fc_v_chgid         TYPE zarn_chg_id.


  DATA : lt_prod_data_n  TYPE ty_t_prod_data,
         ls_prod_data_n  TYPE ty_s_prod_data,
         lt_prod_data_db TYPE ty_t_prod_data,
         ls_prod_data_db TYPE ty_s_prod_data,
         lv_tabix_del    TYPE sy-tabix,
         lv_chgid        TYPE zarn_chg_id,
         lt_cc_hdr       TYPE ztarn_cc_hdr,
         lt_cc_det       TYPE ztarn_cc_det,
         lt_cc_team      TYPE ztarn_cc_team.

  lt_prod_data_n[]  = fu_t_prod_data_n[].
  lt_prod_data_db[] = fu_t_prod_data_db[].

  IF lt_prod_data_n[] IS NOT INITIAL.
    CALL FUNCTION 'NUMBER_GET_NEXT'
      EXPORTING
        nr_range_nr             = '01'
        object                  = 'ZARN_CHGID'
*       QUANTITY                = '1'
*       SUBOBJECT               = ' '
*       TOYEAR                  = '0000'
*       IGNORE_BUFFER           = ' '
      IMPORTING
        number                  = lv_chgid
*       QUANTITY                =
*       RETURNCODE              =
      EXCEPTIONS
        interval_not_found      = 1
        number_range_not_intern = 2
        object_not_found        = 3
        quantity_is_0           = 4
        quantity_is_not_1       = 5
        interval_overflow       = 6
        buffer_overflow         = 7
        OTHERS                  = 8.
  ELSE.
    EXIT.
  ENDIF.

  LOOP AT lt_prod_data_n[] INTO ls_prod_data_n.

    CLEAR ls_prod_data_db.
    READ TABLE lt_prod_data_db[] INTO ls_prod_data_db
    WITH KEY idno = ls_prod_data_n-idno.
*    version = ls_prod_data_n-version.


    CLEAR: lt_cc_hdr[], lt_cc_det[], lt_cc_team[].
* Build Change Category Data
    CALL FUNCTION 'ZARN_CC_BUILD_NATIONAL_DATA'
      EXPORTING
        it_table_mastr  = fu_t_table_mastr[]
        is_prod_data_db = ls_prod_data_db
        is_prod_data_n  = ls_prod_data_n
        iv_chgid        = lv_chgid
      IMPORTING
        et_cc_hdr       = lt_cc_hdr[]
        et_cc_det       = lt_cc_det[].

    APPEND LINES OF lt_cc_hdr[]  TO fc_t_cc_hdr[].
    APPEND LINES OF lt_cc_det[]  TO fc_t_cc_det[].

  ENDLOOP.

  SORT fc_t_cc_hdr[] BY chgid idno chg_category chg_area.
  SORT fc_t_cc_det[] BY chgid idno chg_category chg_table chg_field reference_data.

  fc_v_chgid = lv_chgid.

ENDFORM.                    " CHANGE_CATEGORY
*&---------------------------------------------------------------------*
*&      Form  SET_VERSION_STATUS_TO_OBS
*&---------------------------------------------------------------------*
* Set Version Status to Obsolete
*----------------------------------------------------------------------*
FORM set_version_status_to_obs USING fu_t_ver_status_o   TYPE ty_t_ver_status
                                     fu_t_ver_status_n   TYPE ty_t_ver_status
                            CHANGING fc_t_prd_version_o  TYPE ty_t_prd_version
                                     fc_t_prd_version_n  TYPE ty_t_prd_version.

* Un-reviewed national version is the version which is having the national header
* status as CHG/NEW. Un-reviewed regional data is having regional header status
* as CHG/NEW.
*
* Un-Posted national version is the version which is having the national header
* status as APR/WIP. Un-Posted regional data is having the regional status
* as CHG/WIP/APR.

* If a new version is introduced, all the previous Un-Reviewed and Un-Posted
* national versions will get the status changed as OBS-Obsolete.
* OBS national version could not be referred/selected to edit the regional data.

  DATA: lt_prd_version   TYPE ty_t_prd_version,
        lt_prd_version_o TYPE ty_t_prd_version,
        ls_prd_version   TYPE ty_s_prd_version,
        ls_ver_status_o  TYPE ty_s_ver_status,
        ls_ver_status_n  TYPE ty_s_ver_status.


  FIELD-SYMBOLS: <ls_prd_version>   TYPE ty_s_prd_version,
                 <ls_prd_version_o> TYPE ty_s_prd_version.


  IF fc_t_prd_version_o[] IS INITIAL.
    EXIT.
  ENDIF.

  IF fu_t_ver_status_n[] IS INITIAL.
    EXIT.
  ENDIF.

  lt_prd_version_o[] = fc_t_prd_version_o[].

  CLEAR: lt_prd_version[].
* Get Product Version Details for Un-Reviewed and Un-Posted versions
  SELECT * FROM zarn_prd_version
    INTO CORRESPONDING FIELDS OF TABLE lt_prd_version[]
    FOR ALL ENTRIES IN fu_t_ver_status_n[]
    WHERE idno           EQ fu_t_ver_status_n-idno
      AND version        LT fu_t_ver_status_n-latest_ver
      AND (   version_status EQ gc_nat_ver_stat-new    "  New
           OR version_status EQ gc_nat_ver_stat-chg    "  Change
           OR version_status EQ gc_nat_ver_stat-wip    "  Work In-Progress
           OR version_status EQ gc_nat_ver_stat-apr ). "  Approved

****************************************
***     Product Version Status       ***
****************************************

  IF lt_prd_version[] IS NOT INITIAL.

    SORT fc_t_prd_version_o[] BY idno version.

    LOOP AT lt_prd_version[] INTO ls_prd_version.

* Don't set the status to OBS for Newer current version
      CLEAR ls_ver_status_n.
      READ TABLE fu_t_ver_status_n[] INTO ls_ver_status_n
      WITH KEY idno = ls_prd_version-idno.
      IF sy-subrc = 0 AND ls_ver_status_n-current_ver NE ls_prd_version-version.

        ls_prd_version-version_status = gc_nat_ver_stat-obs. "  Obsolete
        ls_prd_version-changed_on     = sy-datum.
        ls_prd_version-changed_at     = sy-uzeit.
        ls_prd_version-changed_by     = sy-uname.

* Update New Product Version
        IF <ls_prd_version> IS ASSIGNED. UNASSIGN <ls_prd_version>. ENDIF.
        READ TABLE fc_t_prd_version_n[] ASSIGNING <ls_prd_version>
          WITH KEY idno    = ls_prd_version-idno
                   version = ls_prd_version-version.
        IF sy-subrc = 0.
          <ls_prd_version> = ls_prd_version.
        ELSE.
          APPEND ls_prd_version TO fc_t_prd_version_n[].
        ENDIF.

      ENDIF.  " READ TABLE fu_t_ver_status_o[] INTO ls_ver_status_n
    ENDLOOP.  " LOOP AT lt_prd_version[] INTO ls_prd_version

    SORT fc_t_prd_version_n[] BY idno version.

  ENDIF.  " IF lt_prd_version[] IS NOT INITIAL

ENDFORM.                    " SET_VERSION_STATUS_TO_OBS
*&---------------------------------------------------------------------*
*&      Form  DETERMINE_COMPARISON_VERSION
*&---------------------------------------------------------------------*
*       Determine the comparison version
*----------------------------------------------------------------------*
*      -->P_LS_PRD_VERSION_OVR_N_IDNO  text
*      -->P_LT_PROD_DATA_O  text
*      -->P_LT_PROD_DATA_CURR  text
*      <--P_LV_COMP_VER  text
*----------------------------------------------------------------------*
FORM determine_comparison_version  USING    iv_idno           TYPE zarn_idno
                                            it_prod_data_o    TYPE ty_t_prod_data
                                            it_prod_data_curr TYPE ty_t_prod_data
                                   CHANGING cv_comp_ver       TYPE zarn_version.

  CLEAR cv_comp_ver.

  TRY.
* Get the comparison version from the last article version
      cv_comp_ver = it_prod_data_o[ idno = iv_idno ]-version.
    CATCH cx_sy_itab_line_not_found.
* Get the comparison version from the current approved version
      cv_comp_ver = VALUE #( it_prod_data_curr[ idno = iv_idno ]-version OPTIONAL ).
  ENDTRY.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  SET_REGIONAL_DATA
*&---------------------------------------------------------------------*
* Set Regional Data
*----------------------------------------------------------------------*
FORM set_regional_data  USING    it_prod_data_n TYPE ty_t_prod_data
                        CHANGING ct_reg_data_o  TYPE ztarn_reg_data
                                 ct_reg_data_n  TYPE ztarn_reg_data.

  CHECK it_prod_data_n IS NOT INITIAL.

  PERFORM set_regional_header USING    it_prod_data_n
                              CHANGING ct_reg_data_o
                                       ct_reg_data_n.

  PERFORM set_regional_header_string USING    it_prod_data_n
                                     CHANGING ct_reg_data_o
                                              ct_reg_data_n.

  PERFORM set_reg_uom_descriptions USING    it_prod_data_n
                                   CHANGING ct_reg_data_o
                                            ct_reg_data_n.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  set_regional_Header
*&---------------------------------------------------------------------*
* Set Regional Header Data
*----------------------------------------------------------------------*
FORM set_regional_header  USING    it_prod_data_n TYPE ty_t_prod_data
                          CHANGING ct_reg_data_o  TYPE ztarn_reg_data
                                   ct_reg_data_n  TYPE ztarn_reg_data.

  DATA: lt_field_compare TYPE tt_field_compare.

* Un-reviewed national version is the version which is having the national header
* status as CHG/NEW. Un-reviewed regional data is having regional header status
* as CHG/NEW.
*
* Un-Posted national version is the version which is having the national header
* status as APR/WIP. Un-Posted regional data is having the regional status
* as CHG/WIP/APR.

* If a new version is introduced, all the previous Un-Reviewed and Un-Posted
* national versions will get the status changed as OBS-Obsolete.
* OBS national version could not be referred/selected to edit the regional data.
*
* The existing un-posted regional data will get the regional header status changed
* to WIP if is in CHG, else no change in the status.

  CHECK it_prod_data_n IS NOT INITIAL.

  SELECT *                                    "#EC CI_ALL_FIELDS_NEEDED
    FROM zarn_reg_hdr
    INTO TABLE @DATA(lt_reg_hdr)
    FOR ALL ENTRIES IN @it_prod_data_n
      WHERE idno   EQ @it_prod_data_n-idno.
  IF sy-subrc <> 0.
    RETURN.
  ENDIF.

  " Fill fields to compare
  lt_field_compare = VALUE #( ( national_field = 'FS_BRAND_DESC'    regional_field = 'BRAND_OVERRIDE' )
                              ( national_field = 'FS_SHORT_DESCR' regional_field = 'FS_CUST_SHORT_DESCR' )
                            ).

  LOOP AT lt_reg_hdr INTO DATA(ls_reg_hdr_o).

    DATA(ls_reg_hdr_n) = ls_reg_hdr_o.

    " Status Change Required?
    IF ls_reg_hdr_o-status = zif_arn_regional_data_status_c=>gc_change.
      ls_reg_hdr_n-status = zif_arn_regional_data_status_c=>gc_work_in_progress.
    ENDIF.

    " Clear regional property values if certain properties are same as National
    PERFORM set_regional_table_data USING    zif_arn_table_name_c=>gc_table_name-national-product_master
                                             zif_arn_table_name_c=>gc_table_name-regional-header
                                             ls_reg_hdr_o
                                             it_prod_data_n
                                             lt_field_compare
                                    CHANGING ls_reg_hdr_n
                                             ct_reg_data_o
                                             ct_reg_data_n.

  ENDLOOP.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  set_regional_Header_String
*&---------------------------------------------------------------------*
* Set Regional Header Strings
*----------------------------------------------------------------------*
FORM set_regional_header_string  USING    it_prod_data_n TYPE ty_t_prod_data
                                 CHANGING ct_reg_data_o  TYPE ztarn_reg_data
                                          ct_reg_data_n  TYPE ztarn_reg_data.

  DATA: lt_field_compare TYPE tt_field_compare,
        lt_id            TYPE RANGE OF zarn_idno.

  CHECK it_prod_data_n IS NOT INITIAL.

  lt_id = VALUE #( FOR ls_rec IN it_prod_data_n ( sign = 'I' option = 'EQ' low = ls_rec-idno ) ).
  SELECT *
    FROM zarn_reg_str
    INTO TABLE @DATA(lt_reg_hdr_str)
      WHERE idno IN @lt_id.
  IF sy-subrc <> 0.
    RETURN.
  ENDIF.

  " Fill fields to compare
  lt_field_compare = VALUE #( ( national_field = 'FS_CUST_MEDIUM_DESCR'    regional_field = 'FS_CUST_MEDIUM_DESCR' )
                              ( national_field = 'FS_CUST_LONG_DESCR'      regional_field = 'FS_CUST_LONG_DESCR' )
                            ).

  " Clear regional property values if certain properties are same as National
  LOOP AT lt_reg_hdr_str INTO DATA(ls_reg_hdr_str_o).

    DATA(ls_reg_hdr_str_n) = ls_reg_hdr_str_o.

    " Clear regional property values if certain properties are same as National
    PERFORM set_regional_table_data USING    zif_arn_table_name_c=>gc_table_name-national-product_master_string
                                             zif_arn_table_name_c=>gc_table_name-regional-header_string
                                             ls_reg_hdr_str_o
                                             it_prod_data_n
                                             lt_field_compare
                                    CHANGING ls_reg_hdr_str_n
                                             ct_reg_data_o
                                             ct_reg_data_n.

  ENDLOOP.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  set_reg_uom_descriptions
*&---------------------------------------------------------------------*
* Set Regional UoM Descriptions
*----------------------------------------------------------------------*
FORM set_reg_uom_descriptions  USING    it_prod_data_n TYPE ty_t_prod_data
                               CHANGING ct_reg_data_o  TYPE ztarn_reg_data
                                        ct_reg_data_n  TYPE ztarn_reg_data.

  DATA: ls_reg_uom TYPE zarn_reg_uom.

  CHECK it_prod_data_n IS NOT INITIAL.

* Update UoM descriptions
  IF gv_feature_a4_enabled EQ abap_true.
    LOOP AT it_prod_data_n INTO DATA(ls_prod_data).
      READ TABLE ct_reg_data_n TRANSPORTING NO FIELDS WITH KEY idno = ls_prod_data-idno.
      IF sy-subrc NE 0.
        APPEND INITIAL LINE TO ct_reg_data_n ASSIGNING FIELD-SYMBOL(<ls_reg_data_n>).
        <ls_reg_data_n>-idno = ls_prod_data-idno.
      ENDIF.
      READ TABLE ct_reg_data_o TRANSPORTING NO FIELDS WITH KEY idno = ls_prod_data-idno.
      IF sy-subrc NE 0.
        APPEND INITIAL LINE TO ct_reg_data_o ASSIGNING FIELD-SYMBOL(<ls_reg_data_o>).
        <ls_reg_data_o>-idno = ls_prod_data-idno.
      ENDIF.
      READ TABLE ct_reg_data_n ASSIGNING <ls_reg_data_n> WITH KEY idno = ls_prod_data-idno.
      IF sy-subrc EQ 0.
        READ TABLE ct_reg_data_o ASSIGNING <ls_reg_data_o> WITH KEY idno = ls_prod_data-idno.
        IF sy-subrc EQ 0.
          LOOP AT ls_prod_data-zarn_uom_variant INTO DATA(ls_uom_variant).
            IF ls_uom_variant-pkging_rcycling_scheme_code    IS NOT INITIAL
            OR ls_uom_variant-pkging_rcycling_process_type_c IS NOT INITIAL
            OR ls_uom_variant-sustainability_feature_code    IS NOT INITIAL
            OR ls_uom_variant-net_content_short_desc         IS NOT INITIAL
            OR ls_uom_variant-fscust_prod_name40             IS NOT INITIAL
            OR ls_uom_variant-fscust_prod_name80             IS NOT INITIAL
            OR ls_uom_variant-fscust_prod_name255            IS NOT INITIAL.
              READ TABLE <ls_reg_data_n>-zarn_reg_uom TRANSPORTING NO FIELDS WITH KEY pim_uom_code = ls_uom_variant-uom_code num_base_units = ls_uom_variant-num_base_units.
              IF sy-subrc NE 0.
                READ TABLE <ls_reg_data_o>-zarn_reg_uom ASSIGNING FIELD-SYMBOL(<ls_reg_uom_o>) WITH KEY pim_uom_code = ls_uom_variant-uom_code num_base_units = ls_uom_variant-num_base_units.
                IF sy-subrc EQ 0.
                  INSERT <ls_reg_uom_o> INTO TABLE <ls_reg_data_n>-zarn_reg_uom.
                ELSE.
                  SELECT SINGLE *             "#EC CI_ALL_FIELDS_NEEDED
                    INTO ls_reg_uom
                    FROM zarn_reg_uom                "#EC CI_SEL_NESTED
                    WHERE idno           EQ ls_prod_data-idno
                    AND   pim_uom_code   EQ ls_uom_variant-uom_code
                    AND   num_base_units EQ ls_uom_variant-num_base_units.
                  INSERT ls_reg_uom INTO TABLE <ls_reg_data_o>-zarn_reg_uom.
                  INSERT ls_reg_uom INTO TABLE <ls_reg_data_n>-zarn_reg_uom.
                ENDIF.
              ENDIF.
              READ TABLE <ls_reg_data_n>-zarn_reg_uom ASSIGNING FIELD-SYMBOL(<ls_reg_uom>) WITH KEY pim_uom_code = ls_uom_variant-uom_code num_base_units = ls_uom_variant-num_base_units.
              IF sy-subrc EQ 0.
                <ls_reg_uom>-pkging_rcycling_scheme_code    = ls_uom_variant-pkging_rcycling_scheme_code.
                <ls_reg_uom>-pkging_rcycling_process_type_c = ls_uom_variant-pkging_rcycling_process_type_c.
                <ls_reg_uom>-sustainability_feature_code    = ls_uom_variant-sustainability_feature_code.
                <ls_reg_uom>-net_content_short_desc         = ls_uom_variant-net_content_short_desc.
                <ls_reg_uom>-fscust_prod_name40             = ls_uom_variant-fscust_prod_name40.
                <ls_reg_uom>-fscust_prod_name80             = ls_uom_variant-fscust_prod_name80.
                <ls_reg_uom>-fscust_prod_name255            = ls_uom_variant-fscust_prod_name255.
              ENDIF.
            ENDIF.
          ENDLOOP.
        ENDIF.
      ENDIF.
    ENDLOOP.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  set_regional_table_data
*&---------------------------------------------------------------------*
* Checks if regional data field is same as national data field and clears
* regional data. This is a generic method for comparision
*----------------------------------------------------------------------*
FORM set_regional_table_data USING    iv_national_table TYPE tabname16
                                      iv_regional_table TYPE tabname16
                                      is_reg_data_o     TYPE any
                                      it_prod_data_n    TYPE ty_t_prod_data
                                      it_field_compare  TYPE tt_field_compare
                             CHANGING cs_reg_data_n     TYPE any
                                      ct_reg_data_o     TYPE ztarn_reg_data
                                      ct_reg_data_n     TYPE ztarn_reg_data.

  CONSTANTS: gc_key TYPE string VALUE 'IDNO'.

  FIELD-SYMBOLS: <lt_national_table> TYPE ANY TABLE,
                 <lt_regional_table> TYPE ANY TABLE,
                 <ls_national_data>  TYPE any.

  ASSIGN COMPONENT gc_key OF STRUCTURE is_reg_data_o TO FIELD-SYMBOL(<lv_idno>).
  READ TABLE it_prod_data_n INTO DATA(ls_prod_data_n) WITH KEY idno = <lv_idno>.
  ASSIGN COMPONENT iv_national_table OF STRUCTURE ls_prod_data_n TO <lt_national_table>.
  READ TABLE <lt_national_table> ASSIGNING <ls_national_data> WITH KEY (gc_key) = <lv_idno>.

  " If regional data same as national, clear the value
  LOOP AT it_field_compare INTO DATA(ls_field_compare).
    ASSIGN COMPONENT ls_field_compare-national_field OF STRUCTURE <ls_national_data> TO FIELD-SYMBOL(<lv_national_field>).
    ASSIGN COMPONENT ls_field_compare-regional_field OF STRUCTURE is_reg_data_o      TO FIELD-SYMBOL(<lv_regional_field_o>).
    ASSIGN COMPONENT ls_field_compare-regional_field OF STRUCTURE cs_reg_data_n      TO FIELD-SYMBOL(<lv_regional_field_n>).
    IF <lv_regional_field_o> = <lv_national_field>.
      CLEAR: <lv_regional_field_n>.
    ENDIF.
  ENDLOOP.

  IF is_reg_data_o = cs_reg_data_n. " No Change?
    RETURN.
  ENDIF.

  " Old Record
  READ TABLE ct_reg_data_o ASSIGNING FIELD-SYMBOL(<ls_reg_data_o>) WITH KEY idno = <lv_idno>.
  IF sy-subrc <> 0.
    APPEND INITIAL LINE TO ct_reg_data_o ASSIGNING <ls_reg_data_o>.
    <ls_reg_data_o>-idno = <lv_idno>.
  ENDIF.
  ASSIGN COMPONENT iv_regional_table OF STRUCTURE <ls_reg_data_o> TO <lt_regional_table>.
  INSERT is_reg_data_o INTO TABLE <lt_regional_table>.

  " New Record
  READ TABLE ct_reg_data_n ASSIGNING FIELD-SYMBOL(<ls_reg_data_n>) WITH KEY idno = <lv_idno>.
  IF sy-subrc <> 0.
    APPEND INITIAL LINE TO ct_reg_data_n ASSIGNING <ls_reg_data_n>.
    <ls_reg_data_n>-idno = <lv_idno>.
  ENDIF.
  ASSIGN COMPONENT iv_regional_table OF STRUCTURE <ls_reg_data_n> TO <lt_regional_table>.
  INSERT cs_reg_data_n INTO TABLE <lt_regional_table>.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  BUILD_BENEFITS_TABLE
*&---------------------------------------------------------------------*
* Build Feature Benefits Table
*----------------------------------------------------------------------*
FORM build_benefits_table USING fu_s_master_product TYPE zmaster_products_product
                                   fu_v_idno           TYPE zarn_idno
                          CHANGING fc_t_benefits       TYPE ztarn_benefits.

  DATA: ls_benefit TYPE zarn_benefits.

  REFRESH: fc_t_benefits[].

  LOOP AT fu_s_master_product-trade_item_feature_benefits-trade_item_feature_benefit ASSIGNING FIELD-SYMBOL(<ls_benefit>).
    CLEAR ls_benefit.
    ls_benefit-mandt           = sy-mandt.
    ls_benefit-idno            = fu_v_idno.
    ls_benefit-benefit_no      = sy-tabix.
    ls_benefit-feature_benefit = <ls_benefit>.
    APPEND ls_benefit TO fc_t_benefits[].
  ENDLOOP.

ENDFORM.                    " BUILD_BENEFITS_TABLE
