FUNCTION z_arn_approvals_email_team2.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(IV_TEAM_CODE) TYPE  ZARN_TEAM_CODE
*"     REFERENCE(IV_USER) TYPE  XUBNAME
*"     REFERENCE(IT_APPROVALS) TYPE  ZARN_T_APPROVAL
*"     REFERENCE(IV_COMMIT) TYPE  ABAP_BOOL DEFAULT ABAP_FALSE
*"  EXCEPTIONS
*"      EMAIL_PROBLEM
*"      TEAM_NOT_FOUND
*"      NO_EMAIL_FOR_USER
*"----------------------------------------------------------------------
*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Feature
* DATE WRITTEN     # 07/10/16
* SAP VERSION      # 740
* TYPE             # Function module
* AUTHOR           # C90003972, Paul Collins
*-----------------------------------------------------------------------
* TITLE            # Approvals stabilisation - Email notifications
* PURPOSE          # Copied from Z_ARN_APPROVALS_EMAIL_TEAM with the
*                    following adjustments:
*                    - Use new approvals data model
*                    - Remove enrichment vs approval concept
* COPIED FROM      # Z_ARN_APPROVALS_EMAIL_TEAM
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  CONSTANTS:  lc_std_text        TYPE tdid         VALUE 'ST',
              lc_text_object     TYPE tdobject     VALUE 'TEXT',
              lc_std_txt_approve TYPE tdobname     VALUE 'ZARN_APPROVAL_EMAIL',
              lc_std_txt_enrich  TYPE tdobname     VALUE 'ZARN_ENRICHMENT_EMAIL',
              lc_raw             TYPE so_obj_tp    VALUE 'RAW',
              lc_bin             TYPE so_obj_tp    VALUE 'BIN',
              lc_xls             TYPE so_obj_tp    VALUE 'XLS'.

  DATA: lo_send_request       TYPE REF TO cl_bcs,
        lo_document           TYPE REF TO cl_document_bcs,
        lo_recipient          TYPE REF TO if_recipient_bcs,
        lv_email_subject      TYPE so_obj_des,
        lv_email_subject_long TYPE string,
        lv_xstring            TYPE xstring,
        lv_text_name          TYPE tdobname,
        lt_lines              TYPE tline_t,
        lt_body               TYPE soli_tab,
        ls_body               TYPE soli,
        lv_attach_str         TYPE string,
        lt_attach             TYPE solix_tab,
        lv_attach_size        TYPE so_obj_len,
        lv_attach_subject     TYPE sood-objdes,
        lv_sent_to_all        TYPE os_boolean,
        lo_root               TYPE REF TO cx_root,
        lv_msg                TYPE string,
        lt_arn_products       TYPE SORTED TABLE OF zarn_products WITH NON-UNIQUE KEY idno,
        lv_chg_cat_desc       TYPE zarn_chg_cat_desc,
        ls_arn_team           TYPE zarn_teams,
        lv_username           TYPE bapibname-bapibname,
        lt_return             TYPE STANDARD TABLE OF bapiret2,
        lt_addsmtp            TYPE STANDARD TABLE OF bapiadsmtp,
        ls_addsmtp            LIKE LINE OF lt_addsmtp,
        lv_datum              TYPE char10,
        lv_uzeit              TYPE char8,
        lv_datetime           TYPE char20.

  FIELD-SYMBOLS: <ls_line>        LIKE LINE OF lt_lines,
                 <ls_approval>    LIKE LINE OF it_approvals,
                 <ls_arn_product> LIKE LINE OF lt_arn_products,
                 <lv_field>       TYPE any.

  TRY.
      WRITE: sy-datum TO lv_datum,
             sy-uzeit TO lv_uzeit.
      CONCATENATE lv_datum lv_uzeit(5) INTO lv_datetime SEPARATED BY space.

      SELECT SINGLE *
        INTO ls_arn_team
        FROM zarn_teams
        WHERE team_code EQ iv_team_code.
      IF sy-subrc NE 0.
*   Team & not found
        MESSAGE e101 WITH iv_team_code RAISING team_not_found.
      ENDIF.


      lv_username = iv_user.
      REFRESH: lt_return,
               lt_addsmtp.
      CALL FUNCTION 'BAPI_USER_GET_DETAIL'
        EXPORTING
          username = lv_username
        TABLES
          return   = lt_return
          addsmtp  = lt_addsmtp.
      DELETE lt_addsmtp WHERE e_mail IS INITIAL.
      IF lt_addsmtp[] IS INITIAL.
*   No email found for user &1
        MESSAGE e102 WITH lv_username RAISING no_email_for_user..
      ENDIF.

      IF NOT it_approvals[] IS INITIAL.
        SELECT *
          INTO CORRESPONDING FIELDS OF TABLE lt_arn_products
          FROM zarn_products
          INNER JOIN zarn_ver_status
            ON zarn_ver_status~idno EQ zarn_products~idno
          FOR ALL ENTRIES IN it_approvals
          WHERE zarn_products~idno    EQ it_approvals-idno
          AND   zarn_products~version EQ zarn_ver_status~current_ver.
      ENDIF.

      FREE lo_send_request.
      lo_send_request = cl_bcs=>create_persistent( ).

* Default to approve text as we're not interested in enrichment at the moment
      lv_text_name = lc_std_txt_approve.

      REFRESH lt_lines.
      CALL FUNCTION 'READ_TEXT'
        EXPORTING
          id                      = lc_std_text "Standard Text
          language                = sy-langu    "System Language
          name                    = lv_text_name
          object                  = lc_text_object
        TABLES
          lines                   = lt_lines
        EXCEPTIONS
          id                      = 1
          language                = 2
          name                    = 3
          not_found               = 4
          object                  = 5
          reference_check         = 6
          wrong_access_to_archive = 7
          OTHERS                  = 8.

*     Get the subject from the first line of the text
      CLEAR lv_email_subject.
      READ TABLE lt_lines ASSIGNING <ls_line> INDEX 1.
      IF sy-subrc EQ 0.
        PERFORM email_substitute USING lv_datetime ls_arn_team CHANGING <ls_line>-tdline.
        lv_email_subject = <ls_line>-tdline.
        lv_email_subject_long = <ls_line>-tdline.
        DELETE lt_lines INDEX 1.
      ENDIF.

      IF lv_email_subject IS INITIAL.
        lv_email_subject = 'An article in SAP AReNa is ready for your review and approval'.
      ENDIF.

*     Build the email body from the text
      IF NOT lt_lines[] IS INITIAL.
        LOOP AT lt_lines ASSIGNING <ls_line>.
          PERFORM email_substitute USING lv_datetime ls_arn_team CHANGING <ls_line>-tdline.
          PERFORM email_add_line USING <ls_line>-tdline CHANGING lt_body.
        ENDLOOP.
      ENDIF.

*     Add a footer to indicate where/when/who
      CLEAR ls_body.
      APPEND ls_body TO lt_body.
      APPEND ls_body TO lt_body.
      CONCATENATE
        'Sent from'
        sy-sysid
        sy-mandt
        'on'
        lv_datum
        'at'
        lv_uzeit
        'by'
        sy-uname
        INTO ls_body-line SEPARATED BY space.
      PERFORM email_add_line USING ls_body-line CHANGING lt_body.

*     Build the attachment header line
      CLEAR lv_attach_str.
      CONCATENATE
        'AReNa ID'
        'FAN ID'
        'Article'
        'Article Description'
        'Change Area'
        'Change Category'
        INTO lv_attach_str SEPARATED BY cl_abap_char_utilities=>horizontal_tab.

*     Build the attachment details
      LOOP AT it_approvals ASSIGNING <ls_approval>.
        READ TABLE lt_arn_products ASSIGNING <ls_arn_product> WITH TABLE KEY idno = <ls_approval>-idno.
        CHECK sy-subrc EQ 0.

        CALL FUNCTION 'Z_ARN_APPROVALS_GET_CHGCAT_DES'
          EXPORTING
            chg_category = <ls_approval>-chg_category
          IMPORTING
            chg_cat_desc = lv_chg_cat_desc.
        CONCATENATE
          lv_attach_str
          cl_abap_char_utilities=>cr_lf
          <ls_approval>-idno
          cl_abap_char_utilities=>horizontal_tab
          <ls_arn_product>-fan_id
          cl_abap_char_utilities=>horizontal_tab
          <ls_arn_product>-matnr_ni
          cl_abap_char_utilities=>horizontal_tab
          '"'
          <ls_arn_product>-description
          '"'
          cl_abap_char_utilities=>horizontal_tab
          <ls_approval>-approval_area
          cl_abap_char_utilities=>horizontal_tab
          '"'
          <ls_approval>-chg_category
          '-'
          lv_chg_cat_desc
          '"'
          INTO lv_attach_str.
      ENDLOOP.

      lo_document = cl_document_bcs=>create_document(
                      i_type    = lc_raw
                      i_text    = lt_body
                      i_subject = lv_email_subject ).
      IF NOT lo_document IS INITIAL.
        REFRESH lt_attach.
        CALL METHOD cl_bcs_convert=>string_to_solix
          EXPORTING
            iv_string = lv_attach_str
          IMPORTING
            et_solix  = lt_attach
            ev_size   = lv_attach_size.

        CONCATENATE
          'articles_'
          iv_team_code
          '_'
          sy-datum
          '.xls'
          INTO lv_attach_subject.

        CALL METHOD lo_document->add_attachment
          EXPORTING
            i_attachment_type    = lc_xls
            i_attachment_subject = lv_attach_subject
            i_attachment_size    = lv_attach_size
            i_att_content_hex    = lt_attach.

        CALL METHOD lo_send_request->set_document( lo_document ).

        CALL METHOD lo_send_request->set_message_subject
          EXPORTING
            ip_subject = lv_email_subject_long.

        FREE lo_recipient.
        READ TABLE lt_addsmtp INTO ls_addsmtp INDEX 1.
        IF sy-subrc EQ 0.
          lo_recipient ?= cl_cam_address_bcs=>create_internet_address( ls_addsmtp-e_mail ).
          IF NOT lo_recipient IS INITIAL.
            "Add recipient to send request
            CALL METHOD lo_send_request->add_recipient
              EXPORTING
                i_recipient = lo_recipient.
          ENDIF.
        ENDIF.
*     lo_recipient ?= cl_sapuser_bcs=>create( iv_USER ).

        lo_send_request->set_send_immediately( 'X' ).

        CALL METHOD lo_send_request->send(
          RECEIVING
            result = lv_sent_to_all ).

        "Commit to send email
        IF iv_commit = abap_true.
          COMMIT WORK.
        ENDIF.
      ENDIF.

    CATCH cx_root INTO lo_root.
      CALL METHOD lo_root->if_message~get_text
        RECEIVING
          result = lv_msg.
*   Error sending email: &1
      MESSAGE e103 WITH lv_msg RAISING email_problem.
  ENDTRY.

ENDFUNCTION.
