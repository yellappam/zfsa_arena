*&---------------------------------------------------------------------*
*& Report  ZRARN_DATA_MIGRATION
*&
*&---------------------------------------------------------------------*
*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # PIM CR092 Schema Change CHARM 9000004438
* DATE WRITTEN     # 26.10.2018
* SAP VERSION      # 731
* TYPE             # Report Program
* AUTHOR           # C90012814, Brad Gorlicki
*-----------------------------------------------------------------------
* TITLE            # AReNa: Date Migration/Upload Tool
* PURPOSE          # Upload tool to support data migration for following fields:
*                  # - Net Quantity and Net Quantity UOM
*	                 # - Serving Size and Serving Size UOM
*	                 # - Biorganism Max Value and Max Value UOM
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

REPORT zrarn_data_migration.

INCLUDE ziarn_data_migration_top.                     " global Data
INCLUDE ziarn_data_migration_sel.                     " selection screen
INCLUDE ziarn_data_migration_f01.                     " FORM-Routines

START-OF-SELECTION.
  PERFORM get_old_data.
  PERFORM build_update_data.
  perform update_db.
  perform write_log.
