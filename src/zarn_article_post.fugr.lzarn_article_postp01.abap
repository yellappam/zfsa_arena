*&---------------------------------------------------------------------*
*&  Include           LZARN_ARTICLE_POSTP01
*&---------------------------------------------------------------------*
CLASS lcl_article_post_helper IMPLEMENTATION.

  METHOD get_instance.

    IF so_singleton IS NOT BOUND.
      so_singleton = NEW #( ).
    ENDIF.
    ro_instance = so_singleton.

  ENDMETHOD.

  METHOD add_message.

    DATA: ls_message       TYPE bal_s_msg,
          ls_log_params    TYPE zsarn_slg1_log_params_art_post,
          lv_context_value TYPE string.

    IF is_bapiret2 IS INITIAL.
      ls_message-msgty  = iv_msgty.
      ls_message-msgid  = iv_msgid.
      ls_message-msgno  = iv_msgno.
      ls_message-msgv1  = iv_msgv1.
      ls_message-msgv2  = iv_msgv2.
      ls_message-msgv3  = iv_msgv3.
      ls_message-msgv4  = iv_msgv4.
    ELSE.
      ls_message-msgty  = is_bapiret2-type.
      ls_message-msgid  = is_bapiret2-id.
      ls_message-msgno  = is_bapiret2-number.
      ls_message-msgv1  = is_bapiret2-message_v1.
      ls_message-msgv2  = is_bapiret2-message_v2.
      ls_message-msgv3  = is_bapiret2-message_v3.
      ls_message-msgv4  = is_bapiret2-message_v4.
    ENDIF.

    ls_log_params-idno        = is_idno_data-idno.
    ls_log_params-fanid       = is_idno_data-fan_id.
    ls_log_params-version     = is_idno_data-current_ver.
    ls_log_params-nat_status  = is_idno_data-nat_status.
    ls_log_params-reg_status  = is_idno_data-reg_status.
    ls_log_params-saparticle  = is_idno_data-sap_article.
    ls_log_params-batch_mode  = iv_batch.

    lv_context_value           = ls_log_params.
    ls_message-context-tabname = 'ZSARN_SLG1_LOG_PARAMS_ART_POST'.
    ls_message-context-value   = lv_context_value.

    APPEND ls_message TO ct_bal_msg.

  ENDMETHOD.

  METHOD post_layout_module.

    DATA: lt_message TYPE bapiret2_t.

    DATA(lo_helper) = zcl_arena_helper=>get_instance( ).
    lo_helper->post_layout_module( EXPORTING iv_article     = is_idno_data-sap_article
                                             it_reg_cluster = it_reg_cluster
                                   IMPORTING et_message     = lt_message ).

    " Note: AReNa cluster tables will be updated irrespective of assignment failure/success. Hence, we need to
    " save assignments which were successful even though we have few failures. In the next run, it will process
    " failed assignments
    CALL FUNCTION 'BAPI_TRANSACTION_COMMIT'.

    " Add messages to log
    LOOP AT lt_message INTO DATA(ls_message).
      me->add_message( EXPORTING is_idno_data = is_idno_data
                                 is_bapiret2  = ls_message
                                 iv_batch     = iv_batch
                       CHANGING  ct_bal_msg   = ct_bal_msg ).
    ENDLOOP.

  ENDMETHOD.

ENDCLASS.
