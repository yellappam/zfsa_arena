*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_CHANGE_CATEGORYTOP.          " Global Data
  INCLUDE LZARN_CHANGE_CATEGORYUXX.          " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_CHANGE_CATEGORYF...          " Subroutines
* INCLUDE LZARN_CHANGE_CATEGORYO...          " PBO-Modules
* INCLUDE LZARN_CHANGE_CATEGORYI...          " PAI-Modules
* INCLUDE LZARN_CHANGE_CATEGORYE...          " Events
* INCLUDE LZARN_CHANGE_CATEGORYP...          " Local class implement.
* INCLUDE LZARN_CHANGE_CATEGORYT99.          " ABAP Unit tests

INCLUDE lzarn_change_categoryf02.
