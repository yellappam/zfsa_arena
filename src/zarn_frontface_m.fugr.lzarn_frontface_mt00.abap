*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 24.08.2018 at 13:21:17
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_FRONTFACE..................................*
DATA:  BEGIN OF STATUS_ZARN_FRONTFACE                .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_FRONTFACE                .
CONTROLS: TCTRL_ZARN_FRONTFACE
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_FRONTFACE                .
TABLES: *ZARN_FRONTFACE_T              .
TABLES: ZARN_FRONTFACE                 .
TABLES: ZARN_FRONTFACE_T               .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
