*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 23.02.2016 at 11:39:19 by user C90001448
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_REJ_REASON.................................*
DATA:  BEGIN OF STATUS_ZARN_REJ_REASON               .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_REJ_REASON               .
CONTROLS: TCTRL_ZARN_REJ_REASON
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_REJ_REASON               .
TABLES: ZARN_REJ_REASON                .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
