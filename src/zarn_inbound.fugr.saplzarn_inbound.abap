*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_INBOUNDTOP.                  " Global Data
  INCLUDE LZARN_INBOUNDUXX.                  " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_INBOUNDF...                  " Subroutines
* INCLUDE LZARN_INBOUNDO...                  " PBO-Modules
* INCLUDE LZARN_INBOUNDI...                  " PAI-Modules
* INCLUDE LZARN_INBOUNDE...                  " Events
* INCLUDE LZARN_INBOUNDP...                  " Local class implement.
* INCLUDE LZARN_INBOUNDT99.                  " ABAP Unit tests

INCLUDE LZARN_INBOUNDF01.

INCLUDE lzarn_inboundf02.
