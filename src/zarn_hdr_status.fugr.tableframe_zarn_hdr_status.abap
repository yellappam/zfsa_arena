*---------------------------------------------------------------------*
*    program for:   TABLEFRAME_ZARN_HDR_STATUS
*   generation date: 23.02.2016 at 08:21:25 by user C90001448
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
FUNCTION TABLEFRAME_ZARN_HDR_STATUS    .

  PERFORM TABLEFRAME TABLES X_HEADER X_NAMTAB DBA_SELLIST DPL_SELLIST
                            EXCL_CUA_FUNCT
                     USING  CORR_NUMBER VIEW_ACTION VIEW_NAME.

ENDFUNCTION.
