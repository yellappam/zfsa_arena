*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Feature xxxx
* DATE WRITTEN     # 04-10-16
* SAP VERSION      # 740
* TYPE             # Report Program
* AUTHOR           # C90003972, Paul Collins
*-----------------------------------------------------------------------
* TITLE            # Arena Approval Notifications
* PURPOSE          # New notification program to replace ZRARN_APPROVALS_NOTIFY
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 08.01.2018
* CHANGE No.       # Charm 9000003127
* DESCRIPTION      # 1. Added date parameter to restrict amount of data
*                  # 2. Performance improvement
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------

INCLUDE ziarn_approvals_notify2top.

INCLUDE ziarn_approvals_notify2sel.

INCLUDE ziarn_approvals_notify2ci1.

INCLUDE ziarn_approvals_notify2main.
