* regenerated at 05.05.2016 10:20:42 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_TABLE_MASTRTOP.              " Global Data
  INCLUDE LZARN_TABLE_MASTRUXX.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_TABLE_MASTRF...              " Subroutines
* INCLUDE LZARN_TABLE_MASTRO...              " PBO-Modules
* INCLUDE LZARN_TABLE_MASTRI...              " PAI-Modules
* INCLUDE LZARN_TABLE_MASTRE...              " Events
* INCLUDE LZARN_TABLE_MASTRP...              " Local class implement.
* INCLUDE LZARN_TABLE_MASTRT99.              " ABAP Unit tests
  INCLUDE LZARN_TABLE_MASTRF00                    . " subprograms
  INCLUDE LZARN_TABLE_MASTRI00                    . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
