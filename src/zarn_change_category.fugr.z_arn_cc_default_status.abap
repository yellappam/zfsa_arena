FUNCTION z_arn_cc_default_status .
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(ARN_CC_HDR) TYPE  ZTARN_CC_HDR
*"  EXPORTING
*"     REFERENCE(ARN_CC_TEAM) TYPE  ZTARN_CC_TEAM
*"  CHANGING
*"     REFERENCE(IS_PROD_DATA_N) TYPE  ZSARN_PROD_DATA OPTIONAL
*"     REFERENCE(IS_REG_DATA_N) TYPE  ZSARN_REG_DATA OPTIONAL
*"----------------------------------------------------------------------

*>>>DELETED - Ivo Skolek, 3162 approval stabilisation clean up - code not USED anymore *<<<

*
*
*  TYPES: BEGIN OF ty_fan_id,
*           idno   TYPE zarn_products-idno,
*           fan_id TYPE zarn_products-fan_id,
*         END OF ty_fan_id.
*
*  STATICS: lt_matrix       TYPE zarn_rel_matrix_tab,
*           lt_teams_buffer TYPE STANDARD TABLE OF zarn_teams.
*
*  DATA: lv_data_level   TYPE zarn_rel_matrix-data_level,
*        lv_process_type TYPE zarn_rel_matrix-process_type,
*        ls_arn_cc_team  LIKE LINE OF arn_cc_team[].
*
*  FIELD-SYMBOLS: <ls_matrix>      LIKE LINE OF lt_matrix,
*                 <ls_team_buffer> LIKE LINE OF lt_teams_buffer,
*                 <ls_arn_cc_hdr>  LIKE LINE OF arn_cc_hdr[].
*
*  IF lt_teams_buffer[] IS INITIAL.
*    SELECT *
*      INTO TABLE lt_teams_buffer
*      FROM zarn_teams
*      WHERE display_order GE 1.
*  ENDIF.
*
*  REFRESH arn_cc_team.
*  LOOP AT arn_cc_hdr ASSIGNING <ls_arn_cc_hdr>.
*    CLEAR: lv_data_level,
*           lv_process_type.
*
*    IF <ls_arn_cc_hdr>-chg_area EQ 'N'.
*      lv_data_level = co_appr_called_from_nat.
*    ELSEIF <ls_arn_cc_hdr>-chg_area EQ 'R'.
*      lv_data_level = co_appr_called_from_reg.
*    ENDIF.
*
*    CALL FUNCTION 'Z_ARN_APPROVALS_GET_MATRIX'
*      EXPORTING
*        idno           = <ls_arn_cc_hdr>-idno
*        version        = <ls_arn_cc_hdr>-national_ver_id
*      TABLES
*        release_matrix = lt_matrix.
*
*    PERFORM get_process_type USING <ls_arn_cc_hdr>-idno CHANGING lv_process_type.
*
*    LOOP AT lt_teams_buffer ASSIGNING <ls_team_buffer>.
*      READ TABLE lt_matrix TRANSPORTING NO FIELDS
*        WITH TABLE KEY data_level      = lv_data_level
*                       process_type    = lv_process_type
*                       change_category = <ls_arn_cc_hdr>-chg_category
*                       release_team    = <ls_team_buffer>-team_code.
*      IF sy-subrc EQ 0.
**       We have something in the matrix for this change category and team
*        CLEAR ls_arn_cc_team.
*        ls_arn_cc_team-mandt        = sy-mandt.
*        ls_arn_cc_team-chgid        = <ls_arn_cc_hdr>-chgid.
*        ls_arn_cc_team-idno         = <ls_arn_cc_hdr>-idno.
*        ls_arn_cc_team-chg_category = <ls_arn_cc_hdr>-chg_category.
*        ls_arn_cc_team-chg_area     = <ls_arn_cc_hdr>-chg_area.
*        ls_arn_cc_team-team_code    = <ls_team_buffer>-team_code.
*        ls_arn_cc_team-status       = zif_arn_approval_status=>gc_arn_appr_cc_status_pending.
*        ls_arn_cc_team-upd_by       = sy-uname.
*        CONCATENATE sy-datum sy-uzeit INTO ls_arn_cc_team-upd_on.
*        APPEND ls_arn_cc_team TO arn_cc_team.
*      ELSE.
**       Nothing in the matrix so not relevant
*        CLEAR ls_arn_cc_team.
*        ls_arn_cc_team-mandt        = sy-mandt.
*        ls_arn_cc_team-chgid        = <ls_arn_cc_hdr>-chgid.
*        ls_arn_cc_team-idno         = <ls_arn_cc_hdr>-idno.
*        ls_arn_cc_team-chg_category = <ls_arn_cc_hdr>-chg_category.
*        ls_arn_cc_team-chg_area     = <ls_arn_cc_hdr>-chg_area.
*        ls_arn_cc_team-team_code    = <ls_team_buffer>-team_code.
*        ls_arn_cc_team-status       = zif_arn_approval_status=>gc_arn_appr_cc_status_notrele.
*        ls_arn_cc_team-upd_by       = sy-uname.
*        CONCATENATE sy-datum sy-uzeit INTO ls_arn_cc_team-upd_on.
*        APPEND ls_arn_cc_team TO arn_cc_team.
*      ENDIF.
*    ENDLOOP.
*  ENDLOOP.
*
*  SORT arn_cc_team BY chgid idno chg_category chg_area team_code.
*  DELETE ADJACENT DUPLICATES FROM arn_cc_team COMPARING chgid idno chg_category chg_area team_code.

ENDFUNCTION.
