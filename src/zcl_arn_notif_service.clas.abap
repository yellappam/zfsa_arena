class ZCL_ARN_NOTIF_SERVICE definition
  public
  create public .

public section.

  class-methods GET_CATMAN_USER
    importing
      !IV_ROLE_ID type ZMD_E_CATMAN
    returning
      value(RV_USER) type ZMD_E_CM_USER .
  class-methods GET_REJECTION_DLS
    returning
      value(RT_RECIPIENTS) type STRING_TABLE .
  class-methods GET_LATEST_VERSION
    importing
      !IT_APPROVALS type ZARN_T_APPROVAL
    returning
      value(RV_VERSION) type ZARN_VERSION .
  class-methods GET_ATTACH_REJECT_FILENAME
    returning
      value(RV_FILENAME) type STRING .
  class-methods GET_REG_REJECT_REASONS
    returning
      value(RT_REASONS) type ZTARN_PARAM_RANGE .
protected section.
private section.

  class-data ST_CATMAN type ZMD_T_ZMD_CATEGORY .
ENDCLASS.



CLASS ZCL_ARN_NOTIF_SERVICE IMPLEMENTATION.


  METHOD GET_ATTACH_REJECT_FILENAME.
* Return the attachment filename

    TRY.
        rv_filename = zcl_arn_param_prov=>get( zif_arn_param_const=>c_notify_reject_filename ).
      CATCH zcx_arn_param_err.
* Default one
        rv_filename = 'rejected_articles'.
    ENDTRY.

  ENDMETHOD.


  METHOD get_catman_user.
* Return the category manager user

    TRY.
        rv_user = st_catman[ role_id = iv_role_id ]-role_user.
      CATCH cx_sy_itab_line_not_found.
        SELECT SINGLE *
          FROM zmd_category
          INTO @DATA(ls_category)
          WHERE role_id = @iv_role_id.
        IF sy-subrc = 0.
          rv_user = ls_category-role_user.
          INSERT ls_category INTO TABLE st_catman.
        ENDIF.
    ENDTRY.

  ENDMETHOD.


  METHOD GET_LATEST_VERSION.
* Returns the version for the approval

    CHECK it_approvals IS NOT INITIAL.

    DATA(lt_approvals) = it_approvals.
    SORT lt_approvals DESCENDING BY nat_version.

    rv_version = lt_approvals[ 1 ]-nat_version.

  ENDMETHOD.


  METHOD get_reg_reject_reasons.

    TRY.
        rt_reasons  = zcl_arn_param_prov=>get_range( zif_arn_param_const=>c_notify_reject_reg_reas ).
      CATCH zcx_arn_param_err ##NO_HANDLER.
    ENDTRY.

  ENDMETHOD.


  METHOD GET_REJECTION_DLS.
* Return the rejection dl

    TRY.
        DATA(lt_recipients)  = zcl_arn_param_prov=>get_range( zif_arn_param_const=>c_notify_reject_dl ).
        LOOP AT lt_recipients INTO DATA(ls_recipient).
          APPEND ls_recipient-low TO rt_recipients.
        ENDLOOP.
      CATCH zcx_arn_param_err ##NO_HANDLER.
    ENDTRY.

  ENDMETHOD.
ENDCLASS.
