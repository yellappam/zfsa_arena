interface ZIF_ARN_ID_TYPE
  public .


  constants GC_SEARCH type ZARN_ID_TYPE value 'S' ##NO_TEXT.
  constants GC_HYBRIS type ZARN_ID_TYPE value 'H' ##NO_TEXT.
  constants GC_ICARE type ZARN_ID_TYPE value 'I' ##NO_TEXT.
  constants GC_MIGRATION type ZARN_ID_TYPE value 'M' ##NO_TEXT.
  constants GC_REQUESTED type ZARN_ID_TYPE value 'R' ##NO_TEXT.
  constants GC_COMPARISION type ZARN_ID_TYPE value 'C' ##NO_TEXT.
endinterface.
