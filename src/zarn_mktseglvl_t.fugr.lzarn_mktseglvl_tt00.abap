*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 02.05.2016 at 15:27:34 by user C90001363
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_MKTSEGLVL_T................................*
DATA:  BEGIN OF STATUS_ZARN_MKTSEGLVL_T              .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_MKTSEGLVL_T              .
CONTROLS: TCTRL_ZARN_MKTSEGLVL_T
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_MKTSEGLVL_T              .
TABLES: ZARN_MKTSEGLVL_T               .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
