* regenerated at 05.06.2019 16:08:08
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_PIR_PUR_CNFTOP.              " Global Declarations
  INCLUDE LZARN_PIR_PUR_CNFUXX.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_PIR_PUR_CNFF...              " Subroutines
* INCLUDE LZARN_PIR_PUR_CNFO...              " PBO-Modules
* INCLUDE LZARN_PIR_PUR_CNFI...              " PAI-Modules
* INCLUDE LZARN_PIR_PUR_CNFE...              " Events
* INCLUDE LZARN_PIR_PUR_CNFP...              " Local class implement.
* INCLUDE LZARN_PIR_PUR_CNFT99.              " ABAP Unit tests
  INCLUDE LZARN_PIR_PUR_CNFF00                    . " subprograms
  INCLUDE LZARN_PIR_PUR_CNFI00                    . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
