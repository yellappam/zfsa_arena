*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_UOM_CATEGORYTOP.             " Global Data
  INCLUDE LZARN_UOM_CATEGORYUXX.             " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_UOM_CATEGORYF...             " Subroutines
* INCLUDE LZARN_UOM_CATEGORYO...             " PBO-Modules
* INCLUDE LZARN_UOM_CATEGORYI...             " PAI-Modules
* INCLUDE LZARN_UOM_CATEGORYE...             " Events
* INCLUDE LZARN_UOM_CATEGORYP...             " Local class implement.
* INCLUDE LZARN_UOM_CATEGORYT99.             " ABAP Unit tests
