*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Functional Specification 3113
* DATE WRITTEN     # 01032016
* SAP VERSION      # 731
* TYPE             # Function Module
* AUTHOR           # C90001363, Jitin Kharbanda
*-----------------------------------------------------------------------
* TITLE            # Change document create
* PURPOSE          # Maintain Change documents for Change Category Tables
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------
FUNCTION zarn_chg_cat_change_doc.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(IT_CC_HDR) TYPE  ZTARN_CC_HDR
*"     VALUE(IT_CC_HDR_OLD) TYPE  ZTARN_CC_HDR OPTIONAL
*"     VALUE(IT_CC_DET) TYPE  ZTARN_CC_DET OPTIONAL
*"     VALUE(IT_CC_DET_OLD) TYPE  ZTARN_CC_DET OPTIONAL
*"     VALUE(IV_CHGID) TYPE  ZARN_CHG_ID OPTIONAL
*"----------------------------------------------------------------------

  DATA: ls_cc_hdr   TYPE zarn_cc_hdr,
        ls_cc_det   TYPE zarn_cc_det,
        ls_cc_team  TYPE zarn_cc_team,
        lt_cdtxt    TYPE isu_cdtxt,
        lv_objectid TYPE cdhdr-objectid,

        lt_xcc_hdr  TYPE STANDARD TABLE OF yzarn_cc_hdr,
        lt_ycc_hdr  TYPE STANDARD TABLE OF yzarn_cc_hdr,
        ls_xcc_hdr  TYPE yzarn_cc_hdr,
        ls_ycc_hdr  TYPE yzarn_cc_hdr,

        lt_xcc_det  TYPE STANDARD TABLE OF yzarn_cc_det,
        lt_ycc_det  TYPE STANDARD TABLE OF yzarn_cc_det,
        ls_xcc_det  TYPE yzarn_cc_det,
        ls_ycc_det  TYPE yzarn_cc_det,

        lt_xcc_team TYPE STANDARD TABLE OF yzarn_cc_team,
        lt_ycc_team TYPE STANDARD TABLE OF yzarn_cc_team,
        ls_xcc_team TYPE yzarn_cc_team,
        ls_ycc_team TYPE yzarn_cc_team.

* Old CC HDR Table
  LOOP AT it_cc_hdr_old[] INTO ls_cc_hdr.
    CLEAR ls_ycc_hdr.
    MOVE-CORRESPONDING ls_cc_hdr TO ls_ycc_hdr.
    APPEND ls_ycc_hdr TO lt_ycc_hdr[].
  ENDLOOP.

* New CC HDR Table
  LOOP AT it_cc_hdr[] INTO ls_cc_hdr.
    CLEAR ls_xcc_hdr.
    MOVE-CORRESPONDING ls_cc_hdr TO ls_xcc_hdr.
*    ls_xcc_hdr-kz = 'I'.
    APPEND ls_xcc_hdr TO lt_xcc_hdr[].
  ENDLOOP.



* Old CC DET Table
  LOOP AT it_cc_det_old[] INTO ls_cc_det.
    CLEAR ls_ycc_det.
    MOVE-CORRESPONDING ls_cc_det TO ls_ycc_det.
    APPEND ls_ycc_det TO lt_ycc_det[].
  ENDLOOP.

* New CC DET Table
  LOOP AT it_cc_det[] INTO ls_cc_det.
    CLEAR ls_xcc_det.
    MOVE-CORRESPONDING ls_cc_det TO ls_xcc_det.
*    ls_xcc_det-kz = 'I'.
    APPEND ls_xcc_det TO lt_xcc_det[].
  ENDLOOP.


  lv_objectid = iv_chgid.
  CALL FUNCTION 'ZARN_CHG_CAT_WRITE_DOCUMENT'
    EXPORTING
      objectid                = lv_objectid
      tcode                   = sy-tcode
      utime                   = sy-uzeit
      udate                   = sy-datum
      username                = sy-uname
*     PLANNED_CHANGE_NUMBER   = ' '
      object_change_indicator = 'I'
*     PLANNED_OR_REAL_CHANGES = ' '
*     NO_CHANGE_POINTERS      = ' '
      upd_icdtxt_zarn_chg_cat = ' '
      upd_zarn_cc_det         = 'I'
      upd_zarn_cc_hdr         = 'I'
      upd_zarn_cc_team        = 'I'
    TABLES
      icdtxt_zarn_chg_cat     = lt_cdtxt
      xzarn_cc_det            = lt_xcc_det[]
      yzarn_cc_det            = lt_ycc_det[]
      xzarn_cc_hdr            = lt_xcc_hdr[]
      yzarn_cc_hdr            = lt_ycc_hdr[].


ENDFUNCTION.
