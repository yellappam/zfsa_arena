*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 28.07.2016 at 11:36:13
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_COO_T......................................*
DATA:  BEGIN OF STATUS_ZARN_COO_T                    .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_COO_T                    .
CONTROLS: TCTRL_ZARN_COO_T
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_COO_T                    .
TABLES: ZARN_COO_T                     .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
