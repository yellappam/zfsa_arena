*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_NAT_RECONCILIATION_TOP
*&---------------------------------------------------------------------*


TABLES zarn_products.

CONSTANTS: BEGIN OF c_ts_nat_reco,
             tab1 LIKE sy-ucomm VALUE 'TAB1',
             tab2 LIKE sy-ucomm VALUE 'TAB2',
           END OF c_ts_nat_reco.


DATA: gt_prdver_alv     TYPE ztarn_nat_reco_prdver_alv,
      gt_idno_data      TYPE ztarn_idno_data,
      gt_prod_data      TYPE ztarn_prod_data,
      gt_prd_version    TYPE ztarn_prd_version,
      gt_ver_status     TYPE ztarn_ver_status,
      gt_cc_det         TYPE ztarn_cc_det,

      go_container_9001 TYPE REF TO cl_gui_custom_container,
      go_container_9002 TYPE REF TO cl_gui_custom_container,

      go_alvgrid_9001   TYPE REF TO cl_gui_alv_grid,
      go_alvgrid_9002   TYPE REF TO cl_gui_alv_grid.











CONTROLS:  ts_nat_reco TYPE TABSTRIP.
DATA:      BEGIN OF g_ts_nat_reco,
             subscreen   LIKE sy-dynnr,
             prog        LIKE sy-repid VALUE sy-repid,
             pressed_tab LIKE sy-ucomm VALUE c_ts_nat_reco-tab1,
           END OF g_ts_nat_reco.
DATA:      ok_code LIKE sy-ucomm.
