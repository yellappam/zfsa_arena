FUNCTION-POOL zarn_read.                    "MESSAGE-ID ..

* INCLUDE LZARN_READD...                     " Local class definition



TYPES:
  BEGIN OF ty_s_chg_doc,
    objectclas TYPE cdobjectcl,
    objectid   TYPE cdobjectv,
    changenr   TYPE cdchangenr,
    tabname    TYPE tabname,
    tabkey     TYPE cdtabkeylo,
    fname      TYPE fieldname,
    chngind    TYPE cdchngind,
    userid     TYPE zarn_userid,
    username   TYPE zarn_username,
    udate      TYPE cddatum,
    utime      TYPE cduzeit,
    tcode      TYPE cdtcode,
    value_new  TYPE cdfldvaln,
    value_old  TYPE cdfldvalo,
  END OF ty_s_chg_doc,
  ty_t_chg_doc TYPE HASHED TABLE OF ty_s_chg_doc
               WITH UNIQUE KEY objectclas objectid changenr
                               tabname tabkey fname chngind,

  BEGIN OF ty_s_keyguid,
    keyguid TYPE sysuuid_c,
  END OF ty_s_keyguid,
  ty_t_keyguid   TYPE STANDARD TABLE OF ty_s_keyguid,

  ty_s_cdpos_uid TYPE cdpos_uid,
  ty_t_cdpos_uid TYPE SORTED TABLE OF ty_s_cdpos_uid WITH UNIQUE KEY keyguid,


* Regional Banner Key
  BEGIN OF ty_s_reg_banner_key,
    mandt  TYPE mandt,
    idno   TYPE zarn_idno,
    banner TYPE vkorg,
  END OF ty_s_reg_banner_key,

* Product Version Key
  BEGIN OF ty_s_prd_version_key,
    mandt   TYPE mandt,
    idno    TYPE zarn_idno,
    version TYPE zarn_version,
  END OF ty_s_prd_version_key,

* Control Table
  BEGIN OF ty_s_control_key,
    mandt TYPE mandt,
    guid  TYPE zarn_guid,
  END OF ty_s_control_key,

* Regional Data UOM
  BEGIN OF ty_s_reg_uom_key,
    mandt                TYPE mandt,
    idno                 TYPE zarn_idno,
    uom_category         TYPE zarn_uom_category,
    pim_uom_code         TYPE zarn_uom_code,
    hybris_internal_code TYPE zarn_hybris_internal_code,
    lower_uom            TYPE zarn_lower_uom,
    lower_child_uom      TYPE zarn_lower_child_uom,
  END OF ty_s_reg_uom_key,

* Regional Data EAN
  BEGIN OF ty_s_reg_ean_key,
    mandt TYPE mandt,
    idno  TYPE zarn_idno,
    meinh TYPE meinh,
    ean11 TYPE ean11,
  END OF ty_s_reg_ean_key,

* Regional Data PIR
  BEGIN OF ty_s_reg_pir_key,
    mandt                TYPE mandt,
    idno                 TYPE zarn_idno,
    order_uom_pim        TYPE zarn_uom_pim,
    hybris_internal_code TYPE zarn_hybris_internal_code,
  END OF ty_s_reg_pir_key,

* Regional Data Price Family
  BEGIN OF ty_s_reg_prfam_key,
    mandt     TYPE mandt,
    idno      TYPE zarn_idno,
    prfam_uom TYPE zarn_prfam_uom,
  END OF ty_s_reg_prfam_key,

* Regional List Price Conditions
  BEGIN OF ty_s_reg_lst_prc_key,
    mandt          TYPE mandt,
    idno           TYPE zarn_idno,
    condition_type TYPE kscha,
    ekorg          TYPE ekorg,
    lifnr          TYPE elifn,
    cond_unit      TYPE zarn_cond_unit,
  END OF ty_s_reg_lst_prc_key,

* Regional Standard terms
  BEGIN OF ty_s_reg_std_ter_key,
    mandt TYPE mandt,
    idno  TYPE zarn_idno,
    vkorg TYPE vkorg,
  END OF ty_s_reg_std_ter_key,

* Regional DC Sell
  BEGIN OF ty_s_reg_dc_sell_key,
    mandt          TYPE mandt,
    idno           TYPE zarn_idno,
    condition_type TYPE kscha,
    vkorg          TYPE vkorg,
    lifnr          TYPE elifn,
    cond_unit      TYPE zarn_cond_unit,
  END OF ty_s_reg_dc_sell_key,

* Regional Recommended Ratail Price (RRP)
  BEGIN OF ty_s_reg_rrp_key,
    mandt     TYPE mandt,
    idno      TYPE zarn_idno,
    vkorg     TYPE vkorg,
    pltyp     TYPE pltyp,
    cond_unit TYPE zarn_cond_unit,
  END OF ty_s_reg_rrp_key,

* Regional Online Category
  BEGIN OF ty_s_reg_onlcat_key,              "++ONLD-835 JKH 30.05.2017
    mandt        TYPE mandt,
    idno         TYPE zarn_idno,
    bunit        TYPE zonl_bunit,
    catalog_type TYPE zonl_catalog_type,
    seqno        TYPE zarn_seqno,
  END OF ty_s_reg_onlcat_key,


* Change Category Header
  BEGIN OF ty_s_cc_hdr_key,
    mandt        TYPE mandt,
    chgid        TYPE zarn_chg_id,
    idno         TYPE zarn_idno,
    chg_category TYPE zarn_chg_category,
    chg_area     TYPE zarn_chg_area,
  END OF ty_s_cc_hdr_key,

* Change Category Detail
  BEGIN OF ty_s_cc_det_key,
    mandt          TYPE mandt,
    chgid          TYPE zarn_chg_id,
    idno           TYPE zarn_idno,
    chg_category   TYPE zarn_chg_category,
    chg_table      TYPE zarn_chg_table,
    chg_field      TYPE zarn_chg_field,
    reference_data TYPE zarn_reference_data,
  END OF ty_s_cc_det_key,

* Change Category Team
  BEGIN OF ty_s_cc_team_key,
    mandt        TYPE mandt,
    chgid        TYPE zarn_chg_id,
    idno         TYPE zarn_idno,
    chg_category TYPE zarn_chg_category,
    chg_area     TYPE zarn_chg_area,
    team_code    TYPE zarn_team_code,
  END OF ty_s_cc_team_key,




* Renge for Object Id
  BEGIN OF ty_s_objid,
    sign   TYPE rsparams-sign,
    option TYPE rsparams-option,
    low    TYPE cdobjectv,
    high   TYPE cdobjectv,
  END OF ty_s_objid,
  ty_t_objid TYPE STANDARD TABLE OF ty_s_objid.

TYPES:
  BEGIN OF ty_s_tabname,
    tabname TYPE tabname,
  END OF ty_s_tabname,

  ty_t_tabname TYPE STANDARD TABLE OF ty_s_tabname.

CONSTANTS:
  gc_nat TYPE char01   VALUE 'N', "  National Table
  gc_reg TYPE char01   VALUE 'R'. "  Regional Table
