class ZCL_ARN_READ_DB definition
  public
  final
  create public .

public section.

  class-methods READ_REGIONAL
    importing
      !IV_IDNO type ZARN_IDNO
    exporting
      !ES_HDR type ZARN_REG_HDR
      !ET_BANNER type ZTARN_REG_BANNER .
  class-methods READ_NATIONAL
    importing
      !IV_IDNO type ZARN_IDNO
      !IV_VERSION type ZARN_VERSION
    exporting
      !ES_PRODUCT type ZARN_PRODUCTS
      !ET_PIR type ZTARN_PIR
      !ES_VERSION_STATUS type ZARN_VER_STATUS
      !ET_UOM_VARIANT type ZTARN_UOM_VARIANT
      !ET_GTIN_VARIANT type ZTARN_GTIN_VAR
      !ES_PRODUCTS_EX type ZARN_PRODUCTS_EX
      !ES_PRODUCT_STR type ZARN_PROD_STR .
  class-methods READ_HDR
    importing
      !IV_IDNO type ZARN_IDNO
    returning
      value(RS_HDR) type ZARN_REG_HDR .
  class-methods READ_BANNER
    importing
      !IV_IDNO type ZARN_IDNO
    returning
      value(RT_BANNER) type ZTARN_REG_BANNER .
  class-methods READ_PRODUCT
    importing
      !IV_IDNO type ZARN_IDNO
      !IV_VERSION type ZARN_VERSION
    returning
      value(RS_PRODUCT) type ZARN_PRODUCTS .
  class-methods READ_PRODUCTS_EX
    importing
      !IV_IDNO type ZARN_IDNO
      !IV_VERSION type ZARN_VERSION
    returning
      value(RS_PRODUCTS_EX) type ZARN_PRODUCTS_EX .
  class-methods READ_PRODUCT_STR
    importing
      !IV_IDNO type ZARN_IDNO
      !IV_VERSION type ZARN_VERSION
    returning
      value(RS_PROD_STR) type ZARN_PROD_STR .
  class-methods READ_PIR
    importing
      !IV_IDNO type ZARN_IDNO
      !IV_VERSION type ZARN_VERSION
    returning
      value(RT_PIR) type ZTARN_PIR .
  class-methods READ_VERSION_STATUS
    importing
      !IV_IDNO type ZARN_IDNO
    returning
      value(RS_VERSION_STATUS) type ZARN_VER_STATUS .
  class-methods READ_UOM_VARIANT
    importing
      !IV_IDNO type ZARN_IDNO
      !IV_VERSION type ZARN_VERSION
    returning
      value(RT_UOM_VARIANT) type ZTARN_UOM_VARIANT .
  class-methods READ_GTIN_VARIANT
    importing
      !IV_IDNO type ZARN_IDNO
      !IV_VERSION type ZARN_VERSION
    returning
      value(RT_GTIN_VARIANT) type ZTARN_GTIN_VAR .
protected section.
private section.
ENDCLASS.



CLASS ZCL_ARN_READ_DB IMPLEMENTATION.


  METHOD read_banner.

    SELECT *
      FROM zarn_reg_banner
      INTO TABLE @rt_banner
      WHERE idno = @iv_idno.

  ENDMETHOD.


  METHOD read_gtin_variant.

    SELECT *
      FROM zarn_gtin_var
      INTO TABLE @rt_gtin_variant
      WHERE idno = @iv_idno AND
            version = @iv_version.

  ENDMETHOD.


  METHOD read_hdr.

    SELECT SINGLE *
      FROM zarn_reg_hdr
      INTO @rs_hdr
      WHERE idno = @iv_idno.

  ENDMETHOD.


  METHOD read_national.


    CLEAR: es_product, et_pir, es_version_status, et_uom_variant, et_gtin_variant.

* Always give the product
    es_product = read_product( iv_idno = iv_idno iv_version = iv_version ).

* Version status
    IF es_version_status IS REQUESTED.
      es_version_status = read_version_status( iv_idno ).
    ENDIF.

* PIR
    IF et_pir IS REQUESTED.
      et_pir = read_pir( iv_idno = iv_idno iv_version = iv_version ).
    ENDIF.

* UOM variant
    IF et_uom_variant IS REQUESTED.
      et_uom_variant = read_uom_variant( iv_idno = iv_idno iv_version = iv_version ).
    ENDIF.

* GTIN variant
    IF et_gtin_variant IS REQUESTED.
      et_gtin_variant = read_gtin_variant( iv_idno = iv_idno iv_version = iv_version ).
    ENDIF.

*-- >>> NL:Defect B-06151
* Product strings
    IF es_product_str IS REQUESTED.
      es_product_str = read_product_str( iv_idno = iv_idno iv_version = iv_version ).
    ENDIF.

* Product Extensions
    IF es_products_ex IS REQUESTED.
      es_products_ex = read_products_ex( iv_idno = iv_idno iv_version = iv_version ).
    ENDIF.
*-- <<< NL:Defect B-06151

  ENDMETHOD.


  METHOD read_pir.

    SELECT *
      FROM zarn_pir
      INTO TABLE @rt_pir
      WHERE idno = @iv_idno and
            version = @iv_version.

  ENDMETHOD.


  METHOD read_product.

    SELECT SINGLE *
      FROM zarn_products
      INTO @rs_product
      WHERE idno = @iv_idno AND
            version = @iv_version.

  ENDMETHOD.


  METHOD read_products_ex.
*-- >>> NL:Defect B-06151
*-- Get the product extension data

    SELECT SINGLE *
      FROM zarn_products_ex
      INTO @rs_products_ex
      WHERE idno = @iv_idno AND
            version = @iv_version.

  ENDMETHOD.


  METHOD read_product_str.
*-- >>> NL:Defect B-06151
*-- Get the product string data

    SELECT SINGLE *
      FROM zarn_prod_str
      INTO @rs_prod_str
      WHERE idno = @iv_idno AND
            version = @iv_version.

  ENDMETHOD.


  METHOD read_regional.

* Initialise
    CLEAR: es_hdr, et_banner.

* Always return header
    es_hdr = read_hdr( iv_idno ).

* Read banner
    IF et_banner IS REQUESTED.
      et_banner = read_banner( iv_idno ).
    ENDIF.



  ENDMETHOD.


  METHOD read_uom_variant.

    SELECT *
      FROM zarn_uom_variant
      INTO TABLE @rt_uom_variant
      WHERE idno = @iv_idno AND
            version = @iv_version.

  ENDMETHOD.


  METHOD read_version_status.

    SELECT SINGLE *
      FROM zarn_ver_status
      INTO @rs_version_status
      WHERE idno = @iv_idno.

  ENDMETHOD.
ENDCLASS.
