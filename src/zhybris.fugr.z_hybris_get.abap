*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Functional Specification 3122
* DATE WRITTEN     # 01 03 2016
* SAP VERSION      # 731
* TYPE             # Function
* AUTHOR           # C90001448, Greg van der Loeff
*-----------------------------------------------------------------------
* TITLE            # AReNa: Search Hybris for article
* PURPOSE          # AReNa: Search Hybris for article
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------
FUNCTION z_hybris_get.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(HYBRIS_SEARCH_INPUT) TYPE  ZHYBRIS_SEARCH_INPUT
*"     REFERENCE(FAN_IDS) TYPE  ZARN_FAN_ID_T OPTIONAL
*"     REFERENCE(CALL_ARENA_INBOUND) TYPE  FLAG DEFAULT ABAP_TRUE
*"     REFERENCE(CALL_EXTERNAL_SYSTEM) TYPE  FLAG DEFAULT ABAP_FALSE
*"     REFERENCE(ARENA_INBOUND_TYPE) TYPE  ZARN_ID_TYPE DEFAULT 'S'
*"     REFERENCE(RUN_ID) TYPE  ZARN_RUN_ID OPTIONAL
*"     REFERENCE(RUN_SEQUENCE) TYPE  ZARN_RUN_SEQ OPTIONAL
*"  EXPORTING
*"     REFERENCE(HYBRIS_RESULTS) TYPE  ZHYBRIS_RESULTS_TAB
*"     REFERENCE(MESSAGES) TYPE  BAL_T_MSG
*"  EXCEPTIONS
*"      HYBRIS_NOT_AVAILABLE
*"----------------------------------------------------------------------

  DATA: lo_hybris          TYPE REF TO zmaster_co_product_search_requ,
        lo_root            TYPE REF TO cx_root,
        lv_msg             TYPE string,
        ls_search          TYPE zmaster_product_search1,
        ls_results         TYPE zmaster_product_with_price_cre,
        ls_hybris_result   LIKE LINE OF hybris_results,
        lv_log_no          TYPE balognr,
        ls_arn_get_cleanup TYPE zarn_get_cleanup,
        lv_fan             LIKE LINE OF ls_search-product_search-fan,
        ls_message         LIKE LINE OF messages.

  FIELD-SYMBOLS: <ls_product>      LIKE LINE OF ls_results-master_product_with_price_crea-product,
                 <ls_uom_variant>  LIKE LINE OF <ls_product>-uomvariants-uomvariant,
                 <ls_purch_info>   LIKE LINE OF <ls_uom_variant>-purch_info_records-purchasing_info_record,
                 <ls_gtin_variant> LIKE LINE OF <ls_uom_variant>-gtinvariants-gtinvariant,
                 <ls_fan_id>       LIKE LINE OF fan_ids.

  TRY.

      REFRESH hybris_results.

      CLEAR ls_search.
      IF NOT fan_ids[] IS INITIAL.
        REFRESH ls_search-product_search-fan.
        LOOP AT fan_ids ASSIGNING <ls_fan_id>.
          lv_fan = <ls_fan_id>-fan_id.
          APPEND lv_fan TO ls_search-product_search-fan.
        ENDLOOP.
      ELSE.
        ls_search-product_search-gtin                  = hybris_search_input-gtin.
        ls_search-product_search-gln_description       = hybris_search_input-gln_description.
        ls_search-product_search-gln_code              = hybris_search_input-gln.
        ls_search-product_search-fsshort_description   = hybris_search_input-short_description.
        ls_search-product_search-fsbrand               = hybris_search_input-fs_brand_desc.
        ls_search-product_search-brand                 = hybris_search_input-brand_name.
        ls_search-product_search-vendor_article_number = hybris_search_input-vendor_article_number.
      ENDIF.

      CREATE OBJECT lo_hybris.
      IF lo_hybris IS BOUND.
        PERFORM set_before_hybris_call USING run_id run_sequence.
        CALL METHOD lo_hybris->product_search_request_out
          EXPORTING
            output = ls_search
          IMPORTING
            input  = ls_results.
        LOOP AT ls_results-master_product_with_price_crea-product ASSIGNING <ls_product>.
          CLEAR ls_hybris_result.
          ls_hybris_result-fan_id          = <ls_product>-fan.
          ls_hybris_result-matnr_ni        = <ls_product>-article_number_ni.
          ls_hybris_result-description     = <ls_product>-description.
          ls_hybris_result-gpc_code        = <ls_product>-gpc-code.
          ls_hybris_result-gpc_description = <ls_product>-gpc-description.
          ls_hybris_result-code            = <ls_product>-code.
          LOOP AT <ls_product>-uomvariants-uomvariant ASSIGNING <ls_uom_variant>.
            ls_hybris_result-prodtyp = <ls_uom_variant>-uom.
            LOOP AT <ls_uom_variant>-purch_info_records-purchasing_info_record ASSIGNING <ls_purch_info>.
              ls_hybris_result-gln       = <ls_purch_info>-gln_pir-gln.
              ls_hybris_result-gln_descr = <ls_purch_info>-gln_pir-description.
            ENDLOOP.
            LOOP AT <ls_uom_variant>-gtinvariants-gtinvariant ASSIGNING <ls_gtin_variant>.
              ls_hybris_result-gtin_code = <ls_gtin_variant>-code.
            ENDLOOP.
          ENDLOOP.

          APPEND ls_hybris_result TO hybris_results.

          IF arena_inbound_type EQ 'S'. "Search
            CLEAR ls_arn_get_cleanup.
            ls_arn_get_cleanup-idno = <ls_product>-code.
            CALL FUNCTION 'CONVERSION_EXIT_MATN1_INPUT'
              EXPORTING
                input        = ls_arn_get_cleanup-idno
              IMPORTING
                output       = ls_arn_get_cleanup-idno
              EXCEPTIONS
                length_error = 1.
            ls_arn_get_cleanup-uname = sy-uname.
            ls_arn_get_cleanup-datum = sy-datum.
            ls_arn_get_cleanup-uzeit = sy-uzeit.
            MODIFY zarn_get_cleanup FROM ls_arn_get_cleanup.
          ENDIF.
        ENDLOOP.

*       Push the results into AReNa
        PERFORM save_results_to_table USING run_id run_sequence fan_ids ls_results.
        IF call_arena_inbound EQ abap_true.
          PERFORM set_before_arena_call USING run_id run_sequence.
          CALL FUNCTION 'ZARN_INBOUND_DATA_PROCESSING'
            EXPORTING
              it_product     = ls_results-master_product_with_price_crea-product
              is_admin       = ls_results-master_product_with_price_crea-admin
              iv_id_type     = arena_inbound_type
            IMPORTING
*             ET_RETURN      = ET_RETURN
*             ET_PRODUCT_ERROR = ET_PRODUCT_ERROR
              ev_slg1_log_no = lv_log_no.
          PERFORM set_after_arena_call USING run_id run_sequence.
        ENDIF.

        IF call_external_system EQ abap_true.
          PERFORM call_external_system USING ls_results.
        ENDIF.
      ENDIF.

      COMMIT WORK AND WAIT.

    CATCH cx_root INTO lo_root.
      lv_msg = lo_root->if_message~get_text( ).
      CLEAR ls_message.
      ls_message-msgid = 'ZARENA_MSG'.
      ls_message-msgty = 'E'.
      ls_message-msgno = '000'.
      PERFORM split_msg USING lv_msg CHANGING ls_message-msgv1 ls_message-msgv2 ls_message-msgv3 ls_message-msgv4.
      APPEND ls_message TO messages.
      PERFORM log_message USING lv_msg.
      WHILE NOT lo_root->previous IS INITIAL.
        lo_root = lo_root->previous.
        lv_msg = lo_root->if_message~get_text( ).
        CLEAR ls_message.
        ls_message-msgid = 'ZARENA_MSG'.
        ls_message-msgty = 'E'.
        ls_message-msgno = '000'.
        PERFORM split_msg USING lv_msg CHANGING ls_message-msgv1 ls_message-msgv2 ls_message-msgv3 ls_message-msgv4.
        APPEND ls_message TO messages.
        PERFORM log_message USING lv_msg.
      ENDWHILE.
      RAISE hybris_not_available.
  ENDTRY.

ENDFUNCTION.
