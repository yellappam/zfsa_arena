* regenerated at 25.05.2016 11:50:38 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_HANDLINST_TTOP.              " Global Data
  INCLUDE LZARN_HANDLINST_TUXX.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_HANDLINST_TF...              " Subroutines
* INCLUDE LZARN_HANDLINST_TO...              " PBO-Modules
* INCLUDE LZARN_HANDLINST_TI...              " PAI-Modules
* INCLUDE LZARN_HANDLINST_TE...              " Events
* INCLUDE LZARN_HANDLINST_TP...              " Local class implement.
* INCLUDE LZARN_HANDLINST_TT99.              " ABAP Unit tests
  INCLUDE LZARN_HANDLINST_TF00                    . " subprograms
  INCLUDE LZARN_HANDLINST_TI00                    . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
