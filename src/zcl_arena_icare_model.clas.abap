class ZCL_ARENA_ICARE_MODEL definition
  public
  final
  create private .

public section.

  class-methods GET_INSTANCE
    importing
      !IV_ID type ZARN_IDNO
      !IV_CREATION_TIME_R1 type ZARN_CREATION_TIME_R1
      !IV_SHORT_DESC type ZARN_FS_SHORT_DESCR_UPPER optional
      !IV_BRAND_DESC type ZARN_FS_BRAND_DESC_UPPER optional
      !IV_CATALOG_VERSION type ZARN_E_CATALOG_VERSION
      !IV_GTIN type ZARN_GTIN_CODE
    returning
      value(RO_INSTANCE) type ref to ZCL_ARENA_ICARE_MODEL
    raising
      ZCX_ARENA_ICARE_MODEL .
  methods SAVE
    raising
      ZCX_ARENA_ICARE_MODEL .
  methods SET_REQUESTED_BY
    importing
      !IV_REQUESTED_BY type ZARN_REQUESTED_BY
    returning
      value(RO_INSTANCE) type ref to ZCL_ARENA_ICARE_MODEL
    raising
      ZCX_ARENA_ICARE_MODEL .
  methods SET_REQUESTED_BY_DATE
    importing
      !IV_DATE type ZARN_REQUESTED_BY_DATE
    returning
      value(RO_INSTANCE) type ref to ZCL_ARENA_ICARE_MODEL
    raising
      ZCX_ARENA_ICARE_MODEL .
  methods SET_REQUESTED_MC
    importing
      !IV_VALUE type ZARN_REQUESTED_MC
    returning
      value(RO_INSTANCE) type ref to ZCL_ARENA_ICARE_MODEL .
  methods SET_CATEGORY_MNGR_ID
    importing
      !IV_GIL_CATEGORY_MNGR_ID type ZARN_CATMAN optional
      !IV_RET_CATEGORY_MNGR_ID type ZARN_CATMAN optional
    returning
      value(RO_INSTANCE) type ref to ZCL_ARENA_ICARE_MODEL .
  methods SET_CATALOG_VERSION
    importing
      !IV_CATALOG_VERSION type ZARN_E_CATALOG_VERSION
    returning
      value(RO_INSTANCE) type ref to ZCL_ARENA_ICARE_MODEL
    raising
      ZCX_ARENA_ICARE_MODEL .
  methods SET_INITIATION
    importing
      !IV_INITIATION type ZARN_INITIATION
    returning
      value(RO_INSTANCE) type ref to ZCL_ARENA_ICARE_MODEL
    raising
      ZCX_ARENA_ICARE_MODEL .
  methods SET_REQUESTOR
    importing
      !IV_REQUESTOR type ZARN_REQUESTOR
    returning
      value(RO_INSTANCE) type ref to ZCL_ARENA_ICARE_MODEL
    raising
      ZCX_ARENA_ICARE_MODEL .
  methods LOCK
    importing
      !IV_MODE type ENQMODE default ZIF_ENQUEUE_MODE_C=>GC_EXCLUSIVE
      !IV_SCOPE type DDENQSCOPE default '2'
    raising
      ZCX_ARENA_ICARE_MODEL .
  methods UNLOCK
    importing
      !IV_MODE type ENQMODE default ZIF_ENQUEUE_MODE_C=>GC_EXCLUSIVE
      !IV_SCOPE type DDENQSCOPE default '3' .
protected section.
private section.

  types:
    BEGIN OF ts_data,
      id                   TYPE zarn_idno,
      catalog_version      TYPE zarn_e_catalog_version,
      short_desc           TYPE zarn_fs_short_descr_upper,
      brand_desc           TYPE zarn_fs_brand_desc_upper,
      creation_time_r1     TYPE zarn_creation_time_r1,
      gil_category_mngr_id TYPE zarn_catman,
      ret_category_mngr_id TYPE zarn_catman,
      initiation           TYPE zarn_initiation,
      requested_by         TYPE zarn_requested_by,
      requested_by_date    TYPE zarn_requested_by_date,
      requestor            TYPE zarn_requestor,
      requested_mc         TYPE zarn_requested_mc,
      gtin                 TYPE zarn_gtin_code,
    END   OF ts_data .

  data MS_DATA type TS_DATA .
  data MS_HYBRIS_STAGING_REQUEST type ZPRODUCT_STAGING_ICARE_REQUES4 .
  data MS_REG_DATA type ZSARN_REG_DATA .
  data MS_HYBRIS_ONLINE_REQUEST type ZARN_HYBRIS_SET_FIELDS .
  data MO_MESSAGE_SERVICE type ref to ZCL_MESSAGE_SERVICES .

  methods CONSTRUCTOR
    importing
      !IV_ID type ZARN_IDNO
      !IV_CREATION_TIME_R1 type ZARN_CREATION_TIME_R1
      !IV_SHORT_DESC type ZARN_FS_SHORT_DESCR_UPPER optional
      !IV_BRAND_DESC type ZARN_FS_BRAND_DESC_UPPER optional
      !IV_CATALOG_VERSION type ZARN_E_CATALOG_VERSION
      !IV_GTIN type ZARN_GTIN_CODE
    raising
      ZCX_ARENA_ICARE_MODEL .
  methods RAISE_EXCEPTION
    importing
      !IT_MESSAGES type BAPIRET2_TAB optional
    raising
      ZCX_ARENA_ICARE_MODEL .
  methods VALIDATE
    raising
      ZCX_ARENA_ICARE_MODEL .
  methods SAVE_INT
    raising
      ZCX_ARENA_ICARE_MODEL .
  methods PREPARE_REGIONAL_DATA .
  methods PREPARE_REGIONAL_HEADER .
  methods SAVE_REGIONAL_DATA .
  methods PREPARE_HYBRIS_ONLINE_REQUEST .
  methods PREPARE_HYBRIS_STAGE_REQUEST .
  methods ONLINE_ICARE_REQUEST
    raising
      ZCX_ARENA_ICARE_MODEL .
  methods STAGED_ICARE_REQUEST
    raising
      ZCX_ARENA_ICARE_MODEL .
  methods CHECK_MANDATORY_FOR_STAGED
    raising
      ZCX_ARENA_ICARE_MODEL .
  methods CHECK_MANDATORY_FOR_ONLINE
    raising
      ZCX_ARENA_ICARE_MODEL .
ENDCLASS.



CLASS ZCL_ARENA_ICARE_MODEL IMPLEMENTATION.


METHOD check_mandatory_for_online.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 12.03.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Check Mandatory Parameters for Online Product
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  DATA: lt_message TYPE bapiret2_t.

  CHECK ms_data-catalog_version = zif_arn_catalog_version_c=>gc_online.

  IF ms_data-ret_category_mngr_id IS INITIAL.
    MESSAGE e146 WITH 'Retail Category Manager'(001) INTO mo_message_service->sv_msg_dummy.
    mo_message_service->add_message_to_bapiret_tab( CHANGING ct_return = lt_message ).
  ENDIF.

  IF ms_data-initiation IS INITIAL.
    MESSAGE e146 WITH 'Initiation'(002) INTO mo_message_service->sv_msg_dummy.
    mo_message_service->add_message_to_bapiret_tab( CHANGING ct_return = lt_message ).
  ENDIF.

  IF lt_message IS NOT INITIAL.
    MESSAGE e145 WITH ms_data-id INTO mo_message_service->sv_msg_dummy.
    mo_message_service->add_message_to_bapiret_tab( CHANGING ct_return = lt_message ).
    raise_exception( it_messages = lt_message ).
  ENDIF.

ENDMETHOD.


METHOD check_mandatory_for_staged.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 12.03.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Check Mandatory Parameters for Staged Product
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  DATA: lt_message TYPE bapiret2_t.

  CHECK ms_data-catalog_version = zif_arn_catalog_version_c=>gc_staged.

  IF ms_data-ret_category_mngr_id IS INITIAL.
    MESSAGE e146 WITH 'Retail Category Manager'(001) INTO mo_message_service->sv_msg_dummy.
    mo_message_service->add_message_to_bapiret_tab( CHANGING ct_return = lt_message ).
  ENDIF.

  IF ms_data-initiation IS INITIAL.
    MESSAGE e146 WITH 'Initiation'(002) INTO mo_message_service->sv_msg_dummy.
    mo_message_service->add_message_to_bapiret_tab( CHANGING ct_return = lt_message ).
  ENDIF.

  IF ms_data-requested_by IS INITIAL.
    MESSAGE e146 WITH 'Requested By'(003) INTO mo_message_service->sv_msg_dummy.
    mo_message_service->add_message_to_bapiret_tab( CHANGING ct_return = lt_message ).
  ENDIF.

  IF ms_data-requested_by_date IS INITIAL.
    MESSAGE e146 WITH 'Requested By Date'(004) INTO mo_message_service->sv_msg_dummy.
    mo_message_service->add_message_to_bapiret_tab( CHANGING ct_return = lt_message ).
  ENDIF.

  IF ms_data-requestor IS INITIAL.
    MESSAGE e146 WITH 'Requestor'(005) INTO mo_message_service->sv_msg_dummy.
    mo_message_service->add_message_to_bapiret_tab( CHANGING ct_return = lt_message ).
  ENDIF.

  IF ms_data-requested_mc IS INITIAL.
    MESSAGE e146 WITH 'Requested MC'(006) INTO mo_message_service->sv_msg_dummy.
    mo_message_service->add_message_to_bapiret_tab( CHANGING ct_return = lt_message ).
  ENDIF.

  IF lt_message IS NOT INITIAL.
    MESSAGE e141 WITH ms_data-id INTO mo_message_service->sv_msg_dummy.
    mo_message_service->add_message_to_bapiret_tab( CHANGING ct_return = lt_message ).
    raise_exception( it_messages = lt_message ).
  ENDIF.

ENDMETHOD.


METHOD constructor.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 24.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          #
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  ms_data-id               = iv_id.
  ms_data-gtin             = iv_gtin.
  ms_data-creation_time_r1 = iv_creation_time_r1.
  ms_data-short_desc       = iv_short_desc.
  ms_data-brand_desc       = iv_brand_desc.
  me->set_catalog_version( iv_catalog_version = iv_catalog_version ).

  mo_message_service = zcl_message_services=>get_instance( ).

ENDMETHOD.


METHOD get_instance.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 24.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Returns class instance
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  ro_instance = NEW #(  iv_id               = iv_id
                        iv_gtin             = iv_gtin
                        iv_creation_time_r1 = iv_creation_time_r1
                        iv_brand_desc       = iv_brand_desc
                        iv_short_desc       = iv_short_desc
                        iv_catalog_version  = iv_catalog_version ).

ENDMETHOD.


METHOD lock.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 24.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Lock record
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  CALL FUNCTION 'ENQUEUE_EZARN_REGIONAL'
    EXPORTING
      mode_zsarn_regional_lock = iv_mode
      _scope                   = iv_scope
      idno                     = ms_data-id
    EXCEPTIONS
      foreign_lock             = 1
      system_failure           = 2
      OTHERS                   = 3.
  IF sy-subrc <> 0.
    raise_exception( ).
  ENDIF.

ENDMETHOD.


METHOD online_icare_request.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 27.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Sends proxy request for Online product and save data to DB
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  DATA: lt_request     TYPE zarn_hybris_set_fields_tab,
        lt_bal_message TYPE bal_t_msg.

  me->prepare_hybris_online_request( ).

  APPEND ms_hybris_online_request TO lt_request.
  CALL FUNCTION 'Z_HYBRIS_SET'
    EXPORTING
      icare                = lt_request
      iv_commit            = abap_false
    IMPORTING
      messages             = lt_bal_message
    EXCEPTIONS
      hybris_not_available = 1
      OTHERS               = 2 ##FM_SUBRC_OK.
  IF lt_bal_message IS NOT INITIAL.
    DATA(lt_message) = zcl_message_services=>get_instance( )->convert_bal_to_bapiret_tab( lt_bal_message ).
    raise_exception( it_messages = lt_message ).
  ENDIF.

  save_regional_data( ).

ENDMETHOD.


METHOD prepare_hybris_online_request.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 27.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Prepare Hybris Staged Request
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  ms_hybris_online_request-idno  = ms_data-id.
  ms_hybris_online_request-gtin  = ms_data-gtin.
  ms_hybris_online_request-icare = abap_true.

ENDMETHOD.


METHOD prepare_hybris_stage_request.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 27.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Prepare Hybris Staged Request
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  ASSIGN ms_hybris_staging_request-product_staging_icare_request-product_enrichment_priority_re TO FIELD-SYMBOL(<ls_pepr>).
  <ls_pepr>-product_code-base_product_code = ms_data-id.
  <ls_pepr>-national_merch_hierachy        = ms_data-requested_mc.

  <ls_pepr>-niprioritisation-requested_by      = ms_data-requested_by.
  <ls_pepr>-niprioritisation-requested_by_date = ms_data-requested_by_date.
  <ls_pepr>-niprioritisation-requestor         = ms_data-requestor.

ENDMETHOD.


METHOD prepare_regional_data.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 24.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Prepare Regional Data
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  ms_reg_data-idno = ms_data-id.
  prepare_regional_header( ).

ENDMETHOD.


METHOD prepare_regional_header.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 24.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Prepare Regional Header
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  APPEND INITIAL LINE TO ms_reg_data-zarn_reg_hdr ASSIGNING FIELD-SYMBOL(<ls_reg_hdr>).

  " Common Fields
  <ls_reg_hdr>-idno                 = ms_data-id.
  <ls_reg_hdr>-version              = 1.
  <ls_reg_hdr>-status               = zif_arn_regional_data_status_c=>gc_new.
  <ls_reg_hdr>-gil_zzcatman         = ms_data-gil_category_mngr_id.
  <ls_reg_hdr>-ret_zzcatman         = ms_data-ret_category_mngr_id.
  <ls_reg_hdr>-initiation           = ms_data-initiation.
  <ls_reg_hdr>-ersda                = sy-datum.
  <ls_reg_hdr>-erzet                = sy-uzeit.
  <ls_reg_hdr>-ernam                = sy-uname.

  " Stage Fields
  IF ms_data-catalog_version = zif_arn_catalog_version_c=>gc_staged.
    <ls_reg_hdr>-fs_short_descr_upper = to_upper( val = ms_data-short_desc ).
    <ls_reg_hdr>-fs_brand_desc_upper  = to_upper( val = ms_data-brand_desc ).
    <ls_reg_hdr>-requested_mc         = ms_data-requested_mc.
    <ls_reg_hdr>-requested_on         = |{ sy-datum }{ sy-uzeit }|.
    <ls_reg_hdr>-requested_by         = ms_data-requested_by.
    <ls_reg_hdr>-requested_by_date    = ms_data-requested_by_date.
    <ls_reg_hdr>-requestor            = ms_data-requestor.
  ENDIF.

ENDMETHOD.


METHOD raise_exception.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 24.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Raise Exception
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  DATA: lt_message TYPE bapiret2_t.

  IF it_messages IS NOT INITIAL.
    lt_message = it_messages.
  ELSE.
    lt_message = zcl_message_services=>convert_message_to_bapiret_tab( ).
  ENDIF.

  RAISE EXCEPTION TYPE zcx_arena_icare_model
    EXPORTING
      mt_bapi_return = lt_message.

ENDMETHOD.


METHOD save.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 24.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Save the changes
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  validate( ).
  prepare_regional_data( ).
  save_int( ).

ENDMETHOD.


METHOD save_int.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 24.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Sends request and save data to DB
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  IF ms_data-catalog_version = zif_arn_catalog_version_c=>gc_staged.
    me->staged_icare_request( ).
  ELSEIF ms_data-catalog_version = zif_arn_catalog_version_c=>gc_online.
    me->online_icare_request( ).
  ENDIF.

ENDMETHOD.


METHOD save_regional_data.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 24.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Saves Regional Data
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  DATA: lt_reg_data        TYPE ztarn_reg_data,
        lt_idno_noautoappr TYPE ztarn_idno_key.

  APPEND ms_reg_data TO lt_reg_data.
  lt_idno_noautoappr = VALUE #( (  idno = ms_data-id ) ).

  " Note: COMMIT WORK option is not given to the caller as proxy and DB updates can't be combined
  " in single update task. However, DB update is triggered only when proxy update is successful,
  " The chances of DB update failure is very unlikely and incase if it happens, we can process them
  " separately using SM13
  CALL FUNCTION 'ZARN_REGIONAL_DB_UPDATE'
    IN UPDATE TASK
    EXPORTING
      it_reg_data       = lt_reg_data
      it_idno_val_error = lt_idno_noautoappr[].

  COMMIT WORK AND WAIT.

ENDMETHOD.


METHOD set_catalog_version.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 26.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Set Catalog Version
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  TRY.
      zcl_ddic_services=>check_domain_value( iv_value = iv_catalog_version ).
      ms_data-catalog_version = iv_catalog_version.
    CATCH zcx_ddic_services INTO DATA(lo_x_error).
      raise_exception( it_messages = lo_x_error->mt_bapi_return ).
  ENDTRY.

  ro_instance = me.

ENDMETHOD.


METHOD set_category_mngr_id.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 24.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Sets Category Manager Role Id
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  IF iv_gil_category_mngr_id IS SUPPLIED.
    ms_data-gil_category_mngr_id = iv_gil_category_mngr_id.
  ENDIF.

  IF iv_ret_category_mngr_id IS SUPPLIED.
    ms_data-ret_category_mngr_id = iv_ret_category_mngr_id.
  ENDIF.

  ro_instance = me.

ENDMETHOD.


METHOD set_initiation.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 24.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Set initiation
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  TRY.
      zcl_ddic_services=>check_domain_value( iv_value = iv_initiation ).
      ms_data-initiation = iv_initiation.
    CATCH zcx_ddic_services INTO DATA(lo_x_error).
      raise_exception( it_messages = lo_x_error->mt_bapi_return ).
  ENDTRY.

  ro_instance = me.

ENDMETHOD.


METHOD set_requested_by.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 24.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Sets requested by date
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  ms_data-requested_by = iv_requested_by.
  ro_instance = me.

ENDMETHOD.


METHOD set_requested_by_date.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 24.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Sets requested by date
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  IF iv_date <= sy-datum.
    MESSAGE e142 INTO zcl_message_services=>sv_msg_dummy.
    raise_exception( ).
  ENDIF.

  ms_data-requested_by_date = iv_date.
  ro_instance = me.

ENDMETHOD.


METHOD set_requested_mc.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 24.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Sets requested by date
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  ms_data-requested_mc = iv_value.
  ro_instance = me.

ENDMETHOD.


METHOD set_requestor.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 24.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Set initiation
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  TRY.
      zcl_ddic_services=>check_domain_value( iv_value = iv_requestor ).
      ms_data-requestor = iv_requestor.
    CATCH zcx_ddic_services INTO DATA(lo_x_error).
      raise_exception( it_messages = lo_x_error->mt_bapi_return ).
  ENDTRY.

  ro_instance = me.

ENDMETHOD.


METHOD staged_icare_request.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 27.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Sends proxy request for Staged product and save data to DB
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  DATA: ls_response    TYPE zproduct_staging_icare_respon4,
        lt_bal_message TYPE bal_t_msg.

  me->prepare_hybris_stage_request( ).

  CALL FUNCTION 'Z_HYBRIS_SET_R3'
    EXPORTING
      request              = ms_hybris_staging_request
    IMPORTING
      messages             = lt_bal_message
      response             = ls_response
    EXCEPTIONS
      hybris_not_available = 1
      OTHERS               = 2 ##FM_SUBRC_OK.
  IF lt_bal_message IS NOT INITIAL.
    DATA(lt_message) = zcl_message_services=>get_instance( )->convert_bal_to_bapiret_tab( lt_bal_message ).
    raise_exception( it_messages = lt_message ).
  ENDIF.

  DATA(lv_id) = ls_response-product_staging_icare_response-product_info-product-code.
  IF lv_id IS NOT INITIAL.
    save_regional_data( ).
  ENDIF.

ENDMETHOD.


METHOD unlock.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 24.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Unlock record
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  CALL FUNCTION 'DEQUEUE_EZARN_REGIONAL'
    EXPORTING
      mode_zsarn_regional_lock = iv_mode
      idno                     = ms_data-id
      _scope                   = iv_scope.

ENDMETHOD.


METHOD validate.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 24.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Validate data before posting
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  IF ms_data-catalog_version IS INITIAL.
    MESSAGE e144 INTO zcl_message_services=>sv_msg_dummy.
    raise_exception( ).
  ENDIF.

  check_mandatory_for_staged( ).
  check_mandatory_for_online( ).

  SELECT SINGLE @abap_true
    FROM zarn_reg_hdr
    INTO @DATA(lv_exists)
    WHERE idno = @ms_data-id.
  IF sy-subrc = 0.
    MESSAGE e143 WITH ms_data-id INTO zcl_message_services=>sv_msg_dummy.
    raise_exception( ).
  ENDIF.

ENDMETHOD.
ENDCLASS.
