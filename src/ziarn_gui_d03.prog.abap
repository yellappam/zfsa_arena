*&---------------------------------------------------------------------*
*&  Include           ZIARN_GUI_CL03
*&---------------------------------------------------------------------*

CLASS lcl_controller DEFINITION FINAL CREATE PRIVATE.

  PUBLIC SECTION.
    CLASS-METHODS: get_instance RETURNING VALUE(ro_instance) TYPE REF TO lcl_controller.
    METHODS: set_display IMPORTING iv_display TYPE flag,
      raise_display_mode_changed.

    EVENTS: display_mode_changed EXPORTING VALUE(ev_display)       TYPE flag
                                           VALUE(ev_alv_edit_mode) TYPE int4.

  PRIVATE SECTION.
    DATA: mv_display       TYPE flag,
          mv_alv_edit_mode TYPE int4.
    METHODS: constructor.
    CLASS-DATA: so_singleton TYPE REF TO lcl_controller.

ENDCLASS.

CLASS lcl_reg_cluster_ui DEFINITION FINAL CREATE PRIVATE.

  PUBLIC SECTION.
    CLASS-METHODS: get_instance RETURNING VALUE(ro_instance) TYPE REF TO lcl_reg_cluster_ui.
    METHODS: constructor,
      align_regional_cluster IMPORTING iv_force_cluster_update TYPE flag DEFAULT ''
                                       is_reg_banner_ui        TYPE zsarn_reg_banner_ui
                             CHANGING  ct_reg_cluster_ui       TYPE ztarn_reg_cluster_ui,
      is_toggle_framework_active RETURNING VALUE(rv_value) TYPE flag,
      refresh,
      enrich_reg_cluster CHANGING  ct_reg_cluster_ui TYPE ztarn_reg_cluster_ui,
      set_alv_instance IMPORTING io_alv TYPE REF TO cl_gui_alv_grid,
      check_changed_data,
      update_regional_cluster IMPORTING iv_force_cluster_update TYPE flag DEFAULT ''
                                        it_zarn_reg_banner      TYPE ztarn_reg_banner_ui
                              CHANGING  ct_reg_cluster_ui       TYPE ztarn_reg_cluster_ui.

    METHODS:
      handle_data_changed
                  FOR EVENT data_changed OF cl_gui_alv_grid
        IMPORTING er_data_changed
                  e_ucomm
                  sender,
      handle_display_mode_changed
                  FOR EVENT display_mode_changed OF lcl_controller
        IMPORTING ev_display
                  ev_alv_edit_mode.

  PRIVATE SECTION.

    TYPES: BEGIN OF ts_cluster_range,
             cluster_range TYPE zmd_e_cluster_range,
             description   TYPE bezei40,
           END   OF ts_cluster_range,
           tt_cluster_range TYPE SORTED TABLE OF ts_cluster_range WITH UNIQUE KEY cluster_range.
    TYPES: BEGIN OF ts_laymod_control,
             sales_org TYPE vkorg,
             merch_cat TYPE matkl,
           END   OF ts_laymod_control,
           tt_laymod_control TYPE SORTED TABLE OF ts_laymod_control WITH UNIQUE KEY sales_org merch_cat.

    CLASS-DATA: so_singleton TYPE REF TO lcl_reg_cluster_ui.

    METHODS: read_cluster_ranges,
      read_layout_module_control,
      is_layout_module_allowed IMPORTING iv_sales_org      TYPE vkorg
                                         iv_merch_cat      TYPE matkl
                               RETURNING VALUE(rv_allowed) TYPE flag,
      is_cluster_change_allowed IMPORTING io_data_changed   TYPE REF TO cl_alv_changed_data_protocol
                                RETURNING VALUE(rv_allowed) TYPE flag.

    DATA: mt_cluster_range  TYPE tt_cluster_range,
          mt_laymod_control TYPE tt_laymod_control,
          mo_alv            TYPE REF TO cl_gui_alv_grid.

ENDCLASS.                    "lcl_reg_cluster_ui DEFINITION

CLASS lcl_reg_allergen_ui DEFINITION FINAL CREATE PRIVATE.

  PUBLIC SECTION.
    CLASS-METHODS: get_instance RETURNING VALUE(ro_instance) TYPE REF TO lcl_reg_allergen_ui.
    METHODS:
      handle_data_changed
                  FOR EVENT data_changed OF cl_gui_alv_grid
        IMPORTING er_data_changed
                  e_ucomm
                  sender,
      refresh.

  PRIVATE SECTION.
    CLASS-DATA: so_singleton TYPE REF TO lcl_reg_allergen_ui.

ENDCLASS.
