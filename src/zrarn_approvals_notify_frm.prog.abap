*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Functional Specification 3132
* DATE WRITTEN     # 07 03 2016
* SAP VERSION      # 731
* TYPE             # Report Program
* AUTHOR           # C90001448, Greg van der Loeff
*-----------------------------------------------------------------------
* TITLE            # AReNa: Send notifications for approvals
* PURPOSE          # AReNa: Send notifications for approvals
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------
*&---------------------------------------------------------------------*
*&  Include           ZRARN_APPROVALS_NOTIFY_FRM
*&---------------------------------------------------------------------*

*&---------------------------------------------------------------------*
*&      Form  send_notifications
*&---------------------------------------------------------------------*
FORM send_notifications.

  CONSTANTS: lc_mestyp TYPE edi_mestyp VALUE 'ZMD_ARN_CC_LOG'.

  TYPES: BEGIN OF ty_arn_ver_status,
           idno        TYPE zarn_ver_status-idno,
           article_ver TYPE zarn_ver_status-article_ver,
         END OF ty_arn_ver_status.

  DATA: lt_changes             TYPE ty_tab_changes,
        ls_change              LIKE LINE OF lt_changes,
        lt_idno                TYPE SORTED TABLE OF ty_idno WITH UNIQUE KEY idno,
        ls_idno                LIKE LINE OF lt_idno,
        lt_idno_db             TYPE SORTED TABLE OF ty_idno WITH NON-UNIQUE KEY idno,
        lt_teamusers           TYPE ty_tab_team_users,
        ls_teamuser            LIKE LINE OF lt_teamusers,
        lv_chg_area            TYPE zarn_chg_area,
        lv_data_level          TYPE zarn_data_level,
        lv_process_type        TYPE zarn_process_type,
        lt_approvals           TYPE zarn_approvals_t,
        lt_arn_cc_hdr          TYPE SORTED TABLE OF zarn_cc_hdr WITH NON-UNIQUE KEY idno chg_area,
        lt_arn_cc_hdr_all      TYPE SORTED TABLE OF zarn_cc_hdr WITH NON-UNIQUE KEY idno chg_area,
        lt_arn_cc_hdr_idno     TYPE ztarn_cc_hdr,
        lt_arn_ver_status      TYPE SORTED TABLE OF ty_arn_ver_status WITH NON-UNIQUE KEY idno,
        lt_approval_users      TYPE zarn_approvals_team_users_t,
        lt_arn_cc_det          TYPE SORTED TABLE OF zarn_cc_det WITH NON-UNIQUE KEY idno chg_area chg_category,
        lt_notif_enrich        TYPE ty_tab_notif,
        lt_arn_cc_hdr_enrich   TYPE ztarn_cc_hdr,
        lt_notif_approve       TYPE ty_tab_notif,
        lt_arn_cc_hdr_approve  TYPE ztarn_cc_hdr,
        lt_bdcp                TYPE STANDARD TABLE OF bdcp,
        lt_bdicpident          TYPE wes_t_cpident,
        ls_arn_cc_hdr          TYPE zarn_cc_hdr,
        ls_arn_cc_team         TYPE zarn_cc_team,
        lt_arn_reg_hdr         TYPE SORTED TABLE OF zarn_reg_hdr WITH NON-UNIQUE KEY idno,
        lt_arn_teams           TYPE SORTED TABLE OF zarn_teams WITH UNIQUE KEY team_code,
        lv_enrichment_notified TYPE flag.

  FIELD-SYMBOLS: <ls_change>         LIKE LINE OF lt_changes,
                 <ls_idno>           LIKE LINE OF lt_idno,
                 <ls_idno_db>        LIKE LINE OF lt_idno_db,
                 <ls_arn_cc_hdr>     LIKE LINE OF lt_arn_cc_hdr,
                 <ls_arn_cc_hdr_all> LIKE LINE OF lt_arn_cc_hdr_all,
                 <ls_approval>       LIKE LINE OF lt_approvals,
                 <ls_notif_enrich>   LIKE LINE OF lt_notif_enrich,
                 <ls_notif_approve>  LIKE LINE OF lt_notif_approve,
                 <ls_bdcp>           LIKE LINE OF lt_bdcp,
                 <ls_arn_reg_hdr>    LIKE LINE OF lt_arn_reg_hdr,
                 <ls_arn_team>       LIKE LINE OF lt_arn_teams.

* Get all change category change pointer
  CALL FUNCTION 'ENQUEUE_E_BDCPS'
    EXPORTING
      mode_bdcps     = 'E'
      mestype        = lc_mestyp
    EXCEPTIONS
      foreign_lock   = 1
      system_failure = 2.
  IF sy-subrc NE 0.
    MESSAGE e000 WITH 'Change pointers locked for' lc_mestyp.
    EXIT.
  ELSE.

    CALL FUNCTION 'CHANGE_POINTERS_READ'
      EXPORTING
        message_type                = lc_mestyp
        creation_date_high          = sy-datum
        creation_time_high          = sy-uzeit
        read_not_processed_pointers = 'X'
      TABLES
        change_pointers             = lt_bdcp.
    IF lt_bdcp[] IS INITIAL.
      MESSAGE i000 WITH 'No change pointers found'.
    ELSE.
      LOOP AT lt_bdcp ASSIGNING <ls_bdcp>.
        IF <ls_bdcp>-tabname EQ 'ZARN_CC_HDR'.
          CLEAR ls_arn_cc_hdr.
          TRY.
              ls_arn_cc_hdr = <ls_bdcp>-tabkey.
              CLEAR ls_change.
              ls_change-idno    = ls_arn_cc_hdr-idno.
              ls_change-chgid   = ls_arn_cc_hdr-chgid.
              INSERT ls_change INTO TABLE lt_changes.
              READ TABLE lt_changes ASSIGNING <ls_change> WITH TABLE KEY idno = ls_change-idno chgid = ls_change-chgid.
              IF sy-subrc EQ 0.
                APPEND <ls_bdcp>-cpident TO <ls_change>-cpidents.
              ENDIF.
              CLEAR ls_idno.
              ls_idno-idno    = ls_change-idno.
              INSERT ls_idno INTO TABLE lt_idno.
            CATCH cx_root.
          ENDTRY.
        ELSEIF <ls_bdcp>-tabname EQ 'ZARN_CC_TEAM'.
          CLEAR ls_arn_cc_team.
          TRY.
              ls_arn_cc_team = <ls_bdcp>-tabkey.
              CLEAR ls_change.
              ls_change-idno    = ls_arn_cc_team-idno.
              ls_change-chgid   = ls_arn_cc_team-chgid.
              INSERT ls_change INTO TABLE lt_changes.
              READ TABLE lt_changes ASSIGNING <ls_change> WITH TABLE KEY idno = ls_change-idno chgid = ls_change-chgid.
              IF sy-subrc EQ 0.
                APPEND <ls_bdcp>-cpident TO <ls_change>-cpidents.
              ENDIF.
              CLEAR ls_idno.
              ls_idno-idno    = ls_change-idno.
              INSERT ls_idno INTO TABLE lt_idno.
            CATCH cx_root.
          ENDTRY.
        ELSE.
*         Do not care about this change pointer so consume
          APPEND <ls_bdcp>-cpident TO lt_bdicpident.
        ENDIF.
      ENDLOOP.

      IF NOT lt_idno[] IS INITIAL.
        REFRESH lt_idno_db.
        SELECT p~idno p~version
          INTO TABLE lt_idno_db
          FROM zarn_products AS p
          INNER JOIN zarn_ver_status AS v
            ON  v~idno        EQ p~idno
            AND v~current_ver EQ p~version
          FOR ALL ENTRIES IN lt_idno
          WHERE p~idno    EQ lt_idno-idno
          AND   p~idno    IN s_idno
          AND   p~fan_id  IN s_fanid.
        LOOP AT lt_idno ASSIGNING <ls_idno>.
          READ TABLE lt_idno_db TRANSPORTING NO FIELDS WITH TABLE KEY idno = <ls_idno>-idno.
          IF sy-subrc NE 0.
            DELETE lt_changes WHERE idno EQ <ls_idno>-idno.
          ENDIF.
        ENDLOOP.
        REFRESH lt_idno.
      ENDIF.

      IF lt_changes[] IS INITIAL.
        MESSAGE i000 WITH 'No ZARN_CC* changes found'.
      ELSE.

        SELECT *
          INTO TABLE lt_arn_teams
          FROM zarn_teams
          WHERE display_order GE 1.

*       Get the changes
        SELECT *
          INTO TABLE lt_arn_cc_hdr
          FROM zarn_cc_hdr
          FOR ALL ENTRIES IN lt_changes
          WHERE chgid EQ lt_changes-chgid
          AND   idno  EQ lt_changes-idno.

*       Get everything for the products
        IF NOT lt_idno_db[] IS INITIAL.
          SELECT *
            INTO TABLE lt_arn_cc_hdr_all
            FROM zarn_cc_hdr
            FOR ALL ENTRIES IN lt_idno_db
            WHERE idno EQ lt_idno_db-idno.

*         Get field changes to see if data has been enriched
          SELECT idno chg_area chg_category
            INTO CORRESPONDING FIELDS OF TABLE lt_arn_cc_det
            FROM zarn_cc_det
            FOR ALL ENTRIES IN lt_idno_db
            WHERE idno      EQ lt_idno_db-idno
            AND   value_new NE space
            GROUP BY idno chg_area chg_category.

*         Get the products so we can work out of new or change
          SELECT idno article_ver
            INTO TABLE lt_arn_ver_status
            FROM zarn_ver_status
            FOR ALL ENTRIES IN lt_idno_db
            WHERE idno        EQ lt_idno_db-idno
            AND   article_ver GT 0.

*         Get the category managers
          SELECT *
            INTO CORRESPONDING FIELDS OF TABLE lt_arn_reg_hdr
            FROM zarn_reg_hdr
            FOR ALL ENTRIES IN lt_idno_db
            WHERE zarn_reg_hdr~idno EQ lt_idno_db-idno.

        ENDIF.  "we have entries in LT_IDNO_DB

        REFRESH: lt_notif_enrich,
                 lt_notif_approve.

        LOOP AT lt_idno_db ASSIGNING <ls_idno_db>.
          CLEAR: lv_data_level,
                 lv_process_type.
          REFRESH: lt_approvals,
                   lt_arn_cc_hdr_idno.

          READ TABLE lt_arn_reg_hdr ASSIGNING <ls_arn_reg_hdr> WITH TABLE KEY idno = <ls_idno_db>-idno.
          IF sy-subrc NE 0.
            MESSAGE s000 WITH 'No ZARN_REG_HDR entry for' <ls_idno_db>-idno.
          ELSE.
            IF  <ls_arn_reg_hdr>-gil_zzcatman IS INITIAL
            AND <ls_arn_reg_hdr>-ret_zzcatman IS INITIAL.
              MESSAGE s000 WITH 'No category managers populated for' <ls_idno_db>-idno.
              CONTINUE.
            ENDIF.

            READ TABLE lt_arn_ver_status TRANSPORTING NO FIELDS WITH TABLE KEY idno = <ls_idno_db>-idno.
            IF sy-subrc EQ 0.
              lv_process_type = 'CHG'.
            ELSE.
              lv_process_type = 'NEW'.
            ENDIF.

            lv_chg_area = 'N'.
            lv_data_level = 'NAT'.
            LOOP AT lt_arn_cc_hdr_all ASSIGNING <ls_arn_cc_hdr_all> WHERE idno EQ <ls_idno_db>-idno AND chg_area EQ lv_chg_area.
              APPEND <ls_arn_cc_hdr_all> TO lt_arn_cc_hdr_idno.
            ENDLOOP.

            CALL FUNCTION 'Z_ARN_APPROVALS_GET_LIST'
              EXPORTING
                idno            = <ls_idno_db>-idno
                version         = <ls_idno_db>-version
                data_level      = lv_data_level
                process_type    = lv_process_type
                zarn_cc_hdr_tab = lt_arn_cc_hdr_idno
                check_auths     = abap_false
              IMPORTING
                approvals       = lt_approvals.
            DELETE lt_approvals WHERE approval_status NE zif_arn_approval_status=>gc_arn_appr_status_awaitappr.
            IF lt_approvals[] IS INITIAL.
              MESSAGE s000 WITH 'No pending national approvals for' <ls_idno_db>-idno.

*             National fully approved so get any regional approvals
              lv_chg_area = 'R'.
              lv_data_level = 'REG'.
              REFRESH lt_arn_cc_hdr_idno.
              LOOP AT lt_arn_cc_hdr_all ASSIGNING <ls_arn_cc_hdr_all> WHERE idno EQ <ls_idno_db>-idno AND chg_area EQ lv_chg_area.
                APPEND <ls_arn_cc_hdr_all> TO lt_arn_cc_hdr_idno.
              ENDLOOP.
              CALL FUNCTION 'Z_ARN_APPROVALS_GET_LIST'
                EXPORTING
                  idno            = <ls_idno_db>-idno
                  version         = <ls_idno_db>-version
                  data_level      = lv_data_level
                  process_type    = lv_process_type
                  zarn_cc_hdr_tab = lt_arn_cc_hdr_idno
                  check_auths     = abap_false
                IMPORTING
                  approvals       = lt_approvals.
              DELETE lt_approvals WHERE approval_status NE zif_arn_approval_status=>gc_arn_appr_status_awaitappr.
              IF lt_approvals[] IS INITIAL.
                MESSAGE s000 WITH 'No pending regional approvals for' <ls_idno_db>-idno.
              ENDIF.
            ENDIF.
            IF NOT lt_approvals[] IS INITIAL.
*             Send notification for the article
              LOOP AT lt_approvals ASSIGNING <ls_approval>.
                lv_enrichment_notified = abap_false.
*               Is there an enriching team ?
                IF NOT <ls_approval>-enriching_team IS INITIAL.
*                 Has there been some enrichment ?
                  READ TABLE lt_arn_cc_det TRANSPORTING NO FIELDS WITH TABLE KEY idno = <ls_idno_db>-idno chg_area = lv_chg_area chg_category = <ls_approval>-change_category.
                  IF sy-subrc NE 0.
*                   No field changed for this change category so send notification and carry on to the next
                    lv_enrichment_notified = abap_true.
                    READ TABLE lt_arn_teams ASSIGNING <ls_arn_team> WITH TABLE KEY team_code = <ls_approval>-enriching_team.
                    IF sy-subrc EQ 0.
                      PERFORM get_notification_user USING 'E' lv_chg_area <ls_approval> <ls_idno_db> <ls_arn_team> <ls_arn_reg_hdr>-ret_zzcatman lt_changes CHANGING lt_notif_enrich lt_teamusers lt_bdicpident.
                      PERFORM get_notification_user USING 'E' lv_chg_area <ls_approval> <ls_idno_db> <ls_arn_team> <ls_arn_reg_hdr>-gil_zzcatman lt_changes CHANGING lt_notif_enrich lt_teamusers lt_bdicpident.
                    ENDIF. "got a team
                  ENDIF. "no field has been enriched
                ENDIF. "enriching team populated

                IF lv_enrichment_notified EQ abap_false.
                  READ TABLE lt_arn_teams ASSIGNING <ls_arn_team> WITH TABLE KEY team_code = <ls_approval>-approval_team.
                  IF sy-subrc EQ 0.
                    PERFORM get_notification_user USING 'A' lv_chg_area <ls_approval> <ls_idno_db> <ls_arn_team> <ls_arn_reg_hdr>-ret_zzcatman lt_changes CHANGING lt_notif_approve lt_teamusers lt_bdicpident.
                    PERFORM get_notification_user USING 'A' lv_chg_area <ls_approval> <ls_idno_db> <ls_arn_team> <ls_arn_reg_hdr>-gil_zzcatman lt_changes CHANGING lt_notif_approve lt_teamusers lt_bdicpident.
                  ENDIF.    "got a team
                ENDIF.    "enrichment notified
              ENDLOOP.  "approvals
            ENDIF.    "any approvals ?
          ENDIF.    "got ARN_REG_HDR for category manager role ID
        ENDLOOP.  "IDNO level

        LOOP AT lt_teamusers INTO ls_teamuser.
*         For each team user send a consolidated email
          REFRESH: lt_arn_cc_hdr_enrich,
                   lt_arn_cc_hdr_approve.

          LOOP AT lt_notif_enrich ASSIGNING <ls_notif_enrich> WHERE team_code EQ ls_teamuser-team_code AND user EQ ls_teamuser-user.
            LOOP AT lt_arn_cc_hdr_all ASSIGNING <ls_arn_cc_hdr> WHERE idno EQ <ls_notif_enrich>-idno AND chg_area EQ <ls_notif_enrich>-chg_area AND chg_category EQ <ls_notif_enrich>-chg_category.
              APPEND <ls_arn_cc_hdr> TO lt_arn_cc_hdr_enrich.
            ENDLOOP.
          ENDLOOP.
          LOOP AT lt_notif_approve ASSIGNING <ls_notif_approve> WHERE team_code EQ ls_teamuser-team_code AND user EQ ls_teamuser-user.
            LOOP AT lt_arn_cc_hdr_all ASSIGNING <ls_arn_cc_hdr> WHERE idno EQ <ls_notif_approve>-idno AND chg_area EQ <ls_notif_approve>-chg_area AND chg_category EQ <ls_notif_approve>-chg_category.
              APPEND <ls_arn_cc_hdr> TO lt_arn_cc_hdr_approve.
            ENDLOOP.
          ENDLOOP.
          IF NOT lt_arn_cc_hdr_enrich[] IS INITIAL.
            MESSAGE s000 WITH 'Sending enrichment notifications to team ' ls_teamuser-team_code 'user' ls_teamuser-user.
            SORT lt_arn_cc_hdr_enrich BY idno chg_category.
            DELETE ADJACENT DUPLICATES FROM lt_arn_cc_hdr_enrich COMPARING idno chg_category.
            CALL FUNCTION 'Z_ARN_APPROVALS_EMAIL_TEAM'
              EXPORTING
                team_code                 = ls_teamuser-team_code
                user                      = ls_teamuser-user
                arn_cc_hdr_tab            = lt_arn_cc_hdr_enrich
                notif_enrich              = abap_true
              EXCEPTIONS
                email_problem             = 1
                no_users_in_team          = 2
                invalid_notification_type = 3
                team_not_found            = 4
                no_email_for_user         = 5
                no_user_for_team          = 6.
          ENDIF.
          IF NOT lt_arn_cc_hdr_approve[] IS INITIAL.
            MESSAGE s000 WITH 'Sending approval notifications to team ' ls_teamuser-team_code 'user' ls_teamuser-user.
            SORT lt_arn_cc_hdr_approve BY idno chg_category.
            DELETE ADJACENT DUPLICATES FROM lt_arn_cc_hdr_approve COMPARING idno chg_category.
            CALL FUNCTION 'Z_ARN_APPROVALS_EMAIL_TEAM'
              EXPORTING
                team_code                 = ls_teamuser-team_code
                user                      = ls_teamuser-user
                arn_cc_hdr_tab            = lt_arn_cc_hdr_approve
                notif_approve             = abap_true
              EXCEPTIONS
                email_problem             = 1
                no_users_in_team          = 2
                invalid_notification_type = 3
                team_not_found            = 4
                no_email_for_user         = 5
                no_user_for_team          = 6.
          ENDIF.
        ENDLOOP.

        MESSAGE s000 WITH 'All done'.

      ENDIF. "No changes found

      IF p_consum EQ abap_true.
        IF NOT lt_bdicpident[] IS INITIAL.
          CALL FUNCTION 'CHANGE_POINTERS_STATUS_WRITE'
            EXPORTING
              message_type           = lc_mestyp
            TABLES
              change_pointers_idents = lt_bdicpident.
        ENDIF.
      ENDIF.

    ENDIF.  "No change pointers found

    CALL FUNCTION 'DEQUEUE_E_BDCPS'
      EXPORTING
        mode_bdcps = 'E'
        mestype    = lc_mestyp.

  ENDIF.  "Change pointers locked

ENDFORM.                    "send_notifications
*&---------------------------------------------------------------------*
*&      Form  GET_NOTIFICATION_USER
*&---------------------------------------------------------------------*
FORM get_notification_user  USING    pv_notif_type  TYPE char7
                                     pv_chg_area    TYPE zarn_chg_area
                                     ps_approval    TYPE zarn_approvals
                                     ps_idno        TYPE ty_idno
                                     ps_arn_team    TYPE zarn_teams
                                     pv_catman      TYPE zarn_catman
                                     pt_changes     TYPE ty_tab_changes
                            CHANGING pt_notif       TYPE ty_tab_notif
                                     pt_team_users  TYPE ty_tab_team_users
                                     pt_bdicpident  TYPE wes_t_cpident.

  STATICS: lt_md_category TYPE SORTED TABLE OF zmd_category WITH UNIQUE KEY role_id,
           lt_md_dc_by_cm TYPE SORTED TABLE OF zmd_dc_by_cm_ass WITH NON-UNIQUE KEY cm_code,
           lt_md_dc_buyer TYPE SORTED TABLE OF zmd_dc_buyer WITH UNIQUE KEY dc_buyer_code.

  DATA: lv_table     TYPE char40,
        lv_field     TYPE char40,
        lv_fieldname TYPE char40,
        lv_username  TYPE xubname,
        ls_notif     LIKE LINE OF pt_notif,
        ls_teamuser  LIKE LINE OF pt_team_users.

  FIELD-SYMBOLS: <ls_md_category> LIKE LINE OF lt_md_category,
                 <ls_md_dc_buyer> LIKE LINE OF lt_md_dc_buyer,
                 <ls_md_dc_by_cm> LIKE LINE OF lt_md_dc_by_cm,
                 <ls_change>      LIKE LINE OF pt_changes,
                 <ls_cpident>     LIKE LINE OF <ls_change>-cpidents,
                 <lv_field>       TYPE any.

  CHECK NOT pv_catman IS INITIAL.

  IF lt_md_category[] IS INITIAL.
    SELECT *
      INTO TABLE lt_md_category
      FROM zmd_category.

    SELECT *
      INTO TABLE lt_md_dc_buyer
      FROM zmd_dc_buyer.

    SELECT *
      INTO TABLE lt_md_dc_by_cm
      FROM zmd_dc_by_cm_ass.
  ENDIF.

  CLEAR: lv_username,
         lv_table,
         lv_field,
         lv_fieldname.
  SPLIT ps_arn_team-fieldtoemail AT '-' INTO lv_table lv_field.
  CASE lv_table.
    WHEN 'ZMD_CATEGORY'.
      READ TABLE lt_md_category ASSIGNING <ls_md_category> WITH TABLE KEY role_id = pv_catman.
      IF sy-subrc EQ 0.
        CONCATENATE '<ls_md_category>-' lv_field INTO lv_fieldname.
        UNASSIGN <lv_field>.
        ASSIGN (lv_fieldname) TO <lv_field>.
        IF <lv_field> IS ASSIGNED.
          lv_username = <lv_field>.
        ENDIF.
      ENDIF.
    WHEN 'ZMD_DC_BUYER'.
      LOOP AT lt_md_dc_by_cm ASSIGNING <ls_md_dc_by_cm> WHERE cm_code EQ pv_catman.
        CHECK lv_username IS INITIAL.
        READ TABLE lt_md_dc_buyer ASSIGNING <ls_md_dc_buyer> WITH TABLE KEY dc_buyer_code = <ls_md_dc_by_cm>-dc_buyer_code.
        IF sy-subrc EQ 0.
          lv_username = <ls_md_dc_buyer>-dc_buyer_user.
        ENDIF.
      ENDLOOP.
  ENDCASE.

  IF lv_username IS INITIAL.
    IF pv_notif_type EQ 'E'.
      MESSAGE s043 WITH 'enriching' pv_catman ps_arn_team-team_code ps_arn_team-fieldtoemail.
    ELSEIF pv_notif_type EQ 'A'.
      MESSAGE s043 WITH 'approving' pv_catman ps_arn_team-team_code ps_arn_team-fieldtoemail.
    ENDIF.
  ELSE.
    CLEAR ls_notif.
    ls_notif-team_code    = ps_arn_team-team_code.
    ls_notif-user         = lv_username.
    ls_notif-idno         = ps_idno-idno.
    ls_notif-chg_area     = pv_chg_area.
    ls_notif-chg_category = ps_approval-change_category.
    INSERT ls_notif INTO TABLE pt_notif.
    CLEAR ls_teamuser.
    ls_teamuser-team_code = ps_arn_team-team_code.
    ls_teamuser-user      = lv_username.
    INSERT ls_teamuser INTO TABLE pt_team_users.

    LOOP AT pt_changes ASSIGNING <ls_change> WHERE idno EQ ps_idno-idno.
      LOOP AT <ls_change>-cpidents ASSIGNING <ls_cpident>.
        APPEND <ls_cpident>-cpident TO pt_bdicpident.
      ENDLOOP.
    ENDLOOP.
  ENDIF.

ENDFORM.                    " GET_NOTIFICATION_USER
