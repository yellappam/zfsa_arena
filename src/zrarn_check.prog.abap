*&---------------------------------------------------------------------*
*& Report  ZRARN_CHECK
*&
*&---------------------------------------------------------------------*
* PROJECT        # One Data
* SPECIFICATION  # Feature 3126 - AReNa Validation Engine
* DATE WRITTEN   # 2016-02
* SAP VERSION    # SAPK-606 05 (SAP Enterprise Extension Retail)
* TYPE           # Method
* AUTHOR         # Ivo Skolek, C90001587
*-----------------------------------------------------------------------
* TITLE          # On-demand Check Report - calling validation engine
* PURPOSE        # Run configured validations on AReNa Article Data
*                #
*-----------------------------------------------------------------------
* CALLED FROM    # validations configured in tx. ZARN_CONSISTENCY_CHK
*                #
*-----------------------------------------------------------------------
* CALLS TO       # validation engine - class zcl_arn_validation_engine
*-----------------------------------------------------------------------
* RESTRICTIONS
*
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE           # 24.06.2020
* CHANGE No.     # SSM-1 NW Range Policy ZARN_GUI
* DESCRIPTION    # Added ZARN_REG_CLUSTER and ZARN_CLUSTER_CFG table extraction
* WHO            # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------


REPORT zrarn_check.

*----------------------------------------------------------------------*
*       CLASS lcl_arena_check_rep DEFINITION
*----------------------------------------------------------------------*
*
*----------------------------------------------------------------------*
CLASS lcl_arena_check_rep DEFINITION.
  PUBLIC SECTION.
    METHODS:
      constructor,
      select_arena_data,
      run_validations,
      display_alv.

    CLASS-METHODS:
      switch_reg_fields.

  PRIVATE SECTION.

    METHODS:
      crossfilter_reg_data,
      crossfilter_nat_data.


    TYPES: BEGIN OF ty_s_idno_vers,
             idno    TYPE zarn_idno,
             version TYPE zarn_version,
           END OF ty_s_idno_vers.

    DATA:
      gr_validation_engine  TYPE REF TO zcl_arn_validation_engine,
      gs_zsarn_prod_data    TYPE zsarn_prod_data,
      gs_zsarn_reg_data     TYPE zsarn_reg_data,
      gs_zsarn_all_table_wa TYPE zsarn_all_table_wa,
      gt_idno_vers          TYPE STANDARD TABLE OF ty_s_idno_vers,
      gt_validation_output  TYPE zarn_t_rl_output.

ENDCLASS.                    "lcl_arena_check_rep DEFINITION

TABLES: zarn_rl_ev_val, zarn_rl_valid.

DATA: lr_arena_check_rep TYPE REF TO lcl_arena_check_rep,

      "data for select options
      lv_art             TYPE zarn_reg_hdr-matnr,
      lv_ban             TYPE zarn_reg_banner-banner,
      lv_vnd             TYPE zarn_reg_pir-lifnr,
      lv_ean             TYPE zarn_reg_ean-ean11,
      lv_gtn             TYPE zarn_gtin_var-gtin_code,
      lv_idn             TYPE zarn_products-idno,
      lv_cat             TYPE zarn_catman,
      lv_fan             TYPE zarn_products-fan_id,
      lv_gln             TYPE zarn_pir-gln.



SELECTION-SCREEN BEGIN OF BLOCK blr WITH FRAME TITLE TEXT-blr.
SELECT-OPTIONS: so_art FOR lv_art MODIF ID reg,
                so_ban FOR lv_ban MODIF ID reg,
                so_ean FOR lv_ean MODIF ID reg,
                so_vnd FOR lv_vnd MODIF ID reg,
                so_cat FOR lv_cat MODIF ID reg.
SELECTION-SCREEN END OF BLOCK blr.


PARAMETERS: p_naton AS CHECKBOX DEFAULT space USER-COMMAND nat.
SELECTION-SCREEN BEGIN OF BLOCK bln WITH FRAME TITLE TEXT-bln.
SELECT-OPTIONS: so_idn FOR lv_idn,
                so_fan FOR lv_fan,
                so_gtn FOR lv_gtn,
                so_gln FOR lv_gln.
PARAMETERS      p_ver TYPE zarn_version.
SELECTION-SCREEN END OF BLOCK bln.


SELECTION-SCREEN BEGIN OF BLOCK blv WITH FRAME TITLE TEXT-blv.
SELECT-OPTIONS : so_event FOR zarn_rl_ev_val-event_id
                    NO-EXTENSION NO INTERVALS DEFAULT 'CHECK_REP'
                    MATCHCODE OBJECT zharn_rl_event_check_report,
                 so_valid FOR zarn_rl_valid-validation_id
                    MATCHCODE OBJECT zharn_rl_valid_id.
SELECTION-SCREEN END OF BLOCK blv.

START-OF-SELECTION.

  "crossreference only to business rule repository
  IF 1 = 2. MESSAGE s032(zarena_business_rule). ENDIF.

  CREATE OBJECT lr_arena_check_rep.
  lr_arena_check_rep->select_arena_data( ).
  lr_arena_check_rep->run_validations( ).
  lr_arena_check_rep->display_alv( ).

AT SELECTION-SCREEN OUTPUT.
  lcl_arena_check_rep=>switch_reg_fields( ).


*----------------------------------------------------------------------*
*       CLASS lcl_arena_check_rep IMPLEMENTATION
*----------------------------------------------------------------------*
*
*----------------------------------------------------------------------*
CLASS lcl_arena_check_rep IMPLEMENTATION.

  METHOD constructor.

    DATA: lv_event TYPE zarn_e_rl_event_id,
          lr_excep TYPE REF TO zcx_arn_validation_err.
    lv_event = so_event-low.
    IF lv_event IS INITIAL.
      lv_event =
        zcl_arn_validation_engine=>gc_dummy_event_all_validations.
    ENDIF.

    TRY.

        CREATE OBJECT gr_validation_engine
          EXPORTING
            iv_event          = lv_event
            iv_bapiret_output = abap_true.

      CATCH zcx_arn_validation_err INTO lr_excep.
        MESSAGE i000(zarena_msg) WITH lr_excep->msgv1 lr_excep->msgv2
                                      lr_excep->msgv3 lr_excep->msgv4.
        LEAVE LIST-PROCESSING.
    ENDTRY.

    "do filtering by selected validations
    IF so_valid[] IS NOT INITIAL.
      DELETE gr_validation_engine->gt_validation[]
        WHERE validation_id NOT IN so_valid.
    ENDIF.

  ENDMETHOD.                    "constructor

  METHOD crossfilter_reg_data.

    DEFINE delete_hdr_where_not_in.
      REFRESH lt_idno_rng.
      "&1 - reg/prod meaning source tabe is regional/national
      "&2 - table name

      "make range of IDNOs from child table
      LOOP AT gs_zsarn_&1_data-&2 INTO gs_zsarn_all_table_wa-&2.
        ls_idno_rng-low = gs_zsarn_all_table_wa-&2-idno.
        APPEND ls_idno_rng TO lt_idno_rng.
      ENDLOOP.
      IF sy-subrc EQ 0.
        "filter header only for these IDNOs
        DELETE gs_zsarn_reg_data-zarn_reg_hdr
          WHERE idno NOT IN lt_idno_rng.
      ELSE.
        REFRESH gs_zsarn_reg_data-zarn_reg_hdr.
      ENDIF.
    END-OF-DEFINITION.

    DATA: lt_idno_rng TYPE RANGE OF zarn_idno,
          ls_idno_rng LIKE LINE OF lt_idno_rng.

    ls_idno_rng-option = 'EQ'.
    ls_idno_rng-sign = 'I'.

    "deleting the Regional Header will ensure that those IDNOs will not
    "be processed in validation engine (no need to cross-delete all tables)

    IF so_ban IS NOT INITIAL.
      "delete all headers for which specified banner records not found
      delete_hdr_where_not_in reg zarn_reg_banner.
    ENDIF.

    IF so_vnd IS NOT INITIAL.
      "delete all headers for which specified lifnr records not found
      delete_hdr_where_not_in reg zarn_reg_pir.
    ENDIF.

    IF so_ean IS NOT INITIAL.
      "delete all headers for which specified barcode records not found
      delete_hdr_where_not_in reg zarn_reg_ean.
    ENDIF.

    IF so_fan IS NOT INITIAL.
      "delete all headers for which specified FAN ID records not found
      delete_hdr_where_not_in prod zarn_products.
    ENDIF.

    IF so_gtn IS NOT INITIAL.
      "delete all headers for which specified GTIN records not found
      delete_hdr_where_not_in prod zarn_gtin_var.
    ENDIF.

    IF so_gln IS NOT INITIAL.
      "delete all headers for which specified GLN records not found
      delete_hdr_where_not_in prod zarn_pir.
    ENDIF.




  ENDMETHOD.                    "crossfilter_reg_data

  METHOD crossfilter_nat_data.

    DEFINE delete_hdr_where_not_in.
      REFRESH lt_idno_rng.
      "make range of IDNOs from child table
      LOOP AT gs_zsarn_prod_data-&1 INTO gs_zsarn_all_table_wa-&1.
        ls_idno_rng-low = gs_zsarn_all_table_wa-&1-idno.
        APPEND ls_idno_rng TO lt_idno_rng.
      ENDLOOP.
      IF sy-subrc EQ 0.
        "filter header only for these IDNOs
        DELETE gs_zsarn_prod_data-zarn_products
          WHERE idno NOT IN lt_idno_rng.
      ELSE.
        REFRESH gs_zsarn_prod_data-zarn_products.
      ENDIF.
    END-OF-DEFINITION.


    DATA: lt_idno_rng TYPE RANGE OF zarn_idno,
          ls_idno_rng LIKE LINE OF lt_idno_rng.

    "deleting the National Header will ensure that those IDNOs will not
    "be processed in validation engine (no need to cross-delete all tables)
    "(if ticked as "national" only on input)

    ls_idno_rng-option = 'EQ'.
    ls_idno_rng-sign   = 'I'.

    IF so_gtn IS NOT INITIAL.
      delete_hdr_where_not_in zarn_gtin_var.
    ENDIF.

    IF so_gln IS NOT INITIAL.
      delete_hdr_where_not_in zarn_pir.
    ENDIF.



  ENDMETHOD.                    "crossfilter_nat_data


  METHOD select_arena_data.

    DATA lt_idnos TYPE STANDARD TABLE OF zarn_reg_hdr-idno.

    "pre select record IDs in a most effective way based on provided data

    IF so_art IS NOT INITIAL.
      SELECT idno FROM zarn_reg_hdr
        INTO TABLE lt_idnos
        WHERE idno           IN so_idn
          AND matnr          IN so_art
          AND ( gil_zzcatman IN so_cat
             OR ret_zzcatman IN so_cat ).
    ELSEIF so_vnd IS NOT INITIAL.
      SELECT idno FROM zarn_reg_pir
        INTO TABLE lt_idnos
        WHERE idno        IN so_idn
          AND lifnr       IN so_vnd.
    ELSEIF so_ean IS NOT INITIAL.
      SELECT idno FROM zarn_reg_ean
        INTO TABLE lt_idnos
        WHERE idno        IN so_idn
          AND ean11       IN so_ean.
    ELSEIF so_idn IS NOT INITIAL OR so_fan IS NOT INITIAL.
      SELECT idno FROM zarn_products
         INTO TABLE lt_idnos
         WHERE idno  IN so_idn
           AND fan_id IN so_fan.
    ELSEIF so_gln IS NOT INITIAL.
      SELECT idno FROM zarn_pir
         INTO TABLE lt_idnos
        WHERE idno  IN so_idn
          AND gln   IN so_gln.
    ELSEIF so_gtn IS NOT INITIAL.
      SELECT idno FROM zarn_gtin_var
         INTO TABLE lt_idnos
       WHERE idno  IN so_idn
         AND gtin_code  IN so_gtn.
    ELSEIF so_cat IS NOT INITIAL.
      SELECT idno FROM zarn_reg_hdr
        INTO TABLE lt_idnos
        WHERE idno           IN so_idn
          AND matnr    IN so_art
          AND ( gil_zzcatman IN so_cat
             OR ret_zzcatman IN so_cat ).
    ELSEIF so_ban IS NOT INITIAL.
      SELECT idno FROM zarn_reg_banner
         INTO TABLE lt_idnos
        WHERE idno IN so_idn
          AND banner IN so_ban.
    ELSE.
      "please enter at least 1 of the selection criteria
      MESSAGE i008(zarena_msg).
      LEAVE LIST-PROCESSING.
    ENDIF.

    IF  lt_idnos[] IS INITIAL.
      "no records found
      MESSAGE i007(zarena_msg).
      LEAVE LIST-PROCESSING.
    ENDIF.


    IF p_naton IS INITIAL.
      "full data - both regional and national
      SELECT * FROM zarn_reg_hdr
        INTO TABLE gs_zsarn_reg_data-zarn_reg_hdr
        FOR ALL ENTRIES IN lt_idnos
          WHERE idno = lt_idnos-table_line
            AND matnr IN so_art
            AND ( gil_zzcatman IN so_cat
               OR ret_zzcatman IN so_cat ).
      SELECT * FROM zarn_reg_uom
        INTO TABLE gs_zsarn_reg_data-zarn_reg_uom
         FOR ALL ENTRIES IN lt_idnos
          WHERE idno = lt_idnos-table_line.
      SELECT * FROM zarn_reg_ean
        INTO TABLE gs_zsarn_reg_data-zarn_reg_ean
        FOR ALL ENTRIES IN lt_idnos
           WHERE idno = lt_idnos-table_line
            AND ean11   IN so_ean.
      SELECT * FROM zarn_reg_pir
        INTO TABLE gs_zsarn_reg_data-zarn_reg_pir
        FOR ALL ENTRIES IN lt_idnos
           WHERE idno = lt_idnos-table_line
            AND lifnr IN so_vnd.
      SELECT * FROM zarn_reg_banner
        INTO TABLE gs_zsarn_reg_data-zarn_reg_banner
        FOR ALL ENTRIES IN lt_idnos
           WHERE idno = lt_idnos-table_line
            AND banner IN so_ban.

      SELECT *
        FROM zarn_reg_cluster
        INTO TABLE gs_zsarn_reg_data-zarn_reg_cluster
        FOR ALL ENTRIES IN lt_idnos
        WHERE idno = lt_idnos-table_line
          AND banner IN so_ban.

    ENDIF.

    "select national data for particular version
    IF p_ver IS NOT INITIAL.
      SELECT * FROM zarn_products INTO TABLE gs_zsarn_prod_data-zarn_products
        FOR ALL ENTRIES IN lt_idnos
        WHERE idno = lt_idnos-table_line
          AND version EQ p_ver
          AND fan_id  IN so_fan.
    ELSE. "version not entered, will use only current version
      SELECT idno current_ver FROM zarn_ver_status
         INTO TABLE gt_idno_vers
         FOR ALL ENTRIES IN lt_idnos
         WHERE idno = lt_idnos-table_line.
      IF sy-subrc EQ 0.
        SELECT * FROM zarn_products
          INTO TABLE gs_zsarn_prod_data-zarn_products
          FOR ALL ENTRIES IN gt_idno_vers
          WHERE idno EQ gt_idno_vers-idno
            AND version EQ gt_idno_vers-version
            AND fan_id  IN so_fan.
      ENDIF.
    ENDIF.
    IF gs_zsarn_prod_data-zarn_products IS NOT INITIAL.
      SELECT * FROM zarn_list_price INTO TABLE
        gs_zsarn_prod_data-zarn_list_price
        FOR ALL ENTRIES IN gs_zsarn_prod_data-zarn_products
        WHERE idno = gs_zsarn_prod_data-zarn_products-idno
          AND version = gs_zsarn_prod_data-zarn_products-version.

      SELECT * FROM zarn_gtin_var INTO TABLE
        gs_zsarn_prod_data-zarn_gtin_var
        FOR ALL ENTRIES IN gs_zsarn_prod_data-zarn_products
        WHERE idno = gs_zsarn_prod_data-zarn_products-idno
          AND version = gs_zsarn_prod_data-zarn_products-version
          AND gtin_code IN so_gtn.

      SELECT * FROM zarn_pir INTO TABLE
        gs_zsarn_prod_data-zarn_pir
        FOR ALL ENTRIES IN gs_zsarn_prod_data-zarn_products
        WHERE idno = gs_zsarn_prod_data-zarn_products-idno
          AND version = gs_zsarn_prod_data-zarn_products-version
          AND gln IN so_gln.
    ENDIF.

    "cross filter will delete header records which
    "were not found in child tables
    "in case particular select options were entered
    IF p_naton IS INITIAL.
      crossfilter_reg_data( ).
      IF gs_zsarn_reg_data-zarn_reg_hdr[] IS INITIAL.
        "no records found
        MESSAGE i007(zarena_msg).
        LEAVE LIST-PROCESSING.
      ENDIF.
    ELSE.
      crossfilter_nat_data( ).
      IF gs_zsarn_prod_data-zarn_products[] IS INITIAL.
        "no records found
        MESSAGE i007(zarena_msg).
        LEAVE LIST-PROCESSING.
      ENDIF.
    ENDIF.

  ENDMETHOD.                    "select_arena_data


  METHOD run_validations.

    DATA: lr_excep TYPE REF TO zcx_arn_validation_err.

    TRY.
        IF p_naton IS INITIAL.
          "full data - regional + national
          gr_validation_engine->validate_arn_bulk(
            EXPORTING is_zsarn_reg_data  = gs_zsarn_reg_data
                      is_zsarn_prod_data = gs_zsarn_prod_data
            IMPORTING et_output       = gt_validation_output ).
        ELSE.
          "national data only
          gr_validation_engine->validate_arn_bulk(
            EXPORTING is_zsarn_prod_data = gs_zsarn_prod_data
            IMPORTING et_output       = gt_validation_output ).
        ENDIF.

      CATCH zcx_arn_validation_err INTO lr_excep.
        MESSAGE i000(zarena_msg) WITH lr_excep->msgv1 lr_excep->msgv2
                                      lr_excep->msgv3 lr_excep->msgv4.
        LEAVE LIST-PROCESSING.

    ENDTRY.
  ENDMETHOD.                    "run_validations

  METHOD display_alv.
    DATA: lr_table   TYPE REF TO cl_salv_table,
          "lr_columns TYPE REF TO cl_salv_columns_table,
          lr_layout  TYPE REF TO cl_salv_layout,
          ls_lay_key TYPE salv_s_layout_key,
          lv_header  TYPE lvc_title.

    TRY.

        cl_salv_table=>factory( IMPORTING r_salv_table = lr_table
                                CHANGING t_table = gt_validation_output ).

        lr_table->get_columns( )->set_optimize( 'X' ).

        lr_layout = lr_table->get_layout( ).
        lr_layout->set_default( 'X' ).
        ls_lay_key-report = sy-repid.
        lr_layout->set_key( ls_lay_key ).
        lr_layout->set_save_restriction( if_salv_c_layout=>restrict_none ).

        lv_header = |AReNa On-Demand Check Report - Number of errors { lines( gt_validation_output ) } |.
        lr_table->get_display_settings( )->set_list_header( lv_header ).
        lr_table->get_functions( )->set_all( ).


        lr_table->display( ).

      CATCH cx_salv_msg.
        MESSAGE e009(zarena_msg).
        LEAVE LIST-PROCESSING.
    ENDTRY.

  ENDMETHOD.                    "display_alv

  METHOD switch_reg_fields.

    "dont allow entry for regional data if "national only" ticked
    LOOP AT SCREEN.
      IF screen-group1 = 'REG'.
        IF p_naton = abap_true.
          screen-input = 0.
        ELSE.
          IF screen-group3 NE 'OPU'.
            screen-input = 1.
          ENDIF.
        ENDIF.
        MODIFY SCREEN.
      ENDIF.
    ENDLOOP.

    "also clear if something was entered already
    IF p_naton = abap_true.
      CLEAR: so_art, so_ban, so_ean, so_vnd, so_cat.
      REFRESH: so_art, so_ban, so_ean, so_vnd, so_cat.
    ENDIF.

  ENDMETHOD.                    "switch_reg_fields

ENDCLASS.                    "lcl_arena_check_rep IMPLEMENTATION
