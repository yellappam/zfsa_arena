FUNCTION zarn_map_article_post_data.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(IS_IDNO_DATA) TYPE  ZSARN_IDNO_DATA
*"     REFERENCE(IS_PROD_DATA) TYPE  ZSARN_PROD_DATA
*"     REFERENCE(IS_REG_DATA) TYPE  ZSARN_REG_DATA
*"     REFERENCE(IR_STATUS) TYPE  ZTARN_STATUS_RANGE
*"     REFERENCE(IT_MARA) TYPE  MARA_TAB
*"     REFERENCE(IT_MARM) TYPE  MARM_TAB
*"     REFERENCE(IT_MEAN) TYPE  MEAN_TAB
*"     REFERENCE(IT_EINA) TYPE  MMPR_EINA
*"     REFERENCE(IT_EINE) TYPE  MMPR_EINE
*"     REFERENCE(IT_MAW1) TYPE  WRF_MAW1_TTY
*"     REFERENCE(IT_HOST_SAP) TYPE  ZTMD_HOST_SAP
*"     REFERENCE(IT_T006) TYPE  TT_T006
*"     REFERENCE(IT_T005) TYPE  WGDS_COUNTRIES_TTY
*"     REFERENCE(IT_TCURC) TYPE  FIAPPT_T_TCURC_CBR
*"     REFERENCE(IT_COO_T) TYPE  ZTARN_COO_T
*"     REFERENCE(IT_GEN_UOM_T) TYPE  ZTARN_GEN_UOM_T
*"     REFERENCE(IT_T134) TYPE  WRF_T134_TTY
*"     REFERENCE(IT_TVARVC) TYPE  TVARVC_T
*"     REFERENCE(IT_TVARVC_HYB_CODE) TYPE  TVARVC_T
*"     REFERENCE(IT_TVTA) TYPE  TVTA_TT
*"     REFERENCE(IT_UOM_CAT) TYPE  ZTARN_UOM_CAT
*"     REFERENCE(IS_MATGRP_HIER) TYPE  WRF_MATGRP_HIER
*"     REFERENCE(IT_MATGRP_PROD) TYPE  WRF_T_MATGRP_PROD
*"     REFERENCE(IT_ART_HIER) TYPE  ZTARN_ART_HIER
*"     REFERENCE(IV_EKORG_MAIN) TYPE  EKORG DEFAULT '9999'
*"     REFERENCE(IV_BATCH) TYPE  FLAG OPTIONAL
*"     REFERENCE(IT_PIR_EKORG) TYPE  ZARN_T_PIR_PUR_CNF OPTIONAL
*"  EXPORTING
*"     REFERENCE(ES_PIR_KEY) TYPE  ZSARN_KEY
*"     REFERENCE(EV_NAT_MAP) TYPE  FLAG
*"     REFERENCE(EV_REG_MAP) TYPE  FLAG
*"     REFERENCE(ES_HEADDATA) TYPE  BAPIE1MATHEAD
*"     REFERENCE(ES_HIERARCHY_DATA) TYPE  BAPI_WRF_HIER_CHANGE_HEAD
*"     REFERENCE(ET_ERRORKEYS) TYPE  WDSD_VALKEY_TTY
*"     REFERENCE(ET_VARIANTSKEYS) TYPE  BAPIE1VARKEY_TAB
*"     REFERENCE(ET_CHARACTERISTICVALUE) TYPE  BAPIE1AUSPRT_TAB
*"     REFERENCE(ET_CHARACTERISTICVALUEX) TYPE  BAPIE1AUSPRTX_TAB
*"     REFERENCE(ET_CLIENTDATA) TYPE  BAPIE1MARART_TAB
*"     REFERENCE(ET_CLIENTDATAX) TYPE  BAPIE1MARARTX_TAB
*"     REFERENCE(ET_CLIENTEXT) TYPE  BAPIE1MARAEXTRT_TAB
*"     REFERENCE(ET_CLIENTEXTX) TYPE  BAPIE1MARAEXTRTX_TAB
*"     REFERENCE(ET_ADDNLCLIENTDATA) TYPE  BAPIE1MAW1RT_TAB
*"     REFERENCE(ET_ADDNLCLIENTDATAX) TYPE  BAPIE1MAW1RTX_TAB
*"     REFERENCE(ET_MATERIALDESCRIPTION) TYPE  BAPIE1MAKTRT_TAB
*"     REFERENCE(ET_PLANTDATA) TYPE  BAPIE1MARCRT_TAB
*"     REFERENCE(ET_PLANTDATAX) TYPE  BAPIE1MARCRTX_TAB
*"     REFERENCE(ET_PLANTEXT) TYPE  BAPIE1MARCEXTRT_TAB
*"     REFERENCE(ET_PLANTEXTX) TYPE  BAPIE1MARCEXTRTX_TAB
*"     REFERENCE(ET_FORECASTPARAMETERS) TYPE  BAPIE1MPOPRT_TAB
*"     REFERENCE(ET_FORECASTPARAMETERSX) TYPE  BAPIE1MPOPRTX_TAB
*"     REFERENCE(ET_FORECASTVALUES) TYPE  BAPIE1MPRWRT_TAB
*"     REFERENCE(ET_TOTALCONSUMPTION) TYPE  BAPIE1MVEGRT_TAB
*"     REFERENCE(ET_UNPLNDCONSUMPTION) TYPE  BAPIE1MVEURT_TAB
*"     REFERENCE(ET_PLANNINGDATA) TYPE  BAPIE1MPGDRT_TAB
*"     REFERENCE(ET_PLANNINGDATAX) TYPE  BAPIE1MPGDRTX_TAB
*"     REFERENCE(ET_STORAGELOCATIONDATA) TYPE  BAPIE1MARDRT_TAB
*"     REFERENCE(ET_STORAGELOCATIONDATAX) TYPE  BAPIE1MARDRTX_TAB
*"     REFERENCE(ET_STORAGELOCATIONEXT) TYPE  BAPIE1MARDEXTRT_TAB
*"     REFERENCE(ET_STORAGELOCATIONEXTX) TYPE  BAPIE1MARDEXTRTX_TAB
*"     REFERENCE(ET_UNITSOFMEASURE) TYPE  BAPIE1MARMRT_TAB
*"     REFERENCE(ET_UNITSOFMEASUREX) TYPE  BAPIE1MARMRTX_TAB
*"     REFERENCE(ET_UNITOFMEASURETEXTS) TYPE  BAPIE1MAMTRT_TAB
*"     REFERENCE(ET_INTERNATIONALARTNOS) TYPE  BAPIE1MEANRT_TAB
*"     REFERENCE(ET_VENDOREAN) TYPE  BAPIE1MLEART_TAB
*"     REFERENCE(ET_LAYOUTMODULEASSGMT) TYPE  WSTN_BAPIE1MALGRT_TAB
*"     REFERENCE(ET_LAYOUTMODULEASSGMTX) TYPE  WSTN_BAPIE1MALGRTX_TAB
*"     REFERENCE(ET_TAXCLASSIFICATIONS) TYPE  BAPIE1MLANRT_TAB
*"     REFERENCE(ET_VALUATIONDATA) TYPE  BAPIE1MBEWRT_TAB
*"     REFERENCE(ET_VALUATIONDATAX) TYPE  BAPIE1MBEWRTX_TAB
*"     REFERENCE(ET_VALUATIONEXT) TYPE  BAPIE1MBEWEXTRT_TAB
*"     REFERENCE(ET_VALUATIONEXTX) TYPE  BAPIE1MBEWEXTRTX_TAB
*"     REFERENCE(ET_WAREHOUSENUMBERDATA) TYPE  BAPIE1MLGNRT_TAB
*"     REFERENCE(ET_WAREHOUSENUMBERDATAX) TYPE  BAPIE1MLGNRTX_TAB
*"     REFERENCE(ET_WAREHOUSENUMBEREXT) TYPE  BAPIE1MLGNEXTRT_TAB
*"     REFERENCE(ET_WAREHOUSENUMBEREXTX) TYPE  BAPIE1MLGNEXTRTX_TAB
*"     REFERENCE(ET_STORAGETYPEDATA) TYPE  BAPIE1MLGTRT_TAB
*"     REFERENCE(ET_STORAGETYPEDATAX) TYPE  BAPIE1MLGTRTX_TAB
*"     REFERENCE(ET_STORAGETYPEEXT) TYPE  BAPIE1MLGTEXTRT_TAB
*"     REFERENCE(ET_STORAGETYPEEXTX) TYPE  BAPIE1MLGTEXTRTX_TAB
*"     REFERENCE(ET_SALESDATA) TYPE  BAPIE1MVKERT_TAB
*"     REFERENCE(ET_SALESDATAX) TYPE  BAPIE1MVKERTX_TAB
*"     REFERENCE(ET_SALESEXT) TYPE  BAPIE1MVKEEXTRT_TAB
*"     REFERENCE(ET_SALESEXTX) TYPE  BAPIE1MVKEEXTRTX_TAB
*"     REFERENCE(ET_POSDATA) TYPE  BAPIE1WLK2RT_TAB
*"     REFERENCE(ET_POSDATAX) TYPE  BAPIE1WLK2RTX_TAB
*"     REFERENCE(ET_POSEXT) TYPE  BAPIE1WLK2EXTRT_TAB
*"     REFERENCE(ET_POSEXTX) TYPE  BAPIE1WLK2EXTRTX_TAB
*"     REFERENCE(ET_MATERIALLONGTEXT) TYPE  BAPIE1MLTXRT_TAB
*"     REFERENCE(ET_PLANTKEYS) TYPE  BAPIE1WRKKEY_TAB
*"     REFERENCE(ET_STORAGELOCATIONKEYS) TYPE  BAPIE1LGOKEY_TAB
*"     REFERENCE(ET_DISTRCHAINKEYS) TYPE  BAPIE1VTLKEY_TAB
*"     REFERENCE(ET_WAREHOUSENOKEYS) TYPE  BAPIE1LGNKEY_TAB
*"     REFERENCE(ET_STORAGETYPEKEYS) TYPE  BAPIE1LGTKEY_TAB
*"     REFERENCE(ET_VALUATIONTYPEKEYS) TYPE  BAPIE1BWAKEY_TAB
*"     REFERENCE(ET_IMPORTEXTENSION) TYPE  WRF_BAPIPARMATEX_TTY
*"     REFERENCE(ET_INFORECORD_GENERAL) TYPE  WRF_BAPIEINA_TTY
*"     REFERENCE(ET_INFORECORD_PURCHORG) TYPE  WRF_BAPIEINE_TTY
*"     REFERENCE(ET_EINE_UNIT_ERROR) TYPE  WRF_BAPIEINE_TTY
*"     REFERENCE(ET_SOURCE_LIST) TYPE  WRF_EORDU_TTY
*"     REFERENCE(ET_ADDITIONALDATA) TYPE  WRF_BAPIE1WTA01_TTY
*"     REFERENCE(ET_CALCULATIONITEMIN) TYPE  WRF_BAPICALCITEMIN_TTY
*"     REFERENCE(ET_CALCULATIONITEMINX) TYPE  WRF_BAPICALCITEMINX_TTY
*"     REFERENCE(ET_CALCULATIONEXTIN) TYPE  BAPIPAREX_TAB
*"     REFERENCE(ET_CALCULATIONITEMOUT) TYPE  WRF_BAPICALCITEMOUT_TTY
*"     REFERENCE(ET_RECIPIENTPARAMETERS) TYPE  WRF_BAPI_WRPL_IMPORT_TTY
*"     REFERENCE(ET_RECIPIENTPARAMETERSX) TYPE
*"        WRF_BAPI_WRPL_IMPORTX_TTY
*"     REFERENCE(ET_VENDORMATHEADER) TYPE
*"        WRF_BAPI_VENDOR_MAT_HEADER_TTY
*"     REFERENCE(ET_VENDORMATCHARVALUES) TYPE  WRF_BAPI_VENDOR_MCV_TTY
*"     REFERENCE(ET_LISTINGCONDITIONS) TYPE  WSTN_WLK1_UEB_TAB
*"     REFERENCE(ET_BOMHEADER) TYPE  WRF_WSTR_IN_TTY
*"     REFERENCE(ET_BOMPOSITIONS) TYPE  WRF_STPOB_TTY
*"     REFERENCE(ET_PRICECONDITIONS) TYPE  EDIDD_TT
*"     REFERENCE(ET_HIERARCHY_ITEMS) TYPE  BAPI_WRF_HIER_CH_ITEMS_TTY
*"     REFERENCE(ET_FOLLOWUPMATDATA) TYPE  WRF_FOLUP_TYP_A_TTY
*"     REFERENCE(ET_MATERIALDATAX) TYPE  WRF_MATDATAX_TTY
*"     REFERENCE(ET_CALP_VB) TYPE  CALP_VB_TAB
*"     REFERENCE(ET_BAPI_CLIENTEXT) TYPE  ZMD_T_BAPI_CLIENTEXT
*"     REFERENCE(ET_BAPI_PLANTEXT) TYPE  ZMD_T_BAPI_PLANTEXT
*"     REFERENCE(ET_BAPI_SALESEXT) TYPE  ZMD_T_BAPI_SALESEXT
*"----------------------------------------------------------------------
* DATE             # 06.06.2018
* CHANGE No.       # ChaRM 9000003675; JIRA CI18-506 NEW Range Status flag
* DESCRIPTION      # New Range Flag, Range Availability and Range Detail
*                  # fields added in Arena and on material master in Listing
*                  # tab and Basic Data Text tab (materiallongtext).
*                  # Needed to include materiallongtext export parameter
* WHO              # C90005557, Tessa Newman
*-----------------------------------------------------------------------

  DATA: ls_idno_data            TYPE ty_s_idno_data,
        ls_prod_data            TYPE zsarn_prod_data,
        ls_reg_data             TYPE zsarn_reg_data,
        lr_status               TYPE ztarn_status_range,
        lv_ekorg_main           TYPE ekorg,
        lv_batch                TYPE flag,
        lv_nat_map              TYPE flag,
        lv_reg_map              TYPE flag,
        lv_art_mode             TYPE char1,
        lt_mara                 TYPE ty_t_mara,
        lt_marm                 TYPE ty_t_marm,
        lt_mean                 TYPE ty_t_mean,
        lt_eina                 TYPE ty_t_eina,
        lt_eine                 TYPE ty_t_eine,
        lt_maw1                 TYPE ty_t_maw1,
        lt_host_sap             TYPE ty_t_host_sap,
        lt_t006                 TYPE ty_t_t006,
        lt_t005                 TYPE ty_t_t005,
        lt_tcurc                TYPE ty_t_tcurc,
        lt_coo_t                TYPE ty_t_coo_t,
        lt_gen_uom_t            TYPE ty_t_gen_uom_t,
        lt_t134                 TYPE ty_t_t134,
        "lt_tvarvc_ekorg         TYPE tvarvc_t,             "IS 2019/06
        lt_pir_ekorg            TYPE zarn_t_pir_pur_cnf,    "IS 2019/06
        lt_tvarvc               TYPE tvarvc_t,
        lt_tvarvc_hyb_code      TYPE tvarvc_t,
        lt_tvta                 TYPE tvta_tt,
        lt_uom_cat              TYPE ty_t_uom_cat,
        ls_matgrp_hier          TYPE ty_s_matgrp_hier,
        lt_matgrp_prod          TYPE ty_t_matgrp_prod,
        lt_art_hier             TYPE ty_t_art_hier,
        lt_lfa1                 TYPE ty_t_lfa1,
        ls_lfa1                 TYPE ty_s_lfa1,
        lt_lfm1                 TYPE ty_t_lfm1,
        ls_lfm1                 TYPE ty_s_lfm1,
        lt_reg_pir_lifnr        TYPE ztarn_reg_pir,
        ls_bapi_clientext       TYPE zmd_s_bapi_clientext,
        ls_bapi_clientextx      TYPE zmd_s_bapi_clientextx,
        lt_bapi_plantext        TYPE zmd_t_bapi_plantext,                    "++ONLD-822 JKH 12.01.2017
        ls_bapi_salesext        TYPE zmd_s_bapi_salesext,
        ls_bapi_salesextx       TYPE zmd_s_bapi_salesextx,
        lt_bapi_salesext        TYPE zmd_t_bapi_salesext,
        lt_legacy_struct        TYPE ztarn_legacy_struct,
        lt_eine_unit_error      TYPE wrf_bapieine_tty,           "++IR5101117 JKH 25.08.2016
        ls_message              TYPE bal_s_msg,
        ls_log_params           TYPE zsarn_slg1_log_params_art_post,
        ls_pir_key              TYPE zsarn_key,
        ls_headdata             TYPE bapie1mathead,
        ls_hierarchy_data       TYPE bapi_wrf_hier_change_head,
        lt_return               TYPE wty_bapireturn1_tab,
        ls_return               TYPE bapireturn1,
        lt_errorkeys            TYPE STANDARD TABLE OF wrfvalkey,
        lt_variantskeys         TYPE STANDARD TABLE OF bapie1varkey,
        lt_characteristicvalue  TYPE STANDARD TABLE OF bapie1ausprt,
        lt_characteristicvaluex TYPE STANDARD TABLE OF bapie1ausprtx,
        lt_clientdata           TYPE STANDARD TABLE OF bapie1marart,
        lt_clientdatax          TYPE STANDARD TABLE OF bapie1marartx,
        lt_clientext            TYPE STANDARD TABLE OF bapie1maraextrt,
        lt_clientextx           TYPE STANDARD TABLE OF bapie1maraextrtx,
        lt_addnlclientdata      TYPE STANDARD TABLE OF bapie1maw1rt,
        lt_addnlclientdatax     TYPE STANDARD TABLE OF bapie1maw1rtx,
        lt_materialdescription  TYPE STANDARD TABLE OF bapie1maktrt,
        lt_plantdata            TYPE STANDARD TABLE OF bapie1marcrt,
        lt_plantdatax           TYPE STANDARD TABLE OF bapie1marcrtx,
        lt_plantext             TYPE STANDARD TABLE OF bapie1marcextrt,
        lt_plantextx            TYPE STANDARD TABLE OF bapie1marcextrtx,
        lt_forecastparameters   TYPE STANDARD TABLE OF bapie1mpoprt,
        lt_forecastparametersx  TYPE STANDARD TABLE OF bapie1mpoprtx,
        lt_forecastvalues       TYPE STANDARD TABLE OF bapie1mprwrt,
        lt_totalconsumption     TYPE STANDARD TABLE OF bapie1mvegrt,
        lt_unplndconsumption    TYPE STANDARD TABLE OF bapie1mveurt,
        lt_planningdata         TYPE STANDARD TABLE OF bapie1mpgdrt,
        lt_planningdatax        TYPE STANDARD TABLE OF bapie1mpgdrtx,
        lt_storagelocationdata  TYPE STANDARD TABLE OF bapie1mardrt,
        lt_storagelocationdatax TYPE STANDARD TABLE OF bapie1mardrtx,
        lt_storagelocationext   TYPE STANDARD TABLE OF bapie1mardextrt,
        lt_storagelocationextx  TYPE STANDARD TABLE OF bapie1mardextrtx,
        lt_unitsofmeasure       TYPE STANDARD TABLE OF bapie1marmrt,
        lt_unitsofmeasurex      TYPE STANDARD TABLE OF bapie1marmrtx,
        lt_unitofmeasuretexts   TYPE STANDARD TABLE OF bapie1mamtrt,
        lt_internationalartnos  TYPE STANDARD TABLE OF bapie1meanrt,
        lt_vendorean            TYPE STANDARD TABLE OF bapie1mleart,
        lt_layoutmoduleassgmt   TYPE STANDARD TABLE OF bapie1malgrt,
        lt_layoutmoduleassgmtx  TYPE STANDARD TABLE OF bapie1malgrtx,
        lt_taxclassifications   TYPE STANDARD TABLE OF bapie1mlanrt,
        lt_valuationdata        TYPE STANDARD TABLE OF bapie1mbewrt,
        lt_valuationdatax       TYPE STANDARD TABLE OF bapie1mbewrtx,
        lt_valuationext         TYPE STANDARD TABLE OF bapie1mbewextrt,
        lt_valuationextx        TYPE STANDARD TABLE OF bapie1mbewextrtx,
        lt_warehousenumberdata  TYPE STANDARD TABLE OF bapie1mlgnrt,
        lt_warehousenumberdatax TYPE STANDARD TABLE OF bapie1mlgnrtx,
        lt_warehousenumberext   TYPE STANDARD TABLE OF bapie1mlgnextrt,
        lt_warehousenumberextx  TYPE STANDARD TABLE OF bapie1mlgnextrtx,
        lt_storagetypedata      TYPE STANDARD TABLE OF bapie1mlgtrt,
        lt_storagetypedatax     TYPE STANDARD TABLE OF bapie1mlgtrtx,
        lt_storagetypeext       TYPE STANDARD TABLE OF bapie1mlgtextrt,
        lt_storagetypeextx      TYPE STANDARD TABLE OF bapie1mlgtextrtx,
        lt_salesdata            TYPE STANDARD TABLE OF bapie1mvkert,
        lt_salesdatax           TYPE STANDARD TABLE OF bapie1mvkertx,
        lt_salesext             TYPE STANDARD TABLE OF bapie1mvkeextrt,
        lt_salesextx            TYPE STANDARD TABLE OF bapie1mvkeextrtx,
        lt_posdata              TYPE STANDARD TABLE OF bapie1wlk2rt,
        lt_posdatax             TYPE STANDARD TABLE OF bapie1wlk2rtx,
        lt_posext               TYPE STANDARD TABLE OF bapie1wlk2extrt,
        lt_posextx              TYPE STANDARD TABLE OF bapie1wlk2extrtx,
        lt_materiallongtext     TYPE STANDARD TABLE OF bapie1mltxrt,
        lt_plantkeys            TYPE STANDARD TABLE OF bapie1wrkkey,
        lt_storagelocationkeys  TYPE STANDARD TABLE OF bapie1lgokey,
        lt_distrchainkeys       TYPE STANDARD TABLE OF bapie1vtlkey,
        lt_warehousenokeys      TYPE STANDARD TABLE OF bapie1lgnkey,
        lt_storagetypekeys      TYPE STANDARD TABLE OF bapie1lgtkey,
        lt_valuationtypekeys    TYPE STANDARD TABLE OF bapie1bwakey,
        lt_importextension      TYPE STANDARD TABLE OF bapiparmatex,
        lt_inforecord_general   TYPE STANDARD TABLE OF bapieina,
        lt_inforecord_purchorg  TYPE STANDARD TABLE OF wrfbapieine,
        lt_source_list          TYPE STANDARD TABLE OF eordu,
        lt_additionaldata       TYPE STANDARD TABLE OF bapie1wta01,
        lt_calculationitemin    TYPE STANDARD TABLE OF bapicalcitemin,
        lt_calculationiteminx   TYPE STANDARD TABLE OF bapicalciteminx,
        lt_calculationextin     TYPE STANDARD TABLE OF bapiparex,
        lt_calculationitemout   TYPE STANDARD TABLE OF bapicalcitemout,
        lt_recipientparameters  TYPE STANDARD TABLE OF bapi_wrpl_import,
        lt_recipientparametersx TYPE STANDARD TABLE OF bapi_wrpl_importx,
        lt_vendormatheader      TYPE STANDARD TABLE OF bapi_vendor_mat_header,
        lt_vendormatcharvalues  TYPE STANDARD TABLE OF bapi_vendor_mat_char_valu,
        lt_listingconditions    TYPE STANDARD TABLE OF wlk1_ueb,
        lt_bomheader            TYPE STANDARD TABLE OF wstr_in,
        lt_bompositions         TYPE STANDARD TABLE OF wrfstpob,
        lt_priceconditions      TYPE STANDARD TABLE OF edidd,
        lt_hierarchy_items      TYPE STANDARD TABLE OF bapi_wrf_hier_change_items,
        lt_followupmatdata      TYPE STANDARD TABLE OF wrf_folup_typ_a,
        lt_materialdatax        TYPE STANDARD TABLE OF wrfmatdatax,

        lt_calp_vb              TYPE calp_vb_tab.


* Initialize
  ls_idno_data  = is_idno_data.
  ls_prod_data  = is_prod_data.
  ls_reg_data   = is_reg_data.
  lr_status[]   = ir_status[].
  lv_batch      = iv_batch.
  lv_ekorg_main = iv_ekorg_main.

  IF lv_ekorg_main IS INITIAL.
    lv_ekorg_main = '9999'.
  ENDIF.

*>>> IS 2019/06 decomissioning TVARVC for PIR ekorg (EINE)

  "TVARVC is replaced by config table ZARN_PIR_PUR_CNF
  "if it is not filled by any caller, then we just load
  "the record for main EKORG

  IF it_pir_ekorg[] IS INITIAL.
    SELECT * FROM zarn_pir_pur_cnf INTO TABLE lt_pir_ekorg
      WHERE ekorg = lv_ekorg_main.
  ELSE.
    lt_pir_ekorg[] = it_pir_ekorg[].
  ENDIF.

*<<< IS 2019/06 decomissioning TVARVC for PIR ekorg (EINE)



  INSERT LINES OF it_mara[]  INTO TABLE lt_mara[].
  INSERT LINES OF it_marm[]  INTO TABLE lt_marm[].
  INSERT LINES OF it_mean[]  INTO TABLE lt_mean[].
  INSERT LINES OF it_eina[]  INTO TABLE lt_eina[].
  INSERT LINES OF it_eine[]  INTO TABLE lt_eine[].
  INSERT LINES OF it_maw1[]  INTO TABLE lt_maw1[].
  INSERT LINES OF it_host_sap[] INTO TABLE lt_host_sap[].
  INSERT LINES OF it_t006[]  INTO TABLE lt_t006[].
  INSERT LINES OF it_t005[]  INTO TABLE lt_t005[].
  INSERT LINES OF it_tcurc[] INTO TABLE lt_tcurc[].
  INSERT LINES OF it_coo_t[] INTO TABLE lt_coo_t[].
  INSERT LINES OF it_gen_uom_t[] INTO TABLE lt_gen_uom_t[].
  INSERT LINES OF it_t134[]      INTO TABLE lt_t134[].
  INSERT LINES OF it_uom_cat[]   INTO TABLE lt_uom_cat[].

  " lt_tvarvc_ekorg[]    = it_tvarvc_ekorg[].
  lt_tvarvc[]          = it_tvarvc[].
  lt_tvarvc_hyb_code[] = it_tvarvc_hyb_code[].
  lt_tvta[]            = it_tvta[].
  ls_matgrp_hier       = is_matgrp_hier.
  lt_matgrp_prod[]     = it_matgrp_prod[].
  lt_art_hier[]        = it_art_hier[].


* Get Vendor details for main Purch Org for PIR
  CLEAR lt_lfm1[].
  IF ls_reg_data-zarn_reg_pir[] IS NOT INITIAL.

    lt_reg_pir_lifnr[] = ls_reg_data-zarn_reg_pir[].

    DELETE lt_reg_pir_lifnr[]
     WHERE lifnr EQ 'MULTIPLE'
        OR lifnr IS INITIAL
        OR lifnr EQ space.

    IF lt_reg_pir_lifnr[] IS NOT INITIAL.
      SELECT lifnr ekorg rdprf webre bstae           "#EC CI_SEL_NESTED
        FROM lfm1
        INTO CORRESPONDING FIELDS OF TABLE lt_lfm1
        FOR ALL ENTRIES IN lt_reg_pir_lifnr[]
        WHERE lifnr = lt_reg_pir_lifnr-lifnr
          AND ekorg = lv_ekorg_main.
    ENDIF.
  ENDIF.



  CLEAR: lv_nat_map, lv_reg_map.

* Common Mapping Data for National/Regional
  PERFORM map_common_data USING ls_idno_data
                                ls_prod_data
                                ls_reg_data
                                lt_mara[]
                                lt_t005[]
                                lt_coo_t[]
                                lr_status[]
                                ls_matgrp_hier
                       CHANGING ls_headdata
                                ls_hierarchy_data
                                lv_art_mode
                                lt_clientdata[]
                                lt_clientdatax[]
                                lt_addnlclientdata[]
                                lt_addnlclientdatax[].



* If National Data is Approved, perform mapping for National data only
  IF ls_idno_data-nat_status IN lr_status[] OR
     lr_status[] IS INITIAL                 OR
     ls_idno_data-nat_status = 'CRE'.
    lv_nat_map = abap_true.

* Map National Data
    PERFORM map_national_data USING ls_idno_data
                                    ls_prod_data
                                    ls_reg_data
                                    lv_art_mode
                                    lt_mara[]
                           CHANGING ls_headdata
                                    lt_clientext[]
                                    lt_clientextx[]
                                    ls_bapi_clientext
                                    ls_bapi_clientextx.

  ENDIF.





* If Regional Data is Approved, perform mapping for Regional data only
  IF ls_idno_data-reg_status IN lr_status[] OR lr_status[] IS INITIAL.
    lv_reg_map = abap_true.

    CLEAR ls_pir_key.
* Map Regional Data
    PERFORM map_regional_data USING ls_idno_data
                                    ls_prod_data
                                    ls_reg_data
                                    lv_art_mode
                                    lt_mara[]
                                    lt_marm[]
                                    "lt_mean[]
                                    lt_eina[]
                                    lt_eine[]
                                    lt_lfm1[]
                                    "lt_maw1[]
                                    lt_host_sap[]
                                    lt_t006[]
                                    lt_t005[]
                                    lt_tcurc[]
                                    lt_coo_t[]
                                    lt_gen_uom_t[]
                                    lt_t134[]
                                    "lt_tvarvc_ekorg[]     "IS 2019/06
                                    lt_pir_ekorg            "IS 2019/06
                                    lt_tvarvc[]
                                    lt_tvarvc_hyb_code[]
                                    lt_tvta[]
                                    lt_uom_cat[]
                                    ls_matgrp_hier
                                    lt_matgrp_prod[]
                                    lt_art_hier[]
                                    lv_ekorg_main
                           CHANGING ls_headdata
                                    lt_clientdata[]
                                    lt_clientdatax[]
                                    lt_clientext[]
                                    lt_clientextx[]
                                    ls_bapi_clientext
                                    ls_bapi_clientextx
                                    lt_addnlclientdata[]
                                    lt_addnlclientdatax[]
                                    lt_materialdescription[]
                                    lt_plantdata[]
                                    lt_plantdatax[]
                                    lt_plantext[]                   "++ONLD-822 JKH 12.01.2017
                                    lt_plantextx[]                  "++ONLD-822 JKH 12.01.2017
                                    lt_bapi_plantext[]              "++ONLD-822 JKH 12.01.2017
                                    lt_internationalartnos[]
                                    lt_taxclassifications[]
                                    lt_valuationdata[]
                                    lt_valuationdatax[]
                                    lt_warehousenumberdata[]        "++ONLD-822 JKH 12.01.2017
                                    lt_warehousenumberdatax[]       "++ONLD-822 JKH 12.01.2017
                                    lt_salesdata[]
                                    lt_salesdatax[]
                                    lt_salesext[]
                                    lt_salesextx[]
                                    ls_bapi_salesext
                                    ls_bapi_salesextx
                                    lt_bapi_salesext[]
                                    lt_posdata[]
                                    lt_posdatax[]
                                    lt_materiallongtext[]
                                    lt_plantkeys[]
                                    lt_storagelocationkeys[]
                                    lt_distrchainkeys[]
                                    lt_valuationtypekeys[]
                                    lt_importextension[]
                                    lt_legacy_struct[]
                                    lt_inforecord_general[]
                                    lt_inforecord_purchorg[]
                                    lt_eine_unit_error[]    "++IR5101117 JKH 25.08.2016
                                    lt_hierarchy_items[]
                                    lt_calp_vb[]
                                    lt_unitsofmeasure[]
                                    lt_unitsofmeasurex[]
                                    ls_pir_key.

  ENDIF.



  es_pir_key                = ls_pir_key.
  ev_nat_map                = lv_nat_map.
  ev_reg_map                = lv_reg_map.
  es_headdata               = ls_headdata.
  es_hierarchy_data         = ls_hierarchy_data.
  et_errorkeys[]            = lt_errorkeys[].
  et_variantskeys[]         = lt_variantskeys[].
  et_characteristicvalue[]  = lt_characteristicvalue[].
  et_characteristicvaluex[] = lt_characteristicvaluex[].
  et_clientdata[]           = lt_clientdata[].
  et_clientdatax[]          = lt_clientdatax[].
  et_clientext[]            = lt_clientext[].
  et_clientextx[]           = lt_clientextx[].
  et_addnlclientdata[]      = lt_addnlclientdata[].
  et_addnlclientdatax[]     = lt_addnlclientdatax[].
  et_materialdescription[]  = lt_materialdescription[].
  et_plantdata[]            = lt_plantdata[].
  et_plantdatax[]           = lt_plantdatax[].
  et_plantext[]             = lt_plantext[].
  et_plantextx[]            = lt_plantextx[].
  et_forecastparameters[]   = lt_forecastparameters[].
  et_forecastparametersx[]  = lt_forecastparametersx[].
  et_forecastvalues[]       = lt_forecastvalues[].
  et_totalconsumption[]     = lt_totalconsumption[].
  et_unplndconsumption[]    = lt_unplndconsumption[].
  et_planningdata[]         = lt_planningdata[].
  et_planningdatax[]        = lt_planningdatax[].
  et_storagelocationdata[]  = lt_storagelocationdata[].
  et_storagelocationdatax[] = lt_storagelocationdatax[].
  et_storagelocationext[]   = lt_storagelocationext[].
  et_storagelocationextx[]  = lt_storagelocationextx[].
  et_unitsofmeasure[]       = lt_unitsofmeasure[].
  et_unitsofmeasurex[]      = lt_unitsofmeasurex[].
  et_unitofmeasuretexts[]   = lt_unitofmeasuretexts[].
  et_internationalartnos[]  = lt_internationalartnos[].
  et_vendorean[]            = lt_vendorean[].
  et_layoutmoduleassgmt[]   = lt_layoutmoduleassgmt[].
  et_layoutmoduleassgmtx[]  = lt_layoutmoduleassgmtx[].
  et_taxclassifications[]   = lt_taxclassifications[].
  et_valuationdata[]        = lt_valuationdata[].
  et_valuationdatax[]       = lt_valuationdatax[].
  et_valuationext[]         = lt_valuationext[].
  et_valuationextx[]        = lt_valuationextx[].
  et_warehousenumberdata[]  = lt_warehousenumberdata[].
  et_warehousenumberdatax[] = lt_warehousenumberdatax[].
  et_warehousenumberext[]   = lt_warehousenumberext[].
  et_warehousenumberextx[]  = lt_warehousenumberextx[].
  et_storagetypedata[]      = lt_storagetypedata[].
  et_storagetypedatax[]     = lt_storagetypedatax[].
  et_storagetypeext[]       = lt_storagetypeext[].
  et_storagetypeextx[]      = lt_storagetypeextx[].
  et_salesdata[]            = lt_salesdata[].
  et_salesdatax[]           = lt_salesdatax[].
  et_salesext[]             = lt_salesext[].
  et_salesextx[]            = lt_salesextx[].
  et_posdata[]              = lt_posdata[].
  et_posdatax[]             = lt_posdatax[].
  et_posext[]               = lt_posext[].
  et_posextx[]              = lt_posextx[].
  et_materiallongtext[]     = lt_materiallongtext[].
  et_plantkeys[]            = lt_plantkeys[].
  et_storagelocationkeys[]  = lt_storagelocationkeys[].
  et_distrchainkeys[]       = lt_distrchainkeys[].
  et_warehousenokeys[]      = lt_warehousenokeys[].
  et_storagetypekeys[]      = lt_storagetypekeys[].
  et_valuationtypekeys[]    = lt_valuationtypekeys[].
  et_importextension[]      = lt_importextension[].
  et_inforecord_general[]   = lt_inforecord_general[].
  et_inforecord_purchorg[]  = lt_inforecord_purchorg[].
  et_eine_unit_error[]      = lt_eine_unit_error[].         "++IR5101117 JKH 25.08.2016
  et_source_list[]          = lt_source_list[].
  et_additionaldata[]       = lt_additionaldata[].
  et_calculationitemin[]    = lt_calculationitemin[].
  et_calculationiteminx[]   = lt_calculationiteminx[].
  et_calculationextin[]     = lt_calculationextin[].
  et_calculationitemout[]   = lt_calculationitemout[].
  et_recipientparameters[]  = lt_recipientparameters[].
  et_recipientparametersx[] = lt_recipientparametersx[].
  et_vendormatheader[]      = lt_vendormatheader[].
  et_vendormatcharvalues[]  = lt_vendormatcharvalues[].
  et_listingconditions[]    = lt_listingconditions[].
  et_bomheader[]            = lt_bomheader[].
  et_bompositions[]         = lt_bompositions[].
  et_priceconditions[]      = lt_priceconditions[].
  et_hierarchy_items[]      = lt_hierarchy_items[].
  et_followupmatdata[]      = lt_followupmatdata[].
  et_materialdatax[]        = lt_materialdatax[].
  et_calp_vb[]              = lt_calp_vb[].

  APPEND ls_bapi_clientext TO et_bapi_clientext[].
  APPEND LINES OF lt_bapi_salesext[] TO et_bapi_salesext[].
  APPEND LINES OF lt_bapi_plantext[] TO et_bapi_plantext[].              "++ONLD-822 JKH 12.01.2017

ENDFUNCTION.
