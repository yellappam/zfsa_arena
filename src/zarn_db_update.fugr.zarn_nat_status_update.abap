FUNCTION zarn_nat_status_update.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(IV_IDNO) TYPE  ZARN_IDNO
*"     VALUE(IV_VERSION) TYPE  ZARN_VERSION
*"     VALUE(IV_STATUS) TYPE  ZARN_VERSION_STATUS
*"----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Functional Specification 3162
* DATE WRITTEN     # 02112015
* SAP VERSION      # 740
* TYPE             # Function Module
* AUTHOR           # Ivo Skolek, C90001587
*-----------------------------------------------------------------------
* TITLE            # Update national version status - RFC enabled
* PURPOSE          #
*                  #
* COPIED FROM      # ZIARN_GUI_F03, form update_nat_db_status
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  DATA : ls_zarn_prd_version_old TYPE zarn_prd_version,
         ls_zarn_prd_version_new TYPE zarn_prd_version.



  DATA: lv_objectid               TYPE cdhdr-objectid,
        lv_tcode                  TYPE cdhdr-tcode,
        lv_utime                  TYPE cdhdr-utime,
        lv_udate                  TYPE cdhdr-udate,
        lv_username               TYPE cdhdr-username,
        lt_icdtxt_zarn_ctrl_regnl TYPE STANDARD TABLE OF cdtxt,
        lt_xzarn_control          TYPE STANDARD TABLE OF yzarn_control,
        lt_yzarn_control          TYPE STANDARD TABLE OF yzarn_control,
        lt_xzarn_prd_version      TYPE STANDARD TABLE OF yzarn_prd_version,
        lt_yzarn_prd_version      TYPE STANDARD TABLE OF yzarn_prd_version,
        lt_xzarn_reg_banner       TYPE STANDARD TABLE OF yzarn_reg_banner,
        lt_yzarn_reg_banner       TYPE STANDARD TABLE OF yzarn_reg_banner,
        lt_xzarn_reg_ean          TYPE STANDARD TABLE OF yzarn_reg_ean,
        lt_yzarn_reg_ean          TYPE STANDARD TABLE OF yzarn_reg_ean,
        lt_xzarn_reg_hdr          TYPE STANDARD TABLE OF yzarn_reg_hdr,
        lt_yzarn_reg_hdr          TYPE STANDARD TABLE OF yzarn_reg_hdr,
        lt_xzarn_reg_hsno         TYPE STANDARD TABLE OF yzarn_reg_hsno,
        lt_yzarn_reg_hsno         TYPE STANDARD TABLE OF yzarn_reg_hsno,
        lt_xzarn_reg_pir          TYPE STANDARD TABLE OF yzarn_reg_pir,
        lt_yzarn_reg_pir          TYPE STANDARD TABLE OF yzarn_reg_pir,
        lt_xzarn_reg_prfam        TYPE STANDARD TABLE OF yzarn_reg_prfam,
        lt_yzarn_reg_prfam        TYPE STANDARD TABLE OF yzarn_reg_prfam,
        lt_xzarn_reg_txt          TYPE STANDARD TABLE OF yzarn_reg_txt,
        lt_yzarn_reg_txt          TYPE STANDARD TABLE OF yzarn_reg_txt,
        lt_xzarn_reg_uom          TYPE STANDARD TABLE OF yzarn_reg_uom,
        lt_yzarn_reg_uom          TYPE STANDARD TABLE OF yzarn_reg_uom,
        lt_xzarn_ver_status       TYPE STANDARD TABLE OF yzarn_ver_status,
        lt_yzarn_ver_status       TYPE STANDARD TABLE OF yzarn_ver_status,
        lt_xzarn_reg_dc_sell      TYPE STANDARD TABLE OF yzarn_reg_dc_sell,
        lt_yzarn_reg_dc_sell      TYPE STANDARD TABLE OF yzarn_reg_dc_sell,
        lt_xzarn_reg_lst_prc      TYPE STANDARD TABLE OF yzarn_reg_lst_prc,
        lt_yzarn_reg_lst_prc      TYPE STANDARD TABLE OF yzarn_reg_lst_prc,
        lt_xzarn_reg_rrp          TYPE STANDARD TABLE OF yzarn_reg_rrp,
        lt_yzarn_reg_rrp          TYPE STANDARD TABLE OF yzarn_reg_rrp,
        lt_xzarn_reg_std_ter      TYPE STANDARD TABLE OF yzarn_reg_std_ter,
        lt_yzarn_reg_std_ter      TYPE STANDARD TABLE OF yzarn_reg_std_ter,
        lt_xzarn_reg_artlink      TYPE STANDARD TABLE OF yzarn_reg_artlink,
        lt_yzarn_reg_artlink      TYPE STANDARD TABLE OF yzarn_reg_artlink,
        lt_xzarn_reg_onlcat       TYPE STANDARD TABLE OF yzarn_reg_onlcat,
        lt_yzarn_reg_onlcat       TYPE STANDARD TABLE OF yzarn_reg_onlcat,
        lt_xzarn_reg_str          TYPE yzarn_reg_strs,
        lt_yzarn_reg_str          TYPE yzarn_reg_strs.



  "get current record
  SELECT SINGLE * FROM zarn_prd_version INTO  ls_zarn_prd_version_old
    WHERE idno = iv_idno
      AND version = iv_version.
  IF sy-subrc NE 0.
    "TBD error
    RETURN.
  ENDIF.

  APPEND ls_zarn_prd_version_old TO lt_yzarn_prd_version.

  ls_zarn_prd_version_new = ls_zarn_prd_version_old.

  ls_zarn_prd_version_new-version_status = iv_status.
  ls_zarn_prd_version_new-changed_on = sy-datum.
  ls_zarn_prd_version_new-changed_at = sy-uzeit.
  ls_zarn_prd_version_new-changed_by = sy-uname.
  APPEND ls_zarn_prd_version_new TO lt_xzarn_prd_version.

  lv_objectid = ls_zarn_prd_version_new-idno.
  lv_tcode    = 'ARENA_INBOUND'.
  lv_utime    = sy-uzeit.
  lv_udate    = sy-datum.
  lv_username = sy-uname.

  CALL FUNCTION 'ZARN_CTRL_REGNL_WRITE_DOC_CUST'
    EXPORTING
      objectid               = lv_objectid
      tcode                  = lv_tcode
      utime                  = lv_utime
      udate                  = lv_udate
      username               = lv_username
      upd_zarn_prd_version   = 'U'
      xzarn_reg_str          = lt_xzarn_reg_str
      yzarn_reg_str          = lt_yzarn_reg_str
    TABLES
      icdtxt_zarn_ctrl_regnl = lt_icdtxt_zarn_ctrl_regnl
      xzarn_control          = lt_xzarn_control
      yzarn_control          = lt_yzarn_control
      xzarn_prd_version      = lt_xzarn_prd_version
      yzarn_prd_version      = lt_yzarn_prd_version
      xzarn_reg_artlink      = lt_xzarn_reg_artlink
      yzarn_reg_artlink      = lt_yzarn_reg_artlink
      xzarn_reg_banner       = lt_xzarn_reg_banner
      yzarn_reg_banner       = lt_yzarn_reg_banner
      xzarn_reg_dc_sell      = lt_xzarn_reg_dc_sell
      yzarn_reg_dc_sell      = lt_yzarn_reg_dc_sell
      xzarn_reg_ean          = lt_xzarn_reg_ean
      yzarn_reg_ean          = lt_yzarn_reg_ean
      xzarn_reg_hdr          = lt_xzarn_reg_hdr
      yzarn_reg_hdr          = lt_yzarn_reg_hdr
      xzarn_reg_hsno         = lt_xzarn_reg_hsno
      yzarn_reg_hsno         = lt_yzarn_reg_hsno
      xzarn_reg_lst_prc      = lt_xzarn_reg_lst_prc
      yzarn_reg_lst_prc      = lt_yzarn_reg_lst_prc
      xzarn_reg_onlcat       = lt_xzarn_reg_onlcat
      yzarn_reg_onlcat       = lt_yzarn_reg_onlcat
      xzarn_reg_pir          = lt_xzarn_reg_pir
      yzarn_reg_pir          = lt_yzarn_reg_pir
      xzarn_reg_prfam        = lt_xzarn_reg_prfam
      yzarn_reg_prfam        = lt_yzarn_reg_prfam
      xzarn_reg_rrp          = lt_xzarn_reg_rrp
      yzarn_reg_rrp          = lt_yzarn_reg_rrp
      xzarn_reg_std_ter      = lt_xzarn_reg_std_ter
      yzarn_reg_std_ter      = lt_yzarn_reg_std_ter
      xzarn_reg_txt          = lt_xzarn_reg_txt
      yzarn_reg_txt          = lt_yzarn_reg_txt
      xzarn_reg_uom          = lt_xzarn_reg_uom
      yzarn_reg_uom          = lt_yzarn_reg_uom
      xzarn_ver_status       = lt_xzarn_ver_status
      yzarn_ver_status       = lt_yzarn_ver_status.

  UPDATE zarn_prd_version
    SET version_status = iv_status
    WHERE idno  EQ iv_idno
    AND version EQ iv_version.



ENDFUNCTION.
