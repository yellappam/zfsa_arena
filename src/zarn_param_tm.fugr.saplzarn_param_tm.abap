* regenerated at 18.10.2016 09:11:46
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_PARAM_TMTOP.                 " Global Data
  INCLUDE LZARN_PARAM_TMUXX.                 " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_PARAM_TMF...                 " Subroutines
* INCLUDE LZARN_PARAM_TMO...                 " PBO-Modules
* INCLUDE LZARN_PARAM_TMI...                 " PAI-Modules
* INCLUDE LZARN_PARAM_TME...                 " Events
* INCLUDE LZARN_PARAM_TMP...                 " Local class implement.
* INCLUDE LZARN_PARAM_TMT99.                 " ABAP Unit tests
  INCLUDE LZARN_PARAM_TMF00                       . " subprograms
  INCLUDE LZARN_PARAM_TMI00                       . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
