*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Functional Specification 3131
* DATE WRITTEN     # 01 03 2016
* SAP VERSION      # 731
* TYPE             # Function
* AUTHOR           # C90001448, Greg van der Loeff
*-----------------------------------------------------------------------
* TITLE            # AReNa: Send I-Care flag to Hybris
* PURPOSE          # AReNa: Send I-Care flag to Hybris
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 2019/05
* CHANGE No.       # CIP-621 Autoapproval issue from inbound
* DESCRIPTION      # Enabled "no commit" so that commit can be done by
*                  # by caller after all update functions are registered
*                  # Removed ASYNC possibility as it is not used
*                  # if required needs to be designed properly
* WHO              # Ivo Skolek, I90018154
*-----------------------------------------------------------------------
FUNCTION z_hybris_set.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(ICARE) TYPE  ZARN_HYBRIS_SET_FIELDS_TAB
*"     REFERENCE(IV_COMMIT) TYPE  XFELD DEFAULT 'X'
*"  EXPORTING
*"     REFERENCE(MESSAGES) TYPE  BAL_T_MSG
*"  EXCEPTIONS
*"      HYBRIS_NOT_AVAILABLE
*"----------------------------------------------------------------------

  DATA: ls_tvarvc  TYPE tvarvc,
        lo_root    TYPE REF TO cx_root,
        lv_msg     TYPE string,
        ls_message LIKE LINE OF messages.

  FIELD-SYMBOLS: <ls_icare> LIKE LINE OF icare[].

  TRY.

      LOOP AT icare ASSIGNING <ls_icare>.
        DELETE FROM zarn_get_cleanup WHERE idno EQ <ls_icare>-idno AND uname EQ sy-uname.
      ENDLOOP.

      PERFORM hybris_set USING icare
                               iv_commit. "IS 2019/05 enable no commit

      IF iv_commit EQ abap_true. "IS 2019/05 enable no commit
        COMMIT WORK AND WAIT.
      ENDIF.

    CATCH cx_root INTO lo_root.
      lv_msg = lo_root->if_message~get_text( ).
      CLEAR ls_message.
      ls_message-msgid = 'ZARENA_MSG'.
      ls_message-msgty = 'E'.
      ls_message-msgno = '000'.
      PERFORM split_msg USING lv_msg CHANGING ls_message-msgv1 ls_message-msgv2 ls_message-msgv3 ls_message-msgv4.
      APPEND ls_message TO messages.
      PERFORM log_message USING lv_msg.
      WHILE NOT lo_root->previous IS INITIAL.
        lo_root = lo_root->previous.
        lv_msg = lo_root->if_message~get_text( ).
        CLEAR ls_message.
        ls_message-msgid = 'ZARENA_MSG'.
        ls_message-msgty = 'E'.
        ls_message-msgno = '000'.
        PERFORM split_msg USING lv_msg CHANGING ls_message-msgv1 ls_message-msgv2 ls_message-msgv3 ls_message-msgv4.
        APPEND ls_message TO messages.
        PERFORM log_message USING lv_msg.
      ENDWHILE.
  ENDTRY.

ENDFUNCTION.
