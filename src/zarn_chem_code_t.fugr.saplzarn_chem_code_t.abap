* regenerated at 02.05.2016 14:18:56 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_CHEM_CODE_TTOP.              " Global Data
  INCLUDE LZARN_CHEM_CODE_TUXX.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_CHEM_CODE_TF...              " Subroutines
* INCLUDE LZARN_CHEM_CODE_TO...              " PBO-Modules
* INCLUDE LZARN_CHEM_CODE_TI...              " PAI-Modules
* INCLUDE LZARN_CHEM_CODE_TE...              " Events
* INCLUDE LZARN_CHEM_CODE_TP...              " Local class implement.
* INCLUDE LZARN_CHEM_CODE_TT99.              " ABAP Unit tests
  INCLUDE LZARN_CHEM_CODE_TF00                    . " subprograms
  INCLUDE LZARN_CHEM_CODE_TI00                    . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
