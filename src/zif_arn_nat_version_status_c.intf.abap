interface ZIF_ARN_NAT_VERSION_STATUS_C
  public .


  constants GC_APPROVED type ZARN_VERSION_STATUS value 'APR' ##NO_TEXT.
  constants GC_MIGRATED type ZARN_VERSION_STATUS value 'MIG' ##NO_TEXT.
  constants GC_ARTICLE_POSTED type ZARN_VERSION_STATUS value 'CRE' ##NO_TEXT.
endinterface.
