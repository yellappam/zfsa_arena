*-----------------------------------------------------------------------
* PROJECT          # AReNa
* SPECIFICATION    #
* DATE WRITTEN     # 26.07.2016
* SYSTEM           # DE0
* SAP VERSION      # ECC6
* TYPE             # Conversion Exit
* AUTHOR           # C90003202, Howard Chow
*-----------------------------------------------------------------------
* PURPOSE          # Conversion exit input for domain ZARN_DEC29_14_D_NEG
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------
FUNCTION CONVERSION_EXIT_ZD29N_INPUT.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(INPUT)
*"  EXPORTING
*"     REFERENCE(OUTPUT)
*"----------------------------------------------------------------------

  DATA:
    lv_output   TYPE zarn_flash_pt_temp_val.


* copy input to variable with domain ZARN_DEC29_14_D_NEG to get full decimal
  lv_output = input.
  output    = lv_output.


ENDFUNCTION.
