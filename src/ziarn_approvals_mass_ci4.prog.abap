*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_APPROVALS_MASS_CI4
*&---------------------------------------------------------------------*

CLASS lcl_appr_mass_event_handler IMPLEMENTATION.

* Link Click on Tree items
  METHOD appr_mass_handle_link_click.


    DATA: ls_output TYPE zsarn_approvals_mass_tree,
          lt_nodes  TYPE lvc_t_nkey,
          ls_nodes  TYPE lvc_s_nkey.


* Get Node details which is Clicked
    CLEAR ls_output.
    TRY.
        ls_output = gt_output[ node_key = node_key ].
      CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
    ENDTRY.



* Handle Grid Actions
    PERFORM appr_handle_link_click USING fieldname node_key.

    IF gt_log[] IS NOT INITIAL.
* Display Log
      PERFORM display_log.
    ENDIF.

* Refresh Tree
    PERFORM refresh_tree.





* Get Node details which is Clicked
    TRY.
        ls_output = gt_output[ idno         = ls_output-idno
                               chg_area     = ls_output-chg_area
                               chg_category = ls_output-chg_category
                               line_type    = 'N'
                             ].
      CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
        CLEAR ls_output.
    ENDTRY.


    IF ls_output IS NOT INITIAL.
      ls_nodes-node_key = ls_output-node_key.
      APPEND ls_nodes TO lt_nodes[].

* Set Node as Selected which is Clicked
      CALL METHOD sender->set_selected_nodes
        EXPORTING
          it_selected_nodes       = lt_nodes[]
        EXCEPTIONS
          cntl_system_error       = 1
          dp_error                = 2
          failed                  = 3
          error_in_node_key_table = 4
          OTHERS                  = 5.
    ENDIF.

  ENDMETHOD.                    " APPR_NAT_HANDLE_HOTSPOT_CLICK

  METHOD on_function_selected.

* Take action on function selected
    PERFORM function_selected USING fcode.

  ENDMETHOD.                    " ON_FUNCTION_SELECTED



  METHOD dialog_close.
    IF NOT sender IS INITIAL.
      CALL METHOD sender->free.
    ENDIF.
  ENDMETHOD.                    " DIALOG_CLOSE

ENDCLASS.                    "lcl_appr_nat_event_handler IMPLEMENTATION
