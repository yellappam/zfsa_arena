* regenerated at 25.05.2016 11:44:54 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_GROWING_TTOP.                " Global Data
  INCLUDE LZARN_GROWING_TUXX.                " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_GROWING_TF...                " Subroutines
* INCLUDE LZARN_GROWING_TO...                " PBO-Modules
* INCLUDE LZARN_GROWING_TI...                " PAI-Modules
* INCLUDE LZARN_GROWING_TE...                " Events
* INCLUDE LZARN_GROWING_TP...                " Local class implement.
* INCLUDE LZARN_GROWING_TT99.                " ABAP Unit tests
  INCLUDE LZARN_GROWING_TF00                      . " subprograms
  INCLUDE LZARN_GROWING_TI00                      . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
