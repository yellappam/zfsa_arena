* regenerated at 11.12.2018 09:43:59
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_NPD_DELTOP.                  " Global Declarations
  INCLUDE LZARN_NPD_DELUXX.                  " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_NPD_DELF...                  " Subroutines
* INCLUDE LZARN_NPD_DELO...                  " PBO-Modules
* INCLUDE LZARN_NPD_DELI...                  " PAI-Modules
* INCLUDE LZARN_NPD_DELE...                  " Events
* INCLUDE LZARN_NPD_DELP...                  " Local class implement.
* INCLUDE LZARN_NPD_DELT99.                  " ABAP Unit tests
  INCLUDE LZARN_NPD_DELF00                        . " subprograms
  INCLUDE LZARN_NPD_DELI00                        . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
