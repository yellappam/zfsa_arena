*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 25.05.2016 at 11:23:54 by user C90001363
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_PREP_TYPE_T................................*
DATA:  BEGIN OF STATUS_ZARN_PREP_TYPE_T              .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_PREP_TYPE_T              .
CONTROLS: TCTRL_ZARN_PREP_TYPE_T
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_PREP_TYPE_T              .
TABLES: ZARN_PREP_TYPE_T               .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
