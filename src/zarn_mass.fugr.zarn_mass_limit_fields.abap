FUNCTION zarn_mass_limit_fields.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  TABLES
*"      ALL_FIELDS TYPE  MASS_ALLFIELDS OPTIONAL
*"----------------------------------------------------------------------




  DATA: lt_field_confg TYPE ty_t_field_confg,
        lt_all_fields  TYPE mass_allfields,
        ls_all_fields  TYPE massdfies,

        lv_tabix       TYPE sy-tabix.




* Get Regional Mass Relevant fields only
  SELECT a~tabname a~fldname a~reg_mass_relevant
    INTO CORRESPONDING FIELDS OF TABLE lt_field_confg[]
    FROM zarn_field_confg AS a
    JOIN zarn_table_mastr AS b
    ON  a~tabname = b~arena_table
    WHERE b~arena_table_type  = 'R'     " Regional Tables only
      AND a~reg_mass_relevant = abap_true.




  CLEAR lt_all_fields[].
  LOOP AT all_fields[] INTO ls_all_fields.

    READ TABLE lt_field_confg[] TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = ls_all_fields-tabname
                   fldname = ls_all_fields-fieldname.
    IF sy-subrc = 0.
      INSERT ls_all_fields INTO TABLE lt_all_fields[].
    ENDIF.
  ENDLOOP.  " LOOP AT all_fields[] INTO ls_all_fields



  all_fields[] = lt_all_fields[].

ENDFUNCTION.
