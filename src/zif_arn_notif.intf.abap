interface ZIF_ARN_NOTIF
  public .


  methods GENERATE
    returning
      value(RV_GENERATED) type BOOLE_D
    raising
      ZCX_ARN_NOTIF_ERR .
  methods TRANSMIT
    importing
      !IV_COMMIT type ABAP_BOOL .
  methods GET_APPROVALS
    returning
      value(RT_APPROVALS) type ZARN_T_APPROVAL .
  methods GET_NOTIF_TEXT
    returning
      value(RV_TEXT) type ZARN_E_APPR_COMMENT .
  methods GET_NOTIF_PARAM
    returning
      value(RS_PARAM) type ZSARN_NOTIF_PARAM .
endinterface.
