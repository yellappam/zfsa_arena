*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 02.05.2016 at 17:33:46 by user C90001363
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_ARN_MODE_T.................................*
DATA:  BEGIN OF STATUS_ZARN_ARN_MODE_T               .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_ARN_MODE_T               .
CONTROLS: TCTRL_ZARN_ARN_MODE_T
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_ARN_MODE_T               .
TABLES: ZARN_ARN_MODE_T                .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
