*---------------------------------------------------------------------*
*    program for:   TABLEFRAME_ZARN_LINK_SCN_T
*   generation date: 18.11.2016 at 14:20:42
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
FUNCTION TABLEFRAME_ZARN_LINK_SCN_T    .

  PERFORM TABLEFRAME TABLES X_HEADER X_NAMTAB DBA_SELLIST DPL_SELLIST
                            EXCL_CUA_FUNCT
                     USING  CORR_NUMBER VIEW_ACTION VIEW_NAME.

ENDFUNCTION.
