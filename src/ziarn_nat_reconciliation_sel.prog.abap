*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_NAT_RECONCILIATION_SEL
*&---------------------------------------------------------------------*


SELECTION-SCREEN BEGIN OF BLOCK b1 WITH FRAME TITLE text-0b1.
SELECT-OPTIONS: s_fanid FOR zarn_products-fan_id NO INTERVALS OBLIGATORY.
SELECTION-SCREEN END OF BLOCK b1.








INITIALIZATION.

  PERFORM restrict_select_options.
