*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_APPROVALS_MASS_TOP
*&---------------------------------------------------------------------*

TABLES: zarn_cc_hdr, zarn_reg_hdr, zarn_prd_version.

CLASS lcl_view DEFINITION DEFERRED.
CLASS lcl_model DEFINITION DEFERRED.
CLASS lcl_controller DEFINITION DEFERRED.
CLASS lcl_appr_mass_event_handler DEFINITION DEFERRED.

CONSTANTS:
  co_appr_called_from_nat   TYPE char3 VALUE 'NAT',
  co_appr_called_from_reg   TYPE char3 VALUE 'REG',
  co_status_wip             TYPE char3 VALUE 'WIP',
  co_status_rej             TYPE char3 VALUE 'REJ',
  co_status_apr             TYPE char3 VALUE 'APR',
  co_status_cre             TYPE char3 VALUE 'CRE',

  co_appr_mode_approve      TYPE c VALUE 'A',
  co_appr_mode_reject       TYPE c VALUE 'R',
  co_appr_mode_add_text     TYPE c VALUE 'T',
  co_appr_mode_display_text TYPE c VALUE 'D',
  co_appr_result_approved   TYPE c VALUE 'A',
  co_appr_result_rejected   TYPE c VALUE 'R',
  co_appr_result_reset      TYPE c VALUE 'S',
  co_appr_result_cancelled  TYPE c VALUE 'C',
  co_appr_result_text_added TYPE c VALUE 'T',
  co_appr_column_status     TYPE char4 VALUE 'STAT',
  co_appr_column_approve    TYPE char4 VALUE 'APPR',
  co_appr_column_reject     TYPE char4 VALUE 'REJE',
  co_appr_column_text       TYPE char4 VALUE 'TEXT',


  BEGIN OF gc_fcode,
    refresh   TYPE ui_func VALUE 'REFRESH',
    excel     TYPE ui_func VALUE 'EXCEL',
    mass_appr TYPE ui_func VALUE 'MASS_APPR',
  END OF gc_fcode.


TYPES: ty_r_idno        TYPE RANGE OF zarn_reg_hdr-idno,
       ty_r_chgcat      TYPE RANGE OF zarn_cc_hdr-chg_category,
       ty_r_chgare      TYPE RANGE OF zarn_cc_hdr-chg_area,
       ty_r_nstat       TYPE RANGE OF zarn_prd_version-version_status,
       ty_r_rstat       TYPE RANGE OF zarn_reg_hdr-status,
       ty_r_aprare      TYPE RANGE OF zarn_approval-approval_area,

       ty_t_cc_hdr      TYPE SORTED TABLE OF zarn_cc_hdr  WITH NON-UNIQUE KEY idno chg_category chg_area,
       ty_t_cc_det      TYPE SORTED TABLE OF zarn_cc_det  WITH NON-UNIQUE KEY idno chg_category chg_area,
       ty_t_reg_hdr     TYPE SORTED TABLE OF zarn_reg_hdr WITH NON-UNIQUE KEY idno,
       ty_t_prd_version TYPE SORTED TABLE OF zarn_prd_version WITH UNIQUE KEY idno version,
       ty_t_cc_desc     TYPE SORTED TABLE OF zarn_cc_desc WITH UNIQUE KEY chg_category,
       ty_t_approval    TYPE SORTED TABLE OF zarn_approval WITH NON-UNIQUE KEY idno approval_area chg_category,
       ty_t_appr_ch     TYPE STANDARD TABLE OF zarn_approval_ch,
       ty_t_appr_output TYPE STANDARD TABLE OF zarn_approvals_fields,
       ty_t_rel_matrix  TYPE STANDARD TABLE OF zarn_rel_matrix,

* National Data
       ty_s_prod_data   TYPE zsarn_prod_data,
       ty_t_prod_data   TYPE ztarn_prod_data,

* Regional Data
       ty_s_reg_data    TYPE zsarn_reg_data,
       ty_t_reg_data    TYPE ztarn_reg_data,

       BEGIN OF ty_s_appr_hdr,
         idno          TYPE zarn_idno,
         chg_area      TYPE zarn_chg_area,
         chg_category  TYPE zarn_chg_category,
         approval_area TYPE zarn_e_appr_area,
       END OF ty_s_appr_hdr,
       ty_t_appr_hdr TYPE SORTED TABLE OF ty_s_appr_hdr WITH NON-UNIQUE KEY idno chg_area chg_category,

       BEGIN OF ty_s_ver_data,
         idno             TYPE zarn_idno,
         version_status   TYPE zarn_version_status,
         previous_ver     TYPE zarn_previous_ver,
         current_ver      TYPE zarn_current_ver,
         latest_ver       TYPE zarn_latest_ver,
         article_ver      TYPE zarn_article_ver,
         fan_id           TYPE zarn_fan_id,
         fsni_icare_value TYPE zarn_fsni_icare_value,
       END OF ty_s_ver_data,
       ty_t_ver_data    TYPE SORTED TABLE OF ty_s_ver_data WITH NON-UNIQUE KEY idno,

       ty_t_table_mastr TYPE SORTED TABLE OF zarn_table_mastr WITH UNIQUE KEY arena_table,
       ty_t_field_confg TYPE SORTED TABLE OF zarn_field_confg WITH UNIQUE KEY tabname fldname,



       BEGIN OF ty_cc_comment,
         idno         TYPE zarn_cc_log-idno,
         chg_category TYPE zarn_cc_log-chg_category,
         team_code    TYPE zarn_cc_log-team_code,
         comment      TYPE zarn_cc_log-approval_comment,
       END OF ty_cc_comment,

       BEGIN OF ty_s_line,
         line(3500) TYPE c,
       END OF ty_s_line,
       ty_t_line TYPE STANDARD TABLE OF ty_s_line,

       BEGIN OF ty_s_idno_appr,
         idno                          TYPE zarn_idno,
         chg_area                      TYPE zarn_chg_area,
         chg_category                  TYPE zarn_chg_category,
         go_approval                   TYPE REF TO zcl_arn_approval_backend,
         gt_appr_nat_approvals         TYPE ztarn_approvals_fields,
         gv_appr_nat_approvals         TYPE flag,
         gt_appr_reg_approvals         TYPE ztarn_approvals_fields,
         gv_appr_reg_approvals         TYPE flag,
         gt_approval_teams             TYPE zarn_t_teams,
         gt_approval_comments          TYPE SORTED TABLE OF zarn_cc_log WITH NON-UNIQUE KEY chg_category team_code,
         gv_appr_nat_status            TYPE zarn_ver_status,
         gv_appr_reg_status            TYPE zarn_reg_status,
         gv_appr_nat_release_status    TYPE zarn_rel_status,
         gv_appr_nat_release_statust   TYPE zarn_release_status_desc,
         gv_appr_reg_release_status    TYPE zarn_rel_status,
         gv_appr_reg_release_statust   TYPE zarn_release_status_desc,
         gv_appr_nat_is_approved       TYPE flag,
         gv_appr_national_not_approved TYPE text120,

***      gv_approval_data              TYPE abap_bool,
         gv_appr_mode                  TYPE c,
         gv_appr_result                TYPE c,
         gv_appr_comment               TYPE string,
         gv_appr_rejection_reason      TYPE zarn_rejection_reason,
       END OF ty_s_idno_appr,
       ty_t_idno_appr TYPE SORTED TABLE OF ty_s_idno_appr WITH NON-UNIQUE KEY idno chg_area chg_category.












DATA: gt_idno_appr               TYPE ty_t_idno_appr,
      go_appr_mass_event_handler TYPE REF TO lcl_appr_mass_event_handler,
      go_approval                TYPE REF TO zcl_arn_approval_backend,
*      go_dialogbox_container     TYPE REF TO cl_gui_dialogbox_container,
*      go_log_alv                 TYPE REF TO cl_salv_table,

      go_appr_editor_container   TYPE REF TO cl_gui_custom_container,
      go_appr_text_editor        TYPE REF TO cl_gui_textedit,

      gv_display                 TYPE xfeld,
      gt_appr_nat_fieldcat       TYPE lvc_t_fcat,
      gt_appr_reg_fieldcat       TYPE lvc_t_fcat,
      gt_output                  TYPE ztarn_approvals_mass_tree,
      gt_reg_hdr                 TYPE ty_t_reg_hdr,
      gt_prd_version             TYPE ty_t_prd_version,
      gt_ver_data                TYPE ty_t_ver_data,
      gt_log                     TYPE ztarn_approvals_mass_log,
      gt_val_output              TYPE zarn_t_rl_output,

      gt_prod_data               TYPE ty_t_prod_data,
      gt_reg_data                TYPE ty_t_reg_data,
      gt_prod_data_o             TYPE ty_t_prod_data,
      gt_reg_data_o              TYPE ty_t_reg_data.



************************************************************************
* OLE objects Declarations
************************************************************************
DATA: w_excel      TYPE ole2_object,
      w_workbook   TYPE ole2_object,
      w_worksheet  TYPE ole2_object,
      w_columns    TYPE ole2_object,
      w_column_ent TYPE ole2_object,
      w_cell       TYPE ole2_object,
      w_int        TYPE ole2_object,
      w_range      TYPE ole2_object.



FIELD-SYMBOLS: <gs_idno_appr> TYPE ty_s_idno_appr.
