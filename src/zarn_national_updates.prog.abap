*&---------------------------------------------------------------------*
*& Report  ZARN_DATA_RECONCILIATION
*&
*&---------------------------------------------------------------------*
*&---------------------------------------------------------------------*
* PROJECT          # FSA
* SPECIFICATION    # B-03406
* DATE WRITTEN     # 23.11.2018
* SYSTEM           # DE0
* SAP VERSION      # ECC750
* TYPE             # Report
* AUTHOR           # C90002903
*-----------------------------------------------------------------------
* TITLE            # National Updates Report
* PURPOSE          # AS an Arena User
*                  # I WANT to identify articles that have had a recent PIM update to National attributes
*                  # SO THAT I can subsequently edit the Regional attribute appropriately
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      #
*-----------------------------------------------------------------------
* CALLS TO         #
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

REPORT zarn_national_updates MESSAGE-ID zarena_msg.

*--------------------------------------------------------------------*
TABLES: zarn_products, zarn_prd_version, mara.
RANGES: lr_date FOR zarn_products-fsni_last_trans_date.
DATA: go_alvgrid_9001   TYPE REF TO cl_gui_alv_grid,
      go_container_9001 TYPE REF TO cl_gui_custom_container.
DATA: go_alvgrid_9002   TYPE REF TO cl_gui_alv_grid,
      go_container_9002 TYPE REF TO cl_gui_custom_container.

DATA: gt_products TYPE TABLE OF zsarn_prod_upd.
DATA: gt_prod_attr TYPE TABLE OF zsarn_attr_comp.
DATA: gt_reg_hdr  TYPE TABLE OF zarn_reg_hdr.


CONSTANTS: BEGIN OF c_ts_recon,
             tab1 LIKE sy-ucomm VALUE 'TS_FAN',
             tab2 LIKE sy-ucomm VALUE 'TS_BASIC1',
           END OF c_ts_recon.

CONTROLS:  ts_recon TYPE TABSTRIP.
DATA: BEGIN OF g_ts_recon,
        subscreen   LIKE sy-dynnr,
        prog        LIKE sy-repid VALUE 'ZARN_NATIONAL_UPDATES',
        pressed_tab LIKE sy-ucomm VALUE c_ts_recon-tab1,
      END OF g_ts_recon.
DATA:      gv_okcode LIKE sy-ucomm.
DATA gt_rows_9001 TYPE lvc_t_row.
*--------------------------------------------------------------------*
SELECTION-SCREEN BEGIN OF BLOCK b1 WITH FRAME TITLE TEXT-001.

SELECT-OPTIONS: s_date      FOR sy-datum OBLIGATORY NO-EXTENSION.
SELECT-OPTIONS: s_fan       FOR zarn_products-fan_id.
SELECT-OPTIONS: s_vers      FOR zarn_prd_version-version_status.

SELECTION-SCREEN SKIP.

PARAMETERS: p_diff AS CHECKBOX.

SELECTION-SCREEN END OF BLOCK b1.

*--------------------------------------------------------------------*
START-OF-SELECTION.

* Initialize
  PERFORM init.

  CALL SCREEN 9000.

*--------------------------------------------------------------------*
  INCLUDE zarn_national_updates_subr.
  INCLUDE zarn_national_updates_mod.
