interface ZIF_ARN_UOM_CATEGORY_C
  public .


  constants GC_INNER type ZARN_UOM_CATEGORY value 'INNER' ##NO_TEXT.
  constants GC_LAYER type ZARN_UOM_CATEGORY value 'LAYER' ##NO_TEXT.
  constants GC_PALLET type ZARN_UOM_CATEGORY value 'PALLET' ##NO_TEXT.
  constants GC_RETAIL type ZARN_UOM_CATEGORY value 'RETAIL' ##NO_TEXT.
  constants GC_SHIPPER type ZARN_UOM_CATEGORY value 'SHIPPER' ##NO_TEXT.
endinterface.
