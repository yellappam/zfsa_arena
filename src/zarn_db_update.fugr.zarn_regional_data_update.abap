*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Functional Specification 3125
* DATE WRITTEN     # 07 03 2016
* SAP VERSION      # 731
* TYPE             # Function Module
* AUTHOR           # C90002769, Rajesh Anumolu
*-----------------------------------------------------------------------
* TITLE            # Update data into DB
* PURPOSE          # Regional Data Update into DB
* COPIED FROM      # ZARN_NATIONAL_DATA_UPDATE
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------
FUNCTION zarn_regional_data_update.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(IT_REG_DATA) TYPE  ZTARN_REG_DATA
*"     VALUE(IT_REG_DATA_DEL) TYPE  ZTARN_REG_DATA OPTIONAL
*"----------------------------------------------------------------------


  DATA: ls_reg_data       TYPE zsarn_reg_data,
        ls_reg_data_upd   TYPE zsarn_reg_data,
        ls_reg_data_del   TYPE zsarn_reg_data,
        lv_itabname       TYPE char50,
        lt_dfies          TYPE dfies_tab,
        ls_dfies          TYPE dfies.

  FIELD-SYMBOLS: <table>              TYPE table,
                 <table_upd>          TYPE table,
                 <table_str>          TYPE any,
                 <table_val>          TYPE any.


* Get Fields of the table
  REFRESH: lt_dfies[].
  CALL FUNCTION 'DDIF_FIELDINFO_GET'
    EXPORTING
      tabname        = 'ZSARN_REG_DATA'
      langu          = sy-langu
      all_types      = abap_true
    TABLES
      dfies_tab      = lt_dfies[]
    EXCEPTIONS
      not_found      = 1
      internal_error = 2
      OTHERS         = 3.
  IF sy-subrc <> 0.
    EXIT.
  ENDIF.

**-----------------------------------------------------------------------------*
**-----------------------------------------------------------------------------*
** DELETION
**-----------------------------------------------------------------------------*
**-----------------------------------------------------------------------------*
* For each table -
* Collect the regional table data for all proucts into one table
* so as to delete frome DB tables at once for all products
  IF it_reg_data_del[] IS NOT INITIAL.
    CLEAR ls_reg_data.
    LOOP AT it_reg_data_del[] INTO ls_reg_data.
      LOOP AT lt_dfies[] INTO ls_dfies.

        IF ls_dfies-fieldname = 'IDNO' OR ls_dfies-fieldname = 'VERSION'.
          CONTINUE.
        ENDIF.

        IF <table>      IS ASSIGNED. UNASSIGN <table>.       ENDIF.
        IF <table_upd>  IS ASSIGNED. UNASSIGN <table_upd>.   ENDIF.

        CLEAR: lv_itabname.
        CONCATENATE 'LS_REG_DATA-' ls_dfies-fieldname INTO lv_itabname.
        ASSIGN (lv_itabname) TO <table>.

        CLEAR: lv_itabname.
        CONCATENATE 'LS_REG_DATA_DEL-' ls_dfies-fieldname INTO lv_itabname.
        ASSIGN (lv_itabname) TO <table_upd>.


        APPEND LINES OF <table> TO <table_upd>.

      ENDLOOP.  " LOOP AT lt_dfies[] INTO ls_dfies.
    ENDLOOP.  " LOOP AT it_REG_data[] INTO ls_REG_data



* Delete Regional Data
    LOOP AT lt_dfies[] INTO ls_dfies.

      IF ls_dfies-fieldname = 'IDNO' OR ls_dfies-fieldname = 'VERSION'.
        CONTINUE.
      ENDIF.

      IF <table_upd>  IS ASSIGNED. UNASSIGN <table_upd>.   ENDIF.

      CLEAR: lv_itabname.
      CONCATENATE 'LS_REG_DATA_DEL-' ls_dfies-fieldname INTO lv_itabname.
      ASSIGN (lv_itabname) TO <table_upd>.

      IF <table_upd> IS NOT INITIAL.
        DELETE (ls_dfies-fieldname) FROM TABLE <table_upd>. "#EC CI_IMUD_NESTED
      ENDIF.

    ENDLOOP.  " LOOP AT lt_dfies[] INTO ls_dfies.
  ENDIF.

**-----------------------------------------------------------------------------*
**-----------------------------------------------------------------------------*
** MODIFY
**-----------------------------------------------------------------------------*
**-----------------------------------------------------------------------------*

* For each table -
* Collect the regional table data for all proucts into one table
* so as to Modify DB tables at once for all products
  CLEAR: ls_reg_data,
         ls_reg_data_upd.

  LOOP AT it_reg_data[] INTO ls_reg_data.
    LOOP AT lt_dfies[] INTO ls_dfies.

      IF ls_dfies-fieldname = 'IDNO' OR ls_dfies-fieldname = 'VERSION'.
        CONTINUE.
      ENDIF.

      IF <table>      IS ASSIGNED. UNASSIGN <table>.       ENDIF.
      IF <table_upd>  IS ASSIGNED. UNASSIGN <table_upd>.   ENDIF.

      CLEAR: lv_itabname.
      CONCATENATE 'LS_REG_DATA-' ls_dfies-fieldname INTO lv_itabname.
      ASSIGN (lv_itabname) TO <table>.

      CLEAR: lv_itabname.
      CONCATENATE 'LS_REG_DATA_UPD-' ls_dfies-fieldname INTO lv_itabname.
      ASSIGN (lv_itabname) TO <table_upd>.


      APPEND LINES OF <table> TO <table_upd>.

    ENDLOOP.  " LOOP AT lt_dfies[] INTO ls_dfies.
  ENDLOOP.  " LOOP AT it_REG_data[] INTO ls_REG_data



* Update Regional Data
  LOOP AT lt_dfies[] INTO ls_dfies.

    IF ls_dfies-fieldname = 'IDNO' OR ls_dfies-fieldname = 'VERSION'.
      CONTINUE.
    ENDIF.

    IF <table_upd>  IS ASSIGNED. UNASSIGN <table_upd>.   ENDIF.

    CLEAR: lv_itabname.
    CONCATENATE 'LS_REG_DATA_UPD-' ls_dfies-fieldname INTO lv_itabname.
    ASSIGN (lv_itabname) TO <table_upd>.

    IF <table_upd> IS NOT INITIAL.
      MODIFY (ls_dfies-fieldname) FROM TABLE <table_upd>.   "#EC CI_IMUD_NESTED
    ENDIF.

  ENDLOOP.  " LOOP AT lt_dfies[] INTO ls_dfies.





ENDFUNCTION.   "#EC CI_VALPAR
