*&---------------------------------------------------------------------*
*&  Include           ZIARN_GUI_O01
*&---------------------------------------------------------------------*
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 13.04.2018
* CHANGE No.       # Charm 9000003482; Jira CI18-554
* DESCRIPTION      # CI18-554 Recipe Management - Ingredient Changes
*                  # New regional table for Allergen Type overrides and
*                  # fields for Ingredients statement overrides in Arena.
*                  # New Ingredients Type field in the Scales block in
*                  # Arena and NONSAP tab in material master.
* WHO              # C90005557, Tessa Newman
*-----------------------------------------------------------------------
*&---------------------------------------------------------------------*
*&      Module  STATUS_0100  OUTPUT
*&---------------------------------------------------------------------*

MODULE status_0100 OUTPUT.

  DATA lt_ucomm TYPE TABLE OF sy-ucomm.
  DATA ls_ucomm TYPE sy-ucomm.

  FREE lt_ucomm[].

  IF zarn_reg_hdr-idno IS INITIAL.
    ls_ucomm = 'BACKTOLIST'.
    APPEND ls_ucomm TO lt_ucomm.
    ls_ucomm = 'DISPLAY'.
    APPEND ls_ucomm TO lt_ucomm.
    ls_ucomm = 'ENRICH'.
    APPEND ls_ucomm TO lt_ucomm.
  ENDIF.

  IF gv_row_total LE 1.
    ls_ucomm = 'PREV'.
    APPEND ls_ucomm TO lt_ucomm.
    CLEAR: ls_ucomm.
    ls_ucomm = 'NEXT'.
    APPEND ls_ucomm TO lt_ucomm.
    CLEAR: ls_ucomm.
  ELSEIF gv_row_total GT 1 AND gv_row_index = 1.
    ls_ucomm = 'PREV'.
    APPEND ls_ucomm TO lt_ucomm.
    CLEAR: ls_ucomm.
  ELSEIF gv_row_total GT 1 AND gv_row_index = gv_row_total.
    ls_ucomm = 'NEXT'.
    APPEND ls_ucomm TO lt_ucomm.
    CLEAR: ls_ucomm.
  ENDIF.

  "if display only, dont show ENRICH button
  IF gv_auth_display_only = abap_true.
    ls_ucomm = 'ENRICH'.
    COLLECT ls_ucomm INTO lt_ucomm.
  ENDIF.

  IF  gv_sel_tab EQ gc_icare_tab.
*   ICare Status
    SET PF-STATUS 'STATUS_0100_ICARE' EXCLUDING lt_ucomm.
*   ICare Title
    SET TITLEBAR '100_ICARE'.
  ELSE.

    IF gs_worklist-rowcolor EQ gc_alv_locked OR
       gv_display           IS NOT INITIAL.

      SET PF-STATUS 'STATUS_0100_DISP' EXCLUDING lt_ucomm.
    ELSE.
      SET PF-STATUS 'STATUS_0100' EXCLUDING lt_ucomm.
    ENDIF.

*   Title with current article
    SET TITLEBAR '100'
            WITH zarn_products-matnr_ni
                 zarn_products-name.
  ENDIF.

ENDMODULE.                 " STATUS_0100  OUTPUT
*&---------------------------------------------------------------------*
*&      Module  STATUS_0110  OUTPUT
*&---------------------------------------------------------------------*

MODULE screen_controls_0110 OUTPUT.

  PERFORM screen_controls.

ENDMODULE.                 " STATUS_0110  OUTPUT

*&---------------------------------------------------------------------*
*&      Module  CONTROL_LOAD_0102  OUTPUT
*&---------------------------------------------------------------------*

MODULE control_load_0102 OUTPUT.

  PERFORM reg_uom_alv.
  PERFORM reg_pf_alv.
  PERFORM reg_pir_alv.
  PERFORM reg_gtn_alv.
  PERFORM reg_con_alv_rrp.  "03
  PERFORM reg_con_alv_dis.  "02

  PERFORM reg_hsno_alv.                  "++3162 - dngrs goods - JKH

  PERFORM reg_art_link_alv.              " ++ONED-217 JKH 11.11.2016

  PERFORM reg_onncat_alv.                "++ONLD-835 JKH 12.05.2017

  PERFORM reg_allerg_ovr_alv.

  PERFORM reg_sc_alv.                    "9000004661 Supply Chain Tab

ENDMODULE.                 " CONTROL_LOAD_0102  OUTPUT
*&---------------------------------------------------------------------*
*&      Module  SCREEN_ATTRIBUTES_0101  OUTPUT
*&---------------------------------------------------------------------*

MODULE screen_attributes_0101 OUTPUT.

  PERFORM screen_when_icare.

  PERFORM screen_attributes_general.

  PERFORM appr_highlight_logic.

  PERFORM default_data_201.
  PERFORM default_data_203.


ENDMODULE.                 " SCREEN_ATTRIBUTES_0101  OUTPUT
*&---------------------------------------------------------------------*
*&      Module  SCREEN_ATTRIBUTES_0102  OUTPUT
*&---------------------------------------------------------------------*

MODULE screen_attributes_0102 OUTPUT.

  PERFORM screen_when_icare.

* Check Lock
  READ TABLE gt_worklist INTO gs_worklist
       WITH KEY  idno = zarn_products-idno.

  IF sy-subrc             IS NOT INITIAL   OR
     gs_worklist-rowcolor EQ gc_alv_locked OR
     gv_display           IS NOT INITIAL.
*   Display only Logic
    PERFORM screen_display_logic.

*   set ALV output only
    CLEAR gv_edit_toggle.  " Display mode

  ELSE.
* Field selection logic
    PERFORM screen_field_sel_logic.
    gv_edit_toggle = 1.  " Edit mode
  ENDIF.

  PERFORM check_online_tab_active.

* Call Regional ALV controls EDIT/DISPLAY
  CALL METHOD go_reg_uom_alv->set_ready_for_input      "UOM
    EXPORTING
      i_ready_for_input = gv_edit_toggle.

  CALL METHOD go_reg_con_rrp_alv->set_ready_for_input  "RRP
    EXPORTING
      i_ready_for_input = gv_edit_toggle.

  CALL METHOD go_reg_con_dis_alv->set_ready_for_input  "Discount
    EXPORTING
      i_ready_for_input = gv_edit_toggle.

  CALL METHOD go_reg_gtn_alv->set_ready_for_input      "GTIN
    EXPORTING
      i_ready_for_input = gv_edit_toggle.

  CALL METHOD go_reg_pir_alv->set_ready_for_input      "PIR
    EXPORTING
      i_ready_for_input = gv_edit_toggle.

  CALL METHOD go_reg_pf_alv->set_ready_for_input       "Price Family
    EXPORTING
      i_ready_for_input = gv_edit_toggle.

  CALL METHOD go_reg_con_artlink_alv->set_ready_for_input      "Articl Linking
    EXPORTING
      i_ready_for_input = gv_edit_toggle.                      " ++ONED-217 JKH 23.11.2016

  CALL METHOD go_reg_con_onlcat_alv->set_ready_for_input      "Online Category
    EXPORTING
      i_ready_for_input = gv_edit_toggle.                      "++ONLD-835 JKH 12.05.2017

  CALL METHOD go_reg_allerg_alv->set_ready_for_input      "Allergen and Ingredient override
    EXPORTING
      i_ready_for_input = gv_edit_toggle.

  CALL METHOD go_reg_sc_alv->set_ready_for_input      "900000466 Supply Chain
    EXPORTING
      i_ready_for_input = gv_edit_toggle.

  PERFORM appr_highlight_logic.

  PERFORM text_editor_prfam. " text editor for Price family   "++ONLD-835 JKH 27.04.2017

  PERFORM text_descr_prfam. " text editor for Descriptions and Ingredient statement

ENDMODULE.                 " SCREEN_ATTRIBUTES_0102  OUTPUT

*&---------------------------------------------------------------------*
*&      Module  TEXT_EDITOR  OUTPUT
*&---------------------------------------------------------------------*

MODULE text_editor OUTPUT.
  PERFORM text_editor.
ENDMODULE.                 " TEXT_EDITOR  OUTPUT
*&---------------------------------------------------------------------*
*&      Module  SCREEN_DISPLAY_0102  OUTPUT
*&---------------------------------------------------------------------*

MODULE screen_text_display_0102 OUTPUT.

* product hierarchy text  to display on screen
  PERFORM read_prod_hierarchy
    USING zarn_reg_hdr-prdha.   "uct_hierarchy.

ENDMODULE.                 " SCREEN_DISPLAY_0102  OUTPUT

*&---------------------------------------------------------------------*
*&      Module  STATUS_0700  OUTPUT
*&---------------------------------------------------------------------*
MODULE status_0700 OUTPUT.


  IF g_ts_main-pressed_tab = c_ts_main-tab1. " TS_MAIN_FC1 - NAT TAB
    PERFORM approvals_build_grid2 USING co_appr_called_from_nat.
  ENDIF.

ENDMODULE.                 " STATUS_0700  OUTPUT

*&---------------------------------------------------------------------*
*&      Module  STATUS_0701  OUTPUT
*&---------------------------------------------------------------------*
MODULE status_0701 OUTPUT.

  IF g_ts_main-pressed_tab = c_ts_main-tab2. " TS_MAIN_FC2 - REG TAB
    PERFORM approvals_build_grid2 USING co_appr_called_from_reg.
  ENDIF.

  LOOP AT SCREEN.
    IF  screen-name EQ 'BUT_APPR_TOGGLE_EDIT'
    AND gv_display EQ abap_false.
      screen-invisible = 1.
      MODIFY SCREEN.
    ENDIF.
    IF gv_appr_nat_is_approved EQ abap_true.
      IF screen-name EQ 'GV_APPR_NATIONAL_NOT_APPROVED'.
        screen-invisible = 1.
        MODIFY SCREEN.
      ENDIF.
    ELSE.
      IF screen-name EQ 'BUT_APPR_APPROVE'
      OR screen-name EQ 'BUT_APPR_REJECT'.
        screen-input = 0.
        MODIFY SCREEN.
      ENDIF.
    ENDIF.
  ENDLOOP.

  " for reg. approval set intial cursor to status to avoid screen movement
  IF gv_reg_pb_00 EQ icon_collapse AND g_ts_main-pressed_tab = 'TS_MAIN_FC2'
    AND gv_appr_syucomm EQ 'RPB00'.

    CLEAR gv_appr_syucomm.
    SET CURSOR FIELD 'GV_APPR_REG_STATUS'.
  ENDIF.

ENDMODULE.                 " STATUS_0701  OUTPUT

*&---------------------------------------------------------------------*
*&      Module  STATUS_0703  OUTPUT
*&---------------------------------------------------------------------*
MODULE status_0703 OUTPUT.

  PERFORM appr_set_status.

  PERFORM appr_populate_drop_down.

  PERFORM appr_create_text_edit.

ENDMODULE.                 " STATUS_0703  OUTPUT
*&---------------------------------------------------------------------*
*&      Module  SCREEN_DISPLAY_LOGIC_0111  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE screen_display_logic_0111 OUTPUT.

** DEL Begin of Change ONLD-835 JKH 27.04.2017
*  LOOP AT SCREEN INTO gs_screen.
*    IF gs_screen-name EQ 'GV_PRFAM_DONE_TEXT'.
**        IF zarn_reg_prfam-prfam_done_flag = abap_true.
**          WRITE icon_locked TO gv_prfam_done_text.
**          gv_prfam_done_text = gv_prfam_done_text && text-039. "Done
**
**        ENDIF.
*      IF gv_prfam_done_text EQ text-039 OR gv_display IS INITIAL.
*        gs_screen-input = 1.
*      ELSE.
*        gs_screen-input = 0.
*      ENDIF.
*
*      MODIFY screen FROM gs_screen.
*    ENDIF.
*  ENDLOOP.
** DEL End of Change ONLD-835 JKH 27.04.2017


ENDMODULE.                 " SCREEN_DISPLAY_LOGIC_0111  OUTPUT
*&---------------------------------------------------------------------*
*&      Module  TEXT_EDITOR_PRFAM  OUTPUT
*&---------------------------------------------------------------------*

MODULE text_editor_prfam OUTPUT.
  PERFORM text_editor_prfam. " text editor for Price family
ENDMODULE.                 " TEXT_EDITOR_PRFAM  OUTPUT
*&---------------------------------------------------------------------*
*&      Module  Screen_display_logic_0113  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE  screen_display_logic_0113 OUTPUT.
*
*  IF zarn_reg_hdr-matnr IS INITIAL.
*    LOOP AT SCREEN INTO gs_screen.
*      IF gs_screen-name EQ 'RPB_003'.
*        gs_screen-input = 0.
*        MODIFY screen FROM gs_screen.
*        EXIT.
*      ENDIF.
*    ENDLOOP.
*  ENDIF.



  IF zarn_reg_hdr-host_product = abap_true.
    LOOP AT SCREEN INTO gs_screen.
      IF gs_screen-name CS 'ZARN_REG_HDR-HOST_PRODUCT'.
        gs_screen-input = 0.
        MODIFY SCREEN FROM gs_screen.
        EXIT.
      ENDIF.
    ENDLOOP.
  ENDIF.




ENDMODULE.                 "  Screen_display_logic_0113  OUTPUT
*&---------------------------------------------------------------------*
*&      Module  STATUS_0708  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE status_0708 OUTPUT.
*  SET PF-STATUS 'xxxxxxxx'.
*  SET TITLEBAR 'xxx'.
  PERFORM alv_comm_channel.
  PERFORM screen_field_sel_logic.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  STATUS_0710  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE status_0710 OUTPUT.

*  SET PF-STATUS 'PF_710'.
  PERFORM alv_ins_code.
  PERFORM alv_cdt_code.
  PERFORM screen_field_sel_logic.
ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  STATUS_0712  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE status_0712 OUTPUT.
*  SET PF-STATUS 'xxxxxxxx'.
*  SET TITLEBAR 'xxx'.
  PERFORM alv_prep_type.
  PERFORM alv_growing.
  PERFORM screen_field_sel_logic.
ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  STATUS_0714  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE status_0714 OUTPUT.
*  SET PF-STATUS 'xxxxxxxx'.
*  SET TITLEBAR 'xxx'.
  PERFORM alv_nutri_claim.
  PERFORM alv_nutri_star.
  PERFORM nat_serve_size_alv.
  PERFORM screen_field_sel_logic.
ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  STATUS_0716  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE status_0716 OUTPUT.
*  SET PF-STATUS 'xxxxxxxx'.
*  SET TITLEBAR 'xxx'.
  PERFORM alv_addit_info.
  PERFORM screen_field_sel_logic.
ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  STATUS_0718  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE status_0718 OUTPUT.
*  SET PF-STATUS 'xxxxxxxx'.
*  SET TITLEBAR 'xxx'.
  PERFORM alv_hsno.
  PERFORM screen_field_sel_logic.
ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  STATUS_9002  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE status_9001 OUTPUT.

  SET TITLEBAR 'CHANGE_NON_SAP_UOM'.
  SET PF-STATUS 'DUMMY'.

  SET CURSOR FIELD 'CANCEL'.

  PERFORM approve_button_auth.

  PERFORM disp_open_po_alv.

  PERFORM disp_open_stock_alv.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  SCREEN_DISPLAY_LOGIC_112  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE screen_display_logic_112 OUTPUT.

* INS Begin of Change ONLD-835 JKH 27.04.2017
  LOOP AT SCREEN INTO gs_screen.
    IF gs_screen-name EQ 'GV_PRFAM_DONE_TEXT'.
      IF gv_prfam_done_text EQ TEXT-039 OR gv_display IS INITIAL.
        gs_screen-input = 1.
      ELSE.
        gs_screen-input = 0.
      ENDIF.

      MODIFY SCREEN FROM gs_screen.
    ENDIF.

    IF gs_screen-group1 = 'CLU' AND lcl_reg_cluster_ui=>get_instance( )->is_toggle_framework_active( ) = abap_false.
      gs_screen-invisible = 1.
      MODIFY SCREEN FROM gs_screen.
    ENDIF.

  ENDLOOP.
* INS End of Change ONLD-835 JKH 27.04.2017

  PERFORM determine_reg_cluster_screen.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  GET_VENDOR_TEXT_111  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE get_vendor_text_111 OUTPUT.


  IF zarn_reg_pir-lifnr IS INITIAL.
    CLEAR gs_text-name1.
  ENDIF.

  SELECT SINGLE name1 INTO gs_text-name1 FROM lfa1
         WHERE lifnr = zarn_reg_pir-lifnr.
  IF sy-subrc IS NOT INITIAL.
    CLEAR gs_text-name1.
  ENDIF.


ENDMODULE.

*&---------------------------------------------------------------------*
*&      Module  display_text_115  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE display_text_115 OUTPUT.

* Issue Unit (Display next to sales units)
  READ TABLE gt_zarn_reg_uom INTO DATA(ls_zarn_reg_uom)
  WITH KEY issue_unit = abap_true.
  IF sy-subrc = 0.
    gs_text-issue_unit = ls_zarn_reg_uom-meinh.
  ELSE.
    CLEAR gs_text-issue_unit.
  ENDIF.
* Order Unit (Display next to Initial Order Units)
  READ TABLE gt_zarn_reg_uom INTO ls_zarn_reg_uom
  WITH KEY unit_purord = abap_true.
  IF sy-subrc = 0.
    gs_text-order_unit = ls_zarn_reg_uom-meinh.
  ELSE.
    CLEAR gs_text-order_unit.
  ENDIF.

* Reference Material Description
  IF zarn_reg_sc-ref_matnr IS INITIAL.
    CLEAR gs_text-maktx.
  ENDIF.

  SELECT SINGLE maktx INTO gs_text-maktx FROM makt
         WHERE matnr = zarn_reg_sc-ref_matnr.
  IF sy-subrc IS NOT INITIAL.
    CLEAR gs_text-maktx.
  ENDIF.


ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  DEFAULT_FIELDS_111  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE default_fields_111 OUTPUT.

  IF gv_display IS INITIAL.
    PERFORM default_brand_id_111.
  ENDIF.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  GET_BRAND_ID_DESC_111  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE get_brand_id_desc_111 OUTPUT.

  CLEAR gv_brand_id_desc.
  SELECT SINGLE brand_descr
    FROM wrf_brands_t
    INTO gv_brand_id_desc
    WHERE brand_id = zarn_reg_hdr-brand_id
      AND language = sy-langu.

ENDMODULE.
*//START insert Charm 9000003206
MODULE set_days_111 OUTPUT.
  CHECK gv_display IS INITIAL.
  LOOP AT SCREEN INTO gs_screen.
    CASE gs_screen-name.
      WHEN 'ZARN_REG_HDR-ZZSELL' OR 'ZARN_REG_HDR-ZZUSE'.
        IF zarn_reg_hdr-zzbbdays_bypass EQ abap_true.
          gs_screen-input = 0.
        ELSE.
          gs_screen-input = 1.
        ENDIF.
        MODIFY SCREEN FROM gs_screen.
    ENDCASE.
  ENDLOOP.


ENDMODULE.
*//END insert Charm 9000003206

MODULE status_0114 OUTPUT.
*  SET PF-STATUS 'xxxxxxxx'.
*  SET TITLEBAR 'xxx'.
  zarn_reg_hdr-onl_pres_height = zcl_onl_arena=>get_online_dimension( EXPORTING  iv_article = zarn_reg_hdr-matnr
                                                                  iv_idno    = zarn_reg_hdr-idno
                                                                  iv_onl_pres_height_ddn = zarn_reg_hdr-onl_pres_height_ddn
                                                                  iv_onl_pres_height = zarn_reg_hdr-onl_pres_height ).

  CREATE OBJECT lo_report.
  lo_report->generate_html_button( ).

  PERFORM reg_online_alv.

  IF zarn_reg_hdr-onl_pres_height_ddn NE 'O'.
    LOOP AT SCREEN.
      IF screen-name = 'ZARN_REG_HDR-ONL_PRES_HEIGHT'.
        screen-input = 0.
        MODIFY SCREEN.
      ENDIF.
    ENDLOOP.
  ENDIF.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  STATUS_0720  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE status_0720 OUTPUT.
*  SET PF-STATUS 'xxxxxxxx'.
*  SET TITLEBAR 'xxx'.

*  PERFORM default_data_201.
  PERFORM screen_field_sel_logic.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  STATUS_0115  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE status_0115 OUTPUT.


ENDMODULE.
*&---------------------------------------------------------------------*
*&      Form  REG_SC_ALV
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM reg_sc_alv .

  DATA:
    lt_fieldcat    TYPE lvc_t_fcat,
    ls_fieldcat    TYPE lvc_s_fcat,
    lt_exclude     TYPE ui_functions,
    ls_layout      TYPE lvc_s_layo,
    ls_variant     TYPE disvariant,
    lt_filter      TYPE lvc_t_filt,
    ls_filter      TYPE lvc_s_filt,
    lv_edit_switch TYPE boole_d.

  IF go_reg_sc_alv IS INITIAL.
*   Regional UOM
    CREATE OBJECT go_rsccc09
      EXPORTING
        container_name = gc_rsccc09.

* Create ALV grid
    CREATE OBJECT go_reg_sc_alv
      EXPORTING
        i_parent = go_rsccc09.

    PERFORM alv_build_fieldcat   USING  gc_alv_reg_sc CHANGING lt_fieldcat.

*   Exclude all edit functions in this example since we do not need them:
    PERFORM exclude_tb_functions_edit USING  gc_alv_reg_sc CHANGING lt_exclude.


*   Variant management
    ls_variant-handle   = 'RSC'.

    PERFORM alv_variant_default CHANGING ls_variant.

*    ls_layout-edit       = abap_true.
*    ls_layout-no_rowins  = abap_true.   " keyboard selections
*    ls_layout-cwidth_opt = abap_true.
    ls_layout-sel_mode   = abap_true.
    ls_layout-zebra      = abap_true.
*    ls_layout-stylefname = gc_celltab.

    CALL METHOD go_reg_sc_alv->set_table_for_first_display
      EXPORTING
        i_bypassing_buffer            = abap_true
        is_variant                    = ls_variant
        i_save                        = gc_save
        is_layout                     = ls_layout
        it_toolbar_excluding          = lt_exclude
      CHANGING
        it_outtab                     = gt_zarn_reg_uom[]
        it_fieldcatalog               = lt_fieldcat
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.
    IF sy-subrc IS NOT INITIAL.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

* set editable cells to ready for input
    IF lv_edit_switch = abap_true.
      CALL METHOD go_reg_sc_alv->set_ready_for_input
        EXPORTING
          i_ready_for_input = 1.
    ENDIF.

    CALL METHOD go_reg_sc_alv->register_edit_event
      EXPORTING
        i_event_id = cl_gui_alv_grid=>mc_evt_enter.

    CALL METHOD go_reg_sc_alv->register_edit_event
      EXPORTING
        i_event_id = cl_gui_alv_grid=>mc_evt_modified.


    SET HANDLER  go_event_receiver_edit->handle_data_changed FOR go_reg_sc_alv.
*    SET HANDLER  go_event_receiver_edit->handle_hotspot_click FOR go_reg_sc_alv.

    SET HANDLER go_event_receiver_edit->handle_toolbar FOR go_reg_sc_alv.
    SET HANDLER go_event_receiver_edit->handle_user_command FOR go_reg_sc_alv.

*-- Register F4 event
*    PERFORM register_f4_event_uom_alv USING go_reg_sc_alv.
*    PERFORM register_f4_event USING go_reg_uom_alv gc_hyb_code.

  ELSE.
** Set Filter to not show blank MEINH by default
*    CALL METHOD go_reg_uom_alv->set_filter_criteria
*      EXPORTING
*        it_filter = gt_filter_uom[].

    PERFORM refresh_reg_alv USING go_reg_sc_alv lt_fieldcat.

  ENDIF.
ENDFORM.

MODULE status_0722 OUTPUT.
*  SET PF-STATUS 'xxxxxxxx'.
*  SET TITLEBAR 'xxx'.

  PERFORM screen_field_sel_logic.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  PBO_0114  OUTPUT
*&---------------------------------------------------------------------*
* Online
*----------------------------------------------------------------------*
MODULE pbo_0114 OUTPUT.
  PERFORM banner_and_cluster_alv.
ENDMODULE.
