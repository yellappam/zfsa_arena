FUNCTION zarn_get_nat_lower_child_uom.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(IV_IDNO) TYPE  ZARN_IDNO
*"     REFERENCE(IV_VERSION) TYPE  ZARN_VERSION
*"     REFERENCE(IV_UOM_CATEGORY) TYPE  ZARN_UOM_CATEGORY DEFAULT
*"       'RETAIL'
*"     REFERENCE(IV_UOM_CODE) TYPE  ZARN_UOM_CODE
*"     REFERENCE(IV_HYBRIS_CODE) TYPE  ZARN_HYBRIS_INTERNAL_CODE
*"       DEFAULT SPACE
*"     REFERENCE(IT_UOM_VARIANT) TYPE  ZTARN_UOM_VARIANT OPTIONAL
*"     REFERENCE(IT_GTIN_VAR) TYPE  ZTARN_GTIN_VAR OPTIONAL
*"     REFERENCE(IT_PIR) TYPE  ZTARN_PIR OPTIONAL
*"  EXPORTING
*"     REFERENCE(EV_LOWER_UOM) TYPE  ZARN_LOWER_UOM
*"     REFERENCE(EV_LOWER_CHILD_UOM) TYPE  ZARN_LOWER_CHILD_UOM
*"  EXCEPTIONS
*"      INPUT_REQD
*"----------------------------------------------------------------------

  DATA: lv_idno            TYPE zarn_idno,
        lv_version         TYPE zarn_version,
        lv_uom_category    TYPE zarn_uom_category,
        lv_uom_code        TYPE zarn_uom_code,
        lv_hybris_code     TYPE zarn_hybris_internal_code,
        lt_uom_variant     TYPE ztarn_uom_variant,
        lt_gtin_var        TYPE ztarn_gtin_var,
        lt_pir             TYPE ztarn_pir,

        ls_uom_variant     TYPE zarn_uom_variant,
        ls_gtin_var        TYPE zarn_gtin_var,
        ls_pir             TYPE zarn_pir,

        lv_lower_uom       TYPE zarn_lower_uom,
        lv_lower_child_uom TYPE zarn_lower_child_uom.



  lv_idno           = iv_idno.
  lv_version        = iv_version.
  lv_uom_category   = iv_uom_category.
  lv_uom_code       = iv_uom_code.
  lv_hybris_code    = iv_hybris_code.
  lt_uom_variant[]  = it_uom_variant[].
  lt_gtin_var[]     = it_gtin_var[].
  lt_pir[]          = it_pir[].



  IF lv_idno         IS INITIAL OR
     lv_version      IS INITIAL OR
     lv_uom_category IS INITIAL.
    RAISE input_reqd.
  ENDIF.

  TRANSLATE lv_uom_category TO UPPER CASE.

  IF lv_uom_category = gc_uom_cat-layer OR lv_uom_category = gc_uom_cat-pallet.
    IF lv_hybris_code IS INITIAL.
      RAISE input_reqd.
    ENDIF.
  ENDIF.


  IF lt_uom_variant[] IS INITIAL.
    SELECT child_gtin idno uom_code version FROM zarn_uom_variant
      INTO CORRESPONDING FIELDS OF TABLE lt_uom_variant[]
     WHERE idno    = lv_idno
       AND version = lv_version.
  ENDIF.

  IF lt_gtin_var[] IS INITIAL.
    SELECT * FROM zarn_gtin_var
      INTO TABLE lt_gtin_var[]
     WHERE idno    = lv_idno
       AND version = lv_version.
  ENDIF.


  CLEAR : lv_lower_uom, lv_lower_child_uom.




  CASE lv_uom_category.

    WHEN gc_uom_cat-inner OR gc_uom_cat-shipper OR gc_uom_cat-retail.

      CLEAR ls_uom_variant.
      READ TABLE lt_uom_variant[] INTO ls_uom_variant
      WITH KEY idno     = lv_idno
               version  = lv_version
               uom_code = lv_uom_code.
      IF sy-subrc = 0 AND ls_uom_variant-child_gtin IS NOT INITIAL.

        CLEAR ls_gtin_var.
        READ TABLE lt_gtin_var[] INTO ls_gtin_var
        WITH KEY idno      = lv_idno
                 version   = lv_version
                 gtin_code = ls_uom_variant-child_gtin.
        IF sy-subrc = 0.
* Lower UOM
          lv_lower_uom = ls_gtin_var-uom_code.



          "" Process same for lower child uom

          CLEAR ls_uom_variant.
          READ TABLE lt_uom_variant[] INTO ls_uom_variant
          WITH KEY idno     = lv_idno
                   version  = lv_version
                   uom_code = ls_gtin_var-uom_code.
          IF sy-subrc = 0 AND ls_uom_variant-child_gtin IS NOT INITIAL.
            CLEAR ls_gtin_var.
            READ TABLE lt_gtin_var[] INTO ls_gtin_var
            WITH KEY idno      = lv_idno
                     version   = lv_version
                     gtin_code = ls_uom_variant-child_gtin.
            IF sy-subrc = 0.
* Lower Child UOM
              lv_lower_child_uom = ls_gtin_var-uom_code.
            ENDIF.  " READ TABLE lt_gtin_var[] INTO ls_gtin_var - lower child

          ENDIF.  " READ TABLE lt_uom_variant[] INTO ls_uom_variant - lower child
        ENDIF.   " READ TABLE lt_gtin_var[] INTO ls_gtin_var - lower
      ENDIF.  " READ TABLE lt_uom_variant[] INTO ls_uom_variant - lower



    WHEN gc_uom_cat-layer OR gc_uom_cat-pallet.


      IF lt_pir[] IS INITIAL.
        SELECT * FROM zarn_pir
          INTO TABLE lt_pir[]
          WHERE idno                 = lv_idno
            AND version              = lv_version
            AND uom_code             = lv_uom_code
            AND hybris_internal_code = lv_hybris_code.
      ENDIF.


      CLEAR ls_pir.
      READ TABLE lt_pir[] INTO ls_pir
      WITH KEY idno                  = lv_idno
               version               = lv_version
               uom_code              = lv_uom_code
               hybris_internal_code  = lv_hybris_code.
      IF sy-subrc = 0.

        IF ls_pir-qty_per_pallet_layer  IS NOT INITIAL OR
           ls_pir-qty_layers_per_pallet IS NOT INITIAL OR
           ls_pir-qty_units_per_pallet  IS NOT INITIAL.

          "" Process same for lower child uom

          CLEAR ls_uom_variant.
          READ TABLE lt_uom_variant[] INTO ls_uom_variant
          WITH KEY idno     = lv_idno
                   version  = lv_version
                   uom_code = lv_uom_code.
          IF sy-subrc = 0 AND ls_uom_variant-child_gtin IS NOT INITIAL.
            CLEAR ls_gtin_var.
            READ TABLE lt_gtin_var[] INTO ls_gtin_var
            WITH KEY idno      = lv_idno
                     version   = lv_version
                     gtin_code = ls_uom_variant-child_gtin.
            IF sy-subrc = 0.
* Lower Child UOM
              lv_lower_child_uom = ls_gtin_var-uom_code.
            ENDIF.  " READ TABLE lt_gtin_var[] INTO ls_gtin_var - lower child
          ENDIF.  " READ TABLE lt_uom_variant[] INTO ls_uom_variant - lower child
        ENDIF.  " ZARN_PIR Qty check - lower child

      ENDIF.    " READ TABLE lt_pir[] INTO ls_pir - lower child

  ENDCASE.

  ev_lower_uom       = lv_lower_uom.
  ev_lower_child_uom = lv_lower_child_uom.

ENDFUNCTION.
