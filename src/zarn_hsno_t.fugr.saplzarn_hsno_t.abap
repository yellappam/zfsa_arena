* regenerated at 05.05.2016 14:43:31 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_HSNO_TTOP.                   " Global Data
  INCLUDE LZARN_HSNO_TUXX.                   " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_HSNO_TF...                   " Subroutines
* INCLUDE LZARN_HSNO_TO...                   " PBO-Modules
* INCLUDE LZARN_HSNO_TI...                   " PAI-Modules
* INCLUDE LZARN_HSNO_TE...                   " Events
* INCLUDE LZARN_HSNO_TP...                   " Local class implement.
* INCLUDE LZARN_HSNO_TT99.                   " ABAP Unit tests
  INCLUDE LZARN_HSNO_TF00                         . " subprograms
  INCLUDE LZARN_HSNO_TI00                         . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
