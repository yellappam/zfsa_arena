class ZCL_ARN_PIR_UTILS definition
  public
  final
  create public .

public section.

  methods IS_EKORG_RELEVANT
    importing
      !IV_EKORG type EKORG
      !IS_REG_HDR type ZARN_REG_HDR
    returning
      value(RV_RELEVANT) type ABAP_BOOL .
  methods DEFAULT_FIELDS_EKORG
    importing
      !IV_LIFNR type LIFNR
      !IV_EKORG type EKORG
      !IV_BASE_UOM type MEINS
    changing
      !CS_INFOREC_PURCHORG type WRFBAPIEINE .
  PROTECTED SECTION.
private section.

  class-data:
    st_deliver_toler_config TYPE STANDARD TABLE OF zarn_pir_dlv_tol .

  methods DEFAULT_DELIV_TOLERANCE
    importing
      !IV_TOLERANCE_TYPE type ZMD_E_DELIV_TOLER_TYPE
      !IV_EKORG type EKORG
      !IV_BASE_UOM type MEINS
    changing
      !CV_TOLERANCE type UNTTO .
ENDCLASS.



CLASS ZCL_ARN_PIR_UTILS IMPLEMENTATION.


  METHOD default_deliv_tolerance.

    LOOP AT st_deliver_toler_config ASSIGNING FIELD-SYMBOL(<ls_criteria>)
                                WHERE ekorg = iv_ekorg
                                      AND deliv_toler_type = iv_tolerance_type.

      "take first criteria which is met

      CASE <ls_criteria>-criteria_type.

        WHEN 'BUOM'. "check that base unit is equal

          IF iv_base_uom = <ls_criteria>-criteria_value.
            cv_tolerance = <ls_criteria>-tolerance_value.
            RETURN.
          ENDIF.

        WHEN space. "fallback value
          cv_tolerance = <ls_criteria>-tolerance_value.
          RETURN.

      ENDCASE.

    ENDLOOP.

  ENDMETHOD.


  METHOD  default_fields_ekorg.

    SELECT SINGLE rdprf, webre, bstae, ekgrp, plifz FROM lfm1
    INTO @DATA(ls_lfm1)
        WHERE lifnr = @iv_lifnr
          AND ekorg = @iv_ekorg
          and loevm = @space.
    IF sy-subrc EQ 0.
      cs_inforec_purchorg-round_prof = ls_lfm1-rdprf.
      cs_inforec_purchorg-gr_basediv = ls_lfm1-webre.
      cs_inforec_purchorg-conf_ctrl = ls_lfm1-bstae.
      cs_inforec_purchorg-pur_group = ls_lfm1-ekgrp.
      cs_inforec_purchorg-plnd_delry  = ls_lfm1-plifz.
    ENDIF.

    "default over/under delivery tolerance
    IF st_deliver_toler_config[] IS INITIAL.
      SELECT * FROM zarn_pir_dlv_tol INTO TABLE st_deliver_toler_config.
      SORT st_deliver_toler_config BY ekorg deliv_toler_type priority ASCENDING.
    ENDIF.

    default_deliv_tolerance( EXPORTING iv_tolerance_type = 'UDT'
                                       iv_ekorg          = iv_ekorg
                                       iv_base_uom       = iv_base_uom
                             CHANGING cv_tolerance = cs_inforec_purchorg-under_tol ).

    default_deliv_tolerance( EXPORTING iv_tolerance_type = 'ODT'
                                       iv_ekorg          = iv_ekorg
                                       iv_base_uom       = iv_base_uom
                             CHANGING cv_tolerance = cs_inforec_purchorg-overdeltol ).


  ENDMETHOD.


  METHOD is_ekorg_relevant.

    CLEAR rv_relevant.

    CASE iv_ekorg.

      WHEN  zif_purchase_org_c=>gc_fsni.
        "for 1000 it is relevant only of SOS = DC and there is a record in TVARVC ZWMS_JDA_LIVE_SITES
        IF is_reg_hdr-bwscl = zif_supply_source_c=>gc_stock_transfer.

          SELECT COUNT(*) FROM tvarvc WHERE name = 'ZWMS_JDA_LIVE_SITES'
                                        AND low NE space.
          IF sy-subrc EQ 0.
            rv_relevant = abap_true.
          ENDIF.

        ENDIF.
    ENDCASE.

  ENDMETHOD.
ENDCLASS.
