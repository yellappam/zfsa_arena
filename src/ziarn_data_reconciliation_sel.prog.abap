*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_DATA_RECONCILIATION_SEL
*&---------------------------------------------------------------------*

TABLES: zarn_products.


SELECTION-SCREEN BEGIN OF BLOCK b1 WITH FRAME TITLE text-001.

SELECT-OPTIONS: s_fan       FOR zarn_products-fan_id NO INTERVALS OBLIGATORY.

SELECTION-SCREEN SKIP.

PARAMETERS: p_ecc RADIOBUTTON GROUP rad,
            p_dsp RADIOBUTTON GROUP rad.

SELECTION-SCREEN END OF BLOCK b1.
