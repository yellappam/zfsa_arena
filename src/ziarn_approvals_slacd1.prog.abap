*&---------------------------------------------------------------------*
*&  Include           ZIARN_APPROVALS_SLACD1
*&---------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*&       Class lcl_approvals_sla
*&---------------------------------------------------------------------*
*        Text
*----------------------------------------------------------------------*
CLASS lcl_approvals_sla DEFINITION.

*---------------------------------------------------------------------*
*       Public
*---------------------------------------------------------------------*
  PUBLIC SECTION.

    TYPES: BEGIN OF ty_control,
             pwa       TYPE abap_bool,
             awa       TYPE abap_bool,
             report(1) TYPE c,
           END   OF ty_control.

    TYPES: BEGIN OF ty_selections,
             fanid         TYPE RANGE OF zarn_fan_id,
             idno          TYPE RANGE OF zarn_idno,
             catman        TYPE RANGE OF zarn_catman,
             wfca          TYPE RANGE OF datum,
             wfca_ts       TYPE RANGE OF zarn_e_appr_upd_tmstmp,
             new           TYPE abap_bool,
             chg           TYPE abap_bool,
             nat           TYPE abap_bool,
             reg           TYPE abap_bool,
             approval_area TYPE RANGE OF zarn_e_appr_area,
             matnr         TYPE RANGE OF matnr,
             team_code     TYPE RANGE OF zarn_team_code,
             ccat          TYPE RANGE OF zarn_chg_category,
             status        TYPE RANGE OF zarn_e_appr_status,
             initiation    TYPE RANGE OF zarn_initiation,
             lifnr         TYPE RANGE OF elifn,
           END OF ty_selections.

    TYPES: BEGIN OF ty_result,
             idno            TYPE zarn_idno,
             fan_id          TYPE zarn_fan_id,
             version         TYPE zarn_version,
             description     TYPE zarn_description,
             approval_area   TYPE zarn_e_appr_area,
             matnr           TYPE matnr,
             matkl           TYPE matkl,
             wgbez60         TYPE wgbez60,
             ret_zzcatman    TYPE zarn_catman_ret,
             gil_zzcatman    TYPE zarn_catman_gil,
             initiation      TYPE zarn_initiation,
             process_type    TYPE zarn_process_type,
             team_code       TYPE zarn_team_code,
             chg_category    TYPE zarn_chg_category,
             ar_timestamp    TYPE zarn_ar_timestamp,
             ap_timestamp    TYPE zarn_ap_timestamp,
             days_to_approve TYPE zarn_approval_days,
             guid            TYPE zarn_e_appr_guid,
             posted_on       TYPE zarn_posting_date,
             posted_at       TYPE zarn_posting_time,
           END OF ty_result.

    TYPES: BEGIN OF ty_output,
             idno              TYPE zarn_idno,
             fan_id            TYPE zarn_fan_id,
             version           TYPE zarn_version,
             approval_area     TYPE zarn_e_appr_area,
             description       TYPE zarn_description,
             matnr             TYPE matnr,
             matkl             TYPE matkl,
             wgbez60           TYPE wgbez60,
             ret_zzcatman      TYPE zarn_catman_ret,
             ret_zzcatman_name TYPE zmd_e_cat_name,
             gil_zzcatman      TYPE zarn_catman_gil,
             gil_zzcatman_name TYPE zmd_e_cat_name,
             initiation        TYPE zarn_initiation,
             process_type      TYPE zarn_process_type,
             team_code         TYPE zarn_team_code,
             chg_category      TYPE zarn_chg_category,
             ar_timestamp      TYPE zarn_ar_timestamp,
             ap_timestamp      TYPE zarn_ap_timestamp,
             sla_date          TYPE zarn_sla_date,
             days_to_approve   TYPE zarn_approval_days,
             sla_target_days   TYPE zarn_sla_days,
             sla_remaining     TYPE zarn_sla_days_remaining,
             guid              TYPE zarn_e_appr_guid,
             posted_on         TYPE zarn_posting_date,
             posted_at         TYPE zarn_posting_time,
           END OF ty_output.

    TYPES: BEGIN OF ty_history,
             idno              TYPE zarn_idno,
             nat_version       TYPE zarn_e_appr_nat_vers,
             team_code         TYPE zarn_team_code,
             chg_category      TYPE zarn_chg_cat,
             updated_timestamp TYPE zarn_e_appr_upd_tmstmp,
           END OF ty_history.

    TYPES: BEGIN OF ty_s_matnr,
             matnr TYPE matnr_d,
           END OF ty_s_matnr.

    TYPES: ty_t_matnr TYPE TABLE OF ty_s_matnr.

    CONSTANTS: BEGIN OF c_proc_type,
                 change TYPE zarn_process_type VALUE 'CHG',
                 new    TYPE zarn_process_type VALUE 'NEW',
               END OF c_proc_type.

    CONSTANTS: BEGIN OF c_report,
                 pending  TYPE char1 VALUE '1',
                 approved TYPE char1 VALUE '2',
                 product  TYPE char1 VALUE '3',
                 vendor   TYPE char1 VALUE '4',
               END OF c_report.

    CLASS-METHODS:
      get_default_wf_created_date
        RETURNING VALUE(rv_date) TYPE datum.

    METHODS:
      constructor
        IMPORTING is_selections TYPE ty_selections
                  is_control    TYPE ty_control,
      run
        RETURNING VALUE(ro_result) TYPE REF TO data,
      run_pending
        RETURNING VALUE(ro_result) TYPE REF TO data,
      run_approved
        RETURNING VALUE(ro_result) TYPE REF TO data,
      run_product
        RETURNING VALUE(ro_result) TYPE REF TO data,
      run_vendor
        RETURNING VALUE(ro_result) TYPE REF TO data,
      write_log
        IMPORTING iv_type    TYPE bapi_mtype OPTIONAL
                  iv_id      TYPE symsgid OPTIONAL
                  iv_number  TYPE symsgno OPTIONAL
                  iv_message TYPE bapi_msg OPTIONAL,
      display
        IMPORTING io_result TYPE REF TO data,
      query.


*---------------------------------------------------------------------*
*       Private
*---------------------------------------------------------------------*
  PRIVATE SECTION.


    DATA: mt_result                 TYPE STANDARD TABLE OF ty_result,
          mt_output                 TYPE STANDARD TABLE OF ty_output,
          mt_item_output            TYPE ztarn_sla_item_ui,
          mt_product_output         TYPE ztarn_sla_po_ui,
          mt_vendor_output          TYPE ztarn_sla_vo_ui,                                 "++JKH 3142 04.04.2017
          mt_category               TYPE SORTED TABLE OF zmd_category WITH UNIQUE KEY role_id,
          mt_mcat_text              TYPE SORTED TABLE OF t023t WITH NON-UNIQUE KEY matkl,
          mt_lifnr_text             TYPE SORTED TABLE OF lfa1 WITH UNIQUE KEY lifnr,       "++JKH 3142 04.04.2017
          mt_tech_cols              TYPE fieldname_tab,
          mt_history_ar             TYPE SORTED TABLE OF ty_history WITH NON-UNIQUE KEY idno nat_version team_code chg_category updated_timestamp,
          mt_history_po             TYPE SORTED TABLE OF ty_history WITH NON-UNIQUE KEY idno nat_version updated_timestamp,  "++JKH 3142 04.04.2017
          mt_history_ar_po          TYPE SORTED TABLE OF ty_history WITH NON-UNIQUE KEY idno nat_version updated_timestamp,  "++JKH 3142 04.04.2017
          mt_history_chg_ar         TYPE SORTED TABLE OF ty_history WITH NON-UNIQUE KEY idno nat_version updated_timestamp,  "++JKH 3142 04.04.2017
          mt_article_create_history TYPE ztt_arn_article_created,
          mt_article_update_history TYPE ztt_arn_article_created.                          "++JKH 3142 04.04.2017

    DATA: mo_result_alv TYPE REF TO cl_salv_table.

    DATA: ms_control    TYPE ty_control,
          ms_selections TYPE ty_selections.

    DATA: mv_factory_calendar TYPE wfcid.


    METHODS:
      prepare_display
        IMPORTING io_result TYPE REF TO data,
      prepare_item_output
        IMPORTING it_sla_items     TYPE ztarn_sla_item
        RETURNING VALUE(rt_output) TYPE ztarn_sla_item_ui,
      prepare_product_output
        IMPORTING it_product_items TYPE ztarn_sla_po
        RETURNING VALUE(rt_output) TYPE ztarn_sla_po_ui,
      prepare_vendor_output        IMPORTING it_vendor_items  TYPE ztarn_sla_vo       "++JKH 3142 04.04.2017
                                   RETURNING VALUE(rt_output) TYPE ztarn_sla_vo_ui,
      build_vendor_output          RETURNING VALUE(rt_output) TYPE ztarn_sla_vo_ui,      "++JKH 3142B 06.06.2017


      preprocess,
      postprocess_query,
      filter_results,
      prepare_selections,
      load_result_texts,
      load_catman,
      load_mcat_texts
        IMPORTING it_item_data TYPE ztarn_sla_item,
      load_lifnr_texts
        IMPORTING it_vend_data TYPE ztarn_sla_vo,                                    "++JKH 3142 04.04.2017
      get_article_create_history
        IMPORTING it_item_data TYPE ztarn_sla_item,
      get_ar_history_data
        IMPORTING it_item_data TYPE ztarn_sla_item,
      get_po_history_data
        IMPORTING it_item_data TYPE ztarn_sla_item,                                    "++JKH 3142 04.04.2017
      get_ar_po_history_data
        IMPORTING it_item_data TYPE ztarn_sla_item,                                    "++JKH 3142 04.04.2017
      get_chg_ar_history_data
        IMPORTING it_item_data TYPE ztarn_sla_item,                                    "++JKH 3142 04.04.2017
      determine_process_type
        IMPORTING is_result              TYPE zsarn_sla_item
        RETURNING VALUE(rv_process_type) TYPE zarn_process_type,
      determine_earliest_ar
        IMPORTING is_result       TYPE zsarn_sla_item
        RETURNING VALUE(rv_ar_ts) TYPE zarn_e_appr_upd_tmstmp,
      determine_earliest_ar_po                                                         "++JKH 3142 04.04.2017
        IMPORTING is_result       TYPE zsarn_sla_item
        RETURNING VALUE(rv_ar_ts) TYPE zarn_e_appr_upd_tmstmp,
      determine_earliest_chg                                                           "++JKH 3142 04.04.2017
        IMPORTING is_result       TYPE zsarn_sla_item
        RETURNING VALUE(rv_ar_ts) TYPE zarn_e_appr_upd_tmstmp,
      get_awa_data
        RETURNING VALUE(rt_approved_data) TYPE ztarn_sla_item,
      get_pwa_data
        RETURNING VALUE(rt_pending_data) TYPE ztarn_sla_item,
      get_post_data
        RETURNING VALUE(rt_posted_data) TYPE ztarn_sla_item,      "++JKH 3142 04.04.2017
      calc_duration
        IMPORTING it_sla_items        TYPE ztarn_sla_item
        RETURNING VALUE(rt_sla_items) TYPE ztarn_sla_item,
      calc_duration_vendor
        IMPORTING it_sla_items        TYPE ztarn_sla_item
        RETURNING VALUE(rt_sla_items) TYPE ztarn_sla_item,        "++JKH 3142 04.04.2017
      calc_sla
        CHANGING ct_sla_items TYPE ztarn_sla_item,
      calc_product_items
        IMPORTING it_pending_items   TYPE ztarn_sla_item
                  it_approved_items  TYPE ztarn_sla_item
        RETURNING VALUE(rt_po_items) TYPE ztarn_sla_po,
      calc_vendor_items
        IMPORTING it_posted_items    TYPE ztarn_sla_item          "++JKH 3142 04.04.2017
        RETURNING VALUE(rt_vo_items) TYPE ztarn_sla_vo,
      calc_backlog_stat
        IMPORTING it_pending_items TYPE ztarn_sla_item
        CHANGING  ct_po_items      TYPE ztarn_sla_po,
      calc_sla_stat
        IMPORTING it_approved_items TYPE ztarn_sla_item
        CHANGING  ct_po_items       TYPE ztarn_sla_po,
      adjust_results
        IMPORTING iv_scrub_ccat TYPE abap_bool OPTIONAL
        CHANGING  ct_sla_items  TYPE ztarn_sla_item,
      setup_alv
        IMPORTING io_result TYPE REF TO data,
      hide_tech_columns
        IMPORTING io_columns TYPE REF TO cl_salv_columns_table,
      get_tech_columns
        RETURNING VALUE(rt_tech_columns) TYPE fieldname_tab,
      get_list_header
        RETURNING VALUE(rv_list_header) TYPE string.


ENDCLASS.
