*&---------------------------------------------------------------------*
*&  Include           ZARN_REG_SYNCUP_SCR
*&---------------------------------------------------------------------*
SELECTION-SCREEN PUSHBUTTON 1(26) text-002 USER-COMMAND btn.

SELECTION-SCREEN: BEGIN OF BLOCK sel1 WITH FRAME TITLE text-001.
PARAMETER     : p_i_main TYPE rlgrap-filename.
PARAMETER     : p_i_uom  TYPE rlgrap-filename.
PARAMETER     : p_i_pir  TYPE rlgrap-filename.
SELECTION-SCREEN BEGIN OF LINE.
PARAMETERS    : p_migrtn TYPE char1 AS CHECKBOX. " DEFAULT 'X'.    " --3169 JKH 20.10.2016
SELECTION-SCREEN COMMENT 2(60) text-008 FOR FIELD p_migrtn.
SELECTION-SCREEN END OF LINE.
SELECTION-SCREEN: END OF BLOCK sel1.

**In the INITIALIZATION event, set migration to read only
INITIALIZATION.
* DEL Begin of Change 3169 JKH 26.10.2016
*  LOOP AT SCREEN.
*    IF screen-name = 'P_MIGRTN'.
*      screen-input = '0'.
*      MODIFY SCREEN.
*      EXIT.
*    ENDIF.
*  ENDLOOP.
* DEL End of Change 3169 JKH 26.10.2016



*--------------------------------------------------------------------*
* at selection screen ON VALUE-REQUEST
*--------------------------------------------------------------------*
AT SELECTION-SCREEN ON VALUE-REQUEST FOR p_i_main.
*** Get F4 for Input File Name
  PERFORM file_open_dialog CHANGING p_i_main.

AT SELECTION-SCREEN ON VALUE-REQUEST FOR p_i_pir.
*** Get F4 for Input File Name
  PERFORM file_open_dialog CHANGING p_i_pir.

AT SELECTION-SCREEN ON VALUE-REQUEST FOR p_i_uom.
*** Get F4 for Input File Name
  PERFORM file_open_dialog CHANGING p_i_uom.

*--------------------------------------------------------------------*
* at selection screen
*--------------------------------------------------------------------*
AT SELECTION-SCREEN.
  gv_fcode = sy-ucomm.
  IF gv_fcode EQ 'BTN'.

* INS Begin of Change 3169 JKH 01.11.2016
    CLEAR gv_name_text.
    PERFORM lock_syncup CHANGING gv_name_text.

    IF gv_name_text IS NOT INITIAL.
      " Customizing of Sync Fields is locked by &
      MESSAGE s076(zarena_msg) WITH gv_name_text DISPLAY LIKE 'E'.
      CALL TRANSACTION 'ZARN_REG_SYNC_DISP'.
      CLEAR gv_fcode.
    ELSE.
      CALL TRANSACTION 'ZARN_REG_SYNC'.
      CLEAR gv_fcode.
    ENDIF.
* INS End of Change 3169 JKH 01.11.2016

* DEL Begin of Change 3169 JKH 01.11.2016
*      CALL TRANSACTION 'ZARN_REG_SYNC'.
*      CLEAR gv_fcode.
* DEL End of Change 3169 JKH 01.11.2016

  ENDIF.

** Check for mandatory fields
  PERFORM check_mandatory_fields.

*--------------------------------------------------------------------*
* start of selection
*--------------------------------------------------------------------*
START-OF-SELECTION.
  " get data from excel file/SAP tables and execute upload logic to fill regional tables
  PERFORM execute.

*--------------------------------------------------------------------*
* end of selection
*--------------------------------------------------------------------*
END-OF-SELECTION.
  PERFORM free_memory.
