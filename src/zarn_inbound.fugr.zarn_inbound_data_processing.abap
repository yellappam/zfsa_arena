*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Functional Specification 3113
* DATE WRITTEN     # 02112015
* SAP VERSION      # 731
* TYPE             # Function Module
* AUTHOR           # C90001363, Jitin Kharbanda
*-----------------------------------------------------------------------
* TITLE            # Maintain Products from PI
* PURPOSE          # To maintain Products in National Tables from PI
*                  # and also manage their versions
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # September 2018
* CHANGE No.       # Charm #9000004244 - CR092/93 PIM Schema changes
* DESCRIPTION      # Changes due to PIM schema changes
*                  # Introduced error handling when building new product
*                  # because now generating GUID to maintain unique
*                  # entries for the data that is now repeatable. Net
*                  # quantities, serving size and organism max values can
*                  # now have multiple values per product. New attributes
*                  # were also introduced and some attribute were changed.
*                  # Created new routines and adjusted the code where
*                  # necessary.
* WHO              # C90001929, Aruna Mackenjee
*-----------------------------------------------------------------------
* DATE             # 17.04.2019
* CHANGE No.       # CIP-148 - AReNa - Online Characteristics Change
* DESCRIPTION      # Clear certain properties from Regional Tables if they
*                  # match with National Data. And code clean up
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* DATE             # 2019/05
* CHANGE No.       # CIP-621
* DESCRIPTION      # Enable no commit logic - commit done by caller
*                  #
* WHO              # I90018154, Ivo Skolek
*-----------------------------------------------------------------------
* DATE             # 20/11/2019
* CHANGE No.       # Charm 9000006356; Jira CIP-148
* DESCRIPTION      # Pass product and regional data to buffer for use
*                  # in approval processing.
* WHO              # C90001929, Aruna Mackenjee
*-----------------------------------------------------------------------
FUNCTION zarn_inbound_data_processing.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(IT_PRODUCT) TYPE  ZMASTER_PRODUCTS_PRODUCT_TAB
*"     REFERENCE(IS_ADMIN) TYPE  ZMASTER_PRODUCTS_ADMIN
*"     REFERENCE(IV_ID_TYPE) TYPE  ZARN_ID_TYPE
*"     REFERENCE(IV_COMMIT) TYPE  XFELD DEFAULT 'X'
*"  EXPORTING
*"     REFERENCE(ET_RETURN) TYPE  BAL_T_MSG
*"     REFERENCE(ET_PRODUCT_ERROR) TYPE  ZMASTER_PRODUCTS_PRODUCT_TAB
*"     REFERENCE(EV_SLG1_LOG_NO) TYPE  BALOGNR
*"     REFERENCE(ET_CONTROL) TYPE  ZTARN_CONTROL
*"     REFERENCE(ET_PRD_VERSION) TYPE  ZTARN_PRD_VERSION
*"     REFERENCE(ET_VER_STATUS) TYPE  ZTARN_VER_STATUS
*"     REFERENCE(ET_CC_HDR) TYPE  ZTARN_CC_HDR
*"     REFERENCE(ET_CC_DET) TYPE  ZTARN_CC_DET
*"     REFERENCE(ET_PROD_DATA) TYPE  ZTARN_PROD_DATA
*"     REFERENCE(ET_REG_DATA) TYPE  ZTARN_REG_DATA
*"----------------------------------------------------------------------


* Internal Tables
* _db - Old Data/Existing Data in DB
* _o  - Old Data/Existing Data in DB (comparable fields only)
* _c  - Data to be compared - Inbound data from PI
* _n  - New Data - to be maintained in DB

  DATA: lt_master_product       TYPE zmaster_products_product_tab,
        ls_master_product       TYPE zmaster_products_product,
        lt_master_product_error TYPE zmaster_products_product_tab,
        ls_control              TYPE ty_s_control,
        lt_errmail              TYPE ztarn_inb_errmail,
        ls_errmail              TYPE zarn_inb_errmail,
*        lt_recipient            TYPE ty_t_recipient,
*        ls_recipient            TYPE ty_s_recipient,
        lt_errmail_data         TYPE ty_t_errmail_data,
        ls_errmail_data         TYPE ty_s_errmail_data,

        lt_ovr_mode_range       TYPE ty_t_ovr_mode_range,


        lt_ovr_version          TYPE ty_t_ovr_version,
        ls_ovr_version          TYPE ty_s_ovr_version,

        lt_ver_status_all       TYPE ty_t_ver_status,
        ls_ver_status_all       TYPE zarn_ver_status,   "++3171 JKH 05.05.2017
        lt_ver_status_n         TYPE ty_t_ver_status,
        lt_prd_version_n        TYPE ty_t_prd_version,
        lt_control_n            TYPE ty_t_control,
        lt_ver_status_o         TYPE ty_t_ver_status,
        lt_prd_version_o        TYPE ty_t_prd_version,
        ls_prd_version_o        TYPE zarn_prd_version,   "++3171 JKH 05.05.2017
        lt_control_o            TYPE ty_t_control,
        lt_prd_version_ovr_n    TYPE ty_t_prd_version,
        ls_prd_version_ovr_n    TYPE ty_s_prd_version,

        lt_prod_data_db         TYPE ty_t_prod_data,
        ls_prod_data_db         TYPE ty_s_prod_data,
        lt_prod_data_o          TYPE ty_t_prod_data,
        ls_prod_data_o          TYPE ty_s_prod_data,
        lt_prod_data_n          TYPE ty_t_prod_data,
        ls_prod_data_n          TYPE ty_s_prod_data,
        lt_prod_data_c          TYPE ty_t_prod_data,
        ls_prod_data_c          TYPE ty_s_prod_data,
        lt_prod_data_ovr_db     TYPE ty_t_prod_data,
        ls_prod_data_ovr_db     TYPE ty_s_prod_data,
        lt_prod_data_ovr_n      TYPE ty_t_prod_data,
        ls_prod_data_ovr_n      TYPE ty_s_prod_data,
        lt_prod_data_curr       TYPE ty_t_prod_data,
        ls_prod_data_curr       TYPE ty_s_prod_data,
        lt_prod_data_n_tmp      TYPE ztarn_prod_data,  "++SUP-20 JKH 02.10.2017

        lt_reg_data_o           TYPE ztarn_reg_data,
        lt_reg_data_n           TYPE ztarn_reg_data,

        lt_prod_uom_t           TYPE ty_t_prod_uom_t,

        lt_cc_hdr               TYPE ztarn_cc_hdr,
        lt_cc_det               TYPE ztarn_cc_det,
        ls_cc_hdr               TYPE zarn_cc_hdr,
        ls_cc_det               TYPE zarn_cc_det,
        lv_chgid                TYPE zarn_chg_id,

        lt_message              TYPE bal_t_msg,
        ls_message              TYPE bal_s_msg,
        ls_params               TYPE bal_s_parm,
        ls_log_params           TYPE zsarn_slg1_log_params_inbound,

        lt_dfies                TYPE dfies_tab,
        ls_dfies                TYPE dfies,

        lt_idno_err_range       TYPE RANGE OF zarn_idno,
        ls_idno_err_range       LIKE LINE OF lt_idno_err_range,

        lt_idno                 TYPE ztarn_national_lock, "ty_t_idno,
        ls_idno                 TYPE zsarn_national_lock, "ty_s_idno,
        lt_field_confg          TYPE ty_t_field_confg,
        lt_table_mastr          TYPE ty_t_table_mastr,
        lt_tabname              TYPE ty_t_tabname,
        lt_amd_tvarvc           TYPE STANDARD TABLE OF tvarvc,
        ls_amd_tvarvc           TYPE tvarvc.


  DATA: lv_idno        TYPE zarn_idno,
        lv_ver_status  TYPE zarn_version_status,
        lv_curr_status TYPE zarn_version_status,   "++3171 JKH 05.05.2017
        lv_new_product TYPE flag,
        lv_continue    TYPE flag,
        lv_no_chg_cat  TYPE flag,
        lv_guid        TYPE zarn_guid,
        lv_msgtyp      TYPE sy-msgty,
        lv_change      TYPE flag,
        lv_success     TYPE flag,
        lv_success_cnt TYPE i,
        lv_error_cnt   TYPE i,
        lv_warning_cnt TYPE i,
        lv_log_no      TYPE balognr,
        lv_mail_error  TYPE flag,
        lv_comp_ver    TYPE zarn_version.

  DATA: lo_validation_engine TYPE REF TO zcl_arn_validation_engine,
        lv_event             TYPE zarn_e_rl_event_id,
        lo_excep             TYPE REF TO zcx_arn_validation_err.

*>>>IS1609ins Feature 3162 - AReNa Approval Stabilisation
  DATA: lo_approvals          TYPE REF TO zcl_arn_approval_backend,
        lt_approval           TYPE zarn_t_approval,
        lt_approval_ch        TYPE zarn_t_approval_ch,
        lt_approval_ch_del    TYPE zarn_t_approval_ch,
        lt_appr_hist          TYPE zarn_t_appr_hist,
        lt_after_appr_actions TYPE zarn_t_after_appr_action.
*<<<IS1609ins Feature 3162 - AReNa Approval Stabilisation

  FIELD-SYMBOLS: <ls_prod_data_n> TYPE ty_s_prod_data.

* For debugging to check data from PI - uncomment the following loop
* and debug through SM50
*  WHILE 1 = 1.
*  ENDWHILE.

  TRY.
      gv_feature_a4_enabled = zcl_ftf_store_factory=>get_instance(
                                       )->get_feature( iv_store     = 'RFST'    "Dummy Value
                                                       iv_appl_area = zif_ftf_appl_area_c=>gc_arena
                                                       iv_feature   = gc_feature_a4
                                       )->is_active( ).
    CATCH zcx_object_not_found.
      gv_feature_a4_enabled = abap_false.
  ENDTRY.

  lt_master_product[] = it_product[].
  lv_guid             = is_admin-national_guid.

  CONDENSE lv_guid.

  REFRESH: lt_message[].

  REFRESH: lt_dfies[].
  CALL FUNCTION 'DDIF_FIELDINFO_GET'
    EXPORTING
      tabname        = 'ZSARN_SLG1_LOG_PARAMS_INBOUND'
      langu          = sy-langu
    TABLES
      dfies_tab      = lt_dfies[]
    EXCEPTIONS
      not_found      = 1
      internal_error = 2
      OTHERS         = 3.

* Get AMD email address for error's
  CLEAR: lt_amd_tvarvc[].
  SELECT *
    FROM tvarvc
    INTO CORRESPONDING FIELDS OF TABLE lt_amd_tvarvc[]
    WHERE name = 'ZARN_INBOUND_ERROR_EMAIL_AMD'
      AND type = 'S'.

* Get ID Types for Overwrite Mode
  CLEAR : lt_ovr_mode_range[].
  SELECT sign opti AS option low high
    FROM tvarvc
    INTO CORRESPONDING FIELDS OF TABLE lt_ovr_mode_range[]
    WHERE name = 'ZARN_INBOUND_OVR_MODE'
      AND type = 'S'.



* Get Product UOM Text
  CLEAR lt_prod_uom_t[].
  SELECT * FROM zarn_prod_uom_t
    INTO CORRESPONDING FIELDS OF TABLE lt_prod_uom_t[].



* Get Email Id for error
  REFRESH: lt_errmail[].
  SELECT * FROM zarn_inb_errmail
    INTO CORRESPONDING FIELDS OF TABLE lt_errmail[].
  IF sy-subrc = 0.
    SORT lt_errmail[] BY dept_code sub_dept_code matkl.
  ENDIF.

  CLEAR : lv_success_cnt, lv_error_cnt, lv_warning_cnt.

  IF lt_master_product[] IS INITIAL.

* Build Return Message

    CLEAR: ls_log_params, ls_params.
* Build Log Parameters
    PERFORM build_log_parameters USING lt_dfies[]
                                       ls_master_product
                                       is_admin
                                       lv_guid
                                       lt_prod_uom_t[]
                              CHANGING ls_params
                                       ls_log_params.


* Product Master data not found
    PERFORM build_message USING gc_message-error
                                gc_message-id
                                '001'
                                space
                                space
                                space
                                space
                                ls_params
                       CHANGING lt_message[].

    IF 1 = 2. MESSAGE e001(zarena_msg). ENDIF.

* Prepare Email Data
    LOOP AT lt_amd_tvarvc[] INTO ls_amd_tvarvc.
      ls_errmail_data-email_id   = ls_amd_tvarvc-low.
      ls_errmail_data-email_data = ls_log_params.
      APPEND ls_errmail_data TO lt_errmail_data[].
    ENDLOOP.

  ELSE.




* Get Code as ID Numbers

    CLEAR: lt_idno[], lt_ovr_version[].
    LOOP AT lt_master_product[] INTO ls_master_product.
      IF ls_master_product-code IS NOT INITIAL.

        CLEAR lv_idno.
        lv_idno = ls_master_product-code.
        CONDENSE lv_idno NO-GAPS.

        IF iv_id_type IN lt_ovr_mode_range[].
* Check 'OVR' Mode Version existence - overwrite mode
          CLEAR lv_continue.
          PERFORM check_ovr_mode_version USING lv_idno
                                               lt_ovr_mode_range[]
                                      CHANGING lv_continue
                                               lt_ovr_version[].
          IF lv_continue = abap_true.
* Build Return Message

            CLEAR: ls_log_params, ls_params.
* Build Log Parameters
            PERFORM build_log_parameters USING lt_dfies[]
                                               ls_master_product
                                               is_admin
                                               lv_guid
                                               lt_prod_uom_t[]
                                      CHANGING ls_params
                                               ls_log_params.

* IDNO already exist with ID type other than 'S/M'
            PERFORM build_message USING gc_message-warning "gc_message-error
                                        gc_message-id
                                        '010'
                                        space
                                        space
                                        space
                                        space
                                        ls_params
                               CHANGING lt_message[].

            IF 1 = 2. MESSAGE w010(zarena_msg). ENDIF.

            lv_warning_cnt = lv_warning_cnt + 1.
          ENDIF.  " IF lv_continue = abap_true.
        ENDIF.   " IF iv_id_type IN lt_ovr_mode_range[]


        IF iv_id_type EQ 'C'.     "++JKH 28.04.2017

          CLEAR ls_idno.
          READ TABLE lt_idno[] INTO ls_idno
          WITH KEY idno = lv_idno.
          IF sy-subrc NE 0.
            ls_idno-mandt = sy-mandt.
            ls_idno-idno  = lv_idno.
            INSERT ls_idno INTO TABLE  lt_idno[].
          ENDIF.

        ELSE.     "++JKH 28.04.2017

* Lock IDNO
          CALL FUNCTION 'ENQUEUE_EZARN_NATIONAL'
            EXPORTING
              idno           = lv_idno
              _wait          = abap_true
            EXCEPTIONS
              foreign_lock   = 1
              system_failure = 2
              OTHERS         = 3.
          IF sy-subrc = 0.

            CLEAR ls_idno.
            READ TABLE lt_idno[] INTO ls_idno
            WITH KEY idno = lv_idno.
            IF sy-subrc NE 0.
              ls_idno-mandt = sy-mandt.
              ls_idno-idno  = lv_idno.
              INSERT ls_idno INTO TABLE  lt_idno[].
            ENDIF.  " IDNO already received

          ELSE.
*** If IDNO already Locked

* Build Return Message

            CLEAR: ls_log_params, ls_params.
* Build Log Parameters
            PERFORM build_log_parameters USING lt_dfies[]
                                               ls_master_product
                                               is_admin
                                               lv_guid
                                               lt_prod_uom_t[]
                                      CHANGING ls_params
                                               ls_log_params.

* IDNO is Locked
            PERFORM build_message USING gc_message-error
                                        gc_message-id
                                        '005'
                                        space
                                        space
                                        space
                                        space
                                        ls_params
                               CHANGING lt_message[].

            IF 1 = 2. MESSAGE e005(zarena_msg). ENDIF.

            lv_error_cnt = lv_error_cnt + 1.
            APPEND ls_master_product TO lt_master_product_error[].

* Build Email Data for Error
            PERFORM build_error_email_data USING ls_log_params
                                                 ls_master_product
                                                 lt_errmail[]
                                        CHANGING lt_errmail_data[].


          ENDIF.  " If IDNO is locked

        ENDIF.  "   IF iv_id_type EQ 'C'.        "++JKH 28.04.2017


      ELSE.
*** If Code is missing

* Build Return Message

        CLEAR: ls_log_params, ls_params.
* Build Log Parameters
        PERFORM build_log_parameters USING lt_dfies[]
                                           ls_master_product
                                           is_admin
                                           lv_guid
                                           lt_prod_uom_t[]
                                  CHANGING ls_params
                                           ls_log_params.

* Code mising in Product Master Data
        PERFORM build_message USING gc_message-error
                                    gc_message-id
                                    '002'
                                    space
                                    space
                                    space
                                    space
                                    ls_params
                           CHANGING lt_message[].

        IF 1 = 2. MESSAGE e002(zarena_msg). ENDIF.

        lv_error_cnt = lv_error_cnt + 1.
        APPEND ls_master_product TO lt_master_product_error[].

* Build Email Data for Error
        PERFORM build_error_email_data USING ls_log_params
                                             ls_master_product
                                             lt_errmail[]
                                    CHANGING lt_errmail_data[].

      ENDIF.
    ENDLOOP.  " LOOP AT lt_master_product[] INTO ls_master_product.

    IF lt_idno[] IS NOT INITIAL.

      SORT lt_idno[] BY idno.
      DELETE ADJACENT DUPLICATES FROM lt_idno[] COMPARING idno.


      REFRESH: lt_field_confg[], lt_table_mastr[], lt_tabname[], lt_prod_data_o[],
               lt_prod_data_db[], lt_ver_status_o[], lt_prd_version_o[], lt_control_o[],
               lt_ver_status_all[], lt_prod_data_curr[].

* Retreive IDNO data for curent version, if any
      PERFORM retreive_idno_data USING lt_idno[]
                              CHANGING lt_field_confg[]
                                       lt_table_mastr[]
                                       lt_tabname[]
                                       lt_ver_status_all[]
                                       lt_ver_status_o[]
                                       lt_prd_version_o[]
                                       lt_control_o[]
                                       lt_prod_data_o[]
                                       lt_prod_data_db[]
                                       lt_prod_data_curr[].


* Move Old to New - Control Data
      lt_ver_status_n[]  = lt_ver_status_o[].
      lt_ver_status_o[]  = lt_ver_status_all[].
      lt_prd_version_n[] = lt_prd_version_o[].
*      lt_control_n[]     = lt_control_o[].



      TRY.
          lv_event = zcl_arn_validation_engine=>gc_event_pim_inbound.  " 'NAT_IN'

          CREATE OBJECT lo_validation_engine
            EXPORTING
              iv_event          = lv_event
              iv_bapiret_output = abap_true.

        CATCH zcx_arn_validation_err INTO lo_excep.
          MESSAGE i000(zarena_msg) WITH lo_excep->msgv1 lo_excep->msgv2
                                        lo_excep->msgv3 lo_excep->msgv4.
      ENDTRY.

      CLEAR: lt_prd_version_ovr_n[], lt_idno_err_range[].
      CLEAR: lt_prod_data_n[], lt_prod_data_c[].
      LOOP AT lt_master_product[] INTO ls_master_product.

        CLEAR lv_idno.
        lv_idno = ls_master_product-code.
        CONDENSE lv_idno NO-GAPS.

* INS Begin of change ++3171 JKH 05.05.2017
        CLEAR: ls_ver_status_all, ls_prd_version_o, lv_curr_status.
        READ TABLE lt_ver_status_all[] INTO ls_ver_status_all
        WITH KEY idno = lv_idno.
        IF sy-subrc = 0.
          READ TABLE lt_prd_version_o[] INTO ls_prd_version_o
        WITH KEY idno = lv_idno
                 version = ls_ver_status_all-current_ver.
          IF sy-subrc = 0.
            lv_curr_status = ls_prd_version_o-version_status.
          ENDIF.
        ENDIF.
* INS End of change ++3171 JKH 05.05.2017

        CLEAR: ls_prod_data_o, ls_prod_data_n, ls_prod_data_c,
               ls_prod_data_curr, lv_new_product.               "++JKH 13.09.2016

        IF iv_id_type NE 'C'.     "++3171 JKH 05.05.2017
* INS Begin of Change JKH 13.09.2016
* Check if Data for Article version exist
          READ TABLE lt_prod_data_o[] INTO ls_prod_data_o
          WITH KEY idno = lv_idno.
          IF sy-subrc NE 0.

* Check if Data for Current version exist with status APR
            READ TABLE lt_prod_data_curr[] INTO ls_prod_data_curr
            WITH KEY idno = lv_idno.
            IF sy-subrc EQ 0 AND lv_curr_status = 'APR'.  "++3171 JKH 05.05.2017

              ls_prod_data_o = ls_prod_data_curr.
            ELSE.

              lv_new_product = abap_true.
            ENDIF. " READ TABLE lt_prod_data_curr[] INTO ls_prod_data_curr
          ENDIF. " READ TABLE lt_prod_data_o[] INTO ls_prod_data_o

* INS Begin of change ++3171 JKH 05.05.2017
        ELSEIF iv_id_type EQ 'C'.

* Check if Data for Current version exist
          READ TABLE lt_prod_data_curr[] INTO ls_prod_data_curr
          WITH KEY idno = lv_idno.
          IF sy-subrc EQ 0.  "++3171 JKH 05.05.2017

            ls_prod_data_o = ls_prod_data_curr.
          ELSE.

            lv_new_product = abap_true.
          ENDIF. " READ TABLE lt_prod_data_curr[] INTO ls_prod_data_curr

        ENDIF.
* INS End of change ++3171 JKH 05.05.2017

        IF lv_new_product = abap_true.
* If article doesn't exist then create first version

* Build New Product Data
          PERFORM build_new_prod_data USING ls_master_product
                                            lv_idno
                                   CHANGING ls_prod_data_n.

        ELSE.
* If article already exist then perform version management

* Version Management
          PERFORM version_management USING ls_master_product
                                           lv_idno
                                           lt_field_confg[]
                                           lt_tabname[]
                                           iv_id_type  "++3171 JKH 05.05.2017
                                  CHANGING lt_message[]
                                           lt_ver_status_n[]
                                           lt_ver_status_all[]
                                           ls_prod_data_o
                                           ls_prod_data_n
                                           ls_prod_data_c
                                           lv_change.

          INSERT ls_prod_data_c INTO TABLE lt_prod_data_c[].

        ENDIF.
* INS End of Change JKH 13.09.2016

* To have Business rule frame work during inbound processing
        PERFORM validate_business_rules USING ls_master_product
                                              lv_idno
                                              lt_errmail[]
                                              lv_guid
                                              lt_dfies[]
                                              is_admin
                                              lo_validation_engine
                                              lt_prod_uom_t[]
                                     CHANGING lt_message[]
                                              lt_master_product_error[]
                                              ls_prod_data_n
                                              lv_msgtyp
                                              lv_error_cnt
                                              lv_warning_cnt
                                              lv_success_cnt
                                              lt_errmail_data[].

        IF lv_msgtyp = gc_message-error.

          CLEAR ls_idno_err_range.
          ls_idno_err_range-sign   = 'I'.
          ls_idno_err_range-option = 'EQ'.
          ls_idno_err_range-low    = lv_idno.
          APPEND ls_idno_err_range TO lt_idno_err_range[].

          CONTINUE.
        ENDIF.


        TRY.
            CLEAR lv_ver_status.
* Generate Version
            PERFORM generate_version USING ls_master_product
                                           ls_prod_data_o
                                           lv_change
                                           lt_tabname[]
                                           lv_idno
                                           lv_guid
                                           iv_id_type
                                           lt_ovr_version[]
                                           lt_ver_status_all[]
                                           lt_ovr_mode_range[]
                                  CHANGING ls_prod_data_n
                                           lt_ver_status_n[]
                                           lt_prd_version_n[]
                                           lt_prd_version_ovr_n[]
                                           lv_ver_status.

          CATCH  cx_uuid_error INTO DATA(lo_excp).
            " UUID generation shouldn't really occur but need to cater for it
            " in the case that error does occur.
            " Do not process the product - error logging as per original design
            ADD 1 TO lv_error_cnt.

* Build Return Message
            CLEAR: ls_log_params, ls_params.
* Build Log Parameters
            PERFORM build_log_parameters USING lt_dfies[]
                                               ls_master_product
                                               is_admin
                                               lv_guid
                                               lt_prod_uom_t[]
                                      CHANGING ls_params
                                               ls_log_params.

            MESSAGE e044(zca) INTO DATA(lv_excp_msg).

            PERFORM build_message USING sy-msgty
                                        sy-msgid
                                        sy-msgno
                                        sy-msgv1
                                        sy-msgv2
                                        sy-msgv3
                                        sy-msgv4
                                        ls_params
                               CHANGING lt_message[].

* Build Email Data for Error
            PERFORM build_error_email_data USING ls_log_params
                                                 ls_master_product
                                                 lt_errmail[]
                                        CHANGING lt_errmail_data[].

            APPEND ls_master_product TO lt_master_product_error[].

            " Add to error range
            lt_idno_err_range = VALUE #( BASE lt_idno_err_range
                                         ( sign   = 'I'
                                           option = 'EQ'
                                           low    = lv_idno )
                                       ).

            CONTINUE.

        ENDTRY.

* Collect New Product Data
        CLEAR lv_success.
        IF <ls_prod_data_n> IS ASSIGNED. UNASSIGN <ls_prod_data_n>. ENDIF.
        READ TABLE lt_prod_data_n[] ASSIGNING <ls_prod_data_n>
        WITH KEY idno = lv_idno.
        IF sy-subrc = 0.
*          <ls_prod_data_n> = ls_prod_data_n.
*          lv_success = abap_true.
          IF ls_prod_data_n-idno IS NOT INITIAL  AND ls_prod_data_n-version IS NOT INITIAL.
            DELETE lt_prod_data_n[] WHERE idno    = ls_prod_data_n-idno
                                      AND version = ls_prod_data_n-version.
            INSERT  ls_prod_data_n INTO TABLE lt_prod_data_n[].
            lv_success = abap_true.
          ENDIF.
        ELSE.

          IF lv_ver_status = 'NCH'.
            lv_success = abap_true.
          ELSE.
            IF ls_prod_data_n-idno IS NOT INITIAL  AND ls_prod_data_n-version IS NOT INITIAL.
              INSERT  ls_prod_data_n INTO TABLE lt_prod_data_n[].
              lv_success = abap_true.
            ENDIF.
          ENDIF.

        ENDIF.

        IF lv_success = abap_true.
* Build Return Message
          CLEAR: ls_log_params, ls_params.
* Build Log Parameters
          PERFORM build_log_parameters USING lt_dfies[]
                                             ls_master_product
                                             is_admin
                                             lv_guid
                                             lt_prod_uom_t[]
                                    CHANGING ls_params
                                             ls_log_params.

* IDNO maintained successfully in National Data
          PERFORM build_message USING gc_message-success
                                      gc_message-id
                                      '011'
                                      space
                                      space
                                      space
                                      space
                                      ls_params
                             CHANGING lt_message[].

          IF 1 = 2. MESSAGE e011(zarena_msg). ENDIF.

* Build Email Data for Error
          PERFORM build_error_email_data USING ls_log_params
                                               ls_master_product
                                               lt_errmail[]
                                      CHANGING lt_errmail_data[].


        ENDIF.

      ENDLOOP.  " LOOP AT lt_master_product[] INTO ls_master_product

    ENDIF.  " IF lt_idno[] IS INITIAL
  ENDIF.  " IF lt_master_product[] IS INITIAL




* Build Control Data
  CLEAR ls_control.
  ls_control-mandt              = sy-mandt.
  ls_control-guid               = lv_guid.
  ls_control-hybris_msgid       = is_admin-sending_system_id.
  ls_control-external_reference = is_admin-external_reference.
  ls_control-test_indicator     = is_admin-test_indicator.
  ls_control-arn_mode           = is_admin-operation_mode.
*  ls_control-status             = is_admin-
  ls_control-date_in            = sy-datum.
  ls_control-time_in            = sy-uzeit.
  ls_control-date_pi            = sy-datum.
  ls_control-time_pi            = sy-uzeit.
  ls_control-date_exp           = sy-datum.
  ls_control-time_exp           = sy-uzeit.
  ls_control-success_cnt        = lv_success_cnt.
  ls_control-error_cnt          = lv_error_cnt.
  ls_control-warning_cnt        = lv_warning_cnt.
  ls_control-created_on         = sy-datum.
  ls_control-created_at         = sy-uzeit.
  ls_control-created_by         = sy-uname.
  ls_control-log_no             = lv_log_no.
  APPEND ls_control TO lt_control_n[].


* 'OVR' Mode - Overwrite Mode
* For 'OVR' mode if any article version exist and a change is identified, then only
* create change Categories else not
  CLEAR: lt_cc_hdr[], lt_cc_det[], lv_chgid.
  CLEAR lv_no_chg_cat.
  IF iv_id_type IN lt_ovr_mode_range[].
*    lt_prod_data_ovr_n[]  = lt_prod_data_n[].
*    lt_prod_data_ovr_db[] = lt_prod_data_db[].

    IF lt_prd_version_ovr_n[] IS NOT INITIAL.

      LOOP AT lt_prd_version_ovr_n[] INTO ls_prd_version_ovr_n.

* New Product Data for 'CHG' changed product
        CLEAR ls_prod_data_n.
        READ TABLE lt_prod_data_n[] INTO ls_prod_data_n
        WITH KEY idno    = ls_prd_version_ovr_n-idno
                 version = ls_prd_version_ovr_n-version.
        IF sy-subrc = 0.
          INSERT ls_prod_data_n INTO TABLE lt_prod_data_ovr_n[].
        ENDIF.


* DB/Old Product Data for 'CHG' changed product
        CLEAR ls_prod_data_db.
        READ TABLE lt_prod_data_db[] INTO ls_prod_data_db
        WITH KEY idno    = ls_prd_version_ovr_n-idno
                 version = ls_prd_version_ovr_n-version.
        IF sy-subrc = 0.
          INSERT ls_prod_data_db INTO TABLE lt_prod_data_ovr_db[].
*{ Begin - Change categories determined incorrectly for fetch
        ELSE.
* Above code does not behave correctly. lt_prd_version_ovr_n can(will?) contain
* a newly generated version. If this occurs the system generates all change
* categories because it has nothing to compare against in  lt_prod_data_ovr_db
* To correct this we fallback to look at the last article or current approved version.
* Refer line 546-560 for logic we inherit to determine what we want to compare against
          PERFORM determine_comparison_version USING    ls_prd_version_ovr_n-idno
                                                        lt_prod_data_o
                                                        lt_prod_data_curr
                                               CHANGING lv_comp_ver.
          IF lv_comp_ver IS NOT INITIAL.
* Get the DB data using the correct comparison version
            ls_prod_data_db = VALUE #( lt_prod_data_db[ idno = ls_prd_version_ovr_n-idno version = lv_comp_ver ] OPTIONAL ).
            IF ls_prod_data_db IS NOT INITIAL.
              INSERT ls_prod_data_db INTO TABLE lt_prod_data_ovr_db[].
            ENDIF.
          ENDIF.
* } End - Change categories determined incorrectly for fetch
        ENDIF.

      ENDLOOP.

* Change Category
      PERFORM change_category USING lt_table_mastr[]
                                    lt_prod_data_ovr_n[]
                                    lt_prod_data_ovr_db[]
                           CHANGING lt_cc_hdr[]
                                    lt_cc_det[]
                                    lv_chgid.

    ENDIF.  " IF lt_prd_version_ovr_n[] IS NOT INITIAL



  ELSE.

* Change Category
    PERFORM change_category USING lt_table_mastr[]
                                  lt_prod_data_n[]
                                  lt_prod_data_db[]
                         CHANGING lt_cc_hdr[]
                                  lt_cc_det[]
                                  lv_chgid.
  ENDIF.

  " Set Regional Data
  PERFORM set_regional_data USING    lt_prod_data_n
                            CHANGING lt_reg_data_o
                                     lt_reg_data_n.

* Set Version Status to Obsolete
  PERFORM set_version_status_to_obs USING lt_ver_status_o[]
                                          lt_ver_status_n[]
                                 CHANGING lt_prd_version_o[]
                                          lt_prd_version_n[].

* Delete IDNO if any in error so as to not get updated
  IF lt_idno_err_range[] IS NOT INITIAL.
    DELETE lt_prd_version_n[] WHERE idno IN lt_idno_err_range[].
    DELETE lt_prd_version_o[] WHERE idno IN lt_idno_err_range[].

    DELETE lt_ver_status_n[] WHERE idno IN lt_idno_err_range[].
    DELETE lt_ver_status_o[] WHERE idno IN lt_idno_err_range[].

    DELETE lt_reg_data_n[] WHERE idno IN lt_idno_err_range[].
    DELETE lt_reg_data_o[] WHERE idno IN lt_idno_err_range[].

    DELETE lt_prod_data_n[] WHERE idno IN lt_idno_err_range[].
    DELETE lt_prod_data_o[] WHERE idno IN lt_idno_err_range[].

    DELETE lt_cc_hdr[]  WHERE idno IN lt_idno_err_range[].
    DELETE lt_cc_det[]  WHERE idno IN lt_idno_err_range[].
  ENDIF.


** If ID Type is 'C' - Comparision mode then don't maintain any data
** for 'C' mode - just execute inbound and return the results
  IF iv_id_type NE 'C'.     "++3171 JKH 30.03.2017

    lt_prod_data_n_tmp[] = lt_prod_data_n[].  "++SUP-20 JKH 02.10.2017

    go_inbound_utils = zcl_arn_inbound_utils=>get_instance( ).
    go_inbound_utils->set_prod_reg_data( it_product_data  = lt_prod_data_n_tmp
                                         it_regional_data = lt_reg_data_n ).

*>>>IS1609ins Feature 3162 - AReNa Approval Stabilisation

    lo_approvals = NEW #( iv_approval_event = zcl_arn_approval_backend=>gc_event_inbound ).
    lo_approvals->build_approvals_from_chg_bulk( EXPORTING it_zarn_cc_hdr       = lt_cc_hdr
                                                 IMPORTING et_zarn_approval     = lt_approval
                                                           et_zarn_approval_ch  = lt_approval_ch
                                                           et_zarn_appr_hist    = lt_appr_hist
                                                           et_zarn_approval_ch_del = lt_approval_ch_del
                                                           et_after_appr_actions = lt_after_appr_actions ).


*<<<IS1609ins Feature 3162 - AReNa Approval Stabilisation


*    lt_prod_data_n_tmp[] = lt_prod_data_n[].  "++SUP-20 JKH 02.10.2017
* Update Data to National Tables
    CALL FUNCTION 'ZARN_INBOUND_DATA_UPDATE' IN UPDATE TASK
      EXPORTING
        it_control         = lt_control_n[]
        it_prd_version     = lt_prd_version_n[]
        it_ver_status      = lt_ver_status_n[]
        it_reg_data        = lt_reg_data_n[]
        it_prod_data       = lt_prod_data_n_tmp[]
        it_control_old     = lt_control_o[]
        it_prd_version_old = lt_prd_version_o[]
        it_ver_status_old  = lt_ver_status_o[]
        it_reg_data_old    = lt_reg_data_o[]
        it_cc_hdr          = lt_cc_hdr[]
        it_cc_det          = lt_cc_det[]
*>>>IS1609ins Feature 3162 - AReNa Approval Stabilisation
        it_approval        = lt_approval[]
        it_approval_ch     = lt_approval_ch[]
        it_approval_ch_del = lt_approval_ch_del[]
        it_appr_hist       = lt_appr_hist[]
*<<<IS1609ins Feature 3162 - AReNa Approval Stabilisation
        iv_chgid           = lv_chgid
        it_idno_locked     = lt_idno[]
        iv_no_change_doc   = space
        iv_no_chg_cat      = lv_no_chg_cat.

*>>>IS1609ins Feature 3162 - AReNa Approval Stabilisation

    "this can call automatic enrichment etc. if all national were approved
    lo_approvals->call_after_appr_actions( lt_after_appr_actions ).

*<<<IS1609ins Feature 3162 - AReNa Approval Stabilisation



* Send Email for Products in Error
    PERFORM send_error_email USING lt_errmail_data[]
                          CHANGING lv_mail_error.

  ENDIF.  " IF iv_id_type NE 'C'                      "++3171 JKH 30.03.2017

* Maintain SLG1 Log
  CLEAR lv_log_no.
  PERFORM maintain_slg1_log USING lt_message[]
                         CHANGING lv_log_no.

  IF iv_id_type NE 'C'                     "++3171 JKH 30.03.2017
    AND iv_commit EQ abap_true.          "IS 2019/05 - enable no commit
    COMMIT WORK AND WAIT.
  ENDIF.  " IF iv_id_type NE 'C'

  et_return[]        = lt_message[].
  et_product_error[] = lt_master_product_error[].
  ev_slg1_log_no     = lv_log_no.

* INS Begin of Change JKH 24.03.2017
  et_control[]      = lt_control_n[].
  et_prd_version[]  = lt_prd_version_n[].
  et_ver_status[]   = lt_ver_status_n[].
  et_cc_hdr[]       = lt_cc_hdr[].
  et_cc_det[]       = lt_cc_det[].
  et_prod_data[]    = lt_prod_data_n[].
  et_reg_data[]     = lt_reg_data_n[].
* INS End of Change KH 24.03.2017

ENDFUNCTION.
