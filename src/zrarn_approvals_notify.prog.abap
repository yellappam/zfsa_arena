*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Functional Specification 3132
* DATE WRITTEN     # 07 03 2016
* SAP VERSION      # 731
* TYPE             # Report Program
* AUTHOR           # C90001448, Greg van der Loeff
*-----------------------------------------------------------------------
* TITLE            # AReNa: Send notifications for approvals
* PURPOSE          # AReNa: Send notifications for approvals
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------
REPORT zrarn_approvals_notify
       MESSAGE-ID zarena_msg.

INCLUDE zrarn_approvals_notify_def.
INCLUDE zrarn_approvals_notify_sel.
INCLUDE zrarn_approvals_notify_frm.

END-OF-SELECTION.
  PERFORM send_notifications.
