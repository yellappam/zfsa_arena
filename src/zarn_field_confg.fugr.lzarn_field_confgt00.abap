*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 03.03.2017 at 09:57:14
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_FIELD_CONFG................................*
DATA:  BEGIN OF STATUS_ZARN_FIELD_CONFG              .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_FIELD_CONFG              .
CONTROLS: TCTRL_ZARN_FIELD_CONFG
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_FIELD_CONFG              .
TABLES: ZARN_FIELD_CONFG               .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
