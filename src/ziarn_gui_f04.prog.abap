*&---------------------------------------------------------------------*
*&  Include           ZIARN_GUI_F04
*&---------------------------------------------------------------------*
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 25/11/2019
* CHANGE No.       # Charm 9000006356; Jira CIP-148
* DESCRIPTION      # Created new routine for saving regional data from
*                  # teh UI.
*                  # Created to include posting of data when there are
*                  # no outstanding approvals. Prior to this change the
*                  # posting did not occur after change category auto
*                  # approval. This change will post data when user saves
*                  # changes and after auto approval there are no
*                  # outstanding approvals.
*                  # Added routine SAVE_REGIONAL_DATA and replaced
*                  # relevant calls to PROCESS_REGIONAL_DATA with call
*                  # to new routine.
* WHO              # C90001929, Aruna Mackenjee
*-----------------------------------------------------------------------

FORM populate_banner_descriptions.

  STATICS: lt_tvkot  TYPE SORTED TABLE OF tvkot WITH UNIQUE KEY vkorg,
           lt_tmbwt  TYPE SORTED TABLE OF tmbwt WITH UNIQUE KEY bwscl,
           lt_twsst  TYPE SORTED TABLE OF twsst WITH UNIQUE KEY sstuf,
           lt_t179t  TYPE SORTED TABLE OF t179t WITH UNIQUE KEY prodh,
           lt_catman TYPE SORTED TABLE OF zmd_category WITH UNIQUE KEY role_id.

  DATA: ls_t179t  LIKE LINE OF lt_t179t,
        ls_catman LIKE LINE OF lt_catman.

  FIELD-SYMBOLS: <ls_zarn_reg_banner> LIKE LINE OF gt_zarn_reg_banner,
                 <ls_tvkot>           LIKE LINE OF lt_tvkot,
                 <ls_tmbwt>           LIKE LINE OF lt_tmbwt,
                 <ls_twsst>           LIKE LINE OF lt_twsst,
                 <ls_t179t>           LIKE LINE OF lt_t179t,
                 <ls_catman>          LIKE LINE OF lt_catman.

  IF lt_tvkot[] IS INITIAL.
    SELECT *
      INTO TABLE lt_tvkot
      FROM tvkot
      WHERE spras EQ sy-langu.
  ENDIF.
  IF lt_tmbwt[] IS INITIAL.
    SELECT *
      INTO TABLE lt_tmbwt
      FROM tmbwt
      WHERE spras EQ sy-langu.
  ENDIF.
  IF lt_twsst[] IS INITIAL.
    SELECT *
      INTO TABLE lt_twsst
      FROM twsst
      WHERE spras EQ sy-langu.
  ENDIF.

  LOOP AT gt_zarn_reg_banner ASSIGNING <ls_zarn_reg_banner>.
    CLEAR: <ls_zarn_reg_banner>-vtext_banner,
           <ls_zarn_reg_banner>-text_source_lni,
           <ls_zarn_reg_banner>-text_source_uni,
           <ls_zarn_reg_banner>-name_zzcatman,
           <ls_zarn_reg_banner>-vtext_sstuf,
           <ls_zarn_reg_banner>-vtext_prodh.
    READ TABLE lt_tvkot ASSIGNING <ls_tvkot> WITH TABLE KEY vkorg = <ls_zarn_reg_banner>-banner.
    IF sy-subrc EQ 0.
      <ls_zarn_reg_banner>-vtext_banner = <ls_tvkot>-vtext.
    ENDIF.
    IF NOT <ls_zarn_reg_banner>-source_lni IS INITIAL.
      READ TABLE lt_tmbwt ASSIGNING <ls_tmbwt> WITH TABLE KEY bwscl = <ls_zarn_reg_banner>-source_lni.
      IF sy-subrc EQ 0.
        <ls_zarn_reg_banner>-text_source_lni = <ls_tmbwt>-bwscb.
      ENDIF.
    ENDIF.
    IF NOT <ls_zarn_reg_banner>-source_uni IS INITIAL.
      READ TABLE lt_tmbwt ASSIGNING <ls_tmbwt> WITH TABLE KEY bwscl = <ls_zarn_reg_banner>-source_uni.
      IF sy-subrc EQ 0.
        <ls_zarn_reg_banner>-text_source_uni = <ls_tmbwt>-bwscb.
      ENDIF.
    ENDIF.
    IF NOT <ls_zarn_reg_banner>-zzcatman IS INITIAL.
      READ TABLE lt_catman ASSIGNING <ls_catman> WITH TABLE KEY role_id = <ls_zarn_reg_banner>-zzcatman.
      IF sy-subrc EQ 0.
        <ls_zarn_reg_banner>-name_zzcatman = <ls_catman>-role_name.
      ELSE.
        SELECT SINGLE *
          FROM zmd_category
          INTO ls_catman
          WHERE role_id EQ <ls_zarn_reg_banner>-zzcatman.
        IF sy-subrc EQ 0.
          INSERT ls_catman INTO TABLE lt_catman.
          <ls_zarn_reg_banner>-name_zzcatman = ls_catman-role_name.
        ENDIF.
      ENDIF.
    ENDIF.
    IF NOT <ls_zarn_reg_banner>-sstuf IS INITIAL.
      READ TABLE lt_twsst ASSIGNING <ls_twsst> WITH TABLE KEY sstuf = <ls_zarn_reg_banner>-sstuf.
      IF sy-subrc EQ 0.
        <ls_zarn_reg_banner>-vtext_sstuf = <ls_twsst>-vtext.
      ENDIF.
    ENDIF.
    IF NOT <ls_zarn_reg_banner>-prodh IS INITIAL.
      READ TABLE lt_t179t ASSIGNING <ls_t179t> WITH TABLE KEY prodh = <ls_zarn_reg_banner>-prodh.
      IF sy-subrc EQ 0.
        <ls_zarn_reg_banner>-vtext_prodh = <ls_t179t>-vtext.
      ELSE.
        SELECT SINGLE *
          INTO ls_t179t
          FROM t179t
          WHERE spras EQ sy-langu
          AND   prodh EQ <ls_zarn_reg_banner>-prodh.
        IF sy-subrc EQ 0.
          INSERT ls_t179t INTO TABLE lt_t179t.
          <ls_zarn_reg_banner>-vtext_prodh = ls_t179t-vtext.
        ENDIF.
      ENDIF.
    ENDIF.
    "{ +ONLD440 - Online ranging
    IF NOT <ls_zarn_reg_banner>-online_status IS INITIAL.
      <ls_zarn_reg_banner>-text_online_status = zcl_md_olr_cust=>get_ols_single( <ls_zarn_reg_banner>-online_status )-description.
    ENDIF.
    "} +ONLD440 - Online ranging

    IF NOT <ls_zarn_reg_banner>-pbs_code IS INITIAL.
      <ls_zarn_reg_banner>-pbs_name_text = zcl_arn_pbs_data_check=>get_pbs_single( <ls_zarn_reg_banner>-pbs_code )-pbs_name_text.
    ENDIF.
  ENDLOOP.

ENDFORM.

*&---------------------------------------------------------------------*
*&      Form  DEFAULT_LOGIC_SCREEN_0112
*&---------------------------------------------------------------------*
FORM default_logic_screen_0112 .

  DATA: lv_changed  TYPE boole_d,
        lt_fieldcat TYPE lvc_t_fcat.

  FIELD-SYMBOLS:
    <ls_zarn_reg_banner>  LIKE LINE OF gt_zarn_reg_banner,
    <ls_zarn_reg_std_ter> LIKE LINE OF gt_zarn_reg_std_ter,
    <ls_zarn_reg_rrp>     LIKE LINE OF gt_zarn_reg_rrp.

  CLEAR lv_changed.

  IF gt_zarn_reg_banner[] IS NOT INITIAL.

    LOOP AT gt_zarn_reg_banner ASSIGNING <ls_zarn_reg_banner>.

      READ TABLE gt_zarn_reg_std_ter TRANSPORTING NO FIELDS
        WITH KEY vkorg = <ls_zarn_reg_banner>-banner.
      IF sy-subrc NE 0.
        lv_changed = abap_true.

        " Default Standard Terms
        APPEND INITIAL LINE TO gt_zarn_reg_std_ter ASSIGNING <ls_zarn_reg_std_ter>.
        <ls_zarn_reg_std_ter>-mandt = sy-mandt.
        <ls_zarn_reg_std_ter>-idno = <ls_zarn_reg_banner>-idno.
        <ls_zarn_reg_std_ter>-vkorg = <ls_zarn_reg_banner>-banner.
      ENDIF.

      READ TABLE gt_zarn_reg_rrp TRANSPORTING NO FIELDS
        WITH KEY vkorg = <ls_zarn_reg_banner>-banner.
      IF sy-subrc NE 0.
        lv_changed = abap_true.
        " Default RRP
        APPEND INITIAL LINE TO gt_zarn_reg_rrp ASSIGNING <ls_zarn_reg_rrp>.
        <ls_zarn_reg_rrp>-mandt = sy-mandt.
        <ls_zarn_reg_rrp>-idno = <ls_zarn_reg_banner>-idno.
        <ls_zarn_reg_rrp>-vkorg = <ls_zarn_reg_banner>-banner.
        <ls_zarn_reg_rrp>-condition_type = 'VKP0'.
        <ls_zarn_reg_rrp>-cond_unit = 'EA'.
        <ls_zarn_reg_rrp>-cond_curr = 'NZD'.
      ENDIF.

    ENDLOOP.

    IF go_reg_con_dis_alv IS BOUND AND lv_changed IS NOT INITIAL.
      PERFORM refresh_reg_alv USING go_reg_con_dis_alv lt_fieldcat.
    ENDIF.

    IF go_reg_con_rrp_alv IS BOUND AND lv_changed IS NOT INITIAL.
      PERFORM refresh_reg_alv USING go_reg_con_rrp_alv lt_fieldcat.
    ENDIF.

  ENDIF.

* refresh Banner ALV
*  PERFORM refresh_reg_alv USING go_reg_rb_alv.

  SORT gt_zarn_reg_std_ter BY vkorg.
  SORT gt_zarn_reg_rrp BY vkorg.

ENDFORM.                    " DEFAULT_LOGIC_SCREEN_0112

*&---------------------------------------------------------------------*
*&      Form  DEFAULT_BANNERS
*&---------------------------------------------------------------------*
FORM default_banners USING pv_type         TYPE c
                           iv_force_cluster_update TYPE flag.

*** Retail Manager
*  Generation of DC Banner 1000
  IF zarn_reg_hdr-zz_uni_dc IS NOT INITIAL
  OR zarn_reg_hdr-zz_lni_dc IS NOT INITIAL.
    PERFORM default_banner USING     zcl_constants=>gc_banner_1000
                                     gc_dc_sos
                                     iv_force_cluster_update
                           CHANGING  gt_zarn_reg_banner
                                     gt_zarn_reg_cluster.
  ELSE.
    PERFORM undefault_banner USING     zcl_constants=>gc_banner_1000
                                       gc_dc_sos
                             CHANGING  gt_zarn_reg_banner.
  ENDIF.

*>>> IS 2019/02 LRA 3a - automatic ZN blocking / unblocking for dc supplied articles

  "this will automatically determine ZN status on 1000 banner
  zcl_arn_automatic_dc_block=>get_instance( )->default_zn_ui_reg_banner_dc(
   EXPORTING iv_idno           = zarn_products-idno
             is_reg_hdr        = zarn_reg_hdr
             iv_dcstock_rel_dt = zarn_reg_sc-dc_stk_rel_date
    CHANGING ct_reg_banner     = gt_zarn_reg_banner ).
*<<< IS 2019/02 LRA 3a - automatic ZN blocking / unblocking for dc supplied articles

  IF pv_type EQ gc_banner_def_ret.
* Generation of Banner = 4000, 5000,6000
    IF gv_banner_def_ret EQ abap_true.

      PERFORM default_banner USING     zcl_constants=>gc_banner_4000
                                       zarn_reg_hdr-bwscl
                                       iv_force_cluster_update
                             CHANGING  gt_zarn_reg_banner
                                       gt_zarn_reg_cluster.
      PERFORM default_banner USING     zcl_constants=>gc_banner_5000
                                       zarn_reg_hdr-bwscl
                                       iv_force_cluster_update
                             CHANGING  gt_zarn_reg_banner
                                       gt_zarn_reg_cluster.
      PERFORM default_banner USING     zcl_constants=>gc_banner_6000
                                       zarn_reg_hdr-bwscl
                                       iv_force_cluster_update
                             CHANGING  gt_zarn_reg_banner
                                       gt_zarn_reg_cluster.
    ELSE.
      PERFORM undefault_banner USING     zcl_constants=>gc_banner_4000
                                         zarn_reg_hdr-bwscl
                               CHANGING  gt_zarn_reg_banner.
      PERFORM undefault_banner USING     zcl_constants=>gc_banner_5000
                                         zarn_reg_hdr-bwscl
                               CHANGING  gt_zarn_reg_banner.
      PERFORM undefault_banner USING     zcl_constants=>gc_banner_6000
                                         zarn_reg_hdr-bwscl
                               CHANGING  gt_zarn_reg_banner.
    ENDIF.
  ENDIF.


  IF pv_type EQ gc_banner_def_gil.
*** Wholesale Manager
* Generation of Banner = 3000
    IF gv_banner_def_gil EQ abap_true.
      PERFORM default_banner USING     gc_gilmours_banner
                                       zarn_reg_hdr-bwscl
                                       iv_force_cluster_update
                             CHANGING  gt_zarn_reg_banner
                                       gt_zarn_reg_cluster.
    ELSE.
      PERFORM undefault_banner USING     gc_gilmours_banner
                                         zarn_reg_hdr-bwscl
                               CHANGING  gt_zarn_reg_banner.
    ENDIF.
  ENDIF.

  SORT gt_zarn_reg_banner BY banner.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  DEFAULT_BANNER
*&---------------------------------------------------------------------*
FORM default_banner  USING    pv_banner               TYPE vkorg
                              pv_bwscl                TYPE bwscl
                              iv_force_cluster_update TYPE flag
                     CHANGING pt_zarn_reg_banner      TYPE ztarn_reg_banner_ui
                              ct_zarn_reg_cluster     TYPE ztarn_reg_cluster_ui.

  DATA:
    ls_reg_banner   TYPE zarn_reg_banner,
    ls_reg_uom      LIKE LINE OF gt_zarn_reg_uom,
    ls_reg_uom_base LIKE LINE OF gt_zarn_reg_uom.

  FIELD-SYMBOLS: <ls_reg_banner> LIKE LINE OF pt_zarn_reg_banner.

* default Banner
  READ TABLE  pt_zarn_reg_banner ASSIGNING <ls_reg_banner>
       WITH  KEY idno   = zarn_products-idno
                 banner = pv_banner.
  IF sy-subrc EQ 0.
*   Already exists
    IF <ls_reg_banner>-zzcatman IS INITIAL.
      IF   pv_banner EQ gc_gilmours_banner.
        <ls_reg_banner>-zzcatman   = zarn_reg_hdr-gil_zzcatman.
      ELSE.
        <ls_reg_banner>-zzcatman   = zarn_reg_hdr-ret_zzcatman.
      ENDIF.
    ENDIF.

* INS Begin of Change CI18-124 JKH 29.06.2017
    IF pv_banner = zcl_constants=>gc_banner_4000 OR
       pv_banner = zcl_constants=>gc_banner_5000 OR
       pv_banner = zcl_constants=>gc_banner_6000.
      <ls_reg_banner>-scagr = zarn_reg_hdr-scagr.
    ENDIF.
* INS End of Change CI18-124 JKH 29.06.2017

  ELSE.
*   Generation of Banner
    ls_reg_banner-mandt      = sy-mandt.
    ls_reg_banner-idno       = zarn_products-idno.
    ls_reg_banner-banner     = pv_banner.

*   other obvious defaults
    IF   pv_banner EQ gc_gilmours_banner.
      ls_reg_banner-zzcatman   = zarn_reg_hdr-gil_zzcatman.
    ELSE.
      ls_reg_banner-zzcatman   = zarn_reg_hdr-ret_zzcatman.
    ENDIF.

    " set sos
    CASE pv_banner.
      WHEN zcl_constants=>gc_banner_4000
        OR zcl_constants=>gc_banner_5000
        OR zcl_constants=>gc_banner_6000
        OR zcl_constants=>gc_banner_3000.

        IF zarn_reg_hdr-zz_uni_dc IS NOT INITIAL.
          ls_reg_banner-source_uni = gc_dc_sos.
        ELSE.
          ls_reg_banner-source_uni = gc_non_dc_sos.
        ENDIF.

        IF zarn_reg_hdr-zz_lni_dc IS NOT INITIAL.
          ls_reg_banner-source_lni =   gc_dc_sos.
        ELSE.
          ls_reg_banner-source_lni = gc_non_dc_sos.
        ENDIF.

* Check if Banner if Live for Online
        IF abap_true = zcl_onl_art_range_exclusion=>is_banner_prep_for_online( ls_reg_banner-banner ).
          "++ONLD-822 JKH 16.01.2017
          TRY.
              ls_reg_banner-online_status = zcl_onl_arena=>get_default_onl_stat( is_reg_banner = ls_reg_banner
                                                                                 is_reg_hdr    = zarn_reg_hdr ).
            CATCH zcx_invalid_input INTO DATA(lo_exc).
          ENDTRY.
        ENDIF.

      WHEN zcl_constants=>gc_banner_1000.
        ls_reg_banner-source_uni =   gc_non_dc_sos.
        ls_reg_banner-source_lni =   gc_non_dc_sos.

      WHEN OTHERS.
    ENDCASE.


    " DC Issue Unit
    READ TABLE gt_zarn_reg_uom INTO ls_reg_uom_base
         WITH KEY idno =  zarn_products-idno
                  unit_base = abap_true.
    IF pv_banner EQ zcl_constants=>gc_banner_1000.
*     Issue Unit = Issue UoM from REGIONAL UOM
      READ TABLE gt_zarn_reg_uom INTO ls_reg_uom
           WITH KEY idno =  zarn_products-idno
                    issue_unit = abap_true.
      IF sy-subrc EQ 0 AND ls_reg_uom_base-meinh NE ls_reg_uom-meinh.
        ls_reg_banner-vrkme = ls_reg_uom-meinh.
      ENDIF.
    ELSE.
*     Sales Unit = RETAIL UoM from REGIONAL UOM
      READ TABLE gt_zarn_reg_uom INTO ls_reg_uom
           WITH KEY idno =  zarn_products-idno
                    sales_unit = abap_true.
      IF sy-subrc EQ 0 AND ls_reg_uom_base-meinh NE ls_reg_uom-meinh.
        ls_reg_banner-vrkme = ls_reg_uom-meinh.
      ENDIF.
    ENDIF.



    ls_reg_banner-pns_tpr_flag = zarn_reg_hdr-pns_tpr_flag.
    ls_reg_banner-scagr        = zarn_reg_hdr-scagr.
    ls_reg_banner-prerf        = zarn_reg_hdr-prerf.
    ls_reg_banner-qty_required = zarn_reg_hdr-qty_required.
    ls_reg_banner-prodh        = zarn_reg_hdr-prdha.

    APPEND ls_reg_banner TO gt_zarn_reg_banner ASSIGNING <ls_reg_banner>.
  ENDIF.

  lcl_reg_cluster_ui=>get_instance( )->update_regional_cluster( EXPORTING iv_force_cluster_update = iv_force_cluster_update
                                                                          it_zarn_reg_banner      = pt_zarn_reg_banner
                                                                CHANGING  ct_reg_cluster_ui       = ct_zarn_reg_cluster ).

*  lcl_reg_cluster_ui=>get_instance( )->align_regional_cluster( EXPORTING iv_force_cluster_update = iv_force_cluster_update
*                                                                         is_reg_banner_ui        = <ls_reg_banner>
*                                                               CHANGING  ct_reg_cluster_ui       = ct_zarn_reg_cluster ).

ENDFORM.                    " DEFAULT_BANNER

*&---------------------------------------------------------------------*
*&      Form  UNDEFAULT_BANNER
*&---------------------------------------------------------------------*
FORM undefault_banner  USING    pv_banner TYPE vkorg
                                pv_bwscl  TYPE bwscl
                       CHANGING pt_zarn_reg_banner TYPE ztarn_reg_banner_ui.

  FIELD-SYMBOLS: <ls_reg_banner> LIKE LINE OF pt_zarn_reg_banner.

* Un-default Banner
  READ TABLE  pt_zarn_reg_banner ASSIGNING <ls_reg_banner>
       WITH  KEY idno   = zarn_products-idno
                 banner = pv_banner.
  IF sy-subrc EQ 0.
    CLEAR: <ls_reg_banner>-zzcatman,
           <ls_reg_banner>-sstuf.
  ENDIF.

ENDFORM.                    " UNDEFAULT_BANNER
*&---------------------------------------------------------------------*
*&      Form  VALIDATE_REG_GTIN_100
*&---------------------------------------------------------------------*
* Validate Regional GTINs
*----------------------------------------------------------------------*
FORM validate_reg_gtin_100 USING fu_s_mod_cells    TYPE lvc_s_modi
                        CHANGING fc_s_mod_rows_ean TYPE zsarn_reg_ean_ui
                                 fc_t_reg_ean      TYPE ztarn_reg_ean_ui.

  FIELD-SYMBOLS <ls_reg_ean> TYPE zsarn_reg_ean_ui.



  IF fc_s_mod_rows_ean-meinh IS INITIAL AND
     fc_s_mod_rows_ean-ean11 IS INITIAL AND
     fc_s_mod_rows_ean-eantp IS INITIAL.


  ELSEIF fc_s_mod_rows_ean-meinh IS NOT INITIAL AND
         fc_s_mod_rows_ean-ean11 IS NOT INITIAL AND
         fc_s_mod_rows_ean-eantp IS NOT INITIAL.

    IF fu_s_mod_cells-fieldname NE 'HPEAN'.
      LOOP AT fc_t_reg_ean ASSIGNING <ls_reg_ean>
        WHERE idno  = fc_s_mod_rows_ean-idno
          AND ean11 = fc_s_mod_rows_ean-ean11.

        IF sy-tabix NE fu_s_mod_cells-row_id.
* Unit, GTIN and GTIN Category are required and must be identical.
          MESSAGE s080 DISPLAY LIKE 'E'.
          CLEAR: fc_s_mod_rows_ean-ean11, fc_s_mod_rows_ean-eantp.
          EXIT.
        ENDIF.
      ENDLOOP.
    ENDIF.

*    IF fu_s_mod_cells-fieldname EQ 'EANTP'.
*      READ TABLE fc_t_reg_ean ASSIGNING <ls_reg_ean> INDEX fu_s_mod_cells-tabix.
*      IF sy-subrc = 0.
*        <ls_reg_ean>-ean11 = fc_s_mod_rows_ean-ean11.
*      ENDIF.
*    ENDIF.

    IF fc_s_mod_rows_ean-hpean = abap_true AND fu_s_mod_cells-fieldname = 'HPEAN'.
* Main GTIN already exist for Unit &
      READ TABLE fc_t_reg_ean ASSIGNING <ls_reg_ean>
      WITH KEY idno  = fc_s_mod_rows_ean-idno
               meinh = fc_s_mod_rows_ean-meinh
               hpean = abap_true.
      IF sy-subrc = 0.
        IF sy-tabix NE fu_s_mod_cells-row_id.
          CLEAR: fc_s_mod_rows_ean-hpean.
          MESSAGE s081 WITH fc_s_mod_rows_ean-meinh DISPLAY LIKE 'E'.
          EXIT.
        ENDIF.
      ENDIF.
    ENDIF.

  ELSE.
* Unit, GTIN and GTIN Category are required and must be identical.
    MESSAGE s080 DISPLAY LIKE 'E'.
    EXIT.
  ENDIF.



ENDFORM.
*&---------------------------------------------------------------------*
*&      Module  VALIDATE_CATMAN  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE validate_catman_ret INPUT.

*  DATA: ls_reg_hdr_cat    TYPE zarn_reg_hdr.
*
*
** Default REG HDR fields
*  CLEAR ls_reg_hdr_cat.
*  READ TABLE gt_zarn_reg_hdr[] INTO ls_reg_hdr_cat
*    WITH KEY idno = zarn_reg_hdr-idno.
*
*
*  IF gv_banner_def_ret EQ abap_true.
*    IF zarn_reg_hdr-ret_zzcatman IS INITIAL.
*      zarn_reg_hdr-ret_zzcatman = ls_reg_hdr_cat-ret_zzcatman.
*    ENDIF.
*  ELSE.
*    CLEAR zarn_reg_hdr-ret_zzcatman.
*  ENDIF.







  IF zarn_reg_hdr-ret_zzcatman IS INITIAL
    AND gv_banner_def_ret IS NOT INITIAL.

    SET CURSOR FIELD 'ZARN_REG_HDR-RET_ZZCATMAN'.
    MESSAGE w019 DISPLAY LIKE 'E'.

  ELSEIF zarn_reg_hdr-ret_zzcatman IS NOT INITIAL
    AND gv_banner_def_ret IS NOT INITIAL
    AND zarn_reg_hdr-bwscl IS INITIAL.

    SET CURSOR FIELD 'ZARN_REG_HDR-BWSCL'.
    MESSAGE e029.

  ELSEIF zarn_reg_hdr-ret_zzcatman IS NOT INITIAL
    AND gv_banner_def_ret IS NOT INITIAL
    AND zarn_reg_hdr-bwscl EQ gc_bwscl_2
    AND zarn_reg_hdr-zz_lni_dc IS INITIAL
    AND zarn_reg_hdr-zz_uni_dc IS INITIAL.

    SET CURSOR FIELD 'ZARN_REG_HDR-ZZ_LNI_DC'.
    MESSAGE e030.

  ENDIF.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  VALIDATE_CATMAN_GIL  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE validate_catman_gil INPUT.



** Default REG HDR fields
*  CLEAR ls_reg_hdr_cat.
*  READ TABLE gt_zarn_reg_hdr[] INTO ls_reg_hdr_cat
*    WITH KEY idno = zarn_reg_hdr-idno.
*
*
*  IF gv_banner_def_gil EQ abap_true.
*    IF zarn_reg_hdr-gil_zzcatman IS INITIAL.
*      zarn_reg_hdr-gil_zzcatman = ls_reg_hdr_cat-gil_zzcatman.
*    ENDIF.
*  ELSE.
*    CLEAR zarn_reg_hdr-gil_zzcatman.
*  ENDIF.


  IF zarn_reg_hdr-gil_zzcatman IS INITIAL
    AND gv_banner_def_gil IS NOT INITIAL.

    SET CURSOR FIELD 'ZARN_REG_HDR-GIL_ZZCATMAN'.
    MESSAGE w031 DISPLAY LIKE 'E'.

  ELSEIF zarn_reg_hdr-gil_zzcatman IS NOT INITIAL
    AND gv_banner_def_gil IS NOT INITIAL
    AND zarn_reg_hdr-bwscl IS INITIAL.

    SET CURSOR FIELD 'ZARN_REG_HDR-BWSCL'.
    MESSAGE e029.

  ELSEIF zarn_reg_hdr-gil_zzcatman IS NOT INITIAL
    AND gv_banner_def_gil IS NOT INITIAL
    AND zarn_reg_hdr-bwscl EQ gc_bwscl_2
    AND zarn_reg_hdr-zz_lni_dc IS INITIAL
    AND zarn_reg_hdr-zz_uni_dc IS INITIAL.

    SET CURSOR FIELD 'ZARN_REG_HDR-ZZ_LNI_DC'.
    MESSAGE e030.

  ENDIF.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  VALIDATE_SOS  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE validate_sos INPUT.

  IF zarn_reg_hdr-bwscl EQ gc_bwscl_2
    AND zarn_reg_hdr-ret_zzcatman IS INITIAL.

    SET CURSOR FIELD 'ZARN_REG_HDR-RET_ZZCATMAN'.
    MESSAGE e019.

  ELSEIF zarn_reg_hdr-bwscl EQ gc_bwscl_2
    AND zarn_reg_hdr-zz_lni_dc IS INITIAL
    AND zarn_reg_hdr-zz_uni_dc IS INITIAL.

    SET CURSOR FIELD 'ZARN_REG_HDR-ZZ_LNI_DC'.
    MESSAGE e030.

  ELSEIF zarn_reg_hdr-bwscl EQ gc_bwscl_1
    AND ( zarn_reg_hdr-zz_lni_dc IS NOT INITIAL
    OR zarn_reg_hdr-zz_uni_dc IS NOT INITIAL ).

    IF zarn_reg_hdr-zz_lni_dc IS NOT INITIAL.
      SET CURSOR FIELD 'ZARN_REG_HDR-ZZ_LNI_DC'.
    ELSE.
      SET CURSOR FIELD 'ZARN_REG_HDR-ZZ_UNI_DC'.
    ENDIF.

    MESSAGE e032.

  ELSEIF ( zarn_reg_hdr-zz_lni_dc IS NOT INITIAL
    OR zarn_reg_hdr-zz_uni_dc IS NOT INITIAL )
    AND zarn_reg_hdr-bwscl NE gc_bwscl_2.

    SET CURSOR FIELD 'ZARN_REG_HDR-BWSCL'.
    MESSAGE e033.

  ENDIF.

* Now validate the banners
  CALL METHOD go_reg_rb_alv->check_changed_data.
  LOOP AT gt_zarn_reg_banner INTO gs_zarn_reg_banner WHERE banner NE zcl_constants=>gc_banner_1000.
    IF zarn_reg_hdr-zz_uni_dc EQ abap_false AND gs_zarn_reg_banner-source_uni NE gc_bwscl_1.
      SET CURSOR FIELD 'ZARN_REG_HDR-ZZ_UNI_DC'.
      MESSAGE e039 WITH 'UNI' gs_zarn_reg_banner-banner.
    ENDIF.
    IF zarn_reg_hdr-zz_lni_dc EQ abap_false AND gs_zarn_reg_banner-source_lni NE gc_bwscl_1.
      SET CURSOR FIELD 'ZARN_REG_HDR-ZZ_LNI_DC'.
      MESSAGE e039 WITH 'LNI' gs_zarn_reg_banner-banner.
    ENDIF.
  ENDLOOP.


* INS Begin of Change 3181 JKH 23.05.2017
* Along with other current vaidation on 'UNI DC Flag', addition stock report check
* is required when ever user unticks the UNIDC flag.
  IF zarn_reg_hdr-zz_uni_dc EQ abap_false AND sy-ucomm = 'ONUNI_SEL'.
    CLEAR gv_revert_issue_uom.
*
*    READ TABLE gt_zarn_reg_uom INTO DATA(ls_zarn_ru) TRANSPORTING meinh
*      WITH KEY issue_unit = abap_true.
*    IF sy-subrc EQ 0.
*      PERFORM popup_approve_issue_unit USING ls_zarn_ru-meinh " old
*                                             ls_zarn_ru-meinh. " new
*    ENDIF.

* Get Open PO/SO and display Stock Report
    PERFORM display_stock_popup.


    IF gv_revert_issue_uom IS NOT INITIAL.
      zarn_reg_hdr-zz_uni_dc = abap_true.
    ENDIF.

  ENDIF.
* INS End of Change 3181 JKH 23.05.2017


ENDMODULE.

MODULE change_dc_variable_weight INPUT.

  IF zarn_reg_hdr-bwscl <> zif_supply_source_c=>gc_stock_transfer AND
     zarn_reg_hdr-zz_uni_dc IS INITIAL AND
     zarn_reg_hdr-zz_lni_dc IS INITIAL AND
     zarn_reg_hdr-zzvar_wt_flag IS NOT INITIAL.
    CLEAR: zarn_reg_hdr-zzvar_wt_flag.
    MESSAGE i139.
  ENDIF.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Form  DELETE_BANNER
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM delete_banner USING pt_rows TYPE lvc_t_row.

  DATA ls_rows TYPE LINE OF lvc_t_row.
  DATA ls_zarn_reg_banner LIKE LINE OF gt_zarn_reg_banner.
  DATA lt_banner_ret LIKE gt_zarn_reg_banner.
  DATA lt_banner_gil LIKE gt_zarn_reg_banner.
  DATA lt_banner_dc  LIKE gt_zarn_reg_banner.
  DATA lv_error      TYPE boole_d.
  DATA lt_fieldcat   TYPE lvc_t_fcat.
  DATA: lv_delete TYPE flag.

  LOOP AT pt_rows INTO ls_rows.
    READ TABLE gt_zarn_reg_banner INTO ls_zarn_reg_banner INDEX ls_rows-index.
    IF sy-subrc EQ 0.
      IF ls_zarn_reg_banner-banner EQ zcl_constants=>gc_banner_4000 OR
        ls_zarn_reg_banner-banner EQ zcl_constants=>gc_banner_5000 OR
        ls_zarn_reg_banner-banner EQ zcl_constants=>gc_banner_6000.

        APPEND ls_zarn_reg_banner TO lt_banner_ret.
        CLEAR ls_zarn_reg_banner .

      ELSEIF ls_zarn_reg_banner-banner EQ zcl_constants=>gc_banner_1000.
        APPEND ls_zarn_reg_banner TO lt_banner_dc.
        CLEAR ls_zarn_reg_banner.

      ELSEIF ls_zarn_reg_banner-banner EQ zcl_constants=>gc_banner_3000.
        APPEND ls_zarn_reg_banner TO lt_banner_gil.
        CLEAR ls_zarn_reg_banner.

      ENDIF.
    ENDIF.

  ENDLOOP.

  CLEAR lv_error.

  " get all banners for retail
  IF lt_banner_ret[] IS NOT INITIAL.
    FREE lt_banner_ret[].
    LOOP AT gt_zarn_reg_banner INTO ls_zarn_reg_banner.
      IF ls_zarn_reg_banner-banner EQ zcl_constants=>gc_banner_4000 OR
         ls_zarn_reg_banner-banner EQ zcl_constants=>gc_banner_5000 OR
         ls_zarn_reg_banner-banner EQ zcl_constants=>gc_banner_6000.

        APPEND ls_zarn_reg_banner TO lt_banner_ret.
        CLEAR ls_zarn_reg_banner .
      ENDIF.
    ENDLOOP.
  ENDIF.

**  RETAIL
  IF lt_banner_ret[] IS NOT INITIAL AND gv_banner_def_ret IS NOT INITIAL.
    MESSAGE s035 DISPLAY LIKE 'E'.
    RETURN.
  ELSEIF lt_banner_ret[] IS NOT INITIAL AND gv_banner_def_ret IS INITIAL.
    LOOP AT lt_banner_ret INTO ls_zarn_reg_banner.
      READ TABLE gt_mvke TRANSPORTING NO FIELDS
        WITH KEY vkorg = ls_zarn_reg_banner-banner.
      IF sy-subrc EQ 0.
        lv_error = abap_true.
      ENDIF.
    ENDLOOP.

    IF lv_error IS NOT INITIAL.
      MESSAGE s034 DISPLAY LIKE 'E'.
      RETURN.
    ENDIF.

  ENDIF.

  " GILMOURS
  IF lt_banner_gil[] IS NOT INITIAL AND gv_banner_def_gil IS NOT INITIAL.
    MESSAGE s036 DISPLAY LIKE 'E'.
    RETURN.
  ELSEIF lt_banner_gil[] IS NOT INITIAL AND gv_banner_def_gil IS INITIAL.
    LOOP AT lt_banner_gil INTO ls_zarn_reg_banner.
      READ TABLE gt_mvke TRANSPORTING NO FIELDS
        WITH KEY vkorg = ls_zarn_reg_banner-banner.
      IF sy-subrc EQ 0.
        lv_error = abap_true.
      ENDIF.
    ENDLOOP.

    IF lv_error IS NOT INITIAL.
      MESSAGE s034 DISPLAY LIKE 'E'.
      RETURN.
    ENDIF.

  ENDIF.

  " DC
  IF lt_banner_dc[] IS NOT INITIAL AND zarn_reg_hdr-bwscl EQ gc_bwscl_2.
    MESSAGE s037 DISPLAY LIKE 'E'.
    RETURN.
  ELSEIF lt_banner_dc[] IS NOT INITIAL AND zarn_reg_hdr-bwscl NE gc_bwscl_2.
    LOOP AT lt_banner_dc INTO ls_zarn_reg_banner.
      READ TABLE gt_mvke TRANSPORTING NO FIELDS
        WITH KEY vkorg = ls_zarn_reg_banner-banner.
      IF sy-subrc EQ 0.
        lv_error = abap_true.
      ENDIF.
    ENDLOOP.

    IF lv_error IS NOT INITIAL.
      MESSAGE s034 DISPLAY LIKE 'E'.
      RETURN.
    ENDIF.

  ENDIF.

  " SUCCESS
  LOOP AT gt_zarn_reg_banner ASSIGNING FIELD-SYMBOL(<ls_zarn_reg_banner>).

    CLEAR: lv_delete.

    " retail
    READ TABLE lt_banner_ret TRANSPORTING NO FIELDS WITH KEY banner = <ls_zarn_reg_banner>-banner.
    IF sy-subrc EQ 0.
      lv_delete = abap_true.
    ENDIF.

    " wholesale
    READ TABLE lt_banner_gil TRANSPORTING NO FIELDS WITH KEY banner = <ls_zarn_reg_banner>-banner.
    IF sy-subrc EQ 0.
      lv_delete = abap_true.
    ENDIF.

    " DC
    READ TABLE lt_banner_dc TRANSPORTING NO FIELDS WITH KEY banner = <ls_zarn_reg_banner>-banner.
    IF sy-subrc EQ 0.
      lv_delete = abap_true.
    ENDIF.

    IF lv_delete = abap_true.
      READ TABLE gt_zarn_reg_cluster ASSIGNING FIELD-SYMBOL(<ls_reg_cluster>)
                               WITH KEY idno   = <ls_zarn_reg_banner>-idno
                                        banner = <ls_zarn_reg_banner>-banner.
      IF sy-subrc = 0.
        CLEAR: <ls_reg_cluster>-banner.
      ENDIF.
      CLEAR: <ls_zarn_reg_banner>-banner.
    ENDIF.

  ENDLOOP.

  DELETE gt_zarn_reg_banner  WHERE banner IS INITIAL.
  DELETE gt_zarn_reg_cluster WHERE banner IS INITIAL.

  PERFORM refresh_reg_alv USING go_reg_rb_alv lt_fieldcat.
  lcl_reg_cluster_ui=>get_instance( )->refresh( ).

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  ALV_ADD_BANNER_INFO
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM alv_add_banner_info USING pt_rows      TYPE lvc_t_row
                               p_er_sender  TYPE REF TO cl_gui_alv_grid.

  DATA ls_rows            TYPE LINE OF lvc_t_row.
  DATA ls_zarn_reg_banner LIKE LINE OF gt_zarn_reg_banner.
  DATA lt_fieldcat        TYPE lvc_t_fcat.

  FIELD-SYMBOLS:
    <fs_reg_std_ter> LIKE LINE OF gt_zarn_reg_std_ter,
    <fs_reg_rrp>     LIKE LINE OF gt_zarn_reg_rrp.

  LOOP AT pt_rows INTO ls_rows.
    READ TABLE gt_zarn_reg_banner INTO ls_zarn_reg_banner INDEX ls_rows-index.
    IF sy-subrc EQ 0.
      CASE p_er_sender.
        WHEN go_reg_con_dis_alv.
          READ TABLE gt_zarn_reg_std_ter TRANSPORTING NO FIELDS
            WITH KEY idno = ls_zarn_reg_banner-idno vkorg = ls_zarn_reg_banner-banner.
          IF sy-subrc NE 0.
            APPEND INITIAL LINE TO gt_zarn_reg_std_ter ASSIGNING <fs_reg_std_ter>.
            <fs_reg_std_ter>-mandt = sy-mandt.
            <fs_reg_std_ter>-idno = ls_zarn_reg_banner-idno.
            <fs_reg_std_ter>-vkorg = ls_zarn_reg_banner-banner.
          ENDIF.

        WHEN go_reg_con_rrp_alv.
          APPEND INITIAL LINE TO gt_zarn_reg_rrp ASSIGNING <fs_reg_rrp>.
          <fs_reg_rrp>-mandt = sy-mandt.
          <fs_reg_rrp>-idno = ls_zarn_reg_banner-idno.
          <fs_reg_rrp>-vkorg = ls_zarn_reg_banner-banner.
          <fs_reg_rrp>-condition_type = 'VKP0'.
          <fs_reg_rrp>-cond_unit = 'EA'.
          <fs_reg_rrp>-cond_curr = 'NZD'.
        WHEN OTHERS.
      ENDCASE.

    ENDIF.

  ENDLOOP.

  SORT gt_zarn_reg_std_ter BY vkorg.
  SORT gt_zarn_reg_rrp BY vkorg.

  CALL METHOD p_er_sender->refresh_table_display.


ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  POPUP_UNSAVED_DATA
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM popup_unsaved_data USING p_v_answer TYPE char1.

  CALL FUNCTION 'POPUP_TO_CONFIRM_STEP'
    EXPORTING
      textline1 = TEXT-211
      textline2 = TEXT-212
      titel     = TEXT-015
    IMPORTING
      answer    = p_v_answer
    EXCEPTIONS
      OTHERS    = 1.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  SWITCH_MODE_RECORD
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM switch_mode_or_record USING p_v_answer TYPE char1.

  IF gv_display IS INITIAL.
**        check ALV changes
*    PERFORM check_alv_data_change.

*    IF gv_data_change IS INITIAL.
    PERFORM check_reg_changes CHANGING gv_data_change.
*    ENDIF.
*       SAVE on change
    IF gv_data_change IS NOT INITIAL OR sy-datar EQ abap_true.

      CLEAR p_v_answer.
      PERFORM popup_unsaved_data USING p_v_answer.

      IF p_v_answer = gc_yes.          "'J'.  "yes
        " continuw after Save
*        PERFORM process_regional_data USING abap_true.
        PERFORM save_regional_data.

      ELSEIF  p_v_answer EQ gc_no.      "'N'.  " No
        " continue without save
      ELSEIF  p_v_answer = gc_cancel.  "'A'.  " Cancel
        " return without save and remain on same screen
        RETURN.
      ENDIF.
    ENDIF.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  SET_BLOCK_LABEL_COLOR
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM set_block_label_color.

  IF gs_screen-name EQ 'TXT_APPROVAL'
    OR gs_screen-name EQ 'TXT_HDR'
    OR gs_screen-name EQ 'TXT_UOM'
    OR gs_screen-name EQ 'TXT_PIR'
    OR gs_screen-name EQ 'TXT_LIST_PRICE'
    OR ( gs_screen-name EQ 'TXT_FOOD_N_SAFETY' AND gv_nat_fsi IS NOT INITIAL )
    OR ( gs_screen-name EQ 'TXT_CONSUMER_INFORMATION' AND gv_nat_ci IS NOT INITIAL )
    OR ( gs_screen-name EQ 'ADDITIONAL_INFO' AND gv_nat_ai IS NOT INITIAL )
    OR ( gs_screen-name EQ 'DANGEROUS_GOODS_INFORMATION' AND gv_nat_dgi IS NOT INITIAL )
    OR ( gs_screen-name EQ 'NUTRITIONAL_INFORMATION' AND gv_nat_ni IS NOT INITIAL )
    OR ( gs_screen-name EQ 'INGREDIENT_INFORMATION' AND gv_nat_ii IS NOT INITIAL ).

    gs_screen-intensified = 1.

  ENDIF.

ENDFORM.

*FORM get_lifnr CHANGING pc_lifnr TYPE lifnr.
*
*  DATA: lt_return TYPE STANDARD TABLE OF ddshretval,
*        ls_return LIKE LINE OF lt_return,
*        ls_lfa1   TYPE lfa1.
*
*  REFRESH lt_return.
*  CALL FUNCTION 'F4IF_FIELD_VALUE_REQUEST'
*    EXPORTING
*      tabname           = 'LFA1'
*      fieldname         = 'LIFNR'
*      searchhelp        = 'ZHARN_VENDOR'
*      shlpparam         = 'LIFNR'
*    TABLES
*      return_tab        = lt_return
*    EXCEPTIONS
*      field_not_found   = 1
*      no_help_for_field = 2
*      inconsistent_help = 3
*      no_values_found   = 4
*      OTHERS            = 5.
*
*  READ TABLE lt_return INTO ls_return WITH KEY fieldname = 'LIFNR'.
*  IF sy-subrc EQ 0.
*    ls_lfa1-lifnr = ls_return-fieldval.
*    CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'
*      EXPORTING
*        input  = ls_lfa1-lifnr
*      IMPORTING
*        output = ls_lfa1-lifnr.
*    SELECT SINGLE lifnr bbbnr bbsnr bubkz
*      INTO CORRESPONDING FIELDS OF ls_lfa1
*      FROM lfa1
*      WHERE lifnr EQ ls_lfa1-lifnr.
*    IF sy-subrc EQ 0.
*      pc_lifnr = ls_lfa1-lifnr.
*    ENDIF.
*  ENDIF.
*
*ENDFORM.

FORM get_lifnr_gln CHANGING pc_gln TYPE zarn_gln.

  DATA: lt_return TYPE STANDARD TABLE OF ddshretval,
        ls_return LIKE LINE OF lt_return,
        ls_lfa1   TYPE lfa1.

  REFRESH lt_return.
  CALL FUNCTION 'F4IF_FIELD_VALUE_REQUEST'
    EXPORTING
      tabname           = 'ZARN_PIR'
      fieldname         = 'GLN'
      searchhelp        = 'ZHARN_VENDOR'
      shlpparam         = 'LIFNR'
    TABLES
      return_tab        = lt_return
    EXCEPTIONS
      field_not_found   = 1
      no_help_for_field = 2
      inconsistent_help = 3
      no_values_found   = 4
      OTHERS            = 5.
  READ TABLE lt_return INTO ls_return WITH KEY fieldname = 'LIFNR'.
  IF sy-subrc EQ 0.
    ls_lfa1-lifnr = ls_return-fieldval.
    CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'
      EXPORTING
        input  = ls_lfa1-lifnr
      IMPORTING
        output = ls_lfa1-lifnr.
    SELECT SINGLE lifnr bbbnr bbsnr bubkz
      INTO CORRESPONDING FIELDS OF ls_lfa1
      FROM lfa1
      WHERE lifnr EQ ls_lfa1-lifnr.
    IF sy-subrc EQ 0.
      pc_gln+0(7)  = ls_lfa1-bbbnr.
      pc_gln+7(5)  = ls_lfa1-bbsnr.
      pc_gln+12(1) = ls_lfa1-bubkz.
    ENDIF.
  ENDIF.

ENDFORM.

*FORM get_gln_for_lifnr USING pv_lifnr    TYPE lifnr
*                             pv_glnfield TYPE char20.
*
*  DATA: ls_lfa1       TYPE lfa1,
*        lv_gln        TYPE zarn_gln,
*        lv_dyname     TYPE d020s-prog,
*        lv_dynumb     TYPE d020s-dnum,
*        lt_dynpfields TYPE STANDARD TABLE OF dynpread,
*        ls_dynpfields LIKE LINE OF lt_dynpfields.
*
*  FIELD-SYMBOLS: <ls_gln_low> TYPE zarn_gln.
*
*  CHECK NOT pv_lifnr IS INITIAL.
*
*  SELECT SINGLE lifnr bbbnr bbsnr bubkz
*    INTO CORRESPONDING FIELDS OF ls_lfa1
*    FROM lfa1
*    WHERE lifnr EQ pv_lifnr.
*  IF sy-subrc EQ 0.
*    lv_gln+0(7)  = ls_lfa1-bbbnr.
*    lv_gln+7(5)  = ls_lfa1-bbsnr.
*    lv_gln+12(1) = ls_lfa1-bubkz.
*    IF lv_gln IS INITIAL
*    OR lv_gln EQ '0000000000000'.
*      MESSAGE s062 DISPLAY LIKE 'E' WITH pv_lifnr.
*    ELSE.
*      IF NOT pv_glnfield IS INITIAL.
*        lv_dyname = sy-repid.
*        lv_dynumb = sy-dynnr.
*        CLEAR ls_dynpfields.
*        ls_dynpfields-fieldname = pv_glnfield.
*        ls_dynpfields-fieldvalue = lv_gln.
*        APPEND ls_dynpfields TO lt_dynpfields.
*        CALL FUNCTION 'DYNP_VALUES_UPDATE'
*          EXPORTING
*            dyname               = lv_dyname
*            dynumb               = lv_dynumb
*          TABLES
*            dynpfields           = lt_dynpfields
*          EXCEPTIONS
*            invalid_abapworkarea = 1
*            invalid_dynprofield  = 2
*            invalid_dynproname   = 3
*            invalid_dynpronummer = 4
*            invalid_request      = 5
*            no_fielddescription  = 6
*            undefind_error       = 7
*            OTHERS               = 8.
*        ASSIGN (pv_glnfield) TO <ls_gln_low>.
*        IF <ls_gln_low> IS ASSIGNED.
*          <ls_gln_low> = lv_gln.
*        ENDIF.
*      ENDIF.
*    ENDIF.
*  ELSE.
*    MESSAGE s063 DISPLAY LIKE 'E' WITH pv_lifnr.
*  ENDIF.
*
*ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  APPROVE_BUTTON_AUTH
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM approve_button_auth .

  " Check user-authorisation to change the CPQ related- Master Data fields
  AUTHORITY-CHECK OBJECT 'ZMD_CPQCHG'
                      ID 'ACTVT' FIELD '37'.
  IF sy-subrc <> 0.
    " Incase of authorisation failed; Disable Approve button, else approve button is enabled by default...
    LOOP AT SCREEN.
      IF screen-name EQ 'APPROVE'.
        screen-input = 0.
        MODIFY SCREEN.
        EXIT.
      ENDIF.
    ENDLOOP.
  ENDIF.

  gv_heading_9001 = '- Not blocking Latest Ind Change'.



ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  DISP_OPEN_PO_ALV
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM disp_open_po_alv .

  " Don't refresh ALV in case user is not pressing cancel or approve buttons...
  CHECK gv_no_refresh IS INITIAL.

  " Create ALV grid object, if initial...
  IF gr_ccont IS INITIAL.
    CREATE OBJECT gr_ccont
      EXPORTING
        container_name = 'CC_OPEN_PO_SO'.

    CREATE OBJECT gr_alvgd
      EXPORTING
        i_parent = gr_ccont.

    " Get field catalog
    PERFORM get_fieldcat CHANGING gt_fcat.

  ENDIF.

**  display alv
  IF gr_alvgd IS NOT INITIAL.
    CALL METHOD gr_alvgd->set_table_for_first_display
      EXPORTING
*       is_variant      = st_var
        i_save          = abap_true
*       is_layout       = loyo1
      CHANGING
        it_outtab       = gt_open_po_so_disp
        it_fieldcatalog = gt_fcat.

  ENDIF.
ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  GET_FIELDCAT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      <--P_GT_FCAT  text
*----------------------------------------------------------------------*
FORM get_fieldcat  CHANGING lt_fcat1 TYPE lvc_t_fcat.

  DATA ls_fcat1 TYPE LINE OF lvc_t_fcat.

  ls_fcat1-col_pos   = 1.
  ls_fcat1-fieldname = 'PRODUCT'.
  ls_fcat1-tabname   = 'ZMD_HOST_SAP'.
  ls_fcat1-coltext   = 'PRODUCT'.
  ls_fcat1-seltext   = 'PRODUCT'.
  ls_fcat1-outputlen = 8.
  APPEND ls_fcat1 TO lt_fcat1.
  CLEAR ls_fcat1.

  ls_fcat1-col_pos   = 2.
  ls_fcat1-fieldname = 'INNER_UOM'.
  ls_fcat1-tabname   = 'ZMD_HOST_SAP'.
  ls_fcat1-coltext   = 'REPACK'.
  ls_fcat1-seltext   = 'REPACK'.
  ls_fcat1-outputlen = 6.
  APPEND ls_fcat1 TO lt_fcat1.
  CLEAR ls_fcat1.

  ls_fcat1-col_pos   = 3.
  ls_fcat1-fieldname = 'SHIPPER_UOM'.
  ls_fcat1-tabname   = 'ZMD_HOST_SAP'.
  ls_fcat1-coltext   = 'BULK'.
  ls_fcat1-seltext   = 'BULK'.
  ls_fcat1-outputlen = 4.
  APPEND ls_fcat1 TO lt_fcat1.
  CLEAR ls_fcat1.

  ls_fcat1-col_pos   = 4.
  ls_fcat1-fieldname = 'RETAIL_UOM'.
  ls_fcat1-tabname   = 'ZMD_HOST_SAP'.
  ls_fcat1-coltext   = 'DOC TYPE'.
  ls_fcat1-seltext   = 'DOCUMENT TYPE'.
  ls_fcat1-outputlen = 6.
  APPEND ls_fcat1 TO lt_fcat1.
  CLEAR ls_fcat1.

  ls_fcat1-col_pos   = 5.
  ls_fcat1-fieldname = 'EBELN'.
  ls_fcat1-tabname   = 'EKPO'.
  ls_fcat1-coltext   = 'DOCUMENT NUMBER'.
  ls_fcat1-seltext   = 'DOCUMENT NO'.
  ls_fcat1-outputlen = 10.
  APPEND ls_fcat1 TO lt_fcat1.
  CLEAR ls_fcat1.

  ls_fcat1-col_pos   = 6.
  ls_fcat1-fieldname = 'EBELP'.
  ls_fcat1-tabname   = 'EKPO'.
  ls_fcat1-coltext   = 'ITEM NO'.
  ls_fcat1-seltext   = 'ITEM NUMBER'.
  ls_fcat1-outputlen = 7.
  APPEND ls_fcat1 TO lt_fcat1.
  CLEAR ls_fcat1.

  ls_fcat1-col_pos   = 7.
  ls_fcat1-fieldname = 'MATNR'.
  ls_fcat1-tabname   = 'EKPO'.
  ls_fcat1-coltext   = 'ARTICLE'.
  ls_fcat1-seltext   = 'ARTICLE'.
  ls_fcat1-outputlen = 18.
  APPEND ls_fcat1 TO lt_fcat1.
  CLEAR ls_fcat1.

  ls_fcat1-col_pos   = 8.
  ls_fcat1-fieldname = 'MEINS'.
  ls_fcat1-tabname   = 'EKPO'.
  ls_fcat1-coltext   = 'UOM-ORDER'.
  ls_fcat1-seltext   = 'UOM-ORDER'.
  ls_fcat1-outputlen = 6.
  APPEND ls_fcat1 TO lt_fcat1.
  CLEAR ls_fcat1.

  ls_fcat1-col_pos   = 9.
  ls_fcat1-fieldname = 'KUNNR'.
  ls_fcat1-tabname   = 'VBAK'.
  ls_fcat1-coltext   = 'Receiving Site'.
  ls_fcat1-seltext   = 'Receiving Site'.
  ls_fcat1-outputlen = 10.
  APPEND ls_fcat1 TO lt_fcat1.
  CLEAR ls_fcat1.

  ls_fcat1-col_pos   = 10.
  ls_fcat1-fieldname = 'WERKS'.
  ls_fcat1-tabname   = 'VBAP'.
  ls_fcat1-coltext   = 'Supplying Site'.
  ls_fcat1-seltext   = 'Supplying Site'.
  ls_fcat1-outputlen = 6.
  APPEND ls_fcat1 TO lt_fcat1.
  CLEAR ls_fcat1.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  DISP_OPEN_STOCK_ALV
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM disp_open_stock_alv .

  " Don't refresh ALV in case user is not pressing cancel or approve buttons...
  CHECK gv_no_refresh IS INITIAL.

  " Create ALV grid object, if initial...
  IF gr_ccont_stock IS INITIAL.
    CREATE OBJECT gr_ccont_stock
      EXPORTING
        container_name = 'CC_STOCK_CHECK'.

    CREATE OBJECT gr_alvgd_stock
      EXPORTING
        i_parent = gr_ccont_stock.

    " Get field catalog
    PERFORM get_fieldcat_stock CHANGING gt_fcat_stock.

  ENDIF.

**  display alv
  IF gr_alvgd_stock IS NOT INITIAL.

    CALL METHOD gr_alvgd_stock->set_table_for_first_display
      EXPORTING
*       is_variant      = st_var
        i_save          = abap_true
*       is_layout       = loyo1
      CHANGING
        it_outtab       = gt_open_stock
        it_fieldcatalog = gt_fcat_stock.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  GET_FIELDCAT_STOCK
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      <--P_GT_FCAT_STOCK  text
*----------------------------------------------------------------------*
FORM get_fieldcat_stock CHANGING fc_t_fieldcat TYPE lvc_t_fcat      .

  DATA ls_fcat1 TYPE LINE OF lvc_t_fcat.

  ls_fcat1-col_pos   = 1.
  ls_fcat1-fieldname = 'PRODUCT'.
  ls_fcat1-tabname   = 'ZZD_ST_RECON_WMS'.
  ls_fcat1-coltext   = 'PRODUCT'.
  ls_fcat1-seltext   = 'PRODUCT'.
  ls_fcat1-outputlen = 10.
  APPEND ls_fcat1 TO fc_t_fieldcat.
  CLEAR ls_fcat1.

  ls_fcat1-col_pos   = 2.
  ls_fcat1-fieldname = 'WHSE'.
  ls_fcat1-tabname   = 'ZZD_ST_RECON_WMS'.
  ls_fcat1-coltext   = 'SITE'.
  ls_fcat1-seltext   = 'SITE'.
  ls_fcat1-outputlen = 6.
  APPEND ls_fcat1 TO fc_t_fieldcat.
  CLEAR ls_fcat1.

  ls_fcat1-col_pos   = 3.
  ls_fcat1-fieldname = 'ONHAND'.
  ls_fcat1-tabname   = 'ZZD_ST_RECON_WMS'.
  ls_fcat1-coltext   = 'ON-HAND'.
  ls_fcat1-seltext   = 'ON-HAND'.
  ls_fcat1-outputlen = 10.
  APPEND ls_fcat1 TO fc_t_fieldcat.
  CLEAR ls_fcat1.

  ls_fcat1-col_pos   = 4.
  ls_fcat1-fieldname = 'REJECT'.
  ls_fcat1-tabname   = 'ZZD_ST_RECON_WMS'.
  ls_fcat1-coltext   = 'REJECTED'.
  ls_fcat1-seltext   = 'REJECTED'.
  ls_fcat1-outputlen = 10.
  APPEND ls_fcat1 TO fc_t_fieldcat.
  CLEAR ls_fcat1.

  ls_fcat1-col_pos   = 5.
  ls_fcat1-fieldname = 'HOLD'.
  ls_fcat1-tabname   = 'ZZD_ST_RECON_WMS'.
  ls_fcat1-coltext   = 'ON-HOLD'.
  ls_fcat1-seltext   = 'ON-HOLD'.
  ls_fcat1-outputlen = 10.
  APPEND ls_fcat1 TO fc_t_fieldcat.
  CLEAR ls_fcat1.

  ls_fcat1-col_pos   = 6.
  ls_fcat1-fieldname = 'RETRN'.
  ls_fcat1-tabname   = 'ZZD_ST_RECON_WMS'.
  ls_fcat1-coltext   = 'RETURN'.
  ls_fcat1-seltext   = 'RETURN'.
  ls_fcat1-outputlen = 10.
  APPEND ls_fcat1 TO fc_t_fieldcat.
  CLEAR ls_fcat1.

  ls_fcat1-col_pos   = 7.
  ls_fcat1-fieldname = 'SNAP_DATE '.
  ls_fcat1-tabname   = 'ZZD_ST_RECON_WMS'.
  ls_fcat1-coltext   = 'SNAPSHOT DATE'.
  ls_fcat1-seltext   = 'SNAPSHOT DATE'.
  ls_fcat1-outputlen = 15.
  APPEND ls_fcat1 TO fc_t_fieldcat.

  ls_fcat1-col_pos   = 7.
  ls_fcat1-fieldname = 'SNAP_TIME'.
  ls_fcat1-tabname   = 'ZZD_ST_RECON_WMS'.
  ls_fcat1-coltext   = 'SNAPSHOT TIME'.
  ls_fcat1-seltext   = 'SNAPSHOT TIME'.
  ls_fcat1-outputlen = 15.
  APPEND ls_fcat1 TO fc_t_fieldcat.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  SET_OLD_UOM
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*&      Form  POPUP_APPROVE_ISSUE_UNIT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM popup_approve_issue_unit USING p_issue_uom_old TYPE meinh
                                    p_issue_uom_new TYPE meinh.

  DATA ls_old_issue_uom LIKE LINE OF gt_zmd_host_sap_itab.
  DATA ls_host_sap      LIKE LINE OF gt_zmd_host_sap_itab.
  DATA ls_open_po       TYPE ty_open_po.
  DATA ls_open_so       TYPE ty_open_so.
  DATA ls_open_po_so    TYPE ty_open_po_so.
  DATA lt_open_po_so    TYPE TABLE OF ty_open_po_so.
  DATA lv_issue_uom     TYPE meinh.
  DATA lv_host_iss_uom  TYPE meinh.
  DATA lv_hostprd_found TYPE boole_d.
  DATA lv_noof_hostprd  TYPE i.

  CLEAR: gt_open_po, gt_open_po_disp, gt_open_so, gt_open_so_disp, gt_open_po_so_disp,
         gt_open_stock.
  REFRESH: gt_open_po[], gt_open_po_disp[], gt_open_so[], gt_open_so_disp[], gt_open_po_so_disp[],
           gt_open_stock[].

  READ TABLE gt_zmd_host_sap_itab INTO ls_old_issue_uom WITH KEY latest = abap_true.

  IF sy-subrc EQ 0.

* Get Issue Unit
    CLEAR lv_issue_uom.
    IF ls_old_issue_uom-prboly = 'B'.
      lv_issue_uom = ls_old_issue_uom-shipper_uom.
    ELSE.
      lv_issue_uom = ls_old_issue_uom-inner_uom.
    ENDIF.

    IF lv_issue_uom EQ p_issue_uom_old OR " if old issue uom is same as latest
      lv_issue_uom NE p_issue_uom_new.    " if new issue uom is not same as latest

      CLEAR: lv_hostprd_found, lv_noof_hostprd.

      LOOP AT gt_zmd_host_sap_itab INTO ls_host_sap
        WHERE deleted EQ ''.
* Get host Issue Unit
        CLEAR lv_host_iss_uom.
        IF ls_host_sap-prboly = 'B'.
          lv_host_iss_uom = ls_host_sap-shipper_uom.
        ELSE.
          lv_host_iss_uom = ls_host_sap-inner_uom.
        ENDIF.

        " if there is a host product existing for the new issue uom
        IF lv_host_iss_uom EQ p_issue_uom_new.
          lv_hostprd_found = abap_true.
          lv_noof_hostprd = lv_noof_hostprd + 1.
        ENDIF.

      ENDLOOP.

      IF lv_hostprd_found EQ abap_false." AND lv_noof_hostprd EQ 1.
        gv_revert_issue_uom = abap_true.
        MESSAGE i040 INTO gv_issue_uom_msg.
        " show popup
        CALL SCREEN 9002 STARTING AT 20 10.
        RETURN.
      ELSEIF lv_hostprd_found EQ abap_true AND lv_noof_hostprd GT 1.
        gv_revert_issue_uom = abap_true.
        MESSAGE i059 INTO gv_issue_uom_msg.
        " show popup
        CALL SCREEN 9002 STARTING AT 20 10.
        RETURN.
      ENDIF.

      " Get open POs
      PERFORM get_open_po_lines USING ls_old_issue_uom-matnr
                                CHANGING gt_open_po.

      " Get open SOs
      PERFORM get_open_so_lines USING ls_old_issue_uom-matnr
                                CHANGING gt_open_so.

*      " Prepare global table for used POs/SOs to be displayed in popup
      PERFORM set_open_po_so_table USING ls_old_issue_uom.

      " Get Open WMS stock
      PERFORM get_stock_for_wms_product USING  ls_old_issue_uom-product
                                        CHANGING gt_open_stock.

*      IF gt_open_po_so_disp IS NOT INITIAL OR gt_open_stock IS NOT INITIAL.
      IF gt_open_stock IS NOT INITIAL.

        gv_revert_issue_uom = abap_true.
        " show popup
        CALL SCREEN 9001 STARTING AT 18 15.

      ENDIF.

    ENDIF.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  GET_OPEN_PO_LINES
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->P_P_LS_ZMD_HOST_SAP_ITAB_MATNR  text
*      <--P_GT_OPEN_PO  text
*----------------------------------------------------------------------*
FORM get_open_po_lines   USING p_iv_matnr TYPE matnr
                       CHANGING p_ct_open_po TYPE ty_t_open_po.

  DATA: ltr_dc     TYPE RANGE OF werks_d,
        lsr_dc     LIKE LINE OF ltr_dc,
        ltr_reswk  TYPE RANGE OF reswk,
        lsr_reswk  LIKE LINE OF ltr_reswk,
        lt_open_po TYPE TABLE OF ty_open_po.

  CONSTANTS:
    lc_loekz_blank TYPE loekz VALUE IS INITIAL,
    lc_elikz_blank TYPE elikz VALUE IS INITIAL.

  CLEAR: lt_open_po, p_ct_open_po.
  REFRESH: lt_open_po[], p_ct_open_po[].

  lsr_dc-sign = 'I'.
  lsr_dc-option = 'CP'.
  lsr_dc-low = 'DC*'.
  APPEND lsr_dc TO ltr_dc.

  lsr_reswk-sign = 'I'.
  lsr_reswk-option = 'CP'.
  lsr_reswk-low = 'DC*'.
  APPEND lsr_reswk TO ltr_reswk.

*  Select the open PO Lines having the Ordering UOM as REPACK...
**  Select the non-deleted PO Lines for DCs and which are still not completed
  SELECT a~ebeln b~ebelp b~matnr b~meins b~werks a~reswk
    INTO TABLE lt_open_po
    FROM ekko AS a
    INNER JOIN ekpo AS b
    ON a~ebeln = b~ebeln
    WHERE
    a~bsart NE 'ZNS' AND
    a~bstyp EQ 'F' AND
    a~loekz EQ lc_loekz_blank AND
*    a~ekorg EQ '1000' AND "-- 2330 Jitin 16.04.2015 " to include STOs
    b~loekz EQ lc_loekz_blank AND
    b~elikz EQ lc_elikz_blank AND
    ( b~werks IN ltr_dc  " ( DC01, DC02, DC03) &
     OR a~reswk IN ltr_reswk ) AND
    b~matnr EQ p_iv_matnr.

  IF sy-subrc EQ 0.
    p_ct_open_po[] = lt_open_po[].
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  GET_OPEN_SO_LINES
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->P_LS_OLD_ISSUE_UOM_MATNR  text
*      <--P_GT_OPEN_SO  text
*----------------------------------------------------------------------*
FORM get_open_so_lines    USING p_iv_matnr TYPE matnr
                       CHANGING p_ct_open_so TYPE ty_t_open_so.

  DATA lt_open_so     TYPE TABLE OF ty_open_so.
  DATA ls_open_so     TYPE ty_open_so.
  DATA ltr_gbsta      TYPE RANGE OF gbsta.
  DATA lsr_gbsta      LIKE LINE OF ltr_gbsta.
  DATA lv_wms_product TYPE zmd_e_host_product.
  DATA ltr_werks      TYPE RANGE OF werks_ext.
  DATA lsr_werks      LIKE LINE OF ltr_werks.

  lsr_werks-sign = 'I'.
  lsr_werks-option = 'CP'.
  lsr_werks-low = 'DC*'.
  APPEND lsr_werks TO ltr_werks.

  lsr_gbsta-sign = 'I'.
  lsr_gbsta-option = 'EQ'.

  lsr_gbsta-low = 'A'.
  APPEND lsr_gbsta TO ltr_gbsta.

  lsr_gbsta-low = 'B'.
  APPEND lsr_gbsta TO ltr_gbsta.

  " Select the non-deleted SO Lines for DCs and which are still not completed
  SELECT a~vbeln b~posnr b~matnr b~vrkme b~kdmat a~kunnr b~werks
    INTO CORRESPONDING FIELDS OF TABLE lt_open_so
    FROM vbak AS a
    INNER JOIN vbap AS b
    ON a~vbeln = b~vbeln
    INNER JOIN vbup AS c
    ON b~vbeln = c~vbeln
    AND b~posnr EQ c~posnr
    WHERE
*    a~auart IN ltr_auart AND
    a~vbtyp EQ 'C' AND
    a~vkorg EQ '1000' AND
    a~vtweg EQ '10' AND
    b~matnr EQ p_iv_matnr AND
    b~werks IN ltr_werks AND
*    b~posnr EQ c~posnr AND
    c~gbsta IN ltr_gbsta.

  IF sy-subrc EQ 0.
    SORT lt_open_so BY vbeln posnr matnr vrkme kdmat.
    DELETE ADJACENT DUPLICATES FROM lt_open_so COMPARING vbeln posnr matnr vrkme kdmat.

    LOOP AT lt_open_so INTO ls_open_so.
      CLEAR lv_wms_product.

      CALL FUNCTION 'Z_MD_DECODE_KDMAT'
        EXPORTING
          iv_wms_info    = ls_open_so-kdmat
        IMPORTING
*         EV_RSM_PRODUCT =
          ev_wms_product = lv_wms_product.
*         EV_LEGACY_UOM        =
*         EV_MENGE             =

      IF lv_wms_product IS NOT INITIAL.
        ls_open_so-wms_product = lv_wms_product.
        APPEND ls_open_so TO p_ct_open_so.
      ENDIF.

    ENDLOOP.

  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  SET_OPEN_PO_SO_TABLE
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->P_LS_OLD_INNER_UOM  text
*      -->P_P_LS_ZMD_HOST_SAP_ITAB  text
*----------------------------------------------------------------------*
FORM set_open_po_so_table  USING  p_is_old_host_uom TYPE zmd_host_sap.

  DATA ls_open_po       TYPE ty_open_po.
  DATA ls_open_so       TYPE ty_open_so.
  DATA ls_open_po_so    TYPE ty_open_po_so.
  DATA lt_open_po_so    TYPE TABLE OF ty_open_po_so.

  " select only the Line matching with the REPACK or BULK UOM
  LOOP AT gt_open_po INTO ls_open_po WHERE meins = p_is_old_host_uom-inner_uom OR
                                           meins = p_is_old_host_uom-shipper_uom.
    MOVE-CORRESPONDING ls_open_po TO ls_open_po_so.
    ls_open_po_so-product = p_is_old_host_uom-product.
    ls_open_po_so-inner_uom = p_is_old_host_uom-inner_uom.
    ls_open_po_so-shipper_uom = p_is_old_host_uom-shipper_uom.
    ls_open_po_so-retail_uom = 'PO'.
    ls_open_po_so-kunnr = ls_open_po-werks.
    ls_open_po_so-werks = ls_open_po-reswk.
    APPEND ls_open_po_so TO gt_open_po_so_disp. CLEAR ls_open_po_so. CLEAR ls_open_po.
  ENDLOOP.

  " else consider the lines based on Article - all records of that article
  IF gt_open_po_so_disp IS INITIAL.
    LOOP AT gt_open_po INTO ls_open_po.
      MOVE-CORRESPONDING ls_open_po TO ls_open_po_so.
      ls_open_po_so-product = p_is_old_host_uom-product.
      ls_open_po_so-inner_uom = p_is_old_host_uom-inner_uom.
      ls_open_po_so-shipper_uom = p_is_old_host_uom-shipper_uom.
      ls_open_po_so-retail_uom = 'PO'.
      ls_open_po_so-kunnr = ls_open_po-werks.
      ls_open_po_so-werks = ls_open_po-reswk.
      APPEND ls_open_po_so TO gt_open_po_so_disp. CLEAR ls_open_po_so. CLEAR ls_open_po.
    ENDLOOP.
  ENDIF.

  " select only the Line matching with the product entered in VBAP-KDMAT - as per updated spec...
  LOOP AT gt_open_so INTO ls_open_so WHERE wms_product = p_is_old_host_uom-product.
    MOVE-CORRESPONDING ls_open_so TO ls_open_po_so.
    ls_open_po_so-ebeln = ls_open_so-vbeln.
    ls_open_po_so-ebelp = ls_open_so-posnr.
    ls_open_po_so-meins = ls_open_so-vrkme.
    CLEAR ls_open_so.

    ls_open_po_so-product = p_is_old_host_uom-product.
    ls_open_po_so-inner_uom = p_is_old_host_uom-inner_uom.
    ls_open_po_so-shipper_uom = p_is_old_host_uom-shipper_uom.
    ls_open_po_so-retail_uom = 'SO'.
    APPEND ls_open_po_so TO gt_open_po_so_disp. CLEAR ls_open_po_so.
  ENDLOOP.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  GET_STOCK_FOR_WMS_PRODUCT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->P_P_LS_ZMD_HOST_SAP_ITAB_MATNR  text
*      -->P_P_LS_ZMD_HOST_SAP_ITAB_PRODUCT  text
*      <--P_GT_OPEN_STOCK  text
*----------------------------------------------------------------------*
FORM get_stock_for_wms_product USING    p_iv_product    TYPE zmd_host_sap-product
                               CHANGING p_ct_open_stock TYPE  ztmd_stock_check_alv.

  CALL FUNCTION 'Z_SD_GET_WMS_PRODUCT_RECORD'
    EXPORTING
      iv_wms_product     = p_iv_product
*     IV_START_DATE      = SY-DATLO
*     IV_BACK_DAYS       = 1
    IMPORTING
      et_wms_prod_record = p_ct_open_stock.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  BACK_TO_WORKLIST
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM back_to_worklist USING iv_ratio TYPE ajhranp.

  DATA lv_answer TYPE char1.

  CONSTANTS:
    lc_yes    TYPE char01 VALUE 'J',
    lc_no     TYPE char01 VALUE 'N',
    lc_cancel TYPE char01 VALUE 'A'.

  PERFORM check_reg_changes CHANGING gv_data_change.

  IF gv_data_change IS NOT INITIAL AND gv_display IS INITIAL.

    PERFORM popup_unsaved_data USING lv_answer.

    IF lv_answer = lc_yes.          "'J'.  "yes

*     Save data first
*      PERFORM process_regional_data USING abap_true.
      PERFORM save_regional_data.
*     then leave
      SET SCREEN '0100'.
    ELSEIF  lv_answer = lc_no.      "'N'.  " No
*     no Save ...
      SET SCREEN '0100'.
    ELSEIF  lv_answer = lc_cancel.  "'A'.  " Cancel
      RETURN.
**    Do nothing
    ENDIF.

  ELSE. " No change so leave
    SET SCREEN '0100'.
  ENDIF.

  gv_extension = ( iv_ratio * 20 ).
  gv_text_size-text      = TEXT-i01.
  gv_text_size-icon_text = TEXT-i01.

  CALL METHOD go_docking_alv->set_extension
    EXPORTING
      extension = gv_extension.  " Max 2000

  PERFORM unlock_regional_data USING gt_rows.

  IF go_approval IS BOUND.
    go_approval->dequeue_appr( zif_enq_mode_c=>optimistic ).
  ENDIF.

  CLEAR gv_toolbar_ucomm.

* Refresh ALV worklist
  PERFORM refresh_worklist.

  PERFORM free_screen_objects.

*>>>IS full refresh - select all the data as well
  DATA lv_full_refresh TYPE xfeld VALUE abap_true.

  IF lv_full_refresh = abap_true.
    "do full refresh - repeat the initial selection
    PERFORM back_to_worklist_full_refresh.
  ENDIF.

* I Care only lock - NOT required
*  PERFORM lock_icare_worklist.

  "IS - don't set the handlers again

*  SET HANDLER:
*    go_event_receiver->on_toolbar FOR go_worklist_alv,
*    go_event_receiver->handle_toolbar FOR go_worklist_alv.
*
*<<<IS full refresh - select all the data as well

  "close the approvals on back button
  WRITE icon_expand   TO gv_nat_pb_00 AS ICON.
  WRITE icon_expand   TO gv_reg_pb_00 AS ICON.
  gv_appr_rscreen_no  = '0702'.
  gv_appr_screen_no   = '0702'.
  gv_appr_nat_okcode  = 'NPB00'.
  gv_appr_reg_okcode  = 'RPB00'.

  IF go_appr_nat_grid IS BOUND.
    go_appr_nat_grid->refresh_table_display( ).
  ENDIF.

  IF go_appr_reg_grid IS BOUND.
    go_appr_reg_grid->refresh_table_display( ).
  ENDIF.

  CALL METHOD go_worklist_alv->refresh_table_display.

  cl_gui_cfw=>set_new_ok_code( new_code = 'SYSTEM').

  LEAVE SCREEN.

ENDFORM.

FORM back_to_worklist_full_refresh.

  CLEAR : gt_worklist[], gt_prod_data, gt_reg_data[],
          gt_prd_version[], gt_control[], gt_cc_hdr[].

  PERFORM get_data.

  SET PARAMETER ID 'ZARN_GUI_TAB' FIELD gv_sel_tab.

  IF gt_prod_data IS INITIAL.
    MESSAGE i000 WITH TEXT-201.
    RETURN.
  ENDIF.

* Create ALV worklist
  PERFORM create_worklist.

* I Care only lock
  PERFORM lock_icare_worklist.

ENDFORM.

FORM get_data.

* get data based on selection screen active tab
  CASE gv_sel_tab.   "?? this will not work if user fills selection screen tab
      "?? and then tabs to another before F8 ;-)
    WHEN gc_arena_tab.
*     Read National and Regional data
      PERFORM get_data_arena.

    WHEN gc_icare_tab.
*     send request to Hybris and Read National and Regional data
      PERFORM get_data_icare.

    WHEN gc_appro_tab.      " gc_appro_tab.
      gv_display = abap_true.
      CLEAR: gv_appr_nat_okcode,
             gv_appr_reg_okcode.
      gv_appr_screen_no   = '0700'.
      gv_appr_rscreen_no  = '0701'.
*     if coming from approvals then start approvals expanded
      WRITE icon_collapse  TO gv_nat_pb_00 AS ICON.
      WRITE icon_collapse  TO gv_reg_pb_00 AS ICON.
      PERFORM preprocess_approval. "+3162
      PERFORM get_data_approval.
  ENDCASE.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  UNLOCK_REGIONAL_DATA
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->P_LT_ROWS  text
*      -->P_ABAP_FALSE  text
*----------------------------------------------------------------------*
FORM unlock_regional_data  USING pt_rows       TYPE lvc_t_row.

  DATA:
    ls_rows     LIKE LINE OF  pt_rows.
  FIELD-SYMBOLS <fs_worklist> LIKE LINE OF gt_worklist.

  LOOP AT pt_rows INTO ls_rows.

*   read selected articles from worklist
    READ TABLE gt_worklist ASSIGNING <fs_worklist> INDEX ls_rows-index.
    IF sy-subrc IS NOT INITIAL.
      CONTINUE.
    ENDIF.

*   Unlock locked object
    CALL FUNCTION 'DEQUEUE_EZARN_REGIONAL'
      EXPORTING
        idno           = <fs_worklist>-idno
*       _wait          = abap_true
      EXCEPTIONS
        foreign_lock   = 1
        system_failure = 2
        OTHERS         = 3.
    IF sy-subrc IS INITIAL.
      CLEAR <fs_worklist>-lock_user.
      CLEAR <fs_worklist>-rowcolor.
      CLEAR <fs_worklist>-icon.
    ENDIF.

  ENDLOOP.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  FREE_SCREEN_OBJECTS
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM free_screen_objects .

  FREE :
  " free global tables
  gt_rows,
  gt_line, gt_line_prfam,
  gs_nat_alv_uom,gt_nat_alv_uom,
  gt_prod_str, gs_prod_str, gs_prod_data_o, gs_prod_data_n, gs_prod_data,
  gs_reg_data_o, gs_reg_data_n, gs_reg_data, gs_reg_data_copy,
  gt_appr_reg_approvals,
  gt_appr_nat_approvals,
  gv_appr_nat_release_statust,
  gv_appr_reg_release_statust,
  gv_appr_nat_status,
  gv_appr_reg_status,
  gv_appr_nat_release_status,
  gv_appr_reg_release_status,
  zarn_ver_status,
  zarn_prd_version,
  zarn_products_ex,
  zarn_nat_mc_hier,
  zarn_uom_variant,
  zarn_control,
  gs_worklist,
*  go_appr_nat_grid,
*  go_appr_reg_grid,
*  gt_appr_nat_fieldcat,
*  gt_appr_reg_fieldcat,

** National Tables
  gt_products, gt_products_ex, gt_cdt_level, gt_zarn_nutrients,
  gt_zarn_ntr_health, gt_ntr_claimtx, gt_zarn_ntr_claims,gt_zarn_allergen,
  gt_hsno, gt_dgi_margin,gt_zarn_prep_type, gt_ins_code,gt_zarn_diet_info,
  gt_zarn_additives,gt_zarn_growing,gt_zarn_fb_ingre,gt_zarn_chem_char,
  gt_zarn_organism,
  gt_nat_mc_hier,
  gt_zarn_addit_info,
  gt_zarn_uom_variant,
  gt_zarn_gtin_var,
  gt_zarn_pir,
  gt_pir_comm_ch,
  gt_zarn_list_price,
  gt_zarn_hsno,
  gt_zarn_pir_comm_ui,
  gt_zarn_pir_comm_ch,
  gt_zarn_cdt_level,
  gt_zarn_ins_code,
  gt_zarn_net_qty,
  gt_zarn_serve_size,
** National structures
  gs_cdt_level,
  gs_nutrients,
  gs_ntr_claimtx,
  gs_allergen,
  gs_hsno,
  gs_dgi_margin,
  gs_ins_code,
  gs_diet_info,
  gs_additives,
  gs_fb_ingre,
  gs_chem_char,
  gs_organism,
  gs_nat_mc_hier,
  gs_addit_info,
  gs_uom_variant,
  gs_gtin_var,
  gs_pir ,
  gs_pir_comm_ch,
  gs_list_price,
** Regional tables
  gt_zarn_reg_hdr,
  gt_zarn_reg_banner,
  gt_zarn_reg_onlcat,             "++ONLD-835 JKH 12.05.2017
  gt_zarn_reg_ean,
  gt_zarn_reg_pir,
  gt_zarn_reg_prfam,
  gt_zarn_reg_txt,
  gt_zarn_reg_uom,
  gt_zarn_reg_allerg,
*  gt_zarn_reg_sc,  9000004661
* Regional condition tables
*  gt_zarn_reg_dc_sell,
  gt_zarn_reg_rrp,
  gt_zarn_reg_std_ter,
* Ranges for upper case
  gr_short_descr,
  gr_gln_descr,
** Regional structures
  gs_reg_hdr,
  gs_reg_banner,
  gs_reg_ean,
  gs_reg_pir,
  gs_reg_prfam,
  gs_reg_txt,
  gs_reg_uom,
  gs_reg_rrp,
  gt_multi_gtin,
  gs_reg_allerg,
*  gs_reg_sc, 9000004661
* Text tables for screen
  gs_text.

  CLEAR:
   gt_multi_gtin,
   zarn_growing,
   zarn_reg_hdr,
   zarn_reg_prfam,
   zarn_reg_ean,
   zarn_reg_uom,
   zarn_reg_allerg,
   gt_zarn_addit_info,
   gt_zarn_hsno,
   gt_pir_comm_ch,
   gt_zarn_pir_comm_ui,
   gt_zarn_pir_comm_ch,
   gt_zarn_cdt_level,
   gt_zarn_ins_code,
   gt_marm_uom,
   gv_new,
   gv_existing,
   gv_change,
   gs_reg_data_db,
   gs_reg_data_save,
   gv_banner_def_gil,
   gv_banner_def_ret,
   gt_zmd_host_sap_itab,
   zarn_reg_hdr,
   zarn_products,
   zarn_pir,
   zarn_fb_ingre.


ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  REFRESH_WORKLIST
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM refresh_worklist .
  DATA:
    lt_enq  TYPE STANDARD TABLE OF seqg3,
    ls_enq  LIKE LINE OF lt_enq,
    lv_garg TYPE eqegraarg.

  FIELD-SYMBOLS <fs_worklist> LIKE LINE OF gt_worklist.

*   read selected articles from worklist
  LOOP AT gt_worklist ASSIGNING <fs_worklist>.
    lv_garg = sy-mandt && <fs_worklist>-idno.

*     check lock
    CALL FUNCTION 'ENQUEUE_READ'
      EXPORTING
        gclient               = sy-mandt
        gname                 = gc_lock_reg
        garg                  = lv_garg
        guname                = ' '
      TABLES
        enq                   = lt_enq
      EXCEPTIONS
        communication_failure = 1
        system_failure        = 2
        OTHERS                = 3.

    IF lt_enq IS NOT INITIAL.
      READ TABLE lt_enq INTO ls_enq INDEX 1.
      IF sy-subrc EQ 0.
        PERFORM get_control_data USING    ls_enq-guname
                                 CHANGING gs_worklist-lock_user.

*       Locked Entries
        gs_worklist-rowcolor = gc_alv_locked .
        WRITE icon_locked TO gs_worklist-icon.
        gs_worklist-lock_user = gs_worklist-icon && gs_worklist-lock_user.
      ENDIF.

    ENDIF.

  ENDLOOP.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  ALV_ADD_REG_UOM
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->P_LT_ROWS  text
*      -->P_GO_REG_CON_DIS_ALV  text
*----------------------------------------------------------------------*
FORM alv_add_reg_uom  USING p_er_sender  TYPE REF TO cl_gui_alv_grid.

  DATA lv_pim_uom_code  TYPE zarn_uom_code.
  DATA lt_celltab       TYPE lvc_t_styl.

  FIELD-SYMBOLS:
    <fs_reg_uom> LIKE LINE OF gt_zarn_reg_uom.

  CHECK zarn_reg_hdr-idno IS NOT INITIAL.
  CHECK p_er_sender IS BOUND.

  APPEND INITIAL LINE TO gt_zarn_reg_uom ASSIGNING <fs_reg_uom>.
  <fs_reg_uom>-mandt = sy-mandt.
  <fs_reg_uom>-idno = zarn_reg_hdr-idno.
  <fs_reg_uom>-obsolete_ind = icon_dummy.
  PERFORM get_reg_uom_pim_code CHANGING lv_pim_uom_code.

  IF lv_pim_uom_code IS NOT INITIAL.
    <fs_reg_uom>-pim_uom_code = lv_pim_uom_code.
  ENDIF.

  PERFORM set_uom_row_disable CHANGING lt_celltab.
  IF  lt_celltab[] IS NOT INITIAL.
    <fs_reg_uom>-celltab = lt_celltab[].
  ENDIF.

  <fs_reg_uom>-erzet = sy-timlo.
  <fs_reg_uom>-ersda = sy-datlo.

  CALL METHOD p_er_sender->refresh_table_display.
  "Every time new UoM record added - Supply chain should also update
  CALL METHOD go_reg_sc_alv->refresh_table_display.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  ALV_ADD_REG_ONLCAT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
FORM alv_add_reg_onlcat  USING p_er_sender  TYPE REF TO cl_gui_alv_grid.

  DATA: ls_zarn_reg_onlcat TYPE zarn_reg_onlcat,
        lt_zarn_reg_onlcat LIKE gt_zarn_reg_onlcat,
        ls_bu_level        TYPE zonl_bu_level.


  FIELD-SYMBOLS:
    <fs_reg_onlcat>    LIKE LINE OF gt_zarn_reg_onlcat.

  CHECK zarn_reg_hdr-idno IS NOT INITIAL.
  CHECK p_er_sender IS BOUND.

  IF gv_bunit_drdn        IS INITIAL OR
     gv_catalog_type_drdn IS INITIAL.
* Please select Business Unit and Catalog type
    MESSAGE s130 DISPLAY LIKE 'E'.
    EXIT.
  ENDIF.


  APPEND INITIAL LINE TO gt_zarn_reg_onlcat ASSIGNING <fs_reg_onlcat>.
  <fs_reg_onlcat>-mandt        = sy-mandt.
  <fs_reg_onlcat>-idno         = zarn_reg_hdr-idno.
  <fs_reg_onlcat>-bunit        = gv_bunit_drdn.
  <fs_reg_onlcat>-catalog_type = gv_catalog_type_drdn.

* Online Catalog
  CLEAR ls_bu_level.
  READ TABLE gt_bu_level[] INTO ls_bu_level
    WITH KEY bunit        = gv_bunit_drdn
             catalog_type = gv_catalog_type_drdn.
  IF sy-subrc = 0.
    <fs_reg_onlcat>-onl_catalog = ls_bu_level-onl_catalog.
  ENDIF.


* Sequence No
  lt_zarn_reg_onlcat[] = gt_zarn_reg_onlcat[].
  SORT lt_zarn_reg_onlcat[] BY seqno DESCENDING.
  CLEAR ls_zarn_reg_onlcat.
  READ TABLE lt_zarn_reg_onlcat[] INTO ls_zarn_reg_onlcat
  WITH KEY bunit        = gv_bunit_drdn
           catalog_type = gv_catalog_type_drdn.
  IF sy-subrc NE 0.
    <fs_reg_onlcat>-seqno = 1.
  ELSE.
    <fs_reg_onlcat>-seqno = ls_zarn_reg_onlcat-seqno + 1.
  ENDIF.

  CALL METHOD p_er_sender->refresh_table_display.

ENDFORM.  " ALV_ADD_REG_ONLCAT
*&---------------------------------------------------------------------*
*&      Form  GET_REG_UOM_PIM_CODE
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM get_reg_uom_pim_code CHANGING p_c_pim_uom_code TYPE zarn_uom_code.

  TYPES:
    BEGIN OF ty_uom,
      idno         TYPE zarn_idno,
      pim_uom_code TYPE zarn_uom_code,
      suffix       TYPE i,
    END OF ty_uom.

  DATA ls_uom             TYPE ty_uom.
  DATA lv_char            TYPE char4.
  DATA lt_uom             TYPE TABLE OF ty_uom.
  DATA ls_zarn_reg_uom    LIKE LINE OF gt_zarn_reg_uom.
  DATA lv_len             TYPE i.

  LOOP AT gt_zarn_reg_uom INTO ls_zarn_reg_uom.
    CLEAR ls_uom.
    ls_uom-idno = ls_zarn_reg_uom-idno.

    lv_len = strlen( ls_zarn_reg_uom-pim_uom_code ).
    lv_len = lv_len - 4.
    IF lv_len GE 4.
      IF ls_zarn_reg_uom-pim_uom_code+lv_len(4) CO '1234567890'
        AND ls_zarn_reg_uom-pim_uom_code IS NOT INITIAL.

        ls_uom-pim_uom_code = ls_zarn_reg_uom-pim_uom_code+lv_len(4).
        CONDENSE: ls_uom-pim_uom_code, ls_uom-idno.
        ls_uom-suffix = ls_uom-pim_uom_code.
        APPEND ls_uom TO lt_uom.
      ENDIF.
    ENDIF.

  ENDLOOP.

  SORT lt_uom BY suffix DESCENDING.

  CLEAR ls_uom.
  READ TABLE lt_uom INTO ls_uom INDEX 1.
  IF sy-subrc NE 0.
    " not found any
    ls_uom-idno = zarn_reg_hdr-idno.
    CONDENSE ls_uom-idno.
    ls_uom-pim_uom_code = ls_uom-idno && '_' && '0001'.
  ELSE.
    ls_uom-pim_uom_code = ls_uom-pim_uom_code + 1.
    CONDENSE: ls_uom-idno, ls_uom-pim_uom_code.
    lv_char = ls_uom-pim_uom_code.

    CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'
      EXPORTING
        input  = lv_char
      IMPORTING
        output = lv_char.

    ls_uom-pim_uom_code = ls_uom-idno && '_' && lv_char.
  ENDIF.

  IF ls_uom-pim_uom_code IS NOT INITIAL.
    p_c_pim_uom_code = ls_uom-pim_uom_code.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  SET_UOM_ROW_DISABLE
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM set_uom_row_disable USING pc_t_style TYPE lvc_t_styl.

  DATA ls_celltab   TYPE lvc_s_styl.
  DATA lt_celltab   TYPE lvc_t_styl.

  FREE pc_t_style[].

  ls_celltab-style = cl_gui_alv_grid=>mc_style_disabled.

  ls_celltab-fieldname = 'MANDT'.
  INSERT ls_celltab INTO TABLE lt_celltab.

  ls_celltab-fieldname = 'IDNO'.
  INSERT ls_celltab INTO TABLE lt_celltab.

  ls_celltab-fieldname = 'PIM_UOM_CODE'.
  INSERT ls_celltab INTO TABLE lt_celltab.

  ls_celltab-fieldname = 'LOWER_UOM'.
  INSERT ls_celltab INTO TABLE lt_celltab.

  ls_celltab-fieldname = 'LOWER_CHILD_UOM'.
  INSERT ls_celltab INTO TABLE lt_celltab.

  ls_celltab-fieldname = 'ERSDA'.
  INSERT ls_celltab INTO TABLE lt_celltab.

  ls_celltab-fieldname = 'ERZET'.
  INSERT ls_celltab INTO TABLE lt_celltab.

  ls_celltab-fieldname = 'CAT_SEQNO'.
  INSERT ls_celltab INTO TABLE lt_celltab.

  ls_celltab-fieldname = 'SEQNO'.
  INSERT ls_celltab INTO TABLE lt_celltab.

  ls_celltab-fieldname = 'HIGHER_LEVEL_UNITS'.     "++IR5119489 JKH 01.09.2016
  INSERT ls_celltab INTO TABLE lt_celltab.

*  ls_celltab-fieldname = 'LOWER_MEINH'.            "++IR5061015 JKH 07.09.2016
*  INSERT ls_celltab INTO TABLE lt_celltab.

  ls_celltab-fieldname = 'OBSOLETE_IND'.            "++IR5061015 JKH 07.09.2016
  INSERT ls_celltab INTO TABLE lt_celltab.


  pc_t_style[] = lt_celltab[].

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  ALV_DEL_REG_UOM
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM alv_del_reg_uom USING p_t_rows TYPE lvc_t_row.

  DATA ls_zarn_reg_uom LIKE LINE OF gt_zarn_reg_uom.
  DATA ls_rows         TYPE lvc_s_row.
  DATA lv_error        TYPE boole_d.
  DATA lt_fieldcat     TYPE lvc_t_fcat.

  FIELD-SYMBOLS:
    <fs_zarn_reg_uom> LIKE LINE OF gt_zarn_reg_uom.

  CLEAR lv_error.

  LOOP AT p_t_rows INTO ls_rows.

*   read selected articles from worklist
    READ TABLE gt_zarn_reg_uom INTO ls_zarn_reg_uom INDEX ls_rows-index.
    IF sy-subrc IS NOT INITIAL.
      CONTINUE.
    ENDIF.

    READ TABLE gt_zarn_uom_variant TRANSPORTING NO FIELDS
      WITH KEY uom_code = ls_zarn_reg_uom-pim_uom_code.
    IF sy-subrc EQ 0.
      lv_error = abap_true.
      MESSAGE s066 DISPLAY LIKE 'E' WITH ls_zarn_reg_uom-pim_uom_code 'exists in National data'.
      EXIT.
    ENDIF.

    READ TABLE gt_zarn_reg_ean TRANSPORTING NO FIELDS
      WITH KEY meinh = ls_zarn_reg_uom-meinh.
    IF sy-subrc EQ 0.
      lv_error = abap_true.
      MESSAGE s066 DISPLAY LIKE 'E' WITH ls_zarn_reg_uom-pim_uom_code 'exists in Regional EAN'.
      EXIT.
    ENDIF.

    IF ls_zarn_reg_uom-unit_base IS NOT INITIAL
      OR ls_zarn_reg_uom-issue_unit IS NOT INITIAL
      OR ls_zarn_reg_uom-unit_purord IS NOT INITIAL
      OR ls_zarn_reg_uom-sales_unit IS NOT INITIAL.

      lv_error = abap_true.
      MESSAGE s066 DISPLAY LIKE 'E' WITH ls_zarn_reg_uom-pim_uom_code 'cannot be base/order/issue/sales unit'.
      EXIT.
    ENDIF.

    IF ls_zarn_reg_uom-meinh IS NOT INITIAL.
      READ TABLE gt_zarn_reg_uom TRANSPORTING NO FIELDS
       WITH KEY lower_meinh = ls_zarn_reg_uom-meinh
                lower_uom = ''
                lower_child_uom = ''.
      IF sy-subrc EQ 0.

        lv_error = abap_true.
        MESSAGE s066 DISPLAY LIKE 'E' WITH ls_zarn_reg_uom-pim_uom_code 'used as lower UOM'.
        EXIT.
      ENDIF.
    ENDIF.

  ENDLOOP.

  IF lv_error IS INITIAL.
    LOOP AT p_t_rows INTO ls_rows.

*   read selected articles from worklist
      READ TABLE gt_zarn_reg_uom ASSIGNING <fs_zarn_reg_uom> INDEX ls_rows-index.
      IF sy-subrc IS INITIAL.
        CLEAR <fs_zarn_reg_uom>-idno.
      ENDIF.

    ENDLOOP.

    DELETE gt_zarn_reg_uom WHERE idno IS INITIAL.


    PERFORM refresh_reg_alv USING go_reg_uom_alv lt_fieldcat.

  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  ALV_DEL_REG_ONLCAT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
FORM alv_del_reg_onlcat USING p_t_rows TYPE lvc_t_row.

  DATA ls_zarn_reg_onlcat LIKE LINE OF gt_zarn_reg_onlcat.
  DATA ls_rows         TYPE lvc_s_row.
  DATA lv_error        TYPE boole_d.
  DATA lt_fieldcat     TYPE lvc_t_fcat.

  FIELD-SYMBOLS:
    <fs_zarn_reg_onlcat>  LIKE LINE OF gt_zarn_reg_onlcat.

  CLEAR lv_error.

  LOOP AT p_t_rows INTO ls_rows.
*   read selected articles from worklist
    READ TABLE gt_zarn_reg_onlcat INTO ls_zarn_reg_onlcat INDEX ls_rows-index.
    IF sy-subrc IS NOT INITIAL.
      CONTINUE.
    ENDIF.
  ENDLOOP.

  IF lv_error IS INITIAL.
    LOOP AT p_t_rows INTO ls_rows.

*   read selected articles from worklist
      READ TABLE gt_zarn_reg_onlcat ASSIGNING <fs_zarn_reg_onlcat> INDEX ls_rows-index.
      IF sy-subrc IS INITIAL.
        CLEAR <fs_zarn_reg_onlcat>-idno.
      ENDIF.

    ENDLOOP.

    DELETE gt_zarn_reg_onlcat WHERE idno IS INITIAL.

    PERFORM refresh_reg_alv USING go_reg_con_onlcat_alv lt_fieldcat.

  ENDIF.

ENDFORM.  " ALV_DEL_REG_ONLCAT
*&---------------------------------------------------------------------*
*&      Form  CHECK_DUPLICATES
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM check_duplicates CHANGING p_v_error TYPE boole_d.

  DATA lt_zarn_reg_rrp LIKE gt_zarn_reg_rrp.

  FREE lt_zarn_reg_rrp[].
  CLEAR p_v_error.

  lt_zarn_reg_rrp[] = gt_zarn_reg_rrp[].

  SORT lt_zarn_reg_rrp BY idno vkorg pltyp cond_unit condition_type.

  DELETE ADJACENT DUPLICATES FROM lt_zarn_reg_rrp
    COMPARING idno vkorg pltyp cond_unit condition_type.

  IF lines( lt_zarn_reg_rrp[] ) NE lines( gt_zarn_reg_rrp[] ).
    p_v_error = abap_true.
    MESSAGE s070 DISPLAY LIKE 'E'.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  F4_ONLINE_CATEGORIES
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
FORM f4_online_categories  USING    ps_zarn_reg_onlcat TYPE zarn_reg_onlcat
                                    pv_fieldname       TYPE lvc_fname
                                    ps_row_no          TYPE lvc_s_roid
                           CHANGING pcr_event_data     TYPE REF TO cl_alv_event_data.

  TYPES: BEGIN OF ty_s_category,
           onl_level       TYPE zonl_level,
           category        TYPE zonl_category_de,
           cat_description TYPE zonl_cat_description,
           onl_catalog     TYPE zonl_catalog,
         END OF ty_s_category,
         ty_t_category TYPE STANDARD TABLE OF ty_s_category.

  DATA: lt_category TYPE ty_t_category,
        ls_category TYPE ty_s_category,
        lt_ret      TYPE TABLE OF ddshretval,
        ls_sel      TYPE ddshretval,
        ls_modi     TYPE lvc_s_modi.


  FIELD-SYMBOLS: <itab> TYPE lvc_t_modi.


  CASE pv_fieldname.
    WHEN 'CATEGORY_1'.
      lt_category =  VALUE #( FOR ls_wa IN gt_onl_category
                       WHERE ( onl_catalog = ps_zarn_reg_onlcat-onl_catalog  AND
                               onl_level   = '1' )
                                  ( onl_level       = ls_wa-onl_level
                                    category        = ls_wa-category
                                    cat_description = ls_wa-cat_description
                                    onl_catalog     = ls_wa-onl_catalog )
                              ).

    WHEN 'CATEGORY_2'.
      IF ps_zarn_reg_onlcat-category_1 IS NOT INITIAL.
        lt_category =  VALUE #( FOR ls_wa IN gt_onl_category
                         WHERE ( onl_catalog = ps_zarn_reg_onlcat-onl_catalog  AND
                                 onl_level   = '2'                             AND
                                 parent_category = ps_zarn_reg_onlcat-category_1 )
                                    ( onl_level       = ls_wa-onl_level
                                      category        = ls_wa-category
                                      cat_description = ls_wa-cat_description
                                      onl_catalog     = ls_wa-onl_catalog )
                                ).
      ENDIF.

    WHEN 'CATEGORY_3'.
      IF ps_zarn_reg_onlcat-category_2 IS NOT INITIAL.
        lt_category =  VALUE #( FOR ls_wa IN gt_onl_category
                         WHERE ( onl_catalog = ps_zarn_reg_onlcat-onl_catalog  AND
                                 onl_level   = '3'                             AND
                                 parent_category = ps_zarn_reg_onlcat-category_2 )
                                    ( onl_level       = ls_wa-onl_level
                                      category        = ls_wa-category
                                      cat_description = ls_wa-cat_description
                                      onl_catalog     = ls_wa-onl_catalog )
                                ).
      ENDIF.


    WHEN 'CATEGORY_4'.
      IF ps_zarn_reg_onlcat-category_3 IS NOT INITIAL.
        lt_category =  VALUE #( FOR ls_wa IN gt_onl_category
                         WHERE ( onl_catalog = ps_zarn_reg_onlcat-onl_catalog  AND
                                 onl_level   = '4'                             AND
                                 parent_category = ps_zarn_reg_onlcat-category_3 )
                                    ( onl_level       = ls_wa-onl_level
                                      category        = ls_wa-category
                                      cat_description = ls_wa-cat_description
                                      onl_catalog     = ls_wa-onl_catalog )
                                ).
      ENDIF.


    WHEN 'CATEGORY_5'.
      IF ps_zarn_reg_onlcat-category_4 IS NOT INITIAL.
        lt_category =  VALUE #( FOR ls_wa IN gt_onl_category
                         WHERE ( onl_catalog = ps_zarn_reg_onlcat-onl_catalog  AND
                                 onl_level   = '5'                             AND
                                 parent_category = ps_zarn_reg_onlcat-category_4 )
                                    ( onl_level       = ls_wa-onl_level
                                      category        = ls_wa-category
                                      cat_description = ls_wa-cat_description
                                      onl_catalog     = ls_wa-onl_catalog )
                                ).
      ENDIF.


    WHEN 'CATEGORY_6'.
      IF ps_zarn_reg_onlcat-category_5 IS NOT INITIAL.
        lt_category =  VALUE #( FOR ls_wa IN gt_onl_category
                         WHERE ( onl_catalog = ps_zarn_reg_onlcat-onl_catalog  AND
                                 onl_level   = '6'                             AND
                                 parent_category = ps_zarn_reg_onlcat-category_5 )
                                    ( onl_level       = ls_wa-onl_level
                                      category        = ls_wa-category
                                      cat_description = ls_wa-cat_description
                                      onl_catalog     = ls_wa-onl_catalog )
                                ).
      ENDIF.


  ENDCASE.




*-- Call the function module to display the custom F4 values
  CLEAR: lt_ret[].
  CALL FUNCTION 'F4IF_INT_TABLE_VALUE_REQUEST'
    EXPORTING
      retfield        = 'CATEGORY'
      window_title    = 'Online Category'
      value_org       = 'S'
      display         = gv_display
    TABLES
      value_tab       = lt_category[]
      return_tab      = lt_ret[]
    EXCEPTIONS
      parameter_error = 1
      no_values_found = 2
      OTHERS          = 3.
  IF sy-subrc = 0.
    CLEAR ls_sel.
    READ TABLE lt_ret[] INTO ls_sel INDEX 1.
    IF ls_sel-fieldval IS NOT INITIAL.
      ASSIGN pcr_event_data->m_data->* TO <itab>.
      ls_modi-row_id    = ps_row_no-row_id.
      ls_modi-fieldname = pv_fieldname.
      ls_modi-value     = ls_sel-fieldval.
      APPEND ls_modi TO <itab>.
    ENDIF.
  ENDIF.

  pcr_event_data->m_event_handled = 'X'.



ENDFORM.

FORM save_regional_data.

  PERFORM process_regional_data USING abap_true.

  " Post changes if fully approved
  go_approval->load_recent_approval_data( ).

  DATA(lv_nat_approved) = go_approval->is_fully_approved( iv_approval_area          = zcl_arn_approval_backend=>gc_appr_area_national
                                                          iv_no_records_as_approved = abap_true ).
  DATA(lv_reg_approved) = go_approval->is_fully_approved( zcl_arn_approval_backend=>gc_appr_area_regional ).

  IF lv_nat_approved = abap_true
 AND lv_reg_approved = abap_true.
    PERFORM appr_post_article.

    TRY.
        go_approval->enqueue_appr_idno_or_wait( zif_enq_mode_c=>optimistic ).
      CATCH zcx_excep_cls.
        MESSAGE a099.
    ENDTRY.
  ENDIF.

ENDFORM.
