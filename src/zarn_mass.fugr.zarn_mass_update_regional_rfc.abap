FUNCTION zarn_mass_update_regional_rfc .
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(SELDATA) TYPE  MASS_TABDATA
*"     VALUE(TESTMODE) TYPE  FLAG OPTIONAL
*"     VALUE(IV_IMPORTANT_MSGS_ONLY) TYPE  XFELD DEFAULT SPACE
*"     VALUE(IV_COMMIT) TYPE  XFELD DEFAULT SPACE
*"  EXPORTING
*"     VALUE(ET_MESSAGE) TYPE  BAL_T_MSG
*"  TABLES
*"      SZARN_REG_BANNER STRUCTURE  ZARN_REG_BANNER OPTIONAL
*"      SZARN_REG_DC_SELL STRUCTURE  ZARN_REG_DC_SELL OPTIONAL
*"      SZARN_REG_EAN STRUCTURE  ZARN_REG_EAN OPTIONAL
*"      SZARN_REG_HDR STRUCTURE  ZARN_REG_HDR OPTIONAL
*"      SZARN_REG_LST_PRC STRUCTURE  ZARN_REG_LST_PRC OPTIONAL
*"      SZARN_REG_PIR STRUCTURE  ZARN_REG_PIR OPTIONAL
*"      SZARN_REG_PRFAM STRUCTURE  ZARN_REG_PRFAM OPTIONAL
*"      SZARN_REG_RRP STRUCTURE  ZARN_REG_RRP OPTIONAL
*"      SZARN_REG_STD_TER STRUCTURE  ZARN_REG_STD_TER OPTIONAL
*"      SZARN_REG_TXT STRUCTURE  ZARN_REG_TXT OPTIONAL
*"      SZARN_REG_UOM STRUCTURE  ZARN_REG_UOM OPTIONAL
*"      SZARN_REG_CLUSTER STRUCTURE  ZARN_REG_CLUSTER OPTIONAL
*"      MSG STRUCTURE  MASSMSG OPTIONAL
*"----------------------------------------------------------------------
* DATE             # 23.10.2019
* CHANGE No.       # SUP-317
* DESCRIPTION      # EINE table not updating EKKO field. Introduced commit
*                  # work as EINE table getting updated in UPDATE TASK and
*                  # there seems to be no commit work issues by framework
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* DATE             # 27.07.2020
* CHANGE No.       # SSM-1: NW Range-ZARN_GUI CHANGE
* DESCRIPTION      # Added ZARN_REG_CLUSTER for mass maintenance
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
  DATA: lo_mass_update TYPE REF TO zcl_arn_mass_update,
        lt_message     TYPE bal_t_msg.

  ">>>IS 1902 - enable mode returning only important message
  "previously methods like FILTER_REG_BANNER_DATA inside the class
  "were returning error messages which actually didnt have any impact
  "and the update of AReNa / SAP article happened anyway - calling application couldnt tell
  "if it was success or error

  CREATE OBJECT lo_mass_update EXPORTING iv_important_msgs_only = iv_important_msgs_only.

  "<<<IS 1902


  CALL METHOD lo_mass_update->regional_mass_update_process
    EXPORTING
      it_seldata     = seldata[]
      iv_testmode    = testmode
    IMPORTING
      et_msg         = msg[]
      et_message     = et_message[]
    CHANGING
      ct_reg_banner  = szarn_reg_banner[]
      ct_reg_dc_sell = szarn_reg_dc_sell[]
      ct_reg_ean     = szarn_reg_ean[]
      ct_reg_hdr     = szarn_reg_hdr[]
      ct_reg_lst_prc = szarn_reg_lst_prc[]
      ct_reg_pir     = szarn_reg_pir[]
      ct_reg_prfam   = szarn_reg_prfam[]
      ct_reg_rrp     = szarn_reg_rrp[]
      ct_reg_std_ter = szarn_reg_std_ter[]
      ct_reg_txt     = szarn_reg_txt[]
      ct_reg_uom     = szarn_reg_uom[]
      ct_reg_cluster = szarn_reg_cluster[]
    EXCEPTIONS
      error          = 1
      OTHERS         = 2.

  " Ideally commit work shouldn't be issued when at least one error exists.
  " However, the existing logic is updating records even though some error exists.
  " This needs clear understanding before stopping the updates completely when
  " error exists. Hence, no check performed before commiting
  IF iv_commit = abap_true.
    COMMIT WORK.
  ENDIF.

ENDFUNCTION.
