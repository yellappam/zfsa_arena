*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_NAT_RECONCILIATION_MAIN
*&---------------------------------------------------------------------*

START-OF-SELECTION.

* Get data from Hybris
  PERFORM get_data_from_hybris CHANGING gt_prd_version[]
                                        gt_ver_status[]
                                        gt_cc_det[]
                                        gt_prod_data[].

* Build Product Version ALV data
  IF gt_prod_data IS NOT INITIAL.
    PERFORM build_prdver_alv_data USING gt_prd_version[]
                                        gt_ver_status[]
                                        gt_cc_det[]
                                        gt_prod_data[]
                               CHANGING gt_prdver_alv[]
                                        gt_idno_data[].
  ENDIF.

END-OF-SELECTION.

  CALL SCREEN 9000.
