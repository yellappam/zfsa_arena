FUNCTION zarn_mass_limit_sel_fields.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(MODE_ID) TYPE  I
*"  TABLES
*"      TABLIST STRUCTURE  MASSTABLIST
*"      FIELDLIST STRUCTURE  MASSFIELDLIST
*"      FIELDEXCL STRUCTURE  MASSFIELDLIST
*"----------------------------------------------------------------------

  DATA: lv_lines TYPE sy-dbcnt.


  DESCRIBE TABLE tablist[] LINES lv_lines.

  IF lv_lines GT 1 AND sy-ucomm = 'NEXT'.
* Only records having common matching keys will be displayed.
    MESSAGE s108(zarena_msg).
*    LEAVE TO TRANSACTION 'ZARN_MASS'.
  ENDIF.


ENDFUNCTION.
