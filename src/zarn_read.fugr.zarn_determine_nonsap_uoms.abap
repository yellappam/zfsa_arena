FUNCTION zarn_determine_nonsap_uoms.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(IV_IDNO) TYPE  ZARN_IDNO
*"     REFERENCE(IS_REG_HDR) TYPE  ZARN_REG_HDR OPTIONAL
*"     REFERENCE(IT_NSAP_LEG_UOM) TYPE  ZTMD_NSAP_LEG_UOM OPTIONAL
*"     REFERENCE(IT_REG_UOM) TYPE  ZTARN_REG_UOM OPTIONAL
*"  EXPORTING
*"     REFERENCE(ES_REG_HDR) TYPE  ZARN_REG_HDR
*"  EXCEPTIONS
*"      INPUT_REQD
*"----------------------------------------------------------------------

  DATA: lt_nsap_leg_uom_db TYPE ztmd_nsap_leg_uom,
        ls_nsap_leg_uom_db TYPE zmd_nsap_leg_uom,
        lt_reg_uom         TYPE ztarn_reg_uom,
        ls_reg_uom         TYPE zarn_reg_uom,
        ls_reg_hdr         TYPE zarn_reg_hdr,
        lv_idno            TYPE zarn_idno,
        lv_pur_only_flag   TYPE flag,      "++ONED-217 JKH 24.11.2016

        lt_nsap_leg_uom    TYPE ztmd_nsap_leg_uom,
        ls_nsap_leg_uom    TYPE zmd_nsap_leg_uom,

        lt_matkl_range     TYPE RANGE OF matkl,
        ls_matkl_range     LIKE LINE OF lt_matkl_range,

        lt_reg_uom_ui      TYPE ztarn_reg_uom_ui,
        ls_reg_uom_ui      TYPE zsarn_reg_uom_ui,
        ls_reg_uom_ret     TYPE zsarn_reg_uom_ui,
        ls_reg_uom_iss     TYPE zsarn_reg_uom_ui,
        ls_reg_uom_ord     TYPE zsarn_reg_uom_ui,


        lv_retail_cat      TYPE zmd_retail_cat,
        lv_issue_cat       TYPE zmd_issue_cat,
        lv_order_cat       TYPE zmd_order_cat.






* Check input Required
  IF iv_idno IS INITIAL.
*  OR is_reg_hdr IS INITIAL.
    RAISE input_reqd.
  ENDIF.


  lv_idno              = iv_idno.
  ls_reg_hdr           = is_reg_hdr.
  lt_nsap_leg_uom_db[] = it_nsap_leg_uom[].
  lt_reg_uom[]         = it_reg_uom[].

  IF ls_reg_hdr IS INITIAL.
* Get Reg_Hdr data
    SELECT SINGLE * FROM zarn_reg_hdr INTO ls_reg_hdr
      WHERE idno = lv_idno.
  ENDIF.

  IF lt_nsap_leg_uom_db[] IS INITIAL.
* Get Non-SAP Legacy UOMs
    SELECT *
      FROM zmd_nsap_leg_uom
      INTO CORRESPONDING FIELDS OF TABLE lt_nsap_leg_uom_db[].
  ENDIF.

  IF lt_reg_uom[] IS INITIAL.
* Get Regional UOMs
    SELECT *
      FROM zarn_reg_uom
      INTO CORRESPONDING FIELDS OF TABLE lt_reg_uom[]
      WHERE idno = lv_idno.
  ENDIF.

  CLEAR lt_reg_uom_ui[].
  MOVE-CORRESPONDING lt_reg_uom[] TO lt_reg_uom_ui[].






  IF ls_reg_hdr-leg_retail IS NOT INITIAL AND
     ls_reg_hdr-leg_repack IS NOT INITIAL AND
     ls_reg_hdr-leg_bulk   IS NOT INITIAL. " AND
*     ls_reg_hdr-leg_prboly IS NOT INITIAL.
    EXIT.
  ENDIF.

  CLEAR : ls_reg_uom_ret,
          ls_reg_uom_iss,
          ls_reg_uom_ord.


* Retail Category
  CLEAR lv_retail_cat.
  READ TABLE lt_reg_uom_ui INTO ls_reg_uom_ret
  WITH KEY idno = ls_reg_hdr-idno
           unit_base = abap_true.
  IF sy-subrc = 0.
    lv_retail_cat = ls_reg_uom_ret-uom_category.
  ELSE.
    EXIT.
  ENDIF.

* Issue Category
  CLEAR lv_issue_cat.
  READ TABLE lt_reg_uom_ui INTO ls_reg_uom_iss
  WITH KEY idno = ls_reg_hdr-idno
           issue_unit = abap_true.
  IF sy-subrc = 0.
    lv_issue_cat = ls_reg_uom_iss-uom_category.
  ELSE.
    EXIT.
  ENDIF.


* Order Category
  CLEAR lv_order_cat.
  READ TABLE lt_reg_uom_ui INTO ls_reg_uom_ord
  WITH KEY idno = ls_reg_hdr-idno
           unit_purord = abap_true.
  IF sy-subrc = 0.
    lv_order_cat = ls_reg_uom_ord-uom_category.
  ELSE.
    EXIT.
  ENDIF.



* If any Category is RETAIL, then consider on unit, for others consider Category only

  IF lv_retail_cat = 'RETAIL'.
    lv_retail_cat = ls_reg_uom_ret-meinh.
  ENDIF.

  IF lv_issue_cat = 'RETAIL'.
    lv_issue_cat = ls_reg_uom_iss-meinh.
  ENDIF.

  IF lv_order_cat = 'RETAIL'.
    lv_order_cat = ls_reg_uom_ord-meinh.
  ENDIF.



* Filter Non-SAP Legacy UOMs on MC, ignore if MC is not matching
  LOOP AT lt_nsap_leg_uom_db[] INTO ls_nsap_leg_uom_db.

    CLEAR: ls_matkl_range, lt_matkl_range[].

    IF ls_nsap_leg_uom_db-matkl CS '*'.
      ls_matkl_range-sign = 'I'.
      ls_matkl_range-option = 'CP'.
      ls_matkl_range-low = ls_nsap_leg_uom_db-matkl.
      APPEND ls_matkl_range TO lt_matkl_range[].
    ELSE.
      ls_matkl_range-sign = 'I'.
      ls_matkl_range-option = 'EQ'.
      ls_matkl_range-low = ls_nsap_leg_uom_db-matkl.
      APPEND ls_matkl_range TO lt_matkl_range[].
    ENDIF.


    IF ls_reg_hdr-matkl IN lt_matkl_range[].

* If any Category is RETAIL, then consider on unit, for others consider Category only
      IF ls_nsap_leg_uom_db-retail_cat = 'RETAIL'.
        ls_nsap_leg_uom_db-retail_cat = ls_nsap_leg_uom_db-retail_uom.
      ENDIF.

      IF ls_nsap_leg_uom_db-issue_cat = 'RETAIL'.
        ls_nsap_leg_uom_db-issue_cat = ls_nsap_leg_uom_db-issue_uom.
      ENDIF.

      IF ls_nsap_leg_uom_db-order_cat = 'RETAIL'.
        ls_nsap_leg_uom_db-order_cat = ls_nsap_leg_uom_db-order_uom.
      ENDIF.


      APPEND ls_nsap_leg_uom_db TO lt_nsap_leg_uom[].
    ENDIF.

  ENDLOOP.  " LOOP AT lt_nsap_leg_uom_db[] INTO ls_nsap_leg_uom_db


  IF lt_nsap_leg_uom[] IS INITIAL.
    EXIT.
  ENDIF.

* Ignore Irrelevant Unit/Categories
  DELETE lt_nsap_leg_uom[] WHERE retail_cat NE lv_retail_cat.
  DELETE lt_nsap_leg_uom[] WHERE issue_cat  NE lv_issue_cat.
  DELETE lt_nsap_leg_uom[] WHERE order_cat  NE lv_order_cat.



  IF lt_nsap_leg_uom[] IS INITIAL.
    EXIT.
  ENDIF.


* INS Begin of Change ONED-217 JKH 24.11.2016
  CLEAR lv_pur_only_flag.
  IF ls_reg_hdr-zzbuy_sell = zcl_constants=>gc_buy_sell_3 OR     " '3' - Buying only
     ls_reg_hdr-pur_only   = abap_true.
    lv_pur_only_flag = abap_true.
  ENDIF.
* INS End of Change ONED-217 JKH 24.11.2016




* Get Exactly matching Record
  CLEAR ls_nsap_leg_uom.
  READ TABLE lt_nsap_leg_uom[] INTO ls_nsap_leg_uom
  WITH KEY retail_cat = lv_retail_cat
           issue_cat  = lv_issue_cat
           order_cat  = lv_order_cat
           pur_only   = lv_pur_only_flag.                 "++ONED-217 JKH 24.11.2016
*           pur_only   = ls_reg_hdr-pur_only.             "--ONED-217 JKH 24.11.2016
  IF sy-subrc NE 0.
* If not found then get based on Unit/Categor Only
    READ TABLE lt_nsap_leg_uom[] INTO ls_nsap_leg_uom
    WITH KEY retail_cat = lv_retail_cat
             issue_cat  = lv_issue_cat
             order_cat  = lv_order_cat.
  ENDIF.


* Assigh Categories only is initial
  IF ls_nsap_leg_uom IS NOT INITIAL.

    IF ls_reg_hdr-leg_retail IS INITIAL.
      ls_reg_hdr-leg_retail = ls_nsap_leg_uom-leg_retail.
    ENDIF.

    IF ls_reg_hdr-leg_repack IS INITIAL.
      ls_reg_hdr-leg_repack = ls_nsap_leg_uom-leg_repack.
    ENDIF.

    IF ls_reg_hdr-leg_bulk   IS INITIAL.
      ls_reg_hdr-leg_bulk = ls_nsap_leg_uom-leg_bulk.
    ENDIF.

    ls_reg_hdr-leg_prboly = ls_nsap_leg_uom-leg_prboly.

  ENDIF.

  es_reg_hdr = ls_reg_hdr.


ENDFUNCTION.
