*&---------------------------------------------------------------------*
*&  Include           ZARN_REG_SYNCUP_MAIN
*&---------------------------------------------------------------------*
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 13.04.2018
* CHANGE No.       # Charm 9000003482; Jira CI18-554
* DESCRIPTION      # CI18-554 Recipe Management - Ingredient Changes
*                  # New regional table for Allergen Type overrides and
*                  # fields for Ingredients statement overrides in Arena.
*                  # New Ingredients Type field in the Scales block in
*                  # Arena and NONSAP tab in material master.
* WHO              # C90005557, Tessa Newman
*-----------------------------------------------------------------------
* DATE             # 06.06.2018
* CHANGE No.       # ChaRM 9000003675; JIRA CI18-506 NEW Range Status flag
* DESCRIPTION      # New Range Flag, Range Availability and Range Detail
*                  # fields added in Arena and on material master in Listing
*                  # tab and Basic Data Text tab (materiallongtext).
* WHO              # C90005557, Tessa Newman
*-----------------------------------------------------------------------
*&---------------------------------------------------------------------*
*&      Form  FILE_OPEN_DIALOG
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      <--P_P_I_MAIN  text
*----------------------------------------------------------------------*
FORM file_open_dialog CHANGING  fc_v_io_f TYPE rlgrap-filename.

  DATA : lt_retfiletable  TYPE filetable,
         lv_retrc         TYPE sysubrc,
         lv_retuseraction TYPE i.

  CONSTANTS: lc_file_filter TYPE string  VALUE '*.xls',
             lc_default_ext TYPE string  VALUE 'xls'.

*** Get F4 for Input File Name
  CALL METHOD cl_gui_frontend_services=>file_open_dialog
    EXPORTING
      multiselection    = abap_false
      file_filter       = lc_file_filter "'*.xls'
      default_extension = lc_default_ext "'xls'
    CHANGING
      file_table        = lt_retfiletable
      rc                = lv_retrc
      user_action       = lv_retuseraction.

  CLEAR fc_v_io_f.
  READ TABLE lt_retfiletable INTO fc_v_io_f INDEX 1.
  IF sy-subrc NE 0.
    CLEAR fc_v_io_f.
  ENDIF.

ENDFORM.                    " FILE_OPEN_DIALOG
*&---------------------------------------------------------------------*
*&      Form  EXECUTE
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM execute .

  DATA lv_file_name TYPE string.

  lv_file_name = 'MAIN'.
  PERFORM convert_excel_to_int_tbl USING p_i_main lv_file_name.

  DELETE ADJACENT DUPLICATES FROM gt_main[] COMPARING zzfan.        " ++3169 JKH 27.10.2016


  " If 'MAIN' upload file is empty then give error and exit further execution
  IF gt_main[] IS INITIAL.
    MESSAGE TEXT-004 TYPE 'S'.
    RETURN.
  ENDIF.

  lv_file_name = 'UOM'.
  PERFORM convert_excel_to_int_tbl USING p_i_uom lv_file_name.

  DELETE ADJACENT DUPLICATES FROM gt_uom[] COMPARING ALL FIELDS.    " ++3169 JKH 27.10.2016


  lv_file_name = 'PIR'.
  PERFORM convert_excel_to_int_tbl USING p_i_pir lv_file_name.

  DELETE ADJACENT DUPLICATES FROM gt_pir[] COMPARING ALL FIELDS.    " ++3169 JKH 27.10.2016




  " get data from SAP master data tables
  PERFORM get_data_from_sap_tbl.

  IF gt_arn_sync[] IS INITIAL AND p_migrtn IS INITIAL.
    MESSAGE TEXT-005 TYPE 'S'.
    RETURN.
  ENDIF.

  IF gt_mara[] IS INITIAL.
    MESSAGE TEXT-004 TYPE 'S'.
    RETURN.
  ENDIF.

  " get data from ARENA national tables
  PERFORM get_data_from_arena_tbl.



  IF p_migrtn IS NOT INITIAL.
    " Fill Data into internal tables to update ARENA regional tables
    PERFORM fill_arena_tables_migrate.
    " Get existing data from Arena regional tables which needs to be updated
    PERFORM get_data_to_update_migrate.
  ELSE.

* INS Begin of Change 3169 JKH 20.10.2016
    " get data from ARENA regional tables
    PERFORM get_data_from_arena_reg_tbl.             " ++3169 JKH 27.10.2016

    " Fill Data into internal tables to update ARENA regional tables
    PERFORM fill_arena_tables_bau.

    " Get existing data from Arena regional tables which needs to be updated
    PERFORM get_data_to_update_bau.
* INS End of Change 3169 JKH 20.10.2016


* DEL Begin of Change 3169 JKH 20.10.2016
*    " Fill Data into internal tables to update ARENA regional tables
*    PERFORM fill_arena_tables.
*    " Get existing data from Arena regional tables which needs to be updated
*    PERFORM get_data_to_update.
* DEL End of Change 3169 JKH 20.10.2016






  ENDIF.



  " Update data to Arena regional tables
  PERFORM update_data_to_db.

  IF gt_arn_reg_hdr_in_db[] IS INITIAL.
    MESSAGE TEXT-004 TYPE 'S'.
  ENDIF.

ENDFORM.                    " EXECUTE
*&---------------------------------------------------------------------*
*&      Form  CONVERT_EXCEL_TO_INT_TBL
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->P_P_I_MAIN  text
*      -->P_GT_MAIN  text
*----------------------------------------------------------------------*
FORM convert_excel_to_int_tbl  USING p_p_i_main TYPE localfile
                               p_v_file_name TYPE string.

  TYPES :BEGIN OF ty_rawdata,
           data TYPE  char1250,
         END OF ty_rawdata.

  DATA:
    lt_intern       TYPE TABLE OF alsmex_tabline,
    ls_intern       TYPE alsmex_tabline,
    lv_index        TYPE i,
    ls_file_main    TYPE ty_s_main,
    ls_file_uom     TYPE ty_s_uom,
    ls_file_pir     TYPE ty_s_pir,
    lv_filename     TYPE string,
    lv_filename_new TYPE rlgrap-filename,
    lt_file_data    TYPE STANDARD TABLE OF ty_rawdata,
    lv_lines        TYPE i,
    lv_brow         TYPE i VALUE -9998,
    lv_erow         TYPE i VALUE 0,
    lv_lin          TYPE i VALUE 1,
    lv_ecol         TYPE i,
    ls_main         TYPE ty_s_main,
    ls_uom          TYPE ty_s_uom,
    ls_pir          TYPE ty_s_pir.

  FIELD-SYMBOLS : <lv_val>.

  lv_filename = p_p_i_main.

  IF lv_filename CS  '.xls'.

    lv_filename_new = lv_filename.

    CALL FUNCTION 'UPLOAD_XLS_FILE_2_ITAB'
      EXPORTING
        i_filename = lv_filename_new
      TABLES
        e_itab     = lt_file_data
      EXCEPTIONS
        file_error = 1
        OTHERS     = 2.
    IF sy-subrc <> 0.
      RETURN.
* Implement suitable error handling here
    ENDIF.

    lv_lines = lines( lt_file_data ) - 1.

    lv_lin = lv_lines DIV 10000.
    ADD 10000 TO lv_brow.
    ADD 10000 TO lv_erow.

    " No. of times the function 'ALSM_EXCEL_TO_INTERNAL_TABLE' needs to be called for data load
    DO ( lv_lin + 1 ) TIMES.

      CLEAR  lt_intern.
      REFRESH  lt_intern[].

      CASE p_v_file_name.
        WHEN 'MAIN'.
          lv_ecol = 2.
        WHEN 'UOM'.
          lv_ecol = 4.
        WHEN 'PIR'.
          lv_ecol = 3.
        WHEN OTHERS.
      ENDCASE.

      " Since function 'ALSM_EXCEL_TO_INTERNAL_TABLE' can't handle more than '9999' records at once,
      " looping this Function with the number of executions required for full data load
      CALL FUNCTION 'ALSM_EXCEL_TO_INTERNAL_TABLE'
        EXPORTING
          filename                = p_p_i_main
          i_begin_col             = 1
          i_begin_row             = lv_brow
          i_end_col               = lv_ecol
          i_end_row               = lv_erow
        TABLES
          intern                  = lt_intern
        EXCEPTIONS
          inconsistent_parameters = 1
          upload_ole              = 2
          OTHERS                  = 3.

      IF sy-subrc <> 0.
* Implement suitable error handling here
        RETURN.
      ENDIF.

      ADD 9999 TO lv_brow.
      ADD 9999 TO lv_erow.

      SORT lt_intern BY row col.

      LOOP AT lt_intern INTO ls_intern.

        MOVE ls_intern-col TO lv_index.
        CASE p_v_file_name.
          WHEN 'MAIN'.
            ASSIGN COMPONENT lv_index OF STRUCTURE ls_file_main TO <lv_val>.
          WHEN 'UOM'.
            ASSIGN COMPONENT lv_index OF STRUCTURE ls_file_uom TO <lv_val>.
          WHEN 'PIR'.
            ASSIGN COMPONENT lv_index OF STRUCTURE ls_file_pir TO <lv_val>.
          WHEN OTHERS.
        ENDCASE.

        IF sy-subrc IS INITIAL.
          TRY .
              MOVE ls_intern-value TO <lv_val>.
            CATCH cx_sy_conversion_no_number.
            CATCH cx_sy_conversion_overflow.
            CATCH cx_sy_move_cast_error.
          ENDTRY.

        ENDIF.

        AT END OF row.
          CASE p_v_file_name.
            WHEN 'MAIN'.
              MOVE-CORRESPONDING ls_file_main TO ls_main.
**              Update the values to global table
              INSERT ls_main INTO TABLE gt_main. CLEAR: ls_main, ls_file_main.

            WHEN 'UOM'.
              MOVE-CORRESPONDING ls_file_uom TO ls_uom.
**              Update the values to global table
              INSERT ls_uom INTO TABLE gt_uom. CLEAR: ls_uom, ls_file_uom.

            WHEN 'PIR'.
              MOVE-CORRESPONDING ls_file_pir TO ls_pir.
**              Update the values to global table
              INSERT ls_pir INTO TABLE gt_pir. CLEAR: ls_pir, ls_file_pir.

            WHEN OTHERS.
          ENDCASE.

        ENDAT.

      ENDLOOP.

    ENDDO.

  ELSE.
    MESSAGE 'File extension .xls only supported' TYPE 'E'.
  ENDIF.

ENDFORM.                    " CONVERT_EXCEL_TO_INT_TBL
*&---------------------------------------------------------------------*
*&      Form  GET_DATA_FROM_SAP_TBL
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM get_data_from_sap_tbl .

  DATA ls_tvarvc                TYPE tvarvc.
  DATA lsr_numtp                LIKE LINE OF gtr_numtp.
  DATA lsr_uoms_range_tbl       LIKE LINE OF gtr_layer_uoms.
  DATA ls_uom_cat               TYPE zarn_uom_cat.
  DATA ls_ref_sites_lni         LIKE LINE OF gt_tvarv_ref_sites_lni.
  DATA ls_products              TYPE ty_s_arn_products.            " ++3169 JKH 31.10.2016

  CONSTANTS:
    lc_ref_sites_uni TYPE rvari_vnam VALUE 'ZC_REF_VKORG_VTREFS',
    lc_ref_sites_lni TYPE rvari_vnam VALUE 'ZMD_VKORG_REF_SITES_LNI'.

  FIELD-SYMBOLS: <ls_main> TYPE ty_s_main.                          " ++3169 JKH 31.10.2016


  FREE: gtr_numtp, gt_uom_cat.

  " get sync params from DB table for all selected entries
  SELECT * FROM zarn_sync
    INTO TABLE gt_arn_sync
    WHERE syncup = abap_true.
  IF sy-subrc NE 0 AND p_migrtn IS INITIAL.
    RETURN.
  ENDIF.

  CHECK gt_main[] IS NOT INITIAL.


* INS Begin of Change 3169 JKH 31.10.2016
  CLEAR gt_arn_products[].
  SELECT  a~idno a~version a~fan_id
    INTO CORRESPONDING FIELDS OF TABLE gt_arn_products
    FROM zarn_products AS a
    JOIN zarn_ver_status AS b
    ON a~idno = b~idno
    AND a~version = b~current_ver
    FOR ALL ENTRIES IN gt_main[]
    WHERE a~fan_id = gt_main-zzfan.

  LOOP AT gt_main[] ASSIGNING <ls_main> WHERE version IS INITIAL.
    CLEAR ls_products.
    READ TABLE gt_arn_products[] INTO ls_products
    WITH KEY fan_id = <ls_main>-zzfan.
    IF sy-subrc = 0.
      <ls_main>-version = ls_products-version.
    ENDIF.
  ENDLOOP.
* INS End of Change 3169 JKH 31.10.2016


  " Read MARA table for the FANs in the MAIN upload file
  SELECT * FROM mara INTO TABLE gt_mara
    FOR ALL ENTRIES IN gt_main[]
    WHERE zzfan = gt_main-zzfan.

  IF sy-subrc EQ 0.
    " Read MARM
    SELECT matnr meinh umrez umren mesub
           laeng breit hoehe meabm brgew gewei                   " ++3169 JKH 27.10.2016
      FROM marm
      INTO CORRESPONDING FIELDS OF TABLE gt_marm[]
      FOR ALL ENTRIES IN gt_mara
      WHERE matnr = gt_mara-matnr.



* INS Begin of Change 3169 JKH 20.10.2016
    " GET ALL THE VALID ID NUMBERS FOR THE UPLOAD FILE FANS IDS
    CLEAR gt_arn_products[].
    SELECT fan_id idno version irradiated "#EC CI_NOFIELD  "#EC CI_NOWHERE  "#EC CI_NOFIRST

           dgi_packing_group dgi_regulation_code dgi_technical_name
           dgi_hazardous_code dgi_unn_code dgi_unn_shipping_name
           dgi_class hsno_app hsno_hsr flash_pt_temp_val flash_pt_temp_uom
           dangerous_good hazardous_good
      FROM zarn_products INTO CORRESPONDING FIELDS OF TABLE gt_arn_products
      FOR ALL ENTRIES IN gt_main[]
      WHERE fan_id  = gt_main-zzfan
        AND version = gt_main-version.
* INS End of Change 3169 JKH 20.10.2016


* DEL Begin of Change 3169 JKH 20.10.2016
*    " GET ALL THE VALID ID NUMBERS FOR THE UPLOAD FILE FANS IDS
*    SELECT fan_id idno version irradiated FROM zarn_products INTO TABLE gt_arn_products
*      FOR ALL ENTRIES IN gt_main[]
*      WHERE fan_id = gt_main-zzfan.
* DEL End of Change 3169 JKH 20.10.2016


    IF sy-subrc EQ 0.
      " GET ALL THE VALID VERSIONS FOR THE IDNOs
      SELECT idno version  FROM zarn_prd_version INTO TABLE gt_arn_prd_ver
        FOR ALL ENTRIES IN gt_arn_products[]
        WHERE idno = gt_arn_products-idno
          AND version = gt_arn_products-version.                       " ++3169 JKH 31.10.2016

      " GET ALL THE VALID VERSIONS FOR THE IDNOs
      SELECT idno current_ver  FROM zarn_ver_status INTO TABLE gt_arn_curr_prd_ver
        FOR ALL ENTRIES IN gt_arn_products[]
        WHERE idno = gt_arn_products-idno.

      SELECT * FROM zarn_growing INTO TABLE gt_arn_growing
        FOR ALL ENTRIES IN gt_arn_products[]
        WHERE idno = gt_arn_products-idno
          AND version = gt_arn_products-version.                       " ++3169 JKH 31.10.2016

      SELECT * FROM zarn_diet_info INTO TABLE gt_arn_diet_info
        FOR ALL ENTRIES IN gt_arn_products[]
        WHERE idno = gt_arn_products-idno
          AND version = gt_arn_products-version.                       " ++3169 JKH 31.10.2016

      SELECT * FROM zarn_allergen INTO TABLE gt_arn_allergen
        FOR ALL ENTRIES IN gt_arn_products[]
        WHERE idno = gt_arn_products-idno
          AND version = gt_arn_products-version.                       " ++3169 JKH 31.10.2016

      SELECT * FROM zarn_ntr_claims INTO TABLE gt_arn_ntr_claims
        FOR ALL ENTRIES IN gt_arn_products[]
        WHERE idno = gt_arn_products-idno
          AND version = gt_arn_products-version.                       " ++3169 JKH 31.10.2016

    ENDIF.

    SELECT * FROM makt INTO TABLE gt_makt
      FOR ALL ENTRIES IN gt_mara[]
      WHERE matnr = gt_mara-matnr
      AND spras = sy-langu.

    " Gilmours category manager
    SELECT matnr zzcatman FROM mvke INTO TABLE gt_gil_zzcatman
      FOR ALL ENTRIES IN gt_mara[]
      WHERE matnr = gt_mara-matnr
      AND vkorg EQ '3000'
      AND vtweg EQ '20'.


    " retail category manager
    SELECT matnr vkorg zzcatman FROM mvke INTO TABLE gt_ret_zzcatman
      FOR ALL ENTRIES IN gt_mara[]
      WHERE matnr = gt_mara-matnr
      AND ( vkorg EQ zcl_constants=>gc_vkorg_4000 OR vkorg EQ zcl_constants=>gc_vkorg_5000 OR vkorg EQ zcl_constants=>gc_vkorg_6000 )
      AND vtweg EQ '20'
      AND zzcatman NE ''.

    " retail- 1000/10 category manager
    SELECT matnr zzcatman FROM mvke INTO TABLE gt_retdc_zzcatman
      FOR ALL ENTRIES IN gt_mara[]
      WHERE matnr = gt_mara-matnr
      AND vkorg EQ zcl_constants=>gc_banner_1000
      AND vtweg EQ zcl_constants=>gc_distr_chan_10.

    " get latest products
    SELECT * FROM zmd_host_sap INTO TABLE gt_host_sap
      FOR ALL ENTRIES IN gt_mara[]
      WHERE matnr = gt_mara-matnr
      AND latest EQ zcl_constants=>gc_true.

    " get records for all banners and later select the banner sepc.
    SELECT * FROM wlk2 INTO TABLE gt_wlk2
      FOR ALL ENTRIES IN gt_mara[]
      WHERE matnr = gt_mara-matnr
      AND werks EQ ''.

    " get SOS records for sites
    SELECT matnr werks bwscl zzonline_status zz_pbs
      FROM marc INTO TABLE gt_marc_sos
      FOR ALL ENTRIES IN gt_mara[]
      WHERE matnr = gt_mara-matnr.

    " get GTINs for the set of articles
    SELECT * FROM mean INTO TABLE gt_mean
      FOR ALL ENTRIES IN gt_mara[]
      WHERE matnr = gt_mara-matnr.

    " get Node from the prod. hierarchy assignment table, for the set of articles
    SELECT matnr node FROM wrf_matgrp_prod INTO TABLE gt_wrf_matgrp_prod
      FOR ALL ENTRIES IN gt_mara[]
      WHERE matnr   = gt_mara-matnr
        AND mainflg = abap_true.

    " For regional banner info.
    SELECT matnr vkorg vtweg zzcatman sstuf vrkme prodh versg ktgrm vmsta vmstd aumng
      FROM mvke INTO TABLE gt_reg_banner
      FOR ALL ENTRIES IN gt_mara[]
      WHERE matnr = gt_mara-matnr
      AND
      " VKORG where VKORG in (3000,4000,5000,6000) & VTWEG=20 & ZZCATMAN<> " " OR SSTUF <> " "
      (
        ( ( vkorg EQ '3000' OR vkorg EQ zcl_constants=>gc_vkorg_4000
          OR vkorg EQ zcl_constants=>gc_vkorg_5000 OR vkorg EQ zcl_constants=>gc_vkorg_6000 )
          AND vtweg EQ '20'
          AND ( zzcatman NE '' OR sstuf NE '' ) )
      OR
      " VKORG where VKORG=1000 & VTWEG=10 & ZZCATMAN<> " "
        ( vkorg EQ zcl_constants=>gc_banner_1000
          AND vtweg EQ zcl_constants=>gc_distr_chan_10
          AND zzcatman NE '' )
      ).

    CLEAR: gt_tvarv_ref_sites_lni, gt_tvarv_ref_sites_uni.

    ls_tvarvc-name = lc_ref_sites_uni.
    APPEND ls_tvarvc TO gt_tvarv_ref_sites_uni.

*-- Global method to fetch the TVARV for UNI reference sites
    CALL METHOD zcl_stvarv_constant=>get_stvarv_constant
      CHANGING
        xt_stvarv_constant = gt_tvarv_ref_sites_uni.

    CLEAR ls_tvarvc.
    ls_tvarvc-name = lc_ref_sites_lni.
    APPEND ls_tvarvc TO gt_tvarv_ref_sites_lni.

*-- Global method to fetch the TVARV for LNI reference sites
    CALL METHOD zcl_stvarv_constant=>get_stvarv_constant
      CHANGING
        xt_stvarv_constant = gt_tvarv_ref_sites_lni.

    " since the TVARV has no value for 1000 banner... adding it in global table via code
    READ TABLE gt_tvarv_ref_sites_lni INTO ls_ref_sites_lni INDEX 1.
    IF sy-subrc EQ 0.
      ls_ref_sites_lni-low = zcl_constants=>gc_banner_1000.
      ls_ref_sites_lni-high = 'RFDC'.
      APPEND ls_ref_sites_lni TO gt_tvarv_ref_sites_lni.
    ENDIF.

** read UOM category from the custom table to check the UOM's category
    CALL FUNCTION 'ZARN_READ_UOM_CATEGORY'
      IMPORTING
        it_uom_cat = gt_uom_cat.

    lsr_uoms_range_tbl-sign = 'I'.
    lsr_uoms_range_tbl-option = 'EQ'.

    LOOP AT gt_uom_cat INTO ls_uom_cat.
      CASE ls_uom_cat-uom_category.
          " LAYER
        WHEN gc_cat_layer.
          lsr_uoms_range_tbl-low = ls_uom_cat-uom.
          APPEND lsr_uoms_range_tbl TO gtr_layer_uoms.
          " PALLET
        WHEN gc_cat_pallet.
          lsr_uoms_range_tbl-low = ls_uom_cat-uom.
          APPEND lsr_uoms_range_tbl TO gtr_pallets_uoms.
        WHEN OTHERS.
          " Do nothing
      ENDCASE.
    ENDLOOP.

    " Fill NUMTP range
*    lsr_numtp-sign = 'I'.
*    lsr_numtp-option = 'EQ'.
*    lsr_numtp-low = 'ZP'.
*    APPEND lsr_numtp TO gtr_numtp.
*    lsr_numtp-low = 'ZD'.
*    APPEND lsr_numtp TO gtr_numtp.
*    lsr_numtp-low = 'ZI'.
*    APPEND lsr_numtp TO gtr_numtp.
*    lsr_numtp-low = 'ZS'.
*    APPEND lsr_numtp TO gtr_numtp.
*    lsr_numtp-low = 'ZB'.
*    APPEND lsr_numtp TO gtr_numtp.
*    lsr_numtp-low = 'ZR'.
*    APPEND lsr_numtp TO gtr_numtp.
*    lsr_numtp-low = 'IE'.
*    APPEND lsr_numtp TO gtr_numtp.

    " Get article's Issue and sales unit
    SELECT matnr wvrkm wausm FROM maw1 INTO TABLE gt_maw1
     FOR ALL ENTRIES IN gt_mara[]
     WHERE matnr = gt_mara-matnr.

    " Get PIRs for the set of articles
    SELECT * FROM eina INTO TABLE gt_eina
      FOR ALL ENTRIES IN gt_mara[]
      WHERE matnr = gt_mara-matnr.
    IF sy-subrc EQ 0.
      SELECT aplfz bprme ekgrp ekkol esokz infnr loekz minbm
             mwskz netpr norbm uebto untto
      FROM eine INTO CORRESPONDING FIELDS OF TABLE gt_eine
      FOR ALL ENTRIES IN gt_eina
      WHERE infnr = gt_eina-infnr
      AND ekorg EQ '9999'
      AND werks EQ ''.

      SELECT bbbnr bbsnr bubkz lifnr
        INTO TABLE gt_pir_vendor[]
        FROM lfa1
        FOR ALL ENTRIES IN gt_eina[]
        WHERE lifnr EQ gt_eina-lifnr.
    ENDIF.

  ELSE.
    " No corresponding SAP data found for data in file
    " error and exit
    RETURN.

  ENDIF.

ENDFORM.                    " GET_DATA_FROM_SAP_TBL
*&---------------------------------------------------------------------*
*&      Form  FILL_ARENA_TABLES
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM fill_arena_tables .

  DATA ls_mara                  LIKE LINE OF gt_mara.
  DATA ls_main                  LIKE LINE OF gt_main.
  DATA ls_arn_products          LIKE LINE OF gt_arn_products.
  DATA ls_arn_prd_ver           LIKE LINE OF gt_arn_prd_ver.
  DATA ls_arn_reg_hdr           TYPE zarn_reg_hdr.
  DATA lt_arn_reg_banner        TYPE ztarn_reg_banner.
  DATA lt_arn_reg_uom           TYPE ztarn_reg_uom.
  DATA lt_arn_reg_uom_lay_pal   TYPE ztarn_reg_uom.
  DATA lt_arn_reg_ean           TYPE ztarn_reg_ean.
  DATA lt_arn_reg_pir           TYPE ztarn_reg_pir.
  DATA ls_arn_ver_status        TYPE zarn_ver_status.


  FREE: gt_arn_ver_status,gt_arn_reg_banner_all, gt_arn_reg_ean_all,
        gt_arn_reg_uom_all, gt_arn_reg_pir_all, gt_arn_reg_hdr.

  LOOP AT gt_mara INTO ls_mara.
    CLEAR: ls_arn_reg_hdr, ls_arn_products, ls_main, ls_arn_prd_ver, ls_arn_ver_status.
    FREE: lt_arn_reg_banner, lt_arn_reg_ean, lt_arn_reg_uom, lt_arn_reg_uom_lay_pal, lt_arn_reg_pir.

**********************************************************************
*********************FILL REGIONAL HEADER TABLE***********************
**********************************************************************
    READ TABLE gt_arn_sync TRANSPORTING NO FIELDS WITH KEY tabname = 'ZARN_REG_HDR'.
    IF sy-subrc EQ 0.

      ls_arn_reg_hdr-mandt = sy-mandt.

      READ TABLE gt_arn_products INTO ls_arn_products WITH TABLE KEY fan_id = ls_mara-zzfan.
      IF sy-subrc EQ 0.
**   IDNO
        ls_arn_reg_hdr-idno = ls_arn_products-idno.

**    VERSION
        READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'VERSION'.
        IF sy-subrc EQ 0.
          CLEAR ls_arn_prd_ver.

          READ TABLE gt_main INTO ls_main WITH TABLE KEY zzfan = ls_mara-zzfan.
          IF sy-subrc EQ 0.

**      where ZARN_PRD_VERSION-VERSION=Version in the file & ZARN_PRD_VERSION-IDNO = ZARN_REG_HDR-IDNO
            READ TABLE gt_arn_prd_ver INTO ls_arn_prd_ver
              WITH TABLE KEY idno = ls_arn_products-idno version = ls_main-version.
            IF sy-subrc EQ 0.
              ls_arn_reg_hdr-version = ls_arn_prd_ver-version.
            ELSE.
**        ZARN_VER_STATUS-CURRENT_VER where ZARN_VER_STATUS-IDNO = ZARN_REG_HDR-IDNO
              READ TABLE gt_arn_curr_prd_ver INTO ls_arn_prd_ver
                WITH TABLE KEY idno = ls_arn_products-idno. " version = ls_main-version.
              IF sy-subrc EQ 0.
                ls_arn_reg_hdr-version = ls_arn_prd_ver-version.
              ENDIF.
            ENDIF.

          ENDIF.
        ENDIF.

      ELSE.
        " log error - MISSING IDNO
        CONTINUE.
      ENDIF.

      " Fill other header fields of Regional Header table
      PERFORM fill_reg_header_tbl
        USING ls_mara
        CHANGING ls_arn_reg_hdr.

      INSERT ls_arn_reg_hdr INTO TABLE gt_arn_reg_hdr.

    ENDIF. " sync header fields

**********************************************************************
*********************FILL REGIONAL BANNER TABLE***********************
**********************************************************************

    " Fill other fields for Regional banner table
    PERFORM fill_reg_banner
      USING ls_mara-matnr ls_arn_reg_hdr-idno
      CHANGING lt_arn_reg_banner.

    IF lt_arn_reg_banner[] IS NOT INITIAL.
      INSERT LINES OF lt_arn_reg_banner INTO TABLE gt_arn_reg_banner_all.
    ENDIF.

**********************************************************************
********** Fill REGIONAL UOM TABLE- RETAIL/INNER/SHIPPER**************
**********************************************************************
** Fill ZARN_REG_UOM for RETAIL/INNER/SHIPPER
    PERFORM fill_reg_uom
      USING ls_mara ls_arn_reg_hdr-version ls_arn_reg_hdr-idno
      CHANGING lt_arn_reg_uom.

    IF lt_arn_reg_uom[] IS NOT INITIAL.
      INSERT LINES OF lt_arn_reg_uom INTO TABLE gt_arn_reg_uom_all.
    ENDIF.

**********************************************************************
*************** Fill REGIONAL UOM TABLE- LAYER/PALLET*****************
**********************************************************************
** Fill ZARN_REG_UOM for LAYER and PALLET: Read all the ZARN_PIR-HYBRIS_INTERNAL_CODE where IDNO=ZARN_REG_HDR_IDNO & VERSION = ZARN_REG_HDR-VERSION
    PERFORM fill_reg_uom_lay_pal
      USING ls_mara ls_arn_reg_hdr-version ls_arn_reg_hdr-idno
      CHANGING lt_arn_reg_uom_lay_pal.

    IF lt_arn_reg_uom_lay_pal[] IS NOT INITIAL.
      INSERT LINES OF lt_arn_reg_uom_lay_pal INTO TABLE gt_arn_reg_uom_all.
    ENDIF.

**********************************************************************
***************************Fill Regional EAN TABLE********************
**********************************************************************
**  Fill ZARN_REG_EAN EAN for Retail/Inner/Shipper in ZARN_REG_UOM-PIM_UOM_CODE
    PERFORM fill_reg_ean
      USING ls_mara-matnr ls_arn_reg_hdr-idno
            gt_arn_reg_uom_all
      CHANGING lt_arn_reg_ean.

    IF lt_arn_reg_ean[] IS NOT INITIAL.
      INSERT LINES OF lt_arn_reg_ean INTO TABLE gt_arn_reg_ean_all.
    ENDIF.

**********************************************************************
*********************** Fill REGIONAL PIR TABLE***********************
**********************************************************************
**  Fill ZARN_REG_PIR table
    PERFORM fill_reg_pir
      USING ls_mara ls_arn_reg_hdr-version ls_arn_reg_hdr-idno
      CHANGING lt_arn_reg_pir.

    IF lt_arn_reg_pir[] IS NOT INITIAL.
      INSERT LINES OF lt_arn_reg_pir INTO TABLE gt_arn_reg_pir_all.
    ENDIF.


    READ TABLE gt_arn_sync TRANSPORTING NO FIELDS WITH KEY tabname = 'ZARN_VER_STATUS'.
    IF sy-subrc EQ 0.
**  Fill ZARN_VER_STATUS table - ?????????? only CURRENT_VER If SYNC BACK successfully
      ls_arn_ver_status-mandt = sy-mandt.
      ls_arn_ver_status-idno = ls_arn_reg_hdr-idno.

      READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
        WITH TABLE KEY tabname = 'ZARN_VER_STATUS' fldname = 'ARTICLE_VER'.
      IF sy-subrc EQ 0.
        ls_arn_ver_status-article_ver = ls_arn_reg_hdr-version.
      ENDIF.

      READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
        WITH TABLE KEY tabname = 'ZARN_VER_STATUS' fldname = 'CHANGED_ON'.
      IF sy-subrc EQ 0.
        ls_arn_ver_status-changed_on = sy-datlo.
      ENDIF.

      READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
        WITH TABLE KEY tabname = 'ZARN_VER_STATUS' fldname = 'CHANGED_AT'.
      IF sy-subrc EQ 0.
        ls_arn_ver_status-changed_at = sy-timlo.
      ENDIF.

      READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
        WITH TABLE KEY tabname = 'ZARN_VER_STATUS' fldname = 'CHANGED_BY'.
      IF sy-subrc EQ 0.
        ls_arn_ver_status-changed_by = sy-uname.
      ENDIF.

      INSERT ls_arn_ver_status INTO TABLE gt_arn_ver_status.
    ENDIF.

  ENDLOOP.

ENDFORM.                    " FILL_ARENA_TABLES
*&---------------------------------------------------------------------*
*&      Form  FILL_REG_HEADER_TBL
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM fill_reg_header_tbl
  USING fi_s_mara           TYPE mara
  CHANGING fc_s_arn_reg_hdr TYPE zarn_reg_hdr.

  DATA ls_makt              LIKE LINE OF gt_makt.
  DATA ls_marc_sos          LIKE LINE OF gt_marc_sos.
  DATA ls_gil_zzcatman      LIKE LINE OF gt_gil_zzcatman.
  DATA ls_ret_zzcatman      LIKE LINE OF gt_ret_zzcatman.
  DATA ls_retdc_zzcatman    LIKE LINE OF gt_retdc_zzcatman.
  DATA ls_host_sap          LIKE LINE OF gt_host_sap.
  DATA ls_wlk2              LIKE LINE OF gt_wlk2.
  DATA ls_wrf_matgrp_prod   LIKE LINE OF gt_wrf_matgrp_prod.
  DATA ls_uom_cat           LIKE LINE OF gt_uom_cat.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'MATNR'.
  IF sy-subrc EQ 0.
    " MARA-MATNR where MARA-ZZFAN = ZARN_PRODUCTS-FAN_ID
    fc_s_arn_reg_hdr-matnr = fi_s_mara-matnr.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'STATUS'.
  IF sy-subrc EQ 0.
    " MIG
    fc_s_arn_reg_hdr-status = 'MIG'.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'APP_MODE'.
  IF sy-subrc EQ 0.
    " not to be updated
    fc_s_arn_reg_hdr-app_mode = ''.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
  WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'RELEASE_STATUS'.
  IF sy-subrc EQ 0.
    " not to be updated
    fc_s_arn_reg_hdr-release_status = ''.
  ENDIF.


  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'MATKL'.
  IF sy-subrc EQ 0.
    " MATKL where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-matkl = fi_s_mara-matkl.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'MTART'.
  IF sy-subrc EQ 0.
    " MTART  where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-mtart = fi_s_mara-mtart.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ATTYP'.
  IF sy-subrc EQ 0.
    " ATTYP  where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-attyp = fi_s_mara-attyp.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'STORAGE_TEMP'.
  IF sy-subrc EQ 0.
    " TEMPB  where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-storage_temp = fi_s_mara-tempb.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'RETAIL_NET_WEIGHT'.
  IF sy-subrc EQ 0.
    " NTGEW  where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-retail_net_weight = fi_s_mara-ntgew.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'RETAIL_NET_WEIGHT_UOM'.
  IF sy-subrc EQ 0.
    " GEWEI  where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-retail_net_weight_uom = fi_s_mara-gewei.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'RETAIL_UNIT_DESC'.
  IF sy-subrc EQ 0.
    READ TABLE gt_makt INTO ls_makt WITH TABLE KEY matnr = fi_s_mara-matnr.
    IF sy-subrc EQ 0.
      " MAKTX  where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
      fc_s_arn_reg_hdr-retail_unit_desc = ls_makt-maktx.
    ENDIF.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'PRDHA'.
  IF sy-subrc EQ 0.
    " PRDHA where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-prdha = fi_s_mara-prdha.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
      WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'BWSCL'.
  IF sy-subrc EQ 0.
    "BWSCL where WERKS=RFST & MARC-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    READ TABLE gt_marc_sos INTO ls_marc_sos
      WITH TABLE KEY matnr = fi_s_mara-matnr werks = 'RFST'.
    IF sy-subrc EQ 0.
      fc_s_arn_reg_hdr-bwscl = ls_marc_sos-bwscl.
    ENDIF.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZPRDTYPE'.
  IF sy-subrc EQ 0.
    " ZZPRDTYPE where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzprdtype = fi_s_mara-zzprdtype.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZSELLING_ONLY'.
  IF sy-subrc EQ 0.
    " ZZSELLING_ONLY where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzselling_only = fi_s_mara-zzselling_only.
  ENDIF.


  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZBUY_SELL'.       " ++ONED-217 JKH 24.11.2016
  IF sy-subrc EQ 0.
    " ZZBUY_SELL where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzbuy_sell = fi_s_mara-zzbuy_sell.
  ENDIF.


  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZAS4SUBDEPT'.
  IF sy-subrc EQ 0.
    " ZZAS4SUBDEPT where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzas4subdept = fi_s_mara-zzas4subdept.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZVAR_WT_FLAG'.
  IF sy-subrc EQ 0.
    " ZZVAR_WT_FLAG where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzvar_wt_flag = fi_s_mara-zzvar_wt_flag.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'GIL_ZZCATMAN'.
  IF sy-subrc EQ 0.
    CLEAR ls_gil_zzcatman.
    " ZZCATMAN where VKORG=3000 & VTWEG=20 &  MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    READ TABLE gt_gil_zzcatman INTO ls_gil_zzcatman WITH TABLE KEY matnr = fi_s_mara-matnr.
    IF sy-subrc EQ 0.
      fc_s_arn_reg_hdr-gil_zzcatman = ls_gil_zzcatman-zzcatman.
    ENDIF.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'RET_ZZCATMAN'.
  IF sy-subrc EQ 0.
    " ZZCATMAN where VKORG=4000/5000/6000 & VTWEG=20
    " MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    CLEAR ls_ret_zzcatman.
    READ TABLE gt_ret_zzcatman INTO ls_ret_zzcatman
      WITH KEY matnr = fi_s_mara-matnr. " getting the first record for banner 4000/5000/6000 sorted already
    IF sy-subrc EQ 0 AND ls_ret_zzcatman-zzcatman IS NOT INITIAL.
      fc_s_arn_reg_hdr-ret_zzcatman = ls_ret_zzcatman-zzcatman.
      " If Blank then ZZCATMAN where VKORG=1000 & VTWEG=10
    ELSE.
      CLEAR ls_retdc_zzcatman.
      READ TABLE gt_retdc_zzcatman INTO ls_retdc_zzcatman WITH TABLE KEY matnr = fi_s_mara-matnr.
      IF sy-subrc EQ 0.
        fc_s_arn_reg_hdr-ret_zzcatman = ls_retdc_zzcatman-zzcatman.
      ENDIF.
    ENDIF.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZ_UNI_DC'.
  IF sy-subrc EQ 0.
**  ZZ_UNI_DC  where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zz_uni_dc = fi_s_mara-zz_uni_dc.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZ_LNI_DC'.
  IF sy-subrc EQ 0.
**  ZZ_LNI_DC  where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zz_lni_dc = fi_s_mara-zz_lni_dc.
  ENDIF.

**********************************************************************
***************************** FILL UOM CATEGORY **********************
**********************************************************************
  READ TABLE gt_host_sap INTO ls_host_sap WITH TABLE KEY matnr = fi_s_mara-matnr.
  IF sy-subrc EQ 0.

**  UOM Category of RETAIL_UOM where LATEST=X  & MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
      WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'LEG_RETAIL'.
    IF sy-subrc EQ 0.
      CLEAR ls_uom_cat.
      READ TABLE gt_uom_cat INTO ls_uom_cat WITH KEY uom = ls_host_sap-retail_uom.
      IF sy-subrc EQ 0.
        fc_s_arn_reg_hdr-leg_retail = ls_uom_cat-uom_category.
      ENDIF.
    ENDIF.

**  UOM Category of INNER_UOM where LATEST=X  & MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
      WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'LEG_REPACK'.
    IF sy-subrc EQ 0.
      CLEAR ls_uom_cat.
      READ TABLE gt_uom_cat INTO ls_uom_cat WITH KEY uom = ls_host_sap-inner_uom.
      IF sy-subrc EQ 0.
        fc_s_arn_reg_hdr-leg_repack = ls_uom_cat-uom_category.
      ENDIF.
    ENDIF.

**  UOM Category of SHIPPER_UOM where LATEST=X & MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
      WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'LEG_BULK'.
    IF sy-subrc EQ 0.
      CLEAR ls_uom_cat.
      READ TABLE gt_uom_cat INTO ls_uom_cat WITH KEY uom = ls_host_sap-shipper_uom.
      IF sy-subrc EQ 0.
        fc_s_arn_reg_hdr-leg_bulk = ls_uom_cat-uom_category.
      ENDIF.
    ENDIF.
**********************************************************************

    READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
      WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'LEG_PRBOLY'.
    IF sy-subrc EQ 0.
**  PRBOLY where LATEST=X & & MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
      fc_s_arn_reg_hdr-leg_prboly = ls_host_sap-prboly.
    ENDIF.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZSELL'.
  IF sy-subrc EQ 0.
**  ZZSELL Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzsell = fi_s_mara-zzsell.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZUSE'.
  IF sy-subrc EQ 0.
**  ZZUSE Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzuse = fi_s_mara-zzuse.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'QTY_REQUIRED'.
  IF sy-subrc EQ 0.
**  KWDHT where KWDHT<> "" Where WLK2-MATNR = ZARN_REG_HDR-SAP_ARTICLE (First record)
    LOOP AT gt_wlk2 INTO ls_wlk2 WHERE matnr = fi_s_mara-matnr AND kwdht IS NOT INITIAL.
      fc_s_arn_reg_hdr-qty_required = ls_wlk2-kwdht.
      EXIT.
    ENDLOOP.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'PRERF'.
  IF sy-subrc EQ 0.
**  PRERF where PRERF<>"" Where WLK2-MATNR = ZARN_REG_HDR-SAP_ARTICLE (First record)
    LOOP AT gt_wlk2 INTO ls_wlk2 WHERE matnr = fi_s_mara-matnr AND prerf IS NOT INITIAL.
      fc_s_arn_reg_hdr-prerf = ls_wlk2-prerf.
      EXIT.
    ENDLOOP.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'PNS_TPR_FLAG'.
  IF sy-subrc EQ 0.
**  RBZUL where RBZUL<> "" & VKORG=5000 & VTWEG =20 &MATNR = ZARN_REG_HDR-SAP_ARTICLE
    READ TABLE gt_wlk2 INTO ls_wlk2 WITH TABLE KEY matnr = fi_s_mara-matnr vkorg = '5000'
      vtweg = '20' werks = ''.
    IF sy-subrc EQ 0 AND ls_wlk2-rbzul IS NOT INITIAL.
      fc_s_arn_reg_hdr-pns_tpr_flag = ls_wlk2-rbzul.
    ENDIF.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'SCAGR'.
  IF sy-subrc EQ 0.
**  SCAGR where SCAGR<> " " Where WLK2-MATNR = ZARN_REG_HDR-SAP_ARTICLE (First record)
    LOOP AT gt_wlk2 INTO ls_wlk2 WHERE matnr = fi_s_mara-matnr AND scagr IS NOT INITIAL.
      fc_s_arn_reg_hdr-scagr = ls_wlk2-scagr.
      EXIT.
    ENDLOOP.
  ENDIF.


  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'MSTAE'.
  IF sy-subrc EQ 0.
**   MSTAE Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-mstae = fi_s_mara-mstae.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'MSTDE'.
  IF sy-subrc EQ 0.
**  MSTDE Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-mstde = fi_s_mara-mstde.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'MSTAV'.
  IF sy-subrc EQ 0.
**  MSTAV Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-mstav = fi_s_mara-mstav.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'MSTDV'.
  IF sy-subrc EQ 0.
**  MSTDV Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-mstdv = fi_s_mara-mstdv.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZTKTPRNT'.
  IF sy-subrc EQ 0.
**  ZZTKTPRNT Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zztktprnt = fi_s_mara-zztktprnt.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZPEDESC_FLAG'.
  IF sy-subrc EQ 0.
**  ZZPEDESC_FLAG Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzpedesc_flag = fi_s_mara-zzpedesc_flag.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'SAISJ'.
  IF sy-subrc EQ 0.
**  SAISJ Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-saisj = fi_s_mara-saisj.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'SAISO'.
  IF sy-subrc EQ 0.
**  SAISO Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-saiso = fi_s_mara-saiso.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'BSTAT'.
  IF sy-subrc EQ 0.
**  BSTAT Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-bstat = fi_s_mara-bstat.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZATTR1'.
  IF sy-subrc EQ 0.
**  ZZATTR1 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr1 = fi_s_mara-zzattr1.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
  WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZATTR2'.
  IF sy-subrc EQ 0.
**  ZZATTR2 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr2 = fi_s_mara-zzattr2.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
  WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZATTR3'.
  IF sy-subrc EQ 0.
**  ZZATTR3 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr3 = fi_s_mara-zzattr3.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
  WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZATTR4'.
  IF sy-subrc EQ 0.
**  ZZATTR4 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr4 = fi_s_mara-zzattr4.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
  WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZATTR5'.
  IF sy-subrc EQ 0.
**  ZZATTR5 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr5 = fi_s_mara-zzattr5.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
  WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZATTR6'.
  IF sy-subrc EQ 0.
**  ZZATTR6 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr6 = fi_s_mara-zzattr6.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
  WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZATTR7'.
  IF sy-subrc EQ 0.
**  ZZATTR7 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr7 = fi_s_mara-zzattr7.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
  WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZATTR8'.
  IF sy-subrc EQ 0.
**  ZZATTR8 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr8 = fi_s_mara-zzattr8.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
  WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZATTR9'.
  IF sy-subrc EQ 0.
**  ZZATTR9 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr9 = fi_s_mara-zzattr9.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
  WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZATTR10'.
  IF sy-subrc EQ 0.
**  ZZATTR10 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr10 = fi_s_mara-zzattr10.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZATTR11'.
  IF sy-subrc EQ 0.
**  ZZATTR11 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr11 = fi_s_mara-zzattr11.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
  WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZATTR12'.
  IF sy-subrc EQ 0.
**  ZZATTR12 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr12 = fi_s_mara-zzattr12.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
  WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZATTR13'.
  IF sy-subrc EQ 0.
**  ZZATTR13 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr13 = fi_s_mara-zzattr13.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
  WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZATTR14'.
  IF sy-subrc EQ 0.
**  ZZATTR14 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr14 = fi_s_mara-zzattr14.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
  WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZATTR15'.
  IF sy-subrc EQ 0.
**  ZZATTR15 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr15 = fi_s_mara-zzattr15.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
  WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZATTR16'.
  IF sy-subrc EQ 0.
**  ZZATTR16 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr16 = fi_s_mara-zzattr16.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
  WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZATTR17'.
  IF sy-subrc EQ 0.
**  ZZATTR17 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr17 = fi_s_mara-zzattr17.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
  WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZATTR18'.
  IF sy-subrc EQ 0.
**  ZZATTR18 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr18 = fi_s_mara-zzattr18.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
  WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZATTR19'.
  IF sy-subrc EQ 0.
**  ZZATTR19 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr19 = fi_s_mara-zzattr19.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
  WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZATTR20'.
  IF sy-subrc EQ 0.
**  ZZATTR20 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr20 = fi_s_mara-zzattr20.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZATTR21'.
  IF sy-subrc EQ 0.
**  ZZATTR21 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr21 = fi_s_mara-zzattr21.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
  WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZATTR22'.
  IF sy-subrc EQ 0.
**  ZZATTR22 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr22 = fi_s_mara-zzattr22.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
  WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZATTR23'.
  IF sy-subrc EQ 0.
**  ZZATTR23 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr23 = fi_s_mara-zzattr23.
  ENDIF.



  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZSTRD'.
  IF sy-subrc EQ 0.
**    ZZSTRD Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzstrd = fi_s_mara-zzstrd.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZSCO_WEIGHED_FLAG'.
  IF sy-subrc EQ 0.
**    ZZSCO_WEIGHED_FLAG Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzsco_weighed_flag = fi_s_mara-zzsco_weighed_flag.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZSTRG'.
  IF sy-subrc EQ 0.
**    ZZSTRG Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzstrg = fi_s_mara-zzstrg.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZLABNUM'.
  IF sy-subrc EQ 0.
**    ZZLABNUM Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzlabnum = fi_s_mara-zzlabnum.
  ENDIF.


  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZPKDDT'.
  IF sy-subrc EQ 0.
**    ZZPKDDT Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzpkddt = fi_s_mara-zzpkddt.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZMANDST'.
  IF sy-subrc EQ 0.
**    ZZMANDST Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzmandst = fi_s_mara-zzmandst.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZWNMSG'.
  IF sy-subrc EQ 0.
**    ZZWNMSG  Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzwnmsg = fi_s_mara-zzwnmsg.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'BRAND_ID'.
  IF sy-subrc EQ 0.
**    BRAND_ID Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-brand_id = fi_s_mara-brand_id.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'MHDRZ'.
  IF sy-subrc EQ 0.
**    MHDRZ Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-mhdrz = fi_s_mara-mhdrz.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZINGRETYPE'.
  IF sy-subrc EQ 0.
**    ZZINGRETYPE Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzingretype = fi_s_mara-zzingretype.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'GILMOURS_WEB'.
  IF sy-subrc EQ 0.
**    If MARA-PRDHA <> "" Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE then "X"
    IF fi_s_mara-prdha IS NOT INITIAL.
      fc_s_arn_reg_hdr-gilmours_web = zcl_constants=>gc_true.
    ENDIF.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'PLU_ARTICLE'.
  IF sy-subrc EQ 0.
**   If Count(MEAN-EAN11 where MEAN-EANTP='ZP' & MEAN-MATNR = ZARN_REG_HDR-SAP_ARTICLE)>0 then "X"
    READ TABLE gt_mean TRANSPORTING NO FIELDS WITH KEY matnr = fi_s_mara-matnr eantp = 'ZP'.
    IF sy-subrc EQ 0.
      fc_s_arn_reg_hdr-plu_article = zcl_constants=>gc_true.
    ENDIF.
  ENDIF.


  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ARTICLE_HIERARCHY'.
  IF sy-subrc EQ 0.
**    NODE Where WRF_MATGRP_PROD-MATNR =  ZARN_REG_HDR-SAP_ARTICLE
    READ TABLE gt_wrf_matgrp_prod INTO ls_wrf_matgrp_prod WITH TABLE KEY matnr = fi_s_mara-matnr.
    IF sy-subrc EQ 0.
      fc_s_arn_reg_hdr-article_hierarchy = ls_wrf_matgrp_prod-node.
    ENDIF.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ERSDA'.
  IF sy-subrc EQ 0.
**      Current Date when creating the records
    fc_s_arn_reg_hdr-ersda = sy-datlo.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ERZET'.
  IF sy-subrc EQ 0.
**      Current Time when creating the records
    fc_s_arn_reg_hdr-erzet = sy-timlo.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ERNAM'.
  IF sy-subrc EQ 0.
**      Current UserId when creating the records
    fc_s_arn_reg_hdr-ernam = sy-uname.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'LAEDA'.
  IF sy-subrc EQ 0.
**      Current Date when changing the records
    fc_s_arn_reg_hdr-laeda = sy-datlo.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ERUET'.
  IF sy-subrc EQ 0.
**      Current Time when changing the records
    fc_s_arn_reg_hdr-eruet = sy-timlo.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'AENAM'.
  IF sy-subrc EQ 0.
**      Current User when changing the records
    fc_s_arn_reg_hdr-aenam = sy-uname.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
   WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZRANGE_FLAG'.
  IF sy-subrc EQ 0.
**    ZZRANGE_FLAG Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzrange_flag = fi_s_mara-zzrange_flag.
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
 WITH TABLE KEY tabname = 'ZARN_REG_HDR' fldname = 'ZZRANGE_AVAIL'.
  IF sy-subrc EQ 0.
**    ZZRANGE_AVAIL Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzrange_avail = fi_s_mara-zzrange_avail.
  ENDIF.

ENDFORM.                    " FILL_REG_HEADER_TBL
*&---------------------------------------------------------------------*
*&      Form  FILL_REG_BANNER
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->P_LS_MARA  text
*      <--P_LS_ARN_REG_BANNER  text
*----------------------------------------------------------------------*
FORM fill_reg_banner
  USING    fi_v_matnr           TYPE matnr
           fi_v_idno            TYPE zarn_idno
  CHANGING fc_t_arn_reg_banner  TYPE ztarn_reg_banner.

  DATA ls_tvarvc                TYPE tvarvc.
  DATA ls_marc_sos              LIKE LINE OF gt_marc_sos.
  DATA ls_arn_reg_banner        TYPE zarn_reg_banner.
  DATA ls_reg_banner            LIKE LINE OF gt_reg_banner.
  DATA lv_werks                 TYPE werks_d.
  DATA ls_wlk2                  LIKE LINE OF gt_wlk2.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS WITH KEY tabname = 'ZARN_REG_BANNER'.
  IF sy-subrc EQ 0.

    LOOP AT gt_reg_banner INTO ls_reg_banner WHERE matnr = fi_v_matnr.

      CLEAR ls_arn_reg_banner.

      " fill data for all relevant banners
      ls_arn_reg_banner-idno = fi_v_idno.
      ls_arn_reg_banner-banner = ls_reg_banner-vkorg.

      " UNI SOS
      READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
        WITH TABLE KEY tabname = 'ZARN_REG_BANNER' fldname = 'SOURCE_UNI'.
      IF sy-subrc EQ 0.
        CLEAR: ls_marc_sos,ls_tvarvc.
        READ TABLE gt_tvarv_ref_sites_uni INTO ls_tvarvc WITH KEY low = ls_reg_banner-vkorg.
        IF sy-subrc EQ 0.
          CLEAR lv_werks.
          lv_werks = ls_tvarvc-high.
          CONDENSE lv_werks.
          READ TABLE gt_marc_sos INTO ls_marc_sos
            WITH TABLE KEY matnr = fi_v_matnr werks = lv_werks.
          IF sy-subrc EQ 0.
            ls_arn_reg_banner-source_uni = ls_marc_sos-bwscl.
          ENDIF.
        ENDIF.
      ENDIF.

      " LNI SOS
      READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
        WITH TABLE KEY tabname = 'ZARN_REG_BANNER' fldname = 'SOURCE_LNI'.
      IF sy-subrc EQ 0.
        CLEAR: ls_marc_sos,ls_tvarvc.
        READ TABLE gt_tvarv_ref_sites_lni INTO ls_tvarvc WITH KEY low = ls_reg_banner-vkorg.
        IF sy-subrc EQ 0.
          CLEAR lv_werks.
          lv_werks = ls_tvarvc-high.
          CONDENSE lv_werks.
          READ TABLE gt_marc_sos INTO ls_marc_sos
            WITH TABLE KEY matnr = fi_v_matnr werks = lv_werks.
          IF sy-subrc EQ 0.
            ls_arn_reg_banner-source_lni = ls_marc_sos-bwscl.
          ENDIF.
        ENDIF.
      ENDIF.

      " Category manager
      READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
        WITH TABLE KEY tabname = 'ZARN_REG_BANNER' fldname = 'ZZCATMAN'.
      IF sy-subrc EQ 0.
**   ZZCATMAN where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
        ls_arn_reg_banner-zzcatman = ls_reg_banner-zzcatman.
      ENDIF.

**    SSTUF where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
      READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
        WITH TABLE KEY tabname = 'ZARN_REG_BANNER' fldname = 'SSTUF'.
      IF sy-subrc EQ 0.
        ls_arn_reg_banner-sstuf  = ls_reg_banner-sstuf.
      ENDIF.

**  VRKME where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
      READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
        WITH TABLE KEY tabname = 'ZARN_REG_BANNER' fldname = 'VRKME'.
      IF sy-subrc EQ 0.
*        IF ls_reg_banner-vrkme IS NOT INITIAL.
        ls_arn_reg_banner-vrkme = ls_reg_banner-vrkme.
        "If Blank then MARA-MEINS" - NOT REQUIRED AS PER NEW LOGIC
*        ELSE.
*          CLEAR ls_mara-meins.
*          READ TABLE gt_mara INTO ls_mara TRANSPORTING meins WITH TABLE KEY matnr = fi_v_matnr.
*          IF sy-subrc EQ 0.
*            ls_arn_reg_banner-sales_unit = ls_mara-meins.
*          ENDIF.
*        ENDIF.
      ENDIF.

**    PRODH where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
      READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
        WITH TABLE KEY tabname = 'ZARN_REG_BANNER' fldname = 'PRODH'.
      IF sy-subrc EQ 0.
        ls_arn_reg_banner-prodh = ls_reg_banner-prodh.
      ENDIF.

**    STGMA where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
      READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
        WITH TABLE KEY tabname = 'ZARN_REG_BANNER' fldname = 'VERSG'.
      IF sy-subrc EQ 0.
        ls_arn_reg_banner-versg = ls_reg_banner-versg.
      ENDIF.

**    KTGRM where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
      READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
        WITH TABLE KEY tabname = 'ZARN_REG_BANNER' fldname = 'KTGRM'.
      IF sy-subrc EQ 0.
        ls_arn_reg_banner-ktgrm = ls_reg_banner-ktgrm.
      ENDIF.

      CLEAR ls_wlk2.
      READ TABLE gt_wlk2 INTO ls_wlk2 WITH TABLE KEY matnr = fi_v_matnr vkorg = ls_reg_banner-vkorg
        vtweg = ls_reg_banner-vtweg werks = ''.
      IF sy-subrc EQ 0.
*    KWDHT where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
        READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname = 'ZARN_REG_BANNER' fldname = 'QTY_REQUIRED'.
        IF sy-subrc EQ 0.
          ls_arn_reg_banner-qty_required = ls_wlk2-kwdht.
        ENDIF.

*    PRERF where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
        READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname = 'ZARN_REG_BANNER' fldname = 'PRERF'.
        IF sy-subrc EQ 0.
          ls_arn_reg_banner-prerf = ls_wlk2-prerf.
        ENDIF.

*    RBZUL where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
        READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname = 'ZARN_REG_BANNER' fldname = 'PNS_TPR_FLAG'.
        IF sy-subrc EQ 0.
          ls_arn_reg_banner-pns_tpr_flag = ls_wlk2-rbzul.
        ENDIF.

*    scagr where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
        READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname = 'ZARN_REG_BANNER' fldname = 'SCAGR'.
        IF sy-subrc EQ 0.
          ls_arn_reg_banner-scagr = ls_wlk2-scagr.
        ENDIF.
      ENDIF.

**    VMSTA where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
      READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
        WITH TABLE KEY tabname = 'ZARN_REG_BANNER' fldname = 'VMSTA'.
      IF sy-subrc EQ 0.
        ls_arn_reg_banner-vmsta = ls_reg_banner-vmsta.
      ENDIF.

**  VMSTD where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
      READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
        WITH TABLE KEY tabname = 'ZARN_REG_BANNER' fldname = 'VMSTD'.
      IF sy-subrc EQ 0.
        ls_arn_reg_banner-vmstd = ls_reg_banner-vmstd.
      ENDIF.

**  AUMNG where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
      READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
        WITH TABLE KEY tabname = 'ZARN_REG_BANNER' fldname = 'AUMNG'.
      IF sy-subrc EQ 0.
        ls_arn_reg_banner-aumng = ls_reg_banner-aumng.
      ENDIF.

      APPEND ls_arn_reg_banner TO fc_t_arn_reg_banner.

    ENDLOOP.

  ENDIF.

ENDFORM.                    " FILL_REG_BANNER
*&---------------------------------------------------------------------*
*&      Form  FILL_REG_EAN
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->P_LS_MARA_MATNR  text
*      -->P_LS_ARN_REG_HDR_IDNO  text
*      <--P_LT_ARN_REG_EAN  text
*----------------------------------------------------------------------*
FORM fill_reg_ean
  USING    fi_v_matnr           TYPE matnr
           fi_v_idno            TYPE zarn_idno
           fu_t_reg_uom         TYPE ztarn_reg_uom
  CHANGING fc_t_arn_reg_ean     TYPE ztarn_reg_ean.

  DATA ls_arn_reg_ean TYPE zarn_reg_ean.
  DATA ls_mean        LIKE LINE OF gt_mean.
*  DATA ls_marm        LIKE LINE OF gt_marm.
  DATA ls_reg_uom     TYPE zarn_reg_uom.

  FREE fc_t_arn_reg_ean.


  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS WITH KEY tabname = 'ZARN_REG_EAN'.
  IF sy-subrc EQ 0.

    LOOP AT gt_mean INTO ls_mean WHERE matnr = fi_v_matnr.

      CLEAR ls_arn_reg_ean.

      ls_arn_reg_ean-mandt = sy-mandt.

      " fill data for all relevant EANs
      ls_arn_reg_ean-idno = fi_v_idno.

      CLEAR: ls_reg_uom.
      READ TABLE fu_t_reg_uom INTO ls_reg_uom
      WITH KEY idno  = fi_v_idno
               meinh = ls_mean-meinh.

      IF ls_mean-ean11 IS NOT INITIAL AND
*         ls_mean-eantp IN gtr_numtp[] AND
         ls_reg_uom    IS NOT INITIAL.

**  Populate the  above ZARN_REG_UOM-MEINH which is having the MEAN-EAN11 with
**  MEAN-NUMTP in "ZP","ZD","ZI",","ZS","ZB","ZR" & MEAN-MATNR = ZARN_REG_HDR-SAP_ARTICLE
        ls_arn_reg_ean-meinh = ls_mean-meinh.

**  EAN11 where  MEAN-NUMTP in ""ZP"",""ZD"",""ZI"","",""ZS"",""ZB"",""ZR"" &
**  MEAN-MEINH=ZARN_REG_EAN-MEINH & MEAN-MATNR = ZARN_REG_HDR-SAP_ARTICLE"
        ls_arn_reg_ean-ean11 = ls_mean-ean11.

**NUMTP of the EAN11 above
        READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname = 'ZARN_REG_EAN' fldname = 'EANTP'.
        IF sy-subrc EQ 0.
          ls_arn_reg_ean-eantp = ls_mean-eantp.
        ENDIF.

**      HPEAN of the EAN11 above
        READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname = 'ZARN_REG_EAN' fldname = 'HPEAN'.
        IF sy-subrc EQ 0.
          ls_arn_reg_ean-hpean = ls_mean-hpean.
        ENDIF.

        INSERT ls_arn_reg_ean INTO TABLE fc_t_arn_reg_ean.

      ENDIF.

    ENDLOOP.

  ENDIF.

ENDFORM.                    " FILL_REG_EAN
*&---------------------------------------------------------------------*
*&      Form  FILL_REG_UOM
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->P_LS_MARA  text
*      -->P_LS_ARN_REG_HDR_IDNO  text
*      <--P_LT_ARN_REG_BANNER  text
*----------------------------------------------------------------------*
FORM fill_reg_uom
   USING fi_s_mara            TYPE mara
         fi_v_version         TYPE zarn_version
         fi_v_idno            TYPE zarn_idno
  CHANGING fc_t_arn_reg_uom   TYPE ztarn_reg_uom.

  DATA ls_arn_reg_uom           LIKE LINE OF fc_t_arn_reg_uom.
  DATA ls_arn_uom_variant       LIKE LINE OF gt_arn_uom_variant.
  DATA ls_arn_uom_variant_child LIKE LINE OF gt_arn_uom_variant.
  DATA ls_arn_gtin_var_child    LIKE LINE OF gt_arn_gtin_var.
  DATA ls_uom                   LIKE LINE OF gt_uom.
  DATA ls_maw1                  LIKE LINE OF gt_maw1.
  DATA ls_arn_prod_uom_t        LIKE LINE OF gt_arn_prod_uom_t.

  CHECK gt_arn_uom_variant[] IS NOT INITIAL.

  FREE fc_t_arn_reg_uom.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS WITH KEY tabname = 'ZARN_REG_UOM'.
  IF sy-subrc EQ 0.

    LOOP AT gt_arn_uom_variant INTO ls_arn_uom_variant WHERE idno = fi_v_idno. " AND version = fi_v_version.

      CLEAR ls_arn_reg_uom.

      ls_arn_reg_uom-mandt = sy-mandt.

      " fill data for all relevant UOMs
      ls_arn_reg_uom-idno = fi_v_idno.
      ls_arn_reg_uom-hybris_internal_code = ''.

      " UOM_CODE where ZARN_UOM_VARIANT-IDNO=ZARN_REG_HDR-IDNO &
      " ZARN_UOM_VARIANT-VERSION = ZARN_REG_HDR-VERSION"
      ls_arn_reg_uom-pim_uom_code = ls_arn_uom_variant-uom_code.

      " If PRODUCT_UOM = EA/KGM then 'Retail' Elseif  PRODUCT_UOM = INN Then 'Inner'
      " ElseIf  PRODUCT_UOM = 'CS' Then 'Shipper'"

      " check if the entry is in upload UOM file
      READ TABLE gt_uom TRANSPORTING NO FIELDS
        WITH KEY zzfan = fi_s_mara-zzfan hyb_uom_code = ls_arn_reg_uom-pim_uom_code hyb_pir = ''.
      IF sy-subrc NE 0.
        CONTINUE.
      ENDIF.

      CLEAR ls_arn_prod_uom_t.
      READ TABLE gt_arn_prod_uom_t INTO ls_arn_prod_uom_t
        WITH TABLE KEY product_uom = ls_arn_uom_variant-product_uom.
      IF sy-subrc EQ 0.
        ls_arn_reg_uom-uom_category = ls_arn_prod_uom_t-sap_uom_cat.
      ENDIF.

      CLEAR ls_arn_gtin_var_child.
      " ZARN_GTIN_VAR-UOM_CODE where ZARN_GTIN_VAR-GTIN_CODE = ZARN_UOM_VARIANT-CHILD_GTIN
      READ TABLE gt_arn_gtin_var INTO ls_arn_gtin_var_child
        WITH KEY idno = fi_v_idno version = fi_v_version gtin_code = ls_arn_uom_variant-child_gtin.
      IF sy-subrc EQ 0.
        ls_arn_reg_uom-lower_uom = ls_arn_gtin_var_child-uom_code.
      ENDIF.

      " ZARN_GTIN_VAR-UOM_CODE where ZARN_GTIN_VAR-GTIN_CODE = ZARN_UOM_VARIANT-CHILD_GTIN
      " & ZARN_UOM_VARIANT-UOM_CODE= ZARN_REG_UOM-LOWER_UOM
      CLEAR ls_arn_uom_variant_child.
      READ TABLE gt_arn_uom_variant INTO ls_arn_uom_variant_child
        WITH KEY idno = fi_v_idno version = fi_v_version uom_code = ls_arn_reg_uom-lower_uom.
      IF sy-subrc EQ 0.
        CLEAR ls_arn_gtin_var_child.
        READ TABLE gt_arn_gtin_var INTO ls_arn_gtin_var_child
           WITH KEY idno = fi_v_idno version = fi_v_version gtin_code = ls_arn_uom_variant_child-child_gtin.
        IF sy-subrc EQ 0.
          ls_arn_reg_uom-lower_child_uom = ls_arn_gtin_var_child-uom_code.
        ENDIF.
      ENDIF.

**    FILE-SAP_UOMCODE where FILE-HYBRIS_UOMCODE= ZARN_REG_UOM-PIM_UOM_CODE
      READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
        WITH TABLE KEY tabname = 'ZARN_REG_UOM' fldname = 'MEINH'.
      IF sy-subrc EQ 0.
        CLEAR ls_uom.
        READ TABLE gt_uom INTO ls_uom WITH TABLE KEY zzfan = fi_s_mara-zzfan hyb_uom_code = ls_arn_reg_uom-pim_uom_code
          hyb_pir = ''.
        IF sy-subrc EQ 0.
          ls_arn_reg_uom-meinh = ls_uom-sap_uom_code.
        ENDIF.
      ENDIF.

**  FILE-SAP_UOMCODE where FILE-HYBRIS_UOMCODE= ZARN_REG_UOM-LOWER_UOM
      READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
        WITH TABLE KEY tabname = 'ZARN_REG_UOM' fldname = 'LOWER_MEINH'.
      IF sy-subrc EQ 0.
        CLEAR ls_uom.
        READ TABLE gt_uom INTO ls_uom WITH TABLE KEY zzfan = fi_s_mara-zzfan hyb_uom_code = ls_arn_reg_uom-lower_uom
          hyb_pir = ''.
        IF sy-subrc EQ 0.
          ls_arn_reg_uom-lower_meinh = ls_uom-sap_uom_code.
        ENDIF.
      ENDIF.

**  UNIT_BASE = "X" if above ZARN_REG_UOM-MEINH=MARA-MEINS
      READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
        WITH TABLE KEY tabname = 'ZARN_REG_UOM' fldname = 'UNIT_BASE'.
      IF sy-subrc EQ 0.
        IF ls_arn_reg_uom-meinh EQ fi_s_mara-meins.
          ls_arn_reg_uom-unit_base = abap_true.
        ENDIF.
      ENDIF.

**  UNIT_PURORD = "X" if above ZARN_REG_UOM-MEINH=MARA-BSTME
**  OR If above ZARN_REG_UOM-MEINH=MARA-MEINS & if MARA-BSTME="" "" then ""X"""
      READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
        WITH TABLE KEY tabname = 'ZARN_REG_UOM' fldname = 'UNIT_PURORD'.
      IF sy-subrc EQ 0.
        IF ( ls_arn_reg_uom-meinh EQ fi_s_mara-bstme AND fi_s_mara-bstme IS NOT INITIAL ) OR
          ( ls_arn_reg_uom-meinh EQ fi_s_mara-meins AND fi_s_mara-bstme IS INITIAL ).

          ls_arn_reg_uom-unit_purord = abap_true.
        ENDIF.
      ENDIF.

**  SALES_UNIT = ""X"" if above ZARN_REG_UOM-MEINH=MAW1-WVRKM
      READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
        WITH TABLE KEY tabname = 'ZARN_REG_UOM' fldname = 'SALES_UNIT'.
      IF sy-subrc EQ 0.
        CLEAR ls_maw1.
        READ TABLE gt_maw1 INTO ls_maw1 WITH TABLE KEY matnr = fi_s_mara-matnr.
        IF sy-subrc EQ 0 AND ls_maw1-wvrkm EQ ls_arn_reg_uom-meinh AND ls_maw1-wvrkm IS NOT INITIAL.
          ls_arn_reg_uom-sales_unit = abap_true.
**  If above ZARN_REG_UOM-MEINH=MARA-MEINS & if MAW1-WVRKM="" "" then ""X"""
        ELSEIF sy-subrc EQ 0 AND ls_maw1-wvrkm IS INITIAL AND ls_arn_reg_uom-meinh EQ fi_s_mara-meins.
          ls_arn_reg_uom-sales_unit = abap_true.
        ENDIF.
      ENDIF.

**  ISSUE_UNIT = "X" if above ZARN_REG_UOM-MEINH=MAW1-WAUSM
      READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
        WITH TABLE KEY tabname = 'ZARN_REG_UOM' fldname = 'ISSUE_UNIT'.
      IF sy-subrc EQ 0.
        CLEAR ls_maw1.
        READ TABLE gt_maw1 INTO ls_maw1 WITH TABLE KEY matnr = fi_s_mara-matnr.
        IF sy-subrc EQ 0 AND ls_maw1-wausm EQ ls_arn_reg_uom-meinh AND ls_maw1-wausm IS NOT INITIAL.
          ls_arn_reg_uom-issue_unit = abap_true.
**  If above ZARN_REG_UOM-MEINH=MARA-MEINS & if MAW1-WAUSM="" "" then ""X"""
        ELSEIF sy-subrc EQ 0 AND ls_maw1-wausm IS INITIAL AND ls_arn_reg_uom-meinh EQ fi_s_mara-meins.
          ls_arn_reg_uom-issue_unit = abap_true.
        ENDIF.
      ENDIF.

**  System Date of Upload (No update)
      READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
        WITH TABLE KEY tabname = 'ZARN_REG_UOM' fldname = 'ERSDA'.
      IF sy-subrc EQ 0.
        ls_arn_reg_uom-ersda = sy-datlo.
      ENDIF.

**  System Date of Upload (No update)
      READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
        WITH TABLE KEY tabname = 'ZARN_REG_UOM' fldname = 'ERZET'.
      IF sy-subrc EQ 0.
        ls_arn_reg_uom-erzet = sy-timlo.
      ENDIF.

      INSERT ls_arn_reg_uom INTO TABLE fc_t_arn_reg_uom.

    ENDLOOP.

  ENDIF.

ENDFORM.                    " FILL_REG_UOM
*&---------------------------------------------------------------------*
*&      Form  GET_DATA_FROM_ARENA_TBL
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM get_data_from_arena_tbl .

*  DATA:
*    lt_pir_vendor1 TYPE ty_t_pir_vendor,
*    ls_pir_vendor  TYPE ty_s_pir_vendor,
*    ls_arn_pir     LIKE LINE OF gt_arn_pir.

  CHECK gt_arn_products[] IS NOT INITIAL.

  " get all version's UOM data from the Arena UOM table
  SELECT * FROM zarn_uom_variant
    INTO TABLE gt_arn_uom_variant
    FOR ALL ENTRIES IN gt_arn_products[]
    WHERE idno = gt_arn_products-idno
    AND version = gt_arn_products-version.

  " get all version's UOM data from the Arena UOM table
  SELECT * FROM zarn_gtin_var
    INTO TABLE gt_arn_gtin_var
    FOR ALL ENTRIES IN gt_arn_products[]
    WHERE idno = gt_arn_products-idno
    AND version = gt_arn_products-version.

**  LAYER and PALLET: Read all the ZARN_PIR-HYBRIS_INTERNAL_CODE
**  where IDNO=ZARN_REG_HDR_IDNO & VERSION = ZARN_REG_HDR-VERSION
  SELECT * FROM zarn_pir INTO TABLE gt_arn_pir
    FOR ALL ENTRIES IN gt_arn_products[]
    WHERE idno = gt_arn_products-idno
    AND version = gt_arn_products-version.

** Read all the ZARN_LIST_PRICE - for filling ZARN_REG_PIR
  SELECT * FROM zarn_list_price INTO TABLE gt_arn_list_price
    FOR ALL ENTRIES IN gt_arn_products[]
    WHERE idno = gt_arn_products-idno
    AND version = gt_arn_products-version.

** Read all entries of product uoms
  SELECT * FROM zarn_prod_uom_t INTO TABLE gt_arn_prod_uom_t
    WHERE spras = sy-langu.


** Read all the ZARN_HSNO - for filling ZARN_REG_HSNO
  SELECT * FROM zarn_hsno INTO TABLE gt_arn_hsno
    FOR ALL ENTRIES IN gt_arn_products[]
    WHERE idno    = gt_arn_products-idno
      AND version = gt_arn_products-version.                    " ++3169 JKH 20.10.2016




*** Read all the ZARN_REG_UOM
*  SELECT * FROM ZARN_REG_UOM INTO TABLE gt_arn_list_price
*    FOR ALL ENTRIES IN gt_arn_products[]
*    WHERE idno = gt_arn_products-idno
*    AND version = gt_arn_products-version.

*  IF gt_arn_pir[] IS NOT INITIAL.
*    FREE lt_pir_vendor1[].
*    LOOP AT gt_arn_pir INTO ls_arn_pir.
*      CLEAR ls_pir_vendor.
*      ls_pir_vendor-bbbnr = ls_arn_pir-gln+0(7).
*      ls_pir_vendor-bbsnr = ls_arn_pir-gln+7(5).
*      ls_pir_vendor-bubkz = ls_arn_pir-gln+12(1).
*      INSERT ls_pir_vendor INTO TABLE lt_pir_vendor1[].
*    ENDLOOP.
*
*    IF lt_pir_vendor1[] IS NOT INITIAL.
*      SELECT lifnr bbbnr bbsnr bubkz
*        INTO CORRESPONDING FIELDS OF TABLE gt_pir_vendor[]
*        FROM lfa1
*        FOR ALL ENTRIES IN lt_pir_vendor1[]
*        WHERE bbbnr EQ lt_pir_vendor1-bbbnr
*        AND bbsnr EQ lt_pir_vendor1-bbsnr
*        AND bubkz EQ lt_pir_vendor1-bubkz.
*    ENDIF.
*  ENDIF.

ENDFORM.                    " GET_DATA_FROM_ARENA_TBL

*&---------------------------------------------------------------------*
*&      Form  GET_DATA_FROM_ARENA_REG_TBL
*&---------------------------------------------------------------------*
* get data from ARENA regional tables
*----------------------------------------------------------------------*
FORM get_data_from_arena_reg_tbl.

  FREE: gt_arn_reg_hdr_in_db, gt_arn_reg_banner_in_db, gt_arn_reg_uom_in_db,
        gt_arn_reg_ean_in_db, gt_arn_reg_pir_in_db, gt_arn_ver_status_in_db,
        gt_arn_reg_hsno_in_db.

  FREE: gt_arn_reg_hdr_in_db_old, gt_arn_reg_banner_in_db_old, gt_arn_reg_uom_in_db_old,
        gt_arn_reg_ean_in_db_old, gt_arn_reg_pir_in_db_old, gt_arn_ver_status_in_db_old,
        gt_arn_reg_hsno_in_db_old.

  CHECK gt_arn_products[] IS NOT INITIAL.


**  get old DB values for existing records
  SELECT * FROM zarn_reg_hdr INTO TABLE gt_arn_reg_hdr_in_db
    FOR ALL ENTRIES IN gt_arn_products[]
    WHERE idno = gt_arn_products-idno.


**  get old DB values for existing records
  SELECT * FROM zarn_reg_banner INTO TABLE gt_arn_reg_banner_in_db
  FOR ALL ENTRIES IN gt_arn_products[]
  WHERE idno = gt_arn_products-idno.

**  get old DB values for existing records
  SELECT * FROM zarn_reg_uom INTO TABLE gt_arn_reg_uom_in_db
  FOR ALL ENTRIES IN gt_arn_products[]
  WHERE idno = gt_arn_products-idno.

**  get old DB values for existing records
  SELECT * FROM zarn_reg_ean INTO TABLE gt_arn_reg_ean_in_db
  FOR ALL ENTRIES IN gt_arn_products[]
  WHERE idno = gt_arn_products-idno.

**  get old DB values for existing records
  SELECT * FROM zarn_reg_pir INTO TABLE gt_arn_reg_pir_in_db
  FOR ALL ENTRIES IN gt_arn_products[]
  WHERE idno = gt_arn_products-idno.

**  get old DB values for existing records
  SELECT * FROM zarn_reg_hsno INTO TABLE gt_arn_reg_hsno_in_db
  FOR ALL ENTRIES IN gt_arn_products[]
  WHERE idno = gt_arn_products-idno.


**  get old DB values for existing records
  SELECT * FROM zarn_ver_status INTO TABLE gt_arn_ver_status_in_db
  FOR ALL ENTRIES IN gt_arn_products[]
  WHERE idno = gt_arn_products-idno.



  gt_arn_reg_hdr_in_db_old      = gt_arn_reg_hdr_in_db.
  gt_arn_reg_banner_in_db_old   = gt_arn_reg_banner_in_db.
  gt_arn_reg_uom_in_db_old      = gt_arn_reg_uom_in_db.
  gt_arn_reg_ean_in_db_old      = gt_arn_reg_ean_in_db.
  gt_arn_reg_pir_in_db_old      = gt_arn_reg_pir_in_db.
  gt_arn_ver_status_in_db_old   = gt_arn_ver_status_in_db.
  gt_arn_reg_hsno_in_db_old     = gt_arn_reg_hsno_in_db.



ENDFORM.

*&---------------------------------------------------------------------*
*&      Form  FILL_REG_UOM_LAY_PAL
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->P_LS_MARA  text
*      -->P_LS_ARN_REG_HDR_VERSION  text
*      -->P_LS_ARN_REG_HDR_IDNO  text
*      <--P_LT_ARN_REG_UOM  text
*----------------------------------------------------------------------*
FORM fill_reg_uom_lay_pal
  USING  fi_s_mara            TYPE mara
         fi_v_version         TYPE zarn_version
         fi_v_idno            TYPE zarn_idno
  CHANGING fc_t_arn_reg_uom   TYPE ztarn_reg_uom.

  TYPES:
    BEGIN OF ty_added_pir,
      uom_code              TYPE zarn_uom_code,
      base_units_per_pallet TYPE zarn_base_units_per_pallet,
      qty_layers_per_pallet TYPE zarn_qty_layers_per_pallet,
      qty_units_per_pallet  TYPE zarn_qty_units_per_pallet,
    END OF ty_added_pir.

  DATA lt_added_pir               TYPE SORTED TABLE OF ty_added_pir WITH NON-UNIQUE KEY uom_code base_units_per_pallet
                                                                                        qty_layers_per_pallet qty_units_per_pallet.
  DATA ls_added_pir               TYPE ty_added_pir.
  DATA ls_arn_reg_uom             LIKE LINE OF fc_t_arn_reg_uom.
  DATA ls_arn_uom_variant         LIKE LINE OF gt_arn_uom_variant.
  DATA ls_arn_pir                 LIKE LINE OF gt_arn_pir.
  DATA ls_arn_uom_variant_child   LIKE LINE OF gt_arn_uom_variant.
  DATA ls_arn_gtin_var_child      LIKE LINE OF gt_arn_gtin_var.
  DATA ls_uom                     LIKE LINE OF gt_uom.
  DATA ls_maw1                    LIKE LINE OF gt_maw1.
  DATA ls_arn_reg_uom_temp        TYPE zarn_reg_uom.
  DATA ls_arn_reg_uom_all         LIKE LINE OF gt_arn_reg_uom_all.

  CHECK gt_arn_pir[] IS NOT INITIAL.

  FREE: fc_t_arn_reg_uom, lt_added_pir.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS WITH KEY tabname = 'ZARN_REG_UOM'.
  IF sy-subrc EQ 0.

    LOOP AT gt_arn_pir INTO ls_arn_pir WHERE idno = fi_v_idno.

      IF ls_arn_pir-base_units_per_pallet IS NOT INITIAL
        OR ls_arn_pir-qty_layers_per_pallet IS NOT INITIAL
        OR ls_arn_pir-qty_units_per_pallet IS NOT INITIAL.

        CLEAR ls_arn_reg_uom.
        " fill data for all relevant UOMs
        ls_arn_reg_uom-mandt = sy-mandt.
        ls_arn_reg_uom-idno = fi_v_idno.

        " in case same uom record exists then process it only when there is change in
        " base_units_per_pallet or qty_layers_per_pallet value or qty_units_per_pallet
*        CLEAR: ls_added_pir_old, lv_pir_exist.
        READ TABLE lt_added_pir TRANSPORTING NO FIELDS
          WITH TABLE KEY uom_code = ls_arn_pir-uom_code
          base_units_per_pallet = ls_arn_pir-base_units_per_pallet
          qty_layers_per_pallet = ls_arn_pir-qty_layers_per_pallet
          qty_units_per_pallet = ls_arn_pir-qty_units_per_pallet.

        " record not found, then add to local table
        IF sy-subrc NE 0.
          CLEAR ls_added_pir.
          ls_added_pir-uom_code = ls_arn_pir-uom_code.
*          ls_added_pir-hybris_internal_code = ls_arn_pir-hybris_internal_code.
          ls_added_pir-base_units_per_pallet = ls_arn_pir-base_units_per_pallet.
          ls_added_pir-qty_layers_per_pallet = ls_arn_pir-qty_layers_per_pallet.
          ls_added_pir-qty_units_per_pallet = ls_arn_pir-qty_units_per_pallet.
          INSERT ls_added_pir INTO TABLE lt_added_pir.
        ELSE.
          CONTINUE.
        ENDIF.

        ls_arn_reg_uom-pim_uom_code = ls_arn_pir-uom_code.

**    For Layer: ZARN_PIR-HYBRIS_INTERNAL_CODE selected above
**    For Pallet: ZARN_PIR-HYBRIS_INTERNAL_CODE selected above"
        ls_arn_reg_uom-hybris_internal_code = ls_arn_pir-hybris_internal_code.

        " check if the entry is in upload UOM file
        READ TABLE gt_uom TRANSPORTING NO FIELDS
          WITH TABLE KEY zzfan = fi_s_mara-zzfan hyb_uom_code = ls_arn_reg_uom-pim_uom_code hyb_pir = ls_arn_reg_uom-hybris_internal_code.
        IF sy-subrc NE 0.
          CONTINUE.
        ENDIF.

        DO 2 TIMES.

          IF sy-index EQ 1.
            ls_arn_reg_uom-uom_category = gc_cat_layer.
          ELSE.
            ls_arn_reg_uom-uom_category = gc_cat_pallet.
          ENDIF.

          CLEAR: ls_arn_gtin_var_child, ls_arn_uom_variant, ls_arn_reg_uom-lower_uom.

          " ZARN_GTIN_VAR-UOM_CODE where ZARN_GTIN_VAR-GTIN_CODE = ZARN_UOM_VARIANT-CHILD_GTIN
          READ TABLE gt_arn_uom_variant INTO ls_arn_uom_variant
            WITH KEY idno = fi_v_idno version = fi_v_version uom_code = ls_arn_pir-uom_code.
          IF sy-subrc EQ 0.
            ls_arn_reg_uom-lower_uom = ls_arn_pir-uom_code.
          ENDIF.

          " ZARN_GTIN_VAR-UOM_CODE where ZARN_GTIN_VAR-GTIN_CODE = ZARN_UOM_VARIANT-CHILD_GTIN
          " & ZARN_UOM_VARIANT-UOM_CODE= ZARN_REG_UOM-LOWER_UOM
          CLEAR: ls_arn_uom_variant_child, ls_arn_reg_uom-lower_child_uom.
          READ TABLE gt_arn_uom_variant INTO ls_arn_uom_variant_child
            WITH KEY idno = fi_v_idno version = fi_v_version uom_code = ls_arn_reg_uom-lower_uom.
          IF sy-subrc EQ 0.
            CLEAR ls_arn_gtin_var_child.
            READ TABLE gt_arn_gtin_var INTO ls_arn_gtin_var_child
               WITH KEY idno = fi_v_idno version = fi_v_version gtin_code = ls_arn_uom_variant_child-child_gtin.
            IF sy-subrc EQ 0.
              ls_arn_reg_uom-lower_child_uom = ls_arn_gtin_var_child-uom_code.
            ENDIF.
          ENDIF.

**  For LAYER:FILE-SAP_UOMCODE where FILE-HYBRIS_UOMCODE= ZARN_REG_UOM-PIM_UOM_CODE &
**  FILE-PIM_PIRCODE = ZARN_REG_UOM-HYBRIS_INTERNAL_CODE &
**  belong to TVARVC-NAME= 'GC_LAYER_UNOMS'
          READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_UOM' fldname = 'MEINH'.
          IF sy-subrc EQ 0.
            CLEAR: ls_uom, ls_arn_reg_uom-meinh.

            LOOP AT gt_uom INTO ls_uom
              WHERE zzfan = fi_s_mara-zzfan AND hyb_uom_code = ls_arn_reg_uom-pim_uom_code AND hyb_pir = ls_arn_reg_uom-hybris_internal_code.
              " Layer record
              IF ls_arn_reg_uom-uom_category EQ gc_cat_layer.
                IF ls_uom-sap_uom_code IN gtr_layer_uoms[].
                  ls_arn_reg_uom-meinh = ls_uom-sap_uom_code.
                  EXIT.
                ENDIF.
              ELSE.
                " Pallet record
                IF ls_uom-sap_uom_code IN gtr_pallets_uoms[].
                  ls_arn_reg_uom-meinh = ls_uom-sap_uom_code.
                  EXIT.
                ENDIF.
              ENDIF.
            ENDLOOP.
          ENDIF.

          "For Layer: FILE-SAP_UOMCODE where FILE-HYBRIS_UOMCODE= ZARN_REG_UOM-LOWER_UOM
          READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_UOM' fldname = 'LOWER_MEINH'.
          IF sy-subrc EQ 0.
            CLEAR ls_arn_reg_uom-lower_meinh.
            " Layer record
            IF ls_arn_reg_uom-uom_category EQ gc_cat_layer.
              CLEAR ls_arn_reg_uom_all.
              READ TABLE gt_arn_reg_uom_all INTO ls_arn_reg_uom_all TRANSPORTING meinh
                WITH KEY idno = fi_v_idno pim_uom_code = ls_arn_reg_uom-lower_uom.
              IF sy-subrc EQ 0.
                ls_arn_reg_uom-lower_meinh = ls_arn_reg_uom_all-meinh.
              ENDIF.
              "For Pallet: MARM-MEINH for Layer from above step"
            ELSE.
              CLEAR ls_arn_reg_uom_temp.
              READ TABLE fc_t_arn_reg_uom INTO ls_arn_reg_uom_temp
                WITH KEY pim_uom_code = ls_arn_reg_uom-pim_uom_code uom_category = gc_cat_layer.
              IF sy-subrc EQ 0.
                ls_arn_reg_uom-lower_meinh = ls_arn_reg_uom_temp-meinh.
              ENDIF.
            ENDIF.
          ENDIF.


*  UNIT_BASE = "X" if above ZARN_REG_UOM-MEINH=MARA-MEINS
          READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_UOM' fldname = 'UNIT_BASE'.
          IF sy-subrc EQ 0.
            CLEAR ls_arn_reg_uom-unit_base.
            IF ls_arn_reg_uom-meinh EQ fi_s_mara-meins.
              ls_arn_reg_uom-unit_base = abap_true.
            ENDIF.
          ENDIF.

**  UNIT_PURORD = "X" if above ZARN_REG_UOM-MEINH=MARA-BSTME
**  OR If above ZARN_REG_UOM-MEINH=MARA-MEINS & if MARA-BSTME="" "" then ""X"""
          READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_UOM' fldname = 'UNIT_PURORD'.
          IF sy-subrc EQ 0.
            CLEAR ls_arn_reg_uom-unit_purord.
            IF ( ls_arn_reg_uom-meinh EQ fi_s_mara-bstme AND fi_s_mara-bstme IS NOT INITIAL ) OR
              ( ls_arn_reg_uom-meinh EQ fi_s_mara-meins AND fi_s_mara-bstme IS INITIAL ).

              ls_arn_reg_uom-unit_purord = abap_true.
            ENDIF.
          ENDIF.

**  SALES_UNIT = ""X"" if above ZARN_REG_UOM-MEINH=MAW1-WVRKM
          READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_UOM' fldname = 'SALES_UNIT'.
          IF sy-subrc EQ 0.
            CLEAR: ls_maw1, ls_arn_reg_uom-sales_unit.
            READ TABLE gt_maw1 INTO ls_maw1 WITH TABLE KEY matnr = fi_s_mara-matnr.
            IF sy-subrc EQ 0 AND ls_maw1-wvrkm EQ ls_arn_reg_uom-meinh AND ls_maw1-wvrkm IS NOT INITIAL.
              ls_arn_reg_uom-sales_unit = abap_true.
**  If above ZARN_REG_UOM-MEINH=MARA-MEINS & if MAW1-WVRKM="" "" then ""X"""
            ELSEIF sy-subrc EQ 0 AND ls_maw1-wvrkm IS INITIAL AND ls_arn_reg_uom-meinh EQ fi_s_mara-meins.
              ls_arn_reg_uom-sales_unit = abap_true.
            ENDIF.
          ENDIF.

**  ISSUE_UNIT = "X" if above ZARN_REG_UOM-MEINH=MAW1-WAUSM
          READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_UOM' fldname = 'ISSUE_UNIT'.
          IF sy-subrc EQ 0.
            CLEAR: ls_maw1, ls_arn_reg_uom-issue_unit.
            READ TABLE gt_maw1 INTO ls_maw1 WITH TABLE KEY matnr = fi_s_mara-matnr.
            IF sy-subrc EQ 0 AND ls_maw1-wausm EQ ls_arn_reg_uom-meinh AND ls_maw1-wausm IS NOT INITIAL.
              ls_arn_reg_uom-issue_unit = abap_true.
**  If above ZARN_REG_UOM-MEINH=MARA-MEINS & if MAW1-WAUSM="" "" then ""X"""
            ELSEIF sy-subrc EQ 0 AND ls_maw1-wausm IS INITIAL AND ls_arn_reg_uom-meinh EQ fi_s_mara-meins.
              ls_arn_reg_uom-issue_unit = abap_true.
            ENDIF.
          ENDIF.

**  System Date of Upload (No update)
          READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_UOM' fldname = 'ERSDA'.
          IF sy-subrc EQ 0.
            ls_arn_reg_uom-ersda = sy-datlo.
          ENDIF.

**  System Date of Upload (No update)
          READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_UOM' fldname = 'ERZET'.
          IF sy-subrc EQ 0.
            ls_arn_reg_uom-erzet = sy-timlo.
          ENDIF.

          INSERT ls_arn_reg_uom INTO TABLE fc_t_arn_reg_uom.

        ENDDO.

      ENDIF.

    ENDLOOP.

  ENDIF.

ENDFORM.                    " FILL_REG_UOM_LAY_PAL
*&---------------------------------------------------------------------*
*&      Form  FILL_REG_PIR
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->P_LS_MARA  text
*      -->P_LS_ARN_REG_HDR_VERSION  text
*      -->P_LS_ARN_REG_HDR_IDNO  text
*      <--P_LT_ARN_REG_PIR  text
*----------------------------------------------------------------------*
FORM fill_reg_pir
   USING fi_s_mara                    TYPE mara
         fi_v_version                 TYPE zarn_version
         fi_v_idno                    TYPE zarn_idno
  CHANGING fc_t_arn_reg_uom           TYPE ztarn_reg_pir.

  DATA ls_arn_list_price              LIKE LINE OF gt_arn_list_price.
  DATA ls_arn_reg_pir                 LIKE LINE OF fc_t_arn_reg_uom.
  DATA ls_arn_pir                     LIKE LINE OF gt_arn_pir.
  DATA ls_arn_pir_eine                LIKE LINE OF gt_arn_pir.
  DATA ls_eina                        LIKE LINE OF gt_eina.
  DATA ls_pir                         LIKE LINE OF gt_pir.
  DATA ls_arn_reg_uom                 LIKE LINE OF gt_arn_reg_uom_all.
  DATA ls_eine                        LIKE LINE OF gt_eine.
  DATA lv_no_of_rec                   TYPE i.
  DATA lv_eina_key                    TYPE boole_d.
  DATA lv_eine_order_uom_pim          TYPE zarn_uom_code.
  DATA lv_eine_hybris_internal_code   TYPE zarn_hybris_internal_code.

  CHECK gt_arn_pir[] IS NOT INITIAL.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS WITH KEY tabname = 'ZARN_REG_PIR'.
  IF sy-subrc EQ 0.

    " Loop at National PIR records to create/update the regional PIRs
    LOOP AT gt_arn_pir INTO ls_arn_pir WHERE idno = fi_v_idno.

      CLEAR: ls_arn_reg_pir, ls_pir, ls_eina, ls_arn_reg_uom, ls_eine, lv_eine_order_uom_pim,
             lv_eine_hybris_internal_code, ls_arn_pir_eine, lv_eina_key, lv_no_of_rec.

      ls_arn_reg_pir-mandt = sy-mandt.
      ls_arn_reg_pir-idno  = ls_arn_pir-idno.

**      Fill order_uom_pim and hybris_internal_code from ZARN_PIR table
      ls_arn_reg_pir-order_uom_pim        = ls_arn_pir-uom_code.
      ls_arn_reg_pir-hybris_internal_code = ls_arn_pir-hybris_internal_code.

**    Read the SAP PIR from the PIR upload file for the corresponding FAN/Hybris PIR
      READ TABLE gt_pir INTO ls_pir
        WITH TABLE KEY zzfan = fi_s_mara-zzfan hyb_pir = ls_arn_reg_pir-hybris_internal_code.
      IF sy-subrc EQ 0.
        " Read the VENDOR from the EINA and update the regional table field for the vendor
        READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'LIFNR'.
        IF sy-subrc EQ 0.
          READ TABLE gt_eina INTO ls_eina WITH TABLE KEY infnr = ls_pir-sap_pir.
          IF sy-subrc EQ 0.
            ls_arn_reg_pir-lifnr = ls_eina-lifnr.
            ls_arn_reg_pir-lifab = ls_eina-lifab.
            ls_arn_reg_pir-lifbi = ls_eina-lifbi.



            CLEAR lv_no_of_rec.
            " Read eine
            READ TABLE gt_eine INTO ls_eine WITH TABLE KEY infnr = ls_eina-infnr.
            IF sy-subrc EQ 0.
              IF ls_eine-bprme EQ ls_eina-meins.
                lv_no_of_rec = 1.
              ELSE.
                " PIM uom code
                CLEAR ls_arn_reg_uom.
                READ TABLE gt_arn_reg_uom_all INTO ls_arn_reg_uom TRANSPORTING pim_uom_code
                  WITH KEY idno = fi_v_idno meinh = ls_eine-bprme.
                IF sy-subrc EQ 0.
                  CLEAR ls_arn_pir_eine.
                  READ TABLE gt_arn_pir INTO ls_arn_pir_eine WITH KEY idno = fi_v_idno version = fi_v_version
                    uom_code = ls_arn_reg_uom-pim_uom_code gln = ls_arn_pir-gln.
                  IF sy-subrc EQ 0.
                    lv_eine_order_uom_pim        = ls_arn_pir_eine-uom_code.
                    lv_eine_hybris_internal_code = ls_arn_pir_eine-hybris_internal_code.
                  ENDIF.
                ENDIF.
                lv_no_of_rec = 2.
              ENDIF.
            ENDIF.
          ENDIF.
        ENDIF.

        " if corresponding PIR record found in upload file then continue to next record
      ELSE.
        CONTINUE.
      ENDIF.

      DO lv_no_of_rec TIMES.

        " if sy-index = 1 -> create primary keys from EINA-MEINS
        " if sy-index = 2 -> create primary keys from EINE-BPRME
        IF sy-index EQ 1.
          lv_eina_key = abap_true.
        ELSE.
          lv_eina_key = abap_false.
          ls_arn_reg_pir-order_uom_pim = lv_eine_order_uom_pim.
          ls_arn_reg_pir-hybris_internal_code = lv_eine_hybris_internal_code.
        ENDIF.

**   ZARN_LIST_PRICE-GLN
        READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'GLN_NO'.
        IF sy-subrc EQ 0.
          ls_arn_reg_pir-gln_no = ls_arn_pir-gln.
        ENDIF.

**  ZARN_LIST_PRICE-GLN_DESCRIPTION
        READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'GLN_NAME'.
        IF sy-subrc EQ 0.
          ls_arn_reg_pir-gln_name = ls_arn_pir-gln_description.
        ENDIF.

**  EINA-VABME  for the EINA-INFNR found in "SAP-PIR"
        READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'VABME'.
        IF sy-subrc EQ 0.
          ls_arn_reg_pir-vabme = ls_eina-vabme.
        ENDIF.

**  PIR_REL_EINA => If  EINA-MEINS = ZARN_REG_UOM-MEINH
**                  where ZARN_LIST_PRICE-UOM_CODE = ZARN_REG_UOM-PIM_UOM_CODE then ""X"" else "" """
        IF lv_eina_key EQ abap_true.
          READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'PIR_REL_EINA'.
          IF sy-subrc EQ 0.
            CLEAR ls_arn_reg_uom.
            READ TABLE gt_arn_reg_uom_all INTO ls_arn_reg_uom
              WITH KEY idno = fi_v_idno pim_uom_code = ls_arn_reg_pir-order_uom_pim hybris_internal_code = ''.
            IF sy-subrc EQ 0 AND ls_arn_reg_uom-meinh EQ ls_eina-meins.
              ls_arn_reg_pir-pir_rel_eina = abap_true.
            ENDIF.
          ENDIF.

**  EINA-RELIF   for the EINA-INFNR found in "SAP-PIR"
          READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'RELIF'.
          IF sy-subrc EQ 0.
            ls_arn_reg_pir-relif = ls_eina-relif.
          ENDIF.

        ELSE.
          CLEAR: ls_arn_reg_pir-pir_rel_eina, ls_arn_reg_pir-relif.
        ENDIF.

        CLEAR ls_eine.
        READ TABLE gt_eine INTO ls_eine WITH TABLE KEY infnr = ls_eina-infnr.
        IF sy-subrc EQ 0.

**  BPRME where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
          READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'BPRME'.
          IF sy-subrc EQ 0.

            IF ls_eine-bprme EQ ls_eina-meins.
              ls_arn_reg_pir-bprme = ls_eine-bprme.
            ELSE.
              IF lv_eina_key EQ abap_true.
                " case 1
                ls_arn_reg_pir-bprme = ls_eina-meins.
              ELSE.
                " case 2
                ls_arn_reg_pir-bprme = ls_eine-bprme.
              ENDIF.
            ENDIF.
          ENDIF.

**  ESOKZ where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
          READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'ESOKZ'. " case 2
          IF sy-subrc EQ 0.
            ls_arn_reg_pir-esokz = ls_eine-esokz.
          ENDIF.

**  PIR_REL_EINE => If EINE-BPRME = ZARN_REG_UOM-MEINH where ZARN_LIST_PRICE-UOM_CODE = ZARN_REG_UOM-PIM_UOM_CODE and
**                  EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS="" then "X" else " "
          READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'PIR_REL_EINE'.
          IF sy-subrc EQ 0 AND lv_eina_key EQ abap_false.
            CLEAR ls_arn_reg_uom.
            READ TABLE gt_arn_reg_uom_all INTO ls_arn_reg_uom
              WITH KEY idno = fi_v_idno pim_uom_code = ls_arn_reg_pir-order_uom_pim hybris_internal_code = ''.
            IF sy-subrc EQ 0 AND ls_arn_reg_uom-meinh EQ ls_eine-bprme.
              ls_arn_reg_pir-pir_rel_eine = abap_true.
            ENDIF.
          ELSE.
            CLEAR ls_arn_reg_pir-pir_rel_eine.
          ENDIF.

**  EKGRP where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
          READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'EKGRP'.
          IF sy-subrc EQ 0.
            ls_arn_reg_pir-ekgrp = ls_eine-ekgrp.
          ENDIF.

**    MINBM where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
          READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'MINBM'.
          IF sy-subrc EQ 0.
            ls_arn_reg_pir-minbm = ls_eine-minbm.
          ENDIF.

**  NORBM where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
          READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'NORBM'.
          IF sy-subrc EQ 0.
            ls_arn_reg_pir-norbm = ls_eine-norbm.
          ENDIF.

**  APLFZ where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
          READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'APLFZ'.
          IF sy-subrc EQ 0.
            ls_arn_reg_pir-aplfz = ls_eine-aplfz.
          ENDIF.

**  EKKOL where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
          READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'EKKOL'.
          IF sy-subrc EQ 0.
            ls_arn_reg_pir-ekkol = ls_eine-ekkol.
          ENDIF.

**  MWSKZ where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
          READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'MWSKZ'.
          IF sy-subrc EQ 0.
            ls_arn_reg_pir-mwskz = ls_eine-mwskz.
          ENDIF.

**  LOEKZ where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
          READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'LOEKZ'.
          IF sy-subrc EQ 0.
            ls_arn_reg_pir-loekz = ls_eine-loekz.
          ENDIF.

**  "X" if NETPR = 0 where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
          READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'ALLOW_ZERO_PRICE'.
          IF sy-subrc EQ 0.
            IF ls_eine-netpr EQ 0.
              ls_arn_reg_pir-allow_zero_price = abap_true.
            ENDIF.
          ENDIF.

        ENDIF. " EINE READ

        " lower_uom
        CLEAR ls_arn_reg_uom.
        READ TABLE gt_arn_reg_uom_all INTO ls_arn_reg_uom TRANSPORTING lower_uom lower_child_uom
          WITH KEY idno = fi_v_idno pim_uom_code = ls_arn_reg_pir-order_uom_pim
                   hybris_internal_code = '' meinh = ls_arn_reg_pir-bprme. " ls_eina-meins.
        IF sy-subrc EQ 0.
          ls_arn_reg_pir-lower_uom = ls_arn_reg_uom-lower_uom.
          " lower_child_uom
          ls_arn_reg_pir-lower_child_uom = ls_arn_reg_uom-lower_child_uom.
        ELSE.
          CLEAR: ls_arn_reg_pir-lower_uom, ls_arn_reg_pir-lower_child_uom.
        ENDIF.

        CLEAR ls_arn_list_price.
        " Select list price
        READ TABLE gt_arn_list_price INTO ls_arn_list_price
          WITH TABLE KEY idno = fi_v_idno version = ls_arn_pir-version
                         uom_code = ls_arn_reg_pir-order_uom_pim gln = ls_arn_pir-gln.
        IF sy-subrc EQ 0.
**CURRENCY where ZARN_REG_PIR-ORDER_UOM_PIM & ZARN_LIST_PRICE-GLN
          READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'WAERS'.
          IF sy-subrc EQ 0.
            ls_arn_reg_pir-waers = ls_arn_list_price-currency.
          ENDIF.

**PRICE_VALUE where ZARN_REG_PIR-ORDER_UOM_PIM & ZARN_LIST_PRICE-GLN
          READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'NETPR'.
          IF sy-subrc EQ 0.
            ls_arn_reg_pir-netpr = ls_arn_list_price-price_value.
          ENDIF.

**QUANTITY where ZARN_REG_PIR-ORDER_UOM_PIM & ZARN_LIST_PRICE-GLN found
          READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'PEINH'.
          IF sy-subrc EQ 0.
            ls_arn_reg_pir-peinh = ls_arn_list_price-quantity.
          ENDIF.

**EFF_START_DATE where ZARN_REG_PIR-ORDER_UOM_PIM & ZARN_LIST_PRICE-GLN found in "SAP-PIR"
          READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'DATLB'.
          IF sy-subrc EQ 0.
            ls_arn_reg_pir-datlb = ls_arn_list_price-eff_start_date.
          ENDIF.

**EFF_END_DATE where ZARN_REG_PIR-ORDER_UOM_PIM & ZARN_LIST_PRICE-GLN found in "SAP-PIR"
          READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'PRDAT'.
          IF sy-subrc EQ 0.
            ls_arn_reg_pir-prdat = ls_arn_list_price-eff_end_date.
          ENDIF.

        ENDIF. " Select list price`

**    insert as the records to be created/updated...
        INSERT ls_arn_reg_pir INTO TABLE fc_t_arn_reg_uom.

      ENDDO.

    ENDLOOP.

  ENDIF.

ENDFORM.                    " FILL_REG_PIR
*&---------------------------------------------------------------------*
*&      Form  GET_ARENA_TO_UPDATE
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM get_data_to_update .

  DATA:
    ls_arn_reg_hdr    TYPE zarn_reg_hdr,
    ls_arn_reg_banner TYPE zarn_reg_banner,
    ls_arn_reg_uom    TYPE zarn_reg_uom,
    ls_arn_reg_ean    TYPE zarn_reg_ean,
    ls_arn_reg_pir    TYPE zarn_reg_pir,
    ls_arn_ver_status TYPE zarn_ver_status,
    ls_arn_sync       TYPE zarn_sync.

  FIELD-SYMBOLS:
    <fs_arn_reg_hdr_in_db>    LIKE LINE OF gt_arn_reg_hdr_in_db,
    <fs_arn_reg_banner_in_db> LIKE LINE OF gt_arn_reg_banner_in_db,
    <fs_arn_reg_uom_in_db>    LIKE LINE OF gt_arn_reg_uom_in_db,
    <fs_arn_reg_ean_in_db>    LIKE LINE OF gt_arn_reg_ean_in_db,
    <fs_arn_reg_pir_in_db>    LIKE LINE OF gt_arn_reg_pir_in_db,
    <fs_arn_ver_status_in_db> LIKE LINE OF gt_arn_ver_status_in_db,
    <fs_value_new>            TYPE any,
    <fs_value_old>            TYPE any.

  FREE: gt_arn_reg_hdr_in_db, gt_arn_reg_banner_in_db, gt_arn_reg_uom_in_db,
        gt_arn_reg_ean_in_db, gt_arn_reg_pir_in_db, gt_arn_ver_status_in_db,
        gt_arn_reg_hdr_in_db_old, gt_arn_reg_banner_in_db_old, gt_arn_reg_uom_in_db_old,
        gt_arn_reg_ean_in_db_old, gt_arn_reg_pir_in_db_old.

  " Regional HEADER update
  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS WITH KEY tabname = 'ZARN_REG_HDR'.
  IF sy-subrc EQ 0 AND gt_arn_reg_hdr[] IS NOT INITIAL.
**  get old DB values for existing records
    SELECT idno FROM zarn_reg_hdr INTO CORRESPONDING FIELDS OF TABLE gt_arn_reg_hdr_in_db
      FOR ALL ENTRIES IN gt_arn_reg_hdr
      WHERE idno = gt_arn_reg_hdr-idno.
    IF sy-subrc EQ 0.

      gt_arn_reg_hdr_in_db_old[] = gt_arn_reg_hdr_in_db[].

      LOOP AT gt_arn_reg_hdr INTO ls_arn_reg_hdr.

        READ TABLE gt_arn_reg_hdr_in_db ASSIGNING <fs_arn_reg_hdr_in_db> WITH TABLE KEY idno = ls_arn_reg_hdr-idno.
        IF sy-subrc EQ 0.
**        UPDATE values OF records which are SET 'X' IN sync UP TABLE
          LOOP AT gt_arn_sync INTO ls_arn_sync WHERE tabname = 'ZARN_REG_HDR'.
            " don't change CREATED DATE/TIME/BY for changing entries in header table
            IF ls_arn_sync-fldname EQ 'ERSDA' OR ls_arn_sync-fldname EQ 'ERZET' OR ls_arn_sync-fldname EQ 'ERNAM'.
              CONTINUE.
            ENDIF.

            ASSIGN COMPONENT ls_arn_sync-fldname OF STRUCTURE <fs_arn_reg_hdr_in_db> TO <fs_value_old>.
            IF sy-subrc IS INITIAL.
              ASSIGN COMPONENT ls_arn_sync-fldname OF STRUCTURE ls_arn_reg_hdr TO <fs_value_new>.
              IF sy-subrc EQ 0.
                <fs_value_old> = <fs_value_new>.
              ENDIF.
              UNASSIGN: <fs_value_old>, <fs_value_new>.
            ENDIF.
          ENDLOOP.

        ELSE.
          INSERT ls_arn_reg_hdr INTO TABLE gt_arn_reg_hdr_in_db.
        ENDIF.
      ENDLOOP.
**  NO existing records
    ELSE.
      gt_arn_reg_hdr_in_db[] = gt_arn_reg_hdr[].
    ENDIF.

  ENDIF.

  " Regional BANNER update
  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS WITH KEY tabname = 'ZARN_REG_BANNER'.
  IF sy-subrc EQ 0 AND gt_arn_reg_banner_all[] IS NOT INITIAL.

**  get old DB values for existing records
    SELECT idno banner FROM zarn_reg_banner INTO CORRESPONDING FIELDS OF TABLE gt_arn_reg_banner_in_db
      FOR ALL ENTRIES IN gt_arn_reg_banner_all
      WHERE idno = gt_arn_reg_banner_all-idno
      AND banner = gt_arn_reg_banner_all-banner.
    IF sy-subrc EQ 0.

      gt_arn_reg_banner_in_db_old[] = gt_arn_reg_banner_in_db[].

      LOOP AT gt_arn_reg_banner_all INTO ls_arn_reg_banner.

        READ TABLE gt_arn_reg_banner_in_db ASSIGNING <fs_arn_reg_banner_in_db>
          WITH TABLE KEY idno = ls_arn_reg_banner-idno banner = ls_arn_reg_banner-banner.
        IF sy-subrc EQ 0.
**        UPDATE values OF records which are SET 'X' IN sync UP TABLE
          LOOP AT gt_arn_sync INTO ls_arn_sync WHERE tabname = 'ZARN_REG_BANNER'.
            ASSIGN COMPONENT ls_arn_sync-fldname OF STRUCTURE <fs_arn_reg_banner_in_db> TO <fs_value_old>.
            IF sy-subrc IS INITIAL.
              ASSIGN COMPONENT ls_arn_sync-fldname OF STRUCTURE ls_arn_reg_banner TO <fs_value_new>.
              IF sy-subrc EQ 0.
                <fs_value_old> = <fs_value_new>.
              ENDIF.
              UNASSIGN: <fs_value_old>, <fs_value_new>.
            ENDIF.
          ENDLOOP.
**  NO existing records
        ELSE.
          INSERT ls_arn_reg_banner INTO TABLE gt_arn_reg_banner_in_db.
        ENDIF.
      ENDLOOP.
    ELSE.
      gt_arn_reg_banner_in_db[] = gt_arn_reg_banner_all[].
    ENDIF.

  ENDIF.

  " Regional UOM update
  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS WITH KEY tabname = 'ZARN_REG_UOM'.
  IF sy-subrc EQ 0 AND gt_arn_reg_uom_all[] IS NOT INITIAL.

**  get old DB values for existing records
    SELECT * FROM zarn_reg_uom INTO TABLE gt_arn_reg_uom_in_db
      FOR ALL ENTRIES IN gt_arn_reg_uom_all
      WHERE idno = gt_arn_reg_uom_all-idno
      AND uom_category = gt_arn_reg_uom_all-uom_category
      AND pim_uom_code = gt_arn_reg_uom_all-pim_uom_code
      AND hybris_internal_code = gt_arn_reg_uom_all-hybris_internal_code
      AND lower_uom = gt_arn_reg_uom_all-lower_uom
      AND lower_child_uom = gt_arn_reg_uom_all-lower_child_uom.

    IF sy-subrc EQ 0.

      gt_arn_reg_uom_in_db_old[] = gt_arn_reg_uom_in_db[].

      LOOP AT gt_arn_reg_uom_all INTO ls_arn_reg_uom.

        READ TABLE gt_arn_reg_uom_in_db ASSIGNING <fs_arn_reg_uom_in_db>
          WITH TABLE KEY idno = ls_arn_reg_uom-idno uom_category = ls_arn_reg_uom-uom_category
          pim_uom_code = ls_arn_reg_uom-pim_uom_code hybris_internal_code = ls_arn_reg_uom-hybris_internal_code
          lower_uom = ls_arn_reg_uom-lower_uom lower_child_uom = ls_arn_reg_uom-lower_child_uom.
        IF sy-subrc EQ 0.
**        UPDATE values OF records which are SET 'X' IN sync UP TABLE
          LOOP AT gt_arn_sync INTO ls_arn_sync WHERE tabname = 'ZARN_REG_UOM'.
            " don't change CREATED DATE/TIME for changing entries
            IF ls_arn_sync-fldname EQ 'ERSDA' OR ls_arn_sync-fldname EQ 'ERZET'.
              CONTINUE.
            ENDIF.

            ASSIGN COMPONENT ls_arn_sync-fldname OF STRUCTURE <fs_arn_reg_uom_in_db> TO <fs_value_old>.
            IF sy-subrc IS INITIAL.
              ASSIGN COMPONENT ls_arn_sync-fldname OF STRUCTURE ls_arn_reg_uom TO <fs_value_new>.
              IF sy-subrc EQ 0.
                <fs_value_old> = <fs_value_new>.
              ENDIF.
              UNASSIGN: <fs_value_old>, <fs_value_new>.
            ENDIF.
          ENDLOOP.
**  NO existing records
        ELSE.
          INSERT ls_arn_reg_uom INTO TABLE gt_arn_reg_uom_in_db.
        ENDIF.
      ENDLOOP.
    ELSE.
      gt_arn_reg_uom_in_db[] = gt_arn_reg_uom_all[].
    ENDIF.

  ENDIF.

  " Regional EAN update
  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS WITH KEY tabname = 'ZARN_REG_EAN'.
  IF sy-subrc EQ 0 AND gt_arn_reg_ean_all[] IS NOT INITIAL.

**  get old DB values for existing records
    SELECT * FROM zarn_reg_ean INTO TABLE gt_arn_reg_ean_in_db
      FOR ALL ENTRIES IN gt_arn_reg_ean_all
      WHERE idno = gt_arn_reg_ean_all-idno
      AND meinh = gt_arn_reg_ean_all-meinh
      AND ean11 = gt_arn_reg_ean_all-ean11.
    IF sy-subrc EQ 0.

      gt_arn_reg_ean_in_db_old[] = gt_arn_reg_ean_in_db[].

      LOOP AT gt_arn_reg_ean_all INTO ls_arn_reg_ean.

        READ TABLE gt_arn_reg_ean_in_db ASSIGNING <fs_arn_reg_ean_in_db>
          WITH TABLE KEY idno = ls_arn_reg_ean-idno meinh = ls_arn_reg_ean-meinh
          ean11 = ls_arn_reg_ean-ean11.
        IF sy-subrc EQ 0.
**        UPDATE values OF records which are SET 'X' IN sync UP TABLE
          LOOP AT gt_arn_sync INTO ls_arn_sync WHERE tabname = 'ZARN_REG_EAN'.
            ASSIGN COMPONENT ls_arn_sync-fldname OF STRUCTURE <fs_arn_reg_ean_in_db> TO <fs_value_old>.
            IF sy-subrc IS INITIAL.
              ASSIGN COMPONENT ls_arn_sync-fldname OF STRUCTURE ls_arn_reg_ean TO <fs_value_new>.
              IF sy-subrc EQ 0.
                <fs_value_old> = <fs_value_new>.
              ENDIF.
              UNASSIGN: <fs_value_old>, <fs_value_new>.
            ENDIF.
          ENDLOOP.
**  NO existing records
        ELSE.
          INSERT ls_arn_reg_ean INTO TABLE gt_arn_reg_ean_in_db.
        ENDIF.
      ENDLOOP.
    ELSE.
      gt_arn_reg_ean_in_db[] = gt_arn_reg_ean_all[].
    ENDIF.

  ENDIF.

  " Regional PIR update
  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS WITH KEY tabname = 'ZARN_REG_PIR'.
  IF sy-subrc EQ 0 AND gt_arn_reg_pir_all[] IS NOT INITIAL.

**  get old DB values for existing records
    SELECT hybris_internal_code idno order_uom_pim
      FROM zarn_reg_pir INTO CORRESPONDING FIELDS OF TABLE gt_arn_reg_pir_in_db
      FOR ALL ENTRIES IN gt_arn_reg_pir_all
      WHERE idno = gt_arn_reg_pir_all-idno
      AND order_uom_pim = gt_arn_reg_pir_all-order_uom_pim
      AND hybris_internal_code = gt_arn_reg_pir_all-hybris_internal_code.
    IF sy-subrc EQ 0.

      gt_arn_reg_pir_in_db_old[] = gt_arn_reg_pir_in_db[].

      LOOP AT gt_arn_reg_pir_all INTO ls_arn_reg_pir.

        READ TABLE gt_arn_reg_pir_in_db ASSIGNING <fs_arn_reg_pir_in_db>
          WITH TABLE KEY idno = ls_arn_reg_pir-idno order_uom_pim = ls_arn_reg_pir-order_uom_pim
          hybris_internal_code = ls_arn_reg_pir-hybris_internal_code.
        IF sy-subrc EQ 0.
**        UPDATE values OF records which are SET 'X' IN sync UP TABLE
          LOOP AT gt_arn_sync INTO ls_arn_sync WHERE tabname = 'ZARN_REG_PIR'.
            ASSIGN COMPONENT ls_arn_sync-fldname OF STRUCTURE <fs_arn_reg_pir_in_db> TO <fs_value_old>.
            IF sy-subrc IS INITIAL.
              ASSIGN COMPONENT ls_arn_sync-fldname OF STRUCTURE ls_arn_reg_pir TO <fs_value_new>.
              IF sy-subrc EQ 0.
                <fs_value_old> = <fs_value_new>.
              ENDIF.
              UNASSIGN: <fs_value_old>, <fs_value_new>.
            ENDIF.

          ENDLOOP.
**  NO existing records
        ELSE.
          INSERT ls_arn_reg_pir INTO TABLE gt_arn_reg_pir_in_db.
        ENDIF.
      ENDLOOP.
    ELSE.
      gt_arn_reg_pir_in_db[] = gt_arn_reg_pir_all[].
    ENDIF.

  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS WITH KEY tabname = 'ZARN_VER_STATUS'.
  IF sy-subrc EQ 0 AND gt_arn_ver_status[] IS NOT INITIAL.

**  get old DB values for existing records
    SELECT idno FROM zarn_ver_status INTO CORRESPONDING FIELDS OF TABLE gt_arn_ver_status_in_db
      FOR ALL ENTRIES IN gt_arn_ver_status
      WHERE idno = gt_arn_ver_status-idno.
    IF sy-subrc EQ 0.
      gt_arn_ver_status_in_db_old[] = gt_arn_ver_status_in_db[].

      LOOP AT gt_arn_ver_status INTO ls_arn_ver_status.

        READ TABLE gt_arn_ver_status_in_db ASSIGNING <fs_arn_ver_status_in_db>
          WITH TABLE KEY idno = ls_arn_ver_status-idno.
        IF sy-subrc EQ 0.
          ls_arn_ver_status-changed_at = sy-timlo.
          ls_arn_ver_status-changed_by = sy-uname.
          ls_arn_ver_status-changed_on = sy-datlo.
**        UPDATE values OF records which are SET 'X' IN sync UP TABLE
          LOOP AT gt_arn_sync INTO ls_arn_sync WHERE tabname = 'ZARN_VER_STATUS'.
            ASSIGN COMPONENT ls_arn_sync-fldname OF STRUCTURE <fs_arn_ver_status_in_db> TO <fs_value_old>.
            IF sy-subrc IS INITIAL.
              ASSIGN COMPONENT ls_arn_sync-fldname OF STRUCTURE ls_arn_ver_status TO <fs_value_new>.
              IF sy-subrc EQ 0.
                <fs_value_old> = <fs_value_new>.
              ENDIF.
              UNASSIGN: <fs_value_old>, <fs_value_new>.
            ENDIF.
          ENDLOOP.
**  NO existing records
        ELSE.
          INSERT ls_arn_ver_status INTO TABLE gt_arn_ver_status_in_db.
        ENDIF.
      ENDLOOP.
    ELSE.
      gt_arn_ver_status_in_db[] = gt_arn_ver_status[].
    ENDIF.

  ENDIF.

ENDFORM.                    " GET_ARENA_TO_UPDATE
*&---------------------------------------------------------------------*
*&      Form  UPDATE_DATA_TO_DB
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM update_data_to_db .

  DATA lt_arn_reg_data              TYPE ztarn_reg_data.
  DATA lt_arn_reg_data_old          TYPE ztarn_reg_data.
  DATA ls_arn_reg_data              TYPE zsarn_reg_data.
  DATA ls_arn_reg_data_old          TYPE zsarn_reg_data.
  DATA ls_arn_reg_hdr_in_db         TYPE zarn_reg_hdr.
  DATA ls_arn_reg_banner_in_db      TYPE zarn_reg_banner.
  DATA ls_arn_reg_uom_in_db         TYPE zarn_reg_uom.
  DATA ls_arn_reg_ean_in_db         TYPE zarn_reg_ean.
  DATA ls_arn_reg_pir_in_db         TYPE zarn_reg_pir.
  DATA ls_arn_ver_status_in_db      TYPE zarn_ver_status.
  DATA ls_arn_ver_status_in_db_old  TYPE zarn_ver_status.
  DATA ls_arn_reg_hdr               TYPE zarn_reg_hdr.
  DATA lt_arn_reg_hdr_old           TYPE ztarn_reg_hdr.
  DATA lt_arn_reg_banner_old        TYPE ztarn_reg_banner.
  DATA lt_arn_reg_uom_old           TYPE ztarn_reg_uom.
  DATA lt_arn_reg_ean_old           TYPE ztarn_reg_ean.
  DATA lt_arn_reg_pir_old           TYPE ztarn_reg_pir.
  DATA lt_arn_reg_hdr               TYPE ztarn_reg_hdr.
  DATA lt_arn_reg_banner            TYPE ztarn_reg_banner.
  DATA lt_arn_reg_uom               TYPE ztarn_reg_uom.
  DATA lt_arn_reg_ean               TYPE ztarn_reg_ean.
  DATA lt_arn_reg_pir               TYPE ztarn_reg_pir.
  DATA lt_arn_ver_status            TYPE ztarn_ver_status.
  DATA lt_arn_ver_status_old        TYPE ztarn_ver_status.
  DATA ls_arn_reg_hdr_in_db_old     LIKE LINE OF gt_arn_reg_hdr_in_db_old.
  DATA ls_arn_reg_banner_in_db_old  TYPE zarn_reg_banner.
  DATA ls_arn_reg_uom_in_db_old     TYPE zarn_reg_uom.
  DATA ls_arn_reg_ean_in_db_old     TYPE zarn_reg_ean.
  DATA ls_arn_reg_pir_in_db_old     TYPE zarn_reg_pir.
  DATA lt_arn_reg_hsno              TYPE ztarn_reg_hsno.  " ++3169 JKH 20.10.2016
  DATA lt_arn_reg_hsno_old          TYPE ztarn_reg_hsno.  " ++3169 JKH 20.10.2016


  LOOP AT gt_arn_reg_hdr_in_db INTO ls_arn_reg_hdr_in_db.

    FREE: ls_arn_reg_data, lt_arn_reg_hdr, lt_arn_reg_banner, lt_arn_reg_ean, lt_arn_reg_pir, lt_arn_reg_uom,
          lt_arn_reg_hsno.
    FREE: ls_arn_reg_data_old, lt_arn_reg_hdr_old, lt_arn_reg_banner_old, lt_arn_reg_ean_old, lt_arn_reg_pir_old,
          lt_arn_reg_uom_old, lt_arn_reg_hsno_old.

    ls_arn_reg_data-idno = ls_arn_reg_hdr_in_db-idno.
    ls_arn_reg_data_old-idno = ls_arn_reg_hdr_in_db-idno.

    " FILL HEADER DATA
    ls_arn_reg_hdr = ls_arn_reg_hdr_in_db.
    INSERT ls_arn_reg_hdr INTO TABLE lt_arn_reg_hdr.

    " FILL OLD HEADER DATA
    CLEAR ls_arn_reg_hdr_in_db_old.
    READ TABLE gt_arn_reg_hdr_in_db_old INTO ls_arn_reg_hdr_in_db_old
      WITH TABLE KEY idno = ls_arn_reg_hdr_in_db-idno.
    IF sy-subrc EQ 0.
      INSERT ls_arn_reg_hdr_in_db_old INTO TABLE lt_arn_reg_hdr_old.
    ENDIF.

    " Fill Banner data
    LOOP AT gt_arn_reg_banner_in_db INTO ls_arn_reg_banner_in_db WHERE idno = ls_arn_reg_hdr_in_db-idno.
      INSERT ls_arn_reg_banner_in_db INTO TABLE lt_arn_reg_banner.
      CLEAR ls_arn_reg_banner_in_db.
    ENDLOOP.

    " Fill old Banner data
    LOOP AT gt_arn_reg_banner_in_db_old INTO ls_arn_reg_banner_in_db_old WHERE idno = ls_arn_reg_hdr_in_db-idno.
      INSERT ls_arn_reg_banner_in_db_old INTO TABLE lt_arn_reg_banner_old.
      CLEAR ls_arn_reg_banner_in_db_old.
    ENDLOOP.

    " Fill UOM data
    LOOP AT gt_arn_reg_uom_in_db INTO ls_arn_reg_uom_in_db WHERE idno = ls_arn_reg_hdr_in_db-idno.
      INSERT ls_arn_reg_uom_in_db INTO TABLE lt_arn_reg_uom.
      CLEAR ls_arn_reg_uom_in_db.
    ENDLOOP.

    " Fill old UOM data
    LOOP AT gt_arn_reg_uom_in_db_old INTO ls_arn_reg_uom_in_db_old WHERE idno = ls_arn_reg_hdr_in_db-idno.
      INSERT ls_arn_reg_uom_in_db_old INTO TABLE lt_arn_reg_uom_old.
      CLEAR ls_arn_reg_uom_in_db_old.
    ENDLOOP.

    " Fill EAN data
    LOOP AT gt_arn_reg_ean_in_db INTO ls_arn_reg_ean_in_db WHERE idno = ls_arn_reg_hdr_in_db-idno.
      INSERT ls_arn_reg_ean_in_db INTO TABLE lt_arn_reg_ean.
      CLEAR ls_arn_reg_ean_in_db.
    ENDLOOP.

    " Fill OLD EAN data
    LOOP AT gt_arn_reg_ean_in_db_old INTO ls_arn_reg_ean_in_db_old WHERE idno = ls_arn_reg_hdr_in_db-idno.
      INSERT ls_arn_reg_ean_in_db_old INTO TABLE lt_arn_reg_ean_old.
      CLEAR ls_arn_reg_ean_in_db_old.
    ENDLOOP.

    " Fill PIR data
    LOOP AT gt_arn_reg_pir_in_db INTO ls_arn_reg_pir_in_db WHERE idno = ls_arn_reg_hdr_in_db-idno.
      INSERT ls_arn_reg_pir_in_db INTO TABLE lt_arn_reg_pir.
      CLEAR ls_arn_reg_pir_in_db.
    ENDLOOP.

    " Fill old PIR data
    LOOP AT gt_arn_reg_pir_in_db_old INTO ls_arn_reg_pir_in_db_old WHERE idno = ls_arn_reg_hdr_in_db-idno.
      INSERT ls_arn_reg_pir_in_db_old INTO TABLE lt_arn_reg_pir_old.
      CLEAR ls_arn_reg_pir_in_db_old.
    ENDLOOP.

    " Fill version status data
    LOOP AT gt_arn_ver_status_in_db INTO ls_arn_ver_status_in_db WHERE idno = ls_arn_reg_hdr_in_db-idno.
      INSERT ls_arn_ver_status_in_db INTO TABLE lt_arn_ver_status.
      CLEAR ls_arn_ver_status_in_db.
    ENDLOOP.

    " Fill OLD version status data
    LOOP AT gt_arn_ver_status_in_db_old INTO ls_arn_ver_status_in_db_old WHERE idno = ls_arn_reg_hdr_in_db-idno.
      INSERT ls_arn_ver_status_in_db_old INTO TABLE lt_arn_ver_status_old.
      CLEAR ls_arn_ver_status_in_db_old.
    ENDLOOP.

* INS Begin of Change 3169 JKH 20.10.2016

    " Fill HSNO data
    lt_arn_reg_hsno[]     = VALUE #( FOR hsno IN gt_arn_reg_hsno_in_db[]
                                              WHERE ( idno = ls_arn_reg_hdr_in_db-idno ) ( hsno ) ).

    " Fill old HSNO data
    lt_arn_reg_hsno_old[] = VALUE #( FOR hsno IN gt_arn_reg_hsno_in_db_old[]
                                              WHERE ( idno = ls_arn_reg_hdr_in_db-idno ) ( hsno ) ).

* INS End of Change 3169 JKH 20.10.2016



    " FILL REGIONAL DATA
    ls_arn_reg_data-zarn_reg_hdr    = lt_arn_reg_hdr.
    ls_arn_reg_data-zarn_reg_banner = lt_arn_reg_banner.
    ls_arn_reg_data-zarn_reg_uom    = lt_arn_reg_uom.
    ls_arn_reg_data-zarn_reg_ean    = lt_arn_reg_ean.
    ls_arn_reg_data-zarn_reg_pir    = lt_arn_reg_pir.
    ls_arn_reg_data-zarn_reg_hsno   = lt_arn_reg_hsno.                " ++3169 JKH 20.10.2016
    INSERT ls_arn_reg_data INTO TABLE lt_arn_reg_data.

    " FILL OLD REGIONAL DATA
    ls_arn_reg_data_old-zarn_reg_hdr    = lt_arn_reg_hdr_old.
    ls_arn_reg_data_old-zarn_reg_banner = lt_arn_reg_banner_old.
    ls_arn_reg_data_old-zarn_reg_uom    = lt_arn_reg_uom_old.
    ls_arn_reg_data_old-zarn_reg_ean    = lt_arn_reg_ean_old.
    ls_arn_reg_data_old-zarn_reg_pir    = lt_arn_reg_pir_old.
    ls_arn_reg_data_old-zarn_reg_hsno   = lt_arn_reg_hsno_old.         " ++3169 JKH 20.10.2016
    INSERT ls_arn_reg_data_old INTO TABLE lt_arn_reg_data_old.

  ENDLOOP.

  CALL FUNCTION 'ZARN_REGIONAL_DB_UPDATE' "IN UPDATE TASK
    EXPORTING
      it_reg_data     = lt_arn_reg_data[]
      it_reg_data_old = lt_arn_reg_data_old[]
      iv_no_delete    = abap_true
      iv_no_chg_cat   = abap_true.

* Update Version table
  CALL FUNCTION 'ZARN_VERSION_CONTROL_UPDATE' IN UPDATE TASK
    EXPORTING
*     it_control    =
*     it_prd_version =
      it_ver_status = lt_arn_ver_status[].

  CALL FUNCTION 'Z_ARN_CHG_VER_STS_CHANGE_DOC' IN UPDATE TASK
    EXPORTING
      it_ver_status     = lt_arn_ver_status[]
      it_ver_status_old = lt_arn_ver_status_old[]
      iv_chgid          = 'U'.

  COMMIT WORK AND WAIT.

* Clear ticks in Syncup customizing table and release lock
  PERFORM initialize_zarn_sync_table.          " ++3169 JKH 01.11.2016

  IF sy-subrc EQ 0.
    " generate logging on screen
    PERFORM generate_log USING lt_arn_reg_data.

  ELSE.
    " DB Update Failed
    MESSAGE TEXT-007 TYPE 'I'.
  ENDIF.

ENDFORM.                    " UPDATE_DATA_TO_DB
*&---------------------------------------------------------------------*
*&      Form  CHECK_MANDATORY_FIELDS
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM check_mandatory_fields .

  IF p_i_main IS INITIAL OR p_i_pir IS INITIAL OR p_i_uom IS INITIAL.
    MESSAGE TEXT-003 TYPE 'E'.
  ENDIF.

ENDFORM.                    " CHECK_MANDATORY_FIELDS
*&---------------------------------------------------------------------*
*&      Form  FREE_MEMORY
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM free_memory .

  FREE: gt_main, gt_uom, gt_pir,gt_mara,gt_gil_zzcatman,gt_ret_zzcatman,
        gt_retdc_zzcatman,gt_host_sap,gt_wlk2,gt_marc_sos,gt_mean,gt_arn_products,
        gt_arn_prd_ver,gt_arn_curr_prd_ver,gt_makt,gt_wrf_matgrp_prod,gt_reg_banner,
        gt_tvarv_ref_sites_uni,gt_tvarv_ref_sites_lni,gt_arn_reg_banner_all,gt_arn_reg_ean_all,
        gtr_numtp,gt_arn_reg_uom_all,gt_arn_uom_variant,gt_arn_gtin_var,gt_maw1,gt_arn_pir,
        gt_arn_list_price,gt_eina,gt_eine,gt_arn_reg_pir_all,gtr_layer_uoms,gtr_pallets_uoms,
        gt_arn_ver_status,gt_arn_reg_hdr,gt_arn_reg_hdr_in_db,gt_arn_reg_banner_in_db,
        gt_arn_reg_uom_in_db,gt_arn_reg_ean_in_db,gt_arn_reg_pir_in_db,gt_arn_ver_status_in_db,
        gt_arn_reg_hdr_in_db_old,gt_arn_reg_banner_in_db_old,gt_arn_reg_uom_in_db_old,gt_arn_reg_ean_in_db_old,
        gt_arn_reg_pir_in_db_old,gt_arn_ver_status_in_db_old,gt_arn_sync,gv_fcode,
        gt_arn_prod_uom_t, gt_pir_not_found, gt_marm_idno, gt_arn_reg_uom_ui, gt_arn_reg_uom_all_ui,
        gt_pir_not_found_all,
        gt_arn_growing ,
        gt_arn_diet_info,
        gt_arn_allergen  ,
        gt_arn_ntr_claims,
        gt_arn_hsno, gt_arn_reg_hsno_all, gt_arn_reg_hsno_in_db, gt_arn_reg_hsno_in_db_old.          " ++3169 JKH 20.10.2016






ENDFORM.                    " FREE_MEMORY
*&---------------------------------------------------------------------*
*&      Form  GENERATE_LOG
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM generate_log USING p_it_arn_reg_data TYPE ztarn_reg_data..

  DATA ls_arn_reg_data              TYPE zsarn_reg_data.
  DATA ls_main                      LIKE LINE OF gt_main.
  DATA ls_arn_products              LIKE LINE OF gt_arn_products.
  DATA ls_uom                       LIKE LINE OF gt_uom.
  DATA ls_pir                       LIKE LINE OF gt_pir.

********************* MAIN FILE LOGGING *****************************
  WRITE 'MAIN File logging'.
  NEW-LINE.
  WRITE : AT 2 'FAN ID'.
  WRITE : AT 20 'ID NO'.
  WRITE : AT 40 'Update Status'.

  LOOP AT gt_main INTO ls_main.
    CLEAR ls_arn_products.
    READ TABLE gt_arn_products INTO ls_arn_products WITH TABLE KEY fan_id = ls_main-zzfan.
    IF sy-subrc EQ 0.
      CLEAR ls_arn_reg_data.
      READ TABLE p_it_arn_reg_data INTO ls_arn_reg_data WITH KEY idno = ls_arn_products-idno.
      IF sy-subrc EQ 0.
        NEW-LINE.
        WRITE : AT 2 ls_main-zzfan.
        WRITE : AT 20 ls_arn_products-idno.
        WRITE : AT 40 'Updated successfully in DB'.
      ELSE.
        NEW-LINE.
        WRITE : AT 2 ls_main-zzfan.
        WRITE : AT 20 ls_arn_products-idno.
        WRITE : AT 40 'Not updated in DB'.
      ENDIF.
    ELSE.
      NEW-LINE.
      WRITE : AT 2 ls_main-zzfan.
      WRITE : AT 20 ''.
      WRITE : AT 40 'No IDNO found in AReNa Product Master DB for FAN ID'.
    ENDIF.

  ENDLOOP.

************************* UOM FILE LOGGING **************************
  NEW-LINE.
  ULINE.
  WRITE 'UOM File Issue logging'.
  NEW-LINE.
  WRITE : AT 2 'FAN ID'.
  WRITE : AT 20 'ID NO'.
  WRITE : AT 40 'Hybris UOM'.
  WRITE : AT 60 'Hybris PIR'.
  WRITE : AT 80 'Update Status'.
  LOOP AT gt_uom INTO ls_uom.
    CLEAR ls_arn_products.
    READ TABLE gt_arn_products INTO ls_arn_products WITH TABLE KEY fan_id = ls_uom-zzfan.
    IF sy-subrc EQ 0.

      READ TABLE gt_arn_reg_uom_in_db TRANSPORTING NO FIELDS
        WITH KEY idno = ls_arn_products-idno pim_uom_code = ls_uom-hyb_uom_code meinh = ls_uom-sap_uom_code.
      IF sy-subrc NE 0.
        NEW-LINE.
        WRITE : AT 2 ls_uom-zzfan.
        WRITE : AT 20 ls_arn_products-idno.
        WRITE : AT 40 ls_uom-hyb_uom_code.
        WRITE : AT 60 ls_uom-hyb_pir.
        WRITE : AT 80 ls_uom-sap_uom_code.
        WRITE : AT 100 'Not updated in DB'.
      ENDIF.
    ELSE.
      NEW-LINE.
      WRITE : AT 2 ls_uom-zzfan.
      WRITE : AT 20 ''.
      WRITE : AT 40 'No IDNO found in AReNa Product Master DB for FAN ID'.
    ENDIF.

  ENDLOOP.

************************ PIR FILE LOGGING ***************************
  NEW-LINE.
  ULINE.
  WRITE 'PIR File Issue logging'.
  NEW-LINE.
  WRITE : AT 2 'FAN ID'.
  WRITE : AT 20 'ID NO'.
  WRITE : AT 40 'Hybris PIR'.
  WRITE : AT 80 'Update Status'.
  LOOP AT gt_pir INTO ls_pir.
    CLEAR ls_arn_products.
    READ TABLE gt_arn_products INTO ls_arn_products WITH TABLE KEY fan_id = ls_pir-zzfan.
    IF sy-subrc EQ 0.

      READ TABLE gt_arn_reg_pir_in_db TRANSPORTING NO FIELDS
        WITH KEY idno = ls_arn_products-idno hybris_internal_code = ls_pir-hyb_pir.
      IF sy-subrc NE 0.
        NEW-LINE.
        WRITE : AT 2 ls_pir-zzfan.
        WRITE : AT 20 ls_arn_products-idno.
        WRITE : AT 40 ls_pir-hyb_pir.
        WRITE : AT 100 'Not updated in DB'.
      ENDIF.
    ELSE.
      NEW-LINE.
      WRITE : AT 2 ls_uom-zzfan.
      WRITE : AT 20 ''.
      WRITE : AT 40 'No IDNO found in AReNa Product Master DB for FAN ID'.
    ENDIF.

  ENDLOOP.

ENDFORM.                    " GENERATE_LOG
*&---------------------------------------------------------------------*
*&      Form  LOCK_SYNCUP
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      <--P_GV_NAME_TEXT  text
*----------------------------------------------------------------------*
FORM lock_syncup  CHANGING fc_v_name_text TYPE ad_namtext.

  DATA: lt_enq          TYPE STANDARD TABLE OF seqg3,
        ls_enq          LIKE LINE OF  lt_enq,
        ls_user_address TYPE addr3_val,
        lv_name_text    TYPE ad_namtext,
        lv_tabnam       TYPE zarn_sync-tabname.


  CLEAR fc_v_name_text.

  lv_tabnam = 'ZARN_SYNC'.


  CALL FUNCTION 'ENQUEUE_EZARN_SYNC'
    EXCEPTIONS
      foreign_lock   = 1
      system_failure = 2
      OTHERS         = 3.
  IF sy-subrc <> 0.
    CLEAR: lt_enq[].
*         check lock and get user name
    CALL FUNCTION 'ENQUEUE_READ'
      EXPORTING
        gclient               = sy-mandt
        gname                 = 'ZARN_SYNC'
        guname                = ' '
      TABLES
        enq                   = lt_enq
      EXCEPTIONS
        communication_failure = 1
        system_failure        = 2
        OTHERS                = 3.
    IF lt_enq[] IS NOT INITIAL.
      READ TABLE lt_enq INTO ls_enq INDEX 1.

      CLEAR: ls_user_address, lv_name_text.
      CALL FUNCTION 'SUSR_USER_ADDRESS_READ'
        EXPORTING
          user_name              = ls_enq-guname
        IMPORTING
          user_address           = ls_user_address
        EXCEPTIONS
          user_address_not_found = 1
          OTHERS                 = 2.
      IF sy-subrc IS INITIAL.
        lv_name_text = ls_user_address-name_text.
      ELSE.
        lv_name_text = ls_enq-guname.
      ENDIF.
    ENDIF.  " IF lt_enq[] IS NOT INITIAL

  ENDIF.



  fc_v_name_text = lv_name_text.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  INITIALIZE_ZARN_SYNC_TABLE
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
FORM initialize_zarn_sync_table .

  FIELD-SYMBOLS: <ls_zarn_sync> LIKE LINE OF gt_arn_sync.


  LOOP AT gt_arn_sync ASSIGNING <ls_zarn_sync>.
    CLEAR <ls_zarn_sync>-syncup.
  ENDLOOP.


  MODIFY zarn_sync FROM TABLE gt_arn_sync[].

  CALL FUNCTION 'DEQUEUE_EZARN_SYNC'
    EXPORTING
*     MODE_ZARN_SYNC       = 'E'
      mandt   = sy-mandt
      tabname = 'ZARN_SYNC'
*     FLDNAME =
*     X_TABNAME            = ' '
*     X_FLDNAME            = ' '
*     _SCOPE  = '3'
*     _SYNCHRON            = ' '
*     _COLLECT             = ' '
    .



ENDFORM.
