*&---------------------------------------------------------------------*
*&  Include           ZIARN_GUI_F02
*&---------------------------------------------------------------------*

*&---------------------------------------------------------------------*
*&      Form  get_data_approval
*&---------------------------------------------------------------------*
FORM get_data_approval.

  DATA: lt_key         TYPE ztarn_key,
        lt_key_gtin    TYPE ztarn_key,
        lt_key_cr      TYPE ztarn_key,
        lt_key_vn      TYPE ztarn_key,
        lv_version1    TYPE zarn_prd_version-version,
        ls_short_descr LIKE LINE OF  gr_short_descr,
        lr_short_descr TYPE RANGE OF zarn_fs_short_descr_upper,
        lv_select      TYPE abap_bool.

*  RANGES: lr_gln FOR zarn_products-gln.

  FIELD-SYMBOLS: <ls_key> LIKE LINE OF lt_key.

  FREE : lt_key[],
         lt_key_cr[],
         lt_key_vn[].

* at the moment we have 3 description fields in ZARN_PRODUCTS
* of which FS_SHORT_DESCR_UPPER, the following logic an SELECT caters for
* case sensitive search !
*   upper case search

** added for new selection criteria - START
  IF s_aprdes[]  IS NOT INITIAL OR
     s_aprdes    IS NOT INITIAL.
    LOOP AT s_aprdes.
      MOVE-CORRESPONDING s_aprdes TO ls_short_descr.
      TRANSLATE  ls_short_descr TO UPPER CASE.
      APPEND ls_short_descr TO lr_short_descr[].
    ENDLOOP.
  ENDIF.

* data selection
  FREE lt_key_gtin[].
  IF s_aprplu[] IS NOT INITIAL.
    SELECT e~idno p~version
      INTO TABLE lt_key_gtin
      FROM zarn_reg_ean AS e
      INNER JOIN zarn_products AS p
        ON p~idno EQ e~idno
      WHERE e~ean11 IN s_aprplu[]
      AND   e~eantp EQ 'ZP'.
    lv_select = abap_true.
  ENDIF.

  IF s_aprgt[] IS NOT INITIAL.
    IF lt_key_gtin[] IS INITIAL.
      SELECT e~idno p~version
        INTO TABLE lt_key_gtin
        FROM zarn_reg_ean AS e
        INNER JOIN zarn_products AS p
          ON p~idno EQ e~idno
        WHERE e~ean11 IN s_aprgt[].
    ELSE.
*      IF lv_select EQ abap_false.
      SELECT e~idno p~version
        INTO TABLE lt_key_gtin
        FROM zarn_reg_ean AS e
        INNER JOIN zarn_products AS p
          ON p~idno EQ e~idno
          FOR ALL ENTRIES IN lt_key_gtin
          WHERE e~idno  EQ lt_key_gtin-idno
          AND   e~ean11 IN s_aprgt[].
*      ENDIF.
    ENDIF.
    lv_select = abap_true.
  ENDIF.

  IF s_aprgt[] IS NOT INITIAL.
    IF lt_key_gtin[] IS INITIAL.
      SELECT idno version
        INTO TABLE lt_key_gtin
        FROM zarn_gtin_var
        WHERE gtin_code IN s_aprgt[].

    ELSE.
      SELECT idno version
        APPENDING TABLE lt_key_gtin
        FROM zarn_gtin_var
        WHERE gtin_code IN s_aprgt[].

    ENDIF.
    lv_select = abap_true.
  ENDIF.
** added for new selection criteria - END

  gv_approval_data = abap_false.

** added for new selection criteria - START
  IF lv_select EQ abap_true.

    IF lt_key_gtin[] IS NOT INITIAL.

      SELECT p~idno p~version
        INTO TABLE lt_key
        FROM zarn_products AS p
*        INNER JOIN zarn_gtin_var AS g
*          ON  g~idno    EQ p~idno
*          AND g~version EQ p~version
        INNER JOIN zarn_prd_version AS v
          ON  v~idno    EQ p~idno
          AND v~version EQ p~version
        INNER JOIN zarn_ver_status AS s " added
          ON  s~idno         EQ v~idno
          AND s~current_ver  EQ v~version
*          INNER JOIN zarn_reg_hdr AS r
*            ON  r~idno    EQ p~idno
*            AND r~version EQ p~version
        INNER JOIN zarn_reg_hdr AS h
          ON  h~idno    EQ p~idno
        INNER JOIN zarn_approval AS a
          ON a~idno EQ p~idno
        FOR ALL ENTRIES IN lt_key_gtin[]
          WHERE v~idno                EQ lt_key_gtin-idno
          AND v~version               EQ lt_key_gtin-version
          AND s~article_ver           IN gt_art_ver_ra[] "+3162
*            AND v~version_status        IN s_aprhs[] "+3162
*            AND v~release_status        IN s_aprhr[] "+3162
          AND p~idno                  IN s_aprhid[]
*         AND g~gtin_code             IN s_aprgt
*            AND p~matnr_ni              IN s_aprmt[]         "--IR5118966 JKH 02.09.2016
          AND p~fan_id                IN s_aprfi[]
          AND p~fan_id                NE space
          AND p~fsni_icare_value      EQ abap_true
*            AND p~status                IN s_aprhs
          AND p~fs_brand_id           IN s_aprbrn[]         " added
          AND p~fs_short_descr_upper  IN lr_short_descr[]   " added
          AND h~matnr                 IN s_aprmt[]           "++IR5118966 JKH 02.09.2016
*            AND h~status                IN s_aprrs[] "+3162
*            AND h~release_status        IN s_aprrr[] "+3162
          AND ( h~ret_zzcatman        IN s_aprrcm[]          " added
          OR  h~gil_zzcatman          IN s_aprcmn[] )        " added
*            AND h~ersda                 IN s_aprcr[] "+3162          " added
*            AND h~laeda                 IN s_aprco[] "+3162          " added
          AND a~approval_area         IN gt_appr_area_ra[] AND "+3162
              a~chg_category          IN s_apcat[] AND "+3162
              a~team_code             IN s_aptem[] AND "+3162
              a~status                IN s_apsta[]. "+3162

      SORT lt_key BY idno version.
      DELETE ADJACENT DUPLICATES FROM lt_key COMPARING idno version.
*      ENDIF. "+3162

    ENDIF.

  ELSE.

    SELECT p~idno p~version
      INTO TABLE lt_key
      FROM zarn_products AS p
      INNER JOIN zarn_prd_version AS v
        ON  v~idno    EQ p~idno
        AND v~version EQ p~version
      INNER JOIN zarn_ver_status AS s " added
        ON  s~idno         EQ v~idno
        AND s~current_ver  EQ v~version
      INNER JOIN zarn_gtin_var AS g
        ON  g~idno    EQ p~idno
        AND g~version EQ p~version
*      INNER JOIN zarn_reg_hdr AS r +3162 Bug
*        ON  r~idno    EQ p~idno
*        AND r~version EQ p~version
      INNER JOIN zarn_reg_hdr AS h
        ON  h~idno    EQ p~idno
      INNER JOIN zarn_approval AS a
        ON a~idno = p~idno
      WHERE g~gtin_code             IN s_aprgt[]
        AND p~idno                  IN s_aprhid[]
*          AND p~matnr_ni              IN s_aprmt[]         "--IR5118966 JKH 02.09.2016
        AND p~fan_id                IN s_aprfi[]
        AND p~fan_id                NE space
        AND p~fsni_icare_value      EQ abap_true
*          AND p~status                IN s_aprhs
        AND p~fs_brand_id           IN s_aprbrn[]         " added
        AND p~fs_short_descr_upper  IN lr_short_descr[]   " added
        AND s~article_ver           IN gt_art_ver_ra[]
*          AND v~version_status        IN s_aprhs[] "+3612
*          AND v~release_status        IN s_aprhr[] "+3612
*          AND v~changed_on            IN s_aprco[] "+3612
*          AND r~status                IN s_aprrs[] "+3612
*          AND r~release_status        IN s_aprrr[] "+3612
        AND h~matnr                 IN s_aprmt[]           "++IR5118966 JKH 02.09.2016
        AND ( h~ret_zzcatman        IN s_aprrcm[]          " added
        OR  h~gil_zzcatman          IN s_aprcmn[] )        " added
*          AND h~ersda                 IN s_aprcr[] "+3612           " added
*          AND h~laeda                 IN s_aprco[] "+3612          " added
        AND a~approval_area         IN gt_appr_area_ra[] AND "+3162
            a~chg_category          IN s_apcat[] AND "+3162
            a~team_code             IN s_aptem[] AND "+3162
            a~status                IN s_apsta[] "+3162
        GROUP BY p~idno p~version.
*    ENDIF. "+3162
  ENDIF.

  IF NOT lt_key_cr[] IS INITIAL.
    SORT lt_key_cr BY idno.
    LOOP AT lt_key ASSIGNING <ls_key>.
      READ TABLE lt_key_cr TRANSPORTING NO FIELDS WITH KEY idno = <ls_key>-idno BINARY SEARCH.
      IF sy-subrc NE 0.
        CLEAR <ls_key>-idno.
      ENDIF.
    ENDLOOP.
  ENDIF.
  DELETE lt_key WHERE idno IS INITIAL.

  IF NOT s_aprgl[] IS INITIAL
  OR NOT s_aprgn[] IS INITIAL
  OR NOT s_aprgv[] IS INITIAL.
    IF NOT lt_key[] IS INITIAL.
      SELECT idno version
        INTO TABLE lt_key_vn
        FROM zarn_pir
        FOR ALL ENTRIES IN lt_key
        WHERE idno                  EQ lt_key-idno
        AND   version               EQ lt_key-version
        AND   gln                   IN s_aprgl[]
*        AND   gln                   IN lr_gln
        AND   gln_description       IN s_aprgn
        AND   vendor_article_number IN s_aprgv.
      IF lt_key_vn[] IS INITIAL.
*       Nothing matching vendor search so exit
        EXIT.
      ENDIF.
    ENDIF.

    IF NOT lt_key_vn[] IS INITIAL.
      SORT lt_key_vn BY idno version.
      LOOP AT lt_key ASSIGNING <ls_key>.
        READ TABLE lt_key_vn TRANSPORTING NO FIELDS WITH KEY idno = <ls_key>-idno version = <ls_key>-version BINARY SEARCH.
        IF sy-subrc NE 0.
          CLEAR <ls_key>-idno.
        ENDIF.
      ENDLOOP.
    ENDIF.
    DELETE lt_key WHERE idno IS INITIAL.
  ENDIF.

  IF NOT lt_key[] IS INITIAL.
    PERFORM appr_query_post_process CHANGING lt_key.
  ENDIF.

ENDFORM.                    "get_data_approval

*>>>IS1609mod Feature 3162 - AReNa Approval Stabilisation

FORM appr_initialize  USING  pv_called_from TYPE char3
                    CHANGING pcv_exit TYPE xfeld
                             pcv_input_disabled TYPE xfeld.

  "---refreshing some globals
  CLEAR pcv_exit.
  CLEAR pcv_input_disabled.

  REFRESH gt_approval_comments.

  IF pv_called_from EQ co_appr_called_from_nat.
    REFRESH gt_appr_nat_approvals.
  ENDIF.
  IF pv_called_from EQ co_appr_called_from_reg.
    REFRESH gt_appr_reg_approvals.
  ENDIF.

  IF pv_called_from EQ co_appr_called_from_nat.
    gv_appr_nat_approvals = abap_false.
  ENDIF.
  IF pv_called_from EQ co_appr_called_from_reg.
    gv_appr_reg_approvals = abap_false.
  ENDIF.


  "---determine "complete exit
  IF gs_worklist-idno IS INITIAL "if IDNO is initial
  OR ( gv_nat_pb_00 NE icon_collapse
    AND gv_reg_pb_00 NE icon_collapse ). "or both approvals are hidden
    "do not proceed and exit completele
    pcv_exit = abap_true.
    RETURN.
  ENDIF.



  "---determine "input disabled"
  IF zarn_products-fan_id IS INITIAL
  OR zarn_products-fsni_icare_value IS INITIAL.
    pcv_input_disabled = abap_true.
  ENDIF.


  IF pv_called_from EQ co_appr_called_from_nat
   AND gs_worklist-version_status CS 'OBS'.
    pcv_input_disabled = abap_true.
  ENDIF.

  IF pv_called_from EQ co_appr_called_from_reg
    AND  zarn_reg_hdr-status CS 'OBS'.
    pcv_input_disabled = abap_true.
  ENDIF.

  IF pv_called_from EQ co_appr_called_from_reg.

    gv_appr_nat_is_approved =  go_approval->is_fully_approved( iv_approval_area = zcl_arn_approval_backend=>gc_appr_area_national
                                                               iv_no_records_as_approved = abap_true ).

    IF gv_appr_nat_is_approved EQ abap_false.
      gv_appr_national_not_approved = 'Regional cannot be approved until National is approved'(ap9).
      pcv_input_disabled = abap_true.
    ELSE.
      CLEAR gv_appr_national_not_approved.
    ENDIF.
  ENDIF.


  IF gv_display = abap_true.
    pcv_input_disabled = abap_true.
  ENDIF.

  "---disable buttons based on flag "input disabled"
  IF pcv_input_disabled = abap_true.
    LOOP AT SCREEN.
      IF screen-name EQ 'BUT_APPR_APPROVE'
      OR screen-name EQ 'BUT_APPR_REJECT'
      OR screen-name EQ 'BUT_APPR_TOGGLE_EDIT'.
        screen-input = 0.
        MODIFY SCREEN.
      ENDIF.
    ENDLOOP.
  ENDIF.

ENDFORM.

FORM appr_update_hdr_status USING pv_called_from TYPE char3.

  DATA: ls_prd_version LIKE LINE OF gt_prd_version,
        ls_reg_hdr     TYPE zarn_reg_hdr.

  IF pv_called_from EQ co_appr_called_from_nat.
    READ TABLE gt_prd_version ASSIGNING FIELD-SYMBOL(<ls_prd_version>)
      WITH KEY idno = gs_worklist-idno version = gs_worklist-version.
    IF sy-subrc EQ 0.
      CLEAR ls_prd_version.
      ls_prd_version = <ls_prd_version>.
      IF gv_appr_nat_release_status NE space.
        <ls_prd_version>-release_status = gv_appr_nat_release_status.
      ELSE.
        gv_appr_nat_release_status = <ls_prd_version>-release_status.
      ENDIF.
      IF <ls_prd_version>-version_status EQ co_status_apr.
        IF gv_appr_nat_approvals EQ abap_true.
*         we have approvals to do so switch from APR to WIP
          <ls_prd_version>-version_status = co_status_wip.
        ENDIF.
      ENDIF.
      IF <ls_prd_version> NE ls_prd_version.
        PERFORM update_nat_db_status USING ls_prd_version <ls_prd_version>.
      ENDIF.
      gv_appr_nat_status = <ls_prd_version>-version_status.
    ENDIF.
  ENDIF.

  IF pv_called_from EQ co_appr_called_from_reg.
    READ TABLE gt_reg_data ASSIGNING FIELD-SYMBOL(<ls_reg_data>)
           WITH KEY idno = gs_worklist-idno.
    IF sy-subrc EQ 0.
      READ TABLE <ls_reg_data>-zarn_reg_hdr ASSIGNING FIELD-SYMBOL(<ls_reg_hdr>)
         WITH KEY idno = gs_worklist-idno.
      IF sy-subrc EQ 0.
        ls_reg_hdr = <ls_reg_hdr>.
        IF gv_appr_reg_release_status NE space.
          <ls_reg_hdr>-release_status = gv_appr_reg_release_status.
        ELSE.
          gv_appr_reg_release_status = <ls_reg_hdr>-release_status.
        ENDIF.
        IF ( <ls_reg_hdr>-status EQ co_status_apr
          OR <ls_reg_hdr>-status EQ co_status_cre )  "IS FIX - allow change to WIP also from CRE
          AND gv_appr_reg_approvals EQ abap_true.
*           we have approvals to do so switch from APR/CRE to WIP
          <ls_reg_hdr>-status = co_status_wip.
        ENDIF.
        IF <ls_reg_hdr> NE ls_reg_hdr.
          PERFORM update_reg_db_status USING ls_reg_hdr <ls_reg_hdr>.
        ENDIF.
        gv_appr_reg_status = <ls_reg_hdr>-status.
      ENDIF.
    ENDIF.
  ENDIF.

  "update the texts
  CLEAR: gv_appr_nat_release_statust,
         gv_appr_reg_release_statust.
  SELECT SINGLE release_status_desc
    INTO gv_appr_nat_release_statust
    FROM zarn_rel_statust                               "#EC CI_NOFIELD
    WHERE release_status EQ gv_appr_nat_release_status.
  SELECT SINGLE release_status_desc
    INTO gv_appr_reg_release_statust
    FROM zarn_rel_statust                               "#EC CI_NOFIELD
    WHERE release_status EQ gv_appr_reg_release_status.


ENDFORM.


FORM appr_create_alv   USING piv_container_name    TYPE char20
                       CHANGING pcr_grid_container TYPE REF TO cl_gui_custom_container
                                pcr_grid           TYPE REF TO cl_gui_alv_grid.


  IF pcr_grid_container  IS INITIAL.
    CREATE OBJECT pcr_grid_container
      EXPORTING
        container_name              = piv_container_name
      EXCEPTIONS
        cntl_error                  = 1
        cntl_system_error           = 2
        create_error                = 3
        lifetime_error              = 4
        lifetime_dynpro_dynpro_link = 5
        OTHERS                      = 6.
  ENDIF.

  IF NOT pcr_grid_container IS INITIAL.
    IF NOT  pcr_grid   IS INITIAL.
      CALL METHOD pcr_grid->free
        EXCEPTIONS
          cntl_error        = 1
          cntl_system_error = 2
          OTHERS            = 3.
    ENDIF.
    FREE  pcr_grid   .
    CREATE OBJECT pcr_grid
      EXPORTING
        i_parent          = pcr_grid_container
      EXCEPTIONS
        error_cntl_create = 1
        error_cntl_init   = 2
        error_cntl_link   = 3
        error_dp_create   = 4
        OTHERS            = 5.
  ENDIF.


ENDFORM.

FORM appr_disable_buttons USING pv_called_from TYPE char3.

  IF ( pv_called_from EQ co_appr_called_from_nat AND gv_appr_nat_approvals EQ abap_false )
OR ( pv_called_from EQ co_appr_called_from_reg AND gv_appr_reg_approvals EQ abap_false ).
*   Nothing to do so disable the buttons
    LOOP AT SCREEN.
      IF screen-name EQ 'BUT_APPR_APPROVE'
      OR screen-name EQ 'BUT_APPR_REJECT'.
        screen-input = 0.
        MODIFY SCREEN.
      ENDIF.
    ENDLOOP.
  ENDIF.


ENDFORM.

FORM appr_adjust_colours CHANGING pct_appr_output TYPE ty_t_appr_output.

  DATA : ls_cellcolour TYPE lvc_s_scol,
         lv_fieldname  TYPE char40.

  FIELD-SYMBOLS : <ls_col_appr> TYPE zarn_approval_column,
                  <ls_col_stat> TYPE zarn_approval_column,
                  <ls_col_reje> TYPE zarn_approval_column,
                  <ls_col_text> TYPE zarn_approval_column.

* need to recolor some of the cells
  LOOP AT pct_appr_output ASSIGNING FIELD-SYMBOL(<ls_appr_output>).
    LOOP AT gt_approval_teams ASSIGNING FIELD-SYMBOL(<ls_approval_team>).
      UNASSIGN: <ls_col_appr>,
                <ls_col_reje>,
                <ls_col_text>.
      CONCATENATE
        '<ls_appr_output>-'
        co_appr_column_approve
        <ls_approval_team>-display_order
        INTO lv_fieldname.
      ASSIGN (lv_fieldname) TO <ls_col_appr>.
      CONCATENATE
        '<ls_appr_output>-'
        co_appr_column_reject
        <ls_approval_team>-display_order
        INTO lv_fieldname.
      ASSIGN (lv_fieldname) TO <ls_col_reje>.
      CONCATENATE
        '<ls_appr_output>-'
        co_appr_column_text
        <ls_approval_team>-display_order
        INTO lv_fieldname.
      ASSIGN (lv_fieldname) TO <ls_col_text>.

      IF <ls_col_appr> IS ASSIGNED.
        IF <ls_col_appr> IS INITIAL
        OR <ls_col_appr> NE icon_okay.
          CLEAR ls_cellcolour.
          ls_cellcolour-color-col = 2.
          CONCATENATE
            co_appr_column_approve
            <ls_approval_team>-display_order
            INTO ls_cellcolour-fname.
          APPEND ls_cellcolour TO <ls_appr_output>-cellcolour.
        ENDIF.
      ENDIF.
      IF <ls_col_reje> IS ASSIGNED.
        IF <ls_col_reje> IS INITIAL
        OR <ls_col_reje> NE icon_cancel.
          CLEAR ls_cellcolour.
          ls_cellcolour-color-col = 2.
          CONCATENATE
            co_appr_column_reject
            <ls_approval_team>-display_order
            INTO ls_cellcolour-fname.
          APPEND ls_cellcolour TO <ls_appr_output>-cellcolour.
        ENDIF.
      ENDIF.
      IF <ls_col_text> IS ASSIGNED.
        IF <ls_col_text> IS INITIAL
        OR <ls_col_text> EQ icon_display_text.
          CLEAR ls_cellcolour.
          ls_cellcolour-color-col = 2.
          CONCATENATE
            co_appr_column_text
            <ls_approval_team>-display_order
            INTO ls_cellcolour-fname.
          APPEND ls_cellcolour TO <ls_appr_output>-cellcolour.
        ENDIF.
      ENDIF.
    ENDLOOP.
  ENDLOOP.


ENDFORM.

FORM appr_finalize_and_display USING pit_appr_output TYPE ty_t_appr_output
                                     pir_grid        TYPE REF TO cl_gui_alv_grid
                                     pit_fieldcat    TYPE lvc_t_fcat
                            CHANGING pct_gt_appr     TYPE ty_t_appr_output.


  REFRESH pct_gt_appr .
  APPEND LINES OF pit_appr_output TO pct_gt_appr.

  gs_appr_layout-ctab_fname = 'CELLCOLOUR'.
  gs_appr_layout-sel_mode   = 'B'.
  gs_appr_layout-no_rowmark = abap_true.
  gs_appr_layout-no_toolbar = abap_true.

  CALL METHOD pir_grid->set_table_for_first_display
    EXPORTING
      is_layout                     = gs_appr_layout
    CHANGING
      it_outtab                     = pct_gt_appr
      it_fieldcatalog               = pit_fieldcat
    EXCEPTIONS
      invalid_parameter_combination = 1
      program_error                 = 2
      too_many_lines                = 3
      OTHERS                        = 4.

ENDFORM.



FORM appr_action_check_before USING   pv_called_from   TYPE char3
                                      pv_status        TYPE zarn_approval_status
                             CHANGING pcv_exit         TYPE xfeld.

  DATA lv_new_version      TYPE flag.

  CLEAR pcv_exit.

  PERFORM check_for_new_version CHANGING lv_new_version.
  IF lv_new_version EQ abap_true.
    MESSAGE i046.
    pcv_exit = abap_true.
    EXIT.
  ENDIF.

  IF ( pv_called_from EQ co_appr_called_from_nat
    AND gv_appr_nat_approvals EQ abap_false )
    OR ( pv_called_from EQ co_appr_called_from_reg
    AND gv_appr_reg_approvals EQ abap_false ).

    CASE pv_status.
      WHEN zif_arn_approval_status=>gc_arn_appr_status_approved.
        MESSAGE s041.
        pcv_exit = abap_true.
        EXIT.
      WHEN zif_arn_approval_status=>gc_arn_appr_status_rejected.
        MESSAGE s042.
        pcv_exit = abap_true.
        EXIT.
    ENDCASE.
  ENDIF.


  IF pv_status EQ zif_arn_approval_status=>gc_arn_appr_status_approved
  OR pv_status EQ zif_arn_approval_status=>gc_arn_appr_status_rejected.
*    PERFORM check_alv_data_change.
*    IF gv_data_change IS INITIAL.
    PERFORM check_reg_changes CHANGING gv_data_change.
    IF gv_data_change EQ abap_true.
      MESSAGE i049.
      pcv_exit = abap_true.
      EXIT.
    ENDIF.


    IF pv_called_from EQ co_appr_called_from_reg
      AND  pv_status  EQ zif_arn_approval_status=>gc_arn_appr_status_approved.
      gv_validation_errors = abap_false.
      PERFORM process_regional_data USING abap_false.
      IF gv_validation_errors NE abap_false.
        MESSAGE s047.
        pcv_exit = abap_true.
        EXIT.
      ENDIF.
    ENDIF.

  ENDIF.


ENDFORM.


FORM appr_action_stat_update USING pv_called_from   TYPE char3
                                   pv_status        TYPE zarn_approval_status.

  CASE pv_called_from .
    WHEN co_appr_called_from_nat.


      CASE pv_status.

        WHEN zif_arn_approval_status=>gc_arn_appr_status_approved.
          PERFORM update_nat_status USING gs_worklist-idno gs_worklist-version co_status_wip.

        WHEN zif_arn_approval_status=>gc_arn_appr_status_rejected.
          PERFORM update_nat_status USING gs_worklist-idno gs_worklist-version co_status_rej.

        WHEN zif_arn_approval_status=>gc_arn_appr_status_reset.
          PERFORM update_nat_status USING gs_worklist-idno gs_worklist-version co_status_wip.

      ENDCASE.

    WHEN co_appr_called_from_reg.


      CASE pv_status.

        WHEN zif_arn_approval_status=>gc_arn_appr_status_approved.
          PERFORM update_reg_status USING gs_worklist-idno co_status_wip.

        WHEN zif_arn_approval_status=>gc_arn_appr_status_rejected.
          PERFORM update_reg_status USING gs_worklist-idno co_status_rej.

        WHEN zif_arn_approval_status=>gc_arn_appr_status_reset.
          PERFORM update_reg_status USING gs_worklist-idno co_status_wip.

      ENDCASE.

  ENDCASE.

ENDFORM.


FORM appr_do_reg_defaulting.

  DATA: ls_reg_data           TYPE zsarn_reg_data,
        ls_reg_data_def       TYPE zsarn_reg_data,
        ls_prod_data          TYPE zsarn_prod_data,
        ls_reg_pir_relif      LIKE LINE OF gt_zarn_reg_pir,
        lv_no_uom_avail       TYPE flag,
        lv_no_uom_avail_ean   TYPE flag,
        lv_dim_overflow_error TYPE flag.

  CLEAR: ls_reg_data_def,
          ls_prod_data,
          ls_reg_data.

  READ TABLE gt_prod_data INTO ls_prod_data
      WITH KEY idno    =  gs_worklist-idno
               version =  gs_worklist-version.
  IF sy-subrc EQ 0.
    READ TABLE gt_reg_data INTO ls_reg_data
         WITH KEY idno =  gs_worklist-idno.
    IF sy-subrc EQ 0.
      CLEAR: lv_no_uom_avail, lv_no_uom_avail_ean, lv_dim_overflow_error.
      CALL METHOD gr_gui_load->set_regional_default
        EXPORTING
          is_reg_data           = ls_reg_data
          is_prod_data          = ls_prod_data
          iv_reg_only           = gv_display
        IMPORTING
          es_reg_data_def       = ls_reg_data_def
          et_reg_uom_ui         = gt_zarn_reg_uom[]
          et_reg_pir_ui         = gt_zarn_reg_pir[]
          et_reg_ean_ui         = gt_zarn_reg_ean[]
          ev_no_uom_avail       = lv_no_uom_avail
          ev_no_uom_avail_ean   = lv_no_uom_avail_ean
          ev_dim_overflow_error = lv_dim_overflow_error.

      gt_zarn_reg_hdr[] = ls_reg_data_def-zarn_reg_hdr[].
*        gt_zarn_reg_ean[] = ls_reg_data_def-zarn_reg_ean[].

      " set reg. uom table field celltab to disable fields
      PERFORM set_disable_columns USING ls_prod_data-zarn_uom_variant.

* Default REG HDR fields
      READ TABLE gt_zarn_reg_hdr[] INTO zarn_reg_hdr
        WITH KEY idno = gs_worklist-idno.


* Default Regular Vendor for Basic screen
      CLEAR zarn_reg_pir-lifnr.

      CLEAR: ls_reg_pir_relif.
      READ TABLE gt_zarn_reg_pir[] INTO ls_reg_pir_relif
      WITH KEY idno = gs_worklist-idno
               relif = abap_true.
      IF sy-subrc EQ 0 AND
         ls_reg_pir_relif-lifnr IS NOT INITIAL AND
         ls_reg_pir_relif-lifnr NE 'MULTIPLE'.
        zarn_reg_pir-lifnr = ls_reg_pir_relif-lifnr.
      ENDIF.

      IF lv_no_uom_avail = abap_true OR lv_no_uom_avail_ean = abap_true.
* No more UOMs available to be defaulted. Kindly Contact IMS.
        MESSAGE s085 DISPLAY LIKE 'E'.
      ENDIF.

* Exceeds the maximum length for Dim. and Wt. values, proceed manually.
      IF lv_dim_overflow_error = abap_true.
        MESSAGE s087 DISPLAY LIKE 'E'.
      ENDIF.

      "TBD automatic save after regional defaulting


    ENDIF.
  ENDIF.


ENDFORM.

FORM appr_post_article.

  DATA: lt_key        TYPE ztarn_key,
        ls_key        LIKE LINE OF lt_key,
        lr_status     TYPE ztarn_status_range,
        lrs_status    LIKE LINE OF lr_status,
        lt_message    TYPE bal_t_msg,
        lv_post_dest  TYPE rfcdes-rfcdest,
        lv_own_logsys TYPE tbdls-logsys,
        ls_tvarvc     TYPE tvarvc,
        lv_rfcchk     TYPE boolean.

  REFRESH: lt_key,
           lr_status,
           lt_message.
  CLEAR ls_key.
  ls_key-idno    = gs_worklist-idno.
  ls_key-version = gs_worklist-version.
  APPEND ls_key TO lt_key.

  REFRESH lr_status.
  SELECT sign opti AS option low high
    INTO TABLE lr_status
    FROM tvarvc
    WHERE name EQ 'ZARN_ARTICLE_UPDATE_STATUS'.
  IF lr_status[] IS INITIAL.
    CLEAR lrs_status.
    lrs_status-sign   = 'I'.
    lrs_status-option = 'EQ'.
    lrs_status-low    = 'APR'.
    APPEND lrs_status TO lr_status.
  ENDIF.

  CLEAR: lv_post_dest,
         ls_tvarvc.
  SELECT SINGLE *
    INTO ls_tvarvc
    FROM tvarvc
    WHERE name EQ gc_stvarv_post_dest
    AND   type EQ 'P'.
  IF sy-subrc EQ 0 AND ls_tvarvc-low IS NOT INITIAL.
    lv_post_dest = ls_tvarvc-low.

    CALL FUNCTION 'CHECK_RFC_DESTINATION'
      EXPORTING
        i_destination              = lv_post_dest
      IMPORTING
        e_user_password_incomplete = lv_rfcchk.
    IF lv_rfcchk NE space.
      CLEAR lv_post_dest.
    ENDIF.
  ENDIF.

  IF lv_post_dest IS INITIAL.
    CALL FUNCTION 'OWN_LOGICAL_SYSTEM_GET'
      IMPORTING
        own_logical_system             = lv_own_logsys
      EXCEPTIONS
        own_logical_system_not_defined = 1
        OTHERS                         = 2.
    lv_post_dest = lv_own_logsys.
  ENDIF.

  CALL FUNCTION 'ZARN_ARTICLE_POSTING'
    DESTINATION lv_post_dest
    EXPORTING
      it_key           = lt_key
      ir_status        = lr_status
      iv_batch         = ' '
      iv_display_log   = ' '
      iv_no_change_doc = ' '
    IMPORTING
      et_message       = lt_message.
  PERFORM popup_appl_log USING lt_message.

* Refresh Screen fields after Article Posting
  PERFORM refresh_screen_field.

ENDFORM.



*<<<IS1609mod Feature 3162 - AReNa Approval Stabilisation

*&---------------------------------------------------------------------*
*&      Form  check_for_new_version
*&---------------------------------------------------------------------*
FORM check_for_new_version CHANGING pv_new_version_exists TYPE flag.

  DATA: ls_ver_status TYPE zarn_ver_status.

  pv_new_version_exists = abap_false.

  SELECT SINGLE @abap_true INTO @pv_new_version_exists
    FROM zarn_ver_status
    WHERE idno        EQ @gs_worklist-idno
    AND   current_ver GT @gs_worklist-version.

ENDFORM.                    "check_for_new_version

*&---------------------------------------------------------------------*
*&      Form  approvals_reset
*&---------------------------------------------------------------------*
FORM approvals_reset USING pv_called_from     TYPE char3
                           ps_approval        TYPE zarn_approvals_fields
                           pv_col_number      TYPE n
                           pv_change_category TYPE zarn_chg_category
                           pv_team_code       TYPE zarn_team_code
                           pv_status          TYPE zarn_approval_status.

  DATA: lv_col_number      TYPE numc2,
        lv_field           TYPE char40,
        lv_action          TYPE text20,
        lv_status          TYPE text20,
        lv_approval_result TYPE zarn_cc_log-approval_result,
        lt_cc_log          TYPE STANDARD TABLE OF zarn_cc_log.

  FIELD-SYMBOLS: <lv_field>  TYPE zarn_approval_column,
                 <ls_cc_hdr> LIKE LINE OF gt_cc_hdr.

* If we got to here we can reset
  PERFORM approvals_update2 USING pv_called_from pv_change_category pv_team_code zif_arn_approval_status=>gc_arn_appr_status_reset.

ENDFORM.                    "approvals_reset


*&---------------------------------------------------------------------*
*&      Form  APPR_APPROVE
*&---------------------------------------------------------------------*
FORM appr_approve.

  gv_appr_mode = co_appr_mode_approve.
  CLEAR: gv_appr_comment,
         gv_appr_rejection_reason.

  CALL SCREEN 0703 STARTING AT 5 5.

ENDFORM.                    "appr_approve

*&---------------------------------------------------------------------*
*&      Form  APPR_REJECT
*&---------------------------------------------------------------------*
FORM appr_reject.

  gv_appr_mode = co_appr_mode_reject.
  CLEAR: gv_appr_comment,
         gv_appr_rejection_reason.

  CALL SCREEN 0703 STARTING AT 5 5.

ENDFORM.                    "appr_reject

*&---------------------------------------------------------------------*
*&      Form  APPR_ADD_TEXT
*&---------------------------------------------------------------------*
FORM appr_add_text.

  gv_appr_mode = co_appr_mode_add_text.

  CALL SCREEN 0703 STARTING AT 5 5.

ENDFORM.                    "appr_add_text

*&---------------------------------------------------------------------*
*&      Form  appr_display_text
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
FORM appr_display_text.

  gv_appr_mode = co_appr_mode_display_text.

  CALL SCREEN 0703 STARTING AT 5 5.

ENDFORM.                    "appr_display_text
*&---------------------------------------------------------------------*
*&      Form  APPR_SET_STATUS
*&---------------------------------------------------------------------*
FORM appr_set_status .

  CLEAR: gv_appr_result.

  IF gv_appr_mode EQ co_appr_mode_display_text.
    SET PF-STATUS 'STATUS_0703_DISP'.
  ELSE.
    SET PF-STATUS 'STATUS_0703'.
  ENDIF.

  IF gv_appr_mode EQ co_appr_mode_approve.
    SET TITLEBAR '703A'.
    LOOP AT SCREEN.
      IF screen-name CS 'REJECTION_REASON'.
        screen-input = 0.
        screen-output = 0.
        screen-invisible = 1.
        MODIFY SCREEN.
      ENDIF.
    ENDLOOP.
  ELSEIF gv_appr_mode EQ co_appr_mode_reject.
    SET TITLEBAR '703R'.
  ELSEIF gv_appr_mode EQ co_appr_mode_add_text.
    SET TITLEBAR '703T'.
    LOOP AT SCREEN.
      IF screen-name CS 'REJECTION_REASON'.
        screen-input = 0.
        screen-output = 0.
        screen-invisible = 1.
        MODIFY SCREEN.
      ENDIF.
    ENDLOOP.
  ELSEIF gv_appr_mode EQ co_appr_mode_display_text.
    SET TITLEBAR '703D'.
    LOOP AT SCREEN.
      IF screen-name CS 'REJECTION_REASON'.
        IF gv_appr_rejection_reason IS INITIAL.
          screen-input = 0.
          screen-output = 0.
          screen-invisible = 1.
          MODIFY SCREEN.
        ELSE.
          screen-input = 0.
          MODIFY SCREEN.
        ENDIF.
      ENDIF.
    ENDLOOP.
  ELSE.
    LEAVE.
  ENDIF.

ENDFORM.                    " APPR_SET_STATUS

*&---------------------------------------------------------------------*
*&      Form  APPR_CREATE_TEXT_EDIT
*&---------------------------------------------------------------------*
FORM appr_create_text_edit.

  IF go_appr_editor_container IS INITIAL.
    CREATE OBJECT go_appr_editor_container
      EXPORTING
        container_name              = 'APPR_COMMENTS_CTRL'
      EXCEPTIONS
        cntl_error                  = 1
        cntl_system_error           = 2
        create_error                = 3
        lifetime_error              = 4
        lifetime_dynpro_dynpro_link = 5
        OTHERS                      = 6.
  ENDIF.
  IF NOT go_appr_editor_container IS INITIAL.
    CREATE OBJECT go_appr_text_editor
      EXPORTING
        max_number_chars           = 4000
*       style                      = 0
        wordwrap_mode              = cl_gui_textedit=>wordwrap_at_windowborder
        wordwrap_to_linebreak_mode = cl_gui_textedit=>false
        parent                     = go_appr_editor_container
*       lifetime                   =
*       name                       =
      EXCEPTIONS
        error_cntl_create          = 1
        error_cntl_init            = 2
        error_cntl_link            = 3
        error_dp_create            = 4
        gui_type_not_supported     = 5
        OTHERS                     = 6.
    IF NOT go_appr_text_editor IS INITIAL.
      CALL METHOD go_appr_text_editor->set_toolbar_mode
        EXPORTING
          toolbar_mode           = cl_gui_textedit=>false
        EXCEPTIONS
          error_cntl_call_method = 1
          invalid_parameter      = 2
          OTHERS                 = 3.
      CALL METHOD go_appr_text_editor->set_statusbar_mode
        EXPORTING
          statusbar_mode         = cl_gui_textedit=>false
        EXCEPTIONS
          error_cntl_call_method = 1
          invalid_parameter      = 2
          OTHERS                 = 3.
    ENDIF.
  ENDIF.

  IF NOT go_appr_text_editor IS INITIAL.
    CALL METHOD go_appr_text_editor->set_textstream
      EXPORTING
        text                   = gv_appr_comment
      EXCEPTIONS
        error_cntl_call_method = 1
        not_supported_by_gui   = 2
        OTHERS                 = 3.
    CALL METHOD cl_gui_textedit=>set_focus
      EXPORTING
        control           = go_appr_text_editor
      EXCEPTIONS
        cntl_error        = 1
        cntl_system_error = 2
        OTHERS            = 3.
    IF gv_appr_mode EQ co_appr_mode_display_text.
      CALL METHOD go_appr_text_editor->set_readonly_mode
        EXPORTING
          readonly_mode          = cl_gui_textedit=>true
        EXCEPTIONS
          error_cntl_call_method = 1
          invalid_parameter      = 2
          OTHERS                 = 3.
    ENDIF.
  ENDIF.

ENDFORM.                    " APPR_CREATE_TEXT_EDIT

*&---------------------------------------------------------------------*
*&      Form  APPR_USER_COMMAND_POPUP
*&---------------------------------------------------------------------*
FORM appr_user_command_popup.

  DATA: lv_ucomm TYPE sy-ucomm.

  CLEAR: gv_appr_comment.
  CALL METHOD go_appr_text_editor->get_textstream
    EXPORTING
      only_when_modified     = cl_gui_textedit=>false
    IMPORTING
      text                   = gv_appr_comment
*     is_modified            =
    EXCEPTIONS
      error_cntl_call_method = 1
      not_supported_by_gui   = 2
      OTHERS                 = 3.
  CALL METHOD cl_gui_cfw=>flush
    EXCEPTIONS
      cntl_system_error = 1
      cntl_error        = 2
      OTHERS            = 3.

  lv_ucomm = sy-ucomm.
  CLEAR sy-ucomm.
  CASE lv_ucomm.
    WHEN 'APPROVE_CA'.
      gv_appr_result = co_appr_result_cancelled.
      LEAVE TO SCREEN 0.
    WHEN 'APPROVE_OK'.
      IF gv_appr_mode EQ co_appr_mode_reject.
        IF gv_appr_rejection_reason IS INITIAL.
          MESSAGE i000 WITH 'Rejection Reason is mandatory'(arm).
          RETURN.
        ENDIF.
      ENDIF.
      IF gv_appr_mode EQ co_appr_mode_approve.
        gv_appr_result = co_appr_result_approved.
      ELSEIF gv_appr_mode EQ co_appr_mode_reject.
        gv_appr_result = co_appr_result_rejected.
      ELSEIF gv_appr_mode EQ co_appr_mode_add_text.
        gv_appr_result = co_appr_result_text_added.
      ENDIF.
      LEAVE TO SCREEN 0.
    WHEN 'BACK'.
      LEAVE.
    WHEN 'EXIT'.
      LEAVE.
    WHEN 'CANC'.
      LEAVE.
  ENDCASE.

ENDFORM.                    " APPR_USER_COMMAND_POPUP

*&---------------------------------------------------------------------*
*&      Form  APPR_POPULATE_DROP_DOWN
*&---------------------------------------------------------------------*
FORM appr_populate_drop_down.

  STATICS: lt_rej_reason TYPE STANDARD TABLE OF zarn_rej_reason.

  DATA: lt_values TYPE vrm_values,
        ls_value  LIKE LINE OF lt_values.

  FIELD-SYMBOLS: <ls_rej_reason> LIKE LINE OF lt_rej_reason.

  IF lt_rej_reason[] IS INITIAL.
    SELECT *
      INTO TABLE lt_rej_reason
      FROM zarn_rej_reason.                             "#EC CI_NOWHERE
  ENDIF.

  IF gv_appr_mode EQ co_appr_mode_reject.

    REFRESH lt_values.
    LOOP AT lt_rej_reason ASSIGNING <ls_rej_reason>.
      CLEAR ls_value.
      ls_value-key  = <ls_rej_reason>-rejection_reason.
      ls_value-text = <ls_rej_reason>-rejection_reason_desc.
      APPEND ls_value TO lt_values.
    ENDLOOP.

    CALL FUNCTION 'VRM_SET_VALUES'
      EXPORTING
        id              = 'GV_APPR_REJECTION_REASON'
        values          = lt_values
      EXCEPTIONS
        id_illegal_name = 1.
  ELSEIF gv_appr_mode EQ co_appr_mode_display_text.

    REFRESH lt_values.
    LOOP AT lt_rej_reason ASSIGNING <ls_rej_reason> WHERE rejection_reason EQ gv_appr_rejection_reason.
      CLEAR ls_value.
      ls_value-key  = <ls_rej_reason>-rejection_reason.
      ls_value-text = <ls_rej_reason>-rejection_reason_desc.
      APPEND ls_value TO lt_values.
    ENDLOOP.

    CALL FUNCTION 'VRM_SET_VALUES'
      EXPORTING
        id              = 'GV_APPR_REJECTION_REASON'
        values          = lt_values
      EXCEPTIONS
        id_illegal_name = 1.
  ENDIF.

ENDFORM.                    " APPR_POPULATE_DROP_DOWN


*&---------------------------------------------------------------------*
*&      Form  APPR_HANDLE_HOTSPOT_CLICK
*&---------------------------------------------------------------------*
*       Hotspot Click on ALV rows
*----------------------------------------------------------------------*
FORM appr_handle_hotspot_click USING fu_s_row_id    TYPE lvc_s_row
                                     fu_s_column_id TYPE lvc_s_col
                                     fu_s_row_no    TYPE lvc_s_roid
                                     pv_called_from TYPE char3.

  DATA: lv_change_category TYPE zarn_chg_category,
        lt_appr_approvals  TYPE STANDARD TABLE OF zarn_approvals_fields,
        lv_status          TYPE zarn_approval_status,
        lv_fieldname       TYPE char40,
        lv_col_number      TYPE numc2.

  FIELD-SYMBOLS: <ls_column> TYPE zarn_approval_column.

  FIELD-SYMBOLS: <ls_approval_team> LIKE LINE OF gt_approval_teams,
                 <ls_appr_approval> LIKE LINE OF lt_appr_approvals.

  REFRESH lt_appr_approvals.
  IF pv_called_from EQ co_appr_called_from_nat.
    lt_appr_approvals[] = gt_appr_nat_approvals[].
  ELSEIF pv_called_from EQ co_appr_called_from_reg.
    lt_appr_approvals[] = gt_appr_reg_approvals[].
  ENDIF.

  IF fu_s_column_id-fieldname EQ 'CHG_CATDESC'.
    READ TABLE lt_appr_approvals ASSIGNING <ls_appr_approval> INDEX fu_s_row_id-index.
    IF sy-subrc EQ 0.
      PERFORM appr_highlight_changed_fields2 USING pv_called_from <ls_appr_approval>-chg_cat.
    ENDIF.
  ELSEIF fu_s_column_id-fieldname+4(2) CO '0123456789'.
    lv_col_number = fu_s_column_id-fieldname+4(2).
    READ TABLE lt_appr_approvals ASSIGNING <ls_appr_approval> INDEX fu_s_row_id-index.
    IF sy-subrc EQ 0.
      UNASSIGN <ls_column>.
      CONCATENATE
        '<ls_appr_approval>-'
        fu_s_column_id-fieldname
        INTO lv_fieldname.
      ASSIGN (lv_fieldname) TO <ls_column>.
      IF <ls_column> IS ASSIGNED.
        IF NOT <ls_column> IS INITIAL.
          READ TABLE gt_approval_teams ASSIGNING <ls_approval_team> WITH KEY display_order = lv_col_number.
          IF sy-subrc EQ 0.
            lv_change_category = <ls_appr_approval>-chg_cat.
            READ TABLE lt_appr_approvals ASSIGNING <ls_appr_approval> INDEX fu_s_row_no-row_id.
            IF sy-subrc EQ 0.
              IF fu_s_column_id-fieldname(4) EQ co_appr_column_approve.
                IF <ls_column> EQ icon_led_green.
                  PERFORM approvals_reset USING pv_called_from <ls_appr_approval> lv_col_number lv_change_category <ls_approval_team>-team_code zif_arn_approval_status=>gc_arn_appr_status_approved.
                ELSEIF <ls_column> EQ icon_led_red.
                  PERFORM approvals_reset USING pv_called_from <ls_appr_approval> lv_col_number lv_change_category <ls_approval_team>-team_code zif_arn_approval_status=>gc_arn_appr_status_rejected.
                ELSE.
                  PERFORM approvals_update2 USING pv_called_from lv_change_category <ls_approval_team>-team_code zif_arn_approval_status=>gc_arn_appr_status_approved.
                ENDIF.
              ELSEIF fu_s_column_id-fieldname(4) EQ co_appr_column_reject.
                PERFORM approvals_update2 USING pv_called_from lv_change_category <ls_approval_team>-team_code zif_arn_approval_status=>gc_arn_appr_status_rejected.
              ELSEIF fu_s_column_id-fieldname(4) EQ co_appr_column_status.
                IF <ls_column> EQ icon_led_green.
                  PERFORM approvals_reset USING pv_called_from <ls_appr_approval> lv_col_number lv_change_category <ls_approval_team>-team_code zif_arn_approval_status=>gc_arn_appr_status_approved.
                ELSEIF <ls_column> EQ icon_led_red.
                  PERFORM approvals_reset USING pv_called_from <ls_appr_approval> lv_col_number lv_change_category <ls_approval_team>-team_code zif_arn_approval_status=>gc_arn_appr_status_rejected.
                ENDIF.
              ELSEIF fu_s_column_id-fieldname(4) EQ co_appr_column_text.
                IF <ls_column> EQ icon_display_text.
                  PERFORM approvals_display_text2 USING pv_called_from lv_change_category <ls_approval_team>-team_code.
                ELSE.
                  PERFORM approvals_add_text2 USING pv_called_from lv_change_category <ls_approval_team>-team_code CHANGING <ls_column>.
                ENDIF.
                IF pv_called_from EQ co_appr_called_from_nat.
                  gt_appr_nat_approvals[] = lt_appr_approvals[].
                  IF NOT go_appr_nat_grid IS INITIAL.
                    CALL METHOD go_appr_nat_grid->refresh_table_display
                      EXCEPTIONS
                        finished = 1
                        OTHERS   = 2.
                  ENDIF.
                ELSEIF pv_called_from EQ co_appr_called_from_reg.
                  gt_appr_reg_approvals[] = lt_appr_approvals[].
                  IF NOT go_appr_reg_grid IS INITIAL.
                    CALL METHOD go_appr_reg_grid->refresh_table_display
                      EXCEPTIONS
                        finished = 1
                        OTHERS   = 2.
                  ENDIF.
                ENDIF.
              ENDIF.
            ENDIF.
          ENDIF.
        ENDIF.
      ENDIF.
    ENDIF.
  ENDIF.

ENDFORM.                    "appr_handle_hotspot_click

*&---------------------------------------------------------------------*
*&      Form  get_process_type
*&---------------------------------------------------------------------*
FORM get_process_type USING pv_idno CHANGING pv_process_type TYPE zarn_process_type.

  DATA: ls_ver_status TYPE zarn_ver_status.

* Is new if we do not have a SAP article
  pv_process_type = 'NEW'.
  CLEAR ls_ver_status.
  SELECT SINGLE article_ver
    INTO CORRESPONDING FIELDS OF ls_ver_status
    FROM zarn_ver_status
    WHERE idno EQ pv_idno.
  IF NOT ls_ver_status-article_ver IS INITIAL.
    pv_process_type = 'CHG'.
  ENDIF.

ENDFORM.                    "get_process_type

*&---------------------------------------------------------------------*
*&      Form  APPR_HIGHLIGHT_LOGIC
*&---------------------------------------------------------------------*
FORM appr_highlight_logic.

  EXIT.

*  DATA: lt_cc_det TYPE HASHED TABLE OF zarn_cc_det WITH UNIQUE KEY chg_table chg_field,
*        lv_table  TYPE zarn_cc_det-chg_table,
*        lv_field  TYPE zarn_cc_det-chg_field.
*
*  RANGES: lr_status FOR zarn_cc_hdr-cm_status.
*
*  FIELD-SYMBOLS: <ls_cc_hdr> LIKE LINE OF gt_cc_hdr,
*                 <ls_cc_det> LIKE LINE OF gt_cc_det.
*
*  CHECK NOT gv_appr_chg_cat IS INITIAL.
*
*  CLEAR lr_status.
*  lr_status-sign   = 'I'.
*  lr_status-option = 'EQ'.
*  lr_status-low    = space.
*  APPEND lr_status.
*  lr_status-low    = zif_arn_approval_status=>gc_arn_appr_cc_status_pending.
*  APPEND lr_status.
*
*  REFRESH lt_cc_det.
** Get all the changes which are pending approval for the change category
*  LOOP AT gt_cc_hdr ASSIGNING <ls_cc_hdr>
*    WHERE idno         EQ gs_worklist-idno
*    AND   chg_category EQ gv_appr_chg_cat
*    AND   ( cm_status         IN lr_status
*    OR      sc_buyer_status   IN lr_status
*    OR      sc_stkcntl_status IN lr_status
*    OR      amd_spl_status    IN lr_status
*    OR      amd_pr_status     IN lr_status
*    OR      amd_mgr_status    IN lr_status
*    OR      apr7_status       IN lr_status
*    OR      apr8_status       IN lr_status
*    OR      apr9_status       IN lr_status
*    OR      apr10_status      IN lr_status ).
*    READ TABLE gt_cc_det TRANSPORTING NO FIELDS WITH TABLE KEY chgid = <ls_cc_hdr>-chgid idno = <ls_cc_hdr>-idno.
*    IF sy-subrc NE 0.
*      SELECT *
*        APPENDING TABLE gt_cc_det
*        FROM zarn_cc_det
*        WHERE chgid        EQ <ls_cc_hdr>-chgid
*        AND   idno         EQ <ls_cc_hdr>-idno.
*    ENDIF.
*    LOOP AT gt_cc_det ASSIGNING <ls_cc_det> WHERE chgid EQ <ls_cc_hdr>-chgid AND idno EQ <ls_cc_hdr>-idno.
*      IF <ls_cc_det>-chg_category EQ <ls_cc_hdr>-chg_category.
*        INSERT <ls_cc_det> INTO TABLE lt_cc_det.
*      ENDIF.
*    ENDLOOP.
*  ENDLOOP.
*
*  LOOP AT SCREEN.
*    CHECK screen-name CS '-'.
*    SPLIT screen-name AT '-' INTO lv_table lv_field.
*    READ TABLE lt_cc_det TRANSPORTING NO FIELDS WITH TABLE KEY chg_table = lv_table chg_field = lv_field.
*    IF sy-subrc EQ 0.
*      screen-intensified = 1.
*    ELSE.
*      screen-intensified = 0.
*    ENDIF.
*    MODIFY SCREEN.
*  ENDLOOP.

ENDFORM.                    " APPR_HIGHLIGHT_LOGIC

*&---------------------------------------------------------------------*
*&      Form  update_nat_status
*&---------------------------------------------------------------------*
FORM update_nat_status USING pv_idno TYPE zarn_idno pv_version TYPE zarn_version pv_status TYPE char3.

  DATA: ls_prd_version LIKE LINE OF gt_prd_version.

  FIELD-SYMBOLS: <ls_prd_version> LIKE LINE OF gt_prd_version.

  READ TABLE gt_prd_version ASSIGNING <ls_prd_version> WITH KEY idno = pv_idno version = pv_version.
  IF sy-subrc EQ 0.
    IF <ls_prd_version>-version_status NE pv_status.

      ls_prd_version = <ls_prd_version>.
      <ls_prd_version>-version_status = pv_status.

      PERFORM update_nat_db_status USING ls_prd_version <ls_prd_version>.

      gs_worklist-version_status      = <ls_prd_version>-version_status.
      zarn_prd_version-version_status = <ls_prd_version>-version_status.
    ENDIF.
  ENDIF.

ENDFORM.                    "update_nat_status

*&---------------------------------------------------------------------*
*&      Form  update_reg_status
*&---------------------------------------------------------------------*
FORM update_reg_status USING pv_idno TYPE zarn_idno pv_status TYPE char3.

  DATA: ls_reg_hdr TYPE zarn_reg_hdr.

  FIELD-SYMBOLS: <ls_reg_data> LIKE LINE OF gt_reg_data,
                 <ls_reg_hdr>  LIKE LINE OF <ls_reg_data>-zarn_reg_hdr.

  READ TABLE gt_reg_data ASSIGNING <ls_reg_data> WITH KEY idno = pv_idno.
  IF sy-subrc EQ 0.
    READ TABLE <ls_reg_data>-zarn_reg_hdr ASSIGNING <ls_reg_hdr> WITH KEY idno = pv_idno.
    IF sy-subrc EQ 0.
      IF <ls_reg_hdr>-status NE pv_status.
        ls_reg_hdr = <ls_reg_hdr>.
        <ls_reg_hdr>-status = pv_status.

        PERFORM update_reg_db_status USING ls_reg_hdr <ls_reg_hdr>.

        zarn_reg_hdr-status = pv_status.
      ENDIF.
    ENDIF.
  ENDIF.

ENDFORM.                    "update_reg_status
*&---------------------------------------------------------------------*
*&      Form  ALV_HSNO
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM alv_hsno .

  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo.

  IF go_nat_hsno_alv IS INITIAL.
* Nutritional Information
    CREATE OBJECT go_nncc11
      EXPORTING
        container_name = gc_nncc11.

* Create ALV grid
    CREATE OBJECT go_nat_hsno_alv
      EXPORTING
        i_parent = go_nncc11.

*   Call GRID
    PERFORM alv_build_fieldcat USING gc_alv_nat_hsno CHANGING lt_fieldcat.
**  Exclude all edit functions in this example since we do not need them:
    PERFORM exclude_tb_functions_display CHANGING lt_exclude.

*    ls_layout-cwidth_opt = abap_true.
    ls_layout-zebra      = abap_true.
    ls_layout-no_toolbar = abap_true.

    CALL METHOD go_nat_hsno_alv->set_table_for_first_display
      EXPORTING
        is_layout                     = ls_layout
        it_toolbar_excluding          = lt_exclude
      CHANGING
        it_outtab                     = gt_zarn_hsno
        it_fieldcatalog               = lt_fieldcat
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.

    IF sy-subrc IS NOT INITIAL.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

  ELSE.

    CALL METHOD go_nat_hsno_alv->refresh_table_display.
  ENDIF.

ENDFORM.



"***********************NEW LOGIC - 3162 APPROVAL STABILISATION**************


*&---------------------------------------------------------------------*
*&      Form  APPROVALS_BUILD_GRID
*&---------------------------------------------------------------------*
FORM approvals_build_grid2 USING pv_called_from TYPE char3.


  DATA: lt_approvals      TYPE zarn_t_approval,
        lt_appr_output    TYPE ty_t_appr_output,
        lv_approval_area  TYPE zarn_e_appr_area,
        lt_fieldcat       TYPE lvc_t_fcat,
        lv_input_disabled TYPE xfeld,
        lv_exit           TYPE xfeld.

  IF go_approval IS NOT BOUND.
    go_approval = NEW #( iv_idno  = gs_worklist-idno
                         iv_load_db_approvals = abap_true
                         iv_approval_event = zcl_arn_approval_backend=>gc_event_ui ).
  ENDIF.
  go_approval->load_recent_approval_data( ).

  "---initialize and determine editable / non editable
  PERFORM appr_initialize USING pv_called_from
                       CHANGING lv_exit
                                lv_input_disabled.

  IF lv_exit = abap_true.
    EXIT.
  ENDIF.

  "---create ALV grid
  CASE pv_called_from.
    WHEN co_appr_called_from_nat.
      PERFORM appr_create_alv USING 'APPR_NAT_GRID_CTRL'
                           CHANGING go_appr_nat_grid_container
                                    go_appr_nat_grid.
      IF go_appr_nat_event_handler IS INITIAL.
        CREATE OBJECT go_appr_nat_event_handler.
      ENDIF.

      "Set event handlers - Hotspots in row
      SET HANDLER go_appr_nat_event_handler->appr_nat_handle_hotspot_click FOR go_appr_nat_grid.

    WHEN co_appr_called_from_reg.
      PERFORM appr_create_alv USING 'APPR_REG_GRID_CTRL'
                      CHANGING go_appr_reg_grid_container
                               go_appr_reg_grid.
      IF go_appr_reg_event_handler IS INITIAL.
        CREATE OBJECT go_appr_reg_event_handler.
      ENDIF.
      SET HANDLER go_appr_reg_event_handler->appr_reg_handle_hotspot_click FOR go_appr_reg_grid.
  ENDCASE.


  gt_approval_teams = go_approval->get_conf_teams( iv_display_only = abap_true ).

  lv_approval_area = pv_called_from.

  go_approval->set_approval_area( lv_approval_area  ).

  lt_approvals[] = go_approval->get_approvals( ).

  "do not display posted approvals
  DELETE lt_approvals WHERE status = zcl_arn_approval_backend=>gc_stat-posted.


  "---dynamically build fieldcatalog of the Approval
  PERFORM appr_build_fieldcat2 USING pv_called_from
                                     lv_input_disabled
                                     lt_approvals
                           CHANGING  lt_fieldcat.

*
  "---build the output table (to be displayed in ALV)
  PERFORM appr_build_output2 USING pv_called_from
                                   lt_approvals
                          CHANGING lt_appr_output.



  "---update header statuses accordingly in global tables
  PERFORM appr_update_hdr_status USING pv_called_from.

  "--disable approve/reject all buttons if nothing to be done
  PERFORM appr_disable_buttons USING pv_called_from.

  "---adjust colours in ALV cells
  PERFORM appr_adjust_colours CHANGING lt_appr_output.


  SORT lt_appr_output BY sort_order chg_cat chg_catdesc.

  "---finalize and display ALV
  CASE pv_called_from.

    WHEN co_appr_called_from_nat.

      PERFORM appr_finalize_and_display USING lt_appr_output
                                              go_appr_nat_grid
                                              gt_appr_nat_fieldcat
                                     CHANGING gt_appr_nat_approvals.

    WHEN co_appr_called_from_reg.

      PERFORM appr_finalize_and_display USING lt_appr_output
                                              go_appr_reg_grid
                                              gt_appr_reg_fieldcat
                                     CHANGING gt_appr_reg_approvals.

  ENDCASE.

ENDFORM.                    " APPROVALS_BUILD_GRID

FORM appr_build_fieldcat2 USING pv_called_from         TYPE char3
                                piv_input_disabled     TYPE xfeld
                                pit_approvals          TYPE zarn_t_approval
                       CHANGING pct_fieldcat TYPE lvc_t_fcat.

  CONSTANTS: co_no_team TYPE char3 VALUE 'N/A'.

  CALL FUNCTION 'LVC_FIELDCATALOG_MERGE'
    EXPORTING
*     I_BUFFER_ACTIVE        = I_BUFFER_ACTIVE
      i_structure_name       = 'ZARN_APPROVALS_FIELDS'
*     I_CLIENT_NEVER_DISPLAY = 'X'
*     I_BYPASSING_BUFFER     = I_BYPASSING_BUFFER
*     I_INTERNAL_TABNAME     = I_INTERNAL_TABNAME
    CHANGING
      ct_fieldcat            = pct_fieldcat
    EXCEPTIONS
      inconsistent_interface = 1
      program_error          = 2.

  LOOP AT pct_fieldcat ASSIGNING FIELD-SYMBOL(<ls_fieldcat>).
    CASE <ls_fieldcat>-fieldname.
      WHEN 'CHG_CAT'.
        <ls_fieldcat>-outputlen = 8.
        <ls_fieldcat>-scrtext_s = TEXT-chc.
        <ls_fieldcat>-scrtext_m = TEXT-chc.
        <ls_fieldcat>-scrtext_l = TEXT-chc.
        <ls_fieldcat>-coltext   = TEXT-chc.
        <ls_fieldcat>-reptext   = TEXT-chc.
      WHEN 'CHG_CATDESC'.
        <ls_fieldcat>-outputlen = 20.
        <ls_fieldcat>-hotspot = abap_true.
      WHEN 'DATA_LEVEL'.
        <ls_fieldcat>-no_out = abap_true.
      WHEN 'SORT_ORDER'.
        <ls_fieldcat>-no_out = abap_true.
      WHEN OTHERS.
        IF <ls_fieldcat>-fieldname+4(2) CO '0123456789'.
          READ TABLE gt_approval_teams ASSIGNING FIELD-SYMBOL(<ls_approval_team>)
           WITH KEY display_order = <ls_fieldcat>-fieldname+4(2).
          IF sy-subrc EQ 0.
            <ls_fieldcat>-icon      = abap_true.
            <ls_fieldcat>-scrtext_s = <ls_approval_team>-team_code.
            <ls_fieldcat>-scrtext_m = <ls_approval_team>-team_code.
            <ls_fieldcat>-scrtext_l = <ls_approval_team>-team_code.
            <ls_fieldcat>-coltext   = <ls_approval_team>-team_code.
            <ls_fieldcat>-reptext   = <ls_approval_team>-team_code.
            IF <ls_fieldcat>-fieldname(4) EQ co_appr_column_status.
              <ls_fieldcat>-outputlen = 6.
              <ls_fieldcat>-tooltip   = <ls_approval_team>-team_name.
            ELSEIF <ls_fieldcat>-fieldname(4) EQ co_appr_column_approve.
              <ls_fieldcat>-outputlen = 3.
              CONCATENATE
                <ls_approval_team>-team_name
                '- Approve'(ata)
                INTO <ls_fieldcat>-tooltip SEPARATED BY space.
            ELSEIF <ls_fieldcat>-fieldname(4) EQ co_appr_column_reject.
              <ls_fieldcat>-outputlen = 3.
              CONCATENATE
                <ls_approval_team>-team_name
                '- Reject'(atr)
                INTO <ls_fieldcat>-tooltip SEPARATED BY space.
            ELSEIF <ls_fieldcat>-fieldname(4) EQ co_appr_column_text.
              <ls_fieldcat>-outputlen = 3.
              CONCATENATE
                <ls_approval_team>-team_name
                '- Comment'(atc)
                INTO <ls_fieldcat>-tooltip SEPARATED BY space.
            ENDIF.

            <ls_fieldcat>-no_out    = abap_false.

            IF line_exists( pit_approvals[ team_code = <ls_approval_team>-team_code
                                           status = zcl_arn_approval_backend=>gc_stat-appr_ready ] ).

*             we have an approval waiting so need appr/reje/text columns
              IF <ls_fieldcat>-fieldname(4) EQ co_appr_column_status.
                <ls_fieldcat>-no_out    = abap_true.
              ELSEIF <ls_fieldcat>-fieldname(4) EQ co_appr_column_approve
              OR     <ls_fieldcat>-fieldname(4) EQ co_appr_column_reject
              OR     <ls_fieldcat>-fieldname(4) EQ co_appr_column_text.
                <ls_fieldcat>-hotspot = abap_true.
                <ls_fieldcat>-edit    = abap_true.
              ENDIF.

            ELSE.
*             we do not have an approval waiting so just need stat column
              IF <ls_fieldcat>-fieldname(4) EQ co_appr_column_status.
*               need to be able to undo approve/reject
                <ls_fieldcat>-hotspot = abap_true.
              ENDIF.

              IF <ls_fieldcat>-fieldname(4) EQ co_appr_column_approve
              OR <ls_fieldcat>-fieldname(4) EQ co_appr_column_reject.
                <ls_fieldcat>-no_out    = abap_true.
              ENDIF.

*             do we need the text column?
              IF <ls_fieldcat>-fieldname(4) EQ co_appr_column_text.

                "tbd this might be different if we want to allow "comments" at any time
                LOOP AT pit_approvals TRANSPORTING NO FIELDS
                                      WHERE team_code = <ls_approval_team>-team_code
                                        AND status_comment IS NOT INITIAL.
                  EXIT.
                ENDLOOP.
                IF sy-subrc NE 0.
                  <ls_fieldcat>-no_out = abap_true.
                ELSE.
                  <ls_fieldcat>-hotspot = abap_true.
                ENDIF.
              ENDIF.
            ENDIF.
          ELSE.
            <ls_fieldcat>-no_out    = abap_true.
            <ls_fieldcat>-scrtext_s = co_no_team.
            <ls_fieldcat>-scrtext_m = co_no_team.
            <ls_fieldcat>-scrtext_l = co_no_team.
            <ls_fieldcat>-coltext   = co_no_team.
            <ls_fieldcat>-reptext   = co_no_team.
            <ls_fieldcat>-tooltip   = co_no_team.
            <ls_fieldcat>-outputlen = 3.
          ENDIF.
        ENDIF.
        IF pv_called_from EQ co_appr_called_from_reg.
          IF gv_appr_nat_is_approved EQ abap_false.
            <ls_fieldcat>-edit    = abap_false.
            <ls_fieldcat>-hotspot = abap_false.
          ENDIF.
        ENDIF.
        IF piv_input_disabled = abap_true.
          <ls_fieldcat>-edit    = abap_false.
          IF <ls_fieldcat>-fieldname(4) NE co_appr_column_text.
            "enable to display text even in Display mode
            <ls_fieldcat>-hotspot = abap_false.
          ENDIF.
        ENDIF.
    ENDCASE.
  ENDLOOP.
  DELETE pct_fieldcat WHERE reptext EQ co_no_team.

  IF pv_called_from EQ co_appr_called_from_nat.
    REFRESH gt_appr_nat_fieldcat.
    APPEND LINES OF pct_fieldcat TO gt_appr_nat_fieldcat.
  ENDIF.
  IF pv_called_from EQ co_appr_called_from_reg.
    REFRESH gt_appr_reg_fieldcat.
    APPEND LINES OF pct_fieldcat TO gt_appr_reg_fieldcat.
  ENDIF.

ENDFORM.


FORM appr_build_output2 USING pv_called_from  TYPE char3
                             pit_approvals    TYPE zarn_t_approval
                    CHANGING pct_appr_output  TYPE ty_t_appr_output.

  DATA       lv_fieldname          TYPE char40.

  FIELD-SYMBOLS: <ls_col_appr> TYPE zarn_approval_column,
                 <ls_col_stat> TYPE zarn_approval_column,
                 <ls_col_reje> TYPE zarn_approval_column,
                 <ls_col_text> TYPE zarn_approval_column.

* build the table
  REFRESH pct_appr_output.

  LOOP AT pit_approvals ASSIGNING FIELD-SYMBOL(<ls_approval>).

    READ TABLE pct_appr_output ASSIGNING FIELD-SYMBOL(<ls_appr_output>)
     WITH KEY chg_cat = <ls_approval>-chg_category.

    IF sy-subrc NE 0.
      APPEND INITIAL LINE TO pct_appr_output ASSIGNING <ls_appr_output>.
      <ls_appr_output>-data_level = pv_called_from .
      <ls_appr_output>-sort_order = 'B'.
      <ls_appr_output>-chg_cat    = <ls_approval>-chg_category.
      CALL FUNCTION 'Z_ARN_APPROVALS_GET_CHGCAT_DES'
        EXPORTING
          chg_category = <ls_appr_output>-chg_cat
        IMPORTING
          chg_cat_desc = <ls_appr_output>-chg_catdesc.
    ENDIF.
    CHECK <ls_appr_output> IS ASSIGNED.


    READ TABLE gt_approval_teams ASSIGNING FIELD-SYMBOL(<ls_approval_team>)
     WITH KEY team_code = <ls_approval>-team_code.
    IF sy-subrc EQ 0.
      CONCATENATE
        '<ls_appr_output>-'
        co_appr_column_approve
        <ls_approval_team>-display_order
        INTO lv_fieldname.
      ASSIGN (lv_fieldname) TO <ls_col_appr>.
      CONCATENATE
        '<ls_appr_output>-'
        co_appr_column_status
        <ls_approval_team>-display_order
        INTO lv_fieldname.
      ASSIGN (lv_fieldname) TO <ls_col_stat>.
      CONCATENATE
        '<ls_appr_output>-'
        co_appr_column_reject
        <ls_approval_team>-display_order
        INTO lv_fieldname.
      ASSIGN (lv_fieldname) TO <ls_col_reje>.
      CONCATENATE
        '<ls_appr_output>-'
        co_appr_column_text
        <ls_approval_team>-display_order
        INTO lv_fieldname.
      ASSIGN (lv_fieldname) TO <ls_col_text>.
    ENDIF.
    IF  <ls_col_appr> IS ASSIGNED
    AND <ls_col_stat> IS ASSIGNED
    AND <ls_col_reje> IS ASSIGNED
    AND <ls_col_text> IS ASSIGNED.

      CLEAR: <ls_col_appr>,
             <ls_col_stat>,
             <ls_col_reje>,
             <ls_col_text>.
      IF <ls_approval>-status_comment IS NOT INITIAL.
        <ls_col_text> = icon_display_text.
      ENDIF.

      CASE <ls_approval>-status.


        WHEN zcl_arn_approval_backend=>gc_stat-appr_ready.
          "check authorisations
          IF go_approval->is_authorised_for_team( <ls_approval>-team_code ).
            "yes this user is authorised for approvals

            <ls_appr_output>-sort_order = 'A'.
            <ls_col_appr> = icon_okay.
            <ls_col_reje> = icon_cancel.
            IF gv_display NE abap_true.
              IF <ls_col_text> EQ icon_display_text.
                <ls_col_text> = icon_change_text.
              ELSE.
                <ls_col_text> = icon_create_text.
              ENDIF.
            ENDIF.
            IF pv_called_from EQ co_appr_called_from_nat.
              gv_appr_nat_approvals = abap_true.
            ENDIF.
            IF pv_called_from EQ co_appr_called_from_reg.
              gv_appr_reg_approvals = abap_true.
            ENDIF.

          ELSE.
            <ls_col_appr> = icon_led_inactive.
            <ls_col_stat> = icon_led_inactive.
          ENDIF.

        WHEN zcl_arn_approval_backend=>gc_stat-appr_in_queue.
          <ls_col_appr> = icon_led_yellow.
          <ls_col_stat> = icon_led_yellow.

        WHEN  zcl_arn_approval_backend=>gc_stat-approved.
          <ls_col_appr> = icon_led_green.
          <ls_col_stat> = icon_led_green.

        WHEN zcl_arn_approval_backend=>gc_stat-rejected.
          <ls_col_appr> = icon_led_red.
          <ls_col_stat> = icon_led_red.
      ENDCASE.


    ENDIF.
  ENDLOOP.

ENDFORM.

FORM approvals_update2 USING pv_called_from     TYPE char3
                             pv_change_category TYPE zarn_chg_category
                             pv_team_code       TYPE zarn_team_code
                             pv_status          TYPE zarn_approval_status.

  DATA: lt_appr_approvals  TYPE ty_t_appr_output,
        lt_approval_teams  LIKE gt_approval_teams,
        lv_rej_comment     LIKE gv_appr_comment,
        lv_exit            TYPE xfeld,
        lv_approval_area   TYPE zarn_e_appr_area,
        lv_processing_team TYPE zarn_team_code.

  lv_approval_area = pv_called_from.

  go_approval->set_approval_area( lv_approval_area ).


  "---initial checks whether action is allowed
  PERFORM appr_action_check_before USING pv_called_from
                                         pv_status
                                CHANGING lv_exit.
  IF lv_exit = abap_true.
    EXIT.
  ENDIF.

  "---prepare data required to perform the action
  PERFORM appr_action_data_prepare2    USING pv_called_from
                                            pv_team_code
                                            pv_status
                                   CHANGING lv_processing_team
                                            lt_appr_approvals
                                            lt_approval_teams
                                            gv_appr_result
                                            lv_rej_comment
                                            lv_exit.

  IF lv_exit = abap_true.
    "action not relevant for further processing here
    RETURN.
  ENDIF.

  "---this will do all the determination logic
  "and change ZARN_APPROVAL
  PERFORM appr_action_post2 USING  pv_called_from
                                   lt_appr_approvals
                                   pv_change_category
                                   lt_approval_teams
                                   lv_rej_comment
                                   lv_processing_team
                          CHANGING lv_exit.

  IF lv_exit = abap_true.
    RETURN.
  ENDIF.



  "---update statuses on header (regional/national)
  "this is only for WIP / REJ status (work in progress)
  PERFORM appr_action_stat_update USING pv_called_from
                                        pv_status.


  COMMIT WORK AND WAIT.

  TRY.
      "action posted, continue with optimistic lock
      go_approval->enqueue_appr_idno_or_wait( zif_enq_mode_c=>optimistic ).

    CATCH zcx_excep_cls.
      MESSAGE a099.
  ENDTRY.

  "---do additional actions upon approvals
  IF pv_status EQ zif_arn_approval_status=>gc_arn_appr_status_approved.

    "this can trigger defaulting (if national fully approved)
    "or article posting (if everything is approved)
    PERFORM appr_after_approval2 USING pv_called_from.

  ENDIF.

  IF pv_called_from EQ co_appr_called_from_nat.
    REFRESH gt_appr_nat_approvals.
  ENDIF.
  IF pv_called_from EQ co_appr_called_from_reg.
    REFRESH gt_appr_reg_approvals.
  ENDIF.

**   refresh the grid
*  PERFORM approvals_build_grid USING pv_called_from.
*
  cl_gui_cfw=>set_new_ok_code( 'APPR_DUMMY' ).
*  cl_gui_cfw=>flush( ).



ENDFORM.

FORM appr_action_data_prepare2 USING pv_called_from  TYPE char3
                                     pv_team_code    TYPE zarn_team_code
                                     pv_status       TYPE zarn_approval_status
                           CHANGING pcv_proc_team    TYPE zarn_team_code
                                    pct_approvals    TYPE ty_t_appr_output
                                    pct_appr_teams   TYPE zarn_t_teams
                                    pcv_appr_result  TYPE c
                                    pcv_rej_comment  TYPE string
                                    pcv_exit         TYPE xfeld.

  DATA: lv_authorized  TYPE flag.

  CLEAR pcv_exit.

  REFRESH pct_approvals.
  IF pv_called_from EQ co_appr_called_from_nat.
    pct_approvals[] = gt_appr_nat_approvals.
  ELSEIF pv_called_from EQ co_appr_called_from_reg.
    pct_approvals[] = gt_appr_reg_approvals.
  ENDIF.

  "check which team is ready for approvals
  DATA(lv_team_ready) = go_approval->get_team_ready_for_approval( ).
  IF lv_team_ready IS INITIAL.
    "there is no team ready for this matrix.
    pcv_exit = abap_true.
    RETURN.
  ENDIF.

  IF  pv_team_code IS NOT INITIAL.
    "if clicking on particular cell then take
    "processing team from there
    pcv_proc_team = pv_team_code.
  ELSE.
    "PV_TEAM_CODE will be initial in case of pressing button APPROVE / REJECT ALL
    "if clicking on "approve/reject all"
    "take processing team as the team which is currently ready
    pcv_proc_team = lv_team_ready.
  ENDIF.



  CASE  pv_status.
    WHEN zif_arn_approval_status=>gc_arn_appr_status_approved
      OR zif_arn_approval_status=>gc_arn_appr_status_rejected.

      "in case of approving or rejecting only team which is currently ready
      "for approving can change something


      IF pv_team_code IS NOT INITIAL
        AND pv_team_code NE lv_team_ready.

        "trying to approve a team column which is not ready for approval yet
        pcv_exit = abap_true.
        RETURN.
      ENDIF.


    WHEN zif_arn_approval_status=>gc_arn_appr_status_reset.

      "in case of reset it is possible to reset
      "a) current team
      "b) previous team if current team hasn't started yet

      IF pv_team_code IS NOT INITIAL
        AND pv_team_code NE lv_team_ready.

        "if trying to reset something for different team than the current one
        "it is possible only for previous team and
        "if current team hasn't started working yet

        IF go_approval->has_team_started_approving( lv_team_ready ) = abap_true
          OR pv_team_code NE go_approval->get_previous_team( lv_team_ready ).

          "team already started approving, can't reset anything that doesn't belong
          "to this team
          pcv_exit = abap_true.
          RETURN.

        ENDIF.
      ENDIF.
  ENDCASE.

  IF go_approval->is_authorised_for_team( pcv_proc_team )
    NE abap_true.

    "trying to change something I am not authorised to
    pcv_exit = abap_true.
    RETURN.
  ENDIF.


  CLEAR pcv_appr_result.
  IF pv_status EQ zif_arn_approval_status=>gc_arn_appr_status_approved.
*   Do not want mandatory comment on approval
*   PERFORM appr_approve.
    pcv_appr_result = co_appr_result_approved.
  ELSEIF pv_status EQ zif_arn_approval_status=>gc_arn_appr_status_rejected.
    CLEAR gv_appr_comment.
    PERFORM appr_reject.
    pcv_rej_comment = gv_appr_comment.
  ELSEIF pv_status EQ zif_arn_approval_status=>gc_arn_appr_status_reset.
    CLEAR gv_appr_comment.
    pcv_appr_result = co_appr_result_reset.
  ENDIF.


  CASE pcv_appr_result.
    WHEN co_appr_result_approved OR co_appr_result_rejected
      OR co_appr_result_reset.
      "if one of those actions- OK, proceed


    WHEN OTHERS.
      "if other actions - don't do anything
      pcv_exit = abap_true.
      RETURN.
  ENDCASE.


ENDFORM.

FORM appr_action_post2 USING   pv_called_from     TYPE char3
                               pit_approvals      TYPE ty_t_appr_output
                               pv_change_category TYPE zarn_chg_category
                               pit_approval_teams TYPE zarn_t_teams
                               piv_rej_comment    TYPE string
                               piv_team           TYPE zarn_team_code
                      CHANGING pcv_exit           TYPE xfeld.

  DATA : lt_approval_new   TYPE zarn_t_approval,
         lt_appr_hist      TYPE zarn_t_appr_hist,
         lv_status_reason  TYPE zarn_e_appr_status_reason,
         lt_after_appr_act TYPE zarn_t_after_appr_action.

  "action on approval automatically determines
  "statuses on following approvals

  TRY.
      CASE gv_appr_result.

        WHEN co_appr_result_approved.

          go_approval->action_approve(
            EXPORTING
              iv_team_code    = piv_team
              iv_chg_category = pv_change_category
            IMPORTING
              et_zarn_approval  = lt_approval_new
              et_zarn_appr_hist = lt_appr_hist ).
          "et_after_appr_actions = lt_after_appr_act ).

        WHEN co_appr_result_rejected.

          lv_status_reason = gv_appr_rejection_reason.

          go_approval->action_reject(
           EXPORTING
             iv_team_code     = piv_team
             iv_chg_category  = pv_change_category
             iv_comment       = piv_rej_comment
             iv_status_reason = lv_status_reason
             iv_approval_area = pv_called_from
           IMPORTING
             et_zarn_approval  = lt_approval_new
             et_zarn_appr_hist = lt_appr_hist ).

        WHEN co_appr_result_reset.

          go_approval->action_reset(
            EXPORTING
              iv_team_code    = piv_team
              iv_chg_category = pv_change_category
            IMPORTING
              et_zarn_approval  = lt_approval_new
              et_zarn_appr_hist = lt_appr_hist ).

      ENDCASE.

* Notifcation exceptions. We're fine to continue after this. Just notify
    CATCH zcx_arn_notif_err INTO DATA(lo_ex).
      MESSAGE lo_ex TYPE 'I' DISPLAY LIKE 'W'.

    CATCH zcx_excep_cls.
      MESSAGE i099 DISPLAY LIKE 'E' .
      pcv_exit = abap_true.
  ENDTRY.

  go_approval->post_approvals( EXPORTING it_approval  = lt_approval_new
                                         it_appr_hist = lt_appr_hist ).

*** ONLD-1061 Field used by Online Approved?
**  zcl_onl_arena=>zarn_approval_process( EXPORTING it_approval_new = lt_approval_new it_approval_hist = lt_appr_hist ).


ENDFORM.

FORM appr_after_approval2 USING pv_called_from   TYPE char3.

  DATA : lv_nat_approved        TYPE flag,
         lv_reg_approved        TYPE flag,
         lv_carry_over_happened TYPE flag.

  CLEAR lv_carry_over_happened.

  lv_nat_approved = go_approval->is_fully_approved( iv_approval_area = zcl_arn_approval_backend=>gc_appr_area_national
                                                    iv_no_records_as_approved = abap_true ).
  lv_reg_approved = go_approval->is_fully_approved( zcl_arn_approval_backend=>gc_appr_area_regional ).

  "if national fully approved, do the regional defaulting
  IF  lv_nat_approved EQ abap_true
  AND pv_called_from  EQ co_appr_called_from_nat.

    "do the defaulting ( = national to regional carry over)
    PERFORM appr_do_reg_defaulting.

    "automatically save data
    "this can create additional regional approvals
    PERFORM process_regional_data
        USING abap_true.

    lv_carry_over_happened = abap_true.

    MESSAGE s079.

  ENDIF.

  "if fully approved, update header statuses to approved
  IF  lv_nat_approved EQ abap_true.
    PERFORM update_nat_status USING gs_worklist-idno gs_worklist-version co_status_apr.
  ENDIF.
  IF lv_reg_approved EQ abap_true.
    PERFORM update_reg_status USING gs_worklist-idno co_status_apr.
  ENDIF.

  COMMIT WORK AND WAIT.
  TRY.
      go_approval->enqueue_appr_idno_or_wait( zif_enq_mode_c=>optimistic ).
    CATCH zcx_excep_cls.
      MESSAGE a099.
  ENDTRY.


  IF lv_carry_over_happened = abap_true.

    "if carryover happened we need to check the recent approval data
    "before we continue to posting - carryover could have created
    "a new regional change categories which are not approved yet
    go_approval->load_recent_approval_data( ).

    lv_nat_approved = go_approval->is_fully_approved( iv_approval_area = zcl_arn_approval_backend=>gc_appr_area_national
                                                      iv_no_records_as_approved = abap_true ).
    lv_reg_approved = go_approval->is_fully_approved( zcl_arn_approval_backend=>gc_appr_area_regional ).

  ENDIF.

  IF  lv_nat_approved EQ abap_true
  AND lv_reg_approved EQ abap_true.
    "if both national and regional is fully approved
    "call the article posting

    PERFORM appr_post_article.
    TRY.
        go_approval->enqueue_appr_idno_or_wait( zif_enq_mode_c=>optimistic ).
      CATCH zcx_excep_cls.
        MESSAGE a099.
    ENDTRY.
  ENDIF.

ENDFORM.

FORM approvals_display_text2 USING pv_called_from     TYPE char3
                                   pv_change_category TYPE zarn_chg_category
                                   pv_team_code       TYPE zarn_team_code.

  DATA lv_appr_area TYPE zarn_e_appr_area.

  lv_appr_area = pv_called_from.
  go_approval->set_approval_area( lv_appr_area ).

  "get the approval record
  DATA(ls_approval) = go_approval->get_approval_record(
       EXPORTING iv_chg_category = pv_change_category
                 iv_team_code    = pv_team_code  ).

  "and display screen with text
  gv_appr_comment = ls_approval-status_comment.

  IF ls_approval-status = zcl_arn_approval_backend=>gc_stat-rejected.
    gv_appr_rejection_reason = ls_approval-status_reason.
  ELSE.
    CLEAR gv_appr_rejection_reason.
  ENDIF.

  PERFORM appr_display_text.

ENDFORM.

FORM approvals_add_text2 USING pv_called_from     TYPE char3
                              pv_change_category TYPE zarn_chg_category
                              pv_team_code      TYPE zarn_team_code
                     CHANGING pv_column TYPE zarn_approval_column.


  DATA: lv_appr_area    TYPE zarn_e_appr_area,
        lt_approval_new TYPE zarn_t_approval,
        lt_appr_hist    TYPE zarn_t_appr_hist.

  lv_appr_area = pv_called_from.

  go_approval->set_approval_area( lv_appr_area ).

  DATA(ls_approval) = go_approval->get_approval_record(
        EXPORTING iv_chg_category = pv_change_category
                  iv_team_code    = pv_team_code  ).

  gv_appr_comment = ls_approval-status_comment.

  PERFORM appr_add_text.

  TRY.

      go_approval->action_change_comment(
          EXPORTING
            iv_team_code     = pv_team_code
            iv_chg_category  = pv_change_category
            iv_comment       = gv_appr_comment
          IMPORTING
            et_zarn_approval  = lt_approval_new
            et_zarn_appr_hist = lt_appr_hist ).

      go_approval->post_approvals( EXPORTING it_approval  = lt_approval_new
                                            it_appr_hist = lt_appr_hist ).


      COMMIT WORK AND WAIT.

    CATCH zcx_excep_cls.
      MESSAGE i099 DISPLAY LIKE 'E' .
      RETURN.
  ENDTRY.


  TRY.
      go_approval->enqueue_appr_idno_or_wait( zif_enq_mode_c=>optimistic ).
    CATCH zcx_excep_cls.
      MESSAGE a099.
  ENDTRY.

  IF gv_appr_comment IS INITIAL.
    pv_column = icon_create_text.
  ELSE.
    pv_column = icon_change_text.
    gv_change = abap_true.
  ENDIF.

ENDFORM.                    "approvals_add_text

*&---------------------------------------------------------------------*
*&      Form  HIGHLIGHT_CHANGED_FIELDS
*&---------------------------------------------------------------------*
FORM appr_highlight_changed_fields2 USING pv_called_from TYPE char3
                                         pv_chg_cat     TYPE zarn_chg_category.

  DATA: lt_cc_det               TYPE ztarn_cc_det,
        lt_cc_hdr               TYPE zarn_t_cc_hdr_sorted,
        lv_table                TYPE zarn_cc_det-chg_table,
        lv_field                TYPE zarn_cc_det-chg_field,
        lo_table                TYPE REF TO cl_salv_table,
        lo_columns              TYPE REF TO cl_salv_columns_table,
        lv_caption              TYPE text80,
        lv_width                TYPE i,
        lv_chg_area             LIKE zarn_cc_team-chg_area,
        lv_chg_cat_desc         TYPE zarn_chg_cat_desc,
        ls_chg_category_changes LIKE LINE OF gt_chg_category_changes,
        lv_tabname              TYPE ddobjname,
        lv_fieldname            TYPE dfies-fieldname,
        lv_label                TYPE string,
        lt_chg_category_changes TYPE STANDARD TABLE OF zarn_cc_det_popup_fields,
        ls_user_address         TYPE addr3_val.


  CLEAR: gt_chg_category_changes.

  go_approval->set_approval_area( CONV zarn_e_appr_area( pv_called_from ) ).

  go_approval->get_approvals_change_ids( EXPORTING iv_chg_category = pv_chg_cat
                                         IMPORTING et_cc_det  = lt_cc_det
                                                   et_cc_hdr  = lt_cc_hdr ).

  LOOP AT lt_cc_det INTO DATA(ls_cc_det).

    CLEAR ls_chg_category_changes.
    MOVE-CORRESPONDING ls_cc_det TO ls_chg_category_changes.
    CLEAR lv_label.
    lv_tabname   = ls_chg_category_changes-chg_table.
    lv_fieldname = ls_chg_category_changes-chg_field.
    CALL FUNCTION 'DDIF_FIELDLABEL_GET'
      EXPORTING
        tabname        = lv_tabname
        fieldname      = lv_fieldname
      IMPORTING
        label          = lv_label
      EXCEPTIONS
        not_found      = 1
        internal_error = 2.
    IF lv_label IS INITIAL.
      ls_chg_category_changes-chg_field_desc = ls_chg_category_changes-chg_field.
    ELSE.
      ls_chg_category_changes-chg_field_desc = lv_label.
    ENDIF.

    TRY.
        DATA(ls_cc_hdr) = lt_cc_hdr[  chgid = ls_cc_det-chgid
                                      idno = ls_cc_det-idno
                                      chg_category = ls_cc_det-chg_category
                                      chg_area = ls_cc_det-chg_area ].



        CLEAR ls_user_address.
        CALL FUNCTION 'SUSR_USER_ADDRESS_READ'
          EXPORTING
            user_name              = ls_cc_hdr-chg_cat_cre_by
          IMPORTING
            user_address           = ls_user_address
          EXCEPTIONS
            user_address_not_found = 1
            OTHERS                 = 2.
        IF sy-subrc EQ 0.
          ls_chg_category_changes-username = ls_user_address-name_text.
        ELSE.
          CLEAR ls_chg_category_changes-username.
        ENDIF.

      CATCH cx_sy_itab_line_not_found.

    ENDTRY.


    APPEND ls_chg_category_changes TO gt_chg_category_changes.
  ENDLOOP.


  SORT gt_chg_category_changes BY chg_cat_cre_on DESCENDING
       chg_table chg_field DESCENDING national_ver_id ver_comp_with reference_data value_old value_new chg_cat_cre_by.

  DELETE ADJACENT DUPLICATES FROM gt_chg_category_changes COMPARING ALL FIELDS.

  IF pv_chg_cat IS INITIAL.
    lv_caption = 'All Change Categories'(apa).
  ELSE.
    CALL FUNCTION 'Z_ARN_APPROVALS_GET_CHGCAT_DES'
      EXPORTING
        chg_category = pv_chg_cat
      IMPORTING
        chg_cat_desc = lv_chg_cat_desc.
    CONCATENATE
      'Change Category - '(ap4)
      lv_chg_cat_desc
      INTO lv_caption SEPARATED BY space.
  ENDIF.

  IF go_dialogbox_chg_category IS NOT INITIAL.
*   release change category change listing dialog box
    CALL METHOD go_dialogbox_chg_category->free.
    FREE go_dialogbox_chg_category.
  ENDIF.

  IF go_dialogbox_chg_category IS INITIAL.
    IF pv_called_from EQ co_appr_called_from_nat.
      lv_width = 1200.
    ELSEIF pv_called_from EQ co_appr_called_from_reg.
      lv_width = 1000.
    ENDIF.
    CREATE OBJECT go_dialogbox_chg_category
      EXPORTING
        top     = 40
        left    = 200
        caption = lv_caption
        width   = lv_width
        height  = 150.
    IF go_appr_nat_event_handler IS INITIAL.
*     If they do not open the national approvals screen we can get here from the regional screen so need to create the handler
      CREATE OBJECT go_appr_nat_event_handler.
    ENDIF.
    SET HANDLER go_appr_nat_event_handler->appr_handle_close FOR go_dialogbox_chg_category.
  ELSE.
    CALL METHOD go_dialogbox_chg_category->set_caption
      EXPORTING
        caption           = lv_caption
      EXCEPTIONS
        cntl_error        = 1
        cntl_system_error = 2
        OTHERS            = 3.
  ENDIF.

  TRY.
      cl_salv_table=>factory( EXPORTING r_container = go_dialogbox_chg_category
                              IMPORTING r_salv_table = lo_table
                              CHANGING t_table = gt_chg_category_changes ).
      lo_columns = lo_table->get_columns( ).
      IF pv_called_from EQ co_appr_called_from_reg.
        lo_columns->get_column( 'NATIONAL_VER_ID' )->set_visible( abap_false ).
        lo_columns->get_column( 'VER_COMP_WITH' )->set_visible( abap_false ).
      ENDIF.
      lo_columns->set_optimize( 'X' ).
      lo_table->display( ).
    CATCH cx_salv_msg.
    CATCH cx_salv_not_found.
  ENDTRY.


ENDFORM.                    " HIGHLIGHT_CHANGED_FIELDS
*&---------------------------------------------------------------------*
*&      Form  PREPROCESS_APPROVAL
*&---------------------------------------------------------------------*
*       Preprocessing for approval selections
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM preprocess_approval .

  DATA: ls_art_ver  LIKE LINE OF gt_art_ver_ra,
        ls_app_area LIKE LINE OF gt_appr_area_ra.

  CLEAR: gt_art_ver_ra[], gt_appr_area_ra.

* Radio buttons New/Change
  CASE abap_true.
* New article
    WHEN p_apnew.
      ls_art_ver-sign = 'I'.
      ls_art_ver-option = 'EQ'.
      ls_art_ver-low = space.
      APPEND ls_art_ver TO gt_art_ver_ra.
* Changed article
    WHEN p_apchg.
      ls_art_ver-sign = 'I'.
      ls_art_ver-option = 'NE'.
      ls_art_ver-low = space.
      APPEND ls_art_ver TO gt_art_ver_ra.
    WHEN OTHERS. "Empty range OK
  ENDCASE.

* Radio buttons National/Regional
  CASE abap_true.
    WHEN p_apnat.
      ls_app_area-sign = 'I'.
      ls_app_area-option = 'EQ'.
      ls_app_area-low = zcl_arn_approval_backend=>gc_appr_area_national.
      APPEND ls_app_area TO gt_appr_area_ra.
    WHEN p_apreg.
      ls_app_area-sign = 'I'.
      ls_app_area-option = 'EQ'.
      ls_app_area-low = zcl_arn_approval_backend=>gc_appr_area_regional.
      APPEND ls_app_area TO gt_appr_area_ra.
    WHEN OTHERS. "Empty range OK
  ENDCASE.


ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  APPR_QUERY_POST_PROCESS
*&---------------------------------------------------------------------*
*       Approval query post processing
*----------------------------------------------------------------------*
*      <--P_LT_KEY  text
*----------------------------------------------------------------------*
FORM appr_query_post_process CHANGING fu_t_key TYPE ztarn_key.



* Perform some filtering
  PERFORM appr_filter_query_result CHANGING fu_t_key.
  CHECK fu_t_key IS NOT INITIAL.

*   get all National and Regional DB table data
  PERFORM read_national_data
    USING fu_t_key
    CHANGING gt_prod_data.

  PERFORM read_regional
    USING fu_t_key
    CHANGING gt_reg_data.

  PERFORM get_other_table_data USING      fu_t_key.

  gv_approval_data = abap_true.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  APPR_FILTER_QUERY_RESULT
*&---------------------------------------------------------------------*
*       Filter query results
*----------------------------------------------------------------------*
*      <--P_FU_T_KEY  text
*----------------------------------------------------------------------*
FORM appr_filter_query_result  CHANGING fu_t_key TYPE ztarn_key.

  DATA: lv_apply_filter TYPE abap_bool.

* With the new approval design, when users are searching for AP or PO status
* then we need to ensure that all approvals for the chosen id have this number
  IF s_apsta[] IS NOT INITIAL.
    DATA(lt_apsta) = s_apsta[].
    DELETE lt_apsta[] WHERE low = 'AP' OR low = 'PO'.
    IF lines( lt_apsta ) = 0.
* Apply the filter?
      DATA(lt_key) = fu_t_key.
      SORT lt_key BY idno.
      DELETE ADJACENT DUPLICATES FROM lt_key COMPARING idno.
* Identify any approvals that are not in the allowed range
      SELECT DISTINCT idno
        FROM zarn_approval
        INTO TABLE @DATA(lt_approval)
        FOR ALL ENTRIES IN @lt_key
        WHERE idno = @lt_key-idno AND
              status NOT IN @s_apsta[].
      IF sy-subrc = 0.
* Dispose of these from the result
        LOOP AT lt_approval ASSIGNING FIELD-SYMBOL(<ls_approval>).
          DELETE fu_t_key WHERE idno = <ls_approval>-idno.
        ENDLOOP.
      ENDIF.
    ENDIF.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  SET_DEFAULT_CURSOR
*&---------------------------------------------------------------------*
*       Set the default cursor on the UI
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM set_default_cursor .

  IF gv_set_default_cursor = abap_true.
    CLEAR gv_set_default_cursor.
    SET CURSOR FIELD zarn_products-fan_id.
  ENDIF.

ENDFORM.
