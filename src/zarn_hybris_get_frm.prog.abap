*&---------------------------------------------------------------------*
*&  Include           ZARN_HYBRIS_GET_FRM
*&---------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*&      Form  GET_HYBRIS
*&---------------------------------------------------------------------*
FORM get_hybris.

  DATA: lt_fanids TYPE zarn_fan_id_t,
        lv_fanid  TYPE zarn_fan_id,
        lt_fanidb TYPE zarn_fan_id_t,
        ls_fanidb LIKE LINE OF lt_fanidb,
        ls_input  TYPE zhybris_search_input,
        ls_hybris TYPE zarn_hybris,
        lt_hybris TYPE STANDARD TABLE OF zarn_hybris,
        lv_file   TYPE text255.

  FIELD-SYMBOLS: <ls_fanid> LIKE LINE OF lt_fanids.

  CLEAR: ls_input,
         ls_hybris.

  REFRESH: lt_fanids.

  LOOP AT s_fanid.
    APPEND s_fanid-low TO lt_fanids.
  ENDLOOP.
  IF p_file NE space.
    IF sy-batch EQ space.
      CALL METHOD cl_gui_frontend_services=>gui_upload
        EXPORTING
          filename                = p_file
        CHANGING
          data_tab                = lt_fanids
        EXCEPTIONS
          file_open_error         = 1
          file_read_error         = 2
          no_batch                = 3
          gui_refuse_filetransfer = 4
          invalid_type            = 5
          no_authority            = 6
          unknown_error           = 7
          bad_data_format         = 8
          header_not_allowed      = 9
          separator_not_allowed   = 10
          header_too_long         = 11
          unknown_dp_error        = 12
          access_denied           = 13
          dp_out_of_memory        = 14
          disk_full               = 15
          dp_timeout              = 16
          not_supported_by_gui    = 17
          error_no_gui            = 18
          OTHERS                  = 19.
    ELSE.
      lv_file = p_file.
      OPEN DATASET lv_file FOR INPUT IN LEGACY TEXT MODE.
      IF sy-subrc EQ 0.
        WHILE 1 EQ 1.
          READ DATASET lv_file INTO lv_fanid.
          IF sy-subrc EQ 0.
            APPEND lv_fanid TO lt_fanids.
          ELSE.
            EXIT. "do
          ENDIF.
        ENDWHILE.
        CLOSE DATASET lv_file.
      ENDIF.
    ENDIF.
  ENDIF.

  SORT lt_fanids.
  DELETE ADJACENT DUPLICATES FROM lt_fanids COMPARING fan_id.

  REFRESH lt_fanidb.
  LOOP AT lt_fanids ASSIGNING <ls_fanid>.
    APPEND <ls_fanid>-fan_id TO lt_fanidb.
    IF lines( lt_fanidb ) GE p_block.
      IF ls_hybris-run_id IS INITIAL.
        SELECT MAX( run_id )
          INTO ls_hybris-run_id
          FROM zarn_hybris.
        IF sy-subrc EQ 0.
          ls_hybris-run_id = ls_hybris-run_id + 1.
        ELSE.
          ls_hybris-run_id = 1.
        ENDIF.
        ls_hybris-create_user  = sy-uname.
        ls_hybris-create_date  = sy-datum.
        ls_hybris-create_time  = sy-uzeit.
        ls_hybris-run_sequence = 1.
      ELSE.
        ls_hybris-run_sequence = ls_hybris-run_sequence + 1.
      ENDIF.
      CLEAR ls_hybris-fan_ids.
      LOOP AT lt_fanidb INTO ls_fanidb.
        IF ls_hybris-fan_ids IS INITIAL.
          ls_hybris-fan_ids = ls_fanidb-fan_id.
        ELSE.
          CONCATENATE
            ls_fanidb-fan_id
            ls_hybris-fan_ids
            INTO ls_hybris-fan_ids SEPARATED BY ','.
        ENDIF.
      ENDLOOP.
      ls_hybris-fan_id_count = lines( lt_fanidb ).
      INSERT zarn_hybris FROM ls_hybris.
      COMMIT WORK AND WAIT.
      CALL FUNCTION 'Z_HYBRIS_GET'
        EXPORTING
          hybris_search_input  = ls_input
          fan_ids              = lt_fanidb
          call_arena_inbound   = p_arena
          call_external_system = p_calles
          arena_inbound_type   = co_arena_inbound
          run_id               = ls_hybris-run_id
          run_sequence         = ls_hybris-run_sequence
        EXCEPTIONS
          hybris_not_available = 1.
      IF p_arena EQ abap_true.
        PERFORM check_arena USING ls_hybris.
      ENDIF.
      REFRESH lt_fanidb.
      IF p_wait GT 0.
        WAIT UP TO p_wait SECONDS.
      ENDIF.
    ENDIF.
  ENDLOOP.
  IF lines( lt_fanidb ) GT 0.
    IF ls_hybris-run_id IS INITIAL.
      SELECT MAX( run_id )
        INTO ls_hybris-run_id
        FROM zarn_hybris.
      IF sy-subrc EQ 0.
        ls_hybris-run_id = ls_hybris-run_id + 1.
      ELSE.
        ls_hybris-run_id = 1.
      ENDIF.
      ls_hybris-create_user  = sy-uname.
      ls_hybris-create_date  = sy-datum.
      ls_hybris-create_time  = sy-uzeit.
      ls_hybris-run_sequence = 1.
    ELSE.
      ls_hybris-run_sequence = ls_hybris-run_sequence + 1.
    ENDIF.
    LOOP AT lt_fanidb INTO ls_fanidb.
      IF ls_hybris-fan_ids IS INITIAL.
        ls_hybris-fan_ids = ls_fanidb-fan_id.
      ELSE.
        CONCATENATE
          ls_fanidb-fan_id
          ls_hybris-fan_ids
          INTO ls_hybris-fan_ids SEPARATED BY ','.
      ENDIF.
    ENDLOOP.
    ls_hybris-fan_id_count = lines( lt_fanidb ).
    INSERT zarn_hybris FROM ls_hybris.
    CALL FUNCTION 'Z_HYBRIS_GET'
      EXPORTING
        hybris_search_input  = ls_input
        fan_ids              = lt_fanidb
        call_arena_inbound   = p_arena
        call_external_system = p_calles
        arena_inbound_type   = co_arena_inbound
        run_id               = ls_hybris-run_id
        run_sequence         = ls_hybris-run_sequence
      EXCEPTIONS
        hybris_not_available = 1.
    IF p_arena EQ abap_true.
      PERFORM check_arena USING ls_hybris.
    ENDIF.
    REFRESH lt_fanids.
  ENDIF.
  SELECT *
    INTO TABLE gt_hybris
    FROM zarn_hybris
    WHERE run_id EQ ls_hybris-run_id.

ENDFORM.                    "get_hybris

*&---------------------------------------------------------------------*
*&      Form  GET_FILE
*&---------------------------------------------------------------------*
FORM get_file CHANGING pc_file TYPE string.

  DATA: lt_file_table TYPE filetable,
        ls_file_table LIKE LINE OF lt_file_table,
        lv_rc         TYPE i.

  REFRESH lt_file_table.
  CALL METHOD cl_gui_frontend_services=>file_open_dialog
*    EXPORTING
*      window_title            =
*      default_extension       =
*      default_filename        =
*      file_filter             =
*      with_encoding           =
*      initial_directory       =
*      multiselection          =
    CHANGING
      file_table              = lt_file_table
      rc                      = lv_rc
*     user_action             =
*     file_encoding           =
    EXCEPTIONS
      file_open_dialog_failed = 1
      cntl_error              = 2
      error_no_gui            = 3
      not_supported_by_gui    = 4
      OTHERS                  = 5.
  READ TABLE lt_file_table INTO ls_file_table INDEX 1.
  IF sy-subrc EQ 0.
    pc_file = ls_file_table-filename.
  ENDIF.

ENDFORM.                    " GET_FILE

*&---------------------------------------------------------------------*
*&      Form  UPDATE_ARENA
*&---------------------------------------------------------------------*
FORM update_arena.

  DATA: lt_hybris     TYPE STANDARD TABLE OF zarn_hybris,
        ls_results    TYPE zmaster_product_with_price_cre,
        ls_results_i  TYPE zmaster_product_with_price_cre,
        lv_log_no     TYPE balognr,
        lv_timestamp  TYPE timestampl,
        lv_timestamp2 TYPE timestampl,
        lo_root       TYPE REF TO cx_root,
        lv_msg        TYPE string.

  FIELD-SYMBOLS: <ls_hybris>  LIKE LINE OF lt_hybris,
                 <ls_product> LIKE LINE OF ls_results-master_product_with_price_crea-product.

  IF p_run_id IS INITIAL.
    MESSAGE e000 WITH 'Run ID is required'.
  ENDIF.

  SELECT *
    INTO TABLE lt_hybris
    FROM zarn_hybris
    WHERE run_id EQ p_run_id.
  IF lt_hybris[] IS INITIAL.
    MESSAGE i000 WITH 'No details for Run ID' p_run_id.
  ELSE.
    LOOP AT lt_hybris ASSIGNING <ls_hybris>.
      CHECK <ls_hybris>-hybris_result NE space.
      CLEAR ls_results.
      CALL TRANSFORMATION id
        SOURCE XML <ls_hybris>-hybris_result
        RESULT data = ls_results.

      UPDATE zarn_hybris
        SET before_arena_call  = lv_timestamp
            after_arena_call   = lv_timestamp2
            fan_id_count_arena = 0
            fan_ids_arena      = space
        WHERE run_id       EQ <ls_hybris>-run_id
        AND   run_sequence EQ <ls_hybris>-run_sequence.


      GET TIME STAMP FIELD lv_timestamp.

      TRY.
          IF p_indiv EQ abap_false.
            CALL FUNCTION 'ZARN_INBOUND_DATA_PROCESSING'
              EXPORTING
                it_product     = ls_results-master_product_with_price_crea-product
                is_admin       = ls_results-master_product_with_price_crea-admin
                iv_id_type     = co_arena_inbound
              IMPORTING
*               ET_RETURN      = ET_RETURN
*               ET_PRODUCT_ERROR = ET_PRODUCT_ERROR
                ev_slg1_log_no = lv_log_no.
          ELSE.
            LOOP AT ls_results-master_product_with_price_crea-product ASSIGNING <ls_product>.
              TRY.
                  CLEAR ls_results_i.
                  ls_results_i-master_product_with_price_crea-admin = ls_results-master_product_with_price_crea-admin.
                  APPEND <ls_product> TO ls_results_i-master_product_with_price_crea-product.
                  CALL FUNCTION 'ZARN_INBOUND_DATA_PROCESSING'
                    EXPORTING
                      it_product     = ls_results_i-master_product_with_price_crea-product
                      is_admin       = ls_results_i-master_product_with_price_crea-admin
                      iv_id_type     = co_arena_inbound
                    IMPORTING
*                     ET_RETURN      = ET_RETURN
*                     ET_PRODUCT_ERROR = ET_PRODUCT_ERROR
                      ev_slg1_log_no = lv_log_no.
                CATCH cx_root INTO lo_root.
                  lv_msg = lo_root->if_message~get_text( ).
                  WRITE:/ 'Exception during call to ZARN_INBOUND_DATA_PROCESSING:', lv_msg.
              ENDTRY.
            ENDLOOP.
          ENDIF.
          GET TIME STAMP FIELD lv_timestamp2.

          UPDATE zarn_hybris
            SET before_arena_call = lv_timestamp
                after_arena_call  = lv_timestamp2
            WHERE run_id       EQ <ls_hybris>-run_id
            AND   run_sequence EQ <ls_hybris>-run_sequence.
          PERFORM check_arena USING <ls_hybris>.
        CATCH cx_root INTO lo_root.
          lv_msg = lo_root->if_message~get_text( ).
          WRITE:/ 'Exception during call to ZARN_INBOUND_DATA_PROCESSING:', lv_msg.
      ENDTRY.
    ENDLOOP.
  ENDIF.

  SELECT *
    INTO TABLE gt_hybris
    FROM zarn_hybris
    WHERE run_id EQ p_run_id.

ENDFORM.                    " UPDATE_ARENA

*&---------------------------------------------------------------------*
*&      Form  delete_arena
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
FORM delete_arena USING pv_run_id TYPE zarn_run_id
                        pv_quiet TYPE flag.

  DATA: lt_hybris  TYPE STANDARD TABLE OF zarn_hybris,
        lt_fan_ids TYPE zarn_fan_id_t,
        lt_tables  TYPE STANDARD TABLE OF zarn_table_mastr,
        lv_count   TYPE sy-dbcnt,
        lv_idno    TYPE zarn_products-idno.

  FIELD-SYMBOLS: <ls_hybris> LIKE LINE OF lt_hybris,
                 <ls_fan_id> LIKE LINE OF lt_fan_ids,
                 <ls_table>  LIKE LINE OF lt_tables.

  IF pv_run_id IS INITIAL.
    IF pv_quiet EQ abap_false.
      MESSAGE e000 WITH 'Run ID is required'.
    ENDIF.
  ENDIF.

  SELECT *
    INTO TABLE lt_hybris
    FROM zarn_hybris
    WHERE run_id EQ pv_run_id.
  IF lt_hybris[] IS INITIAL.
    IF pv_quiet EQ abap_false.
      MESSAGE i000 WITH 'No details for Run ID' pv_run_id.
    ENDIF.
  ELSE.
    SELECT *
      INTO TABLE lt_tables
      FROM zarn_table_mastr
      WHERE arena_table_type EQ 'N'.

*   Control tables that can be deleted
    APPEND INITIAL LINE TO lt_tables ASSIGNING <ls_table>.
    <ls_table>-arena_table = 'ZARN_PRD_VERSION'.
    APPEND INITIAL LINE TO lt_tables ASSIGNING <ls_table>.
    <ls_table>-arena_table = 'ZARN_VER_STATUS'.

    LOOP AT lt_hybris ASSIGNING <ls_hybris>.
      REFRESH lt_fan_ids.
      SPLIT <ls_hybris>-fan_ids AT ',' INTO TABLE lt_fan_ids.
      LOOP AT lt_fan_ids ASSIGNING <ls_fan_id>.
        SELECT SINGLE idno
          INTO lv_idno
          FROM zarn_products
          WHERE fan_id EQ <ls_fan_id>-fan_id.
        IF sy-subrc EQ 0 AND NOT lv_idno IS INITIAL.
          LOOP AT lt_tables ASSIGNING <ls_table>.
            DELETE FROM (<ls_table>-arena_table) WHERE idno EQ lv_idno.
          ENDLOOP.
        ENDIF.
      ENDLOOP.

      UPDATE zarn_hybris
        SET fan_id_count_arena = 0
            before_arena_call  = 0
            after_arena_call   = 0
            fan_ids_arena      = space
        WHERE run_id       EQ <ls_hybris>-run_id
        AND   run_sequence EQ <ls_hybris>-run_sequence.
    ENDLOOP.
  ENDIF.

ENDFORM.                    "delete_arena

*&---------------------------------------------------------------------*
*&      Form  DISPLAY_RESULTS
*&---------------------------------------------------------------------*
FORM display_results.

  DATA: ls_layout     TYPE slis_layout_alv,
        lt_fieldcat   TYPE slis_t_fieldcat_alv,
        ls_hybris_res LIKE LINE OF gt_hybris_res.

  FIELD-SYMBOLS: <ls_fieldcat> LIKE LINE OF lt_fieldcat,
                 <ls_hybris>   LIKE LINE OF gt_hybris.

  LOOP AT gt_hybris ASSIGNING <ls_hybris>.
    CLEAR ls_hybris_res.
    MOVE-CORRESPONDING <ls_hybris> TO ls_hybris_res.

    IF  NOT <ls_hybris>-before_hybris_call IS INITIAL
    AND NOT <ls_hybris>-sap_transmission IS INITIAL.
      CALL METHOD cl_abap_tstmp=>subtract
        EXPORTING
          tstmp1 = <ls_hybris>-sap_transmission
          tstmp2 = <ls_hybris>-before_hybris_call
        RECEIVING
          r_secs = ls_hybris_res-sap_hybris_time.
      PERFORM fix_time CHANGING ls_hybris_res-sap_hybris_time.
    ENDIF.

    IF  NOT <ls_hybris>-sap_transmission IS INITIAL
    AND NOT <ls_hybris>-hybris_returned IS INITIAL.
      CALL METHOD cl_abap_tstmp=>subtract
        EXPORTING
          tstmp1 = <ls_hybris>-hybris_returned
          tstmp2 = <ls_hybris>-sap_transmission
        RECEIVING
          r_secs = ls_hybris_res-hybris_time.
      PERFORM fix_time CHANGING ls_hybris_res-hybris_time.
    ENDIF.

    IF  NOT <ls_hybris>-before_arena_call IS INITIAL
    AND NOT <ls_hybris>-after_arena_call IS INITIAL.
      CALL METHOD cl_abap_tstmp=>subtract
        EXPORTING
          tstmp1 = <ls_hybris>-after_arena_call
          tstmp2 = <ls_hybris>-before_arena_call
        RECEIVING
          r_secs = ls_hybris_res-arena_time.
      PERFORM fix_time CHANGING ls_hybris_res-arena_time.
    ENDIF.

    ls_hybris_res-xml = icon_xml_doc.
    APPEND ls_hybris_res TO gt_hybris_res.
  ENDLOOP.

  CALL FUNCTION 'REUSE_ALV_FIELDCATALOG_MERGE'
    EXPORTING
      i_structure_name       = 'ZARN_HYBRIS_RESULT'
      i_bypassing_buffer     = 'X'
    CHANGING
      ct_fieldcat            = lt_fieldcat
    EXCEPTIONS
      inconsistent_interface = 1
      program_error          = 2.
  LOOP AT lt_fieldcat ASSIGNING <ls_fieldcat>.
    CASE <ls_fieldcat>-fieldname.
      WHEN 'FAN_ID_COUNT'.
        <ls_fieldcat>-hotspot = abap_true.
      WHEN 'FAN_ID_COUNT_MSG'.
        <ls_fieldcat>-hotspot = abap_true.
      WHEN 'FAN_ID_COUNT_ARENA'.
        <ls_fieldcat>-hotspot = abap_true.
      WHEN 'XML'.
        <ls_fieldcat>-icon = abap_true.
        <ls_fieldcat>-hotspot = abap_true.
      WHEN 'CREATE_USER'.
        <ls_fieldcat>-seltext_l    = 'User'.
        <ls_fieldcat>-seltext_m    = <ls_fieldcat>-seltext_l.
        <ls_fieldcat>-seltext_s    = <ls_fieldcat>-seltext_l.
        <ls_fieldcat>-reptext_ddic = <ls_fieldcat>-seltext_l.
      WHEN 'CREATE_DATE'.
        <ls_fieldcat>-seltext_l    = 'Created'.
        <ls_fieldcat>-seltext_m    = <ls_fieldcat>-seltext_l.
        <ls_fieldcat>-seltext_s    = <ls_fieldcat>-seltext_l.
        <ls_fieldcat>-reptext_ddic = <ls_fieldcat>-seltext_l.
    ENDCASE.
  ENDLOOP.

  ls_layout-zebra             = abap_true.
  ls_layout-colwidth_optimize = abap_true.
  CALL FUNCTION 'REUSE_ALV_GRID_DISPLAY'
    EXPORTING
*     I_INTERFACE_CHECK       = ' '
*     I_BYPASSING_BUFFER      = ' '
*     I_BUFFER_ACTIVE         = ' '
      i_callback_program      = 'ZARN_HYBRIS_GET'
*     I_CALLBACK_PF_STATUS_SET          = ' '
      i_callback_user_command = 'USER_COMMAND'
*     I_CALLBACK_TOP_OF_PAGE  = ' '
*     I_CALLBACK_HTML_TOP_OF_PAGE       = ' '
*     I_CALLBACK_HTML_END_OF_LIST       = ' '
*     I_STRUCTURE_NAME        = I_STRUCTURE_NAME
*     I_BACKGROUND_ID         = ' '
*     I_GRID_TITLE            = I_GRID_TITLE
*     I_GRID_SETTINGS         = I_GRID_SETTINGS
      is_layout               = ls_layout
      it_fieldcat             = lt_fieldcat
*     IT_EXCLUDING            = IT_EXCLUDING
*     IT_SPECIAL_GROUPS       = IT_SPECIAL_GROUPS
*     IT_SORT                 = IT_SORT
*     IT_FILTER               = IT_FILTER
*     IS_SEL_HIDE             = IS_SEL_HIDE
*     I_DEFAULT               = 'X'
*     I_SAVE                  = ' '
*     IS_VARIANT              = IS_VARIANT
*     IT_EVENTS               = IT_EVENTS
*     IT_EVENT_EXIT           = IT_EVENT_EXIT
*     IS_PRINT                = IS_PRINT
*     IS_REPREP_ID            = IS_REPREP_ID
*     I_SCREEN_START_COLUMN   = 0
*     I_SCREEN_START_LINE     = 0
*     I_SCREEN_END_COLUMN     = 0
*     I_SCREEN_END_LINE       = 0
*     I_HTML_HEIGHT_TOP       = 0
*     I_HTML_HEIGHT_END       = 0
*     IT_ALV_GRAPHICS         = IT_ALV_GRAPHICS
*     IT_HYPERLINK            = IT_HYPERLINK
*     IT_ADD_FIELDCAT         = IT_ADD_FIELDCAT
*     IT_EXCEPT_QINFO         = IT_EXCEPT_QINFO
*     IR_SALV_FULLSCREEN_ADAPTER        = IR_SALV_FULLSCREEN_ADAPTER
*   IMPORTING
*     E_EXIT_CAUSED_BY_CALLER = E_EXIT_CAUSED_BY_CALLER
*     ES_EXIT_CAUSED_BY_USER  = ES_EXIT_CAUSED_BY_USER
    TABLES
      t_outtab                = gt_hybris_res
    EXCEPTIONS
      program_error           = 1.

ENDFORM.                    " DISPLAY_RESULTS

*&---------------------------------------------------------------------*
*&      Form  USER_COMMAND
*&---------------------------------------------------------------------*
FORM user_command USING r_ucomm     LIKE sy-ucomm
                        rs_selfield TYPE slis_selfield.

  FIELD-SYMBOLS: <ls_hybris_res> LIKE LINE OF gt_hybris_res,
                 <ls_hybris>     LIKE LINE OF gt_hybris.

  READ TABLE gt_hybris_res ASSIGNING <ls_hybris_res> INDEX rs_selfield-tabindex.
  IF sy-subrc EQ 0.
    READ TABLE gt_hybris ASSIGNING <ls_hybris> WITH KEY run_id = <ls_hybris_res>-run_id run_sequence = <ls_hybris_res>-run_sequence.
    IF sy-subrc EQ 0.
      CASE rs_selfield-fieldname.
        WHEN 'FAN_ID_COUNT'.
          PERFORM show_fan_ids USING <ls_hybris>-fan_ids.
        WHEN 'FAN_ID_COUNT_MSG'.
          PERFORM show_fan_ids_diff USING <ls_hybris>-fan_ids <ls_hybris>-fan_ids_msg.
        WHEN 'FAN_ID_COUNT_ARENA'.
          PERFORM show_fan_ids_diff USING <ls_hybris>-fan_ids <ls_hybris>-fan_ids_arena.
        WHEN 'XML'.
          PERFORM show_xml USING <ls_hybris>-hybris_result.
      ENDCASE.
    ENDIF.
  ENDIF.

ENDFORM.                    "user_command

*&---------------------------------------------------------------------*
*&      Form  SHOW_FAN_IDS
*&---------------------------------------------------------------------*
FORM show_fan_ids USING pv_fan_ids TYPE string.

  DATA: lt_fan_ids      TYPE zarn_fan_id_t,
        lt_display      TYPE STANDARD TABLE OF text100,
        ls_display      LIKE LINE OF lt_display,
        lt_arn_products TYPE SORTED TABLE OF zarn_products WITH NON-UNIQUE KEY fan_id.

  FIELD-SYMBOLS: <ls_fan_id>      LIKE LINE OF lt_fan_ids,
                 <ls_arn_product> LIKE LINE OF lt_arn_products.

  REFRESH lt_fan_ids.
  SPLIT pv_fan_ids AT ',' INTO TABLE lt_fan_ids.
  IF NOT lt_fan_ids[] IS INITIAL.
    SORT lt_fan_ids.
    SELECT *
      INTO TABLE lt_arn_products
      FROM zarn_products
      FOR ALL ENTRIES IN lt_fan_ids
      WHERE fan_id EQ lt_fan_ids-fan_id.
    LOOP AT lt_fan_ids ASSIGNING <ls_fan_id>.
      CLEAR ls_display.
      ls_display(7) = <ls_fan_id>-fan_id.
      READ TABLE lt_arn_products ASSIGNING <ls_arn_product> WITH TABLE KEY fan_id = <ls_fan_id>-fan_id.
      IF sy-subrc EQ 0.
        ls_display+9(40) = <ls_arn_product>-fs_short_descr.
        ls_display+51(3) = <ls_arn_product>-seling_uom.
      ELSE.
        ls_display+9(40) = 'not found'.
      ENDIF.
      APPEND ls_display TO lt_display.
    ENDLOOP.
    CALL FUNCTION 'POPUP_WITH_TABLE_DISPLAY_OK'
      EXPORTING
        endpos_col   = 60
        endpos_row   = 20
        startpos_col = 5
        startpos_row = 5
        titletext    = 'FAN IDs'
      TABLES
        valuetab     = lt_display
      EXCEPTIONS
        break_off    = 1.
  ENDIF.

ENDFORM.                    " SHOW_FAN_IDS

*&---------------------------------------------------------------------*
*&      Form  SHOW_FAN_IDS_DIFF
*&---------------------------------------------------------------------*
FORM show_fan_ids_diff USING pv_fan_ids_orig TYPE string
                             pv_fan_ids      TYPE string.

  DATA: lt_fan_ids      TYPE zarn_fan_id_t,
        lt_fan_ids_orig TYPE zarn_fan_id_t,
        lt_display      TYPE STANDARD TABLE OF text100,
        lt_display_same TYPE STANDARD TABLE OF text100,
        lt_display_diff TYPE STANDARD TABLE OF text100,
        ls_display      LIKE LINE OF lt_display,
        lt_arn_products TYPE SORTED TABLE OF zarn_products WITH NON-UNIQUE KEY fan_id.

  FIELD-SYMBOLS: <ls_fan_id>      LIKE LINE OF lt_fan_ids,
                 <ls_arn_product> LIKE LINE OF lt_arn_products.

  REFRESH: lt_fan_ids,
           lt_fan_ids_orig.
  SPLIT pv_fan_ids AT ',' INTO TABLE lt_fan_ids.
  SPLIT pv_fan_ids_orig AT ',' INTO TABLE lt_fan_ids_orig.
  IF NOT lt_fan_ids_orig[] IS INITIAL.
    SORT lt_fan_ids_orig.
    SORT lt_fan_ids.
    SELECT *
      INTO TABLE lt_arn_products
      FROM zarn_products
      FOR ALL ENTRIES IN lt_fan_ids_orig
      WHERE fan_id EQ lt_fan_ids_orig-fan_id.
    LOOP AT lt_fan_ids_orig ASSIGNING <ls_fan_id>.
      CLEAR ls_display.
      ls_display(7) = <ls_fan_id>-fan_id.
      READ TABLE lt_arn_products ASSIGNING <ls_arn_product> WITH TABLE KEY fan_id = <ls_fan_id>-fan_id.
      IF sy-subrc EQ 0.
        ls_display+9(40) = <ls_arn_product>-fs_short_descr.
        ls_display+51(3) = <ls_arn_product>-seling_uom.
      ELSE.
        ls_display+9(40) = '(details not found)'.
      ENDIF.
      READ TABLE lt_fan_ids TRANSPORTING NO FIELDS WITH KEY fan_id = <ls_fan_id>-fan_id.
      IF sy-subrc EQ 0.
        APPEND ls_display TO lt_display_same.
      ELSE.
        APPEND ls_display TO lt_display_diff.
      ENDIF.
    ENDLOOP.
    IF NOT lt_display_diff[] IS INITIAL.
      CLEAR ls_display.
      ls_display = 'FAN IDs missing'.
      APPEND ls_display TO lt_display.
      APPEND LINES OF lt_display_diff TO lt_display.
    ENDIF.
    IF NOT lt_display_same[] IS INITIAL.
      IF NOT lt_display[] IS INITIAL.
        CLEAR ls_display.
        APPEND ls_display TO lt_display.
      ENDIF.
      CLEAR ls_display.
      ls_display = 'FAN IDs matching'.
      APPEND ls_display TO lt_display.
      APPEND LINES OF lt_display_same TO lt_display.
    ENDIF.

    CALL FUNCTION 'POPUP_WITH_TABLE_DISPLAY_OK'
      EXPORTING
        endpos_col   = 60
        endpos_row   = 20
        startpos_col = 5
        startpos_row = 5
        titletext    = 'FAN IDs'
      TABLES
        valuetab     = lt_display
      EXCEPTIONS
        break_off    = 1.
  ENDIF.

ENDFORM.                    "SHOW_FAN_IDS_DIFF

*&---------------------------------------------------------------------*
*&      Form  SHOW_XML
*&---------------------------------------------------------------------*
FORM show_xml USING pv_hybris_result TYPE string.

  DATA: lo_xml  TYPE REF TO cl_xml_document.

  IF pv_hybris_result EQ space.
    MESSAGE s000 WITH 'No XML found'.
    EXIT.
  ENDIF.

  CHECK pv_hybris_result NE space.

  CREATE OBJECT lo_xml.

  CALL METHOD lo_xml->parse_string
    EXPORTING
      stream = pv_hybris_result.
  CALL METHOD lo_xml->display.

ENDFORM.                    " SHOW_XML

*&---------------------------------------------------------------------*
*&      Form  CHECK_ARENA
*&---------------------------------------------------------------------*
FORM check_arena USING ps_hybris TYPE zarn_hybris.

  DATA: lt_fan_ids TYPE zarn_fan_id_t,
        lt_fan_idp TYPE SORTED TABLE OF zarn_fan_id_s WITH NON-UNIQUE KEY fan_id,
        lv_fan_ids TYPE string,
        lv_fan_idc TYPE i.

  FIELD-SYMBOLS: <ls_fan_id> LIKE LINE OF lt_fan_ids.

  SPLIT ps_hybris-fan_ids AT ',' INTO TABLE lt_fan_ids.
  CLEAR lv_fan_ids.
  lv_fan_idc = 0.
  IF NOT lt_fan_ids[] IS INITIAL.
    SELECT DISTINCT fan_id
      INTO TABLE lt_fan_idp
      FROM zarn_products
      FOR ALL ENTRIES IN lt_fan_ids
      WHERE fan_id EQ lt_fan_ids-fan_id.
    LOOP AT lt_fan_ids ASSIGNING <ls_fan_id>.
      READ TABLE lt_fan_idp TRANSPORTING NO FIELDS WITH TABLE KEY fan_id = <ls_fan_id>-fan_id.
      IF sy-subrc EQ 0.
        lv_fan_idc = lv_fan_idc + 1.
        IF lv_fan_ids IS INITIAL.
          lv_fan_ids = <ls_fan_id>-fan_id.
        ELSE.
          CONCATENATE
            lv_fan_ids
            <ls_fan_id>-fan_id
            INTO lv_fan_ids SEPARATED BY ','.
        ENDIF.
      ENDIF.
    ENDLOOP.
  ENDIF.
  UPDATE zarn_hybris
    SET fan_id_count_arena = lv_fan_idc
        fan_ids_arena      = lv_fan_ids
    WHERE run_id       EQ ps_hybris-run_id
    AND   run_sequence EQ ps_hybris-run_sequence.

ENDFORM.                    " CHECK_ARENA

*&---------------------------------------------------------------------*
*&      Form  DELETE_RESULTS
*&---------------------------------------------------------------------*
FORM delete_results.

  DATA: lv_answer TYPE c,
        lt_hybris TYPE STANDARD TABLE OF zarn_hybris.

  FIELD-SYMBOLS: <ls_hybris> LIKE LINE OF lt_hybris.

  IF s_run_id[] IS INITIAL.
    MESSAGE e000 WITH 'FAN ID is mandatory when deleting results'.
    EXIT.
  ENDIF.

  CALL FUNCTION 'POPUP_TO_CONFIRM'
    EXPORTING
*     TITLEBAR              = ' '
*     DIAGNOSE_OBJECT       = ' '
      text_question         = 'Are you sure you want to delete results ?'
*     TEXT_BUTTON_1         = 'Ja'(001)
*     ICON_BUTTON_1         = ' '
*     TEXT_BUTTON_2         = 'Nein'(002)
*     ICON_BUTTON_2         = ' '
      default_button        = '2'
      display_cancel_button = ' '
*     USERDEFINED_F1_HELP   = ' '
*     START_COLUMN          = 25
*     START_ROW             = 6
*     POPUP_TYPE            = POPUP_TYPE
*     IV_QUICKINFO_BUTTON_1 = ' '
*     IV_QUICKINFO_BUTTON_2 = ' '
    IMPORTING
      answer                = lv_answer
*   TABLES
*     PARAMETER             = PARAMETER
    EXCEPTIONS
      text_not_found        = 1.
  IF lv_answer EQ '1'.
    REFRESH lt_hybris.
    SELECT *
      INTO TABLE lt_hybris
      FROM zarn_hybris
      WHERE run_id IN s_run_id.
    LOOP AT lt_hybris ASSIGNING <ls_hybris>.
      IF p_darena IS NOT INITIAL.
        PERFORM delete_arena USING <ls_hybris>-run_id abap_true.
      ENDIF.
      DELETE FROM zarn_hybris WHERE run_id EQ <ls_hybris>-run_id AND run_sequence EQ <ls_hybris>-run_sequence.
    ENDLOOP.
  ENDIF.

ENDFORM.                    " DELETE_RESULTS

FORM fix_time CHANGING pv_timestamp TYPE tzntstmpl.

* 11 hours = 11 * 60 * 60 = 39600
* 12 hours = 12 * 60 * 60 = 43200

  IF pv_timestamp LT 0.
    IF pv_timestamp LT -39600.
      pv_timestamp = pv_timestamp + 43200.
    ENDIF.
  ELSE.
    IF pv_timestamp GT 39600.
      pv_timestamp = pv_timestamp - 43200.
    ENDIF.
  ENDIF.

ENDFORM.
