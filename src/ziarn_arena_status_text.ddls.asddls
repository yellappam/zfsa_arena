@AbapCatalog.sqlViewName: 'ziarnstatust'
@AbapCatalog.compiler.compareFilter: true
@AccessControl.authorizationCheck: #NOT_REQUIRED
@EndUserText.label: 'Arena Status Text'
define view ziarn_arena_status_text as select from dd07t {    
  key cast( substring( domvalue_l, 1, 1 ) as ZARN_E_ARENA_STATUS ) as Arena_Status,
  @Semantics.language: true
  key dd07t.ddlanguage as Language,
  @Semantics.text: true
  dd07t.ddtext as Arena_Status_Text
}
  where dd07t.domname = 'ZARN_D_ARENA_STATUS'
    and dd07t.as4local = 'A'
