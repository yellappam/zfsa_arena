* regenerated at 31.05.2017 16:34:14
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_ALLERGEN_TTOP.               " Global Data
  INCLUDE LZARN_ALLERGEN_TUXX.               " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_ALLERGEN_TF...               " Subroutines
* INCLUDE LZARN_ALLERGEN_TO...               " PBO-Modules
* INCLUDE LZARN_ALLERGEN_TI...               " PAI-Modules
* INCLUDE LZARN_ALLERGEN_TE...               " Events
* INCLUDE LZARN_ALLERGEN_TP...               " Local class implement.
* INCLUDE LZARN_ALLERGEN_TT99.               " ABAP Unit tests
  INCLUDE LZARN_ALLERGEN_TF00                     . " subprograms
  INCLUDE LZARN_ALLERGEN_TI00                     . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
