FUNCTION zarn_read_regional_data .
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(IT_KEY) TYPE  ZTARN_REG_KEY
*"  EXPORTING
*"     REFERENCE(ET_DATA) TYPE  ZTARN_REG_DATA
*"     REFERENCE(ES_DATA_ALL) TYPE  ZSARN_REG_DATA
*"----------------------------------------------------------------------
*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Functional Specification 3125
* DATE WRITTEN     # 01 03 2016
* SAP VERSION      # 731
* TYPE             # Function Module
* AUTHOR           # C90002769, Rajesh Anumolu
*-----------------------------------------------------------------------
* TITLE            # AReNa: Read all Regional DB tables
* PURPOSE          # AReNa: provide data for all Regional DB tables
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  DATA:
    lv_where_str   TYPE string,
    lt_key         TYPE ztarn_reg_key,
    ls_key         LIKE LINE OF lt_key,
    lv_itabname    TYPE char50,
    lv_datatabname TYPE char50,
    lv_success     TYPE char01,
    lt_tabname     TYPE ty_t_tabname,
    ls_tabname     TYPE ty_s_tabname,
    ls_data        LIKE LINE OF et_data,
    ls_reg_data    TYPE zsarn_reg_data,
    lo_reg_artlink TYPE REF TO zcl_arn_reg_artlink.  " ++ONED-217 JKH 21.11.2016


  FIELD-SYMBOLS:
    <tabname>      TYPE tabname,
    <itabname>     TYPE table,
    <table>        TYPE table,
    <dataitabname> TYPE table,
    <table_str>    TYPE any.


  lt_key = it_key.

  DELETE ADJACENT DUPLICATES FROM lt_key COMPARING idno .

  IF lt_key IS INITIAL.
    RETURN.
  ENDIF.


* Get regional Tables to be checked
  SELECT arena_table
    INTO TABLE lt_tabname[]
    FROM zarn_table_mastr
    WHERE arena_table_type = gc_reg.

  CREATE OBJECT lo_reg_artlink. " ++ONED-217 JKH 21.11.2016

* Read Database
  LOOP AT lt_tabname INTO ls_tabname.

    IF <tabname>  IS ASSIGNED. UNASSIGN <tabname>.  ENDIF.
    IF <itabname> IS ASSIGNED. UNASSIGN <itabname>. ENDIF.

    ASSIGN ls_tabname-tabname TO <tabname>.


** Select Data for all the Product tables
    CLEAR: lv_itabname.
    CONCATENATE 'LS_REG_DATA-' ls_tabname-tabname INTO lv_itabname.
    ASSIGN (lv_itabname) TO <itabname>.

* INS Begin of Change ONED-217 JKH 21.11.2016
    IF ls_tabname-tabname = 'ZARN_REG_ARTLINK'.

      CALL METHOD lo_reg_artlink->get_reg_artlink_idno
        EXPORTING
          it_idno        = lt_key[]
        IMPORTING
          et_reg_artlink = <itabname>.


    ELSE.
* INS End of Change ONED-217 JKH 21.11.2016

      SELECT * FROM (<tabname>)   "#EC CI_SEL_NESTED
        INTO CORRESPONDING FIELDS OF TABLE <itabname>
          FOR ALL ENTRIES IN lt_key[]
          WHERE idno    EQ lt_key-idno.
    ENDIF.

  ENDLOOP.

* export single layer
  es_data_all = ls_reg_data.

* now split the tables into ZTARN_PROD_DATA Format
  LOOP AT lt_key INTO ls_key.
    ls_data-idno    =  ls_key-idno.

    CLEAR lv_where_str.

    CONCATENATE 'IDNO EQ ' '''' INTO lv_where_str SEPARATED BY space.
    CONCATENATE lv_where_str ls_data-idno '''' INTO lv_where_str.

*   loop at seperate tables
    LOOP AT lt_tabname INTO ls_tabname.
*      IF <tabname>  IS ASSIGNED. UNASSIGN <tabname>.  ENDIF.
      IF <itabname> IS ASSIGNED. UNASSIGN <itabname>. ENDIF.

*     Select Data for all the Product tables
      CLEAR: lv_itabname.
      CONCATENATE 'LS_REG_DATA-' ls_tabname-tabname INTO lv_itabname.
      ASSIGN (lv_itabname) TO <itabname>.

*     Select Data for all the Product tables
      CLEAR: lv_datatabname.
      CONCATENATE 'LS_DATA-' ls_tabname-tabname INTO lv_datatabname.
      ASSIGN (lv_datatabname) TO <dataitabname>.

* INS Begin of Change ONED-217 JKH 21.11.2016
      IF ls_tabname-tabname = 'ZARN_REG_ARTLINK'.

        CALL METHOD lo_reg_artlink->get_reg_artlink_idno
          EXPORTING
            iv_idno        = ls_data-idno
          IMPORTING
            et_reg_artlink = <dataitabname>.

        IF <dataitabname> IS NOT INITIAL.
          lv_success = abap_true.
        ENDIF.
      ELSE.
* INS End of Change ONED-217 JKH 21.11.2016

*     loop at tables for KEY and append
        LOOP AT <itabname> ASSIGNING <table_str> WHERE (lv_where_str).
*       append data to LS_DATA structure
          APPEND <table_str> TO <dataitabname>.
        ENDLOOP.
        IF sy-subrc IS INITIAL.
          lv_success = abap_true.
        ENDIF.

      ENDIF.


    ENDLOOP.

*   append to data table
    IF lv_success IS NOT INITIAL.
      APPEND ls_data TO et_data.
      CLEAR: ls_data,
             lv_success.
    ENDIF.
  ENDLOOP.

ENDFUNCTION.
