*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_NAT_RECONCILIATION_PBO
*&---------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*&      Module  STATUS_9000  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE status_9000 OUTPUT.
  CONSTANTS : lc_pf     TYPE char20 VALUE 'ZPF_ARN_NAT_RECO',
              lc_tb_dsp TYPE char20 VALUE 'ZTB_ARN_NAT_RECO'.


* Set PF status and title bar
  SET PF-STATUS lc_pf.
  SET TITLEBAR  lc_tb_dsp.

ENDMODULE.  " STATUS_9000
*&---------------------------------------------------------------------*
*&      Module  TS_NAT_RECO_ACTIVE_TAB_SET  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE ts_nat_reco_active_tab_set OUTPUT.

  ts_nat_reco-activetab = g_ts_nat_reco-pressed_tab.
  CASE g_ts_nat_reco-pressed_tab.
    WHEN c_ts_nat_reco-tab1.
      g_ts_nat_reco-subscreen = '9001'.
    WHEN c_ts_nat_reco-tab2.
      g_ts_nat_reco-subscreen = '9002'.
    WHEN OTHERS.
  ENDCASE.

ENDMODULE.  " TS_NAT_RECO_ACTIVE_TAB_SET
*&---------------------------------------------------------------------*
*&      Module  DISPLAY_ALV_9001  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE display_alv_9001 OUTPUT.

* Display ALV 9001
  PERFORM display_alv_9001.

ENDMODULE.  " DISPLAY_ALV_9001
*&---------------------------------------------------------------------*
*&      Module  DISPLAY_ALV_9002  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE display_alv_9002 OUTPUT.

* Display ALV 9002
  PERFORM display_alv_9002.

ENDMODULE.  " DISPLAY_ALV_9002
