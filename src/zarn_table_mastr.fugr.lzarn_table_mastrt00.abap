*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 05.05.2016 at 10:20:42 by user C90001363
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_TABLE_MASTR................................*
DATA:  BEGIN OF STATUS_ZARN_TABLE_MASTR              .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_TABLE_MASTR              .
CONTROLS: TCTRL_ZARN_TABLE_MASTR
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_TABLE_MASTR              .
TABLES: ZARN_TABLE_MASTR               .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
