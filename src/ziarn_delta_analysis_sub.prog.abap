*&---------------------------------------------------------------------*
*&  Include           ZIARN_DELTA_ANALYSIS_SUB
*&---------------------------------------------------------------------*
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 13.04.2018
* CHANGE No.       # Charm 9000003482; Jira CI18-554
* DESCRIPTION      # CI18-554 Recipe Management - Ingredient Changes
*                  # New regional table for Allergen Type overrides and
*                  # fields for Ingredients statement overrides in Arena.
*                  # New Ingredients Type field in the Scales block in
*                  # Arena and NONSAP tab in material master.
* WHO              # C90005557, Tessa Newman
*-----------------------------------------------------------------------
* DATE             # 06.06.2018
* CHANGE No.       # ChaRM 9000003675; JIRA CI18-506 NEW Range Status flag
* DESCRIPTION      # New Range Flag, Range Availability and Range Detail
*                  # fields added in Arena and on material master in Listing
*                  # tab and Basic Data Text tab (materiallongtext).
* WHO              # C90005557, Tessa Newman
*-----------------------------------------------------------------------
*&---------------------------------------------------------------------*
*&      Form  GET_INITIAL_DATA_SINGLE
*&---------------------------------------------------------------------*
* Get Initial Data for Single FAN
*----------------------------------------------------------------------*
FORM get_initial_data_single.

  DATA : ls_fan_ver_stat TYPE ty_s_fan_ver_stat,
         lv_fan_cnt      TYPE i,
         ls_mara         TYPE ty_s_mara.

  IF gt_mara[] IS NOT INITIAL.
    CLEAR ls_mara.
    READ TABLE gt_mara[] INTO ls_mara INDEX 1.
    gv_old_fan = ls_mara-zzfan.
  ENDIF.

  IF gv_old_fan = p_fan.
    gv_user_input = abap_true.
  ENDIF.

  IF gv_old_fan IS INITIAL.
    gv_old_fan = p_fan.
  ENDIF.



* Default SAP Article
  CLEAR: p_matnr, gt_mara[].
  SELECT *
    FROM mara
    INTO CORRESPONDING FIELDS OF TABLE gt_mara[]
    WHERE zzfan = p_fan.
  IF sy-subrc = 0.
    CLEAR ls_mara.
    READ TABLE gt_mara[] INTO ls_mara INDEX 1.
    IF sy-subrc = 0.
      p_matnr = ls_mara-matnr.
    ENDIF.
  ENDIF.


  CLEAR gt_fan_ver_stat[].
* Single Article
  SELECT a~idno a~previous_ver a~current_ver a~latest_ver a~article_ver
         a~changed_on a~changed_at a~changed_by
         b~fan_id  AS fan
    INTO CORRESPONDING FIELDS OF TABLE gt_fan_ver_stat[]
    FROM zarn_ver_status AS a
    JOIN zarn_products AS b
    ON a~idno = b~idno
    WHERE b~fan_id = p_fan.

  SORT gt_fan_ver_stat[].
  DELETE ADJACENT DUPLICATES FROM gt_fan_ver_stat[] COMPARING ALL FIELDS.

  CLEAR lv_fan_cnt.
  DESCRIBE TABLE gt_fan_ver_stat[] LINES lv_fan_cnt.

* Multiple records found for FAN ID &
  IF lv_fan_cnt GT 1.
    MESSAGE s051 WITH p_fan DISPLAY LIKE 'E'.
    gv_error = abap_true.
    EXIT.
  ELSEIF lv_fan_cnt EQ 1.

    READ TABLE gt_fan_ver_stat[] INTO ls_fan_ver_stat INDEX 1.
    IF sy-subrc = 0.

      IF p_fan = gv_old_fan.

        IF p_ver1 IS INITIAL AND gv_user_input IS INITIAL.
          IF ls_fan_ver_stat-article_ver IS INITIAL.
            IF ls_fan_ver_stat-previous_ver IS INITIAL.
              p_ver1 = ls_fan_ver_stat-current_ver.
            ELSE.
              p_ver1 = ls_fan_ver_stat-previous_ver.
            ENDIF.
          ELSE.
            p_ver1 = ls_fan_ver_stat-article_ver.
          ENDIF.

        ENDIF.

        IF p_ver2 IS INITIAL.
          p_ver2 = ls_fan_ver_stat-current_ver.
        ENDIF.

      ELSE.
*        p_ver1 = ls_fan_ver_stat-article_ver.

        IF ls_fan_ver_stat-article_ver IS INITIAL.
          IF ls_fan_ver_stat-previous_ver IS INITIAL.
            p_ver1 = ls_fan_ver_stat-current_ver.
          ELSE.
            p_ver1 = ls_fan_ver_stat-previous_ver.
          ENDIF.
        ELSE.
          p_ver1 = ls_fan_ver_stat-article_ver.
        ENDIF.


        p_ver2 = ls_fan_ver_stat-current_ver.
        CLEAR: p_ver3, p_ver4, p_ver5,
               gv_user_input.
      ENDIF.



    ENDIF.



  ENDIF.


* FAN ID does not exist
  IF gt_fan_ver_stat[] IS INITIAL.
    MESSAGE s052 WITH p_fan DISPLAY LIKE 'E'.
    gv_error = abap_true.
    EXIT.
  ENDIF.

ENDFORM.                    " GET_INITIAL_DATA_SINGLE
*&---------------------------------------------------------------------*
*&      Form  VALIDATE_SINGLE_FAN_VERSIONS
*&---------------------------------------------------------------------*
* Validate Single FAN ID Versions
*----------------------------------------------------------------------*
FORM validate_single_fan_versions.

  DATA : ls_version      TYPE ty_s_ver_screen,
         ls_fan_ver_stat TYPE ty_s_fan_ver_stat,
         ls_prd_version  TYPE zarn_prd_version,
         ls_mara         TYPE ty_s_mara,
         lv_vno          TYPE i.

  FIELD-SYMBOLS: <ls_version> TYPE ty_s_ver_screen.


  CLEAR gt_version[].

  CLEAR ls_fan_ver_stat.
  READ TABLE gt_fan_ver_stat[] INTO ls_fan_ver_stat INDEX 1.
  IF sy-subrc = 0.

    CLEAR ls_mara.
    READ TABLE gt_mara[] INTO ls_mara INDEX 1.

    lv_vno = 1.

    IF p_ver1 IS NOT INITIAL.
      CLEAR ls_version.
      READ TABLE gt_version[] INTO ls_version
      WITH KEY version = p_ver1.
      IF sy-subrc = 0.
** Only Distinct Versions can be compared : &
*        MESSAGE s053 WITH p_ver1 DISPLAY LIKE 'E'.
*        gv_error = abap_true.
*        EXIT.
      ELSE.
        ls_version-fan     = ls_fan_ver_stat-fan.
        ls_version-idno    = ls_fan_ver_stat-idno.
        ls_version-matnr   = ls_mara-matnr.
        ls_version-vno     = lv_vno.
        ls_version-version = p_ver1.
        INSERT ls_version INTO TABLE gt_version[].
        lv_vno = lv_vno + 1.
      ENDIF.
    ELSE.
      ls_version-fan     = ls_fan_ver_stat-fan.
      ls_version-idno    = ls_fan_ver_stat-idno.
      ls_version-matnr   = ls_mara-matnr.
      ls_version-vno     = lv_vno.
*        ls_version-version = p_ver1.
      INSERT ls_version INTO TABLE gt_version[].
      lv_vno = lv_vno + 1.
    ENDIF.


    IF p_ver2 IS NOT INITIAL.
      CLEAR ls_version.
      READ TABLE gt_version[] INTO ls_version
      WITH KEY version = p_ver2.
      IF sy-subrc = 0.
** Only Distinct Versions can be compared : &
*        MESSAGE s053 WITH p_ver2 DISPLAY LIKE 'E'.
*        gv_error = abap_true.
*        EXIT.
      ELSE.
        ls_version-fan     = ls_fan_ver_stat-fan.
        ls_version-idno    = ls_fan_ver_stat-idno.
        ls_version-matnr   = ls_mara-matnr.
        ls_version-vno     = lv_vno.
        ls_version-version = p_ver2.
        INSERT ls_version INTO TABLE gt_version[].
        lv_vno = lv_vno + 1.
      ENDIF.
    ENDIF.


    IF p_ver3 IS NOT INITIAL.
      CLEAR ls_version.
      READ TABLE gt_version[] INTO ls_version
      WITH KEY version = p_ver3.
      IF sy-subrc = 0.
** Only Distinct Versions can be compared : &
*        MESSAGE s053 WITH p_ver3 DISPLAY LIKE 'E'.
*        gv_error = abap_true.
*        EXIT.
      ELSE.
        ls_version-fan     = ls_fan_ver_stat-fan.
        ls_version-idno    = ls_fan_ver_stat-idno.
        ls_version-matnr   = ls_mara-matnr.
        ls_version-vno     = lv_vno.
        ls_version-version = p_ver3.
        INSERT ls_version INTO TABLE gt_version[].
        lv_vno = lv_vno + 1.
      ENDIF.
    ENDIF.


    IF p_ver4 IS NOT INITIAL.
      CLEAR ls_version.
      READ TABLE gt_version[] INTO ls_version
      WITH KEY version = p_ver4.
      IF sy-subrc = 0.
** Only Distinct Versions can be compared : &
*        MESSAGE s053 WITH p_ver4 DISPLAY LIKE 'E'.
*        gv_error = abap_true.
*        EXIT.
      ELSE.
        ls_version-fan     = ls_fan_ver_stat-fan.
        ls_version-idno    = ls_fan_ver_stat-idno.
        ls_version-matnr   = ls_mara-matnr.
        ls_version-vno     = lv_vno.
        ls_version-version = p_ver4.
        INSERT ls_version INTO TABLE gt_version[].
        lv_vno = lv_vno + 1.
      ENDIF.
    ENDIF.


    IF p_ver5 IS NOT INITIAL.
      CLEAR ls_version.
      READ TABLE gt_version[] INTO ls_version
      WITH KEY version = p_ver5.
      IF sy-subrc = 0.
** Only Distinct Versions can be compared : &
*        MESSAGE s053 WITH p_ver5 DISPLAY LIKE 'E'.
*        gv_error = abap_true.
*        EXIT.
      ELSE.
        ls_version-fan     = ls_fan_ver_stat-fan.
        ls_version-idno    = ls_fan_ver_stat-idno.
        ls_version-matnr   = ls_mara-matnr.
        ls_version-vno     = lv_vno.
        ls_version-version = p_ver5.
        INSERT ls_version INTO TABLE gt_version[].
        lv_vno = lv_vno + 1.
      ENDIF.
    ENDIF.


* Get product version data
    CLEAR gt_prd_version[].
    SELECT idno version version_status release_status
           changed_on changed_at changed_by
      FROM zarn_prd_version
      INTO CORRESPONDING FIELDS OF TABLE gt_prd_version[]
      WHERE idno = ls_fan_ver_stat-idno.
    IF sy-subrc = 0.

      LOOP AT gt_version[] ASSIGNING <ls_version> WHERE version IS NOT INITIAL.

        CLEAR ls_prd_version.
        READ TABLE gt_prd_version[] INTO ls_prd_version
        WITH KEY idno    = ls_fan_ver_stat-idno
                 version = <ls_version>-version.
        IF sy-subrc NE 0.
* Version & does not exist for FAN ID &
          MESSAGE s054 WITH <ls_version>-version p_fan DISPLAY LIKE 'E'.
          gv_error = abap_true.
          EXIT.
        ELSE.
          <ls_version>-status = ls_prd_version-version_status.
        ENDIF.


      ENDLOOP.  " LOOP AT gt_version[] INTO ls_version

    ENDIF.  " if prd_version found

  ENDIF.  " if fan_ver_stat found


ENDFORM.                    " VALIDATE_SINGLE_FAN_VERSIONS
*&---------------------------------------------------------------------*
*&      Form  GET_INITIAL_DATA_MULTIPLE
*&---------------------------------------------------------------------*
* Get Initial Data for Multiple FAN
*----------------------------------------------------------------------*
FORM get_initial_data_multiple.


  CLEAR : gt_fan_ver_stat[].

* Multiple Article
  SELECT a~idno a~previous_ver a~current_ver a~latest_ver a~article_ver
         a~changed_on a~changed_at a~changed_by
         b~fan_id  AS fan
    INTO CORRESPONDING FIELDS OF TABLE gt_fan_ver_stat[]
    FROM zarn_ver_status AS a
    JOIN zarn_products AS b
    ON a~idno = b~idno
    WHERE b~fan_id IN s_fan.

  SORT gt_fan_ver_stat[].
  DELETE ADJACENT DUPLICATES FROM gt_fan_ver_stat[] COMPARING ALL FIELDS.

* FAN ID does not exist
  IF gt_fan_ver_stat[] IS INITIAL.
    MESSAGE s052 WITH p_fan DISPLAY LIKE 'E'.
    gv_error = abap_true.
    EXIT.
  ENDIF.


  "  * Multiple records found for FAN ID &



* Get MARA
  CLEAR: gt_mara[].
  SELECT *
    FROM mara
    INTO CORRESPONDING FIELDS OF TABLE gt_mara[]
    WHERE zzfan IN s_fan[].

* Get product version data
  CLEAR gt_prd_version[].
  SELECT idno version version_status release_status
         changed_on changed_at changed_by
    FROM zarn_prd_version
    INTO CORRESPONDING FIELDS OF TABLE gt_prd_version[]
    FOR ALL ENTRIES IN gt_fan_ver_stat[]
    WHERE idno = gt_fan_ver_stat-idno.

ENDFORM.                    " GET_INITIAL_DATA_MULTIPLE
*&---------------------------------------------------------------------*
*&      Form  VALIDATE_MULTIPLE_FAN_VERSIONS
*&---------------------------------------------------------------------*
* Validate Multiple FAN ID Versions
*----------------------------------------------------------------------*
FORM validate_multiple_fan_versions.

  DATA : ls_version      TYPE ty_s_ver_screen,
         ls_prd_version  TYPE zarn_prd_version,
         ls_fan_ver_stat TYPE ty_s_fan_ver_stat,
         ls_mara         TYPE ty_s_mara,
         lv_vno          TYPE i,
         lv_version      TYPE zarn_article_ver.

  CLEAR gt_version[].


  LOOP AT gt_fan_ver_stat[] INTO ls_fan_ver_stat.

*    IF ls_fan_ver_stat-article_ver = ls_fan_ver_stat-current_ver.
*
***** Only Distinct Versions can be compared : &
****        MESSAGE s053 WITH p_ver1 DISPLAY LIKE 'E'.
****        gv_error = abap_true.
*      CONTINUE.
*
*    ELSE.

    lv_vno = 1.

    CLEAR ls_mara.
    READ TABLE gt_mara[] INTO ls_mara
    WITH KEY zzfan = ls_fan_ver_stat-fan.


    CLEAR lv_version.

    IF ls_fan_ver_stat-article_ver IS INITIAL.
      IF ls_fan_ver_stat-previous_ver IS INITIAL.
        lv_version = ls_fan_ver_stat-current_ver.
      ELSE.
        lv_version = ls_fan_ver_stat-previous_ver.
      ENDIF.
    ELSE.
      lv_version = ls_fan_ver_stat-article_ver.
    ENDIF.



    CLEAR ls_prd_version.
    READ TABLE gt_prd_version[] INTO ls_prd_version
    WITH KEY idno    = ls_fan_ver_stat-idno
             version = lv_version.

    ls_version-fan     = ls_fan_ver_stat-fan.
    ls_version-idno    = ls_fan_ver_stat-idno.
    ls_version-matnr   = ls_mara-matnr.
    ls_version-vno     = lv_vno.
    ls_version-version = lv_version.
    ls_version-status  = ls_prd_version-version_status.
    INSERT ls_version INTO TABLE gt_version[].
    lv_vno = lv_vno + 1.



    CLEAR ls_prd_version.
    READ TABLE gt_prd_version[] INTO ls_prd_version
    WITH KEY idno    = ls_fan_ver_stat-idno
             version = ls_fan_ver_stat-current_ver.

    ls_version-fan     = ls_fan_ver_stat-fan.
    ls_version-idno    = ls_fan_ver_stat-idno.
    ls_version-matnr   = ls_mara-matnr.
    ls_version-vno     = lv_vno.
    ls_version-version = ls_fan_ver_stat-current_ver.
    ls_version-status  = ls_prd_version-version_status.
    INSERT ls_version INTO TABLE gt_version[].
    lv_vno = lv_vno + 1.


*    ENDIF.



  ENDLOOP.  " LOOP AT gt_fan_ver_stat[] INTO ls_fan_ver_stat



ENDFORM.                    " VALIDATE_MULTIPLE_FAN_VERSIONS
*&---------------------------------------------------------------------*
*&      Form  GET_INITIAL_DATA
*&---------------------------------------------------------------------*
* Get Initial Data
*----------------------------------------------------------------------*
FORM get_initial_data .

  DATA : ls_table_mastr TYPE zarn_table_mastr,

         ls_version     TYPE ty_s_ver_screen,
         lt_key         TYPE ztarn_key,
         ls_key         TYPE zsarn_key,
         lt_reg_key     TYPE ztarn_reg_key,
         ls_reg_key     TYPE zsarn_reg_key.

* Get Table Master Data
  CLEAR: gt_table_mastr[].
  SELECT * FROM zarn_table_mastr
    INTO CORRESPONDING FIELDS OF TABLE gt_table_mastr[]
    WHERE arena_table      IN s_chgtab[]
      AND arena_table_type IN s_chgara[].

  IF gt_table_mastr[] IS NOT INITIAL.
    CLEAR: gt_field_confg[].
    SELECT * FROM zarn_field_confg
      INTO CORRESPONDING FIELDS OF TABLE gt_field_confg[]
      FOR ALL ENTRIES IN gt_table_mastr[]
      WHERE tabname = gt_table_mastr-arena_table.
  ENDIF.

* Check National exist
  CLEAR : ls_table_mastr, gv_national.
  READ TABLE gt_table_mastr[] INTO ls_table_mastr
  WITH KEY arena_table_type = gc_arn_tab_typ-nat.  " 'N' National Table
  IF sy-subrc = 0.
    gv_national = abap_true.
  ENDIF.


* Check Regional exist
  CLEAR : ls_table_mastr, gv_regional.
  READ TABLE gt_table_mastr[] INTO ls_table_mastr
  WITH KEY arena_table_type = gc_arn_tab_typ-reg.  " 'R' Regional Table
  IF sy-subrc = 0.
    gv_regional = abap_true.
  ENDIF.





  CLEAR: lt_key[], lt_reg_key[].
  LOOP AT gt_version[] INTO ls_version.

* Build National Key
    CLEAR ls_key.
    ls_key-idno    = ls_version-idno.
    ls_key-version = ls_version-version.
    APPEND ls_key TO lt_key[].

* Build Regiponal Key
    CLEAR ls_reg_key.
    ls_reg_key-idno = ls_version-idno.
    APPEND ls_reg_key TO lt_reg_key[].
  ENDLOOP.


* Get National Data
  CLEAR gt_prod_data[].
  CALL FUNCTION 'ZARN_READ_NATIONAL_DATA'
    EXPORTING
      it_key  = lt_key[]
    IMPORTING
      et_data = gt_prod_data[].


* Get Regional Data
  CLEAR gt_reg_data[].
  CALL FUNCTION 'ZARN_READ_REGIONAL_DATA'
    EXPORTING
      it_key  = lt_reg_key[]
    IMPORTING
      et_data = gt_reg_data[].


  IF gt_mara[] IS NOT INITIAL.
    CLEAR gt_marm[].
    SELECT matnr meinh laeng breit hoehe meabm brgew gewei mesub
      FROM marm
      INTO CORRESPONDING FIELDS OF TABLE gt_marm[]
     FOR ALL ENTRIES IN gt_mara[]
      WHERE matnr = gt_mara-matnr.

    " get SOS records for sites
    SELECT matnr werks bwscl zzonline_status zz_pbs
      FROM marc INTO TABLE gt_marc_sos
      FOR ALL ENTRIES IN gt_mara[]
      WHERE matnr = gt_mara-matnr.

    SELECT * FROM mean INTO TABLE gt_mean
      FOR ALL ENTRIES IN gt_mara[]
      WHERE matnr = gt_mara-matnr.

    SELECT matnr wvrkm wausm FROM maw1 INTO TABLE gt_maw1
     FOR ALL ENTRIES IN gt_mara[]
     WHERE matnr = gt_mara-matnr.

    SELECT * FROM eina INTO TABLE gt_eina
      FOR ALL ENTRIES IN gt_mara[]
      WHERE matnr = gt_mara-matnr.
    IF sy-subrc EQ 0.
      SELECT aplfz bprme ekgrp ekkol esokz infnr loekz minbm
           mwskz netpr norbm peinh prdat uebto untto waers
        FROM eine INTO CORRESPONDING FIELDS OF TABLE gt_eine
      FOR ALL ENTRIES IN gt_eina
      WHERE infnr = gt_eina-infnr
      AND ekorg EQ '9999'
      AND werks EQ ''.
    ENDIF.


  ENDIF.


ENDFORM.                    " GET_INITIAL_DATA
*&---------------------------------------------------------------------*
*&      Form  BUILD_NATIONAL_DATA
*&---------------------------------------------------------------------*
* Build National Data
*----------------------------------------------------------------------*
FORM build_national_data.

  DATA: lv_lines   TYPE i,
        ls_version TYPE ty_s_ver_screen.

  CLEAR lv_lines.
  DESCRIBE TABLE gt_version[] LINES lv_lines.
  IF lv_lines LT 2.
    EXIT.
  ENDIF.


* Get National Data
  PERFORM get_national_data.


* Build ALV Data - National
  PERFORM build_alv_data_national.


ENDFORM.                    " BUILD_NATIONAL_DATA
*&---------------------------------------------------------------------*
*&      Form  GET_NATIONAL_DATA
*&---------------------------------------------------------------------*
* Get National Data
*----------------------------------------------------------------------*
FORM get_national_data.


  DATA: ls_base        TYPE ty_s_ver_screen,
        ls_version     TYPE ty_s_ver_screen,
        lt_table_mastr TYPE ty_t_table_mastr,

        ls_prod_data_b TYPE zsarn_prod_data,
        ls_prod_data_v TYPE zsarn_prod_data,
        ls_reg_data    TYPE zsarn_reg_data,
        lt_cc_det      TYPE ztarn_cc_det,
        ls_cc_det      TYPE zarn_cc_det,
        ls_cc_fields   TYPE ty_s_cc_fields,


        lv_chgid       TYPE zarn_chg_id.



  FIELD-SYMBOLS: <ls_version>        TYPE ty_s_ver_screen.


  lt_table_mastr[] = gt_table_mastr[].
  DELETE lt_table_mastr[] WHERE arena_table_type NE gc_arn_tab_typ-nat.  " 'N' National Table


  IF <ls_version> IS ASSIGNED.
    UNASSIGN <ls_version>.
  ENDIF.






  CLEAR : gt_cc_fields[], gt_cc_det_nat[].

  LOOP AT gt_version[] ASSIGNING <ls_version>." WHERE vno GT 1.

* Get Base Version
    CLEAR ls_base.
    READ TABLE gt_version[] INTO ls_base
    WITH KEY idno = <ls_version>-idno
             vno  = 1.

* Get Regional Data
    CLEAR ls_reg_data.
    READ TABLE gt_reg_data[] INTO ls_reg_data
    WITH KEY idno    = <ls_version>-idno.
    IF sy-subrc = 0.
      <ls_version>-reg_data = ls_reg_data.
    ENDIF.


* Get Base Version National Data to compare
    CLEAR ls_prod_data_b.
    READ TABLE gt_prod_data[] INTO ls_prod_data_b
    WITH KEY idno    = ls_base-idno
             version = ls_base-version.

    IF <ls_version>-vno = 1.

      <ls_version>-prod_data_b = ls_prod_data_b.

    ELSE.
* Get National Data to compare for Version
      CLEAR ls_prod_data_v.
      READ TABLE gt_prod_data[] INTO ls_prod_data_v
      WITH KEY idno    = <ls_version>-idno
               version = <ls_version>-version.



      lv_chgid        = <ls_version>-vno.

      CLEAR: lt_cc_det[].
* Build Change Category Data
      CALL FUNCTION 'ZARN_CC_BUILD_NATIONAL_DATA'
        EXPORTING
          it_table_mastr  = lt_table_mastr[]
          is_prod_data_db = ls_prod_data_b
          is_prod_data_n  = ls_prod_data_v
          iv_chgid        = lv_chgid
        IMPORTING
          et_cc_det       = lt_cc_det[].

      DELETE lt_cc_det[] WHERE chg_field = 'MANDT'.
      DELETE lt_cc_det[] WHERE chg_field = 'IDNO'.
      DELETE lt_cc_det[] WHERE chg_field = 'VERSION'.

      <ls_version>-prod_data_b = ls_prod_data_b.
      <ls_version>-prod_data_v = ls_prod_data_v.
      <ls_version>-cc_det      = lt_cc_det[].

      APPEND LINES OF lt_cc_det[] TO gt_cc_det_nat[].

      LOOP AT lt_cc_det[] INTO ls_cc_det.
        CLEAR ls_cc_fields.

        ls_cc_fields-chgid        = ls_cc_det-chgid.
        ls_cc_fields-idno         = ls_cc_det-idno.
        ls_cc_fields-chg_category = ls_cc_det-chg_category.
        ls_cc_fields-chg_table    = ls_cc_det-chg_table.
        ls_cc_fields-chg_field    = ls_cc_det-chg_field.
        ls_cc_fields-version      = ls_cc_det-national_ver_id.
        ls_cc_fields-chg_area     = ls_cc_det-chg_area.
        ls_cc_fields-chg_ind      = ls_cc_det-chg_ind.
        ls_cc_fields-ref_data     = ls_cc_det-reference_data.

        IF ls_cc_det-chg_ind     = 'U'.
          ls_cc_fields-value = ls_cc_det-value_new.
        ELSEIF ls_cc_det-chg_ind = 'I'.
          ls_cc_fields-value = ls_cc_det-value_new.
        ELSEIF ls_cc_det-chg_ind = 'D'.
          ls_cc_fields-value = ls_cc_det-value_old.
        ENDIF.

        CONDENSE ls_cc_fields-value.

        APPEND ls_cc_fields TO gt_cc_fields[].

      ENDLOOP.  " LOOP AT lt_cc_det[] INTO ls_cc_det

    ENDIF.  " IF <ls_version>-vno = 1

  ENDLOOP.  " LOOP AT gt_version[] ASSIGNING <ls_version>


  DELETE gt_cc_fields[] WHERE chg_field = 'MANDT'.
  DELETE gt_cc_fields[] WHERE chg_field = 'IDNO'.
  DELETE gt_cc_fields[] WHERE chg_field = 'VERSION'.

  SORT gt_cc_fields[] BY idno chg_category chg_table chg_field ref_data.
  DELETE ADJACENT DUPLICATES FROM gt_cc_fields[] COMPARING idno chg_category chg_table chg_field ref_data.

  SORT gt_cc_det_nat[] BY chgid idno chg_table chg_field.



ENDFORM.                    " GET_NATIONAL_DATA
*&---------------------------------------------------------------------*
*&      Form  BUILD_ALV_DATA_NATIONAL
*&---------------------------------------------------------------------*
* Build ALV Data - National
*----------------------------------------------------------------------*
FORM build_alv_data_national .


  DATA: ls_fan_ver_stat TYPE ty_s_fan_ver_stat,
        ls_base         TYPE ty_s_ver_screen,
        ls_version      TYPE ty_s_ver_screen,
        ls_prd_version  TYPE zarn_prd_version,
        ls_field_confg  TYPE zarn_field_confg,
        ls_cc_det       TYPE zarn_cc_det,
        ls_cc_fields    TYPE ty_s_cc_fields,
        ls_cc_det_nat   TYPE zarn_cc_det,
        lt_dfies        TYPE STANDARD TABLE OF dfies,
        ls_dfies        TYPE dfies,
        lv_exist        TYPE flag,
        lv_value        TYPE zarn_reference_value,


        ls_delta_alv    TYPE zsarn_delta_analysis_alv,
        lv_lines        TYPE i.

  CLEAR lv_lines.
  DESCRIBE TABLE gt_version[] LINES lv_lines.
  IF lv_lines LT 2.
    EXIT.
  ENDIF.


  LOOP AT gt_fan_ver_stat[] INTO ls_fan_ver_stat.

    LOOP AT gt_cc_fields[] INTO ls_cc_fields
      WHERE idno = ls_fan_ver_stat-idno.

      CLEAR ls_cc_det_nat.
      LOOP AT gt_cc_det_nat[] INTO ls_cc_det_nat
      WHERE chgid          = ls_cc_fields-chgid
        AND idno           = ls_cc_fields-idno
        AND chg_category   = ls_cc_fields-chg_category
        AND chg_table      = ls_cc_fields-chg_table
        AND chg_field      = ls_cc_fields-chg_field
        AND ( ( chg_ind = 'D' AND  value_old = ls_cc_fields-value ) OR
              ( ( chg_ind = 'I' OR chg_ind = 'U' ) AND value_new = ls_cc_fields-value ) )
        AND reference_data = ls_cc_fields-ref_data.

        EXIT.
      ENDLOOP.

      CLEAR ls_delta_alv.

      ls_delta_alv-fan_id          = ls_fan_ver_stat-fan.
      ls_delta_alv-chg_area        = ls_cc_fields-chg_area.
      ls_delta_alv-chg_category    = ls_cc_fields-chg_category.
      ls_delta_alv-chg_table       = ls_cc_fields-chg_table.
      ls_delta_alv-chg_field       = ls_cc_fields-chg_field.
      ls_delta_alv-reference_data  = ls_cc_fields-ref_data.

*      IF ls_cc_fields-ref_data NE ls_cc_fields-value.
*        ls_delta_alv-reference_value = ls_cc_fields-value.
*      ENDIF.

* If Multiple Articles are compared then only fill change indicator
      IF lv_lines EQ 2 OR p_multi = abap_true.
        ls_delta_alv-chg_ind = ls_cc_fields-chg_ind.
      ENDIF.

* Data for Version 1
      CLEAR ls_base.
      READ TABLE gt_version INTO ls_base
      WITH TABLE KEY fan = ls_fan_ver_stat-fan
                     vno = 1.
      IF sy-subrc = 0.
        ls_delta_alv-matnr    = ls_base-matnr.
        ls_delta_alv-version1 = ls_base-version.
        ls_delta_alv-status1  = ls_base-status.

* Get Value for Version 1
        CLEAR: lv_exist, lv_value.
        PERFORM get_tab_fld_value_nat USING ls_fan_ver_stat-idno
                                            ls_base-version
                                            ls_cc_fields-chg_table
                                            ls_cc_fields-chg_field
                                            ls_cc_fields-value
                                            ls_cc_fields-ref_data
                                            ls_base-prod_data_b
                                   CHANGING lv_exist
                                            lv_value.

        IF lv_exist = abap_true.
          ls_delta_alv-value1   = lv_value.
        ENDIF.

      ENDIF.  " Check Version 1



* Fill data for Version 2
      CLEAR ls_version.
      READ TABLE gt_version INTO ls_version
      WITH TABLE KEY fan = ls_fan_ver_stat-fan
                     vno = 2.
      IF sy-subrc = 0.

        ls_delta_alv-version2 = ls_version-version.
        ls_delta_alv-status2  = ls_version-status.

* Get Value for Version 2
        CLEAR: lv_exist, lv_value.
        PERFORM get_tab_fld_value_nat USING ls_fan_ver_stat-idno
                                            ls_version-version
                                            ls_cc_fields-chg_table
                                            ls_cc_fields-chg_field
                                            ls_cc_fields-value
                                            ls_cc_fields-ref_data
                                            ls_version-prod_data_v
                                   CHANGING lv_exist
                                            lv_value.

        IF lv_exist = abap_true.
          ls_delta_alv-value2   = lv_value.
        ENDIF.



      ENDIF.  " Check Version 2


* Fill data for Version 3
      CLEAR ls_version.
      READ TABLE gt_version INTO ls_version
      WITH TABLE KEY fan = ls_fan_ver_stat-fan
                     vno = 3.
      IF sy-subrc = 0.

        ls_delta_alv-version3 = ls_version-version.
        ls_delta_alv-status3  = ls_version-status.

* Get Value for Version 3
        CLEAR: lv_exist, lv_value.
        PERFORM get_tab_fld_value_nat USING ls_fan_ver_stat-idno
                                            ls_version-version
                                            ls_cc_fields-chg_table
                                            ls_cc_fields-chg_field
                                            ls_cc_fields-value
                                            ls_cc_fields-ref_data
                                            ls_version-prod_data_v
                                   CHANGING lv_exist
                                            lv_value.

        IF lv_exist = abap_true.
          ls_delta_alv-value3   = lv_value.
        ENDIF.

      ENDIF.  " Check Version 3

* Fill data for Version 4
      CLEAR ls_version.
      READ TABLE gt_version INTO ls_version
      WITH TABLE KEY fan = ls_fan_ver_stat-fan
                     vno = 4.
      IF sy-subrc = 0.

        ls_delta_alv-version4 = ls_version-version.
        ls_delta_alv-status4  = ls_version-status.

* Get Value for Version 4
        CLEAR: lv_exist, lv_value.
        PERFORM get_tab_fld_value_nat USING ls_fan_ver_stat-idno
                                            ls_version-version
                                            ls_cc_fields-chg_table
                                            ls_cc_fields-chg_field
                                            ls_cc_fields-value
                                            ls_cc_fields-ref_data
                                            ls_version-prod_data_v
                                   CHANGING lv_exist
                                            lv_value.

        IF lv_exist = abap_true.
          ls_delta_alv-value4   = lv_value.
        ENDIF.

      ENDIF.  " Check Version 4

* Fill data for Version 5
      CLEAR ls_version.
      READ TABLE gt_version INTO ls_version
      WITH TABLE KEY fan = ls_fan_ver_stat-fan
                     vno = 5.
      IF sy-subrc = 0.

        ls_delta_alv-version5 = ls_version-version.
        ls_delta_alv-status5  = ls_version-status.

* Get Value for Version 5
        CLEAR: lv_exist, lv_value.
        PERFORM get_tab_fld_value_nat USING ls_fan_ver_stat-idno
                                            ls_version-version
                                            ls_cc_fields-chg_table
                                            ls_cc_fields-chg_field
                                            ls_cc_fields-value
                                            ls_cc_fields-ref_data
                                            ls_version-prod_data_v
                                   CHANGING lv_exist
                                            lv_value.

        IF lv_exist = abap_true.
          ls_delta_alv-value5   = lv_value.
        ENDIF.

      ENDIF.  " Check Version 5




      ls_delta_alv-field_desc = ls_delta_alv-chg_field.

      IF ls_delta_alv-chg_table IS NOT INITIAL AND
         ls_delta_alv-chg_field IS NOT INITIAL.
* Get field Description
        REFRESH lt_dfies[].
        CALL FUNCTION 'DDIF_FIELDINFO_GET'
          EXPORTING
            tabname        = ls_delta_alv-chg_table
            fieldname      = ls_delta_alv-chg_field
            langu          = sy-langu
          TABLES
            dfies_tab      = lt_dfies
          EXCEPTIONS
            not_found      = 1
            internal_error = 2
            OTHERS         = 3.
        IF sy-subrc = 0.
          CLEAR ls_dfies.
          READ TABLE lt_dfies[] INTO ls_dfies INDEX 1.
          IF sy-subrc = 0.
            ls_delta_alv-field_desc = ls_dfies-fieldtext.
          ENDIF.
        ENDIF.
      ENDIF.

* Article Update?? only if SAP_TABLE SAP_FIELD is available in field config
      CLEAR ls_field_confg.
      READ TABLE gt_field_confg[] INTO ls_field_confg
      WITH KEY tabname = ls_cc_fields-chg_table
               fldname = ls_cc_fields-chg_field.
      IF sy-subrc = 0.
        IF ls_field_confg-sap_table IS NOT INITIAL AND
           ls_field_confg-sap_field IS NOT INITIAL.

          ls_delta_alv-art_upd = 'Y'.


          IF ls_cc_det_nat IS NOT INITIAL.
* Get Ref Data and Ref Value for National
            PERFORM get_ref_data_value_national USING ls_base
                                                      ls_field_confg-sap_table
                                                      ls_field_confg-sap_field
                                                      ls_cc_fields
                                                      ls_cc_det_nat
                                             CHANGING ls_delta_alv-ref_data
                                                      ls_delta_alv-ref_value
                                                      ls_delta_alv-art_upd
                                                      ls_delta_alv-reference_value.
          ENDIF.

        ENDIF.
      ENDIF.








      APPEND ls_delta_alv TO gt_delta_alv[].

    ENDLOOP.  " LOOP AT gt_cc_fields[] INTO ls_cc_fields

  ENDLOOP.  " LOOP AT gt_fan_ver_stat[] INTO ls_fan_ver_stat


  SORT gt_delta_alv[] BY fan_id chg_table chg_field.

ENDFORM.                    " BUILD_ALV_DATA_NATIONAL
*&---------------------------------------------------------------------*
*&      Form  GET_TAB_FLD_VALUE_NAT
*&---------------------------------------------------------------------*
* Get Value for Version - National
*----------------------------------------------------------------------*
FORM get_tab_fld_value_nat USING fu_v_idno      TYPE zarn_idno
                                 fu_v_version   TYPE zarn_version
                                 fu_v_table     TYPE zarn_chg_table
                                 fu_v_field     TYPE zarn_chg_field
                                 fu_v_value     TYPE cdfldvaln
                                 fu_v_ref_data  TYPE zarn_reference_data
                                 fu_s_prod_data TYPE zsarn_prod_data
                        CHANGING fc_v_exist     TYPE flag
                                 fc_v_value     TYPE zarn_reference_value.


  DATA : lv_table    TYPE char100,
         lv_table_wa TYPE char30,
         lv_key      TYPE string,
         lt_dfies    TYPE STANDARD TABLE OF dfies,
         ls_dfies    TYPE dfies,

         lt_line     TYPE STANDARD TABLE OF string,
         ls_line     TYPE string,
         lv_tabix    TYPE sy-tabix.

  FIELD-SYMBOLS: <table>    TYPE table,
                 <table_wa> TYPE any,
                 <value>    TYPE any.

  CLEAR: fc_v_exist, fc_v_value.

  CLEAR lv_table.
  CONCATENATE 'FU_S_PROD_DATA-' fu_v_table '[]' INTO lv_table.


* Get field Description
  REFRESH lt_dfies[].
  CALL FUNCTION 'DDIF_FIELDINFO_GET'
    EXPORTING
      tabname        = fu_v_table
      langu          = sy-langu
    TABLES
      dfies_tab      = lt_dfies[]
    EXCEPTIONS
      not_found      = 1
      internal_error = 2
      OTHERS         = 3.

  DELETE lt_dfies[] WHERE keyflag NE abap_true.
  DELETE lt_dfies[] WHERE fieldname = 'MANDT'.
  DELETE lt_dfies[] WHERE fieldname = 'IDNO'.
  DELETE lt_dfies[] WHERE fieldname = 'VERSION'.

  CLEAR lv_key.
  CONCATENATE 'IDNO' '=' '''' INTO lv_key SEPARATED BY space.
  CONCATENATE lv_key fu_v_idno ''''  INTO lv_key.

  CONCATENATE lv_key 'AND' 'VERSION' '=' fu_v_version INTO lv_key SEPARATED BY space.

  IF <table>    IS ASSIGNED. UNASSIGN <table>.    ENDIF.
  IF <table_wa> IS ASSIGNED. UNASSIGN <table_wa>. ENDIF.
  IF <value>    IS ASSIGNED. UNASSIGN <value>.    ENDIF.

  IF lt_dfies[] IS INITIAL.

    ASSIGN (lv_table) TO <table>.

    LOOP AT <table> ASSIGNING <table_wa> WHERE (lv_key).
      fc_v_exist = abap_true.

      IF fu_v_field = 'KEY'.
        fc_v_value = fu_v_ref_data.
      ELSE.
        ASSIGN COMPONENT fu_v_field OF STRUCTURE <table_wa> TO <value>.
        fc_v_value = <value>.
      ENDIF.

      CONDENSE fc_v_value.
      EXIT.
    ENDLOOP.

  ELSE.  " IF lt_dfies[] IS INITIAL.

    IF fu_v_ref_data IS INITIAL.
      EXIT.
    ENDIF.

    CLEAR lt_line[].
    SPLIT fu_v_ref_data AT '/' INTO TABLE lt_line[].


    LOOP AT lt_dfies INTO ls_dfies.
      lv_tabix = sy-tabix.

      CLEAR ls_line.
      READ TABLE lt_line[] INTO ls_line INDEX lv_tabix.
      IF sy-subrc = 0.
        CONDENSE ls_line.
        CONCATENATE lv_key 'AND' ls_dfies-fieldname '=' '''' INTO lv_key SEPARATED BY space.
        CONCATENATE lv_key ls_line ''''  INTO lv_key.
      ENDIF.
    ENDLOOP.  " LOOP AT lt_dfies INTO ls_dfies.


    ASSIGN (lv_table) TO <table>.

    LOOP AT <table> ASSIGNING <table_wa> WHERE (lv_key).
      fc_v_exist = abap_true.

      IF fu_v_field = 'KEY'.
        fc_v_value = fu_v_ref_data.
      ELSE.
        ASSIGN COMPONENT fu_v_field OF STRUCTURE <table_wa> TO <value>.
        fc_v_value = <value>.
      ENDIF.

      CONDENSE fc_v_value.
      EXIT.
    ENDLOOP.

  ENDIF.  " IF lt_dfies[] IS INITIAL.


  IF <table>    IS ASSIGNED. UNASSIGN <table>.    ENDIF.
  IF <table_wa> IS ASSIGNED. UNASSIGN <table_wa>. ENDIF.
  IF <value>    IS ASSIGNED. UNASSIGN <value>.    ENDIF.

ENDFORM.                    " GET_TAB_FLD_VALUE_NAT
*&---------------------------------------------------------------------*
*&      Form  GET_REF_DATA_VALUE_NATIONAL
*&---------------------------------------------------------------------*
* Get Ref Data and Ref Value for National
*----------------------------------------------------------------------*
FORM get_ref_data_value_national USING fu_s_version    TYPE ty_s_ver_screen
                                       fu_v_sap_table  TYPE zarn_sap_table
                                       fu_v_sap_field  TYPE zarn_sap_field
                                       fu_s_cc_fields  TYPE ty_s_cc_fields
                                       fu_s_cc_det_nat TYPE zarn_cc_det
                              CHANGING fc_v_ref_data   TYPE zarn_ref_data
                                       fc_v_ref_value  TYPE zarn_ref_value
                                       fc_v_art_upd    TYPE zarn_art_upd
                                       fc_v_value_ref  TYPE zarn_reference_value.

  DATA: ls_mara       TYPE ty_s_mara,
        ls_marm       TYPE ty_s_marm,
        ls_maw1       TYPE maw1,
        ls_ntr_claims TYPE zarn_ntr_claims,
        ls_reg_uom    TYPE zarn_reg_uom,
        ls_pir        TYPE zarn_pir,
        ls_reg_pir    TYPE zarn_reg_pir,
        ls_eina       TYPE eina,
        ls_gtin       TYPE zarn_gtin_var,
        ls_mean       TYPE mean,
        ls_eine       TYPE eine,



        lv_mara       TYPE char50,
        lv_value      TYPE cdfldvaln,
        lv_lines      TYPE i.



  FIELD-SYMBOLS: <ls_mara>  TYPE any.


  CLEAR lv_value.
  IF fu_s_cc_det_nat-chg_ind = 'D'.
    lv_value = fu_s_cc_det_nat-value_old.
  ELSE.
    lv_value = fu_s_cc_det_nat-value_new.
  ENDIF.

  CONDENSE lv_value.


  CLEAR ls_mara.
  READ TABLE gt_mara[] INTO ls_mara
  WITH KEY matnr = fu_s_version-matnr.

  CLEAR ls_maw1.
  READ TABLE gt_maw1[] INTO ls_maw1
  WITH KEY matnr = fu_s_version-matnr.

  CLEAR: fc_v_ref_data, fc_v_ref_value, fc_v_value_ref.

  TRANSLATE lv_value TO UPPER CASE.

  CASE fu_s_cc_fields-chg_table.
    WHEN 'ZARN_PRODUCTS'.
      CASE fu_s_cc_fields-chg_field.
        WHEN 'NET_QUANTITY_UOM'.
          fc_v_ref_value = ls_mara-inhme.
        WHEN 'NET_QUANTITY'.
          fc_v_ref_value = ls_mara-inhal.
        WHEN 'FAN_ID'.
          fc_v_ref_value = ls_mara-zzfan.
        WHEN 'DGI_UNN_CODE'.
          fc_v_ref_value = ls_mara-zzunnum.
        WHEN 'DGI_PACKING_GROUP'.
          fc_v_ref_value = ls_mara-zzpackgrp.
        WHEN 'DGI_CLASS'.
          fc_v_ref_value = ls_mara-zzhcls.
        WHEN 'DGI_HAZARDOUS_CODE'.
          fc_v_ref_value = ls_mara-zzhsno.
        WHEN 'GENETICALLY_MODIFIED'.
          fc_v_ref_value = ls_mara-zzattr1.
        WHEN 'IRRADIATED'.
          fc_v_ref_value = ls_mara-zzattr3.
        WHEN 'HAZARDOUS_GOOD'.
          fc_v_ref_value = ls_mara-zzhard.
        WHEN 'EXPIRATION_DATE_TYPE'.
          IF lv_value = 'BEST_BEFORE_DATE'.
            fc_v_ref_value = ls_mara-zzbbdf.
          ELSE.
            CLEAR fc_v_art_upd.
          ENDIF.

          IF fu_s_cc_fields-ref_data NE lv_value.
            fc_v_value_ref = lv_value.
          ENDIF.

        WHEN OTHERS.
          CLEAR: fc_v_ref_data, fc_v_ref_value.
      ENDCASE.





    WHEN 'ZARN_DIET_INFO'.
      IF fu_s_cc_fields-chg_field = 'DIET_TYPE' OR fu_s_cc_fields-chg_field = 'KEY'.
        CASE lv_value.
          WHEN 'KOSHER'.
            fc_v_ref_value = ls_mara-zzattr4.
          WHEN 'HALAL'.
            fc_v_ref_value = ls_mara-zzattr9.
          WHEN 'VEGAN'.
            fc_v_ref_value = ls_mara-zzattr14.
          WHEN 'VEGETARIAN'.
            fc_v_ref_value = ls_mara-zzattr23.
          WHEN 'FREE_FROM_GLUTEN'.
            fc_v_ref_value = ls_mara-zzattr11.
          WHEN OTHERS.
            CLEAR: fc_v_ref_data, fc_v_ref_value.
            CLEAR fc_v_art_upd.
        ENDCASE.
      ENDIF.


    WHEN 'ZARN_GROWING'.
      IF fu_s_cc_fields-chg_field = 'GROWING_METHOD' OR fu_s_cc_fields-chg_field = 'KEY'.
        CASE lv_value.
          WHEN 'ORGANIC'.
            fc_v_ref_value = ls_mara-zzattr2.
          WHEN 'FREE_RANGE'.
            fc_v_ref_value = ls_mara-zzattr10.
          WHEN OTHERS.
            CLEAR: fc_v_ref_data, fc_v_ref_value.
            CLEAR fc_v_art_upd.
        ENDCASE.
      ENDIF.

    WHEN 'ZARN_ALLERGEN'.
      fc_v_ref_value = ls_mara-zzattr5.


    WHEN 'ZARN_NTR_CLAIMS'.
      IF fu_s_cc_fields-chg_field = 'NUTRITIONAL_CLAIMS'      OR
         fu_s_cc_fields-chg_field = 'NUTRITIONAL_CLAIMS_ELEM' OR
         fu_s_cc_fields-chg_field = 'KEY' .

        CLEAR ls_ntr_claims.
        SPLIT fu_s_cc_fields-ref_data AT '/' INTO ls_ntr_claims-nutritional_claims ls_ntr_claims-nutritional_claims_elem.


        IF ls_ntr_claims-nutritional_claims_elem CS '/'.
          REPLACE '/' IN ls_ntr_claims-nutritional_claims_elem WITH ''.
        ENDIF.

        IF ls_ntr_claims-nutritional_claims      = 'LOW' AND
           ls_ntr_claims-nutritional_claims_elem = 'FAT'.
          fc_v_ref_value = ls_mara-zzattr7.
        ELSEIF ls_ntr_claims-nutritional_claims      CS 'FREE' AND
               ls_ntr_claims-nutritional_claims_elem CP 'SUGAR*'.
          fc_v_ref_value = ls_mara-zzattr13.
        ELSEIF ls_ntr_claims-nutritional_claims      = 'LOW' AND
               ls_ntr_claims-nutritional_claims_elem = 'CHOLESTEROL'.
          fc_v_ref_value = ls_mara-zzattr22.
        ELSE.
          CLEAR fc_v_art_upd.
        ENDIF.

      ENDIF.

    WHEN 'ZARN_PREP_TYPE'.
      IF fu_s_cc_fields-chg_field = 'PREPARATION_TYPE' OR
         fu_s_cc_fields-chg_field = 'KEY' .
        fc_v_ref_value = ls_mara-zzprep.
      ENDIF.

    WHEN 'ZARN_UOM_VARIANT'.
      CLEAR ls_reg_uom.
      LOOP AT fu_s_version-reg_data-zarn_reg_uom[] INTO ls_reg_uom
      WHERE pim_uom_code EQ fu_s_cc_fields-ref_data
        AND uom_category NE 'LAYER'
        AND uom_category NE 'PALLET'.

        CLEAR ls_marm.
        READ TABLE gt_marm[] INTO ls_marm
        WITH KEY matnr = fu_s_version-matnr
                 meinh = ls_reg_uom-meinh.
        IF sy-subrc = 0.

          CASE fu_s_cc_fields-chg_field.
            WHEN 'DEPTH_VALUE'.
              fc_v_ref_value = ls_marm-laeng.
              fc_v_ref_data  = ls_marm-meinh.
            WHEN 'WIDTH_VALUE'.
              fc_v_ref_value = ls_marm-breit.
              fc_v_ref_data  = ls_marm-meinh.
            WHEN 'HEIGHT_VALUE'.
              fc_v_ref_value = ls_marm-hoehe.
              fc_v_ref_data  = ls_marm-meinh.
            WHEN 'HEIGHT_UOM'.
              fc_v_ref_value = ls_marm-meabm.
              fc_v_ref_data  = ls_marm-meinh.
            WHEN 'GROSS_WEIGHT_VALUE'.
              fc_v_ref_value = ls_marm-brgew.
              fc_v_ref_data  = ls_marm-meinh.
            WHEN 'GROSS_WEIGHT_UOM'.
              fc_v_ref_value = ls_marm-gewei.
              fc_v_ref_data  = ls_marm-meinh.
            WHEN 'PRODUCT_UOM'.
              fc_v_ref_value = ls_marm-mesub.
              fc_v_ref_data  = ls_marm-meinh.
            WHEN OTHERS.
              CLEAR: fc_v_ref_data, fc_v_ref_value.
          ENDCASE.
        ENDIF.

        EXIT.
      ENDLOOP.

    WHEN 'ZARN_PIR'.
      CLEAR ls_pir.
      SPLIT fu_s_cc_fields-ref_data AT '/' INTO ls_pir-uom_code ls_pir-hybris_internal_code.

      IF ls_pir-hybris_internal_code CS '/'.
        REPLACE '/' IN ls_pir-hybris_internal_code WITH ''.
      ENDIF.

      CLEAR ls_reg_pir.
      READ TABLE fu_s_version-reg_data-zarn_reg_pir[] INTO ls_reg_pir
      WITH KEY order_uom_pim        = ls_pir-uom_code
               hybris_internal_code = ls_pir-hybris_internal_code.
      IF sy-subrc = 0.

        CLEAR ls_eina.
        READ TABLE gt_eina[] INTO ls_eina
        WITH KEY matnr = fu_s_version-matnr
                 lifnr = ls_reg_pir-lifnr.
        IF sy-subrc = 0.

          CASE fu_s_cc_fields-chg_field.
            WHEN 'VENDOR_ARTICLE_NUMBER'.
              fc_v_ref_value = ls_eina-idnlf.
              fc_v_ref_data  = ls_eina-lifnr.
            WHEN 'AVAIL_START_DATE'.
              fc_v_ref_value = ls_eina-lifab.
              fc_v_ref_data  = ls_eina-lifnr.
            WHEN 'AVAIL_END_DATE'.
              fc_v_ref_value = ls_eina-lifbi.
              fc_v_ref_data  = ls_eina-lifnr.
            WHEN 'COUNTRY_OF_ORIGIN'.
              fc_v_ref_value = ls_maw1-wherl.
              fc_v_ref_data  = ls_eina-lifnr.
            WHEN OTHERS.
              CLEAR: fc_v_ref_data, fc_v_ref_value.
          ENDCASE.
        ENDIF.
      ENDIF.

    WHEN 'ZARN_GTIN_VAR'.
      CLEAR ls_gtin.
      SPLIT fu_s_cc_fields-ref_data AT '/' INTO ls_gtin-uom_code ls_gtin-gtin_code.

      IF ls_gtin-gtin_code CS '/'.
        REPLACE '/' IN ls_gtin-gtin_code WITH ''.
      ENDIF.

      CLEAR ls_reg_uom.
      LOOP AT fu_s_version-reg_data-zarn_reg_uom[] INTO ls_reg_uom
      WHERE pim_uom_code EQ ls_gtin-uom_code
        AND uom_category NE 'LAYER'
        AND uom_category NE 'PALLET'.

        CASE fu_s_cc_fields-chg_field.
          WHEN 'GTIN_CODE' OR 'UOM_CODE' OR 'KEY'.
            CLEAR ls_mean.
            READ TABLE gt_mean[] INTO ls_mean
            WITH KEY matnr = fu_s_version-matnr
                     meinh = ls_reg_uom-meinh
                     ean11 = ls_gtin-gtin_code.
            IF sy-subrc = 0.
              fc_v_ref_value = ls_mean-ean11.
              fc_v_ref_data  = ls_reg_uom-meinh.
            ELSE.
              CLEAR ls_mean.
              READ TABLE gt_mean[] INTO ls_mean
              WITH KEY matnr = fu_s_version-matnr
                       meinh = ls_reg_uom-meinh
                       hpean = abap_true.
              IF sy-subrc = 0.
                fc_v_ref_value = ls_mean-ean11.
                fc_v_ref_data  = ls_reg_uom-meinh.
              ENDIF.
            ENDIF.

          WHEN 'GTIN_CURRENT'.
            CLEAR ls_mean.
            READ TABLE gt_mean[] INTO ls_mean
            WITH KEY matnr = fu_s_version-matnr
                     meinh = ls_reg_uom-meinh
                     hpean = abap_true.
            IF sy-subrc = 0.
              fc_v_ref_value = ls_mean-ean11.
              fc_v_ref_data  = ls_reg_uom-meinh.
            ENDIF.

          WHEN OTHERS.
            CLEAR: fc_v_ref_data, fc_v_ref_value.
        ENDCASE.

        EXIT.
      ENDLOOP.  " LOOP AT fu_s_version-reg_data-zarn_reg_uom[] INTO ls_reg_uom

    WHEN 'ZARN_NAT_MC_HIER'.
      IF fu_s_cc_fields-chg_field = 'NAT_MC_CODE' OR
         fu_s_cc_fields-chg_field = 'KEY'.
        fc_v_ref_value = ls_mara-matkl.
      ENDIF.

    WHEN OTHERS.
      CLEAR: fc_v_ref_data, fc_v_ref_value.
  ENDCASE.


  CONDENSE: fc_v_ref_data, fc_v_ref_value.

ENDFORM.                    " GET_REF_DATA_VALUE_NATIONAL

*&---------------------------------------------------------------------*
*&      Form  BUILD_REGIONAL_DATA
*&---------------------------------------------------------------------*
* Build Regional Data
*----------------------------------------------------------------------*
FORM build_regional_data .

* Get Regional Data
  PERFORM get_regional_data.


* Build ALV Data - Regional
  PERFORM build_alv_data_regional.

ENDFORM.                    " BUILD_REGIONAL_DATA
*&---------------------------------------------------------------------*
*&      Form  GET_REGIONAL_DATA
*&---------------------------------------------------------------------*
* Get Regional Data
*----------------------------------------------------------------------*
FORM get_regional_data .

  DATA: ls_fan_ver_stat TYPE ty_s_fan_ver_stat,
        ls_reg_data     TYPE zsarn_reg_data,
        ls_reg_data_ecc TYPE zsarn_reg_data,

        lt_table_mastr  TYPE ty_t_table_mastr,
        lt_cc_det       TYPE ztarn_cc_det,
        ls_cc_det       TYPE zarn_cc_det,
        ls_cc_fields    TYPE ty_s_cc_fields,


        lv_chgid        TYPE zarn_chg_id,
        lv_chgno        TYPE cdchangenr.



* Retreive ECC data for Regional
  PERFORM retreive_ecc_data_regional.


* Fill ECC Data to Regional
  PERFORM fill_ecc_data_to_regional.


  lt_table_mastr[] = gt_table_mastr[].
  DELETE lt_table_mastr[] WHERE arena_table_type NE gc_arn_tab_typ-reg.  " 'R' "  Regional Table


  CLEAR : gt_cc_det_reg[], gt_cc_fields[].

  LOOP AT gt_fan_ver_stat[] INTO ls_fan_ver_stat.


    CLEAR ls_reg_data.
    READ TABLE gt_reg_data[] INTO ls_reg_data
    WITH KEY idno = ls_fan_ver_stat-idno.
    IF sy-subrc = 0.
      CLEAR :  ls_reg_data-zarn_reg_prfam[],   ls_reg_data-zarn_reg_txt[],
               ls_reg_data-zarn_reg_dc_sell[], ls_reg_data-zarn_reg_hsno[],
               ls_reg_data-zarn_reg_lst_prc[], ls_reg_data-zarn_reg_rrp[],
               ls_reg_data-zarn_reg_std_ter[], ls_reg_data-zarn_reg_artlink[],
               ls_reg_data-zarn_reg_onlcat[],  ls_reg_data-zarn_reg_str[].


      SORT ls_reg_data-zarn_reg_banner[] BY idno banner.

      SORT ls_reg_data-zarn_reg_uom[] BY idno uom_category pim_uom_code hybris_internal_code
                                         lower_uom lower_child_uom.

      SORT ls_reg_data-zarn_reg_ean[] BY idno meinh ean11.

      SORT ls_reg_data-zarn_reg_pir[] BY idno order_uom_pim hybris_internal_code.

    ENDIF.


    CLEAR ls_reg_data_ecc.
    READ TABLE gt_reg_data_ecc[] INTO ls_reg_data_ecc
    WITH KEY idno = ls_fan_ver_stat-idno.
    IF sy-subrc = 0.
      CLEAR :  ls_reg_data_ecc-zarn_reg_prfam[],   ls_reg_data_ecc-zarn_reg_txt[],
               ls_reg_data_ecc-zarn_reg_dc_sell[], ls_reg_data_ecc-zarn_reg_hsno[],
               ls_reg_data_ecc-zarn_reg_lst_prc[], ls_reg_data_ecc-zarn_reg_rrp[],
               ls_reg_data_ecc-zarn_reg_std_ter[], ls_reg_data_ecc-zarn_reg_artlink[],
               ls_reg_data_ecc-zarn_reg_onlcat[],  ls_reg_data_ecc-zarn_reg_str[].

      SORT ls_reg_data_ecc-zarn_reg_banner[] BY idno banner.

      SORT ls_reg_data_ecc-zarn_reg_uom[] BY idno uom_category pim_uom_code hybris_internal_code
                                         lower_uom lower_child_uom.

      SORT ls_reg_data_ecc-zarn_reg_ean[] BY idno meinh ean11.

      SORT ls_reg_data_ecc-zarn_reg_pir[] BY idno order_uom_pim hybris_internal_code.

    ENDIF.

    lv_chgid        = 1.
    lv_chgno        = 1.

    CLEAR: lt_cc_det[].
* Build Change Category Data
    CALL FUNCTION 'ZARN_CC_BUILD_REGIONAL_DATA'
      EXPORTING
        it_table_mastr   = lt_table_mastr[]
        is_reg_data_db   = ls_reg_data_ecc
        is_reg_data_n    = ls_reg_data
        iv_chgid         = lv_chgid
        iv_change_number = lv_chgno
      IMPORTING
        et_cc_det        = lt_cc_det[].



    DELETE lt_cc_det[] WHERE chg_field = 'MANDT'.
    DELETE lt_cc_det[] WHERE chg_field = 'IDNO'.

    APPEND LINES OF lt_cc_det[] TO gt_cc_det_reg[].

    LOOP AT lt_cc_det[] INTO ls_cc_det.
      CLEAR ls_cc_fields.

      ls_cc_fields-idno         = ls_cc_det-idno.
      ls_cc_fields-version      = ls_cc_det-national_ver_id.
      ls_cc_fields-chg_category = ls_cc_det-chg_category.
      ls_cc_fields-chg_table    = ls_cc_det-chg_table.
      ls_cc_fields-chg_field    = ls_cc_det-chg_field.
      ls_cc_fields-chg_area     = ls_cc_det-chg_area.
      ls_cc_fields-chg_ind      = ls_cc_det-chg_ind.
      ls_cc_fields-ref_data     = ls_cc_det-reference_data.

      IF ls_cc_det-chg_ind     = 'U'.
        ls_cc_fields-value = ls_cc_det-value_new.
      ELSEIF ls_cc_det-chg_ind = 'I'.
        ls_cc_fields-value = ls_cc_det-value_new.
      ELSEIF ls_cc_det-chg_ind = 'D'.
        ls_cc_fields-value = ls_cc_det-value_old.
      ENDIF.

      CONDENSE ls_cc_fields-value.

      APPEND ls_cc_fields TO gt_cc_fields[].

    ENDLOOP.  " LOOP AT lt_cc_det[] INTO ls_cc_det


  ENDLOOP.  " LOOP AT gt_fan_ver_stat[] INTO ls_fan_ver_stat


  DELETE gt_cc_fields[] WHERE chg_field = 'MANDT'.
  DELETE gt_cc_fields[] WHERE chg_field = 'IDNO'.

  SORT gt_cc_fields[] BY idno chg_table chg_field value.
  DELETE ADJACENT DUPLICATES FROM gt_cc_fields[] COMPARING idno chg_table chg_field value.


  SORT gt_cc_det_reg[] BY idno chg_category chg_table chg_field.

ENDFORM.                    " GET_REGIONAL_DATA
*&---------------------------------------------------------------------*
*&      Form  RETREIVE_ECC_DATA_REGIONAL
*&---------------------------------------------------------------------*
* Retreive ECC data for Regional
*----------------------------------------------------------------------*
FORM retreive_ecc_data_regional .


  DATA: ls_tvarvc          TYPE tvarvc,
        lsr_numtp          LIKE LINE OF gtr_numtp,
        lt_tvarv_uoms      TYPE tvarvc_t,
        ls_tvarv_uoms      TYPE LINE OF tvarvc_t,
        lt_layer_uoms      TYPE TABLE OF meins,
        ls_layer_uoms      LIKE LINE OF lt_layer_uoms,
        lt_pallet_uoms     TYPE TABLE OF meins,
        ls_pallet_uoms     LIKE LINE OF lt_pallet_uoms,
        lt_inner_uoms      TYPE TABLE OF meins,
        ls_inner_uoms      LIKE LINE OF lt_pallet_uoms,
        lt_retail_uoms     TYPE TABLE OF meins,
        ls_retail_uoms     LIKE LINE OF lt_pallet_uoms,
        lt_shipper_uoms    TYPE TABLE OF meins,
        ls_shipper_uoms    LIKE LINE OF lt_pallet_uoms,
        lsr_uoms_range_tbl LIKE LINE OF gtr_layer_uoms.

  CONSTANTS:
    lc_ref_sites_uni TYPE rvari_vnam VALUE 'ZC_REF_VKORG_VTREFS',
    lc_ref_sites_lni TYPE rvari_vnam VALUE 'ZMD_VKORG_REF_SITES_LNI',
    lc_tvarv_layer   TYPE rvari_vnam VALUE 'GC_LAYER_UNOMS',
    lc_tvarv_pallet  TYPE rvari_vnam VALUE 'GC_PALLET_UNOMS',
    lc_tvarv_inner   TYPE rvari_vnam VALUE 'GC_INNER_UNOMS',
    lc_tvarv_retail  TYPE rvari_vnam VALUE 'GC_RETAIL_UNOMS',
    lc_tvarv_shipper TYPE rvari_vnam VALUE 'GC_SHIPPER_UNOMS'.





  CLEAR: gt_makt[], gt_gil_zzcatman[], gt_ret_zzcatman[], gt_retdc_zzcatman[],
         gt_host_sap[], gt_wlk2[], gt_wrf_matgrp_prod[], gt_reg_banner[],
         gt_tvarv_ref_sites_uni[], gt_tvarv_ref_sites_lni[],
         gtr_layer_uoms[], gtr_pallets_uoms[], gtr_inner_uoms[], gtr_retail_uoms[],
         gtr_shipper_uoms[], gtr_numtp[].


  IF gt_mara[] IS NOT INITIAL.

    SELECT * FROM makt INTO TABLE gt_makt
      FOR ALL ENTRIES IN gt_mara[]
      WHERE matnr = gt_mara-matnr
        AND spras = sy-langu.

    " Gilmours category manager
    SELECT matnr zzcatman FROM mvke INTO TABLE gt_gil_zzcatman
      FOR ALL ENTRIES IN gt_mara[]
      WHERE matnr EQ gt_mara-matnr
        AND vkorg EQ '3000'
        AND vtweg EQ '20'.

    " retail category manager
    SELECT matnr vkorg zzcatman FROM mvke INTO TABLE gt_ret_zzcatman
      FOR ALL ENTRIES IN gt_mara[]
      WHERE matnr EQ gt_mara-matnr
        AND ( vkorg EQ zcl_constants=>gc_vkorg_4000 OR vkorg EQ zcl_constants=>gc_vkorg_5000 OR vkorg EQ zcl_constants=>gc_vkorg_6000 )
        AND vtweg    EQ '20'
        AND zzcatman NE ''.

    " retail- 1000/10 category manager
    SELECT matnr zzcatman FROM mvke INTO TABLE gt_retdc_zzcatman
      FOR ALL ENTRIES IN gt_mara[]
      WHERE matnr EQ gt_mara-matnr
        AND vkorg EQ zcl_constants=>gc_banner_1000
        AND vtweg EQ zcl_constants=>gc_distr_chan_10.

    " get latest products
    SELECT * FROM zmd_host_sap INTO TABLE gt_host_sap
      FOR ALL ENTRIES IN gt_mara[]
      WHERE matnr  EQ gt_mara-matnr
        AND latest EQ zcl_constants=>gc_true.

    " get records for all banners and later select the banner sepc.
    SELECT * FROM wlk2 INTO TABLE gt_wlk2
      FOR ALL ENTRIES IN gt_mara[]
      WHERE matnr EQ gt_mara-matnr
        AND werks EQ ''.



    SELECT matnr node mainflg FROM wrf_matgrp_prod INTO TABLE gt_wrf_matgrp_prod
      FOR ALL ENTRIES IN gt_mara[]
      WHERE matnr = gt_mara-matnr.

    " For regional banner
    SELECT matnr vkorg vtweg zzcatman sstuf vrkme prodh versg ktgrm vmsta vmstd aumng
      FROM mvke INTO TABLE gt_reg_banner
      FOR ALL ENTRIES IN gt_mara[]
      WHERE matnr = gt_mara-matnr
      AND
      " VKORG where VKORG in (3000,4000,5000,6000) & VTWEG=20 & ZZCATMAN<> " " OR SSTUF <> " "
      (
        ( ( vkorg EQ '3000' OR vkorg EQ zcl_constants=>gc_vkorg_4000
          OR vkorg EQ zcl_constants=>gc_vkorg_5000 OR vkorg EQ zcl_constants=>gc_vkorg_6000 )
          AND vtweg EQ '20'
          AND ( zzcatman NE '' OR sstuf NE '' ) )
      OR
      " VKORG where VKORG=1000 & VTWEG=10 & ZZCATMAN<> " "
        ( vkorg EQ zcl_constants=>gc_banner_1000
          AND vtweg EQ zcl_constants=>gc_distr_chan_10
          AND zzcatman NE '' )
      ).



    CLEAR: gt_tvarv_ref_sites_lni, gt_tvarv_ref_sites_uni.

    ls_tvarvc-name = lc_ref_sites_uni.
    APPEND ls_tvarvc TO gt_tvarv_ref_sites_uni.

*-- Global method to fetch the TVARV
    CALL METHOD zcl_stvarv_constant=>get_stvarv_constant
      CHANGING
        xt_stvarv_constant = gt_tvarv_ref_sites_uni.

    CLEAR ls_tvarvc.
    ls_tvarvc-name = lc_ref_sites_lni.
    APPEND ls_tvarvc TO gt_tvarv_ref_sites_lni.

*-- Global method to fetch the TVARV
    CALL METHOD zcl_stvarv_constant=>get_stvarv_constant
      CHANGING
        xt_stvarv_constant = gt_tvarv_ref_sites_lni.

    CLEAR ls_tvarv_uoms.
*-- Global method to fetch the TVARV for LAYERS
    ls_tvarv_uoms-name = lc_tvarv_layer.
    APPEND ls_tvarv_uoms TO lt_tvarv_uoms.

*-- Global method to fetch the TVARV for PALLETS
    ls_tvarv_uoms-name = lc_tvarv_pallet.
    APPEND ls_tvarv_uoms TO lt_tvarv_uoms.

*-- Global method to fetch the TVARV for INNER
    ls_tvarv_uoms-name = lc_tvarv_inner.
    APPEND ls_tvarv_uoms TO lt_tvarv_uoms.

*-- Global method to fetch the TVARV for RETAIL
    ls_tvarv_uoms-name = lc_tvarv_retail.
    APPEND ls_tvarv_uoms TO lt_tvarv_uoms.

*-- Global method to fetch the TVARV for SHIPPER
    ls_tvarv_uoms-name = lc_tvarv_shipper.
    APPEND ls_tvarv_uoms TO lt_tvarv_uoms.

    CALL METHOD zcl_stvarv_constant=>get_stvarv_constant
      CHANGING
        xt_stvarv_constant = lt_tvarv_uoms.

    " LAYER
    READ TABLE lt_tvarv_uoms INTO ls_tvarv_uoms WITH KEY name = lc_tvarv_layer.
    IF sy-subrc EQ 0.
      SPLIT ls_tvarv_uoms-low AT '~' INTO TABLE lt_layer_uoms.
      lsr_uoms_range_tbl-sign = 'I'.
      lsr_uoms_range_tbl-option = 'EQ'.

      LOOP AT lt_layer_uoms INTO ls_layer_uoms.
        lsr_uoms_range_tbl-low = ls_layer_uoms.
        APPEND lsr_uoms_range_tbl TO gtr_layer_uoms.
      ENDLOOP.
    ENDIF.
    CLEAR ls_tvarv_uoms.

    " PALLET
    READ TABLE lt_tvarv_uoms INTO ls_tvarv_uoms WITH KEY name = lc_tvarv_pallet.
    IF sy-subrc EQ 0.
      SPLIT ls_tvarv_uoms-low AT '~' INTO TABLE lt_pallet_uoms.

      CLEAR lsr_uoms_range_tbl.
      lsr_uoms_range_tbl-sign = 'I'.
      lsr_uoms_range_tbl-option = 'EQ'.

      LOOP AT lt_pallet_uoms INTO ls_pallet_uoms.
        lsr_uoms_range_tbl-low = ls_pallet_uoms.
        APPEND lsr_uoms_range_tbl TO gtr_pallets_uoms.
      ENDLOOP.
    ENDIF.
    CLEAR ls_tvarv_uoms.

    " INNER
    READ TABLE lt_tvarv_uoms INTO ls_tvarv_uoms WITH KEY name = lc_tvarv_inner.
    IF sy-subrc EQ 0.
      SPLIT ls_tvarv_uoms-low AT '~' INTO TABLE lt_inner_uoms.

      CLEAR lsr_uoms_range_tbl.
      lsr_uoms_range_tbl-sign = 'I'.
      lsr_uoms_range_tbl-option = 'EQ'.

      LOOP AT lt_inner_uoms INTO ls_inner_uoms.
        lsr_uoms_range_tbl-low = ls_inner_uoms.
        APPEND lsr_uoms_range_tbl TO gtr_inner_uoms.
      ENDLOOP.
    ENDIF.
    CLEAR ls_tvarv_uoms.

    " RETAIL
    READ TABLE lt_tvarv_uoms INTO ls_tvarv_uoms WITH KEY name = lc_tvarv_retail.
    IF sy-subrc EQ 0.
      SPLIT ls_tvarv_uoms-low AT '~' INTO TABLE lt_retail_uoms.

      CLEAR lsr_uoms_range_tbl.
      lsr_uoms_range_tbl-sign = 'I'.
      lsr_uoms_range_tbl-option = 'EQ'.

      LOOP AT lt_retail_uoms INTO ls_retail_uoms.
        lsr_uoms_range_tbl-low = ls_retail_uoms.
        APPEND lsr_uoms_range_tbl TO gtr_retail_uoms.
      ENDLOOP.
    ENDIF.

    " SHIPPER
    READ TABLE lt_tvarv_uoms INTO ls_tvarv_uoms WITH KEY name = lc_tvarv_shipper.
    IF sy-subrc EQ 0.
      SPLIT ls_tvarv_uoms-low AT '~' INTO TABLE lt_shipper_uoms.

      CLEAR lsr_uoms_range_tbl.
      lsr_uoms_range_tbl-sign = 'I'.
      lsr_uoms_range_tbl-option = 'EQ'.

      LOOP AT lt_shipper_uoms INTO ls_shipper_uoms.
        lsr_uoms_range_tbl-low = ls_shipper_uoms.
        APPEND lsr_uoms_range_tbl TO gtr_shipper_uoms.
      ENDLOOP.
    ENDIF.

*    " Fill NUMTP range
*    lsr_numtp-sign = 'I'.
*    lsr_numtp-option = 'EQ'.
*    lsr_numtp-low = 'ZP'.
*    APPEND lsr_numtp TO gtr_numtp.
*    lsr_numtp-low = 'ZD'.
*    APPEND lsr_numtp TO gtr_numtp.
*    lsr_numtp-low = 'ZI'.
*    APPEND lsr_numtp TO gtr_numtp.
*    lsr_numtp-low = 'ZS'.
*    APPEND lsr_numtp TO gtr_numtp.
*    lsr_numtp-low = 'ZB'.
*    APPEND lsr_numtp TO gtr_numtp.
*    lsr_numtp-low = 'ZR'.
*    APPEND lsr_numtp TO gtr_numtp.



  ENDIF.  " IF gt_mara[] IS NOT INITIAL



ENDFORM.                    " RETREIVE_ECC_DATA_REGIONAL
*&---------------------------------------------------------------------*
*&      Form  FILL_ECC_DATA_TO_REGIONAL
*&---------------------------------------------------------------------*
* Fill ECC Data to Regional
*----------------------------------------------------------------------*
FORM fill_ecc_data_to_regional .

  DATA: ls_fan_ver_stat    TYPE ty_s_fan_ver_stat,
        ls_reg_data        TYPE zsarn_reg_data,
        ls_reg_data_ecc    TYPE zsarn_reg_data,

        ls_mara            TYPE ty_s_mara,
        ls_reg_hdr         TYPE zarn_reg_hdr,
        lt_reg_banner      TYPE ztarn_reg_banner,
        lt_reg_uom         TYPE ztarn_reg_uom,
        lt_reg_uom_lay_pal TYPE ztarn_reg_uom,
        lt_reg_ean         TYPE ztarn_reg_ean,
        lt_reg_pir         TYPE ztarn_reg_pir.


  LOOP AT gt_fan_ver_stat[] INTO ls_fan_ver_stat.

    CLEAR ls_mara.
    READ TABLE gt_mara[] INTO ls_mara
    WITH KEY zzfan = ls_fan_ver_stat-fan.
    IF ls_mara-matnr IS INITIAL.
      CONTINUE.
    ENDIF.

    CLEAR ls_reg_data.
    READ TABLE gt_reg_data[] INTO ls_reg_data
    WITH KEY idno = ls_fan_ver_stat-idno.


    CLEAR: ls_reg_data_ecc.
    ls_reg_data_ecc-idno = ls_fan_ver_stat-idno.

**********************************************************************
*********************FILL REGIONAL HEADER TABLE***********************
**********************************************************************

    CLEAR ls_reg_hdr.
    ls_reg_hdr-idno    = ls_fan_ver_stat-idno.
    ls_reg_hdr-version = ls_fan_ver_stat-current_ver.

* Fill other header fields of Regional Header table
    PERFORM fill_reg_header_tbl USING ls_mara
                                      ls_reg_data
                             CHANGING ls_reg_hdr.

    APPEND ls_reg_hdr TO ls_reg_data_ecc-zarn_reg_hdr[].


**********************************************************************
*********************FILL REGIONAL BANNER TABLE***********************
**********************************************************************

    CLEAR lt_reg_banner[].
    " Fill other fields for Regional banner table
    PERFORM fill_reg_banner USING ls_mara-matnr
                                  ls_fan_ver_stat-idno
                                  ls_reg_data
                         CHANGING lt_reg_banner[].

    IF lt_reg_banner[] IS NOT INITIAL.
      ls_reg_data_ecc-zarn_reg_banner[] = lt_reg_banner[].
    ENDIF.


**********************************************************************
********** Fill REGIONAL UOM TABLE- RETAIL/INNER/SHIPPER**************
**********************************************************************
    CLEAR:lt_reg_uom[].
* Fill ZARN_REG_UOM for RETAIL/INNER/SHIPPER
    PERFORM fill_reg_uom USING ls_mara
                               ls_fan_ver_stat-idno
                               ls_reg_data
                      CHANGING lt_reg_uom[].

    IF lt_reg_uom[] IS NOT INITIAL.
      ls_reg_data_ecc-zarn_reg_uom[] = lt_reg_uom[].
    ENDIF.


**********************************************************************
***************************Fill Regional EAN TABLE********************
**********************************************************************
    CLEAR lt_reg_ean[].
*  Fill ZARN_REG_EAN EAN for Retail/Inner/Shipper in ZARN_REG_UOM-PIM_UOM_CODE
    PERFORM fill_reg_ean USING ls_mara-matnr
                               ls_fan_ver_stat-idno
                               ls_reg_data
                      CHANGING lt_reg_ean.

    IF lt_reg_ean[] IS NOT INITIAL.
      ls_reg_data_ecc-zarn_reg_ean[] = lt_reg_ean[].
    ENDIF.



**********************************************************************
*********************** Fill REGIONAL PIR TABLE***********************
**********************************************************************
    CLEAR lt_reg_pir[].
*  Fill ZARN_REG_PIR table
    PERFORM fill_reg_pir USING ls_mara
                               ls_fan_ver_stat-idno
                               ls_reg_data
                      CHANGING lt_reg_pir[].

    IF lt_reg_pir[] IS NOT INITIAL.
      ls_reg_data_ecc-zarn_reg_pir[] = lt_reg_pir[].
    ENDIF.


    APPEND ls_reg_data_ecc TO gt_reg_data_ecc[].
  ENDLOOP.  " LOOP AT gt_fan_ver_stat[] INTO ls_fan_ver_stat.

  SORT gt_reg_data_ecc[] BY idno.


ENDFORM.                    " FILL_ECC_DATA_TO_REGIONAL
*&---------------------------------------------------------------------*
*&      Form  FILL_REG_HEADER_TBL
*&---------------------------------------------------------------------*
* Fill other header fields of Regional Header table
*----------------------------------------------------------------------*
FORM fill_reg_header_tbl USING fu_s_mara        TYPE mara
                               fu_s_reg_data    TYPE zsarn_reg_data
                      CHANGING fc_s_arn_reg_hdr TYPE zarn_reg_hdr.


  DATA ls_makt              LIKE LINE OF gt_makt.
  DATA ls_marc_sos          LIKE LINE OF gt_marc_sos.
  DATA ls_gil_zzcatman      LIKE LINE OF gt_gil_zzcatman.
  DATA ls_ret_zzcatman      LIKE LINE OF gt_ret_zzcatman.
  DATA ls_retdc_zzcatman    LIKE LINE OF gt_retdc_zzcatman.
  DATA ls_host_sap          LIKE LINE OF gt_host_sap.
  DATA ls_wlk2              LIKE LINE OF gt_wlk2.
  DATA ls_wrf_matgrp_prod   LIKE LINE OF gt_wrf_matgrp_prod.
  DATA ls_reg_hdr           TYPE zarn_reg_hdr.

  CLEAR ls_reg_hdr.
  READ TABLE fu_s_reg_data-zarn_reg_hdr INTO ls_reg_hdr
  WITH KEY idno = fc_s_arn_reg_hdr-idno.

  fc_s_arn_reg_hdr = ls_reg_hdr.

  fc_s_arn_reg_hdr-matnr                 = fu_s_mara-matnr.
  fc_s_arn_reg_hdr-matkl                 = fu_s_mara-matkl.
  fc_s_arn_reg_hdr-mtart                 = fu_s_mara-mtart.
  fc_s_arn_reg_hdr-attyp                 = fu_s_mara-attyp.
  fc_s_arn_reg_hdr-storage_temp          = fu_s_mara-tempb.
  fc_s_arn_reg_hdr-retail_net_weight     = fu_s_mara-ntgew.
  fc_s_arn_reg_hdr-retail_net_weight_uom = fu_s_mara-gewei.
  fc_s_arn_reg_hdr-prdha                 = fu_s_mara-prdha.
  fc_s_arn_reg_hdr-zzprdtype             = fu_s_mara-zzprdtype.
*  fc_s_arn_reg_hdr-zzselling_only        = fu_s_mara-zzselling_only.             "--ONED-217 JKH 24.11.2016
  fc_s_arn_reg_hdr-zzbuy_sell            = fu_s_mara-zzbuy_sell.                  "++ONED-217 JKH 24.11.2016
  fc_s_arn_reg_hdr-zzas4subdept          = fu_s_mara-zzas4subdept.
  fc_s_arn_reg_hdr-zzvar_wt_flag         = fu_s_mara-zzvar_wt_flag.
  fc_s_arn_reg_hdr-zz_uni_dc             = fu_s_mara-zz_uni_dc.
  fc_s_arn_reg_hdr-zz_lni_dc             = fu_s_mara-zz_lni_dc.
  fc_s_arn_reg_hdr-zzsell                = fu_s_mara-zzsell.
  fc_s_arn_reg_hdr-zzuse                 = fu_s_mara-zzuse.
  fc_s_arn_reg_hdr-mstae                 = fu_s_mara-mstae.
  fc_s_arn_reg_hdr-mstde                 = fu_s_mara-mstde.
  fc_s_arn_reg_hdr-mstav                 = fu_s_mara-mstav.
  fc_s_arn_reg_hdr-mstdv                 = fu_s_mara-mstdv.
  fc_s_arn_reg_hdr-zztktprnt             = fu_s_mara-zztktprnt.
  fc_s_arn_reg_hdr-zzpedesc_flag         = fu_s_mara-zzpedesc_flag.
  fc_s_arn_reg_hdr-saisj                 = fu_s_mara-saisj.
  fc_s_arn_reg_hdr-saiso                 = fu_s_mara-saiso.
  fc_s_arn_reg_hdr-inhal                 = fu_s_mara-inhal.
  fc_s_arn_reg_hdr-inhme                 = fu_s_mara-inhme.
  fc_s_arn_reg_hdr-bstat                 = fu_s_mara-bstat.
  fc_s_arn_reg_hdr-zzattr1               = fu_s_mara-zzattr1.
  fc_s_arn_reg_hdr-zzattr2               = fu_s_mara-zzattr2.
  fc_s_arn_reg_hdr-zzattr3               = fu_s_mara-zzattr3.
  fc_s_arn_reg_hdr-zzattr4               = fu_s_mara-zzattr4.
  fc_s_arn_reg_hdr-zzattr5               = fu_s_mara-zzattr5.
  fc_s_arn_reg_hdr-zzattr6               = fu_s_mara-zzattr6.
  fc_s_arn_reg_hdr-zzattr7               = fu_s_mara-zzattr7.
  fc_s_arn_reg_hdr-zzattr8               = fu_s_mara-zzattr8.
  fc_s_arn_reg_hdr-zzattr9               = fu_s_mara-zzattr9.
  fc_s_arn_reg_hdr-zzattr10              = fu_s_mara-zzattr10.
  fc_s_arn_reg_hdr-zzattr11              = fu_s_mara-zzattr11.
  fc_s_arn_reg_hdr-zzattr12              = fu_s_mara-zzattr12.
  fc_s_arn_reg_hdr-zzattr13              = fu_s_mara-zzattr13.
  fc_s_arn_reg_hdr-zzattr14              = fu_s_mara-zzattr14.
  fc_s_arn_reg_hdr-zzattr15              = fu_s_mara-zzattr15.
  fc_s_arn_reg_hdr-zzattr16              = fu_s_mara-zzattr16.
  fc_s_arn_reg_hdr-zzattr17              = fu_s_mara-zzattr17.
  fc_s_arn_reg_hdr-zzattr18              = fu_s_mara-zzattr18.
  fc_s_arn_reg_hdr-zzattr19              = fu_s_mara-zzattr19.
  fc_s_arn_reg_hdr-zzattr20              = fu_s_mara-zzattr20.
  fc_s_arn_reg_hdr-zzattr21              = fu_s_mara-zzattr21.
  fc_s_arn_reg_hdr-zzattr22              = fu_s_mara-zzattr22.
  fc_s_arn_reg_hdr-zzattr23              = fu_s_mara-zzattr23.
  fc_s_arn_reg_hdr-zzstrd                = fu_s_mara-zzstrd.
  fc_s_arn_reg_hdr-zzsco_weighed_flag    = fu_s_mara-zzsco_weighed_flag.
  fc_s_arn_reg_hdr-zzstrg                = fu_s_mara-zzstrg.
  fc_s_arn_reg_hdr-zzlabnum              = fu_s_mara-zzlabnum.
  fc_s_arn_reg_hdr-zzpkddt               = fu_s_mara-zzpkddt.
  fc_s_arn_reg_hdr-zzmandst              = fu_s_mara-zzmandst.
  fc_s_arn_reg_hdr-zzwnmsg               = fu_s_mara-zzwnmsg.
  fc_s_arn_reg_hdr-brand_id              = fu_s_mara-brand_id.
  fc_s_arn_reg_hdr-mhdrz                 = fu_s_mara-mhdrz.
  fc_s_arn_reg_hdr-zzingretype           = fu_s_mara-zzingretype.
  fc_s_arn_reg_hdr-zzrange_flag          = fu_s_mara-zzrange_flag.
  fc_s_arn_reg_hdr-zzrange_avail         = fu_s_mara-zzrange_avail.

* DEL Begin of Change INC5348789 JKH 31.10.2016
** INS Begin of Change 3162 Jitin 13.10.2016
*  fc_s_arn_reg_hdr-zzunnum               = fu_s_mara-zzunnum.
*  fc_s_arn_reg_hdr-zzpackgrp             = fu_s_mara-zzpackgrp.
*  fc_s_arn_reg_hdr-zzhcls                = fu_s_mara-zzhcls.
*  fc_s_arn_reg_hdr-zzhsno                = fu_s_mara-zzhsno.
*  fc_s_arn_reg_hdr-zzhard                = fu_s_mara-zzhard.
** INS End of Change 3162 Jitin 13.10.2016
* DEL End of Change INC5348789 JKH 31.10.2016


*  fc_s_arn_reg_hdr-ersda                 = fu_s_mara-ersda.
*  fc_s_arn_reg_hdr-ernam                 = fu_s_mara-ernam.
*  fc_s_arn_reg_hdr-laeda                 = fu_s_mara-laeda.
*  fc_s_arn_reg_hdr-aenam                 = fu_s_mara-aenam.
*
*  fc_s_arn_reg_hdr-erzet                 = ls_reg_hdr-erzet.
*  fc_s_arn_reg_hdr-eruet                 = ls_reg_hdr-eruet.



  READ TABLE gt_makt INTO ls_makt WITH TABLE KEY matnr = fu_s_mara-matnr.
  IF sy-subrc EQ 0.
    fc_s_arn_reg_hdr-retail_unit_desc = ls_makt-maktx.
  ENDIF.


  READ TABLE gt_marc_sos INTO ls_marc_sos
    WITH TABLE KEY matnr = fu_s_mara-matnr werks = 'RFST'.
  IF sy-subrc EQ 0.
    fc_s_arn_reg_hdr-bwscl = ls_marc_sos-bwscl.
  ENDIF.


  CLEAR ls_gil_zzcatman.
  READ TABLE gt_gil_zzcatman INTO ls_gil_zzcatman WITH TABLE KEY matnr = fu_s_mara-matnr.
  IF sy-subrc EQ 0.
    fc_s_arn_reg_hdr-gil_zzcatman = ls_gil_zzcatman-zzcatman.
  ENDIF.

  " getting the first record for banner 4000/5000/6000 sorted already
  CLEAR ls_ret_zzcatman.
  READ TABLE gt_ret_zzcatman INTO ls_ret_zzcatman
    WITH KEY matnr = fu_s_mara-matnr.
  IF sy-subrc EQ 0 AND ls_ret_zzcatman-zzcatman IS NOT INITIAL.
    fc_s_arn_reg_hdr-ret_zzcatman = ls_ret_zzcatman-zzcatman.
    " If Blank then ZZCATMAN where VKORG=1000 & VTWEG=10
  ELSE.
    CLEAR ls_retdc_zzcatman.
    READ TABLE gt_retdc_zzcatman INTO ls_retdc_zzcatman WITH TABLE KEY matnr = fu_s_mara-matnr.
    IF sy-subrc EQ 0.
      fc_s_arn_reg_hdr-ret_zzcatman = ls_retdc_zzcatman-zzcatman.
    ENDIF.
  ENDIF.


  " RETAIL
**  UOM Category of RETAIL_UOM where LATEST=X  & MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
  READ TABLE gt_host_sap INTO ls_host_sap WITH TABLE KEY matnr = fu_s_mara-matnr.
  IF sy-subrc EQ 0.

    IF ls_host_sap-retail_uom IN gtr_retail_uoms[].
      fc_s_arn_reg_hdr-leg_retail = 'RETAIL'.
    ELSEIF ls_host_sap-retail_uom IN gtr_inner_uoms[].
      fc_s_arn_reg_hdr-leg_retail = 'INNER'.
    ELSEIF ls_host_sap-retail_uom IN gtr_shipper_uoms[].
      fc_s_arn_reg_hdr-leg_retail = 'SHIPPER'.
    ELSEIF ls_host_sap-retail_uom IN gtr_layer_uoms[].
      fc_s_arn_reg_hdr-leg_retail = 'LAYER'.
    ELSEIF ls_host_sap-retail_uom IN gtr_pallets_uoms[].
      fc_s_arn_reg_hdr-leg_retail = 'PALLET'.
    ENDIF.


    IF ls_host_sap-inner_uom IN gtr_inner_uoms[].
      fc_s_arn_reg_hdr-leg_repack = 'INNER'.
    ELSEIF ls_host_sap-inner_uom IN gtr_retail_uoms[].
      fc_s_arn_reg_hdr-leg_repack = 'RETAIL'.
    ELSEIF ls_host_sap-inner_uom IN gtr_shipper_uoms[].
      fc_s_arn_reg_hdr-leg_repack = 'SHIPPER'.
    ELSEIF ls_host_sap-inner_uom IN gtr_layer_uoms[].
      fc_s_arn_reg_hdr-leg_repack = 'LAYER'.
    ELSEIF ls_host_sap-inner_uom IN gtr_pallets_uoms[].
      fc_s_arn_reg_hdr-leg_repack = 'PALLET'.
    ENDIF.



    IF ls_host_sap-shipper_uom IN gtr_shipper_uoms[].
      fc_s_arn_reg_hdr-leg_bulk = 'SHIPPER'.
    ELSEIF ls_host_sap-shipper_uom IN gtr_inner_uoms[].
      fc_s_arn_reg_hdr-leg_bulk = 'INNER'.
    ELSEIF ls_host_sap-shipper_uom IN gtr_retail_uoms[].
      fc_s_arn_reg_hdr-leg_bulk = 'RETAIL'.
    ELSEIF ls_host_sap-shipper_uom IN gtr_layer_uoms[].
      fc_s_arn_reg_hdr-leg_bulk = 'LAYER'.
    ELSEIF ls_host_sap-shipper_uom IN gtr_pallets_uoms[].
      fc_s_arn_reg_hdr-leg_bulk = 'PALLET'.
    ENDIF.

    fc_s_arn_reg_hdr-leg_prboly = ls_host_sap-prboly.

  ENDIF.  " READ TABLE gt_host_sap INTO ls_host_sap






**  KWDHT where KWDHT<> "" Where WLK2-MATNR = ZARN_REG_HDR-SAP_ARTICLE (First record)
  LOOP AT gt_wlk2 INTO ls_wlk2 WHERE matnr = fu_s_mara-matnr AND kwdht IS NOT INITIAL.
    fc_s_arn_reg_hdr-qty_required = ls_wlk2-kwdht.
    EXIT.
  ENDLOOP.


**  PRERF where PRERF<>"" Where WLK2-MATNR = ZARN_REG_HDR-SAP_ARTICLE (First record)
  LOOP AT gt_wlk2 INTO ls_wlk2 WHERE matnr = fu_s_mara-matnr AND prerf IS NOT INITIAL.
    fc_s_arn_reg_hdr-prerf = ls_wlk2-prerf.
    EXIT.
  ENDLOOP.


**  RBZUL where RBZUL<> "" & VKORG=5000 & VTWEG =20 &MATNR = ZARN_REG_HDR-SAP_ARTICLE
  READ TABLE gt_wlk2 INTO ls_wlk2 WITH TABLE KEY matnr = fu_s_mara-matnr vkorg = '5000'
    vtweg = '20' werks = ''.
  IF sy-subrc EQ 0 AND ls_wlk2-rbzul IS NOT INITIAL.
    fc_s_arn_reg_hdr-pns_tpr_flag = ls_wlk2-rbzul.
  ENDIF.


**  SCAGR where SCAGR<> " " Where WLK2-MATNR = ZARN_REG_HDR-SAP_ARTICLE (First record)
  LOOP AT gt_wlk2 INTO ls_wlk2 WHERE matnr = fu_s_mara-matnr AND scagr IS NOT INITIAL.
    fc_s_arn_reg_hdr-scagr = ls_wlk2-scagr.
    EXIT.
  ENDLOOP.



**    If MARA-PRDHA <> "" Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE then "X"
  IF fu_s_mara-prdha IS NOT INITIAL.
    fc_s_arn_reg_hdr-gilmours_web = zcl_constants=>gc_true.
  ENDIF.

**   If Count(MEAN-EAN11 where MEAN-EANTP='ZP' & MEAN-MATNR = ZARN_REG_HDR-SAP_ARTICLE)>0 then "X"
  READ TABLE gt_mean TRANSPORTING NO FIELDS WITH KEY matnr = fu_s_mara-matnr eantp = 'ZP'.
  IF sy-subrc EQ 0.
    fc_s_arn_reg_hdr-plu_article = zcl_constants=>gc_true.
  ENDIF.

**    NODE Where WRF_MATGRP_PROD-MATNR =  ZARN_REG_HDR-SAP_ARTICLE AND NODE = REG_HDR-NODE
  READ TABLE gt_wrf_matgrp_prod INTO ls_wrf_matgrp_prod
  WITH KEY matnr = fu_s_mara-matnr
           node  = ls_reg_hdr-article_hierarchy.
  IF sy-subrc EQ 0.
    fc_s_arn_reg_hdr-article_hierarchy = ls_wrf_matgrp_prod-node.
  ELSE.
**    NODE Where WRF_MATGRP_PROD-MATNR =  ZARN_REG_HDR-SAP_ARTICLE
    READ TABLE gt_wrf_matgrp_prod INTO ls_wrf_matgrp_prod
    WITH KEY matnr   = fu_s_mara-matnr
             mainflg = abap_true.
    IF sy-subrc EQ 0.
      fc_s_arn_reg_hdr-article_hierarchy = ls_wrf_matgrp_prod-node.
    ENDIF.
  ENDIF.




ENDFORM.                    " FILL_REG_HEADER_TBL
*&---------------------------------------------------------------------*
*&      Form  FILL_REG_BANNER
*&---------------------------------------------------------------------*
* Fill other fields for Regional banner table
*----------------------------------------------------------------------*
FORM fill_reg_banner USING fu_v_matnr           TYPE matnr
                           fu_v_idno            TYPE zarn_idno
                           fu_s_reg_data        TYPE zsarn_reg_data
                  CHANGING fc_t_arn_reg_banner  TYPE ztarn_reg_banner.


  DATA ls_tvarvc                TYPE tvarvc.
  DATA ls_marc_sos              TYPE ty_s_marc_sos.
  DATA ls_arn_reg_banner        TYPE zarn_reg_banner.
  DATA ls_reg_banner            TYPE ty_s_reg_banner.
  DATA lv_werks                 TYPE werks_d.
  DATA ls_wlk2                  LIKE LINE OF gt_wlk2.


  LOOP AT gt_reg_banner INTO ls_reg_banner WHERE matnr = fu_v_matnr.

    CLEAR ls_arn_reg_banner.

    " fill data for all relevant banners
    ls_arn_reg_banner-idno               = fu_v_idno.
    ls_arn_reg_banner-banner             = ls_reg_banner-vkorg.


    ls_arn_reg_banner-zzcatman           = ls_reg_banner-zzcatman.   " Category manager
    ls_arn_reg_banner-sstuf              = ls_reg_banner-sstuf.
    ls_arn_reg_banner-vrkme              = ls_reg_banner-vrkme.
    ls_arn_reg_banner-prodh              = ls_reg_banner-prodh.
    ls_arn_reg_banner-versg              = ls_reg_banner-versg.
    ls_arn_reg_banner-ktgrm              = ls_reg_banner-ktgrm.
    ls_arn_reg_banner-vmsta              = ls_reg_banner-vmsta.
    ls_arn_reg_banner-vmstd              = ls_reg_banner-vmstd.
    ls_arn_reg_banner-aumng              = ls_reg_banner-aumng.

    " UNI SOS
    CLEAR: ls_marc_sos,ls_tvarvc.
    READ TABLE gt_tvarv_ref_sites_uni INTO ls_tvarvc WITH KEY low = ls_reg_banner-vkorg.
    IF sy-subrc EQ 0.
      CLEAR lv_werks.
      lv_werks = ls_tvarvc-high.
      CONDENSE lv_werks.
      READ TABLE gt_marc_sos INTO ls_marc_sos
        WITH TABLE KEY matnr = fu_v_matnr werks = lv_werks.
      IF sy-subrc EQ 0.
        ls_arn_reg_banner-source_uni    = ls_marc_sos-bwscl.
        ls_arn_reg_banner-online_status = ls_marc_sos-zzonline_status.     "++ONLD-822 JKH 17.02.2017
        ls_arn_reg_banner-pbs_code      = ls_marc_sos-zz_pbs.
      ENDIF.
    ENDIF.


    " LNI SOS
    CLEAR: ls_marc_sos,ls_tvarvc.
    READ TABLE gt_tvarv_ref_sites_lni INTO ls_tvarvc WITH KEY low = ls_reg_banner-vkorg.
    IF sy-subrc EQ 0.
      CLEAR lv_werks.
      lv_werks = ls_tvarvc-high.
      CONDENSE lv_werks.
      READ TABLE gt_marc_sos INTO ls_marc_sos
        WITH TABLE KEY matnr = fu_v_matnr werks = lv_werks.
      IF sy-subrc EQ 0.
        ls_arn_reg_banner-source_lni = ls_marc_sos-bwscl.
      ENDIF.
    ENDIF.


    CLEAR ls_wlk2.
    READ TABLE gt_wlk2 INTO ls_wlk2 WITH TABLE KEY matnr = fu_v_matnr vkorg = ls_reg_banner-vkorg
      vtweg = ls_reg_banner-vtweg werks = ''.
    IF sy-subrc EQ 0.
      ls_arn_reg_banner-qty_required    = ls_wlk2-kwdht.
      ls_arn_reg_banner-prerf           = ls_wlk2-prerf.
      ls_arn_reg_banner-pns_tpr_flag    = ls_wlk2-rbzul.
      ls_arn_reg_banner-scagr           = ls_wlk2-scagr.
    ENDIF.  " READ TABLE gt_wlk2 INTO ls_wlk2


* Fields not to be posted
    READ TABLE fu_s_reg_data-zarn_reg_banner[] INTO DATA(ls_reg_banner_db)
    WITH KEY idno               = fu_v_idno
             banner             = ls_reg_banner-vkorg.
    IF sy-subrc = 0.
      ls_arn_reg_banner-average_weight     = ls_reg_banner_db-average_weight.
      ls_arn_reg_banner-average_weight_uom = ls_reg_banner_db-average_weight_uom.
      ls_arn_reg_banner-sale_type          = ls_reg_banner_db-sale_type.
      ls_arn_reg_banner-step_size          = ls_reg_banner_db-step_size.
    ENDIF.



    APPEND ls_arn_reg_banner TO fc_t_arn_reg_banner.

  ENDLOOP.




ENDFORM.                    " FILL_REG_BANNER
*&---------------------------------------------------------------------*
*&      Form  FILL_REG_UOM
*&---------------------------------------------------------------------*
* Fill ZARN_REG_UOM for RETAIL/INNER/SHIPPER
*----------------------------------------------------------------------*
FORM fill_reg_uom USING fu_s_mara          TYPE mara
                        fu_v_idno          TYPE zarn_idno
                        fu_s_reg_data      TYPE zsarn_reg_data
               CHANGING fc_t_arn_reg_uom   TYPE ztarn_reg_uom.

  DATA: ls_reg_uom     TYPE zarn_reg_uom,
        ls_arn_reg_uom TYPE zarn_reg_uom,
        ls_marm        TYPE ty_s_marm,
        ls_maw1        TYPE ty_s_maw1.


  CLEAR ls_maw1.
  READ TABLE gt_maw1[] INTO ls_maw1
  WITH KEY matnr = fu_s_mara-matnr.


  LOOP AT fu_s_reg_data-zarn_reg_uom[] INTO ls_reg_uom.

    CLEAR ls_arn_reg_uom.

    ls_arn_reg_uom = ls_reg_uom.

    CLEAR ls_marm.
    READ TABLE gt_marm INTO ls_marm
    WITH KEY meinh = ls_reg_uom-meinh.
    IF sy-subrc NE 0.
      CONTINUE.
    ENDIF.

* MEINH
    ls_arn_reg_uom-meinh = ls_marm-meinh.

* LOWER_MEINH
    IF ls_arn_reg_uom-meinh = fu_s_mara-meins.
      ls_arn_reg_uom-lower_meinh = ls_marm-mesub.
    ELSE.
      IF ls_marm-mesub IS INITIAL.
        ls_arn_reg_uom-lower_meinh = fu_s_mara-meins.
      ELSE.
        ls_arn_reg_uom-lower_meinh = ls_marm-mesub.
      ENDIF.
    ENDIF.

* UNIT_BASE
    IF ls_reg_uom-meinh = fu_s_mara-meins.
      ls_arn_reg_uom-unit_base = abap_true.
    ENDIF.

* UNIT_PURORD
    IF fu_s_mara-bstme IS INITIAL.
      IF ls_reg_uom-meinh = fu_s_mara-meins.
        ls_arn_reg_uom-unit_purord = abap_true.
      ENDIF.
    ELSE.
      IF ls_reg_uom-meinh = fu_s_mara-bstme.
        ls_arn_reg_uom-unit_purord = abap_true.
      ENDIF.
    ENDIF.


* ISSUE_UNIT
    IF ls_maw1-wausm IS INITIAL.
      IF ls_reg_uom-meinh = fu_s_mara-meins.
        ls_arn_reg_uom-issue_unit = abap_true.
      ENDIF.
    ELSE.
      IF ls_reg_uom-meinh = ls_maw1-wausm.
        ls_arn_reg_uom-issue_unit = abap_true.
      ENDIF.
    ENDIF.


* SALES_UNIT
    IF ls_maw1-wvrkm IS INITIAL.
      IF ls_reg_uom-meinh = fu_s_mara-meins.
        ls_arn_reg_uom-sales_unit = abap_true.
      ENDIF.
    ELSE.
      IF ls_reg_uom-meinh = ls_maw1-wvrkm.
        ls_arn_reg_uom-sales_unit = abap_true.
      ENDIF.
    ENDIF.


    APPEND ls_arn_reg_uom TO fc_t_arn_reg_uom[].

  ENDLOOP.



ENDFORM.                    " FILL_REG_UOM
*&---------------------------------------------------------------------*
*&      Form  FILL_REG_EAN
*&---------------------------------------------------------------------*
*  Fill ZARN_REG_EAN EAN for Retail/Inner/Shipper in ZARN_REG_UOM-PIM_UOM_CODE
*----------------------------------------------------------------------*
FORM fill_reg_ean USING fu_v_matnr           TYPE matnr
                        fu_v_idno            TYPE zarn_idno
                        fu_s_reg_data      TYPE zsarn_reg_data
               CHANGING fc_t_arn_reg_ean     TYPE ztarn_reg_ean.



  DATA ls_arn_reg_ean TYPE zarn_reg_ean.
  DATA ls_reg_ean     TYPE zarn_reg_ean.
  DATA ls_mean        LIKE LINE OF gt_mean.
  DATA lv_ean11       TYPE ean11.

  FREE fc_t_arn_reg_ean.


  LOOP AT fu_s_reg_data-zarn_reg_ean[] INTO ls_reg_ean.

    IF ls_reg_ean-ean11 IS NOT INITIAL.
      CLEAR lv_ean11.
      CALL FUNCTION 'CONVERSION_EXIT_EAN11_INPUT'
        EXPORTING
          input  = ls_reg_ean-ean11
        IMPORTING
          output = lv_ean11.
    ENDIF.

    READ TABLE gt_mean[] INTO ls_mean
    WITH KEY matnr = fu_v_matnr
             meinh = ls_reg_ean-meinh
             ean11 = lv_ean11.
    IF sy-subrc = 0.
      CLEAR ls_arn_reg_ean.
      ls_arn_reg_ean-idno  = ls_reg_ean-idno.
      ls_arn_reg_ean-meinh = ls_reg_ean-meinh.
      ls_arn_reg_ean-ean11 = ls_reg_ean-ean11.
      ls_arn_reg_ean-eantp = ls_mean-eantp.
      ls_arn_reg_ean-hpean = ls_mean-hpean.

      APPEND ls_arn_reg_ean TO fc_t_arn_reg_ean[].
    ENDIF.

  ENDLOOP.





*  LOOP AT gt_mean INTO ls_mean WHERE matnr = fu_v_matnr.
*
*    CLEAR ls_arn_reg_ean.
*
*    " fill data for all relevant EANs
*    ls_arn_reg_ean-idno = fu_v_idno.
*
**    IF ls_mean-ean11 IS NOT INITIAL AND ls_mean-eantp IN gtr_numtp[].
*
***  Populate the  above ZARN_REG_UOM-MEINH which is having the MEAN-EAN11 with
***  MEAN-NUMTP in "ZP","ZD","ZI",","ZS","ZB","ZR" & MEAN-MATNR = ZARN_REG_HDR-SAP_ARTICLE
*    ls_arn_reg_ean-meinh = ls_mean-meinh.
*
***  EAN11 where  MEAN-NUMTP in ""ZP"",""ZD"",""ZI"","",""ZS"",""ZB"",""ZR"" &
***  MEAN-MEINH=ZARN_REG_EAN-MEINH & MEAN-MATNR = ZARN_REG_HDR-SAP_ARTICLE"
*    ls_arn_reg_ean-ean11 = ls_mean-ean11.
*
***NUMTP of the EAN11 above
*    ls_arn_reg_ean-eantp = ls_mean-eantp.
*
*
***      HPEAN of the EAN11 above
*    ls_arn_reg_ean-hpean = ls_mean-hpean.
*
*
*    APPEND ls_arn_reg_ean TO fc_t_arn_reg_ean[].
*
**    ENDIF.
*
*  ENDLOOP.



ENDFORM.                    " FILL_REG_EAN
*&---------------------------------------------------------------------*
*&      Form  FILL_REG_PIR
*&---------------------------------------------------------------------*
*  Fill ZARN_REG_PIR table
*----------------------------------------------------------------------*
FORM fill_reg_pir USING fu_s_mara          TYPE mara
                        fu_v_idno          TYPE zarn_idno
                        fu_s_reg_data      TYPE zsarn_reg_data
               CHANGING fc_t_reg_pir       TYPE ztarn_reg_pir.

  DATA: lt_vendor       TYPE ztarn_reg_pir,
        ls_vendor       TYPE zarn_reg_pir,
        ls_eina         TYPE eina,
        ls_eine         TYPE eine,

        lt_reg_pir_ecc  TYPE ztarn_reg_pir,
        ls_reg_pir_ecc  TYPE zarn_reg_pir,
        ls_reg_pir      TYPE zarn_reg_pir,
        ls_reg_pir_eina TYPE zarn_reg_pir,
        ls_reg_pir_eine TYPE zarn_reg_pir.


  FIELD-SYMBOLS: <ls_reg_pir>  TYPE zarn_reg_pir.

  lt_vendor[] = fu_s_reg_data-zarn_reg_pir[].

  SORT lt_vendor[] BY lifnr.
  DELETE ADJACENT DUPLICATES FROM lt_vendor[] COMPARING lifnr.

  CHECK lt_vendor[] IS NOT INITIAL.


  LOOP AT lt_vendor[] INTO ls_vendor.

    CLEAR ls_eina.
    READ TABLE gt_eina[] INTO ls_eina
    WITH KEY matnr = fu_s_mara-matnr
             lifnr = ls_vendor-lifnr.
    IF sy-subrc = 0.

      CLEAR ls_reg_pir_eina.
      READ TABLE fu_s_reg_data-zarn_reg_pir[] INTO ls_reg_pir_eina
      WITH KEY lifnr = ls_vendor-lifnr
               bprme = ls_eina-meins.
      IF sy-subrc = 0.
        ls_reg_pir_eina-pir_rel_eina = abap_true.
        ls_reg_pir_eina-vabme        = ls_eina-vabme.
        ls_reg_pir_eina-relif        = ls_eina-relif.
        ls_reg_pir_eina-lifab        = ls_eina-lifab.
        ls_reg_pir_eina-lifbi        = ls_eina-lifbi.
        ls_reg_pir_eina-idnlf        = ls_eina-idnlf.




        CLEAR ls_eine.
        READ TABLE gt_eine[] INTO ls_eine
        WITH TABLE KEY infnr = ls_eina-infnr.
        IF sy-subrc = 0.

          IF ls_reg_pir_eina-bprme = ls_eine-bprme.
            ls_reg_pir_eine = ls_reg_pir_eina.
          ELSE.
            APPEND ls_reg_pir_eina TO lt_reg_pir_ecc[].

            CLEAR ls_reg_pir_eine.
            READ TABLE fu_s_reg_data-zarn_reg_pir[] INTO ls_reg_pir_eine
            WITH KEY lifnr = ls_vendor-lifnr
                     bprme = ls_eine-bprme.
          ENDIF.  " IF ls_reg_pir_eina-orderpr_un = ls_eine-bprme


          IF ls_reg_pir_eine IS NOT INITIAL.
            ls_reg_pir_eine-pir_rel_eine = abap_true.

            ls_reg_pir_eine-esokz = ls_eine-esokz.
            ls_reg_pir_eine-ekgrp = ls_eine-ekgrp.
            ls_reg_pir_eine-minbm = ls_eine-minbm.
            ls_reg_pir_eine-norbm = ls_eine-norbm.
            ls_reg_pir_eine-aplfz = ls_eine-aplfz.
            ls_reg_pir_eine-bprme = ls_eine-bprme.
            ls_reg_pir_eine-ekkol = ls_eine-ekkol.
            ls_reg_pir_eine-mwskz = ls_eine-mwskz.
            ls_reg_pir_eine-loekz = ls_eine-loekz.

            ls_reg_pir_eine-waers = ls_eine-waers.
            ls_reg_pir_eine-netpr = ls_eine-netpr.
            ls_reg_pir_eine-peinh = ls_eine-peinh.
*            ls_reg_pir_eine-datlb = ls_eine-erdat.
            ls_reg_pir_eine-prdat = ls_eine-prdat.
            ls_reg_pir_eine-uebto = ls_eine-uebto.
            ls_reg_pir_eine-untto = ls_eine-untto.




            APPEND ls_reg_pir_eine TO lt_reg_pir_ecc[].
          ENDIF.  " READ TABLE fu_s_reg_data-zarn_reg_pir[] INTO ls_reg_pir_ecc

        ENDIF.  " READ TABLE gt_eine INTO ls_eine

      ENDIF.  " READ TABLE fu_s_reg_data-zarn_reg_pir[] INTO ls_reg_pir_ecc

    ENDIF.  " READ TABLE gt_eina[] INTO ls_eina

  ENDLOOP.  " LOOP AT lt_vendor[] INTO ls_vendor


  LOOP AT fu_s_reg_data-zarn_reg_pir[] INTO ls_reg_pir.



    CLEAR ls_reg_pir_ecc.
    READ TABLE lt_reg_pir_ecc[] INTO ls_reg_pir_ecc
    WITH KEY idno                 = ls_reg_pir-idno
             order_uom_pim        = ls_reg_pir-order_uom_pim
             hybris_internal_code = ls_reg_pir-hybris_internal_code.
    IF sy-subrc = 0.
      ls_reg_pir = ls_reg_pir_ecc.
    ELSE.
      CLEAR: ls_reg_pir-pir_rel_eina, ls_reg_pir-pir_rel_eine.
    ENDIF.

    APPEND ls_reg_pir TO fc_t_reg_pir[].
  ENDLOOP.


ENDFORM.                    " FILL_REG_PIR
*&---------------------------------------------------------------------*
*&      Form  BUILD_ALV_DATA_REGIONAL
*&---------------------------------------------------------------------*
* Build ALV Data - Regional
*----------------------------------------------------------------------*
FORM build_alv_data_regional.


  DATA: ls_fan_ver_stat TYPE ty_s_fan_ver_stat,
        ls_cc_det_reg   TYPE zarn_cc_det,
        ls_reg_data     TYPE zsarn_reg_data,
        ls_reg_data_ecc TYPE zsarn_reg_data,
        ls_reg_hdr      TYPE zarn_reg_hdr,
        ls_reg_hdr_ecc  TYPE zarn_reg_hdr,
*        ls_base           TYPE ty_s_ver_screen,
*        ls_version        TYPE ty_s_ver_screen,
*        ls_prd_version    TYPE zarn_prd_version,
        ls_field_confg  TYPE zarn_field_confg,
*        ls_cc_det         TYPE zarn_cc_det,
*        ls_cc_fields      TYPE ty_s_cc_fields,
        lt_dfies        TYPE STANDARD TABLE OF dfies,
        ls_dfies        TYPE dfies,
*        lv_exist          TYPE flag,

        lt_delta_alv    TYPE ztarn_delta_analysis_alv,
        ls_delta_alv    TYPE zsarn_delta_analysis_alv.

  CLEAR: lt_delta_alv[].
  LOOP AT gt_fan_ver_stat[] INTO ls_fan_ver_stat.

    LOOP AT gt_cc_det_reg[] INTO ls_cc_det_reg
      WHERE idno = ls_fan_ver_stat-idno.

      CLEAR ls_delta_alv.


      ls_delta_alv-fan_id         = ls_fan_ver_stat-fan.
      ls_delta_alv-chg_area       = ls_cc_det_reg-chg_area.
      ls_delta_alv-chg_category   = ls_cc_det_reg-chg_category.
      ls_delta_alv-chg_table      = ls_cc_det_reg-chg_table.
      ls_delta_alv-chg_field      = ls_cc_det_reg-chg_field.
      ls_delta_alv-reference_data = ls_cc_det_reg-reference_data.
      ls_delta_alv-chg_ind        = ls_cc_det_reg-chg_ind.

      ls_delta_alv-field_desc = ls_delta_alv-chg_field.

      IF ls_delta_alv-chg_table IS NOT INITIAL AND
         ls_delta_alv-chg_field IS NOT INITIAL.
* Get field Description
        REFRESH lt_dfies[].
        CALL FUNCTION 'DDIF_FIELDINFO_GET'
          EXPORTING
            tabname        = ls_delta_alv-chg_table
            fieldname      = ls_delta_alv-chg_field
            langu          = sy-langu
          TABLES
            dfies_tab      = lt_dfies
          EXCEPTIONS
            not_found      = 1
            internal_error = 2
            OTHERS         = 3.
        IF sy-subrc = 0.
          CLEAR ls_dfies.
          READ TABLE lt_dfies[] INTO ls_dfies INDEX 1.
          IF sy-subrc = 0.
            ls_delta_alv-field_desc = ls_dfies-fieldtext.
          ENDIF.
        ENDIF.
      ENDIF.

* Read regional data
      CLEAR: ls_reg_data, ls_reg_hdr.
      READ TABLE gt_reg_data[] INTO ls_reg_data
      WITH KEY idno = ls_fan_ver_stat-idno.
      IF sy-subrc = 0.

* Read regoinal header to get current regional version
        READ TABLE ls_reg_data-zarn_reg_hdr[] INTO ls_reg_hdr
        WITH KEY idno = ls_fan_ver_stat-idno.
        IF sy-subrc = 0.
          ls_delta_alv-reg_last_ver = ls_reg_hdr-version.
        ENDIF.
      ENDIF.



* Read Regional ECC data
      CLEAR: ls_reg_data_ecc, ls_reg_hdr_ecc.
      READ TABLE gt_reg_data_ecc[] INTO ls_reg_data_ecc
      WITH KEY idno = ls_fan_ver_stat-idno.
      IF sy-subrc = 0.

* Read regoinal header to get current regional version
        READ TABLE ls_reg_data_ecc-zarn_reg_hdr[] INTO ls_reg_hdr_ecc
        WITH KEY idno = ls_fan_ver_stat-idno.
        IF sy-subrc = 0.
          ls_delta_alv-matnr = ls_reg_hdr_ecc-matnr.
        ENDIF.
      ENDIF.




* Regional Table Value - New Value
      ls_delta_alv-reg_value    = ls_cc_det_reg-value_new.


* Article Update?? only if SAP_TABLE SAP_FIELD is available in field config
      CLEAR ls_field_confg.
      READ TABLE gt_field_confg[] INTO ls_field_confg
      WITH KEY tabname = ls_cc_det_reg-chg_table
               fldname = ls_cc_det_reg-chg_field.
      IF sy-subrc = 0.
        IF ls_field_confg-sap_table IS NOT INITIAL AND
           ls_field_confg-sap_field IS NOT INITIAL.

          ls_delta_alv-art_upd = 'Y'.


* Get Ref Data and Ref Value for Regional
          PERFORM get_ref_data_value_regional USING ls_fan_ver_stat
                                                    ls_field_confg-sap_table
                                                    ls_field_confg-sap_field
                                                    ls_cc_det_reg
                                                    ls_reg_data
                                           CHANGING ls_delta_alv-ref_data
                                                    ls_delta_alv-ref_value
                                                    ls_delta_alv-reference_value.

        ENDIF.
      ENDIF.


      APPEND ls_delta_alv TO lt_delta_alv[].

    ENDLOOP.  " LOOP AT gt_cc_fields[] INTO ls_cc_fields
  ENDLOOP.  " LOOP AT gt_fan_ver_stat[] INTO ls_fan_ver_stat

  SORT lt_delta_alv[] BY fan_id chg_table chg_field.
  APPEND LINES OF lt_delta_alv[] TO gt_delta_alv[].

ENDFORM.                    " BUILD_ALV_DATA_REGIONAL
*&---------------------------------------------------------------------*
*&      Form  GET_REF_DATA_VALUE_REGIONAL
*&---------------------------------------------------------------------*
* Get Ref Data and Ref Value for Regional
*----------------------------------------------------------------------*
FORM get_ref_data_value_regional USING fu_s_fan_ver_stat TYPE ty_s_fan_ver_stat
                                       fu_v_sap_table    TYPE zarn_sap_table
                                       fu_v_sap_field    TYPE zarn_sap_field
                                       fu_s_cc_det_reg   TYPE zarn_cc_det
                                       fu_s_reg_data     TYPE zsarn_reg_data
                              CHANGING fc_v_ref_data     TYPE zarn_ref_data
                                       fc_v_ref_value    TYPE zarn_ref_value
                                       fc_v_value_ref    TYPE zarn_reference_value.

  DATA : ls_pir     TYPE zarn_reg_pir,
         ls_reg_pir TYPE zarn_reg_pir,
         ls_uom     TYPE zarn_reg_uom,
         ls_reg_uom TYPE zarn_reg_uom,
         ls_tvarvc  TYPE tvarvc,
         lv_werks   TYPE werks_d.

  CLEAR: fc_v_ref_data, fc_v_ref_value, fc_v_value_ref.



  CASE fu_s_cc_det_reg-chg_table.
    WHEN 'ZARN_REG_HDR'.
* ECC Table Value - Old Value
      fc_v_ref_value    = fu_s_cc_det_reg-value_old.

    WHEN 'ZARN_REG_BANNER'.
* ECC Table Value - Old Value
      fc_v_ref_value    = fu_s_cc_det_reg-value_old.
      fc_v_ref_data     = fu_s_cc_det_reg-reference_data.

      CLEAR: ls_tvarvc.
      READ TABLE gt_tvarv_ref_sites_uni INTO ls_tvarvc WITH KEY low = fu_s_cc_det_reg-reference_data.
      IF sy-subrc EQ 0.
        CLEAR lv_werks.
        lv_werks = ls_tvarvc-high.

        IF lv_werks IS NOT INITIAL.
          CONCATENATE '(' lv_werks ')' INTO fc_v_ref_data.
          CONCATENATE fu_s_cc_det_reg-reference_data fc_v_ref_data
                 INTO fc_v_ref_data
         SEPARATED BY space.

        ENDIF.
      ENDIF.


    WHEN 'ZARN_REG_EAN'.
* ECC Table Value - Old Value
      fc_v_ref_value    = fu_s_cc_det_reg-value_old.
      fc_v_ref_data     = fu_s_cc_det_reg-reference_data.

    WHEN 'ZARN_REG_UOM'.
      CLEAR ls_uom.
      SPLIT fu_s_cc_det_reg-reference_data AT '/'
       INTO ls_uom-uom_category
            ls_uom-pim_uom_code
            ls_uom-hybris_internal_code
            ls_uom-lower_uom
            ls_uom-lower_child_uom.

      IF ls_uom-lower_child_uom CS '/'.
        REPLACE '/' IN ls_uom-lower_child_uom WITH ''.
      ENDIF.

      CLEAR ls_reg_uom.
      READ TABLE fu_s_reg_data-zarn_reg_uom[] INTO ls_reg_uom
      WITH KEY uom_category         = ls_uom-uom_category
               pim_uom_code         = ls_uom-pim_uom_code
               hybris_internal_code = ls_uom-hybris_internal_code
               lower_uom            = ls_uom-lower_uom
               lower_child_uom      = ls_uom-lower_child_uom.
      IF sy-subrc = 0.
        fc_v_value_ref = ls_reg_uom-meinh.
      ENDIF.

* ECC Table Value - Old Value
      fc_v_ref_value    = fu_s_cc_det_reg-value_old.
      fc_v_ref_data     = fu_s_cc_det_reg-reference_data.


    WHEN 'ZARN_REG_PIR'.
      CLEAR ls_pir.
      SPLIT fu_s_cc_det_reg-reference_data AT '/' INTO ls_pir-order_uom_pim ls_pir-hybris_internal_code.

      IF ls_pir-hybris_internal_code CS '/'.
        REPLACE '/' IN ls_pir-hybris_internal_code WITH ''.
      ENDIF.

      CLEAR ls_reg_pir.
      READ TABLE fu_s_reg_data-zarn_reg_pir[] INTO ls_reg_pir
      WITH KEY order_uom_pim        = ls_pir-order_uom_pim
               hybris_internal_code = ls_pir-hybris_internal_code.
      IF sy-subrc = 0.

        fc_v_value_ref = ls_reg_pir-bprme.

        fc_v_ref_data = ls_reg_pir-lifnr.

        CASE fu_s_cc_det_reg-chg_field.
          WHEN 'PIR_REL_EINA'.
            fc_v_ref_value = ls_reg_pir-bprme.
          WHEN 'PIR_REL_EINE'.
            fc_v_ref_value = ls_reg_pir-bprme.
          WHEN OTHERS.
* ECC Table Value - Old Value
            fc_v_ref_value    = fu_s_cc_det_reg-value_old.
        ENDCASE.

      ENDIF.

    WHEN OTHERS.
* ECC Table Value - Old Value
      fc_v_ref_value    = fu_s_cc_det_reg-value_old.

  ENDCASE.


  IF fu_s_cc_det_reg-chg_field = 'KEY'.
    CLEAR: fc_v_ref_data, fc_v_ref_value.
  ENDIF.


ENDFORM.                    " GET_REF_DATA_VALUE_REGIONAL
*&---------------------------------------------------------------------*
*&      Form  APPLY_FILTERS_ON_ALV
*&---------------------------------------------------------------------*
* Apply Filters on ALV
*----------------------------------------------------------------------*
FORM apply_filters_on_alv .


  IF s_chgcat IS NOT INITIAL.
    DELETE gt_delta_alv[] WHERE chg_category NOT IN s_chgcat[].
  ENDIF.

  IF s_chgind IS NOT INITIAL.
    DELETE gt_delta_alv[] WHERE chg_ind IS NOT INITIAL AND
                                chg_ind NOT IN s_chgind[].
  ENDIF.

  IF s_artupd IS NOT INITIAL.
    DELETE gt_delta_alv[] WHERE art_upd NOT IN s_artupd[].
  ENDIF.

ENDFORM.                    " APPLY_FILTERS_ON_ALV
*&---------------------------------------------------------------------*
*&      Form  DISPLAY_ALV
*&---------------------------------------------------------------------*
* Display ALV
*----------------------------------------------------------------------*
FORM display_alv.


  CONSTANTS: lc_a        TYPE char01 VALUE 'A'.

* Declaration
  DATA: lo_docking    TYPE REF TO cl_gui_docking_container,  "Custom container instance reference
        lt_fieldcat   TYPE        lvc_t_fcat,                "Field catalog
        ls_layout     TYPE        lvc_s_layo ,               "Layout structure
*        lt_toolbar       TYPE        ui_functions,              "Toolbar Function
*        lo_event_handler TYPE REF TO lcl_event_handler,         "Event handling
        ls_disvariant TYPE        disvariant,                "Variant
        lo_alvgrid    TYPE REF TO cl_gui_alv_grid.


  IF lo_alvgrid IS INITIAL .
*   Create docking container
    CREATE OBJECT lo_docking
      EXPORTING
        parent = cl_gui_container=>screen0
*       ratio  = 90  " 90% yet not full-screen size
      EXCEPTIONS
        OTHERS = 6.
    IF sy-subrc NE 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

*   Full size screen for ALV
    CALL METHOD lo_docking->set_extension
      EXPORTING
        extension  = 99999  " full-screen size !!!
      EXCEPTIONS
        cntl_error = 1
        OTHERS     = 2.
    IF sy-subrc NE 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

* Create ALV grid
    CREATE OBJECT lo_alvgrid
      EXPORTING
        i_parent = lo_docking
      EXCEPTIONS
        OTHERS   = 5.
    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.


*   Preparing field catalog
    PERFORM prepare_fcat CHANGING lt_fieldcat[].

*   Color Grid
    PERFORM color_grid.

*   Preparing layout structure
    PERFORM prepare_layout CHANGING ls_layout.

* Prepare Variant
    ls_disvariant-report    = sy-repid.
    ls_disvariant-username  = sy-uname.

*   ALV Display
    CALL METHOD lo_alvgrid->set_table_for_first_display
      EXPORTING
        i_bypassing_buffer            = abap_true
*       i_structure_name              = gc_alv_8000-struct
        is_variant                    = ls_disvariant
        i_save                        = lc_a
        is_layout                     = ls_layout
*       it_toolbar_excluding          = lt_toolbar
      CHANGING
        it_outtab                     = gt_delta_alv[]
        it_fieldcatalog               = lt_fieldcat[]
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.


  ELSE.

*   ALV Refresh Display
    CALL METHOD lo_alvgrid->refresh_table_display
      EXPORTING
*       is_stable      = ls_stable
        i_soft_refresh = abap_true
      EXCEPTIONS
        finished       = 1
        OTHERS         = 2.
  ENDIF.



ENDFORM.                    " DISPLAY_ALV
*&---------------------------------------------------------------------*
*&      Form  PREPARE_FCAT
*&---------------------------------------------------------------------*
*   Preparing field catalog
*----------------------------------------------------------------------*
FORM prepare_fcat  CHANGING fc_t_fieldcat TYPE lvc_t_fcat.

* Declaration
  DATA: lt_fieldcat TYPE lvc_t_fcat,
        ls_fieldcat TYPE lvc_s_fcat,
        ls_version  TYPE ty_s_ver_screen.

  FIELD-SYMBOLS : <ls_fieldcat>    TYPE lvc_s_fcat.

* Prepare field catalog for ALV display by merging structure
  REFRESH: lt_fieldcat[].
  CALL FUNCTION 'LVC_FIELDCATALOG_MERGE'
    EXPORTING
      i_structure_name       = 'ZSARN_DELTA_ANALYSIS_ALV'
    CHANGING
      ct_fieldcat            = lt_fieldcat[]
    EXCEPTIONS
      inconsistent_interface = 1
      program_error          = 2
      OTHERS                 = 3.
  IF sy-subrc NE 0.
    REFRESH: fc_t_fieldcat.
  ENDIF.





* Looping to make the output field
  LOOP AT lt_fieldcat ASSIGNING <ls_fieldcat>.


    <ls_fieldcat>-tooltip = <ls_fieldcat>-scrtext_l.


    CASE <ls_fieldcat>-fieldname.
      WHEN 'FAN_ID'.
        <ls_fieldcat>-col_opt = abap_true.
      WHEN 'CHG_AREA'.
        <ls_fieldcat>-col_opt = abap_true.
      WHEN 'CHG_CATEGORY'.
        <ls_fieldcat>-col_opt = abap_true.
      WHEN 'CHG_TABLE'.
        <ls_fieldcat>-col_opt = abap_true.
      WHEN 'CHG_FIELD'.
        <ls_fieldcat>-col_opt = abap_true.
      WHEN 'FIELD_DESC'.
        <ls_fieldcat>-outputlen = 20.
      WHEN 'REFERENCE_DATA'.
        <ls_fieldcat>-outputlen = 20.
      WHEN 'REFERENCE_VALUE'.
        <ls_fieldcat>-outputlen = 20.
      WHEN 'CHG_IND'.
        <ls_fieldcat>-col_opt = abap_true.
      WHEN 'REG_LAST_VER'.
        <ls_fieldcat>-col_opt = abap_true.
      WHEN 'REG_VALUE'.
        <ls_fieldcat>-outputlen = 20.
      WHEN 'VERSION1'.
        IF gv_national IS INITIAL.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

        <ls_fieldcat>-col_opt = abap_true.
      WHEN 'VALUE1'.
        IF gv_national IS INITIAL.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

        <ls_fieldcat>-outputlen = 20.
      WHEN 'STATUS1'.
        IF gv_national IS INITIAL.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

        <ls_fieldcat>-col_opt = abap_true.
      WHEN 'VERSION2'.
        IF gv_national IS INITIAL.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

        <ls_fieldcat>-col_opt = abap_true.
      WHEN 'VALUE2'.
        IF gv_national IS INITIAL.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

        <ls_fieldcat>-outputlen = 20.
      WHEN 'STATUS2'.
        IF gv_national IS INITIAL.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

        <ls_fieldcat>-col_opt = abap_true.
      WHEN 'VERSION3'.
        IF gv_national IS INITIAL.
          <ls_fieldcat>-no_out = abap_true.
        ELSE.
          IF p_multi = abap_true.
            <ls_fieldcat>-no_out = abap_true.
          ELSE.
            CLEAR ls_version.
            READ TABLE gt_version[] INTO ls_version
            WITH KEY vno = 3.
            IF sy-subrc NE 0.
              <ls_fieldcat>-no_out = abap_true.
            ENDIF.
          ENDIF.
        ENDIF.

        <ls_fieldcat>-col_opt = abap_true.

      WHEN 'VALUE3'.
        IF gv_national IS INITIAL.
          <ls_fieldcat>-no_out = abap_true.
        ELSE.
          IF p_multi = abap_true.
            <ls_fieldcat>-no_out = abap_true.
          ELSE.
            CLEAR ls_version.
            READ TABLE gt_version[] INTO ls_version
            WITH KEY vno = 3.
            IF sy-subrc NE 0.
              <ls_fieldcat>-no_out = abap_true.
            ENDIF.
          ENDIF.
        ENDIF.

        <ls_fieldcat>-outputlen = 20.

      WHEN 'STATUS3'.
        IF gv_national IS INITIAL.
          <ls_fieldcat>-no_out = abap_true.
        ELSE.
          IF p_multi = abap_true.
            <ls_fieldcat>-no_out = abap_true.
          ELSE.
            CLEAR ls_version.
            READ TABLE gt_version[] INTO ls_version
            WITH KEY vno = 3.
            IF sy-subrc NE 0.
              <ls_fieldcat>-no_out = abap_true.
            ENDIF.
          ENDIF.
        ENDIF.

        <ls_fieldcat>-col_opt = abap_true.

      WHEN 'VERSION4'.
        IF gv_national IS INITIAL.
          <ls_fieldcat>-no_out = abap_true.
        ELSE.
          IF p_multi = abap_true.
            <ls_fieldcat>-no_out = abap_true.
          ELSE.
            CLEAR ls_version.
            READ TABLE gt_version[] INTO ls_version
            WITH KEY vno = 4.
            IF sy-subrc NE 0.
              <ls_fieldcat>-no_out = abap_true.
            ENDIF.
          ENDIF.
        ENDIF.

        <ls_fieldcat>-col_opt = abap_true.

      WHEN 'VALUE4'.
        IF gv_national IS INITIAL.
          <ls_fieldcat>-no_out = abap_true.
        ELSE.
          IF p_multi = abap_true.
            <ls_fieldcat>-no_out = abap_true.
          ELSE.
            CLEAR ls_version.
            READ TABLE gt_version[] INTO ls_version
            WITH KEY vno = 4.
            IF sy-subrc NE 0.
              <ls_fieldcat>-no_out = abap_true.
            ENDIF.
          ENDIF.
        ENDIF.

        <ls_fieldcat>-outputlen = 20.

      WHEN 'STATUS4'.
        IF gv_national IS INITIAL.
          <ls_fieldcat>-no_out = abap_true.
        ELSE.
          IF p_multi = abap_true.
            <ls_fieldcat>-no_out = abap_true.
          ELSE.
            CLEAR ls_version.
            READ TABLE gt_version[] INTO ls_version
            WITH KEY vno = 4.
            IF sy-subrc NE 0.
              <ls_fieldcat>-no_out = abap_true.
            ENDIF.
          ENDIF.
        ENDIF.

        <ls_fieldcat>-col_opt = abap_true.

      WHEN 'VERSION5'.
        IF gv_national IS INITIAL.
          <ls_fieldcat>-no_out = abap_true.
        ELSE.
          IF p_multi = abap_true.
            <ls_fieldcat>-no_out = abap_true.
          ELSE.
            CLEAR ls_version.
            READ TABLE gt_version[] INTO ls_version
            WITH KEY vno = 5.
            IF sy-subrc NE 0.
              <ls_fieldcat>-no_out = abap_true.
            ENDIF.
          ENDIF.
        ENDIF.

        <ls_fieldcat>-col_opt = abap_true.

      WHEN 'VALUE5'.
        IF gv_national IS INITIAL.
          <ls_fieldcat>-no_out = abap_true.
        ELSE.
          IF p_multi = abap_true.
            <ls_fieldcat>-no_out = abap_true.
          ELSE.
            CLEAR ls_version.
            READ TABLE gt_version[] INTO ls_version
            WITH KEY vno = 5.
            IF sy-subrc NE 0.
              <ls_fieldcat>-no_out = abap_true.
            ENDIF.
          ENDIF.
        ENDIF.

        <ls_fieldcat>-outputlen = 20.

      WHEN 'STATUS5'.
        IF gv_national IS INITIAL.
          <ls_fieldcat>-no_out = abap_true.
        ELSE.
          IF p_multi = abap_true.
            <ls_fieldcat>-no_out = abap_true.
          ELSE.
            CLEAR ls_version.
            READ TABLE gt_version[] INTO ls_version
            WITH KEY vno = 5.
            IF sy-subrc NE 0.
              <ls_fieldcat>-no_out = abap_true.
            ENDIF.
          ENDIF.
        ENDIF.

        <ls_fieldcat>-col_opt = abap_true.

*      WHEN 'CHANGED_ON'.
*<ls_fieldcat>-col_opt = abap_true.
*      WHEN 'CHANGED_AT'.
*<ls_fieldcat>-col_opt = abap_true.
*      WHEN 'CHANGED_BY'.
*<ls_fieldcat>-col_opt = abap_true.
      WHEN 'MATNR'.
        <ls_fieldcat>-col_opt = abap_true.
      WHEN 'ART_UPD'.
        <ls_fieldcat>-col_opt = abap_true.
      WHEN 'REF_DATA'.
        <ls_fieldcat>-col_opt = abap_true.
      WHEN 'REF_VALUE'.
        <ls_fieldcat>-col_opt = abap_true.

      WHEN OTHERS.
        DELETE lt_fieldcat WHERE fieldname = <ls_fieldcat>-fieldname.

    ENDCASE.

  ENDLOOP.

  fc_t_fieldcat[] = lt_fieldcat[].

ENDFORM.                    " PREPARE_FCAT
*&---------------------------------------------------------------------*
*&      Form  COLOR_GRID
*&---------------------------------------------------------------------*
*   Color Grid
*----------------------------------------------------------------------*
FORM color_grid.


  DATA: col    TYPE lvc_s_scol,
        coltab TYPE lvc_t_scol,
        color  TYPE lvc_s_colo.


  FIELD-SYMBOLS: <ls_delta_alv>     TYPE zsarn_delta_analysis_alv.


  LOOP AT gt_delta_alv[] ASSIGNING <ls_delta_alv>.
    CLEAR: coltab[].

    IF <ls_delta_alv>-chg_area = gc_arn_tab_typ-nat.    " 'N'

      CLEAR col. col-fname = 'FAN_ID'.          col-color-col = '1'. col-color-int = '1'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'CHG_AREA'.        col-color-col = '1'. col-color-int = '1'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'CHG_CATEGORY'.    col-color-col = '1'. col-color-int = '1'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'CHG_TABLE'.       col-color-col = '1'. col-color-int = '1'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'CHG_FIELD'.       col-color-col = '1'. col-color-int = '1'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'FIELD_DESC'.      col-color-col = '1'. col-color-int = '1'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'REFERENCE_DATA'.  col-color-col = '1'. col-color-int = '1'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'REFERENCE_VALUE'. col-color-col = '1'. col-color-int = '1'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'CHG_IND'.         col-color-col = '1'. col-color-int = '1'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'REG_LAST_VER'.    col-color-col = '1'. col-color-int = '1'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'REG_VALUE'.       col-color-col = '1'. col-color-int = '1'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'VERSION1'.        col-color-col = '1'. col-color-int = '1'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'VALUE1'.          col-color-col = '1'. col-color-int = '1'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'STATUS1'.         col-color-col = '1'. col-color-int = '1'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'VERSION2'.        col-color-col = '1'. col-color-int = '1'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'VALUE2'.          col-color-col = '1'. col-color-int = '1'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'STATUS2'.         col-color-col = '1'. col-color-int = '1'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'VERSION3'.        col-color-col = '1'. col-color-int = '1'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'VALUE3'.          col-color-col = '1'. col-color-int = '1'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'STATUS3'.         col-color-col = '1'. col-color-int = '1'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'VERSION4'.        col-color-col = '1'. col-color-int = '1'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'VALUE4'.          col-color-col = '1'. col-color-int = '1'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'STATUS4'.         col-color-col = '1'. col-color-int = '1'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'VERSION5'.        col-color-col = '1'. col-color-int = '1'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'VALUE5'.          col-color-col = '1'. col-color-int = '1'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'STATUS5'.         col-color-col = '1'. col-color-int = '1'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'CHANGED_ON'.      col-color-col = '1'. col-color-int = '1'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'CHANGED_AT'.      col-color-col = '1'. col-color-int = '1'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'CHANGED_BY'.      col-color-col = '1'. col-color-int = '1'. APPEND col TO coltab[].

    ELSEIF <ls_delta_alv>-chg_area = gc_arn_tab_typ-reg.  " 'R'

      CLEAR col. col-fname = 'FAN_ID'.          col-color-col = '5'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'CHG_AREA'.        col-color-col = '5'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'CHG_CATEGORY'.    col-color-col = '5'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'CHG_TABLE'.       col-color-col = '5'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'CHG_FIELD'.       col-color-col = '5'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'FIELD_DESC'.      col-color-col = '5'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'REFERENCE_DATA'.  col-color-col = '5'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'REFERENCE_VALUE'. col-color-col = '5'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'CHG_IND'.         col-color-col = '5'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'REG_LAST_VER'.    col-color-col = '5'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'REG_VALUE'.       col-color-col = '5'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'VERSION1'.        col-color-col = '5'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'VALUE1'.          col-color-col = '5'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'STATUS1'.         col-color-col = '5'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'VERSION2'.        col-color-col = '5'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'VALUE2'.          col-color-col = '5'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'STATUS2'.         col-color-col = '5'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'VERSION3'.        col-color-col = '5'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'VALUE3'.          col-color-col = '5'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'STATUS3'.         col-color-col = '5'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'VERSION4'.        col-color-col = '5'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'VALUE4'.          col-color-col = '5'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'STATUS4'.         col-color-col = '5'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'VERSION5'.        col-color-col = '5'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'VALUE5'.          col-color-col = '5'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'STATUS5'.         col-color-col = '5'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'CHANGED_ON'.      col-color-col = '5'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'CHANGED_AT'.      col-color-col = '5'. APPEND col TO coltab[].
      CLEAR col. col-fname = 'CHANGED_BY'.      col-color-col = '5'. APPEND col TO coltab[].

    ENDIF.

    CLEAR col. col-fname = 'MATNR'.     col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'ART_UPD'.   col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'REF_DATA'.  col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'REF_VALUE'. col-color-col = '4'. APPEND col TO coltab[].

    <ls_delta_alv>-color = coltab[].

  ENDLOOP.




ENDFORM.                    " COLOR_GRID
*&---------------------------------------------------------------------*
*&      Form  PREPARE_LAYOUT
*&---------------------------------------------------------------------*
*       Field Catalog for STO Display
*----------------------------------------------------------------------*
FORM prepare_layout CHANGING fc_s_layout    TYPE lvc_s_layo.

  fc_s_layout-ctab_fname = 'COLOR'.
  fc_s_layout-sel_mode   = 'A'.

ENDFORM.                    " PREPARE_LAYOUT
*&---------------------------------------------------------------------*
*&      Form  FAN_HELP
*&---------------------------------------------------------------------*
* FAN Help
*----------------------------------------------------------------------*
FORM fan_help CHANGING fc_v_fan.


  IF gt_fan_help[] IS INITIAL.
    SELECT fan_id description                           "#EC CI_NOFIELD
      FROM zarn_products                                "#EC CI_NOWHERE
      INTO CORRESPONDING FIELDS OF TABLE gt_fan_help[].

    SORT gt_fan_help[] BY fan_id.
    DELETE ADJACENT DUPLICATES FROM gt_fan_help[] COMPARING fan_id.
  ENDIF.

  IF gt_fan_help[] IS NOT INITIAL.
    CALL FUNCTION 'F4IF_INT_TABLE_VALUE_REQUEST'
      EXPORTING
        retfield        = 'FAN_ID'
        value_org       = 'S'
      TABLES
        value_tab       = gt_fan_help[]
        return_tab      = gt_return_help[]
      EXCEPTIONS
        parameter_error = 1
        no_values_found = 2
        OTHERS          = 3.
    IF sy-subrc = 0.
      CLEAR gs_return_help.
      READ TABLE gt_return_help[] INTO gs_return_help INDEX 1.
      IF sy-subrc = 0.
        fc_v_fan = gs_return_help-fieldval.
      ENDIF.
    ENDIF.

  ENDIF.

ENDFORM.                    " FAN_HELP
*&---------------------------------------------------------------------*
*&      Form  VER_HELP
*&---------------------------------------------------------------------*
* Version Help
*----------------------------------------------------------------------*
FORM ver_help CHANGING fc_v_ver.

  IF gt_ver_help[] IS INITIAL.
    IF p_fan IS NOT INITIAL.
      SELECT b~fan_id a~version
        INTO CORRESPONDING FIELDS OF TABLE gt_ver_help[]
        FROM zarn_prd_version AS a
        JOIN zarn_products AS b
        ON a~idno = b~idno
        WHERE b~fan_id = p_fan.

      SORT gt_ver_help[] BY version.
      DELETE ADJACENT DUPLICATES FROM gt_ver_help[] COMPARING version.
    ENDIF.
  ENDIF.

  IF gt_ver_help[] IS NOT INITIAL AND p_fan IS NOT INITIAL.
    CALL FUNCTION 'F4IF_INT_TABLE_VALUE_REQUEST'
      EXPORTING
        retfield        = 'VERSION'
        value_org       = 'S'
      TABLES
        value_tab       = gt_ver_help[]
        return_tab      = gt_return_help[]
      EXCEPTIONS
        parameter_error = 1
        no_values_found = 2
        OTHERS          = 3.
    IF sy-subrc = 0.
      CLEAR gs_return_help.
      READ TABLE gt_return_help[] INTO gs_return_help INDEX 1.
      IF sy-subrc = 0.
        fc_v_ver = gs_return_help-fieldval.
      ENDIF.
    ENDIF.

  ENDIF.

ENDFORM.                    " VER_HELP
*&---------------------------------------------------------------------*
*&      Form  CHG_AREA_HELP
*&---------------------------------------------------------------------*
* Change Area Help
*----------------------------------------------------------------------*
FORM chg_area_help CHANGING fc_v_chg_area.

  IF gt_chg_area_help[] IS INITIAL.
    SELECT domvalue_l AS chg_area
           ddtext     AS desc
      INTO CORRESPONDING FIELDS OF TABLE gt_chg_area_help[]
      FROM dd07v
      WHERE domname = 'ZARN_ARENA_TABLE_TYPE_D'
        AND ( domvalue_l = 'N' OR domvalue_l = 'R' ).
  ENDIF.

  IF gt_chg_area_help[] IS NOT INITIAL.
    CALL FUNCTION 'F4IF_INT_TABLE_VALUE_REQUEST'
      EXPORTING
        retfield        = 'CHG_AREA'
        value_org       = 'S'
      TABLES
        value_tab       = gt_chg_area_help[]
        return_tab      = gt_return_help[]
      EXCEPTIONS
        parameter_error = 1
        no_values_found = 2
        OTHERS          = 3.
    IF sy-subrc = 0.
      CLEAR gs_return_help.
      READ TABLE gt_return_help[] INTO gs_return_help INDEX 1.
      IF sy-subrc = 0.
        fc_v_chg_area = gs_return_help-fieldval.
      ENDIF.
    ENDIF.

  ENDIF.

ENDFORM.                    " CHG_AREA_HELP
*&---------------------------------------------------------------------*
*&      Form  CHG_TABLE_HELP
*&---------------------------------------------------------------------*
* Change Table Help
*----------------------------------------------------------------------*
FORM chg_table_help  CHANGING fc_v_chg_table.

  IF gt_chg_tab_help[] IS INITIAL.
    SELECT arena_table      AS chg_tab
           arena_table_type AS chg_area
      INTO CORRESPONDING FIELDS OF TABLE gt_chg_tab_help[]
      FROM zarn_table_mastr
      WHERE arena_table_type = 'N' OR arena_table_type = 'R'.
  ENDIF.

  IF gt_chg_tab_help[] IS NOT INITIAL.
    CALL FUNCTION 'F4IF_INT_TABLE_VALUE_REQUEST'
      EXPORTING
        retfield        = 'CHG_TAB'
        value_org       = 'S'
      TABLES
        value_tab       = gt_chg_tab_help[]
        return_tab      = gt_return_help[]
      EXCEPTIONS
        parameter_error = 1
        no_values_found = 2
        OTHERS          = 3.
    IF sy-subrc = 0.
      CLEAR gs_return_help.
      READ TABLE gt_return_help[] INTO gs_return_help INDEX 1.
      IF sy-subrc = 0.
        fc_v_chg_table = gs_return_help-fieldval.
      ENDIF.
    ENDIF.
  ENDIF.


ENDFORM.                    " CHG_TABLE_HELP
