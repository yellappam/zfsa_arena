*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_ARTICLE_UPDATE_CONSTANTS
*&---------------------------------------------------------------------*

CONSTANTS: gc_stvarv_post_dest TYPE rvari_vnam VALUE 'ZARN_ARTICLE_POST_LOGSYS',
           gc_type_p           TYPE rsscr_kind VALUE 'P'.
