*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_DELTA_ANALYSIS_SEL
*&---------------------------------------------------------------------*

TABLES : zarn_products, zarn_cc_det, cdhdr.

DATA: gv_art_update      TYPE zarn_article_update.

SELECTION-SCREEN: BEGIN OF BLOCK b1 WITH FRAME TITLE text-001.

PARAMETERS: p_fan     TYPE zarn_products-fan_id          MODIF ID sin.

SELECTION-SCREEN: BEGIN OF LINE.
SELECTION-SCREEN COMMENT 1(31) text-003 FOR FIELD p_ver1 MODIF ID sin.
PARAMETERS: p_ver1    TYPE zarn_prd_version-version      MODIF ID sin.
SELECTION-SCREEN COMMENT 50(50) text-004                 MODIF ID sin.
SELECTION-SCREEN: END OF LINE.

SELECTION-SCREEN: BEGIN OF LINE.
SELECTION-SCREEN COMMENT 1(31) text-005 FOR FIELD p_ver2 MODIF ID sin.
PARAMETERS: p_ver2    TYPE zarn_prd_version-version      MODIF ID sin.
SELECTION-SCREEN COMMENT 50(50) text-006                 MODIF ID sin.
SELECTION-SCREEN: END OF LINE.


PARAMETERS: p_ver3  TYPE zarn_prd_version-version MODIF ID sin,
            p_ver4  TYPE zarn_prd_version-version MODIF ID sin,
            p_ver5  TYPE zarn_prd_version-version MODIF ID sin,
            p_matnr TYPE matnr                    MODIF ID art.

SELECTION-SCREEN: END OF BLOCK b1.


SELECTION-SCREEN: BEGIN OF BLOCK b2 WITH FRAME TITLE text-002.

SELECT-OPTIONS: s_fan    FOR zarn_products-fan_id       MODIF ID sfn,
                s_chgcat FOR zarn_cc_det-chg_category   NO INTERVALS,
                s_chgind FOR zarn_cc_det-chg_ind        NO INTERVALS,
                s_artupd FOR gv_art_update              NO INTERVALS,
                s_chgara FOR zarn_cc_det-chg_area       NO INTERVALS,
                s_chgtab FOR zarn_cc_det-chg_table      NO INTERVALS,
                s_chgdat FOR cdhdr-udate                              NO-DISPLAY,
                s_chgtim FOR cdhdr-utime                              NO-DISPLAY,
                s_chgby  FOR cdhdr-username             NO INTERVALS  NO-DISPLAY.

SELECTION-SCREEN SKIP.

PARAMETERS: p_multi AS CHECKBOX USER-COMMAND multi.

SELECTION-SCREEN: END OF BLOCK b2.






********************************************
INITIALIZATION.
********************************************

  IF s_chgcat[] IS INITIAL.
    s_chgcat-sign   = 'I'.
    s_chgcat-option = 'NE'.
    s_chgcat-low    = 'NR'.
    APPEND s_chgcat TO s_chgcat[].
  ENDIF.


*************************************************
AT SELECTION-SCREEN ON VALUE-REQUEST FOR p_fan.
*************************************************
  PERFORM fan_help CHANGING p_fan.

******************************************************
AT SELECTION-SCREEN ON VALUE-REQUEST FOR s_fan-low.
******************************************************
  PERFORM fan_help CHANGING s_fan-low.

******************************************************
AT SELECTION-SCREEN ON VALUE-REQUEST FOR s_fan-high.
******************************************************
  PERFORM fan_help CHANGING s_fan-high.

*************************************************
AT SELECTION-SCREEN ON VALUE-REQUEST FOR p_ver1.
*************************************************
  PERFORM ver_help CHANGING p_ver1.

*************************************************
AT SELECTION-SCREEN ON VALUE-REQUEST FOR p_ver2.
*************************************************
  PERFORM ver_help CHANGING p_ver2.

*************************************************
AT SELECTION-SCREEN ON VALUE-REQUEST FOR p_ver3.
*************************************************
  PERFORM ver_help CHANGING p_ver3.

*************************************************
AT SELECTION-SCREEN ON VALUE-REQUEST FOR p_ver4.
*************************************************
  PERFORM ver_help CHANGING p_ver4.

*************************************************
AT SELECTION-SCREEN ON VALUE-REQUEST FOR p_ver5.
*************************************************
  PERFORM ver_help CHANGING p_ver5.

*************************************************
AT SELECTION-SCREEN ON VALUE-REQUEST FOR s_chgara-low.
*************************************************
  PERFORM chg_area_help CHANGING s_chgara-low.

*************************************************
AT SELECTION-SCREEN ON VALUE-REQUEST FOR s_chgtab-low.
*************************************************
  PERFORM chg_table_help CHANGING s_chgtab-low.


********************************************
AT SELECTION-SCREEN OUTPUT.
********************************************

  LOOP AT SCREEN.

* For Multiple Articles
    IF p_multi = abap_true.

      IF screen-group1 EQ 'SFN'.
        screen-active = 1.
      ENDIF.

      IF screen-group1 EQ 'SIN'.
        screen-active = 0.
      ENDIF.

      IF screen-group1 EQ 'ART'.
        screen-active = 0.
      ENDIF.

    ELSE.
* For Single Articles

      IF screen-group1 EQ 'SFN'.
        screen-active = 0.
      ENDIF.

      IF screen-group1 EQ 'SIN'.
        screen-active = 1.
      ENDIF.

      IF screen-group1 EQ 'ART'.
        screen-input = 0.
        screen-active = 1.
      ENDIF.

    ENDIF.


    MODIFY SCREEN.
  ENDLOOP.









********************************************
AT SELECTION-SCREEN.
********************************************

  CLEAR gv_error.

  LOOP AT SCREEN.

* For Multiple Articles
    IF p_multi = abap_true.

      IF screen-group1 EQ 'SFN'.
        screen-active = 1.
      ENDIF.

      IF screen-group1 EQ 'SIN'.
        screen-active = 0.
      ENDIF.

      IF screen-group1 EQ 'ART'.
        screen-active = 0.
      ENDIF.

    ELSE.
* For Single Articles

      IF screen-group1 EQ 'SFN'.
        screen-active = 0.
      ENDIF.

      IF screen-group1 EQ 'SIN'.
        screen-active = 1.
      ENDIF.

      IF screen-group1 EQ 'ART'.
        screen-input = 0.
        screen-active = 1.
      ENDIF.

    ENDIF.


    MODIFY SCREEN.
  ENDLOOP.



  IF p_multi IS INITIAL.

* Get Initial Data for Single FAN
    PERFORM get_initial_data_single.

* FAN ID, Version 1 and Version 2 are mandatory for Single Article Analysis
    IF p_fan IS INITIAL OR
*       p_ver1 IS INITIAL OR
       p_ver2 IS INITIAL.
      MESSAGE s050 DISPLAY LIKE 'E'.
      gv_error = abap_true.
      EXIT.
    ENDIF.


* Validate Single FAN ID Versions
    PERFORM validate_single_fan_versions.


  ENDIF.  " IF p_multi IS INITIAL
