*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 24.02.2017 at 11:24:02
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_REL_MATRIXV................................*
TABLES: ZARN_REL_MATRIXV, *ZARN_REL_MATRIXV. "view work areas
CONTROLS: TCTRL_ZARN_REL_MATRIXV
TYPE TABLEVIEW USING SCREEN '0001'.
DATA: BEGIN OF STATUS_ZARN_REL_MATRIXV. "state vector
          INCLUDE STRUCTURE VIMSTATUS.
DATA: END OF STATUS_ZARN_REL_MATRIXV.
* Table for entries selected to show on screen
DATA: BEGIN OF ZARN_REL_MATRIXV_EXTRACT OCCURS 0010.
INCLUDE STRUCTURE ZARN_REL_MATRIXV.
          INCLUDE STRUCTURE VIMFLAGTAB.
DATA: END OF ZARN_REL_MATRIXV_EXTRACT.
* Table for all entries loaded from database
DATA: BEGIN OF ZARN_REL_MATRIXV_TOTAL OCCURS 0010.
INCLUDE STRUCTURE ZARN_REL_MATRIXV.
          INCLUDE STRUCTURE VIMFLAGTAB.
DATA: END OF ZARN_REL_MATRIXV_TOTAL.

*.........table declarations:.................................*
TABLES: ZARN_CC_DESC                   .
TABLES: ZARN_REL_MATRIX                .
TABLES: ZARN_REL_STATUST               .
TABLES: ZARN_TEAMS                     .
