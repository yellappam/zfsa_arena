*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 31.05.2017 at 16:34:14
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_ALLERGEN_T.................................*
DATA:  BEGIN OF STATUS_ZARN_ALLERGEN_T               .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_ALLERGEN_T               .
CONTROLS: TCTRL_ZARN_ALLERGEN_T
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_ALLERGEN_T               .
TABLES: ZARN_ALLERGEN_T                .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
