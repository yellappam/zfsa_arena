*----------------------------------------------------------------------*
*       CLASS ZCL_ARN_GUI_LOAD DEFINITION
*----------------------------------------------------------------------*
*
*----------------------------------------------------------------------*
class ZCL_ARN_GUI_LOAD definition
  public
  final
  create public .

public section.

  types TY_S_REF_ARTICLE type MARA .
  types:
    ty_t_ref_article TYPE SORTED TABLE OF ty_s_ref_article WITH NON-UNIQUE KEY matkl .
  types:
    BEGIN OF ty_s_added_pir,
        uom_code              TYPE zarn_uom_code,
        base_units_per_pallet	TYPE zarn_base_units_per_pallet,
        qty_per_pallet_layer  TYPE zarn_qty_per_pallet_layer,
        qty_layers_per_pallet	TYPE zarn_qty_layers_per_pallet,
        qty_units_per_pallet  TYPE zarn_qty_units_per_pallet,
      END OF ty_s_added_pir .
  types:
    ty_t_added_pir TYPE SORTED TABLE OF ty_s_added_pir
                                                       WITH NON-UNIQUE KEY uom_code
                                                                           base_units_per_pallet
                                                                           qty_per_pallet_layer
                                                                           qty_layers_per_pallet
                                                                           qty_units_per_pallet .
  types:
    BEGIN OF ty_s_uom_var_lower,
        idno                 TYPE zarn_idno,
        version              TYPE zarn_version,
        uom_category         TYPE zarn_uom_category,
        uom_code             TYPE zarn_uom_code,
        hybris_internal_code TYPE zarn_hybris_internal_code,
        lower_uom            TYPE zarn_lower_uom,
        lower_child_uom      TYPE zarn_lower_child_uom,
        meinh                TYPE zarn_sap_uom,
        lower_meinh          TYPE zarn_lower_sap_uom,
        product_uom          TYPE zarn_product_uom,
        base_unit            TYPE zarn_base_unit,
        consumer_unit        TYPE zarn_consumer_unit,
        despatch_unit        TYPE zarn_despatch_unit,
        an_invoice_unit      TYPE zarn_an_invoice_unit,
        cat_seqno            TYPE zarn_cat_seqno,
        seqno                TYPE zarn_seqno,
      END OF ty_s_uom_var_lower .
  types:
    ty_t_uom_var_lower TYPE STANDARD TABLE OF ty_s_uom_var_lower .
  types:
    BEGIN OF ty_s_pir_vendor,
        bbbnr TYPE bbbnr,
        bbsnr TYPE bbsnr,
        bubkz TYPE bubkz,
        lifnr TYPE lifnr,
      END OF ty_s_pir_vendor .
  types:
    ty_t_pir_vendor TYPE SORTED TABLE OF ty_s_pir_vendor WITH NON-UNIQUE KEY bbbnr bbsnr bubkz .
  types:
    BEGIN OF ty_s_marm_idno,
        idno         TYPE zarn_idno,
        meinh        TYPE lrmei,
        umrez        TYPE umrez,
        umren        TYPE umren,
        mesub        TYPE mesub,
        matnr        TYPE matnr,
        uom_category TYPE zarn_uom_category,
        seqno        TYPE zarn_seqno,
        uom          TYPE meinh,
        cat_seqno    TYPE zarn_cat_seqno,
      END OF ty_s_marm_idno .
  types:
    ty_t_marm_idno  TYPE STANDARD TABLE OF ty_s_marm_idno .
  types TY_S_TABLE_MASTR type ZARN_TABLE_MASTR .
  types:
    ty_t_table_mastr TYPE STANDARD TABLE OF zarn_table_mastr .
  types TY_S_LFM1 type LFM1 .
  types:
    ty_t_lfm1 TYPE SORTED TABLE OF ty_s_lfm1 WITH UNIQUE KEY lifnr ekorg .
  types TY_S_FANID type MARA .
  types:
    ty_t_fanid TYPE SORTED TABLE OF ty_s_fanid WITH NON-UNIQUE KEY zzfan .
  types TY_S_MARM type MARM .
  types:
    ty_t_marm TYPE SORTED TABLE OF ty_s_marm WITH UNIQUE KEY matnr meinh .
  types TY_S_MEAN type MEAN .
  types:
    ty_t_mean TYPE SORTED TABLE OF ty_s_mean WITH UNIQUE KEY matnr meinh lfnum .
  types TY_S_PROD_UOM_T type ZARN_PROD_UOM_T .
  types:
    ty_t_prod_uom_t TYPE SORTED TABLE OF ty_s_prod_uom_t WITH UNIQUE KEY product_uom .
  types TY_S_UOM_CAT type ZARN_UOM_CAT .
  types:
    ty_t_uom_cat TYPE SORTED TABLE OF ty_s_uom_cat WITH UNIQUE KEY uom_category seqno uom .
  types TY_S_UOMCATEGORY type ZARN_UOMCATEGORY .
  types:
    ty_t_uomcategory TYPE SORTED TABLE OF ty_s_uomcategory WITH UNIQUE KEY uom_category .
  types TY_S_GEN_UOM_T type ZARN_GEN_UOM_T .
  types:
    ty_t_gen_uom_t TYPE SORTED TABLE OF ty_s_gen_uom_t WITH UNIQUE KEY gen_uom spras .
  types TY_S_HOST_PRDTYP type ZMD_HOST_PRDTYP .
  types:
    ty_t_host_prdtyp TYPE SORTED TABLE OF ty_s_host_prdtyp WITH UNIQUE KEY wwgha .
  types TY_S_MC_SUBDEP type ZMD_MC_SUBDEP .
  types:
    ty_t_mc_subdep TYPE SORTED TABLE OF ty_s_mc_subdep WITH UNIQUE KEY matkl .
  types:
    ty_t_range_frzd_chld TYPE RANGE OF mara-zzprdtype .
  types:
    ty_t_range_greet_mc TYPE RANGE OF matkl .

  class-data ST_T006 type TT_T006 .
  constants GC_RETAIL type ZARN_UOM_CATEGORY value 'RETAIL' ##NO_TEXT.
  constants GC_INNER type ZARN_UOM_CATEGORY value 'INNER' ##NO_TEXT.
  constants GC_SHIPPER type ZARN_UOM_CATEGORY value 'SHIPPER' ##NO_TEXT.
  constants GC_LAYER type ZARN_UOM_CATEGORY value 'LAYER' ##NO_TEXT.
  constants GC_PALLET type ZARN_UOM_CATEGORY value 'PALLET' ##NO_TEXT.
  data GT_TABNAME type TY_T_TABLE_MASTR .
  data GT_GUI_DEF type ZTARN_GUI_DEF .
  data GT_GUI_SEL type ZTARN_GUI_SEL .
  data GT_GUI_SET type ZTARN_GUI_SET .
  data GC_LOGIC_T type ZARN_E_GUI_LOGIC_TYPE value 'T' ##NO_TEXT.
  data GC_LOGIC_L type ZARN_E_GUI_LOGIC_TYPE value 'L' ##NO_TEXT.
  data GC_LOGIC_C type ZARN_E_GUI_LOGIC_TYPE value 'C' ##NO_TEXT.
  data GC_ATTRIB_D type ZARN_E_GUI_ATTRIBUTE value 'D' ##NO_TEXT.
  data GC_ATTRIB_R type ZARN_E_GUI_ATTRIBUTE value 'R' ##NO_TEXT.
  data GC_ATTRIB_O type ZARN_E_GUI_ATTRIBUTE value 'O' ##NO_TEXT.
  data GC_ATTRIB_H type ZARN_E_GUI_ATTRIBUTE value 'H' ##NO_TEXT.
  data GC_TABLE_TYPE_CC type ZARN_ARENA_TABLE_TYPE value 'CC' ##NO_TEXT.
  data GC_TABLE_TYPE_C type ZARN_ARENA_TABLE_TYPE value 'C' ##NO_TEXT.
  data GC_TABLE_TYPE_T type ZARN_ARENA_TABLE_TYPE value 'T' ##NO_TEXT.
  data GC_TABLE_TYPE_N type ZARN_ARENA_TABLE_TYPE value 'N' ##NO_TEXT.
  data GC_TABLE_TYPE_R type ZARN_ARENA_TABLE_TYPE value 'R' ##NO_TEXT.
  data GC_SELECTED type XFELD value 'X' ##NO_TEXT.
  data GT_GUI_DEF_RECON type ZTARN_GUI_DEF .
  data GT_LFM1 type TY_T_LFM1 .
  data GT_FANID type TY_T_FANID .
  data GT_MARM type TY_T_MARM .
  data GT_MARM_IDNO type TY_T_MARM_IDNO .
  data GT_MEAN type TY_T_MEAN .
  data GT_PROD_UOM_T type TY_T_PROD_UOM_T .
  data GT_UOM_CAT type TY_T_UOM_CAT .
  data GT_UOMCATEGORY type TY_T_UOMCATEGORY .
  data GT_GEN_UOM_T type TY_T_GEN_UOM_T .
  data GT_REF_ARTICLE type TY_T_REF_ARTICLE .
  data GT_PIR_VENDOR type TY_T_PIR_VENDOR .
  data GT_TVARV_EAN_CATE type TVARVC_T .
  data GT_HOST_PRDTYP type TY_T_HOST_PRDTYP .
  data GT_MC_SUBDEP type TY_T_MC_SUBDEP .
  data GT_RANGE_FRZD_CHLD type TY_T_RANGE_FRZD_CHLD .
  data GT_RANGE_GREET_MC type TY_T_RANGE_GREET_MC .
  data GS_GREET_LIMIT type TVARVC .
  data GV_PIR_EKORG type EKORG .
  data GT_NET_QTY type ZTARN_NET_QTY .

  methods CONSTRUCTOR
    raising
      ZCX_ARN_GUI_LOAD_ERR .
  methods GET_DEFAULT_LOGIC
    importing
      !IV_TABNAME type TABNAME
      !IV_FLDNAME type FIELDNAME
    exporting
      !ET_GUI_DEF type ZTARN_GUI_DEF .
  methods GET_DEFAULT_VALUES
    importing
      !IT_GUI_DEF type ZTARN_GUI_DEF
    exporting
      !EV_VALUE type TEXT100 .
  methods SET_REGIONAL_DEFAULT
    importing
      !IS_REG_DATA type ZSARN_REG_DATA
      !IS_PROD_DATA type ZSARN_PROD_DATA
      !IT_LFM1 type TY_T_LFM1 optional
      !IT_FANID type TY_T_FANID optional
      !IT_MARM type TY_T_MARM optional
      !IT_MARM_IDNO type TY_T_MARM_IDNO optional
      !IT_MEAN type TY_T_MEAN optional
      !IT_PROD_UOM_T type TY_T_PROD_UOM_T optional
      !IT_UOM_CAT type TY_T_UOM_CAT optional
      !IT_UOMCATEGORY type TY_T_UOMCATEGORY optional
      !IT_GEN_UOM_T type TY_T_GEN_UOM_T optional
      !IT_REF_ARTICLE type TY_T_REF_ARTICLE optional
      !IT_PIR_VENDOR type TY_T_PIR_VENDOR optional
      !IT_TVARV_EAN_CATE type TVARVC_T optional
      !IT_HOST_PRDTYP type TY_T_HOST_PRDTYP optional
      !IT_MC_SUBDEP type TY_T_MC_SUBDEP optional
      !IV_PIR_EKORG type EKORG optional
      !IV_REG_ONLY type XFELD default SPACE
    exporting
      !ES_REG_DATA_DEF type ZSARN_REG_DATA
      !ET_REG_UOM_UI type ZTARN_REG_UOM_UI
      !ET_REG_PIR_UI type ZTARN_REG_PIR_UI
      !ET_REG_EAN_UI type ZTARN_REG_EAN_UI
      !EV_NO_UOM_AVAIL type FLAG
      !EV_NO_UOM_AVAIL_EAN type FLAG
      !EV_DIM_OVERFLOW_ERROR type FLAG .
  methods SET_REG_PIR_DEFAULT
    importing
      !IS_REG_DATA type ZSARN_REG_DATA
      !IS_PROD_DATA type ZSARN_PROD_DATA
      !IT_REG_UOM_UI type ZTARN_REG_UOM_UI
      !IV_REG_ONLY type XFELD default SPACE
    exporting
      !ES_REG_DATA_DEF type ZSARN_REG_DATA
      !ET_REG_PIR_UI type ZTARN_REG_PIR_UI .
  methods GET_INITIAL_DATA
    importing
      !IS_PROD_DATA type ZSARN_PROD_DATA .
  methods SET_REGIONAL_DEFAULT_UI
    importing
      !IS_REG_DATA type ZSARN_REG_DATA
      !IS_PROD_DATA type ZSARN_PROD_DATA
      !IT_LFM1 type TY_T_LFM1 optional
      !IT_FANID type TY_T_FANID optional
      !IT_MARM type TY_T_MARM optional
      !IT_MARM_IDNO type TY_T_MARM_IDNO optional
      !IT_MEAN type TY_T_MEAN optional
      !IT_PROD_UOM_T type TY_T_PROD_UOM_T optional
      !IT_UOM_CAT type TY_T_UOM_CAT optional
      !IT_UOMCATEGORY type TY_T_UOMCATEGORY optional
      !IT_GEN_UOM_T type TY_T_GEN_UOM_T optional
      !IT_REF_ARTICLE type TY_T_REF_ARTICLE optional
      !IT_PIR_VENDOR type TY_T_PIR_VENDOR optional
      !IT_TVARV_EAN_CATE type TVARVC_T optional
      !IT_HOST_PRDTYP type TY_T_HOST_PRDTYP optional
      !IT_MC_SUBDEP type TY_T_MC_SUBDEP optional
      !IV_PIR_EKORG type EKORG optional
      !IV_REG_ONLY type XFELD default SPACE
    exporting
      !ES_REG_DATA_DEF type ZSARN_REG_DATA
      !ET_REG_UOM_UI type ZTARN_REG_UOM_UI
      !ET_REG_PIR_UI type ZTARN_REG_PIR_UI
      !ET_REG_EAN_UI type ZTARN_REG_EAN_UI
      !ET_REG_ALLERGEN_UI type ZTARN_REG_ALLERG_UI .
  class-methods UNIT_CONVERSION_SIMPLE
    importing
      value(INPUT) type ANY
      value(NO_TYPE_CHECK) type ANY default SPACE
      value(ROUND_SIGN) type ANY default SPACE
      value(UNIT_IN) type T006-MSEHI default SPACE
      value(UNIT_OUT) type T006-MSEHI default SPACE
      value(IV_GET_SAP_UOM) type FLAG default 'X'
    exporting
      value(ADD_CONST) type ANY
      value(DECIMALS) type ANY
      value(DENOMINATOR) type ANY
      value(NUMERATOR) type ANY
      value(OUTPUT) type ANY
      value(EV_ERROR) type FLAG .
protected section.
private section.

  types:
    tt_gui_def           TYPE SORTED TABLE OF zarn_gui_def WITH UNIQUE KEY tabname fldname logic_ty .
  types:
    tt_allergen_text TYPE SORTED TABLE OF zarn_allergen_t WITH UNIQUE KEY allergen_type .

  data GV_INITIALIZED type FLAG .
  data GT_ALLERGEN_TEXT type TT_ALLERGEN_TEXT .

  methods SET_REG_SC_DEFAULT_UI
    importing
      !IT_REG_EAN_UI type ZTARN_REG_EAN_UI
    changing
      !CT_REG_UOM_UI type ZTARN_REG_UOM_UI .
  methods SET_REG_HSNO_DEFAULT
    importing
      !IS_REG_DATA type ZSARN_REG_DATA
      !IS_PROD_DATA type ZSARN_PROD_DATA
      !IV_REG_ONLY type XFELD default SPACE
    exporting
      !ES_REG_DATA_DEF type ZSARN_REG_DATA .
  methods SET_REG_UOM_EAN_GREETING_DEF
    importing
      !IS_REG_DATA type ZSARN_REG_DATA
      !IS_PROD_DATA type ZSARN_PROD_DATA
      !IT_REG_UOM_UI type ZTARN_REG_UOM_UI
      !IV_REG_ONLY type XFELD default SPACE
    exporting
      !ES_REG_DATA_DEF type ZSARN_REG_DATA
      !ET_REG_UOM_UI type ZTARN_REG_UOM_UI
      !ET_REG_EAN_UI type ZTARN_REG_EAN_UI
      !EV_NO_UOM_AVAIL type FLAG .
  methods SET_REG_EAN_DEFAULT
    importing
      !IS_REG_DATA type ZSARN_REG_DATA
      !IS_PROD_DATA type ZSARN_PROD_DATA
      !IT_REG_UOM_UI type ZTARN_REG_UOM_UI
      !IV_REG_ONLY type XFELD default SPACE
    exporting
      !ES_REG_DATA_DEF type ZSARN_REG_DATA
      !ET_REG_EAN_UI type ZTARN_REG_EAN_UI .
  methods SET_REG_UOM_DEFAULT
    importing
      !IS_REG_DATA type ZSARN_REG_DATA
      !IS_PROD_DATA type ZSARN_PROD_DATA
      !IV_REG_ONLY type XFELD default SPACE
    exporting
      !ES_REG_DATA_DEF type ZSARN_REG_DATA
      !ET_REG_UOM_UI type ZTARN_REG_UOM_UI
      !EV_NO_UOM_AVAIL type FLAG
      !EV_DIM_OVERFLOW_ERROR type FLAG .
  methods SET_REG_ALLERGEN_DEFAULT
    importing
      !IS_REG_DATA type ZSARN_REG_DATA
      !IS_PROD_DATA type ZSARN_PROD_DATA
    changing
      !CT_REG_ALLERGEN type ZTARN_REG_ALLERG .
  methods SET_REG_HDR_DEFAULT
    importing
      !IS_REG_DATA type ZSARN_REG_DATA
      !IS_PROD_DATA type ZSARN_PROD_DATA
      !IS_REG_HDR_DB type ZARN_REG_HDR
      !IV_REG_ONLY type XFELD default SPACE
    exporting
      !ES_REG_DATA_DEF type ZSARN_REG_DATA .
  methods SET_REG_NUTRIENTS_DEFAULT
    importing
      !IS_PROD_DATA type ZSARN_PROD_DATA
    changing
      !CT_REG_NUTRIEN type ZTARN_REG_NUTRIEN .
  methods SET_REG_STRING_DEFAULT
    importing
      !IS_PROD_DATA type ZSARN_PROD_DATA
    changing
      !CT_REG_STR type ZTARN_REG_STR .
  methods SET_REG_EAN_DEFAULT_UI
    importing
      !IS_REG_DATA type ZSARN_REG_DATA
      !IS_PROD_DATA type ZSARN_PROD_DATA
      !IT_REG_UOM_UI type ZTARN_REG_UOM_UI
      !IV_REG_ONLY type XFELD default SPACE
    exporting
      !ES_REG_DATA_DEF type ZSARN_REG_DATA
      !ET_REG_EAN_UI type ZTARN_REG_EAN_UI .
  methods SET_REG_PIR_DEFAULT_UI
    importing
      !IS_REG_DATA type ZSARN_REG_DATA
      !IS_PROD_DATA type ZSARN_PROD_DATA
      !IT_REG_UOM_UI type ZTARN_REG_UOM_UI
      !IV_REG_ONLY type XFELD default SPACE
    exporting
      !ES_REG_DATA_DEF type ZSARN_REG_DATA
      !ET_REG_PIR_UI type ZTARN_REG_PIR_UI .
  methods SET_REG_UOM_DEFAULT_UI
    importing
      !IS_REG_DATA type ZSARN_REG_DATA
      !IS_PROD_DATA type ZSARN_PROD_DATA
      !IV_REG_ONLY type XFELD default SPACE
    exporting
      !ET_REG_UOM_UI type ZTARN_REG_UOM_UI .
  methods SET_REG_UOM_DEFAULT_INT
    importing
      !IS_REG_DATA type ZSARN_REG_DATA
      !IS_PROD_DATA type ZSARN_PROD_DATA
      !IT_REG_UOM_DB type ZTARN_REG_UOM
      !IT_GUI_DEF type TT_GUI_DEF
    exporting
      !EV_DIM_OVERFLOW_ERROR type FLAG
      !ET_REG_UOM_EXIST type ZTARN_REG_UOM_UI
      !ET_REG_UOM_UI type ZTARN_REG_UOM_UI .
  methods RETAIN_REGIONAL_UOM_DB_DATA
    importing
      !IT_REG_UOM_DB type ZTARN_REG_UOM
      !IT_GUI_DEF type TT_GUI_DEF
      !IS_REG_DATA type ZSARN_REG_DATA
      !IS_PROD_DATA type ZSARN_PROD_DATA optional
    changing
      !CT_REG_UOM_UI type ZTARN_REG_UOM_UI
    returning
      value(RT_REG_UOM_EXIST) type ZTARN_REG_UOM_UI .
  methods MAP_PIR_TO_REG_UOM
    importing
      !IS_PROD_DATA type ZSARN_PROD_DATA
    changing
      !CT_REG_UOM_UI type ZTARN_REG_UOM_UI .
  methods MAP_UOM_VARIANT_TO_REG_UOM
    importing
      !IS_PROD_DATA type ZSARN_PROD_DATA
    exporting
      !EV_DIM_OVERFLOW_ERROR type FLAG
    returning
      value(RT_REG_UOM_UI) type ZTARN_REG_UOM_UI .
  methods MAP_NAT_UOM_TO_REG_UOM_STRU
    importing
      !IS_UOM_VARIANT type ZARN_UOM_VARIANT
    changing
      !CS_REG_UOM_NAT_DATA type ZSARN_REG_UOM_NAT_DATA .
  methods SET_REG_ALLERGEN_DEFAULT_UI
    importing
      !IS_REG_DATA type ZSARN_REG_DATA
      !IS_PROD_DATA type ZSARN_PROD_DATA
    exporting
      !ET_REG_ALLERGEN_UI type ZTARN_REG_ALLERG_UI .
  methods READ_GLOBAL_DATA .
ENDCLASS.



CLASS ZCL_ARN_GUI_LOAD IMPLEMENTATION.


METHOD constructor.

* Get Tables to be checked for National and Regional Database fields
  SELECT DISTINCT a~arena_table
                  a~arena_table_type
    INTO CORRESPONDING FIELDS OF TABLE gt_tabname[]
    FROM zarn_table_mastr AS a
    JOIN zarn_field_confg AS b
      ON a~arena_table  = b~tabname
    WHERE ( a~arena_table_type = gc_table_type_n OR
            a~arena_table_type = gc_table_type_r OR
            a~arena_table_type = gc_table_type_c OR
            a~arena_table_type = gc_table_type_cc OR
            a~arena_table_type = gc_table_type_t ).

  IF gt_tabname[] IS NOT INITIAL.

    "fetch AReNa tables for GUI Settings
    SELECT * FROM zarn_gui_set INTO TABLE gt_gui_set[]
             FOR ALL ENTRIES IN gt_tabname[]
             WHERE tabname   = gt_tabname-arena_table.

    IF gt_gui_set[] IS NOT INITIAL.
* filter field selection reference
      SELECT * FROM zarn_gui_sel INTO TABLE gt_gui_sel[]
               FOR ALL ENTRIES IN gt_gui_set[]
               WHERE field_grp = gt_gui_set-field_grp.

* select Default relevant fields
      SELECT * FROM zarn_gui_def INTO TABLE gt_gui_def[]
               FOR ALL ENTRIES IN gt_gui_set[]
               WHERE tabname EQ gt_gui_set-tabname
                 AND fldname EQ gt_gui_set-fldname.

    ENDIF.  " IF gt_gui_set[] IS NOT INITIAL
  ENDIF.  " IF gt_tabname[] IS NOT INITIAL

ENDMETHOD.


METHOD get_default_logic.
  DATA:
    wa_gui_def LIKE LINE OF gt_gui_def.

  LOOP AT gt_gui_def INTO wa_gui_def
                     WHERE tabname EQ iv_tabname
                     AND fldname   EQ iv_fldname.
    APPEND wa_gui_def TO et_gui_def.
  ENDLOOP.

ENDMETHOD.


METHOD get_default_values.

  DATA:
          wa_gui_def LIKE LINE OF gt_gui_def.

  DATA: lv_table_field_name   TYPE string.

** This logic is required in the program processing as values from reference
** fields are only available at AReNa GUI runtime

*  get defaul;t logic table and work out the sequence

*  T L C

  LOOP AT  it_gui_def INTO wa_gui_def.

    CASE wa_gui_def-logic_ty.

      WHEN 'T'.  "Table and field name
        CONCATENATE wa_gui_def-param1
                    '-'                " Table name
                    wa_gui_def-param1  " Field name
                    INTO lv_table_field_name.

      WHEN 'L'.
*         constant name for class ZZ_CL_->WA_GUI_DEF-PARAM1 returning
*                       the value from parameter WA_GUI_DEF-PARAM2

      WHEN 'C'.
*         simple value from WA_GUI_DEF-PARAM2 assigned to field
        ev_value = wa_gui_def-param2.
    ENDCASE.
  ENDLOOP.

ENDMETHOD.


METHOD get_initial_data.
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 13.12.2018
* CHANGE No.       # WSIWJ-143
* DESCRIPTION      # Fix defaults for Net Content & UoM not coming through
*                  # after switch to using table ZARN_NET_QTY
* WHO              # C90005557, Tessa Newman
*-----------------------------------------------------------------------

  DATA: ls_nat_mc_hier TYPE zarn_nat_mc_hier,
        ls_pir         TYPE zarn_pir,
        lt_pir_vendor  TYPE ty_t_pir_vendor,
        ls_pir_vendor  TYPE ty_s_pir_vendor,
        ls_fanid       TYPE ty_s_fanid,
        ls_marm        TYPE ty_s_marm,
        ls_marm_idno   TYPE ty_s_marm_idno,
        ls_uom_cat     TYPE zarn_uom_cat.


  IF gt_uom_cat[] IS INITIAL.
    SELECT * FROM zarn_uom_cat INTO CORRESPONDING FIELDS OF TABLE gt_uom_cat[].
  ENDIF.

  IF gt_uomcategory[] IS INITIAL.
    SELECT * FROM zarn_uomcategory INTO CORRESPONDING FIELDS OF TABLE gt_uomcategory[].
  ENDIF.


  IF gt_ref_article[] IS INITIAL.

    CLEAR ls_nat_mc_hier.
    READ TABLE is_prod_data-zarn_nat_mc_hier[] INTO ls_nat_mc_hier INDEX 1.
    IF sy-subrc = 0 AND ls_nat_mc_hier-nat_mc_code IS NOT INITIAL.

* Get UOM  from Merch Cat.
      SELECT a~matnr a~matkl a~mtart a~meins a~attyp a~zzfan
        INTO CORRESPONDING FIELDS OF TABLE gt_ref_article[]
        FROM mara AS a
        JOIN t023 AS b
        ON a~matnr = b~wwgda
        WHERE b~matkl = ls_nat_mc_hier-nat_mc_code.

    ENDIF.
  ENDIF. " IF gt_ref_article[] IS INITIAL

  IF gv_pir_ekorg IS INITIAL.
    SELECT SINGLE low INTO gv_pir_ekorg
           FROM tvarvc
           WHERE name EQ 'ZARN_ARTICLE_POST_EINE_EKORG'
             AND type EQ 'S'
             AND high EQ 'X'.
  ENDIF.

  IF gt_pir_vendor[] IS INITIAL OR gt_lfm1[] IS INITIAL.

    CLEAR lt_pir_vendor[].
    LOOP AT is_prod_data-zarn_pir[] INTO ls_pir.
      CLEAR ls_pir_vendor.
      ls_pir_vendor-bbbnr = ls_pir-gln+0(7).
      ls_pir_vendor-bbsnr = ls_pir-gln+7(5).
      ls_pir_vendor-bubkz = ls_pir-gln+12(1).
      INSERT ls_pir_vendor INTO TABLE lt_pir_vendor[].
    ENDLOOP.

    IF lt_pir_vendor[] IS NOT INITIAL.

      SELECT lifnr bbbnr bbsnr bubkz
        INTO CORRESPONDING FIELDS OF TABLE gt_pir_vendor[]
         FROM lfa1
        FOR ALL ENTRIES IN lt_pir_vendor[]
                WHERE bbbnr EQ lt_pir_vendor-bbbnr
                  AND bbsnr EQ lt_pir_vendor-bbsnr
                  AND bubkz EQ lt_pir_vendor-bubkz.

      IF gv_pir_ekorg    IS NOT INITIAL AND
         gt_pir_vendor[] IS NOT INITIAL.

        SELECT lifnr ekorg ekgrp plifz
          INTO CORRESPONDING FIELDS OF TABLE gt_lfm1[]
               FROM lfm1
          FOR ALL ENTRIES IN gt_pir_vendor[]
               WHERE lifnr EQ gt_pir_vendor-lifnr
                 AND ekorg EQ gv_pir_ekorg.
      ENDIF.
    ENDIF.
  ENDIF.  " gt_pir_vendor


  IF gt_range_frzd_chld[] IS INITIAL.
    SELECT sign opti AS option low high
      INTO CORRESPONDING FIELDS OF TABLE gt_range_frzd_chld[]
      FROM tvarvc
        WHERE name = 'GC_LIST_FRZD_CHLD'
        AND   type = 'S'.
  ENDIF.


  IF gt_range_greet_mc[] IS INITIAL.
    SELECT sign opti AS option low high
    INTO CORRESPONDING FIELDS OF TABLE gt_range_greet_mc[]
    FROM tvarvc
      WHERE name = 'ZARN_GREETING_UOM_MC'
      AND   type = 'S'.
  ENDIF.

  IF gs_greet_limit IS INITIAL.
    SELECT SINGLE *
    INTO CORRESPONDING FIELDS OF gs_greet_limit
    FROM tvarvc
      WHERE name = 'ZARN_GREETING_UOM_LIMIT'
      AND   type = 'S'.
  ENDIF.



  IF gt_tvarv_ean_cate[] IS INITIAL.

    SELECT *
    FROM tvarvc
    INTO TABLE gt_tvarv_ean_cate[]
    WHERE name = 'ZARN_GUI_DEFAULT_EAN_CATEGORY'.

    SELECT *
    FROM tvarvc
    APPENDING TABLE gt_tvarv_ean_cate[]
    WHERE name = 'ZARN_GUI_DEFLT_EAN_PREFIX_CATE'.

  ENDIF.



  IF gt_prod_uom_t[] IS INITIAL.
    SELECT *
    FROM zarn_prod_uom_t
    INTO CORRESPONDING FIELDS OF TABLE gt_prod_uom_t[]
    WHERE spras = sy-langu.
  ENDIF.


  IF gt_fanid[] IS INITIAL AND is_prod_data-zarn_products[] IS NOT INITIAL.
    SELECT matnr matkl mtart meins attyp zzfan
      FROM mara
    INTO CORRESPONDING FIELDS OF TABLE gt_fanid[]
      FOR ALL ENTRIES IN is_prod_data-zarn_products[]
      WHERE zzfan = is_prod_data-zarn_products-fan_id.
  ENDIF.


  IF gt_marm[] IS INITIAL AND gt_fanid[] IS NOT INITIAL.
    SELECT matnr meinh umrez umren mesub
      FROM marm
      INTO CORRESPONDING FIELDS OF TABLE gt_marm[]
      FOR ALL ENTRIES IN gt_fanid[]
      WHERE matnr = gt_fanid-matnr.
  ENDIF.


*  CLEAR: gt_marm_idno[].
*  IF gt_marm[]      IS NOT INITIAL AND
*     gt_fanid[]     IS NOT INITIAL.

  IF gt_marm_idno[] IS INITIAL.

    LOOP AT gt_marm[] INTO ls_marm.
      CLEAR ls_uom_cat.
      READ TABLE gt_uom_cat INTO ls_uom_cat
      WITH KEY uom = ls_marm-meinh.

      CLEAR ls_marm_idno.
      ls_marm_idno-idno         = is_prod_data-idno.
      ls_marm_idno-meinh        = ls_marm-meinh.
      ls_marm_idno-umrez        = ls_marm-umrez.
      ls_marm_idno-umren        = ls_marm-umren.
      ls_marm_idno-mesub        = ls_marm-mesub.
      ls_marm_idno-matnr        = ls_marm-matnr.
      ls_marm_idno-uom_category = ls_uom_cat-uom_category.
      ls_marm_idno-seqno        = ls_uom_cat-seqno.
      ls_marm_idno-uom          = ls_uom_cat-uom.
      ls_marm_idno-cat_seqno    = ls_uom_cat-cat_seqno.


* If UOM cat is not 'Retail' and LUOM is blank then consider BAse UOM as LUOM
      IF ls_uom_cat-uom_category NE 'RETAIL' AND ls_marm_idno-mesub IS INITIAL.
        CLEAR ls_fanid.
        READ TABLE gt_fanid INTO ls_fanid
        WITH KEY matnr = ls_marm-matnr.
        IF sy-subrc = 0.
          ls_marm_idno-mesub = ls_fanid-meins.
        ENDIF.
      ENDIF.

      APPEND ls_marm_idno TO gt_marm_idno[].
    ENDLOOP.  " LOOP AT gt_marm[] INTO ls_marm

  ENDIF.

* INS Begin of IR5055159 JKH 16.09.2016
  IF gt_mean[] IS INITIAL AND gt_fanid[] IS NOT INITIAL.
    SELECT matnr meinh lfnum ean11 eantp hpean
    FROM mean
    INTO CORRESPONDING FIELDS OF TABLE gt_mean[]
    FOR ALL ENTRIES IN gt_fanid[]
    WHERE matnr = gt_fanid-matnr.
  ENDIF.
* INS End of IR5055159 JKH 16.09.2016

  IF gt_host_prdtyp[] IS INITIAL OR gt_mc_subdep[] IS INITIAL.
    IF is_prod_data-zarn_nat_mc_hier[] IS NOT INITIAL.
      SELECT * FROM zmd_mc_subdep
        INTO CORRESPONDING FIELDS OF TABLE gt_mc_subdep[]
        FOR ALL ENTRIES IN is_prod_data-zarn_nat_mc_hier[]
        WHERE matkl = is_prod_data-zarn_nat_mc_hier-nat_mc_code.
      IF sy-subrc = 0.
        SELECT * FROM zmd_host_prdtyp
          INTO CORRESPONDING FIELDS OF TABLE gt_host_prdtyp[]
          FOR ALL ENTRIES IN gt_mc_subdep[]
          WHERE wwgha = gt_mc_subdep-subdep.
      ENDIF.
    ENDIF.
  ENDIF.


  IF gt_gui_def_recon[] IS INITIAL.
    SELECT a~tabname a~fldname a~sequence a~priority a~logic_ty a~param1 a~param2
      INTO CORRESPONDING FIELDS OF TABLE gt_gui_def_recon[]
      FROM zarn_gui_def AS a
      JOIN zarn_table_mastr AS b
      ON a~tabname = b~arena_table
      WHERE b~arena_table_type = gc_table_type_r.
  ENDIF.



  IF gt_gen_uom_t[] IS INITIAL.
    SELECT * FROM zarn_gen_uom_t
      INTO CORRESPONDING FIELDS OF TABLE gt_gen_uom_t[]
      WHERE spras = sy-langu.
  ENDIF.


  IF gt_net_qty[] IS INITIAL AND is_prod_data-zarn_products[] IS NOT INITIAL.
    SELECT * INTO TABLE gt_net_qty
      FROM zarn_net_qty
      FOR ALL ENTRIES IN is_prod_data-zarn_products[]
      WHERE idno = is_prod_data-zarn_products-idno.

    SORT gt_net_qty BY idno ASCENDING version DESCENDING.
    DELETE ADJACENT DUPLICATES FROM gt_net_qty COMPARING idno.
  ENDIF.

  gv_initialized = abap_true.

ENDMETHOD.


METHOD map_nat_uom_to_reg_uom_stru.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 13.03.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1102 - Ability to manually maintain Regonal
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Map National UOM Data to Regional UOM Structure
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  cs_reg_uom_nat_data-nat_depth_value        = is_uom_variant-depth_value.
  cs_reg_uom_nat_data-nat_gross_weight_value = is_uom_variant-gross_weight_value.
  cs_reg_uom_nat_data-nat_height_value       = is_uom_variant-height_value.
  cs_reg_uom_nat_data-nat_net_weight_value   = is_uom_variant-net_weight_value.
  cs_reg_uom_nat_data-nat_width_value        = is_uom_variant-width_value.

ENDMETHOD.


METHOD map_pir_to_reg_uom.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 09.03.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Maps National PIR data to regional UOM Data
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  " Note: The logic has been copied from method SET_REG_UOM_DEFAULT as part of
  " modularization and made further changes accordingly.

  DATA: lv_count            TYPE i,
        ls_pir_calc         TYPE zarn_pir,
        ls_reg_uom          TYPE zarn_reg_uom,
        ls_added_pir        TYPE ty_s_added_pir,
        ls_reg_uom_ui       TYPE zsarn_reg_uom_ui,
        ls_uom_variant_calc TYPE zarn_uom_variant,
        lt_added_pir        TYPE ty_t_added_pir.

*******************************************************************************
*** National PIR - LAYER/PALLET
  DATA(lt_pir) = is_prod_data-zarn_pir[].

* Ignore National PIR where units per pallets are not maintained
* this means only records which have layer/pallet UOMs will be considered
  DELETE lt_pir[] WHERE base_units_per_pallet LE 0 AND
                        qty_per_pallet_layer  LE 0 AND
                        qty_layers_per_pallet LE 0 AND
                        qty_units_per_pallet  LE 0.

* Sort sequence important for loop to compare values below
  SORT lt_pir[]
    BY idno
       version
       uom_code
       base_units_per_pallet
       qty_layers_per_pallet
       qty_units_per_pallet.


  LOOP AT lt_pir[] INTO DATA(ls_pir).

    CLEAR: lv_count.

* Logic for selecting PIR for creating Layer and Pallet in Regional UOM
* 1. Consider only those PIR records which has the 'Base/Pallet' or 'Qty./Layer' or 'Ly/Pallet' or 'Qty./Pallet'  <> 0
* 2. Loop for every HYBRIS UOM which is having  unique values in the above mentioned fields
*        and where 'Base/Pallet' / 'Qty./Pallet'  = 'No: of Base Unit' / 'Factor of Base Unit'  OR
*                  'Base/Pallet' / 'Qty./Layer'  * 'Ly/Pallet' = 'No: of Base Unit'  / 'Factor of Base Unit'

    READ TABLE lt_added_pir TRANSPORTING NO FIELDS
      WITH TABLE KEY uom_code              = ls_pir-uom_code
                     base_units_per_pallet = ls_pir-base_units_per_pallet
                     qty_per_pallet_layer  = ls_pir-qty_per_pallet_layer
                     qty_layers_per_pallet = ls_pir-qty_layers_per_pallet
                     qty_units_per_pallet  = ls_pir-qty_units_per_pallet.
    IF sy-subrc = 0.
      CONTINUE.
    ENDIF.


    CLEAR ls_pir_calc.
    ls_pir_calc = ls_pir.

    IF ls_pir_calc-qty_units_per_pallet IS INITIAL.
      ls_pir_calc-qty_units_per_pallet = 1.
    ENDIF.

    IF ls_pir_calc-qty_per_pallet_layer IS INITIAL.
      ls_pir_calc-qty_per_pallet_layer = 1.
    ENDIF.

    IF ls_pir_calc-base_units_per_pallet IS INITIAL.
      ls_pir_calc-base_units_per_pallet = 1.
    ENDIF.


    CLEAR ls_uom_variant_calc.
    READ TABLE is_prod_data-zarn_uom_variant INTO DATA(ls_uom_variant)
            WITH KEY idno     = ls_pir-idno
                     version  = ls_pir-version
                     uom_code = ls_pir-uom_code.
    IF sy-subrc = 0.
      ls_uom_variant_calc = ls_uom_variant.
    ENDIF.


    IF ls_uom_variant_calc-num_base_units IS INITIAL.
      ls_uom_variant_calc-num_base_units = 1.
    ENDIF.

    IF ls_uom_variant_calc-factor_of_base_units IS INITIAL.
      ls_uom_variant_calc-factor_of_base_units = 1.
    ENDIF.


    DATA(lv_marm_calc) = VALUE boole_d( ).
    IF ls_uom_variant_calc-factor_of_base_units GT 1.   " Implies we're dealing with decimals
      lv_marm_calc = abap_true.
      " Identify the base uom
      IF ls_uom_variant_calc-base_unit = abap_true.
        DATA(lv_base_uom) = ls_uom_variant_calc-product_uom.
      ELSE.
        lv_base_uom = VALUE #( is_prod_data-zarn_uom_variant[ idno      = ls_pir-idno
                                                              version   = ls_pir-version
                                                              base_unit = abap_true ]-product_uom OPTIONAL ).
      ENDIF.
    ENDIF.

    IF ( ( ls_pir_calc-base_units_per_pallet / ls_pir_calc-qty_units_per_pallet ) =
                           ( ls_uom_variant_calc-num_base_units / ls_uom_variant_calc-factor_of_base_units ) )
                                        OR
       ( ( ( ls_pir_calc-base_units_per_pallet / ls_pir_calc-qty_per_pallet_layer ) * ls_pir_calc-qty_layers_per_pallet ) =
                           ( ls_uom_variant_calc-num_base_units / ls_uom_variant_calc-factor_of_base_units ) ).

      CLEAR: ls_pir_calc, ls_uom_variant_calc.


      " record not found, then add to local table
      CLEAR ls_added_pir.
      ls_added_pir-uom_code              = ls_pir-uom_code.
      ls_added_pir-base_units_per_pallet = ls_pir-base_units_per_pallet.
      ls_added_pir-qty_per_pallet_layer  = ls_pir-qty_per_pallet_layer.
      ls_added_pir-qty_layers_per_pallet = ls_pir-qty_layers_per_pallet.
      ls_added_pir-qty_units_per_pallet  = ls_pir-qty_units_per_pallet.
      INSERT ls_added_pir INTO TABLE lt_added_pir.

    ELSE.
      CONTINUE.
    ENDIF.

    CLEAR: ls_reg_uom_ui.
    ls_reg_uom_ui-mandt                = sy-mandt.
    ls_reg_uom_ui-idno                 = ls_pir-idno.
    ls_reg_uom_ui-pim_uom_code         = ls_pir-uom_code.
    ls_reg_uom_ui-hybris_internal_code = ls_pir-hybris_internal_code.
    ls_reg_uom_ui-lower_uom            = ls_pir-uom_code.

* Create LAYER and PALLET for each PIR considred
    DO 2 TIMES.

      ADD 1 TO lv_count.
      IF lv_count EQ 1.
        ls_reg_uom_ui-uom_category = gc_layer.
      ELSE.
        ls_reg_uom_ui-uom_category = gc_pallet.
      ENDIF.

      READ TABLE is_prod_data-zarn_uom_variant INTO ls_uom_variant
              WITH KEY idno     = ls_pir-idno
                       version  = ls_pir-version
                       uom_code = ls_pir-uom_code.
      IF sy-subrc = 0.

        " Get Lower child UOM from GTIN
        READ TABLE is_prod_data-zarn_gtin_var[] INTO DATA(ls_gtin_var_child)
        WITH KEY idno      = ls_pir-idno
                 version   = ls_pir-version
                 gtin_code = ls_uom_variant-child_gtin.
        IF sy-subrc IS INITIAL.
          ls_reg_uom_ui-lower_child_uom = ls_gtin_var_child-uom_code.
        ENDIF.

        "LAYER
        IF ls_reg_uom_ui-uom_category = gc_layer.

          IF lv_marm_calc =  abap_true.
            " Implies we're dealing with decimals so perform calculation
            " as per SAP standard - as in article master maintenance
            DATA(lv_input) = VALUE f( ).
            IF ls_uom_variant-product_uom = lv_base_uom.
              lv_input = ls_pir-qty_per_pallet_layer.
            ELSE.
              IF ls_uom_variant-factor_of_base_units IS NOT INITIAL.
                lv_input = ls_pir-qty_per_pallet_layer * ( ls_uom_variant-num_base_units / ls_uom_variant-factor_of_base_units ).
              ENDIF.
            ENDIF.
            DATA(lv_numerator)   = VALUE t006-zaehl( ).
            DATA(lv_denominator) = VALUE t006-nennr( ).
            CALL FUNCTION 'CONVERT_TO_FRACTION'
              EXPORTING
                input               = lv_input
              IMPORTING
                nominator           = lv_numerator
                denominator         = lv_denominator
              EXCEPTIONS
                conversion_overflow = 1
                OTHERS              = 2.
            IF sy-subrc IS INITIAL.
              ls_reg_uom_ui-num_base_units       = lv_numerator.
              ls_reg_uom_ui-factor_of_base_units = lv_denominator.
            ENDIF.
            " Retain layer conversion factors for pallet calculation
            DATA(lv_layer_numerator)   = lv_numerator.
            DATA(lv_layer_denominator) = lv_denominator.

          ELSE.

            IF ls_pir-qty_layers_per_pallet IS NOT INITIAL AND
               ls_pir-base_units_per_pallet IS NOT INITIAL.

              ls_reg_uom_ui-num_base_units = ls_pir-base_units_per_pallet / ls_pir-qty_layers_per_pallet.
              ls_reg_uom_ui-factor_of_base_units = ls_pir-factor_of_buom_per_pallet.


            ELSE.
              IF ls_pir-qty_per_pallet_layer IS NOT INITIAL.

                ls_reg_uom_ui-num_base_units =  ls_pir-qty_per_pallet_layer * ls_uom_variant-num_base_units.
                ls_reg_uom_ui-factor_of_base_units = ls_uom_variant-factor_of_base_units.
              ELSE.
                ls_reg_uom_ui-num_base_units = 1.
              ENDIF.
            ENDIF.

          ENDIF.

          ls_reg_uom_ui-gross_weight_value = 0.     " Gross Weight Value
          ls_reg_uom_ui-height_value       = 0.     " Gross Height Value

          READ TABLE ct_reg_uom_ui INTO DATA(ls_reg_uom_ui_tmp)
          WITH KEY idno                 = ls_reg_uom_ui-idno
                   pim_uom_code         = ls_reg_uom_ui-pim_uom_code
                   hybris_internal_code = space
                   lower_uom            = ls_reg_uom_ui-lower_child_uom.
          IF sy-subrc = 0.
            ls_reg_uom_ui-gross_weight_uom = ls_reg_uom_ui_tmp-gross_weight_uom.       " Gross Weight UOM
            ls_reg_uom_ui-height_uom       = ls_reg_uom_ui_tmp-height_uom.             " Height UOM
          ENDIF.


          " PALLET
        ELSEIF ls_reg_uom_ui-uom_category = gc_pallet.

          IF lv_marm_calc =  abap_true.
            " Implies we're dealing with decimals so perform calculation
            " as per SAP standard - as in article master maintenance
            CLEAR lv_input.
            IF ls_uom_variant-product_uom = lv_base_uom.
              lv_input = ls_pir-qty_layers_per_pallet.
            ELSE.
              IF lv_layer_denominator IS NOT INITIAL.
                lv_input = ls_pir-qty_layers_per_pallet * ( lv_layer_numerator / lv_layer_denominator ).
              ENDIF.
            ENDIF.
            CLEAR: lv_numerator,
                   lv_denominator.
            CALL FUNCTION 'CONVERT_TO_FRACTION'
              EXPORTING
                input               = lv_input
              IMPORTING
                nominator           = lv_numerator
                denominator         = lv_denominator
              EXCEPTIONS
                conversion_overflow = 1
                OTHERS              = 2.
            IF sy-subrc IS INITIAL.
              ls_reg_uom_ui-num_base_units       = lv_numerator.
              ls_reg_uom_ui-factor_of_base_units = lv_denominator.
            ENDIF.

          ELSE.

            IF ls_pir-base_units_per_pallet IS NOT INITIAL.

              ls_reg_uom_ui-num_base_units = ls_pir-base_units_per_pallet.
              ls_reg_uom_ui-factor_of_base_units = ls_pir-factor_of_buom_per_pallet.

            ELSE.
              IF ls_pir-qty_units_per_pallet IS NOT INITIAL.
                ls_reg_uom_ui-num_base_units = ls_pir-qty_units_per_pallet * ls_uom_variant-num_base_units.
                ls_reg_uom_ui-factor_of_base_units = ls_uom_variant-factor_of_base_units.

              ELSE.
                IF ls_pir-qty_layers_per_pallet IS NOT INITIAL.

                  CLEAR ls_reg_uom_ui_tmp.
                  READ TABLE ct_reg_uom_ui INTO ls_reg_uom_ui_tmp
                  WITH KEY idno  = ls_reg_uom-idno
                           meinh = ls_reg_uom-lower_meinh.
                  IF sy-subrc = 0.
                    ls_reg_uom_ui-num_base_units = ls_pir-qty_layers_per_pallet * ls_reg_uom_ui_tmp-num_base_units.
                    ls_reg_uom_ui-factor_of_base_units = ls_reg_uom_ui_tmp-factor_of_base_units.
                  ELSE.
                    ls_reg_uom_ui-num_base_units = 1.
                  ENDIF.
                ELSE.
                  ls_reg_uom_ui-num_base_units = 1.
                ENDIF.
              ENDIF.
            ENDIF.

          ENDIF.

          ls_reg_uom_ui-gross_weight_value = ls_pir-gross_weight.     " Gross Weight Value
          ls_reg_uom_ui-height_value       = ls_pir-gross_height.     " Gross Height Value

          CLEAR ls_reg_uom_ui_tmp.
          READ TABLE ct_reg_uom_ui INTO ls_reg_uom_ui_tmp
          WITH KEY idno                 = ls_reg_uom_ui-idno
                   pim_uom_code         = ls_reg_uom_ui-pim_uom_code
                   hybris_internal_code = space
                   lower_uom            = ls_reg_uom_ui-lower_child_uom.
          IF sy-subrc = 0.
            ls_reg_uom_ui-gross_weight_uom = ls_reg_uom_ui_tmp-gross_weight_uom.       " Gross Weight UOM
            ls_reg_uom_ui-height_uom       = ls_reg_uom_ui_tmp-height_uom.             " Height UOM
          ENDIF.

        ENDIF.      " ls_reg_uom-uom_category = LAYER/PALLET

      ENDIF.

      zcl_arn_gui_load=>unit_conversion_simple(
            EXPORTING input    = ls_reg_uom_ui-height_value
                      unit_in  = ls_reg_uom_ui-height_uom
                      unit_out = 'MM'
            IMPORTING output   = ls_reg_uom_ui-height_value
                      ev_error = DATA(lv_uom_conv_err) ).

      IF lv_uom_conv_err IS INITIAL.
        ls_reg_uom_ui-height_uom = 'MMT'.
      ENDIF.

      IF ls_reg_uom_ui-num_base_units IS INITIAL.
        ls_reg_uom_ui-num_base_units = 1.
      ENDIF.

      IF ls_reg_uom_ui-factor_of_base_units IS INITIAL.
        ls_reg_uom_ui-factor_of_base_units = 1.
      ENDIF.

      " Get UOM Category Sequence
      READ TABLE gt_uomcategory[] INTO DATA(ls_uomcategory)
      WITH TABLE KEY uom_category = ls_reg_uom_ui-uom_category.
      IF sy-subrc = 0.
        ls_reg_uom_ui-cat_seqno = ls_uomcategory-cat_seqno.
      ENDIF.

      " Get UOM Category Sequence
      IF ls_reg_uom_ui-meinh IS NOT INITIAL.
        READ TABLE gt_uom_cat[] INTO DATA(ls_uom_cat)
        WITH KEY uom_category = ls_reg_uom_ui-uom_category
                 uom          = ls_reg_uom_ui-meinh.
        IF sy-subrc = 0.
          ls_reg_uom_ui-seqno = ls_uom_cat-seqno.
        ENDIF.
      ENDIF.

      APPEND ls_reg_uom_ui TO ct_reg_uom_ui.

    ENDDO.

    CLEAR:
      lv_layer_numerator,
      lv_layer_denominator.

  ENDLOOP.

ENDMETHOD.


METHOD map_uom_variant_to_reg_uom.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 09.03.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Map National UOM Variant to Regional UOM data
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  DATA: ls_reg_uom_ui        TYPE zsarn_reg_uom_ui,
        ls_prod_uom_t        TYPE ty_s_prod_uom_t,
        ls_gtin_var_child    TYPE zarn_gtin_var,
        ls_uom_variant_child TYPE zarn_uom_variant,
        ls_uomcategory       TYPE zarn_uomcategory.

  CLEAR: ev_dim_overflow_error.

* Default UOMs from National UOMs
  LOOP AT is_prod_data-zarn_uom_variant INTO DATA(ls_uom_variant).

    CLEAR: ls_reg_uom_ui,
           ls_prod_uom_t,
           ls_gtin_var_child,
           ls_uom_variant_child,
           ls_uomcategory.

    ls_reg_uom_ui-mandt                          = sy-mandt.
    ls_reg_uom_ui-idno                           = ls_uom_variant-idno.
    ls_reg_uom_ui-pim_uom_code                   = ls_uom_variant-uom_code.
    ls_reg_uom_ui-hybris_internal_code           = space.
    ls_reg_uom_ui-pkging_rcycling_scheme_code    = ls_uom_variant-pkging_rcycling_scheme_code.
    ls_reg_uom_ui-pkging_rcycling_process_type_c = ls_uom_variant-pkging_rcycling_process_type_c.
    ls_reg_uom_ui-sustainability_feature_code    = ls_uom_variant-sustainability_feature_code.
    ls_reg_uom_ui-net_content_short_desc         = ls_uom_variant-net_content_short_desc.
    ls_reg_uom_ui-fscust_prod_name40             = ls_uom_variant-fscust_prod_name40.
    ls_reg_uom_ui-fscust_prod_name80             = ls_uom_variant-fscust_prod_name80.
    ls_reg_uom_ui-fscust_prod_name255            = ls_uom_variant-fscust_prod_name255.

* Get UOM Category
    READ TABLE gt_prod_uom_t[] INTO ls_prod_uom_t
      WITH TABLE KEY product_uom = ls_uom_variant-product_uom.
    IF sy-subrc EQ 0.
      ls_reg_uom_ui-uom_category = ls_prod_uom_t-sap_uom_cat.
    ENDIF.

* Get Lower UOM and Lower child UOM
    READ TABLE is_prod_data-zarn_gtin_var[] INTO ls_gtin_var_child
      WITH KEY idno      = ls_uom_variant-idno
               version   = ls_uom_variant-version
               gtin_code = ls_uom_variant-child_gtin.
    IF sy-subrc EQ 0 AND ls_gtin_var_child-uom_code IS NOT INITIAL.
      ls_reg_uom_ui-lower_uom = ls_gtin_var_child-uom_code.

      READ TABLE is_prod_data-zarn_uom_variant[] INTO ls_uom_variant_child
        WITH KEY idno     = ls_uom_variant-idno
                 version  = ls_uom_variant-version
                 uom_code = ls_gtin_var_child-uom_code.
      IF sy-subrc EQ 0 AND ls_uom_variant_child-child_gtin IS NOT INITIAL.
        CLEAR ls_gtin_var_child.
        READ TABLE is_prod_data-zarn_gtin_var[] INTO ls_gtin_var_child
           WITH KEY idno      = ls_uom_variant_child-idno
                    version   = ls_uom_variant_child-version
                    gtin_code = ls_uom_variant_child-child_gtin.
        IF sy-subrc EQ 0.
          ls_reg_uom_ui-lower_child_uom = ls_gtin_var_child-uom_code.
        ENDIF.
      ENDIF.
    ENDIF.

* Get Number of Base Units
    ls_reg_uom_ui-num_base_units = ls_uom_variant-num_base_units.
* Factor of Bsse Units
    ls_reg_uom_ui-factor_of_base_units = ls_uom_variant-factor_of_base_units.


    IF ls_reg_uom_ui-num_base_units IS INITIAL.
      ls_reg_uom_ui-num_base_units = 1.
    ENDIF.

    IF ls_reg_uom_ui-factor_of_base_units IS INITIAL.
      ls_reg_uom_ui-factor_of_base_units = 1.
    ENDIF.

    TRY.
        ls_reg_uom_ui-net_weight_uom       = ls_uom_variant-net_weight_uom.         " Net Weight UOM
        ls_reg_uom_ui-net_weight_value     = ls_uom_variant-net_weight_value.       " Net Weight Value
      CATCH cx_sy_conversion_overflow.
        ev_dim_overflow_error = abap_true.
    ENDTRY.

    TRY.
        ls_reg_uom_ui-gross_weight_uom     = ls_uom_variant-gross_weight_uom.       " Gross Weight UOM
        ls_reg_uom_ui-gross_weight_value   = ls_uom_variant-gross_weight_value.     " Gross Weight Value
      CATCH cx_sy_conversion_overflow.
        ev_dim_overflow_error = abap_true.
    ENDTRY.

    TRY.
        ls_reg_uom_ui-height_uom           = ls_uom_variant-height_uom.             " Height UOM
        ls_reg_uom_ui-height_value         = ls_uom_variant-height_value.           " Height Value
      CATCH cx_sy_conversion_overflow.
        ev_dim_overflow_error = abap_true.
    ENDTRY.

    TRY.
        ls_reg_uom_ui-width_uom            = ls_uom_variant-width_uom.              " Width UOM
        ls_reg_uom_ui-width_value          = ls_uom_variant-width_value.            " Width Value
      CATCH cx_sy_conversion_overflow.
        ev_dim_overflow_error = abap_true.
    ENDTRY.

    TRY.
        ls_reg_uom_ui-depth_uom            = ls_uom_variant-depth_uom.              " Depth UOM
        ls_reg_uom_ui-depth_value          = ls_uom_variant-depth_value.            " Depth Value
      CATCH cx_sy_conversion_overflow.
        ev_dim_overflow_error = abap_true.
    ENDTRY.

    zcl_arn_gui_load=>unit_conversion_simple(
          EXPORTING input    = ls_reg_uom_ui-height_value
                    unit_in  = ls_reg_uom_ui-height_uom
                    unit_out = 'MM'
          IMPORTING output   = ls_reg_uom_ui-height_value
                    ev_error = DATA(lv_uom_conv_err) ).
    IF lv_uom_conv_err IS INITIAL.
      ls_reg_uom_ui-height_uom = 'MMT'.
    ENDIF.


    zcl_arn_gui_load=>unit_conversion_simple(
          EXPORTING input    = ls_reg_uom_ui-width_value
                    unit_in  = ls_reg_uom_ui-width_uom
                    unit_out = 'MM'
          IMPORTING output   = ls_reg_uom_ui-width_value
                    ev_error = lv_uom_conv_err ).
    IF lv_uom_conv_err IS INITIAL.
      ls_reg_uom_ui-width_uom  = 'MMT'.
    ENDIF.

    zcl_arn_gui_load=>unit_conversion_simple(
          EXPORTING input    = ls_reg_uom_ui-depth_value
                    unit_in  = ls_reg_uom_ui-depth_uom
                    unit_out = 'MM'
          IMPORTING output   = ls_reg_uom_ui-depth_value
                    ev_error = lv_uom_conv_err ).
    IF lv_uom_conv_err IS INITIAL.
      ls_reg_uom_ui-depth_uom  = 'MMT'.
    ENDIF.

* Get UOM Category Sequence
    READ TABLE gt_uomcategory[] INTO ls_uomcategory
    WITH TABLE KEY uom_category = ls_reg_uom_ui-uom_category.
    IF sy-subrc = 0.
      ls_reg_uom_ui-cat_seqno = ls_uomcategory-cat_seqno.
    ENDIF.

* Get UOM Category Sequence
    IF ls_reg_uom_ui-meinh IS NOT INITIAL.
      READ TABLE gt_uom_cat[] INTO DATA(ls_uom_cat)
      WITH KEY uom_category = ls_reg_uom_ui-uom_category
               uom          = ls_reg_uom_ui-meinh.
      IF sy-subrc = 0.
        ls_reg_uom_ui-seqno = ls_uom_cat-seqno.
      ENDIF.
    ENDIF.

    APPEND ls_reg_uom_ui TO rt_reg_uom_ui[].

  ENDLOOP.

ENDMETHOD.


METHOD read_global_data.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 17.07.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1359: ZARN_GUI changes (GS1  Updates)
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          #
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  SELECT *
    FROM zarn_allergen_t
    INTO TABLE @gt_allergen_text
    WHERE spras = @sy-langu.

ENDMETHOD.


METHOD retain_regional_uom_db_data.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 09.03.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Retails regional UOM data from database and only replaces
*                  # some values from National data which is already populated
*                  # in table CT_REG_UOM_UI
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  DATA: ls_reg_uom_dim_fields TYPE zsarn_reg_uom_dim_fields,
        lo_stru_descr         TYPE REF TO cl_abap_structdescr.

  FIELD-SYMBOLS: <lv_reg_uom_ui>    TYPE any,
                 <lv_reg_uom_exist> TYPE any.

  lo_stru_descr ?= cl_abap_structdescr=>describe_by_data( ls_reg_uom_dim_fields ).
  DATA(lt_component) = lo_stru_descr->get_components( ).

  DATA(lv_article) = VALUE #( is_reg_data-zarn_reg_hdr[ 1 ]-matnr OPTIONAL ).
  SORT ct_reg_uom_ui BY cat_seqno ASCENDING.
  LOOP AT ct_reg_uom_ui ASSIGNING FIELD-SYMBOL(<ls_reg_uom_ui>).

    READ TABLE it_reg_uom_db[] INTO DATA(ls_reg_uom_db)
         WITH KEY idno                 = <ls_reg_uom_ui>-idno
                  uom_category         = <ls_reg_uom_ui>-uom_category
                  pim_uom_code         = <ls_reg_uom_ui>-pim_uom_code
                  hybris_internal_code = <ls_reg_uom_ui>-hybris_internal_code
                  lower_uom            = <ls_reg_uom_ui>-lower_uom
                  lower_child_uom      = <ls_reg_uom_ui>-lower_child_uom.
    IF sy-subrc <> 0.
      CONTINUE.
    ENDIF.

    APPEND INITIAL LINE TO rt_reg_uom_exist ASSIGNING FIELD-SYMBOL(<ls_reg_uom_exist>).
    MOVE-CORRESPONDING ls_reg_uom_db TO <ls_reg_uom_exist>.

    " Get UOM Category Sequence
    READ TABLE gt_uomcategory[] INTO DATA(ls_uomcategory)
    WITH TABLE KEY uom_category = ls_reg_uom_db-uom_category.
    IF sy-subrc = 0.
      <ls_reg_uom_exist>-cat_seqno = ls_uomcategory-cat_seqno.
    ENDIF.

    " Get UOM Sequence
    READ TABLE gt_uom_cat[] INTO DATA(ls_uom_cat)
    WITH KEY uom_category = ls_reg_uom_db-uom_category
             uom          = ls_reg_uom_db-meinh.
    IF sy-subrc = 0.
      <ls_reg_uom_exist>-seqno     = ls_uom_cat-seqno.                    " UOM Sequence
    ENDIF.

    CLEAR: <ls_reg_uom_ui>-idno.
    <ls_reg_uom_exist>-cat_seqno            = <ls_reg_uom_ui>-cat_seqno.              " UOM Category Sequence
    <ls_reg_uom_exist>-num_base_units       = <ls_reg_uom_ui>-num_base_units.         " Number of base units
    <ls_reg_uom_exist>-factor_of_base_units = <ls_reg_uom_ui>-factor_of_base_units.   " Factor of base units


    " Dimention fields in Regional UOM should not be overwritten for RETAIL category as these fields are
    " editable in ZARN_GUI which shouldn't be overwritten by National unless article is not yet created
    IF <ls_reg_uom_ui>-uom_category <> zif_arn_uom_category_c=>gc_retail OR
       lv_article IS INITIAL.
      LOOP AT lt_component INTO DATA(ls_component).
        READ TABLE it_gui_def TRANSPORTING NO FIELDS
                             WITH TABLE KEY tabname  = 'ZARN_REG_UOM'
                                            fldname  = ls_component-name
                                            logic_ty = gc_logic_l.
        IF sy-subrc = 0.
          ASSIGN COMPONENT ls_component-name OF STRUCTURE <ls_reg_uom_ui>    TO <lv_reg_uom_ui>.
          IF sy-subrc = 0.
            ASSIGN COMPONENT ls_component-name OF STRUCTURE <ls_reg_uom_exist> TO <lv_reg_uom_exist>.
            IF sy-subrc = 0.
              <lv_reg_uom_exist> = <lv_reg_uom_ui>.
            ENDIF.
          ENDIF.
        ENDIF.
      ENDLOOP.
    ENDIF.

    READ TABLE is_prod_data-zarn_uom_variant INTO DATA(ls_uom_variant)
                                             WITH KEY idno     = <ls_reg_uom_exist>-idno
                                                      uom_code = <ls_reg_uom_exist>-pim_uom_code.
    IF sy-subrc = 0.
      map_nat_uom_to_reg_uom_stru( EXPORTING is_uom_variant      = ls_uom_variant
                                   CHANGING  cs_reg_uom_nat_data = <ls_reg_uom_exist>-nat_data ).
    ENDIF.

  ENDLOOP.
  DELETE ct_reg_uom_ui WHERE idno IS INITIAL.

ENDMETHOD.


  METHOD set_regional_default.
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 13.04.2018
* CHANGE No.       # Charm 9000003482; Jira CI18-554
* DESCRIPTION      # CI18-554 Recipe Management - Ingredient Changes
*                  # New regional table for Allergen Type overrides and
*                  # fields for Ingredients statement overrides in Arena.
*                  # New Ingredients Type field in the Scales block in
*                  # Arena and NONSAP tab in material master.
* WHO              # C90005557, Tessa Newman
*-----------------------------------------------------------------------
* DATE             # 06/09/2018
* CHANGE No.       # Charm #9000004244 - CR092/93 PIM schema changes
* DESCRIPTION      # Add work area for new national tables.
* WHO              # C90001929, Aruna Mackenjee
*-----------------------------------------------------------------------
* DATE             # 05/06/2020
* CHANGE No.       # Charm #9000006877 - Trent online changes
* DESCRIPTION      # Add work area for new national tables.
* WHO              # C90001929, Aruna Mackenjee
*-----------------------------------------------------------------------
* DATE             # 19.07.2020
* CHANGE No.       # CIP-1359: ZARN_GUI changes (GS1  Updates)
* DESCRIPTION      # 1. Default Regional Allergen and String Data
*                  # 2. Default newly created table ZARN_REG_NUTRIEN
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
    DATA: lt_tabname       TYPE ty_t_table_mastr,
          ls_tabname       TYPE ty_s_table_mastr,
          lt_dfies         TYPE dfies_tab,
          ls_dfies         TYPE dfies,
          ls_gui_set       TYPE zarn_gui_set,
          ls_gui_def       TYPE zarn_gui_def,
          ls_reg_data      TYPE zsarn_reg_data,
          ls_reg_data_org  TYPE zsarn_reg_data,
          ls_prod_data     TYPE zsarn_prod_data,
          ls_reg_data_def  TYPE zsarn_reg_data,
          ls_reg_hdr_db    TYPE zarn_reg_hdr,

          lv_itabname      TYPE char50,
          lv_itabfieldname TYPE char70,
          lv_deffieldname  TYPE char70,
          "lv_nat_itabname  TYPE char50,
          "lv_ddif_tab      TYPE ddobjname,
          lv_reg_itabname  TYPE char50,
          lv_ddif_tab      TYPE ddobjname,
          lv_table_str     TYPE string,
          lv_wa_str        TYPE string.

    FIELD-SYMBOLS:
      <itabname>      TYPE table,
      <reg_itabname>  TYPE table,
*    <nat_itabname>    TYPE table,
      <itabfieldname> TYPE any,
      <deffieldname>  TYPE any,
      <table_str>     TYPE any,
      <scr_table>     TYPE any,
*    <ls_zarn_reg_ean> TYPE zarn_reg_ean,
*    <ls_zarn_reg_hdr> TYPE zarn_reg_hdr,
      <table>         TYPE table,
      <workarea>      TYPE any.


    DATA: zarn_reg_hdr     TYPE zarn_reg_hdr,
          zarn_reg_banner  TYPE zarn_reg_banner,
          zarn_reg_ean     TYPE zarn_reg_ean,
          zarn_reg_pir     TYPE zarn_reg_pir,
          zarn_reg_prfam   TYPE zarn_reg_prfam,
          zarn_reg_txt     TYPE zarn_reg_txt,
          zarn_reg_uom     TYPE zarn_reg_uom,
          zarn_reg_lst_prc TYPE zarn_reg_lst_prc,
          zarn_reg_dc_sell TYPE zarn_reg_dc_sell,
          zarn_reg_std_ter TYPE zarn_reg_std_ter,
          zarn_reg_rrp     TYPE zarn_reg_rrp,
          zarn_reg_hsno    TYPE zarn_reg_hsno,
          zarn_reg_artlink TYPE zarn_reg_artlink,
          zarn_reg_onlcat  TYPE zarn_reg_onlcat,      "++ONLD-835 JKH 13.06.2017
          zarn_reg_str     TYPE zarn_reg_str,         "++ONLD-835 JKH 13.06.2017
          zarn_reg_allerg  TYPE zarn_reg_allerg,
          zarn_reg_sc      TYPE zarn_reg_sc,          "LRA 2a IS 2019/02
          zarn_products    TYPE zarn_products,
          zarn_products_2  TYPE zarn_products,
          zarn_products_ex TYPE zarn_products_ex,
          zarn_prod_str    TYPE zarn_prod_str,
          zarn_cdt_level   TYPE zarn_cdt_level,
          zarn_nutrients   TYPE zarn_nutrients,
          zarn_ntr_health  TYPE zarn_ntr_health,
          zarn_ntr_claimtx TYPE zarn_ntr_claimtx,
          zarn_ntr_claims  TYPE zarn_ntr_claims,
          zarn_growing     TYPE zarn_growing,
          zarn_allergen    TYPE zarn_allergen,
          zarn_hsno        TYPE zarn_hsno,
          zarn_dgi_margin  TYPE zarn_dgi_margin,
          zarn_prep_type   TYPE zarn_prep_type,
          zarn_ins_code    TYPE zarn_ins_code,
          zarn_diet_info   TYPE zarn_diet_info,
          zarn_additives   TYPE zarn_additives,
          zarn_fb_ingre    TYPE zarn_fb_ingre,
          zarn_chem_char   TYPE zarn_chem_char,
          zarn_organism    TYPE zarn_organism,
          zarn_nat_mc_hier TYPE zarn_nat_mc_hier,
          zarn_addit_info  TYPE zarn_addit_info,
          zarn_uom_variant TYPE zarn_uom_variant,
          zarn_gtin_var    TYPE zarn_gtin_var,
          zarn_pir         TYPE zarn_pir,
          zarn_pir_comm_ch TYPE zarn_pir_comm_ch,
          zarn_list_price  TYPE zarn_list_price,
          zarn_bioorganism TYPE zarn_bioorganism,
          zarn_net_qty     TYPE zarn_net_qty,
          zarn_serve_size  TYPE zarn_serve_size,
          zarn_benefits    TYPE zarn_benefits,
          zarn_onlcat      TYPE zarn_onlcat,
          zarn_reg_nutrien TYPE zarn_reg_nutrien.

    gt_lfm1[]           = it_lfm1[].
    gt_fanid[]          = it_fanid[].
    gt_marm[]           = it_marm[].
    gt_marm_idno[]      = it_marm_idno[].
    gt_mean[]           = it_mean[].
    gt_prod_uom_t[]     = it_prod_uom_t[].
    gt_uom_cat[]        = it_uom_cat[].
    gt_uomcategory[]    = it_uomcategory[].
    gt_gen_uom_t[]      = it_gen_uom_t[].
    gt_ref_article[]    = it_ref_article[].
    gt_pir_vendor[]     = it_pir_vendor[].
    gt_tvarv_ean_cate[] = it_tvarv_ean_cate[].
    gt_host_prdtyp[]    = it_host_prdtyp[].
    gt_mc_subdep[]      = it_mc_subdep[].
    gv_pir_ekorg        = iv_pir_ekorg.


    ls_reg_data  = is_reg_data.
    ls_prod_data = is_prod_data.


    ls_reg_data_def = ls_reg_data.


* Retreive Initial Global Data
    CALL METHOD me->get_initial_data
      EXPORTING
        is_prod_data = ls_prod_data.




* Get Table Master data for Regional and National only
    lt_tabname[] = gt_tabname[].

* Keep
    DELETE lt_tabname[]
        WHERE arena_table_type NE gc_table_type_r
          AND arena_table_type NE gc_table_type_n.


* Read first record of National and Regional Data for one to one mapping
* Ideally, fields with default logic as 'T' will be one to one mapped from nat/reg DB,
* but if there are more than 1 record in nat/reg to be mapped, then consider only first one
    LOOP AT lt_tabname[] INTO ls_tabname.

      IF ls_tabname-arena_table_type EQ gc_table_type_r.
        lv_table_str = 'LS_REG_DATA-' && ls_tabname-arena_table && '[]'.
      ELSEIF ls_tabname-arena_table_type EQ gc_table_type_n.
        lv_table_str = 'LS_PROD_DATA-' && ls_tabname-arena_table && '[]'.
      ENDIF.

      lv_wa_str    = ls_tabname-arena_table.

      ASSIGN (lv_table_str) TO <table>.
      ASSIGN (lv_wa_str)    TO <workarea>.

      READ TABLE <table> INTO <workarea> INDEX 1.
    ENDLOOP.

    ls_reg_hdr_db = zarn_reg_hdr.



* Default Only if asked, else return whatever in REG_HDR Table
    IF iv_reg_only IS INITIAL.


* Default for each regional table and field as per default logic as 'T' and 'C'
*    LOOP AT lt_tabname[] INTO ls_tabname WHERE arena_table_type EQ gc_table_type_r.
      LOOP AT lt_tabname[] INTO ls_tabname." WHERE arena_table = 'ZARN_REG_HDR'.



* Check if Table is default relevant
        READ TABLE me->gt_gui_set INTO ls_gui_set
                 WITH KEY tabname   = ls_tabname-arena_table
                          defaulted = abap_true.
        IF sy-subrc IS NOT INITIAL.
          CONTINUE.
        ENDIF.

        CLEAR: lt_dfies[].
*         table format for FM
        lv_ddif_tab = ls_tabname-arena_table.

* Get fields of reg table
        CALL FUNCTION 'DDIF_FIELDINFO_GET'
          EXPORTING
            tabname        = lv_ddif_tab
            langu          = sy-langu
            all_types      = abap_true
          TABLES
            dfies_tab      = lt_dfies[]
          EXCEPTIONS
            not_found      = 1
            internal_error = 2
            OTHERS         = 3.
        IF sy-subrc IS NOT INITIAL.
          CONTINUE.
        ENDIF.


        IF <itabname>     IS ASSIGNED. UNASSIGN <itabname>.     ENDIF.
        IF <scr_table>    IS ASSIGNED. UNASSIGN <scr_table>.    ENDIF.
        IF <reg_itabname> IS ASSIGNED. UNASSIGN <reg_itabname>. ENDIF.


* Select Data for all the regional tables
        CLEAR: lv_itabname,
               lv_reg_itabname.
        CONCATENATE 'LS_REG_DATA-' ls_tabname-arena_table INTO lv_itabname.
        ASSIGN (lv_itabname) TO <itabname>.
        IF <itabname> IS NOT ASSIGNED.
          CONTINUE.
        ENDIF.

        CONCATENATE 'LS_REG_DATA_DEF-' ls_tabname-arena_table INTO lv_reg_itabname.
        ASSIGN (lv_reg_itabname) TO <reg_itabname>.




* Default only if not enriched yet
*        IF ls_reg_hdr_db-laeda IS NOT INITIAL.
        IF ls_reg_hdr_db-data_enriched IS NOT INITIAL.
          IF <reg_itabname> IS ASSIGNED AND
            <itabname>      IS ASSIGNED.
            <reg_itabname> = <itabname>.
          ENDIF.
          EXIT.
        ENDIF.





* Workarea
        ASSIGN (ls_tabname-arena_table) TO <scr_table>.


        LOOP AT <itabname> ASSIGNING <table_str>.
* For each field
          LOOP AT lt_dfies[] INTO ls_dfies.

            IF ls_dfies-fieldname = 'IDNO'    OR
               ls_dfies-fieldname = 'VERSION' OR
               ls_dfies-fieldname = 'MANDT' .
              CONTINUE.
            ENDIF.


* Check if Table and field are default relevant
            READ TABLE me->gt_gui_set INTO ls_gui_set
                     WITH KEY tabname   = ls_dfies-tabname
                              fldname   = ls_dfies-fieldname
                              defaulted = abap_true.
            IF sy-subrc IS NOT INITIAL.
              CONTINUE.
*            ELSEIF ls_gui_set-overwrite IS INITIAL  AND
*                   zarn_reg_hdr-status  NE 'NEW'.             " Status check ?
*              " this should work as we dont allow regional enrichment until the status
*              " is APR after Article was created
*              CONTINUE.
            ENDIF.


*             Default logic
*             example - field name like ZARN_REG_HDR-FAN_ID
            IF <itabfieldname> IS ASSIGNED. UNASSIGN <itabfieldname>.      ENDIF.
            CLEAR: lv_itabfieldname.

            lv_itabfieldname = '<TABLE_STR>-' && ls_dfies-fieldname.
            ASSIGN (lv_itabfieldname) TO <itabfieldname>.

*             work through the defaults fromt able
            LOOP AT me->gt_gui_def INTO ls_gui_def
                 WHERE tabname   = ls_dfies-tabname
                   AND fldname   = ls_dfies-fieldname
                   AND ( logic_ty = gc_logic_t OR logic_ty = gc_logic_c ).

              CASE  ls_gui_def-logic_ty.
                WHEN me->gc_logic_t.
*                   PARAM1 - Table name
*                   PARAM2 - Field name


                  IF <deffieldname> IS ASSIGNED. UNASSIGN <deffieldname>.      ENDIF.
                  CLEAR: lv_deffieldname.
                  lv_deffieldname = ls_gui_def-param1 && '-' && ls_gui_def-param2  .
                  TRANSLATE lv_deffieldname TO UPPER CASE.
                  ASSIGN (lv_deffieldname) TO <deffieldname>.
                  IF <deffieldname> IS ASSIGNED.
                    <itabfieldname> = <deffieldname>.   " Assign value to fieldname from Itab
                  ENDIF.
                  IF <itabfieldname> IS NOT INITIAL.
                    EXIT.
                  ENDIF.

*                WHEN me->gc_logic_l.



                WHEN me->gc_logic_c.
*                   PARAM1 - Constant Value as is
                  <itabfieldname> = ls_gui_def-param1.
                  IF ls_gui_def-param1 IS NOT INITIAL.
                    EXIT.
                  ENDIF.

              ENDCASE.

            ENDLOOP. "LOOP AT gr_gui_load->gt_gui_def INTO ls_gui_def
          ENDLOOP.   " LOOP AT lt_dfies[] INTO ls_dfies.
        ENDLOOP.     " LOOP AT <itabname> ASSIGNING <table_str>.

*           Assign any global tables that we require data to be filled into for ALV
        IF <reg_itabname> IS ASSIGNED AND
          <itabname>      IS ASSIGNED.
          <reg_itabname> = <itabname>.
        ENDIF.
*      ENDIF.

      ENDLOOP.  " LOOP AT lt_tabname[] INTO ls_tabname


    ELSE.

    ENDIF. " IF iv_reg_only IS INITIAL


    CLEAR: ev_no_uom_avail, ev_no_uom_avail_ean.

* Default REG_HDR for Logic Type 'L'
    CALL METHOD me->set_reg_hdr_default
      EXPORTING
        is_reg_data     = ls_reg_data_def
        is_prod_data    = ls_prod_data
        is_reg_hdr_db   = ls_reg_hdr_db
        iv_reg_only     = iv_reg_only
      IMPORTING
        es_reg_data_def = ls_reg_data_def.

* Default REG_UOM for Logic Type 'L'
    CALL METHOD me->set_reg_uom_default
      EXPORTING
        is_reg_data           = ls_reg_data_def
        is_prod_data          = ls_prod_data
        iv_reg_only           = iv_reg_only
      IMPORTING
        es_reg_data_def       = ls_reg_data_def
        et_reg_uom_ui         = et_reg_uom_ui[]
        ev_no_uom_avail       = ev_no_uom_avail
        ev_dim_overflow_error = ev_dim_overflow_error.



    IF zarn_nat_mc_hier-nat_mc_code IN gt_range_greet_mc[].
* Greeting card MC
* Default REG_UOM, EAN for Logic Type 'L' got Greeting cards
      CALL METHOD me->set_reg_uom_ean_greeting_def
        EXPORTING
          is_reg_data     = ls_reg_data_def
          is_prod_data    = ls_prod_data
          it_reg_uom_ui   = et_reg_uom_ui[]
          iv_reg_only     = iv_reg_only
        IMPORTING
          es_reg_data_def = ls_reg_data_def
          et_reg_uom_ui   = et_reg_uom_ui[]
          et_reg_ean_ui   = et_reg_ean_ui[]
          ev_no_uom_avail = ev_no_uom_avail_ean.

    ELSE.
* NON Greeting cards
* Default REG_EAN for Logic Type 'L'
      CALL METHOD me->set_reg_ean_default
        EXPORTING
          is_reg_data     = ls_reg_data_def
          is_prod_data    = ls_prod_data
          it_reg_uom_ui   = et_reg_uom_ui[]
          iv_reg_only     = iv_reg_only
        IMPORTING
          es_reg_data_def = ls_reg_data_def
          et_reg_ean_ui   = et_reg_ean_ui[].
    ENDIF.



* Default REG_PIR for Logic Type 'L'
    CALL METHOD me->set_reg_pir_default
      EXPORTING
        is_reg_data     = ls_reg_data_def
        is_prod_data    = ls_prod_data
        it_reg_uom_ui   = et_reg_uom_ui[]
        iv_reg_only     = iv_reg_only
      IMPORTING
        es_reg_data_def = ls_reg_data_def
        et_reg_pir_ui   = et_reg_pir_ui[].


* Default REG_HSNO
    CALL METHOD me->set_reg_hsno_default
      EXPORTING
        is_reg_data     = ls_reg_data_def
        is_prod_data    = ls_prod_data
        iv_reg_only     = iv_reg_only
      IMPORTING
        es_reg_data_def = ls_reg_data_def.

* Default Supply Chain tab modifying UoM basic article table
* Added as this method is called after NAT approval and needs to populate
    CALL METHOD me->set_reg_sc_default_ui
      EXPORTING
        it_reg_ean_ui = et_reg_ean_ui[]
      CHANGING
        ct_reg_uom_ui = et_reg_uom_ui[].

    me->set_reg_allergen_default(
      EXPORTING
        is_reg_data     = ls_reg_data_def
        is_prod_data    = ls_prod_data
      CHANGING
        ct_reg_allergen = ls_reg_data_def-zarn_reg_allerg ).

    me->set_reg_string_default(
      EXPORTING
        is_prod_data    = ls_prod_data
      CHANGING
        ct_reg_str = ls_reg_data_def-zarn_reg_str ).

    me->set_reg_nutrients_default(
      EXPORTING
        is_prod_data    = ls_prod_data
      CHANGING
        ct_reg_nutrien = ls_reg_data_def-zarn_reg_nutrien ).

    es_reg_data_def = ls_reg_data_def.

  ENDMETHOD.                    "SET_REGIONAL_DEFAULT


  METHOD set_regional_default_ui.
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             # 17.07.2020
* CHANGE No.       # CIP-1359: ZARN_GUI changes (GS1  Updates)
* DESCRIPTION      # Added Regional Allergen table update
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------

    DATA: ls_reg_data     TYPE zsarn_reg_data,
          ls_prod_data    TYPE zsarn_prod_data,
          ls_reg_data_def TYPE zsarn_reg_data.

    CLEAR: et_reg_allergen_ui.

    gt_lfm1[]           = it_lfm1[].
    gt_fanid[]          = it_fanid[].
    gt_marm[]           = it_marm[].
    gt_marm_idno[]      = it_marm_idno[].
    gt_mean[]           = it_mean[].
    gt_prod_uom_t[]     = it_prod_uom_t[].
    gt_uom_cat[]        = it_uom_cat[].
    gt_uomcategory[]    = it_uomcategory[].
    gt_gen_uom_t[]      = it_gen_uom_t[].
    gt_ref_article[]    = it_ref_article[].
    gt_pir_vendor[]     = it_pir_vendor[].
    gt_tvarv_ean_cate[] = it_tvarv_ean_cate[].
    gt_host_prdtyp[]    = it_host_prdtyp[].
    gt_mc_subdep[]      = it_mc_subdep[].
    gv_pir_ekorg        = iv_pir_ekorg.


    ls_reg_data  = is_reg_data.
    ls_prod_data = is_prod_data.


    ls_reg_data_def = ls_reg_data.


* Retreive Initial Global Data
    CALL METHOD me->get_initial_data
      EXPORTING
        is_prod_data = ls_prod_data.

    me->read_global_data( ).

* Default REG_UOM for Logic Type 'L'
    CALL METHOD me->set_reg_uom_default_ui
      EXPORTING
        is_reg_data   = ls_reg_data_def
        is_prod_data  = ls_prod_data
        iv_reg_only   = iv_reg_only
      IMPORTING
        et_reg_uom_ui = et_reg_uom_ui[].

* Default REG_EAN for Logic Type 'L'
    CALL METHOD me->set_reg_ean_default_ui
      EXPORTING
        is_reg_data   = ls_reg_data_def
        is_prod_data  = ls_prod_data
        it_reg_uom_ui = et_reg_uom_ui[]
        iv_reg_only   = iv_reg_only
      IMPORTING
        et_reg_ean_ui = et_reg_ean_ui[].

* Default REG_PIR for Logic Type 'L'
    CALL METHOD me->set_reg_pir_default_ui
      EXPORTING
        is_reg_data   = ls_reg_data_def
        is_prod_data  = ls_prod_data
        it_reg_uom_ui = et_reg_uom_ui[]
        iv_reg_only   = iv_reg_only
      IMPORTING
        et_reg_pir_ui = et_reg_pir_ui[].

* Default Supply Chain tab modifying UoM basic article table
    CALL METHOD me->set_reg_sc_default_ui
      EXPORTING
        it_reg_ean_ui = et_reg_ean_ui[]
      CHANGING
        ct_reg_uom_ui = et_reg_uom_ui[].

    me->set_reg_allergen_default_ui(
      EXPORTING
        is_reg_data   = ls_reg_data_def
        is_prod_data  = ls_prod_data
      IMPORTING
        et_reg_allergen_ui = et_reg_allergen_ui ).

    es_reg_data_def = ls_reg_data_def.

  ENDMETHOD.                    "SET_REGIONAL_DEFAULT


METHOD set_reg_allergen_default.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 19.07.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1359: ZARN_GUI changes (GS1  Updates)
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Default Regional Allegen Data
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  DATA: lv_exists TYPE flag,
        lv_delete TYPE flag.

  LOOP AT is_prod_data-zarn_allergen INTO DATA(ls_allergen).

    CLEAR: lv_exists.
    READ TABLE ct_reg_allergen WITH KEY idno          = ls_allergen-idno
                                        allergen_type = ls_allergen-allergen_type
                               ASSIGNING FIELD-SYMBOL(<ls_reg_allergen>).
    IF sy-subrc = 0.
      lv_exists = abap_true.
    ELSE.
      APPEND INITIAL LINE TO ct_reg_allergen ASSIGNING <ls_reg_allergen>.
      MOVE-CORRESPONDING ls_allergen TO <ls_reg_allergen>.
    ENDIF.
    <ls_reg_allergen>-allergen_type_reg   = ls_allergen-allergen_type.
    <ls_reg_allergen>-lvl_containment_reg = ls_allergen-lvl_containment.

    IF lv_exists IS INITIAL.
      CONTINUE.
    ENDIF.

    " When override field matches Regional data, clear it as it is no longer relevant
    IF <ls_reg_allergen>-allergen_type_ovr = <ls_reg_allergen>-allergen_type_reg.
      CLEAR: <ls_reg_allergen>-allergen_type_ovr,
             <ls_reg_allergen>-allergen_type_ovr_src,
             <ls_reg_allergen>-allergen_type_ovr_date.
    ENDIF.

    IF <ls_reg_allergen>-lvl_containment_ovr = <ls_reg_allergen>-lvl_containment_reg.
      CLEAR: <ls_reg_allergen>-lvl_containment_ovr,
             <ls_reg_allergen>-lvl_containment_ovr_src,
             <ls_reg_allergen>-lvl_containment_ovr_date.
    ENDIF.

  ENDLOOP.

  " After Regional data update, National might delete the record. Accordingly, delete it from Regional
  LOOP AT ct_reg_allergen ASSIGNING <ls_reg_allergen> WHERE allergen_type_ovr IS INITIAL.

    IF NOT line_exists( is_prod_data-zarn_allergen[ idno          = <ls_reg_allergen>-idno
                                                    allergen_type = <ls_reg_allergen>-allergen_type ] ).
      CLEAR: <ls_reg_allergen>-idno.
      lv_delete = abap_true.
    ENDIF.

  ENDLOOP.
  IF lv_delete = abap_true.
    DELETE ct_reg_allergen WHERE idno IS INITIAL.
  ENDIF.

ENDMETHOD.


METHOD set_reg_allergen_default_ui.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 17.07.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1359: ZARN_GUI changes (GS1  Updates)
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Default Allergen Data
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  CLEAR: et_reg_allergen_ui.

  et_reg_allergen_ui = CORRESPONDING #( is_prod_data-zarn_allergen[] ).

  " In the old design, allergen table was populated only when overwrite field(s) updated. However, in the new design,
  " this table is always populated. Hence, need to combine National and Regional for old data
  LOOP AT is_reg_data-zarn_reg_allerg INTO DATA(ls_reg_allergen).

    READ TABLE et_reg_allergen_ui ASSIGNING FIELD-SYMBOL(<ls_reg_allergen_ui>) WITH KEY allergen_type = ls_reg_allergen-allergen_type.
    IF sy-subrc <> 0.
      APPEND INITIAL LINE TO et_reg_allergen_ui ASSIGNING <ls_reg_allergen_ui>.
    ENDIF.

    <ls_reg_allergen_ui>-data_db = ls_reg_allergen.
    <ls_reg_allergen_ui>-allergen_type_ovr_desc_fsni = VALUE #( gt_allergen_text[ allergen_type = <ls_reg_allergen_ui>-allergen_type_ovr ]-fsni_allergen_desc OPTIONAL ).
    "Do not show allergen type where no national record exists
    IF <ls_reg_allergen_ui>-allergen_type = <ls_reg_allergen_ui>-allergen_type_ovr.
      CLEAR <ls_reg_allergen_ui>-allergen_type.
    ENDIF.
  ENDLOOP.

  IF et_reg_allergen_ui[] IS NOT INITIAL.
    et_reg_allergen_ui = CORRESPONDING #( et_reg_allergen_ui FROM gt_allergen_text
                                                             USING allergen_type = allergen_type
                                                             MAPPING allergen_type_desc_fsni = fsni_allergen_desc ).
  ENDIF.

ENDMETHOD.


  METHOD set_reg_ean_default.

    DATA: ls_reg_data        TYPE zsarn_reg_data,
          ls_prod_data       TYPE zsarn_prod_data,
          lt_gui_def         TYPE SORTED TABLE OF zarn_gui_def WITH UNIQUE KEY tabname fldname logic_ty,


          lt_reg_ean_ui      TYPE ztarn_reg_ean_ui,
          ls_reg_ean_ui      TYPE zsarn_reg_ean_ui,

          lt_reg_ean         TYPE ztarn_reg_ean,
          ls_reg_ean         TYPE zarn_reg_ean,

          lt_reg_ean_db      TYPE ztarn_reg_ean,
          ls_reg_ean_db      TYPE zarn_reg_ean,

          lt_reg_uom_ui      TYPE ztarn_reg_uom_ui,
          ls_reg_uom_ui      TYPE zsarn_reg_uom_ui,
          lt_gtin_var_db     TYPE ztarn_gtin_var,
          ls_gtin_var_db     TYPE zarn_gtin_var,
          lt_gtin_var        TYPE ztarn_gtin_var,
          ls_gtin_var        TYPE zarn_gtin_var,
          ls_tvarv_ean_cate  TYPE tvarvc,

          ls_uom_cat         TYPE zarn_uom_cat,
          ls_mean            TYPE ty_s_mean,
          ls_fanid           TYPE ty_s_fanid,
          ls_products        TYPE zarn_products,

          ls_celltab         TYPE lvc_s_styl,
          lt_celltab         TYPE lvc_t_styl,

          lv_lower_uom       TYPE zarn_lower_uom,
          lv_lower_child_uom TYPE zarn_lower_child_uom,

          lv_ean_prefix      TYPE ean11,
          lv_eantp           TYPE numtp.




    FIELD-SYMBOLS: <ls_reg_ean_ui>      TYPE zsarn_reg_ean_ui,
                   <ls_reg_ean_ui_main> TYPE zsarn_reg_ean_ui.



    ls_reg_data     = is_reg_data.
    ls_prod_data    = is_prod_data.
    lt_reg_uom_ui[] = it_reg_uom_ui[].

* Already existing EAN in Regional Data
    lt_reg_ean_db[] = ls_reg_data-zarn_reg_ean[].


* Check if IDNO exist in Products, if no then no defaulting
    CLEAR ls_products.
    READ TABLE ls_prod_data-zarn_products[] INTO ls_products INDEX 1.
    IF sy-subrc NE 0.
      EXIT.
    ENDIF.


* Retreive Initial Global Data, if not initialized
    IF gv_initialized IS INITIAL.
      CALL METHOD me->get_initial_data
        EXPORTING
          is_prod_data = ls_prod_data.
    ENDIF.

    CLEAR: lt_reg_ean_ui[], lt_reg_ean[].


    INSERT LINES OF gt_gui_def_recon[] INTO TABLE lt_gui_def[].
    DELETE lt_gui_def[] WHERE tabname  NE 'ZARN_REG_EAN'.


* Default Only if asked, else return whatever in REG_EAN Table
    IF iv_reg_only IS INITIAL.

      LOOP AT ls_prod_data-zarn_gtin_var[] INTO ls_gtin_var.

        CLEAR ls_reg_ean_ui.
        ls_reg_ean_ui-mandt = sy-mandt.
        ls_reg_ean_ui-idno  = ls_gtin_var-idno.
        ls_reg_ean_ui-ean11 = ls_gtin_var-gtin_code.
        ls_reg_ean_ui-nat_ean_flag = abap_true.


        READ TABLE lt_gui_def TRANSPORTING NO FIELDS
        WITH TABLE KEY tabname  = 'ZARN_REG_EAN'
                       fldname  = 'HPEAN'
                       logic_ty = gc_logic_l.
        IF sy-subrc = 0.
          ls_reg_ean_ui-hpean = ls_gtin_var-gtin_current.
        ENDIF.


* Get lower uom and lower child uom codes from National
        CLEAR: lv_lower_uom, lv_lower_child_uom.
        CALL FUNCTION 'ZARN_GET_NAT_LOWER_CHILD_UOM'
          EXPORTING
            iv_idno            = ls_gtin_var-idno
            iv_version         = ls_gtin_var-version
            iv_uom_category    = 'RETAIL'
            iv_uom_code        = ls_gtin_var-uom_code
            iv_hybris_code     = space
            it_uom_variant     = ls_prod_data-zarn_uom_variant[]
            it_gtin_var        = ls_prod_data-zarn_gtin_var[]
          IMPORTING
            ev_lower_uom       = lv_lower_uom
            ev_lower_child_uom = lv_lower_child_uom
          EXCEPTIONS
            input_reqd         = 1
            OTHERS             = 2.



* Get MEINH from REG_UOM
        CLEAR ls_reg_uom_ui.
        READ TABLE lt_reg_uom_ui INTO ls_reg_uom_ui
        WITH KEY idno                 = ls_gtin_var-idno
                 pim_uom_code         = ls_gtin_var-uom_code
                 hybris_internal_code = space
                 lower_uom            = lv_lower_uom
                 lower_child_uom      = lv_lower_child_uom.
        IF sy-subrc EQ 0.

          ls_reg_ean_ui-meinh = ls_reg_uom_ui-meinh.



          READ TABLE lt_gui_def TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname  = 'ZARN_REG_EAN'
                         fldname  = 'EANTP'
                         logic_ty = gc_logic_l.
          IF sy-subrc = 0.

            LOOP AT gt_tvarv_ean_cate INTO ls_tvarv_ean_cate.
              CLEAR: lv_ean_prefix, lv_eantp.

* If GTIN starts from '9400989' then default it to "ZN"
              IF ls_tvarv_ean_cate-name EQ 'ZARN_GUI_DEFLT_EAN_PREFIX_CATE'.
                SPLIT ls_tvarv_ean_cate-low AT '~' INTO lv_ean_prefix lv_eantp.
                CONCATENATE lv_ean_prefix '*' INTO lv_ean_prefix.
                IF ls_reg_ean_ui-ean11 CP lv_ean_prefix.
                  ls_reg_ean_ui-eantp = lv_eantp.
                  EXIT.
                ENDIF.
              ENDIF.
            ENDLOOP.

* in case no prefix is matched then default it to "IE"
            IF ls_reg_ean_ui-eantp IS INITIAL.
              CLEAR ls_tvarv_ean_cate.
              READ TABLE gt_tvarv_ean_cate INTO ls_tvarv_ean_cate
              WITH KEY name = 'ZARN_GUI_DEFAULT_EAN_CATEGORY'.
              IF sy-subrc EQ 0.
                ls_reg_ean_ui-eantp = ls_tvarv_ean_cate-low.
              ENDIF.
            ENDIF.

          ENDIF.  " EANTP


          CLEAR ls_uom_cat.
          READ TABLE gt_uom_cat[] INTO ls_uom_cat
          WITH KEY uom = ls_reg_ean_ui-meinh.
          IF sy-subrc = 0.
            ls_reg_ean_ui-cat_seqno = ls_uom_cat-cat_seqno.
            ls_reg_ean_ui-seqno     = ls_uom_cat-seqno.
          ENDIF.



          IF iv_reg_only = abap_true.
            CLEAR: ls_reg_ean_ui-meinh, ls_reg_ean_ui-eantp.
          ENDIF.


          CLEAR: ls_celltab, lt_celltab[].
          ls_celltab-fieldname = 'IDNO'.
          ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
          INSERT ls_celltab INTO TABLE lt_celltab[].

          ls_celltab-fieldname = 'MEINH'.
          ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
          INSERT ls_celltab INTO TABLE lt_celltab[].

          ls_celltab-fieldname = 'EAN11'.
          ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
          INSERT ls_celltab INTO TABLE lt_celltab[].

          ls_celltab-fieldname = 'EANTP'.
          ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
          INSERT ls_celltab INTO TABLE lt_celltab[].

          ls_celltab-fieldname = 'HPEAN'.
          ls_celltab-style     = cl_gui_alv_grid=>mc_style_enabled.
          INSERT ls_celltab INTO TABLE lt_celltab[].

          ls_celltab-fieldname = 'NAT_EAN_FLAG'.
          ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
          INSERT ls_celltab INTO TABLE lt_celltab[].

          ls_celltab-fieldname = 'CAT_SEQNO'.
          ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
          INSERT ls_celltab INTO TABLE lt_celltab[].

          ls_celltab-fieldname = 'SEQNO'.
          ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
          INSERT ls_celltab INTO TABLE lt_celltab[].


          ls_reg_ean_ui-celltab[] = lt_celltab[].

          APPEND ls_reg_ean_ui TO lt_reg_ean_ui[].

        ENDIF.

      ENDLOOP.  " LOOP AT ls_prod_data-zarn_gtin_var[] INTO ls_gtin_var

    ENDIF.  " IF iv_reg_only IS INITIAL

**********************************************************************************


* If EAN already exist in DataBase
    LOOP AT lt_reg_ean_db[] INTO ls_reg_ean_db.


      CLEAR: ls_celltab, lt_celltab[].
      ls_celltab-fieldname = 'IDNO'.
      ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
      INSERT ls_celltab INTO TABLE lt_celltab[].

      ls_celltab-fieldname = 'NAT_EAN_FLAG'.
      ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
      INSERT ls_celltab INTO TABLE lt_celltab[].

      ls_celltab-fieldname = 'CAT_SEQNO'.
      ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
      INSERT ls_celltab INTO TABLE lt_celltab[].

      ls_celltab-fieldname = 'SEQNO'.
      ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
      INSERT ls_celltab INTO TABLE lt_celltab[].


      IF iv_reg_only = abap_true.

        ls_celltab-fieldname = 'MEINH'.
        ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
        INSERT ls_celltab INTO TABLE lt_celltab[].

        ls_celltab-fieldname = 'EAN11'.
        ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
        INSERT ls_celltab INTO TABLE lt_celltab[].

        ls_celltab-fieldname = 'EANTP'.
        ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
        INSERT ls_celltab INTO TABLE lt_celltab[].

        ls_celltab-fieldname = 'HPEAN'.
        ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
        INSERT ls_celltab INTO TABLE lt_celltab[].

      ENDIF.   " IF iv_reg_only = abap_true



      CLEAR ls_uom_cat.
      READ TABLE gt_uom_cat[] INTO ls_uom_cat
      WITH KEY uom = ls_reg_ean_db-meinh.


      READ TABLE lt_gui_def TRANSPORTING NO FIELDS
      WITH TABLE KEY tabname  = 'ZARN_REG_EAN'
                     fldname  = 'HPEAN'
                     logic_ty = gc_logic_l.
      IF sy-subrc = 0.
* Remove national Main GTIN if it is there in Regional
        IF ls_reg_ean_db-hpean = abap_true.
          IF <ls_reg_ean_ui_main> IS ASSIGNED. UNASSIGN <ls_reg_ean_ui_main>. ENDIF.
          READ TABLE lt_reg_ean_ui[] ASSIGNING <ls_reg_ean_ui_main>
          WITH KEY idno         = ls_reg_ean_db-idno
                   meinh        = ls_reg_ean_db-meinh
                   hpean        = abap_true
                   nat_ean_flag = abap_true.
          IF sy-subrc = 0.
            CLEAR <ls_reg_ean_ui_main>-hpean.
          ENDIF.
        ENDIF.
      ENDIF.  " HPEAN


      IF <ls_reg_ean_ui> IS ASSIGNED. UNASSIGN <ls_reg_ean_ui>. ENDIF.
      READ TABLE lt_reg_ean_ui[] ASSIGNING <ls_reg_ean_ui>
      WITH KEY idno  = ls_reg_ean_db-idno
               meinh = ls_reg_ean_db-meinh
               ean11 = ls_reg_ean_db-ean11.
      IF sy-subrc = 0.
        MOVE-CORRESPONDING ls_reg_ean_db TO <ls_reg_ean_ui>.

        <ls_reg_ean_ui>-cat_seqno    = ls_uom_cat-cat_seqno.
        <ls_reg_ean_ui>-seqno        = ls_uom_cat-seqno.
        ls_reg_ean_ui-nat_ean_flag   = abap_true.

      ELSE.
        CLEAR ls_reg_ean_ui.
        MOVE-CORRESPONDING ls_reg_ean_db TO ls_reg_ean_ui.
        ls_reg_ean_ui-nat_ean_flag = space.
        ls_reg_ean_ui-cat_seqno = ls_uom_cat-cat_seqno.
        ls_reg_ean_ui-seqno     = ls_uom_cat-seqno.
        ls_reg_ean_ui-celltab   = lt_celltab[].

        APPEND ls_reg_ean_ui TO lt_reg_ean_ui[].
      ENDIF.

    ENDLOOP.   " LOOP AT lt_reg_ean_db[] INTO ls_reg_ean_db

********************************************************************************

* INS Begin of IR5055159 JKH 16.09.2016
    IF iv_reg_only IS INITIAL.
* For enrichment mode, for any MEAN GTIN, if it doecn't exist in defaulted EANs
* and MEAN UOM exist in defaulted UOMs then that MEAN GTIN should also be defaulted


      CLEAR ls_fanid.
      READ TABLE gt_fanid[] INTO ls_fanid
      WITH TABLE KEY zzfan = ls_products-fan_id.
      IF sy-subrc = 0.
        LOOP AT gt_mean[] INTO ls_mean
          WHERE matnr = ls_fanid-matnr.

* Check if MEAN GTIN already exist in defaulted EANs
          READ TABLE lt_reg_ean_ui[] TRANSPORTING NO FIELDS
          WITH KEY idno  = ls_products-idno
                   ean11 = ls_mean-ean11.
          IF sy-subrc NE 0.
* If MEAN GTIN doesn't exist then,
* Check if MEAN UOM exist in defaulted UOM list, then only consider the MEAN GTINs
            READ TABLE lt_reg_uom_ui TRANSPORTING NO FIELDS
            WITH KEY idno  = ls_products-idno
                     meinh = ls_mean-meinh.
            IF sy-subrc = 0.

              CLEAR: ls_celltab, lt_celltab[].
              ls_celltab-fieldname = 'IDNO'.
              ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
              INSERT ls_celltab INTO TABLE lt_celltab[].

              ls_celltab-fieldname = 'MEINH'.
              ls_celltab-style     = cl_gui_alv_grid=>mc_style_enabled.
              INSERT ls_celltab INTO TABLE lt_celltab[].

              ls_celltab-fieldname = 'EAN11'.
              ls_celltab-style     = cl_gui_alv_grid=>mc_style_enabled.
              INSERT ls_celltab INTO TABLE lt_celltab[].

              ls_celltab-fieldname = 'EANTP'.
              ls_celltab-style     = cl_gui_alv_grid=>mc_style_enabled.
              INSERT ls_celltab INTO TABLE lt_celltab[].

              ls_celltab-fieldname = 'HPEAN'.
              ls_celltab-style     = cl_gui_alv_grid=>mc_style_enabled.
              INSERT ls_celltab INTO TABLE lt_celltab[].

              ls_celltab-fieldname = 'NAT_EAN_FLAG'.
              ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
              INSERT ls_celltab INTO TABLE lt_celltab[].

              ls_celltab-fieldname = 'CAT_SEQNO'.
              ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
              INSERT ls_celltab INTO TABLE lt_celltab[].

              ls_celltab-fieldname = 'SEQNO'.
              ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
              INSERT ls_celltab INTO TABLE lt_celltab[].


              CLEAR ls_uom_cat.
              READ TABLE gt_uom_cat[] INTO ls_uom_cat
              WITH KEY uom = ls_mean-meinh.

              CLEAR ls_reg_ean_ui.
              ls_reg_ean_ui-idno  = ls_products-idno.
              ls_reg_ean_ui-meinh = ls_mean-meinh.
              ls_reg_ean_ui-ean11 = ls_mean-ean11.

*              CALL FUNCTION 'CONVERSION_EXIT_EAN11_INPUT'
*                EXPORTING
*                  input  = ls_reg_ean_ui-ean11
*                IMPORTING
*                  output = ls_reg_ean_ui-ean11.


              READ TABLE lt_gui_def TRANSPORTING NO FIELDS
              WITH TABLE KEY tabname  = 'ZARN_REG_EAN'
                             fldname  = 'HPEAN'
                             logic_ty = gc_logic_l.
              IF sy-subrc = 0.
                ls_reg_ean_ui-eantp = ls_mean-eantp.
              ENDIF.

              READ TABLE lt_gui_def TRANSPORTING NO FIELDS
              WITH TABLE KEY tabname  = 'ZARN_REG_EAN'
                             fldname  = 'HPEAN'
                             logic_ty = gc_logic_l.
              IF sy-subrc = 0.
                ls_reg_ean_ui-hpean = space.
              ENDIF.

              ls_reg_ean_ui-nat_ean_flag = space.
              ls_reg_ean_ui-cat_seqno = ls_uom_cat-cat_seqno.
              ls_reg_ean_ui-seqno     = ls_uom_cat-seqno.
              ls_reg_ean_ui-celltab   = lt_celltab[].

              APPEND ls_reg_ean_ui TO lt_reg_ean_ui[].

            ENDIF. " READ TABLE lt_reg_uom_ui TRANSPORTING NO FIELDS
          ENDIF.  " READ TABLE lt_reg_eanm_ui TRANSPORTING NO FIELDS
        ENDLOOP.  " LOOP AT gt_mean[] INTO ls_mean
      ENDIF.  " READ TABLE gt_fanid[] INTO ls_fanid
    ENDIF.  " IF iv_reg_only IS INITIAL
* INS End of IR5055159 JKH 16.09.2016

********************************************************************************

    SORT lt_reg_ean_ui[] BY cat_seqno seqno ASCENDING
                               nat_ean_flag DESCENDING
                                      hpean DESCENDING.


    DELETE lt_reg_ean_ui[] WHERE meinh IS INITIAL.

* Move the Defaulted all EANs to export parameters
    LOOP AT lt_reg_ean_ui[] INTO ls_reg_ean_ui.
      CLEAR ls_reg_ean.
      MOVE-CORRESPONDING ls_reg_ean_ui TO ls_reg_ean.
      APPEND ls_reg_ean TO lt_reg_ean[].
    ENDLOOP.


    et_reg_ean_ui[] = lt_reg_ean_ui[].
    es_reg_data_def-zarn_reg_ean[] = lt_reg_ean[].

  ENDMETHOD.


  METHOD set_reg_ean_default_ui.

    DATA: ls_reg_data    TYPE zsarn_reg_data,
          ls_prod_data   TYPE zsarn_prod_data,

          lt_reg_ean_ui  TYPE ztarn_reg_ean_ui,
          ls_reg_ean_ui  TYPE zsarn_reg_ean_ui,
          lt_reg_ean_db  TYPE ztarn_reg_ean,
          ls_reg_ean_db  TYPE zarn_reg_ean,
          lt_reg_uom_ui  TYPE ztarn_reg_uom_ui,
          ls_reg_uom_ui  TYPE zsarn_reg_uom_ui,

          lt_gtin_var_db TYPE ztarn_gtin_var,
          ls_gtin_var_db TYPE zarn_gtin_var,

          ls_uom_cat     TYPE zarn_uom_cat,
          ls_celltab     TYPE lvc_s_styl,
          lt_celltab     TYPE lvc_t_styl.




*******************************************************************************

    ls_reg_data      = is_reg_data.
    ls_prod_data     = is_prod_data.
    lt_reg_uom_ui[]  = it_reg_uom_ui[].

    lt_reg_ean_db[]  = ls_reg_data-zarn_reg_ean[].
    lt_gtin_var_db[] = ls_prod_data-zarn_gtin_var[].


* Retreive Initial Global Data, if not initialized
    IF gv_initialized IS INITIAL.
      CALL METHOD me->get_initial_data
        EXPORTING
          is_prod_data = ls_prod_data.
    ENDIF.

*******************************************************************************

    LOOP AT lt_reg_ean_db[] INTO ls_reg_ean_db.

      CLEAR ls_reg_ean_ui.
      MOVE-CORRESPONDING ls_reg_ean_db TO ls_reg_ean_ui.

*NAT_EAN_FLAG
*CELLTAB

* Get UOM Category Sequence
      CLEAR ls_uom_cat.
      READ TABLE gt_uom_cat[] INTO ls_uom_cat
      WITH KEY uom = ls_reg_ean_db-meinh.
      IF sy-subrc = 0.
        ls_reg_ean_ui-seqno     = ls_uom_cat-seqno.
        ls_reg_ean_ui-cat_seqno = ls_uom_cat-cat_seqno.
      ENDIF.


* If EAN exist in National then set flag as true
      CLEAR ls_reg_uom_ui.
      READ TABLE lt_reg_uom_ui[] INTO ls_reg_uom_ui
      WITH KEY idno  = ls_reg_ean_db-idno
               meinh = ls_reg_ean_db-meinh.
      IF sy-subrc = 0.
        CLEAR ls_gtin_var_db.
        READ TABLE lt_gtin_var_db[] INTO ls_gtin_var_db
        WITH KEY idno      = ls_reg_ean_db-idno
                 uom_code  = ls_reg_uom_ui-pim_uom_code
                 gtin_code = ls_reg_ean_db-ean11.
        IF sy-subrc = 0.
          ls_reg_ean_ui-nat_ean_flag   = abap_true.
        ENDIF.
      ENDIF.


      CLEAR: ls_celltab, lt_celltab[].
      ls_celltab-fieldname = 'IDNO'.
      ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
      INSERT ls_celltab INTO TABLE lt_celltab[].

      ls_celltab-fieldname = 'NAT_EAN_FLAG'.
      ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
      INSERT ls_celltab INTO TABLE lt_celltab[].

      ls_celltab-fieldname = 'CAT_SEQNO'.
      ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
      INSERT ls_celltab INTO TABLE lt_celltab[].

      ls_celltab-fieldname = 'SEQNO'.
      ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
      INSERT ls_celltab INTO TABLE lt_celltab[].

      IF iv_reg_only = abap_true.

        ls_celltab-fieldname = 'MEINH'.
        ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
        INSERT ls_celltab INTO TABLE lt_celltab[].

        ls_celltab-fieldname = 'EAN11'.
        ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
        INSERT ls_celltab INTO TABLE lt_celltab[].

        ls_celltab-fieldname = 'EANTP'.
        ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
        INSERT ls_celltab INTO TABLE lt_celltab[].

        ls_celltab-fieldname = 'HPEAN'.
        ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
        INSERT ls_celltab INTO TABLE lt_celltab[].

      ELSE.

        IF ls_reg_ean_ui-nat_ean_flag   = abap_true.
          ls_celltab-fieldname = 'MEINH'.
          ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
          INSERT ls_celltab INTO TABLE lt_celltab[].

          ls_celltab-fieldname = 'EAN11'.
          ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
          INSERT ls_celltab INTO TABLE lt_celltab[].

          ls_celltab-fieldname = 'EANTP'.
          ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
          INSERT ls_celltab INTO TABLE lt_celltab[].

          ls_celltab-fieldname = 'HPEAN'.
          ls_celltab-style     = cl_gui_alv_grid=>mc_style_enabled.
          INSERT ls_celltab INTO TABLE lt_celltab[].
        ELSE.

          ls_celltab-fieldname = 'MEINH'.
          ls_celltab-style     = cl_gui_alv_grid=>mc_style_enabled.
          INSERT ls_celltab INTO TABLE lt_celltab[].

          ls_celltab-fieldname = 'EAN11'.
          ls_celltab-style     = cl_gui_alv_grid=>mc_style_enabled.
          INSERT ls_celltab INTO TABLE lt_celltab[].

          ls_celltab-fieldname = 'EANTP'.
          ls_celltab-style     = cl_gui_alv_grid=>mc_style_enabled.
          INSERT ls_celltab INTO TABLE lt_celltab[].

          ls_celltab-fieldname = 'HPEAN'.
          ls_celltab-style     = cl_gui_alv_grid=>mc_style_enabled.
          INSERT ls_celltab INTO TABLE lt_celltab[].

        ENDIF.  " IF ls_reg_ean_ui-nat_ean_flag   = abap_true
      ENDIF.   " IF iv_reg_only = abap_true

      ls_reg_ean_ui-celltab[] = lt_celltab[].

      APPEND ls_reg_ean_ui TO lt_reg_ean_ui[].

    ENDLOOP.



*******************************************************************************


    SORT lt_reg_ean_ui[] BY cat_seqno seqno ASCENDING
                               nat_ean_flag DESCENDING
                                      hpean DESCENDING.

    DELETE lt_reg_ean_ui[] WHERE meinh IS INITIAL.

    et_reg_ean_ui[] = lt_reg_ean_ui[].

  ENDMETHOD.


  METHOD set_reg_hdr_default.
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 13.12.2018
* CHANGE No.       # WSIWJ-143
* DESCRIPTION      # Fix defaults for Net Content & UoM not coming through
*                  # after switch to using table ZARN_NET_QTY
* WHO              # C90005557, Tessa Newman
*-----------------------------------------------------------------------
* DATE             # 16.01.2019
* CHANGE No.       # SUP-200
* DESCRIPTION      # Defaulting should't be performed for certain attributes
*                  # during the change
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
    DATA: ls_reg_data    TYPE zsarn_reg_data,
          ls_prod_data   TYPE zsarn_prod_data,
          lt_gui_def     TYPE SORTED TABLE OF zarn_gui_def WITH UNIQUE KEY tabname fldname logic_ty,

          ls_products    TYPE zarn_products,
          ls_products_ex TYPE zarn_products_ex,  "++ONLD-835 JKH 09.05.2017
          ls_prod_str    TYPE zarn_prod_str,     "++ONLD-835 JKH 09.05.2017
          ls_nat_mc_hier TYPE zarn_nat_mc_hier,
          ls_uom_variant TYPE zarn_uom_variant,
          ls_growing     TYPE zarn_growing,
          ls_diet_info   TYPE zarn_diet_info,
          ls_allergen    TYPE zarn_allergen,
          ls_ntr_claims  TYPE zarn_ntr_claims,
          ls_fanid       TYPE ty_s_fanid,
          ls_ref_article TYPE ty_s_ref_article,
          ls_mc_subdep   TYPE zmd_mc_subdep,
          ls_host_prdtyp TYPE zmd_host_prdtyp,
          ls_gen_uom_t   TYPE ty_s_gen_uom_t,


          lt_reg_hdr     TYPE ztarn_reg_hdr,
          ls_reg_hdr     TYPE zarn_reg_hdr,
          ls_reg_hdr_db  TYPE zarn_reg_hdr.


*    FIELD-SYMBOLS: <ls_reg_hdr> TYPE zarn_reg_hdr.



    ls_reg_data  = is_reg_data.
    ls_prod_data = is_prod_data.


    CLEAR: lt_reg_hdr[].

    INSERT LINES OF gt_gui_def_recon[] INTO TABLE lt_gui_def[].
    DELETE lt_gui_def[] WHERE tabname  NE 'ZARN_REG_HDR'.


* Check if IDNO exist in Products, if no then no defaulting
    CLEAR ls_products.
    READ TABLE ls_prod_data-zarn_products[] INTO ls_products INDEX 1.
    IF sy-subrc NE 0.
      EXIT.
    ENDIF.

    CLEAR ls_products_ex.
    READ TABLE ls_prod_data-zarn_products_ex[] INTO ls_products_ex INDEX 1.  "++ONLD-835 JKH 09.05.2017

    CLEAR ls_prod_str.
    READ TABLE ls_prod_data-zarn_prod_str[] INTO ls_prod_str INDEX 1.        "++ONLD-835 JKH 09.05.2017

* Default Only if asked, else return whatever in REG_HDR Table
    IF iv_reg_only = abap_true.

* Get REG_HDR data, if not found then also feilds need to be defaulted
      CLEAR ls_reg_hdr.
      READ TABLE ls_reg_data-zarn_reg_hdr[] INTO ls_reg_hdr INDEX 1.
      IF sy-subrc = 0.
        APPEND ls_reg_hdr TO lt_reg_hdr[].
      ENDIF.

      es_reg_data_def-zarn_reg_hdr[] = lt_reg_hdr[].
      es_reg_data_def-idno           = ls_products-idno.

      EXIT.
    ENDIF.  " IF iv_reg_only = abap_true


** Get REG_HDR data, if not found then also feilds need to be defaulted
*    IF <ls_reg_hdr> IS ASSIGNED. UNASSIGN <ls_reg_hdr>. ENDIF.
*    READ TABLE ls_reg_data-zarn_reg_hdr[] ASSIGNING <ls_reg_hdr> INDEX 1.
*    IF sy-subrc = 0.
*      CLEAR ls_nat_mc_hier.
*      READ TABLE ls_prod_data-zarn_nat_mc_hier[] INTO ls_nat_mc_hier INDEX 1.
*      IF sy-subrc = 0.
*        READ TABLE lt_gui_def TRANSPORTING NO FIELDS
*        WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
*                       fldname  = 'MATKL'
*                       logic_ty = gc_logic_l.
*        IF sy-subrc = 0.
*          <ls_reg_hdr>-matkl = ls_nat_mc_hier-nat_mc_code.
*        ENDIF.
*      ENDIF.
*    ENDIF.
*
*    IF <ls_reg_hdr> IS ASSIGNED. UNASSIGN <ls_reg_hdr>. ENDIF.




    ls_reg_hdr_db = is_reg_hdr_db.

** Default only if not enriched yet
*    IF ls_reg_hdr_db-laeda IS NOT INITIAL.
**      IF ls_reg_hdr IS NOT INITIAL.
**      APPEND ls_reg_hdr TO lt_reg_hdr[].
*
*      es_reg_data_def-zarn_reg_hdr[] = ls_reg_data-zarn_reg_hdr[].
*      es_reg_data_def-idno = ls_products-idno.
**      ENDIF.
*
*      EXIT.
*    ENDIF.


* Retreive Initial Global Data, if not initialized
    IF gv_initialized IS INITIAL.
      CALL METHOD me->get_initial_data
        EXPORTING
          is_prod_data = ls_prod_data.
    ENDIF.






* Get REG_HDR data, if not found then also feilds need to be defaulted
    CLEAR ls_reg_hdr.
    READ TABLE ls_reg_data-zarn_reg_hdr[] INTO ls_reg_hdr INDEX 1.


    ls_reg_hdr-idno    = ls_products-idno.
    ls_reg_hdr-version = ls_products-version.
*    ls_reg_hdr-laeda   = sy-datum.
*    ls_reg_hdr-eruet   = sy-uzeit.
*    ls_reg_hdr-aenam   = sy-uname.


    READ TABLE lt_gui_def TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
                   fldname  = 'MATNR'
                   logic_ty = gc_logic_l.
    IF sy-subrc = 0.
* Get Article Details
      CLEAR ls_fanid.
      READ TABLE gt_fanid[] INTO ls_fanid
      WITH TABLE KEY zzfan = ls_products-fan_id.
      IF sy-subrc = 0.
        ls_reg_hdr-matnr = ls_fanid-matnr.
      ENDIF.
    ENDIF.



    READ TABLE lt_gui_def TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
                   fldname  = 'RETAIL_NET_WEIGHT_UOM'
                   logic_ty = gc_logic_l.
    IF sy-subrc = 0.
* UOM Variants for Base Unit
      CLEAR ls_uom_variant.
      READ TABLE ls_prod_data-zarn_uom_variant[] INTO ls_uom_variant
      WITH KEY base_unit = abap_true.
      IF sy-subrc = 0.
        CLEAR ls_gen_uom_t.
        READ TABLE gt_gen_uom_t[] INTO ls_gen_uom_t
        WITH TABLE KEY gen_uom = ls_uom_variant-net_weight_uom
                       spras = sy-langu.
        IF sy-subrc = 0.
          ls_reg_hdr-retail_net_weight_uom = ls_gen_uom_t-sap_uom.
        ENDIF.
      ENDIF.
    ENDIF.



    READ TABLE lt_gui_def TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
                   fldname  = 'MSTDE'
                   logic_ty = gc_logic_l.
    IF sy-subrc = 0.
* Default to system date if MSTAE is changed from " " to <> " "
      IF ls_reg_hdr_db       IS NOT INITIAL AND
         ls_reg_hdr_db-mstae IS INITIAL     AND
         ls_reg_hdr-mstae    IS NOT INITIAL.
        ls_reg_hdr-mstde = sy-datum.
      ENDIF.
    ENDIF.



    READ TABLE lt_gui_def TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
                   fldname  = 'MSTDV'
                   logic_ty = gc_logic_l.
    IF sy-subrc = 0.
* Default to system date if MSTAV is changed from " " to <> " "
      IF ls_reg_hdr_db       IS NOT INITIAL AND
         ls_reg_hdr_db-mstav IS INITIAL     AND
         ls_reg_hdr-mstav    IS NOT INITIAL.
        ls_reg_hdr-mstdv = sy-datum.
      ENDIF.
    ENDIF.



    READ TABLE lt_gui_def TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
                   fldname  = 'SAISJ'
                   logic_ty = gc_logic_l.
    IF sy-subrc = 0.
* Default to Current year if Season (SAISO) is populated and Year (SAISJ) is " " or changed to " "
      IF ls_reg_hdr-saisj IS INITIAL     AND
         ls_reg_hdr-saiso IS NOT INITIAL.
        ls_reg_hdr-saisj = sy-datum+0(4).
      ENDIF.
    ENDIF.


    READ TABLE lt_gui_def TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
                   fldname  = 'INHAL'
                   logic_ty = gc_logic_l.
    IF sy-subrc = 0.
      IF ls_reg_hdr-inhal IS INITIAL.
*       first read net quantity table
        ls_reg_hdr-inhal = VALUE #( gt_net_qty[ idno = ls_products-idno ]-quantity OPTIONAL ).
        IF ls_reg_hdr-inhal IS INITIAL.
          ls_reg_hdr-inhal = ls_products-net_quantity.
        ENDIF.
      ENDIF.
    ENDIF.

    READ TABLE lt_gui_def TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
                   fldname  = 'INHME'
                   logic_ty = gc_logic_l.
    IF sy-subrc = 0.
      IF ls_reg_hdr-inhal IS NOT INITIAL AND ls_reg_hdr-inhme IS INITIAL.
        "first try net quantity table
        DATA(lv_net_qty_uom) = VALUE #( gt_net_qty[ idno = ls_products-idno ]-uom OPTIONAL ).
        IF lv_net_qty_uom IS INITIAL.
          lv_net_qty_uom = ls_products-net_quantity_uom.
        ENDIF.
        "convert unom
        IF lv_net_qty_uom IS NOT INITIAL.
          ls_reg_hdr-inhme = VALUE #( gt_gen_uom_t[ gen_uom = lv_net_qty_uom
                                                    spras = sy-langu ]-sap_uom OPTIONAL ).
        ENDIF.
      ENDIF.
    ENDIF.

* INS Begin of Change CTS-160 JKH 10.01.2017
* Default Brand Id to 'OTHM' if blank
    READ TABLE lt_gui_def TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
                   fldname  = 'BRAND_ID'
                   logic_ty = gc_logic_l.
    IF sy-subrc = 0.
      IF ls_reg_hdr-brand_id IS INITIAL.
        ls_reg_hdr-brand_id = zcl_constants=>gc_brand_id_default.    " 'OTHM'.
      ENDIF.
    ENDIF.
* INS End of Change CTS-160 JKH 10.01.2017



* INS Begin of Change Feature 3162 Jitin 13.10.2016
* Default Dangerous Goods Info Attributes from National

    READ TABLE lt_gui_def TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
                   fldname  = 'ZZUNNUM'
                   logic_ty = gc_logic_l.
    IF sy-subrc = 0.
      ls_reg_hdr-zzunnum    = ls_products-dgi_unn_code.
    ENDIF.

    READ TABLE lt_gui_def TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
                   fldname  = 'ZZPACKGRP'
                   logic_ty = gc_logic_l.
    IF sy-subrc = 0.
      ls_reg_hdr-zzpackgrp    = ls_products-dgi_packing_group.
    ENDIF.

    READ TABLE lt_gui_def TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
               fldname  = 'ZZHCLS'
               logic_ty = gc_logic_l.
    IF sy-subrc = 0.
      ls_reg_hdr-zzhcls    = ls_products-dgi_class.
    ENDIF.

    READ TABLE lt_gui_def TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
               fldname  = 'ZZHSNO'
               logic_ty = gc_logic_l.
    IF sy-subrc = 0.
      ls_reg_hdr-zzhsno    = ls_products-dgi_hazardous_code.
    ENDIF.

    READ TABLE lt_gui_def TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
               fldname  = 'ZZHARD'
               logic_ty = gc_logic_l.
    IF sy-subrc = 0.
      ls_reg_hdr-zzhard    = ls_products-hazardous_good.
    ENDIF.

    READ TABLE lt_gui_def TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
               fldname  = 'DGI_REGULATION_CODE'
               logic_ty = gc_logic_l.
    IF sy-subrc = 0.
      ls_reg_hdr-dgi_regulation_code   = ls_products-dgi_regulation_code.
    ENDIF.

    READ TABLE lt_gui_def TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
               fldname  = 'DGI_TECHNICAL_NAME'
               logic_ty = gc_logic_l.
    IF sy-subrc = 0.
      ls_reg_hdr-dgi_technical_name    = ls_products-dgi_technical_name.
    ENDIF.

    READ TABLE lt_gui_def TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
               fldname  = 'DGI_UNN_SHIPPING_NAME'
               logic_ty = gc_logic_l.
    IF sy-subrc = 0.
      ls_reg_hdr-dgi_unn_shipping_name = ls_products-dgi_unn_shipping_name.
    ENDIF.

    READ TABLE lt_gui_def TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
               fldname  = 'HSNO_APP'
               logic_ty = gc_logic_l.
    IF sy-subrc = 0.
      ls_reg_hdr-hsno_app              = ls_products-hsno_app.
    ENDIF.

    READ TABLE lt_gui_def TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
               fldname  = 'HSNO_HSR'
               logic_ty = gc_logic_l.
    IF sy-subrc = 0.
      ls_reg_hdr-hsno_hsr              = ls_products-hsno_hsr.
    ENDIF.

    READ TABLE lt_gui_def TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
               fldname  = 'DANGEROUS_GOOD'
               logic_ty = gc_logic_l.
    IF sy-subrc = 0.
      ls_reg_hdr-dangerous_good        = ls_products-dangerous_good.
    ENDIF.

    READ TABLE lt_gui_def TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
               fldname  = 'FLASH_PT_TEMP_VAL'
               logic_ty = gc_logic_l.
    IF sy-subrc = 0.
      ls_reg_hdr-flash_pt_temp_val     = ls_products-flash_pt_temp_val.
    ENDIF.

    READ TABLE lt_gui_def TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
               fldname  = 'FLASH_PT_TEMP_UOM'
               logic_ty = gc_logic_l.
    IF sy-subrc = 0.
      ls_reg_hdr-flash_pt_temp_uom     = ls_products-flash_pt_temp_uom.
    ENDIF.
* INS End of Change Feature 3162 Jitin 13.10.2016


    "++SUP-20 JKH 27.10.2017
    " As MC change for national is now captured on every update, thus MC
    " and related fields must be defaulted on every update
    " Earlier this was done on initialization, code moved from there.
* Get National MC code
    CLEAR ls_nat_mc_hier.
    READ TABLE ls_prod_data-zarn_nat_mc_hier[] INTO ls_nat_mc_hier INDEX 1.
    IF sy-subrc = 0.

      READ TABLE lt_gui_def TRANSPORTING NO FIELDS
      WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
                     fldname  = 'MATKL'
                     logic_ty = gc_logic_l.
      IF sy-subrc = 0.
        ls_reg_hdr-matkl = ls_nat_mc_hier-nat_mc_code.
      ENDIF.



      READ TABLE lt_gui_def TRANSPORTING NO FIELDS
      WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
                     fldname  = 'MTART'
                     logic_ty = gc_logic_l.
      IF sy-subrc = 0.
* Get Reference Article Details
        CLEAR ls_ref_article.
        READ TABLE gt_ref_article[] INTO ls_ref_article
        WITH TABLE KEY matkl = ls_nat_mc_hier-nat_mc_code.
        IF sy-subrc = 0.
          ls_reg_hdr-mtart = ls_ref_article-mtart.
        ENDIF.  " READ TABLE gt_ref_article[] INTO ls_ref_article
      ENDIF.


      READ TABLE lt_gui_def TRANSPORTING NO FIELDS
      WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
                     fldname  = 'ATTYP'
                     logic_ty = gc_logic_l.
      IF sy-subrc = 0.
        ls_reg_hdr-attyp = '00'.
      ENDIF.

      IF is_reg_hdr_db-matnr IS INITIAL.
        READ TABLE lt_gui_def TRANSPORTING NO FIELDS
        WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
                       fldname  = 'ZZAS4SUBDEPT'
                       logic_ty = gc_logic_l.
        IF sy-subrc = 0.
* Get Sub-Department
          CLEAR ls_mc_subdep.
          READ TABLE gt_mc_subdep[] INTO ls_mc_subdep
          WITH TABLE KEY matkl = ls_nat_mc_hier-nat_mc_code.
          IF sy-subrc = 0.

            ls_reg_hdr-zzas4subdept = ls_mc_subdep-subdep.


            READ TABLE lt_gui_def TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
                           fldname  = 'ZZPRDTYPE'
                           logic_ty = gc_logic_l.
            IF sy-subrc = 0.
              CLEAR ls_host_prdtyp.
              READ TABLE gt_host_prdtyp[] INTO ls_host_prdtyp
              WITH TABLE KEY wwgha = ls_mc_subdep-subdep.
              IF sy-subrc = 0.
                ls_reg_hdr-zzprdtype = ls_host_prdtyp-zzprdtype.
              ENDIF.  " READ TABLE gt_host_prdtyp[] INTO ls_host_prdtyp
            ENDIF.

          ENDIF.  " READ TABLE gt_mc_subdep[] INTO ls_mc_subdep

        ENDIF.
      ENDIF.

    ENDIF.  " READ TABLE ls_prod_data-zarn_nat_mc_hier[] INTO ls_nat_mc_hier

***********************************************************************

***************** N A T I O N A L   A T T R I B U T E S ***************

    READ TABLE lt_gui_def TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
                   fldname  = 'ZZATTR1'
                   logic_ty = gc_logic_l.
    IF sy-subrc = 0.
* Genetically Modified
      IF ls_products-genetically_modified NE 'UNDECLARED' AND
         ls_products-genetically_modified NE 'FREE_FROM'  AND
         ls_products-genetically_modified IS NOT INITIAL.
        ls_reg_hdr-zzattr1 = abap_true.
      ENDIF.
    ENDIF.



* Organic/Free Range
    LOOP AT ls_prod_data-zarn_growing[] INTO ls_growing.
      CASE ls_growing-growing_method.
        WHEN 'ORGANIC'.
          READ TABLE lt_gui_def TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
                         fldname  = 'ZZATTR2'
                         logic_ty = gc_logic_l.
          IF sy-subrc = 0.
            ls_reg_hdr-zzattr2 = abap_true.
          ENDIF.

        WHEN 'FREE_RANGE'.
          READ TABLE lt_gui_def TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
                         fldname  = 'ZZATTR10'
                         logic_ty = gc_logic_l.
          IF sy-subrc = 0.
            ls_reg_hdr-zzattr10 = abap_true.
          ENDIF.


      ENDCASE.
    ENDLOOP.


    READ TABLE lt_gui_def TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
                   fldname  = 'ZZATTR3'
                   logic_ty = gc_logic_l.
    IF sy-subrc = 0.
* Irradiated
      IF ls_products-irradiated = abap_true.
        ls_reg_hdr-zzattr3 = abap_true.
      ENDIF.
    ENDIF.



* Diet Type Attributes
    LOOP AT ls_prod_data-zarn_diet_info[] INTO ls_growing.
      CASE ls_diet_info-diet_type.
        WHEN 'KOSHER'.
          READ TABLE lt_gui_def TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
                         fldname  = 'ZZATTR4'
                         logic_ty = gc_logic_l.
          IF sy-subrc = 0.
            ls_reg_hdr-zzattr4 = abap_true.
          ENDIF.

        WHEN 'HALAL'.
          READ TABLE lt_gui_def TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
                         fldname  = 'ZZATTR9'
                         logic_ty = gc_logic_l.
          IF sy-subrc = 0.
            ls_reg_hdr-zzattr9 = abap_true.
          ENDIF.

        WHEN 'FREE_FROM_GLUTEN'.
          READ TABLE lt_gui_def TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
                         fldname  = 'ZZATTR11'
                         logic_ty = gc_logic_l.
          IF sy-subrc = 0.
            ls_reg_hdr-zzattr11 = abap_true.
          ENDIF.

        WHEN 'VEGAN'.
          READ TABLE lt_gui_def TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
                         fldname  = 'ZZATTR14'
                         logic_ty = gc_logic_l.
          IF sy-subrc = 0.
            ls_reg_hdr-zzattr14 = abap_true.
          ENDIF.

        WHEN 'VEGETARIAN'.
          READ TABLE lt_gui_def TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
                         fldname  = 'ZZATTR23'
                         logic_ty = gc_logic_l.
          IF sy-subrc = 0.
            ls_reg_hdr-zzattr23 = abap_true.
          ENDIF.

      ENDCASE.
    ENDLOOP.


    READ TABLE lt_gui_def TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
                   fldname  = 'ZZATTR5'
                   logic_ty = gc_logic_l.
    IF sy-subrc = 0.
* Allergy
      LOOP AT  ls_prod_data-zarn_allergen[] INTO ls_allergen
      WHERE allergen_type IS NOT INITIAL.
        ls_reg_hdr-zzattr5 = abap_true.
        EXIT.
      ENDLOOP.
    ENDIF.

* Nutritional Claims attributes
    LOOP AT ls_prod_data-zarn_ntr_claims[] INTO ls_ntr_claims.

      READ TABLE lt_gui_def TRANSPORTING NO FIELDS
      WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
                     fldname  = 'ZZATTR7'
                     logic_ty = gc_logic_l.
      IF sy-subrc = 0.
        IF ls_ntr_claims-nutritional_claims      = 'LOW' AND
           ls_ntr_claims-nutritional_claims_elem = 'FAT'.
          ls_reg_hdr-zzattr7 = abap_true.
        ENDIF.
      ENDIF.

      READ TABLE lt_gui_def TRANSPORTING NO FIELDS
      WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
                     fldname  = 'ZZATTR13'
                     logic_ty = gc_logic_l.
      IF sy-subrc = 0.
        IF ls_ntr_claims-nutritional_claims      CS 'FREE' AND
           ls_ntr_claims-nutritional_claims_elem CP 'SUGAR*'.
          ls_reg_hdr-zzattr13 = abap_true.
        ENDIF.
      ENDIF.

      READ TABLE lt_gui_def TRANSPORTING NO FIELDS
      WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
                     fldname  = 'ZZATTR22'
                     logic_ty = gc_logic_l.
      IF sy-subrc = 0.
        IF ls_ntr_claims-nutritional_claims      = 'LOW' AND
           ls_ntr_claims-nutritional_claims_elem = 'CHOLESTEROL'.
          ls_reg_hdr-zzattr22 = abap_true.
        ENDIF.
      ENDIF.

    ENDLOOP.



***********************************************************************

****************** R E G I O N A L    C R E A T I O N******************

*** Default only those fields which are required on REG_HDR Creation
*    IF ls_reg_hdr_db-laeda IS INITIAL.
    IF ls_reg_hdr_db-data_enriched IS INITIAL.

      READ TABLE lt_gui_def TRANSPORTING NO FIELDS       "++CI17-9 JKH 26.06.2017
      WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
                     fldname  = 'RETAIL_UNIT_DESC'
                     logic_ty = gc_logic_l.
      IF sy-subrc = 0.
        ls_reg_hdr-retail_unit_desc    = ls_products-fs_short_descr.
        REPLACE ALL OCCURRENCES OF '''' IN ls_reg_hdr-retail_unit_desc WITH ''.
      ENDIF.


      READ TABLE lt_gui_def TRANSPORTING NO FIELDS
      WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
                     fldname  = 'STATUS'
                     logic_ty = gc_logic_l.
      IF sy-subrc = 0.
        ls_reg_hdr-status    = 'NEW'.
      ENDIF.


      READ TABLE lt_gui_def TRANSPORTING NO FIELDS
      WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
               fldname  = 'ZZTKTPRNT'
               logic_ty = gc_logic_l.
      IF sy-subrc = 0.
        ls_reg_hdr-zztktprnt = space.
      ENDIF.


      READ TABLE lt_gui_def TRANSPORTING NO FIELDS
      WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
               fldname  = 'BSTAT'
               logic_ty = gc_logic_l.
      IF sy-subrc = 0.
        ls_reg_hdr-bstat     = space.
      ENDIF.


      READ TABLE lt_gui_def TRANSPORTING NO FIELDS
      WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
                     fldname  = 'RETAIL_NET_WEIGHT'
                     logic_ty = gc_logic_l.
      IF sy-subrc = 0.
* UOM Variants for Base Unit
        CLEAR ls_uom_variant.
        READ TABLE ls_prod_data-zarn_uom_variant[] INTO ls_uom_variant
        WITH KEY base_unit = abap_true.
        IF sy-subrc = 0.
          ls_reg_hdr-retail_net_weight     = ls_uom_variant-net_weight_value.
        ENDIF.
      ENDIF.


*      READ TABLE lt_gui_def TRANSPORTING NO FIELDS
*      WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
*                     fldname  = 'MSTAE'
*                     logic_ty = gc_logic_l.
*      IF sy-subrc = 0.
*        ls_reg_hdr-mstae = '13'.
*
*        READ TABLE lt_gui_def TRANSPORTING NO FIELDS
*        WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
*                       fldname  = 'MSTDE'
*                       logic_ty = gc_logic_l.
*        IF sy-subrc = 0.
*          ls_reg_hdr-mstde = sy-datum.
*        ENDIF.  " MSTDE
*      ENDIF.  " MSTAE



*      ls_reg_hdr-ersda     = sy-datum.
*      ls_reg_hdr-erzet     = sy-uzeit.
*      ls_reg_hdr-ernam     = sy-uname.




      READ TABLE lt_gui_def TRANSPORTING NO FIELDS
      WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
                     fldname  = 'MHDRZ'
                     logic_ty = gc_logic_l.
      IF sy-subrc = 0.
        IF ls_reg_hdr-zzprdtype IN gt_range_frzd_chld[].
          ls_reg_hdr-mhdrz = floor( ls_products-lifespan_production * ( 5 / 6 ) ).
        ENDIF.
      ENDIF.



* INS Begin of Change ONLD-835 JKH 30.05.2017
      READ TABLE lt_gui_def TRANSPORTING NO FIELDS
      WITH TABLE KEY tabname  = 'ZARN_REG_HDR'
                     fldname  = 'ONL_PRES_HEIGHT_DDN'
                     logic_ty = gc_logic_l.
      IF sy-subrc = 0.
        ls_reg_hdr-onl_pres_height_ddn = 'H'.
      ENDIF.
* INS End of Change ONLD-835 JKH 30.05.2017



      ls_reg_hdr-data_enriched = abap_true.

    ENDIF.  " IF ls_reg_hdr_db-data_enriched IS INITIAL


    IF ls_reg_hdr IS NOT INITIAL.
      APPEND ls_reg_hdr TO lt_reg_hdr[].

      es_reg_data_def-zarn_reg_hdr[] = lt_reg_hdr[].
      es_reg_data_def-idno = ls_products-idno.
    ENDIF.



  ENDMETHOD.


  METHOD set_reg_hsno_default.

    DATA: ls_reg_data  TYPE zsarn_reg_data,
          ls_prod_data TYPE zsarn_prod_data,

          ls_products  TYPE zarn_products,
          ls_hsno      TYPE zarn_hsno,

          lt_reg_hsno  TYPE ztarn_reg_hsno,
          ls_reg_hsno  TYPE zarn_reg_hsno.



    ls_reg_data  = is_reg_data.
    ls_prod_data = is_prod_data.


    CLEAR: lt_reg_hsno[].

* Check if IDNO exist in Products, if no then no defaulting
    CLEAR ls_products.
    READ TABLE ls_prod_data-zarn_products[] INTO ls_products INDEX 1.
    IF sy-subrc NE 0.
      EXIT.
    ENDIF.


    IF iv_reg_only = abap_true.
* Get REG_HSNO data from Regional data

      lt_reg_hsno[] = ls_reg_data-zarn_reg_hsno[].

      es_reg_data_def-zarn_reg_hsno[] = lt_reg_hsno[].
      es_reg_data_def-idno            = ls_products-idno.

      EXIT.
    ENDIF.  " IF iv_reg_only = abap_true


* Default from National data
    LOOP AT ls_prod_data-zarn_hsno[] INTO ls_hsno.
      CLEAR ls_reg_hsno.
      ls_reg_hsno-mandt     = sy-mandt.
      ls_reg_hsno-idno      = ls_hsno-idno.
      ls_reg_hsno-hsno_code = ls_hsno-hsno_code.
      APPEND ls_reg_hsno TO lt_reg_hsno[].
    ENDLOOP.


    es_reg_data_def-zarn_reg_hsno[] = lt_reg_hsno[].
    es_reg_data_def-idno            = ls_products-idno.

  ENDMETHOD.


METHOD set_reg_nutrients_default.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 20.07.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1359: ZARN_GUI changes (GS1  Updates)
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Default Regional Nutrients Data
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  " Update existing data
  LOOP AT is_prod_data-zarn_nutrients INTO DATA(ls_nutrient).

    READ TABLE ct_reg_nutrien ASSIGNING FIELD-SYMBOL(<ls_reg_nutrien>)
                              WITH KEY idno          = ls_nutrient-idno
                                       nutrient_type = ls_nutrient-nutrient_type.
    IF sy-subrc <> 0.
      APPEND INITIAL LINE TO ct_reg_nutrien ASSIGNING <ls_reg_nutrien>.
    ENDIF.
    MOVE-CORRESPONDING ls_nutrient TO <ls_reg_nutrien>.

  ENDLOOP.

  " Delete regional data when National is removed
  LOOP AT ct_reg_nutrien ASSIGNING <ls_reg_nutrien>.
    READ TABLE is_prod_data-zarn_nutrients WITH KEY idno          = <ls_reg_nutrien>-idno
                                                    nutrient_type = <ls_reg_nutrien>-nutrient_type
                                                    TRANSPORTING NO FIELDS.
    IF sy-subrc <> 0.
      CLEAR: <ls_reg_nutrien>-idno.
    ENDIF.
  ENDLOOP.
  DELETE ct_reg_nutrien WHERE idno IS INITIAL.

ENDMETHOD.


  METHOD set_reg_pir_default.
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             # 06.08.2020
* CHANGE No.       # SSM-1:	ZARN_GUI - Layout Module Changes
* DESCRIPTION      # Default variable order unit active both new and existing
*                  # records
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------

    DATA: ls_reg_data          TYPE zsarn_reg_data,
          ls_prod_data         TYPE zsarn_prod_data,

          lt_reg_uom_ui        TYPE ztarn_reg_uom_ui,
          ls_reg_uom_ui        TYPE zsarn_reg_uom_ui,
          lt_reg_uom_tmp       TYPE ztarn_reg_uom_ui,
          ls_reg_uom_tmp       TYPE zsarn_reg_uom_ui,
          lt_reg_pir_ui        TYPE ztarn_reg_pir_ui,
          ls_reg_pir_ui        TYPE zsarn_reg_pir_ui,
          lt_reg_pir_new       TYPE ztarn_reg_pir_ui,
          ls_reg_pir_new       TYPE zsarn_reg_pir_ui,
          lt_reg_pir_gln       TYPE ztarn_reg_pir_ui,
          ls_reg_pir_gln       TYPE zsarn_reg_pir_ui,
          lt_reg_pir_tmp       TYPE ztarn_reg_pir_ui,
          ls_reg_pir_tmp       TYPE zsarn_reg_pir_ui,
          lt_reg_pir_sort      TYPE ztarn_reg_pir_ui,
          ls_reg_pir_sort      TYPE zsarn_reg_pir_ui,
          ls_reg_pir_eina      TYPE zsarn_reg_pir_ui,
          ls_reg_pir_eine      TYPE zsarn_reg_pir_ui,
          lt_pir               TYPE zarn_pir,
          ls_pir               TYPE zarn_pir,
          lt_pir_tmp           TYPE ztarn_pir,
          ls_pir_tmp           TYPE zarn_pir,
          lt_reg_pir           TYPE ztarn_reg_pir,
          ls_reg_pir           TYPE zarn_reg_pir,
          lt_reg_pir_db        TYPE ztarn_reg_pir,
          ls_reg_pir_db        TYPE zarn_reg_pir,
          ls_products          TYPE zarn_products,

          ls_uom_variant_child TYPE zarn_uom_variant,
          ls_gtin_var_child    TYPE zarn_gtin_var,
          ls_uom_variant       TYPE zarn_uom_variant,
          lt_pir_vendor        TYPE ty_t_pir_vendor,
          ls_pir_vendor        TYPE ty_s_pir_vendor,
          ls_lfm1              TYPE ty_s_lfm1,


          lt_reg_lst_prc       TYPE ztarn_reg_lst_prc,
          ls_reg_lst_prc       TYPE zarn_reg_lst_prc,
          lt_list_price        TYPE ztarn_list_price,
          ls_list_price        TYPE zarn_list_price,
          lt_list_price_tmp    TYPE ztarn_list_price,
          ls_list_price_tmp    TYPE zarn_list_price,
          lt_list_price_gln    TYPE ztarn_list_price,
          ls_list_price_gln    TYPE zarn_list_price,

          lv_count             TYPE i,
          lv_idnlf             TYPE idnlf,
          lv_stop              TYPE flag.

    FIELD-SYMBOLS:
      <ls_reg_pir_ui>     TYPE zsarn_reg_pir_ui,
      <ls_reg_pir_ui_tmp> TYPE zsarn_reg_pir_ui,
      <ls_reg_pir_obs>    TYPE zsarn_reg_pir_ui,
      <ls_reg_pir_eina>   TYPE zsarn_reg_pir_ui,
      <ls_reg_pir_eine>   TYPE zsarn_reg_pir_ui,
      <ls_reg_pir_new>    TYPE zsarn_reg_pir_ui.

    ls_reg_data     = is_reg_data.
    ls_prod_data    = is_prod_data.
    lt_reg_uom_ui[] = it_reg_uom_ui[].


* Already existing PIR in Regional Data
    lt_reg_pir_db[] = ls_reg_data-zarn_reg_pir[].

* Already existing List Price in National Data
    lt_list_price[] = ls_prod_data-zarn_list_price[].
    DELETE lt_list_price[] WHERE active_lp NE abap_true.


* Retreive Initial Global Data, if not initialized
    IF gv_initialized IS INITIAL.
      CALL METHOD me->get_initial_data
        EXPORTING
          is_prod_data = ls_prod_data.
    ENDIF.

    CLEAR: lt_reg_pir[], lt_reg_pir_ui[], lt_reg_pir_new[].


* Default Only if asked, else return whatever in REG_PIR Table
    IF iv_reg_only IS INITIAL.

* Default PIR from National PIR
      LOOP AT ls_prod_data-zarn_pir[] INTO ls_pir.

        CLEAR ls_reg_pir_ui.
        ls_reg_pir_ui-mandt                 = sy-mandt.
        ls_reg_pir_ui-idno                  = ls_pir-idno.
        ls_reg_pir_ui-order_uom_pim         = ls_pir-uom_code.
        ls_reg_pir_ui-hybris_internal_code  = ls_pir-hybris_internal_code.
        ls_reg_pir_ui-vabme                 = zif_var_order_unit_act_ind_c=>gc_active.

        ls_reg_pir_ui-gln_no                = ls_pir-gln.
        ls_reg_pir_ui-gln_name              = ls_pir-gln_description.
        ls_reg_pir_ui-esokz                 = '0'.
        ls_reg_pir_ui-minbm                 = 1.
        ls_reg_pir_ui-norbm                 = 1.
        ls_reg_pir_ui-mwskz                 = 'P1'.
        ls_reg_pir_ui-lifab                 = sy-datum.                           "++3181 JKH 22.05.2017
        ls_reg_pir_ui-lifbi                 = ls_pir-avail_end_date+0(8).
        ls_reg_pir_ui-idnlf                 = ls_pir-vendor_article_number.
        ls_reg_pir_ui-pir_last_changed_date = ls_pir-last_changed_date.        "++IR5061015 JKH 26.08.2016

        CLEAR lv_idnlf.
        lv_idnlf = ls_reg_pir_ui-idnlf.
        TRANSLATE lv_idnlf TO UPPER CASE.
        CONDENSE lv_idnlf.

        IF lv_idnlf EQ 'UNKNOWN-CONFIRM' OR lv_idnlf EQ 'BLANK'.
          CLEAR ls_reg_pir_ui-idnlf.
        ENDIF.


        IF ls_reg_pir_ui-lifab IS NOT INITIAL AND
            ( ls_reg_pir_ui-lifbi IS INITIAL OR ls_reg_pir_ui-lifbi EQ space ).
          ls_reg_pir_ui-lifbi = '99991231'.
        ENDIF.


        CLEAR ls_uom_variant.
        READ TABLE ls_prod_data-zarn_uom_variant[] INTO ls_uom_variant
          WITH KEY idno     = ls_pir-idno
                   version  = ls_pir-version
                   uom_code = ls_pir-uom_code.
        IF sy-subrc = 0.

* Get Lower UOM and Lower child UOM
          CLEAR ls_gtin_var_child.
          READ TABLE ls_prod_data-zarn_gtin_var[] INTO ls_gtin_var_child
            WITH KEY idno      = ls_uom_variant-idno
                     version   = ls_uom_variant-version
                     gtin_code = ls_uom_variant-child_gtin.
          IF sy-subrc EQ 0 AND ls_gtin_var_child-uom_code IS NOT INITIAL.

            ls_reg_pir_ui-lower_uom = ls_gtin_var_child-uom_code.

            CLEAR ls_uom_variant_child.
            READ TABLE ls_prod_data-zarn_uom_variant[] INTO ls_uom_variant_child
              WITH KEY idno     = ls_uom_variant-idno
                       version  = ls_uom_variant-version
                       uom_code = ls_gtin_var_child-uom_code.
            IF sy-subrc EQ 0 AND ls_uom_variant_child-child_gtin IS NOT INITIAL.
              CLEAR ls_gtin_var_child.
              READ TABLE ls_prod_data-zarn_gtin_var[] INTO ls_gtin_var_child
                 WITH KEY idno      = ls_uom_variant_child-idno
                          version   = ls_uom_variant_child-version
                          gtin_code = ls_uom_variant_child-child_gtin.
              IF sy-subrc EQ 0.

                ls_reg_pir_ui-lower_child_uom = ls_gtin_var_child-uom_code.

              ENDIF.
            ENDIF.
          ENDIF.

        ENDIF.  " READ TABLE ls_prod_data-zarn_uom_variant[] INTO ls_uom_variant


* Get vendor from GLN
        CLEAR: ls_pir_vendor, lv_count.
        LOOP AT gt_pir_vendor[] INTO ls_pir_vendor
        WHERE bbbnr = ls_pir-gln(7)
          AND bbsnr = ls_pir-gln+7(5)
          AND bubkz = ls_pir-gln+12(1).
          lv_count = lv_count + 1.
        ENDLOOP.

        IF lv_count GT 1.
* multiple Vendors
          ls_reg_pir_ui-lifnr = 'MULTIPLE'.

        ELSEIF lv_count EQ 1.
* single Vendors
          ls_reg_pir_ui-lifnr = ls_pir_vendor-lifnr.

* Vendor master record purchasing organization data
          CLEAR ls_lfm1.
          READ TABLE gt_lfm1[] INTO ls_lfm1
          WITH TABLE KEY lifnr = ls_reg_pir_ui-lifnr
                         ekorg = gv_pir_ekorg.
          IF sy-subrc = 0.
            ls_reg_pir_ui-ekgrp = ls_lfm1-ekgrp.
            ls_reg_pir_ui-aplfz = ls_lfm1-plifz.
          ENDIF.
        ENDIF. " IF lv_count GT 1

* Get Pricing Data from national
        CLEAR ls_list_price.
        READ TABLE lt_list_price[] INTO ls_list_price
             WITH KEY idno      = ls_pir-idno
                      version   = ls_pir-version
                      uom_code  = ls_pir-uom_code
                      gln       = ls_pir-gln.
        IF sy-subrc = 0.
          ls_reg_pir_ui-waers                = ls_list_price-currency.
          ls_reg_pir_ui-netpr                = ls_list_price-price_value.
          ls_reg_pir_ui-peinh                = ls_list_price-quantity.
          ls_reg_pir_ui-datlb                = ls_list_price-eff_start_date.
          ls_reg_pir_ui-prdat                = ls_list_price-eff_end_date.
          ls_reg_pir_ui-lp_last_changed_date = ls_list_price-last_changed_date.  "++IR5061015 JKH 26.08.2016
        ENDIF.  " READ TABLE lt_list_price[] INTO ls_list_price


* Get Order Unit from already defaulted REG_UOM
        CLEAR ls_reg_uom_ui.
        READ TABLE lt_reg_uom_ui INTO ls_reg_uom_ui
        WITH KEY idno                 = ls_pir-idno
                 pim_uom_code         = ls_reg_pir_ui-order_uom_pim
                 hybris_internal_code = space
                 lower_uom            = ls_reg_pir_ui-lower_uom
                 lower_child_uom      = ls_reg_pir_ui-lower_child_uom.     "++INC5599313 JKH 03.02.2017
        IF sy-subrc = 0.
          ls_reg_pir_ui-bprme                = ls_reg_uom_ui-meinh.
          ls_reg_pir_ui-meinh                = ls_reg_uom_ui-meinh.
          ls_reg_pir_ui-lower_meinh          = ls_reg_uom_ui-lower_meinh.
          ls_reg_pir_ui-num_base_units       = ls_reg_uom_ui-num_base_units.
          ls_reg_pir_ui-factor_of_base_units = ls_reg_uom_ui-factor_of_base_units.
          ls_reg_pir_ui-unit_purord          = ls_reg_uom_ui-unit_purord.
          ls_reg_pir_ui-cat_seqno            = ls_reg_uom_ui-cat_seqno.

* INS Begin of Change 3181 JKH 22.05.2017
* Over delivery tollerence and Under delivery tollerence to default to 50%
* if these fields are initial and ZARN_REG_UOM- MEINH=’KG’ and UNIT_BASE=’X’.

* Over delivery tollerence
          IF ls_reg_pir_ui-uebto IS INITIAL AND
             ls_reg_uom_ui-meinh     = 'KG' AND
             ls_reg_uom_ui-unit_base = 'X'.
            ls_reg_pir_ui-uebto = '50'.
          ENDIF.

* Under delivery tollerence
          IF ls_reg_pir_ui-untto IS INITIAL AND
             ls_reg_uom_ui-meinh     = 'KG' AND
             ls_reg_uom_ui-unit_base = 'X'.
            ls_reg_pir_ui-untto = '50'.
          ENDIF.
* INS End of Change 3181 JKH 22.05.2017


        ENDIF.  " READ TABLE lt_reg_uom_ui INTO ls_reg_uom_ui

        ls_reg_pir_ui-bbbnr = ls_pir-gln(7).
        ls_reg_pir_ui-bbsnr = ls_pir-gln+7(5).
        ls_reg_pir_ui-bubkz = ls_pir-gln+12(1).


* Check if REG_PIR already exist in Regional DB then dont default that row, use from DB
        CLEAR ls_reg_pir.
        READ TABLE ls_reg_data-zarn_reg_pir[] INTO ls_reg_pir
        WITH KEY idno                 = ls_reg_pir_ui-idno
                 order_uom_pim        = ls_reg_pir_ui-order_uom_pim
                 hybris_internal_code = ls_reg_pir_ui-hybris_internal_code.
        IF sy-subrc = 0.

          ls_reg_pir-vabme    = zif_var_order_unit_act_ind_c=>gc_active.
          ls_reg_pir-gln_no   = ls_reg_pir_ui-gln_no.
          ls_reg_pir-gln_name = ls_reg_pir_ui-gln_name.
          ls_reg_pir-waers    = ls_reg_pir_ui-waers.
          ls_reg_pir-netpr    = ls_reg_pir_ui-netpr.
          ls_reg_pir-peinh    = ls_reg_pir_ui-peinh.
          ls_reg_pir-datlb    = ls_reg_pir_ui-datlb.
          ls_reg_pir-prdat    = ls_reg_pir_ui-prdat.
          ls_reg_pir-bprme    = ls_reg_pir_ui-bprme.
          ls_reg_pir-lower_uom       = ls_reg_pir_ui-lower_uom.          "++INC5599313 JKH 03.02.2017
          ls_reg_pir-lower_child_uom = ls_reg_pir_ui-lower_child_uom.    "++INC5599313 JKH 03.02.2017

* If value received from National then only default with national value
* else keep it as it is in regional
          IF ls_reg_pir_ui-idnlf IS NOT INITIAL.
            ls_reg_pir-idnlf = ls_reg_pir_ui-idnlf.
          ENDIF.

          IF ls_reg_pir-lifab IS INITIAL OR ls_reg_pir-lifab EQ space.             "++3181 JKH 22.05.2017
            ls_reg_pir-lifab = sy-datum.
          ENDIF.


          IF ls_reg_pir-lifab IS NOT INITIAL AND
             ( ls_reg_pir-lifbi IS INITIAL OR ls_reg_pir-lifbi EQ space ).
            ls_reg_pir-lifbi = '99991231'.
          ENDIF.

* INS Begin of Change 3181 JKH 22.05.2017
          CLEAR ls_reg_uom_ui.
          READ TABLE lt_reg_uom_ui INTO ls_reg_uom_ui
          WITH KEY idno                 = ls_reg_pir-idno
                   pim_uom_code         = ls_reg_pir-order_uom_pim
                   hybris_internal_code = space
                   lower_uom            = ls_reg_pir-lower_uom
                   lower_child_uom      = ls_reg_pir-lower_child_uom.
          IF sy-subrc = 0.
* Over delivery tollerence and Under delivery tollerence to default to 50%
* if these fields are initial and ZARN_REG_UOM- MEINH=’KG’ and UNIT_BASE=’X’.

* Over delivery tollerence
            IF ls_reg_pir-uebto IS INITIAL AND
               ls_reg_uom_ui-meinh     = 'KG' AND
               ls_reg_uom_ui-unit_base = 'X'.
              ls_reg_pir-uebto = '50'.
            ENDIF.

* Under delivery tollerence
            IF ls_reg_pir-untto IS INITIAL AND
               ls_reg_uom_ui-meinh     = 'KG' AND
               ls_reg_uom_ui-unit_base = 'X'.
              ls_reg_pir-untto = '50'.
            ENDIF.
          ENDIF.  " READ TABLE lt_reg_uom_ui INTO ls_reg_uom_ui
* INS End of Change 3181 JKH 22.05.2017

          MOVE-CORRESPONDING ls_reg_pir TO ls_reg_pir_ui.
          APPEND ls_reg_pir_ui TO lt_reg_pir_ui[].
        ELSE.

          APPEND ls_reg_pir_ui TO lt_reg_pir_ui[].
          APPEND ls_reg_pir_ui TO lt_reg_pir_new[].
        ENDIF.


      ENDLOOP.  " LOOP AT ls_prod_data-zarn_pir[] INTO ls_pir

    ENDIF.  " IF iv_reg_only IS INITIAL

********************************************************************************

* Add PIR which exist in Regional PIR but are missing in National PIR
    LOOP AT lt_reg_pir_db[] INTO ls_reg_pir_db.

      CLEAR ls_reg_pir_ui.
      READ TABLE lt_reg_pir_ui[] INTO ls_reg_pir_ui
      WITH KEY idno                 = ls_reg_pir_db-idno
               order_uom_pim        = ls_reg_pir_db-order_uom_pim
               hybris_internal_code = ls_reg_pir_db-hybris_internal_code.
      IF sy-subrc NE 0.


        IF ls_reg_pir_db-lifab IS INITIAL OR ls_reg_pir_db-lifab EQ space.             "++3181 JKH 22.05.2017
          ls_reg_pir_db-lifab = sy-datum.
        ENDIF.

        IF ls_reg_pir_db-lifab IS NOT INITIAL AND
           ( ls_reg_pir_db-lifbi IS INITIAL OR ls_reg_pir_db-lifbi EQ space ).
          ls_reg_pir_db-lifbi = '99991231'.
        ENDIF.


        MOVE-CORRESPONDING ls_reg_pir_db TO ls_reg_pir_ui.

* Set Obsolete Indicator
        IF iv_reg_only IS INITIAL.
          ls_reg_pir_ui-obsolete_ind = icon_dummy.
        ELSE.

          CLEAR ls_pir.
          READ TABLE ls_prod_data-zarn_pir[] INTO ls_pir
            WITH KEY idno                 = ls_reg_pir_ui-idno
                     uom_code             = ls_reg_pir_ui-order_uom_pim
                     hybris_internal_code = ls_reg_pir_ui-hybris_internal_code.
          IF sy-subrc NE 0.
            ls_reg_pir_ui-obsolete_ind = icon_dummy.
          ENDIF.

        ENDIF.  " IF iv_reg_only IS INITIAL




        CLEAR ls_reg_uom_ui.
        READ TABLE lt_reg_uom_ui INTO ls_reg_uom_ui
        WITH KEY idno                 = ls_reg_pir_ui-idno
                 pim_uom_code         = ls_reg_pir_ui-order_uom_pim
                 hybris_internal_code = space
                 lower_uom            = ls_reg_pir_ui-lower_uom
                 lower_child_uom      = ls_reg_pir_ui-lower_child_uom.     "++INC5599313 JKH 03.02.2017

        IF sy-subrc = 0.
          ls_reg_pir_ui-bprme                = ls_reg_uom_ui-meinh.  "++INC5599313 JKH 03.02.2017
          ls_reg_pir_ui-meinh                = ls_reg_uom_ui-meinh.
          ls_reg_pir_ui-lower_meinh          = ls_reg_uom_ui-lower_meinh.
          ls_reg_pir_ui-num_base_units       = ls_reg_uom_ui-num_base_units.
          ls_reg_pir_ui-factor_of_base_units = ls_reg_uom_ui-factor_of_base_units.
          ls_reg_pir_ui-unit_purord          = ls_reg_uom_ui-unit_purord.
          ls_reg_pir_ui-cat_seqno            = ls_reg_uom_ui-cat_seqno.


* INS Begin of Change 3181 JKH 22.05.2017
* Over delivery tollerence and Under delivery tollerence to default to 50%
* if these fields are initial and ZARN_REG_UOM- MEINH=’KG’ and UNIT_BASE=’X’.

* Over delivery tollerence
          IF ls_reg_pir_ui-uebto IS INITIAL AND
             ls_reg_uom_ui-meinh     = 'KG' AND
             ls_reg_uom_ui-unit_base = 'X'.
            ls_reg_pir_ui-uebto = '50'.
          ENDIF.

* Under delivery tollerence
          IF ls_reg_pir_ui-untto IS INITIAL AND
             ls_reg_uom_ui-meinh     = 'KG' AND
             ls_reg_uom_ui-unit_base = 'X'.
            ls_reg_pir_ui-untto = '50'.
          ENDIF.
* INS End of Change 3181 JKH 22.05.2017
        ENDIF.  " READ TABLE lt_reg_uom_ui INTO ls_reg_uom_ui

        ls_reg_pir_ui-bbbnr = ls_reg_pir_ui-gln_no(7).
        ls_reg_pir_ui-bbsnr = ls_reg_pir_ui-gln_no+7(5).
        ls_reg_pir_ui-bubkz = ls_reg_pir_ui-gln_no+12(1).


* Begin of Change IR5061015 JKH 26.08.2016
* Get Pricing Data from national
        CLEAR ls_pir.
        READ TABLE ls_prod_data-zarn_pir[] INTO ls_pir
          WITH KEY idno                 = ls_reg_pir_ui-idno
                   uom_code             = ls_reg_pir_ui-order_uom_pim
                   hybris_internal_code = ls_reg_pir_ui-hybris_internal_code.
        IF sy-subrc = 0.
          ls_reg_pir_ui-pir_last_changed_date = ls_pir-last_changed_date.
        ENDIF.


        CLEAR ls_list_price.
        READ TABLE lt_list_price[] INTO ls_list_price
             WITH KEY idno      = ls_reg_pir_ui-idno
                      uom_code  = ls_reg_pir_ui-order_uom_pim
                      gln       = ls_reg_pir_ui-gln_no.
        IF sy-subrc = 0.
          ls_reg_pir_ui-lp_last_changed_date = ls_list_price-last_changed_date.
        ENDIF.
* End of Change IR5061015 JKH 26.08.2016

        APPEND ls_reg_pir_ui TO lt_reg_pir_ui[].
      ENDIF.
    ENDLOOP.

*******************************************************************************


* Default Only if asked, else return whatever in REG_PIR Table
    IF iv_reg_only = abap_true.

*** Sort PIR as Regular Vendor GLN should be on the top, if found

      SORT lt_reg_pir_ui[] BY gln_no       ASCENDING
                              pir_rel_eina DESCENDING
                              pir_rel_eine DESCENDING.


      lt_reg_pir_sort[] = lt_reg_pir_ui[].

      CLEAR ls_reg_pir_sort.
      READ TABLE lt_reg_pir_sort[] INTO ls_reg_pir_sort
      WITH KEY relif = abap_true.
      IF sy-subrc = 0.
        DELETE lt_reg_pir_sort[] WHERE gln_no NE ls_reg_pir_sort-gln_no.
        DELETE lt_reg_pir_ui[]   WHERE gln_no EQ ls_reg_pir_sort-gln_no.

        SORT lt_reg_pir_sort[] BY relif        DESCENDING
                                  pir_rel_eina DESCENDING
                                  pir_rel_eine DESCENDING.

        APPEND LINES OF lt_reg_pir_ui[] TO lt_reg_pir_sort[].
        lt_reg_pir_ui[] = lt_reg_pir_sort[].

      ENDIF.


* Move the Defaulted all PIRs to export parameters
      LOOP AT lt_reg_pir_ui[] INTO ls_reg_pir_ui.
        CLEAR ls_reg_pir.
        MOVE-CORRESPONDING ls_reg_pir_ui TO ls_reg_pir.
        APPEND ls_reg_pir TO lt_reg_pir[].
      ENDLOOP.



      et_reg_pir_ui[] = lt_reg_pir_ui[].
      es_reg_data_def-zarn_reg_pir[] = lt_reg_pir[].

      EXIT.
    ENDIF.  " IF iv_reg_only = abap_true

*******************************************************************************


* EINA/EINE flag has to be set for each GLN, hence get the list of distinct GLNs
    lt_reg_pir_gln[] = lt_reg_pir_new[].

    SORT lt_reg_pir_gln[] BY gln_no.
    DELETE ADJACENT DUPLICATES FROM lt_reg_pir_gln[] COMPARING gln_no.

* For each GLN, set EINA/EINE flag
    LOOP AT lt_reg_pir_gln[] INTO ls_reg_pir_gln
      WHERE lifnr NE 'MULTIPLE'
        AND lifnr NE space.

* no defaulting if GLN already exist in DB
      CLEAR ls_reg_pir_db.
      READ TABLE lt_reg_pir_db[] INTO ls_reg_pir_db
      WITH KEY idno   = ls_reg_pir_gln-idno
               gln_no = ls_reg_pir_gln-gln_no.
      IF sy-subrc EQ 0.
        CONTINUE.
      ENDIF.


* If vendor is not found then, dont default EINA/EINE flag
      CLEAR ls_pir_vendor.
      READ TABLE gt_pir_vendor[] INTO ls_pir_vendor
      WITH KEY lifnr = ls_reg_pir_gln-lifnr.
      IF sy-subrc NE 0.
        CONTINUE.
      ENDIF.

*** Update PIR EINA flag
      CLEAR ls_reg_pir_ui.
      READ TABLE lt_reg_pir_ui[] INTO ls_reg_pir_ui
      WITH KEY idno         = ls_reg_pir_gln-idno
               gln_no       = ls_reg_pir_gln-gln_no
               pir_rel_eina = abap_true.
      IF sy-subrc NE 0.

        lt_pir_tmp[] = ls_prod_data-zarn_pir[].
        DELETE lt_pir_tmp[] WHERE gln NE ls_reg_pir_gln-gln_no.

* Get the highest effective date
        SORT lt_pir_tmp[] BY effective_date DESCENDING.
        CLEAR ls_pir_tmp.
        READ TABLE lt_pir_tmp[] INTO ls_pir_tmp INDEX 1.
        IF sy-subrc = 0.
* Keep only records for highest effective date, ignore rest
          DELETE lt_pir_tmp[] WHERE effective_date NE ls_pir_tmp-effective_date.
        ENDIF.


        CLEAR: lt_reg_uom_tmp[].
        LOOP AT lt_pir_tmp[] INTO ls_pir_tmp.

          lt_reg_pir_tmp[] = lt_reg_pir_ui[].

* Get lower and lower child uom for each PIR for highest effective date
          CLEAR ls_reg_pir_tmp.
          READ TABLE lt_reg_pir_tmp[] INTO ls_reg_pir_tmp
          WITH KEY idno                 = ls_pir_tmp-idno
                   order_uom_pim        = ls_pir_tmp-uom_code
                   hybris_internal_code = ls_pir_tmp-hybris_internal_code
                   gln_no               = ls_reg_pir_gln-gln_no.
          IF sy-subrc = 0.

* Get All the UOMs for lower and lower child uom for each PIR for highest effective date
            LOOP AT lt_reg_uom_ui[] INTO ls_reg_uom_ui
              WHERE idno                 = ls_pir_tmp-idno
                AND pim_uom_code         = ls_reg_pir_tmp-order_uom_pim
                AND hybris_internal_code = space
                AND lower_uom            = ls_reg_pir_tmp-lower_uom
                AND lower_child_uom      = ls_reg_pir_tmp-lower_child_uom.

              CLEAR ls_reg_uom_tmp.
              ls_reg_uom_tmp = ls_reg_uom_ui.
              APPEND ls_reg_uom_tmp TO lt_reg_uom_tmp[].
            ENDLOOP.

          ENDIF.
        ENDLOOP.

* Get the highest UOM Category and UOM on the top
        SORT lt_reg_uom_tmp[] BY cat_seqno DESCENDING seqno DESCENDING.

* Get the topmost UOM
        CLEAR ls_reg_uom_tmp.
        READ TABLE lt_reg_uom_tmp[] INTO ls_reg_uom_tmp INDEX 1.
        IF sy-subrc = 0.

* Default EINA Flag for topmost UOM
          READ TABLE lt_reg_pir_ui[] ASSIGNING <ls_reg_pir_ui>
          WITH KEY idno            = ls_reg_pir_gln-idno
                   order_uom_pim   = ls_reg_uom_tmp-pim_uom_code
                   lower_uom       = ls_reg_uom_tmp-lower_uom
                   lower_child_uom = ls_reg_uom_tmp-lower_child_uom
                   gln_no          = ls_reg_pir_gln-gln_no.
          IF sy-subrc = 0.
            <ls_reg_pir_ui>-pir_rel_eina = abap_true.
          ENDIF.


        ENDIF.  " READ TABLE lt_reg_uom_tmp[] INTO ls_reg_uom_tmp INDEX 1


      ENDIF.  "  READ TABLE lt_reg_pir_ui[] INTO ls_reg_pir_ui - Update PIR EINA flag


*** Update PIR EINE flag
      CLEAR ls_reg_pir_ui.
      READ TABLE lt_reg_pir_ui[] INTO ls_reg_pir_ui
      WITH KEY idno         = ls_reg_pir_gln-idno
               gln_no       = ls_reg_pir_gln-gln_no
               pir_rel_eine = abap_true.
      IF sy-subrc NE 0.

        lt_list_price_tmp[] = lt_list_price[].
        DELETE lt_list_price_tmp[] WHERE gln NE ls_reg_pir_gln-gln_no.

* Get the highest last changed date
        SORT lt_list_price_tmp[] BY last_changed_date DESCENDING.
        CLEAR ls_list_price_tmp.
        READ TABLE lt_list_price_tmp[] INTO ls_list_price_tmp INDEX 1.
        IF sy-subrc = 0.
* Keep only records for highest last changed date, ignore rest
          DELETE lt_list_price_tmp[] WHERE last_changed_date NE ls_list_price_tmp-last_changed_date.
        ENDIF.




        CLEAR: lt_reg_uom_tmp[].
        LOOP AT lt_list_price_tmp[] INTO ls_list_price_tmp.

          lt_reg_pir_tmp[] = lt_reg_pir_ui[].

* Get lower and lower child uom for each PIR for highest last changed date
          CLEAR ls_reg_pir_tmp.
          READ TABLE lt_reg_pir_tmp[] INTO ls_reg_pir_tmp
          WITH KEY idno                 = ls_list_price_tmp-idno
                   order_uom_pim        = ls_list_price_tmp-uom_code
                   gln_no               = ls_reg_pir_gln-gln_no.
          IF sy-subrc = 0.

* Get All the UOMs for lower and lower child uom for each PIR for highest last changed date
            LOOP AT lt_reg_uom_ui[] INTO ls_reg_uom_ui
              WHERE idno                 = ls_list_price_tmp-idno
                AND pim_uom_code         = ls_reg_pir_tmp-order_uom_pim
                AND hybris_internal_code = space
                AND lower_uom            = ls_reg_pir_tmp-lower_uom
                AND lower_child_uom      = ls_reg_pir_tmp-lower_child_uom.

              CLEAR ls_reg_uom_tmp.
              ls_reg_uom_tmp = ls_reg_uom_ui.
              APPEND ls_reg_uom_tmp TO lt_reg_uom_tmp[].
            ENDLOOP.
          ENDIF.
        ENDLOOP.

* Get the highest UOM Category and UOM on the top
        SORT lt_reg_uom_tmp[] BY cat_seqno DESCENDING seqno DESCENDING.

* Get the topmost UOM
        CLEAR ls_reg_uom_tmp.
        READ TABLE lt_reg_uom_tmp[] INTO ls_reg_uom_tmp INDEX 1.
        IF sy-subrc = 0.

* Default EINE Flag for topmost UOM
          READ TABLE lt_reg_pir_ui[] ASSIGNING <ls_reg_pir_ui>
          WITH KEY idno            = ls_reg_pir_gln-idno
                   order_uom_pim   = ls_reg_uom_tmp-pim_uom_code
                   lower_uom       = ls_reg_uom_tmp-lower_uom
                   lower_child_uom = ls_reg_uom_tmp-lower_child_uom
                   gln_no          = ls_reg_pir_gln-gln_no.
          IF sy-subrc = 0.
            <ls_reg_pir_ui>-pir_rel_eine = abap_true.
          ENDIF.

        ENDIF.  " READ TABLE lt_reg_uom_tmp[] INTO ls_reg_uom_tmp INDEX 1

      ENDIF.  "  READ TABLE lt_reg_pir_ui[] INTO ls_reg_pir_ui - Update PIR EINA flag

    ENDLOOP.  " LOOP AT lt_pir_gln[] INTO ls_reg_pir_gln



********************************************************************************


* Nationally proposed EINA/EINE flag has to be set for each GLN, hence get the list of distinct GLNs
    lt_reg_pir_gln[] = lt_reg_pir_ui[].

    SORT lt_reg_pir_gln[] BY gln_no.
    DELETE ADJACENT DUPLICATES FROM lt_reg_pir_gln[] COMPARING gln_no.

* For each GLN, set EINA/EINE flag
    LOOP AT lt_reg_pir_gln[] INTO ls_reg_pir_gln.

** If vendor is not found then, dont default EINA/EINE flag

*** Update PIR EINA flag
      lt_pir_tmp[] = ls_prod_data-zarn_pir[].
      DELETE lt_pir_tmp[] WHERE gln NE ls_reg_pir_gln-gln_no.

* Get the highest effective date
      SORT lt_pir_tmp[] BY effective_date DESCENDING.
      CLEAR ls_pir_tmp.
      READ TABLE lt_pir_tmp[] INTO ls_pir_tmp INDEX 1.
      IF sy-subrc = 0.
* Keep only records for highest effective date, ignore rest
        DELETE lt_pir_tmp[] WHERE effective_date NE ls_pir_tmp-effective_date.
      ENDIF.


      CLEAR: lt_reg_uom_tmp[].
      LOOP AT lt_pir_tmp[] INTO ls_pir_tmp.

        lt_reg_pir_tmp[] = lt_reg_pir_ui[].

* Get lower and lower child uom for each PIR for highest effective date
        CLEAR ls_reg_pir_tmp.
        READ TABLE lt_reg_pir_tmp[] INTO ls_reg_pir_tmp
        WITH KEY idno                 = ls_pir_tmp-idno
                 order_uom_pim        = ls_pir_tmp-uom_code
                 hybris_internal_code = ls_pir_tmp-hybris_internal_code
                 gln_no               = ls_reg_pir_gln-gln_no.
        IF sy-subrc = 0.

* Get All the UOMs for lower and lower child uom for each PIR for highest effective date
          LOOP AT lt_reg_uom_ui[] INTO ls_reg_uom_ui
            WHERE idno                 = ls_pir_tmp-idno
              AND pim_uom_code         = ls_reg_pir_tmp-order_uom_pim
              AND hybris_internal_code = space
              AND lower_uom            = ls_reg_pir_tmp-lower_uom
              AND lower_child_uom      = ls_reg_pir_tmp-lower_child_uom.

            CLEAR ls_reg_uom_tmp.
            ls_reg_uom_tmp = ls_reg_uom_ui.
            APPEND ls_reg_uom_tmp TO lt_reg_uom_tmp[].
          ENDLOOP.

        ENDIF.
      ENDLOOP.

* Get the highest UOM Category and UOM on the top
      SORT lt_reg_uom_tmp[] BY cat_seqno DESCENDING seqno DESCENDING.

* Get the topmost UOM
      CLEAR ls_reg_uom_tmp.
      READ TABLE lt_reg_uom_tmp[] INTO ls_reg_uom_tmp INDEX 1.
      IF sy-subrc = 0.

* Default EINA Flag for topmost UOM
        READ TABLE lt_reg_pir_ui[] ASSIGNING <ls_reg_pir_ui>
        WITH KEY idno            = ls_reg_pir_gln-idno
                 order_uom_pim   = ls_reg_uom_tmp-pim_uom_code
                 lower_uom       = ls_reg_uom_tmp-lower_uom
                 lower_child_uom = ls_reg_uom_tmp-lower_child_uom
                 gln_no          = ls_reg_pir_gln-gln_no.
        IF sy-subrc = 0.
          <ls_reg_pir_ui>-nat_pir_rel_eina = abap_true.
        ENDIF.


      ENDIF.  " READ TABLE lt_reg_uom_tmp[] INTO ls_reg_uom_tmp INDEX 1


*** Update PIR EINE flag
      lt_list_price_tmp[] = lt_list_price[].
      DELETE lt_list_price_tmp[] WHERE gln NE ls_reg_pir_gln-gln_no.

* Get the highest last changed date
      SORT lt_list_price_tmp[] BY last_changed_date DESCENDING.
      CLEAR ls_list_price_tmp.
      READ TABLE lt_list_price_tmp[] INTO ls_list_price_tmp INDEX 1.
      IF sy-subrc = 0.
* Keep only records for highest last changed date, ignore rest
        DELETE lt_list_price_tmp[] WHERE last_changed_date NE ls_list_price_tmp-last_changed_date.
      ENDIF.




      CLEAR: lt_reg_uom_tmp[].
      LOOP AT lt_list_price_tmp[] INTO ls_list_price_tmp.

        lt_reg_pir_tmp[] = lt_reg_pir_ui[].

* Get lower and lower child uom for each PIR for highest last changed date
        CLEAR ls_reg_pir_tmp.
        READ TABLE lt_reg_pir_tmp[] INTO ls_reg_pir_tmp
        WITH KEY idno                 = ls_list_price_tmp-idno
                 order_uom_pim        = ls_list_price_tmp-uom_code
                 gln_no               = ls_reg_pir_gln-gln_no.
        IF sy-subrc = 0.

* Get All the UOMs for lower and lower child uom for each PIR for highest last changed date
          LOOP AT lt_reg_uom_ui[] INTO ls_reg_uom_ui
            WHERE idno                 = ls_list_price_tmp-idno
              AND pim_uom_code         = ls_reg_pir_tmp-order_uom_pim
              AND hybris_internal_code = space
              AND lower_uom            = ls_reg_pir_tmp-lower_uom
              AND lower_child_uom      = ls_reg_pir_tmp-lower_child_uom.

            CLEAR ls_reg_uom_tmp.
            ls_reg_uom_tmp = ls_reg_uom_ui.
            APPEND ls_reg_uom_tmp TO lt_reg_uom_tmp[].
          ENDLOOP.
        ENDIF.
      ENDLOOP.

* Get the highest UOM Category and UOM on the top
      SORT lt_reg_uom_tmp[] BY cat_seqno DESCENDING seqno DESCENDING.

* Get the topmost UOM
      CLEAR ls_reg_uom_tmp.
      READ TABLE lt_reg_uom_tmp[] INTO ls_reg_uom_tmp INDEX 1.
      IF sy-subrc = 0.

* Default EINE Flag for topmost UOM
        READ TABLE lt_reg_pir_ui[] ASSIGNING <ls_reg_pir_ui>
        WITH KEY idno            = ls_reg_pir_gln-idno
                 order_uom_pim   = ls_reg_uom_tmp-pim_uom_code
                 lower_uom       = ls_reg_uom_tmp-lower_uom
                 lower_child_uom = ls_reg_uom_tmp-lower_child_uom
                 gln_no          = ls_reg_pir_gln-gln_no.
        IF sy-subrc = 0.
          <ls_reg_pir_ui>-nat_pir_rel_eine = abap_true.
        ENDIF.

      ENDIF.  " READ TABLE lt_reg_uom_tmp[] INTO ls_reg_uom_tmp INDEX 1

    ENDLOOP.  " LOOP AT lt_pir_gln[] INTO ls_reg_pir_gln


********************************************************************************
*** Update PIR RELIF flag - regular vendor

* Check if Regular Vendor if already defaulted or not, if not then only default
    CLEAR ls_reg_pir_ui.
    READ TABLE lt_reg_pir_ui[] INTO ls_reg_pir_ui
    WITH KEY relif  = abap_true.
    IF sy-subrc NE 0.

* If not defaulted then get the GLN of the Last Information Provider from national products table
      CLEAR ls_products.
      READ TABLE ls_prod_data-zarn_products INTO ls_products INDEX 1.
      IF sy-subrc = 0.

* if found, then default that GLN in PIR where EINA is defaulted
        LOOP AT lt_reg_pir_ui[] ASSIGNING <ls_reg_pir_ui>
          WHERE idno         EQ ls_products-idno  " ls_reg_pir_gln-idno
            AND pir_rel_eina EQ abap_true
            AND gln_no       EQ ls_products-gln   " ls_reg_pir_gln-gln_no
            AND ( lifnr      NE space AND lifnr NE 'MULTIPLE' ).
          <ls_reg_pir_ui>-relif = abap_true.
          EXIT.
        ENDLOOP.

      ENDIF.

    ENDIF.  " READ TABLE lt_reg_pir_ui[] INTO ls_reg_pir_ui


********************************************************************************

*INS Begin of IR5061015 JKH 09.09.2016
* Regional PIR defaulting logic needs to be changed due to National deleting old PIRs
* When a UOM code with GLN comes again in a new version with new hybris code, then
* default the editable fields only from obsolete records for same UOM code and GLN
* Also, Update the EINA/EINE flags

    LOOP AT lt_reg_pir_ui[] ASSIGNING <ls_reg_pir_ui>
      WHERE idno         EQ ls_prod_data-idno
        AND obsolete_ind IS INITIAL.

* Read obsolete data
      IF <ls_reg_pir_obs> IS ASSIGNED. UNASSIGN <ls_reg_pir_obs>. ENDIF.
      READ TABLE lt_reg_pir_ui[] ASSIGNING <ls_reg_pir_obs>
      WITH KEY idno           = <ls_reg_pir_ui>-idno
               order_uom_pim  = <ls_reg_pir_ui>-order_uom_pim
               gln_no         = <ls_reg_pir_ui>-gln_no
               obsolete_ind   = icon_dummy.
      IF sy-subrc = 0.
* default the editable fields only from obsolete records for same UOM code and GLN
        <ls_reg_pir_ui>-lifnr            = <ls_reg_pir_obs>-lifnr.
        <ls_reg_pir_ui>-vabme            = <ls_reg_pir_obs>-vabme.
        <ls_reg_pir_ui>-esokz            = <ls_reg_pir_obs>-esokz.
        <ls_reg_pir_ui>-ekgrp            = <ls_reg_pir_obs>-ekgrp.
        <ls_reg_pir_ui>-minbm            = <ls_reg_pir_obs>-minbm.
        <ls_reg_pir_ui>-norbm            = <ls_reg_pir_obs>-norbm.
        <ls_reg_pir_ui>-aplfz            = <ls_reg_pir_obs>-aplfz.
        <ls_reg_pir_ui>-ekkol            = <ls_reg_pir_obs>-ekkol.
        <ls_reg_pir_ui>-mwskz            = <ls_reg_pir_obs>-mwskz.
        <ls_reg_pir_ui>-loekz            = <ls_reg_pir_obs>-loekz.
        <ls_reg_pir_ui>-allow_zero_price = <ls_reg_pir_obs>-allow_zero_price.
        <ls_reg_pir_ui>-uebto            = <ls_reg_pir_obs>-uebto.
        <ls_reg_pir_ui>-untto            = <ls_reg_pir_obs>-untto.


* Check and set EINA flag for new PIRs
        IF <ls_reg_pir_obs>-pir_rel_eina = abap_true.
* Check EINA flag for new PIRs
          IF <ls_reg_pir_eina> IS ASSIGNED. UNASSIGN <ls_reg_pir_eina>. ENDIF.
          READ TABLE lt_reg_pir_ui[] ASSIGNING <ls_reg_pir_eina>
          WITH KEY idno           = <ls_reg_pir_ui>-idno
                   gln_no         = <ls_reg_pir_ui>-gln_no
                   obsolete_ind   = space
                   pir_rel_eina   = abap_true.
          IF sy-subrc = 0.

            IF <ls_reg_pir_eina>-bprme NE <ls_reg_pir_obs>-bprme.

* Set EINA flag for new PIRs
              IF <ls_reg_pir_new> IS ASSIGNED. UNASSIGN <ls_reg_pir_new>. ENDIF.
              READ TABLE lt_reg_pir_ui[] ASSIGNING <ls_reg_pir_new>
              WITH KEY idno           = <ls_reg_pir_ui>-idno
                       order_uom_pim  = <ls_reg_pir_ui>-order_uom_pim
                       gln_no         = <ls_reg_pir_ui>-gln_no
                       obsolete_ind   = space.
              IF sy-subrc = 0.
                CLEAR: <ls_reg_pir_eina>-pir_rel_eina, <ls_reg_pir_obs>-pir_rel_eina.
                <ls_reg_pir_new>-pir_rel_eina = abap_true.
              ENDIF.
            ENDIF.

          ELSE.

* Clear EINA flag for obsolete PIRs
            CLEAR <ls_reg_pir_obs>-pir_rel_eina.

            <ls_reg_pir_ui>-pir_rel_eina  = abap_true.

          ENDIF.
        ENDIF.  " IF <ls_reg_pir_obs>-pir_rel_eina = abap_true





* Check and set EINE flag for new PIRs
        IF <ls_reg_pir_obs>-pir_rel_eine = abap_true.
* Check EINE flag for new PIRs
          IF <ls_reg_pir_eine> IS ASSIGNED. UNASSIGN <ls_reg_pir_eine>. ENDIF.
          READ TABLE lt_reg_pir_ui[] ASSIGNING <ls_reg_pir_eine>
          WITH KEY idno           = <ls_reg_pir_ui>-idno
                   gln_no         = <ls_reg_pir_ui>-gln_no
                   obsolete_ind   = space
                   pir_rel_eine   = abap_true.
          IF sy-subrc = 0.

            IF <ls_reg_pir_eine>-bprme NE <ls_reg_pir_obs>-bprme.

* Set EINE flag for new PIRs
              IF <ls_reg_pir_new> IS ASSIGNED. UNASSIGN <ls_reg_pir_new>. ENDIF.
              READ TABLE lt_reg_pir_ui[] ASSIGNING <ls_reg_pir_new>
              WITH KEY idno           = <ls_reg_pir_ui>-idno
                       order_uom_pim  = <ls_reg_pir_ui>-order_uom_pim
                       gln_no         = <ls_reg_pir_ui>-gln_no
                       obsolete_ind   = space.
              IF sy-subrc = 0.
                CLEAR: <ls_reg_pir_eine>-pir_rel_eine, <ls_reg_pir_obs>-pir_rel_eine.
                <ls_reg_pir_new>-pir_rel_eine = abap_true.
              ENDIF.
            ENDIF.

          ELSE.

* Clear EINE flag for obsolete PIRs
            CLEAR <ls_reg_pir_obs>-pir_rel_eine.

            <ls_reg_pir_ui>-pir_rel_eine  = abap_true.
          ENDIF.
        ENDIF.  " IF <ls_reg_pir_obs>-pir_rel_EINE = abap_true



* Check and set Regular Vendor Flag
        IF <ls_reg_pir_obs>-relif = abap_true.

* Clear Regular Vendor Flag for obsolete PIRs
          CLEAR <ls_reg_pir_obs>-relif.

          <ls_reg_pir_ui>-relif  = abap_true.
        ENDIF.  " IF <ls_reg_pir_obs>-relif = abap_true

      ENDIF.  " READ TABLE lt_reg_pir_ui[] ASSIGNING <ls_reg_pir_obs>

    ENDLOOP.  "LOOP AT lt_reg_pir_ui[] ASSIGNING <ls_reg_pir_ui>


*INS En of IR5061015 JKH 09.09.2016

********************************************************************************

*** Sort PIR as Regular Vendor GLN should be on the top, if found

    SORT lt_reg_pir_ui[] BY gln_no       ASCENDING
                            pir_rel_eina DESCENDING
                            pir_rel_eine DESCENDING.


    lt_reg_pir_sort[] = lt_reg_pir_ui[].

    CLEAR ls_reg_pir_sort.
    READ TABLE lt_reg_pir_sort[] INTO ls_reg_pir_sort
    WITH KEY relif = abap_true.
    IF sy-subrc = 0.
      DELETE lt_reg_pir_sort[] WHERE gln_no NE ls_reg_pir_sort-gln_no.
      DELETE lt_reg_pir_ui[]   WHERE gln_no EQ ls_reg_pir_sort-gln_no.

      SORT lt_reg_pir_sort[] BY relif        DESCENDING
                                pir_rel_eina DESCENDING
                                pir_rel_eine DESCENDING.

      APPEND LINES OF lt_reg_pir_ui[] TO lt_reg_pir_sort[].
      lt_reg_pir_ui[] = lt_reg_pir_sort[].

    ENDIF.


* Move the Defaulted all PIRs to export parameters
    LOOP AT lt_reg_pir_ui[] INTO ls_reg_pir_ui.
      CLEAR ls_reg_pir.
      MOVE-CORRESPONDING ls_reg_pir_ui TO ls_reg_pir.
      APPEND ls_reg_pir TO lt_reg_pir[].
    ENDLOOP.

    et_reg_pir_ui[] = lt_reg_pir_ui[].
    es_reg_data_def-zarn_reg_pir[] = lt_reg_pir[].

  ENDMETHOD.                    "SET_REG_PIR_DEFAULT


    METHOD set_reg_pir_default_ui.

      DATA: ls_reg_data       TYPE zsarn_reg_data,
            ls_prod_data      TYPE zsarn_prod_data,

            lt_reg_pir_ui     TYPE ztarn_reg_pir_ui,
            ls_reg_pir_ui     TYPE zsarn_reg_pir_ui,
            lt_reg_pir_db     TYPE ztarn_reg_pir,
            ls_reg_pir_db     TYPE zarn_reg_pir,

            lt_reg_uom_ui     TYPE ztarn_reg_uom_ui,
            ls_reg_uom_ui     TYPE zsarn_reg_uom_ui,

            lt_reg_pir_gln    TYPE ztarn_reg_pir_ui,
            ls_reg_pir_gln    TYPE zsarn_reg_pir_ui,
            lt_reg_pir_tmp    TYPE ztarn_reg_pir_ui,
            ls_reg_pir_tmp    TYPE zsarn_reg_pir_ui,
            lt_reg_pir_sort   TYPE ztarn_reg_pir_ui,
            ls_reg_pir_sort   TYPE zsarn_reg_pir_ui,

            ls_uom_cat        TYPE zarn_uom_cat,
            ls_pir            TYPE zarn_pir,
            lt_list_price     TYPE ztarn_list_price,
            ls_list_price     TYPE zarn_list_price,
            lt_list_price_tmp TYPE ztarn_list_price,
            ls_list_price_tmp TYPE zarn_list_price,
            lt_pir_tmp        TYPE ztarn_pir,
            ls_pir_tmp        TYPE zarn_pir,
            lt_reg_uom_tmp    TYPE ztarn_reg_uom_ui,
            ls_reg_uom_tmp    TYPE zsarn_reg_uom_ui.


      FIELD-SYMBOLS:
        <ls_reg_pir_ui>     TYPE zsarn_reg_pir_ui.



*******************************************************************************


      ls_reg_data  = is_reg_data.
      ls_prod_data = is_prod_data.
      lt_reg_uom_ui[] = it_reg_uom_ui[].

      lt_reg_pir_db[] = ls_reg_data-zarn_reg_pir[].
      lt_list_price[] = ls_prod_data-zarn_list_price[].

* Retreive Initial Global Data, if not initialized
      IF gv_initialized IS INITIAL.
        CALL METHOD me->get_initial_data
          EXPORTING
            is_prod_data = ls_prod_data.
      ENDIF.

********************************************************************************

      LOOP AT lt_reg_pir_db[] INTO ls_reg_pir_db.

        CLEAR ls_reg_pir_ui.
        MOVE-CORRESPONDING ls_reg_pir_db TO ls_reg_pir_ui.

        ls_reg_pir_ui-bbbnr = ls_reg_pir_db-gln_no+0(7).
        ls_reg_pir_ui-bbsnr = ls_reg_pir_db-gln_no+7(5).
        ls_reg_pir_ui-bubkz = ls_reg_pir_db-gln_no+12(1).


        CLEAR ls_pir.
        READ TABLE ls_prod_data-zarn_pir[] INTO ls_pir
          WITH KEY idno                 = ls_reg_pir_db-idno
                   uom_code             = ls_reg_pir_db-order_uom_pim
                   hybris_internal_code = ls_reg_pir_db-hybris_internal_code.
        IF sy-subrc NE 0.
          ls_reg_pir_ui-obsolete_ind = icon_dummy.
        ENDIF.


        CLEAR ls_reg_uom_ui.
        READ TABLE lt_reg_uom_ui INTO ls_reg_uom_ui
        WITH KEY idno                 = ls_reg_pir_db-idno
                 pim_uom_code         = ls_reg_pir_db-order_uom_pim
                 hybris_internal_code = space
                 lower_uom            = ls_reg_pir_db-lower_uom
                 lower_child_uom      = ls_reg_pir_db-lower_child_uom.     "++INC5599313 JKH 03.02.2017
        IF sy-subrc = 0.
          ls_reg_pir_ui-bprme                = ls_reg_uom_ui-meinh.  "++INC5599313 JKH 03.02.2017
          ls_reg_pir_ui-meinh                = ls_reg_uom_ui-meinh.
          ls_reg_pir_ui-lower_meinh          = ls_reg_uom_ui-lower_meinh.
          ls_reg_pir_ui-num_base_units       = ls_reg_uom_ui-num_base_units.
          ls_reg_pir_ui-factor_of_base_units = ls_reg_uom_ui-factor_of_base_units.
          ls_reg_pir_ui-unit_purord          = ls_reg_uom_ui-unit_purord.
          ls_reg_pir_ui-cat_seqno            = ls_reg_uom_ui-cat_seqno.
        ENDIF.

        CLEAR ls_pir.
        READ TABLE ls_prod_data-zarn_pir[] INTO ls_pir
          WITH KEY idno                 = ls_reg_pir_db-idno
                   uom_code             = ls_reg_pir_db-order_uom_pim
                   hybris_internal_code = ls_reg_pir_db-hybris_internal_code.
        IF sy-subrc = 0.
          ls_reg_pir_ui-pir_last_changed_date = ls_pir-last_changed_date.
        ENDIF.


        CLEAR ls_list_price.
        READ TABLE ls_prod_data-zarn_list_price[] INTO ls_list_price
             WITH KEY idno      = ls_reg_pir_db-idno
                      uom_code  = ls_reg_pir_db-order_uom_pim
                      gln       = ls_reg_pir_db-gln_no.
        IF sy-subrc = 0.
          ls_reg_pir_ui-lp_last_changed_date = ls_list_price-last_changed_date.
        ENDIF.



        APPEND ls_reg_pir_ui TO lt_reg_pir_ui[].

      ENDLOOP.


********************************************************************************

* Nationally proposed EINA/EINE flag has to be set for each GLN, hence get the list of distinct GLNs
      lt_reg_pir_gln[] = lt_reg_pir_ui[].

      SORT lt_reg_pir_gln[] BY gln_no.
      DELETE ADJACENT DUPLICATES FROM lt_reg_pir_gln[] COMPARING gln_no.

* For each GLN, set EINA/EINE flag
      LOOP AT lt_reg_pir_gln[] INTO ls_reg_pir_gln.

*** Update PIR EINA flag
        lt_pir_tmp[] = ls_prod_data-zarn_pir[].
        DELETE lt_pir_tmp[] WHERE gln NE ls_reg_pir_gln-gln_no.

* Get the highest effective date
        SORT lt_pir_tmp[] BY effective_date DESCENDING.
        CLEAR ls_pir_tmp.
        READ TABLE lt_pir_tmp[] INTO ls_pir_tmp INDEX 1.
        IF sy-subrc = 0.
* Keep only records for highest effective date, ignore rest
          DELETE lt_pir_tmp[] WHERE effective_date NE ls_pir_tmp-effective_date.
        ENDIF.


        CLEAR: lt_reg_uom_tmp[].
        LOOP AT lt_pir_tmp[] INTO ls_pir_tmp.

          lt_reg_pir_tmp[] = lt_reg_pir_ui[].

* Get lower and lower child uom for each PIR for highest effective date
          CLEAR ls_reg_pir_tmp.
          READ TABLE lt_reg_pir_tmp[] INTO ls_reg_pir_tmp
          WITH KEY idno                 = ls_pir_tmp-idno
                   order_uom_pim        = ls_pir_tmp-uom_code
                   hybris_internal_code = ls_pir_tmp-hybris_internal_code
                   gln_no               = ls_reg_pir_gln-gln_no.
          IF sy-subrc = 0.

* Get All the UOMs for lower and lower child uom for each PIR for highest effective date
            LOOP AT lt_reg_uom_ui[] INTO ls_reg_uom_ui
              WHERE idno                 = ls_pir_tmp-idno
                AND pim_uom_code         = ls_reg_pir_tmp-order_uom_pim
                AND hybris_internal_code = space
                AND lower_uom            = ls_reg_pir_tmp-lower_uom
                AND lower_child_uom      = ls_reg_pir_tmp-lower_child_uom.

              CLEAR ls_reg_uom_tmp.
              ls_reg_uom_tmp = ls_reg_uom_ui.
              APPEND ls_reg_uom_tmp TO lt_reg_uom_tmp[].
            ENDLOOP.

          ENDIF.
        ENDLOOP.

* Get the highest UOM Category and UOM on the top
        SORT lt_reg_uom_tmp[] BY cat_seqno DESCENDING seqno DESCENDING.

* Get the topmost UOM
        CLEAR ls_reg_uom_tmp.
        READ TABLE lt_reg_uom_tmp[] INTO ls_reg_uom_tmp INDEX 1.
        IF sy-subrc = 0.

* Default EINA Flag for topmost UOM
          READ TABLE lt_reg_pir_ui[] ASSIGNING <ls_reg_pir_ui>
          WITH KEY idno            = ls_reg_pir_gln-idno
                   order_uom_pim   = ls_reg_uom_tmp-pim_uom_code
                   lower_uom       = ls_reg_uom_tmp-lower_uom
                   lower_child_uom = ls_reg_uom_tmp-lower_child_uom
                   gln_no          = ls_reg_pir_gln-gln_no.
          IF sy-subrc = 0.
            <ls_reg_pir_ui>-nat_pir_rel_eina = abap_true.
          ENDIF.


        ENDIF.  " READ TABLE lt_reg_uom_tmp[] INTO ls_reg_uom_tmp INDEX 1


*** Update PIR EINE flag
        lt_list_price_tmp[] = lt_list_price[].
        DELETE lt_list_price_tmp[] WHERE gln NE ls_reg_pir_gln-gln_no.

* Get the highest last changed date
        SORT lt_list_price_tmp[] BY last_changed_date DESCENDING.
        CLEAR ls_list_price_tmp.
        READ TABLE lt_list_price_tmp[] INTO ls_list_price_tmp INDEX 1.
        IF sy-subrc = 0.
* Keep only records for highest last changed date, ignore rest
          DELETE lt_list_price_tmp[] WHERE last_changed_date NE ls_list_price_tmp-last_changed_date.
        ENDIF.




        CLEAR: lt_reg_uom_tmp[].
        LOOP AT lt_list_price_tmp[] INTO ls_list_price_tmp.

          lt_reg_pir_tmp[] = lt_reg_pir_ui[].

* Get lower and lower child uom for each PIR for highest last changed date
          CLEAR ls_reg_pir_tmp.
          READ TABLE lt_reg_pir_tmp[] INTO ls_reg_pir_tmp
          WITH KEY idno                 = ls_list_price_tmp-idno
                   order_uom_pim        = ls_list_price_tmp-uom_code
                   gln_no               = ls_reg_pir_gln-gln_no.
          IF sy-subrc = 0.

* Get All the UOMs for lower and lower child uom for each PIR for highest last changed date
            LOOP AT lt_reg_uom_ui[] INTO ls_reg_uom_ui
              WHERE idno                 = ls_list_price_tmp-idno
                AND pim_uom_code         = ls_reg_pir_tmp-order_uom_pim
                AND hybris_internal_code = space
                AND lower_uom            = ls_reg_pir_tmp-lower_uom
                AND lower_child_uom      = ls_reg_pir_tmp-lower_child_uom.

              CLEAR ls_reg_uom_tmp.
              ls_reg_uom_tmp = ls_reg_uom_ui.
              APPEND ls_reg_uom_tmp TO lt_reg_uom_tmp[].
            ENDLOOP.
          ENDIF.
        ENDLOOP.

* Get the highest UOM Category and UOM on the top
        SORT lt_reg_uom_tmp[] BY cat_seqno DESCENDING seqno DESCENDING.

* Get the topmost UOM
        CLEAR ls_reg_uom_tmp.
        READ TABLE lt_reg_uom_tmp[] INTO ls_reg_uom_tmp INDEX 1.
        IF sy-subrc = 0.

* Default EINE Flag for topmost UOM
          READ TABLE lt_reg_pir_ui[] ASSIGNING <ls_reg_pir_ui>
          WITH KEY idno            = ls_reg_pir_gln-idno
                   order_uom_pim   = ls_reg_uom_tmp-pim_uom_code
                   lower_uom       = ls_reg_uom_tmp-lower_uom
                   lower_child_uom = ls_reg_uom_tmp-lower_child_uom
                   gln_no          = ls_reg_pir_gln-gln_no.
          IF sy-subrc = 0.
            <ls_reg_pir_ui>-nat_pir_rel_eine = abap_true.
          ENDIF.

        ENDIF.  " READ TABLE lt_reg_uom_tmp[] INTO ls_reg_uom_tmp INDEX 1

      ENDLOOP.  " LOOP AT lt_pir_gln[] INTO ls_reg_pir_gln


********************************************************************************

*** Sort PIR as Regular Vendor GLN should be on the top, if found
      "SAPTR-147 Post Tech Refresh Sort Issues
*      SORT lt_reg_pir_ui[] BY gln_no       ASCENDING
*                              pir_rel_eina DESCENDING
*                              pir_rel_eine DESCENDING.
      SORT lt_reg_pir_ui[] BY gln_no cat_seqno.
      "END OF SAPTR-147
      lt_reg_pir_sort[] = lt_reg_pir_ui[].

      CLEAR ls_reg_pir_sort.
      READ TABLE lt_reg_pir_sort[] INTO ls_reg_pir_sort
      WITH KEY relif = abap_true.
      IF sy-subrc = 0.
        DELETE lt_reg_pir_sort[] WHERE gln_no NE ls_reg_pir_sort-gln_no.
        DELETE lt_reg_pir_ui[]   WHERE gln_no EQ ls_reg_pir_sort-gln_no.

        SORT lt_reg_pir_sort[] BY relif        DESCENDING
                                  pir_rel_eina DESCENDING
                                  pir_rel_eine DESCENDING.

        APPEND LINES OF lt_reg_pir_ui[] TO lt_reg_pir_sort[].
        lt_reg_pir_ui[] = lt_reg_pir_sort[].

      ENDIF.



      et_reg_pir_ui[] = lt_reg_pir_ui[].

    ENDMETHOD.                    "SET_REG_PIR_DEFAULT


  METHOD SET_REG_SC_DEFAULT_UI.

     FIELD-SYMBOLS:     <ls_zarn_reg_uom_ui> TYPE zsarn_reg_uom_ui.

* This is a UI modification only. Supply chain tab uses the existing UoM basic article
* data but with different fields editable for the supply chain team
* There are a few different fields added to simplify for the team
     LOOP AT ct_reg_uom_ui ASSIGNING <ls_zarn_reg_uom_ui>.
        <ls_zarn_reg_uom_ui>-weight_uom    = <ls_zarn_reg_uom_ui>-net_weight_uom.
        "One Dimension UoM field created - take from height, depth or width
        <ls_zarn_reg_uom_ui>-dimension_uom = <ls_zarn_reg_uom_ui>-height_uom.
        IF <ls_zarn_reg_uom_ui>-dimension_uom IS INITIAL.
          <ls_zarn_reg_uom_ui>-dimension_uom = <ls_zarn_reg_uom_ui>-depth_uom.
        ENDIF.
        IF <ls_zarn_reg_uom_ui>-dimension_uom IS INITIAL.
          <ls_zarn_reg_uom_ui>-dimension_uom = <ls_zarn_reg_uom_ui>-width_uom.
        ENDIF.
        READ TABLE it_reg_ean_ui    INTO DATA(ls_zarn_reg_ean)
                         WITH KEY idno  = <ls_zarn_reg_uom_ui>-idno
                                  meinh = <ls_zarn_reg_uom_ui>-meinh
                                  hpean = abap_true.
        IF sy-subrc = 0.
          <ls_zarn_reg_uom_ui>-ean11 = ls_zarn_reg_ean-ean11.
          <ls_zarn_reg_uom_ui>-eantp = ls_zarn_reg_ean-eantp.
        ENDIF.
      ENDLOOP.

  ENDMETHOD.                    "SET_REG_UOM_DEFAULT


METHOD set_reg_string_default.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 19.07.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1359: ZARN_GUI changes (GS1  Updates)
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Default Regional String Data
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  LOOP AT is_prod_data-zarn_prod_str INTO DATA(ls_prod_str).

    READ TABLE ct_reg_str ASSIGNING FIELD-SYMBOL(<ls_reg_str>)
                          WITH KEY idno = ls_prod_str-idno.
    IF sy-subrc <> 0.
      APPEND INITIAL LINE TO ct_reg_str ASSIGNING <ls_reg_str>.
      <ls_reg_str>-idno = ls_prod_str-idno.
    ENDIF.
    <ls_reg_str>-reg_ingredient_statement = ls_prod_str-ingredient_statement.

    DATA(lv_ingredient_stmt) = <ls_reg_str>-ingredient_statement.
    DATA(lv_reg_ingredient_stmt) = <ls_reg_str>-reg_ingredient_statement.
    REPLACE ALL OCCURRENCES OF cl_abap_char_utilities=>cr_lf IN lv_ingredient_stmt WITH ''.
    REPLACE ALL OCCURRENCES OF cl_abap_char_utilities=>cr_lf IN lv_reg_ingredient_stmt WITH ''.

    IF lv_ingredient_stmt = lv_reg_ingredient_stmt.
      CLEAR: <ls_reg_str>-ingredient_statement.
    ENDIF.

  ENDLOOP.

ENDMETHOD.


  METHOD set_reg_uom_default.
*-----------------------------------------------------------------------
* DATE             # 23/01/2020
* CHANGE No.       # SUP-502 AReNa LAY-PAL issue
* DESCRIPTION      # When the base unit has decimals the conversion factors
*                  # are not as per standard SAP calculation in article
*                  # master i.e. MARM data. Added logic to determine the
*                  # conversion factors as per SAP code - restricted to
*                  # only when base has factor greater than 1.
* WHO              # C90001929, Aruna Mackenjee
*-----------------------------------------------------------------------
* DATE             # 05.03.2020
* CHANGE No.       # CIP-1102 - Ability to manually maintain Regonal
* DESCRIPTION      # Stop defauting some of the fields when article has
*                  # already been created as these fields are manually
*                  # editable from now. Code cleanup done
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------


    DATA: ls_reg_data           TYPE zsarn_reg_data,
          ls_prod_data          TYPE zsarn_prod_data,
          lt_gui_def            TYPE tt_gui_def,


          lt_reg_uom_ui         TYPE ztarn_reg_uom_ui,
          ls_reg_uom_ui         TYPE zsarn_reg_uom_ui,
          ls_reg_uom_ui_tmp     TYPE zsarn_reg_uom_ui,
          lt_reg_uom            TYPE ztarn_reg_uom,
          ls_reg_uom            TYPE zarn_reg_uom,
          lt_reg_uom_db         TYPE ztarn_reg_uom,
          ls_reg_uom_db         TYPE zarn_reg_uom,
          lt_reg_uom_tmp        TYPE ztarn_reg_uom_ui,
          ls_reg_uom_tmp        TYPE zsarn_reg_uom_ui,
          lt_reg_uom_exist      TYPE ztarn_reg_uom_ui,
          ls_reg_uom_exist      TYPE zsarn_reg_uom_ui,
          ls_reg_uom_exist_base TYPE zsarn_reg_uom_ui,
          ls_reg_uom_exist_inv  TYPE zsarn_reg_uom_ui,
          ls_reg_uom_exist_sale TYPE zsarn_reg_uom_ui,
          ls_reg_uom_exist_issu TYPE zsarn_reg_uom_ui,
          ls_reg_uom_lower      TYPE zsarn_reg_uom_ui,

          lt_added_pir          TYPE ty_t_added_pir,
          ls_added_pir          TYPE ty_s_added_pir,
          lt_uom_var_lower      TYPE ty_t_uom_var_lower,
          ls_uom_var_lower      TYPE ty_s_uom_var_lower,
          lt_uom_var_lower_tmp  TYPE ty_t_uom_var_lower,
          ls_uom_var_lower_tmp  TYPE ty_s_uom_var_lower,

          lt_uom_variant        TYPE ztarn_uom_variant,
          ls_uom_variant        TYPE zarn_uom_variant,
          ls_uom_variant_calc   TYPE zarn_uom_variant,

          lt_pir                TYPE ztarn_pir,
          ls_pir                TYPE zarn_pir,
          ls_pir_calc           TYPE zarn_pir,
          ls_gtin_var           TYPE zarn_gtin_var,
          ls_reg_pir            TYPE zarn_reg_pir,
          ls_uom_variant_child  TYPE zarn_uom_variant,
          ls_gtin_var_child     TYPE zarn_gtin_var,
          ls_uom_cat            TYPE zarn_uom_cat,
          ls_uomcategory        TYPE zarn_uomcategory,
          ls_gen_uom_t          TYPE ty_s_gen_uom_t,

          lt_uomcat_range       TYPE RANGE OF zarn_uom_category,
          ls_uomcat_range       LIKE LINE OF lt_uomcat_range,
          lt_marm_idno          TYPE ty_t_marm_idno,

          lt_marm_idno_tmp      TYPE ty_t_marm_idno,
          ls_marm_idno_tmp      TYPE ty_s_marm_idno,

          lv_seqno              TYPE zarn_seqno,
          lv_count              TYPE i,
          lv_tabix              TYPE sy-tabix,
          lv_max_relation       TYPE i,

          lv_base_count         TYPE i,
          lv_inv_count          TYPE i,
          lv_sale_count         TYPE i,
          lv_issu_count         TYPE i,
          lv_no_base            TYPE flag,
          lv_no_inv             TYPE flag,
          lv_no_sale            TYPE flag,
          lv_no_issu            TYPE flag,
          lv_uom_conv_err       TYPE flag,

          lv_lower_uom          TYPE zarn_lower_uom,
          lv_lower_child_uom    TYPE zarn_lower_child_uom,
          ls_prod_uom_t         TYPE ty_s_prod_uom_t.

    FIELD-SYMBOLS:
      <ls_reg_uom>       TYPE zarn_reg_uom,
      <ls_reg_uom_exist> TYPE zsarn_reg_uom_ui,
      <ls_reg_uom_ui>    TYPE zsarn_reg_uom_ui.

    ls_reg_data  = is_reg_data.
    ls_prod_data = is_prod_data.
    DATA(ls_reg_hdr) = VALUE #( is_reg_data-zarn_reg_hdr[ 1 ] OPTIONAL ).

    CLEAR: lt_reg_uom[], lt_reg_uom_ui[], lt_reg_uom_exist[].

    lt_reg_uom_db[] = ls_reg_data-zarn_reg_uom[].

* Retreive Initial Global Data, if not initialized
    IF gv_initialized IS INITIAL.
      CALL METHOD me->get_initial_data
        EXPORTING
          is_prod_data = ls_prod_data.
    ENDIF.

    INSERT LINES OF gt_gui_def_recon[] INTO TABLE lt_gui_def[].
    DELETE lt_gui_def[] WHERE tabname  NE 'ZARN_REG_UOM'.

    lt_marm_idno[] = gt_marm_idno[].
    DELETE lt_marm_idno[] WHERE idno NE is_prod_data-idno.

* Default Only if asked, else return whatever in REG_UOM Table
    IF iv_reg_only IS INITIAL.
      me->set_reg_uom_default_int( EXPORTING is_reg_data            = is_reg_data
                                             is_prod_data           = ls_prod_data
                                             it_reg_uom_db          = lt_reg_uom_db
                                             it_gui_def             = lt_gui_def
                                   IMPORTING et_reg_uom_exist       = lt_reg_uom_exist
                                             et_reg_uom_ui          = lt_reg_uom_ui
                                             ev_dim_overflow_error  = ev_dim_overflow_error ).
    ENDIF.

*******************************************************************************

    " UOMs which are in DB but missing in national while proposing
    " must be considered to default
    LOOP AT lt_reg_uom_db[] INTO ls_reg_uom_db.

      CLEAR ls_reg_uom_exist.
      READ TABLE lt_reg_uom_exist[] INTO ls_reg_uom_exist
           WITH KEY idno                 = ls_reg_uom_db-idno
                    uom_category         = ls_reg_uom_db-uom_category
                    pim_uom_code         = ls_reg_uom_db-pim_uom_code
                    hybris_internal_code = ls_reg_uom_db-hybris_internal_code
                    lower_uom            = ls_reg_uom_db-lower_uom
                    lower_child_uom      = ls_reg_uom_db-lower_child_uom.
      IF sy-subrc NE 0.

        CLEAR ls_reg_uom_exist.
        MOVE-CORRESPONDING ls_reg_uom_db TO ls_reg_uom_exist.


* Get lower uom and lower child uom codes from National
        CLEAR: lv_lower_uom, lv_lower_child_uom.
        CALL FUNCTION 'ZARN_GET_NAT_LOWER_CHILD_UOM'
          EXPORTING
            iv_idno            = ls_prod_data-idno
            iv_version         = ls_prod_data-version
            iv_uom_category    = 'RETAIL'
            iv_uom_code        = ls_reg_uom_db-pim_uom_code
            iv_hybris_code     = space
            it_uom_variant     = ls_prod_data-zarn_uom_variant[]
            it_gtin_var        = ls_prod_data-zarn_gtin_var[]
          IMPORTING
            ev_lower_uom       = lv_lower_uom
            ev_lower_child_uom = lv_lower_child_uom
          EXCEPTIONS
            input_reqd         = 1
            OTHERS             = 2.

        CLEAR ls_uom_variant.
        IF lv_lower_uom       = ls_reg_uom_db-lower_uom       AND
           lv_lower_child_uom = ls_reg_uom_db-lower_child_uom.
          READ TABLE ls_prod_data-zarn_uom_variant[] INTO ls_uom_variant
          WITH KEY idno     = ls_reg_uom_db-idno
                   uom_code = ls_reg_uom_db-pim_uom_code.
        ENDIF.

        CLEAR ls_pir.
        READ TABLE ls_prod_data-zarn_pir[] INTO ls_pir
        WITH KEY idno                 = ls_reg_uom_db-idno
                 uom_code             = ls_reg_uom_db-pim_uom_code
                 hybris_internal_code = ls_reg_uom_db-hybris_internal_code.

* Set Obsolete Indicator
        IF iv_reg_only IS INITIAL.
          ls_reg_uom_exist-obsolete_ind = icon_dummy.
        ELSE.

          IF ls_reg_uom_db-uom_category = gc_layer
          OR ls_reg_uom_db-uom_category = gc_pallet.

            IF ls_pir IS INITIAL.
              ls_reg_uom_exist-obsolete_ind = icon_dummy.
            ENDIF.

          ELSEIF ls_reg_uom_db-uom_category =  gc_retail
              OR ls_reg_uom_db-uom_category =  gc_inner
              OR ls_reg_uom_db-uom_category =  gc_shipper.

            IF ls_uom_variant IS INITIAL.
              ls_reg_uom_exist-obsolete_ind = icon_dummy.
            ENDIF.

          ELSE.

            ls_reg_uom_exist-obsolete_ind = icon_dummy.

          ENDIF.  " IF ls_reg_uom_db-uom_category = gc_layer/gc_pallet
        ENDIF.  " IF iv_reg_only IS INITIAL


* Get UOM Category Sequence
        CLEAR ls_uomcategory.
        READ TABLE gt_uomcategory[] INTO ls_uomcategory
        WITH TABLE KEY uom_category = ls_reg_uom_db-uom_category.
        IF sy-subrc = 0.
          ls_reg_uom_exist-cat_seqno = ls_uomcategory-cat_seqno.
        ENDIF.

* Get UOM Category Sequence
        CLEAR ls_uom_cat.
        READ TABLE gt_uom_cat[] INTO ls_uom_cat
        WITH KEY uom_category = ls_reg_uom_db-uom_category
                 uom          = ls_reg_uom_db-meinh.
        IF sy-subrc = 0.
          ls_reg_uom_exist-seqno = ls_uom_cat-seqno.
        ENDIF.

        APPEND ls_reg_uom_exist TO lt_reg_uom_exist[].

      ENDIF.
    ENDLOOP.


*******************************************************************************


* Default Only if asked, else return whatever in REG_UOM Table
    IF iv_reg_only = abap_true.

      SORT lt_reg_uom_exist[] BY cat_seqno seqno.

      et_reg_uom_ui[] = lt_reg_uom_exist[].
      es_reg_data_def-zarn_reg_uom[] = ls_reg_data-zarn_reg_uom[].

      EXIT.
    ENDIF.

*******************************************************************************

* Merge the list of New UOMs, DB UOMs and existing UOMs
    APPEND LINES OF lt_reg_uom_ui[] TO lt_reg_uom_exist[].


* Sorting Sequence is required as lower child uom is defaulted assuming lower uom is already defaulted
    SORT lt_reg_uom_ui[]    BY cat_seqno seqno ASCENDING.
    SORT lt_reg_uom_exist[] BY cat_seqno seqno ASCENDING.


*******************************************************************************

    READ TABLE lt_gui_def TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname  = 'ZARN_REG_UOM'
                   fldname  = 'RELATIONSHIP'
                   logic_ty = gc_logic_l.
    IF sy-subrc = 0.
* For all the UOM which are new and are defaulted, get a counter to group them together
* this is next incremented counter from Reg UOM data
      CLEAR lv_max_relation.
      lt_reg_uom[] = ls_reg_data-zarn_reg_uom[].

      SORT lt_reg_uom[] BY relationship DESCENDING.
      CLEAR ls_reg_uom.
      READ TABLE lt_reg_uom[] INTO ls_reg_uom INDEX 1.
      IF sy-subrc = 0.
        lv_max_relation = ls_reg_uom-relationship + 1.
      ENDIF.
    ENDIF.


*******************************************************************************

* Default UOMs only for new UOMs
    LOOP AT lt_reg_uom_ui[] ASSIGNING <ls_reg_uom_ui>.

      <ls_reg_uom_ui>-ersda = sy-datum.
      <ls_reg_uom_ui>-erzet = sy-uzeit.


* Default MEINH and LOWER_MEINH
      CASE <ls_reg_uom_ui>-uom_category.
        WHEN gc_retail.                                                        " RETAIL
          READ TABLE lt_gui_def TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname  = 'ZARN_REG_UOM'
                         fldname  = 'MEINH'
                         logic_ty = gc_logic_l.
          IF sy-subrc = 0.
            CLEAR ls_uom_variant.
            READ TABLE ls_prod_data-zarn_uom_variant[] INTO ls_uom_variant
               WITH KEY uom_code = <ls_reg_uom_ui>-pim_uom_code.
            IF sy-subrc = 0.
              <ls_reg_uom_ui>-meinh = ls_uom_variant-product_uom.
            ENDIF.
          ENDIF. " MEINH



        WHEN gc_inner OR gc_shipper.
          READ TABLE lt_gui_def TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname  = 'ZARN_REG_UOM'
                         fldname  = 'LOWER_MEINH'
                         logic_ty = gc_logic_l.
          IF sy-subrc = 0.
* For LOWER_MEINH, get the MEINH which is already defaulted for current lower child uom
            CLEAR ls_reg_uom_lower.
            READ TABLE lt_reg_uom_exist[] INTO ls_reg_uom_lower
            WITH KEY pim_uom_code = <ls_reg_uom_ui>-lower_uom
                     lower_uom    = <ls_reg_uom_ui>-lower_child_uom.
            IF sy-subrc = 0.
              <ls_reg_uom_ui>-lower_meinh = ls_reg_uom_lower-meinh.
            ENDIF.
          ENDIF.  " LOWER_MEINH


          " INNER/SHIPPER
          READ TABLE lt_gui_def TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname  = 'ZARN_REG_UOM'
                         fldname  = 'MEINH'
                         logic_ty = gc_logic_l.
          IF sy-subrc = 0.

            lt_marm_idno_tmp[] = lt_marm_idno[].

            DELETE lt_marm_idno_tmp[] WHERE uom_category NE <ls_reg_uom_ui>-uom_category.

* Get the highest UOM above so that If multiple UOM exist for the same conversion,
* then the highest is defaulted
            SORT lt_marm_idno_tmp[] BY seqno DESCENDING.

* For MEINH, check if UOM already exist (either in DB or in new UOM),
* then get the next higher UOM
            CLEAR: lt_reg_uom_tmp[].
            lt_reg_uom_tmp[] = lt_reg_uom_exist[].

            DELETE lt_reg_uom_tmp[] WHERE uom_category NE <ls_reg_uom_ui>-uom_category.
            DELETE lt_reg_uom_tmp[] WHERE meinh IS INITIAL.

* Get highest existing UOM
            SORT lt_reg_uom_tmp[] BY seqno DESCENDING.
            CLEAR ls_reg_uom_tmp.
            READ TABLE lt_reg_uom_tmp[] INTO ls_reg_uom_tmp INDEX 1.

* Get highest existing UOM from MARM
            CLEAR ls_marm_idno_tmp.
            READ TABLE lt_marm_idno_tmp[] INTO ls_marm_idno_tmp INDEX 1.

* If highest UOM is same in defaulted UOM and MARM then get the next higher UOM
* else, check the highest UOM in defaulted UOM and MARM and get the next higher UOM

            CLEAR lv_seqno.
            IF ls_reg_uom_tmp-seqno = ls_marm_idno_tmp-seqno.
              lv_seqno = ls_reg_uom_tmp-seqno + 1.
            ELSEIF ls_reg_uom_tmp-seqno GT ls_marm_idno_tmp-seqno.
              lv_seqno = ls_reg_uom_tmp-seqno + 1.
            ELSEIF ls_reg_uom_tmp-seqno LT ls_marm_idno_tmp-seqno.
              lv_seqno = ls_marm_idno_tmp-seqno + 1.
            ENDIF.

            IF lv_seqno GT 0.
* Get next higher UOM to be defaulted
              CLEAR ls_uom_cat.
              READ TABLE gt_uom_cat[] INTO ls_uom_cat
              WITH KEY uom_category = <ls_reg_uom_ui>-uom_category
                       seqno = lv_seqno.
              IF sy-subrc = 0.
                <ls_reg_uom_ui>-meinh = ls_uom_cat-uom.
              ELSE.
                ev_no_uom_avail = abap_true.
              ENDIF.
            ENDIF.
          ENDIF. " MEINH

        WHEN gc_layer OR gc_pallet.                                            " LAYER/PALLET
          READ TABLE lt_gui_def TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname  = 'ZARN_REG_UOM'
                         fldname  = 'LOWER_MEINH'
                         logic_ty = gc_logic_l.
          IF sy-subrc = 0.
            IF <ls_reg_uom_ui>-uom_category = gc_pallet.
* For LOWER_MEINH for PALLET, get the MEINH which is already defaulted for LAYER
              CLEAR ls_reg_uom_lower.
              READ TABLE lt_reg_uom_exist[] INTO ls_reg_uom_lower
              WITH KEY uom_category         = gc_layer
                       pim_uom_code         = <ls_reg_uom_ui>-pim_uom_code
                       hybris_internal_code = <ls_reg_uom_ui>-hybris_internal_code
                       lower_uom            = <ls_reg_uom_ui>-lower_uom
                       lower_child_uom      = <ls_reg_uom_ui>-lower_child_uom.
              IF sy-subrc = 0.
                <ls_reg_uom_ui>-lower_meinh = ls_reg_uom_lower-meinh.
              ENDIF.
            ELSE.
* For LOWER_MEINH, get the MEINH which is already defaulted for current lower child uom
              CLEAR ls_reg_uom_lower.
              READ TABLE lt_reg_uom_exist[] INTO ls_reg_uom_lower
              WITH KEY pim_uom_code = <ls_reg_uom_ui>-lower_uom
                       lower_uom    = <ls_reg_uom_ui>-lower_child_uom.
              IF sy-subrc = 0.
                <ls_reg_uom_ui>-lower_meinh = ls_reg_uom_lower-meinh.
              ENDIF.
            ENDIF.
          ENDIF.  " LOWER_MEINH


          READ TABLE lt_gui_def TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname  = 'ZARN_REG_UOM'
                         fldname  = 'MEINH'
                         logic_ty = gc_logic_l.
          IF sy-subrc = 0.

            lt_marm_idno_tmp[] = lt_marm_idno[].

            DELETE lt_marm_idno_tmp[] WHERE uom_category NE <ls_reg_uom_ui>-uom_category.

* Get the highest UOM at last so that If multiple UOM exist for the same conversion,
* then the highest is defaulted
            SORT lt_marm_idno_tmp[] BY seqno.

* Check in MARM if UOM exist for the same conversion, get count
            CLEAR lv_count.
            CLEAR ls_marm_idno_tmp.
            LOOP AT lt_marm_idno_tmp INTO ls_marm_idno_tmp
             WHERE idno  = <ls_reg_uom_ui>-idno
               AND umrez = <ls_reg_uom_ui>-num_base_units
               AND umren = <ls_reg_uom_ui>-factor_of_base_units
               AND mesub = <ls_reg_uom_ui>-lower_meinh.
              lv_count = lv_count + 1.
            ENDLOOP.

            IF lv_count GE 1.
              <ls_reg_uom_ui>-meinh = ls_marm_idno_tmp-meinh.
            ELSE.

* For MEINH, check if UOM already exist (either in DB or in new UOM),
* then get the next higher UOM
              CLEAR: lt_reg_uom_tmp[].
              lt_reg_uom_tmp[] = lt_reg_uom_exist[].

              DELETE lt_reg_uom_tmp[] WHERE uom_category NE <ls_reg_uom_ui>-uom_category.
              DELETE lt_reg_uom_tmp[] WHERE meinh IS INITIAL.

* Get highest existing UOM
              SORT lt_reg_uom_tmp[] BY seqno DESCENDING.
              CLEAR ls_reg_uom_tmp.
              READ TABLE lt_reg_uom_tmp[] INTO ls_reg_uom_tmp INDEX 1.

* get highest UOM on the top
              SORT lt_marm_idno_tmp[] BY seqno DESCENDING.

* Get highest existing UOM from MARM
              CLEAR ls_marm_idno_tmp.
              READ TABLE lt_marm_idno_tmp[] INTO ls_marm_idno_tmp INDEX 1.

* If highest UOM is same in defaulted UOM and MARM then get the next higher UOM
* else, check the highest UOM in defaulted UOM and MARM and get the next higher UOM

              CLEAR lv_seqno.
              IF ls_reg_uom_tmp-seqno = ls_marm_idno_tmp-seqno.
                lv_seqno = ls_reg_uom_tmp-seqno + 1.
              ELSEIF ls_reg_uom_tmp-seqno GT ls_marm_idno_tmp-seqno.
                lv_seqno = ls_reg_uom_tmp-seqno + 1.
              ELSEIF ls_reg_uom_tmp-seqno LT ls_marm_idno_tmp-seqno.
                lv_seqno = ls_marm_idno_tmp-seqno + 1.
              ENDIF.

              IF lv_seqno GT 0.
* Get next higher UOM to be defaulted
                CLEAR ls_uom_cat.
                READ TABLE gt_uom_cat[] INTO ls_uom_cat
                WITH KEY uom_category = <ls_reg_uom_ui>-uom_category
                         seqno = lv_seqno.
                IF sy-subrc = 0.
                  <ls_reg_uom_ui>-meinh = ls_uom_cat-uom.
                ELSE.
                  ev_no_uom_avail = abap_true.
                ENDIF.
              ENDIF.

            ENDIF.  " READ TABLE lt_marm_idno INTO ls_marm_idno
          ENDIF.  " MEINH

      ENDCASE.


      IF <ls_reg_uom_ui>-meinh  = 'KGM'.
        <ls_reg_uom_ui>-meinh  = 'KG'.
      ENDIF.

      IF <ls_reg_uom_ui>-lower_meinh  = 'KGM'.
        <ls_reg_uom_ui>-lower_meinh  = 'KG'.
      ENDIF.


      IF <ls_reg_uom_ui>-uom_category NE gc_retail.
        IF <ls_reg_uom_ui>-meinh IS INITIAL OR <ls_reg_uom_ui>-lower_meinh IS INITIAL.
          CLEAR : <ls_reg_uom_ui>-meinh, <ls_reg_uom_ui>-lower_meinh.
        ENDIF.
      ENDIF.


      READ TABLE lt_gui_def TRANSPORTING NO FIELDS
      WITH TABLE KEY tabname  = 'ZARN_REG_UOM'
                     fldname  = 'RELATIONSHIP'
                     logic_ty = gc_logic_l.
      IF sy-subrc = 0.
        <ls_reg_uom_ui>-relationship = lv_max_relation.
      ENDIF.


* Get UOM Category Sequence
      CLEAR ls_uom_cat.
      READ TABLE gt_uom_cat[] INTO ls_uom_cat
      WITH KEY uom_category = <ls_reg_uom_ui>-uom_category
               uom          = <ls_reg_uom_ui>-meinh.
      IF sy-subrc = 0.
        <ls_reg_uom_ui>-seqno = ls_uom_cat-seqno.
        <ls_reg_uom_ui>-cat_seqno = ls_uom_cat-cat_seqno.
      ENDIF.



* Update new UOMs values
      READ TABLE lt_reg_uom_exist[] ASSIGNING <ls_reg_uom_exist>
                 WITH KEY idno                 = <ls_reg_uom_ui>-idno
                          uom_category         = <ls_reg_uom_ui>-uom_category
                          pim_uom_code         = <ls_reg_uom_ui>-pim_uom_code
                          hybris_internal_code = <ls_reg_uom_ui>-hybris_internal_code
                          lower_uom            = <ls_reg_uom_ui>-lower_uom
                          lower_child_uom      = <ls_reg_uom_ui>-lower_child_uom.
      IF sy-subrc = 0.
        <ls_reg_uom_exist> = <ls_reg_uom_ui>.
      ENDIF.


    ENDLOOP.  " LOOP AT lt_reg_uom_ui[] ASSIGNING <ls_reg_uom_ui>

*******************************************************************************

* Get Lower and Lower Child UOM codes for UOM variant to identify default units
    CLEAR: lt_uom_var_lower[].

    LOOP AT ls_prod_data-zarn_uom_variant[] INTO ls_uom_variant.
      CLEAR ls_uom_var_lower.
      ls_uom_var_lower-idno                 = ls_uom_variant-idno.
      ls_uom_var_lower-version              = ls_uom_variant-version.
      ls_uom_var_lower-uom_code             = ls_uom_variant-uom_code.
      ls_uom_var_lower-hybris_internal_code = space.

      ls_uom_var_lower-base_unit            = ls_uom_variant-base_unit.
      ls_uom_var_lower-consumer_unit        = ls_uom_variant-consumer_unit.
      ls_uom_var_lower-despatch_unit        = ls_uom_variant-despatch_unit.
      ls_uom_var_lower-an_invoice_unit      = ls_uom_variant-an_invoice_unit.



* Get Lower UOM and Lower child UOM
      CLEAR ls_gtin_var_child.
      " ZGTIN_VAR-UOM_CODE where ZGTIN_VAR-GTIN_CODE = ZUOM_VARIANT-CHILD_GTIN
      READ TABLE ls_prod_data-zarn_gtin_var[] INTO ls_gtin_var_child
        WITH KEY idno      = ls_uom_variant-idno
                 version   = ls_uom_variant-version
                 gtin_code = ls_uom_variant-child_gtin.
      IF sy-subrc EQ 0 AND ls_gtin_var_child-uom_code IS NOT INITIAL.
        ls_uom_var_lower-lower_uom = ls_gtin_var_child-uom_code.

        " ZGTIN_VAR-UOM_CODE where ZGTIN_VAR-GTIN_CODE = ZUOM_VARIANT-CHILD_GTIN
        " & ZUOM_VARIANT-UOM_CODE= ZREG_UOM-LOWER_UOM
        CLEAR ls_uom_variant_child.
        READ TABLE ls_prod_data-zarn_uom_variant[] INTO ls_uom_variant_child
          WITH KEY idno     = ls_uom_variant-idno
                   version  = ls_uom_variant-version
                   uom_code = ls_gtin_var_child-uom_code.
        IF sy-subrc EQ 0 AND ls_uom_variant_child-child_gtin IS NOT INITIAL.
          CLEAR ls_gtin_var_child.
          READ TABLE ls_prod_data-zarn_gtin_var[] INTO ls_gtin_var_child
             WITH KEY idno      = ls_uom_variant_child-idno
                      version   = ls_uom_variant_child-version
                      gtin_code = ls_uom_variant_child-child_gtin.
          IF sy-subrc EQ 0.
            ls_uom_var_lower-lower_child_uom = ls_gtin_var_child-uom_code.
          ENDIF.
        ENDIF.
      ENDIF.

* Get SAP UOM for Product UOM
      CLEAR ls_prod_uom_t.
      READ TABLE gt_prod_uom_t[] INTO ls_prod_uom_t
      WITH TABLE KEY product_uom = ls_uom_variant-product_uom.
      IF sy-subrc = 0.
        ls_uom_var_lower-product_uom  = ls_prod_uom_t-sap_uom.
        ls_uom_var_lower-uom_category = ls_prod_uom_t-sap_uom_cat.
      ELSE.
        ls_uom_var_lower-product_uom = ls_uom_variant-product_uom.
      ENDIF.


* Get UOMs
      CLEAR ls_reg_uom_exist.
      READ TABLE lt_reg_uom_exist[] INTO ls_reg_uom_exist
                 WITH KEY idno                 = ls_uom_var_lower-idno
                          uom_category         = ls_uom_var_lower-uom_category
                          pim_uom_code         = ls_uom_var_lower-uom_code
                          hybris_internal_code = ls_uom_var_lower-hybris_internal_code
                          lower_uom            = ls_uom_var_lower-lower_uom
                          lower_child_uom      = ls_uom_var_lower-lower_child_uom.
      IF sy-subrc = 0.
        ls_uom_var_lower-meinh                = ls_reg_uom_exist-meinh.
        ls_uom_var_lower-lower_meinh          = ls_reg_uom_exist-lower_meinh.
        ls_uom_var_lower-uom_category         = ls_reg_uom_exist-uom_category.
        ls_uom_var_lower-cat_seqno            = ls_reg_uom_exist-cat_seqno.
        ls_uom_var_lower-seqno                = ls_reg_uom_exist-seqno.
      ENDIF.

      APPEND ls_uom_var_lower TO lt_uom_var_lower[].
    ENDLOOP.  " LOOP AT ls_prod_data-zarn_uom_variant[] INTO ls_uom_variant


    SORT lt_uom_var_lower[] BY cat_seqno seqno ASCENDING.


    READ TABLE lt_gui_def TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname  = 'ZARN_REG_UOM'
                   fldname  = 'UNIT_BASE'
                   logic_ty = gc_logic_l.
    IF sy-subrc = 0.
* Check if Base Unit has to be defaulted
* If yes then, get the count from national how many base units are there
* else, mark no_base flag as true -> base unit will not be defaulted as it is already there from DB
      CLEAR: ls_reg_uom_exist_base, ls_uom_var_lower, lv_base_count, lv_count, lv_no_base.
      READ TABLE lt_reg_uom_exist[] INTO ls_reg_uom_exist_base
      WITH KEY unit_base = abap_true.
      IF sy-subrc NE 0.
        LOOP AT lt_uom_var_lower[] INTO ls_uom_var_lower
           WHERE base_unit = abap_true.
          lv_count = lv_count + 1.
        ENDLOOP.

        lv_base_count = lv_count.
      ELSE.
        lv_no_base = abap_true.
      ENDIF.


* Default Base Unit, if required
      IF lv_no_base NE abap_true.

        lt_uom_var_lower_tmp[] = lt_uom_var_lower[].

        IF lv_base_count GE 1.
          DELETE lt_uom_var_lower_tmp[] WHERE base_unit NE abap_true.
        ENDIF.  " IF lv_base_count GE 1

* Get the lowest UOM Category and UOM on the top
        SORT lt_uom_var_lower_tmp[] BY cat_seqno seqno ASCENDING.

* Default the topmost
        CLEAR ls_uom_var_lower_tmp.
        READ TABLE lt_uom_var_lower_tmp[] INTO ls_uom_var_lower_tmp INDEX 1.
        IF sy-subrc = 0.
          READ TABLE lt_reg_uom_exist[] ASSIGNING <ls_reg_uom_exist>
                     WITH KEY idno                 = ls_uom_var_lower_tmp-idno
                              uom_category         = ls_uom_var_lower_tmp-uom_category
                              pim_uom_code         = ls_uom_var_lower_tmp-uom_code
                              hybris_internal_code = ls_uom_var_lower_tmp-hybris_internal_code
                              lower_uom            = ls_uom_var_lower_tmp-lower_uom
                              lower_child_uom      = ls_uom_var_lower_tmp-lower_child_uom.
          IF sy-subrc = 0.
            <ls_reg_uom_exist>-unit_base = abap_true.
          ENDIF.

        ENDIF.

      ENDIF.  " IF lv_no_base NE abap_true

    ENDIF.  " UNIT_BASE



    READ TABLE lt_gui_def TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname  = 'ZARN_REG_UOM'
                   fldname  = 'SALES_UNIT'
                   logic_ty = gc_logic_l.
    IF sy-subrc = 0.
* Check if Sale Unit has to be defaulted
* If yes then, get the count from national how many Sale units are there
* else, mark no_Sale flag as true -> Sale unit will not be defaulted as it is already there from DB
      CLEAR: ls_reg_uom_exist_sale, ls_uom_var_lower, lv_sale_count, lv_count, lv_no_sale.
      READ TABLE lt_reg_uom_exist[] INTO ls_reg_uom_exist_sale
      WITH KEY sales_unit = abap_true.
      IF sy-subrc NE 0.
        LOOP AT lt_uom_var_lower[] INTO ls_uom_var_lower
           WHERE consumer_unit = abap_true.
          lv_count = lv_count + 1.
        ENDLOOP.

        lv_sale_count = lv_count.
      ELSE.
        lv_no_sale = abap_true.
      ENDIF.

* Default Sale Unit, if required
      IF lv_no_sale NE abap_true.

        lt_uom_var_lower_tmp[] = lt_uom_var_lower[].

        IF lv_sale_count GE 1.
          DELETE lt_uom_var_lower_tmp[] WHERE consumer_unit NE abap_true.
        ENDIF.  " IF lv_sale_count GE 1. GE 1

* Get the lowest UOM Category and UOM on the top
        SORT lt_uom_var_lower_tmp[] BY cat_seqno seqno ASCENDING.

* Default the topmost
        CLEAR ls_uom_var_lower_tmp.
        READ TABLE lt_uom_var_lower_tmp[] INTO ls_uom_var_lower_tmp INDEX 1.
        IF sy-subrc = 0.
          READ TABLE lt_reg_uom_exist[] ASSIGNING <ls_reg_uom_exist>
                     WITH KEY idno                 = ls_uom_var_lower_tmp-idno
                              uom_category         = ls_uom_var_lower_tmp-uom_category
                              pim_uom_code         = ls_uom_var_lower_tmp-uom_code
                              hybris_internal_code = ls_uom_var_lower_tmp-hybris_internal_code
                              lower_uom            = ls_uom_var_lower_tmp-lower_uom
                              lower_child_uom      = ls_uom_var_lower_tmp-lower_child_uom.
          IF sy-subrc = 0.
            <ls_reg_uom_exist>-sales_unit = abap_true.
          ENDIF.

        ENDIF.

      ENDIF.  " IF lv_no_sale NE abap_true

    ENDIF.  " SALES_UNIT




    READ TABLE lt_gui_def TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname  = 'ZARN_REG_UOM'
                   fldname  = 'UNIT_PURORD'
                   logic_ty = gc_logic_l.
    IF sy-subrc = 0.
* Check if Invoice Unit has to be defaulted
* If yes then, get the count from national how many Invoice units are there
* else, mark no_Inv flag as true -> Invoice unit will not be defaulted as it is already there from DB
      CLEAR: ls_reg_uom_exist_inv, ls_uom_var_lower, lv_inv_count, lv_count, lv_no_inv.
      READ TABLE lt_reg_uom_exist[] INTO ls_reg_uom_exist_inv
      WITH KEY unit_purord = abap_true.
      IF sy-subrc NE 0.
        LOOP AT lt_uom_var_lower[] INTO ls_uom_var_lower
           WHERE an_invoice_unit = abap_true.
          lv_count = lv_count + 1.
        ENDLOOP.

        lv_inv_count = lv_count.
      ELSE.
        lv_no_inv = abap_true.
      ENDIF.


* Default Invoice Unit, if required
      IF lv_no_inv NE abap_true.

        lt_uom_var_lower_tmp[] = lt_uom_var_lower[].

        IF lv_inv_count GE 1.
          DELETE lt_uom_var_lower_tmp[] WHERE an_invoice_unit NE abap_true.
        ENDIF.  " IF lv_inv_count GE 1. GE 1

* Get the highest UOM Category and UOM on the top
        SORT lt_uom_var_lower_tmp[] BY cat_seqno DESCENDING seqno DESCENDING.

* Default the topmost
        CLEAR ls_uom_var_lower_tmp.
        READ TABLE lt_uom_var_lower_tmp[] INTO ls_uom_var_lower_tmp INDEX 1.
        IF sy-subrc = 0.
          READ TABLE lt_reg_uom_exist[] ASSIGNING <ls_reg_uom_exist>
                     WITH KEY idno                 = ls_uom_var_lower_tmp-idno
                              uom_category         = ls_uom_var_lower_tmp-uom_category
                              pim_uom_code         = ls_uom_var_lower_tmp-uom_code
                              hybris_internal_code = ls_uom_var_lower_tmp-hybris_internal_code
                              lower_uom            = ls_uom_var_lower_tmp-lower_uom
                              lower_child_uom      = ls_uom_var_lower_tmp-lower_child_uom.
          IF sy-subrc = 0.
            <ls_reg_uom_exist>-unit_purord = abap_true.
          ENDIF.

        ENDIF.

      ENDIF.  " IF lv_no_inv NE abap_true

    ENDIF.  " UNIT_PURORD





    READ TABLE lt_gui_def TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname  = 'ZARN_REG_UOM'
                   fldname  = 'ISSUE_UNIT'
                   logic_ty = gc_logic_l.
    IF sy-subrc = 0.
* Check if Issue Unit has to be defaulted
* If yes then, get the count from national how many Issue units are there
* else, mark no_Issu flag as true -> Issue unit will not be defaulted as it is already there from DB
      CLEAR: ls_reg_uom_exist_issu, ls_uom_var_lower, lv_issu_count, lv_count, lv_no_issu.
      READ TABLE lt_reg_uom_exist[] INTO ls_reg_uom_exist_issu
      WITH KEY issue_unit = abap_true.
      IF sy-subrc NE 0.
        LOOP AT lt_uom_var_lower[] INTO ls_uom_var_lower
           WHERE despatch_unit = abap_true.
          lv_count = lv_count + 1.
        ENDLOOP.

        lv_issu_count = lv_count.
      ELSE.
        lv_no_issu = abap_true.
      ENDIF.

* Default Issue Unit, if required
      IF lv_no_issu NE abap_true.


        lt_uom_var_lower_tmp[] = lt_uom_var_lower[].

        IF lv_issu_count GE 1.
          DELETE lt_uom_var_lower_tmp[] WHERE despatch_unit NE abap_true.

* Get the highest UOM Category and UOM on the top
          SORT lt_uom_var_lower_tmp[] BY cat_seqno DESCENDING seqno DESCENDING.

* Default the topmost
          CLEAR ls_uom_var_lower_tmp.
          READ TABLE lt_uom_var_lower_tmp[] INTO ls_uom_var_lower_tmp INDEX 1.
          IF sy-subrc = 0.
            READ TABLE lt_reg_uom_exist[] ASSIGNING <ls_reg_uom_exist>
                       WITH KEY idno                 = ls_uom_var_lower_tmp-idno
                                uom_category         = ls_uom_var_lower_tmp-uom_category
                                pim_uom_code         = ls_uom_var_lower_tmp-uom_code
                                hybris_internal_code = ls_uom_var_lower_tmp-hybris_internal_code
                                lower_uom            = ls_uom_var_lower_tmp-lower_uom
                                lower_child_uom      = ls_uom_var_lower_tmp-lower_child_uom.
            IF sy-subrc = 0.
              <ls_reg_uom_exist>-issue_unit = abap_true.
            ENDIF.

          ENDIF.

        ELSEIF lv_issu_count EQ 0.
* If no Issue Unit is found in National/Regional then default Order Unit as Issue Unit
          READ TABLE lt_reg_uom_exist[] ASSIGNING <ls_reg_uom_exist>
          WITH KEY unit_purord = abap_true.
          IF sy-subrc = 0.
            <ls_reg_uom_exist>-issue_unit = abap_true.
          ENDIF.

        ENDIF.  " IF lv_issu_count GE 1

      ENDIF.  " IF lv_no_issu NE abap_true
    ENDIF.  " ISSUE_UNIT

    SORT lt_reg_uom_exist[] BY cat_seqno seqno.

    CLEAR: lt_reg_uom_ui[], lt_reg_uom[].

* Move the Defaulted all UOMs to export parameters
    LOOP AT lt_reg_uom_exist[] ASSIGNING <ls_reg_uom_exist>.


      READ TABLE lt_gui_def TRANSPORTING NO FIELDS
      WITH TABLE KEY tabname  = 'ZARN_REG_UOM'
                     fldname  = 'HIGHER_LEVEL_UNITS'
                     logic_ty = gc_logic_l.
      IF sy-subrc = 0.

        CLEAR ls_reg_uom_ui_tmp.

        IF <ls_reg_uom_exist>-uom_category = gc_pallet.
          READ TABLE lt_reg_uom_exist[] INTO ls_reg_uom_ui_tmp
          WITH KEY idno                 = <ls_reg_uom_exist>-idno
                   uom_category         = gc_layer
                   pim_uom_code         = <ls_reg_uom_exist>-pim_uom_code
                   hybris_internal_code = <ls_reg_uom_exist>-hybris_internal_code
                   lower_uom            = <ls_reg_uom_exist>-lower_uom
                   lower_child_uom      = <ls_reg_uom_exist>-lower_child_uom.
        ELSE.
          READ TABLE lt_reg_uom_exist[] INTO ls_reg_uom_ui_tmp
          WITH KEY idno                 = <ls_reg_uom_exist>-idno
                   pim_uom_code         = <ls_reg_uom_exist>-lower_uom
                   hybris_internal_code = space
                   lower_uom            = <ls_reg_uom_exist>-lower_child_uom.
        ENDIF.


*        IF ls_reg_uom_ui_tmp IS NOT INITIAL AND ls_reg_uom_ui_tmp-num_base_units NE 0.

        IF <ls_reg_uom_exist>-num_base_units IS INITIAL.
          <ls_reg_uom_exist>-num_base_units = 1.
        ENDIF.

        IF <ls_reg_uom_exist>-factor_of_base_units IS INITIAL.
          <ls_reg_uom_exist>-factor_of_base_units = 1.
        ENDIF.

        IF ls_reg_uom_ui_tmp-num_base_units IS INITIAL.
          ls_reg_uom_ui_tmp-num_base_units = 1.
        ENDIF.

        IF ls_reg_uom_ui_tmp-factor_of_base_units IS INITIAL.
          ls_reg_uom_ui_tmp-factor_of_base_units = 1.
        ENDIF.


        <ls_reg_uom_exist>-higher_level_units =
          ( <ls_reg_uom_exist>-num_base_units / <ls_reg_uom_exist>-factor_of_base_units ) /
          ( ls_reg_uom_ui_tmp-num_base_units / ls_reg_uom_ui_tmp-factor_of_base_units ).


      ENDIF.  " HIGHER_LEVEL_UNITS

      CLEAR: ls_reg_uom.
      MOVE-CORRESPONDING <ls_reg_uom_exist> TO ls_reg_uom.
      APPEND ls_reg_uom TO lt_reg_uom[].

      APPEND <ls_reg_uom_exist> TO lt_reg_uom_ui[].

    ENDLOOP.

    et_reg_uom_ui[] = lt_reg_uom_ui[].
    es_reg_data_def-zarn_reg_uom[] = lt_reg_uom[].


  ENDMETHOD.                    "SET_REG_UOM_DEFAULT


METHOD set_reg_uom_default_int.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 09.03.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Default Regional UOM fields
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  " Note: The logic has been copied from method SET_REG_UOM_DEFAULT as part of
  " modularization. However, this code can be further modularized where required
  " in the future

  CLEAR: ev_dim_overflow_error,
         et_reg_uom_exist,
         et_reg_uom_ui.

  DATA(lt_reg_uom_ui) = me->map_uom_variant_to_reg_uom( EXPORTING is_prod_data          = is_prod_data
                                                        IMPORTING ev_dim_overflow_error = ev_dim_overflow_error ).

  me->map_pir_to_reg_uom( EXPORTING is_prod_data  = is_prod_data
                          CHANGING  ct_reg_uom_ui = lt_reg_uom_ui ).

  " If UOM is already maintained in Regional then don't default and use the values as in DB
  et_reg_uom_exist = me->retain_regional_uom_db_data( EXPORTING is_reg_data   = is_reg_data
                                                                is_prod_data  = is_prod_data
                                                                it_reg_uom_db = it_reg_uom_db
                                                                it_gui_def    = it_gui_def
                                                      CHANGING  ct_reg_uom_ui = lt_reg_uom_ui ).

  et_reg_uom_ui = lt_reg_uom_ui.

ENDMETHOD.


  METHOD set_reg_uom_default_ui.
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             # 04.03.2020
* CHANGE No.       # CIP-1102 - Ability to manually maintain Regonal
* DESCRIPTION      # Added National UOM fields to Regional UOM structure
*                  # and populated
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------

    DATA: ls_reg_data        TYPE zsarn_reg_data,
          ls_prod_data       TYPE zsarn_prod_data,

          lt_reg_uom_ui      TYPE ztarn_reg_uom_ui,
          ls_reg_uom_ui      TYPE zsarn_reg_uom_ui,
          lt_reg_uom_db      TYPE ztarn_reg_uom,
          ls_reg_uom_db      TYPE zarn_reg_uom,

          ls_uom_cat         TYPE zarn_uom_cat,
          lv_lower_uom       TYPE zarn_lower_uom,
          lv_lower_child_uom TYPE zarn_lower_child_uom,
          ls_uom_variant     TYPE zarn_uom_variant,
          ls_pir             TYPE zarn_pir,
          lv_feature_a4_enabled TYPE abap_bool,
          lv_nat_texts_exist TYPE abap_bool.

*******************************************************************************

    TRY.
        lv_feature_a4_enabled = zcl_ftf_store_factory=>get_instance(
                                         )->get_feature( iv_store     = 'RFST'    "Dummy Value
                                                         iv_appl_area = zif_ftf_appl_area_c=>gc_arena
                                                         iv_feature   = 'A4'
                                         )->is_active( ).
      CATCH zcx_object_not_found.
        lv_feature_a4_enabled = abap_false.
    ENDTRY.


    ls_reg_data  = is_reg_data.
    ls_prod_data = is_prod_data.

    lt_reg_uom_db[] = ls_reg_data-zarn_reg_uom[].

* Retreive Initial Global Data, if not initialized
    IF gv_initialized IS INITIAL.
      CALL METHOD me->get_initial_data
        EXPORTING
          is_prod_data = ls_prod_data.
    ENDIF.

*******************************************************************************

    lv_nat_texts_exist = abap_false.
    IF lv_feature_a4_enabled EQ abap_true.
      LOOP AT ls_prod_data-zarn_uom_variant[] INTO ls_uom_variant.
        IF ls_uom_variant-net_content_short_desc IS NOT INITIAL
        OR ls_uom_variant-fscust_prod_name40     IS NOT INITIAL
        OR ls_uom_variant-fscust_prod_name80     IS NOT INITIAL
        OR ls_uom_variant-fscust_prod_name255    IS NOT INITIAL.
          lv_nat_texts_exist = abap_true.
        ENDIF.
      ENDLOOP.
    ENDIF.


    LOOP AT lt_reg_uom_db[] INTO ls_reg_uom_db.

      CLEAR ls_reg_uom_ui.
      MOVE-CORRESPONDING ls_reg_uom_db TO ls_reg_uom_ui.

* Get UOM Category Sequence
      CLEAR ls_uom_cat.
      READ TABLE gt_uom_cat[] INTO ls_uom_cat
      WITH KEY uom_category = ls_reg_uom_db-uom_category
               uom          = ls_reg_uom_db-meinh.
      IF sy-subrc = 0.
        ls_reg_uom_ui-seqno     = ls_uom_cat-seqno.
        ls_reg_uom_ui-cat_seqno = ls_uom_cat-cat_seqno.
      ENDIF.


* Get lower uom and lower child uom codes from National
      CLEAR: lv_lower_uom, lv_lower_child_uom.
      CALL FUNCTION 'ZARN_GET_NAT_LOWER_CHILD_UOM'
        EXPORTING
          iv_idno            = ls_prod_data-idno
          iv_version         = ls_prod_data-version
          iv_uom_category    = 'RETAIL'
          iv_uom_code        = ls_reg_uom_db-pim_uom_code
          iv_hybris_code     = space
          it_uom_variant     = ls_prod_data-zarn_uom_variant[]
          it_gtin_var        = ls_prod_data-zarn_gtin_var[]
        IMPORTING
          ev_lower_uom       = lv_lower_uom
          ev_lower_child_uom = lv_lower_child_uom
        EXCEPTIONS
          input_reqd         = 1
          OTHERS             = 2.

* Check if current UOM exist in National data
      CLEAR ls_uom_variant.
      IF lv_lower_uom       = ls_reg_uom_db-lower_uom       AND
         lv_lower_child_uom = ls_reg_uom_db-lower_child_uom.
        READ TABLE ls_prod_data-zarn_uom_variant[] INTO ls_uom_variant
        WITH KEY idno     = ls_reg_uom_db-idno
                 uom_code = ls_reg_uom_db-pim_uom_code.
      ENDIF.

* check if current UOM exist in National PIR
      CLEAR ls_pir.
      READ TABLE ls_prod_data-zarn_pir[] INTO ls_pir
      WITH KEY idno                 = ls_reg_uom_db-idno
               uom_code             = ls_reg_uom_db-pim_uom_code
               hybris_internal_code = ls_reg_uom_db-hybris_internal_code.



      IF ls_reg_uom_db-uom_category = gc_layer
      OR ls_reg_uom_db-uom_category = gc_pallet.

        IF ls_pir IS INITIAL.
          ls_reg_uom_ui-obsolete_ind = icon_dummy.
        ENDIF.

      ELSEIF ls_reg_uom_db-uom_category =  gc_retail
          OR ls_reg_uom_db-uom_category =  gc_inner
          OR ls_reg_uom_db-uom_category =  gc_shipper.

        IF ls_uom_variant IS INITIAL.
          ls_reg_uom_ui-obsolete_ind = icon_dummy.
        ENDIF.

      ELSE.

        ls_reg_uom_ui-obsolete_ind = icon_dummy.

      ENDIF.  " IF ls_reg_uom_db-uom_category = gc_layer/gc_pallet

      "National Data
      map_nat_uom_to_reg_uom_stru( EXPORTING is_uom_variant          = ls_uom_variant
                                   CHANGING  cs_reg_uom_nat_data     = ls_reg_uom_ui-nat_data ).

      APPEND ls_reg_uom_ui TO lt_reg_uom_ui[].

    ENDLOOP.

*******************************************************************************

    SORT lt_reg_uom_ui[] BY cat_seqno seqno.

    et_reg_uom_ui[] = lt_reg_uom_ui[].

  ENDMETHOD.                    "SET_REG_UOM_DEFAULT


  METHOD set_reg_uom_ean_greeting_def.



    TYPES:
      BEGIN OF ty_uom,
        idno         TYPE zarn_idno,
        pim_uom_code TYPE zarn_uom_code,
        suffix       TYPE i,
      END OF ty_uom.


    DATA: ls_reg_data        TYPE zsarn_reg_data,
          ls_prod_data       TYPE zsarn_prod_data,
          lt_gui_def         TYPE SORTED TABLE OF zarn_gui_def WITH UNIQUE KEY tabname fldname logic_ty,


          lt_gtin_var        TYPE ztarn_gtin_var,
          ls_gtin_var        TYPE zarn_gtin_var,
          lt_uom_variant     TYPE ztarn_uom_variant,
          ls_uom_variant     TYPE zarn_uom_variant,

*          lt_reg_uom_db        TYPE ztarn_reg_uom,
*          ls_reg_uom_db        TYPE zarn_reg_uom,
          lt_reg_ean_db      TYPE ztarn_reg_ean,
          ls_reg_ean_db      TYPE zarn_reg_ean,


          ls_tvarv_ean_cate  TYPE tvarvc,
          lv_ean_prefix      TYPE ean11,
          lv_eantp           TYPE numtp,


          lt_reg_uom_ui      TYPE ztarn_reg_uom_ui,
          ls_reg_uom_ui      TYPE zsarn_reg_uom_ui,
          ls_reg_uom_ui_ea   TYPE zsarn_reg_uom_ui,

          lt_reg_uom         TYPE ztarn_reg_uom,
          ls_reg_uom         TYPE zarn_reg_uom,
          lt_reg_ean         TYPE ztarn_reg_ean,
          ls_reg_ean         TYPE zarn_reg_ean,

          lt_marm_idno       TYPE ty_t_marm_idno,
          ls_marm_idno       TYPE ty_s_marm_idno,

          lt_reg_ean_ui      TYPE ztarn_reg_ean_ui,
          ls_reg_ean_ui      TYPE zsarn_reg_ean_ui,
          lt_reg_ean_new     TYPE ztarn_reg_ean_ui,
          ls_reg_ean_new     TYPE zsarn_reg_ean_ui,



          lt_uom_cat         TYPE ztarn_uom_cat,
          ls_uom_cat         TYPE zarn_uom_cat,
          lt_uom             TYPE TABLE OF ty_uom,
          ls_uom             TYPE ty_uom,
          lt_uom_added       TYPE ztarn_uom_cat,
          ls_uom_added       TYPE zarn_uom_cat,
          ls_celltab         TYPE lvc_s_styl,
          lt_celltab         TYPE lvc_t_styl,

          lv_suffix          TYPE n LENGTH 4,
          lv_tabix           TYPE sy-tabix,
          lv_limit           TYPE i,
          lv_start           TYPE i,
          lv_len             TYPE i,
          lv_max_relation    TYPE i,

          lv_lower_uom       TYPE zarn_lower_uom,
          lv_lower_child_uom TYPE zarn_lower_child_uom,

          lv_no_uom_avail    TYPE flag,

          ls_prod_uom_t      TYPE ty_s_prod_uom_t.



    FIELD-SYMBOLS: <ls_reg_ean_ui>      TYPE zsarn_reg_ean_ui,
                   <ls_reg_ean_ui_main> TYPE zsarn_reg_ean_ui.



    ls_reg_data     = is_reg_data.
    ls_prod_data    = is_prod_data.
    lt_reg_uom_ui[] = it_reg_uom_ui[].
    lt_marm_idno[]  = gt_marm_idno[].

    lt_gtin_var    = ls_prod_data-zarn_gtin_var[].
    lt_uom_variant = ls_prod_data-zarn_uom_variant[].

*    lt_reg_uom_db  = ls_reg_data-zarn_reg_uom[].
    lt_reg_ean_db  = ls_reg_data-zarn_reg_ean[].


* Retreive Initial Global Data, if not initialized
    IF gv_initialized IS INITIAL.
      CALL METHOD me->get_initial_data
        EXPORTING
          is_prod_data = ls_prod_data.
    ENDIF.

    INSERT LINES OF gt_gui_def_recon[] INTO TABLE lt_gui_def[].
    DELETE lt_gui_def[] WHERE tabname NE 'ZARN_REG_UOM' AND tabname NE 'ZARN_REG_EAN'.


**********************************************************************************

*** Get Existing and New EANs


    CLEAR: lt_reg_ean_ui[], lt_reg_ean_new[].

* Default Only if asked, else return whatever in REG_EAN Table
    IF iv_reg_only IS INITIAL.

* Check if National GTIN exist in Regional
      LOOP AT lt_gtin_var[] INTO ls_gtin_var.

*      lv_tabix = sy-tabix.

        CLEAR: ls_celltab, lt_celltab[].
        ls_celltab-fieldname = 'IDNO'.
        ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
        INSERT ls_celltab INTO TABLE lt_celltab[].

        ls_celltab-fieldname = 'MEINH'.
        ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
        INSERT ls_celltab INTO TABLE lt_celltab[].

        ls_celltab-fieldname = 'EAN11'.
        ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
        INSERT ls_celltab INTO TABLE lt_celltab[].

        ls_celltab-fieldname = 'EANTP'.
        ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
        INSERT ls_celltab INTO TABLE lt_celltab[].

        ls_celltab-fieldname = 'HPEAN'.
        ls_celltab-style     = cl_gui_alv_grid=>mc_style_enabled.
        INSERT ls_celltab INTO TABLE lt_celltab[].

        ls_celltab-fieldname = 'NAT_EAN_FLAG'.
        ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
        INSERT ls_celltab INTO TABLE lt_celltab[].

        ls_celltab-fieldname = 'CAT_SEQNO'.
        ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
        INSERT ls_celltab INTO TABLE lt_celltab[].

        ls_celltab-fieldname = 'SEQNO'.
        ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
        INSERT ls_celltab INTO TABLE lt_celltab[].



        CLEAR ls_reg_ean_db.
        READ TABLE lt_reg_ean_db[] INTO ls_reg_ean_db
        WITH KEY idno  = ls_gtin_var-idno
                 ean11 = ls_gtin_var-gtin_code.
        IF sy-subrc EQ 0.

          CLEAR ls_reg_ean_ui.
          MOVE-CORRESPONDING ls_reg_ean_db TO ls_reg_ean_ui.

          ls_reg_ean_ui-nat_ean_flag = abap_true.

          CLEAR ls_uom_cat.
          READ TABLE gt_uom_cat INTO ls_uom_cat
          WITH KEY uom = ls_reg_ean_ui-meinh.
          IF sy-subrc = 0.
            ls_reg_ean_ui-cat_seqno = ls_uom_cat-cat_seqno.
            ls_reg_ean_ui-seqno     = ls_uom_cat-seqno.
          ENDIF.

          ls_reg_ean_ui-celltab[] = lt_celltab[].


          APPEND ls_reg_ean_ui TO lt_reg_ean_ui[].


        ELSE.

          CLEAR ls_reg_ean_new.
          ls_reg_ean_new-mandt        = sy-mandt.
          ls_reg_ean_new-idno         = ls_gtin_var-idno.
          ls_reg_ean_new-ean11        = ls_gtin_var-gtin_code.
          ls_reg_ean_new-nat_ean_flag = abap_true.


          READ TABLE lt_gui_def TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname  = 'ZARN_REG_EAN'
                         fldname  = 'HPEAN'
                         logic_ty = gc_logic_l.
          IF sy-subrc = 0.
            ls_reg_ean_new-hpean        = ls_gtin_var-gtin_current.
          ENDIF.



* Get lower uom and lower child uom codes from National
          CLEAR: lv_lower_uom, lv_lower_child_uom.
          CALL FUNCTION 'ZARN_GET_NAT_LOWER_CHILD_UOM'
            EXPORTING
              iv_idno            = ls_gtin_var-idno
              iv_version         = ls_gtin_var-version
              iv_uom_category    = 'RETAIL'
              iv_uom_code        = ls_gtin_var-uom_code
              iv_hybris_code     = space
              it_uom_variant     = ls_prod_data-zarn_uom_variant[]
              it_gtin_var        = ls_prod_data-zarn_gtin_var[]
            IMPORTING
              ev_lower_uom       = lv_lower_uom
              ev_lower_child_uom = lv_lower_child_uom
            EXCEPTIONS
              input_reqd         = 1
              OTHERS             = 2.





* Get MEINH from REG_UOM
          CLEAR ls_reg_uom_ui.
          READ TABLE lt_reg_uom_ui INTO ls_reg_uom_ui
          WITH KEY idno                 = ls_gtin_var-idno
                   pim_uom_code         = ls_gtin_var-uom_code
                   hybris_internal_code = space
                   lower_uom            = lv_lower_uom
                   lower_child_uom      = lv_lower_child_uom.
          IF sy-subrc EQ 0.
            ls_reg_ean_new-meinh = ls_reg_uom_ui-meinh.


            READ TABLE lt_gui_def TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname  = 'ZARN_REG_EAN'
                           fldname  = 'EANTP'
                           logic_ty = gc_logic_l.
            IF sy-subrc = 0.
              LOOP AT gt_tvarv_ean_cate INTO ls_tvarv_ean_cate.
                CLEAR: lv_ean_prefix, lv_eantp.

* If GTIN starts from '9400989' then default it to "ZN"
                IF ls_tvarv_ean_cate-name EQ 'ZARN_GUI_DEFLT_EAN_PREFIX_CATE'.
                  SPLIT ls_tvarv_ean_cate-low AT '~' INTO lv_ean_prefix lv_eantp.
                  CONCATENATE lv_ean_prefix '*' INTO lv_ean_prefix.
                  IF ls_reg_ean_ui-ean11 CP lv_ean_prefix.
                    ls_reg_ean_new-eantp = lv_eantp.
                    EXIT.
                  ENDIF.
                ENDIF.
              ENDLOOP.

* in case no prefix is matched then default it to "IE"
              IF ls_reg_ean_new-eantp IS INITIAL.
                CLEAR ls_tvarv_ean_cate.
                READ TABLE gt_tvarv_ean_cate INTO ls_tvarv_ean_cate
                WITH KEY name = 'ZARN_GUI_DEFAULT_EAN_CATEGORY'.
                IF sy-subrc EQ 0.
                  ls_reg_ean_new-eantp = ls_tvarv_ean_cate-low.
                ENDIF.
              ENDIF.

            ENDIF.  " EANTP

            IF iv_reg_only = abap_true.
              CLEAR: ls_reg_ean_new-meinh, ls_reg_ean_new-eantp.
            ENDIF.

            ls_reg_ean_new-celltab[] = lt_celltab[].



            CLEAR ls_uom_cat.
            READ TABLE gt_uom_cat[] INTO ls_uom_cat
            WITH KEY uom = ls_reg_ean_new-meinh.
            IF sy-subrc = 0.
              ls_reg_ean_new-cat_seqno = ls_uom_cat-cat_seqno.
              ls_reg_ean_new-seqno     = ls_uom_cat-seqno.
            ENDIF.

          ENDIF.

          APPEND ls_reg_ean_new TO lt_reg_ean_new[].
        ENDIF.
      ENDLOOP.  " LOOP AT lt_gtin_var[] INTO ls_gtin_var


      IF lt_reg_ean_new[] IS INITIAL.
        es_reg_data_def = is_reg_data.
        et_reg_uom_ui   = it_reg_uom_ui[].
        et_reg_ean_ui   = lt_reg_ean_ui[].
*        ev_no_uom_avail = space.
        EXIT.
      ENDIF.


    ENDIF.  " IF iv_reg_only IS INITIAL
**********************************************************************************

* If EAN already exist in DataBase
    LOOP AT lt_reg_ean_db[] INTO ls_reg_ean_db.

      CLEAR: ls_celltab, lt_celltab[].
      ls_celltab-fieldname = 'IDNO'.
      ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
      INSERT ls_celltab INTO TABLE lt_celltab[].

      ls_celltab-fieldname = 'NAT_EAN_FLAG'.
      ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
      INSERT ls_celltab INTO TABLE lt_celltab[].

      ls_celltab-fieldname = 'CAT_SEQNO'.
      ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
      INSERT ls_celltab INTO TABLE lt_celltab[].

      ls_celltab-fieldname = 'SEQNO'.
      ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
      INSERT ls_celltab INTO TABLE lt_celltab[].


      IF iv_reg_only = abap_true.

        ls_celltab-fieldname = 'MEINH'.
        ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
        INSERT ls_celltab INTO TABLE lt_celltab[].

        ls_celltab-fieldname = 'EAN11'.
        ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
        INSERT ls_celltab INTO TABLE lt_celltab[].

        ls_celltab-fieldname = 'EANTP'.
        ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
        INSERT ls_celltab INTO TABLE lt_celltab[].

        ls_celltab-fieldname = 'HPEAN'.
        ls_celltab-style     = cl_gui_alv_grid=>mc_style_disabled.
        INSERT ls_celltab INTO TABLE lt_celltab[].

      ENDIF.   " IF iv_reg_only = abap_true





      CLEAR ls_uom_cat.
      READ TABLE gt_uom_cat[] INTO ls_uom_cat
      WITH KEY uom = ls_reg_ean_db-meinh.



      READ TABLE lt_gui_def TRANSPORTING NO FIELDS
      WITH TABLE KEY tabname  = 'ZARN_REG_EAN'
                     fldname  = 'HPEAN'
                     logic_ty = gc_logic_l.
      IF sy-subrc = 0.
* Remove national Main GTIN if it is there in Regional
        IF ls_reg_ean_db-hpean = abap_true.
          IF <ls_reg_ean_ui_main> IS ASSIGNED. UNASSIGN <ls_reg_ean_ui_main>. ENDIF.
          READ TABLE lt_reg_ean_ui[] ASSIGNING <ls_reg_ean_ui_main>
          WITH KEY idno         = ls_reg_ean_db-idno
                   meinh        = ls_reg_ean_db-meinh
                   hpean        = abap_true
                   nat_ean_flag = abap_true.
          IF sy-subrc = 0.
            CLEAR <ls_reg_ean_ui_main>-hpean.
          ENDIF.
        ENDIF.
      ENDIF.  " HPEAN


      IF <ls_reg_ean_ui> IS ASSIGNED. UNASSIGN <ls_reg_ean_ui>. ENDIF.
      READ TABLE lt_reg_ean_ui[] ASSIGNING <ls_reg_ean_ui>
      WITH KEY idno  = ls_reg_ean_db-idno
               meinh = ls_reg_ean_db-meinh
               ean11 = ls_reg_ean_db-ean11.
      IF sy-subrc = 0.
        MOVE-CORRESPONDING ls_reg_ean_db TO <ls_reg_ean_ui>.

        <ls_reg_ean_ui>-cat_seqno    = ls_uom_cat-cat_seqno.
        <ls_reg_ean_ui>-seqno        = ls_uom_cat-seqno.
        ls_reg_ean_ui-nat_ean_flag   = abap_true.

      ELSE.
        CLEAR ls_reg_ean_ui.
        MOVE-CORRESPONDING ls_reg_ean_db TO ls_reg_ean_ui.
        ls_reg_ean_ui-nat_ean_flag = space.
        ls_reg_ean_ui-cat_seqno = ls_uom_cat-cat_seqno.
        ls_reg_ean_ui-seqno     = ls_uom_cat-seqno.
        ls_reg_ean_ui-celltab   = lt_celltab[].

        APPEND ls_reg_ean_ui TO lt_reg_ean_ui[].
      ENDIF.


    ENDLOOP.   " LOOP AT lt_reg_ean_db[] INTO ls_reg_ean_db
**********************************************************************************


    IF iv_reg_only = abap_true.

      SORT lt_reg_ean_ui[] BY cat_seqno seqno ASCENDING
                                 nat_ean_flag DESCENDING
                                        hpean DESCENDING.

      CLEAR lt_reg_ean[].
* Move the Defaulted all EANs to export parameters
      LOOP AT lt_reg_ean_ui[] INTO ls_reg_ean_ui.
        CLEAR ls_reg_ean.
        MOVE-CORRESPONDING ls_reg_ean_ui TO ls_reg_ean.
        APPEND ls_reg_ean TO lt_reg_ean[].
      ENDLOOP.

      SORT lt_reg_uom_ui[] BY cat_seqno seqno.

      CLEAR lt_reg_uom[].
* Move the Defaulted all UOMs to export parameters
      LOOP AT lt_reg_uom_ui[] INTO ls_reg_uom_ui.
        CLEAR ls_reg_uom.
        MOVE-CORRESPONDING ls_reg_uom_ui TO ls_reg_uom.
        APPEND ls_reg_uom TO lt_reg_uom[].
      ENDLOOP.



      es_reg_data_def-zarn_reg_ean[] = lt_reg_ean[].
      es_reg_data_def-zarn_reg_uom[] = lt_reg_uom[].
      et_reg_uom_ui[]                = lt_reg_uom_ui[].
      et_reg_ean_ui[]                = lt_reg_ean_ui[].
*      ev_no_uom_avail                = space.

      EXIT.
    ENDIF.  " IF iv_reg_only = abap_true

**********************************************************************************



*** Filter list of UOMs to be assigned,
*** only those UOMs will be left which are required for creating Local UOM codes

    CLEAR: lv_no_uom_avail.


    lv_limit = gs_greet_limit-low.

    IF lv_limit IS INITIAL.
      lv_limit = 199.   " AS400 limit of EANs/UOM
    ENDIF.


    lt_uom_cat[] = gt_uom_cat[].

* KG is not used to create LOCAL UOM CODEs
    DELETE lt_uom_cat[] WHERE uom = 'KG'.


    CLEAR: lt_uom[].

* Remove those UOMs which are not LOCAL and are already defaulted in REG_UOM
* by comparing last 4 chars as numeric (which indicated local uom code) of UOM Code
    LOOP AT lt_reg_uom_ui INTO ls_reg_uom_ui.

      lv_len = strlen( ls_reg_uom_ui-pim_uom_code ).
      lv_len = lv_len - 4.
      IF lv_len GE 4.
        IF ls_reg_uom_ui-pim_uom_code+lv_len(4) CN '1234567890'.  " considered not Local
          IF ls_reg_uom_ui-meinh NE 'EA'.
            DELETE lt_uom_cat[] WHERE uom = ls_reg_uom_ui-meinh.
          ENDIF.
        ELSE.

          CLEAR ls_uom.
          ls_uom-idno         = ls_reg_uom_ui-idno.
          ls_uom-pim_uom_code = ls_reg_uom_ui-pim_uom_code+lv_len(4).

          CONDENSE: ls_uom-pim_uom_code, ls_uom-idno.
          ls_uom-suffix = ls_uom-pim_uom_code.
          APPEND ls_uom TO lt_uom.

        ENDIF.
      ELSE.
        DELETE lt_uom_cat[] WHERE uom = ls_reg_uom_ui-meinh.
      ENDIF.

    ENDLOOP.  " LOOP AT lt_reg_uom_ui INTO ls_reg_uom_ui


    IF lt_uom_cat[] IS INITIAL.
      SORT lt_reg_ean_ui[] BY cat_seqno seqno ASCENDING
                                 nat_ean_flag DESCENDING
                                        hpean DESCENDING.

      es_reg_data_def = is_reg_data.
      et_reg_uom_ui   = it_reg_uom_ui[].
      et_reg_ean_ui   = lt_reg_ean_ui[].
      ev_no_uom_avail = abap_true.
      EXIT.
    ENDIF.



* Remove all those UOMs where 199 EANs are already defaulted
    LOOP AT lt_uom_cat[] INTO ls_uom_cat.

      lv_tabix = sy-tabix.

      CLEAR  lv_start.
* Count total EANs for highest UOM
      LOOP AT lt_reg_ean_ui[] INTO ls_reg_ean_ui
        WHERE idno  = ls_prod_data-idno
          AND meinh = ls_uom_cat-uom.
        lv_start = lv_start + 1.
      ENDLOOP.

      IF lv_start GE 199.
        DELETE lt_uom_cat[] INDEX lv_tabix.
      ENDIF.

    ENDLOOP.  " LOOP AT lt_uom_cat[] INTO ls_uom_cat



    IF lt_uom_cat[] IS INITIAL.
      SORT lt_reg_ean_ui[] BY cat_seqno seqno ASCENDING
                                 nat_ean_flag DESCENDING
                                        hpean DESCENDING.

      es_reg_data_def = is_reg_data.
      et_reg_uom_ui   = it_reg_uom_ui[].
      et_reg_ean_ui   = lt_reg_ean_ui[].
      ev_no_uom_avail = abap_true.
      EXIT.
    ENDIF.


* Remove all those UOMs which are created in MARM

    DELETE lt_marm_idno[] WHERE idno NE is_prod_data-idno.

    LOOP AT lt_marm_idno[] INTO ls_marm_idno WHERE meinh NE 'EA'.
      DELETE lt_uom_cat[] WHERE uom = ls_marm_idno-meinh.
    ENDLOOP.

    IF lt_uom_cat[] IS INITIAL.
      SORT lt_reg_ean_ui[] BY cat_seqno seqno ASCENDING
                                 nat_ean_flag DESCENDING
                                        hpean DESCENDING.

      es_reg_data_def = is_reg_data.
      et_reg_uom_ui   = it_reg_uom_ui[].
      et_reg_ean_ui   = lt_reg_ean_ui[].
      ev_no_uom_avail = abap_true.
      EXIT.
    ENDIF.

**********************************************************************************



    CLEAR lv_start.

* Get next available UOM to be created for Local UOM code creation
    SORT lt_uom_cat[] BY cat_seqno seqno.
    CLEAR ls_uom_cat.
    READ TABLE lt_uom_cat[] INTO ls_uom_cat INDEX 1.
    IF sy-subrc = 0.

* Count total EANs for next UOM
      LOOP AT lt_reg_ean_ui[] INTO ls_reg_ean_ui
        WHERE idno  = ls_prod_data-idno
          AND meinh = ls_uom_cat-uom.
        lv_start = lv_start + 1.
      ENDLOOP.


      lv_start = lv_start + 1.

      CLEAR : lt_uom_added[].
      APPEND ls_uom_cat TO lt_uom_added[].
    ENDIF.


* Create EANs for RETAIL and Local UOMs
    LOOP AT lt_reg_ean_new[] INTO ls_reg_ean_new
      WHERE meinh = 'EA'.  " cat_seqno = '001'.

      lv_tabix = sy-tabix.


      CLEAR ls_reg_ean_ui.

      ls_reg_ean_ui = ls_reg_ean_new.

      ls_reg_ean_ui-nat_ean_flag = abap_true.


      IF lv_start LE lv_limit.
* Add to existing EAN
        ls_reg_ean_ui-meinh = ls_uom_cat-uom.

        ls_reg_ean_ui-cat_seqno = ls_uom_cat-cat_seqno.
        ls_reg_ean_ui-seqno     = ls_uom_cat-seqno.

        APPEND ls_reg_ean_ui TO lt_reg_ean_ui[].

        lv_start = lv_start + 1.

*        APPEND ls_uom_cat TO lt_uom_added[].


      ELSE.

* Remove current UOM to get next UOM
        DELETE lt_uom_cat[] INDEX 1.

* If no more UOMs are available then quit defaulting for EANs
        IF lt_uom_cat[] IS INITIAL.
          lv_no_uom_avail = abap_true.
          EXIT.
        ENDIF.


        CLEAR lv_start.
* Get next available UOM to be created for Local UOM code creation
        CLEAR ls_uom_cat.
        READ TABLE lt_uom_cat[] INTO ls_uom_cat INDEX 1.
        IF sy-subrc = 0.

* Add to existing EAN
          ls_reg_ean_ui-meinh = ls_uom_cat-uom.

          ls_reg_ean_ui-cat_seqno = ls_uom_cat-cat_seqno.
          ls_reg_ean_ui-seqno     = ls_uom_cat-seqno.

          APPEND ls_reg_ean_ui TO lt_reg_ean_ui[].



* Count total EANs for next UOM
          LOOP AT lt_reg_ean_ui[] INTO ls_reg_ean_ui
            WHERE idno  = ls_prod_data-idno
              AND meinh = ls_uom_cat-uom.
            lv_start = lv_start + 1.
          ENDLOOP.

          lv_start = lv_start + 1.


          APPEND ls_uom_cat TO lt_uom_added[].
        ENDIF.  " READ TABLE lt_uom_cat[] INTO ls_uom_cat INDEX 1


      ENDIF.  " IF lv_start LE lv_limit

      DELETE lt_reg_ean_new[] INDEX lv_tabix.
    ENDLOOP.  " LOOP AT lt_reg_ean_new[] INTO ls_reg_ean_new where meinh = 'EA'


**********************************************************************************

* Set First GTIN of new UOM as Main if no main is set

    READ TABLE lt_gui_def TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname  = 'ZARN_REG_EAN'
                   fldname  = 'HPEAN'
                   logic_ty = gc_logic_l.
    IF sy-subrc = 0.

      LOOP AT lt_uom_added[] INTO ls_uom_added.

        READ TABLE lt_reg_ean_ui[] TRANSPORTING NO FIELDS
        WITH KEY idno  = ls_prod_data-idno
                 meinh = ls_uom_added-uom
                 hpean = abap_true.
        IF sy-subrc NE 0.

          LOOP AT lt_reg_ean_ui[] ASSIGNING <ls_reg_ean_ui>
            WHERE meinh = ls_uom_added-uom.

            CLEAR ls_reg_ean_db.
            READ TABLE lt_reg_ean_db[] INTO ls_reg_ean_db
            WITH KEY idno  = <ls_reg_ean_ui>-idno
                     meinh = <ls_reg_ean_ui>-meinh
                     ean11 = <ls_reg_ean_ui>-ean11.
            IF sy-subrc NE 0.
              <ls_reg_ean_ui>-hpean = abap_true.
              EXIT.
            ENDIF.

          ENDLOOP.  " LOOP AT lt_reg_ean_ui[] ASSIGNING <ls_reg_ean_ui>
        ENDIF.  " READ TABLE lt_reg_ean_ui[] TRANSPORTING NO FIELDS
      ENDLOOP.  " LOOP AT lt_uom_added[] INTO ls_uom_added

    ENDIF.  " HPEAN


**********************************************************************************

* For UOMs which are not RETAIL, add them to their respective UOM CODE,
* greeting card logic NOT applicable
    LOOP AT lt_reg_ean_new[] INTO ls_reg_ean_new.
      APPEND ls_reg_ean_new TO lt_reg_ean_ui[].
    ENDLOOP.

**********************************************************************************


* Get Highest Local UOM Code suffix
    CLEAR lv_suffix.

    IF lt_uom[] IS INITIAL.
      lv_suffix = '0001'.
    ELSE.
      SORT lt_uom[] BY suffix DESCENDING.
      CLEAR ls_uom.
      READ TABLE lt_uom INTO ls_uom INDEX 1.
      IF sy-subrc = 0.
        lv_suffix = ls_uom-suffix + 1.
      ENDIF.
    ENDIF.

    IF lv_suffix IS INITIAL.
      lv_suffix = '0001'.
    ENDIF.



* For all the UOM which are new and are defaulted, get a counter to group them together
* this is next incremented counter from Reg UOM data
    CLEAR lv_max_relation.
    lt_reg_uom[] = ls_reg_data-zarn_reg_uom[].

    SORT lt_reg_uom[] BY relationship DESCENDING.
    CLEAR ls_reg_uom.
    READ TABLE lt_reg_uom[] INTO ls_reg_uom INDEX 1.
    IF sy-subrc = 0.
      lv_max_relation = ls_reg_uom-relationship + 1.
    ENDIF.






* Get REG_UOM for Each
    CLEAR ls_reg_uom_ui_ea.
    READ TABLE lt_reg_uom_ui[] INTO ls_reg_uom_ui_ea
    WITH KEY meinh = 'EA'.
    IF sy-subrc = 0.
* For all the Added UOMs, create LOCAL UOM CODE into REG_UOM
* if UOM CODE doesn't exist for Added UOM
      LOOP AT lt_uom_added[] INTO ls_uom_added.

        CLEAR ls_reg_uom_ui.
        READ TABLE lt_reg_uom_ui[] INTO ls_reg_uom_ui
        WITH KEY meinh = ls_uom_added-uom.
        IF sy-subrc NE 0.

          ls_reg_uom_ui                      = ls_reg_uom_ui_ea.
          ls_reg_uom_ui-hybris_internal_code = gs_greet_limit-high.
          ls_reg_uom_ui-meinh                = ls_uom_added-uom.
          ls_reg_uom_ui-lower_meinh          = 'EA'.
          ls_reg_uom_ui-higher_level_units   = '1'.
          ls_reg_uom_ui-relationship         = lv_max_relation.

          CLEAR: ls_reg_uom_ui-lower_uom,
                 ls_reg_uom_ui-lower_child_uom,
                 ls_reg_uom_ui-unit_base,
                 ls_reg_uom_ui-unit_purord,
                 ls_reg_uom_ui-issue_unit,
                 ls_reg_uom_ui-sales_unit.

          CLEAR ls_uom_cat.
          READ TABLE gt_uom_cat[] INTO ls_uom_cat
          WITH KEY uom = ls_uom_added-uom.
          IF sy-subrc = 0.
            ls_reg_uom_ui-cat_seqno = ls_uom_cat-cat_seqno.
            ls_reg_uom_ui-seqno = ls_uom_cat-seqno.
            ls_reg_uom_ui-uom_category = ls_uom_cat-uom_category.
          ENDIF.


          ls_reg_uom_ui-pim_uom_code = ls_reg_uom_ui-idno && '_' && lv_suffix.
          CONDENSE ls_reg_uom_ui-pim_uom_code.

          ls_reg_uom_ui-obsolete_ind = icon_dummy.

          APPEND ls_reg_uom_ui TO lt_reg_uom_ui[].

          lv_suffix = lv_suffix + 1.

        ENDIF.

      ENDLOOP.  " LOOP AT lt_uom_added[] INTO ls_uom_added
    ENDIF.


**********************************************************************************

    SORT lt_reg_ean_ui[] BY cat_seqno seqno ASCENDING
                               nat_ean_flag DESCENDING
                                      hpean DESCENDING.

    CLEAR lt_reg_ean[].
* Move the Defaulted all EANs to export parameters
    LOOP AT lt_reg_ean_ui[] INTO ls_reg_ean_ui.
      CLEAR ls_reg_ean.
      MOVE-CORRESPONDING ls_reg_ean_ui TO ls_reg_ean.
      APPEND ls_reg_ean TO lt_reg_ean[].
    ENDLOOP.

    SORT lt_reg_uom_ui[] BY cat_seqno seqno.

    CLEAR lt_reg_uom[].
* Move the Defaulted all UOMs to export parameters
    LOOP AT lt_reg_uom_ui[] INTO ls_reg_uom_ui.
      CLEAR ls_reg_uom.
      MOVE-CORRESPONDING ls_reg_uom_ui TO ls_reg_uom.
      APPEND ls_reg_uom TO lt_reg_uom[].
    ENDLOOP.



    es_reg_data_def-zarn_reg_ean[] = lt_reg_ean[].
    es_reg_data_def-zarn_reg_uom[] = lt_reg_uom[].
    et_reg_uom_ui[]                = lt_reg_uom_ui[].
    et_reg_ean_ui[]                = lt_reg_ean_ui[].
    ev_no_uom_avail                = lv_no_uom_avail.



  ENDMETHOD.


  METHOD unit_conversion_simple.

    DATA: ls_t006    TYPE t006,
          lv_unit_in TYPE t006-msehi.

    IF st_t006[] IS INITIAL.
      SELECT * FROM t006 INTO TABLE st_t006.
    ENDIF.

    lv_unit_in = unit_in.

    CLEAR ev_error.

* Get SAP UOM from ISOCODE
    IF iv_get_sap_uom = abap_true.
      CLEAR ls_t006.
      READ TABLE st_t006 INTO ls_t006
      WITH KEY isocode = unit_in.
      IF sy-subrc = 0.
        lv_unit_in = ls_t006-msehi.
      ENDIF.
    ENDIF.


    CALL FUNCTION 'UNIT_CONVERSION_SIMPLE'
      EXPORTING
        input                = input
        no_type_check        = no_type_check
        round_sign           = round_sign
        unit_in              = lv_unit_in
        unit_out             = unit_out
      IMPORTING
        add_const            = add_const
        decimals             = decimals
        denominator          = denominator
        numerator            = numerator
        output               = output
      EXCEPTIONS
        conversion_not_found = 1
        division_by_zero     = 2
        input_invalid        = 3
        output_invalid       = 4
        overflow             = 5
        type_invalid         = 6
        units_missing        = 7
        unit_in_not_found    = 8
        unit_out_not_found   = 9
        OTHERS               = 10.
    IF sy-subrc <> 0.
      ev_error = abap_true.
      output = input.
    ENDIF.









  ENDMETHOD.
ENDCLASS.
