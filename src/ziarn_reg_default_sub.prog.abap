*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_REG_DEFAULT_SUB
*&---------------------------------------------------------------------*


*&---------------------------------------------------------------------*
*&      Form  EXECUTE
*&---------------------------------------------------------------------*
* Execute main process
*----------------------------------------------------------------------*
FORM execute CHANGING cv_return_code TYPE sy-subrc.


  DATA: ls_sel_params TYPE lcl_main=>ty_sel_params.

  ls_sel_params-s_idno[]  = s_idno[].
  ls_sel_params-p_test    = p_test.

  IF go_main IS NOT BOUND.
    go_main = NEW lcl_main( ls_sel_params ).
  ENDIF.

  TRY.
      go_main->process( ).

      cv_return_code = 0.

    CATCH lcx_error.
      cv_return_code = 4.

  ENDTRY.


ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  WRITE_LOG
*&---------------------------------------------------------------------*
* Write Log
*----------------------------------------------------------------------*
FORM write_log .

  IF go_main->mt_log[] IS INITIAL.
    MESSAGE 'No Hybris Code (Id Number) found to default' TYPE 'S' DISPLAY LIKE 'E'.
  ELSE.

    IF p_test EQ abap_true.
      WRITE: 5 'Test Mode is ON' COLOR 3.
    ENDIF.

    WRITE: /5 'Id No.' COLOR 4, 20 'Msg Type' COLOR 4, 30 'Message' COLOR 4.

    LOOP AT go_main->mt_log[] INTO DATA(ls_log).
      IF ls_log-msgty = 'E'.
        FORMAT COLOR 6.
        WRITE: /5 ls_log-idno, 20 ls_log-msgty, 30 ls_log-message.
        FORMAT COLOR OFF.
      ELSE.
        WRITE: /5 ls_log-idno, 20 ls_log-msgty, 30 ls_log-message.
      ENDIF.
    ENDLOOP.


  ENDIF.



ENDFORM.
