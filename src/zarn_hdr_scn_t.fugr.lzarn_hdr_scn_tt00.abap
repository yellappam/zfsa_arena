*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 18.11.2016 at 14:19:18
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_HDR_SCN_T..................................*
DATA:  BEGIN OF STATUS_ZARN_HDR_SCN_T                .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_HDR_SCN_T                .
CONTROLS: TCTRL_ZARN_HDR_SCN_T
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_HDR_SCN_T                .
TABLES: ZARN_HDR_SCN_T                 .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
