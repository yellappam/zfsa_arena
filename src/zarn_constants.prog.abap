*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZARN_CONSTANTS
*&---------------------------------------------------------------------*

CONSTANTS:

* Ranges
           BEGIN OF gc_range,
            sign_i             TYPE rsparams-sign   VALUE 'I',
            option_eq          TYPE rsparams-option VALUE 'EQ',
            option_bt          TYPE rsparams-option VALUE 'BT',
            option_cp          TYPE rsparams-option VALUE 'CP',
           END OF gc_range,

* Message
           BEGIN OF gc_message,
            error              TYPE sy-msgty        VALUE 'E',
            success            TYPE sy-msgty        VALUE 'S',
            warning            TYPE sy-msgty        VALUE 'W',
            info               TYPE sy-msgty        VALUE 'I',
            id                 TYPE sy-msgid        VALUE 'ZARENA_MSG',
           END OF gc_message,

* Error Email
           BEGIN OF gc_errmail,
             std_text_id       TYPE tdid            VALUE 'ST', "Standard Text
             std_text_name     TYPE tdobname        VALUE 'ZARN_INBOUND_ERROR_EMAIL_CONTENT', "Standard Text Name
             std_text_obj      TYPE tdobject        VALUE 'TEXT', "Standard Text Object

             hdr1              TYPE char30          VALUE 'GUID',
             hdr2              TYPE char30          VALUE 'Created On',
             hdr3              TYPE char30          VALUE 'Created At',
             hdr4              TYPE char30          VALUE 'Created By',
             hdr5              TYPE char30          VALUE 'FAN Id',
             hdr6              TYPE char30          VALUE 'Mode',
             hdr7              TYPE char30          VALUE 'Code',
             hdr8              TYPE char30          VALUE 'MC',

             hdr9              TYPE char30          VALUE 'Retail Barcode',
             hdr10             TYPE char30          VALUE 'Inner Barcode',
             hdr11             TYPE char30          VALUE 'Shipper Barcode',
             hdr12             TYPE char30          VALUE 'Vendor Name',
             hdr13             TYPE char30          VALUE 'Name',
             hdr14             TYPE char30          VALUE 'Description',
             hdr15             TYPE char30          VALUE 'FS Short Description',
             hdr16             TYPE char30          VALUE 'Brand',
             hdr17             TYPE char30          VALUE 'Sub Brand',
             hdr18             TYPE char30          VALUE 'FS Brand',
             hdr19             TYPE char30          VALUE 'FS Sub Brand',

             tab               TYPE c               VALUE cl_bcs_convert=>gc_tab,  " Tab space
             crlf              TYPE c               VALUE cl_bcs_convert=>gc_crlf, " Carriage Return and Line Feed
           END OF gc_errmail,

* AReNa Tab Typ
           BEGIN OF gc_arn_tab_typ,
             ctrl              TYPE c               VALUE 'C',  "  Control Table
             chg_cat           TYPE c               VALUE 'CC', "	Change Category Table
             cust              TYPE c               VALUE 'CZ', "	Customizing Table
             gui               TYPE c               VALUE 'G',  "  GUI Table
             map               TYPE c               VALUE 'M',  "  Mapping Table
             nat               TYPE c               VALUE 'N',  "  National Table
             reg               TYPE c               VALUE 'R',  "  Regional Table
             rules             TYPE c               VALUE 'RL', "	Rules Table
             text              TYPE c               VALUE 'T',  "  Text Table
           END OF gc_arn_tab_typ,

* National Version status
           BEGIN OF gc_nat_ver_stat,
             new              TYPE zarn_version_status VALUE 'NEW',     "  New
             chg              TYPE zarn_version_status VALUE 'CHG',     "  Change
             nch              TYPE zarn_version_status VALUE 'NCH',     "  No change
             apr              TYPE zarn_version_status VALUE 'APR',     "  Approved
             cre              TYPE zarn_version_status VALUE 'CRE',     "  Created
             rej              TYPE zarn_version_status VALUE 'REJ',     "  Rejected
             wip              TYPE zarn_version_status VALUE 'WIP',     "  Work In-Progress
             obs              TYPE zarn_version_status VALUE 'OBS',     "  Obsolete
           END OF gc_nat_ver_stat.

  CONSTANTS: gc_feature_a4(2) VALUE 'A4'.  "Online Categories and UoM Descriptions
