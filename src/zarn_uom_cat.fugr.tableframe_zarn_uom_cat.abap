*---------------------------------------------------------------------*
*    program for:   TABLEFRAME_ZARN_UOM_CAT
*   generation date: 24.05.2016 at 11:32:34 by user C90001363
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
FUNCTION TABLEFRAME_ZARN_UOM_CAT       .

  PERFORM TABLEFRAME TABLES X_HEADER X_NAMTAB DBA_SELLIST DPL_SELLIST
                            EXCL_CUA_FUNCT
                     USING  CORR_NUMBER VIEW_ACTION VIEW_NAME.

ENDFUNCTION.
