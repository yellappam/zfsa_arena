*----------------------------------------------------------------------*
***INCLUDE LZARN_INBOUNDF02.
*----------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*&      Form  BUILD_BIOORGANISM_TABLE
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->FU_S_MASTER_PRODUCT  text
*      -->FU_V_IDNO  text
*      <--FC_T_BIOORGANISM  text
*----------------------------------------------------------------------*
FORM build_bioorganism_table  USING    fu_s_master_product TYPE zmaster_products_product
                                       fu_v_idno           TYPE zarn_idno
                              CHANGING fc_t_bioorganism    TYPE ztarn_bioorganism.

* Assign GUID as late as possible to avoid unnecessarily generating value therefore
* not set here

  CLEAR fc_t_bioorganism[].

  LOOP AT fu_s_master_product-organisms-organism[] INTO DATA(ls_prod_organism).

    DATA(lt_value) =  ls_prod_organism-max_values-uom_to_value[].

    fc_t_bioorganism = VALUE #( BASE fc_t_bioorganism FOR ls_wa IN lt_value
                                  mandt     = sy-mandt
                                  idno      = fu_v_idno
                                  type      = ls_prod_organism-type
                                ( max_value = ls_wa-value
                                  uom       = ls_wa-uom )
                              ).

  ENDLOOP.

  " Because there was no proper unique identifier for the table key, all the fields
  " specified as the secondary key will be used to prevent duplicate records semantically.
  " Because the GUID is used as primary key there should never be duplicates.
  " Currently the secondary key are all other fields of the table excluding the GUID.
  SORT fc_t_bioorganism BY sk.
  DELETE ADJACENT DUPLICATES FROM fc_t_bioorganism COMPARING sk.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  BUILD_PROD_SERVING_SIZE_TABLE
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->FU_S_SERVING_SIZE  text
*      -->FU_V_IDNO  text
*      <--FC_T_SERVE_SIZE[]  text
*----------------------------------------------------------------------*
FORM build_prod_serving_size_table  USING    fu_s_serving_size  TYPE zmaster_products_serving_sizes
                                             fu_v_idno          TYPE zarn_idno
                                             fu_v_nutrient_type TYPE ZARN_NUTRIENT_TYPE
                                    CHANGING fc_t_serve_size    TYPE ztarn_serve_size.

* Assign GUID as late as possible to avoid unnecessarily generating value therefore
* not set here

  DATA(lt_data_in) = fu_s_serving_size-serving_size[].

  fc_t_serve_size = VALUE #( BASE fc_t_serve_size FOR ls_wa IN lt_data_in
                              mandt = sy-mandt
                              idno  = fu_v_idno
                            ( nutrient_type = fu_v_nutrient_type
                              serve_size    = ls_wa-uomto_value-value
                              uom           = ls_wa-uomto_value-uom )
                           ).

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  BUILD_NET_QUANTITY_TABLE
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->FU_S_MASTER_PRODUCT  text
*      -->FU_V_IDNO  text
*      <--FC_T_NET_QTY  text
*----------------------------------------------------------------------*
FORM build_net_quantity_table  USING    fu_s_master_product TYPE zmaster_products_product
                                        fu_v_idno           TYPE zarn_idno
                               CHANGING fc_t_net_qty        TYPE ztarn_net_qty.

* Assign GUID as late as possible to avoid unnecessarily generating value therefore
* not set here

  CLEAR fc_t_net_qty[].

  DATA(lt_data_in) =  fu_s_master_product-net_quantities-net_quantity[].

  fc_t_net_qty = VALUE #( BASE fc_t_net_qty FOR ls_wa IN lt_data_in
                           mandt = sy-mandt
                           idno  = fu_v_idno
                          ( quantity   = ls_wa-uomto_value-value
                            uom        = ls_wa-uomto_value-uom )
                        ).

  " Because there was no proper unique identifier for the table key, all the fields
  " specified as the secondary key will be used to prevent duplicate records semantically.
  " Because the GUID is used as primary key there should never be duplicates.
  " Currently the secondary key are all other fields of the table excluding the GUID.
  SORT fc_t_net_qty BY sk.
  DELETE ADJACENT DUPLICATES FROM fc_t_net_qty COMPARING sk.

ENDFORM.
