* regenerated at 03.03.2017 09:52:42
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZVARN_FIELD_CONFTOP.              " Global Data
  INCLUDE LZVARN_FIELD_CONFUXX.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZVARN_FIELD_CONFF...              " Subroutines
* INCLUDE LZVARN_FIELD_CONFO...              " PBO-Modules
* INCLUDE LZVARN_FIELD_CONFI...              " PAI-Modules
* INCLUDE LZVARN_FIELD_CONFE...              " Events
* INCLUDE LZVARN_FIELD_CONFP...              " Local class implement.
* INCLUDE LZVARN_FIELD_CONFT99.              " ABAP Unit tests
  INCLUDE LZVARN_FIELD_CONFF00                    . " subprograms
  INCLUDE LZVARN_FIELD_CONFI00                    . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
