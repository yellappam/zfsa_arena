*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_ARTICLE_UPDATE_SEL
*&---------------------------------------------------------------------*



TABLES: zarn_prd_version, zarn_reg_hdr.

SELECTION-SCREEN: BEGIN OF BLOCK b1 WITH FRAME TITLE text-001.
PARAMETERS: p_idno TYPE zarn_prd_version-idno,
            p_ver  TYPE zarn_prd_version-version.

SELECT-OPTIONS: s_status FOR zarn_prd_version-version_status
                                      NO INTERVALS OBLIGATORY.

SELECTION-SCREEN: SKIP.

SELECT-OPTIONS: s_frmdat FOR zarn_prd_version-changed_on
                                       NO-EXTENSION OBLIGATORY.


SELECTION-SCREEN: SKIP.

PARAMETERS: p_log    TYPE flag DEFAULT abap_true.

SELECTION-SCREEN: END OF BLOCK b1.

INITIALIZATION.
  IF s_status[] IS INITIAL.
    SELECT sign opti AS option low high
      INTO CORRESPONDING FIELDS OF TABLE s_status[]
      FROM tvarvc
      WHERE name = 'ZARN_ARTICLE_UPDATE_STATUS'
        AND type = 'S'.
  ENDIF.

  IF s_frmdat[] IS INITIAL.
    s_frmdat-sign = 'I'.
    s_frmdat-option = 'BT'.
    s_frmdat-low = sy-datum - 1.
    s_frmdat-high = sy-datum.
    APPEND s_frmdat TO s_frmdat[].
  ENDIF.







AT SELECTION-SCREEN OUTPUT.
  LOOP AT SCREEN.
    IF screen-name = 'P_VER'.
      screen-input = 0.
      screen-intensified = 1.
      MODIFY SCREEN.
    ENDIF.
  ENDLOOP.



AT SELECTION-SCREEN.

  LOOP AT SCREEN.
    IF screen-name = 'P_VER'.
      screen-input = 0.
      screen-intensified = 1.
      MODIFY SCREEN.
    ENDIF.
  ENDLOOP.

* Get Version if IDNO is entered
  IF p_idno IS NOT INITIAL.
    SELECT SINGLE current_ver INTO p_ver FROM zarn_ver_status
      WHERE idno = p_idno.
    IF sy-subrc NE 0.
      MESSAGE 'No Current Version Exist for entered IDNO' TYPE 'E'.
    ENDIF.
  ENDIF.



  IF s_frmdat[] IS NOT INITIAL.

    LOOP AT s_frmdat[] INTO s_frmdat.
      IF s_frmdat-high IS INITIAL.
        s_frmdat-high = sy-datum.
      ENDIF.

      IF s_frmdat-low IS INITIAL.
        s_frmdat-low = sy-datum - 1.
      ENDIF.

      MODIFY s_frmdat.
    ENDLOOP.


    LOOP AT s_frmdat[] INTO s_frmdat.
      IF ( s_frmdat-high - s_frmdat-low ) GE 7.
        MESSAGE 'Date Intervals should not be more than 7 days' TYPE 'E'.
      ENDIF.
    ENDLOOP.



  ENDIF.
