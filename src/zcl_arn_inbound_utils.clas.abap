class ZCL_ARN_INBOUND_UTILS definition
  public
  final
  create private .

public section.

  data MV_PROD_REG_DATA_SET type BOOLE_D read-only .

  methods PREPARE_STRUC_FOR_COMPARE
    importing
      !IV_STRUCNAME type TABNAME
    changing
      !CS_STRUCTURE type DATA .
  methods SET_FIELD_CONFIG
    importing
      !IT_FIELD_CONFIG type ZTARN_FIELD_CONFG .
  class-methods GET_INSTANCE
    returning
      value(RO_INSTANCE) type ref to ZCL_ARN_INBOUND_UTILS .
  methods SET_PROD_REG_DATA
    importing
      !IT_PRODUCT_DATA type ZTARN_PROD_DATA
      !IT_REGIONAL_DATA type ZTARN_REG_DATA .
  methods GET_PROD_REG_DATA
    exporting
      !ET_PRODUCT_DATA type ZTARN_PROD_DATA
      !ET_REGIONAL_DATA type ZTARN_REG_DATA .
protected section.
private section.

  data MT_FIELD_CONFIG type ZTARN_FIELD_CONFG .
  class-data GO_INSTANCE type ref to ZCL_ARN_INBOUND_UTILS .
  data MT_PROD_DATA type ZTARN_PROD_DATA .
  data MT_REG_DATA type ZTARN_REG_DATA .
ENDCLASS.



CLASS ZCL_ARN_INBOUND_UTILS IMPLEMENTATION.


  METHOD get_instance.

    IF go_instance IS INITIAL.
      CREATE OBJECT go_instance.
    ENDIF.

    ro_instance = go_instance.

  ENDMETHOD.


  METHOD GET_PROD_REG_DATA.
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 20/11/2019
* CHANGE No.       # Charm 9000006356; Jira CIP-148
* DESCRIPTION      # New method to return product national and regional
*                  # data from buffer.
* WHO              # C90001929, Aruna Mackenjee
*-----------------------------------------------------------------------

    et_product_data[]  = mt_prod_data[].
    et_regional_data[] = mt_reg_data[].

  ENDMETHOD.


  METHOD prepare_struc_for_compare.

    DATA: lt_dfies TYPE dfies_tab,
          ls_dfies TYPE dfies.

* Get Fields of the table
    REFRESH: lt_dfies[].
    CALL FUNCTION 'DDIF_FIELDINFO_GET'
      EXPORTING
        tabname        = iv_strucname
        langu          = sy-langu
      TABLES
        dfies_tab      = lt_dfies[]
      EXCEPTIONS
        not_found      = 1
        internal_error = 2
        OTHERS         = 3.
    IF sy-subrc <> 0.
      RETURN.
    ENDIF.

    LOOP AT lt_dfies INTO ls_dfies.

      ASSIGN COMPONENT ls_dfies-fieldname OF STRUCTURE cs_structure TO FIELD-SYMBOL(<field_val>).

      READ TABLE mt_field_config[] INTO DATA(ls_field_confg)
      WITH KEY tabname = iv_strucname
               fldname = ls_dfies-fieldname.
      IF sy-subrc NE 0.
        CLEAR <field_val>.
      ELSEIF
         ls_dfies-keyflag = abap_true
        OR ls_field_confg-version_compare EQ space.

        CLEAR <field_val>.

*>>> SUP-222 IS 2019/02 - possibility to enable case insensitive compare for specific fields
      ELSEIF ls_field_confg-ignore_case_comp = abap_true.
        <field_val> = to_upper( <field_val> ).
*<<< SUP-222 IS 2019/02 - possibility to enable case insensitive compare for specific fields

      ENDIF.
    ENDLOOP.  " LOOP AT lt_dfies INTO ls_dfies.



  ENDMETHOD.


  METHOD set_field_config.

    mt_field_config = it_field_config.

  ENDMETHOD.


  METHOD set_prod_reg_data.
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 20/11/2019
* CHANGE No.       # Charm 9000006356; Jira CIP-148
* DESCRIPTION      # New method to buffer product national and regional
*                  # data
* WHO              # C90001929, Aruna Mackenjee
*-----------------------------------------------------------------------

    mt_prod_data[] = it_product_data.
    mt_reg_data[]  = it_regional_data.

    mv_prod_reg_data_set = abap_true.

  ENDMETHOD.
ENDCLASS.
