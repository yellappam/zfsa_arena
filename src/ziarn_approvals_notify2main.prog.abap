*&---------------------------------------------------------------------*
*&  Include           ZIARN_APPROVALS_NOTIFY2MAIN
*&---------------------------------------------------------------------*


START-OF-SELECTION.

  go_approvals_notifier =
    NEW lcl_approvals_notify(
     is_selections =
      VALUE lcl_approvals_notify=>ty_selections(
       fanid      = s_fanid[]
       idno       = s_idno[]
       updated_on = s_updon[]
    )
     is_control =
      VALUE lcl_approvals_notify=>ty_control(
        consum = p_consum
        test   = p_test
      )
  ).
* Run
  go_approvals_notifier->run( ).

END-OF-SELECTION.
  go_approvals_notifier->display_log( ).
