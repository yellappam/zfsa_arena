*&---------------------------------------------------------------------*
*&  Include           ZIARN_GUI_P03
*&---------------------------------------------------------------------*

CLASS lcl_controller IMPLEMENTATION.

  METHOD constructor.
    mv_display = abap_true.
  ENDMETHOD.

  METHOD get_instance.
    IF so_singleton IS NOT BOUND.
      so_singleton = NEW #( ).
    ENDIF.
    ro_instance = so_singleton.
  ENDMETHOD.

  METHOD set_display.

    IF mv_display = iv_display.
      RETURN.
    ENDIF.

    mv_display = iv_display.
    CASE mv_display.
      WHEN abap_true.
        mv_alv_edit_mode = 0. " Display
      WHEN abap_false.
        mv_alv_edit_mode = 1. " Edit
    ENDCASE.

    me->raise_display_mode_changed( ).

  ENDMETHOD.

  METHOD raise_display_mode_changed.

    RAISE EVENT display_mode_changed
     EXPORTING
    ev_display       = mv_display
    ev_alv_edit_mode = mv_alv_edit_mode.

  ENDMETHOD.

ENDCLASS.

CLASS lcl_reg_cluster_ui IMPLEMENTATION.

  METHOD constructor.
    read_cluster_ranges( ).
    read_layout_module_control( ).
  ENDMETHOD.

  METHOD get_instance.
    IF so_singleton IS NOT BOUND.
      so_singleton = NEW #( ).
    ENDIF.
    ro_instance = so_singleton.
  ENDMETHOD.

  METHOD handle_data_changed.

    DATA: lv_desc_field TYPE lvc_fname,
          lv_desc       TYPE bezei40.

    FIELD-SYMBOLS: <lt_mod_rows> TYPE ztarn_reg_cluster_ui.

    IF me->is_cluster_change_allowed( io_data_changed = er_data_changed ) = abap_false.
      CLEAR: er_data_changed->mt_mod_cells.
      MESSAGE s148 DISPLAY LIKE 'E'.
      me->refresh( ).
      RETURN.
    ENDIF.

    ASSIGN er_data_changed->mp_mod_rows->* TO <lt_mod_rows>.

    " Note: The fields must follow certain naming convention. Check existing fields before adding new field
    LOOP AT er_data_changed->mt_mod_cells INTO DATA(ls_mod_cell).

      READ TABLE <lt_mod_rows> ASSIGNING FIELD-SYMBOL(<ls_mod_row>) INDEX ls_mod_cell-tabix.
      IF ls_mod_cell-fieldname CP 'CLUSTER*RANGE'.
        lv_desc_field = |{ ls_mod_cell-fieldname }_DESC|.
        ASSIGN COMPONENT ls_mod_cell-fieldname OF STRUCTURE <ls_mod_row> TO FIELD-SYMBOL(<lv_cluster_range>).

        lv_desc = VALUE #( mt_cluster_range[ cluster_range = <lv_cluster_range> ]-description OPTIONAL ).
        er_data_changed->modify_cell( i_row_id    = ls_mod_cell-row_id
                                      i_fieldname = lv_desc_field
                                      i_value     = lv_desc ).
      ENDIF.

    ENDLOOP.

    me->update_regional_cluster( EXPORTING iv_force_cluster_update = abap_true
                                           it_zarn_reg_banner      = gt_zarn_reg_banner
                                 CHANGING  ct_reg_cluster_ui       = gt_zarn_reg_cluster ).

  ENDMETHOD.                    "handle_data_changed

  METHOD handle_display_mode_changed.
    IF mo_alv IS BOUND.
      mo_alv->set_ready_for_input( i_ready_for_input = ev_alv_edit_mode ).
    ENDIF.
  ENDMETHOD.

  METHOD read_cluster_ranges.
    SELECT assordimval,
           vtext
      FROM zvmd_cluster_rng
      INTO TABLE @mt_cluster_range.
  ENDMETHOD.

  METHOD read_layout_module_control.

    CHECK mt_laymod_control IS INITIAL.

    SELECT sales_org,
           merchandise_cat
      FROM zmd_laymod_cntrl
      INTO TABLE @mt_laymod_control.

  ENDMETHOD.

  METHOD is_layout_module_allowed.

    " Step 1: Specific Sales Org, Merchandise Category
    READ TABLE mt_laymod_control WITH KEY sales_org = iv_sales_org
                                          merch_cat = iv_merch_cat
                                 TRANSPORTING NO FIELDS.
    IF sy-subrc = 0.
      rv_allowed = abap_true.
      RETURN.
    ENDIF.

    " Step 2: Specific Sales Org, Generic Merchandise Category
    READ TABLE mt_laymod_control WITH KEY sales_org = iv_sales_org
                                          merch_cat = ''
                                 TRANSPORTING NO FIELDS.
    IF sy-subrc = 0.
      rv_allowed = abap_true.
      RETURN.
    ENDIF.

    " Step 3: Generic Sales Org, Specific Merchandise Category
    READ TABLE mt_laymod_control WITH KEY sales_org = ''
                                          merch_cat = iv_merch_cat
                                 TRANSPORTING NO FIELDS.
    IF sy-subrc = 0.
      rv_allowed = abap_true.
      RETURN.
    ENDIF.

    " Step 4: Generic Sales Org, Generic Merchandise Category
    READ TABLE mt_laymod_control WITH KEY sales_org = ''
                                          merch_cat = ''
                                 TRANSPORTING NO FIELDS.
    IF sy-subrc = 0.
      rv_allowed = abap_true.
      RETURN.
    ENDIF.

  ENDMETHOD.

  METHOD is_cluster_change_allowed.

    IF gs_mara-matkl      IS NOT INITIAL AND
       zarn_reg_hdr-matkl IS NOT INITIAL AND
       gs_mara-matkl <> zarn_reg_hdr-matkl.
      rv_allowed = abap_false.
    ELSE.
      rv_allowed = abap_true.
    ENDIF.

  ENDMETHOD.

  METHOD update_regional_cluster.

    LOOP AT it_zarn_reg_banner INTO DATA(ls_reg_banner).
      me->align_regional_cluster( EXPORTING iv_force_cluster_update = iv_force_cluster_update
                                            is_reg_banner_ui        = ls_reg_banner
                                  CHANGING  ct_reg_cluster_ui       = ct_reg_cluster_ui ).
    ENDLOOP.
    me->refresh( ).

  ENDMETHOD.

  METHOD check_changed_data.
    CHECK mo_alv IS BOUND.

    mo_alv->check_changed_data( ).
  ENDMETHOD.

  METHOD align_regional_cluster.

    DATA: lv_cluster_range  TYPE zmd_e_cluster_range,
          lv_index          TYPE sy-tabix,
          lv_laymod_allowed TYPE flag.

    CHECK me->is_toggle_framework_active( ) = abap_true AND
          ( zarn_reg_hdr-matnr IS INITIAL OR iv_force_cluster_update = abap_true ) AND
          zarn_reg_hdr-matkl IS NOT INITIAL.

    READ TABLE ct_reg_cluster_ui ASSIGNING FIELD-SYMBOL(<ls_reg_cluster>)
                                 WITH KEY idno   = zarn_products-idno
                                          banner = is_reg_banner_ui-banner.
    IF sy-subrc = 0.
      lv_index = sy-tabix.
    ENDIF.

    " Check banner is cluster relevant
    SELECT SINGLE cluster_relevant
      FROM zarn_cluster_cfg
      INTO @DATA(lv_relevant)
      WHERE banner = @is_reg_banner_ui-banner.
    IF lv_relevant = abap_false.
      RETURN.
    ENDIF.

    DATA(ls_cluster_range) = VALUE #( mt_cluster_range[ cluster_range = is_reg_banner_ui-sstuf ] OPTIONAL ).
    IF ls_cluster_range IS NOT INITIAL.
      lv_cluster_range = ls_cluster_range-cluster_range.
    ENDIF.

    " Layout module creation is controlled by Sales Org and Merchandise Category to allow staggerred deployment. At some point,
    " all merchandise categories are eligible.
    lv_laymod_allowed = is_layout_module_allowed( iv_sales_org = is_reg_banner_ui-banner
                                                  iv_merch_cat = zarn_reg_hdr-matkl ).

    IF lv_index IS INITIAL. " New Entry
      IF lv_laymod_allowed = abap_true AND lv_cluster_range IS NOT INITIAL.
        APPEND INITIAL LINE TO ct_reg_cluster_ui ASSIGNING <ls_reg_cluster>.
        <ls_reg_cluster>-mandt          = sy-mandt.
        <ls_reg_cluster>-idno           = zarn_products-idno.
        <ls_reg_cluster>-banner         = is_reg_banner_ui-banner.
      ENDIF.
    ELSEIF lv_cluster_range IS INITIAL OR
           lv_laymod_allowed = abap_false. "Existing Entry and invalid Range or layout not allowed
      DELETE ct_reg_cluster_ui INDEX lv_index.
    ENDIF.
    IF lv_cluster_range IS INITIAL OR
       lv_laymod_allowed = abap_false.
      RETURN.
    ENDIF.

    " Default cluster range only when it is first time default as Regional cluster range can be edited independently
    IF <ls_reg_cluster>-cluster1_range IS NOT INITIAL OR
       <ls_reg_cluster>-cluster2_range IS NOT INITIAL OR
       <ls_reg_cluster>-cluster3_range IS NOT INITIAL OR
       <ls_reg_cluster>-cluster4_range IS NOT INITIAL.
      RETURN.
    ENDIF.

    <ls_reg_cluster>-cluster1_range = lv_cluster_range.
    <ls_reg_cluster>-cluster2_range = lv_cluster_range.
    <ls_reg_cluster>-cluster3_range = lv_cluster_range.
    <ls_reg_cluster>-cluster4_range = lv_cluster_range.

    <ls_reg_cluster>-cluster1_range_desc = ls_cluster_range-description.
    <ls_reg_cluster>-cluster2_range_desc = ls_cluster_range-description.
    <ls_reg_cluster>-cluster3_range_desc = ls_cluster_range-description.
    <ls_reg_cluster>-cluster4_range_desc = ls_cluster_range-description.

  ENDMETHOD.

  METHOD is_toggle_framework_active.
    TRY.
        rv_value = zcl_ftf_store_factory=>get_instance(
                                         )->get_feature( iv_store     = 'RFST'    "Dummy Value
                                                         iv_appl_area = zif_ftf_appl_area_c=>gc_arena
                                                         iv_feature   = 'AC'
                                         )->is_active( ).
      CATCH zcx_object_not_found.
        CLEAR: rv_value.
    ENDTRY.
  ENDMETHOD.

  METHOD refresh.
    IF mo_alv IS BOUND.
      mo_alv->refresh_table_display( ).
    ENDIF.
  ENDMETHOD.

  METHOD enrich_reg_cluster.

    LOOP AT ct_reg_cluster_ui ASSIGNING FIELD-SYMBOL(<ls_reg_cluster>).
      <ls_reg_cluster>-cluster1_range_desc = VALUE #( mt_cluster_range[ cluster_range = <ls_reg_cluster>-cluster1_range ]-description OPTIONAL ).
      <ls_reg_cluster>-cluster2_range_desc = VALUE #( mt_cluster_range[ cluster_range = <ls_reg_cluster>-cluster2_range ]-description OPTIONAL ).
      <ls_reg_cluster>-cluster3_range_desc = VALUE #( mt_cluster_range[ cluster_range = <ls_reg_cluster>-cluster3_range ]-description OPTIONAL ).
      <ls_reg_cluster>-cluster4_range_desc = VALUE #( mt_cluster_range[ cluster_range = <ls_reg_cluster>-cluster4_range ]-description OPTIONAL ).
    ENDLOOP.

  ENDMETHOD.

  METHOD set_alv_instance.

    mo_alv = io_alv.

  ENDMETHOD.

ENDCLASS.

CLASS lcl_reg_allergen_ui IMPLEMENTATION.

  METHOD get_instance.
    IF so_singleton IS NOT BOUND.
      so_singleton = NEW #( ).
    ENDIF.
    ro_instance = so_singleton.
  ENDMETHOD.

  METHOD handle_data_changed.
    DATA: lv_refresh TYPE flag.
    FIELD-SYMBOLS: <lt_modifield_row> TYPE ztarn_reg_allerg_ui.

    LOOP AT er_data_changed->mt_mod_cells INTO DATA(ls_mod_cells).

      ASSIGN er_data_changed->mp_mod_rows->* TO <lt_modifield_row>.

      READ TABLE <lt_modifield_row> ASSIGNING FIELD-SYMBOL(<ls_row>) INDEX ls_mod_cells-tabix.
      IF sy-subrc <> 0 OR
         <ls_row> IS INITIAL. " Insertion of a new record
        EXIT.
      ENDIF.

      READ TABLE gt_zarn_reg_allerg ASSIGNING FIELD-SYMBOL(<ls_zarn_reg_allerg>) INDEX ls_mod_cells-row_id.
      IF sy-subrc <> 0.
        CONTINUE.
      ENDIF.

      CASE ls_mod_cells-fieldname.

        WHEN gc_alg_typ_ovr. "Allergen Override

          "Default todays date if allergen type overide is entered and date is already entered
          IF <ls_zarn_reg_allerg>-allergen_type_ovr IS INITIAL AND  " old value
             <ls_row>-allergen_type_ovr IS NOT INITIAL AND  " new value
             <ls_row>-allergen_type_ovr_date IS INITIAL.

            <ls_row>-allergen_type_ovr_date = sy-datum.
            <ls_zarn_reg_allerg>-allergen_type_ovr_date = sy-datum.

            "Clear other fields if removing allergen override
          ELSEIF <ls_row>-allergen_type_ovr IS INITIAL.

            CLEAR: <ls_row>-allergen_type_ovr_date,
                   <ls_zarn_reg_allerg>-allergen_type_ovr_date,
                   <ls_row>-allergen_type_ovr_src,
                   <ls_zarn_reg_allerg>-allergen_type_ovr_src.

            CLEAR: <ls_row>-lvl_containment_ovr,
                   <ls_zarn_reg_allerg>-lvl_containment_ovr,
                   <ls_row>-lvl_containment_ovr_date,
                   <ls_zarn_reg_allerg>-lvl_containment_ovr_date,
                   <ls_row>-lvl_containment_ovr_src,
                   <ls_zarn_reg_allerg>-lvl_containment_ovr_src.
          ENDIF.

          "Refresh the description
          IF <ls_zarn_reg_allerg>-allergen_type_ovr NE <ls_row>-allergen_type_ovr .
            <ls_row>-allergen_type_ovr_desc_fsni = VALUE #( gt_zarn_allergen_t[ allergen_type = <ls_row>-allergen_type_ovr ]-fsni_allergen_desc OPTIONAL ).
            <ls_zarn_reg_allerg>-allergen_type_ovr_desc_fsni = <ls_row>-allergen_type_ovr_desc_fsni.
          ENDIF.
          lv_refresh = abap_true.

        WHEN gc_lvl_cont_ovr. "Containment Override
          IF <ls_zarn_reg_allerg>-lvl_containment_ovr IS INITIAL AND  " old value
             <ls_row>-lvl_containment_ovr IS NOT INITIAL AND  " new value
             <ls_row>-lvl_containment_ovr_date IS INITIAL.

            "Default todays date if containment overide is entered and date is already entered
            <ls_row>-lvl_containment_ovr_date = sy-datum.
            <ls_zarn_reg_allerg>-lvl_containment_ovr_date = sy-datum.

            "Clear other fields if removing containment override
          ELSEIF <ls_row>-lvl_containment_ovr IS INITIAL.
            CLEAR: <ls_row>-lvl_containment_ovr_date,
                   <ls_zarn_reg_allerg>-lvl_containment_ovr_date,
                   <ls_row>-lvl_containment_ovr_src,
                   <ls_zarn_reg_allerg>-lvl_containment_ovr_src.

          ENDIF.
          lv_refresh = abap_true.

        WHEN OTHERS.
      ENDCASE.

      gv_data_change = abap_true.
    ENDLOOP.

    IF lv_refresh = abap_true.
      me->refresh( ).
    ENDIF.

  ENDMETHOD.

  METHOD refresh.
    go_reg_allerg_alv->refresh_table_display( ).
  ENDMETHOD.

ENDCLASS.
