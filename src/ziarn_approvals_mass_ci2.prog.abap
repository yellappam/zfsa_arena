*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_APPROVALS_MASS_CI2
*&---------------------------------------------------------------------*

*&---------------------------------------------------------------------*
*&       Class (Implementation)  lcl_view
*&---------------------------------------------------------------------*
*        Text
*----------------------------------------------------------------------*
CLASS lcl_view IMPLEMENTATION.

  METHOD initialize.

*    FREE:  mo_alv_tree,
*           mo_main_container,
*           mo_dock_container.

    CLEAR: mt_cc_hdr[],
           mt_cc_desc[],
           mt_reg_hdr[],
           mt_prd_version[],
           mt_ver_data[],
           mt_approval[],
           mt_appr_ch[],
           mt_rel_matrix[],
           mt_table_mastr[],
           mt_field_confg[],

           mr_idno[],
           mr_chgcat[],
           mr_chgare[],
           mr_nstat[],
           mr_rstat[],

           "mo_alv_tree,
           "mo_main_container,
           "mo_dock_container,

           mt_fieldcat[],
           mt_output[].
  ENDMETHOD.

  METHOD get_data.

    eo_alv_tree     = mo_alv_tree.
    eo_toolbar_tree = mo_toolbar_tree.

    et_output[]     = mt_output[].

  ENDMETHOD.

  METHOD display_0100.

    IF me->mo_alv_tree IS NOT BOUND.
* Setup the tree for display
      me->setup_tree_display( ).
    ENDIF.

  ENDMETHOD.

  METHOD setup_tree_display.

    DATA: ls_hierarchy_header TYPE treev_hhdr,
          ls_variant          TYPE disvariant.


    CREATE OBJECT me->mo_dock_container
      EXPORTING
        parent = cl_gui_container=>screen0
*       ratio  = 90  " 90% yet not full-screen size
      EXCEPTIONS
        OTHERS = 6.
    IF sy-subrc NE 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

*   Full size screen for ALV Tree
    CALL METHOD me->mo_dock_container->set_extension
      EXPORTING
        extension  = 99999  " full-screen size !!!
      EXCEPTIONS
        cntl_error = 1
        OTHERS     = 2.
    IF sy-subrc NE 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.


* Create the tree
    CREATE OBJECT me->mo_alv_tree
      EXPORTING
        parent                      = me->mo_dock_container "me->mo_main_container
        node_selection_mode         = cl_gui_column_tree=>node_sel_mode_multiple
        item_selection              = abap_true
        no_html_header              = abap_true
        no_toolbar                  = space
      EXCEPTIONS
        cntl_error                  = 1
        cntl_system_error           = 2
        create_error                = 3
        lifetime_error              = 4
        illegal_node_selection_mode = 5
        failed                      = 6
        illegal_column_name         = 7
        OTHERS                      = 8.
    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

* Set the hierarchy header
    PERFORM build_hierarchy_header_100 CHANGING ls_hierarchy_header.

* Set the variant info
    PERFORM build_variant_100 CHANGING ls_variant.

* Set the fieldcatalog
    PERFORM build_fieldcatalog_100 CHANGING mt_fieldcat[].


* Set the table for first display
    me->mo_alv_tree->set_table_for_first_display(
      EXPORTING
        is_variant           = ls_variant
        i_save               = abap_true
        is_hierarchy_header  = ls_hierarchy_header
*        is_exception_field   = is_exception_field      " Exception Field
*        it_toolbar_excluding = it_toolbar_excluding    " Excluded Toolbar Standard Functions
      CHANGING
        it_outtab            = mt_output[]              " Output Table
        it_fieldcatalog      = mt_fieldcat[]            " Field Catalog
    ).


* Extend funtions of standard toolbar
    PERFORM change_toolbar USING me->mo_alv_tree
                        CHANGING me->mo_toolbar_tree.


* Build hierarchial data for tree
    PERFORM create_hierarchy_100 USING mt_cc_hdr[]
                                       mt_ver_data[]
                                       mt_cc_desc[]
                                       mt_approval[]
                                       mt_appr_ch[]
                                       mt_rel_matrix[]
                                       mt_table_mastr[]
                                       mt_field_confg[]
                                       mr_idno[]
                                       mr_chgcat[]
                                       mr_nstat[]
                                       mr_rstat[]
                                       me->mo_alv_tree
                              CHANGING mt_reg_hdr[]
                                       mt_prd_version[]
                                       mt_fieldcat[]
                                       mt_output[].
* Register Tree Events
    PERFORM register_events_100 USING me->mo_alv_tree
                                      me->mo_toolbar_tree
                                      me->mt_output[].


* Send data to frontend.
    CALL METHOD me->mo_alv_tree->frontend_update.

    CALL METHOD cl_gui_cfw=>flush
      EXCEPTIONS
        cntl_system_error = 1
        cntl_error        = 2.


  ENDMETHOD.

  METHOD refresh_tree_display.

    CALL METHOD me->mo_alv_tree->delete_all_nodes.

* Build hierarchial data for tree
    PERFORM create_hierarchy_100 USING mt_cc_hdr[]
                                       mt_ver_data[]
                                       mt_cc_desc[]
                                       mt_approval[]
                                       mt_appr_ch[]
                                       mt_rel_matrix[]
                                       mt_table_mastr[]
                                       mt_field_confg[]
                                       mr_idno[]
                                       mr_chgcat[]
                                       mr_nstat[]
                                       mr_rstat[]
                                       me->mo_alv_tree
                              CHANGING mt_reg_hdr[]
                                       mt_prd_version[]
                                       mt_fieldcat[]
                                       mt_output[].

* Send data to frontend.
    CALL METHOD me->mo_alv_tree->frontend_update.

    CALL METHOD cl_gui_cfw=>flush
      EXCEPTIONS
        cntl_system_error = 1
        cntl_error        = 2.


    gt_output[] =  mt_output[].


  ENDMETHOD.


  METHOD modify_screen_0100.
  ENDMETHOD.

  METHOD set_data.

    mt_cc_hdr[]      = it_cc_hdr[].
    mt_cc_desc[]     = it_cc_desc[].
    mt_reg_hdr[]     = it_reg_hdr[].
    mt_prd_version[] = it_prd_version[].
    mt_ver_data[]    = it_ver_data[].
    mt_approval[]    = it_approval[]..
    mt_appr_ch[]     = it_appr_ch[].
    mt_rel_matrix[]  = it_rel_matrix[].
    mt_table_mastr[] = it_table_mastr[].
    mt_field_confg[] = it_field_confg[].
    mr_idno[]        = ir_idno[].
    mr_chgcat[]      = ir_chgcat[].
    mr_chgare[]      = ir_chgare[].
    mr_nstat[]       = ir_nstat[].
    mr_rstat[]       = ir_rstat[].
  ENDMETHOD.

ENDCLASS.               "lcl_view
