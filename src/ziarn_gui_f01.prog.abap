*&---------------------------------------------------------------------*
*&  Include               ZIARN_GUI_F01
*&---------------------------------------------------------------------*
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 13.04.2018
* CHANGE No.       # Charm 9000003482; Jira CI18-554
* DESCRIPTION      # CI18-554 Recipe Management - Ingredient Changes
*                  # New regional table for Allergen Type overrides and
*                  # fields for Ingredients statement overrides in Arena.
*                  # New Ingredients Type field in the Scales block in
*                  # Arena and NONSAP tab in material master.
* WHO              # C90005557, Tessa Newman
*-----------------------------------------------------------------------
* DATE             # 06.06.2018
* CHANGE No.       # ChaRM 9000003675; JIRA CI18-506 NEW Range Status flag
* DESCRIPTION      # New Range Flag, Range Availability and Range Detail
*                  # fields added in Arena and on material master in Listing
*                  # tab and Basic Data Text tab (materiallongtext).
* WHO              # C90005557, Tessa Newman
*-----------------------------------------------------------------------
* DATE             # 06/09/2018
* CHANGE No.       # Charm #9000004244 - CR092/93 PIM schema changes
* DESCRIPTION      # New national tables contain UUID as key field so
*                  # adjusted field catalog to prevent field from being
*                  # output. Changed routine ALV_BUILD_FIELDCAT. Other
*                  # changes are:
*                  # 1. Use new table structure for organism ALV. When
*                  #    no data in new table, values from original table
*                  #    mapped to new table for display.
*                  # 2. Changed basic information screen. Moved control
*                  #    fields to last row of sub-screen to make space
*                  #    for 'net quantity' table because now ALV table
*                  #    instead of single value.
*                  #    The original field is still visible and will
*                  #    continue to display old data.
*                  # 3. Nutritional and Ingredient sub-screen changed.
*                  #    Ingredient and additive information moved to
*                  #    new sub-screen, displaying after 'Dangerous goods
*                  #    Information'. This is to make space for 'serving
*                  #    size' ALV and the new 'serving size description'
*                  #    field.
*                  # 4. Log when doing iCare was not being refreshed so
*                  #    cleared relevant entries - changed routine
*                  #    ICARE_ACTIVATION_R3.
* WHO              # C90001929, Aruna Mackenjee
*-----------------------------------------------------------------------
* DATE             # 02.11.2018
* CHANGE No.       # ChaRM 9000004437; JIRA SAPTR-147 Post Tech Refresh Issues
* DESCRIPTION      # Certain fields have lost their original sorting due
*                  # to the HANA upgrade. Added explicit sort for:
*                  # Regional PIR, Regional Banner, Regional Standard Terms
*                  # National UoM, National Vendor List Price, National PIR
* WHO              # C90012814, Brad Gorlicki
*-----------------------------------------------------------------------
* DATE             # November 2018
* CHANGE No.       # Version1 B-03199
* DESCRIPTION      # Add Online tab to Regional
* WHO              # C90007074, Jos van Gysen
*-----------------------------------------------------------------------
* DATE             # 2019/08
* CHANGE No.       # SUP-419
* DESCRIPTION      # Long texts from text editors are being cut after
*                  # 255 characters per line - fix to prevent this
*                  # inside form get_text_editor_descr
* WHO              # I90018154, Ivo Skolek
*-----------------------------------------------------------------------
* DATE             # 10.10.2019
* CHANGE No.       # SUP-1
* DESCRIPTION      # Avoid dump when reading internal table record. Also
*                  # check for IDNO when comparing the changes as existing
*                  # design has known flaw giving popup to confirm when
*                  # no changes made
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* DATE             # 25/11/2019
* CHANGE No.       # Charm 9000006356; Jira CIP-148
* DESCRIPTION      # When saving regional data call new routine to save
*                  # that includes logic to post data when there are
*                  # no outstanding approvals. Prior to this change the
*                  # posting did not occur after auto approval.
*                  # Replaced calls to routine PROCESS_REGIONAL_DATA
*                  # with 'save' with call to new routine where relevant.
* WHO              # C90001929, Aruna Mackenjee
*-----------------------------------------------------------------------
* DATE             # 25.06.2020
* CHANGE No.       # SSM-1 NW Range Policy ZARN_GUI
* DESCRIPTION      # 1. Added Regional Cluster ALV along with logic
*                  # 2. Code clean-up
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* DATE             # 03.09.2020
* CHANGE No.       # DSDSS-2385 ZARN_GUI Allows capture of carriage return in Regional Medium Descr
* DESCRIPTION      # Regional Customer Medium Description
*                     - ensure no CR characters are embedded
* WHO              # C90007074, Jos van Gysen
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&      Form  LOAD_CONTROLS
*&---------------------------------------------------------------------*
FORM load_controls USING iv_ratio TYPE ajhranp.

  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo,
    lv_ratio    TYPE i,
    lv_title    TYPE string.

* ratio for ALV worklist docking container
  lv_ratio = iv_ratio.

* Create docking container for ALV worklist on the LEFT
  CREATE OBJECT go_docking_alv
    EXPORTING
      parent = cl_gui_container=>screen0
      repid  = syst-repid
      dynnr  = gc_screen_0100
      ratio  = lv_ratio  "30     " % of total screen
    EXCEPTIONS
      OTHERS = 6.

  IF sy-subrc IS NOT INITIAL.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
               WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.

*   Create ALV grid for worklist
  CREATE OBJECT go_worklist_alv
    EXPORTING
      i_parent = go_docking_alv   "Left
    EXCEPTIONS
      OTHERS   = 5.
  IF sy-subrc IS NOT INITIAL.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
               WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.

* Call GRID
  PERFORM alv_build_fieldcat USING gc_alv_worklist  CHANGING lt_fieldcat.
* Exclude all functions not required
  PERFORM exclude_tb_functions CHANGING lt_exclude.

  PERFORM alv_build_data.

*  Set event handler
  CREATE OBJECT go_event_receiver.

  SET HANDLER:
      go_event_receiver->handle_toolbar      FOR go_worklist_alv,
      go_event_receiver->handle_user_command FOR go_worklist_alv,
      go_event_receiver->on_toolbar          FOR go_worklist_alv.

* INS Begin of Change 3174 JKH 21.03.2017
  IF gv_cr_r3_switch = abap_true AND mytab-activetab = gc_icare_tab.
    SET HANDLER: go_event_receiver->handle_data_change  FOR go_worklist_alv.

* set editable cells to ready for input
    CALL METHOD go_worklist_alv->set_ready_for_input
      EXPORTING
        i_ready_for_input = 1.

    CALL METHOD go_worklist_alv->register_edit_event
      EXPORTING
        i_event_id = cl_gui_alv_grid=>mc_evt_enter.

    CALL METHOD go_worklist_alv->register_edit_event
      EXPORTING
        i_event_id = cl_gui_alv_grid=>mc_evt_modified.
  ENDIF.  " IF gv_cr_r3_switch = abap_true
* INS End of Change 3174 JKH 21.03.2017



* Use the layout structure to aquaint additional field to ALV.
  ls_layout-stylefname = gc_celltab. "'CELLTAB'.
  ls_layout-cwidth_opt = abap_true.
  ls_layout-sel_mode   = gc_save.
  ls_layout-zebra      = abap_true.
  ls_layout-info_fname = gc_alv_rowcolor.


* INS Begin of Change 3174 JKH 13.03.2017
  IF mytab-activetab = gc_icare_tab AND gv_cr_r1_switch = abap_true.
    CLEAR lv_title.
    lv_title = |Total: { gv_worklist_cnt }, Nat Search: { gv_search_cnt }/{ gv_tot_search_cnt }|.
    lv_title = |{ lv_title }, Nat Err: { gv_error_cnt }|.
    lv_title = |{ lv_title }, Val Err: { gv_valerr_cnt }|.
    ls_layout-grid_title = lv_title.
  ENDIF.
* INS End of Change 3174 JKH 13.03.2017


  gs_variant-report    = sy-repid.
  gs_variant-username  = sy-uname.
  gs_variant-variant   = p_vari.

** display GRID 1
  CALL METHOD go_worklist_alv->set_table_for_first_display
    EXPORTING
      i_bypassing_buffer            = 'X'
      is_variant                    = gs_variant
      i_save                        = gc_save
      is_layout                     = ls_layout
      it_toolbar_excluding          = lt_exclude
    CHANGING
      it_outtab                     = gt_worklist
      it_fieldcatalog               = lt_fieldcat
    EXCEPTIONS
      invalid_parameter_combination = 1
      program_error                 = 2
      too_many_lines                = 3
      OTHERS                        = 4.
  IF sy-subrc IS NOT INITIAL.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
               WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.

  go_worklist_alv->set_toolbar_interactive( ).

ENDFORM.                    " LOAD_CONTROLS
*&---------------------------------------------------------------------*
*&      Form  GET_DATA_ICARE
*&---------------------------------------------------------------------*
FORM get_data_icare.

  TYPES:
    BEGIN OF ty_sel,
      fan_id          TYPE zarn_products-fan_id,
      idno            TYPE zarn_products-idno,
      version         TYPE zarn_products-version,
      guid            TYPE zarn_prd_version-guid,
      matnr_ni        TYPE zarn_products-matnr_ni,
      description     TYPE zarn_products-description,
      gpc_code        TYPE zarn_products-gpc_code,
      gpc_description TYPE zarn_products-gpc_description,
      gln             TYPE zarn_products-gln,
      glnd_descr      TYPE zarn_products-gln_descr,
      code            TYPE zarn_products-code,
      gtin_code       TYPE zarn_gtin_var-gtin_code,
    END OF ty_sel.


  DATA:
    lt_key       TYPE ztarn_key,
    ls_key       LIKE LINE OF lt_key,
    lt_key_sel   TYPE ztarn_key,
    lv_flag      TYPE  flag,
    lv_team_code TYPE  zarn_team_code VALUE 'CT'.

  DATA:
    ls_search_input   TYPE zhybris_search_input,
    lt_search_results TYPE STANDARD TABLE OF zhybris_results,
    lt_hybris_results TYPE SORTED TABLE OF zhybris_results WITH NON-UNIQUE KEY fan_id,
    ls_hybris_results LIKE LINE OF lt_hybris_results,
    ls_short_descr    LIKE LINE OF  gr_short_descr,
    ls_gln_descr      LIKE LINE OF  gr_short_descr,
    lt_sel            TYPE STANDARD TABLE OF ty_sel,
    ls_data           LIKE LINE OF gt_data,
    lt_messages       TYPE bal_t_msg,
    ls_message        LIKE LINE OF lt_messages,
    lv_results        TYPE i,
    ls_lfa1           TYPE lfa1.

  FIELD-SYMBOLS:
    <ls_data>           LIKE LINE OF gt_data,
    <ls_sel>            LIKE LINE OF lt_sel,
    <ls_hybris>         LIKE LINE OF lt_hybris_results,
    <ls_search_results> LIKE LINE OF lt_search_results.


* INS Begin of Change 3174 JKH 06.03.2017
* If CR Switch os ON then get data through R1 Service
  IF gv_cr_r1_switch = abap_true.
* Send request to Hybris and Read National and Regional data
    PERFORM get_data_icare_r1.
    RETURN.
  ENDIF.
* INS End of Change 3174 JKH 06.03.2017



*** Get any records from Hybris matching search criteria
  CLEAR ls_search_input.
  ls_search_input-gtin                  = s_gtin-low.
  ls_search_input-gln_description       = s_glnd-low.
  ls_search_input-gln                   = s_gln-low.
  ls_search_input-short_description     = s_descr-low.
  ls_search_input-brand_name            = s_brand-low.
  ls_search_input-fs_brand_desc         = s_fbrand-low.
  ls_search_input-vendor_article_number = s_vartn-low.

  CALL FUNCTION 'Z_HYBRIS_GET'
    EXPORTING
      hybris_search_input  = ls_search_input
    IMPORTING
      hybris_results       = lt_search_results
      messages             = lt_messages
    EXCEPTIONS
      hybris_not_available = 1
      OTHERS               = 2.

  LOOP AT lt_messages INTO ls_message WHERE msgty EQ 'E' OR msgty EQ 'A'.
    EXIT.
  ENDLOOP.
  IF sy-subrc EQ 0.
    PERFORM popup_appl_log USING lt_messages.
    RETURN.
  ELSE.
    DESCRIBE TABLE lt_search_results LINES lv_results.
    IF lv_results GE 50.
      MESSAGE i061 WITH lv_results.
    ELSE.
      MESSAGE i060 WITH lv_results.
    ENDIF.
  ENDIF.

* at the moment we have 3 description fields in ZARN_PRODUCTS
* of which FS_SHORT_DESCR_UPPER, the following logic an SELECT caters for
* case sensitive search !
*   upper case search

  CLEAR:
    gr_short_descr,
    gr_gln_descr.

  IF s_descr[]  IS NOT INITIAL OR
     s_descr    IS NOT INITIAL.
    LOOP AT s_descr.
      MOVE-CORRESPONDING s_descr TO ls_short_descr.
      TRANSLATE  ls_short_descr TO UPPER CASE.
      APPEND ls_short_descr TO gr_short_descr.
    ENDLOOP.
  ENDIF.


  IF s_glnd[] IS NOT INITIAL OR
     s_glnd IS NOT INITIAL.
*   upper case search
    LOOP AT  s_glnd.
      MOVE-CORRESPONDING  s_glnd TO ls_gln_descr.
      TRANSLATE  ls_gln_descr TO UPPER CASE.
      APPEND ls_gln_descr TO gr_gln_descr.
    ENDLOOP.
  ENDIF.

* The above function module will return data and update the AReNa tables
* it is prudent to read AReNa DB for our latest set of data
* the user requires a list of details from AReNa that are ICare'd already !


* Get any records from AReNa matching search criteria
  SELECT p~fan_id p~idno p~version r~guid
         p~matnr_ni p~description p~gpc_code p~gpc_description
         p~gln  p~gln_descr p~code g~gtin_code
    INTO TABLE lt_sel
    FROM zarn_products AS p
    INNER JOIN zarn_products_ex AS e
      ON  e~idno    EQ p~idno
      AND e~version EQ p~version
    INNER JOIN zarn_gtin_var AS g
      ON  g~idno    EQ p~idno
      AND g~version EQ p~version
    INNER JOIN zarn_pir AS v
      ON  v~idno    EQ p~idno
      AND v~version EQ p~version
    INNER JOIN zarn_ver_status AS s
      ON  s~idno        EQ p~idno
      AND s~current_ver EQ p~version
    INNER JOIN zarn_prd_version AS r
      ON  r~idno    EQ p~idno
      AND r~version EQ p~version
    WHERE g~gtin_code             IN s_gtin
    AND   v~vendor_article_number IN s_vartn
    AND   v~gln                   IN s_gln
    AND   v~gln_description_upper IN gr_gln_descr
    AND   p~fs_short_descr_upper  IN gr_short_descr
    AND   p~brand_id_upper        IN s_brand
    AND   e~fs_brand_desc_upper   IN s_fbrand.

  IF lt_sel IS NOT INITIAL.
    LOOP AT lt_sel ASSIGNING <ls_sel>.
      CLEAR ls_data.
      MOVE-CORRESPONDING <ls_sel> TO ls_data.
      APPEND ls_data TO gt_data.
      MOVE-CORRESPONDING <ls_sel> TO ls_key.
      APPEND ls_key TO lt_key.
    ENDLOOP.
  ENDIF.

*** get all National and Regional DB table data
  SORT lt_key BY idno version.
  DELETE ADJACENT DUPLICATES FROM lt_key COMPARING idno.

  IF lt_key IS INITIAL.
    RETURN.
  ENDIF.

  PERFORM read_national_data
    USING lt_key
    CHANGING gt_prod_data.

  PERFORM read_regional
    USING lt_key
    CHANGING gt_reg_data.

  PERFORM get_other_table_data USING lt_key.

* Check authorisation for I Care
  CALL FUNCTION 'Z_ARN_APPROVALS_TEAM_AUTH'
    EXPORTING
      team_code  = lv_team_code  "'CT'
    IMPORTING
      authorized = lv_flag.

  IF  lv_flag IS INITIAL.
    gv_display = abap_true.
  ENDIF.


ENDFORM.                    " GET_DATA ICARE
*&---------------------------------------------------------------------*
*&      Form  alv_build_fieldcat
*&---------------------------------------------------------------------*
FORM alv_build_fieldcat USING pv_structure CHANGING pt_fieldcat TYPE lvc_t_fcat.

  DATA ls_fcat TYPE lvc_s_fcat.
  "Supply Chain Edit Switch - This can be removed after the feature
  "goes live
  DATA: lv_edit_switch TYPE boole_d.
  CONSTANTS: gc_feature(2) VALUE 'A1'.  "Edit Feature

  CALL FUNCTION 'LVC_FIELDCATALOG_MERGE'
    EXPORTING
      i_structure_name = pv_structure "'ZRSD_ICARE_STRUC'
    CHANGING
      ct_fieldcat      = pt_fieldcat.

*   only for ALV Worklist to select processing
  IF pv_structure EQ gc_alv_worklist.
*   Add an entry for the checkbox in the fieldcatalog
    CLEAR ls_fcat.
    ls_fcat-fieldname = gc_field_chkbox. " 'CHECKBOX'.
*   Essential: declare field as checkbox and
*              mark it as editable field:
    ls_fcat-checkbox = abap_true.
*   only for ICare
    IF  mytab-activetab EQ  gc_icare_tab.
      ls_fcat-edit  = abap_true.
    ENDIF.

*   do not forget to provide texts for this extra field
    ls_fcat-coltext = TEXT-021.
*    ls_fcat-tooltip = text-f02.
*    ls_fcat-seltext = text-f03.

*   optional: set column width
    ls_fcat-outputlen = 10.
*
    APPEND ls_fcat TO pt_fieldcat.

    LOOP AT pt_fieldcat INTO ls_fcat WHERE fieldname EQ gc_gcatman
                                        OR fieldname EQ gc_rcatman
                                        OR fieldname EQ gc_alv_icon
                                        OR fieldname EQ gc_initiation
                                        OR fieldname EQ gc_fsni_icare_value
                                        OR fieldname EQ gc_alv_lock_user
                                        OR fieldname EQ 'VERSION_STATUS'
                                        OR fieldname EQ 'REG_VERSION_STATUS'
                                        OR fieldname EQ 'RELEASE_STATUS'
                                        OR fieldname EQ 'REG_RELEASE_STATUS'
                                        OR fieldname EQ 'CATALOG_VERSION'                      "++3174 JKH 06.03.2017
                                        OR fieldname EQ 'ID_TYPE'                              "++3174 JKH 06.03.2017
                                        OR fieldname EQ 'CREATION_TIME'                        "++3174 JKH 06.03.2017
                                        OR fieldname EQ 'CREATION_TIME_R1'                     "++3174 JKH 06.03.2017
                                        OR fieldname EQ 'REQUESTED_BY'                         "++3174 JKH 06.03.2017
                                        OR fieldname EQ 'REQUESTED_BY_DATE'                    "++3174 JKH 06.03.2017
                                        OR fieldname EQ 'REQUESTOR'                            "++3174 JKH 06.03.2017
                                        OR fieldname EQ 'REQUESTED_MC'                         "++3174 JKH 06.03.2017
                                        OR fieldname EQ 'REQUESTED_ON'                         "++3174 JKH 06.03.2017
                                        OR fieldname EQ 'ENRICH_SLA'.                          "++3174 JKH 06.03.2017







      CASE ls_fcat-fieldname.
        WHEN 'VERSION_STATUS'.
*          ls_fcat-coltext   = text-056.
          ls_fcat-scrtext_l = TEXT-056.
          ls_fcat-scrtext_m = TEXT-056.
          ls_fcat-scrtext_s = 'NATAprSts'.

        WHEN 'REG_VERSION_STATUS'.
*          ls_fcat-coltext   = text-057.
          ls_fcat-scrtext_l = TEXT-057.
          ls_fcat-scrtext_m = TEXT-057.
          ls_fcat-scrtext_s = 'REGAprSts'.

        WHEN 'RELEASE_STATUS'.
*          ls_fcat-coltext   = text-058.
          ls_fcat-scrtext_l = TEXT-058.
          ls_fcat-scrtext_m = TEXT-058.
          ls_fcat-scrtext_s = 'NATRelSts'.

        WHEN 'REG_RELEASE_STATUS'.
*          ls_fcat-coltext   = text-059.
          ls_fcat-scrtext_l = TEXT-059.
          ls_fcat-scrtext_m = TEXT-059.
          ls_fcat-scrtext_s = 'REGRelSts'.

        WHEN gc_gcatman.
          ls_fcat-coltext   = TEXT-002.
          ls_fcat-scrtext_l = TEXT-003.
          ls_fcat-scrtext_m = TEXT-004.
          ls_fcat-scrtext_s = TEXT-005.
*         only for ICare
          IF  mytab-activetab EQ  gc_icare_tab.
            ls_fcat-edit     = abap_true.
          ENDIF.

        WHEN gc_rcatman.
          ls_fcat-coltext   = TEXT-006.
          ls_fcat-scrtext_l = TEXT-007.
          ls_fcat-scrtext_m = TEXT-008.
          ls_fcat-scrtext_s = TEXT-009.

*         only for ICare
          IF  mytab-activetab EQ  gc_icare_tab.
            ls_fcat-edit     = abap_true.
          ENDIF.

        WHEN gc_initiation.
*         only for ICare
          IF  mytab-activetab EQ  gc_icare_tab.
            ls_fcat-edit     = abap_true.
          ENDIF.

        WHEN gc_alv_icon.
          ls_fcat-coltext   = TEXT-014.
          ls_fcat-scrtext_l = TEXT-014.
          ls_fcat-scrtext_m = TEXT-014.
          ls_fcat-scrtext_s = TEXT-014.
          ls_fcat-icon = abap_true.

        WHEN gc_fsni_icare_value OR
             gc_alv_lock_user.


        WHEN 'CATALOG_VERSION' OR 'ID_TYPE' OR 'CREATION_TIME'.
          IF gv_cr_r1_switch IS INITIAL.
            ls_fcat-tech = abap_true.
          ENDIF.

        WHEN 'CREATION_TIME_R1'.
          ls_fcat-tech = abap_true.

        WHEN 'REQUESTED_BY' OR 'REQUESTED_BY_DATE' OR 'REQUESTOR' OR 'REQUESTED_MC'.
          IF gv_cr_r3_switch IS INITIAL.
            ls_fcat-tech = abap_true.
          ENDIF.
*         only for ICare
          IF  mytab-activetab EQ  gc_icare_tab.
            ls_fcat-edit     = abap_true.
          ENDIF.

        WHEN 'REQUESTED_ON' OR 'ENRICH_SLA'.
          IF gv_cr_r3_switch IS INITIAL.
            ls_fcat-tech = abap_true.
          ENDIF.


      ENDCASE.
      MODIFY pt_fieldcat FROM ls_fcat.
*
    ENDLOOP.

  ENDIF.

*** for all tables
  LOOP AT pt_fieldcat INTO ls_fcat WHERE fieldname = gc_idno
                                      OR fieldname = gc_version
                                      OR fieldname = gc_fsni_icare_value
                                      OR fieldname = gc_alv_rowcolor
                                      OR fieldname = gc_guid
                                      OR fieldname = gc_celltab
                                      OR domname   = gc_uuid.
    ls_fcat-no_out = abap_true.
    ls_fcat-edit = abap_false.
    MODIFY pt_fieldcat FROM ls_fcat.
*
  ENDLOOP.

* Individual field settings **** Common processsing *****
  IF pv_structure EQ gc_alv_nat_uom OR
     pv_structure EQ gc_alv_reg_uom OR
     pv_structure EQ gc_alv_reg_rb  OR
     pv_structure EQ gc_alv_reg_pir OR
     pv_structure EQ gc_alv_reg_gtn OR
     pv_structure EQ gc_alv_reg_hsno OR
     pv_structure EQ gc_alv_reg_sc.           "9000004661 - new SC tab

    LOOP AT pt_fieldcat INTO ls_fcat.

      CASE ls_fcat-fieldname.

        WHEN  gc_catman.
          ls_fcat-coltext   = TEXT-041.
          ls_fcat-scrtext_l = TEXT-041.
          ls_fcat-scrtext_m = TEXT-041.
          ls_fcat-scrtext_s = TEXT-041.

        WHEN gc_unit_base    OR
             gc_unit_po      OR
             gc_unit_sales   OR
*             gc_po_eina      OR
*             gc_po_eine      OR
             gc_unit_issue  .
*             gc_norm_vend.

*         radiobutton behaviour for a Checkbox using hotspot
          IF gv_display IS INITIAL.
            ls_fcat-hotspot   = abap_true.               "Interactive Handling
          ENDIF.
          ls_fcat-emphasize = gc_alv_emphasize.
          ls_fcat-checkbox  = abap_true.

          IF  pv_structure      EQ gc_alv_reg_rb AND
              ls_fcat-fieldname EQ gc_unit_sales.  " Remove
            CLEAR: ls_fcat-emphasize,
                   ls_fcat-checkbox,
                   ls_fcat-hotspot.
          ENDIF.

        WHEN gc_gtin_current OR
             gc_bunit        OR
             gc_cunit        OR
             gc_dunit        OR
             gc_iunit        OR
             gc_po_eina      OR
             gc_po_eine      OR
             gc_po_unit      OR
             gc_preq         OR
             gc_pnsflag      OR
             gc_zero_pr      OR
             gc_dele_ind     OR
             gc_main_gtin    OR
             gc_norm_vend.

          ls_fcat-emphasize = gc_alv_emphasize.
          ls_fcat-checkbox = abap_true.

          IF pv_structure EQ gc_alv_reg_pir
            AND ls_fcat-fieldname = gc_dele_ind.

*            ls_fcat-coltext   = text-054.
            ls_fcat-scrtext_l = TEXT-055.
            ls_fcat-scrtext_m = TEXT-054.
            ls_fcat-scrtext_s = TEXT-054.
          ENDIF.

*         Banner only !!
          IF  pv_structure      EQ gc_alv_reg_rb.

            IF ls_fcat-fieldname EQ gc_preq.        " PRERF data element does not have heading!
              ls_fcat-coltext   = TEXT-036.
            ENDIF.
          ENDIF.

        WHEN gc_gtin_multi.
          ls_fcat-coltext   = TEXT-011.
          ls_fcat-scrtext_l = TEXT-012.
          ls_fcat-scrtext_m = TEXT-011.
          ls_fcat-scrtext_s = TEXT-013.
          ls_fcat-hotspot   = abap_true.

        WHEN gc_uom_code.
          ls_fcat-key     = abap_true.
          ls_fcat-key_sel = abap_true.

        WHEN gc_source_uni OR
             gc_prodh      OR
             gc_source_lni.
          ls_fcat-f4availabl = abap_true.

        WHEN gc_sstuf.
          ls_fcat-coltext   = TEXT-051.
          ls_fcat-scrtext_l = TEXT-051.
          ls_fcat-scrtext_m = TEXT-051.
          ls_fcat-scrtext_s = TEXT-051.

        WHEN gc_ktgrm.
          ls_fcat-coltext   = TEXT-052.
          ls_fcat-scrtext_l = TEXT-052.
          ls_fcat-scrtext_m = TEXT-052.
          ls_fcat-scrtext_s = TEXT-052.

        WHEN gc_prerf.
          ls_fcat-coltext   = TEXT-053.
          ls_fcat-scrtext_l = TEXT-053.
          ls_fcat-scrtext_m = TEXT-053.
          ls_fcat-scrtext_s = TEXT-053.

        WHEN gc_vtext_sstuf
          OR gc_vtext_ktgrm
          OR gc_vtext_banner
          OR gc_text_source_uni
          OR gc_text_source_lni
          OR gc_name_zzcatman
          OR gc_vtext_prodh.
          ls_fcat-edit = abap_false.

        WHEN gc_cat_seqno OR gc_seqno.
          IF pv_structure EQ gc_alv_reg_gtn.
            ls_fcat-tech = abap_true.
            ls_fcat-edit = abap_false.
          ENDIF.

          IF pv_structure EQ gc_alv_reg_hsno.
            ls_fcat-tech = abap_true.
          ENDIF.



        WHEN gc_nat_ean_flag.
          IF pv_structure EQ gc_alv_reg_gtn.
            ls_fcat-edit = abap_false.

            IF gv_display IS INITIAL.
              ls_fcat-tech = abap_true.
            ENDIF.
          ENDIF.

        WHEN gc_vmsta OR gc_vmstd.
          IF pv_structure EQ gc_alv_reg_rb.
            ">>>IS 2019/02 LRA 3b - fields used to be invisible
            "now changing to display only

            "ls_fcat-tech = abap_true.
            ls_fcat-edit = abap_false.
            "<<<IS 2019/02 LRA 3b
          ENDIF.

        WHEN gc_obs_ind.             "IR5061015 JKH 09.09.2016
          ls_fcat-col_pos = 1.
          ls_fcat-edit = abap_false.

        WHEN gc_hsno_code.
          ls_fcat-outputlen = 30.

        WHEN gc_pbs_code.
          ls_fcat-scrtext_s  = 'PBS'.
          ls_fcat-f4availabl = abap_true.
          ls_fcat-checktable = 'ZMD_PAY_BY_SCAN'.

        WHEN gc_pbs_name.
          ls_fcat-scrtext_s  = 'PBS Desc'.
          ls_fcat-scrtext_m  = 'PBS Description'.
          ls_fcat-scrtext_l  = 'Pay By Scan Description'.
          ls_fcat-edit = abap_false.

      ENDCASE.

*     modify table
      MODIFY pt_fieldcat FROM ls_fcat.
    ENDLOOP.
  ENDIF.


** Regional UOM only individual Table field catalog
  IF pv_structure EQ gc_alv_reg_uom.
    LOOP AT pt_fieldcat INTO ls_fcat.

      IF ls_fcat-fieldname EQ gc_uom_meins.
        ls_fcat-edit = abap_true.
        ls_fcat-f4availabl = abap_true.
*       modify table
        MODIFY pt_fieldcat FROM ls_fcat.
      ENDIF.

      IF ls_fcat-fieldname EQ gc_uom_l_meins.
        ls_fcat-edit = abap_false.
        ls_fcat-f4availabl = abap_true.
*       modify table
        MODIFY pt_fieldcat FROM ls_fcat.
      ENDIF.



      IF ls_fcat-fieldname EQ gc_pim_uom_code  OR
         ls_fcat-fieldname EQ gc_hyb_code      OR
         ls_fcat-fieldname EQ gc_l_uom         OR
         ls_fcat-fieldname EQ gc_lc_uom.

        ls_fcat-no_out = abap_true.

        IF ls_fcat-fieldname EQ gc_hyb_code.
          ls_fcat-f4availabl = abap_true.
        ENDIF.

        MODIFY pt_fieldcat FROM ls_fcat.
        CONTINUE.
      ENDIF.

    ENDLOOP.
  ENDIF.

* Supply Chain only individual Table field catalog "9000004661
* Header names are to be changed for familiarity with SC team
* However the underlying structure is almost identical to Basic Article UOM ALV
  IF pv_structure EQ gc_alv_reg_sc.
    LOOP AT pt_fieldcat INTO ls_fcat.

      CASE ls_fcat-fieldname.
        WHEN 'NET_WEIGHT_VALUE'.
          ls_fcat-reptext    = 'Net Weight'.
          ls_fcat-scrtext_s  = 'Net Weight'.
          ls_fcat-scrtext_m  = 'Net Weight'.
          ls_fcat-scrtext_l  = 'Net Weight'.
        WHEN 'GROSS_WEIGHT_VALUE'.
          ls_fcat-reptext    = 'Gross Weight'.
          ls_fcat-scrtext_s  = 'Gross Weight'.
          ls_fcat-scrtext_m  = 'Gross Weight'.
          ls_fcat-scrtext_l  = 'Gross Weight'.
        WHEN 'WEIGHT_UOM'.
          ls_fcat-reptext    = 'Wt'.
          ls_fcat-scrtext_s  = 'Wt'.
          ls_fcat-scrtext_m  = 'Wt UoM'.
          ls_fcat-scrtext_l  = 'Weight UoM'.
        WHEN 'HEIGHT_VALUE'.
          ls_fcat-reptext    = 'Height'.
          ls_fcat-scrtext_s  = 'Height'.
          ls_fcat-scrtext_m  = 'Height'.
          ls_fcat-scrtext_l  = 'Height'.
        WHEN 'WIDTH_VALUE'.
          ls_fcat-reptext    = 'Width'.
          ls_fcat-scrtext_s  = 'Width'.
          ls_fcat-scrtext_m  = 'Width'.
          ls_fcat-scrtext_l  = 'Width'.
        WHEN 'DEPTH_VALUE'.
          ls_fcat-reptext    = 'Length'.
          ls_fcat-scrtext_s  = 'Length'.
          ls_fcat-scrtext_m  = 'Length'.
          ls_fcat-scrtext_l  = 'Length'.
        WHEN 'DIMENSION_UOM'.
          ls_fcat-reptext  = 'Unit'.
          ls_fcat-scrtext_s  = 'Unit'.
          ls_fcat-scrtext_m  = 'Unit'.
          ls_fcat-scrtext_l  = 'Dimension UoM'.
      ENDCASE.

      IF ls_fcat-fieldname EQ 'NET_WEIGHT_VALUE'    OR
         ls_fcat-fieldname EQ 'GROSS_WEIGHT_VALUE'  OR
         ls_fcat-fieldname EQ 'DEPTH_VALUE'         OR
         ls_fcat-fieldname EQ 'WIDTH_VALUE'         OR
         ls_fcat-fieldname EQ 'HEIGHT_VALUE'.
        "Only weight, height, depth should be editable fields
        TRY.
            lv_edit_switch = zcl_ftf_store_factory=>get_instance(
                                             )->get_feature( iv_store     = 'RFST'    "Dummy Value
                                                             iv_appl_area = zif_ftf_appl_area_c=>gc_arena
                                                             iv_feature   = gc_feature
                                             )->is_active( ).
          CATCH zcx_object_not_found.
        ENDTRY.
        IF lv_edit_switch = abap_true.
          ls_fcat-edit = abap_true.
        ENDIF.
      ELSE.
        "Make the rest non-editable
        ls_fcat-edit = abap_false.
      ENDIF.
      MODIFY pt_fieldcat FROM ls_fcat.

    ENDLOOP.
  ENDIF.


** Regional PIR only individual Table field catalog
  IF pv_structure EQ gc_alv_reg_pir.

    LOOP AT pt_fieldcat INTO ls_fcat.

      IF ls_fcat-fieldname EQ gc_po_eina    OR
         ls_fcat-fieldname EQ gc_alv_po_grp OR
         ls_fcat-fieldname EQ gc_po_eine    OR
         ls_fcat-fieldname EQ gc_po_ekkol   OR
         ls_fcat-fieldname EQ gc_norm_vend  OR
         ls_fcat-fieldname EQ gc_alv_lifnr  OR
         ls_fcat-fieldname EQ gc_alv_uebto  OR
         ls_fcat-fieldname EQ gc_alv_lifab  OR
         ls_fcat-fieldname EQ gc_alv_lifbi  OR
         ls_fcat-fieldname EQ gc_alv_untto OR
         ls_fcat-fieldname EQ gc_esokz OR
         ls_fcat-fieldname EQ gc_minbm OR
         ls_fcat-fieldname EQ gc_norbm OR
         ls_fcat-fieldname EQ gc_vabme OR
         ls_fcat-fieldname EQ gc_mwskz OR
         ls_fcat-fieldname EQ gc_aplfz OR
         ls_fcat-fieldname EQ gc_allow_zero_price OR
         ls_fcat-fieldname EQ gc_loekz.

        ls_fcat-edit = abap_true.
*       modify table
        MODIFY pt_fieldcat FROM ls_fcat.

      ELSE.
        ls_fcat-edit = abap_false.

*       modify table
        MODIFY pt_fieldcat FROM ls_fcat.
      ENDIF.
    ENDLOOP.
  ENDIF.

* Regional Banner
  IF pv_structure EQ gc_alv_reg_rb.

    LOOP AT pt_fieldcat INTO ls_fcat.

      IF ls_fcat-fieldname EQ gc_vtext_sstuf        OR
         ls_fcat-fieldname EQ gc_vtext_ktgrm        OR
         ls_fcat-fieldname EQ gc_vtext_banner       OR
         ls_fcat-fieldname EQ gc_text_source_uni    OR
         ls_fcat-fieldname EQ gc_text_source_lni    OR
         ls_fcat-fieldname EQ gc_name_zzcatman      OR
         ls_fcat-fieldname EQ gc_vtext_prodh        OR
         ls_fcat-fieldname EQ gc_banner             OR
         ls_fcat-fieldname EQ gc_text_online_status OR  "+ONLD440
         ls_fcat-fieldname EQ gc_pbs_name           OR
         ls_fcat-fieldname EQ gc_vmsta              OR "LRA 3b
         ls_fcat-fieldname EQ gc_vmstd.                "LRA 3b

        ls_fcat-edit = abap_false.
*       modify table
        MODIFY pt_fieldcat FROM ls_fcat.

      ELSE.
        ls_fcat-edit = abap_true.

*       modify table
        MODIFY pt_fieldcat FROM ls_fcat.
      ENDIF.
    ENDLOOP.

  ENDIF.

* Allergen and Ingredients Regional Overrides
  IF pv_structure EQ gc_alv_reg_allerg_ui.
    LOOP AT pt_fieldcat INTO ls_fcat.
      IF ls_fcat-fieldname EQ gc_alg_typ_ovr OR
         ls_fcat-fieldname EQ gc_alg_typ_ovr_src OR
         ls_fcat-fieldname EQ gc_alg_typ_ovr_date OR
         ls_fcat-fieldname EQ gc_lvl_cont_ovr OR
         ls_fcat-fieldname EQ gc_lvl_cont_ovr_src OR
         ls_fcat-fieldname EQ gc_lvl_cont_ovr_date.

        ls_fcat-edit = abap_true.
*        ls_fcat-f4availabl = abap_true.

        MODIFY pt_fieldcat FROM ls_fcat.
      ENDIF.
    ENDLOOP.
  ENDIF.


** Additional Info...
  IF pv_structure EQ gc_alv_nat_addit_info.

    LOOP AT pt_fieldcat INTO ls_fcat.

      IF ls_fcat-fieldname EQ 'ADDIT_TYPE'.
        ls_fcat-outputlen = 34.
*        ls_fcat-fix_column = 'X'.
*       modify table
        MODIFY pt_fieldcat FROM ls_fcat.
      ELSEIF ls_fcat-fieldname EQ 'UNIFORM_RESOURCE_IDENT1' OR
        ls_fcat-fieldname EQ 'UNIFORM_RESOURCE_IDENT2'.
        ls_fcat-outputlen = 92.
*       modify table
        MODIFY pt_fieldcat FROM ls_fcat.
      ELSEIF ls_fcat-fieldname EQ 'FILE_EFF_START_DATETIME'.
        ls_fcat-outputlen = 18.
*       modify table
        MODIFY pt_fieldcat FROM ls_fcat.
      ENDIF.
    ENDLOOP.
  ENDIF.

** HSNO
  IF pv_structure EQ gc_alv_nat_hsno.

    LOOP AT pt_fieldcat INTO ls_fcat.

      IF ls_fcat-fieldname EQ 'HSNO_CODE_TEXT'.
        ls_fcat-outputlen = 50.
*       modify table
        MODIFY pt_fieldcat FROM ls_fcat.
      ELSEIF ls_fcat-fieldname EQ 'HSNO_CAT'.
        ls_fcat-outputlen = 22.
*       modify table
        MODIFY pt_fieldcat FROM ls_fcat.

      ENDIF.
    ENDLOOP.
  ENDIF.

** COMM/ CHANNEL
  IF pv_structure EQ gc_alv_nat_comm_ch.

    LOOP AT pt_fieldcat INTO ls_fcat.

      IF ls_fcat-fieldname EQ 'COMM_CH_TYPE'.
        ls_fcat-outputlen = 10.
*       modify table
        MODIFY pt_fieldcat FROM ls_fcat.

      ELSEIF ls_fcat-fieldname EQ 'COMM_CH_VALUE'
        OR ls_fcat-fieldname EQ 'COMM_CH_NUMBER'.
        ls_fcat-outputlen = 25.
*       modify table
        MODIFY pt_fieldcat FROM ls_fcat.

      ENDIF.
    ENDLOOP.
  ENDIF.

** Instruction handling
  IF pv_structure EQ gc_alv_nat_ins_code.

    LOOP AT pt_fieldcat INTO ls_fcat.

      IF ls_fcat-fieldname EQ 'HANDLING_INS_CODE'.
        ls_fcat-outputlen = 8.
*       modify table
        MODIFY pt_fieldcat FROM ls_fcat.

      ENDIF.
    ENDLOOP.
  ENDIF.

** NUTRITIONAL_CLAIMS
  IF pv_structure EQ gc_alv_nat_nutri_clm
    OR pv_structure EQ gc_alv_nat_nutri_str.

    LOOP AT pt_fieldcat INTO ls_fcat.

      IF ls_fcat-fieldname EQ 'NUTRITIONAL_CLAIMS'
        OR ls_fcat-fieldname EQ 'NUTRITIONAL_CLAIMS_ELEM'
        OR ls_fcat-fieldname EQ 'NUTRIENT'
        OR ls_fcat-fieldname EQ 'HEALTH_STAR_VALUES'.

        ls_fcat-outputlen = 31.
*       modify table
        MODIFY pt_fieldcat FROM ls_fcat.

      ENDIF.
    ENDLOOP.
  ENDIF.

** Serving size
  IF pv_structure = gc_alv_nat_serve_size.

    LOOP AT pt_fieldcat ASSIGNING FIELD-SYMBOL(<ls_fcat>).

      CASE <ls_fcat>-fieldname.
        WHEN 'NUTRIENT_TYPE'.
          " Expect that serving size is per product but schema seems to
          " hold data at nutrient type so have included in table but will
          " output data per product - unique records based on serve size
          " and uom will be displayed.
          <ls_fcat>-no_out = abap_true.
          <ls_fcat>-edit = abap_false.

        WHEN 'SERVE_SIZE'
          OR 'UOM'.
          <ls_fcat>-coltext = <ls_fcat>-scrtext_m.

        WHEN OTHERS.
      ENDCASE.

    ENDLOOP.

  ENDIF.

  IF pv_structure EQ gc_alv_nat_uom.
    IF gv_feature_a4_enabled EQ abap_false.
      DELETE pt_fieldcat WHERE fieldname EQ 'NET_CONTENT_SHORT_DESC'.
      DELETE pt_fieldcat WHERE fieldname EQ 'FSCUST_PROD_NAME40'.
      DELETE pt_fieldcat WHERE fieldname EQ 'FSCUST_PROD_NAME80'.
      DELETE pt_fieldcat WHERE fieldname EQ 'FSCUST_PROD_NAME255'.
      DELETE pt_fieldcat WHERE fieldname EQ 'PKGING_RCYCLING_SCHEME_CODE'.
      DELETE pt_fieldcat WHERE fieldname EQ 'PKGING_RCYCLING_PROCESS_TYPE_C'.
      DELETE pt_fieldcat WHERE fieldname EQ 'SUSTAINABILITY_FEATURE_CODE'.
    ENDIF.
  ENDIF.
  IF pv_structure EQ gc_alv_reg_uom.
    IF gv_feature_a4_enabled EQ abap_false.
      DELETE pt_fieldcat WHERE fieldname EQ 'NET_CONTENT_SHORT_DESC'.
      DELETE pt_fieldcat WHERE fieldname EQ 'FSCUST_PROD_NAME40'.
      DELETE pt_fieldcat WHERE fieldname EQ 'FSCUST_PROD_NAME80'.
      DELETE pt_fieldcat WHERE fieldname EQ 'FSCUST_PROD_NAME255'.
    ENDIF.
  ENDIF.

  IF pv_structure EQ gc_alv_nat_benefits.
    LOOP AT pt_fieldcat INTO ls_fcat.

      IF ls_fcat-fieldname EQ 'BENEFIT_NO'.
        ls_fcat-no_out = abap_true.
*       modify table
        MODIFY pt_fieldcat FROM ls_fcat.
      ENDIF.
    ENDLOOP.
  ENDIF.

ENDFORM.                    "alv_build_fieldcat
*&---------------------------------------------------------------------*
*&      Form  EXCLUDE_TB_FUNCTIONS
*&---------------------------------------------------------------------*

FORM exclude_tb_functions CHANGING pt_exclude TYPE ui_functions.

  DATA ls_exclude TYPE ui_func.

  ls_exclude = cl_gui_alv_grid=>mc_fc_info.
  APPEND ls_exclude TO pt_exclude.
  ls_exclude = cl_gui_alv_grid=>mc_fc_refresh.
  APPEND ls_exclude TO pt_exclude.
  ls_exclude = cl_gui_alv_grid=>mc_fc_graph.
  APPEND ls_exclude TO pt_exclude.
  ls_exclude = cl_gui_alv_grid=>mc_fc_check.
  APPEND ls_exclude TO pt_exclude.
  ls_exclude = cl_gui_alv_grid=>mc_fc_views.
  APPEND ls_exclude TO pt_exclude.
  ls_exclude = cl_gui_alv_grid=>mc_mb_sum.
  APPEND ls_exclude TO pt_exclude.
  ls_exclude = cl_gui_alv_grid=>mc_fc_subtot.
  APPEND ls_exclude TO pt_exclude.
  ls_exclude = cl_gui_alv_grid=>mc_fc_sum.
  APPEND ls_exclude TO pt_exclude.
  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_copy_row.
  APPEND ls_exclude TO pt_exclude.
  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_delete_row.
  APPEND ls_exclude TO pt_exclude.
  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_append_row.
  APPEND ls_exclude TO pt_exclude.
  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_insert_row.
  APPEND ls_exclude TO pt_exclude.
  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_move_row.
  APPEND ls_exclude TO pt_exclude.
  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_copy.
  APPEND ls_exclude TO pt_exclude.
  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_cut.
  APPEND ls_exclude TO pt_exclude.
  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_paste.
  APPEND ls_exclude TO pt_exclude.
  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_paste_new_row.
  APPEND ls_exclude TO pt_exclude.
  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_undo.
  APPEND ls_exclude TO pt_exclude.



ENDFORM.                               " EXCLUDE_TB_FUNCTIONS
*&---------------------------------------------------------------------*
*&      Form  alv_build_data
*&---------------------------------------------------------------------*

FORM alv_build_data .

  TYPES:
    BEGIN OF ty_wgbez,
      matkl TYPE matkl,
      wgbez TYPE wgbez,
    END OF ty_wgbez.

  DATA:
    ls_celltab TYPE lvc_s_styl,
    lt_celltab TYPE lvc_t_styl,
    ls_data    LIKE LINE OF gt_data,
    l_index    TYPE i,
    lt_wgbez   TYPE SORTED TABLE OF ty_wgbez WITH NON-UNIQUE KEY matkl,
    ls_wgbez   TYPE ty_wgbez.

  IF gt_worklist[] IS NOT INITIAL.
    SELECT matkl wgbez INTO TABLE lt_wgbez FROM t023t
      FOR ALL ENTRIES IN gt_worklist[]
      WHERE spras = sy-langu
      AND matkl = gt_worklist-nat_mc_code.
  ENDIF.

  LOOP AT gt_worklist INTO gs_worklist.
    l_index = sy-tabix.
    REFRESH lt_celltab.



**    assign a style for each row of your checkbox column.
    IF gs_worklist-checkbox = abap_true        OR                                                         "++3174 JKH 15.03.2017
       gs_worklist-rowcolor = gc_alv_locked    OR
       ( gs_worklist-catalog_version = gc_staged AND gv_cr_r3_switch NE abap_true ) OR                    "++3174 JKH 15.03.2017
       gv_display IS NOT INITIAL.
      ls_celltab-style = cl_gui_alv_grid=>mc_style_disabled.
    ELSE.
      ls_celltab-style = cl_gui_alv_grid=>mc_style_enabled.
    ENDIF.

*   Checkbox for I Care
    ls_celltab-fieldname = gc_field_chkbox. " 'CHECKBOX'.
    INSERT ls_celltab INTO TABLE lt_celltab.




    IF ( gs_worklist-checkbox = abap_true AND gs_worklist-id_type NE 'R' )   OR                 "++3174 JKH 15.03.2017
       gs_worklist-rowcolor = gc_alv_locked    OR
       ( gs_worklist-catalog_version = gc_staged AND gv_cr_r3_switch NE abap_true ) OR          "++3174 JKH 15.03.2017
       gv_display IS NOT INITIAL.
      ls_celltab-style = cl_gui_alv_grid=>mc_style_disabled.
    ELSE.

      IF gs_worklist-id_type EQ 'R' AND gv_rereq_auth EQ abap_true.
        ls_celltab-style = cl_gui_alv_grid=>mc_style_enabled.
      ELSEIF gs_worklist-id_type NE 'R'.
        ls_celltab-style = cl_gui_alv_grid=>mc_style_enabled.
      ELSE.
        ls_celltab-style = cl_gui_alv_grid=>mc_style_disabled.
      ENDIF.

    ENDIF.

*   Gilmours cat manager
    ls_celltab-fieldname = gc_gcatman.
    INSERT ls_celltab INTO TABLE lt_celltab.
*   Retail cat manager
    ls_celltab-fieldname = gc_rcatman.
    INSERT ls_celltab INTO TABLE lt_celltab.
*   Initiation
    ls_celltab-fieldname = gc_initiation.
    INSERT ls_celltab INTO TABLE lt_celltab.



* INS Begin of Change 3174 JKH 16.03.2017
    IF gv_cr_r3_switch = abap_true.

      IF gs_worklist-catalog_version = gc_staged AND gv_display IS INITIAL.

        IF gs_worklist-id_type EQ 'R' AND gv_rereq_auth EQ abap_true.
          ls_celltab-style = cl_gui_alv_grid=>mc_style_enabled.
        ELSEIF gs_worklist-id_type NE 'R'.
          ls_celltab-style = cl_gui_alv_grid=>mc_style_enabled.
        ELSE.
          ls_celltab-style = cl_gui_alv_grid=>mc_style_disabled.
        ENDIF.

      ELSE.
        ls_celltab-style = cl_gui_alv_grid=>mc_style_disabled.
      ENDIF.

* Requested By
      ls_celltab-fieldname = 'REQUESTED_BY'.
      INSERT ls_celltab INTO TABLE lt_celltab.
* Requested By Date
      ls_celltab-fieldname = 'REQUESTED_BY_DATE'.
      INSERT ls_celltab INTO TABLE lt_celltab.
* RequestOr
      ls_celltab-fieldname = 'REQUESTOR'.
      INSERT ls_celltab INTO TABLE lt_celltab.
* Requested MC
      ls_celltab-fieldname = 'REQUESTED_MC'.
      INSERT ls_celltab INTO TABLE lt_celltab.

    ENDIF. " IF gv_cr_r3_switch = abap_true
* INS End of Change 3174 JKH 16.03.2017


*    INSERT LINES OF lt_celltab INTO TABLE gs_worklist-celltab.
    gs_worklist-celltab[] = lt_celltab[].


    CLEAR ls_wgbez.
    READ TABLE lt_wgbez INTO ls_wgbez WITH TABLE KEY matkl = gs_worklist-nat_mc_code.
    IF sy-subrc EQ 0.
      gs_worklist-wgbez = ls_wgbez-wgbez.
    ENDIF.

    MODIFY  gt_worklist FROM gs_worklist INDEX l_index.
  ENDLOOP.

ENDFORM.                    " alv_build_data
*&---------------------------------------------------------------------*
*&      Form  alv_copy_article_details
*&---------------------------------------------------------------------*

FORM alv_copy_article_details  USING    pt_rows TYPE lvc_t_row
                           CHANGING cs_reg_data TYPE zsarn_reg_data.

  CONSTANTS:
    lc_field_name TYPE fieldname VALUE 'MATNR_NI',
    lc_table_name TYPE tabname   VALUE 'ZARN_PRODUCTS'.

  DATA:
    lt_vals        TYPE STANDARD TABLE OF sval,
    wa_vals        TYPE sval,
    lt_key         TYPE ztarn_reg_key,
    ls_key         LIKE LINE OF lt_key,
    lt_prd_version TYPE STANDARD TABLE OF  zarn_prd_version,
    lt_control     TYPE STANDARD TABLE OF  zarn_control,
    lt_cc_hdr      TYPE STANDARD TABLE OF  zarn_cc_hdr,
    lt_reg_data    TYPE ztarn_reg_data,

    lv_itabname    TYPE char50,
    lv_reg_idno    TYPE char50,
    ls_rows        LIKE LINE OF pt_rows,
    ls_tabname     TYPE ty_s_table_mastr,
    ls_reg_data    TYPE zsarn_reg_data,
    ls_prod_data   TYPE zsarn_prod_data.

  FIELD-SYMBOLS:
    <itabname>  TYPE table,
    <reg_idno>  TYPE any,
    <table_str> TYPE any.


* create table and field for input
  wa_vals-tabname   = lc_table_name.
  wa_vals-fieldname = lc_field_name.
  APPEND wa_vals TO lt_vals.

  CALL FUNCTION 'POPUP_GET_VALUES'
    EXPORTING
      popup_title     = TEXT-001 " 'Article to be Copied from ?'(001)
    TABLES
      fields          = lt_vals
    EXCEPTIONS
      error_in_fields = 1
      OTHERS          = 2.

* read the result
  READ TABLE lt_vals INTO wa_vals
       WITH KEY fieldname = lc_field_name.
  IF sy-subrc  IS INITIAL.
    SHIFT wa_vals-value LEFT DELETING LEADING '0'.
*   copy to a global variable for processing later ??
    SELECT SINGLE idno INTO ls_key-idno FROM zarn_products WHERE "#EC CI_NOFIELD
          matnr_ni = wa_vals-value.
    IF sy-subrc  IS NOT INITIAL.
      CLEAR cs_reg_data.
      MESSAGE i000 WITH wa_vals-value TEXT-203.
    ENDIF.
  ENDIF.

*** May have to process selected ALV worklist data here ??? as LOCAL
*** Read  article from regional tables and
*** Copy to all articles from ALV worklist into the internal tables for regional DB
*** the user still has to confirm individually and save on the GUI

  IF ls_key-idno IS NOT INITIAL.

    APPEND ls_key TO lt_key.

    CALL FUNCTION 'ZARN_READ_REGIONAL_DATA'
      EXPORTING
        it_key      = lt_key
      IMPORTING
        es_data_all = cs_reg_data.

    IF gs_reg_data_copy-zarn_reg_hdr IS NOT INITIAL.

      gs_reg_data_copy-idno = ls_key-idno.

*     rows selected from ALV Worklist
      LOOP AT pt_rows INTO ls_rows.

*       read selected articles from worklist
        READ TABLE gt_worklist INTO gs_worklist INDEX ls_rows-index.
        IF sy-subrc IS NOT INITIAL.
          CONTINUE.
        ENDIF.

*       read Regional data to copy over
        READ TABLE gt_reg_data INTO ls_reg_data
             WITH KEY idno =  gs_worklist-idno.
        IF sy-subrc IS NOT INITIAL.
          MESSAGE i000 WITH  TEXT-204 gs_worklist-matnr_ni.
          CLEAR gs_reg_data_copy.
          EXIT.
        ENDIF.
        ls_reg_data      = gs_reg_data_copy.
        ls_reg_data-idno = gs_worklist-idno.

*       Regional tables
        LOOP AT gt_tabname INTO ls_tabname
                           WHERE arena_table_type EQ gc_reg.

          IF <itabname>     IS ASSIGNED. UNASSIGN <itabname>.     ENDIF.
          IF <reg_idno> IS ASSIGNED. UNASSIGN <reg_idno>. ENDIF.
          IF <table_str> IS ASSIGNED. UNASSIGN <table_str>. ENDIF.

*  *      Select Data for all the regional tables
          CLEAR: lv_itabname,
                 lv_reg_idno.

          CONCATENATE gc_reg_data ls_tabname-arena_table INTO lv_itabname.
          ASSIGN (lv_itabname) TO <itabname>.
          IF <itabname> IS NOT ASSIGNED.
            CONTINUE.
          ENDIF.

          CONCATENATE '<TABLE_STR>' '-' gc_idno INTO lv_reg_idno.
*          ASSIGN (lv_reg_idno) TO <reg_idno>.

          LOOP AT <itabname> ASSIGNING <table_str>.
            ASSIGN (lv_reg_idno) TO <reg_idno>.
*           reassign the old IDNO
            <reg_idno> = ls_reg_data-idno.
          ENDLOOP.
        ENDLOOP.

*       change the table
        MODIFY TABLE gt_reg_data FROM ls_reg_data.

      ENDLOOP.

    ELSE.
      CLEAR cs_reg_data.
    ENDIF.

  ENDIF.

ENDFORM.                    " alv_copy_article_details

*&---------------------------------------------------------------------*
*&      Form  CHECK_DATA_LOSS
*&---------------------------------------------------------------------*
FORM check_data_loss.

  DATA:
     lv_answer TYPE char01.

  CONSTANTS:
    lc_yes    TYPE char01 VALUE 'J',
    lc_no     TYPE char01 VALUE 'N',
    lc_cancel TYPE char01 VALUE 'A'.

  PERFORM check_reg_changes CHANGING gv_data_change.

  IF gv_data_change IS NOT INITIAL AND gv_display IS INITIAL.

    PERFORM popup_unsaved_data USING lv_answer.

    IF lv_answer = lc_yes.          "'J'.  "yes

*     Save data first
*      PERFORM process_regional_data USING abap_true.
      PERFORM save_regional_data.
*     then leave
      SET SCREEN 0. LEAVE SCREEN.
    ELSEIF  lv_answer = lc_no.      "'N'.  " No
*     no Save ...
      SET SCREEN 0. LEAVE SCREEN.
    ELSEIF  lv_answer = lc_cancel.  "'A'.  " Cancel
**    Do nothing
    ENDIF.

  ELSE. " No change so leave
    SET SCREEN 0. LEAVE SCREEN.
  ENDIF.

ENDFORM.                    " CHECK_DATA_LOSS
*&---------------------------------------------------------------------*
*&      Form  SCREEN_CONTROLS
*&---------------------------------------------------------------------*
FORM screen_controls .

  PERFORM nat_nutri_alv.
  PERFORM nat_uom_alv.
  PERFORM nat_vendor_price_alv.
  PERFORM nat_ingre_additive_alv.
  PERFORM nat_additive_alv.
  PERFORM nat_pir_alv.
  PERFORM nat_allergen_alv.
  PERFORM nat_zarn_diet_alv.
  PERFORM nat_zarn_chem_alv.
  PERFORM nat_organism_alv.
  PERFORM nat_net_qty_alv.
  PERFORM nat_onlcat_alv.
  PERFORM nat_benefits_alv.

ENDFORM.                    " SCREEN_CONTROLS
*&---------------------------------------------------------------------*
*&      Form  EXCLUDE_TB_FUNCTIONS_DISPLAY
*&---------------------------------------------------------------------*
FORM exclude_tb_functions_display  CHANGING pt_exclude TYPE ui_functions.
  DATA ls_exclude TYPE ui_func.

  ls_exclude = cl_gui_alv_grid=>mc_fc_excl_all.
  APPEND ls_exclude TO pt_exclude.

ENDFORM.                    " EXCLUDE_TB_FUNCTIONS_DISPLAY
*&---------------------------------------------------------------------*
*&      Form  NAT_NUTRI_ALV
*&---------------------------------------------------------------------*
FORM nat_nutri_alv .
  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo.

  IF go_nutri_alv IS INITIAL.
* Nutritional Information
    CREATE OBJECT go_nncc01
      EXPORTING
        container_name = gc_nncc01.

* Create ALV grid
    CREATE OBJECT go_nutri_alv
      EXPORTING
        i_parent = go_nncc01.


*   Call GRID
    PERFORM alv_build_fieldcat USING gc_alv_nat_nutri  CHANGING lt_fieldcat.

**  Exclude all edit functions in this example since we do not need them:
    PERFORM exclude_tb_functions_display CHANGING lt_exclude.


    ls_layout-cwidth_opt = abap_true.
    ls_layout-zebra      = abap_true.
    ls_layout-no_toolbar = abap_true.

    CALL METHOD go_nutri_alv->set_table_for_first_display
      EXPORTING
        is_layout                     = ls_layout
        it_toolbar_excluding          = lt_exclude
      CHANGING
        it_outtab                     = gt_zarn_nutrients
        it_fieldcatalog               = lt_fieldcat
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.

    IF sy-subrc IS NOT INITIAL.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

  ELSE.

    CALL METHOD go_nutri_alv->refresh_table_display.
  ENDIF.
ENDFORM.                    " NAT_NUTRI_ALV
*&---------------------------------------------------------------------*
*&      Form  NAT_UOM_ALV
*&---------------------------------------------------------------------*

FORM nat_uom_alv .
  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo.

  IF go_nat_uom_alv IS INITIAL.
*   Nutritional Information
    CREATE OBJECT go_nucc02
      EXPORTING
        container_name = gc_nucc02.

*   Create ALV grid
    CREATE OBJECT go_nat_uom_alv
      EXPORTING
        i_parent = go_nucc02.


*   Call GRID
    PERFORM alv_build_fieldcat USING gc_alv_nat_uom  CHANGING lt_fieldcat.
**  Exclude all edit functions in this example since we do not need them:
    PERFORM exclude_tb_functions_display CHANGING lt_exclude.


    ls_layout-cwidth_opt = abap_true.
    ls_layout-zebra = abap_true.
    ls_layout-no_toolbar = abap_true.

    CALL METHOD go_nat_uom_alv->set_table_for_first_display
      EXPORTING
        is_layout                     = ls_layout
        it_toolbar_excluding          = lt_exclude
      CHANGING
        it_outtab                     = gt_nat_alv_uom  "gs_prod_data-zarn_nutrients
        it_fieldcatalog               = lt_fieldcat
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.
    IF sy-subrc IS NOT INITIAL.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

    SET HANDLER  go_event_receiver->handle_hotspot_click FOR go_nat_uom_alv.

  ELSE.

    CALL METHOD go_nat_uom_alv->refresh_table_display.
  ENDIF.
ENDFORM.                    " NAT_UOM_ALV
*&---------------------------------------------------------------------*
*&      Form  NAT_VENDOR_PRICE_ALV
*&---------------------------------------------------------------------*

FORM nat_vendor_price_alv .
  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
*    lt_sortcat  TYPE lvc_t_sort,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo.

  IF go_nat_price_alv IS INITIAL.
*   Nutritional Information
    CREATE OBJECT go_nlcc03
      EXPORTING
        container_name = gc_nlcc03.

*   Create ALV grid
    CREATE OBJECT go_nat_price_alv
      EXPORTING
        i_parent = go_nlcc03.


*   Call GRID
    PERFORM alv_build_fieldcat USING gc_alv_nat_lprice  CHANGING lt_fieldcat.
*    PERFORM alv_build_sortcat  USING gc_alv_nat_lprice  CHANGING lt_sortcat.

*   Exclude all edit functions in this example since we do not need them:
    PERFORM exclude_tb_functions_display CHANGING lt_exclude.

    ls_layout-cwidth_opt = abap_true.
    ls_layout-zebra = abap_true.
    ls_layout-no_toolbar = abap_true.

    CALL METHOD go_nat_price_alv->set_table_for_first_display
      EXPORTING
        is_layout                     = ls_layout
        it_toolbar_excluding          = lt_exclude
      CHANGING
        it_outtab                     = gt_zarn_list_price
        it_fieldcatalog               = lt_fieldcat
*       it_sort                       = lt_sortcat
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.
    IF sy-subrc IS NOT INITIAL.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

  ELSE.

    CALL METHOD go_nat_price_alv->refresh_table_display.
  ENDIF.
ENDFORM.                    " NAT_VENDOR_PRICE_ALV
*&---------------------------------------------------------------------*
*&      Form  NAT_INGRE_ADDITIVE_ALV
*&---------------------------------------------------------------------*
FORM nat_ingre_additive_alv .
  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo.

  IF go_nat_ingr_alv IS INITIAL.
* Nutritional Information
    CREATE OBJECT go_nicc04
      EXPORTING
        container_name = gc_nicc04.

* Create ALV grid
    CREATE OBJECT go_nat_ingr_alv
      EXPORTING
        i_parent = go_nicc04.


*Call GRID
    PERFORM alv_build_fieldcat USING gc_alv_nat_ingre  CHANGING lt_fieldcat.
** Exclude all edit functions in this example since we do not need them:
    PERFORM exclude_tb_functions_display CHANGING lt_exclude.

    ls_layout-cwidth_opt = abap_true.
    ls_layout-zebra = abap_true.
    ls_layout-no_toolbar = abap_true.


    CALL METHOD go_nat_ingr_alv->set_table_for_first_display
      EXPORTING
        is_layout                     = ls_layout
        it_toolbar_excluding          = lt_exclude
      CHANGING
        it_outtab                     = gt_zarn_fb_ingre  "gt_nat_alv_ingr
        it_fieldcatalog               = lt_fieldcat
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.

    IF sy-subrc IS NOT INITIAL.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

  ELSE.

    CALL METHOD go_nat_ingr_alv->refresh_table_display.
  ENDIF.
ENDFORM.                    " NAT_INGRE_ADDITIVE_ALV
*&---------------------------------------------------------------------*
*&      Form  NAT_PIR_ALV
*&---------------------------------------------------------------------*
FORM nat_pir_alv .
  DATA:
    lt_fieldcat       TYPE lvc_t_fcat,
*    lt_sortcat        TYPE lvc_t_sort,
    lt_exclude        TYPE ui_functions,
    ls_layout         TYPE lvc_s_layo,
    lo_event_receiver TYPE REF TO lcl_event_handler.

  IF go_nat_pir_alv IS INITIAL.
*   Nutritional Information
    CREATE OBJECT go_npcc05
      EXPORTING
        container_name = gc_npcc05.

*   Create ALV grid
    CREATE OBJECT go_nat_pir_alv
      EXPORTING
        i_parent = go_npcc05.


*   Call GRID
    PERFORM alv_build_fieldcat USING gc_alv_nat_pir  CHANGING lt_fieldcat.
*    PERFORM alv_build_sortcat  USING gc_alv_nat_pir  CHANGING lt_sortcat.

*   Exclude all edit functions in this example since we do not need them:
    PERFORM exclude_tb_functions_display CHANGING lt_exclude.


    ls_layout-cwidth_opt = abap_true.
    ls_layout-zebra = abap_true.
    ls_layout-no_toolbar = abap_true.
    ls_layout-sel_mode   = gc_save.

    CALL METHOD go_nat_pir_alv->set_table_for_first_display
      EXPORTING
        is_layout                     = ls_layout
        it_toolbar_excluding          = lt_exclude
      CHANGING
        it_outtab                     = gt_zarn_pir
        it_fieldcatalog               = lt_fieldcat
*       it_sort                       = lt_sortcat
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.

    IF sy-subrc IS NOT INITIAL.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

*  Set event handler
    CREATE OBJECT lo_event_receiver.

    SET HANDLER:
      lo_event_receiver->handle_double_click FOR go_nat_pir_alv.
  ELSE.

    CALL METHOD go_nat_pir_alv->refresh_table_display.
  ENDIF.
ENDFORM.                    " NAT_PIR_ALV
*&---------------------------------------------------------------------*
*&      Form  REG_PIR_ALV
*&---------------------------------------------------------------------*
FORM reg_pir_alv .
  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
*    lt_sortcat  TYPE lvc_t_sort,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo,
    ls_variant  TYPE disvariant.

  IF go_reg_pir_alv IS INITIAL.
*   Regional PIR
    CREATE OBJECT go_rpcc05
      EXPORTING
        container_name = gc_rpircc05.

* Create ALV grid
    CREATE OBJECT go_reg_pir_alv
      EXPORTING
        i_parent = go_rpcc05.

    PERFORM alv_build_fieldcat   USING  gc_alv_reg_pir CHANGING lt_fieldcat.

*Call GRID
** Exclude all edit functions in this example since we do not need them:
    PERFORM exclude_tb_functions_edit  USING  gc_alv_reg_pir CHANGING lt_exclude.


*   Variant management
    ls_variant-handle   = 'RPIR'.

    PERFORM alv_variant_default CHANGING ls_variant.

*    ls_layout-edit = abap_true.
    ls_layout-no_rowins  = abap_true.   " keyboard selections
    ls_layout-cwidth_opt = abap_true.
*    ls_layout-sel_mode = abap_true.
    ls_layout-sel_mode = gc_save.
    ls_layout-zebra = abap_true.

    CALL METHOD go_reg_pir_alv->set_table_for_first_display
      EXPORTING
        is_variant                    = ls_variant
        i_save                        = gc_save
        is_layout                     = ls_layout
        it_toolbar_excluding          = lt_exclude
      CHANGING
        it_outtab                     = gt_zarn_reg_pir
        it_fieldcatalog               = lt_fieldcat
*       it_sort                       = lt_sortcat
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.

    IF sy-subrc IS NOT INITIAL.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

    CALL METHOD go_reg_pir_alv->register_edit_event
      EXPORTING
        i_event_id = cl_gui_alv_grid=>mc_evt_enter.

    CALL METHOD go_reg_pir_alv->register_edit_event
      EXPORTING
        i_event_id = cl_gui_alv_grid=>mc_evt_modified.
    SET HANDLER  go_event_receiver_edit->handle_hotspot_click FOR go_reg_pir_alv.
  ELSE.

    PERFORM refresh_reg_alv USING go_reg_pir_alv lt_fieldcat.

  ENDIF.
ENDFORM.                    " REG_PIR_ALV
*&---------------------------------------------------------------------*
*&      Form  EXCLUDE_TB_FUNCTIONS_EDIT
*&---------------------------------------------------------------------*
FORM exclude_tb_functions_edit  USING pv_structure CHANGING pt_exclude TYPE ui_functions.
* Only allow to change data not to create new entries (exclude
* generic functions).

  DATA ls_exclude TYPE ui_func.

  FREE pt_exclude[].

  ls_exclude = cl_gui_alv_grid=>mc_fc_detail.
  APPEND ls_exclude TO pt_exclude.

  IF  pv_structure NE gc_alv_reg_pir AND
      pv_structure NE gc_alv_reg_rb  AND
      pv_structure NE gc_alv_reg_uom AND
      pv_structure NE gc_alv_reg_cluster AND
      pv_structure NE gc_alv_reg_gtn AND
      pv_structure NE gc_alv_reg_artlink_ui AND       " ++ONED-217 JKH 11.11.2016
      pv_structure NE gc_alv_reg_onlcat_ui AND        "++ONLD-835 JKH 12.05.2017
      pv_structure NE gc_alv_reg_allerg_ui AND
      pv_structure NE gc_alv_reg_sc.                        "9000004661
    ls_exclude = cl_gui_alv_grid=>mc_fc_current_variant.
    APPEND ls_exclude TO pt_exclude.
  ENDIF.

  ls_exclude = cl_gui_alv_grid=>mc_fc_info.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_refresh.
  APPEND ls_exclude TO pt_exclude.
  ls_exclude = cl_gui_alv_grid=>mc_fc_graph.
  APPEND ls_exclude TO pt_exclude.
  ls_exclude = cl_gui_alv_grid=>mc_fc_check.
  APPEND ls_exclude TO pt_exclude.
  ls_exclude = cl_gui_alv_grid=>mc_fc_views.
  APPEND ls_exclude TO pt_exclude.
  ls_exclude = cl_gui_alv_grid=>mc_mb_sum.
  APPEND ls_exclude TO pt_exclude.
  ls_exclude = cl_gui_alv_grid=>mc_fc_subtot.
  APPEND ls_exclude TO pt_exclude.
  ls_exclude = cl_gui_alv_grid=>mc_fc_sum.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_pc_file.
  APPEND ls_exclude TO pt_exclude.
  ls_exclude = cl_gui_alv_grid=>mc_fc_print.
  APPEND ls_exclude TO pt_exclude.


  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_copy.
  APPEND ls_exclude TO pt_exclude.
  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_copy_row.
  APPEND ls_exclude TO pt_exclude.
  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_cut.
  APPEND ls_exclude TO pt_exclude.

  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_move_row.
  APPEND ls_exclude TO pt_exclude.
  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_paste.
  APPEND ls_exclude TO pt_exclude.
  ls_exclude = cl_gui_alv_grid=>mc_fc_loc_paste_new_row.
  APPEND ls_exclude TO pt_exclude.


*   editing functions disabled
  IF pv_structure   EQ gc_alv_reg_pir
    OR pv_structure EQ gc_alv_reg_rb
    OR pv_structure EQ gc_alv_reg_cluster
    OR pv_structure EQ gc_alv_reg_uom
    OR pv_structure EQ gc_alv_reg_sc                        "9000004661
    OR pv_structure EQ gc_alv_reg_onlcat_ui        "++ONLD-835 JKH 12.05.2017
    OR pv_structure EQ gc_alv_reg_allerg_ui.

    IF pv_structure NE gc_alv_reg_allerg_ui.
      ls_exclude = cl_gui_alv_grid=>mc_fc_loc_append_row.
    ENDIF.
    APPEND ls_exclude TO pt_exclude.
    ls_exclude = cl_gui_alv_grid=>mc_fc_loc_delete_row.
    APPEND ls_exclude TO pt_exclude.
    ls_exclude = cl_gui_alv_grid=>mc_fc_loc_insert_row.
    APPEND ls_exclude TO pt_exclude.
    ls_exclude = cl_gui_alv_grid=>mc_fc_loc_undo.
    APPEND ls_exclude TO pt_exclude.
  ENDIF.


  IF pv_structure EQ gc_alv_reg_dis
    OR pv_structure EQ gc_alv_reg_rrp.

    ls_exclude = cl_gui_alv_grid=>mc_fc_loc_undo.
    APPEND ls_exclude TO pt_exclude.
    ls_exclude = cl_gui_alv_grid=>mc_fc_loc_append_row.
    APPEND ls_exclude TO pt_exclude.
    ls_exclude = cl_gui_alv_grid=>mc_fc_loc_insert_row.
    APPEND ls_exclude TO pt_exclude.
  ENDIF.

  IF pv_structure EQ gc_alv_reg_gtn OR
     pv_structure EQ gc_alv_reg_artlink_ui.    " ++ONED-217 JKH 11.11.2016
    ls_exclude = cl_gui_alv_grid=>mc_fc_loc_insert_row.
    APPEND ls_exclude TO pt_exclude.
  ENDIF.


  IF pv_structure NE gc_alv_reg_gtn AND
     pv_structure NE gc_alv_reg_uom AND
     pv_structure NE gc_alv_reg_sc  AND                     "9000004661
     pv_structure NE gc_alv_reg_artlink_ui AND    " ++ONED-217 JKH 11.11.2016
     pv_structure NE gc_alv_reg_onlcat_ui AND        "++ONLD-835 JKH 12.05.2017
     pv_structure NE gc_alv_reg_allerg_ui.

    ls_exclude = cl_gui_alv_grid=>mc_fc_sort_asc.
    APPEND ls_exclude TO pt_exclude.
    ls_exclude = cl_gui_alv_grid=>mc_fc_sort_dsc.
    APPEND ls_exclude TO pt_exclude.
    ls_exclude = cl_gui_alv_grid=>mc_fc_filter.
    APPEND ls_exclude TO pt_exclude.
    ls_exclude = cl_gui_alv_grid=>mc_fc_find.
    APPEND ls_exclude TO pt_exclude.
  ENDIF.

  ls_exclude = cl_gui_alv_grid=>mc_mb_export.
  APPEND ls_exclude TO pt_exclude.

ENDFORM.                    " EXCLUDE_TB_FUNCTIONS_EDIT
*&---------------------------------------------------------------------*
*&      Form  GET_DATA_ARENA
*&---------------------------------------------------------------------*
FORM get_data_arena .

  DATA:
    lt_key         TYPE ztarn_key,
    lt_key_rmode   TYPE ztarn_key,             "++3174 JKH 23.03.2017
    lt_key_gtin    TYPE ztarn_key,
    lv_select      TYPE abap_bool,
*    lt_key_sel   TYPE ztarn_key,

    ls_short_descr LIKE LINE OF  gr_short_descr,
    ls_gln_descr   LIKE LINE OF  gr_short_descr,
    ls_lfa1        TYPE lfa1.

* at the moment we have 3 description fields in ZARN_PRODUCTS
* of which FS_SHORT_DESCR_UPPER, the following logic an SELECT caters for
* case sensitive search !
*   upper case search
  CLEAR:
    gr_short_descr,
    gr_gln_descr.

  IF s_desc1[]  IS NOT INITIAL OR
     s_desc1    IS NOT INITIAL.
    LOOP AT s_desc1.
      MOVE-CORRESPONDING s_desc1 TO ls_short_descr.
      TRANSLATE  ls_short_descr TO UPPER CASE.
      APPEND ls_short_descr TO gr_short_descr.
    ENDLOOP.
  ENDIF.


  IF s_glname[] IS NOT INITIAL OR
     s_glname IS NOT INITIAL.
*   upper case search
    LOOP AT s_glname.
      MOVE-CORRESPONDING  s_glname TO ls_gln_descr.
      TRANSLATE  ls_gln_descr TO UPPER CASE.
      APPEND ls_gln_descr TO gr_gln_descr.
    ENDLOOP.
  ENDIF.

* data selection
  REFRESH lt_key_gtin.
  IF s_plu[] IS NOT INITIAL.
    SELECT e~idno p~version
      INTO TABLE lt_key_gtin
      FROM zarn_reg_ean AS e
      INNER JOIN zarn_products AS p
        ON p~idno EQ e~idno
      WHERE e~ean11 IN s_plu[]
      AND   e~eantp EQ 'ZP'.
    lv_select = abap_true.
  ENDIF.
  IF s_gtin1[] IS NOT INITIAL.
    IF lt_key_gtin[] IS INITIAL.
      SELECT e~idno p~version
        INTO TABLE lt_key_gtin
        FROM zarn_reg_ean AS e
        INNER JOIN zarn_products AS p
          ON p~idno EQ e~idno
        WHERE e~ean11 IN s_gtin1[].
    ELSE.
*      IF lv_select EQ abap_false.
      SELECT e~idno p~version
        INTO TABLE lt_key_gtin
        FROM zarn_reg_ean AS e
        INNER JOIN zarn_products AS p
          ON p~idno EQ e~idno
          FOR ALL ENTRIES IN lt_key_gtin
          WHERE e~idno  EQ lt_key_gtin-idno
          AND   e~ean11 IN s_gtin1[].
*      ENDIF.
    ENDIF.
    lv_select = abap_true.
  ENDIF.

  IF s_gtin1[] IS NOT INITIAL.
    IF lt_key_gtin[] IS INITIAL.
*      IF lv_select EQ abap_false.
      SELECT idno version
        INTO TABLE lt_key_gtin
        FROM zarn_gtin_var
        WHERE gtin_code IN s_gtin1[].

    ELSE.
      SELECT idno version
        APPENDING TABLE lt_key_gtin
        FROM zarn_gtin_var
        WHERE gtin_code IN s_gtin1[].
    ENDIF.
    lv_select = abap_true.
  ENDIF.

  IF lv_select EQ abap_true.
    IF NOT lt_key_gtin[] IS INITIAL.
      SELECT DISTINCT p~idno p~version
        INTO TABLE lt_key
        FROM zarn_prd_version  AS p
          INNER JOIN zarn_control    AS c
            ON c~guid          EQ p~guid
          INNER JOIN zarn_ver_status AS s
            ON  s~idno         EQ p~idno
            AND s~current_ver  EQ p~version
          INNER JOIN zarn_pir        AS v
            ON  v~idno         EQ p~idno
            AND v~version      EQ p~version
          INNER JOIN zarn_products   AS a
            ON  a~idno         EQ p~idno
            AND a~version      EQ p~version
          INNER JOIN zarn_reg_hdr    AS r
            ON  r~idno         EQ p~idno
         FOR ALL ENTRIES IN lt_key_gtin
         WHERE p~idno                EQ lt_key_gtin-idno
         AND   p~version             EQ lt_key_gtin-version
         AND p~changed_on            IN s_chgdat
         AND p~changed_by            IN s_chgby
         AND p~version_status        IN s_stat
         AND c~created_on            IN s_credat
         AND c~created_by            IN s_creby
         AND c~date_in               IN s_datein
         AND v~idno                  IN s_hid
         AND v~avail_start_date      IN s_atp
         AND v~vendor_article_number IN s_van
         AND v~gln                   IN s_gln1
         AND v~gln_description_upper IN gr_gln_descr
         AND a~fan_id                IN s_fan
         AND a~fsni_icare_value      EQ abap_true
         AND r~matnr                 IN s_matnr
         AND a~fs_brand_id           IN s_bran1
         AND a~fs_short_descr_upper  IN gr_short_descr
         AND ( r~ret_zzcatman        IN s_rcman
          OR   r~gil_zzcatman        IN s_ccman ).
    ENDIF.
  ELSE.
    SELECT DISTINCT p~idno p~version
      INTO TABLE lt_key
      FROM zarn_prd_version  AS p
        INNER JOIN zarn_control    AS c
          ON c~guid          EQ p~guid
        INNER JOIN zarn_ver_status AS s
          ON  s~idno         EQ p~idno
          AND s~current_ver  EQ p~version
        INNER JOIN zarn_pir        AS v
          ON  v~idno         EQ p~idno
          AND v~version      EQ p~version
        INNER JOIN zarn_products   AS a
          ON  a~idno         EQ p~idno
          AND a~version      EQ p~version
        INNER JOIN zarn_reg_hdr    AS r
          ON  r~idno         EQ p~idno
       WHERE p~changed_on          IN s_chgdat
       AND p~changed_by            IN s_chgby
       AND p~version_status        IN s_stat
       AND c~created_on            IN s_credat
       AND c~created_by            IN s_creby
       AND c~date_in               IN s_datein
       AND v~idno                  IN s_hid
       AND v~avail_start_date      IN s_atp
       AND v~vendor_article_number IN s_van
       AND v~gln                   IN s_gln1
       AND v~gln_description_upper IN gr_gln_descr
       AND a~fan_id                IN s_fan
       AND a~fsni_icare_value      EQ abap_true
       AND r~matnr                 IN s_matnr
       AND a~fs_brand_id           IN s_bran1
       AND a~fs_short_descr_upper  IN gr_short_descr
       AND ( r~ret_zzcatman        IN s_rcman
        OR   r~gil_zzcatman        IN s_ccman ).
  ENDIF.


* INS Begin of Change 3174 JKH 23.03.2017
  CLEAR: lt_key_rmode[].
  IF gv_cr_r3_switch = abap_true AND p_rmode = abap_true.
    SELECT DISTINCT p~idno p~version
      INTO TABLE lt_key_rmode[]
      FROM zarn_prd_version  AS p
        INNER JOIN zarn_ver_status AS s
          ON  s~idno         EQ p~idno
          AND s~current_ver  EQ p~version
        INNER JOIN zarn_reg_hdr    AS r
          ON  r~idno         EQ p~idno
       WHERE p~idno IN s_rmhid[]
         AND p~id_type = 'R'
         AND r~fs_short_descr_upper IN s_rmdesc[]
         AND r~fs_brand_desc_upper  IN s_rmbran[].
    IF sy-subrc = 0.
      APPEND LINES OF lt_key_rmode[] TO lt_key[].
    ENDIF.
  ENDIF.  " IF gv_cr_r3_switch = abap_true AND p_rmode = abap_true
* INS End of Change 3174 JKH 3.03.2017





  IF lt_key IS INITIAL.
    RETURN.
  ELSE.
    SORT lt_key.
    DELETE ADJACENT DUPLICATES FROM lt_key.
  ENDIF.

* get all National and Regional DB table data
  PERFORM read_national_data
    USING lt_key
    CHANGING gt_prod_data.

  PERFORM read_regional
    USING lt_key
    CHANGING gt_reg_data.

  PERFORM get_other_table_data USING lt_key.

* INS Begin of Change 3174 JKH 23.03.2017
  IF gv_cr_r3_switch = abap_true.
    IF lt_key_rmode[] IS NOT INITIAL.

* Build R3 Product Data
      PERFORM build_r3_product_data USING lt_key_rmode[]
                                          gt_ver_status[]
                                 CHANGING gt_prod_data[]
                                          gt_reg_data[].

    ENDIF.
  ENDIF.
* INS End of Change 3174 JKH 23.03.2017


ENDFORM.                    " GET_DATA_ARENA

*&---------------------------------------------------------------------*
*&      Form  REG_RB_ALV
*&---------------------------------------------------------------------*
FORM reg_rb_alv .
  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo,
    ls_variant  TYPE disvariant.

  PERFORM populate_banner_descriptions.

  IF go_reg_rb_alv IS INITIAL.
*   Regional Banner
    CREATE OBJECT go_rrbcc06
      EXPORTING
        container_name = gc_rrbcc06.

* Create ALV grid
    CREATE OBJECT go_reg_rb_alv
      EXPORTING
        i_parent = go_rrbcc06.
  ENDIF.

  PERFORM alv_build_fieldcat USING  gc_alv_reg_rb CHANGING lt_fieldcat.
  PERFORM enrich_reg_banner_fieldcat CHANGING lt_fieldcat.
  PERFORM exclude_tb_functions_edit USING  gc_alv_reg_rb CHANGING lt_exclude.

*   Variant management
  IF g_ts_reg-pressed_tab = c_ts_reg-tab2.
    ls_variant-handle   = 'RBAN'.
  ELSE.
    ls_variant-handle   = 'RBAO'.
  ENDIF.
  PERFORM alv_variant_default CHANGING ls_variant.

  ls_layout-cwidth_opt = abap_true.
  ls_layout-sel_mode   = abap_true.
  ls_layout-zebra      = abap_true.

  CALL METHOD go_reg_rb_alv->set_table_for_first_display
    EXPORTING
      is_variant                    = ls_variant
      i_save                        = gc_save
      is_layout                     = ls_layout
      it_toolbar_excluding          = lt_exclude
    CHANGING
      it_outtab                     = gt_zarn_reg_banner
      it_fieldcatalog               = lt_fieldcat
*     it_sort                       = lt_sortcat
    EXCEPTIONS
      invalid_parameter_combination = 1
      program_error                 = 2
      too_many_lines                = 3
      OTHERS                        = 4.
  IF sy-subrc IS NOT INITIAL.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
               WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.

  IF go_reg_banner_ui IS NOT BOUND.
    CALL METHOD go_reg_rb_alv->register_edit_event
      EXPORTING
        i_event_id = cl_gui_alv_grid=>mc_evt_enter.

    CALL METHOD go_reg_rb_alv->register_edit_event
      EXPORTING
        i_event_id = cl_gui_alv_grid=>mc_evt_modified.

    SET HANDLER go_event_receiver->handle_user_command FOR go_reg_rb_alv.
    SET HANDLER go_event_receiver->handle_toolbar      FOR go_reg_rb_alv.

    go_reg_banner_ui = lcl_reg_banner_ui=>get_instance( ).
    go_reg_banner_ui->set_alv_instance( io_alv = go_reg_rb_alv ).
    SET HANDLER go_reg_banner_ui->handle_data_changed FOR go_reg_rb_alv ACTIVATION abap_true.
    SET HANDLER go_reg_banner_ui->handle_data_changed_finished FOR go_reg_rb_alv.

    DATA(lo_controller) = lcl_controller=>get_instance( ).
    SET HANDLER go_reg_banner_ui->handle_display_mode_changed FOR lo_controller.

    PERFORM register_f4_event USING go_reg_rb_alv gc_vrkme.

    lo_controller->raise_display_mode_changed( ).

  ENDIF.

ENDFORM.                    " REG_RB_ALV

FORM reg_cluster_alv.

  CHECK lcl_reg_cluster_ui=>get_instance( )->is_toggle_framework_active( ) = abap_true.

  IF go_reg_cluster_alv IS BOUND.
    go_reg_cluster_alv->refresh_table_display( ).
  ELSE.
    PERFORM init_reg_cluster_alv.
  ENDIF.

ENDFORM.

*&---------------------------------------------------------------------*
*&      Form  REG_PF_ALV
*&---------------------------------------------------------------------*
FORM reg_pf_alv .
  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo.

  IF go_reg_pf_alv IS INITIAL.
* Nutritional Information
    CREATE OBJECT go_rpfcc02
      EXPORTING
        container_name = gc_rpfcc02.

* Create ALV grid
    CREATE OBJECT go_reg_pf_alv
      EXPORTING
        i_parent = go_rpfcc02.

    PERFORM alv_build_fieldcat   USING  gc_alv_reg_pf CHANGING lt_fieldcat.

*Call GRID
** Exclude all edit functions in this example since we do not need them:
    PERFORM exclude_tb_functions_edit USING  gc_alv_reg_pf CHANGING lt_exclude.

    ls_layout-edit = abap_true.
    ls_layout-cwidth_opt = abap_true.
    ls_layout-sel_mode = abap_true.
    ls_layout-zebra = abap_true.

    CALL METHOD go_reg_pf_alv->set_table_for_first_display
      EXPORTING
        is_layout                     = ls_layout
        it_toolbar_excluding          = lt_exclude
      CHANGING
        it_outtab                     = gt_zarn_reg_prfam
        it_fieldcatalog               = lt_fieldcat
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.

    IF sy-subrc IS NOT INITIAL.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

*   set editable cells to ready for input
    CALL METHOD go_reg_pf_alv->set_ready_for_input
      EXPORTING
        i_ready_for_input = 1.

    CALL METHOD go_reg_pf_alv->register_edit_event
      EXPORTING
        i_event_id = cl_gui_alv_grid=>mc_evt_enter.

    CALL METHOD go_reg_pf_alv->register_edit_event
      EXPORTING
        i_event_id = cl_gui_alv_grid=>mc_evt_modified.

  ELSE.
    PERFORM refresh_reg_alv USING go_reg_pf_alv lt_fieldcat.
*    CALL METHOD go_reg_pf_alv->refresh_table_display.
  ENDIF.
ENDFORM.                    " REG_PF_ALV
*&---------------------------------------------------------------------*
*&      Form  REG_UOM_ALV
*&---------------------------------------------------------------------*
FORM reg_uom_alv .
  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
    ls_fieldcat TYPE lvc_s_fcat,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo,
    ls_variant  TYPE disvariant,
    lt_filter   TYPE lvc_t_filt,
    ls_filter   TYPE lvc_s_filt.

  IF go_reg_uom_alv IS INITIAL.
*   Regional UOM
    CREATE OBJECT go_ruomcc01
      EXPORTING
        container_name = gc_ruomcc01.

* Create ALV grid
    CREATE OBJECT go_reg_uom_alv
      EXPORTING
        i_parent = go_ruomcc01.

    PERFORM alv_build_fieldcat   USING  gc_alv_reg_uom CHANGING lt_fieldcat.
    PERFORM enrich_regional_uom_alv_catlog CHANGING lt_fieldcat.

*   Exclude all edit functions in this example since we do not need them:
    PERFORM exclude_tb_functions_edit USING  gc_alv_reg_uom CHANGING lt_exclude.


*   Variant management
    ls_variant-handle   = 'RUOM'.

    PERFORM alv_variant_default CHANGING ls_variant.

    ls_layout-cwidth_opt = abap_true.
    ls_layout-sel_mode   = abap_true.
    ls_layout-zebra      = abap_true.
    ls_layout-edit       = abap_true.
    ls_layout-stylefname = gc_celltab.

    CALL METHOD go_reg_uom_alv->set_table_for_first_display
      EXPORTING
        i_bypassing_buffer            = abap_true
        is_variant                    = ls_variant
        i_save                        = gc_save
        is_layout                     = ls_layout
        it_toolbar_excluding          = lt_exclude
      CHANGING
        it_outtab                     = gt_zarn_reg_uom[]
*       it_outtab                     = gt_zarn_reg_uom_tmp[]
*       it_outtab                     = gt_zarn_reg_uom_alv
        it_fieldcatalog               = lt_fieldcat
*       it_filter                     = gt_filter_uom[]
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.
    IF sy-subrc IS NOT INITIAL.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

* set editable cells to ready for input
    CALL METHOD go_reg_uom_alv->set_ready_for_input
      EXPORTING
        i_ready_for_input = 1.

    CALL METHOD go_reg_uom_alv->register_edit_event
      EXPORTING
        i_event_id = cl_gui_alv_grid=>mc_evt_enter.

    CALL METHOD go_reg_uom_alv->register_edit_event
      EXPORTING
        i_event_id = cl_gui_alv_grid=>mc_evt_modified.


    SET HANDLER  go_event_receiver_edit->handle_data_changed FOR go_reg_uom_alv.
    SET HANDLER  go_event_receiver_edit->handle_hotspot_click FOR go_reg_uom_alv.

    SET HANDLER go_event_receiver_edit->handle_toolbar FOR go_reg_uom_alv.
    SET HANDLER go_event_receiver_edit->handle_user_command FOR go_reg_uom_alv.

*-- Register F4 event
    PERFORM register_f4_event_uom_alv USING go_reg_uom_alv.
*    PERFORM register_f4_event USING go_reg_uom_alv gc_hyb_code.

  ELSE.

    PERFORM refresh_reg_alv USING go_reg_uom_alv lt_fieldcat.



  ENDIF.
ENDFORM.                    " REG_UOM_ALV
*&---------------------------------------------------------------------*
*&      Form  READ_NATIONAL_DATA
*&---------------------------------------------------------------------*
FORM read_national_data  USING    pt_key          TYPE     ztarn_key
                         CHANGING pt_prod_data    TYPE ztarn_prod_data.

  CALL FUNCTION 'ZARN_READ_NATIONAL_DATA'
    EXPORTING
      it_key  = pt_key
    IMPORTING
      et_data = pt_prod_data.

* reformat ingredient statement for text control
  LOOP AT pt_prod_data ASSIGNING FIELD-SYMBOL(<ls_prod_data>).
    LOOP AT <ls_prod_data>-zarn_prod_str ASSIGNING FIELD-SYMBOL(<ls_prod_str>).
      IF NOT <ls_prod_str>-ingredient_statement IS INITIAL.
        PERFORM format_text CHANGING <ls_prod_str>-ingredient_statement.
      ENDIF.
    ENDLOOP.

    " For serving size show unique entries per product i.e. not by
    " nutrient type
    " Doing it here rather than in function module because don't want
    " to impact any updates to the data
    SORT <ls_prod_data>-zarn_serve_size BY serve_size uom.
    DELETE ADJACENT DUPLICATES FROM <ls_prod_data>-zarn_serve_size
      COMPARING serve_size uom.
  ENDLOOP.


ENDFORM.                    " READ_NATIONAL_DATA


FORM format_text CHANGING cv_text TYPE any.

  DATA: lv_text TYPE text1000,
        lt_out  TYPE TABLE OF text1000.

  lv_text = cv_text.

  CALL FUNCTION 'RKD_WORD_WRAP'
    EXPORTING
      textline            = lv_text
      delimiter           = ' '
      outputlen           = 255
    TABLES
      out_lines           = lt_out
    EXCEPTIONS
      outputlen_too_large = 1
      OTHERS              = 2.

  CLEAR cv_text.

  LOOP AT lt_out ASSIGNING FIELD-SYMBOL(<ls_out>).

    IF cv_text IS INITIAL.
      cv_text = <ls_out>.
    ELSE.
      cv_text = cv_text && cl_abap_char_utilities=>cr_lf && <ls_out>.
    ENDIF.
  ENDLOOP.

ENDFORM.

*&---------------------------------------------------------------------*
*&      Form  READ_REGIONAL
*&---------------------------------------------------------------------*
FORM read_regional  USING    pt_key      TYPE     ztarn_key
                    CHANGING pt_reg_data TYPE ztarn_reg_data.

  DATA :
    lt_reg_key TYPE ztarn_reg_key,
    ls_reg_key LIKE LINE OF lt_reg_key,
    ls_key     LIKE LINE OF pt_key.

  LOOP AT pt_key INTO ls_key.
    ls_reg_key-idno  = ls_key-idno.
    APPEND ls_reg_key TO lt_reg_key.
  ENDLOOP.

  SORT lt_reg_key.

  CALL FUNCTION 'ZARN_READ_REGIONAL_DATA'
    EXPORTING
      it_key  = lt_reg_key
    IMPORTING
      et_data = pt_reg_data.



ENDFORM.                    " READ_REGIONAL
*&---------------------------------------------------------------------*
*&      Form  CREATE_WORKLIST
*&---------------------------------------------------------------------*
FORM create_worklist .

  DATA:
    ls_tabname          TYPE ty_s_table_mastr,
    ls_reg_data         TYPE zsarn_reg_data,
    ls_prod_data        TYPE zsarn_prod_data,
    lt_enq              TYPE STANDARD TABLE OF seqg3,
    ls_zarn_prd_version TYPE zarn_prd_version,
    ls_enq              LIKE LINE OF lt_enq,
    lv_garg             TYPE eqegraarg,
    lv_where_nat        TYPE string,
    lv_where_reg        TYPE string,
    lv_itabname         TYPE char50,
    ls_reg_hdr          TYPE zarn_reg_hdr,

    lt_uom_variant      TYPE ztarn_uom_variant,
    ls_uom_variant      TYPE zarn_uom_variant,
    lt_gtin_var         TYPE ztarn_gtin_var,
    ls_gtin_var         TYPE zarn_gtin_var,

    ls_basic_product    TYPE zproduct_lean_search_respons22.
*    ls_arn_products_ex  LIKE LINE OF gt_products_ex.

  FIELD-SYMBOLS:
    <itabname>  TYPE table,
    <table_str> TYPE any.


* Get Table Master Data
  SELECT * FROM zarn_table_mastr
    INTO CORRESPONDING FIELDS OF TABLE gt_tabname
    WHERE ( arena_table_type EQ gc_nat OR
            arena_table_type EQ gc_reg ).

  IF gv_feature_a4_enabled EQ abap_false.
    DELETE gt_tabname WHERE arena_table EQ 'ZARN_ONLCAT'.
  ENDIF.

  LOOP AT gt_prod_data INTO ls_prod_data.

    "++3174 JKH 13.03.2017
    IF mytab-activetab EQ gc_icare_tab.
* If IDNO already exist in worklist then don't reprocess data and
* append to worklist again
      READ TABLE gt_worklist TRANSPORTING NO FIELDS
      WITH KEY idno = ls_prod_data-idno.
      IF sy-subrc = 0.
        CONTINUE.
      ENDIF.


* If total no. of records added to worklist matches with Display items per page then don't add more
* Give Information message
      CLEAR gv_worklist_cnt.
      DESCRIBE TABLE gt_worklist[] LINES gv_worklist_cnt.
      IF gv_worklist_cnt GE p_itempp.
        MESSAGE 'Total number of records to be displayed is exceeding the entered limit, please refine your Search Criteria' TYPE 'I'.
        EXIT.
      ENDIF. " IF lv_lines GE p_itempp
    ENDIF.  " IF mytab-activetab EQ gc_icare_tab


    CLEAR lv_where_nat.

*   Dynnamic where clause for Internal Table
    CONCATENATE 'IDNO = ' '''' INTO lv_where_nat SEPARATED BY space.
    CONCATENATE lv_where_nat ls_prod_data-idno '''' INTO lv_where_nat.
    lv_where_reg = lv_where_nat.

    CONCATENATE lv_where_nat 'AND VERSION = ' '''' INTO lv_where_nat SEPARATED BY space.
    CONCATENATE lv_where_nat ls_prod_data-version '''' INTO lv_where_nat.

*   loop at seperate tables specific to worklist fields
    LOOP AT gt_tabname INTO ls_tabname
      WHERE ( arena_table EQ gc_alv_nat_pir
         OR   arena_table EQ gc_alv_nat_hier
         OR   arena_table EQ gc_alv_nat_gtin
         OR   arena_table EQ gc_alv_nat_prod
         OR   arena_table EQ gc_alv_nat_prod_ex
         OR   arena_table EQ gc_alv_reg_hdr ).


      IF <itabname> IS ASSIGNED. UNASSIGN <itabname>. ENDIF.
*     Regional table handling
      IF ls_tabname-arena_table_type EQ gc_reg.
        READ TABLE gt_reg_data INTO ls_reg_data WITH KEY idno = ls_prod_data-idno.
        IF sy-subrc IS INITIAL.
          CLEAR: lv_itabname.
          CONCATENATE gc_reg_data ls_tabname-arena_table INTO lv_itabname.
          ASSIGN (lv_itabname) TO <itabname>.
          IF <itabname> IS NOT ASSIGNED.
            CONTINUE.
          ENDIF.
          LOOP AT <itabname> ASSIGNING <table_str> WHERE (lv_where_reg).
*           append data to LS_DATA structure
            MOVE-CORRESPONDING <table_str> TO gs_worklist.
            EXIT.
          ENDLOOP.
        ENDIF.

      ELSE.
*      National table handling
*      Select Data for all the Product tables
        CLEAR: lv_itabname.
        CONCATENATE gc_prod_data ls_tabname-arena_table INTO lv_itabname.
        ASSIGN (lv_itabname) TO <itabname>.
        IF <itabname> IS NOT ASSIGNED.
          CONTINUE.
        ENDIF.

* Begin of change IR5101533
        IF ls_tabname-arena_table EQ 'ZARN_GTIN_VAR'.

          CLEAR lt_uom_variant[].
          lt_uom_variant[] = ls_prod_data-zarn_uom_variant[].
          CLEAR lt_gtin_var[].
          lt_gtin_var[] = ls_prod_data-zarn_gtin_var[].

          IF gv_cr_r1_switch = abap_true.

* For GTIN code in worklist, get MAIN GTIN for RETAIL only, else leave it blank
* Preferred is KGM if found, else EA

* check and get main gtin_code for KGM, if found
            CLEAR: ls_uom_variant, ls_gtin_var.
            LOOP AT lt_uom_variant[] INTO ls_uom_variant
              WHERE idno        EQ ls_prod_data-idno
                AND version     EQ ls_prod_data-version
                AND uom_code    CS 'KG'.
              READ TABLE lt_gtin_var[] INTO ls_gtin_var
                        WITH KEY idno         = ls_prod_data-idno
                                 version      = ls_prod_data-version
                                 uom_code     = ls_uom_variant-uom_code
                                 gtin_current = abap_true.
              EXIT.
            ENDLOOP.

* check and get main gtin_code for EA, if KGM not found
            IF ls_gtin_var-gtin_code IS INITIAL.
              CLEAR: ls_uom_variant, ls_gtin_var.
              LOOP AT lt_uom_variant[] INTO ls_uom_variant
                WHERE idno        EQ ls_prod_data-idno
                  AND version     EQ ls_prod_data-version
                  AND uom_code    CS 'EA'.
                READ TABLE lt_gtin_var[] INTO ls_gtin_var
                          WITH KEY idno         = ls_prod_data-idno
                                   version      = ls_prod_data-version
                                   uom_code     = ls_uom_variant-uom_code
                                   gtin_current = abap_true.
                EXIT.
              ENDLOOP.
            ENDIF.

            gs_worklist-gtin_code = ls_gtin_var-gtin_code.

          ELSE.  " IF gv_cr_r1_switch = abap_true

* For GTIN code in worklist, get MAIN GTIN for RETAIL only, else leave it blank
* Preferred is KGM if found, else EA

            DELETE lt_uom_variant[] WHERE product_uom NE 'EA' AND product_uom NE 'KGM'.

* check and get main gtin_code for KGM, if found
            CLEAR: ls_uom_variant, ls_gtin_var.
            READ TABLE lt_uom_variant[] INTO ls_uom_variant
            WITH KEY idno        = ls_prod_data-idno
                     version     = ls_prod_data-version
                     product_uom = 'KGM'.
            IF sy-subrc = 0.
              READ TABLE lt_gtin_var[] INTO ls_gtin_var
                        WITH KEY idno         = ls_prod_data-idno
                                 version      = ls_prod_data-version
                                 uom_code     = ls_uom_variant-uom_code
                                 gtin_current = abap_true.
            ENDIF.

* check and get main gtin_code for EA, if KGM not found
            IF ls_gtin_var-gtin_code IS INITIAL.
              CLEAR: ls_uom_variant, ls_gtin_var.
              READ TABLE lt_uom_variant[] INTO ls_uom_variant
              WITH KEY idno        = ls_prod_data-idno
                       version     = ls_prod_data-version
                       product_uom = 'EA'.
              IF sy-subrc = 0.
                READ TABLE lt_gtin_var[] INTO ls_gtin_var
                          WITH KEY idno         = ls_prod_data-idno
                                   version      = ls_prod_data-version
                                   uom_code     = ls_uom_variant-uom_code
                                   gtin_current = abap_true.
              ENDIF.
            ENDIF.


            gs_worklist-gtin_code = ls_gtin_var-gtin_code.
          ENDIF.  " IF gv_cr_r1_switch = abap_true



        ELSE.
          LOOP AT <itabname> ASSIGNING <table_str> WHERE (lv_where_nat).
*         append data to LS_DATA structure
            MOVE-CORRESPONDING <table_str> TO gs_worklist.
            EXIT.
          ENDLOOP.
        ENDIF.  " IF ls_tabname-arena_table EQ 'ZARN_GTIN_VAR'
* End of change IR5101533



      ENDIF.
    ENDLOOP.

*   keep key reference on worklist
    gs_worklist-idno     = ls_prod_data-idno.
    gs_worklist-version  = ls_prod_data-version.

*   I Care Flag was set for Internal Activation
    IF gs_worklist-fsni_icare_value IS INITIAL AND
       ls_reg_data-zarn_reg_hdr[] IS NOT INITIAL.   "internal activation
*      gs_worklist-checkbox = gs_worklist-fsni_icare_value = abap_true.  " assign checkbox value
      gs_worklist-checkbox  = abap_true.  " assign checkbox value

    ELSE.
      gs_worklist-checkbox = gs_worklist-fsni_icare_value.  " assign checkbox value
    ENDIF.

    " Fill matnr
    CLEAR ls_reg_hdr.
    READ TABLE ls_reg_data-zarn_reg_hdr[] INTO ls_reg_hdr "TRANSPORTING matnr retail_unit_desc status release_status
      WITH KEY idno = gs_worklist-idno.
    IF sy-subrc IS INITIAL.
      gs_worklist-matnr_regional = ls_reg_hdr-matnr.
      gs_worklist-matnr_reg_desc = ls_reg_hdr-retail_unit_desc.
      gs_worklist-reg_version_status = ls_reg_hdr-status.
      gs_worklist-reg_release_status = ls_reg_hdr-release_status.
    ENDIF.

* INS Begin of Change 3174 JKH 22.03.2017
*   get version status
    CLEAR ls_zarn_prd_version.
    LOOP AT gt_prd_version INTO ls_zarn_prd_version
             WHERE idno    = gs_worklist-idno
               AND version = gs_worklist-version
               AND ( id_type EQ 'H' OR
                     id_type EQ 'M' OR
                     id_type EQ 'I' OR
                     id_type EQ 'R' ).
      EXIT.
    ENDLOOP.
    gs_worklist-version_status = ls_zarn_prd_version-version_status.
    gs_worklist-release_status = ls_zarn_prd_version-release_status.
* INS End of Change 3174 JKH 22.03.2017


* INS Begin of Change 3174 JKH 06.03.2017
    IF gv_cr_r1_switch = abap_true.
      gs_worklist-id_type = ls_zarn_prd_version-id_type.

      CLEAR ls_basic_product.
      TRY.
          ls_basic_product = gt_basic_product[ code = gs_worklist-idno ].
        CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
      ENDTRY.


      IF ls_basic_product-creation_time IS NOT INITIAL.
        gs_worklist-creation_time   = ls_basic_product-creation_time.
        gs_worklist-creation_time = |{ gs_worklist-creation_time+6(2) }/{ gs_worklist-creation_time+4(2) }/{ gs_worklist-creation_time+0(4) }|.

        gs_worklist-creation_time_r1   = ls_basic_product-creation_time.
      ENDIF.

      IF gs_worklist-creation_time IS INITIAL AND gs_worklist-id_type EQ 'R'.
        gs_worklist-creation_time     = ls_reg_hdr-creation_time_r1.
        gs_worklist-creation_time = |{ gs_worklist-creation_time+6(2) }/{ gs_worklist-creation_time+4(2) }/{ gs_worklist-creation_time+0(4) }|.

        gs_worklist-creation_time_r1   = ls_reg_hdr-creation_time_r1.
      ENDIF.


      IF gs_worklist-id_type EQ 'H' OR gs_worklist-id_type EQ 'M' OR gs_worklist-id_type EQ 'I'.
        gs_worklist-catalog_version = gc_online.      " Online
      ELSE.
        gs_worklist-catalog_version = ls_basic_product-catalog_version.
      ENDIF.
    ENDIF.

    CLEAR ls_zarn_prd_version.
* INS End of Change 3174 JKH 06.03.2017

* INS Begin of Change 3174 JKH 21.03.2017
    IF gv_cr_r3_switch = abap_true.
      gs_worklist-requested_by      = ls_reg_hdr-requested_by.
      gs_worklist-requested_by_date = ls_reg_hdr-requested_by_date.
      gs_worklist-requestor         = ls_reg_hdr-requestor.
      gs_worklist-requested_mc      = ls_reg_hdr-requested_mc.
      gs_worklist-requested_on      = ls_reg_hdr-requested_on.

      IF ls_reg_hdr-requested_by_date IS NOT INITIAL AND ls_reg_hdr-requested_by_date NE space.
        PERFORM get_enrich_sla_from_reqdate USING ls_reg_hdr-requested_by_date
                                                  'ZZ'
                                         CHANGING gs_worklist-enrich_sla.
      ENDIF.

* If throgh AReNa tab, for R mode selection, for R mode products
* if SLA is entered as criteria then
* only consider those requested products which matched entered SLA
      IF mytab-activetab = gc_arena_tab AND
         p_rmode = abap_true            AND
         gs_worklist-id_type = 'R' AND
         s_sla[] IS NOT INITIAL.
        IF gs_worklist-enrich_sla NOT IN s_sla[].
          CONTINUE.
        ENDIF.
      ENDIF.

    ENDIF.  " IF gv_cr_r3_switch = abap_true
* INS End of Change 3174 JKH 21.03.2017


*   if display only not selected from selection screen
    IF gv_display IS INITIAL.
      lv_garg = sy-mandt && ls_prod_data-idno.

*     check lock
      CALL FUNCTION 'ENQUEUE_READ'
        EXPORTING
          gclient               = sy-mandt
          gname                 = gc_lock_reg
          garg                  = lv_garg
          guname                = ' '
        TABLES
          enq                   = lt_enq
        EXCEPTIONS
          communication_failure = 1
          system_failure        = 2
          OTHERS                = 3.

      IF lt_enq IS NOT INITIAL.
        READ TABLE lt_enq INTO ls_enq INDEX 1.
        PERFORM get_control_data USING    ls_enq-guname
                                 CHANGING gs_worklist-lock_user.

*       Locked Entries
        gs_worklist-rowcolor = gc_alv_locked .
        WRITE icon_locked TO gs_worklist-icon.
        gs_worklist-lock_user = gs_worklist-icon && gs_worklist-lock_user.
      ENDIF.
    ENDIF.

*   create worklist table
    APPEND gs_worklist TO gt_worklist.
    CLEAR: gs_worklist, ls_reg_data.
  ENDLOOP.



* INS Begin of Change 3174 JKH 06.03.2017
  IF gv_cr_r1_switch = abap_true.
    CLEAR gv_worklist_cnt.
    DESCRIBE TABLE gt_worklist[] LINES gv_worklist_cnt.
    IF gv_worklist_cnt GE p_itempp.
      gv_smore_disable = abap_true.
    ENDIF.

    IF mytab-activetab EQ gc_icare_tab.
      IF gt_search_sort[] IS NOT INITIAL.
        PERFORM sort_icare_search_worklist.
      ENDIF.
    ENDIF.
  ENDIF.
* INS End of Change 3174 JKH 06.03.2017

  gt_worklist_orig[] = gt_worklist[].

ENDFORM.                    " CREATE_WORKLIST
*&---------------------------------------------------------------------*
*&      Form  PROCESS_WORKLIST_SELECTION
*&---------------------------------------------------------------------*

FORM process_worklist_selection  USING    pt_rows TYPE lvc_t_row.


* restart
  CLEAR gv_row_index.

* count selection
  DESCRIBE TABLE pt_rows LINES gv_row_total.

* Always start on the first selection !
  gv_row_index = 1.

* Always start with the default display
  gv_display = gv_display_default.


* fill screen
  PERFORM process_worklist_to_screen USING pt_rows.
  PERFORM enrich_data.


ENDFORM.                    " PROCESS_WORKLIST_SELECTION
*&---------------------------------------------------------------------*
*&      Form  PROCESS_WORKLIST_TO_SCREEN
*&---------------------------------------------------------------------*

FORM process_worklist_to_screen  USING pt_rows TYPE lvc_t_row.


  DATA :
    lv_times              TYPE zarn_cat_seqno,
    lv_where_nat          TYPE string,
    lv_where_reg          TYPE string,
    lv_itabname           TYPE char50,
    lv_itabfieldname      TYPE char70,
    lv_deffieldname       TYPE char70,
    lv_nat_itabname       TYPE char50,
    lv_ddif_tab           TYPE ddobjname,
    lv_reg_itabname       TYPE char50,
    ls_rows               LIKE LINE OF pt_rows,
    ls_zarn_reg_uom       LIKE LINE OF gt_zarn_reg_uom,
*    ls_zarn_reg_sc_uom    LIKE LINE OF gt_zarn_reg_sc_uom,  "9000004661 - Supply Chain Tab added
    ls_zarn_reg_sc        LIKE LINE OF gt_zarn_reg_sc,      "9000004661 - Supply Chain Tab added
    ls_tabname            TYPE ty_s_table_mastr,
    ls_reg_data           TYPE zsarn_reg_data,
    ls_reg_data_def       TYPE zsarn_reg_data,
    lt_zarn_reg_ean       TYPE ztarn_reg_ean_ui,
    ls_zarn_reg_ean       TYPE zsarn_reg_ean_ui,
    ls_multi_gtin         LIKE LINE OF gt_multi_gtin,
    ls_prod_data          TYPE zsarn_prod_data,
    ls_prod_data_o        TYPE zsarn_prod_data,
    lt_dfies              TYPE dfies_tab,
    ls_dfies              TYPE dfies,
    ls_gui_set            TYPE zarn_gui_set,
    ls_gui_def            TYPE zarn_gui_def,
    ls_zarn_uom_cat       LIKE LINE OF gt_zarn_uom_cat,
    ls_txt                LIKE LINE OF gt_zarn_reg_txt,
    ls_zarn_prod_uom_t    TYPE zarn_prod_uom_t,
    ls_tvarv_ean_cate     LIKE LINE OF gt_tvarv_ean_cate,
    lv_ean_prefix         TYPE ean11,
    lv_eantp              TYPE numtp,
    lt_reg_ean_tmp        TYPE ztarn_reg_ean,
    ls_reg_ean_tmp        TYPE zarn_reg_ean,
    lt_reg_uom_tmp        TYPE ztarn_reg_uom,
    ls_reg_uom_tmp        TYPE zarn_reg_uom,
    ls_reg_sc_tmp         TYPE zarn_reg_sc,                 "9000004661 - Supply Chain Tab added
    ls_reg_allerg_tmp     TYPE zarn_reg_allerg,
    ls_reg_allerg         TYPE zsarn_reg_allerg_ui,

    ls_reg_pir_relif      TYPE zsarn_reg_pir_ui,
    lv_no_uom_avail       TYPE flag,
    lv_no_uom_avail_ean   TYPE flag,
    lv_dim_overflow_error TYPE flag,

    lo_arn_reg_artlink    TYPE REF TO zcl_arn_reg_artlink.    " ++ONED-217 JKH 21.11.2016


  FIELD-SYMBOLS:
    <itabname>           TYPE table,
    <reg_itabname>       TYPE table,
    <nat_itabname>       TYPE table,
    <itabfieldname>      TYPE any,
    <deffieldname>       TYPE any,
    <table_str>          TYPE any,
    <scr_table>          TYPE any,
    <ls_zarn_reg_ean>    TYPE zarn_reg_ean,
    <ls_zarn_reg_ean_ui> TYPE zsarn_reg_ean_ui,
    <ls_zarn_reg_sc_ui>  TYPE zsarn_reg_sc_ui,
    <ls_zarn_reg_uom_ui> TYPE zsarn_reg_uom_ui,
    <ls_zarn_reg_hdr>    TYPE zarn_reg_hdr,
    <ls_reg_allerg>      TYPE zsarn_reg_allerg_ui.


  FREE:
   gt_zarn_nutrients,
   gt_nat_alv_uom,
   gt_zarn_list_price,
   gt_zarn_fb_ingre,
   gt_zarn_additives,
   gt_zarn_pir,
   gt_zarn_allergen,
   gt_zarn_diet_info,
   gt_zarn_chem_char,
   gt_zarn_organism,
   gt_zarn_bioorganism,
   gt_zarn_onlcat,
   gt_zarn_benefits,
   gt_zarn_net_qty,
   gt_zarn_serve_size,
   gt_zarn_reg_hdr,
   gt_zarn_reg_banner,
   gt_zarn_reg_cluster,
   gt_zarn_reg_onlcat,                  "++ONLD-835 JKH 12.05.2017
   gt_zarn_reg_ean,
   gt_zarn_reg_pir,
   gt_zarn_reg_prfam,
   gt_zarn_reg_txt,
   gt_zarn_reg_uom,
   gt_zarn_reg_hsno,
   gt_zarn_reg_allerg,
   gt_zarn_reg_sc,                      "9000004661 - Supply Chain Tab added
*   gt_zarn_reg_sc_uom,                  "9000004661 - Supply Chain Tab added
   gt_line,
   gt_line_prfam,
*   gt_zarn_reg_lst_prc,
   gt_zarn_reg_std_ter,
   gt_zarn_reg_rrp,
*   gt_zarn_reg_dc_sell,
   gt_multi_gtin,
   gt_zarn_addit_info,
   gt_zarn_hsno,
   gt_pir_comm_ch,
   gt_zarn_pir_comm_ui,
   gt_zarn_pir_comm_ch,
   gt_zarn_cdt_level,
   gt_zarn_ins_code,
   gt_marm_uom,
   gt_zmd_host_sap_itab,
   gt_zarn_reg_artlink,         " ++ONED-217 JKH 21.11.2016
   gt_zarn_reg_str,             "++ONLD-835 JKH 26.05.2017
   gt_zarn_reg_allerg.

  CLEAR:
   gv_new,
   gv_existing,
   gv_change,
   gv_zattr6_def,                                             " ++INC5321987 JKH 03.11.2016
   gv_zztktprnt,
   gs_reg_data_db,
   gs_reg_data_save,
   gv_banner_def_gil,
   gv_banner_def_ret,
   zarn_growing,
   zarn_reg_hdr,
   zarn_reg_prfam,
   zarn_reg_ean,
   zarn_reg_uom,
   zarn_reg_hsno,
   zarn_reg_allerg,
   zarn_reg_sc,                          "9000004661 - Supply Chain Tab added
   gs_control,
   gs_prod_str,
   gs_ntr_claimtx,
   gs_prod_data_o,
   gs_reg_data_o,
   gs_reg_str.             "++ONLD-835 JKH 26.05.2017

  CREATE OBJECT lo_arn_reg_artlink. " ++ONED-217 JKH 21.11.2016

* read selected rows
  READ TABLE pt_rows INTO ls_rows INDEX gv_row_index.
  IF sy-subrc <> 0.
    RETURN.
  ENDIF.

*     read selected articles from worklist
  READ TABLE gt_worklist INTO gs_worklist INDEX ls_rows-index.
  IF sy-subrc <> 0.
    RETURN.
  ENDIF.

  " move fields to screen national fields

*     If locked force display mode
  IF gs_worklist-lock_user NE space.
    gv_display = abap_true.
    MESSAGE s005 DISPLAY LIKE 'E'.
  ENDIF.

*>>>IS1609ins Feature 3162 - AReNa Approval Stabilisation

  "dequeue all optimistic locks set by this program
  IF go_approval IS BOUND.

    go_approval->dequeue_appr( zif_enq_mode_c=>optimistic ).

  ENDIF.

  FREE go_approval.
  go_approval = NEW #( iv_idno  = gs_worklist-idno
                       iv_load_db_approvals = abap_true
                       iv_approval_event = zcl_arn_approval_backend=>gc_event_ui ).
  TRY.

      go_approval->enqueue_appr_idno_or_wait( iv_mode = zif_enq_mode_c=>optimistic ).

    CATCH zcx_excep_cls.
      "should not happen
      MESSAGE a099.
  ENDTRY.
*<<<IS1609ins Feature 3162 - AReNa Approval Stabilisation

  READ TABLE gt_prod_data INTO ls_prod_data
       WITH KEY idno    =  gs_worklist-idno
                version =  gs_worklist-version.
  IF sy-subrc IS INITIAL.
    LOOP AT gt_tabname INTO ls_tabname
        WHERE ( arena_table NE gc_zarn_ntr_claimtx AND
                arena_table NE gc_zarn_prod_str )  AND
                arena_table_type EQ gc_nat.
      IF <itabname>     IS ASSIGNED. UNASSIGN <itabname>.     ENDIF.
      IF <scr_table>    IS ASSIGNED. UNASSIGN <scr_table>.    ENDIF.
      IF <nat_itabname> IS ASSIGNED. UNASSIGN <nat_itabname>. ENDIF.
*         Select Data for all the Product tables
      CLEAR: lv_itabname,
             lv_nat_itabname.

      CONCATENATE gc_prod_data ls_tabname-arena_table INTO lv_itabname.
      ASSIGN (lv_itabname) TO <itabname>.
      IF <itabname> IS NOT ASSIGNED.
        CONTINUE.
      ENDIF.
      CONCATENATE gc_gt_prefix ls_tabname-arena_table INTO lv_nat_itabname.
      ASSIGN (lv_nat_itabname) TO <nat_itabname>.

*         workarea
      ASSIGN (ls_tabname-arena_table) TO <scr_table>.

      LOOP AT <itabname> ASSIGNING <table_str>.
        MOVE-CORRESPONDING <table_str> TO <scr_table>.
        EXIT.
      ENDLOOP.

*         Assign any global tables that we require data to be filled into for ALV
      IF <nat_itabname> IS ASSIGNED.
        <nat_itabname> = <itabname>.
      ENDIF.

    ENDLOOP.

*{ The following code should become obsolete once all data from previous locations is
*  migrated to the new tables/locations.
*  To identify the relevant code easily it is blocked - considered moving to separate
*  routine but decided against it
    DATA(lv_migrate) = abap_true.
    IF lv_migrate = abap_true.

      " Take over data from original Organism table when new table is empty - eventually
      " will not be necessary to check the original table.
      IF gt_zarn_bioorganism[] IS INITIAL.
        gt_zarn_bioorganism = VALUE #( FOR ls_wa IN gt_zarn_organism
                                       ( idno       = ls_wa-idno
                                         version    = ls_wa-version
                                         type       = ls_wa-organism_type
                                         max_value  = ls_wa-max_value
                                         uom        = ls_wa-organism_uom )
                                     ).
      ENDIF.

    ENDIF.

*       keep previous Article values
    ls_prod_data_o = gs_prod_data_o.
*       move to Global structure
    gs_prod_data_o = ls_prod_data.

*       G Strings !
    READ TABLE ls_prod_data-zarn_prod_str    INTO gs_prod_str     INDEX 1 .
    READ TABLE ls_prod_data-zarn_ntr_claimtx INTO gs_ntr_claimtx  INDEX 1 .

*       Convert to UOM table - ZARN_UOM_VARIANT, ZARN_GTIN_VAR
    LOOP AT ls_prod_data-zarn_uom_variant INTO zarn_uom_variant.
*         get multiple GTINs
      LOOP AT ls_prod_data-zarn_gtin_var INTO zarn_gtin_var
           WHERE idno     EQ zarn_uom_variant-idno
             AND version  EQ zarn_uom_variant-version
             AND uom_code EQ zarn_uom_variant-uom_code.

        CONCATENATE gs_nat_alv_uom-gtin_multi zarn_gtin_var-gtin_code
               INTO gs_nat_alv_uom-gtin_multi SEPARATED BY ' - '.
        MOVE-CORRESPONDING zarn_gtin_var TO ls_multi_gtin.
        ls_multi_gtin-num_base_units = zarn_uom_variant-num_base_units.
        APPEND ls_multi_gtin TO gt_multi_gtin.  " for dialogbox !
        CLEAR ls_multi_gtin.
        IF zarn_gtin_var-gtin_current IS NOT INITIAL.
          gs_nat_alv_uom-gtin_code    = zarn_gtin_var-gtin_code.
          gs_nat_alv_uom-gtin_current = zarn_gtin_var-gtin_current.
        ENDIF.


      ENDLOOP.
      MOVE-CORRESPONDING zarn_uom_variant TO gs_nat_alv_uom.

*         Get AReNa: Product UOM Text
      READ TABLE gt_zarn_prod_uom_t INTO ls_zarn_prod_uom_t
           WITH KEY product_uom   = zarn_uom_variant-product_uom.
      IF sy-subrc IS INITIAL.
        gs_nat_alv_uom-uom_category = ls_zarn_prod_uom_t-sap_uom_cat.
      ENDIF.

      APPEND gs_nat_alv_uom TO gt_nat_alv_uom.
      CLEAR gs_nat_alv_uom.
    ENDLOOP.

  ENDIF.

  PERFORM get_nat_mc_desc.

  " Set variable for intensifying National sub-tabs
  PERFORM set_nat_intensify_flags.

  IF gt_zarn_pir_comm_ch[] IS NOT INITIAL.
    gt_zarn_pir_comm_ui[] = gt_zarn_pir_comm_ch[].
  ENDIF.

*__________________________________________________________________________________
*_____Regional fields______________________________________________________________
*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  READ TABLE gt_reg_data INTO ls_reg_data
       WITH KEY idno =  gs_worklist-idno.
  IF sy-subrc IS INITIAL.


    CLEAR gs_reg_data_db.
    gs_reg_data_db = ls_reg_data.

    LOOP AT gt_tabname INTO ls_tabname
      WHERE arena_table_type EQ gc_reg
        AND arena_table NE gc_zarn_reg_str.                          "++ONLD-835 JKH 26.05.2017

      IF <itabname>     IS ASSIGNED. UNASSIGN <itabname>.     ENDIF.
      IF <scr_table>    IS ASSIGNED. UNASSIGN <scr_table>.    ENDIF.
      IF <reg_itabname> IS ASSIGNED. UNASSIGN <reg_itabname>. ENDIF.

*  *      Select Data for all the regional tables
      CLEAR: lv_itabname,
             lv_reg_itabname.
      CONCATENATE gc_reg_data ls_tabname-arena_table INTO lv_itabname.
      ASSIGN (lv_itabname) TO <itabname>.
      IF <itabname> IS NOT ASSIGNED.
        CONTINUE.
      ENDIF.

      IF ls_tabname-arena_table = 'ZARN_REG_EAN'.
        lv_reg_itabname = 'LT_REG_EAN_TMP'.
      ELSEIF ls_tabname-arena_table = 'ZARN_REG_UOM'.
        lv_reg_itabname = 'LT_REG_UOM_TMP'.
      ELSEIF ls_tabname-arena_table = gc_zarn_reg_allerg. "'ZARN_REG_ALLERG'
        CONTINUE.
      ELSE.
        CONCATENATE gc_gt_prefix ls_tabname-arena_table INTO lv_reg_itabname.
      ENDIF.

      ASSIGN (lv_reg_itabname) TO <reg_itabname>.
*         workarea
      ASSIGN (ls_tabname-arena_table) TO <scr_table>.
      IF sy-subrc = 0.
        LOOP AT <itabname> ASSIGNING <table_str>.
*           for ZARN_REG* tables sutructure on the screen
          MOVE-CORRESPONDING <table_str> TO <scr_table>.
          EXIT.
        ENDLOOP.
      ENDIF.

*         ALV
*         Assign any global tables that we require data to be filled into for ALV
      IF <reg_itabname> IS ASSIGNED AND <itabname> IS ASSIGNED.
        <reg_itabname> = <itabname>.
      ENDIF.

* INS Begin of Change ONED-217 JKH 21.11.2016
      IF ls_tabname-arena_table = 'ZARN_REG_ARTLINK'.
        CALL METHOD lo_arn_reg_artlink->get_reg_artlink_idno
          EXPORTING
            iv_idno        = gs_worklist-idno
*           it_idno        =
          IMPORTING
            et_reg_artlink = <itabname>.

        gs_reg_data_db-zarn_reg_artlink[] = ls_reg_data-zarn_reg_artlink[].
*            <itabname> = ls_reg_data-zarn_reg_artlink[].
      ENDIF.
* INS End of Change ONED-217 JKH 21.11.2016

    ENDLOOP.

* INS Begin of Change ONED-217 JKH 21.11.2016
    IF zarn_reg_hdr-matnr IS NOT INITIAL.
      CALL METHOD lo_arn_reg_artlink->build_reg_artlink_ui
        EXPORTING
          it_reg_artlink    = ls_reg_data-zarn_reg_artlink[]
        IMPORTING
          et_reg_artlink_ui = gt_zarn_reg_artlink.
    ENDIF.
* INS End of Change ONED-217 JKH 21.11.2016

    IF gt_zarn_reg_banner[] IS NOT INITIAL.
      "logic wrapped into functions that are used
      "also by validation engine
      "please keep the interface as it is and be aware
      "that every change will be reflected also by validations
      "that are dependent on those flags

      CALL FUNCTION 'ZARN_DETERMINE_RET_RANGING'
        EXPORTING
          it_zarn_reg_banner = ls_reg_data-zarn_reg_banner
        CHANGING
          cv_ret_ranging     = gv_banner_def_ret.

      CALL FUNCTION 'ZARN_DETERMINE_WHL_RANGING'
        EXPORTING
          it_zarn_reg_banner = ls_reg_data-zarn_reg_banner
        CHANGING
          cv_whl_ranging     = gv_banner_def_gil.

    ENDIF.


* INS Begin of Change ONLD-835 JKH 16.05.2017
*       REG Strings !
    READ TABLE ls_reg_data-zarn_reg_str INTO gs_reg_str INDEX 1 .

    gt_zarn_reg_str[] = ls_reg_data-zarn_reg_str[].

    IF gt_zarn_reg_onlcat[] IS NOT INITIAL.
      PERFORM get_onlcat_descriptions CHANGING gt_zarn_reg_onlcat.
    ENDIF.
* INS End of Change ONLD-835 JKH 16.05.2017


    LOOP AT lt_reg_ean_tmp INTO ls_reg_ean_tmp.
      CLEAR ls_zarn_reg_ean.
      MOVE-CORRESPONDING ls_reg_ean_tmp TO ls_zarn_reg_ean.
      APPEND ls_zarn_reg_ean TO gt_zarn_reg_ean.
    ENDLOOP.

    PERFORM map_regional_uom USING lt_reg_uom_tmp
                                   gt_nat_alv_uom
                          CHANGING gt_zarn_reg_uom.

    gs_reg_data_o =  ls_reg_data.

*       check price family settings
    IF zarn_reg_prfam-prfam_status EQ 1 OR
       zarn_reg_prfam-prfam_status IS INITIAL.
      gv_new = abap_true.
    ELSEIF zarn_reg_prfam-prfam_status EQ 2.
      gv_existing  = abap_true.
    ELSEIF zarn_reg_prfam-prfam_status EQ 3.
      gv_change  = abap_true.
    ENDIF.

    IF zarn_reg_prfam-prfam_done_flag = abap_true.
      WRITE icon_locked TO gv_prfam_done_text.
      gv_prfam_done_text = gv_prfam_done_text && TEXT-039. "Done

    ENDIF.
  ENDIF.

**    GTIN values from National tables
  IF gt_zarn_reg_ean IS INITIAL.
    gt_zarn_reg_ean = lt_zarn_reg_ean.
  ELSE.
    " add missing national record if any to regional level
    LOOP AT lt_zarn_reg_ean INTO ls_zarn_reg_ean.
      READ TABLE gt_zarn_reg_ean ASSIGNING <ls_zarn_reg_ean_ui>
             WITH KEY idno  =  ls_zarn_reg_ean-idno
*                          meinh = zarn_reg_ean-meinh
                      ean11 = ls_zarn_reg_ean-ean11.
      IF sy-subrc IS NOT INITIAL.
        APPEND ls_zarn_reg_ean TO gt_zarn_reg_ean.
      ELSE.
        <ls_zarn_reg_ean_ui>-nat_ean_flag = abap_true.
      ENDIF.
    ENDLOOP.

  ENDIF.

  FREE: gt_mvke[], gs_mara.

  IF gs_worklist-fan_id IS NOT INITIAL.
    SELECT SINGLE matnr meins mtart matkl INTO gs_mara FROM mara WHERE zzfan = gs_worklist-fan_id.
    IF sy-subrc EQ 0.
      SELECT meinh FROM marm INTO TABLE gt_marm_uom WHERE matnr = gs_mara-matnr.
      SELECT vkorg INTO TABLE gt_mvke FROM mvke WHERE matnr = gs_mara-matnr.
      SELECT * FROM zmd_host_sap INTO TABLE gt_zmd_host_sap_itab WHERE matnr = gs_mara-matnr. "#EC CI_ALL_FIELDS_NEEDED
    ENDIF.
  ENDIF.

  CLEAR: ls_reg_data_def, lv_no_uom_avail, lv_no_uom_avail_ean, lv_dim_overflow_error.


*>>>IS1609ins Feature 3162 - AReNa Approval Stabilisation

  "do not national -> regional carry over when entering in enrichment mode
  "do it only after national changes are fully approved

*********************************************************************************
*     Default logic !!
*********************************************************************************

  " Default Regional Tables - defaulted UI fields only
  gr_gui_load->set_regional_default_ui(
    EXPORTING
      is_reg_data        = ls_reg_data
      is_prod_data       = ls_prod_data
      iv_reg_only        = gv_display
    IMPORTING
      es_reg_data_def    = ls_reg_data_def
      et_reg_uom_ui      = gt_zarn_reg_uom[]
      et_reg_pir_ui      = gt_zarn_reg_pir[]
      et_reg_ean_ui      = gt_zarn_reg_ean[]
      et_reg_allergen_ui = gt_zarn_reg_allerg ).

  gt_zarn_reg_hdr[]  = ls_reg_data_def-zarn_reg_hdr[].
  gt_zarn_reg_hsno[] = ls_reg_data_def-zarn_reg_hsno[].
*INS End of Change Feature 3162 - Jitin 11.10.2016

* INS - Begin of SAPTR-147 Post Tech Refresh Issues Sort Defaults
  SORT gt_zarn_reg_banner BY banner.
  SORT gt_zarn_reg_std_ter BY vkorg.
  SORT gt_zarn_list_price BY eff_start_date uom_code.
  SORT gt_zarn_pir BY hybris_internal_code.
* INS - End of SAPTR-147 Post Tech Refresh Issues Sort

*<<<IS1609ins Feature 3162 - AReNa Approval Stabilisation

  PERFORM set_disable_columns USING ls_prod_data-zarn_uom_variant.

* Default REG HDR fields
  READ TABLE gt_zarn_reg_hdr[] INTO zarn_reg_hdr
    WITH KEY idno = gs_worklist-idno.


* Default Regular Vendor for Basic screen
  CLEAR zarn_reg_pir-lifnr.

  CLEAR: ls_reg_pir_relif.
  READ TABLE gt_zarn_reg_pir[] INTO ls_reg_pir_relif
  WITH KEY idno = gs_worklist-idno
           relif = abap_true.
  IF sy-subrc EQ 0 AND
     ls_reg_pir_relif-lifnr IS NOT INITIAL AND
     ls_reg_pir_relif-lifnr NE 'MULTIPLE'.
    zarn_reg_pir-lifnr = ls_reg_pir_relif-lifnr.
  ENDIF.


  IF lv_no_uom_avail = abap_true OR lv_no_uom_avail_ean = abap_true.
* No more UOMs available to be defaulted. Kindly Contact IMS.
    MESSAGE s085 DISPLAY LIKE 'E'.
  ENDIF.


* Exceeds the maximum length for Dim. and Wt. values, proceed manually.
  IF lv_dim_overflow_error = abap_true.
    MESSAGE s087 DISPLAY LIKE 'E'.
  ENDIF.

*********************************************************************************
  IF gs_reg_data_copy IS NOT INITIAL.
    RETURN.
  ENDIF.

*     read other data !
  READ TABLE gt_prd_version INTO zarn_prd_version
       WITH KEY idno    = gs_worklist-idno
                version = gs_worklist-version.

  READ TABLE gt_ver_status INTO zarn_ver_status
       WITH KEY idno    = gs_worklist-idno.
*                    version = gs_worklist-version.

  READ TABLE gt_control INTO zarn_control
       WITH KEY guid = zarn_prd_version-guid.

  READ TABLE gt_cc_hdr INTO zarn_cc_hdr
       WITH KEY  idno            = gs_worklist-idno
                 national_ver_id = gs_worklist-version.

*     product hierarchy text  to display on screen
  PERFORM read_prod_hierarchy USING zarn_reg_hdr-prdha. " uct_hierarchy.

*     user name for screen
  PERFORM get_control_data USING zarn_control-created_by     CHANGING gs_control-nat_create_name.
  PERFORM get_control_data USING zarn_prd_version-changed_by CHANGING gs_control-nat_change_name.
  PERFORM get_control_data USING zarn_reg_hdr-ernam CHANGING          gs_control-reg_create_name.
  PERFORM get_control_data USING zarn_reg_hdr-aenam CHANGING          gs_control-reg_change_name.

*     Sort criteria for screen display
  SORT:
    gt_nat_alv_uom             BY num_base_units,
    gt_multi_gtin              BY num_base_units.

*     regional text table to display on screen
  LOOP AT gt_zarn_reg_txt INTO ls_txt.
    APPEND ls_txt-line TO gt_line.
  ENDLOOP.
*     send table to control
  CALL METHOD go_reg_editor->set_text_as_r3table
    EXPORTING
      table  = gt_line
    EXCEPTIONS
      OTHERS = 1.
  IF sy-subrc NE 0.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
               WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.

*     send table to control for Price Family
  CLEAR ls_txt.
  ls_txt-line = zarn_reg_prfam-prfam_comments.
*        APPEND ls_txt-line TO gt_line_prfam.

  CLEAR: gt_line_prfam[].
  SPLIT zarn_reg_prfam-prfam_comments AT cl_abap_char_utilities=>newline INTO TABLE gt_line_prfam[].
*        SPLIT zarn_reg_prfam-prfam_comments AT '#' INTO TABLE gt_line_prfam[].
*        DELETE gt_line_prfam[] WHERE line IS INITIAL.

  CALL METHOD go_reg_editor_prfam->set_text_as_r3table
    EXPORTING
      table  = gt_line_prfam
    EXCEPTIONS
      OTHERS = 1.
  IF sy-subrc NE 0.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
               WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.


* Set Descriptions and Ingredients statements in text editors
  PERFORM set_text_editor_descr.


  " fill the version on the GUI...
  PERFORM fill_version_desc.
  PERFORM enrich_data.

ENDFORM.                    " PROCESS_WORKLIST_TO_SCREEN
*&---------------------------------------------------------------------*
*&      Form  GET_OTHER_TABLE_DATA
*&---------------------------------------------------------------------*
FORM get_other_table_data  USING      pt_key          TYPE ztarn_key.


  FIELD-SYMBOLS <ls_prod_uom_t> TYPE zarn_prod_uom_t.

  IF pt_key[] IS NOT INITIAL.                                           "++3174 JKH 24.03.2017
* AReNa: Product Version Master
    SELECT * FROM zarn_prd_version INTO TABLE gt_prd_version
      FOR ALL ENTRIES IN pt_key
            WHERE idno    EQ pt_key-idno
              AND version EQ pt_key-version.
  ENDIF.

  IF gt_prd_version[] IS NOT INITIAL.                                     "++3174 JKH 24.03.2017
* AReNa: Control Records
    SELECT * FROM zarn_control INTO TABLE gt_control
      FOR ALL ENTRIES IN gt_prd_version
            WHERE guid    EQ gt_prd_version-guid.

* AReNa: Change Category Header
    SELECT * FROM zarn_cc_hdr INTO TABLE gt_cc_hdr
      FOR ALL ENTRIES IN  gt_prd_version
            WHERE idno            EQ  gt_prd_version-idno
              AND national_ver_id EQ  gt_prd_version-version.
* AReNa: Version Status
    SELECT * FROM zarn_ver_status INTO TABLE gt_ver_status
      FOR ALL ENTRIES IN  gt_prd_version
            WHERE idno            EQ  gt_prd_version-idno.
  ENDIF.

*Source of Supply Keys
*  SELECT * INTO TABLE gt_tmbwt FROM tmbwt
*           WHERE spras = sy-langu.

* Articles: Product hierarchies: Texts
  SELECT * INTO TABLE gt_brands FROM wrf_brands
           WHERE brand_type EQ '1'
* Defect 205 - include brand type 3 as well
              OR brand_type EQ '3'.

* Articles: Product hierarchies: Texts
  SELECT * INTO TABLE gt_t179t FROM t179t
           WHERE spras = sy-langu.

* AReNa: Product UOM Text
  SELECT * INTO TABLE gt_zarn_prod_uom_t FROM zarn_prod_uom_t.



* Category Manager Role
  SELECT * INTO TABLE gt_md_category[] FROM zmd_category.                         "++3174 JKH 21.03.2017

* Online Categories
  SELECT * INTO TABLE gt_onl_category FROM zonl_category. "#EC CI_NOWHERE        "++ONLD-835 JKH 16.05.2017

* Business unit and level
  SELECT * INTO TABLE gt_bu_level FROM zonl_bu_level.                             "++ONLD-835 JKH 16.05.2017


* convert to upper case SAP UOM Category (SAP_UOM_CAT)
  LOOP AT gt_zarn_prod_uom_t ASSIGNING <ls_prod_uom_t>.
    TRANSLATE <ls_prod_uom_t>-sap_uom_cat TO UPPER CASE.

  ENDLOOP.

* AReNa: Read UOM details
  CALL FUNCTION 'ZARN_READ_UOM_CATEGORY'
    IMPORTING
      it_uom_cat = gt_zarn_uom_cat.

* get reference Purchase Organisation
  SELECT SINGLE low INTO gv_ref_purch_org
         FROM tvarvc
         WHERE name EQ 'ZPURCH_99_ORG'
           AND type EQ 'S'
           AND numb EQ '0000'
           AND sign EQ 'I'
           AND opti EQ 'EQ'.

  CLEAR: gt_nsap_leg_uom[].
  SELECT * FROM zmd_nsap_leg_uom
    INTO CORRESPONDING FIELDS OF TABLE gt_nsap_leg_uom[].


* get reference Purchase Organisation
  SELECT low INTO TABLE gt_hyb_int_code
    FROM tvarvc
    WHERE name EQ 'ZARN_GUI_UOM_ADDITION_SCENARIO'
    AND type EQ 'S'
    AND sign EQ 'I'
    AND opti EQ 'EQ'.

*  Allergen Text
  SELECT * INTO TABLE gt_zarn_allergen_t FROM zarn_allergen_t WHERE spras = sy-langu.

* Supply Chain Dropdown texts - 9000004661
* Launch Priority Text
  SELECT * INTO TABLE gt_zarn_launchp_t FROM zarn_launchp_t.

* Ordering Approach Type Text
  SELECT * INTO TABLE gt_zarn_order_app_t FROM zarn_order_app_t.

* Deletion Code Type Text
  SELECT * INTO TABLE gt_zarn_npd_del_t FROM zarn_npd_del_t.
* END OF CHANGE 9000004661

ENDFORM.                    " GET_OTHER_TABLE_DATA
*&---------------------------------------------------------------------*
*&      Form  NAT_ADDITIVE_ALV
*&---------------------------------------------------------------------*

FORM nat_additive_alv .
  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo.

  IF go_nat_addi_alv IS INITIAL.
* Nutritional Information
    CREATE OBJECT go_nacc04
      EXPORTING
        container_name = gc_nacc04.

* Create ALV grid
    CREATE OBJECT go_nat_addi_alv
      EXPORTING
        i_parent = go_nacc04.


*Call GRID
    PERFORM alv_build_fieldcat USING gc_alv_nat_addi  CHANGING lt_fieldcat.
** Exclude all edit functions in this example since we do not need them:
    PERFORM exclude_tb_functions_display CHANGING lt_exclude.

    ls_layout-cwidth_opt = abap_true.
    ls_layout-zebra      = abap_true.
    ls_layout-no_toolbar = abap_true.

    CALL METHOD go_nat_addi_alv->set_table_for_first_display
      EXPORTING
        is_layout                     = ls_layout
        it_toolbar_excluding          = lt_exclude
      CHANGING
        it_outtab                     = gt_zarn_additives  "gt_nat_alv_ingr
        it_fieldcatalog               = lt_fieldcat
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.

    IF sy-subrc IS NOT INITIAL.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

  ELSE.

    CALL METHOD go_nat_addi_alv->refresh_table_display.
  ENDIF.
ENDFORM.                    " NAT_ADDITIVE_ALV
*&---------------------------------------------------------------------*
*&      Form  CHECK_REG_CHANGES
*&---------------------------------------------------------------------*
* Check Regional Data changes if any
*----------------------------------------------------------------------*
FORM check_reg_changes CHANGING fc_v_data_change.


  DATA:
    ls_tabname        TYPE ty_s_table_mastr,
    ls_reg_data       TYPE zsarn_reg_data,
    lt_reg_data       TYPE ztarn_reg_data,
    lt_reg_data_del   TYPE ztarn_reg_data,
    lv_where_nat      TYPE string,
    lv_where_reg      TYPE string,
    lv_itabname       TYPE char50,
    lv_reg_itabname   TYPE char50,
    ls_txt            LIKE LINE OF gt_zarn_reg_txt,

    ls_reg_ean_ui_tmp TYPE zsarn_reg_ean_ui,
    ls_reg_ean_tmp    TYPE zarn_reg_ean,
    ls_reg_uom_ui_tmp TYPE zsarn_reg_uom_ui,
    ls_reg_uom_tmp    TYPE zarn_reg_uom,
    ls_reg_allerg_tmp TYPE zarn_reg_allerg,

    ls_reg_data_db    TYPE zsarn_reg_data,
    ls_reg_data_save  TYPE zsarn_reg_data,

    lv_prfam_string   TYPE string,
    lines             TYPE sy-tabix.



  FIELD-SYMBOLS:
    <itabname>     TYPE table,
    <reg_itabname> TYPE table,
    <table_str>    TYPE any,
    <scr_table>    TYPE any,
    <ls_reg_hdr>   TYPE zarn_reg_hdr.

* clear text table
  CLEAR:
    gt_zarn_reg_txt.

  CLEAR fc_v_data_change.

  LOOP AT gt_tabname INTO ls_tabname
       WHERE arena_table_type EQ gc_reg.

    IF <itabname>     IS ASSIGNED. UNASSIGN <itabname>.     ENDIF.
    IF <scr_table>    IS ASSIGNED. UNASSIGN <scr_table>.    ENDIF.
    IF <reg_itabname> IS ASSIGNED. UNASSIGN <reg_itabname>. ENDIF.

*   Select Data for all the Product tables
    CLEAR: lv_itabname,
           lv_reg_itabname.

    CONCATENATE gc_reg_data ls_tabname-arena_table INTO lv_itabname.
    ASSIGN (lv_itabname) TO <itabname>.
    IF <itabname> IS NOT ASSIGNED.
      CONTINUE.
    ENDIF.
    CONCATENATE gc_gt_prefix ls_tabname-arena_table INTO lv_reg_itabname.
    ASSIGN (lv_reg_itabname) TO <reg_itabname>.
*   workarea
    ASSIGN (ls_tabname-arena_table) TO <scr_table>.

    CASE ls_tabname-arena_table.
      WHEN  gc_alv_reg_hdr.
*     Append workarea to table

        zarn_reg_hdr-mandt   = zarn_products-mandt.

*       for table workarea
        ASSIGN <scr_table>  TO <table_str>.

        IF <table_str> IS NOT INITIAL.
          APPEND <table_str>  TO <itabname>.
        ENDIF.

      WHEN  gc_alv_reg_pf.

*       for table workarea
        ASSIGN <scr_table>  TO <table_str>.

        IF <table_str> IS NOT INITIAL.
          APPEND <table_str>  TO <itabname>.
        ENDIF.

        "IS fix for LRA 2a
      WHEN  'ZARN_REG_SC'.

*       for table workarea
        ASSIGN <scr_table>  TO <table_str>.

        IF <table_str> IS NOT INITIAL.
          APPEND <table_str>  TO <itabname>.
        ENDIF.

      WHEN 'ZARN_REG_EAN'.

        CLEAR ls_reg_ean_ui_tmp.
        LOOP AT <reg_itabname> INTO ls_reg_ean_ui_tmp.

          CLEAR ls_reg_ean_tmp.
          MOVE-CORRESPONDING ls_reg_ean_ui_tmp TO ls_reg_ean_tmp.

          APPEND ls_reg_ean_tmp TO ls_reg_data-zarn_reg_ean[].
        ENDLOOP.

      WHEN 'ZARN_REG_UOM'.

        CLEAR ls_reg_uom_ui_tmp.
        LOOP AT <reg_itabname> INTO ls_reg_uom_ui_tmp.

          CLEAR ls_reg_uom_tmp.
          MOVE-CORRESPONDING ls_reg_uom_ui_tmp TO ls_reg_uom_tmp.

          APPEND ls_reg_uom_tmp TO ls_reg_data-zarn_reg_uom[].
        ENDLOOP.

      WHEN 'ZARN_REG_ARTLINK'.     " ++ONED-217 JKH 21.11.2016
        IF <reg_itabname> IS ASSIGNED AND <itabname> IS ASSIGNED.
*          <itabname> = <reg_itabname>.
          MOVE-CORRESPONDING <reg_itabname> TO <itabname>.
          ls_reg_data-zarn_reg_artlink[] = <itabname>.
        ENDIF.

      WHEN  gc_zarn_reg_allerg. "'ZARN_REG_ALLERG'.
        CLEAR ls_reg_allerg_tmp.

        LOOP AT <reg_itabname> INTO ls_reg_allerg_tmp.

          IF ls_reg_allerg_tmp-allergen_type_reg       IS NOT INITIAL OR
             ls_reg_allerg_tmp-allergen_type_ovr       IS NOT INITIAL OR
             ls_reg_allerg_tmp-allergen_type_ovr_src   IS NOT INITIAL OR
             ls_reg_allerg_tmp-lvl_containment_ovr     IS NOT INITIAL OR
             ls_reg_allerg_tmp-lvl_containment_ovr_src IS NOT INITIAL.

*           creating new record
            IF ls_reg_allerg_tmp-mandt IS INITIAL.
              ls_reg_allerg_tmp-mandt = sy-mandt.
            ENDIF.
            IF ls_reg_allerg_tmp-idno IS INITIAL.
              ls_reg_allerg_tmp-idno = zarn_reg_hdr-idno.
            ENDIF.
*           if only regional entry exists copy override allergen type over
            IF ls_reg_allerg_tmp-allergen_type IS INITIAL.
              ls_reg_allerg_tmp-allergen_type = ls_reg_allerg_tmp-allergen_type_ovr.
            ENDIF.

            APPEND ls_reg_allerg_tmp TO ls_reg_data-zarn_reg_allerg.

          ENDIF.
        ENDLOOP.

      WHEN 'ZARN_REG_STR'.     "++ONLD-835 JKH 26.05.2017
        IF <reg_itabname> IS ASSIGNED AND <itabname> IS ASSIGNED.
          MOVE-CORRESPONDING <reg_itabname> TO <itabname>.
          ls_reg_data-zarn_reg_str[] = <itabname>.
        ENDIF.

      WHEN OTHERS.

        IF <reg_itabname> IS ASSIGNED AND <itabname> IS ASSIGNED.
          <itabname> = <reg_itabname>.
        ENDIF.
    ENDCASE.

  ENDLOOP.


* Regional text table - get values from container on screen
* retrieve table from control
  CALL METHOD go_reg_editor->get_text_as_r3table
    IMPORTING
      table  = gt_line
    EXCEPTIONS
      OTHERS = 1.

  IF gt_line IS NOT INITIAL.
    LOOP AT gt_line INTO  ls_txt-line.
      ls_txt-mandt   = sy-mandt.
      ls_txt-idno    = zarn_products-idno.
      ADD 1 TO ls_txt-line_no.
      APPEND ls_txt TO gt_zarn_reg_txt.
    ENDLOOP.

    ls_reg_data-zarn_reg_txt[] = gt_zarn_reg_txt[].
  ENDIF.

* Regional text table - get values from container on screen
* retrieve table from control
  CALL METHOD go_reg_editor_prfam->get_text_as_r3table
    IMPORTING
      table       = gt_line_prfam
      is_modified = DATA(lv_modified)
    EXCEPTIONS
      OTHERS      = 1.

  IF lv_modified NE 0.
    CLEAR lv_prfam_string.
    IF gt_line_prfam IS NOT INITIAL.


      DESCRIBE TABLE gt_line_prfam[] LINES lines.

      LOOP AT gt_line_prfam INTO ls_txt-line.
        IF sy-tabix = lines.
          lv_prfam_string = lv_prfam_string && ls_txt-line.
        ELSE.
          lv_prfam_string = lv_prfam_string && ls_txt-line && cl_abap_char_utilities=>newline.
        ENDIF.
      ENDLOOP.

      zarn_reg_prfam-prfam_comments = lv_prfam_string.
      MODIFY ls_reg_data-zarn_reg_prfam[]
      INDEX 1
      FROM zarn_reg_prfam
      TRANSPORTING prfam_comments.

    ENDIF.
  ENDIF.  " IF lv_modified NE 0


* INS Begin of Change ONLD-835 JKH 26.05.2017
* Get Descriptions from text editors
  PERFORM get_text_editor_descr CHANGING ls_reg_data-zarn_reg_hdr[]
                                         ls_reg_data-zarn_reg_str[].
* INS End of Change ONLD-835 JKH 26.05.2017



* Update deep structures
  ls_reg_data-idno           =  zarn_products-idno.

  APPEND ls_reg_data   TO lt_reg_data.
  APPEND gs_reg_data_o TO lt_reg_data_del.



  CLEAR gs_reg_data_save.
  READ TABLE lt_reg_data INTO gs_reg_data_save
  WITH KEY idno = zarn_products-idno.


  IF gs_reg_data_save IS NOT INITIAL AND gs_reg_data_save-idno IS INITIAL.
    CLEAR gs_reg_data_save-idno.
  ENDIF.

  IF gs_reg_data_save IS INITIAL AND
     gs_reg_data_db   IS INITIAL.
    EXIT.
  ENDIF.

* Database Data
  SORT gs_reg_data_db-zarn_reg_hdr.
  SORT gs_reg_data_db-zarn_reg_banner.
  SORT gs_reg_data_db-zarn_reg_ean.
  SORT gs_reg_data_db-zarn_reg_pir.
  SORT gs_reg_data_db-zarn_reg_uom.
  SORT gs_reg_data_db-zarn_reg_artlink.  " ++ONED-217 JKH 21.11.2016
  SORT gs_reg_data_db-zarn_reg_onlcat.   "++ONLD-835 JKH 26.05.2017
  SORT gs_reg_data_db-zarn_reg_str.      "++ONLD-835 JKH 26.05.2017
  SORT gs_reg_data_db-zarn_reg_allerg.

  SORT gs_reg_data_db-zarn_reg_prfam.
  SORT gs_reg_data_db-zarn_reg_txt.
  SORT gs_reg_data_db-zarn_reg_lst_prc.
  SORT gs_reg_data_db-zarn_reg_dc_sell.
  SORT gs_reg_data_db-zarn_reg_std_ter.
  SORT gs_reg_data_db-zarn_reg_rrp.
  SORT gs_reg_data_db-zarn_reg_hsno.



* New to be Saved Values
  SORT gs_reg_data_save-zarn_reg_hdr.
  SORT gs_reg_data_save-zarn_reg_banner.
  SORT gs_reg_data_save-zarn_reg_ean.
  SORT gs_reg_data_save-zarn_reg_pir.
  SORT gs_reg_data_save-zarn_reg_uom.
  SORT gs_reg_data_save-zarn_reg_artlink.  " ++ONED-217 JKH 21.11.2016
  SORT gs_reg_data_save-zarn_reg_onlcat.   "++ONLD-835 JKH 26.05.2017
  SORT gs_reg_data_save-zarn_reg_str.      "++ONLD-835 JKH 26.05.2017
  SORT gs_reg_data_save-zarn_reg_allerg.

  SORT gs_reg_data_save-zarn_reg_prfam.
  SORT gs_reg_data_save-zarn_reg_txt.
  SORT gs_reg_data_save-zarn_reg_lst_prc.
  SORT gs_reg_data_save-zarn_reg_dc_sell.
  SORT gs_reg_data_save-zarn_reg_std_ter.
  SORT gs_reg_data_save-zarn_reg_rrp.
  SORT gs_reg_data_save-zarn_reg_hsno.



  ls_reg_data_db   = gs_reg_data_db.
  ls_reg_data_save = gs_reg_data_save.

* Ignore fields from comparision
  IF <ls_reg_hdr> IS ASSIGNED. UNASSIGN <ls_reg_hdr>. ENDIF.
  LOOP AT ls_reg_data_db-zarn_reg_hdr ASSIGNING <ls_reg_hdr>.
    CLEAR : <ls_reg_hdr>-ersda, <ls_reg_hdr>-erzet, <ls_reg_hdr>-ernam,
            <ls_reg_hdr>-laeda, <ls_reg_hdr>-eruet, <ls_reg_hdr>-aenam ,
            <ls_reg_hdr>-version, <ls_reg_hdr>-status, <ls_reg_hdr>-release_status,
            <ls_reg_hdr>-matnr.
  ENDLOOP.

  IF <ls_reg_hdr> IS ASSIGNED. UNASSIGN <ls_reg_hdr>. ENDIF.
  LOOP AT ls_reg_data_save-zarn_reg_hdr ASSIGNING <ls_reg_hdr>.
    CLEAR : <ls_reg_hdr>-ersda, <ls_reg_hdr>-erzet, <ls_reg_hdr>-ernam,
            <ls_reg_hdr>-laeda, <ls_reg_hdr>-eruet, <ls_reg_hdr>-aenam ,
            <ls_reg_hdr>-version, <ls_reg_hdr>-status, <ls_reg_hdr>-release_status,
            <ls_reg_hdr>-matnr.
  ENDLOOP.

  " Compare changes only when IDNO is not initial as existing old design is giving
  " popup message to save changes even though there is no change. To avoid regression impact,
  " IDNO check has been added
  IF ls_reg_data_db-idno   IS NOT INITIAL AND
     ls_reg_data_save-idno IS NOT INITIAL AND
     ls_reg_data_db NE ls_reg_data_save.
    fc_v_data_change = abap_true.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  Process_regional_data
*&---------------------------------------------------------------------*
FORM process_regional_data USING pv_save.

  DATA:
    ls_tabname        TYPE ty_s_table_mastr,
    ls_reg_data       TYPE zsarn_reg_data,
    lt_reg_data       TYPE ztarn_reg_data,
    lt_reg_data_del   TYPE ztarn_reg_data,
    lv_where_nat      TYPE string,
    lv_where_reg      TYPE string,
    lv_itabname       TYPE char50,
    lv_reg_itabname   TYPE char50,
    ls_txt            LIKE LINE OF gt_zarn_reg_txt,

    ls_reg_ean_ui_tmp TYPE zsarn_reg_ean_ui,
    ls_reg_ean_tmp    TYPE zarn_reg_ean,
    ls_reg_uom_ui_tmp TYPE zsarn_reg_uom_ui,
    ls_reg_uom_tmp    TYPE zarn_reg_uom,
    ls_reg_sc_uom_tmp TYPE zsarn_reg_sc_ui,     "Supply Chain
    ls_reg_allerg_tmp TYPE zarn_reg_allerg,

    lv_prfam_string   TYPE string,
*>>>IS1609ins Feature 3162 - AReNa Approval Stabilisation
    lt_idno_err       TYPE ztarn_idno_key.
*<<<IS1609ins Feature 3162 - AReNa Approval Stabilisation



  FIELD-SYMBOLS:
    <itabname>     TYPE table,
    <reg_itabname> TYPE table,
    <table_str>    TYPE any,
    <scr_table>    TYPE any.


  IF gv_display IS INITIAL.
    PERFORM default_non_sap_fields.

    PERFORM default_brand_id_111.   "++CTS-160 JKH 10.01.2017
  ENDIF.


* clear text table
  CLEAR:
    gt_zarn_reg_txt.

  LOOP AT gt_tabname INTO ls_tabname
       WHERE arena_table_type EQ gc_reg.

    IF <itabname>     IS ASSIGNED. UNASSIGN <itabname>.     ENDIF.
    IF <scr_table>    IS ASSIGNED. UNASSIGN <scr_table>.    ENDIF.
    IF <reg_itabname> IS ASSIGNED. UNASSIGN <reg_itabname>. ENDIF.

*   Select Data for all the Product tables
    CLEAR: lv_itabname,
           lv_reg_itabname.

    CONCATENATE gc_reg_data ls_tabname-arena_table INTO lv_itabname.
    ASSIGN (lv_itabname) TO <itabname>.
    IF <itabname> IS NOT ASSIGNED.
      CONTINUE.
    ENDIF.
    CONCATENATE gc_gt_prefix ls_tabname-arena_table INTO lv_reg_itabname.
    ASSIGN (lv_reg_itabname) TO <reg_itabname>.
*   workarea
    ASSIGN (ls_tabname-arena_table) TO <scr_table>.

    CASE ls_tabname-arena_table.
      WHEN  gc_alv_reg_hdr.
*     Append workarea to table

        zarn_reg_hdr-mandt   = zarn_products-mandt.
        zarn_reg_hdr-idno    = zarn_products-idno.
        zarn_reg_hdr-version = zarn_ver_status-current_ver.
        IF zarn_reg_hdr-ernam IS INITIAL.
          zarn_reg_hdr-ersda = sy-datum.
          zarn_reg_hdr-erzet = sy-uzeit.
          zarn_reg_hdr-ernam = sy-uname.
        ENDIF.
        zarn_reg_hdr-laeda = sy-datum.
        zarn_reg_hdr-eruet = sy-uzeit.
        zarn_reg_hdr-aenam = sy-uname.
        IF zarn_reg_hdr-status  EQ gc_status_new.
          zarn_reg_hdr-status = gc_status_wip.
        ENDIF.

*       for table workarea
        ASSIGN <scr_table>  TO <table_str>.
        APPEND <table_str>  TO <itabname>.


      WHEN  gc_alv_reg_pf.

        zarn_reg_prfam-mandt   = zarn_products-mandt.
        zarn_reg_prfam-idno    = zarn_products-idno.

*       for table workarea
        ASSIGN <scr_table>  TO <table_str>.
        APPEND <table_str>  TO <itabname>.

      WHEN 'ZARN_REG_EAN'.

        CLEAR ls_reg_ean_ui_tmp.
        LOOP AT <reg_itabname> INTO ls_reg_ean_ui_tmp.

          CLEAR ls_reg_ean_tmp.
          MOVE-CORRESPONDING ls_reg_ean_ui_tmp TO ls_reg_ean_tmp.

          APPEND ls_reg_ean_tmp TO ls_reg_data-zarn_reg_ean[].
        ENDLOOP.


      WHEN 'ZARN_REG_UOM'.

        CLEAR ls_reg_uom_ui_tmp.
        LOOP AT <reg_itabname> INTO ls_reg_uom_ui_tmp.

          CLEAR ls_reg_uom_tmp.
          MOVE-CORRESPONDING ls_reg_uom_ui_tmp TO ls_reg_uom_tmp.

          APPEND ls_reg_uom_tmp TO ls_reg_data-zarn_reg_uom[].
        ENDLOOP.

      WHEN 'ZARN_REG_ARTLINK'.     " ++ONED-217 JKH 21.11.2016
        IF <reg_itabname> IS ASSIGNED AND <itabname> IS ASSIGNED.
*          <itabname> = <reg_itabname>.
          MOVE-CORRESPONDING <reg_itabname> TO <itabname>.

          ls_reg_data-zarn_reg_artlink[] = <itabname>.
        ENDIF.


      WHEN 'ZARN_REG_STR'.     "++ONLD-835 JKH 26.05.2017
        IF <reg_itabname> IS ASSIGNED AND <itabname> IS ASSIGNED.
          MOVE-CORRESPONDING <reg_itabname> TO <itabname>.
          ls_reg_data-zarn_reg_str[] = <itabname>.
        ENDIF.

      WHEN gc_zarn_reg_allerg.  "Allergen Type Overide
        LOOP AT <reg_itabname> INTO ls_reg_allerg_tmp.

          IF ls_reg_allerg_tmp-allergen_type_reg   IS NOT INITIAL OR
             ls_reg_allerg_tmp-allergen_type_ovr   IS NOT INITIAL OR
             ls_reg_allerg_tmp-lvl_containment_ovr IS NOT INITIAL.

            IF ls_reg_allerg_tmp-allergen_type IS INITIAL.
              ls_reg_allerg_tmp-allergen_type = ls_reg_allerg_tmp-allergen_type_ovr.
            ENDIF.
            IF ls_reg_allerg_tmp-mandt IS INITIAL.
              ls_reg_allerg_tmp-mandt = sy-mandt.
            ENDIF.
            IF ls_reg_allerg_tmp-idno IS INITIAL.
              ls_reg_allerg_tmp-idno = zarn_reg_hdr-idno.
            ENDIF.

            APPEND ls_reg_allerg_tmp TO ls_reg_data-zarn_reg_allerg.
          ENDIF.

        ENDLOOP.

      WHEN gc_zarn_reg_sc.   "Supply Chain 9000004661
        zarn_reg_sc-mandt   = zarn_products-mandt.
        zarn_reg_sc-idno    = zarn_products-idno.

*       for table workarea
        ASSIGN <scr_table>  TO <table_str>.
        APPEND <table_str>  TO <itabname>.
      WHEN OTHERS.

        IF <reg_itabname> IS ASSIGNED AND <itabname> IS ASSIGNED.
          <itabname> = <reg_itabname>.
        ENDIF.
    ENDCASE.

  ENDLOOP.

* Regional text table - get values from container on screen
* retrieve table from control
  CALL METHOD go_reg_editor->get_text_as_r3table
    IMPORTING
      table  = gt_line
    EXCEPTIONS
      OTHERS = 1.

  IF gt_line IS NOT INITIAL.
    LOOP AT gt_line INTO  ls_txt-line.
      ls_txt-mandt   = sy-mandt.
      ls_txt-idno    = zarn_products-idno.
      ADD 1 TO ls_txt-line_no.
      APPEND ls_txt TO gt_zarn_reg_txt.
    ENDLOOP.

    ls_reg_data-zarn_reg_txt[] = gt_zarn_reg_txt[].
  ENDIF.

* Regional text table - get values from container on screen
* retrieve table from control
  CALL METHOD go_reg_editor_prfam->get_text_as_r3table
    IMPORTING
      table  = gt_line_prfam
    EXCEPTIONS
      OTHERS = 1.

  DATA lines TYPE sy-tabix.


  DESCRIBE TABLE gt_line_prfam[] LINES lines.

  LOOP AT gt_line_prfam INTO ls_txt-line.
    IF sy-tabix = lines.
      lv_prfam_string = lv_prfam_string && ls_txt-line.
    ELSE.
      lv_prfam_string = lv_prfam_string && ls_txt-line && cl_abap_char_utilities=>newline.
    ENDIF.
  ENDLOOP.

  IF gt_line_prfam IS NOT INITIAL.

    zarn_reg_prfam-prfam_comments = lv_prfam_string.
    MODIFY ls_reg_data-zarn_reg_prfam[]
    INDEX 1
    FROM zarn_reg_prfam
    TRANSPORTING prfam_comments.

  ENDIF.


* INS Begin of Change ONLD-835 JKH 26.05.2017
* Get Descriptions from text editors
  PERFORM get_text_editor_descr CHANGING ls_reg_data-zarn_reg_hdr[]
                                         ls_reg_data-zarn_reg_str[].
* INS End of Change ONLD-835 JKH 26.05.2017

*-- >>> B-03401: Arena: Regional Data Update from PIM
  PERFORM check_reg_nat CHANGING ls_reg_data-zarn_reg_hdr[]
                                 ls_reg_data-zarn_reg_str[].
*-- <<< B-03401


* Update deep structures
  ls_reg_data-idno           =  zarn_products-idno.

  APPEND ls_reg_data   TO lt_reg_data.
  APPEND gs_reg_data_o TO lt_reg_data_del.



  IF pv_save IS NOT INITIAL.
    gs_reg_data_db = gs_reg_data_save = ls_reg_data.
  ENDIF.





**  Validation
  PERFORM validation_check_report USING ls_reg_data
                                        gs_prod_data_o
*>>>IS1609ins Feature 3162 - AReNa Approval Stabilisation
                                CHANGING lt_idno_err.
*<<<IS1609ins Feature 3162 - AReNa Approval Stabilisation




  IF pv_save IS NOT INITIAL.

    CALL FUNCTION 'ZARN_REGIONAL_DB_UPDATE'   " Update Module
      EXPORTING
        it_reg_data       = lt_reg_data
        it_reg_data_old   = lt_reg_data_del
*>>>IS1609ins Feature 3162 - AReNa Approval Stabilisation
        it_idno_val_error = lt_idno_err.

    "we want explicit commit to release exclusive approval locks
    COMMIT WORK AND WAIT.

    "and continue with optimistic lock again
    IF go_approval IS BOUND.
      TRY.
          go_approval->enqueue_appr_idno_or_wait( zif_enq_mode_c=>optimistic ).
        CATCH zcx_excep_cls.
          MESSAGE a099.
      ENDTRY.
    ENDIF.

*<<<IS1609ins Feature 3162 - AReNa Approval Stabilisation

*   Also update the global regional table
    MODIFY TABLE gt_reg_data FROM ls_reg_data.

*   assign new data as old for next change if any
    gs_reg_data_o =  ls_reg_data.



*   clear variable as changes are now saved
    CLEAR gv_data_change.
    MESSAGE s000 WITH zarn_products-fan_id TEXT-209 .
  ENDIF.

ENDFORM.                    " Process_regional_data
*&---------------------------------------------------------------------*
*&      Form  NAT_ALLERGEN_ALV
*&---------------------------------------------------------------------*
FORM nat_allergen_alv .
  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo.

  IF go_nat_alle_alv IS INITIAL.
* Nutritional Information
    CREATE OBJECT go_necc06
      EXPORTING
        container_name = gc_necc06.

* Create ALV grid
    CREATE OBJECT go_nat_alle_alv
      EXPORTING
        i_parent = go_necc06.


*Call GRID
    PERFORM alv_build_fieldcat USING gc_alv_nat_alle  CHANGING lt_fieldcat.
** Exclude all edit functions in this example since we do not need them:
    PERFORM exclude_tb_functions_display CHANGING lt_exclude.

    ls_layout-cwidth_opt = abap_true.
    ls_layout-zebra      = abap_true.
    ls_layout-no_toolbar = abap_true.

    CALL METHOD go_nat_alle_alv->set_table_for_first_display
      EXPORTING
        is_layout                     = ls_layout
        it_toolbar_excluding          = lt_exclude
      CHANGING
        it_outtab                     = gt_zarn_allergen
        it_fieldcatalog               = lt_fieldcat
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.
    IF sy-subrc IS NOT INITIAL.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

  ELSE.

    CALL METHOD go_nat_alle_alv->refresh_table_display.
  ENDIF.
ENDFORM.                    " NAT_ALLERGEN_ALV
*&---------------------------------------------------------------------*
*&      Form  NAT_ZARN_DIET_ALV
*&---------------------------------------------------------------------*
FORM nat_zarn_diet_alv .
  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo.

  IF go_nat_diet_alv IS INITIAL.
* Nutritional Information
    CREATE OBJECT go_ndcc07
      EXPORTING
        container_name = gc_ndcc07.

* Create ALV grid
    CREATE OBJECT go_nat_diet_alv
      EXPORTING
        i_parent = go_ndcc07.


*Call GRID
    PERFORM alv_build_fieldcat USING gc_alv_nat_diet  CHANGING lt_fieldcat.
** Exclude all edit functions in this example since we do not need them:
    PERFORM exclude_tb_functions_display CHANGING lt_exclude.

    ls_layout-cwidth_opt = abap_true.
    ls_layout-zebra = abap_true.
    ls_layout-no_toolbar = abap_true.

    CALL METHOD go_nat_diet_alv->set_table_for_first_display
      EXPORTING
        is_layout                     = ls_layout
        it_toolbar_excluding          = lt_exclude
      CHANGING
        it_outtab                     = gt_zarn_diet_info
        it_fieldcatalog               = lt_fieldcat
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.
    IF sy-subrc IS NOT INITIAL.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

  ELSE.

    CALL METHOD go_nat_diet_alv->refresh_table_display.
  ENDIF.
ENDFORM.                    " NAT_ZARN_DIET_ALV
*&---------------------------------------------------------------------*
*&      Form  NAT_ZARN_CHEM_ALV
*&---------------------------------------------------------------------*
FORM nat_zarn_chem_alv .
  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo.

  IF go_nat_chem_alv IS INITIAL.
* Nutritional Information
    CREATE OBJECT go_nccc08
      EXPORTING
        container_name = gc_nccc08.

* Create ALV grid
    CREATE OBJECT go_nat_chem_alv
      EXPORTING
        i_parent = go_nccc08.


*Call GRID
    PERFORM alv_build_fieldcat USING gc_alv_nat_chem  CHANGING lt_fieldcat.
** Exclude all edit functions in this example since we do not need them:
    PERFORM exclude_tb_functions_display CHANGING lt_exclude.

    ls_layout-cwidth_opt = abap_true.
    ls_layout-zebra = abap_true.
    ls_layout-no_toolbar = abap_true.

    CALL METHOD go_nat_chem_alv->set_table_for_first_display
      EXPORTING
        is_layout                     = ls_layout
        it_toolbar_excluding          = lt_exclude
      CHANGING
        it_outtab                     = gt_zarn_chem_char
        it_fieldcatalog               = lt_fieldcat
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.

    IF sy-subrc IS NOT INITIAL.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

  ELSE.

    CALL METHOD go_nat_chem_alv->refresh_table_display.
  ENDIF.
ENDFORM.                    " NAT_ZARN_CHEM_ALV
*&---------------------------------------------------------------------*
*&      Form  NAT_ORGANISM_ALV
*&---------------------------------------------------------------------*
FORM nat_organism_alv .
  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo.

  IF go_nat_organ_alv IS INITIAL.
* Nutritional Information
    CREATE OBJECT go_nocc09
      EXPORTING
        container_name = gc_nocc09.

* Create ALV grid
    CREATE OBJECT go_nat_organ_alv
      EXPORTING
        i_parent = go_nocc09.


*Call GRID
    PERFORM alv_build_fieldcat USING gc_alv_nat_bioorg  CHANGING lt_fieldcat.
** Exclude all edit functions in this example since we do not need them:
    PERFORM exclude_tb_functions_display CHANGING lt_exclude.

    ls_layout-cwidth_opt = abap_true.
    ls_layout-zebra = abap_true.
    ls_layout-no_toolbar = abap_true.


    CALL METHOD go_nat_organ_alv->set_table_for_first_display
      EXPORTING
        is_layout                     = ls_layout
        it_toolbar_excluding          = lt_exclude
      CHANGING
        it_outtab                     = gt_zarn_bioorganism
        it_fieldcatalog               = lt_fieldcat
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.
    IF sy-subrc IS NOT INITIAL.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

  ELSE.

    CALL METHOD go_nat_organ_alv->refresh_table_display.
  ENDIF.
ENDFORM.                    " NAT_ORGANISM_ALV
*&---------------------------------------------------------------------*
*&      Form  MANAGE_KEY_FIELD_VALUES
*&---------------------------------------------------------------------*
FORM manage_key_field_values  USING    pv_alv_reg TYPE tabname
                                       p_er_sender TYPE REF TO cl_gui_alv_grid
                              CHANGING p_er_data_changed TYPE REF TO cl_alv_changed_data_protocol
                                       pv_data_change   TYPE xfeld.

  DATA:
    lv_ean11            TYPE ean11,
    lt_mod_cells        TYPE lvc_t_modi,
    ls_mod_cells        TYPE lvc_s_modi,
    ls_deleted_rows     TYPE lvc_s_moce,
    ls_roid             TYPE lvc_s_roid,

    ls_zarn_reg_ean     LIKE LINE OF gt_zarn_reg_ean,
    ls_zarn_reg_uom     LIKE LINE OF gt_zarn_reg_uom,
    lt_zarn_reg_uom_tmp LIKE gt_zarn_reg_uom,
    ls_zarn_reg_uom_tmp LIKE LINE OF gt_zarn_reg_uom,

*    ls_zarn_reg_lst_prc LIKE LINE OF gt_zarn_reg_lst_prc,
    lv_prfza            TYPE tntp-prfza,
    lv_checkdigit_ok    TYPE boolean,
    lv_ean              TYPE ean11,
    ls_reg_pir          LIKE LINE OF gt_zarn_reg_pir,
    lv_lifnr            TYPE lifnr,
    lv_pir_error        TYPE flag,                   "++3181 JKH 23.05.2017
    ltr_uom             TYPE RANGE OF meinh,
    lsr_uom             LIKE LINE OF ltr_uom,
    ls_zarn_uom_cat     LIKE LINE OF gt_zarn_uom_cat,
    ls_marm             LIKE LINE OF gt_marm_uom,
    lt_fieldcat         TYPE lvc_t_fcat,
    lr_meinh            TYPE RANGE OF meins,
    ls_meinh            LIKE LINE OF lr_meinh,
    ls_artlink_det      TYPE ty_s_artlink_det,        " ++ONED-217 JKH 21.11.2016
    ls_art_link         TYPE zarn_art_link,           " ++ONED-217 JKH 21.11.2016
    lv_hdr_scn          TYPE zarn_hdr_scn,            " ++ONED-217 JKH 21.11.2016
    lv_matnr_exist      TYPE matnr,                   " ++ONED-217 JKH 21.11.2016
    lv_artlink_err      TYPE flag,
    lv_curr_uom         TYPE flag,
    ls_onl_category     LIKE LINE OF gt_onl_category. "++ONLD-835 JKH 16.05.2017


  FIELD-SYMBOLS:
    <lfs_mod_rows>         TYPE table,
    <lfs_mod_rows_uom>     LIKE LINE OF gt_zarn_reg_uom,
    <lfs_mod_rows_rb>      LIKE LINE OF gt_zarn_reg_banner,
    <lfs_mod_rows_pf>      LIKE LINE OF gt_zarn_reg_prfam,
    <lfs_mod_rows_ean>     LIKE LINE OF gt_zarn_reg_ean,
    <lfs_mod_rows_pir>     LIKE LINE OF gt_zarn_reg_pir,
    <lfs_zarn_reg_rrp>     LIKE LINE OF gt_zarn_reg_rrp,
    <lfs_mod_rows_pir_all> LIKE LINE OF gt_zarn_reg_pir,
    <lfs_mod_rows_artlink> LIKE LINE OF gt_zarn_reg_artlink,   " ++ONED-217 JKH 21.11.2016
    <lfs_mod_rows_onlcat>  LIKE LINE OF gt_zarn_reg_onlcat,    "++ONLD-835 JKH 16.05.2017
    <lfs_mod_rows_allerg>  LIKE LINE OF gt_zarn_reg_allerg,
*    <lfs_zarn_reg_dc_sell> LIKE LINE OF gt_zarn_reg_dc_sell,
*    <lfs_zarn_reg_lst_prc> LIKE LINE OF gt_zarn_reg_lst_prc,
    <lfs_zarn_reg_std_ter> LIKE LINE OF gt_zarn_reg_std_ter,
    <lfs_reg_pir>          LIKE LINE OF gt_zarn_reg_pir,
    <ls_zarn_reg_uom>      LIKE LINE OF gt_zarn_reg_uom,
    <ls_zarn_reg_uom_tmp>  LIKE LINE OF gt_zarn_reg_uom,
    <ls_zarn_reg_ean>      LIKE LINE OF gt_zarn_reg_ean,
    <ls_zarn_reg_artlink>  LIKE LINE OF gt_zarn_reg_artlink,   " ++ONED-217 JKH 21.11.2016
    <ls_zarn_reg_onlcat>   LIKE LINE OF gt_zarn_reg_onlcat,    "++ONLD-835 JKH 16.05.2017
    <ls_zarn_reg_allerg>   LIKE LINE OF gt_zarn_reg_allerg.

  CONSTANTS:
    lc_sales_org  TYPE vkorg  VALUE '1000',
    lc_prefix_brc TYPE char02 VALUE '02',
    lc_prefix_sng TYPE char03 VALUE '020',
    lc_filler_brc TYPE char05 VALUE '00000',

    lc_eantp_brc  TYPE numtp VALUE 'ZS',
    lc_eantp_sng  TYPE numtp VALUE 'ZG',
    lc_eantp_plu  TYPE numtp VALUE 'ZP'.

*_________________________________________________________________________________
*  Modification
*_________________________________________________________________________________

  LOOP AT p_er_data_changed->mt_mod_cells INTO ls_mod_cells.

    ASSIGN p_er_data_changed->mp_mod_rows->* TO <lfs_mod_rows>.

    CASE pv_alv_reg.

      WHEN  gc_alv_reg_uom OR gc_alv_reg_sc.
        READ TABLE <lfs_mod_rows> ASSIGNING <lfs_mod_rows_uom> INDEX ls_mod_cells-tabix.
        IF sy-subrc EQ 0.
*          CLEAR ls_zarn_reg_uom.

          READ TABLE gt_zarn_reg_uom ASSIGNING <ls_zarn_reg_uom> INDEX ls_mod_cells-row_id.
          IF sy-subrc EQ 0.
*            CLEAR gt_filter_uom[].
** Set Filter to not show blank MEINH by default
*            CALL METHOD go_reg_uom_alv->get_filter_criteria
*              IMPORTING
*                et_filter = gt_filter_uom[].


*         based on the field clicked !
            IF ls_mod_cells-fieldname EQ gc_uom_l_meins
              AND <lfs_mod_rows_uom>-lower_meinh IS NOT INITIAL.

              lsr_uom-sign = 'I'.
              lsr_uom-option = 'EQ'.
*-- Get the possible UOM's
              LOOP AT gt_zarn_uom_cat INTO ls_zarn_uom_cat
                WHERE cat_seqno LE <lfs_mod_rows_uom>-cat_seqno.

                READ TABLE gt_zarn_reg_uom TRANSPORTING NO FIELDS
                  WITH KEY meinh = ls_zarn_uom_cat-uom.
                IF sy-subrc EQ 0.
                  lsr_uom-low = ls_zarn_uom_cat-uom.
                  APPEND lsr_uom TO ltr_uom.
                ENDIF.
              ENDLOOP.

* Invalid SAP Lower UOM
              IF <lfs_mod_rows_uom>-lower_meinh NOT IN ltr_uom[].
                MESSAGE s016 DISPLAY LIKE 'E'.
              ENDIF.
            ENDIF.





            IF ls_mod_cells-fieldname EQ gc_uom_l_meins OR
               ls_mod_cells-fieldname EQ gc_uom_meins.

*INS Begin of IR5061015 JKH 06.09.2016
              IF ls_mod_cells-fieldname EQ 'MEINH'.

* Remove EAN where MEINH is changed.
* Example, if MEINH was IN2 changed to IN3 then delete EANs where MEINH is IN2
                IF <lfs_mod_rows_uom>-meinh NE <ls_zarn_reg_uom>-meinh
                   AND <lfs_mod_rows_uom>-meinh IS NOT INITIAL.

                  DELETE gt_zarn_reg_ean WHERE meinh IS INITIAL.
                  DELETE gt_zarn_reg_ean WHERE meinh EQ <ls_zarn_reg_uom>-meinh.

* Corresponding SAP Lower UOM and associated EANs are adjusted
                  MESSAGE s086 DISPLAY LIKE 'W'.
                ENDIF.

                IF <lfs_mod_rows_uom>-meinh IS NOT INITIAL.
                  <ls_zarn_reg_uom>-meinh = <lfs_mod_rows_uom>-meinh.
                ENDIF.

                lt_zarn_reg_uom_tmp[] = gt_zarn_reg_uom[].

                IF <ls_zarn_reg_uom_tmp> IS ASSIGNED. UNASSIGN <ls_zarn_reg_uom_tmp>. ENDIF.
                READ TABLE lt_zarn_reg_uom_tmp[] ASSIGNING <ls_zarn_reg_uom_tmp>
                WITH KEY idno                 = <ls_zarn_reg_uom>-idno
                         uom_category         = <ls_zarn_reg_uom>-uom_category
                         pim_uom_code         = <ls_zarn_reg_uom>-pim_uom_code
                         hybris_internal_code = <ls_zarn_reg_uom>-hybris_internal_code
                         lower_uom            = <ls_zarn_reg_uom>-lower_uom
                         lower_child_uom      = <ls_zarn_reg_uom>-lower_child_uom.
                IF sy-subrc = 0.
                  <ls_zarn_reg_uom_tmp> = <lfs_mod_rows_uom>.

* Default PIR data from UOM data dynamically from screen
                  PERFORM default_pir_from_uom_scr USING lt_zarn_reg_uom_tmp[].
                ENDIF.
              ENDIF.  " IF ls_mod_cells-fieldname EQ 'MEINH'
*INS END of IR5061015 JKH 06.09.2016


* If any Unit attribute is assigned, SAP UOM or SAP LUOM cant be set to blank
              IF ( <lfs_mod_rows_uom>-unit_base   = abap_true OR
                   <lfs_mod_rows_uom>-unit_purord = abap_true OR
                   <lfs_mod_rows_uom>-sales_unit  = abap_true OR
                   <lfs_mod_rows_uom>-issue_unit  = abap_true ) AND
                <lfs_mod_rows_uom>-meinh IS INITIAL.

                <lfs_mod_rows_uom>-meinh = <ls_zarn_reg_uom>-meinh.
                <lfs_mod_rows_uom>-lower_meinh = <ls_zarn_reg_uom>-lower_meinh.
* SAP UOM is required for Unit attribute
                MESSAGE s082 DISPLAY LIKE 'E'.
                EXIT.
              ENDIF.


*INS Begin of change IR5061015 JKH 07.09.2016
              IF <lfs_mod_rows_uom>-meinh IS INITIAL.
                CLEAR <lfs_mod_rows_uom>-lower_meinh.

* Clearing of SAP UOM will clear the corresponding SAP Lower UOM as well
* and deletion of associated EANs

                IF <ls_zarn_reg_uom_tmp> IS ASSIGNED. UNASSIGN <ls_zarn_reg_uom_tmp>. ENDIF.
                LOOP AT gt_zarn_reg_uom ASSIGNING <ls_zarn_reg_uom_tmp>
                  WHERE lower_meinh = <ls_zarn_reg_uom>-meinh.
                  CLEAR <ls_zarn_reg_uom_tmp>-lower_meinh.
                ENDLOOP.

                DELETE gt_zarn_reg_ean WHERE meinh IS INITIAL.
                DELETE gt_zarn_reg_ean WHERE meinh EQ <ls_zarn_reg_uom>-meinh.
*                DELETE gt_zarn_reg_ean WHERE meinh NOT IN lr_meinh[].

                CLEAR: <ls_zarn_reg_uom>-lower_meinh.

* Corresponding SAP Lower UOM and associated EANs are adjusted
                MESSAGE s086 DISPLAY LIKE 'W'.

              ELSE.

                IF <ls_zarn_reg_uom_tmp> IS ASSIGNED. UNASSIGN <ls_zarn_reg_uom_tmp>. ENDIF.

* Check if Hybris int code is LOCAL, then only default high level units
                READ TABLE gt_hyb_int_code TRANSPORTING NO FIELDS
                  WITH KEY hyb_int_code = <lfs_mod_rows_uom>-hybris_internal_code.
                IF sy-subrc NE 0.

* The SAP Lower UOM must be auto-populated and auto-cleared based on the
* Hybris Lower UOM + Lower Child UOM, like following INN is populated because
* for IN_4 & EA_1>INN is the SAP UOM

                  LOOP AT gt_zarn_reg_uom ASSIGNING <ls_zarn_reg_uom_tmp>.

                    CLEAR lv_curr_uom.
                    IF <ls_zarn_reg_uom_tmp>-idno                 = <lfs_mod_rows_uom>-idno                  AND
                       <ls_zarn_reg_uom_tmp>-uom_category         = <lfs_mod_rows_uom>-uom_category          AND
                       <ls_zarn_reg_uom_tmp>-pim_uom_code         = <lfs_mod_rows_uom>-pim_uom_code          AND
                       <ls_zarn_reg_uom_tmp>-hybris_internal_code = <lfs_mod_rows_uom>-hybris_internal_code  AND
                       <ls_zarn_reg_uom_tmp>-lower_uom            = <lfs_mod_rows_uom>-lower_uom             AND
                       <ls_zarn_reg_uom_tmp>-lower_child_uom      = <lfs_mod_rows_uom>-lower_child_uom.
                      lv_curr_uom = abap_true.
                    ENDIF.

                    IF <ls_zarn_reg_uom_tmp>-uom_category NE 'PALLET'.
                      CLEAR ls_zarn_reg_uom_tmp.
                      READ TABLE gt_zarn_reg_uom INTO ls_zarn_reg_uom_tmp
                      WITH KEY idno                 = <ls_zarn_reg_uom_tmp>-idno
                               pim_uom_code         = <ls_zarn_reg_uom_tmp>-lower_uom
                               lower_uom            = <ls_zarn_reg_uom_tmp>-lower_child_uom.
                      IF sy-subrc = 0.
                        IF lv_curr_uom = abap_true.
                          <lfs_mod_rows_uom>-lower_meinh     = ls_zarn_reg_uom_tmp-meinh.
                        ENDIF.
                        <ls_zarn_reg_uom_tmp>-lower_meinh  = ls_zarn_reg_uom_tmp-meinh.
                      ELSE.
                        CLEAR: <ls_zarn_reg_uom_tmp>-lower_meinh.
                        IF lv_curr_uom = abap_true.
                          CLEAR: <lfs_mod_rows_uom>-lower_meinh.
                        ENDIF.
                      ENDIF.
                    ELSEIF <ls_zarn_reg_uom_tmp>-uom_category EQ 'PALLET'.
                      CLEAR ls_zarn_reg_uom_tmp.
                      READ TABLE gt_zarn_reg_uom INTO ls_zarn_reg_uom_tmp
                      WITH KEY idno                 = <ls_zarn_reg_uom_tmp>-idno
                               uom_category         = 'LAYER'
                               pim_uom_code         = <ls_zarn_reg_uom_tmp>-pim_uom_code
                               lower_uom            = <ls_zarn_reg_uom_tmp>-lower_uom.
                      IF sy-subrc = 0.
                        IF lv_curr_uom = abap_true.
                          <lfs_mod_rows_uom>-lower_meinh = ls_zarn_reg_uom_tmp-meinh.
                        ENDIF.
                        <ls_zarn_reg_uom_tmp>-lower_meinh  = ls_zarn_reg_uom_tmp-meinh.
                      ELSE.
                        CLEAR: <ls_zarn_reg_uom_tmp>-lower_meinh.
                        IF lv_curr_uom = abap_true.
                          CLEAR: <lfs_mod_rows_uom>-lower_meinh.
                        ENDIF.
                      ENDIF.
                    ENDIF.  " IF <lfs_mod_rows_uom>-uom_category NE 'PALLET'
                  ENDLOOP.  " LOOP AT gt_zarn_reg_uom ASSIGNING <ls_zarn_reg_uom_tmp>

                ENDIF.  " READ TABLE gt_hyb_int_code TRANSPORTING NO FIELDS


              ENDIF.  " IF <lfs_mod_rows_uom>-meinh       IS INITIAL
*INS End of change IR5061015 JKH 07.09.2016

            ENDIF.  " IF ls_mod_cells-fieldname EQ gc_uom_l_meins OR gc_uom_meins


            IF ls_mod_cells-fieldname EQ gc_uom_meins.

              CLEAR ls_zarn_uom_cat.
              READ TABLE gt_zarn_uom_cat INTO ls_zarn_uom_cat
                WITH KEY uom = <lfs_mod_rows_uom>-meinh.
              IF sy-subrc EQ 0.
                <ls_zarn_reg_uom>-cat_seqno = ls_zarn_uom_cat-cat_seqno.
                <ls_zarn_reg_uom>-seqno = ls_zarn_uom_cat-seqno.
              ENDIF.

*              <ls_zarn_reg_uom>-num_base_units = 1.
*              <ls_zarn_reg_uom>-factor_of_base_units = 1.

              " for local UOMs only
              IF ( <ls_zarn_reg_uom>-hybris_internal_code IS INITIAL
                OR <ls_zarn_reg_uom>-uom_category IS INITIAL )
                AND <ls_zarn_reg_uom>-pim_uom_code+10(5) CO ' 1234567890_'.
**              Enter Hybris Code/UOM Category
                MESSAGE s065 DISPLAY LIKE 'E'.
                RETURN.
              ENDIF.

              CLEAR lsr_uom.
              FREE ltr_uom[].
              lsr_uom-sign = 'I'.
              lsr_uom-option = 'EQ'.
*-- Get the possible UOM's
              LOOP AT gt_zarn_uom_cat INTO ls_zarn_uom_cat
                WHERE uom_category EQ <lfs_mod_rows_uom>-uom_category.
                lsr_uom-low = ls_zarn_uom_cat-uom.
                APPEND lsr_uom TO ltr_uom.
              ENDLOOP.

              IF <lfs_mod_rows_uom>-meinh NOT IN ltr_uom[]
                AND <lfs_mod_rows_uom>-meinh IS NOT INITIAL.
                " Invalid SAP UOM
                MESSAGE s073 DISPLAY LIKE 'E'.
                RETURN.
              ENDIF.

              IF <ls_zarn_reg_uom>-meinh NE <lfs_mod_rows_uom>-meinh AND <ls_zarn_reg_uom>-meinh IS NOT INITIAL.
                " check EAN regional table for usage and give error in case of used
                READ TABLE gt_zarn_reg_ean TRANSPORTING NO FIELDS
                  WITH KEY meinh = <ls_zarn_reg_uom>-meinh.
                IF sy-subrc EQ 0.
                  " SAP UOM & with UOM Code & is used in Regional EAN
                  MESSAGE s068 DISPLAY LIKE 'E'
                    WITH <ls_zarn_reg_uom>-meinh <ls_zarn_reg_uom>-pim_uom_code.
                  RETURN.
                ENDIF.

                CLEAR ls_zarn_reg_uom.
                READ TABLE gt_zarn_reg_uom INTO ls_zarn_reg_uom
                  WITH KEY lower_meinh = <ls_zarn_reg_uom>-meinh.
                IF sy-subrc EQ 0.
                  " SAP UOM & with UOM Code & is used in SAP lower UOM with UOM code &
                  MESSAGE s069 DISPLAY LIKE 'E'
                  WITH <ls_zarn_reg_uom>-meinh <ls_zarn_reg_uom>-pim_uom_code
                       ls_zarn_reg_uom-pim_uom_code.
                ENDIF.
              ENDIF.

            ENDIF.

            IF ls_mod_cells-fieldname EQ gc_hyb_code
              AND <lfs_mod_rows_uom>-hybris_internal_code IS NOT INITIAL.

              READ TABLE gt_hyb_int_code TRANSPORTING NO FIELDS
                WITH KEY hyb_int_code = <lfs_mod_rows_uom>-hybris_internal_code.
              IF sy-subrc NE 0.
                MESSAGE s067 DISPLAY LIKE 'E' WITH <lfs_mod_rows_uom>-hybris_internal_code.
                CLEAR <lfs_mod_rows_uom>-hybris_internal_code.
              ENDIF.
            ENDIF.



*INS Begin of IR5119489 JKH 01.09.2016
* Calculate High level units the value based on entered numerator and denominator
            IF ls_mod_cells-fieldname EQ 'NUM_BASE_UNITS' OR
               ls_mod_cells-fieldname EQ 'FACTOR_OF_BASE_UNITS' OR
               ls_mod_cells-fieldname EQ 'MEINH' OR
               ls_mod_cells-fieldname EQ 'LOWER_MEINH' OR
               ls_mod_cells-fieldname EQ 'HYBRIS_INTERNAL_CODE'.

* Check if Hybris int code is LOCAL, then only default high level units
              READ TABLE gt_hyb_int_code TRANSPORTING NO FIELDS
                WITH KEY hyb_int_code = <lfs_mod_rows_uom>-hybris_internal_code.
              IF sy-subrc = 0.

                CLEAR ls_zarn_reg_uom.
                IF <lfs_mod_rows_uom>-lower_meinh IS NOT INITIAL.
                  READ TABLE gt_zarn_reg_uom[] INTO ls_zarn_reg_uom
                  WITH KEY idno                 = <lfs_mod_rows_uom>-idno
                           meinh                = <lfs_mod_rows_uom>-lower_meinh.
                ENDIF.

                IF ls_zarn_reg_uom-num_base_units IS INITIAL.
                  ls_zarn_reg_uom-num_base_units = 1.
                ENDIF.

                IF ls_zarn_reg_uom-factor_of_base_units IS INITIAL.
                  ls_zarn_reg_uom-factor_of_base_units = 1.
                ENDIF.

                IF <lfs_mod_rows_uom>-num_base_units IS INITIAL.
                  <lfs_mod_rows_uom>-num_base_units = 1.
                ENDIF.

                IF <lfs_mod_rows_uom>-factor_of_base_units IS INITIAL.
                  <lfs_mod_rows_uom>-factor_of_base_units = 1.
                ENDIF.

                <lfs_mod_rows_uom>-higher_level_units =
                  ( <lfs_mod_rows_uom>-num_base_units / <lfs_mod_rows_uom>-factor_of_base_units ) /
                  ( ls_zarn_reg_uom-num_base_units   / ls_zarn_reg_uom-factor_of_base_units ).

                <ls_zarn_reg_uom>-higher_level_units = <lfs_mod_rows_uom>-higher_level_units.



              ENDIF.  "  READ TABLE gt_hyb_int_code TRANSPORTING NO FIELDS
            ENDIF.  " IF ls_mod_cells-fieldname EQ 'NUM_BASE_UNITS'/'FACTOR_OF_BASE_UNITS'
*INS END of IR5119489 JKH 01.09.2016

          ENDIF.

*          PERFORM refresh_reg_alv USING go_reg_uom_alv lt_fieldcat .
**       refresh Screen values
          cl_gui_cfw=>set_new_ok_code( new_code = 'SYSTEM').

** flag a change
*          pv_data_change = abap_true.

        ENDIF.   " READ TABLE <lfs_mod_rows> ASSIGNING <lfs_mod_rows_uom>

      WHEN gc_alv_reg_rb.
        READ TABLE <lfs_mod_rows> ASSIGNING <lfs_mod_rows_rb> INDEX ls_mod_cells-tabix.
        IF sy-subrc EQ 0.
*         based on the field clicked !
          IF ls_mod_cells-fieldname EQ gc_vrkme AND <lfs_mod_rows_rb>-vrkme IS NOT INITIAL.
*-- Check the possible UOM's
            READ TABLE gt_zarn_reg_uom TRANSPORTING NO FIELDS
              WITH KEY meinh = <lfs_mod_rows_rb>-vrkme.
            IF sy-subrc NE 0 AND gt_marm_uom[] IS INITIAL.
              MESSAGE s017 DISPLAY LIKE 'E'.
            ELSEIF sy-subrc NE 0 AND gt_marm_uom[] IS NOT INITIAL.
              READ TABLE gt_marm_uom TRANSPORTING NO FIELDS
                WITH KEY uom = <lfs_mod_rows_rb>-vrkme.
              IF sy-subrc NE 0.
                MESSAGE s017 DISPLAY LIKE 'E'.
              ENDIF.
            ENDIF.

          ELSEIF ls_mod_cells-fieldname EQ 'ZZCATMAN' OR ls_mod_cells-fieldname EQ 'SSTUF'.
            IF ls_mod_cells-value IS INITIAL AND
              ( <lfs_mod_rows_rb>-banner EQ zcl_constants=>gc_banner_4000 OR
                <lfs_mod_rows_rb>-banner  EQ zcl_constants=>gc_banner_5000 OR
                <lfs_mod_rows_rb>-banner EQ zcl_constants=>gc_banner_6000 ) AND
              gv_banner_def_ret IS NOT INITIAL.

              MESSAGE s038 WITH ls_mod_cells-fieldname 'Retail Ranging' DISPLAY LIKE 'E'.

            ELSEIF ls_mod_cells-value IS INITIAL AND
              <lfs_mod_rows_rb>-banner EQ zcl_constants=>gc_banner_3000 AND
              gv_banner_def_gil IS NOT INITIAL.

              MESSAGE s038 WITH ls_mod_cells-fieldname 'Wholesale Ranging' DISPLAY LIKE 'E'.

            ELSEIF ls_mod_cells-value IS INITIAL AND
              <lfs_mod_rows_rb>-banner EQ zcl_constants=>gc_banner_1000 AND
              zarn_reg_hdr-bwscl EQ gc_bwscl_2
              AND ls_mod_cells-fieldname NE 'SSTUF'.

              MESSAGE s038 WITH ls_mod_cells-fieldname 'DC Source of Supply' DISPLAY LIKE 'E'.

            ENDIF.

          ENDIF.


* flag a change
          pv_data_change = abap_true.
        ENDIF.

      WHEN gc_alv_reg_pir.

        READ TABLE <lfs_mod_rows> ASSIGNING <lfs_mod_rows_pir> INDEX ls_mod_cells-tabix.
        IF sy-subrc EQ 0.
*         based on the field clicked !
          CASE ls_mod_cells-fieldname.
              " EINE
            WHEN  gc_po_eine.
              LOOP AT gt_zarn_reg_pir ASSIGNING <lfs_reg_pir>.
                " SAME record as selected
                IF <lfs_mod_rows_pir>-order_uom_pim EQ <lfs_reg_pir>-order_uom_pim AND
                  <lfs_mod_rows_pir>-hybris_internal_code EQ <lfs_reg_pir>-hybris_internal_code AND
                  <lfs_mod_rows_pir>-lower_uom EQ <lfs_reg_pir>-lower_uom AND
                  <lfs_mod_rows_pir>-lower_child_uom EQ <lfs_reg_pir>-lower_child_uom.

                  <lfs_reg_pir>-pir_rel_eine = ls_mod_cells-value.

                  " Check Vendor
                  IF ls_mod_cells-value EQ abap_true AND
                    <lfs_mod_rows_pir>-lifnr IS INITIAL.
                    MESSAGE s013 DISPLAY LIKE 'E'.
                  ELSEIF ls_mod_cells-value EQ abap_true AND
                    <lfs_mod_rows_pir>-lifnr IS NOT INITIAL.
                    SELECT SINGLE lifnr FROM lfa1 INTO lv_lifnr "#EC CI_SEL_NESTED
                      WHERE lifnr EQ <lfs_mod_rows_pir>-lifnr.
                    IF sy-subrc NE 0.
                      MESSAGE s013 DISPLAY LIKE 'E'.
                    ENDIF.
                  ENDIF.

                  " set allowed ZERO price to 'X' in case of price is initial
                  IF <lfs_reg_pir>-netpr IS INITIAL AND ls_mod_cells-value EQ abap_true.
                    MESSAGE s015 WITH <lfs_reg_pir>-lifnr <lfs_reg_pir>-meinh DISPLAY LIKE 'E'.
                  ENDIF.

* INS Begin of Change 3181 JKH 23.05.2017
                  IF <lfs_mod_rows_pir>-pir_rel_eine EQ abap_true AND <lfs_mod_rows_pir>-ekkol IS NOT INITIAL.
                    CLEAR lv_pir_error.
* Check Condition Group
                    PERFORM check_condition_group USING <lfs_mod_rows_pir>
                                               CHANGING lv_pir_error.
                    IF lv_pir_error = abap_true.
* Vendor & : Condition group & doesn't exist in condition table A718
                      MESSAGE s151 WITH <lfs_mod_rows_pir>-lifnr
                                        <lfs_mod_rows_pir>-ekkol
                                        DISPLAY LIKE 'E'.
                    ENDIF.  " IF lv_pir_error = abap_true
                  ENDIF.  " IF <lfs_mod_rows_pir>-pir_rel_eine EQ abap_true
* INS End of Change 3181 JKH 23.05.2017

                  " CHECKED
                  IF ls_mod_cells-value EQ abap_true.
                    READ TABLE gt_zarn_reg_pir INTO ls_reg_pir
                      WITH KEY gln_no = <lfs_reg_pir>-gln_no pir_rel_eina = abap_true.
                    IF sy-subrc NE 0.
                      <lfs_reg_pir>-pir_rel_eina = ls_mod_cells-value.
                    ENDIF.
                    " UN-CHECKED
                  ELSEIF ls_mod_cells-value EQ abap_false.
                    READ TABLE gt_zarn_reg_pir INTO ls_reg_pir
                      WITH KEY gln_no = <lfs_reg_pir>-gln_no pir_rel_eina = abap_true.
                    IF sy-subrc EQ 0.
                      MESSAGE s014 WITH 'Purchase PIR' <lfs_reg_pir>-lifnr DISPLAY LIKE 'E'.
                    ENDIF.
                    CLEAR <lfs_reg_pir>-allow_zero_price.
                  ENDIF.

                  " OTHER records
                ELSEIF <lfs_mod_rows_pir>-gln_no EQ <lfs_reg_pir>-gln_no.
                  CLEAR <lfs_reg_pir>-pir_rel_eine.
                  CLEAR <lfs_reg_pir>-allow_zero_price.
                ENDIF.
              ENDLOOP.


              " EINA
            WHEN  gc_po_eina.
              LOOP AT gt_zarn_reg_pir ASSIGNING <lfs_reg_pir>.
                " SAME record as selected
                IF <lfs_mod_rows_pir>-order_uom_pim EQ <lfs_reg_pir>-order_uom_pim AND
                  <lfs_mod_rows_pir>-hybris_internal_code EQ <lfs_reg_pir>-hybris_internal_code AND
                  <lfs_mod_rows_pir>-lower_uom EQ <lfs_reg_pir>-lower_uom AND
                  <lfs_mod_rows_pir>-lower_child_uom EQ <lfs_reg_pir>-lower_child_uom.

                  <lfs_reg_pir>-pir_rel_eina = ls_mod_cells-value.

                  " Check Vendor
                  IF ls_mod_cells-value EQ abap_true AND
                    <lfs_mod_rows_pir>-lifnr IS INITIAL.
                    MESSAGE s013 DISPLAY LIKE 'E'.
                  ELSEIF ls_mod_cells-value EQ abap_true AND
                    <lfs_mod_rows_pir>-lifnr IS NOT INITIAL.
                    SELECT SINGLE lifnr FROM lfa1 INTO lv_lifnr "#EC CI_SEL_NESTED
                      WHERE lifnr EQ <lfs_mod_rows_pir>-lifnr.
                    IF sy-subrc NE 0.
                      MESSAGE s013 DISPLAY LIKE 'E'.
                    ENDIF.
                  ENDIF.

                  IF ls_mod_cells-value EQ abap_true.
                    READ TABLE gt_zarn_reg_pir INTO ls_reg_pir
                      WITH KEY gln_no = <lfs_reg_pir>-gln_no pir_rel_eine = abap_true.
                    IF sy-subrc NE 0.
                      <lfs_reg_pir>-pir_rel_eine = ls_mod_cells-value.
                      " set allowed ZERO price to 'X' in case of price is initial
                      IF <lfs_reg_pir>-netpr IS INITIAL.
                        MESSAGE s015 WITH <lfs_reg_pir>-lifnr <lfs_reg_pir>-meinh DISPLAY LIKE 'E'.
                      ENDIF.
                    ENDIF.
                    " UN-CHECKED
                  ELSEIF ls_mod_cells-value EQ abap_false.
                    READ TABLE gt_zarn_reg_pir INTO ls_reg_pir
                      WITH KEY gln_no = <lfs_reg_pir>-gln_no pir_rel_eine = abap_true.
                    IF sy-subrc EQ 0.
                      MESSAGE s014 WITH 'General PIR' <lfs_reg_pir>-lifnr DISPLAY LIKE 'E'.
                    ENDIF.
                  ENDIF.

                  " OTHERS - clear per GLN
                ELSEIF <lfs_mod_rows_pir>-gln_no EQ <lfs_reg_pir>-gln_no.
                  CLEAR <lfs_reg_pir>-pir_rel_eina.
                ENDIF.
              ENDLOOP.


              " 'RELIF'
            WHEN gc_norm_vend.
              LOOP AT gt_zarn_reg_pir ASSIGNING <lfs_reg_pir>.
                " SAME record as selected
                IF <lfs_mod_rows_pir>-order_uom_pim EQ <lfs_reg_pir>-order_uom_pim AND
                  <lfs_mod_rows_pir>-hybris_internal_code EQ <lfs_reg_pir>-hybris_internal_code AND
                  <lfs_mod_rows_pir>-lower_uom EQ <lfs_reg_pir>-lower_uom AND
                  <lfs_mod_rows_pir>-lower_child_uom EQ <lfs_reg_pir>-lower_child_uom.

                  <lfs_reg_pir>-relif = ls_mod_cells-value.

                  IF ls_mod_cells-value EQ abap_true AND
                    <lfs_mod_rows_pir>-lifnr IS INITIAL.
                    MESSAGE s013 DISPLAY LIKE 'E'.
                  ELSEIF ls_mod_cells-value EQ abap_true AND
                    <lfs_mod_rows_pir>-lifnr IS NOT INITIAL.
                    SELECT SINGLE lifnr FROM lfa1 INTO lv_lifnr "#EC CI_SEL_NESTED
                      WHERE lifnr EQ <lfs_mod_rows_pir>-lifnr.
                    IF sy-subrc NE 0.
                      MESSAGE s013 DISPLAY LIKE 'E'.
                    ENDIF.
                  ENDIF.

                  IF <lfs_reg_pir>-lifnr NE 'MULTIPLE'.
                    zarn_reg_pir-lifnr = <lfs_reg_pir>-lifnr.
                    PERFORM get_vendor_text.
                  ELSE.
                    CLEAR zarn_reg_pir-lifnr.
                    PERFORM get_vendor_text.
                  ENDIF.


* INS Begin of Change 3181 JKH 23.05.2017
                  IF <lfs_mod_rows_pir>-relif = abap_true.
                    CLEAR lv_pir_error.
* Check if selectedregular vendor has the latest last changed date
                    PERFORM check_latest_regular_vendor USING <lfs_mod_rows_pir>
                                                              <lfs_reg_pir>
                                                              gt_zarn_reg_pir
                                                              CHANGING lv_pir_error.
                    IF lv_pir_error = abap_true.
* Vendor & : New regular vendor doesn't have latest PIR date change
                      MESSAGE s078 WITH <lfs_mod_rows_pir>-lifnr
                                        DISPLAY LIKE 'W'.
                    ENDIF.  " IF lv_pir_error = abap_true



* Validation for condition group between old and new regular vendor
* Get old PIR with regular vendor tick
                    READ TABLE gs_reg_data_db-zarn_reg_pir[] INTO DATA(ls_reg_pir_old)
                    WITH KEY relif = abap_true.
                    IF sy-subrc = 0.
* Get EINE of OLD
                      READ TABLE gs_reg_data_db-zarn_reg_pir[] INTO DATA(ls_relif_eine)
                      WITH KEY idno         = ls_reg_pir_old-idno
                               lifnr        = ls_reg_pir_old-lifnr
                               pir_rel_eine = abap_true.
                      IF sy-subrc = 0.
                        IF ls_relif_eine-ekkol IS NOT INITIAL AND ls_reg_pir_old-ekkol IS INITIAL.
* Previous reg. vendor has condition group but missing in new reg. vendor
                          MESSAGE s150 DISPLAY LIKE 'W'.
                        ENDIF.  " IF <lfs_reg_pir_old>-ekkol IS NOT INITIAL AND <lfs_mod_rows_pir>-ekkol IS INITIAL
                      ENDIF.
                    ENDIF.  " IF sy-subrc = 0

                  ENDIF.  " IF <lfs_mod_rows_pir> = abap_true
* INS End of Change 3181 JKH 23.05.2017


                  " OTHERS
                ELSE.
                  CLEAR <lfs_reg_pir>-relif.
                ENDIF.
              ENDLOOP.

**       if a vendor was changed on the screen then
            WHEN gc_alv_lifnr.
*         Re read the Vendor master values
              PERFORM  get_vendor_purch_org_data USING    gv_ref_purch_org
                                                 CHANGING <lfs_mod_rows_pir>.
*              MODIFY gt_zarn_reg_pir FROM <lfs_mod_rows_pir> INDEX ls_mod_cells-row_id.
*              IF sy-subrc EQ 0.
              LOOP AT gt_zarn_reg_pir ASSIGNING <lfs_mod_rows_pir_all>
                WHERE gln_no EQ <lfs_mod_rows_pir>-gln_no.

                <lfs_mod_rows_pir_all>-lifnr = <lfs_mod_rows_pir>-lifnr.
                <lfs_mod_rows_pir_all>-aplfz = <lfs_mod_rows_pir>-aplfz.
                <lfs_mod_rows_pir_all>-ekgrp = <lfs_mod_rows_pir>-ekgrp.
              ENDLOOP.
*              ENDIF.

            WHEN gc_alv_lifab.
              IF  <lfs_mod_rows_pir>-lifab GT <lfs_mod_rows_pir>-lifbi.
                MESSAGE s064 DISPLAY LIKE 'E'.
              ENDIF.

            WHEN gc_alv_lifbi.
              IF  <lfs_mod_rows_pir>-lifab GT <lfs_mod_rows_pir>-lifbi.
                MESSAGE s064 DISPLAY LIKE 'E'.
              ENDIF.


            WHEN gc_po_ekkol.             "++3181 JKH 23.05.2017
              IF <lfs_mod_rows_pir>-pir_rel_eine EQ abap_true AND <lfs_mod_rows_pir>-ekkol IS NOT INITIAL.
                CLEAR lv_pir_error.
* Check Condition Group
                PERFORM check_condition_group USING <lfs_mod_rows_pir>
                                           CHANGING lv_pir_error.
                IF lv_pir_error = abap_true.
* Vendor & : Condition group & doesn't exist in condition table A718
                  MESSAGE s151 WITH <lfs_mod_rows_pir>-lifnr
                                    <lfs_mod_rows_pir>-ekkol
                                    DISPLAY LIKE 'E'.
                ENDIF.  " IF lv_pir_error = abap_true
              ENDIF.  " IF <lfs_mod_rows_pir>-pir_rel_eine EQ abap_true



            WHEN OTHERS.

          ENDCASE.

*          CALL METHOD go_reg_pir_alv->refresh_table_display.
***       refresh Screen values
          cl_gui_cfw=>set_new_ok_code( new_code = 'SYSTEM').

* flag a change
          pv_data_change = abap_true.

        ENDIF.


      WHEN gc_alv_reg_gtn.

        READ TABLE <lfs_mod_rows> ASSIGNING <lfs_mod_rows_ean> INDEX ls_mod_cells-tabix.
        IF sy-subrc = 0.
          <lfs_mod_rows_ean>-mandt = sy-mandt.
          <lfs_mod_rows_ean>-idno  = zarn_products-idno.

* Default PLU Article - Screen 111
          PERFORM default_plu_article_111 USING <lfs_mod_rows_ean>.

*       If the PLU flag is ticked, then ZP barcode is required to be entered by user
          IF zarn_reg_hdr-plu_article IS NOT INITIAL.
*         if ZP barcode added,
            ls_zarn_reg_ean =  <lfs_mod_rows_ean>.


* Begin of Change IR5216639 Jitin 07.10.2016
**EAN check digit generation (PLU issue)

            " only check only in case EAN category is there
            IF ls_zarn_reg_ean-eantp IS NOT INITIAL AND
               ls_zarn_reg_ean-ean11 IS NOT INITIAL.

              CLEAR lv_prfza.
              CALL FUNCTION 'EAN_GET_PRFZA'
                EXPORTING
                  i_numtp = ls_zarn_reg_ean-eantp
                IMPORTING
                  e_prfza = lv_prfza.

              " only check for EAN categories with check degit algos.
              IF lv_prfza IS NOT INITIAL.
                lv_ean = ls_zarn_reg_ean-ean11.
                " check if check digot is OK
                CLEAR lv_checkdigit_ok.
                CALL FUNCTION 'EAN_VERIFY_CHECKDIGIT'
                  IMPORTING
                    checkdigit_ok = lv_checkdigit_ok
                  CHANGING
                    i_ean         = lv_ean.
                " if check digit is NOT OK, then convert EAN with check digit
                IF lv_checkdigit_ok IS INITIAL.
                  PERFORM convert_ean_with_checkdigit USING ls_zarn_reg_ean-eantp
                                                   CHANGING ls_zarn_reg_ean-ean11.
                  <lfs_mod_rows_ean>-ean11 = ls_zarn_reg_ean-ean11.


                  READ TABLE gt_zarn_reg_ean ASSIGNING <ls_zarn_reg_ean>
                    INDEX ls_mod_cells-row_id.
                  IF sy-subrc EQ 0.
                    <ls_zarn_reg_ean>-ean11 = ls_zarn_reg_ean-ean11.
                  ENDIF.
                ENDIF.
              ENDIF.

            ENDIF.
* End of Change IR5216639 Jitin 07.10.2016

*            IF zarn_reg_hdr-scagr           IS NOT  INITIAL AND   "Scales Relevant    "--CI18-124 JKH 06.07.2017
            IF zarn_reg_hdr-scagr         EQ 'YES' AND   "Scales Relevant              "++CI18-124 JKH 06.07.2017
            <lfs_mod_rows_ean>-ean11      IS NOT INITIAL AND   "check there are 5 digits  ** Check Validity of the ZP barcode
*               ls_mod_cells-fieldname       EQ gc_uom_eantp AND
            <lfs_mod_rows_ean>-eantp      EQ lc_eantp_plu.  "'ZP'.
*             ls_mod_cells-value      EQ 'ZP'.
*           SAP Note 545520 - FAQ: International Article Number (EAN/UPC) (especially to do with leading zeros)
*           hence the following code to use conversion exit
              lv_ean11 = <lfs_mod_rows_ean>-ean11.
              PERFORM convert_ean_with_eantp USING <lfs_mod_rows_ean>-eantp CHANGING lv_ean11.

*           automatically add the corresponding ZS barcode
              ls_zarn_reg_ean-eantp = lc_eantp_brc. "'ZS'.
*           ZS barcode format = 02 + 5-digit ZP PLU they have entered + 00000 + C (check digit)
              ls_zarn_reg_ean-ean11 = lc_prefix_brc && lv_ean11(5) && lc_filler_brc.
              PERFORM convert_ean_with_checkdigit   USING ls_zarn_reg_ean-eantp
                                                 CHANGING  ls_zarn_reg_ean-ean11.
              PERFORM convert_ean_with_eantp USING ls_zarn_reg_ean-eantp CHANGING ls_zarn_reg_ean-ean11.

              READ TABLE gt_zarn_reg_ean TRANSPORTING NO FIELDS
              WITH KEY idno  = ls_zarn_reg_ean-idno
                       ean11 = ls_zarn_reg_ean-ean11.
              IF sy-subrc NE 0.
*           add to table
                APPEND ls_zarn_reg_ean TO gt_zarn_reg_ean.
              ENDIF.




*           New barcode type for Shop N Go = ZG
              ls_zarn_reg_ean-eantp = lc_eantp_sng. " 'ZG'.
*           Format (9-digits) = 020 + 5 digit PLU + Check Digit
              ls_zarn_reg_ean-ean11 = lc_prefix_sng && lv_ean11(5).
              PERFORM convert_ean_with_checkdigit  USING ls_zarn_reg_ean-eantp
                                                CHANGING ls_zarn_reg_ean-ean11.
              PERFORM convert_ean_with_eantp USING ls_zarn_reg_ean-eantp CHANGING ls_zarn_reg_ean-ean11.

              READ TABLE gt_zarn_reg_ean TRANSPORTING NO FIELDS
              WITH KEY idno  = ls_zarn_reg_ean-idno
                       ean11 = ls_zarn_reg_ean-ean11.
              IF sy-subrc NE 0.
*           add to table
                APPEND ls_zarn_reg_ean TO gt_zarn_reg_ean.
              ENDIF.

***           refresh Screen values
              cl_gui_cfw=>set_new_ok_code( new_code = 'SYSTEM').
            ENDIF.



          ELSE.



            " check and propose EAN in case of incorrect EAN entered by user
            ls_zarn_reg_ean =  <lfs_mod_rows_ean>.
            " only check only in case EAN category is there
            IF ls_zarn_reg_ean-eantp IS NOT INITIAL AND
               ls_zarn_reg_ean-ean11 IS NOT INITIAL.

              CLEAR lv_prfza.
              CALL FUNCTION 'EAN_GET_PRFZA'
                EXPORTING
                  i_numtp = ls_zarn_reg_ean-eantp
                IMPORTING
                  e_prfza = lv_prfza.

              " only check for EAN categories with check degit algos.
              IF lv_prfza IS NOT INITIAL.
                lv_ean = ls_zarn_reg_ean-ean11.
                " check if check digot is OK
                CLEAR lv_checkdigit_ok.
                CALL FUNCTION 'EAN_VERIFY_CHECKDIGIT'
                  IMPORTING
                    checkdigit_ok = lv_checkdigit_ok
                  CHANGING
                    i_ean         = lv_ean.
                " if check digit is NOT OK, then convert EAN with check digit
                IF lv_checkdigit_ok IS INITIAL.
                  PERFORM convert_ean_with_checkdigit USING ls_zarn_reg_ean-eantp
                                                   CHANGING ls_zarn_reg_ean-ean11.
                  <lfs_mod_rows_ean>-ean11 = ls_zarn_reg_ean-ean11.


                  READ TABLE gt_zarn_reg_ean ASSIGNING <ls_zarn_reg_ean>
                    INDEX ls_mod_cells-row_id.
                  IF sy-subrc EQ 0.
                    <ls_zarn_reg_ean>-ean11 = ls_zarn_reg_ean-ean11.
                  ENDIF.
                ENDIF.
              ENDIF.

            ENDIF.


          ENDIF.  " IF zarn_reg_hdr-plu_article IS NOT INITIAL



* Validate Regional GTINs
          PERFORM validate_reg_gtin_100 USING ls_mod_cells
                                     CHANGING <lfs_mod_rows_ean>
                                              gt_zarn_reg_ean.

*        validation for uom - based on the field clicked !
          IF ls_mod_cells-fieldname EQ gc_uom_meins.

            " DON'T RAISE ERRORS WHEN A NEW LINE IS ADDED TO ALV
            IF lines( p_er_data_changed->mt_mod_cells[] ) EQ 1.

*-- Check the possible UOM's
              READ TABLE gt_zarn_reg_uom TRANSPORTING NO FIELDS
                WITH KEY meinh = <lfs_mod_rows_ean>-meinh.
              IF sy-subrc NE 0 AND gt_marm_uom[] IS INITIAL.
                MESSAGE s018 DISPLAY LIKE 'E'.
              ELSEIF sy-subrc NE 0 AND gt_marm_uom[] IS NOT INITIAL.
                READ TABLE gt_marm_uom TRANSPORTING NO FIELDS
                  WITH KEY uom = <lfs_mod_rows_ean>-meinh.
                IF sy-subrc NE 0.
                  MESSAGE s018 DISPLAY LIKE 'E'.
                ENDIF.
              ENDIF.
            ENDIF.
          ENDIF.




**       refresh Screen values
          cl_gui_cfw=>set_new_ok_code( new_code = 'SYSTEM').


* flag a change
          pv_data_change = abap_true.



        ENDIF.

      WHEN gc_alv_reg_artlink_ui.          " ++ONED-217 JKH 21.11.2016

        READ TABLE <lfs_mod_rows> ASSIGNING <lfs_mod_rows_artlink> INDEX ls_mod_cells-tabix.
        IF sy-subrc EQ 0.

          READ TABLE gt_zarn_reg_artlink  ASSIGNING <ls_zarn_reg_artlink> INDEX ls_mod_cells-row_id.
          IF sy-subrc EQ 0.

            " insertion of a new record
            IF <lfs_mod_rows_artlink> IS INITIAL.
              EXIT.
            ENDIF.

            IF zarn_reg_hdr-matnr IS INITIAL.
* Article Linking can't be maintained as this product is not created yet.
              MESSAGE s117(zarena_msg) DISPLAY LIKE 'E'.
              EXIT.
            ENDIF.

            <lfs_mod_rows_artlink>-mandt = sy-mandt.
            <ls_zarn_reg_artlink>-mandt  = sy-mandt.

            IF <ls_zarn_reg_artlink> = <lfs_mod_rows_artlink>.
              EXIT.
            ENDIF.

            <ls_zarn_reg_artlink>        = <lfs_mod_rows_artlink>.


            CASE ls_mod_cells-fieldname.
              WHEN gc_hdr_matnr.      " HDR_MATNR

                IF <lfs_mod_rows_artlink>-hdr_matnr IS NOT INITIAL.

                  IF lv_artlink_err IS INITIAL.
                    CLEAR lv_matnr_exist.
                    SELECT SINGLE matnr FROM mara INTO lv_matnr_exist "#EC CI_SEL_NESTED
                      WHERE matnr = <lfs_mod_rows_artlink>-hdr_matnr.
                    IF sy-subrc NE 0.
* Article & doesn't exist
                      MESSAGE s116(zarena_msg) DISPLAY LIKE 'E' WITH <lfs_mod_rows_artlink>-hdr_matnr.
                      CLEAR: <lfs_mod_rows_artlink>-hdr_matnr,
                             <ls_zarn_reg_artlink>-hdr_matnr.

                      CLEAR: <lfs_mod_rows_artlink>-hdr_maktx,
                             <lfs_mod_rows_artlink>-hdr_attyp,
                             <lfs_mod_rows_artlink>-hdr_mtart.

                      CLEAR: <ls_zarn_reg_artlink>-hdr_maktx,
                             <ls_zarn_reg_artlink>-hdr_attyp,
                             <ls_zarn_reg_artlink>-hdr_mtart.

                      lv_artlink_err = abap_true.
                    ENDIF.
                  ENDIF.


                  IF lv_artlink_err IS INITIAL.
                    IF <lfs_mod_rows_artlink>-link_matnr IS NOT INITIAL.
                      IF <lfs_mod_rows_artlink>-hdr_matnr  NE zarn_reg_hdr-matnr AND
                         <lfs_mod_rows_artlink>-link_matnr NE zarn_reg_hdr-matnr.
* Either Header article or Linked article must be &
                        MESSAGE s119(zarena_msg) DISPLAY LIKE 'E' WITH zarn_reg_hdr-matnr.
                        CLEAR: <lfs_mod_rows_artlink>-hdr_matnr,
                               <ls_zarn_reg_artlink>-hdr_matnr.

                        CLEAR: <lfs_mod_rows_artlink>-hdr_maktx,
                               <lfs_mod_rows_artlink>-hdr_attyp,
                               <lfs_mod_rows_artlink>-hdr_mtart.

                        CLEAR: <ls_zarn_reg_artlink>-hdr_maktx,
                               <ls_zarn_reg_artlink>-hdr_attyp,
                               <ls_zarn_reg_artlink>-hdr_mtart.

                        lv_artlink_err = abap_true.
                      ENDIF.
                    ENDIF.
                  ENDIF.


                  CLEAR ls_artlink_det.
                  ls_artlink_det = zcl_arn_reg_artlink=>get_article_det(
                            iv_matnr = <lfs_mod_rows_artlink>-hdr_matnr ).
                  IF ls_artlink_det IS NOT INITIAL.
                    <lfs_mod_rows_artlink>-hdr_maktx = ls_artlink_det-maktx.
                    <lfs_mod_rows_artlink>-hdr_attyp = ls_artlink_det-attyp.
                    <lfs_mod_rows_artlink>-hdr_mtart = ls_artlink_det-mtart.

                    <ls_zarn_reg_artlink>-hdr_maktx = ls_artlink_det-maktx.
                    <ls_zarn_reg_artlink>-hdr_attyp = ls_artlink_det-attyp.
                    <ls_zarn_reg_artlink>-hdr_mtart = ls_artlink_det-mtart.

                  ENDIF.

                ELSE.
                  CLEAR: <lfs_mod_rows_artlink>-hdr_maktx,
                         <lfs_mod_rows_artlink>-hdr_attyp,
                         <lfs_mod_rows_artlink>-hdr_mtart.

                  CLEAR: <ls_zarn_reg_artlink>-hdr_maktx,
                         <ls_zarn_reg_artlink>-hdr_attyp,
                         <ls_zarn_reg_artlink>-hdr_mtart.

                  IF lv_artlink_err IS INITIAL.
* Header article and Linked article are required.
                    MESSAGE s118(zarena_msg) DISPLAY LIKE 'E'.
                    lv_artlink_err = abap_true.
                  ENDIF.

                ENDIF.


              WHEN gc_link_matnr.     " LINK_MATNR

                IF <lfs_mod_rows_artlink>-link_matnr IS NOT INITIAL.

                  IF lv_artlink_err IS INITIAL.
                    CLEAR lv_matnr_exist.
                    SELECT SINGLE matnr FROM mara INTO lv_matnr_exist "#EC CI_SEL_NESTED
                      WHERE matnr = <lfs_mod_rows_artlink>-link_matnr.
                    IF sy-subrc NE 0.
* Article & doesn't exist
                      MESSAGE s116(zarena_msg) DISPLAY LIKE 'E' WITH <lfs_mod_rows_artlink>-link_matnr.
                      CLEAR: <lfs_mod_rows_artlink>-link_matnr,
                             <ls_zarn_reg_artlink>-link_matnr.

                      CLEAR: <lfs_mod_rows_artlink>-link_maktx,
                             <lfs_mod_rows_artlink>-link_attyp,
                             <lfs_mod_rows_artlink>-link_mtart.

                      CLEAR: <ls_zarn_reg_artlink>-link_maktx,
                             <ls_zarn_reg_artlink>-link_attyp,
                             <ls_zarn_reg_artlink>-link_mtart.

                      lv_artlink_err = abap_true.
                    ENDIF.
                  ENDIF.

                  IF lv_artlink_err IS INITIAL.
                    IF <lfs_mod_rows_artlink>-hdr_matnr IS NOT INITIAL.
                      IF <lfs_mod_rows_artlink>-hdr_matnr  NE zarn_reg_hdr-matnr AND
                         <lfs_mod_rows_artlink>-link_matnr NE zarn_reg_hdr-matnr.
* Either Header article or Linked article must be &
                        MESSAGE s119(zarena_msg) DISPLAY LIKE 'E' WITH zarn_reg_hdr-matnr.
                        CLEAR: <lfs_mod_rows_artlink>-link_matnr,
                               <ls_zarn_reg_artlink>-link_matnr.

                        CLEAR: <lfs_mod_rows_artlink>-link_maktx,
                               <lfs_mod_rows_artlink>-link_attyp,
                               <lfs_mod_rows_artlink>-link_mtart.

                        CLEAR: <ls_zarn_reg_artlink>-link_maktx,
                               <ls_zarn_reg_artlink>-link_attyp,
                               <ls_zarn_reg_artlink>-link_mtart.

                        lv_artlink_err = abap_true.
                      ENDIF.
                    ENDIF.
                  ENDIF.

                  CLEAR ls_artlink_det.
                  ls_artlink_det = zcl_arn_reg_artlink=>get_article_det(
                            iv_matnr = <lfs_mod_rows_artlink>-link_matnr ).
                  IF ls_artlink_det IS NOT INITIAL.
                    <lfs_mod_rows_artlink>-link_maktx = ls_artlink_det-maktx.
                    <lfs_mod_rows_artlink>-link_attyp = ls_artlink_det-attyp.
                    <lfs_mod_rows_artlink>-link_mtart = ls_artlink_det-mtart.

                    <ls_zarn_reg_artlink>-link_maktx = ls_artlink_det-maktx.
                    <ls_zarn_reg_artlink>-link_attyp = ls_artlink_det-attyp.
                    <ls_zarn_reg_artlink>-link_mtart = ls_artlink_det-mtart.

                  ENDIF.

                ELSE.
                  CLEAR: <lfs_mod_rows_artlink>-link_maktx,
                         <lfs_mod_rows_artlink>-link_attyp,
                         <lfs_mod_rows_artlink>-link_mtart.

                  CLEAR: <ls_zarn_reg_artlink>-link_maktx,
                         <ls_zarn_reg_artlink>-link_attyp,
                         <ls_zarn_reg_artlink>-link_mtart.

                  IF lv_artlink_err IS INITIAL.
* Header article and Linked article are required.
                    MESSAGE s118(zarena_msg) DISPLAY LIKE 'E'.
                    lv_artlink_err = abap_true.
                  ENDIF.

                ENDIF.

              WHEN gc_hdr_scn_drdn.   " HDR_SCN_DRDN
                IF <lfs_mod_rows_artlink>-hdr_scn_drdn IS NOT INITIAL.
                  lv_hdr_scn = <lfs_mod_rows_artlink>-hdr_scn_drdn+0(2).

                  <lfs_mod_rows_artlink>-hdr_scn = lv_hdr_scn.
                  <ls_zarn_reg_artlink>-hdr_scn  = lv_hdr_scn.

                  CLEAR ls_art_link.
                  ls_art_link = zcl_arn_reg_artlink=>get_article_linking_data(
                                            iv_hdr_scn = lv_hdr_scn ).

                  IF ls_art_link IS NOT INITIAL.
                    <lfs_mod_rows_artlink>-link_scn            = ls_art_link-link_scn.
                    <lfs_mod_rows_artlink>-link_scn_drdn+0(2)  = ls_art_link-link_scn.
                    <lfs_mod_rows_artlink>-link_scn_drdn+2(1)  = space.
                    <lfs_mod_rows_artlink>-link_scn_drdn+3(20) = ls_art_link-link_scn_text.

                    <ls_zarn_reg_artlink>-link_scn            = ls_art_link-link_scn.
                    <ls_zarn_reg_artlink>-link_scn_drdn+0(2)  = ls_art_link-link_scn.
                    <ls_zarn_reg_artlink>-link_scn_drdn+2(1)  = space.
                    <ls_zarn_reg_artlink>-link_scn_drdn+3(20) = ls_art_link-link_scn_text.

                  ENDIF.

                ELSE.

                  IF lv_artlink_err IS INITIAL.
* Header scenario is required.
                    MESSAGE s120(zarena_msg) DISPLAY LIKE 'E'.
                    lv_artlink_err = abap_true.
                  ENDIF.

                ENDIF.
            ENDCASE.



            IF lv_artlink_err IS INITIAL.
              LOOP AT gt_zarn_reg_artlink TRANSPORTING NO FIELDS
                WHERE hdr_matnr  = <lfs_mod_rows_artlink>-hdr_matnr
                  AND link_matnr = <lfs_mod_rows_artlink>-link_matnr
                  AND hdr_scn    = <lfs_mod_rows_artlink>-hdr_scn
                  AND link_scn   = <lfs_mod_rows_artlink>-link_scn.

                IF sy-tabix NE ls_mod_cells-row_id.
* Header article, Linked article and Header scenario must be identical.
                  MESSAGE s121 DISPLAY LIKE 'E'.
                  lv_artlink_err = abap_true.
                  CLEAR: <lfs_mod_rows_artlink>-hdr_scn_drdn,  <lfs_mod_rows_artlink>-hdr_scn,
                         <lfs_mod_rows_artlink>-link_scn_drdn, <lfs_mod_rows_artlink>-link_scn.

                  CLEAR: <ls_zarn_reg_artlink>-hdr_scn_drdn,  <ls_zarn_reg_artlink>-hdr_scn,
                         <ls_zarn_reg_artlink>-link_scn_drdn, <ls_zarn_reg_artlink>-link_scn.

                  EXIT.
                ENDIF.
              ENDLOOP.
            ENDIF.


*            MODIFY gt_zarn_reg_artlink FROM <lfs_mod_rows_artlink>
*                                      INDEX ls_mod_cells-row_id.



**       refresh Screen values
            cl_gui_cfw=>set_new_ok_code( new_code = 'SYSTEM').
* flag a change
            pv_data_change = abap_true.

          ENDIF.
        ENDIF.

      WHEN gc_alv_reg_onlcat_ui.          " ++ONLD-835 JKH 16.05.2017
        READ TABLE <lfs_mod_rows> ASSIGNING <lfs_mod_rows_onlcat> INDEX ls_mod_cells-tabix.
        IF sy-subrc EQ 0.

          READ TABLE gt_zarn_reg_onlcat ASSIGNING <ls_zarn_reg_onlcat> INDEX ls_mod_cells-row_id.
          IF sy-subrc EQ 0.

            CASE ls_mod_cells-fieldname.
              WHEN 'CATEGORY_1'.
                CLEAR ls_onl_category.
                READ TABLE gt_onl_category[] INTO ls_onl_category
                WITH KEY onl_catalog = <lfs_mod_rows_onlcat>-onl_catalog
                         onl_level   = '1'
                         category    = <lfs_mod_rows_onlcat>-category_1.
                IF sy-subrc = 0.
                  <lfs_mod_rows_onlcat>-cat_description_1 = ls_onl_category-cat_description.
                  <ls_zarn_reg_onlcat>-cat_description_1 = ls_onl_category-cat_description.
                ENDIF.

                IF <lfs_mod_rows_onlcat>-category_1 IS INITIAL.
                  CLEAR: <lfs_mod_rows_onlcat>-cat_description_1, <ls_zarn_reg_onlcat>-cat_description_1.
                ENDIF.

                IF <lfs_mod_rows_onlcat>-category_1 NE <ls_zarn_reg_onlcat>-category_1.
                  CLEAR: <lfs_mod_rows_onlcat>-category_2, <lfs_mod_rows_onlcat>-category_3,
                         <lfs_mod_rows_onlcat>-category_4, <lfs_mod_rows_onlcat>-category_5,
                         <lfs_mod_rows_onlcat>-category_6.
                  CLEAR: <lfs_mod_rows_onlcat>-cat_description_2, <lfs_mod_rows_onlcat>-cat_description_3,
                         <lfs_mod_rows_onlcat>-cat_description_4, <lfs_mod_rows_onlcat>-cat_description_5,
                         <lfs_mod_rows_onlcat>-cat_description_6.
                  CLEAR: <ls_zarn_reg_onlcat>-category_2, <ls_zarn_reg_onlcat>-category_3,
                         <ls_zarn_reg_onlcat>-category_4, <ls_zarn_reg_onlcat>-category_5,
                         <ls_zarn_reg_onlcat>-category_6.
                  CLEAR: <ls_zarn_reg_onlcat>-cat_description_2, <ls_zarn_reg_onlcat>-cat_description_3,
                         <ls_zarn_reg_onlcat>-cat_description_4, <ls_zarn_reg_onlcat>-cat_description_5,
                         <ls_zarn_reg_onlcat>-cat_description_6.
                ENDIF.


              WHEN 'CATEGORY_2'.
                CLEAR ls_onl_category.
                READ TABLE gt_onl_category[] INTO ls_onl_category
                WITH KEY onl_catalog = <lfs_mod_rows_onlcat>-onl_catalog
                         onl_level   = '2'
                         category    = <lfs_mod_rows_onlcat>-category_2.
                IF sy-subrc = 0.
                  <lfs_mod_rows_onlcat>-cat_description_2 = ls_onl_category-cat_description.
                  <ls_zarn_reg_onlcat>-cat_description_2 = ls_onl_category-cat_description.
                ENDIF.

                IF <lfs_mod_rows_onlcat>-category_2 IS INITIAL.
                  CLEAR: <lfs_mod_rows_onlcat>-cat_description_2, <ls_zarn_reg_onlcat>-cat_description_2.
                ENDIF.

                IF <lfs_mod_rows_onlcat>-category_2 NE <ls_zarn_reg_onlcat>-category_2.
                  CLEAR: <lfs_mod_rows_onlcat>-category_3,
                         <lfs_mod_rows_onlcat>-category_4, <lfs_mod_rows_onlcat>-category_5,
                         <lfs_mod_rows_onlcat>-category_6.
                  CLEAR: <lfs_mod_rows_onlcat>-cat_description_3,
                         <lfs_mod_rows_onlcat>-cat_description_4, <lfs_mod_rows_onlcat>-cat_description_5,
                         <lfs_mod_rows_onlcat>-cat_description_6.
                  CLEAR: <ls_zarn_reg_onlcat>-category_3,
                         <ls_zarn_reg_onlcat>-category_4, <ls_zarn_reg_onlcat>-category_5,
                         <ls_zarn_reg_onlcat>-category_6.
                  CLEAR: <ls_zarn_reg_onlcat>-cat_description_3,
                         <ls_zarn_reg_onlcat>-cat_description_4, <ls_zarn_reg_onlcat>-cat_description_5,
                         <ls_zarn_reg_onlcat>-cat_description_6.
                ENDIF.

              WHEN 'CATEGORY_3'.
                CLEAR ls_onl_category.
                READ TABLE gt_onl_category[] INTO ls_onl_category
                WITH KEY onl_catalog = <lfs_mod_rows_onlcat>-onl_catalog
                         onl_level   = '3'
                         category    = <lfs_mod_rows_onlcat>-category_3.
                IF sy-subrc = 0.
                  <lfs_mod_rows_onlcat>-cat_description_3 = ls_onl_category-cat_description.
                  <ls_zarn_reg_onlcat>-cat_description_3 = ls_onl_category-cat_description.
                ENDIF.

                IF <lfs_mod_rows_onlcat>-category_3 IS INITIAL.
                  CLEAR: <lfs_mod_rows_onlcat>-cat_description_3, <ls_zarn_reg_onlcat>-cat_description_3.
                ENDIF.

                IF <lfs_mod_rows_onlcat>-category_3 NE <ls_zarn_reg_onlcat>-category_3.
                  CLEAR: <lfs_mod_rows_onlcat>-category_4, <lfs_mod_rows_onlcat>-category_5,
                         <lfs_mod_rows_onlcat>-category_6.
                  CLEAR: <lfs_mod_rows_onlcat>-cat_description_4, <lfs_mod_rows_onlcat>-cat_description_5,
                         <lfs_mod_rows_onlcat>-cat_description_6.
                  CLEAR: <ls_zarn_reg_onlcat>-category_4, <ls_zarn_reg_onlcat>-category_5,
                         <ls_zarn_reg_onlcat>-category_6.
                  CLEAR: <ls_zarn_reg_onlcat>-cat_description_4, <ls_zarn_reg_onlcat>-cat_description_5,
                         <ls_zarn_reg_onlcat>-cat_description_6.
                ENDIF.

              WHEN 'CATEGORY_4'.
                CLEAR ls_onl_category.
                READ TABLE gt_onl_category[] INTO ls_onl_category
                WITH KEY onl_catalog = <lfs_mod_rows_onlcat>-onl_catalog
                         onl_level   = '4'
                         category    = <lfs_mod_rows_onlcat>-category_4.
                IF sy-subrc = 0.
                  <lfs_mod_rows_onlcat>-cat_description_4 = ls_onl_category-cat_description.
                  <ls_zarn_reg_onlcat>-cat_description_4 = ls_onl_category-cat_description.
                ENDIF.

                IF <lfs_mod_rows_onlcat>-category_4 IS INITIAL.
                  CLEAR: <lfs_mod_rows_onlcat>-cat_description_4, <ls_zarn_reg_onlcat>-cat_description_4.
                ENDIF.

                IF <lfs_mod_rows_onlcat>-category_4 NE <ls_zarn_reg_onlcat>-category_4.
                  CLEAR: <lfs_mod_rows_onlcat>-category_5, <lfs_mod_rows_onlcat>-category_6.
                  CLEAR: <lfs_mod_rows_onlcat>-cat_description_5, <lfs_mod_rows_onlcat>-cat_description_6.
                  CLEAR: <ls_zarn_reg_onlcat>-category_5, <ls_zarn_reg_onlcat>-category_6.
                  CLEAR: <ls_zarn_reg_onlcat>-cat_description_5, <ls_zarn_reg_onlcat>-cat_description_6.
                ENDIF.

              WHEN 'CATEGORY_5'.
                CLEAR ls_onl_category.
                READ TABLE gt_onl_category[] INTO ls_onl_category
                WITH KEY onl_catalog = <lfs_mod_rows_onlcat>-onl_catalog
                         onl_level   = '5'
                         category    = <lfs_mod_rows_onlcat>-category_5.
                IF sy-subrc = 0.
                  <lfs_mod_rows_onlcat>-cat_description_5 = ls_onl_category-cat_description.
                  <ls_zarn_reg_onlcat>-cat_description_5 = ls_onl_category-cat_description.
                ENDIF.

                IF <lfs_mod_rows_onlcat>-category_5 IS INITIAL.
                  CLEAR: <lfs_mod_rows_onlcat>-cat_description_5, <ls_zarn_reg_onlcat>-cat_description_5.
                ENDIF.

                IF <lfs_mod_rows_onlcat>-category_5 NE <ls_zarn_reg_onlcat>-category_5.
                  CLEAR: <lfs_mod_rows_onlcat>-category_6.
                  CLEAR: <lfs_mod_rows_onlcat>-cat_description_6.
                  CLEAR: <ls_zarn_reg_onlcat>-category_6.
                  CLEAR: <ls_zarn_reg_onlcat>-cat_description_6.
                ENDIF.

              WHEN 'CATEGORY_6'.
                CLEAR ls_onl_category.
                READ TABLE gt_onl_category[] INTO ls_onl_category
                WITH KEY onl_catalog = <lfs_mod_rows_onlcat>-onl_catalog
                         onl_level   = '6'
                         category    = <lfs_mod_rows_onlcat>-category_6.
                IF sy-subrc = 0.
                  <lfs_mod_rows_onlcat>-cat_description_6 = ls_onl_category-cat_description.
                  <ls_zarn_reg_onlcat>-cat_description_6 = ls_onl_category-cat_description.
                ENDIF.

                IF <lfs_mod_rows_onlcat>-category_6 IS INITIAL.
                  CLEAR: <lfs_mod_rows_onlcat>-cat_description_6, <ls_zarn_reg_onlcat>-cat_description_6.
                ENDIF.



            ENDCASE.

**       refresh Screen values
            cl_gui_cfw=>set_new_ok_code( new_code = 'SYSTEM').
* flag a change
            pv_data_change = abap_true.
          ENDIF.
        ENDIF.

      WHEN gc_alv_reg_sc.
        READ TABLE <lfs_mod_rows> ASSIGNING <lfs_mod_rows_allerg> INDEX ls_mod_cells-tabix.
        IF sy-subrc EQ 0.

        ENDIF.
      WHEN OTHERS.

    ENDCASE.

  ENDLOOP.

ENDFORM.                    " MANAGE_KEY_FIELD_VALUES
*&---------------------------------------------------------------------*
*&      Form  GET_CONTROL_DATA
*&---------------------------------------------------------------------*
FORM get_control_data USING pv_uname TYPE sy-uname
                      CHANGING pv_name_text  TYPE ad_namtext.
  DATA:
       ls_user_address TYPE addr3_val.


  CALL FUNCTION 'SUSR_USER_ADDRESS_READ'
    EXPORTING
      user_name              = pv_uname
    IMPORTING
      user_address           = ls_user_address
    EXCEPTIONS
      user_address_not_found = 1
      OTHERS                 = 2.

  IF sy-subrc IS INITIAL.
    pv_name_text = ls_user_address-name_text.
  ELSE.
    CLEAR pv_name_text.
  ENDIF.


ENDFORM.                    " GET_CONTROL_DATA
*&---------------------------------------------------------------------*
*&      Form  VALIDATION_CHECK_REPORT
*&---------------------------------------------------------------------*

FORM validation_check_report  USING    ps_reg_data  TYPE zsarn_reg_data
                                       ps_prod_data TYPE zsarn_prod_data
*>>>IS1609ins Feature 3162 - AReNa Approval Stabilisation
                              CHANGING pct_idno_err TYPE ztarn_idno_key.
*<<<IS1609ins Feature 3162 - AReNa Approval Stabilisation


  DATA:
    "lt_val_output TYPE zarn_t_rl_output,
    "lr_table      TYPE REF TO cl_salv_table,
    lr_layout  TYPE REF TO cl_salv_layout,
    ls_lay_key TYPE salv_s_layout_key,
    lv_header  TYPE lvc_title,
    lr_excep   TYPE REF TO zcx_arn_validation_err.

*    lo_dialogbox_container TYPE REF TO cl_gui_dialogbox_container.

  FREE gt_val_output.

  TRY.
      CALL METHOD gr_validation_engine->validate_arn_single
        EXPORTING
          is_zsarn_prod_data          = ps_prod_data
          is_zsarn_reg_data           = ps_reg_data
          iv_refresh_mstrdata_buffers = abap_true
        IMPORTING
          et_output                   = gt_val_output.
    CATCH zcx_arn_validation_err INTO lr_excep.
      MESSAGE i000(zarena_msg) WITH lr_excep->msgv1 lr_excep->msgv2
                                    lr_excep->msgv3 lr_excep->msgv4.
*        LEAVE LIST-PROCESSING.
  ENDTRY.

*
  IF  gt_val_output IS INITIAL.
    RETURN.
  ENDIF.
  LOOP AT gt_val_output TRANSPORTING NO FIELDS WHERE msgty EQ 'A' OR msgty EQ 'E'.
*   So approvals know that an error happened
    gv_validation_errors = abap_true.
    APPEND ps_reg_data-idno TO pct_idno_err.
    EXIT.
  ENDLOOP.
*
  IF go_dialogbox_container IS INITIAL.
    CREATE OBJECT go_dialogbox_container
      EXPORTING
        top     = 150
        left    = 150
*       lifetime = CNTL_LIFETIME_session
        caption = 'Validation Checks'(034)
        width   = 800
        height  = 200.
*     SET HANDLER go_event_receiver_edit->handle_close FOR ALL INSTANCES.
    SET HANDLER go_event_receiver_edit->handle_close FOR go_dialogbox_container.
  ELSE.
    CALL METHOD go_dialogbox_container->set_visible
      EXPORTING
        visible = abap_true.

  ENDIF.

  CLEAR gr_validation_alv.
  TRY.

      cl_salv_table=>factory( EXPORTING r_container = go_dialogbox_container
                              IMPORTING r_salv_table = gr_validation_alv
                              CHANGING t_table = gt_val_output ).

* CATCH cx_salv_msg .
*ENDTRY.

      gr_validation_alv->get_columns( )->set_optimize( 'X' ).
      lv_header = |AReNa On-Demand Check Report - Number of errors { lines( gt_val_output ) } |.
      gr_validation_alv->get_display_settings( )->set_list_header( lv_header ).
      gr_validation_alv->get_functions( )->set_all( ).

      gr_validation_alv->display( ).
*    lr_table->refresh_table_display( ).
    CATCH cx_salv_msg.
      "TBD
  ENDTRY.

ENDFORM.                    " VALIDATION_CHECK_REPORT
*&---------------------------------------------------------------------*
*&      Form  REG_GTN_ALV
*&---------------------------------------------------------------------*

FORM reg_gtn_alv .

  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
    lt_exclude  TYPE ui_functions,
    ls_variant  TYPE disvariant,
    ls_layout   TYPE lvc_s_layo.
*    lo_event    TYPE REF TO lcl_event_handler.

  IF go_reg_gtn_alv IS INITIAL.
*   Regional EAN
    CREATE OBJECT go_rgtcc07
      EXPORTING
        container_name = gc_rgtcc07.

*  Create ALV grid
    CREATE OBJECT go_reg_gtn_alv
      EXPORTING
        i_parent = go_rgtcc07.

    PERFORM alv_build_fieldcat USING  gc_alv_reg_gtn CHANGING lt_fieldcat.

*  Call GRID
*  Exclude all edit functions in this example since we do not need them:
    PERFORM exclude_tb_functions_edit USING  gc_alv_reg_gtn CHANGING lt_exclude.

*   Variant management
    ls_variant-handle   = 'REAN'.

    PERFORM alv_variant_default CHANGING ls_variant.

    ls_layout-edit = abap_true.
*    ls_layout-cwidth_opt = abap_true.
    ls_layout-sel_mode = abap_true.
    ls_layout-zebra = abap_true.
    ls_layout-stylefname  = gc_celltab.

    CALL METHOD go_reg_gtn_alv->set_table_for_first_display
      EXPORTING
        is_variant                    = ls_variant
        is_layout                     = ls_layout
        it_toolbar_excluding          = lt_exclude
      CHANGING
        it_outtab                     = gt_zarn_reg_ean
        it_fieldcatalog               = lt_fieldcat
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.

    IF sy-subrc IS NOT INITIAL.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

*-- Create object lo_event for event handling on the control
*    CREATE OBJECT lo_event.
*
**-- Calling the events
*    SET HANDLER:
*    lo_event->handle_user_command FOR go_reg_gtn_alv.

*   set editable cells to ready for input
    CALL METHOD go_reg_gtn_alv->set_ready_for_input
      EXPORTING
        i_ready_for_input = 1.

    CALL METHOD go_reg_gtn_alv->register_edit_event
      EXPORTING
        i_event_id = cl_gui_alv_grid=>mc_evt_enter.

    CALL METHOD go_reg_gtn_alv->register_edit_event
      EXPORTING
        i_event_id = cl_gui_alv_grid=>mc_evt_modified.

    PERFORM register_f4_event USING go_reg_gtn_alv gc_uom_meins.

    SET HANDLER go_event_receiver_edit->handle_data_changed FOR go_reg_gtn_alv.



  ELSE.

    PERFORM refresh_reg_alv USING go_reg_gtn_alv lt_fieldcat.
*    CALL METHOD go_reg_gtn_alv->refresh_table_display.
  ENDIF.
ENDFORM.                    " REG_GTN_ALV

*&---------------------------------------------------------------------*
*&      Form  CHANGE_DOCUMENTS_DISPLAY
*&---------------------------------------------------------------------*

FORM change_documents_display   USING    pt_rows TYPE lvc_t_row
                                         pv_idno TYPE zarn_idno
                                         pv_ucomm TYPE sy-ucomm.

  DATA:
    ls_rows      LIKE LINE OF pt_rows,
    lr_obj_idno  TYPE RANGE OF  cdhdr-objectid,
    ls_obj_idno  LIKE LINE OF lr_obj_idno,
    lr_obj_uname TYPE RANGE OF  sy-uname,
    lr_obj_date  TYPE RANGE OF  sy-datum.

* from ALV Worklist if required later
  IF pv_ucomm EQ 'CLOGS'.

    IF pt_rows[] IS NOT INITIAL.
      LOOP AT pt_rows INTO ls_rows.

*     read selected articles from worklist
        READ TABLE gt_worklist INTO gs_worklist INDEX ls_rows-index.
        IF sy-subrc IS NOT INITIAL.
          CONTINUE.
        ENDIF.

        ls_obj_idno-sign   = 'I'.
        ls_obj_idno-option = 'EQ'.
        ls_obj_idno-low = gs_worklist-idno.
        APPEND ls_obj_idno TO lr_obj_idno.
      ENDLOOP.

    ELSE.
      ls_obj_idno-sign   = 'I'.
      ls_obj_idno-option = 'EQ'.
      ls_obj_idno-low = pv_idno.
      APPEND ls_obj_idno TO lr_obj_idno.
    ENDIF.


    IF  lr_obj_idno[] IS NOT INITIAL.
* Call Change document report already created
      SUBMIT zrarn_change_document_display
              WITH s_idno   IN lr_obj_idno
              WITH s_udate  IN lr_obj_date
              WITH s_uname  IN lr_obj_uname
              AND RETURN.
    ENDIF.

  ENDIF.  " IF pv_ucomm EQ 'CLOGS'

ENDFORM.                    " CHANGE_DOCUMENTS_DISPLAY
*&---------------------------------------------------------------------*
*&      Form  LOCK_REGIONAL_DATA
*&---------------------------------------------------------------------*
FORM lock_regional_data  USING     pt_rows       TYPE lvc_t_row
                                   pv_refr_color TYPE abap_bool.

  DATA:
    ls_rows     LIKE LINE OF  pt_rows,
    lr_obj_idno TYPE RANGE OF cdhdr-objectid,
    ls_obj_idno LIKE LINE OF  lr_obj_idno,
    lt_enq      TYPE STANDARD TABLE OF seqg3,
    ls_enq      LIKE LINE OF  lt_enq,
    lv_garg     TYPE eqegraarg,
    ls_worklist TYPE ty_outtab,
    ls_celltab  TYPE lvc_s_styl.

  FIELD-SYMBOLS <fs_worklist> LIKE LINE OF gt_worklist.

  IF pv_refr_color EQ abap_true.
    IF gv_display IS INITIAL.
*     reselection for enrichment - remove selected rows and reassign colour
      LOOP AT gt_worklist ASSIGNING <fs_worklist>
           WHERE rowcolor EQ gc_alv_selected.  " only for selected rows if the user wants to reselect
        CLEAR <fs_worklist>-rowcolor.
      ENDLOOP.
    ENDIF.
  ENDIF.

  LOOP AT pt_rows INTO ls_rows.

*   read selected articles from worklist
    READ TABLE gt_worklist ASSIGNING <fs_worklist> INDEX ls_rows-index.
    IF sy-subrc IS NOT INITIAL.
      CONTINUE.
    ENDIF.

*   check if display only
    IF gv_display IS INITIAL.
*     if the row was not selected
*      IF <fs_worklist>-rowcolor IS INITIAL.
      IF <fs_worklist>-lock_user IS INITIAL.
*       Check lock object
        CALL FUNCTION 'ENQUEUE_EZARN_REGIONAL'
          EXPORTING
            idno           = <fs_worklist>-idno
            _scope         = 1
*           _wait          = abap_true
          EXCEPTIONS
            foreign_lock   = 1
            system_failure = 2
            OTHERS         = 3.
        IF sy-subrc IS NOT INITIAL.
          lv_garg = sy-mandt && <fs_worklist>-idno.
*         check lock and get user name
          CALL FUNCTION 'ENQUEUE_READ'
            EXPORTING
              gclient               = sy-mandt
              gname                 = gc_lock_reg
              garg                  = lv_garg
              guname                = ' '
            TABLES
              enq                   = lt_enq
            EXCEPTIONS
              communication_failure = 1
              system_failure        = 2
              OTHERS                = 3.
          IF lt_enq IS NOT INITIAL.
            READ TABLE lt_enq INTO ls_enq INDEX 1.
            PERFORM get_control_data
              USING ls_enq-guname CHANGING <fs_worklist>-lock_user.
          ENDIF.

          <fs_worklist>-rowcolor = gc_alv_locked.     "Red
          WRITE icon_locked TO <fs_worklist>-icon.
          <fs_worklist>-lock_user = <fs_worklist>-icon && <fs_worklist>-lock_user.
        ELSE.
          IF mytab-activetab NE gc_icare_tab.
            <fs_worklist>-rowcolor = gc_alv_selected.   "Green
          ENDIF.
          WRITE  icon_wizard TO <fs_worklist>-icon.
        ENDIF.
      ENDIF.

    ELSE. " Display only
      <fs_worklist>-rowcolor = gc_alv_selected.   "Green
      WRITE  icon_display TO <fs_worklist>-icon.
    ENDIF.

  ENDLOOP.

ENDFORM.                    " LOCK_REGIONAL_DATA

FORM lock_regional_data_current.

  DATA: lt_rows     TYPE lvc_t_row,
        ls_row      LIKE LINE OF lt_rows,
        ls_worklist LIKE LINE OF gt_worklist.

  READ TABLE gt_worklist INTO ls_worklist WITH KEY idno = gs_worklist-idno version = gs_worklist-version.
  IF sy-subrc <> 0.
    RETURN.
  ENDIF.

  IF ls_worklist-lock_user EQ space.
    gv_display = abap_false.
    CLEAR ls_row.
    ls_row-index = sy-tabix.
    APPEND ls_row TO lt_rows.
    PERFORM lock_regional_data USING lt_rows abap_false.
    CALL METHOD go_worklist_alv->refresh_table_display.
  ELSE.
    gv_display = abap_true.
    MESSAGE s005 DISPLAY LIKE 'E'.
  ENDIF.
  lcl_controller=>get_instance( )->set_display( iv_display = gv_display ).

ENDFORM.

*&---------------------------------------------------------------------*
*&      Form  CHECK_ALV_DATA_CHANGE
*&---------------------------------------------------------------------*
FORM check_alv_data_change .
* only if  a change was not recorded already from the dynpro
*  IF gv_data_change IS INITIAL AND gv_display IS INITIAL. " doesnt record last chg if hit save and and not enter

  IF gv_display IS INITIAL.
    go_reg_uom_alv->check_changed_data( ).
    go_reg_con_dis_alv->check_changed_data( ).
    go_reg_con_rrp_alv->check_changed_data( ).
    go_reg_gtn_alv->check_changed_data( ).
    go_reg_pir_alv->check_changed_data( ).
    go_reg_pf_alv->check_changed_data( ).
    go_reg_con_artlink_alv->check_changed_data( ).
    go_reg_allerg_alv->check_changed_data( ).
    lcl_reg_cluster_ui=>get_instance( )->check_changed_data( ).
  ENDIF.
ENDFORM.                    " CHECK_ALV_DATA_CHANGE
*&---------------------------------------------------------------------*
*&      Form  TEXT_EDITOR
*&---------------------------------------------------------------------*
FORM text_editor .

  DATA:
    lv_mode TYPE i.

  IF go_reg_editor IS INITIAL.

    CREATE OBJECT go_rhtxt01
      EXPORTING
        container_name = gc_rhtxt01.

*   create calls constructor, which initializes, creats and links
*   a TextEdit Control
    CREATE OBJECT go_reg_editor
      EXPORTING
        style         = 0
        parent        = go_rhtxt01
        wordwrap_mode = cl_gui_textedit=>wordwrap_at_windowborder
      EXCEPTIONS
        OTHERS        = 1.

    IF sy-subrc NE 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.
*   remove status bar for space !
    CALL METHOD go_reg_editor->set_statusbar_mode
      EXPORTING
        statusbar_mode         = 0
      EXCEPTIONS
        error_cntl_call_method = 1
        invalid_parameter      = 2
        OTHERS                 = 3.

*   remove toolbar - as per users request !
    CALL METHOD go_reg_editor->set_toolbar_mode
      EXPORTING
        toolbar_mode           = 0
      EXCEPTIONS
        error_cntl_call_method = 1
        invalid_parameter      = 2
        OTHERS                 = 3.

  ELSE.
*   opposite of ALV grid edit mode :-(
    IF gv_edit_toggle IS NOT INITIAL.
      CLEAR lv_mode.
    ELSE.
      ADD 1 TO lv_mode.
    ENDIF.
*   toggle edit/display
    CALL METHOD go_reg_editor->set_readonly_mode
      EXPORTING
        readonly_mode = lv_mode.
  ENDIF.                               " Editor is initial
ENDFORM.                    " TEXT_EDITOR
*&---------------------------------------------------------------------*
*&      Form  alv_build_fieldcat_cond
*&---------------------------------------------------------------------*
FORM alv_build_fieldcat_cond USING pv_structure
*                                   pv_cond_table
                         CHANGING pt_fieldcat TYPE lvc_t_fcat.

  DATA ls_fcat TYPE lvc_s_fcat.

  CALL FUNCTION 'LVC_FIELDCATALOG_MERGE'
    EXPORTING
      i_structure_name = pv_structure
    CHANGING
      ct_fieldcat      = pt_fieldcat.

  LOOP AT pt_fieldcat INTO ls_fcat.
*   suppress ID for all
    IF ls_fcat-fieldname EQ gc_idno.
      ls_fcat-no_out = abap_true.
      " VKORG - display
    ELSEIF ls_fcat-fieldname EQ gc_vkorg.
      ls_fcat-edit = abap_false.
      " Other fields - Editable
    ELSE.
      ls_fcat-edit = abap_true.
    ENDIF.

    MODIFY pt_fieldcat FROM ls_fcat.
  ENDLOOP.

ENDFORM.                    "alv_build_fieldcat
*&---------------------------------------------------------------------*
*&      Form  REG_CON_ALV_RRP
*&---------------------------------------------------------------------*
FORM reg_con_alv_rrp .

  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo.

  IF go_reg_con_rrp_alv IS INITIAL.
*   Regional RRP
    CREATE OBJECT go_rconcc03
      EXPORTING
        container_name = gc_rconcc03.

* Create ALV grid
    CREATE OBJECT go_reg_con_rrp_alv
      EXPORTING
        i_parent = go_rconcc03.

    PERFORM alv_build_fieldcat_cond   USING gc_alv_reg_rrp
*                                            gc_alv_reg_con_rrp
                                  CHANGING lt_fieldcat.
*   Exclude all edit functions in this example since we do not need them:
    PERFORM exclude_tb_functions_edit  USING gc_alv_reg_rrp CHANGING lt_exclude.


*    ls_layout-edit = abap_true.
*    ls_layout-cwidth_opt = abap_true.
    ls_layout-sel_mode = abap_true.
    ls_layout-zebra = abap_true.


    CALL METHOD go_reg_con_rrp_alv->set_table_for_first_display
      EXPORTING
        is_layout                     = ls_layout
        it_toolbar_excluding          = lt_exclude
      CHANGING
        it_outtab                     = gt_zarn_reg_rrp  "gt_zarn_reg_cond
        it_fieldcatalog               = lt_fieldcat
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.

    IF sy-subrc IS NOT INITIAL.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

* set editable cells to ready for input
    CALL METHOD go_reg_con_rrp_alv->set_ready_for_input
      EXPORTING
        i_ready_for_input = 1.

    CALL METHOD go_reg_con_rrp_alv->register_edit_event
      EXPORTING
        i_event_id = cl_gui_alv_grid=>mc_evt_enter.

    CALL METHOD go_reg_con_rrp_alv->register_edit_event
      EXPORTING
        i_event_id = cl_gui_alv_grid=>mc_evt_modified.

    SET HANDLER go_event_receiver->handle_toolbar FOR go_reg_con_rrp_alv.
    SET HANDLER go_event_receiver->handle_user_command FOR go_reg_con_rrp_alv.

  ELSE.
    PERFORM refresh_reg_alv USING go_reg_con_rrp_alv lt_fieldcat.
*    CALL METHOD go_reg_con_rrp_alv->refresh_table_display.
  ENDIF.
ENDFORM.                    " REG_CON_ALV_RRP
*&---------------------------------------------------------------------*
*&      Form  REG_CON_ALV_DIS
*&---------------------------------------------------------------------*

FORM reg_con_alv_dis .
  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
*    lt_sortcat  TYPE lvc_t_sort,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo.

  IF go_reg_con_dis_alv IS INITIAL.
*   Standard Terms (Discount)
    CREATE OBJECT go_rconcc02
      EXPORTING
        container_name = gc_rconcc02.

* Create ALV grid
    CREATE OBJECT go_reg_con_dis_alv
      EXPORTING
        i_parent = go_rconcc02.

    PERFORM alv_build_fieldcat_cond   USING gc_alv_reg_dis CHANGING lt_fieldcat.
*    PERFORM alv_build_sortcat         USING gc_alv_reg_dis CHANGING lt_sortcat.

** Exclude all edit functions in this example since we do not need them:

    PERFORM exclude_tb_functions_edit  USING gc_alv_reg_dis CHANGING lt_exclude.


*    ls_layout-edit = abap_true.
*    ls_layout-cwidth_opt = abap_true.
    ls_layout-sel_mode = abap_true.
    ls_layout-zebra = abap_true.


    CALL METHOD go_reg_con_dis_alv->set_table_for_first_display
      EXPORTING
        is_layout                     = ls_layout
        it_toolbar_excluding          = lt_exclude
      CHANGING
        it_outtab                     = gt_zarn_reg_std_ter "gt_reg_cond_dis  "gt_zarn_reg_cond
        it_fieldcatalog               = lt_fieldcat
*       it_sort                       = lt_sortcat
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.

    IF sy-subrc IS NOT INITIAL.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

* set editable cells to ready for input
    CALL METHOD go_reg_con_dis_alv->set_ready_for_input
      EXPORTING
        i_ready_for_input = 1.

    CALL METHOD go_reg_con_dis_alv->register_edit_event
      EXPORTING
        i_event_id = cl_gui_alv_grid=>mc_evt_enter.

    CALL METHOD go_reg_con_dis_alv->register_edit_event
      EXPORTING
        i_event_id = cl_gui_alv_grid=>mc_evt_modified.

    SET HANDLER go_event_receiver->handle_toolbar FOR go_reg_con_dis_alv.
    SET HANDLER go_event_receiver->handle_user_command FOR go_reg_con_dis_alv.

  ELSE.
    PERFORM refresh_reg_alv USING go_reg_con_dis_alv lt_fieldcat.
*    CALL METHOD go_reg_con_dis_alv->refresh_table_display.
  ENDIF.
ENDFORM.                    " REG_CON_ALV_DIS

*&---------------------------------------------------------------------*
*&      Form  SHOW_MULTI_GTIN
*&---------------------------------------------------------------------*
FORM show_multi_gtin .

  DATA:

    lr_table      TYPE REF TO cl_salv_table.

*
  IF go_dialogbox_cont_gtin IS INITIAL.
    CREATE OBJECT go_dialogbox_cont_gtin
      EXPORTING
        top     = 60
        left    = 1000
        caption = 'Multiple GTIN'(033)
        width   = 350
        height  = 150.
    SET HANDLER go_event_receiver->handle_close FOR go_dialogbox_cont_gtin.

  ELSE.

    CALL METHOD go_dialogbox_cont_gtin->set_visible
      EXPORTING
        visible = abap_true.
  ENDIF.

  TRY.
      cl_salv_table=>factory( EXPORTING r_container = go_dialogbox_cont_gtin
                              IMPORTING r_salv_table = lr_table
                              CHANGING t_table = gt_multi_gtin ).
      lr_table->display( ).
    CATCH cx_salv_msg.
  ENDTRY.


ENDFORM.                    " SHOW_MULTI_GTIN
*&---------------------------------------------------------------------*
*&      Form  SCREEN_ATTRIBUTES_GENERAL
*&---------------------------------------------------------------------*

FORM screen_attributes_general.
* check lock
  READ TABLE gt_worklist INTO gs_worklist
       WITH KEY  idno = zarn_products-idno.

  IF sy-subrc             IS NOT INITIAL   OR
     gs_worklist-rowcolor EQ gc_alv_locked OR
     gv_display           IS NOT INITIAL.
*   Display only Logic
    PERFORM screen_display_logic.

  ELSE.
*   common logic for 101 and 102
    PERFORM screen_field_sel_logic.

  ENDIF.
ENDFORM.                    " SCREEN_ATTRIBUTES_GENERAL
*&---------------------------------------------------------------------*
*&      Form  SCREEN_FIELD_SEL_LOGIC
*&---------------------------------------------------------------------*

FORM screen_field_sel_logic .

  LOOP AT SCREEN INTO gs_screen.

    " set block label color
    PERFORM set_block_label_color.
    IF gs_screen-intensified EQ 1.
      MODIFY SCREEN FROM gs_screen.
      CONTINUE.
    ENDIF.


    IF gs_screen-name(8) EQ 'GS_PRICE'.            " Temporary Remove later
      CONTINUE.
    ENDIF.
*    Screen attributes to be restricted to
    IF gs_screen-name(4) NE 'ZARN' AND
       gs_screen-name(3) NE gc_gs_prefix. "Structure for Texts

      CONTINUE.
    ENDIF.
*     make the key
    SPLIT gs_screen-name AT '-' INTO gv_tabname gv_fldname.

    IF  gs_screen-name(3) NE gc_gs_prefix.  "Structure for Texts
      READ TABLE gr_gui_load->gt_gui_set TRANSPORTING NO FIELDS
               WITH KEY tabname  = gv_tabname
                        fldname  = gv_fldname
                        gui      = abap_true.
      IF sy-subrc IS NOT INITIAL.
        CONTINUE.
      ENDIF.
    ENDIF.

*     Get GUI Relevant settings
    READ TABLE gr_gui_load->gt_gui_sel INTO gs_gui_sel
         WITH KEY tabname   = gv_tabname
                  fldname   = gv_fldname
                  field_ref = gv_field_ref.
    IF sy-subrc IS INITIAL.

      CASE gs_gui_sel-attribute.

        WHEN gr_gui_load->gc_attrib_d. "Display
          gs_screen-input = 0.
*          gs_screen-output = 1.

        WHEN gr_gui_load->gc_attrib_r.  "Required
*          gs_screen-intensified = 1.
          gs_screen-required = 2.  " Recommended !

        WHEN gr_gui_load->gc_attrib_o.  "Optional


        WHEN gr_gui_load->gc_attrib_h.  "Hidden
          gs_screen-input     = 0.
          gs_screen-invisible = 1.

      ENDCASE.

    ELSE.
*       diaply only
      gs_screen-input = 0.
    ENDIF.

    MODIFY SCREEN FROM gs_screen.
  ENDLOOP.
ENDFORM.                    " SCREEN_FIELD_SEL_LOGIC
*&---------------------------------------------------------------------*
*&      Form  SCREEN_DISPLAY_LOGIC
*&---------------------------------------------------------------------*
FORM screen_display_logic .

  LOOP AT SCREEN INTO gs_screen.
*     ignore buttons on the screen
    IF gs_screen-name(9) EQ 'GV_REG_PB' OR
       gs_screen-name(9) EQ 'GV_NAT_PB' OR
       gs_screen-name    EQ 'CALL_MM43' OR
       gs_screen-name(3) EQ gc_ts_prefix OR
       gs_screen-name    EQ 'RPB_003'.
      CONTINUE.
    ENDIF.
*     on Display only from selection screen
*     or locked screen input not allowed
    gs_screen-input = 0.

    PERFORM set_block_label_color.

    MODIFY SCREEN FROM gs_screen.
  ENDLOOP.

ENDFORM.                    " SCREEN_DISPLAY_LOGIC
*&---------------------------------------------------------------------*
*&      Form  READ_PROD_HIERARCHY
*&---------------------------------------------------------------------*

FORM read_prod_hierarchy  USING    pv_product_hierarchy TYPE prodh_d.

  READ TABLE gt_t179t INTO gs_t179t
       WITH KEY spras = sy-langu
                prodh = pv_product_hierarchy.
  IF sy-subrc IS NOT INITIAL.
    CLEAR gs_t179t.
  ENDIF.

ENDFORM.                    " READ_PROD_HIERARCHY
*&---------------------------------------------------------------------*
*&      Form  SCREEN_WHEN_ICARE
*&---------------------------------------------------------------------*

FORM screen_when_icare.

  IF gv_sel_tab       EQ gc_icare_tab.
    LOOP AT SCREEN INTO gs_screen.
*   ICare related - Approvals not required
      IF gs_screen-name EQ gc_nat_pb  OR
         gs_screen-name EQ gc_reg_pb  OR
         gs_screen-name EQ gc_txt_app.

        gs_screen-input     = 0.
        gs_screen-invisible = 1.
      ENDIF.

      MODIFY SCREEN FROM gs_screen.
    ENDLOOP.
  ENDIF.

ENDFORM.                    " SCREEN_WHEN_ICARE
*&---------------------------------------------------------------------*
*&      Form  CALL_MM42_BDC
*&---------------------------------------------------------------------*

FORM call_mm42_bdc .

* Tables and work areas
  DATA:
    wa_ctu_params TYPE ctu_params.

  CHECK zarn_reg_hdr-matnr IS NOT INITIAL.

  wa_ctu_params-dismode = 'E'.
  wa_ctu_params-updmode = 'A'.

  CLEAR gt_bdcdata[].

  PERFORM dynpro USING 'SAPLMGMW'              '0100'.
  PERFORM field  USING 'RMMW1-MATNR'           zarn_reg_hdr-matnr. " sap_article.
  PERFORM field  USING 'MSICHTAUSW-KZSEL(08)'  abap_true.
  PERFORM field  USING 'BDC_OKCODE'            '/00'.

  IF gv_display IS INITIAL.
* call transaction to NON SAP  tab of MM42
    CALL TRANSACTION 'MM42'
         USING gt_bdcdata
         OPTIONS FROM wa_ctu_params.
  ELSE.
* call transaction to NON SAP  tab of MM43
    CALL TRANSACTION 'MM43'
         USING gt_bdcdata
         OPTIONS FROM wa_ctu_params.
  ENDIF.

ENDFORM.                    " CALL_MM42_BDC
*&---------------------------------------------------------------------*
*&      Form  DYNPRO
*&---------------------------------------------------------------------*
FORM dynpro  USING program dynpro.
  DATA:
    ls_bdcdata    LIKE LINE OF gt_bdcdata.

  ls_bdcdata-program  = program.
  ls_bdcdata-dynpro   = dynpro.
  ls_bdcdata-dynbegin = abap_true.
  APPEND ls_bdcdata TO gt_bdcdata.
  CLEAR ls_bdcdata.

ENDFORM.                    " DYNPRO
*&---------------------------------------------------------------------*
*&      Form  FIELD
*&---------------------------------------------------------------------*
FORM field  USING fnam fval.
  DATA:
    ls_bdcdata    LIKE LINE OF gt_bdcdata.

  ls_bdcdata-fnam = fnam.
  ls_bdcdata-fval = fval.
  APPEND ls_bdcdata TO gt_bdcdata.
  CLEAR ls_bdcdata.

ENDFORM.                    " FIELD
*&---------------------------------------------------------------------*
*&      Form  GET_GTIN_SELECTION
*&---------------------------------------------------------------------*
FORM get_gtin_selection  CHANGING pt_key      TYPE ztarn_key.

  DATA:
    lt_key_sel  TYPE ztarn_key.

  IF s_gtin1[] IS NOT INITIAL.
*   GTIN
    SELECT idno version
       FROM zarn_gtin_var
          INTO TABLE lt_key_sel
      FOR ALL ENTRIES IN pt_key
       WHERE  idno      EQ pt_key-idno
         AND  version   EQ pt_key-version
         AND  gtin_code IN  s_gtin1 .

*   use the filtered key from GTIN
    CLEAR pt_key.
    pt_key = lt_key_sel.
    SORT pt_key BY idno version.
    DELETE ADJACENT DUPLICATES FROM pt_key COMPARING idno.
  ENDIF.
ENDFORM.                    " GET_GTIN_SELECTION
*&---------------------------------------------------------------------*
*&      Form  GET_REG_HDR_SELECTION
*&---------------------------------------------------------------------*
FORM get_reg_hdr_selection  CHANGING pt_key  TYPE ztarn_key.

  DATA:
    lt_key_sel  TYPE ztarn_key.
* commented to make sure the records selected are all I Cared
*  IF s_rcman[] IS NOT INITIAL OR
*     s_ccman[] IS NOT INITIAL.
*     Regional Header
  SELECT idno version
     FROM zarn_reg_hdr
        INTO TABLE lt_key_sel
    FOR ALL ENTRIES IN pt_key
     WHERE  idno    EQ pt_key-idno
*       AND  version EQ pt_key-version   " version of I Care
       AND ( ret_zzcatman IN s_rcman
         OR  gil_zzcatman IN s_ccman ).
*   keep it consistent
  CLEAR pt_key.
  pt_key = lt_key_sel.
  SORT pt_key BY idno version.
  DELETE ADJACENT DUPLICATES FROM pt_key COMPARING idno.
*  ENDIF.

ENDFORM.                    " GET_REG_HDR_SELECTION
*&---------------------------------------------------------------------*
*&      Form  PROCESS_REG_UOM
*&---------------------------------------------------------------------*
FORM process_reg_uom_screen  USING    ps_prod_data    TYPE zsarn_prod_data
                             CHANGING pt_zarn_reg_uom TYPE ztarn_reg_uom_ui.

  TYPES:
    BEGIN OF ty_added_pir,
      uom_code              TYPE zarn_uom_code,
      base_units_per_pallet TYPE zarn_base_units_per_pallet,
      qty_layers_per_pallet TYPE zarn_qty_layers_per_pallet,
      qty_units_per_pallet  TYPE zarn_qty_units_per_pallet,
    END OF ty_added_pir.

  DATA:

    ls_zarn_uom_cat       TYPE zarn_uom_cat,
    ls_zarn_reg_uom       TYPE zsarn_reg_uom_ui,
    lt_zarn_reg_uom       TYPE ztarn_reg_uom_ui,
    lt_zarn_reg_uom_comp  TYPE ztarn_reg_uom_ui,
    ls_zarn_uom_variant   TYPE zarn_uom_variant,
    ls_zarn_uom_variant_l TYPE zarn_uom_variant,
    ls_zarn_prod_uom_t    TYPE zarn_prod_uom_t,
    ls_zarn_gtin_var      TYPE zarn_gtin_var,
    ls_zarn_gtin_var_l    TYPE zarn_gtin_var,

    ls_zarn_reg_pir       TYPE zarn_reg_pir,
    lt_zarn_reg_pir       TYPE ztarn_reg_pir,

    lt_zarn_pir           TYPE ztarn_pir,
    ls_zarn_pir           TYPE zarn_pir,
    lv_meinh              TYPE zarn_reg_uom-meinh,
    lv_count              TYPE i.

  DATA lt_added_pir             TYPE SORTED TABLE OF ty_added_pir WITH NON-UNIQUE KEY uom_code base_units_per_pallet
                                                                                              qty_layers_per_pallet qty_units_per_pallet.
  DATA ls_added_pir             TYPE ty_added_pir.
  DATA lt_uomcategory           TYPE TABLE OF zarn_uomcategory.
  DATA ls_uomcategory           TYPE zarn_uomcategory.
  DATA ls_zarn_reg_uom_tmp      TYPE zsarn_reg_uom_ui.
  DATA ls_nat_alv_uom_iu        LIKE LINE OF gt_nat_alv_uom.
  DATA ls_nat_alv_uom_ou        LIKE LINE OF gt_nat_alv_uom.
  DATA ls_nat_alv_uom_su        LIKE LINE OF gt_nat_alv_uom.
  DATA lv_count_su              TYPE i.
  DATA lv_count_iu              TYPE i.
  DATA lv_count_ou              TYPE i.

  CONSTANTS:
    lc_cat_layer  TYPE zarn_uom_category VALUE 'LAYER',
    lc_cat_pallet TYPE zarn_uom_category VALUE 'PALLET',
    lc_cat_retail TYPE zarn_uom_category VALUE 'RETAIL'.

  FIELD-SYMBOLS <ls_zarn_reg_uom>  LIKE LINE OF gt_zarn_reg_uom.

  SELECT * FROM zarn_uomcategory INTO TABLE lt_uomcategory.

* get UOM  from Merch Cat.
  SELECT SINGLE a~meins INTO lv_meinh
    FROM mara AS a
    JOIN t023 AS b
    ON a~matnr = b~wwgda
    AND b~matkl = zarn_reg_hdr-matkl. "mc.

  LOOP AT ps_prod_data-zarn_uom_variant[] INTO ls_zarn_uom_variant.
    ls_zarn_reg_uom-mandt = sy-mandt.
    ls_zarn_reg_uom-idno  = ls_zarn_uom_variant-idno.
    ls_zarn_reg_uom-ersda = sy-datum.
    ls_zarn_reg_uom-erzet = sy-uzeit.

    IF ls_zarn_uom_variant-factor_of_base_units IS NOT INITIAL.
      ls_zarn_reg_uom-num_base_units = ls_zarn_uom_variant-num_base_units / ls_zarn_uom_variant-factor_of_base_units.
    ELSE.
      ls_zarn_reg_uom-num_base_units = ls_zarn_uom_variant-num_base_units.
    ENDIF.


*   Get AReNa: Product UOM Text
    READ TABLE gt_zarn_prod_uom_t INTO ls_zarn_prod_uom_t
         WITH KEY product_uom   = ls_zarn_uom_variant-product_uom.
    IF sy-subrc IS INITIAL.
      ls_zarn_reg_uom-uom_category = ls_zarn_prod_uom_t-sap_uom_cat.
      ls_zarn_reg_uom-pim_uom_code = ls_zarn_uom_variant-uom_code.
    ENDIF.

*         no PIR for now
    ls_zarn_reg_uom-hybris_internal_code  = space.  "!

*-        using zarn_uom_variant-CHILD_GTIN get ls_gtin_var-uom_code as a Child UOM
    READ TABLE ps_prod_data-zarn_gtin_var[] INTO ls_zarn_gtin_var
      WITH KEY idno      = ps_prod_data-idno
               version   = ps_prod_data-version
               gtin_code = ls_zarn_uom_variant-child_gtin.

    IF sy-subrc IS INITIAL AND ls_zarn_gtin_var-uom_code IS NOT INITIAL.
*           Child UOM
      ls_zarn_reg_uom-lower_uom   = ls_zarn_gtin_var-uom_code.

*           using  LS_ZARN_GTIN_VAR-UOM_CODE get ZARN_UOM_VARIANT-CHILD_GTIN
      READ TABLE ps_prod_data-zarn_uom_variant[] INTO ls_zarn_uom_variant_l
           WITH KEY idno     = ps_prod_data-idno
                    version  = ps_prod_data-version
                    uom_code = ls_zarn_gtin_var-uom_code.
      IF sy-subrc IS INITIAL AND ls_zarn_uom_variant_l-child_gtin IS NOT INITIAL.

*             using  LS_ZARN_UOM_VARIANT-CHILD_GTIN get ls_gtin_var-uom_code as a LOWER Child UOM
        CLEAR ls_zarn_gtin_var_l.
        READ TABLE ps_prod_data-zarn_gtin_var[] INTO ls_zarn_gtin_var_l
             WITH KEY idno      = ps_prod_data-idno
                      version   = ps_prod_data-version
                      gtin_code = ls_zarn_uom_variant_l-child_gtin.
*             Lower child UOM
        ls_zarn_reg_uom-lower_child_uom = ls_zarn_gtin_var_l-uom_code..
      ENDIF.
    ENDIF.

*   first time only for RETAILER
    IF pt_zarn_reg_uom[] IS INITIAL AND ls_zarn_reg_uom-uom_category EQ lc_cat_retail.
      ls_zarn_reg_uom-meinh = lv_meinh.

      IF ls_zarn_reg_uom-meinh IS INITIAL.
*       read from customising table for category UOM
        READ TABLE gt_zarn_uom_cat INTO ls_zarn_uom_cat
             WITH KEY uom_category = lc_cat_retail
                      seqno        = 1.
        ls_zarn_reg_uom-meinh = ls_zarn_uom_cat-uom.
      ENDIF.

    ENDIF.

    APPEND ls_zarn_reg_uom TO lt_zarn_reg_uom.

    CLEAR:
      ls_zarn_reg_uom,
      ls_zarn_uom_variant_l,
      ls_zarn_prod_uom_t,
      ls_zarn_gtin_var,
      ls_zarn_gtin_var_l.
  ENDLOOP.


** PIR
*  for PALLET and LAYER
  lt_zarn_pir = ps_prod_data-zarn_pir.

  DELETE lt_zarn_pir WHERE base_units_per_pallet LE 0 AND
                           qty_layers_per_pallet LE 0 AND
                           qty_units_per_pallet  LE 0.

*       Sort sequence important for loop to compare values below
  SORT lt_zarn_pir
    BY idno
       version
       uom_code
       base_units_per_pallet
       qty_layers_per_pallet
       qty_units_per_pallet.
*******************************************************************************
  LOOP AT lt_zarn_pir[] INTO ls_zarn_pir.

    " in case same uom record exists then process it only when there is change in
    " base_units_per_pallet or qty_layers_per_pallet value or qty_units_per_pallet
*        CLEAR: ls_added_pir_old, lv_pir_exist.
    READ TABLE lt_added_pir TRANSPORTING NO FIELDS
      WITH TABLE KEY uom_code = ls_zarn_pir-uom_code
      base_units_per_pallet = ls_zarn_pir-base_units_per_pallet
      qty_layers_per_pallet = ls_zarn_pir-qty_layers_per_pallet
      qty_units_per_pallet = ls_zarn_pir-qty_units_per_pallet.

    " record not found, then add to local table
    IF sy-subrc NE 0.
      CLEAR ls_added_pir.
      ls_added_pir-uom_code = ls_zarn_pir-uom_code.
*          ls_added_pir-hybris_internal_code = ls_arn_pir-hybris_internal_code.
      ls_added_pir-base_units_per_pallet = ls_zarn_pir-base_units_per_pallet.
      ls_added_pir-qty_layers_per_pallet = ls_zarn_pir-qty_layers_per_pallet.
      ls_added_pir-qty_units_per_pallet = ls_zarn_pir-qty_units_per_pallet.
      INSERT ls_added_pir INTO TABLE lt_added_pir.
    ELSE.
      CONTINUE.
    ENDIF.

    CLEAR:
      ls_zarn_reg_uom,
      lv_count.

    " fill data for all relevant UOM
    ls_zarn_reg_uom-mandt = sy-mandt.
    ls_zarn_reg_uom-idno  = ls_zarn_pir-idno.
    ls_zarn_reg_uom-ersda = sy-datum.
    ls_zarn_reg_uom-erzet = sy-uzeit.

*   same PIR UOM code used for PIM and LOWER UOM in regional table
    ls_zarn_reg_uom-pim_uom_code = ls_zarn_pir-uom_code.
    ls_zarn_reg_uom-lower_uom    = ls_zarn_pir-uom_code.

*         Common Hybris internal code data for PALLET and LAYER records
    ls_zarn_reg_uom-hybris_internal_code = ls_zarn_pir-hybris_internal_code.

    DO 2 TIMES.

      ADD 1 TO lv_count.
      IF lv_count EQ 1.
        ls_zarn_reg_uom-uom_category = lc_cat_layer.
      ELSE.
        ls_zarn_reg_uom-uom_category = lc_cat_pallet.
*             add to Regional table as all other values should remain same
*        APPEND ls_zarn_reg_uom TO lt_zarn_reg_uom.
*        EXIT.
      ENDIF.

      CLEAR: ls_zarn_uom_variant.
*           get the child GTIN for UOM_CODE
      READ TABLE gt_zarn_uom_variant INTO ls_zarn_uom_variant
        WITH KEY idno     = ls_zarn_pir-idno
                 version  = ls_zarn_pir-version
                 uom_code = ls_zarn_pir-uom_code.
      IF sy-subrc IS INITIAL.
        " LAYER
        IF ls_zarn_reg_uom-uom_category = lc_cat_layer.

          IF ls_zarn_pir-qty_layers_per_pallet IS NOT INITIAL AND
             ls_zarn_pir-base_units_per_pallet IS NOT INITIAL.

            ls_zarn_reg_uom-num_base_units = ls_zarn_pir-base_units_per_pallet / ls_zarn_pir-qty_layers_per_pallet.
            IF ls_zarn_pir-factor_of_buom_per_pallet IS NOT INITIAL.
              ls_zarn_reg_uom-num_base_units = ls_zarn_reg_uom-num_base_units / ls_zarn_pir-factor_of_buom_per_pallet.
            ENDIF.

          ELSE.
            IF ls_zarn_pir-qty_per_pallet_layer IS NOT INITIAL.
              ls_zarn_reg_uom-num_base_units =  ls_zarn_pir-qty_per_pallet_layer * ls_zarn_uom_variant-num_base_units.

              IF ls_zarn_uom_variant-factor_of_base_units IS NOT INITIAL.
                ls_zarn_reg_uom-num_base_units = ls_zarn_reg_uom-num_base_units / ls_zarn_uom_variant-factor_of_base_units.
              ENDIF.
            ELSE.
              ls_zarn_reg_uom-num_base_units = 1.
            ENDIF.
          ENDIF.

          " PALLET
        ELSEIF ls_zarn_reg_uom-uom_category = lc_cat_pallet.

          IF ls_zarn_pir-base_units_per_pallet IS NOT INITIAL.

            ls_zarn_reg_uom-num_base_units = ls_zarn_pir-base_units_per_pallet.
            IF ls_zarn_pir-factor_of_buom_per_pallet IS NOT INITIAL.
              ls_zarn_reg_uom-num_base_units = ls_zarn_reg_uom-num_base_units / ls_zarn_pir-factor_of_buom_per_pallet.
            ENDIF.

          ELSE.
            IF ls_zarn_pir-qty_units_per_pallet IS NOT INITIAL.
              ls_zarn_reg_uom-num_base_units = ls_zarn_pir-qty_units_per_pallet * ls_zarn_uom_variant-num_base_units.
              IF ls_zarn_uom_variant-factor_of_base_units IS NOT INITIAL.
                ls_zarn_reg_uom-num_base_units = ls_zarn_reg_uom-num_base_units / ls_zarn_uom_variant-factor_of_base_units.
              ENDIF.

            ELSE.
              IF ls_zarn_pir-qty_layers_per_pallet IS NOT INITIAL.

                CLEAR ls_zarn_reg_uom_tmp.
                READ TABLE lt_zarn_reg_uom INTO ls_zarn_reg_uom_tmp
                WITH KEY idno = ls_zarn_reg_uom-idno
                         meinh = ls_zarn_reg_uom-lower_meinh.
                IF sy-subrc = 0.
                  ls_zarn_reg_uom-num_base_units = ls_zarn_pir-qty_layers_per_pallet * ls_zarn_reg_uom_tmp-num_base_units.
                ELSE.
                  ls_zarn_reg_uom-num_base_units = 1.
                ENDIF.
              ELSE.
                ls_zarn_reg_uom-num_base_units = 1.
              ENDIF.
            ENDIF.
          ENDIF.
        ENDIF.

*             Get UOM_CODE for child GTIN
        READ TABLE gt_zarn_gtin_var INTO ls_zarn_gtin_var
        WITH KEY idno      = ls_zarn_pir-idno
                 version   = ls_zarn_pir-version
                 gtin_code = ls_zarn_uom_variant-child_gtin.
        IF sy-subrc IS INITIAL.
          ls_zarn_reg_uom-lower_child_uom = ls_zarn_gtin_var-uom_code.
*                ls_zarn_reg_uom-lower_uom = ls_zarn_gtin_var_l-uom_code.
        ENDIF.
      ENDIF.
*           add to Regional table
      APPEND ls_zarn_reg_uom TO lt_zarn_reg_uom.
    ENDDO.

  ENDLOOP.

*******************************************************************************

*     initial processing when empty
*      IF ls_reg_data-zarn_reg_uom[] IS INITIAL.
  IF pt_zarn_reg_uom[] IS INITIAL.
*       use the newly created table  :-)
    pt_zarn_reg_uom = lt_zarn_reg_uom.
  ELSE.
**     processing when NOT empty !!
**     retain old records and add any new ones identified
    LOOP AT lt_zarn_reg_uom INTO ls_zarn_reg_uom.
*       check if the ZARN_REG_UOM has the record already ?
      READ TABLE pt_zarn_reg_uom ASSIGNING <ls_zarn_reg_uom>
           WITH KEY idno                   = ls_zarn_reg_uom-idno
                    uom_category           = ls_zarn_reg_uom-uom_category
                    pim_uom_code           = ls_zarn_reg_uom-pim_uom_code
                    hybris_internal_code   = ls_zarn_reg_uom-hybris_internal_code
                    lower_uom              = ls_zarn_reg_uom-lower_uom
                    lower_child_uom        = ls_zarn_reg_uom-lower_child_uom.
      IF sy-subrc IS NOT INITIAL.
        ADD 1 TO ls_zarn_reg_uom-relationship.
        APPEND ls_zarn_reg_uom TO pt_zarn_reg_uom.
        CONTINUE.
      ELSE.
        " set base unit which is already fetched in above logic
        <ls_zarn_reg_uom>-num_base_units = ls_zarn_reg_uom-num_base_units.
      ENDIF.
    ENDLOOP.
*
  ENDIF.

  LOOP AT pt_zarn_reg_uom ASSIGNING <ls_zarn_reg_uom>.
    READ TABLE lt_uomcategory INTO ls_uomcategory WITH KEY uom_category = <ls_zarn_reg_uom>-uom_category.
    IF sy-subrc EQ 0.
      <ls_zarn_reg_uom>-cat_seqno = ls_uomcategory-cat_seqno.
    ENDIF.
  ENDLOOP.

  SORT pt_zarn_reg_uom BY cat_seqno ASCENDING.

* get base unit of measure checkbox value
  CLEAR ls_zarn_uom_variant.
  READ TABLE ps_prod_data-zarn_uom_variant[] INTO ls_zarn_uom_variant
     WITH KEY base_unit = abap_true.

* get sales unit of measure checkbox value
  CLEAR ls_nat_alv_uom_su.
  LOOP AT gt_nat_alv_uom INTO ls_nat_alv_uom_su WHERE consumer_unit = abap_true.
    lv_count_su = lv_count_su + 1.
  ENDLOOP.

  IF lv_count_su NE 1.
    CLEAR ls_nat_alv_uom_su.
    READ TABLE gt_nat_alv_uom INTO ls_nat_alv_uom_su WITH KEY product_uom = zarn_products-seling_uom.
    IF sy-subrc EQ 0.
      lv_count_su = 1. " success
    ELSE.
      " read base
      READ TABLE gt_nat_alv_uom INTO ls_nat_alv_uom_su WITH KEY base_unit = abap_true.
      IF sy-subrc EQ 0.
        lv_count_su = 1. " success
      ENDIF.
    ENDIF.
  ENDIF.

* get issue unit of measure checkbox value
  CLEAR ls_nat_alv_uom_iu.
  LOOP AT gt_nat_alv_uom INTO ls_nat_alv_uom_iu WHERE despatch_unit = abap_true.
    lv_count_iu = lv_count_iu + 1.
  ENDLOOP.

  IF lv_count_iu NE 1.
    CLEAR ls_nat_alv_uom_iu.
    READ TABLE gt_nat_alv_uom INTO ls_nat_alv_uom_iu WITH KEY uom_category = 'INNER'.
    IF sy-subrc EQ 0.
      lv_count_iu = 1. " success
    ELSE.
      " read base
      READ TABLE gt_nat_alv_uom INTO ls_nat_alv_uom_iu WITH KEY base_unit = abap_true.
      IF sy-subrc EQ 0.
        lv_count_iu = 1. " success
      ENDIF.
    ENDIF.
  ENDIF.

* get order unit of measure checkbox value
  CLEAR ls_nat_alv_uom_ou.
  LOOP AT gt_nat_alv_uom INTO ls_nat_alv_uom_ou WHERE an_invoice_unit = abap_true.
    lv_count_ou = lv_count_ou + 1.
  ENDLOOP.

  IF lv_count_ou NE 1.
    CLEAR ls_nat_alv_uom_ou.
    READ TABLE gt_nat_alv_uom INTO ls_nat_alv_uom_ou WITH KEY uom_category = 'SHIPPER'.
    IF sy-subrc EQ 0.
      lv_count_ou = 1. " success
    ELSE.
      " read base
      READ TABLE gt_nat_alv_uom INTO ls_nat_alv_uom_ou WITH KEY base_unit = abap_true.
      IF sy-subrc EQ 0.
        lv_count_ou = 1. " success
      ENDIF.
    ENDIF.
  ENDIF.

* Assign SAP UOM field values
  LOOP AT pt_zarn_reg_uom ASSIGNING <ls_zarn_reg_uom>.

    IF <ls_zarn_reg_uom>-meinh       IS INITIAL OR
       <ls_zarn_reg_uom>-lower_meinh IS INITIAL.
*     UOM
      IF <ls_zarn_reg_uom>-meinh        IS     INITIAL AND
         <ls_zarn_reg_uom>-pim_uom_code IS NOT INITIAL.
        CLEAR lv_count.
*     first check the number of records for the category and UOM
*     to propose next UOM
        LOOP AT pt_zarn_reg_uom TRANSPORTING NO FIELDS
             WHERE uom_category EQ <ls_zarn_reg_uom>-uom_category
               AND meinh        IS NOT INITIAL.
          ADD 1 TO lv_count.
        ENDLOOP.

*     now read the next sequence from customising table for UOM
        ADD 1 TO lv_count.
        READ TABLE gt_zarn_uom_cat INTO ls_zarn_uom_cat
             WITH KEY uom_category = <ls_zarn_reg_uom>-uom_category
                      seqno        = lv_count.
        IF sy-subrc IS INITIAL.
          <ls_zarn_reg_uom>-meinh =  ls_zarn_uom_cat-uom. ""the next sequence value in table
        ENDIF.
      ENDIF.

*     Lower UOM when it has a value !
      IF <ls_zarn_reg_uom>-lower_meinh IS      INITIAL AND
         <ls_zarn_reg_uom>-lower_uom   IS  NOT INITIAL.

*       PALLET again !
        IF <ls_zarn_reg_uom>-uom_category EQ lc_cat_pallet.

          READ TABLE pt_zarn_reg_uom INTO ls_zarn_reg_uom
               WITH KEY idno                 = <ls_zarn_reg_uom>-idno
                      uom_category           = lc_cat_layer
                        pim_uom_code         = <ls_zarn_reg_uom>-lower_uom         " only use Lower UOM code
                      hybris_internal_code   = <ls_zarn_reg_uom>-hybris_internal_code.
          IF sy-subrc IS INITIAL.
*        assign lower UOM the value of the record found
            <ls_zarn_reg_uom>-lower_meinh = ls_zarn_reg_uom-meinh .
          ENDIF.

          " LAYER
        ELSE.
          READ TABLE pt_zarn_reg_uom INTO ls_zarn_reg_uom
               WITH KEY idno                   = <ls_zarn_reg_uom>-idno
*                      uom_category           = <ls_zarn_reg_uom>-uom_category
                        pim_uom_code           = <ls_zarn_reg_uom>-lower_uom.         " only use Lower UOM code
*                      hybris_internal_code   = <ls_zarn_reg_uom>-hybris_internal_code.
          IF sy-subrc IS INITIAL.
*        assign lower UOM the value of the record found
            <ls_zarn_reg_uom>-lower_meinh = ls_zarn_reg_uom-meinh .
          ENDIF.
        ENDIF.


      ENDIF.

*     default base unit check box
      IF ls_zarn_uom_variant-uom_code  EQ <ls_zarn_reg_uom>-pim_uom_code.
        <ls_zarn_reg_uom>-unit_base = ls_zarn_uom_variant-base_unit.
      ENDIF.
    ENDIF.

*   Calculate Higher level unit
    READ TABLE pt_zarn_reg_uom INTO ls_zarn_reg_uom
      WITH KEY idno = <ls_zarn_reg_uom>-idno
      meinh = <ls_zarn_reg_uom>-lower_meinh.
*                  pim_uom_code           = <ls_zarn_reg_uom>-lower_uom.
    IF sy-subrc EQ 0 AND ls_zarn_reg_uom-num_base_units NE 0.
      <ls_zarn_reg_uom>-higher_level_units = ( <ls_zarn_reg_uom>-num_base_units / ls_zarn_reg_uom-num_base_units ).
    ENDIF.

    IF <ls_zarn_reg_uom>-pim_uom_code EQ ls_nat_alv_uom_su-uom_code
      AND <ls_zarn_reg_uom>-hybris_internal_code EQ ''.
      <ls_zarn_reg_uom>-sales_unit = abap_true.
    ENDIF.

    IF <ls_zarn_reg_uom>-pim_uom_code EQ ls_nat_alv_uom_ou-uom_code
      AND <ls_zarn_reg_uom>-hybris_internal_code EQ ''.
      <ls_zarn_reg_uom>-unit_purord = abap_true.
    ENDIF.

    IF <ls_zarn_reg_uom>-pim_uom_code EQ ls_nat_alv_uom_iu-uom_code
      AND <ls_zarn_reg_uom>-hybris_internal_code EQ ''.
      <ls_zarn_reg_uom>-issue_unit = abap_true.
    ENDIF.

  ENDLOOP.

ENDFORM.                    " PROCESS_REG_UOM
*&---------------------------------------------------------------------*
*&      Form  PROCESS_REG_EAN_SCREEN
*&---------------------------------------------------------------------*
FORM process_reg_pir_screen  USING    ps_prod_data TYPE zsarn_prod_data
                             CHANGING pt_zarn_reg_pir TYPE ztarn_reg_pir_ui.

  DATA:

    ls_zarn_reg_uom    LIKE LINE OF gt_zarn_reg_uom,
    lt_zarn_reg_pir    TYPE ztarn_reg_pir_ui,
    ls_zarn_reg_pir    LIKE LINE OF lt_zarn_reg_pir,
*    ls_zarn_reg_lst_prc LIKE LINE OF gt_zarn_reg_lst_prc,
    ls_zarn_pir        TYPE zarn_pir,
    ls_zarn_list_price TYPE zarn_list_price,
    lt_zarn_list_price TYPE ztarn_list_price,

    lv_count           TYPE i.


  FIELD-SYMBOLS <ls_zarn_reg_pir>       LIKE LINE OF gt_zarn_reg_pir.

  LOOP AT ps_prod_data-zarn_pir[] INTO ls_zarn_pir.
    CLEAR: ls_zarn_reg_pir.

    ls_zarn_reg_pir-mandt                = sy-mandt.
    ls_zarn_reg_pir-idno                 = ls_zarn_pir-idno.
    ls_zarn_reg_pir-order_uom_pim        = ls_zarn_pir-uom_code.
    ls_zarn_reg_pir-hybris_internal_code = ls_zarn_pir-hybris_internal_code.
    ls_zarn_reg_pir-lower_uom            = ls_zarn_pir-uom_code.

*   ZARN_REG_UOM has the record already ?
    READ TABLE gt_zarn_reg_uom INTO ls_zarn_reg_uom
         WITH KEY idno                   = ls_zarn_reg_pir-idno
                  pim_uom_code           = ls_zarn_reg_pir-lower_uom
*                  lower_uom              = ls_zarn_reg_pir-lower_child_uom.
                  hybris_internal_code   = ''.
*                  pim_uom_code           = ls_zarn_reg_pir-order_uom_pim
*                  hybris_internal_code   = ls_zarn_reg_pir-hybris_internal_code.
    IF sy-subrc IS INITIAL.
*      ls_zarn_reg_pir-lower_uom            = ls_zarn_reg_uom-lower_uom.
      ls_zarn_reg_pir-lower_child_uom      = ls_zarn_reg_uom-lower_uom.
      ls_zarn_reg_pir-meinh                = ls_zarn_reg_uom-meinh.
      ls_zarn_reg_pir-lower_meinh          = ls_zarn_reg_uom-lower_meinh.
      ls_zarn_reg_pir-num_base_units       = ls_zarn_reg_uom-num_base_units.
      ls_zarn_reg_pir-cat_seqno            = ls_zarn_reg_uom-cat_seqno.
    ENDIF.

*        ls_zarn_reg_pir-VENDOR   "LFA1-LIFNR for LFA1-BBBNR or BBSNR = ZARN-PIR-GLN
    PERFORM get_vendor_number USING    ls_zarn_pir-gln
                              CHANGING ls_zarn_reg_pir-lifnr.


    ls_zarn_reg_pir-gln_no               = ls_zarn_pir-gln.
    ls_zarn_reg_pir-gln_name             = ls_zarn_pir-gln_name.

*        ls_zarn_reg_pir-PIR_REL_EINA   " latest effective date from ZARN_PIR
*        ls_zarn_reg_pir-PIR_REL_EINE   " latest changed Data from ZARN_LIST_PRICE
*        ls_zarn_reg_pir-VABME = 1.
*             If ZARN_PIR-VARIABLE_UNIT=True then 1
*        ls_zarn_reg_pir-RELIF   ""X" for the first PIR

    ls_zarn_reg_pir-esokz = '0'.

*        ls_zarn_reg_pir-PUR_GROUP    "LFM1-EKGRP where LFM1-EKORG=9999 and LFM1-LIFNR=ZARN_REG_PIR-VENDOR
*        ls_zarn_reg_pir-PLND_DELRY   "LFM1-PLIFZ where LFM1-EKORG=9999 and LFM1-LIFNR=ZARN_REG_PIR-VENDOR

    PERFORM  get_vendor_purch_org_data USING    gv_ref_purch_org
                                       CHANGING ls_zarn_reg_pir.

    ls_zarn_reg_pir-minbm = 1.
    ls_zarn_reg_pir-norbm = 1.

    READ TABLE ps_prod_data-zarn_list_price[] INTO ls_zarn_list_price
         WITH KEY idno      = ls_zarn_pir-idno
                  version   = ls_zarn_pir-version
                  uom_code  = ls_zarn_reg_pir-order_uom_pim
                  gln       = ls_zarn_pir-gln
                  active_lp = abap_true.
    IF sy-subrc IS INITIAL.
      ls_zarn_reg_pir-waers        = ls_zarn_list_price-currency.     " where ZARN_LIST_PRICE-UOM_CODE=ZARN_REG_PIR-ORDER_UOM_PIM
      ls_zarn_reg_pir-netpr        = ls_zarn_list_price-price_value.    " where ZARN_LIST_PRICE-UOM_CODE=ZARN_REG_PIR-ORDER_UOM_PIM
      ls_zarn_reg_pir-peinh   = ls_zarn_list_price-quantity.       " where ZARN_LIST_PRICE-UOM_CODE=ZARN_REG_PIR-ORDER_UOM_PIM
      ls_zarn_reg_pir-datlb   = ls_zarn_list_price-eff_start_date. " where ZARN_LIST_PRICE-UOM_CODE=ZARN_REG_PIR-ORDER_UOM_PIM
      ls_zarn_reg_pir-prdat     = ls_zarn_list_price-eff_end_date.   "  where ZARN_LIST_PRICE-UOM_CODE=ZARN_REG_PIR-ORDER_UOM_PIM
    ENDIF.

*        ls_zarn_reg_pir-BPRME  "this field value assigned later from ZARN_REG_UOM

*        ls_zarn_reg_pir-EKKOL  " user
    ls_zarn_reg_pir-mwskz   = 'P1'.
    ls_zarn_reg_pir-lifab =  ls_zarn_pir-avail_start_date.
    ls_zarn_reg_pir-lifbi =  ls_zarn_pir-avail_end_date.


    APPEND ls_zarn_reg_pir TO lt_zarn_reg_pir.

  ENDLOOP.

*   ZARN_REG_UOM has the record already ?
  READ TABLE gt_zarn_reg_uom INTO ls_zarn_reg_uom
       WITH KEY idno                   = ls_zarn_reg_pir-idno
                pim_uom_code           = ls_zarn_reg_pir-lower_uom
*                  lower_uom              = ls_zarn_reg_pir-lower_child_uom.
                hybris_internal_code   = ''.
*                  pim_uom_code           = ls_zarn_reg_pir-order_uom_pim
*                  hybris_internal_code   = ls_zarn_reg_pir-hybris_internal_code.
  IF sy-subrc IS INITIAL.
*      ls_zarn_reg_pir-lower_uom            = ls_zarn_reg_uom-lower_uom.
    ls_zarn_reg_pir-lower_child_uom      = ls_zarn_reg_uom-lower_uom.
    ls_zarn_reg_pir-meinh                = ls_zarn_reg_uom-meinh.
    ls_zarn_reg_pir-lower_meinh          = ls_zarn_reg_uom-lower_meinh.
    ls_zarn_reg_pir-num_base_units       = ls_zarn_reg_uom-num_base_units.
    ls_zarn_reg_pir-cat_seqno            = ls_zarn_reg_uom-cat_seqno.
  ENDIF.


  IF pt_zarn_reg_pir[] IS INITIAL.
*       use the newly created table  :-)
    pt_zarn_reg_pir = lt_zarn_reg_pir.
  ELSE.
**  processing when NOT empty !!
**  retain old records and add any new ones identified
    LOOP AT lt_zarn_reg_pir INTO ls_zarn_reg_pir.
*     check if the ZARN_REG_UOM has the record already ?
      READ TABLE pt_zarn_reg_pir ASSIGNING <ls_zarn_reg_pir>
           WITH KEY idno                   = ls_zarn_reg_pir-idno
                    order_uom_pim          = ls_zarn_reg_pir-order_uom_pim
                    hybris_internal_code   = ls_zarn_reg_pir-hybris_internal_code.
      IF sy-subrc IS NOT INITIAL.
        APPEND ls_zarn_reg_pir TO pt_zarn_reg_pir.
        CONTINUE.
      ELSE.
        <ls_zarn_reg_pir>-num_base_units = ls_zarn_reg_pir-num_base_units.
        <ls_zarn_reg_pir>-cat_seqno      = ls_zarn_reg_pir-cat_seqno.
      ENDIF.
    ENDLOOP.

*   re-assign Vendor related details if not done before,
*   this can happen due to master data GAP
    LOOP AT pt_zarn_reg_pir ASSIGNING <ls_zarn_reg_pir>
         WHERE lifnr  IS     INITIAL
           AND gln_no IS NOT INITIAL.
*     Get vendor number
      PERFORM get_vendor_number USING    <ls_zarn_reg_pir>-gln_no
                                CHANGING <ls_zarn_reg_pir>-lifnr.
*     get vendor purchase org data
      PERFORM  get_vendor_purch_org_data USING    gv_ref_purch_org
                                         CHANGING <ls_zarn_reg_pir>.
    ENDLOOP.
  ENDIF.

* logic for Checkbox
* get latest date
  READ TABLE pt_zarn_reg_pir TRANSPORTING NO FIELDS
       WITH KEY pir_rel_eine = abap_true.
  IF sy-subrc IS NOT INITIAL.
    SORT       ps_prod_data-zarn_list_price[] DESCENDING BY last_changed_date.
    READ TABLE ps_prod_data-zarn_list_price[] INTO ls_zarn_list_price INDEX  1.
*   get latest date from national list price
    READ TABLE pt_zarn_reg_pir ASSIGNING <ls_zarn_reg_pir>
         WITH KEY idno                 = ls_zarn_list_price-idno
                  order_uom_pim        = ls_zarn_list_price-uom_code
                  gln_no               = ls_zarn_list_price-gln.    "??
    IF sy-subrc IS INITIAL.
      <ls_zarn_reg_pir>-pir_rel_eine = abap_true.  " latest changed Date from ZARN_LIST_PRICE
    ENDIF.
  ENDIF.

  READ TABLE pt_zarn_reg_pir TRANSPORTING NO FIELDS
       WITH KEY pir_rel_eina = abap_true.
  IF sy-subrc IS NOT INITIAL.
*   get latest date from national PIR
    SORT       ps_prod_data-zarn_pir[] DESCENDING BY effective_date.
    READ TABLE ps_prod_data-zarn_pir[] INTO ls_zarn_pir INDEX  1.
*   get the first record
    READ TABLE pt_zarn_reg_pir ASSIGNING <ls_zarn_reg_pir>
         WITH KEY idno                 = ls_zarn_pir-idno
                  order_uom_pim        = ls_zarn_pir-uom_code
                  hybris_internal_code = ls_zarn_pir-hybris_internal_code.
    IF sy-subrc IS INITIAL.
      <ls_zarn_reg_pir>-pir_rel_eina = abap_true.  " latest effective date from ZARN_PIR
    ENDIF.
  ENDIF.

* Fields from Regional UOM to be displayed in PIR ALV for context
  LOOP AT pt_zarn_reg_pir ASSIGNING <ls_zarn_reg_pir>.
    CLEAR ls_zarn_reg_uom.

    IF <ls_zarn_reg_pir>-order_uom_pim EQ <ls_zarn_reg_pir>-lower_child_uom.
*     most likely the base unit
      READ TABLE gt_zarn_reg_uom INTO ls_zarn_reg_uom
        WITH KEY idno                 = <ls_zarn_reg_pir>-idno
                 pim_uom_code         = <ls_zarn_reg_pir>-order_uom_pim.
      IF sy-subrc IS INITIAL.
        <ls_zarn_reg_pir>-meinh       = ls_zarn_reg_uom-meinh.
        <ls_zarn_reg_pir>-lower_meinh = ls_zarn_reg_uom-lower_meinh.
        <ls_zarn_reg_pir>-unit_purord = ls_zarn_reg_uom-unit_purord.
        <ls_zarn_reg_pir>-bprme       = ls_zarn_reg_uom-meinh.
      ENDIF.
    ELSE.
*     otherwise, the rest of them which do not have a hybris code
      READ TABLE gt_zarn_reg_uom INTO ls_zarn_reg_uom
        WITH KEY idno          = <ls_zarn_reg_pir>-idno
                 pim_uom_code  = <ls_zarn_reg_pir>-order_uom_pim
                 lower_uom     = <ls_zarn_reg_pir>-lower_child_uom.
      IF sy-subrc IS INITIAL.
        <ls_zarn_reg_pir>-meinh       = ls_zarn_reg_uom-meinh.
        <ls_zarn_reg_pir>-lower_meinh = ls_zarn_reg_uom-lower_meinh.
        <ls_zarn_reg_pir>-unit_purord = ls_zarn_reg_uom-unit_purord.
        <ls_zarn_reg_pir>-bprme       = ls_zarn_reg_uom-meinh.
      ENDIF.
    ENDIF.

    <ls_zarn_reg_pir>-bbbnr = <ls_zarn_reg_pir>-gln_no(7).
    <ls_zarn_reg_pir>-bbsnr = <ls_zarn_reg_pir>-gln_no+7(5).
    <ls_zarn_reg_pir>-bubkz = <ls_zarn_reg_pir>-gln_no+12(1).
  ENDLOOP.

  " sort by GLN/Vendor
  SORT gt_zarn_reg_pir BY gln_no cat_seqno.

ENDFORM.                    " PROCESS_REG_pir_SCREEN
*&---------------------------------------------------------------------*
*&      Form  CHECK_REGIONAL_TAB
*&---------------------------------------------------------------------*

FORM check_regional_tab .

* check I care value and disallow
  IF gs_worklist-fsni_icare_value IS INITIAL      AND
     gv_sel_tab                   EQ gc_icare_tab AND
     g_ts_main-pressed_tab        EQ c_ts_main-tab2.
*   back to National tab
    g_ts_main-pressed_tab = c_ts_main-tab1.
    MESSAGE i000 WITH TEXT-206.
    RETURN.
  ENDIF.

ENDFORM.                    " CHECK_REGIONAL_TAB
*&---------------------------------------------------------------------*
*&      Form  REFRESH_REG_ALV
*&---------------------------------------------------------------------*
FORM refresh_reg_alv  USING    po_alv           TYPE REF TO cl_gui_alv_grid
                               pt_fieldcat      TYPE lvc_t_fcat.

  DATA:
    ls_layout TYPE lvc_s_layo,
    ls_row    TYPE i,
    ls_value  TYPE c,
    ls_col    TYPE i,
    ls_row_id TYPE lvc_s_row,
    ls_col_id TYPE  lvc_s_col,
    es_row_no TYPE lvc_s_roid,
    ls_stable TYPE lvc_s_stbl.

** Read column position
  CALL METHOD po_alv->get_current_cell
    IMPORTING
      e_row     = ls_row
      e_value   = ls_value
      e_col     = ls_col
      es_row_id = ls_row_id
      es_col_id = ls_col_id
      es_row_no = es_row_no.

* optimise width
  CALL METHOD po_alv->get_frontend_layout
    IMPORTING
      es_layout = ls_layout.
  IF sy-subrc EQ 0.
    ls_layout-cwidth_opt = abap_true.
    CALL METHOD po_alv->set_frontend_layout
      EXPORTING
        is_layout = ls_layout.
  ENDIF.

* Reassign column position
  CALL METHOD po_alv->set_current_cell_via_id
    EXPORTING
      is_column_id = ls_col_id
      is_row_no    = es_row_no.

  ls_stable-row = abap_true.
  ls_stable-col = abap_true.

  " To avoid dump in case fi_o_grid is initial
  IF NOT po_alv IS INITIAL.
*-- Refresh the table display to reflect changes
    CALL METHOD po_alv->refresh_table_display
      EXPORTING
        is_stable      = ls_stable
        i_soft_refresh = abap_false
      EXCEPTIONS
        finished       = 1
        OTHERS         = 2.

    IF sy-subrc EQ 0.
      " do nothing
    ENDIF.
  ENDIF.

ENDFORM.                    " REFRESH_REG_ALV
*&---------------------------------------------------------------------*
*&      Form  LOCK_ICARE_WORKLIST
*&---------------------------------------------------------------------*
FORM lock_icare_worklist .

  DATA:
    lt_rows TYPE lvc_t_row,
    ls_rows LIKE LINE OF lt_rows.


  IF mytab-activetab EQ gc_icare_tab.

    LOOP AT gt_worklist TRANSPORTING NO FIELDS
         WHERE fsni_icare_value EQ space.  " only for non ICare rows
      ls_rows-index = sy-tabix.
      APPEND ls_rows TO lt_rows.
    ENDLOOP.
*   perform common logic for regional data lock
    PERFORM lock_regional_data  USING lt_rows abap_true.

  ENDIF.
ENDFORM.                    " LOCK_ICARE_WORKLIST
*&---------------------------------------------------------------------*
*&      Form  DELTA_ANALYSIS_DISPLAY
*&---------------------------------------------------------------------*

FORM delta_analysis_display  USING    pt_rows  TYPE lvc_t_row
                                      pv_fanid TYPE zarn_fan_id.

  DATA:
    ls_rows   LIKE LINE OF pt_rows,
    lr_fan_id TYPE RANGE OF zarn_fan_id,
    ls_fan_id LIKE LINE OF lr_fan_id,

    lv_fanid  TYPE zarn_fan_id,
    lv_lines  TYPE i.


  DESCRIBE TABLE pt_rows[] LINES lv_lines.


  CLEAR: lv_fanid.

  LOOP AT pt_rows INTO ls_rows.

*     read selected articles from worklist
    READ TABLE gt_worklist INTO gs_worklist INDEX ls_rows-index.
    IF sy-subrc IS NOT INITIAL.
      CONTINUE.
    ENDIF.

    lv_fanid = gs_worklist-fan_id.

    ls_fan_id-sign   = 'I'.
    ls_fan_id-option = 'EQ'.
    ls_fan_id-low = gs_worklist-fan_id.
    APPEND ls_fan_id TO lr_fan_id.

  ENDLOOP.



  CASE lv_lines.
    WHEN 0.
      SUBMIT zrarn_delta_analysis VIA SELECTION-SCREEN
        WITH p_fan = pv_fanid
        AND RETURN.

    WHEN 1.
      SUBMIT zrarn_delta_analysis VIA SELECTION-SCREEN
        WITH p_fan = lv_fanid
        AND RETURN.

    WHEN OTHERS.
      SUBMIT zrarn_delta_analysis VIA SELECTION-SCREEN
        WITH s_fan IN lr_fan_id[]
        WITH p_multi = 'X'
        AND RETURN.
  ENDCASE.



ENDFORM.                    " DELTA_ANALYSIS_DISPLAY
*&---------------------------------------------------------------------*
*&      Form  INITIATE_VALIDATION_CHECK
*&---------------------------------------------------------------------*
FORM initiate_validation_check .

  DATA:
    lr_excep TYPE REF TO zcx_arn_validation_err.

  TRY.

      CREATE OBJECT gr_validation_engine
        EXPORTING
          iv_event          = zcl_arn_validation_engine=>gc_event_arena_ui
          iv_bapiret_output = abap_true.

    CATCH zcx_arn_validation_err INTO lr_excep.
      MESSAGE i000(zarena_msg) WITH lr_excep->msgv1 lr_excep->msgv2
                                    lr_excep->msgv3 lr_excep->msgv4.
*      LEAVE LIST-PROCESSING.
  ENDTRY.


ENDFORM.                    " INITIATE_VALIDATION_CHECK
*&---------------------------------------------------------------------*
*&      Form  DEFAULT_REGIONAL_ATTRIBUTES
*&---------------------------------------------------------------------*
FORM default_regional_attributes  USING    ps_prod_data   TYPE zsarn_prod_data
                                  CHANGING ps_reg_hdr TYPE zarn_reg_hdr.

  DATA: ls_products   TYPE zarn_products,
        ls_diet_info  TYPE zarn_diet_info,
        ls_ntr_claims TYPE zarn_ntr_claims.

  READ TABLE ps_prod_data-zarn_products[] INTO ls_products INDEX 1.

  ps_reg_hdr-zzattr1   = ls_products-genetically_modified.

  READ TABLE ps_prod_data-zarn_growing[] TRANSPORTING NO FIELDS
    WITH KEY idno      = ps_prod_data-idno
             version   = ps_prod_data-version
             growing_method = 'ORGANIC'.

  IF sy-subrc IS INITIAL.
    ps_reg_hdr-zzattr2   = abap_true.
  ENDIF.

  ps_reg_hdr-zzattr3   = ls_products-irradiated.

  CLEAR ls_diet_info.
  READ TABLE ps_prod_data-zarn_diet_info[] INTO ls_diet_info
    WITH KEY idno      = ps_prod_data-idno
             version   = ps_prod_data-version
             diet_type = 'KOSHER'.
  IF sy-subrc IS INITIAL.
    ps_reg_hdr-zzattr4   = abap_true.
  ENDIF.


  DESCRIBE TABLE ps_prod_data-zarn_allergen[].
  IF sy-tabix GT 0.
    ps_reg_hdr-zzattr5   = abap_true.
  ENDIF.


  CLEAR ls_ntr_claims.
  READ TABLE ps_prod_data-zarn_ntr_claims[] INTO ls_ntr_claims
    WITH KEY idno                    = ps_prod_data-idno
             version                 = ps_prod_data-version
             nutritional_claims      = 'LOW'
             nutritional_claims_elem = 'FAT'.
  IF sy-subrc IS INITIAL.
    ps_reg_hdr-zzattr7   = abap_true.
  ENDIF.

  CLEAR ls_diet_info.
  READ TABLE ps_prod_data-zarn_diet_info[] INTO ls_diet_info
    WITH KEY idno      = ps_prod_data-idno
             version   = ps_prod_data-version
             diet_type = 'HALAL'.
  IF sy-subrc IS INITIAL.
    ps_reg_hdr-zzattr9   = abap_true.
  ENDIF.

  READ TABLE ps_prod_data-zarn_growing[] TRANSPORTING NO FIELDS
    WITH KEY idno      = ps_prod_data-idno
             version   = ps_prod_data-version
             growing_method = 'FREE_RANGE'.

  IF sy-subrc IS INITIAL.
    ps_reg_hdr-zzattr10  = abap_true.
  ENDIF.

  READ TABLE ps_prod_data-zarn_growing[] TRANSPORTING NO FIELDS
    WITH KEY idno      = ps_prod_data-idno
             version   = ps_prod_data-version
             growing_method = 'FREE_FROM_GLUTEN'.

  IF sy-subrc IS INITIAL.
    ps_reg_hdr-zzattr11  = abap_true.
  ENDIF.


  CLEAR ls_ntr_claims.
  READ TABLE ps_prod_data-zarn_ntr_claims[] INTO ls_ntr_claims
    WITH KEY idno                    = ps_prod_data-idno
             version                 = ps_prod_data-version
             nutritional_claims      = 'FREE'
             nutritional_claims_elem = 'SUGAR'.
  IF sy-subrc IS INITIAL.
    ps_reg_hdr-zzattr13  = abap_true.
  ENDIF.

  CLEAR ls_diet_info.
  READ TABLE ps_prod_data-zarn_diet_info[] INTO ls_diet_info
    WITH KEY idno      = ps_prod_data-idno
             version   = ps_prod_data-version
             diet_type = 'VEGAN'.
  IF sy-subrc IS INITIAL.
    ps_reg_hdr-zzattr14  = abap_true.
  ENDIF.

  CLEAR ls_ntr_claims.
  READ TABLE ps_prod_data-zarn_ntr_claims[] INTO ls_ntr_claims
    WITH KEY idno                    = ps_prod_data-idno
             version                 = ps_prod_data-version
             nutritional_claims      = 'LOW'
             nutritional_claims_elem = 'CHOLES'.
  IF sy-subrc IS INITIAL.
    ps_reg_hdr-zzattr22  = abap_true.
  ENDIF.

  CLEAR ls_diet_info.
  READ TABLE ps_prod_data-zarn_diet_info[] INTO ls_diet_info
    WITH KEY idno      = ps_prod_data-idno
             version   = ps_prod_data-version
             diet_type = 'VEGETARIAN'.
  IF sy-subrc IS INITIAL.
    ps_reg_hdr-zzattr23  = abap_true.
  ENDIF.


ENDFORM.                    " DEFAULT_REGIONAL_ATTRIBUTES
*&---------------------------------------------------------------------*
*&      Form  ALV_VARIANT_DEFAULT
*&---------------------------------------------------------------------*

FORM alv_variant_default  CHANGING ps_variant TYPE disvariant.

  ps_variant-report   = sy-repid.
  ps_variant-username = sy-uname.

  CALL FUNCTION 'LVC_VARIANT_DEFAULT_GET'
    EXPORTING
      i_save     = gc_save
    CHANGING
      cs_variant = ps_variant
    EXCEPTIONS
      not_found  = 2.
ENDFORM.                    " ALV_VARIANT_DEFAULT
*&---------------------------------------------------------------------*
*&      Form  TEXT_EDITOR_PRFAM
*&---------------------------------------------------------------------*

FORM text_editor_prfam .

  DATA:
    lv_mode TYPE i.

  IF go_reg_editor_prfam IS INITIAL.

    CREATE OBJECT go_rhtxt02
      EXPORTING
        container_name = gc_rhtxt02.

*   create calls constructor, which initializes, creats and links
*   a TextEdit Control
    CREATE OBJECT go_reg_editor_prfam
      EXPORTING
        style                      = 0
        parent                     = go_rhtxt02
        wordwrap_mode              = cl_gui_textedit=>wordwrap_at_windowborder
        max_number_chars           = 550
        wordwrap_to_linebreak_mode = 1
      EXCEPTIONS
        OTHERS                     = 1.

    IF sy-subrc NE 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.
*   remove status bar for space !
    CALL METHOD go_reg_editor_prfam->set_statusbar_mode
      EXPORTING
        statusbar_mode         = 0
      EXCEPTIONS
        error_cntl_call_method = 1
        invalid_parameter      = 2
        OTHERS                 = 3.

*   remove toolbar - as per users request !
    CALL METHOD go_reg_editor_prfam->set_toolbar_mode
      EXPORTING
        toolbar_mode           = 0
      EXCEPTIONS
        error_cntl_call_method = 1
        invalid_parameter      = 2
        OTHERS                 = 3.

  ELSE.
*   opposite of ALV grid edit mode :-(
    IF gv_edit_toggle IS NOT INITIAL.
      CLEAR lv_mode.
    ELSE.
      ADD 1 TO lv_mode.
    ENDIF.
*   toggle edit/display
    CALL METHOD go_reg_editor_prfam->set_readonly_mode
      EXPORTING
        readonly_mode = lv_mode.
  ENDIF.                               " E
ENDFORM.                    " TEXT_EDITOR_PRFAM
*&---------------------------------------------------------------------*
*&      Form  CALL_TRANSACTION
*&---------------------------------------------------------------------*
FORM call_transaction  USING    pv_tcode TYPE sy-tcode.
  CALL FUNCTION 'CC_CALL_TRANSACTION_NEW_TASK'
    STARTING NEW TASK 'TCODE'
    EXPORTING
      transaction       = pv_tcode
      skip_first_screen = ' '
    EXCEPTIONS
      error             = 1
      OTHERS            = 2.

  IF sy-subrc <> 0.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
              WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.
ENDFORM.                    " CALL_TRANSACTION
*&---------------------------------------------------------------------*
*&      Form  SIMULATE_STANDARD_TERMS
*&---------------------------------------------------------------------*
FORM simulate_standard_terms .

  CONSTANTS:
     lc_gst TYPE zarn_dis_term1 VALUE '1.15'.

  DATA :
     lv_result TYPE decfloat34.

  FIELD-SYMBOLS:
     <lfs_zarn_reg_std_ter>  LIKE LINE OF gt_zarn_reg_std_ter.

* no simulation if
  IF zarn_reg_hdr-no_discount IS INITIAL.

    LOOP AT gt_zarn_reg_std_ter ASSIGNING <lfs_zarn_reg_std_ter>.

      IF gs_price-list_unit              IS NOT INITIAL.

*     Calculation as per category manager Excel formula
        COMPUTE EXACT lv_result =  (  gs_price-list_unit *  ( 1 - ( <lfs_zarn_reg_std_ter>-term1  / 100 ) ) ).
        COMPUTE EXACT lv_result =  (  lv_result * ( 1 - ( <lfs_zarn_reg_std_ter>-term2 / 100 ) ) ).
        COMPUTE EXACT lv_result =  (  lv_result * ( 1 - ( <lfs_zarn_reg_std_ter>-term3 / 100 ) ) ).
        COMPUTE EXACT lv_result =  (  lv_result * ( 1 - ( gs_price-settle  / 100 ) ) ).

        IF <lfs_zarn_reg_std_ter>-wh_margin IS NOT INITIAL.
          COMPUTE       lv_result =  (  lv_result / ( 1 - ( <lfs_zarn_reg_std_ter>-wh_margin / 100 ) ) ).
        ENDIF.
*      COMPUTE EXACT lv_result =  (  lv_result / ( 1 - ( <lfs_zarn_reg_std_ter>-wh_margin / 100 ) ) ).
*     pass result back to 2 decimal
        <lfs_zarn_reg_std_ter>-net_store = lv_result.

      ENDIF.

      IF <lfs_zarn_reg_std_ter>-ret_margin    IS NOT INITIAL.
*       Calculation as per category manager Excel formula
*       COMPUTE EXACT lv_result = ( lv_result  *   lc_gst  ).
        COMPUTE lv_result = ( lv_result  *   lc_gst  ).
        COMPUTE lv_result = ( <lfs_zarn_reg_std_ter>-ret_margin - lv_result ).
        COMPUTE lv_result =  ( ( lv_result / <lfs_zarn_reg_std_ter>-ret_margin  ) * 100 ).
*       pass result back to 2 decimal
        <lfs_zarn_reg_std_ter>-margin =  lv_result.
      ELSE.
        CLEAR <lfs_zarn_reg_std_ter>-margin.

      ENDIF.
      CLEAR lv_result.
    ENDLOOP.
  ENDIF.
ENDFORM.                    " SIMULATE_STANDARD_TERMS
*&---------------------------------------------------------------------*
*&      Form  CONVERT_EAN_WITH_CHECKDIGIT
*&---------------------------------------------------------------------*
FORM convert_ean_with_checkdigit USING fu_v_eantp TYPE numtp
                              CHANGING pv_ean11 TYPE ean11.

  DATA: lv_fromnumber TYPE n LENGTH 20,
        lv_tonumber   TYPE n LENGTH 20,
        lv_ean11      TYPE n LENGTH 20,

        ls_tntp       TYPE tntp,
        ls_nriv       TYPE nriv.




  IF fu_v_eantp IS NOT INITIAL.

    CLEAR: lv_fromnumber, lv_tonumber.

    CLEAR ls_tntp.
    SELECT SINGLE * FROM tntp INTO ls_tntp
      WHERE numtp = fu_v_eantp.
    IF sy-subrc = 0.
      CLEAR ls_nriv.
      SELECT SINGLE * FROM nriv INTO ls_nriv
        WHERE object    = ls_tntp-nkobj
          AND nrrangenr = ls_tntp-nmkre.
      IF sy-subrc = 0.

        lv_fromnumber = ls_nriv-fromnumber.
        lv_tonumber   = ls_nriv-tonumber.

      ENDIF.
    ENDIF.


    lv_ean11 = pv_ean11.

    IF lv_ean11 GE lv_tonumber AND
*       lv_ean11 LE lv_fromnumber AND
       lv_ean11 CO '0123456789'.
* The GTIN & may not be assigned externally
      MESSAGE s084 WITH pv_ean11 DISPLAY LIKE 'E'.
      EXIT.
    ENDIF.
  ENDIF.


  CALL FUNCTION 'EAN_DETERMINE_CHECKDIGIT'
    EXPORTING
      i_ean = pv_ean11
    IMPORTING
      e_ean = pv_ean11.

ENDFORM.                    " CONVERT_EAN_WITH_CHECKDIGIT

*&---------------------------------------------------------------------*
*&      Form  CONVERT_EAN_WITH_EANTP
*&---------------------------------------------------------------------*

FORM convert_ean_with_eantp  USING    pv_eantp TYPE numtp
                             CHANGING pv_ean11 TYPE ean11.

  CALL FUNCTION 'CONVERSION_EAN_OUTPUT'
    EXPORTING
      input   = pv_ean11
      ean_typ = pv_eantp
    IMPORTING
      output  = pv_ean11.


ENDFORM.                    " CONVERT_EAN_WITH_EANTP
*&---------------------------------------------------------------------*
*&      Form  GET_VENDOR_PURCH_ORG_DATA
*&---------------------------------------------------------------------*

FORM get_vendor_purch_org_data  USING    pv_ref_purch_org TYPE ekorg
                                CHANGING ps_zarn_reg_pir  TYPE zsarn_reg_pir_ui.

  IF ps_zarn_reg_pir-lifnr IS INITIAL.
    CLEAR: ps_zarn_reg_pir-ekgrp, ps_zarn_reg_pir-aplfz.
    RETURN.
  ENDIF.

* Vendor master record purchasing organization data
  SELECT SINGLE ekgrp plifz INTO (ps_zarn_reg_pir-ekgrp, ps_zarn_reg_pir-aplfz) "#EC CI_SEL_NESTED
         FROM lfm1
         WHERE lifnr EQ ps_zarn_reg_pir-lifnr
           AND ekorg EQ pv_ref_purch_org.


ENDFORM.                    " GET_VENDOR_PURCH_ORG_DATA
*&---------------------------------------------------------------------*
*&      Form  GET_VENDOR_NUMBER
*&---------------------------------------------------------------------*

FORM get_vendor_number  USING    pv_gln TYPE zarn_gln
                        CHANGING pv_lifnr TYPE elifn.

  DATA: lt_lfa1 TYPE fagl_t_lifnr.

  SELECT lifnr INTO TABLE lt_lfa1 FROM lfa1             "#EC CI_NOFIELD
          WHERE bbbnr EQ pv_gln(7)                   "#EC CI_SEL_NESTED
            AND bbsnr EQ pv_gln+7(5)
            AND bubkz EQ pv_gln+12(1).
  IF sy-dbcnt GT 1.
* multiple Vendors
    pv_lifnr = 'MULTIPLE'. " display blank in case of multiple
    zarn_reg_pir-lifnr = ''.
  ELSEIF sy-dbcnt EQ 1.
    READ TABLE  lt_lfa1 INTO pv_lifnr INDEX 1.
  ENDIF.

ENDFORM.                    " GET_VENDOR_NUMBER
*&---------------------------------------------------------------------*
*&      Form  FILL_TVARV_VALUES
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM fill_tvarv_values .

  DATA ls_tvarvc             TYPE tvarvc.

  FREE gt_tvarv_ean_cate[].

  ls_tvarvc-name = gc_def_ean_cate.
  APPEND ls_tvarvc TO gt_tvarv_ean_cate.

  ls_tvarvc-name = gc_def_prefix_ean_cate.
  APPEND ls_tvarvc TO gt_tvarv_ean_cate.

*-- Global method to fetch the TVARVS
  CALL METHOD zcl_stvarv_constant=>get_stvarv_constant
    CHANGING
      xt_stvarv_constant = gt_tvarv_ean_cate.

ENDFORM.

*&---------------------------------------------------------------------*
*&      Form  POPUP_APPL_LOG
*&---------------------------------------------------------------------*
FORM popup_appl_log USING pt_message TYPE bal_t_msg.

  DATA: ls_log             TYPE bal_s_log,
        ls_display_profile TYPE bal_s_prof,
        lt_log_handle      TYPE bal_t_logh,
        lv_log_handle      TYPE balloghndl,
        lv_text            TYPE text255.

  FIELD-SYMBOLS: <ls_message> LIKE LINE OF pt_message.

* Update SAP Article Number on Basic Screen
  SELECT SINGLE matnr FROM zarn_reg_hdr INTO zarn_reg_hdr-matnr
    WHERE idno = zarn_reg_hdr-idno.

  CHECK NOT pt_message[] IS INITIAL.

  ls_log-object    = 'ZARN'.
  ls_log-subobject = 'ZARN_POPUP'.

  CALL FUNCTION 'BAL_LOG_CREATE'
    EXPORTING
      i_s_log                 = ls_log
    IMPORTING
      e_log_handle            = lv_log_handle
    EXCEPTIONS
      log_header_inconsistent = 1.

  IF sy-subrc EQ 0.
    LOOP AT pt_message ASSIGNING <ls_message>.
      IF  <ls_message>-msgid EQ 'ZARENA_MSG'
      AND <ls_message>-msgno EQ '000'.
        CONCATENATE
          <ls_message>-msgv1
          <ls_message>-msgv2
          <ls_message>-msgv3
          <ls_message>-msgv4
          INTO lv_text.
        CALL FUNCTION 'BAL_LOG_MSG_ADD_FREE_TEXT'
          EXPORTING
            i_log_handle     = lv_log_handle
            i_msgty          = <ls_message>-msgty
            i_text           = lv_text
          EXCEPTIONS
            log_not_found    = 1
            msg_inconsistent = 2
            log_is_full      = 3
            OTHERS           = 4.
      ELSE.
        CALL FUNCTION 'BAL_LOG_MSG_ADD'
          EXPORTING
            i_log_handle     = lv_log_handle
            i_s_msg          = <ls_message>
          EXCEPTIONS
            log_not_found    = 1
            msg_inconsistent = 2
            log_is_full      = 3.
      ENDIF.
    ENDLOOP.

    CALL FUNCTION 'BAL_DSP_PROFILE_POPUP_GET'
*     EXPORTING
*       START_COL                 = 5
*       START_ROW                 = 5
*       END_COL                   = 87
*       END_ROW                   = 25
      IMPORTING
        e_s_display_profile = ls_display_profile.

    APPEND lv_log_handle TO lt_log_handle.
    CALL FUNCTION 'BAL_DSP_LOG_DISPLAY'
      EXPORTING
        i_s_display_profile  = ls_display_profile
        i_t_log_handle       = lt_log_handle
*       I_AMODAL             = ' '
      EXCEPTIONS
        profile_inconsistent = 1
        internal_error       = 2
        no_data_available    = 3
        no_authority         = 4.
  ENDIF.

ENDFORM.                    " POPUP_APPL_LOG
*&---------------------------------------------------------------------*
*&      Form  REFRESH_SCREEN_FIELD
*&---------------------------------------------------------------------*
* Refresh Screen fields after Article Posting
*&---------------------------------------------------------------------*
FORM refresh_screen_field.

* Update SAP Article Number on Basic Screen
  SELECT SINGLE matnr status FROM zarn_reg_hdr
    INTO (zarn_reg_hdr-matnr, zarn_reg_hdr-status)
    WHERE idno = zarn_reg_hdr-idno.

* Update Article Version
  SELECT SINGLE article_ver FROM zarn_ver_status
    INTO zarn_ver_status-article_ver
    WHERE idno = zarn_reg_hdr-idno.

* Update National Approvals Status
  SELECT SINGLE version_status FROM zarn_prd_version
     INTO  zarn_prd_version-version_status
     WHERE idno    = gs_worklist-idno
       AND version = gs_worklist-version.

* Update statuses in internal tables that are used for building of Approval Matrix
  READ TABLE gt_prd_version ASSIGNING FIELD-SYMBOL(<ls_prd_version>)
     WITH KEY idno = gs_worklist-idno
           version = gs_worklist-version.
  IF sy-subrc EQ 0.
    <ls_prd_version>-version_status = zarn_prd_version-version_status.
  ENDIF.

  READ TABLE gt_reg_data ASSIGNING FIELD-SYMBOL(<ls_reg_data>) WITH KEY idno = gs_worklist-idno.
  IF sy-subrc EQ 0.
    READ TABLE <ls_reg_data>-zarn_reg_hdr ASSIGNING FIELD-SYMBOL(<ls_reg_hdr>) WITH KEY idno = gs_worklist-idno.
    IF sy-subrc EQ 0.
      <ls_reg_hdr>-matnr  = zarn_reg_hdr-matnr.
      <ls_reg_hdr>-status = zarn_reg_hdr-status.
    ENDIF.
  ENDIF.


* INS Begin of Change 3169 JKH 01.11.2016
  " fill the version on the GUI...
  PERFORM fill_version_desc.
* INS End of Change 3169 JKH 01.11.2016



ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  WORKLIST_VALIDATION
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->P_GT_ROWS  text
*----------------------------------------------------------------------*
FORM worklist_validation  USING pt_rows TYPE lvc_t_row.

  DATA:
    ls_rows     LIKE LINE OF  pt_rows,
    lr_obj_idno TYPE RANGE OF zarn_products-idno,
    ls_obj_idno LIKE LINE OF  lr_obj_idno.

* from ALV Worklist
  LOOP AT pt_rows INTO ls_rows.

*     read selected articles from worklist
    READ TABLE gt_worklist INTO gs_worklist INDEX ls_rows-index.
    IF sy-subrc IS NOT INITIAL.
      CONTINUE.
    ENDIF.

    ls_obj_idno-sign   = 'I'.
    ls_obj_idno-option = 'EQ'.
    ls_obj_idno-low = gs_worklist-idno.
    APPEND ls_obj_idno TO lr_obj_idno.
  ENDLOOP.


* Call Change document report already created
  IF  lr_obj_idno[] IS NOT INITIAL.
    " from worklist
    SUBMIT zrarn_check
    WITH so_idn IN lr_obj_idno
    WITH so_event-low EQ 'REG_UI'
    AND RETURN.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  ALV_ADDIT_INFO
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM alv_addit_info .

  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo.

  IF go_nat_addit_info_alv IS INITIAL.
* Nutritional Information
    CREATE OBJECT go_nncc10
      EXPORTING
        container_name = gc_nncc10.

* Create ALV grid
    CREATE OBJECT go_nat_addit_info_alv
      EXPORTING
        i_parent = go_nncc10.


*   Call GRID
    PERFORM alv_build_fieldcat USING gc_alv_nat_addit_info CHANGING lt_fieldcat.
**  Exclude all edit functions in this example since we do not need them:
    PERFORM exclude_tb_functions_display CHANGING lt_exclude.


*    ls_layout-cwidth_opt = abap_true.
    ls_layout-zebra      = abap_true.
    ls_layout-no_toolbar = abap_true.

    CALL METHOD go_nat_addit_info_alv->set_table_for_first_display
      EXPORTING
        is_layout                     = ls_layout
        it_toolbar_excluding          = lt_exclude
      CHANGING
        it_outtab                     = gt_zarn_addit_info
        it_fieldcatalog               = lt_fieldcat
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.

    IF sy-subrc IS NOT INITIAL.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

  ELSE.

    CALL METHOD go_nat_addit_info_alv->refresh_table_display.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  ALV_COMM_CHANNEL
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM alv_comm_channel .

  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo.

  IF go_nat_comm_ch_alv IS INITIAL.
* Nutritional Information
    CREATE OBJECT go_nncc12
      EXPORTING
        container_name = gc_nncc12.

* Create ALV grid
    CREATE OBJECT go_nat_comm_ch_alv
      EXPORTING
        i_parent = go_nncc12.


*   Call GRID
    PERFORM alv_build_fieldcat USING gc_alv_nat_comm_ch CHANGING lt_fieldcat.
**  Exclude all edit functions in this example since we do not need them:
    PERFORM exclude_tb_functions_display CHANGING lt_exclude.


    ls_layout-cwidth_opt = abap_true.
    ls_layout-zebra      = abap_true.
    ls_layout-no_toolbar = abap_true.

    CALL METHOD go_nat_comm_ch_alv->set_table_for_first_display
      EXPORTING
        is_layout                     = ls_layout
        it_toolbar_excluding          = lt_exclude
      CHANGING
        it_outtab                     = gt_zarn_pir_comm_ui
        it_fieldcatalog               = lt_fieldcat
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.

    IF sy-subrc IS NOT INITIAL.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

  ELSE.

    CALL METHOD go_nat_comm_ch_alv->refresh_table_display.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  DISP_MULTI_VALUES
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM disp_multi_values USING pv_text TYPE char20
                             p_t_data TYPE STANDARD TABLE .

  DATA:
    lr_table               TYPE REF TO cl_salv_table,
    lo_dialogbox_cont_gtin TYPE REF TO cl_gui_dialogbox_container.

  IF go_dialogbox_cont_gtin IS INITIAL.
    CREATE OBJECT go_dialogbox_cont_gtin
      EXPORTING
        top     = 60
        left    = 1000
        caption = pv_text
        width   = 350
        height  = 150.
    SET HANDLER go_event_receiver->handle_close FOR go_dialogbox_cont_gtin.

  ELSE.

    CALL METHOD go_dialogbox_cont_gtin->set_visible
      EXPORTING
        visible = abap_true.
  ENDIF.

  TRY.
      cl_salv_table=>factory( EXPORTING r_container = go_dialogbox_cont_gtin
                              IMPORTING r_salv_table = lr_table
                              CHANGING t_table = p_t_data ).
      lr_table->display( ).
    CATCH cx_salv_msg.
  ENDTRY.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  ALV_INS_CODE
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM alv_ins_code .
  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo.

  IF go_nat_ins_code_alv IS INITIAL.
* Nutritional Information
    CREATE OBJECT go_nncc13
      EXPORTING
        container_name = gc_nncc13.

* Create ALV grid
    CREATE OBJECT go_nat_ins_code_alv
      EXPORTING
        i_parent = go_nncc13.


*   Call GRID
    PERFORM alv_build_fieldcat USING gc_alv_nat_ins_code CHANGING lt_fieldcat.
**  Exclude all edit functions in this example since we do not need them:
    PERFORM exclude_tb_functions_display CHANGING lt_exclude.


*    ls_layout-cwidth_opt = abap_true.
    ls_layout-zebra      = abap_true.
    ls_layout-no_toolbar = abap_true.

    CALL METHOD go_nat_ins_code_alv->set_table_for_first_display
      EXPORTING
        is_layout                     = ls_layout
        it_toolbar_excluding          = lt_exclude
      CHANGING
        it_outtab                     = gt_zarn_ins_code
        it_fieldcatalog               = lt_fieldcat
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.

    IF sy-subrc IS NOT INITIAL.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

  ELSE.

    CALL METHOD go_nat_ins_code_alv->refresh_table_display.
  ENDIF.
ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  ALV_CDT_CODE
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM alv_cdt_code .

  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo.

  IF go_nat_cdt_code_alv IS INITIAL.
* Nutritional Information
    CREATE OBJECT go_nncc14
      EXPORTING
        container_name = gc_nncc14.

* Create ALV grid
    CREATE OBJECT go_nat_cdt_code_alv
      EXPORTING
        i_parent = go_nncc14.


*   Call GRID
    PERFORM alv_build_fieldcat USING gc_alv_nat_cdt_code CHANGING lt_fieldcat.
**  Exclude all edit functions in this example since we do not need them:
    PERFORM exclude_tb_functions_display CHANGING lt_exclude.


*    ls_layout-cwidth_opt = abap_true.
    ls_layout-zebra      = abap_true.
    ls_layout-no_toolbar = abap_true.

    CALL METHOD go_nat_cdt_code_alv->set_table_for_first_display
      EXPORTING
        is_layout                     = ls_layout
        it_toolbar_excluding          = lt_exclude
      CHANGING
        it_outtab                     = gt_zarn_cdt_level
        it_fieldcatalog               = lt_fieldcat
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.

    IF sy-subrc IS NOT INITIAL.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

  ELSE.

    CALL METHOD go_nat_cdt_code_alv->refresh_table_display.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  ALV_NUTRI_CLAIM
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM alv_nutri_claim .

  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo.

  IF go_nat_nutri_clm_alv IS INITIAL.
* Nutritional Information
    CREATE OBJECT go_nncc15
      EXPORTING
        container_name = gc_nncc15.

* Create ALV grid
    CREATE OBJECT go_nat_nutri_clm_alv
      EXPORTING
        i_parent = go_nncc15.


*   Call GRID
    PERFORM alv_build_fieldcat USING gc_alv_nat_nutri_clm CHANGING lt_fieldcat.
**  Exclude all edit functions in this example since we do not need them:
    PERFORM exclude_tb_functions_display CHANGING lt_exclude.


*    ls_layout-cwidth_opt = abap_true.
    ls_layout-zebra      = abap_true.
    ls_layout-no_toolbar = abap_true.

    CALL METHOD go_nat_nutri_clm_alv->set_table_for_first_display
      EXPORTING
        is_layout                     = ls_layout
        it_toolbar_excluding          = lt_exclude
      CHANGING
        it_outtab                     = gt_zarn_ntr_claims
        it_fieldcatalog               = lt_fieldcat
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.

    IF sy-subrc IS NOT INITIAL.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

  ELSE.

    CALL METHOD go_nat_nutri_clm_alv->refresh_table_display.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  ALV_NUTRI_STAR
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM alv_nutri_star .

  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo.

  IF go_nat_nutri_str_alv IS INITIAL.
* Nutritional Information
    CREATE OBJECT go_nncc16
      EXPORTING
        container_name = gc_nncc16.

* Create ALV grid
    CREATE OBJECT go_nat_nutri_str_alv
      EXPORTING
        i_parent = go_nncc16.


*   Call GRID
    PERFORM alv_build_fieldcat USING gc_alv_nat_nutri_str CHANGING lt_fieldcat.
**  Exclude all edit functions in this example since we do not need them:
    PERFORM exclude_tb_functions_display CHANGING lt_exclude.


*    ls_layout-cwidth_opt = abap_true.
    ls_layout-zebra      = abap_true.
    ls_layout-no_toolbar = abap_true.

    CALL METHOD go_nat_nutri_str_alv->set_table_for_first_display
      EXPORTING
        is_layout                     = ls_layout
        it_toolbar_excluding          = lt_exclude
      CHANGING
        it_outtab                     = gt_zarn_ntr_health
        it_fieldcatalog               = lt_fieldcat
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.

    IF sy-subrc IS NOT INITIAL.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

  ELSE.

    CALL METHOD go_nat_nutri_str_alv->refresh_table_display.
  ENDIF.


ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  ALV_PREP_TYPE
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM alv_prep_type .

  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo.

  IF go_nat_prep_type_alv IS INITIAL.
* Nutritional Information
    CREATE OBJECT go_nncc17
      EXPORTING
        container_name = gc_nncc17.

* Create ALV grid
    CREATE OBJECT go_nat_prep_type_alv
      EXPORTING
        i_parent = go_nncc17.


*   Call GRID
    PERFORM alv_build_fieldcat USING gc_alv_nat_prep_type CHANGING lt_fieldcat.
**  Exclude all edit functions in this example since we do not need them:
    PERFORM exclude_tb_functions_display CHANGING lt_exclude.


*    ls_layout-cwidth_opt = abap_true.
    ls_layout-zebra      = abap_true.
    ls_layout-no_toolbar = abap_true.

    CALL METHOD go_nat_prep_type_alv->set_table_for_first_display
      EXPORTING
        is_layout                     = ls_layout
        it_toolbar_excluding          = lt_exclude
      CHANGING
        it_outtab                     = gt_zarn_prep_type
        it_fieldcatalog               = lt_fieldcat
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.

    IF sy-subrc IS NOT INITIAL.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

  ELSE.

    CALL METHOD go_nat_prep_type_alv->refresh_table_display.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  ALV_GROWING
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM alv_growing .

  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo.

  IF go_nat_growing_alv IS INITIAL.
* Nutritional Information
    CREATE OBJECT go_nncc18
      EXPORTING
        container_name = gc_nncc18.

* Create ALV grid
    CREATE OBJECT go_nat_growing_alv
      EXPORTING
        i_parent = go_nncc18.


*   Call GRID
    PERFORM alv_build_fieldcat USING gc_alv_nat_growing CHANGING lt_fieldcat.
**  Exclude all edit functions in this example since we do not need them:
    PERFORM exclude_tb_functions_display CHANGING lt_exclude.


*    ls_layout-cwidth_opt = abap_true.
    ls_layout-zebra      = abap_true.
    ls_layout-no_toolbar = abap_true.

    CALL METHOD go_nat_growing_alv->set_table_for_first_display
      EXPORTING
        is_layout                     = ls_layout
        it_toolbar_excluding          = lt_exclude
      CHANGING
        it_outtab                     = gt_zarn_growing
        it_fieldcatalog               = lt_fieldcat
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.

    IF sy-subrc IS NOT INITIAL.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

  ELSE.

    CALL METHOD go_nat_growing_alv->refresh_table_display.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  FREE_OBJECTS
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM free_objects .
  " free objects
  FREE:
  go_docking_alv, go_dialogbox_container, go_dialogbox_cont_gtin, go_worklist_alv,
  go_ver_stat_alv, go_nutri_alv ,go_nat_uom_alv , go_nat_price_alv , go_nat_ingr_alv ,
  go_nat_addi_alv , go_nat_addit_info_alv, go_nat_hsno_alv , go_nat_comm_ch_alv ,
  go_nat_alle_alv , go_nat_diet_alv , go_nat_organ_alv, go_nat_chem_alv ,
  go_nat_ins_code_alv , go_nat_cdt_code_alv , go_nat_nutri_clm_alv ,
  go_nat_nutri_str_alv , go_nat_prep_type_alv ,go_nat_growing_alv , go_nat_pir_alv ,
  go_nat_net_qty_alv, go_nat_serve_size_alv,
  go_reg_uom_alv, go_reg_gtn_alv, go_reg_pir_alv, go_reg_rb_alv , go_reg_pf_alv ,
  go_reg_con_rrp_alv, go_reg_con_dis_alv , go_reg_editor, go_reg_con_hsno_alv,
  go_reg_con_artlink_alv, go_reg_con_onlcat_alv, go_reg_allerg_alv ,

  go_reg_editor_prfam , go_reg_editor_desc_n_sh, go_reg_editor_desc_n_md,
  go_reg_editor_desc_n_lg, go_reg_editor_desc_r_sh, go_reg_editor_desc_r_md,
  go_reg_editor_desc_r_lg, go_reg_editor_ingr_stmt_r, go_reg_editor_ingr_stmt_n, go_reg_editor_ingr_stmt_reg,

  go_rhcc01, go_rhtxt01 , go_rhtxt02 , go_ruomcc01,
  go_rhtxt03, go_rhtxt04, go_rhtxt05, go_rhtxt06, go_rhtxt07, go_rhtxt08, go_rhtxt09, go_rhtxt10,

  go_rpfcc02 , go_rconcc01, go_rconcc02, go_rconcc03, go_rconcc04, go_rpcc05  , go_rrbcc06 ,
  go_rgtcc07 ,  go_nncc01  , go_nucc02  , go_nlcc03  , go_nicc04  , go_nacc04  , go_npcc05  ,
  go_necc06  , go_ndcc07  , go_nccc08  , go_nocc09  , go_nncc10  , go_nncc11  , go_nncc12  ,
  go_nncc13  , go_nncc14  , go_nncc15  , go_nncc16  , go_nncc17, go_nncc18 , go_rconcc05,
  go_rconcc06, go_roccc08,

  go_container, go_splitter, go_container_1, go_picture_1, gr_validation_engine ,
  gr_gui_load,

  " free global tables
  gt_tvarv_ean_cate, gt_values, gt_bdcdata,gt_tabname, gt_rows,
  gt_data, gt_data_org, gt_worklist,gt_line, gt_line_prfam, gt_reg_data,
  gt_nat_alv_uom, gt_zarn_prod_uom_t, gt_zarn_uom_cat, gt_prod_data,
  gt_prd_version, gt_ver_status, gt_control, gt_cc_hdr, gt_cc_det, gt_prod_str,

  gs_prod_str, gs_prod_data_o, gs_prod_data_n, gs_prod_data, gs_nat_alv_uom,
  gs_reg_data_o, gs_reg_data_n, gs_reg_data, gs_reg_data_copy,
  gs_values, gs_worklist,


** National Tables
  gt_products, gt_products_ex, gt_cdt_level, gt_zarn_nutrients,
  gt_zarn_ntr_health, gt_ntr_claimtx, gt_zarn_ntr_claims,gt_zarn_allergen,
  gt_hsno, gt_dgi_margin,gt_zarn_prep_type, gt_ins_code,gt_zarn_diet_info,
  gt_zarn_additives,gt_zarn_growing,gt_zarn_fb_ingre,gt_zarn_chem_char,
  gt_zarn_organism,   gt_nat_mc_hier, gt_zarn_addit_info, gt_zarn_uom_variant,
  gt_zarn_gtin_var, gt_zarn_pir, gt_pir_comm_ch, gt_zarn_list_price, gt_zarn_hsno,
  gt_zarn_pir_comm_ui, gt_zarn_pir_comm_ch, gt_zarn_cdt_level, gt_zarn_ins_code,
  gt_zarn_bioorganism, gt_zarn_net_qty, gt_zarn_serve_size,

** National structures
  gs_cdt_level, gs_nutrients, gs_ntr_claimtx, gs_allergen, gs_hsno, gs_dgi_margin,
  gs_ins_code, gs_diet_info, gs_additives, gs_fb_ingre, gs_chem_char, gs_organism,
  gs_nat_mc_hier, gs_addit_info, gs_uom_variant, gs_gtin_var, gs_pir , gs_pir_comm_ch,
  gs_list_price,

* * Regional tables
  gt_zarn_reg_hdr, gt_zarn_reg_banner, gt_zarn_reg_onlcat,             "++ONLD-835 JKH 12.05.2017
  gt_zarn_reg_ean, gt_zarn_reg_pir, gt_zarn_reg_prfam, gt_zarn_reg_txt,
  gt_zarn_reg_uom, gt_zarn_reg_hsno, gt_zarn_reg_artlink, gt_zarn_reg_onlcat,
  gt_zarn_reg_str, gt_zarn_reg_allerg, " gt_zarn_reg_sc,                 "9000004661 New Supply Chain Table

* Regional condition tables
* gt_zarn_reg_dc_sell,
  gt_zarn_reg_rrp,  gt_zarn_reg_std_ter,

* Ranges for upper case
  gr_short_descr, gr_gln_descr,

* Regional structures
  gs_reg_hdr, gs_reg_banner, gs_reg_ean, gs_reg_pir, gs_reg_prfam, gs_reg_txt,
  gs_reg_uom, gs_reg_rrp, gs_reg_hsno, gs_reg_artlink, gt_multi_gtin, gs_reg_onlcat,
  gs_reg_str, gs_reg_allerg, "gs_reg_sc, 9000004661

* Text tables for screen
  gt_brands, gt_t179t, gs_t179t, gs_text,

* Variables
  gv_bunit_drdn, gv_catalog_type_drdn.


ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  FILL_VERSION_DESC
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM fill_version_desc .

  DATA lt_version TYPE ztarn_prd_version.
  DATA ls_version TYPE zarn_prd_version.

  CHECK gs_worklist-idno IS NOT INITIAL.

  SELECT * FROM zarn_prd_version INTO TABLE lt_version
    WHERE idno = gs_worklist-idno.
  IF sy-subrc EQ 0.
    CLEAR ls_version .
    " current version status
    READ TABLE lt_version INTO ls_version
      WITH KEY idno = gs_worklist-idno version = zarn_ver_status-current_ver.
    IF sy-subrc EQ 0.
      gs_zarn_prd_version-cversion_status = ls_version-version_status.
    ENDIF.

    CLEAR ls_version .
    " latest version status
    READ TABLE lt_version INTO ls_version
      WITH KEY idno = gs_worklist-idno version = zarn_ver_status-latest_ver.
    IF sy-subrc EQ 0.
      gs_zarn_prd_version-lversion_status = ls_version-version_status.
    ENDIF.

    CLEAR ls_version .
    " article version status
    READ TABLE lt_version INTO ls_version
      WITH KEY idno = gs_worklist-idno version = zarn_ver_status-article_ver.
    IF sy-subrc EQ 0.
      gs_zarn_prd_version-aversion_status = ls_version-version_status.
    ENDIF.

  ENDIF.


  IF zarn_reg_hdr-idno IS NOT INITIAL.
    SELECT SINGLE status FROM zarn_reg_hdr
      INTO zarn_reg_hdr-status
      WHERE idno = gs_worklist-idno.
  ENDIF.




ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  GET_VENDOR_TEXT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM get_vendor_text .
  SELECT SINGLE name1 INTO gs_text-name1 FROM lfa1   "#EC CI_SEL_NESTED
         WHERE lifnr = zarn_reg_pir-lifnr.
  IF sy-subrc IS NOT INITIAL.
    CLEAR gs_text-name1.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  SET_NAT_INTENSIFY_FLAGS
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM set_nat_intensify_flags .

  " Food and Safety
  IF zarn_products_ex-alcoholic_info IS NOT INITIAL
    OR gt_zarn_cdt_level[] IS NOT INITIAL
    OR gt_zarn_allergen[] IS NOT INITIAL
    OR gs_prod_str-allergen_statement IS NOT INITIAL
    OR gt_zarn_diet_info[] IS NOT INITIAL
    OR gt_zarn_chem_char[] IS NOT INITIAL
    OR gt_zarn_organism[] IS NOT INITIAL
    OR gt_zarn_bioorganism[] IS NOT INITIAL
    OR gt_zarn_ins_code[] IS NOT INITIAL
    OR zarn_products-trade_item_handling_info IS NOT INITIAL
    OR zarn_products-stor_handl_max_temp_val IS NOT INITIAL
    OR zarn_products-stor_handl_max_temp_uom IS NOT INITIAL
    OR zarn_products-stor_handl_min_temp_val IS NOT INITIAL
    OR zarn_products-stor_handl_min_temp_uom IS NOT INITIAL.

    gv_nat_fsi = abap_true.
  ENDIF.

  " Consumer Info.
  IF gt_zarn_prep_type[] IS NOT INITIAL
    OR gt_zarn_growing[] IS NOT INITIAL
    OR zarn_products-preparation_info IS NOT INITIAL
    OR zarn_products-target_consumer_age IS NOT INITIAL
    OR zarn_products-target_consumer_gender IS NOT INITIAL
    OR gs_prod_str-cons_usage_ins IS NOT INITIAL
    OR gs_prod_str-cons_storage_ins IS NOT INITIAL
    OR gs_prod_str-preparation_instructions IS NOT INITIAL.

    gv_nat_ci = abap_true.
  ENDIF.

  " Nutritional Info.
  IF zarn_products-ni_num_serves_per_pack  IS NOT INITIAL
    OR zarn_products-health_star_rating IS NOT INITIAL
    OR gt_zarn_ntr_health[] IS NOT INITIAL
    OR gt_zarn_ntr_claims[] IS NOT INITIAL
    OR gs_ntr_claimtx IS NOT INITIAL
    OR gt_zarn_nutrients[] IS NOT INITIAL.
*    OR gs_prod_str-ingredient_statement IS NOT INITIAL
*    OR zarn_fb_ingre-place_of_activity IS NOT INITIAL
*    OR gt_zarn_additives[] IS NOT INITIAL
*    OR gt_zarn_fb_ingre[] IS NOT INITIAL.

    gv_nat_ni = abap_true.
  ENDIF.

  " Additional Info.
  IF zarn_products-task_id IS NOT INITIAL
    OR zarn_products-fish_catch_zone IS NOT INITIAL
    OR zarn_products_ex-labelling_claims IS NOT INITIAL
    OR zarn_products_ex-fs_cust_short_descr IS NOT INITIAL
    OR zarn_products_ex-full_description IS NOT INITIAL
    OR zarn_products_ex-feature_benefit IS NOT INITIAL
    OR zarn_products_ex-promotional_groups IS NOT INITIAL
    OR gs_prod_str-serving_suggestion IS NOT INITIAL
    OR gs_prod_str-marketing_message IS NOT INITIAL
    OR gs_prod_str-additional_description IS NOT INITIAL
    OR gs_prod_str-fs_cooking_ins IS NOT INITIAL
    OR gs_prod_str-precautions IS NOT INITIAL
    OR gs_prod_str-fs_storage_ins IS NOT INITIAL
    OR gs_prod_str-fs_preparation_ins IS NOT INITIAL
    OR gs_prod_str-fs_tips_and_advice IS NOT INITIAL
    OR gs_prod_str-fs_cust_long_descr IS NOT INITIAL
    OR gs_prod_str-fs_cust_medium_descr IS NOT INITIAL
    OR gt_zarn_addit_info[] IS NOT INITIAL.

    gv_nat_ai = abap_true.
  ENDIF.

  " Dangerous goods info.
  IF zarn_products-dangerous_goods_info IS NOT INITIAL
    OR zarn_products-dangerous_good IS NOT INITIAL
    OR zarn_products-hazardous_good IS NOT INITIAL
    OR zarn_products-flash_pt_temp_val IS NOT INITIAL
    OR zarn_products-flash_pt_temp_uom IS NOT INITIAL
    OR gt_zarn_hsno[] IS NOT INITIAL.

    gv_nat_dgi = abap_true.
  ENDIF.

  " Ingredient information
  IF gs_prod_str-ingredient_statement IS NOT INITIAL
  OR zarn_fb_ingre-place_of_activity IS NOT INITIAL
  OR gt_zarn_additives[] IS NOT INITIAL
  OR gt_zarn_fb_ingre[] IS NOT INITIAL.

    gv_nat_ii = abap_true.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  REGISTER_F4_EVENT_LOWER_SAPUOM
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM register_f4_event USING p_go_alv TYPE REF TO cl_gui_alv_grid
                                          p_fname  TYPE fieldname.

  DATA: lt_f4 TYPE lvc_t_f4,
        ls_f4 TYPE lvc_s_f4.

  ls_f4-fieldname = p_fname.
  ls_f4-register  = abap_true.

  INSERT ls_f4 INTO TABLE lt_f4.

  CALL METHOD p_go_alv->register_f4_for_fields
    EXPORTING
      it_f4 = lt_f4[].

*-- Set handler for event F4
  SET HANDLER go_event_receiver_edit->handle_on_f4 FOR p_go_alv.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  DEFAULT_PLU_ARTICLE_111
*&---------------------------------------------------------------------*
* Default PLU Article
*----------------------------------------------------------------------*
FORM default_plu_article_111 USING fu_s_mod_row_ean TYPE zsarn_reg_ean_ui.


  DATA: lt_zarn_reg_ean TYPE ztarn_reg_ean_ui,
        ls_zarn_reg_ean TYPE zsarn_reg_ean_ui.

* If "Scale Rel." is ticked then "PLU Article" will be ticked automatically
  IF zarn_reg_hdr-scagr = 'YES'.
    zarn_reg_hdr-plu_article = abap_true.
    EXIT.
  ENDIF.

* If ZP barcode is entered then "PLU Article to be ticked"
  IF fu_s_mod_row_ean-eantp = 'ZP'.
    zarn_reg_hdr-plu_article = abap_true.
    EXIT.
  ENDIF.


  lt_zarn_reg_ean[] = gt_zarn_reg_ean[].

  DELETE lt_zarn_reg_ean WHERE idno  = fu_s_mod_row_ean-idno
                           AND meinh = fu_s_mod_row_ean-meinh
                           AND ean11 = fu_s_mod_row_ean-ean11.


* If ZP EAN exists then cannot remove ""PLU Article""
  CLEAR ls_zarn_reg_ean.
  READ TABLE lt_zarn_reg_ean INTO ls_zarn_reg_ean
  WITH KEY idno  = zarn_reg_hdr-idno
           eantp = 'ZP'.
  IF sy-subrc = 0.
    zarn_reg_hdr-plu_article = abap_true.
    EXIT.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  CHECK_MTART
*&---------------------------------------------------------------------*
FORM check_mtart.

  DATA: ls_t023 TYPE t023.

  CHECK NOT zarn_reg_hdr-mtart IS INITIAL.

* Only when not in deisplay mode
  IF gv_display EQ abap_false.
*   Only when on Regional tab
    IF g_ts_main-pressed_tab EQ c_ts_main-tab2.
*   do we have an existing article ?
      IF NOT gs_mara-matnr IS INITIAL.
        IF gs_mara-mtart NE zarn_reg_hdr-mtart.
* SUP-114 change message to error
*          MESSAGE w056 WITH zarn_reg_hdr-mtart gs_mara-mtart.
          MESSAGE e056 WITH zarn_reg_hdr-mtart gs_mara-mtart.
        ENDIF.
      ELSE.
*     No so check if article type matches merchandise reference article type
        IF NOT zarn_reg_hdr-matkl IS INITIAL.
          CLEAR ls_t023.
          SELECT SINGLE wwgda
            INTO ls_t023-wwgda
            FROM t023
            WHERE matkl EQ zarn_reg_hdr-matkl.
          IF NOT ls_t023-wwgda IS INITIAL.
            SELECT SINGLE matnr meins mtart
              INTO CORRESPONDING FIELDS OF gs_mara_mc_ref
              FROM mara
              WHERE matnr EQ ls_t023-wwgda.
            IF sy-subrc EQ 0.
              IF gs_mara_mc_ref-mtart NE zarn_reg_hdr-mtart.
                MESSAGE w057 WITH zarn_reg_hdr-mtart gs_mara_mc_ref-mtart.
              ENDIF.
            ENDIF.
          ENDIF.
        ENDIF.
      ENDIF.
    ENDIF.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  CALL_MM43
*&---------------------------------------------------------------------*
FORM call_mm43.

  IF NOT zarn_reg_hdr-matnr IS INITIAL.
    SET PARAMETER ID 'MAT' FIELD zarn_reg_hdr-matnr.
    CALL TRANSACTION 'MM43'.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  DEFAULT_LEGACY_UOMS_113
*&---------------------------------------------------------------------*
* Default Legacy UOMs for Basic - REG_HDR
*----------------------------------------------------------------------*
FORM default_legacy_uoms_113 .

  DATA: lt_nsap_leg_uom  TYPE ztmd_nsap_leg_uom,
        ls_nsap_leg_uom  TYPE zmd_nsap_leg_uom,

        lt_matkl_range   TYPE RANGE OF matkl,
        ls_matkl_range   LIKE LINE OF lt_matkl_range,

        ls_reg_uom_ret   TYPE zsarn_reg_uom_ui,
        ls_reg_uom_iss   TYPE zsarn_reg_uom_ui,
        ls_reg_uom_ord   TYPE zsarn_reg_uom_ui,


        lv_retail_cat    TYPE zmd_retail_cat,
        lv_issue_cat     TYPE zmd_issue_cat,
        lv_order_cat     TYPE zmd_order_cat,
        lv_pur_only_flag TYPE flag.      "++ONED-217 JKH 24.11.2016

  IF zarn_reg_hdr-leg_retail IS NOT INITIAL AND
     zarn_reg_hdr-leg_repack IS NOT INITIAL AND
     zarn_reg_hdr-leg_bulk   IS NOT INITIAL. " AND
*     zarn_reg_hdr-leg_prboly IS NOT INITIAL.
    EXIT.
  ENDIF.

  CLEAR : ls_reg_uom_ret,
          ls_reg_uom_iss,
          ls_reg_uom_ord.


* Retail Category
  CLEAR lv_retail_cat.
  READ TABLE gt_zarn_reg_uom INTO ls_reg_uom_ret
  WITH KEY idno = zarn_reg_hdr-idno
           unit_base = abap_true.
  IF sy-subrc = 0.
    lv_retail_cat = ls_reg_uom_ret-uom_category.
  ELSE.
    EXIT.
  ENDIF.

* Issue Category
  CLEAR lv_issue_cat.
  READ TABLE gt_zarn_reg_uom INTO ls_reg_uom_iss
  WITH KEY idno = zarn_reg_hdr-idno
           issue_unit = abap_true.
  IF sy-subrc = 0.
    lv_issue_cat = ls_reg_uom_iss-uom_category.
  ELSE.
    EXIT.
  ENDIF.


* Order Category
  CLEAR lv_order_cat.
  READ TABLE gt_zarn_reg_uom INTO ls_reg_uom_ord
  WITH KEY idno = zarn_reg_hdr-idno
           unit_purord = abap_true.
  IF sy-subrc = 0.
    lv_order_cat = ls_reg_uom_ord-uom_category.
  ELSE.
    EXIT.
  ENDIF.



* If any Category is RETAIL, then consider on unit, for others consider Category only

  IF lv_retail_cat = 'RETAIL'.
    lv_retail_cat = ls_reg_uom_ret-meinh.
  ENDIF.

  IF lv_issue_cat = 'RETAIL'.
    lv_issue_cat = ls_reg_uom_iss-meinh.
  ENDIF.

  IF lv_order_cat = 'RETAIL'.
    lv_order_cat = ls_reg_uom_ord-meinh.
  ENDIF.



* Filter Non-SAP Legacy UOMs on MC, ignore if MC is not matching
  LOOP AT gt_nsap_leg_uom[] INTO ls_nsap_leg_uom.

    CLEAR: ls_matkl_range, lt_matkl_range[].

    IF ls_nsap_leg_uom-matkl CS '*'.
      ls_matkl_range-sign = 'I'.
      ls_matkl_range-option = 'CP'.
      ls_matkl_range-low = ls_nsap_leg_uom-matkl.
      APPEND ls_matkl_range TO lt_matkl_range[].
    ELSE.
      ls_matkl_range-sign = 'I'.
      ls_matkl_range-option = 'EQ'.
      ls_matkl_range-low = ls_nsap_leg_uom-matkl.
      APPEND ls_matkl_range TO lt_matkl_range[].
    ENDIF.


    IF zarn_reg_hdr-matkl IN lt_matkl_range[].

* If any Category is RETAIL, then consider on unit, for others consider Category only
      IF ls_nsap_leg_uom-retail_cat = 'RETAIL'.
        ls_nsap_leg_uom-retail_cat = ls_nsap_leg_uom-retail_uom.
      ENDIF.

      IF ls_nsap_leg_uom-issue_cat = 'RETAIL'.
        ls_nsap_leg_uom-issue_cat = ls_nsap_leg_uom-issue_uom.
      ENDIF.

      IF ls_nsap_leg_uom-order_cat = 'RETAIL'.
        ls_nsap_leg_uom-order_cat = ls_nsap_leg_uom-order_uom.
      ENDIF.


      APPEND ls_nsap_leg_uom TO lt_nsap_leg_uom[].
    ENDIF.

  ENDLOOP.


  IF lt_nsap_leg_uom[] IS INITIAL.
    EXIT.
  ENDIF.

* Ignore Irrelevant Unit/Categories
  DELETE lt_nsap_leg_uom[] WHERE retail_cat NE lv_retail_cat.
  DELETE lt_nsap_leg_uom[] WHERE issue_cat  NE lv_issue_cat.
  DELETE lt_nsap_leg_uom[] WHERE order_cat  NE lv_order_cat.



  IF lt_nsap_leg_uom[] IS INITIAL.
    EXIT.
  ENDIF.


* INS Begin of Change ONED-217 JKH 24.11.2016
  CLEAR lv_pur_only_flag.
  IF zarn_reg_hdr-zzbuy_sell = zcl_constants=>gc_buy_sell_3 OR     " '3' - Buying only
     zarn_reg_hdr-pur_only   = abap_true.
    lv_pur_only_flag = abap_true.
  ENDIF.
* INS End of Change ONED-217 JKH 24.11.2016

* Get Exactly matching Record
  CLEAR ls_nsap_leg_uom.
  READ TABLE lt_nsap_leg_uom[] INTO ls_nsap_leg_uom
  WITH KEY retail_cat = lv_retail_cat
           issue_cat  = lv_issue_cat
           order_cat  = lv_order_cat
           pur_only   = lv_pur_only_flag.                 "++ONED-217 JKH 24.11.2016
*           pur_only   = zarn_reg_hdr-pur_only.           "--ONED-217 JKH 24.11.2016
  IF sy-subrc NE 0.
* If not found then get based on Unit/Categor Only
    READ TABLE lt_nsap_leg_uom[] INTO ls_nsap_leg_uom
    WITH KEY retail_cat = lv_retail_cat
             issue_cat  = lv_issue_cat
             order_cat  = lv_order_cat.
  ENDIF.


* Assigh Categories only is initial
  IF ls_nsap_leg_uom IS NOT INITIAL.

    IF zarn_reg_hdr-leg_retail IS INITIAL.
      zarn_reg_hdr-leg_retail = ls_nsap_leg_uom-leg_retail.
    ENDIF.

    IF zarn_reg_hdr-leg_repack IS INITIAL.
      zarn_reg_hdr-leg_repack = ls_nsap_leg_uom-leg_repack.
    ENDIF.

    IF zarn_reg_hdr-leg_bulk   IS INITIAL.
      zarn_reg_hdr-leg_bulk = ls_nsap_leg_uom-leg_bulk.
    ENDIF.

*    IF zarn_reg_hdr-leg_prboly IS INITIAL.
    zarn_reg_hdr-leg_prboly = ls_nsap_leg_uom-leg_prboly.
*    ENDIF.


  ENDIF.




ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  SET_DISABLE_COLUMNS
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM set_disable_columns USING p_t_variant TYPE ztarn_uom_variant.

  DATA ls_celltab   TYPE lvc_s_styl.
  DATA lt_celltab   TYPE lvc_t_styl.
  DATA lt_celltab1  TYPE lvc_t_styl.
  DATA ls_variant   LIKE LINE OF p_t_variant.
  DATA: lt_non_retail       TYPE lvc_t_styl,
        ls_reg_uom_nat_data TYPE zsarn_reg_uom_nat_data,
        lo_abap_structdescr TYPE REF TO cl_abap_structdescr.
  DATA lv_nat_texts_exist TYPE abap_bool.

  FIELD-SYMBOLS:
    <fs_zarn_reg_uom> LIKE LINE OF gt_zarn_reg_uom.

  ls_celltab-style = cl_gui_alv_grid=>mc_style_disabled.

  ls_celltab-fieldname = 'MANDT'.
  INSERT ls_celltab INTO TABLE lt_celltab.
  INSERT ls_celltab INTO TABLE lt_celltab1.

  ls_celltab-fieldname = 'IDNO'.
  INSERT ls_celltab INTO TABLE lt_celltab.
  INSERT ls_celltab INTO TABLE lt_celltab1.

  ls_celltab-fieldname = 'UOM_CATEGORY'.
  INSERT ls_celltab INTO TABLE lt_celltab.
  INSERT ls_celltab INTO TABLE lt_celltab1.

  ls_celltab-fieldname = 'PIM_UOM_CODE'.
  INSERT ls_celltab INTO TABLE lt_celltab.
  INSERT ls_celltab INTO TABLE lt_celltab1.

  ls_celltab-fieldname = 'LOWER_UOM'.
  INSERT ls_celltab INTO TABLE lt_celltab.
  INSERT ls_celltab INTO TABLE lt_celltab1.

  ls_celltab-fieldname = 'HYBRIS_INTERNAL_CODE'.
  INSERT ls_celltab INTO TABLE lt_celltab.
  INSERT ls_celltab INTO TABLE lt_celltab1.

  ls_celltab-fieldname = 'LOWER_CHILD_UOM'.
  INSERT ls_celltab INTO TABLE lt_celltab.
  INSERT ls_celltab INTO TABLE lt_celltab1.

  ls_celltab-fieldname = 'LOWER_MEINH'.
  INSERT ls_celltab INTO TABLE lt_celltab.
  INSERT ls_celltab INTO TABLE lt_celltab1.

  ls_celltab-fieldname = 'HIGHER_LEVEL_UNITS'.
  INSERT ls_celltab INTO TABLE lt_celltab.

  ls_celltab-fieldname = 'RELATIONSHIP'.
  INSERT ls_celltab INTO TABLE lt_celltab.

  ls_celltab-fieldname = 'NUM_BASE_UNITS'.
  INSERT ls_celltab INTO TABLE lt_celltab.

  ls_celltab-fieldname = 'FACTOR_OF_BASE_UNITS'.
  INSERT ls_celltab INTO TABLE lt_celltab.

  ls_celltab-fieldname = 'NET_WEIGHT_UOM'.
  INSERT ls_celltab INTO TABLE lt_celltab.

  ls_celltab-fieldname = 'GROSS_WEIGHT_UOM'.
  INSERT ls_celltab INTO TABLE lt_celltab.

  ls_celltab-fieldname = 'HEIGHT_UOM'.
  INSERT ls_celltab INTO TABLE lt_celltab.

  ls_celltab-fieldname = 'WIDTH_UOM'.
  INSERT ls_celltab INTO TABLE lt_celltab.

  ls_celltab-fieldname = 'DEPTH_UOM'.
  INSERT ls_celltab INTO TABLE lt_celltab.

  ls_celltab-fieldname = 'ERSDA'.
  INSERT ls_celltab INTO TABLE lt_celltab.
  INSERT ls_celltab INTO TABLE lt_celltab1.

  ls_celltab-fieldname = 'ERZET'.
  INSERT ls_celltab INTO TABLE lt_celltab.
  INSERT ls_celltab INTO TABLE lt_celltab1.

  ls_celltab-fieldname = 'CAT_SEQNO'.
  INSERT ls_celltab INTO TABLE lt_celltab.
  INSERT ls_celltab INTO TABLE lt_celltab1.

  ls_celltab-fieldname = 'SEQNO'.
  INSERT ls_celltab INTO TABLE lt_celltab.
  INSERT ls_celltab INTO TABLE lt_celltab1.

  ls_celltab-fieldname = 'OBSOLETE_IND'.
  INSERT ls_celltab INTO TABLE lt_celltab.
  INSERT ls_celltab INTO TABLE lt_celltab1.

  " All National data fields should be disabled
  lo_abap_structdescr ?= cl_abap_structdescr=>describe_by_data( ls_reg_uom_nat_data ).
  DATA(lt_component) = lo_abap_structdescr->get_components( ).
  LOOP AT lt_component INTO DATA(ls_component).
    ls_celltab-fieldname = ls_component-name.
    INSERT ls_celltab INTO TABLE lt_celltab.
    INSERT ls_celltab INTO TABLE lt_celltab1.
  ENDLOOP.

  " These are specific to non-Retail records
  ls_celltab-fieldname = 'NET_WEIGHT_VALUE'.
  INSERT ls_celltab INTO TABLE lt_non_retail.

  ls_celltab-fieldname = 'GROSS_WEIGHT_VALUE'.
  INSERT ls_celltab INTO TABLE lt_non_retail.

  ls_celltab-fieldname = 'HEIGHT_VALUE'.
  INSERT ls_celltab INTO TABLE lt_non_retail.

  ls_celltab-fieldname = 'WIDTH_VALUE'.
  INSERT ls_celltab INTO TABLE lt_non_retail.

  ls_celltab-fieldname = 'DEPTH_VALUE'.
  INSERT ls_celltab INTO TABLE lt_non_retail.

  IF gv_feature_a4_enabled EQ abap_true.
    lv_nat_texts_exist = abap_false.
    LOOP AT p_t_variant ASSIGNING FIELD-SYMBOL(<ls_uom_variant>).
      IF <ls_uom_variant>-net_content_short_desc IS NOT INITIAL
      OR <ls_uom_variant>-fscust_prod_name40     IS NOT INITIAL
      OR <ls_uom_variant>-fscust_prod_name80     IS NOT INITIAL
      OR <ls_uom_variant>-fscust_prod_name255    IS NOT INITIAL.
        lv_nat_texts_exist = abap_true.
        EXIT.
      ENDIF.
    ENDLOOP.
    IF lv_nat_texts_exist EQ abap_false.
      DELETE lt_celltab WHERE fieldname EQ 'NET_CONTENT_SHORT_DESC'.
      DELETE lt_celltab WHERE fieldname EQ 'FSCUST_PROD_NAME40'.
      DELETE lt_celltab WHERE fieldname EQ 'FSCUST_PROD_NAME80'.
      DELETE lt_celltab WHERE fieldname EQ 'FSCUST_PROD_NAME255'.
      ls_celltab-fieldname = 'NET_CONTENT_SHORT_DESC'.
      INSERT ls_celltab INTO TABLE lt_celltab1.
      ls_celltab-fieldname = 'FSCUST_PROD_NAME40'.
      INSERT ls_celltab INTO TABLE lt_celltab1.
      ls_celltab-fieldname = 'FSCUST_PROD_NAME80'.
      INSERT ls_celltab INTO TABLE lt_celltab1.
      ls_celltab-fieldname = 'FSCUST_PROD_NAME255'.
      INSERT ls_celltab INTO TABLE lt_celltab1.
    ENDIF.
  ENDIF.

  ls_celltab-fieldname = 'PKGING_RCYCLING_SCHEME_CODE'.
  INSERT ls_celltab INTO TABLE lt_celltab.
  ls_celltab-fieldname = 'PKGING_RCYCLING_PROCESS_TYPE_C'.
  INSERT ls_celltab INTO TABLE lt_celltab.
  ls_celltab-fieldname = 'SUSTAINABILITY_FEATURE_CODE'.
  INSERT ls_celltab INTO TABLE lt_celltab.

  LOOP AT gt_zarn_reg_uom ASSIGNING <fs_zarn_reg_uom>.

    READ TABLE gt_hyb_int_code TRANSPORTING NO FIELDS
    WITH KEY hyb_int_code = <fs_zarn_reg_uom>-hybris_internal_code.
    IF sy-subrc = 0.
      <fs_zarn_reg_uom>-celltab = lt_celltab1.  " Editable
    ELSE.
      <fs_zarn_reg_uom>-celltab = lt_celltab.   " Display only
    ENDIF.

    IF <fs_zarn_reg_uom>-uom_category <> zif_arn_uom_category_c=>gc_retail.
      INSERT LINES OF lt_non_retail INTO TABLE <fs_zarn_reg_uom>-celltab.
    ENDIF.

  ENDLOOP.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  REGISTER_F4_EVENT_UOM_ALV
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->P_GO_REG_UOM_ALV  text
*      -->P_GC_UOM_L_MEINS  text
*----------------------------------------------------------------------*
FORM register_f4_event_uom_alv  USING p_go_alv TYPE REF TO cl_gui_alv_grid.

  DATA: lt_f4 TYPE lvc_t_f4,
        ls_f4 TYPE lvc_s_f4.

  ls_f4-fieldname = gc_uom_l_meins.
  ls_f4-register  = abap_true.

  INSERT ls_f4 INTO TABLE lt_f4.

  ls_f4-fieldname = gc_hyb_code.
  ls_f4-register  = abap_true.

  INSERT ls_f4 INTO TABLE lt_f4.

  CALL METHOD p_go_alv->register_f4_for_fields
    EXPORTING
      it_f4 = lt_f4[].

*-- Set handler for event F4
  SET HANDLER go_event_receiver_edit->handle_on_f4 FOR p_go_alv.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  REGISTER_F4_EVENT_ONLCAT_ALV
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
FORM register_f4_event_onlcat_alv  USING p_go_alv TYPE REF TO cl_gui_alv_grid.

  DATA: lt_f4 TYPE lvc_t_f4,
        ls_f4 TYPE lvc_s_f4.

  ls_f4-fieldname = 'CATEGORY_1'.
  ls_f4-register  = abap_true.
  INSERT ls_f4 INTO TABLE lt_f4.

  ls_f4-fieldname = 'CATEGORY_2'.
  ls_f4-register  = abap_true.
  INSERT ls_f4 INTO TABLE lt_f4.

  ls_f4-fieldname = 'CATEGORY_3'.
  ls_f4-register  = abap_true.
  INSERT ls_f4 INTO TABLE lt_f4.

  ls_f4-fieldname = 'CATEGORY_4'.
  ls_f4-register  = abap_true.
  INSERT ls_f4 INTO TABLE lt_f4.

  ls_f4-fieldname = 'CATEGORY_5'.
  ls_f4-register  = abap_true.
  INSERT ls_f4 INTO TABLE lt_f4.

  ls_f4-fieldname = 'CATEGORY_6'.
  ls_f4-register  = abap_true.
  INSERT ls_f4 INTO TABLE lt_f4.

  CALL METHOD p_go_alv->register_f4_for_fields
    EXPORTING
      it_f4 = lt_f4[].

*-- Set handler for event F4
  SET HANDLER go_event_receiver_edit->handle_on_f4 FOR p_go_alv.

ENDFORM.  " REGISTER_F4_EVENT_ONLCAT_ALV
*&---------------------------------------------------------------------*
*&      Form  GET_NAT_MC_DESC
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM get_nat_mc_desc .

  IF zarn_nat_mc_hier-nat_mc_code IS INITIAL.
    CLEAR gs_text-nat_mc_desc.
    RETURN.
  ENDIF.

  SELECT SINGLE wgbez INTO gs_text-nat_mc_desc FROM t023t
         WHERE spras = sy-langu
           AND matkl = zarn_nat_mc_hier-nat_mc_code.
  IF sy-subrc IS NOT INITIAL.
    CLEAR gs_text-nat_mc_desc.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  DEFAULT_NON_SAP_FIELDS
*&---------------------------------------------------------------------*
* Default NON SAP fields
*----------------------------------------------------------------------*
FORM default_non_sap_fields .

* Default Legacy fields
  PERFORM default_legacy_uoms_113.

* Default Host Product Indicator
  PERFORM default_host_prod_ind_113.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  DEFAULT_HOST_PROD_IND_113
*&---------------------------------------------------------------------*
* Default Host Product Indicator
*----------------------------------------------------------------------*
FORM default_host_prod_ind_113 .

  DATA: ls_reg_banner   LIKE LINE OF gt_zarn_reg_banner,
        lv_host_product TYPE zarn_host_product.

* If any Host Product exist then set host indicator as true
  IF gt_zmd_host_sap_itab[] IS NOT INITIAL.
    zarn_reg_hdr-host_product = abap_true.
    EXIT.
  ENDIF.


  CLEAR lv_host_product.

  "process host data only IF
  "a) 1000 with Category manager exists OR
  "b) 4000/5000/6000 with Category manager and Assort. Grade exist
  CLEAR ls_reg_banner.
  LOOP AT gt_zarn_reg_banner[] INTO ls_reg_banner
    WHERE idno = zarn_reg_hdr-idno.

    CASE ls_reg_banner-banner.
      WHEN zcl_constants=>gc_banner_1000.
        IF ls_reg_banner-zzcatman IS NOT INITIAL AND zarn_reg_hdr-zz_uni_dc EQ abap_true.
          "if exists 1000 with Catman and UNI DC flag = X, proceed
          lv_host_product = abap_true.
          EXIT.
        ENDIF.
      WHEN  zcl_constants=>gc_banner_4000
         OR zcl_constants=>gc_banner_5000
         OR zcl_constants=>gc_banner_6000.
        "if exists Retail Banner with Catman and Assortment (other than 'OR'), proceed
        IF ls_reg_banner-zzcatman IS NOT INITIAL     AND
           ls_reg_banner-sstuf IS NOT INITIAL AND
           ls_reg_banner-sstuf NE 'OR'.
          lv_host_product = abap_true.
          EXIT.
        ENDIF.
    ENDCASE.

  ENDLOOP.  " LOOP AT gt_zarn_reg_banner[] INTO ls_reg_banner



  zarn_reg_hdr-host_product = lv_host_product.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  DEFAULT_DATA_201
*&---------------------------------------------------------------------*
FORM default_data_201.

  PERFORM get_control_data USING zarn_control-created_by
                        CHANGING gs_control-nat_create_name.

  PERFORM get_control_data USING zarn_prd_version-changed_by
                        CHANGING gs_control-nat_change_name.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  DEFAULT_DATA_203
*&---------------------------------------------------------------------*
FORM default_data_203.

  PERFORM get_control_data USING zarn_reg_hdr-ernam
                        CHANGING gs_control-reg_create_name.

  PERFORM get_control_data USING zarn_reg_hdr-aenam
                        CHANGING gs_control-reg_change_name.

* INS Begin of Change 3169 JKH 01.11.2016
  " fill the version on the GUI...
  PERFORM fill_version_desc.
* INS End of Change 3169 JKH 01.11.2016

ENDFORM.

*&---------------------------------------------------------------------*
*&      Form  FILTER_REG_UOMS_111
*&---------------------------------------------------------------------*
* Filter REG_UOMs based on SHOW/HIDE
*----------------------------------------------------------------------*
FORM filter_reg_uoms_111  USING    fu_t_zarn_reg_uom     TYPE ztarn_reg_uom_ui
                                   fu_v_reg_uom_togl     TYPE char10
                          CHANGING fc_t_zarn_reg_uom_tmp TYPE ztarn_reg_uom_ui.

  fc_t_zarn_reg_uom_tmp[] = fu_t_zarn_reg_uom[].



  IF fu_v_reg_uom_togl = gc_fcode-hide_uom.  " 'HIDE_UOM'.
    DELETE fc_t_zarn_reg_uom_tmp[] WHERE meinh EQ space.
  ENDIF.



ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  REG_HSNO_ALV
*&---------------------------------------------------------------------*
* Regional HSNO ALV
*----------------------------------------------------------------------*
FORM reg_hsno_alv .

  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo,
    ls_variant  TYPE disvariant.


  IF go_reg_con_hsno_alv IS INITIAL.
*   Regional RRP
    CREATE OBJECT go_rconcc05
      EXPORTING
        container_name = gc_rconcc05.

* Create ALV grid
    CREATE OBJECT go_reg_con_hsno_alv
      EXPORTING
        i_parent = go_rconcc05.


    PERFORM alv_build_fieldcat   USING  gc_alv_reg_hsno CHANGING lt_fieldcat.

**  Exclude all edit functions in this example since we do not need them:
    PERFORM exclude_tb_functions_display CHANGING lt_exclude.

    CLEAR ls_layout.
    ls_layout-sel_mode   = abap_true.
    ls_layout-zebra      = abap_true.
    ls_layout-no_toolbar = abap_true.

*   Variant management
    ls_variant-handle   = 'RHSN'.
    PERFORM alv_variant_default CHANGING ls_variant.

    CALL METHOD go_reg_con_hsno_alv->set_table_for_first_display
      EXPORTING
        i_bypassing_buffer            = abap_true
        is_variant                    = ls_variant
        i_save                        = gc_save
        is_layout                     = ls_layout
        it_toolbar_excluding          = lt_exclude
      CHANGING
        it_outtab                     = gt_zarn_reg_hsno
        it_fieldcatalog               = lt_fieldcat
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.

    IF sy-subrc IS NOT INITIAL.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.




  ELSE.
    PERFORM refresh_reg_alv USING go_reg_con_hsno_alv lt_fieldcat.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  MASS_REGIONAL_UPDATE
*&---------------------------------------------------------------------*
* Mass Regional Update
*----------------------------------------------------------------------*
FORM mass_regional_update USING pt_rows TYPE lvc_t_row.


  DATA: ls_rows       TYPE lvc_s_row,
        ls_worklist   TYPE ty_outtab,
        lt_idno_range TYPE ztarn_idno_range,
        ls_idno_range TYPE zsarn_idno,
        lv_arena_mass TYPE flag.




  LOOP AT pt_rows INTO ls_rows.

    CLEAR ls_worklist.
    READ TABLE gt_worklist INTO ls_worklist INDEX ls_rows-index.
    IF sy-subrc = 0.
      CLEAR ls_idno_range.
      ls_idno_range-sign   = 'I'.
      ls_idno_range-option = 'EQ'.
      ls_idno_range-low    = ls_worklist-idno.
      APPEND ls_idno_range TO lt_idno_range[].
    ENDIF.
  ENDLOOP.

** Set MASS Flag and IDNOs to default
*  CALL FUNCTION 'ZARN_SET_GET_MASS_FLAG'
*    EXPORTING
*      iv_mode            = 'W'
*      iv_arena_mass      = abap_true
*      it_idno_mass_range = lt_idno_range[].

  lv_arena_mass = abap_true.
  EXPORT lv_arena_mass   TO MEMORY ID 'ZARN_MASS_FLAG'.
  EXPORT lt_idno_range[] TO MEMORY ID 'ZARN_IDNO_RANGE'.



  CALL TRANSACTION 'ZARN_MASS'.

  CLEAR: lv_arena_mass, lt_idno_range[].
  EXPORT lv_arena_mass   TO MEMORY ID 'ZARN_MASS_FLAG'.
  EXPORT lt_idno_range[] TO MEMORY ID 'ZARN_IDNO_RANGE'.



ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  REG_ART_LINK_ALV
*&---------------------------------------------------------------------*
*----------------------------------------------------------------------*
FORM reg_art_link_alv .

  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
    ls_fieldcat TYPE lvc_s_fcat,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo,
    ls_variant  TYPE disvariant,
    lt_filter   TYPE lvc_t_filt,
    ls_filter   TYPE lvc_s_filt.



  IF go_reg_con_artlink_alv IS INITIAL.
*   Regional RRP
    CREATE OBJECT go_rconcc06
      EXPORTING
        container_name = gc_rconcc06.

* Create ALV grid
    CREATE OBJECT go_reg_con_artlink_alv
      EXPORTING
        i_parent = go_rconcc06.

    PERFORM alv_build_fieldcat_reg_artlink USING gc_alv_reg_artlink_ui
                                        CHANGING lt_fieldcat.


*  Exclude all edit functions in this example since we do not need them:
    PERFORM exclude_tb_functions_edit USING  gc_alv_reg_artlink_ui CHANGING lt_exclude.

    PERFORM build_drdn_data_reg_artlink USING go_reg_con_artlink_alv.


*   Variant management
    ls_variant-handle   = 'RLNK'.

    PERFORM alv_variant_default CHANGING ls_variant.

    CLEAR ls_layout.
    ls_layout-cwidth_opt = abap_false.
    ls_layout-sel_mode   = abap_true.
    ls_layout-zebra      = abap_true.




    CALL METHOD go_reg_con_artlink_alv->set_table_for_first_display
      EXPORTING
        i_bypassing_buffer            = abap_true
        is_variant                    = ls_variant
        i_save                        = gc_save
        is_layout                     = ls_layout
        it_toolbar_excluding          = lt_exclude
      CHANGING
        it_outtab                     = gt_zarn_reg_artlink
        it_fieldcatalog               = lt_fieldcat
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.

    IF sy-subrc IS NOT INITIAL.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.


* set editable cells to ready for input
    CALL METHOD go_reg_con_artlink_alv->set_ready_for_input
      EXPORTING
        i_ready_for_input = 1.

    CALL METHOD go_reg_con_artlink_alv->register_edit_event
      EXPORTING
        i_event_id = cl_gui_alv_grid=>mc_evt_enter.

    CALL METHOD go_reg_con_artlink_alv->register_edit_event
      EXPORTING
        i_event_id = cl_gui_alv_grid=>mc_evt_modified.


    SET HANDLER go_event_receiver_edit->handle_data_changed FOR go_reg_con_artlink_alv.

  ELSE.
    PERFORM refresh_reg_alv USING go_reg_con_artlink_alv lt_fieldcat.
  ENDIF.



ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  alv_build_fieldcat_reg_artlink
*&---------------------------------------------------------------------*
FORM alv_build_fieldcat_reg_artlink USING pv_structure
                                 CHANGING pt_fieldcat TYPE lvc_t_fcat.

  FIELD-SYMBOLS <ls_fcat> TYPE lvc_s_fcat.


  CALL FUNCTION 'LVC_FIELDCATALOG_MERGE'
    EXPORTING
      i_structure_name = pv_structure "'ZRSD_ICARE_STRUC'
    CHANGING
      ct_fieldcat      = pt_fieldcat.

*** for all tables
  LOOP AT pt_fieldcat ASSIGNING <ls_fcat> WHERE fieldname = gc_idno
                                      OR fieldname = gc_version
                                      OR fieldname = gc_fsni_icare_value
                                      OR fieldname = gc_alv_rowcolor
                                      OR fieldname = gc_guid
                                      OR fieldname = gc_celltab.
    <ls_fcat>-no_out = abap_true.
    <ls_fcat>-edit = abap_false.
  ENDLOOP.

  LOOP AT pt_fieldcat ASSIGNING   <ls_fcat>.
    CASE <ls_fcat>-fieldname.
*      WHEN gc_hdr_scn.
**        <ls_fcat>-col_pos = '1'.
**        <ls_fcat>-edit    = abap_true.
***        <ls_fcat>-drdn_field = gc_drdn_hdr_scn.
**        <ls_fcat>-drdn_hndl = 1.
*        <ls_fcat>-tech = abap_true.

      WHEN gc_hdr_scn_drdn.
        <ls_fcat>-col_pos = '1'.
        <ls_fcat>-edit    = abap_true.
*        <ls_fcat>-drdn_field = gc_drdn_hdr_scn.
        <ls_fcat>-drdn_hndl = 1.
        <ls_fcat>-outputlen = 20.

      WHEN gc_hdr_matnr.
        <ls_fcat>-col_pos = '2'.
        <ls_fcat>-edit    = abap_true.


      WHEN gc_hdr_maktx.
        <ls_fcat>-col_pos = '3'.
        <ls_fcat>-edit    = abap_false.


      WHEN gc_hdr_attyp.
        <ls_fcat>-col_pos = '4'.
        <ls_fcat>-edit    = abap_false.


      WHEN gc_hdr_mtart.
        <ls_fcat>-col_pos = '5'.
        <ls_fcat>-edit    = abap_false.


*      WHEN gc_link_scn.
**        <ls_fcat>-col_pos = '6'.
**        <ls_fcat>-edit    = abap_false.
***        <ls_fcat>-drdn_field = gc_drdn_link_scn.
**        <ls_fcat>-drdn_hndl = 2.
*        <ls_fcat>-tech = abap_true.

      WHEN gc_link_scn_drdn.
        <ls_fcat>-col_pos = '6'.
        <ls_fcat>-edit    = abap_false.
*        <ls_fcat>-drdn_field = gc_drdn_link_scn.
        <ls_fcat>-drdn_hndl = 2.
        <ls_fcat>-outputlen = 20.

      WHEN gc_link_matnr.
        <ls_fcat>-col_pos = '7'.
        <ls_fcat>-edit    = abap_true.


      WHEN gc_link_maktx.
        <ls_fcat>-col_pos = '8'.
        <ls_fcat>-edit    = abap_false.


      WHEN gc_link_attyp.
        <ls_fcat>-col_pos = '9'.
        <ls_fcat>-edit    = abap_false.


      WHEN gc_link_mtart.
        <ls_fcat>-col_pos = '10'.
        <ls_fcat>-edit    = abap_false.

      WHEN OTHERS.
        DELETE pt_fieldcat.

    ENDCASE.
  ENDLOOP.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  BUILD_DRDN_DATA_REG_ARTLINK
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM build_drdn_data_reg_artlink USING po_alv TYPE REF TO cl_gui_alv_grid.


  DATA: ls_dropdown   TYPE lvc_s_drop,
        lt_hdr_scn_t  TYPE ztarn_hdr_scn_t,
        ls_hdr_scn_t  TYPE zarn_hdr_scn_t,
        lt_link_scn_t TYPE ztarn_link_scn_t,
        ls_link_scn_t TYPE zarn_link_scn_t,
        lv_value      TYPE char128.

  CLEAR gt_drdn_reg_artlink.

  lt_hdr_scn_t  = zcl_arn_reg_artlink=>get_hdr_scn_t( ).
  lt_link_scn_t = zcl_arn_reg_artlink=>get_link_scn_t( ).


  LOOP AT lt_hdr_scn_t INTO ls_hdr_scn_t.
    CLEAR ls_dropdown.
    ls_dropdown-handle    = 1.

    CLEAR lv_value.
    lv_value+0(2)  = ls_hdr_scn_t-hdr_scn.
    lv_value+2(1)  = space.
    lv_value+3(20) = ls_hdr_scn_t-hdr_scn_text.
    ls_dropdown-value = lv_value.
    APPEND ls_dropdown TO gt_drdn_reg_artlink.
  ENDLOOP.


  LOOP AT lt_link_scn_t INTO ls_link_scn_t.
    CLEAR ls_dropdown.
    ls_dropdown-handle    = 2.

    CLEAR lv_value.
    lv_value+0(2)  = ls_link_scn_t-link_scn.
    lv_value+2(1)  = space.
    lv_value+3(20) = ls_link_scn_t-link_scn_text.
    ls_dropdown-value = lv_value.
    APPEND ls_dropdown TO gt_drdn_reg_artlink.
  ENDLOOP.

  CALL METHOD po_alv->set_drop_down_table
    EXPORTING
      it_drop_down = gt_drdn_reg_artlink.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  DEFAULT_BRAND_ID_111
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
FORM default_brand_id_111 .

  IF zarn_reg_hdr-idno     IS NOT INITIAL AND
     zarn_reg_hdr-brand_id IS INITIAL.
    zarn_reg_hdr-brand_id = zcl_constants=>gc_brand_id_default.    " 'OTHM'.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  MASS_APPROVALS
*&---------------------------------------------------------------------*
* Mass Approvals
*----------------------------------------------------------------------*
FORM mass_approvals USING pt_rows TYPE lvc_t_row.


  DATA: ls_rows       TYPE lvc_s_row,
        ls_worklist   TYPE ty_outtab,
        lt_idno_range TYPE ztarn_idno_range,
        ls_idno_range TYPE zsarn_idno,
        lv_arena_mass TYPE flag.




  LOOP AT pt_rows INTO ls_rows.

    CLEAR ls_worklist.
    READ TABLE gt_worklist INTO ls_worklist INDEX ls_rows-index.
    IF sy-subrc = 0.
      CLEAR ls_idno_range.
      ls_idno_range-sign   = 'I'.
      ls_idno_range-option = 'EQ'.
      ls_idno_range-low    = ls_worklist-idno.
      APPEND ls_idno_range TO lt_idno_range[].
    ENDIF.
  ENDLOOP.


  IF lt_idno_range[] IS NOT INITIAL.
    SUBMIT zrarn_approvals_mass VIA SELECTION-SCREEN
      WITH s_idno IN lt_idno_range[]
      AND RETURN.
  ENDIF.


** Refresh worklist after mass approval
* Refresh ALV worklist
  PERFORM refresh_worklist.

  PERFORM free_screen_objects.

*>>>IS full refresh - select all the data as well
  DATA lv_full_refresh TYPE xfeld VALUE abap_true.

  IF lv_full_refresh = abap_true.
    "do full refresh - repeat the initial selection
    PERFORM back_to_worklist_full_refresh.
  ENDIF.

  CALL METHOD go_worklist_alv->refresh_table_display.

  cl_gui_cfw=>set_new_ok_code( new_code = 'SYSTEM').


ENDFORM.  " MASS_APPROVALS
*&---------------------------------------------------------------------*
*&      Form  INITIALIZE_DATA
*&---------------------------------------------------------------------*
* Initialize Data
*----------------------------------------------------------------------*
FORM initialize_data .

  CLEAR gv_cr_r1_switch.
  SELECT SINGLE low FROM tvarvc INTO gv_cr_r1_switch
    WHERE name = gc_cr_r1_switch
      AND type = 'P'.

  CLEAR gv_cr_r3_switch.
  SELECT SINGLE low FROM tvarvc INTO gv_cr_r3_switch
    WHERE name = gc_cr_r3_switch
      AND type = 'P'.


  CLEAR gv_rereq_auth.
  AUTHORITY-CHECK OBJECT 'ZMD_CPQCHG'
                      ID 'ACTVT' FIELD '37'.
  IF sy-subrc = 0.
    gv_rereq_auth = abap_true.
  ENDIF.


*  IF sy-uname  = 'C90001363'.
*    gv_cr_r1_switch = abap_true.
*  ENDIF.

  TRY.
      gv_feature_a4_enabled = zcl_ftf_store_factory=>get_instance(
                                       )->get_feature( iv_store     = 'RFST'    "Dummy Value
                                                       iv_appl_area = zif_ftf_appl_area_c=>gc_arena
                                                       iv_feature   = gc_feature_a4
                                       )->is_active( ).
    CATCH zcx_object_not_found.
      gv_feature_a4_enabled = abap_false.
  ENDTRY.


ENDFORM.  " INITIALIZE_DATA
*&---------------------------------------------------------------------*
*&      Form  RESTRICT_SELECT_OPTIONS_ICARE
*&---------------------------------------------------------------------*
FORM restrict_select_options_icare .


  DATA: ls_restrict TYPE sscr_restrict,
        ls_opt_list TYPE sscr_opt_list,
        ls_ass_tab  TYPE sscr_ass.

  ls_opt_list-name    = 'OBJ1'.
  ls_opt_list-options-eq = 'X'.
  ls_opt_list-options-cp = 'X'.
  APPEND ls_opt_list TO ls_restrict-opt_list_tab[].

  ls_ass_tab-kind    = 'S'.
  ls_ass_tab-sg_main = 'I'.
  ls_ass_tab-sg_addy = ''.
  ls_ass_tab-op_main = 'OBJ1'.
  ls_ass_tab-op_addy = ''.

  ls_ass_tab-name    = 'S_GTIN5'.  APPEND ls_ass_tab TO ls_restrict-ass_tab.
  ls_ass_tab-name    = 'S_FAN5'.   APPEND ls_ass_tab TO ls_restrict-ass_tab.
  ls_ass_tab-name    = 'S_SHDESC'. APPEND ls_ass_tab TO ls_restrict-ass_tab.
  ls_ass_tab-name    = 'S_DESCR5'. APPEND ls_ass_tab TO ls_restrict-ass_tab.
  ls_ass_tab-name    = 'S_FBRAN5'. APPEND ls_ass_tab TO ls_restrict-ass_tab.
  ls_ass_tab-name    = 'S_BRAND5'. APPEND ls_ass_tab TO ls_restrict-ass_tab.
  ls_ass_tab-name    = 'S_GLN5'.   APPEND ls_ass_tab TO ls_restrict-ass_tab.
  ls_ass_tab-name    = 'S_VARTN5'. APPEND ls_ass_tab TO ls_restrict-ass_tab.
  ls_ass_tab-name    = 'S_GLND5'.  APPEND ls_ass_tab TO ls_restrict-ass_tab.
  ls_ass_tab-name    = 'S_MGLN5'.  APPEND ls_ass_tab TO ls_restrict-ass_tab.
  ls_ass_tab-name    = 'S_MGLND5'. APPEND ls_ass_tab TO ls_restrict-ass_tab.





  ls_opt_list-name    = 'OBJ2'.
  ls_opt_list-options-eq = 'X'.
  APPEND ls_opt_list TO ls_restrict-opt_list_tab[].

  ls_ass_tab-kind    = 'S'.
  ls_ass_tab-sg_main = 'I'.
  ls_ass_tab-sg_addy = ''.
  ls_ass_tab-op_main = 'OBJ2'.
  ls_ass_tab-op_addy = ''.

  ls_ass_tab-name    = 'S_SBDTST'. APPEND ls_ass_tab TO ls_restrict-ass_tab.
  ls_ass_tab-name    = 'S_SBDTEN'. APPEND ls_ass_tab TO ls_restrict-ass_tab.
  ls_ass_tab-name    = 'S_ICFLAG'. APPEND ls_ass_tab TO ls_restrict-ass_tab.
  ls_ass_tab-name    = 'S_INSTOR'. APPEND ls_ass_tab TO ls_restrict-ass_tab.
  ls_ass_tab-name    = 'S_GILMRS'. APPEND ls_ass_tab TO ls_restrict-ass_tab.


  CALL FUNCTION 'SELECT_OPTIONS_RESTRICT'
    EXPORTING
*     PROGRAM                =
      restriction            = ls_restrict
*     DB                     = ' '
    EXCEPTIONS
      too_late               = 1
      repeated               = 2
      selopt_without_options = 3
      selopt_without_signs   = 4
      invalid_sign           = 5
      empty_option_list      = 6
      invalid_kind           = 7
      repeated_kind_a        = 8
      OTHERS                 = 9.
  IF sy-subrc <> 0.
* Implement suitable error handling here
  ENDIF.




ENDFORM.  " RESTRICT_SELECT_OPTIONS_ICARE
*&---------------------------------------------------------------------*
*&      Form  SORT_ICARE_SEARCH_WORKLIST
*&---------------------------------------------------------------------*
* Sort I-Care Search Results as per entered criteria
*----------------------------------------------------------------------*
FORM sort_icare_search_worklist .

  DATA: ls_search_sort TYPE ty_s_search_sort,
        otab           TYPE abap_sortorder_tab,
        oline          TYPE abap_sortorder.



  LOOP AT gt_search_sort[] INTO ls_search_sort
    WHERE sap_attribute IS NOT INITIAL.

    CLEAR oline.
    oline-name = ls_search_sort-sap_attribute.

    IF ls_search_sort-sort_direction = gc_sort_asc.
      oline-descending = space.
    ELSEIF ls_search_sort-sort_direction = gc_sort_desc.
      oline-descending = abap_true.
    ENDIF.

    APPEND oline TO otab[].
  ENDLOOP.

  SORT gt_worklist BY (otab).

ENDFORM.  "SORT_ICARE_SEARCH_WORKLIST
*&---------------------------------------------------------------------*
*&      Form  GET_DATA_ICARE_R1
*&---------------------------------------------------------------------*
* Send request to Hybris and Read National and Regional data
*----------------------------------------------------------------------*
FORM get_data_icare_r1 .

  TYPES:
    BEGIN OF ty_sel,
      fan_id                 TYPE zarn_products-fan_id,
      idno                   TYPE zarn_products-idno,
      version                TYPE zarn_products-version,
      guid                   TYPE zarn_prd_version-guid,
      matnr_ni               TYPE zarn_products-matnr_ni,
      name                   TYPE zarn_products-name,
      fs_short_descr         TYPE zarn_products-fs_short_descr,
      fs_brand_id            TYPE zarn_products-fs_brand_id,
      brand_id               TYPE zarn_products-brand_id,
      description            TYPE zarn_products-description,
      gpc_code               TYPE zarn_products-gpc_code,
      gpc_description        TYPE zarn_products-gpc_description,
      gln                    TYPE zarn_products-gln,
      glnd_descr             TYPE zarn_products-gln_descr,
      vendor_article_number  TYPE zarn_pir-vendor_article_number,
      manufacturer_gln       TYPE zarn_pir-manufacturer_gln,
      manufacturer_gln_descr TYPE zarn_pir-manufacturer_gln_descr_upper,
      code                   TYPE zarn_products-code,
      gtin_code              TYPE zarn_gtin_var-gtin_code,
      fsni_icare_value       TYPE zarn_products-fsni_icare_value,
      fsni_instore_value     TYPE zarn_products-fsni_instore_value,
      fsni_gilmours_value    TYPE zarn_products-fsni_gilmours_value,
    END OF ty_sel.


  DATA:
    lt_key       TYPE ztarn_key,
    ls_key       LIKE LINE OF lt_key,
    lt_key_sel   TYPE ztarn_key,
    lv_flag      TYPE flag,
    lv_team_code TYPE zarn_team_code VALUE 'CT'.

  DATA:
    ls_search_input   TYPE zproduct_lean_search_request_f,
*    lt_search_results TYPE STANDARD TABLE OF zhybris_results,
*    lt_hybris_results TYPE SORTED TABLE OF zhybris_results WITH NON-UNIQUE KEY fan_id,
    ls_hybris_results TYPE zproduct_lean_search_respons24,
    ls_basic_product  TYPE zproduct_lean_search_respons22,
    ls_gtin           TYPE string,
    ls_vlog           TYPE ty_s_vlog,
    ls_gtin_msg       TYPE char256,


    ls_short_descr    LIKE LINE OF  gr_short_descr,
    ls_gln_descr      LIKE LINE OF  gr_short_descr,
    lt_sel            TYPE STANDARD TABLE OF ty_sel,
    ls_data           LIKE LINE OF gt_data,
    lt_messages       TYPE bal_t_msg,
    ls_message        LIKE LINE OF lt_messages,
    lv_results        TYPE i,
    ls_lfa1           TYPE lfa1.

  FIELD-SYMBOLS:
    <ls_data> LIKE LINE OF gt_data,
    <ls_sel>  LIKE LINE OF lt_sel.
*    <ls_hybris>         LIKE LINE OF lt_hybris_results,
*    <ls_search_results> LIKE LINE OF lt_search_results.

  CLEAR: gt_basic_product[], gt_search_sort[], gt_vlog[],
         gv_start_index, gv_tot_search_cnt, gv_smore_disable, gv_worklist_cnt, gv_search_cnt,
         gv_error_cnt, gv_valerr_cnt, gv_vlog_enable.


  gv_start_index = p_stindx.


  CLEAR ls_search_input.

* Build R1 Search Mapping data
  PERFORM build_r1_search_map_data USING gv_start_index
                                CHANGING ls_search_input.


  CALL FUNCTION 'SAPGUI_PROGRESS_INDICATOR'
    EXPORTING
      percentage = 25
      text       = 'Searching data from National'.

  CLEAR: ls_hybris_results, lt_messages[].

* Get any records from Hybris matching search criteria
  CALL FUNCTION 'Z_HYBRIS_GET_LEAN'
    EXPORTING
      hybris_search_input  = ls_search_input
    IMPORTING
      hybris_results       = ls_hybris_results
      messages             = lt_messages[]
    EXCEPTIONS
      hybris_not_available = 1
      OTHERS               = 2.

  LOOP AT lt_messages INTO ls_message WHERE msgty EQ 'E' OR msgty EQ 'A'.
    EXIT.
  ENDLOOP.
  IF sy-subrc EQ 0.
    PERFORM popup_appl_log USING lt_messages.
    gv_smore_disable = abap_true.
*    RETURN.
  ELSE.

    APPEND LINES OF ls_hybris_results-product_lean_search_response-fsvendor_data_res-basic_product[] TO gt_basic_product[].

* Total no. of searched records
    DESCRIBE TABLE gt_basic_product[] LINES gv_search_cnt.

* Total no. of matching records available in National
    gv_tot_search_cnt = ls_hybris_results-product_lean_search_response-fsvendor_data_res-total_records_found.

    IF gv_search_cnt GE gv_tot_search_cnt.
      gv_smore_disable = abap_true.
    ENDIF.

    gv_error_cnt = ls_hybris_results-product_lean_search_response-fsvendor_data_res-failed_validation-total_failed_records.

** National search is completed, Continuing with local search
*    MESSAGE i089 WITH lv_results.


    LOOP AT ls_hybris_results-product_lean_search_response-fsvendor_data_res-failed_validation-gtin[] INTO ls_gtin.
      CLEAR ls_vlog.
      ls_vlog-message_icon  = icon_led_red.
      ls_vlog-validation_id = 'NAT'.

      CLEAR ls_gtin_msg.
      SPLIT ls_gtin AT '|' INTO ls_vlog-code ls_gtin_msg.

      IF ls_gtin_msg IS NOT INITIAL.
        ls_vlog-message_txt1 = ls_gtin_msg+0(128).

        IF ls_gtin_msg+128(1) IS NOT INITIAL.
          ls_vlog-message_txt2 = ls_gtin_msg+128(128).
        ENDIF.
      ENDIF.

      APPEND ls_vlog TO gt_vlog[].
    ENDLOOP.




    CALL FUNCTION 'SAPGUI_PROGRESS_INDICATOR'
      EXPORTING
        percentage = 50
        text       = 'National search is completed, Continuing with local search'.

  ENDIF.




* at the moment we have 3 description fields in ZARN_PRODUCTS
* of which FS_SHORT_DESCR_UPPER, the following logic an SELECT caters for
* case sensitive search !
*   upper case search

  CLEAR:
    gr_short_descr,
    gr_gln_descr.

  IF s_descr5[]  IS NOT INITIAL OR
     s_descr5    IS NOT INITIAL.
    LOOP AT s_descr5.
      MOVE-CORRESPONDING s_descr5 TO ls_short_descr.
      TRANSLATE  ls_short_descr TO UPPER CASE.
      APPEND ls_short_descr TO gr_short_descr.
    ENDLOOP.
  ENDIF.


  IF s_glnd5[] IS NOT INITIAL OR
     s_glnd5 IS NOT INITIAL.
*   upper case search
    LOOP AT  s_glnd5.
      MOVE-CORRESPONDING  s_glnd5 TO ls_gln_descr.
      TRANSLATE  ls_gln_descr TO UPPER CASE.
      APPEND ls_gln_descr TO gr_gln_descr.
    ENDLOOP.
  ENDIF.

* The above function module will return data and update the AReNa tables
* it is prudent to read AReNa DB for our latest set of data
* the user requires a list of details from AReNa that are ICare'd already !

  IF p_flagop = 'AND'.
* Get any records from AReNa matching search criteria
    SELECT p~fan_id p~idno p~version r~guid
           p~matnr_ni p~description p~gpc_code p~gpc_description
           p~gln  p~gln_descr p~code g~gtin_code
           p~name p~fs_short_descr p~fs_brand_id p~brand_id
           v~vendor_article_number v~manufacturer_gln v~manufacturer_gln_descr
           p~fsni_icare_value p~fsni_instore_value p~fsni_gilmours_value
      INTO CORRESPONDING FIELDS OF TABLE lt_sel
      FROM zarn_products AS p
      INNER JOIN zarn_products_ex AS e
        ON  e~idno    EQ p~idno
        AND e~version EQ p~version
      INNER JOIN zarn_products_2 AS e2
        ON  e2~idno    EQ p~idno
        AND e2~version EQ p~version
      INNER JOIN zarn_gtin_var AS g
        ON  g~idno    EQ p~idno
        AND g~version EQ p~version
      INNER JOIN zarn_pir AS v
        ON  v~idno    EQ p~idno
        AND v~version EQ p~version
      INNER JOIN zarn_ver_status AS s
        ON  s~idno        EQ p~idno
        AND s~current_ver EQ p~version
      INNER JOIN zarn_prd_version AS r
        ON  r~idno    EQ p~idno
        AND r~version EQ p~version
      WHERE g~gtin_code                    IN s_gtin5
      AND   v~vendor_article_number        IN s_vartn5
      AND   v~gln                          IN s_gln5
      AND   v~gln_description_upper        IN gr_gln_descr
      AND   v~manufacturer_gln             IN s_mgln5
      AND   v~manufacturer_gln_descr_upper IN s_mglnd5
      AND   p~fan_id                       IN s_fan5
      AND   p~fs_short_descr_upper         IN gr_short_descr
      AND   p~brand_id_upper               IN s_brand5
*    AND   p~fsni_icare_value             IN s_icflag
      AND   p~fsni_gilmours_value          IN s_gilmrs
      AND   p~fsni_instore_value           IN s_instor
      AND   e2~description_upper           IN s_shdesc
      AND   e2~fs_brand_id_upper           IN s_fbran5
      AND   r~id_type                      IN ('H', 'M', 'I').

  ELSEIF p_flagop = 'OR'.
* Get any records from AReNa matching search criteria
    SELECT p~fan_id p~idno p~version r~guid
           p~matnr_ni p~description p~gpc_code p~gpc_description
           p~gln  p~gln_descr p~code g~gtin_code
           p~name p~fs_short_descr p~fs_brand_id p~brand_id
           v~vendor_article_number v~manufacturer_gln v~manufacturer_gln_descr
           p~fsni_icare_value p~fsni_instore_value p~fsni_gilmours_value
      INTO CORRESPONDING FIELDS OF TABLE lt_sel
      FROM zarn_products AS p
      INNER JOIN zarn_products_ex AS e
        ON  e~idno    EQ p~idno
        AND e~version EQ p~version
      INNER JOIN zarn_products_2 AS e2
        ON  e2~idno    EQ p~idno
        AND e2~version EQ p~version
      INNER JOIN zarn_gtin_var AS g
        ON  g~idno    EQ p~idno
        AND g~version EQ p~version
      INNER JOIN zarn_pir AS v
        ON  v~idno    EQ p~idno
        AND v~version EQ p~version
      INNER JOIN zarn_ver_status AS s
        ON  s~idno        EQ p~idno
        AND s~current_ver EQ p~version
      INNER JOIN zarn_prd_version AS r
        ON  r~idno    EQ p~idno
        AND r~version EQ p~version
      WHERE g~gtin_code                    IN s_gtin5
      AND   v~vendor_article_number        IN s_vartn5
      AND   v~gln                          IN s_gln5
      AND   v~gln_description_upper        IN gr_gln_descr
      AND   v~manufacturer_gln             IN s_mgln5
      AND   v~manufacturer_gln_descr_upper IN s_mglnd5
      AND   p~fan_id                       IN s_fan5
      AND   p~fs_short_descr_upper         IN gr_short_descr
      AND   p~brand_id_upper               IN s_brand5
*    AND   p~fsni_icare_value             IN s_icflag
      AND ( p~fsni_gilmours_value          IN s_gilmrs OR
            p~fsni_instore_value           IN s_instor )
      AND   e2~description_upper           IN s_shdesc
      AND   e2~fs_brand_id_upper           IN s_fbran5
      AND   r~id_type                      IN ('H', 'M', 'I').

  ENDIF.



  IF lt_sel IS NOT INITIAL.
    LOOP AT lt_sel ASSIGNING <ls_sel>.
      CLEAR ls_data.
      MOVE-CORRESPONDING <ls_sel> TO ls_data.
      APPEND ls_data TO gt_data.
      MOVE-CORRESPONDING <ls_sel> TO ls_key.
      APPEND ls_key TO lt_key.
    ENDLOOP.
  ENDIF.




  IF gt_basic_product[] IS NOT INITIAL.
* Check and Get Searched IDNOs in National DB
* If found then National/Regional data will be retreived
    SELECT idno current_ver AS version
      FROM zarn_ver_status
      APPENDING CORRESPONDING FIELDS OF TABLE lt_key[]
      FOR ALL ENTRIES IN gt_basic_product[]
      WHERE idno = gt_basic_product-code.
  ENDIF.  " IF lt_basic_product[] IS NOT INITIAL


*** get all National and Regional DB table data
  SORT lt_key BY idno version.
  DELETE ADJACENT DUPLICATES FROM lt_key COMPARING idno.

  IF lt_key IS NOT INITIAL.


    PERFORM read_national_data
      USING lt_key
      CHANGING gt_prod_data.

    PERFORM read_regional
      USING lt_key
      CHANGING gt_reg_data.

  ENDIF.  " IF lt_key IS NOT INITIAL


  PERFORM get_other_table_data USING lt_key.

* Check authorisation for I Care
  CALL FUNCTION 'Z_ARN_APPROVALS_TEAM_AUTH'
    EXPORTING
      team_code  = lv_team_code  "'CT'
    IMPORTING
      authorized = lv_flag.

  IF  lv_flag IS INITIAL.
    gv_display = abap_true.
  ENDIF.




* Build and Merge National/Regional Data for Searched Products
  PERFORM merge_searched_products USING gt_basic_product[]
                                        ls_hybris_results
                                        gt_ver_status[]
                                        gt_prd_version[]
                               CHANGING gt_prod_data[]
                                        gt_reg_data[].



  CALL FUNCTION 'SAPGUI_PROGRESS_INDICATOR'
    EXPORTING
      percentage = 75
      text       = 'Preparing the display'.



ENDFORM.  " GET_DATA_ICARE_R1
*&---------------------------------------------------------------------*
*&      Form  BUILD_R1_SEARCH_MAP_DATA
*&---------------------------------------------------------------------*
* Build R1 Search MApping data
*----------------------------------------------------------------------*
FORM build_r1_search_map_data USING pv_start_index   TYPE int4
                           CHANGING pcs_search_input TYPE zproduct_lean_search_request_f.

  DATA: ls_search_input TYPE zproduct_lean_search_request_f,
        ls_search_sort  TYPE ty_s_search_sort,
        ls_entry        TYPE zproduct_lean_search_request_e,

        lt_controller   TYPE prxctrltab,
        ls_controller   TYPE prxctrl.

  CLEAR: ls_search_input, lt_controller[].

* GTIN Code
  LOOP AT s_gtin5[] INTO s_gtin5.
    APPEND s_gtin5-low TO ls_search_input-gtin[].
  ENDLOOP.

* FAN Id
  LOOP AT s_fan5[] INTO s_fan5.
    APPEND s_fan5-low TO ls_search_input-fan[].
  ENDLOOP.

* Short Description
  LOOP AT s_shdesc[] INTO s_shdesc.
    APPEND s_shdesc-low TO ls_search_input-short_description[].
  ENDLOOP.

* FS Short Description
  LOOP AT s_descr5[] INTO s_descr5.
    APPEND s_descr5-low TO ls_search_input-fsshort_description[].
  ENDLOOP.

* FS Brand
  LOOP AT s_fbran5[] INTO s_fbran5.
    APPEND s_fbran5-low TO ls_search_input-fsbrand[].
  ENDLOOP.

* National Brand
  LOOP AT s_brand5[] INTO s_brand5.
    APPEND s_brand5-low TO ls_search_input-brand[].
  ENDLOOP.

* Vendor GLN
  LOOP AT s_gln5[] INTO s_gln5.
    APPEND s_gln5-low TO ls_search_input-gln_code[].
  ENDLOOP.

* Vendor Reference No.
  LOOP AT s_vartn5[] INTO s_vartn5.
    APPEND s_vartn5-low TO ls_search_input-vendor_article_number[].
  ENDLOOP.

* Vendor GLN Description
  LOOP AT s_glnd5[] INTO s_glnd5.
    APPEND s_glnd5-low TO ls_search_input-gln_description[].
  ENDLOOP.

* Manufacturer GLN Code
  LOOP AT s_mgln5[] INTO s_mgln5.
    APPEND s_mgln5-low TO ls_search_input-manufacturer_gln_code[].
  ENDLOOP.

* Manufacturer GLN Description
  LOOP AT s_mglnd5[] INTO s_mglnd5.
    APPEND s_mglnd5-low TO ls_search_input-manufacturer_gln_description[].
  ENDLOOP.




* Supplier Submitted Start Date
  ls_search_input-creation_date_range-start_date = s_sbdtst-low.

* Supplier Submitted End Date
  ls_search_input-creation_date_range-end_date   = s_sbdten-low.


* I-Care Flag
  ls_search_input-fsnitake_up  = s_icflag-low.
  IF s_icflag-option = 'EQ' AND s_icflag-low IS INITIAL.
    CLEAR ls_controller.
    ls_controller-field = 'FSNITAKE_UP'.
    ls_controller-value = '1'.
    APPEND ls_controller TO lt_controller[].
  ENDIF.

* In-Store
  ls_search_input-fsniin_store = s_instor-low.
  IF s_instor-option = 'EQ' AND s_instor-low IS INITIAL.
    CLEAR ls_controller.
    ls_controller-field = 'FSNIIN_STORE'.
    ls_controller-value = '1'.
    APPEND ls_controller TO lt_controller[].
  ENDIF.

* Gilmours Flag
  ls_search_input-fsnigilmours = s_gilmrs-low.
  IF s_gilmrs-option = 'EQ' AND s_gilmrs-low IS INITIAL.
    CLEAR ls_controller.
    ls_controller-field = 'FSNIGILMOURS'.
    ls_controller-value = '1'.
    APPEND ls_controller TO lt_controller[].
  ENDIF.




* Flag Operator (AND/OR)
  ls_search_input-flag_operator = p_flagop.

* Display Iterms per page
  ls_search_input-page_size     = p_pagesz.

* Start Index
  ls_search_input-start_index   = pv_start_index.


  ls_search_input-controller   = lt_controller[].


  CLEAR: gt_search_sort[].


  CLEAR: ls_search_sort.
  IF p_seq4 IS NOT INITIAL.
    ls_search_sort-sequence = p_seq4.
    ls_search_sort-sort_by_attribute = 'FAN'.
    ls_search_sort-sap_attribute     = 'FAN_ID'.
    IF p_rs4a = abap_true.
      ls_search_sort-sort_direction = gc_sort_asc.
    ELSEIF p_rs4d = abap_true.
      ls_search_sort-sort_direction = gc_sort_desc.
    ENDIF.

    INSERT ls_search_sort INTO TABLE gt_search_sort[].
  ENDIF.

  CLEAR: ls_search_sort.
  IF p_seq6 IS NOT INITIAL.
    ls_search_sort-sequence = p_seq6.
    ls_search_sort-sort_by_attribute = 'FS_SHORT_DESCRIPTION'.
    ls_search_sort-sap_attribute     = 'FS_SHORT_DESCR_UPPER'.
    IF p_rs6a = abap_true.
      ls_search_sort-sort_direction = gc_sort_asc.
    ELSEIF p_rs6d = abap_true.
      ls_search_sort-sort_direction = gc_sort_desc.
    ENDIF.

    INSERT ls_search_sort INTO TABLE gt_search_sort[].
  ENDIF.

  CLEAR: ls_search_sort.
  IF p_seq7 IS NOT INITIAL.
    ls_search_sort-sequence = p_seq7.
    ls_search_sort-sort_by_attribute = 'FS_BRAND'.
    ls_search_sort-sap_attribute     = 'FS_BRAND_DESC_UPPER'.
    IF p_rs7a = abap_true.
      ls_search_sort-sort_direction = gc_sort_asc.
    ELSEIF p_rs7d = abap_true.
      ls_search_sort-sort_direction = gc_sort_desc.
    ENDIF.

    INSERT ls_search_sort INTO TABLE gt_search_sort[].
  ENDIF.

  CLEAR: ls_search_sort.
  IF p_seq8 IS NOT INITIAL.
    ls_search_sort-sequence = p_seq8.
    ls_search_sort-sort_by_attribute = 'CREATION_DATE'.
    ls_search_sort-sap_attribute     = 'CREATION_TIME'.
    IF p_rs8a = abap_true.
      ls_search_sort-sort_direction = gc_sort_asc.
    ELSEIF p_rs8d = abap_true.
      ls_search_sort-sort_direction = gc_sort_desc.
    ENDIF.

    INSERT ls_search_sort INTO TABLE gt_search_sort[].
  ENDIF.

  CLEAR: ls_search_sort.
  IF p_seq10 IS NOT INITIAL.
    ls_search_sort-sequence = p_seq10.
    ls_search_sort-sort_by_attribute = 'NI_TAKEUP'.
    ls_search_sort-sap_attribute     = 'FSNI_ICARE_VALUE'.
    IF p_rs10a = abap_true.
      ls_search_sort-sort_direction = gc_sort_asc.
    ELSEIF p_rs10d = abap_true.
      ls_search_sort-sort_direction = gc_sort_desc.
    ENDIF.

    INSERT ls_search_sort INTO TABLE gt_search_sort[].
  ENDIF.

  CLEAR: ls_search_sort.
  IF p_seq19 IS NOT INITIAL.
    ls_search_sort-sequence = p_seq19.
    ls_search_sort-sort_by_attribute = 'BASE_PRODUCT_CODE'.
    ls_search_sort-sap_attribute     = 'IDNO'.
    IF p_rs19a = abap_true.
      ls_search_sort-sort_direction = gc_sort_asc.
    ELSEIF p_rs19d = abap_true.
      ls_search_sort-sort_direction = gc_sort_desc.
    ENDIF.

    INSERT ls_search_sort INTO TABLE gt_search_sort[].
  ENDIF.


  CLEAR: ls_search_sort.
  LOOP AT gt_search_sort[] INTO ls_search_sort.
    CLEAR ls_entry.
    ls_entry-sort_by_attribute = ls_search_sort-sort_by_attribute.
    ls_entry-sort_direction    = ls_search_sort-sort_direction.
    APPEND ls_entry TO ls_search_input-sorting-entry[].
  ENDLOOP.


  pcs_search_input = ls_search_input.

ENDFORM.  " BUILD_R1_SEARCH_MAP_DATA
*&---------------------------------------------------------------------*
*&      Form  MERGE_SEARCHED_PRODUCTS
*&---------------------------------------------------------------------*
* Build and Merge National/Regional Data for Searched Products
*----------------------------------------------------------------------*
FORM merge_searched_products USING pt_basic_product  TYPE zproduct_lean_search_resp_tab3
                                   ps_hybris_results TYPE zproduct_lean_search_respons24
                                   pt_ver_status     TYPE ztarn_ver_status
                                   pt_prd_status     TYPE ztarn_prd_version
                          CHANGING pct_prod_data     TYPE ztarn_prod_data
                                   pct_reg_data      TYPE ztarn_reg_data.


  DATA: ls_basic_product TYPE zproduct_lean_search_respons22,
        ls_prod_data     TYPE zsarn_prod_data,
        ls_ver_status    TYPE zarn_ver_status,
        ls_products      TYPE zarn_products,
        lv_prod_exist    TYPE flag,
        lv_new_product   TYPE flag,
        lv_val_error     TYPE flag,
        lt_dfies         TYPE dfies_tab,
        lt_message       TYPE bal_t_msg,
        lv_log_no        TYPE balognr.




  REFRESH: lt_dfies[].
  CALL FUNCTION 'DDIF_FIELDINFO_GET'
    EXPORTING
      tabname        = 'ZSARN_SLG1_LOG_PARAMS_INBOUND'
      langu          = sy-langu
    TABLES
      dfies_tab      = lt_dfies[]
    EXCEPTIONS
      not_found      = 1
      internal_error = 2
      OTHERS         = 3.


* For each Searched Products found,
  LOOP AT pt_basic_product[] INTO ls_basic_product.

    CLEAR: lv_new_product, lv_prod_exist.

    CLEAR ls_prod_data.
* Check if National Data already Exist
    READ TABLE pct_prod_data INTO ls_prod_data
    WITH KEY idno = ls_basic_product-code.
    IF sy-subrc = 0.

      CLEAR ls_products.
      READ TABLE ls_prod_data-zarn_products INTO ls_products INDEX 1.
      IF sy-subrc = 0.
* If National data exist and is I-Cared then Ignore Searched product
* Existing National/Regional data will be displayed
        IF ls_products-fsni_icare_value = abap_true.
          CONTINUE.
        ELSE.
* If National data exist and is not I-Cared then
* -Create new product data
* -Delete from reg_data
          lv_prod_exist = lv_new_product = abap_true.
        ENDIF.
      ELSE.
        lv_prod_exist = lv_new_product = abap_true.
      ENDIF.
    ELSE.
* If National data doesn't exist
* -Create new product data
      lv_new_product = abap_true.
    ENDIF.




    IF lv_new_product = abap_true.
      IF lv_prod_exist = abap_true.
        DELETE pct_prod_data[] WHERE idno = ls_basic_product-code.
        DELETE pct_reg_data[]  WHERE idno = ls_basic_product-code.
      ENDIF.

* Get Version Status if Exist
      CLEAR ls_ver_status.
      READ TABLE pt_ver_status INTO ls_ver_status
      WITH KEY idno = ls_basic_product-code.


      CLEAR ls_prod_data.
* Build New Product Data
      PERFORM build_new_prod_data USING ls_basic_product
                                        ls_ver_status
                               CHANGING ls_prod_data.



      CLEAR lv_val_error.
* Validate National Product Data for R1 Lean Search
      PERFORM validate_r1_lean_prod_data USING ls_prod_data
                                               ps_hybris_results
                                               lt_dfies[]
                                      CHANGING lv_val_error
                                               lt_message[]
                                               gt_vlog[].


      IF ls_prod_data IS NOT INITIAL AND lv_val_error IS INITIAL.
        APPEND ls_prod_data TO pct_prod_data[].
      ELSE.
        gv_valerr_cnt = gv_valerr_cnt + 1.
      ENDIF.

    ENDIF.  " IF lv_new_product = abap_true

  ENDLOOP.  " LOOP AT pt_basic_product[] INTO ls_basic_product


  IF gv_error_cnt GT 0 OR gv_valerr_cnt GT 0.
    gv_vlog_enable = abap_true.
  ENDIF.


  IF lt_message[] IS NOT INITIAL.
* Maintain SLG1 Log
    CLEAR lv_log_no.
    PERFORM maintain_slg1_log USING lt_message[]
                           CHANGING lv_log_no.
  ENDIF.

ENDFORM.  " MERGE_SEARCHED_PRODUCTS
*&---------------------------------------------------------------------*
*&      Form  VALIDATE_R1_LEAN_PROD_DATA
*&---------------------------------------------------------------------*
* Validate National Product Data for R1 Lean Search
*----------------------------------------------------------------------*
FORM validate_r1_lean_prod_data USING ps_prod_data      TYPE zsarn_prod_data
                                      ps_hybris_results TYPE zproduct_lean_search_respons24
                                      pt_dfies          TYPE dfies_tab
                             CHANGING pcv_val_error     TYPE flag
                                      pct_message       TYPE bal_t_msg
                                      pct_vlog          TYPE ty_t_vlog.

  DATA: lo_validation_engine TYPE REF TO zcl_arn_validation_engine,
        lo_excep             TYPE REF TO zcx_arn_validation_err,
        lt_output            TYPE zarn_t_rl_output,
        ls_output            TYPE zarn_s_rl_output,
        ls_params            TYPE bal_s_parm,
        ls_log_params        TYPE zsarn_slg1_log_params_inbound,
        ls_vlog              TYPE ty_s_vlog,
        ls_val_msg           TYPE char256.


  CLEAR pcv_val_error.

  TRY.
      CREATE OBJECT lo_validation_engine
        EXPORTING
          iv_event          = zcl_arn_validation_engine=>gc_event_pim_inbound
          iv_bapiret_output = abap_true.

    CATCH zcx_arn_validation_err INTO lo_excep.
      pcv_val_error = abap_true.
      MESSAGE i000(zarena_msg) WITH lo_excep->msgv1 lo_excep->msgv2
                                    lo_excep->msgv3 lo_excep->msgv4.
      EXIT.
  ENDTRY.


  CLEAR: lt_output[].
  TRY.
      CALL METHOD lo_validation_engine->validate_arn_single
        EXPORTING
          is_zsarn_prod_data          = ps_prod_data
          iv_refresh_mstrdata_buffers = abap_true
        IMPORTING
          et_output                   = lt_output[].
    CATCH zcx_arn_validation_err INTO lo_excep.
      pcv_val_error = abap_true.
      MESSAGE i000(zarena_msg) WITH lo_excep->msgv1 lo_excep->msgv2
                                    lo_excep->msgv3 lo_excep->msgv4.
      EXIT.
  ENDTRY.



  LOOP AT lt_output[] INTO ls_output.

* Build Return Message
    CLEAR: ls_log_params, ls_params.
* Build Log Parameters
    PERFORM build_log_parameters USING pt_dfies
                                       ps_prod_data
                                       ps_hybris_results-product_lean_search_response-fsvendor_data_res-admin
                              CHANGING ls_params
                                       ls_log_params.


    PERFORM build_message USING ls_output-msgty
                                ls_output-msgid
                                ls_output-msgno
                                ls_output-msgv1
                                ls_output-msgv2
                                ls_output-msgv3
                                ls_output-msgv4
                                ls_params
                       CHANGING pct_message[].

    CLEAR ls_vlog.
    CASE ls_output-msgty.
      WHEN 'A' OR 'E'.
        ls_vlog-message_icon = icon_led_red.
        pcv_val_error = abap_true.
      WHEN 'W'.
        ls_vlog-message_icon = icon_led_yellow.
      WHEN 'I'.
        ls_vlog-message_icon = icon_led_green.
      WHEN 'S'.
        ls_vlog-message_icon = icon_system_okay.
    ENDCASE.

    ls_vlog-validation_id = ls_output-validation_id.
    ls_vlog-code          = ls_output-idno.

    CLEAR ls_val_msg.
    ls_val_msg   = ls_output-message_txt.

    IF ls_val_msg IS NOT INITIAL.
      ls_vlog-message_txt1 = ls_val_msg+0(128).

      IF ls_val_msg+128(1) IS NOT INITIAL.
        ls_vlog-message_txt2 = ls_val_msg+128(128).
      ENDIF.
    ENDIF.

    APPEND ls_vlog TO pct_vlog[].

  ENDLOOP.

ENDFORM.  " VALIDATE_R1_LEAN_PROD_DATA
*&---------------------------------------------------------------------*
*&      Form  BUILD_NEW_PROD_DATA
*&---------------------------------------------------------------------*
* Build New Product Data
*----------------------------------------------------------------------*
FORM build_new_prod_data  USING    ps_basic_product TYPE zproduct_lean_search_respons22
                                   ps_ver_status    TYPE zarn_ver_status
                          CHANGING pcs_prod_data    TYPE zsarn_prod_data.


  DATA: ls_products    TYPE zarn_products,
        ls_products_ex TYPE zarn_products_ex,
        ls_nat_mc_hier TYPE zarn_nat_mc_hier.




  CLEAR pcs_prod_data.

  pcs_prod_data-idno    = ps_basic_product-code.

  IF ps_ver_status-current_ver IS INITIAL.
    pcs_prod_data-version = 1.
  ELSE.
    pcs_prod_data-version = ps_ver_status-current_ver.
  ENDIF.




* Build Products
  CLEAR: ls_products.
  ls_products-mandt                      = sy-mandt.
  ls_products-idno                       = pcs_prod_data-idno.
  ls_products-version                    = pcs_prod_data-version.


  ls_products-code                       = ps_basic_product-code.
  ls_products-fan_id                     = ps_basic_product-fan.
  ls_products-fs_short_descr             = ps_basic_product-fsshort_description.
  ls_products-fs_short_descr_upper       = ps_basic_product-fsshort_description.
  ls_products-fs_brand_id                = ps_basic_product-fsbrand-code.
  ls_products-fs_brand_desc              = ps_basic_product-fsbrand-description.
  ls_products-status                     = ps_basic_product-status.


  ls_products-fssi_catman_value          = ps_basic_product-fssiranging-category_manager-value.
  ls_products-fssi_catman_datetime       = ps_basic_product-fssiranging-category_manager-date.
  ls_products-fssi_trents_value          = ps_basic_product-fssiranging-trents-value.
  ls_products-fssi_trents_datetime       = ps_basic_product-fssiranging-trents-date.
  ls_products-fssi_fresh_value           = ps_basic_product-fssiranging-fresh-value.
  ls_products-fssi_fresh_datetime        = ps_basic_product-fssiranging-fresh-date.
  ls_products-fssi_gtt_value             = ps_basic_product-fssiranging-gtt-value.
  ls_products-fssi_gtt_datetime          = ps_basic_product-fssiranging-gtt-date.
  ls_products-fssi_instore_value         = ps_basic_product-fssiranging-in_store-value.
  ls_products-fssi_instore_datetime      = ps_basic_product-fssiranging-in_store-date.

  ls_products-fsni_icare_value           = ps_basic_product-fsniranging-fsnitake_up-value.
  ls_products-fsni_icare_datetime        = ps_basic_product-fsniranging-fsnitake_up-date.
*  ls_products-fsni_catman_value          = ps_basic_product-fsniranging-fsnitake_up-value.
*  ls_products-fsni_catman_datetime       = ps_basic_product-fsniranging-fsnitake_up-date.
  ls_products-fsni_gilmours_value        = ps_basic_product-fsniranging-gilmours-value.
  ls_products-fsni_gilmours_datetime     = ps_basic_product-fsniranging-gilmours-date.
  ls_products-fsni_instore_value         = ps_basic_product-fsniranging-in_store-value.
  ls_products-fsni_instore_datetime      = ps_basic_product-fsniranging-in_store-date.

  TRANSLATE ls_products-fs_short_descr_upper TO UPPER CASE.
  TRANSLATE ls_products-gln_descr_upper TO UPPER CASE.
  TRANSLATE ls_products-brand_id_upper TO UPPER CASE.

  APPEND ls_products TO pcs_prod_data-zarn_products[].




* Build Products Extension
  CLEAR: ls_products_ex.
  ls_products_ex-mandt                      = sy-mandt.
  ls_products_ex-idno                       = pcs_prod_data-idno.
  ls_products_ex-version                    = pcs_prod_data-version.

  ls_products_ex-fs_brand_desc_upper        = ps_basic_product-fsbrand-description.

  TRANSLATE ls_products_ex-fs_brand_desc_upper TO UPPER CASE.

  APPEND ls_products_ex TO pcs_prod_data-zarn_products_ex[].


* Build National Merchandise Hierarchies
  CLEAR: ls_nat_mc_hier.
  ls_nat_mc_hier-mandt           = sy-mandt.
  ls_nat_mc_hier-idno            = pcs_prod_data-idno.
  ls_nat_mc_hier-version         = pcs_prod_data-version.

  IF ps_basic_product-national_merchandise_hierarchi-national_merchandise_hierarchy[] IS NOT INITIAL.
    ls_nat_mc_hier-nat_mc_code = ps_basic_product-national_merchandise_hierarchi-national_merchandise_hierarchy[ 1 ]-code.
    APPEND ls_nat_mc_hier TO pcs_prod_data-zarn_nat_mc_hier[].
  ENDIF.



* Build UOM Variants
  PERFORM build_uom_variant_table USING ps_basic_product
                               CHANGING pcs_prod_data.



ENDFORM.  " BUILD_NEW_PROD_DATA
*&---------------------------------------------------------------------*
*&      Form  BUILD_UOM_VARIANT_TABLE
*&---------------------------------------------------------------------*
* Build UOM Variants
*----------------------------------------------------------------------*
FORM build_uom_variant_table USING ps_basic_product TYPE zproduct_lean_search_respons22
                          CHANGING pcs_prod_data    TYPE zsarn_prod_data.


  DATA: ls_prod_uom_variant TYPE zproduct_lean_search_response1,
        ls_uom_variant      TYPE zarn_uom_variant.


  LOOP AT ps_basic_product-uomvariants-uomvariant[] INTO ls_prod_uom_variant.
    CLEAR: ls_uom_variant.
    ls_uom_variant-mandt                = sy-mandt.
    ls_uom_variant-idno                 = pcs_prod_data-idno.
    ls_uom_variant-version              = pcs_prod_data-version.
    ls_uom_variant-uom_code             = ls_prod_uom_variant-code.

    ls_uom_variant-base_unit            = ls_prod_uom_variant-base_unit.
    ls_uom_variant-status               = ls_prod_uom_variant-status.

    APPEND ls_uom_variant TO pcs_prod_data-zarn_uom_variant[].


* Build GTIN Variants
    PERFORM build_gtin_var_table USING ls_prod_uom_variant-gtinvariants-gtinvariant[]
                                       ls_uom_variant
                              CHANGING pcs_prod_data.

* Build Purchasing Info Records
    PERFORM build_pir_table USING ls_prod_uom_variant-purchasing_infor_records-purchasing_info_record[]
                                  ls_uom_variant
                         CHANGING pcs_prod_data.


  ENDLOOP.  " LOOP AT uomvariant[] INTO ls_prod_uom_variant

  SORT pcs_prod_data-zarn_uom_variant[] BY idno version uom_code.
  DELETE ADJACENT DUPLICATES FROM pcs_prod_data-zarn_uom_variant[] COMPARING idno version uom_code.

ENDFORM.  " BUILD_UOM_VARIANT_TABLE
*&---------------------------------------------------------------------*
*&      Form  BUILD_GTIN_VAR_TABLE
*&---------------------------------------------------------------------*
* Build GTIN Variants
*----------------------------------------------------------------------*
FORM build_gtin_var_table  USING    pt_gtinvariant TYPE zproduct_lean_search_resp_tab2
                                    ps_uom_variant TYPE zarn_uom_variant
                           CHANGING pcs_prod_data  TYPE zsarn_prod_data..


  DATA: ls_prod_gtin_var TYPE zproduct_lean_search_respons12,
        ls_gtin_var      TYPE zarn_gtin_var.



  LOOP AT pt_gtinvariant[] INTO ls_prod_gtin_var.
    CLEAR ls_gtin_var.
    ls_gtin_var-mandt        = sy-mandt.
    ls_gtin_var-idno         = pcs_prod_data-idno.
    ls_gtin_var-version      = pcs_prod_data-version.
    ls_gtin_var-uom_code     = ps_uom_variant-uom_code.
    ls_gtin_var-gtin_code    = ls_prod_gtin_var-code.

    ls_gtin_var-gtin_current = ls_prod_gtin_var-current.
    ls_gtin_var-gtin_status  = ls_prod_gtin_var-status.
    APPEND ls_gtin_var TO pcs_prod_data-zarn_gtin_var[].
  ENDLOOP.

  SORT pcs_prod_data-zarn_gtin_var[] BY idno version uom_code gtin_code.
  DELETE ADJACENT DUPLICATES FROM pcs_prod_data-zarn_gtin_var[] COMPARING idno version uom_code gtin_code.



ENDFORM.  " BUILD_GTIN_VAR_TABLE
*&---------------------------------------------------------------------*
*&      Form  BUILD_PIR_TABLE
*&---------------------------------------------------------------------*
* Build Purchasing Info Records
*----------------------------------------------------------------------*
FORM build_pir_table       USING    pt_pir         TYPE zproduct_lean_search_resp_tab1
                                    ps_uom_variant TYPE zarn_uom_variant
                           CHANGING pcs_prod_data  TYPE zsarn_prod_data..

  DATA: ls_prod_pir TYPE zproduct_lean_search_response4,
        ls_pir      TYPE zarn_pir.


  LOOP AT pt_pir[] INTO ls_prod_pir.
    CLEAR ls_pir.
    ls_pir-mandt                  = sy-mandt.
    ls_pir-idno                   = pcs_prod_data-idno.
    ls_pir-version                = pcs_prod_data-version.
    ls_pir-uom_code               = ps_uom_variant-uom_code.
    ls_pir-hybris_internal_code   = ls_prod_pir-hybris_internal_code.

    ls_pir-gln                    = ls_prod_pir-gln-gln.
    ls_pir-gln_description        = ls_prod_pir-gln-description.
    ls_pir-gln_description_upper  = ls_prod_pir-gln-description.
    ls_pir-vendor_article_number  = ls_prod_pir-vendor_article_number.
    ls_pir-avail_start_date       = ls_prod_pir-start_availability_date.
    ls_pir-avail_end_date         = ls_prod_pir-end_availability_date.
    ls_pir-effective_date         = ls_prod_pir-effective_date.

    TRANSLATE ls_pir-gln_description_upper TO UPPER CASE.

    APPEND ls_pir TO pcs_prod_data-zarn_pir[].

  ENDLOOP.

  SORT pcs_prod_data-zarn_pir[] BY idno version uom_code hybris_internal_code.
  DELETE ADJACENT DUPLICATES FROM pcs_prod_data-zarn_pir[] COMPARING idno version uom_code hybris_internal_code.

ENDFORM.  " BUILD_PIR_TABLE
*&---------------------------------------------------------------------*
*&      Form  SEARCH_MORE_R1_LEAN_PAGING
*&---------------------------------------------------------------------*
* Search More R1 Lean Results on basis of Start index and Page size
*----------------------------------------------------------------------*
FORM search_more_r1_lean_paging .

  DATA:
    ls_search_input   TYPE zproduct_lean_search_request_f,
    ls_hybris_results TYPE zproduct_lean_search_respons24,
    lt_basic_product  TYPE zproduct_lean_search_resp_tab3,
    ls_basic_product  TYPE zproduct_lean_search_respons22,
    ls_worklist       TYPE ty_outtab,
    lv_search_cnt     TYPE sy-tabix,
    lv_title          TYPE string,
    ls_layout         TYPE lvc_s_layo,
    lv_tabix          TYPE sy-tabix,
    lt_row_no         TYPE lvc_t_roid,
    ls_row_no         TYPE lvc_s_roid,
    ls_gtin           TYPE string,
    ls_vlog           TYPE ty_s_vlog,
    ls_gtin_msg       TYPE char256,

    lt_messages       TYPE bal_t_msg,
    ls_message        LIKE LINE OF lt_messages.


* Set Start index
  gv_start_index = gv_start_index + p_pagesz.


* Check if there are no more records to be searched after this search then disable Search More button
* thus check total records count with next search count value (adding page size again - just for calc)
  IF ( gv_start_index + p_pagesz ) GE gv_tot_search_cnt.
    gv_smore_disable = abap_true.
  ENDIF.


  CLEAR ls_search_input.

* Build R1 Search MApping data
  PERFORM build_r1_search_map_data USING gv_start_index
                                CHANGING ls_search_input.




  CLEAR: ls_hybris_results, lt_messages[].

* Get any records from Hybris matching search criteria
  CALL FUNCTION 'Z_HYBRIS_GET_LEAN'
    EXPORTING
      hybris_search_input  = ls_search_input
    IMPORTING
      hybris_results       = ls_hybris_results
      messages             = lt_messages[]
    EXCEPTIONS
      hybris_not_available = 1
      OTHERS               = 2.

  LOOP AT lt_messages INTO ls_message WHERE msgty EQ 'E' OR msgty EQ 'A'.
    EXIT.
  ENDLOOP.
  IF sy-subrc EQ 0.
    PERFORM popup_appl_log USING lt_messages.
    gv_smore_disable = abap_true.
    RETURN.
  ELSE.

    lt_basic_product[] = ls_hybris_results-product_lean_search_response-fsvendor_data_res-basic_product[].

    APPEND LINES OF lt_basic_product[] TO gt_basic_product[].

* Total no. of searched records
    CLEAR lv_search_cnt.
    DESCRIBE TABLE lt_basic_product[] LINES lv_search_cnt.
    gv_search_cnt = gv_search_cnt + lv_search_cnt.

* Total no. of matching records available in National
    gv_tot_search_cnt = ls_hybris_results-product_lean_search_response-fsvendor_data_res-total_records_found.

    IF gv_search_cnt GE gv_tot_search_cnt.
      gv_smore_disable = abap_true.
    ENDIF.

    gv_error_cnt = gv_error_cnt + ls_hybris_results-product_lean_search_response-fsvendor_data_res-failed_validation-total_failed_records.

    LOOP AT ls_hybris_results-product_lean_search_response-fsvendor_data_res-failed_validation-gtin[] INTO ls_gtin.
      CLEAR ls_vlog.
      ls_vlog-message_icon  = icon_led_red.
      ls_vlog-validation_id = 'NAT'.

      CLEAR ls_gtin_msg.
      SPLIT ls_gtin AT '|' INTO ls_vlog-code ls_gtin_msg.

      IF ls_gtin_msg IS NOT INITIAL.
        ls_vlog-message_txt1 = ls_gtin_msg+0(128).

        IF ls_gtin_msg+128(1) IS NOT INITIAL.
          ls_vlog-message_txt2 = ls_gtin_msg+128(128).
        ENDIF.
      ENDIF.

      APPEND ls_vlog TO gt_vlog[].
    ENDLOOP.

  ENDIF.



* Build and Merge National/Regional Data for Searched Products
  PERFORM merge_searched_products USING gt_basic_product[]
                                        ls_hybris_results
                                        gt_ver_status[]
                                        gt_prd_version[]
                               CHANGING gt_prod_data[]
                                        gt_reg_data[].



* Create ALV worklist
  PERFORM create_worklist.

  PERFORM alv_build_data.

  CALL METHOD go_worklist_alv->refresh_table_display.

  CLEAR ls_layout.
  CALL METHOD go_worklist_alv->get_frontend_layout
    IMPORTING
      es_layout = ls_layout.


  CLEAR lv_title.
  lv_title = |Total: { gv_worklist_cnt }, Nat Search: { gv_search_cnt }/{ gv_tot_search_cnt }|.
  lv_title = |{ lv_title }, Nat Err: { gv_error_cnt }|.
  lv_title = |{ lv_title }, Val Err: { gv_valerr_cnt }|.
  ls_layout-grid_title = lv_title.


  CALL METHOD go_worklist_alv->set_frontend_layout
    EXPORTING
      is_layout = ls_layout.


* Set cursor at 1st search record if sort is not defined
  IF gt_search_sort[] IS INITIAL.
    IF lt_basic_product[] IS NOT INITIAL.

      CLEAR: ls_basic_product, ls_worklist.

      TRY.
          ls_basic_product = lt_basic_product[ 1 ].

          IF ls_basic_product-code IS NOT INITIAL.
            READ TABLE gt_worklist INTO ls_worklist WITH KEY idno = ls_basic_product-code.
            IF sy-subrc = 0.
              lv_tabix = sy-tabix.

              CLEAR: ls_row_no, lt_row_no[].
              ls_row_no-row_id = lv_tabix.
              APPEND ls_row_no TO lt_row_no[].

              CALL METHOD go_worklist_alv->set_selected_rows
                EXPORTING
*                 it_index_rows            =
                  it_row_no = lt_row_no[].
*                 is_keep_other_selections =




            ENDIF.
          ENDIF.  " IF ls_basic_product-code IS INITIAL
        CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
      ENDTRY.


    ENDIF.  " IF lt_basic_product[] IS NOT INITIAL
  ENDIF.  " IF gt_search_sort[] IS INITIAL




ENDFORM.  " SEARCH_MORE_R1_LEAN_PAGING
*&---------------------------------------------------------------------*
*&      Form  BUILD_LOG_PARAMETERS
*&---------------------------------------------------------------------*
* Build Log Parameters
*----------------------------------------------------------------------*
FORM build_log_parameters USING pt_dfies       TYPE dfies_tab
                                ps_prod_data   TYPE zsarn_prod_data
                                ps_admin       TYPE zproduct_lean_search_respons23
                       CHANGING pcs_params     TYPE bal_s_parm
                                pcs_log_params TYPE zsarn_slg1_log_params_inbound.

  DATA: ls_products   TYPE zarn_products,
        ls_nat_mchier TYPE zarn_nat_mc_hier,
        ls_dfies      TYPE dfies,
        lt_par        TYPE bal_t_par,
        ls_par        TYPE bal_s_par.

  FIELD-SYMBOLS: <param>    TYPE any.


  TRY.
      ls_products   = ps_prod_data-zarn_products[ 1 ].
    CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
  ENDTRY.

  TRY.
      ls_nat_mchier = ps_prod_data-zarn_nat_mc_hier[ 1 ].
    CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
  ENDTRY.

  CLEAR: pcs_log_params.
  pcs_log_params-guid       = ps_admin-national_guid.
  pcs_log_params-created_on = sy-datum.
  pcs_log_params-created_at = sy-uzeit.
  pcs_log_params-created_by = sy-uname.
  pcs_log_params-fan_id     = ls_products-fan_id.
  pcs_log_params-arn_mode   = ps_admin-operation_mode.
  pcs_log_params-code       = ps_prod_data-idno.

  pcs_log_params-matkl      = ls_nat_mchier-nat_mc_code.


  pcs_log_params-name       = ls_products-name.
  pcs_log_params-descr      = ls_products-description.
  pcs_log_params-fs_descr_s = ls_products-fs_short_descr.
  pcs_log_params-brandid    = ls_products-brand_id.
  pcs_log_params-fs_brandid = ls_products-fs_brand_id.
  pcs_log_params-fs_brn_dsc = ls_products-fs_brand_desc.


  TRANSLATE pcs_log_params-name       TO UPPER CASE.
  TRANSLATE pcs_log_params-descr      TO UPPER CASE.
  TRANSLATE pcs_log_params-fs_descr_s TO UPPER CASE.
  TRANSLATE pcs_log_params-brandid    TO UPPER CASE.
  TRANSLATE pcs_log_params-fs_brandid TO UPPER CASE.
  TRANSLATE pcs_log_params-fs_brn_dsc TO UPPER CASE.


  REFRESH: lt_par[].
  CLEAR: pcs_params.

  LOOP AT pt_dfies INTO ls_dfies.

    ASSIGN COMPONENT ls_dfies-fieldname OF STRUCTURE pcs_log_params TO <param>.

    CLEAR ls_par.
    ls_par-parname  = ls_dfies-fieldname.
    ls_par-parvalue = <param>.
    APPEND ls_par TO lt_par[].
  ENDLOOP.

  pcs_params-t_par[] = lt_par[].
  pcs_params-altext = 'ZARN_INBOUND'.

ENDFORM.  " BUILD_LOG_PARAMETERS
*&---------------------------------------------------------------------*
*&      Form  BUILD_MESSAGE
*&---------------------------------------------------------------------*
* Build Return Message
*----------------------------------------------------------------------*
FORM build_message USING fu_v_type       TYPE sy-msgty
                         fu_v_id         TYPE sy-msgid
                         fu_v_number     TYPE sy-msgno
                         fu_v_message_v1 TYPE symsgv
                         fu_v_message_v2 TYPE symsgv
                         fu_v_message_v3 TYPE symsgv
                         fu_v_message_v4 TYPE symsgv
                         fu_v_params     TYPE bal_s_parm
                CHANGING fc_t_message    TYPE bal_t_msg.

  DATA: ls_message      TYPE bal_s_msg.

  CLEAR ls_message.
  ls_message-msgty  = fu_v_type.
  ls_message-msgid  = fu_v_id.
  ls_message-msgno  = fu_v_number.
  ls_message-msgv1  = fu_v_message_v1.
  ls_message-msgv2  = fu_v_message_v2.
  ls_message-msgv3  = fu_v_message_v3.
  ls_message-msgv4  = fu_v_message_v4.
  ls_message-params = fu_v_params.

  APPEND ls_message TO fc_t_message[].

ENDFORM.  " BUILD_MESSAGE
*&---------------------------------------------------------------------*
*&      Form  MAINTAIN_SLG1_LOG
*&---------------------------------------------------------------------*
* Maintain SLG1 Log
*----------------------------------------------------------------------*
FORM maintain_slg1_log USING fu_t_message TYPE bal_t_msg
                    CHANGING fc_v_log_no  TYPE balognr.

  DATA: ls_s_log      TYPE bal_s_log,
        ls_log_handle TYPE balloghndl,
        lt_message    TYPE bal_t_msg,
        ls_message    TYPE bal_s_msg.

  lt_message[] = fu_t_message[].

  ls_s_log-object    = 'ZARN'.
  ls_s_log-subobject = 'ZARN_INBOUND'.
  ls_s_log-aldate    = sy-datum.
  ls_s_log-altime    = sy-uzeit.
  ls_s_log-aluser    = sy-uname.
  ls_s_log-altcode   = sy-tcode.

* Create Log
  PERFORM create_log USING ls_s_log
                  CHANGING ls_log_handle.


  LOOP AT lt_message INTO ls_message.
* Write Message to Log
    PERFORM write_msg USING ls_log_handle
                   CHANGING ls_message.

  ENDLOOP.

* Save and Display the log
  PERFORM save_log USING ls_log_handle
                         abap_true  " Save
                         space      " Display
                CHANGING fc_v_log_no.


ENDFORM.                    " MAINTAIN_SLG1_LOG
*&---------------------------------------------------------------------*
*&      Form  CREATE_LOG
*&---------------------------------------------------------------------*
* Create Log
*----------------------------------------------------------------------*
FORM create_log USING fu_s_log        TYPE bal_s_log
             CHANGING fc_s_log_handle TYPE balloghndl.

  CLEAR fc_s_log_handle.
  CALL FUNCTION 'BAL_LOG_CREATE'
    EXPORTING
      i_s_log                 = fu_s_log
    IMPORTING
      e_log_handle            = fc_s_log_handle
    EXCEPTIONS
      log_header_inconsistent = 1
      OTHERS                  = 2.
  IF sy-subrc <> 0.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
            WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.


ENDFORM.                    " CREATE_LOG
*&---------------------------------------------------------------------*
*&      Form  WRITE_MSG
*&---------------------------------------------------------------------*
* Write Message to Log
*----------------------------------------------------------------------*
FORM write_msg  USING fu_s_log_handle TYPE balloghndl
             CHANGING fc_s_message    TYPE bal_s_msg.


  CALL FUNCTION 'BAL_LOG_MSG_ADD'
    EXPORTING
      i_log_handle     = fu_s_log_handle
      i_s_msg          = fc_s_message
    EXCEPTIONS
      log_not_found    = 1
      msg_inconsistent = 2
      log_is_full      = 3
      OTHERS           = 4.
  IF sy-subrc <> 0.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
            WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.

ENDFORM.                    " WRITE_MSG
*&---------------------------------------------------------------------*
*&      Form  SAVE_LOG
*&---------------------------------------------------------------------*
* Save and Display the log
*----------------------------------------------------------------------*
FORM save_log  USING fu_s_log_handle TYPE balloghndl
                     fi_v_save       TYPE boole_d
                     fi_v_display    TYPE boole_d
            CHANGING fc_v_log_no  TYPE balognr.

  DATA: lt_log_handle TYPE bal_t_logh,
        lt_lognumbers TYPE bal_t_lgnm,
        ls_lognumbers TYPE bal_s_lgnm.

  APPEND fu_s_log_handle TO lt_log_handle.

  IF fi_v_save = abap_true.

    REFRESH: lt_lognumbers[].
    CALL FUNCTION 'BAL_DB_SAVE'
      EXPORTING
        i_t_log_handle   = lt_log_handle
      IMPORTING
        e_new_lognumbers = lt_lognumbers[]
      EXCEPTIONS
        log_not_found    = 1
        save_not_allowed = 2
        numbering_error  = 3
        OTHERS           = 4.
    IF sy-subrc <> 0.

      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
              WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ELSE.

      CLEAR ls_lognumbers.
      READ TABLE lt_lognumbers[] INTO ls_lognumbers
      WITH KEY log_handle = fu_s_log_handle.
      IF sy-subrc = 0.
        fc_v_log_no = ls_lognumbers-lognumber.
      ENDIF.

    ENDIF.

  ENDIF.

  IF fi_v_display = abap_true.

    CALL FUNCTION 'BAL_DSP_LOG_DISPLAY'
      EXPORTING
        i_t_log_handle       = lt_log_handle
      EXCEPTIONS
        profile_inconsistent = 1
        internal_error       = 2
        no_data_available    = 3
        no_authority         = 4
        OTHERS               = 5.
    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
              WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

  ENDIF.

ENDFORM.                    " SAVE_LOG
*&---------------------------------------------------------------------*
*&      Form  DISPLAY_VLOG_R1_LEAN_SEARCH
*&---------------------------------------------------------------------*
* Search More R1 Lean Results on basis of Start index and Page size
*----------------------------------------------------------------------*
FORM display_vlog_r1_lean_search .


  DATA: lo_dialogbox_container TYPE REF TO cl_gui_dialogbox_container,
        lo_log_alv             TYPE REF TO cl_salv_table,
        "lt_val_output TYPE zarn_t_rl_output,
        "lr_table      TYPE REF TO cl_salv_table,
        lr_layout              TYPE REF TO cl_salv_layout,
        ls_lay_key             TYPE salv_s_layout_key,
        lv_header              TYPE lvc_title,
        lr_excep               TYPE REF TO zcx_arn_validation_err.

*  IF lo_dialogbox_container IS INITIAL.
  CREATE OBJECT lo_dialogbox_container
    EXPORTING
      top     = 150
      left    = 150
*     lifetime = CNTL_LIFETIME_session
      caption = 'Message Log'
      width   = 800
      height  = 150.

  SET HANDLER go_event_receiver->close_dialogbox FOR lo_dialogbox_container.
**  ELSE.
**    CALL METHOD lo_dialogbox_container->set_visible
**      EXPORTING
**        visible = abap_true.
**
**  ENDIF.




  CLEAR lo_log_alv.
  TRY.

      cl_salv_table=>factory( EXPORTING r_container  = lo_dialogbox_container
                              IMPORTING r_salv_table = lo_log_alv
                               CHANGING t_table      = gt_vlog[] ).

** CATCH cx_salv_msg .
**ENDTRY.

      lo_log_alv->get_columns( )->set_optimize( 'X' ).
      lo_log_alv->get_functions( )->set_all( ).

      lo_log_alv->display( ).
**    lr_table->refresh_table_display( ).
    CATCH cx_salv_msg.
      "TBD
  ENDTRY.




ENDFORM.  " DISPLAY_VLOG_R1_LEAN_SEARCH
*&---------------------------------------------------------------------*
*&      Form  ICARE_ACTIVATION_R3
*&---------------------------------------------------------------------*
* I-Care Products
*----------------------------------------------------------------------*
FORM icare_activation_r3 USING pt_md_category TYPE zmd_t_zmd_category
                      CHANGING pct_worklist   TYPE ty_t_outtab
                               pct_reg_data   TYPE ztarn_reg_data
                               pct_messages   TYPE bal_t_msg
                               pct_vlog       TYPE ty_t_vlog
                               pcv_r3_sucs    TYPE flag.

  DATA:
    ls_worklist        TYPE ty_outtab,
    lt_worklist        TYPE STANDARD TABLE OF ty_outtab,
*    ls_rows     LIKE LINE OF pt_rows,
    lv_valid           TYPE c,
    ls_product         TYPE zproduct,
    lv_idno            TYPE zarn_idno,
    ls_reg_hdr         TYPE zarn_reg_hdr,
    lt_reg_data        TYPE ztarn_reg_data,
    lt_reg_data_tmp    TYPE ztarn_reg_data,
    ls_reg_data        TYPE zsarn_reg_data,

    ls_reg_data_db     TYPE zsarn_reg_data,
    ls_reg_hdr_db      TYPE zarn_reg_hdr,
    lt_hybris_out      TYPE STANDARD TABLE OF zproduct_staging_icare_reques4,
    ls_hybris_out      TYPE zproduct_staging_icare_reques4,
    lt_hybris_in       TYPE STANDARD TABLE OF zproduct_staging_icare_respon4,
    ls_hybris_in       TYPE zproduct_staging_icare_respon4,
    lv_release_status  TYPE zarn_rel_status,
    lt_idno_noautoappr TYPE ztarn_idno_key,

    ls_md_category     LIKE LINE OF gt_md_category,
    lv_catman          TYPE zmd_e_catman,

    ls_ver_status      TYPE zarn_ver_status,
    ls_prd_version     TYPE zarn_prd_version,
    ls_control         TYPE zarn_control,
    lt_messages        TYPE bal_t_msg,
    lv_resp_desc       TYPE char256,
    lt_vlog            TYPE ty_t_vlog,
    ls_vlog            TYPE ty_s_vlog,

    lt_messages_all    TYPE  bal_t_msg,
    lv_tabix           TYPE sy-tabix.

  FIELD-SYMBOLS: <ls_reg_data>    TYPE zsarn_reg_data,
                 <pcs_reg_data>   TYPE zsarn_reg_data,
                 <pcs_worklist>   TYPE ty_outtab,
                 <ls_reg_hdr>     TYPE zarn_reg_hdr,
                 <ls_ver_status>  TYPE zarn_ver_status,
                 <ls_prd_version> TYPE zarn_prd_version,
                 <ls_control>     TYPE zarn_control.

*   at rows where
  LOOP AT pct_worklist INTO ls_worklist
    WHERE fsni_icare_value IS     INITIAL AND   " ICare not done
          checkbox         IS NOT INITIAL AND   " ICare requested on ALV
          catalog_version  EQ gc_staged.

*   Is this a new I-Care or selected ?
    lv_tabix = sy-tabix.
    READ TABLE gt_worklist_orig ASSIGNING FIELD-SYMBOL(<ls_worklist_orig>) WITH KEY idno = ls_worklist-idno.
    IF sy-subrc EQ 0.
      IF <ls_worklist_orig>-checkbox IS NOT INITIAL.
*       This was selected as already I-Care so do not process unless it was selected
        READ TABLE gt_rows TRANSPORTING NO FIELDS WITH KEY index = lv_tabix.
        IF sy-subrc NE 0.
*         Not a new I-Care and not selected
          CONTINUE.
        ENDIF.
      ENDIF.
    ENDIF.

*     Frist check that a category manager has been assigned
    IF ( ls_worklist-gil_zzcatman IS INITIAL   AND
         ls_worklist-ret_zzcatman IS INITIAL )        OR
         ls_worklist-initiation        IS INITIAL     OR
         ls_worklist-requested_by      IS INITIAL     OR
         ls_worklist-requested_by_date IS INITIAL     OR
         ls_worklist-requestor         IS INITIAL.
* Category manager/Initiation/Request Information not assigned for
      MESSAGE i127 WITH ls_worklist-idno.
      EXIT.
    ENDIF.


*     build Regional header table
    CLEAR ls_reg_hdr.
    ls_reg_hdr-mandt             = sy-mandt.
    ls_reg_hdr-idno              = ls_worklist-idno.
    ls_reg_hdr-version           = ls_worklist-version.
    ls_reg_hdr-status            = gc_status_new.
    ls_reg_hdr-gil_zzcatman      = ls_worklist-gil_zzcatman.
    ls_reg_hdr-ret_zzcatman      = ls_worklist-ret_zzcatman.
    ls_reg_hdr-initiation        = ls_worklist-initiation.

    ls_reg_hdr-creation_time_r1  = ls_worklist-creation_time_r1.
    ls_reg_hdr-requested_by      = ls_worklist-requested_by.
    ls_reg_hdr-requested_by_date = ls_worklist-requested_by_date.
    ls_reg_hdr-requestor         = ls_worklist-requestor.
    ls_reg_hdr-requested_mc      = ls_worklist-requested_mc.
    ls_reg_hdr-requested_on      = sy-datum && sy-uzeit.

    IF ls_worklist-id_type = 'R'.

      " Default only if requested by date is in past/current
      IF ls_reg_hdr-requested_by_date+0(8) LE sy-datum.
        CLEAR lv_catman.
        lv_catman = ls_worklist-ret_zzcatman.
        IF lv_catman IS INITIAL.
          lv_catman = ls_worklist-gil_zzcatman.
        ENDIF.

* Get category Data for defaulting
        CLEAR ls_md_category.
        READ TABLE pt_md_category[] INTO ls_md_category
        WITH TABLE KEY role_id = lv_catman.
        IF sy-subrc = 0.
          ls_reg_hdr-requested_on      = sy-datum && sy-uzeit.

          PERFORM get_requested_by_date USING sy-datum
                                              'ZZ'
                                              ls_md_category-enrich_sla
                                     CHANGING ls_reg_hdr-requested_by_date.

        ENDIF.  " READ TABLE gt_md_category[] INTO ls_md_category
      ENDIF.  " IF ls_reg_hdr-requested_by_date+0(8) LT sy-datum

* Get existing Regional data
      CLEAR: ls_reg_data_db, ls_reg_hdr_db.
      READ TABLE pct_reg_data[] INTO ls_reg_data_db WITH KEY idno = ls_worklist-idno.
      IF sy-subrc = 0.
        READ TABLE ls_reg_data_db-zarn_reg_hdr[] INTO ls_reg_hdr_db WITH KEY idno = ls_worklist-idno.
      ENDIF.

      ls_reg_hdr-ersda        = ls_reg_hdr_db-ersda.
      ls_reg_hdr-erzet        = ls_reg_hdr_db-erzet.
      ls_reg_hdr-ernam        = ls_reg_hdr_db-ernam.
      ls_reg_hdr-laeda        = sy-datum.
      ls_reg_hdr-eruet        = sy-uzeit.
      ls_reg_hdr-aenam        = sy-uname.
    ELSE.
      ls_reg_hdr-ersda        = sy-datum.
      ls_reg_hdr-erzet        = sy-uzeit.
      ls_reg_hdr-ernam        = sy-uname.
    ENDIF.  " IF ls_worklist-id_type = 'R'





*     collect rows that require ICare
*     get ICare
    ls_hybris_out-product_staging_icare_request-product_enrichment_priority_re-product_code-base_product_code  =  ls_reg_hdr-idno.

    ls_hybris_out-product_staging_icare_request-product_enrichment_priority_re-niprioritisation-requested_by      = ls_reg_hdr-requested_by.
    ls_hybris_out-product_staging_icare_request-product_enrichment_priority_re-niprioritisation-requested_by_date = ls_reg_hdr-requested_by_date.
    ls_hybris_out-product_staging_icare_request-product_enrichment_priority_re-niprioritisation-requestor         = ls_reg_hdr-requestor.

    ls_hybris_out-product_staging_icare_request-product_enrichment_priority_re-national_merch_hierachy = ls_reg_hdr-requested_mc.



    APPEND ls_hybris_out TO lt_hybris_out[].
    CLEAR ls_hybris_out.



    CLEAR lv_release_status.

    ls_reg_data-idno        = ls_worklist-idno.

    APPEND:
       ls_reg_hdr  TO ls_reg_data-zarn_reg_hdr,
       ls_reg_data TO lt_reg_data.



    CLEAR:
       ls_reg_hdr,
       ls_reg_data.
  ENDLOOP.  " LOOP AT gt_worklist INTO ls_worklist



  IF lt_hybris_out[] IS NOT INITIAL.
    CLEAR: lt_messages_all[].

    LOOP AT lt_hybris_out INTO ls_hybris_out.
      CLEAR: ls_hybris_in, lt_messages[],
             ls_control, ls_prd_version, ls_ver_status.
      CALL FUNCTION 'Z_HYBRIS_SET_R3'
        EXPORTING
          request              = ls_hybris_out
        IMPORTING
          response             = ls_hybris_in
          es_control           = ls_control
          es_prd_version       = ls_prd_version
          es_ver_status        = ls_ver_status
          messages             = lt_messages[]
        EXCEPTIONS
          hybris_not_available = 1
          OTHERS               = 2.

      IF ls_hybris_in-product_staging_icare_response-resp_code IS NOT INITIAL.
        CLEAR ls_vlog.
        ls_vlog-message_icon  = icon_led_red.
        ls_vlog-validation_id = 'I-CARE'.
        ls_vlog-code = ls_hybris_out-product_staging_icare_request-product_enrichment_priority_re-product_code-base_product_code.

        lv_resp_desc = ls_hybris_in-product_staging_icare_response-resp_desc.

        IF lv_resp_desc IS NOT INITIAL.
          ls_vlog-message_txt1 = lv_resp_desc+0(128).

          IF lv_resp_desc+128(1) IS NOT INITIAL.
            ls_vlog-message_txt2 = lv_resp_desc+128(128).
          ENDIF.
        ENDIF.

        APPEND ls_vlog TO lt_vlog[].
      ENDIF.

      " Clear the corresponding GT_VLOG records to prevent invalid log entries from
      " being displayed. Not clearing entire global table to avoid clearing valid
      " log entries.
      DELETE gt_vlog
        WHERE validation_id = 'I-CARE'
          AND code          = ls_hybris_out-product_staging_icare_request-product_enrichment_priority_re-product_code-base_product_code.



      CLEAR : ls_product, lv_idno.
      ls_product = ls_hybris_in-product_staging_icare_response-product_info-product.
      lv_idno = ls_product-code.

* IF success then update basic product info into REG_HDR to search for R mode
      IF lv_idno IS NOT INITIAL.
        IF <ls_reg_data> IS ASSIGNED. UNASSIGN <ls_reg_data>. ENDIF.
        READ TABLE lt_reg_data ASSIGNING <ls_reg_data>
        WITH KEY idno = lv_idno.
        IF <ls_reg_data> IS ASSIGNED.
          IF <ls_reg_hdr> IS ASSIGNED. UNASSIGN <ls_reg_hdr>. ENDIF.
          READ TABLE <ls_reg_data>-zarn_reg_hdr ASSIGNING <ls_reg_hdr>
          WITH KEY idno = lv_idno.
          IF <ls_reg_hdr> IS ASSIGNED.

            <ls_reg_hdr>-version              = ls_ver_status-current_ver.
            <ls_reg_hdr>-fs_short_descr_upper = ls_product-fsshort_description.
            <ls_reg_hdr>-fs_brand_desc_upper  = ls_product-fsbrand_desc.

            TRANSLATE <ls_reg_hdr>-fs_short_descr_upper TO UPPER CASE.
            TRANSLATE <ls_reg_hdr>-fs_brand_desc_upper  TO UPPER CASE.

** Update Main Regional Data
*            READ TABLE pt_reg_data[] ASSIGNING <pcs_reg_data>
*            WITH KEY idno = lv_idno.
*            IF <pcs_reg_data> IS ASSIGNED.
*              <pcs_reg_data>-zarn_reg_hdr[] = <ls_reg_data>-zarn_reg_hdr[].
*            ENDIF.

          ENDIF.  " IF <ls_reg_hdr> IS ASSIGNED
        ENDIF.  " IF <ls_reg_data> IS ASSIGNED

        CLEAR: ls_reg_data, lt_reg_data_tmp[].
        READ TABLE lt_reg_data[] INTO ls_reg_data WITH KEY idno = lv_idno.
        IF sy-subrc = 0.

          APPEND ls_reg_data TO lt_reg_data_tmp[].

          CLEAR lt_idno_noautoappr[].
          lt_idno_noautoappr[] = CORRESPONDING #( lt_reg_data_tmp ).


          CALL FUNCTION 'ZARN_REGIONAL_DB_UPDATE'   " Update Module
            EXPORTING
              it_reg_data       = lt_reg_data_tmp[]
              "explicitly say, don't do regional autoapprovals at this stage
              it_idno_val_error = lt_idno_noautoappr[].


          pcv_r3_sucs = abap_true.
        ENDIF.


*        MESSAGE s000 WITH text-037 text-038.

      ENDIF.  " IF lv_idno IS NOT INITIAL

      IF ls_hybris_in IS NOT INITIAL.
        APPEND ls_hybris_in TO lt_hybris_in[].
      ENDIF.

      IF lt_messages[] IS NOT INITIAL.
        APPEND LINES OF lt_messages[] TO lt_messages_all[].
      ENDIF.

    ENDLOOP.  " LOOP AT lt_hybris_out INTO ls_hybris_out

  ENDIF.  " IF lt_hybris_out[] IS NOT INITIAL

*  CALL METHOD go_worklist_alv->refresh_table_display.

  IF lt_vlog[] IS NOT INITIAL.
    APPEND LINES OF lt_vlog[] TO gt_vlog[].

    gv_vlog_enable = abap_true.
  ENDIF.


  pct_messages[]  = lt_messages[].
  pct_vlog[]      = lt_vlog[].


ENDFORM.  " ICARE_ACTIVATION_R3
*&---------------------------------------------------------------------*
*&      Form  MANAGE_WORKLIST_DATA_CHANGE
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
FORM manage_worklist_data_change CHANGING pc_er_data_changed TYPE REF TO cl_alv_changed_data_protocol.

  DATA: lt_mod_cells   TYPE lvc_t_modi,
        ls_mod_cells   TYPE lvc_s_modi,
        ls_worklist    LIKE LINE OF gt_worklist,
        ls_stable      TYPE lvc_s_stbl,

        ls_md_category LIKE LINE OF gt_md_category,
        lv_catman      TYPE zmd_e_catman,
        lv_user        TYPE xubname,
        ls_address     TYPE bapiaddr3,
        lt_return      TYPE TABLE OF bapiret2.


  FIELD-SYMBOLS: <lfs_mod_rows>    TYPE table,
                 <lfs_mod_rows_wl> LIKE LINE OF gt_worklist,
                 <ls_worklist>     LIKE LINE OF gt_worklist.


  IF mytab-activetab NE gc_icare_tab.
    RETURN.
  ENDIF.



  ASSIGN pc_er_data_changed->mp_mod_rows->* TO <lfs_mod_rows>.

  LOOP AT pc_er_data_changed->mt_mod_cells INTO ls_mod_cells.

* Current data as changed on ALV
    READ TABLE <lfs_mod_rows> ASSIGNING <lfs_mod_rows_wl> INDEX ls_mod_cells-tabix.
    IF sy-subrc EQ 0.

* Data before change
      READ TABLE gt_worklist[] ASSIGNING <ls_worklist> INDEX ls_mod_cells-row_id.
      IF sy-subrc EQ 0.

* Requested by date must be in the future
        IF ls_mod_cells-fieldname = 'REQUESTED_BY_DATE' AND
           <lfs_mod_rows_wl>-requested_by_date IS NOT INITIAL AND
           <lfs_mod_rows_wl>-requested_by_date+0(8) LE sy-datum AND
           <lfs_mod_rows_wl>-id_type NE 'R'.
          MESSAGE 'Requested by date must be in the future' TYPE 'S' DISPLAY LIKE 'E'.

          <lfs_mod_rows_wl>-requested_by_date = <ls_worklist>-requested_by_date.
          EXIT.
        ENDIF.


* Defaulting should happen for Staged products when they are selected to be I-Cared, else not
        IF <lfs_mod_rows_wl>-catalog_version EQ gc_staged AND
           <lfs_mod_rows_wl>-checkbox        EQ abap_true.


* Either Catnman must be filled
          IF <lfs_mod_rows_wl>-gil_zzcatman IS INITIAL AND <lfs_mod_rows_wl>-ret_zzcatman IS INITIAL.
            CONTINUE.
          ENDIF.


          IF ls_mod_cells-fieldname = 'CHECKBOX' OR
             ls_mod_cells-fieldname = 'GIL_ZZCATMAN' OR
             ls_mod_cells-fieldname = 'RET_ZZCATMAN'.


            CLEAR lv_catman.
            lv_catman = <lfs_mod_rows_wl>-ret_zzcatman.
            IF lv_catman IS INITIAL.
              lv_catman = <lfs_mod_rows_wl>-gil_zzcatman.
            ENDIF.


            CLEAR ls_md_category.
            READ TABLE gt_md_category[] INTO ls_md_category
            WITH TABLE KEY role_id = lv_catman.
            IF sy-subrc = 0.

              CLEAR: lv_user, <lfs_mod_rows_wl>-requested_by.
              lv_user = ls_md_category-merch_user.

              IF <lfs_mod_rows_wl>-ret_zzcatman IS NOT INITIAL.
                IF lv_user IS NOT INITIAL.
                  CLEAR: ls_address, lt_return[].
                  CALL FUNCTION 'BAPI_USER_GET_DETAIL'
                    EXPORTING
                      username = lv_user
                    IMPORTING
                      address  = ls_address
                    TABLES
                      return   = lt_return.
                  IF lt_return IS INITIAL.
                    <lfs_mod_rows_wl>-requested_by = ls_address-e_mail.
                  ENDIF.
                ENDIF.
              ELSEIF <lfs_mod_rows_wl>-gil_zzcatman IS NOT INITIAL.
                <lfs_mod_rows_wl>-requested_by = ls_md_category-alt_email.
              ENDIF.




              <lfs_mod_rows_wl>-requestor         = ls_md_category-requestor.
              <lfs_mod_rows_wl>-requested_mc      = ls_md_category-requested_mc.
              <lfs_mod_rows_wl>-requested_on      = sy-datum && sy-uzeit.
              <lfs_mod_rows_wl>-enrich_sla        = ls_md_category-enrich_sla.

              PERFORM get_requested_by_date USING sy-datum
                                                  'ZZ'
                                                  ls_md_category-enrich_sla
                                         CHANGING <lfs_mod_rows_wl>-requested_by_date.



              <ls_worklist> = <lfs_mod_rows_wl>.
            ENDIF.  " READ TABLE gt_md_category[] INTO ls_md_category

          ENDIF. " IF ls_mod_cells-fieldname = 'CHECKBOX'/'GIL_ZZCATMAN'/'RET_ZZCATMAN'

        ENDIF.  " IF catalog_version EQ gc_staged AND checkbox EQ abap_true

      ENDIF.  " READ TABLE gt_worklist[] ASSIGNING <ls_worklist>
    ENDIF.  " READ TABLE <lfs_mod_rows> ASSIGNING <lfs_mod_rows_wl>

  ENDLOOP.  " LOOP AT pc_er_data_changed->mt_mod_cells INTO ls_mod_cells

*  cl_gui_cfw=>set_new_ok_code( new_code = 'SYSTEM').

  ls_stable-col = abap_true.
  ls_stable-row = abap_true.

  CALL METHOD go_worklist_alv->refresh_table_display
    EXPORTING
      is_stable      = ls_stable
      i_soft_refresh = abap_true
    EXCEPTIONS
      finished       = 1
      OTHERS         = 2.

ENDFORM.  " MANAGE_WORKLIST_DATA_CHANGE
*&---------------------------------------------------------------------*
*&      Form  GET_REQUESTED_BY_DATE
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
FORM get_requested_by_date USING p_date         TYPE datum
                                 p_calid        TYPE wfcid
                                 p_enrich_sla   TYPE zarn_enrich_sla
                        CHANGING pc_req_by_date TYPE zarn_requested_by_date.

  DATA: lv_date    TYPE scdatum,
        lv_facdate TYPE facdate.

  CLEAR pc_req_by_date.

  CLEAR lv_date.
  lv_date = p_date.

  CLEAR lv_facdate.
  CALL FUNCTION 'DATE_CONVERT_TO_FACTORYDATE'
    EXPORTING
      date                         = lv_date
      factory_calendar_id          = p_calid
    IMPORTING
      factorydate                  = lv_facdate
    EXCEPTIONS
      calendar_buffer_not_loadable = 1
      correct_option_invalid       = 2
      date_after_range             = 3
      date_before_range            = 4
      date_invalid                 = 5
      factory_calendar_not_found   = 6
      OTHERS                       = 7.

  lv_facdate = lv_facdate + p_enrich_sla.

  CLEAR lv_date.
  CALL FUNCTION 'FACTORYDATE_CONVERT_TO_DATE'
    EXPORTING
      factorydate                  = lv_facdate
      factory_calendar_id          = 'ZZ'
    IMPORTING
      date                         = lv_date
    EXCEPTIONS
      calendar_buffer_not_loadable = 1
      factorydate_after_range      = 2
      factorydate_before_range     = 3
      factorydate_invalid          = 4
      factory_calendar_id_missing  = 5
      factory_calendar_not_found   = 6
      OTHERS                       = 7.

  pc_req_by_date = lv_date.




ENDFORM.  " GET_REQUESTED_BY_DATE
*&---------------------------------------------------------------------*
*&      Form  GET_ENRICH_SLA_FROM_REQDATE
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
FORM get_enrich_sla_from_reqdate USING p_req_by_date TYPE zarn_requested_by_date
                                       p_calid       TYPE wfcid
                              CHANGING pc_enrich_sla TYPE zarn_enrich_sla.

  DATA: lv_date          TYPE scdatum,
        lv_facdate_curr  TYPE facdate,
        lv_facdate_reqby TYPE facdate.

* Get Current Fatory Date
  CLEAR lv_date.
  lv_date = sy-datum.

  CLEAR lv_facdate_curr.
  CALL FUNCTION 'DATE_CONVERT_TO_FACTORYDATE'
    EXPORTING
      date                         = lv_date
      factory_calendar_id          = p_calid
    IMPORTING
      factorydate                  = lv_facdate_curr
    EXCEPTIONS
      calendar_buffer_not_loadable = 1
      correct_option_invalid       = 2
      date_after_range             = 3
      date_before_range            = 4
      date_invalid                 = 5
      factory_calendar_not_found   = 6
      OTHERS                       = 7.


* Get Requested By Factory Date
  CLEAR lv_date.
  lv_date = p_req_by_date+0(8).

  CLEAR lv_facdate_reqby.
  CALL FUNCTION 'DATE_CONVERT_TO_FACTORYDATE'
    EXPORTING
      date                         = lv_date
      factory_calendar_id          = p_calid
    IMPORTING
      factorydate                  = lv_facdate_reqby
    EXCEPTIONS
      calendar_buffer_not_loadable = 1
      correct_option_invalid       = 2
      date_after_range             = 3
      date_before_range            = 4
      date_invalid                 = 5
      factory_calendar_not_found   = 6
      OTHERS                       = 7.


  pc_enrich_sla = lv_facdate_reqby - lv_facdate_curr.

ENDFORM.  " GET_ENRICH_SLA_FROM_REQDATE
*&---------------------------------------------------------------------*
*&      Form  REPEAT_ENRICHMENT_PROCESS_R3
*&---------------------------------------------------------------------*
* Repeat Enrichment Process
*----------------------------------------------------------------------*
FORM repeat_enrichment_process_r3 USING pt_rows TYPE lvc_t_row.

  DATA: ls_rows       TYPE lvc_s_row,
        lt_worklist   TYPE STANDARD TABLE OF ty_outtab,
        ls_worklist   TYPE ty_outtab,
        lt_idno_range TYPE ztarn_idno_range,
        ls_idno_range TYPE zsarn_idno,
        lv_arena_mass TYPE flag,
        lv_answer     TYPE char1,
        lt_messages   TYPE bal_t_msg,
        lt_vlog       TYPE ty_t_vlog,
        lv_r3_sucs    TYPE flag.



  LOOP AT pt_rows[] INTO ls_rows.
    CLEAR ls_worklist.
    READ TABLE gt_worklist INTO ls_worklist INDEX ls_rows-index.
    IF sy-subrc = 0 AND ls_worklist-id_type = 'R' AND ls_worklist-catalog_version = gc_staged.
      APPEND ls_worklist TO lt_worklist[].
    ENDIF.
  ENDLOOP.

  IF lt_worklist[] IS INITIAL.
    MESSAGE 'Select atleast 1 Requested row' TYPE 'S' DISPLAY LIKE 'E'.
    RETURN.
  ENDIF.

  CLEAR lv_answer.
  CALL FUNCTION 'POPUP_TO_CONFIRM'
    EXPORTING
      titlebar              = 'Re-Request?'
*     DIAGNOSE_OBJECT       = ' '
      text_question         = 'Do you want to Repeat Enrichment Request?'
      text_button_1         = 'Yes'
*     icon_button_1         = ' '
      text_button_2         = 'No'
*     icon_button_2         = ' '
      default_button        = '1'
      display_cancel_button = ' '
*     USERDEFINED_F1_HELP   = ' '
      start_column          = 50
      start_row             = 15
*     POPUP_TYPE            =
*     IV_QUICKINFO_BUTTON_1 = ' '
*     IV_QUICKINFO_BUTTON_2 = ' '
    IMPORTING
      answer                = lv_answer
* TABLES
*     PARAMETER             =
    EXCEPTIONS
      text_not_found        = 1
      OTHERS                = 2.

  IF lv_answer = 1.

    CALL FUNCTION 'SAPGUI_PROGRESS_INDICATOR'
      EXPORTING
        percentage = 50
        text       = 'Re-Requesting Staged data for Enrichment'.

    CLEAR lv_r3_sucs.
    PERFORM icare_activation_r3 USING gt_md_category[]
                             CHANGING lt_worklist[]
                                      gt_reg_data[]
                                      lt_messages[]
                                      lt_vlog[]
                                      lv_r3_sucs.
  ENDIF.

  IF lv_r3_sucs = abap_true.
    MESSAGE s000 WITH TEXT-037 TEXT-038.
*     and EXIT to Selection screen
    SET SCREEN 0. LEAVE SCREEN.
  ENDIF.

ENDFORM.  " REPEAT_ENRICHMENT_PROCESS_R3
*&---------------------------------------------------------------------*
*&      Form  BUILD_R3_PRODUCT_DATA
*&---------------------------------------------------------------------*
* Build R3 Product Data
*----------------------------------------------------------------------*
FORM build_r3_product_data  USING    pt_key_rmode  TYPE ztarn_key
                                     pt_ver_status TYPE ztarn_ver_status
                            CHANGING pt_prod_data  TYPE ztarn_prod_data
                                     pt_reg_data   TYPE ztarn_reg_data.

  DATA: ls_key_rmode     TYPE zsarn_key,
        ls_prod_data     TYPE zsarn_prod_data,
        ls_reg_data      TYPE zsarn_reg_data,
        ls_reg_hdr       TYPE zarn_reg_hdr,
        ls_basic_product TYPE zproduct_lean_search_respons22,
        ls_ver_status    TYPE zarn_ver_status.


  LOOP AT pt_key_rmode INTO ls_key_rmode.

* Get Version Status if Exist
    CLEAR ls_ver_status.
    READ TABLE pt_ver_status INTO ls_ver_status
    WITH KEY idno = ls_key_rmode-idno.

    CLEAR ls_prod_data.
    READ TABLE pt_prod_data INTO ls_prod_data
    WITH KEY idno    = ls_key_rmode-idno
             version = ls_key_rmode-version.
    IF ls_prod_data IS NOT INITIAL.
      CONTINUE.
    ENDIF.





    CLEAR: ls_prod_data, ls_basic_product.

    CLEAR ls_reg_data.
    READ TABLE pt_reg_data INTO ls_reg_data WITH KEY idno = ls_key_rmode-idno.
    IF sy-subrc = 0.

      CLEAR ls_reg_hdr.
      READ TABLE ls_reg_data-zarn_reg_hdr[] INTO ls_reg_hdr WITH KEY idno = ls_key_rmode-idno.
      IF sy-subrc = 0.
        ls_basic_product-creation_time       = ls_reg_hdr-creation_time_r1.
        ls_basic_product-catalog_version     = gc_staged.
        ls_basic_product-code                = ls_reg_hdr-idno.
        ls_basic_product-fsshort_description = ls_reg_hdr-fs_short_descr_upper.
        ls_basic_product-fsbrand-description = ls_reg_hdr-fs_brand_desc_upper.

* Build New Product Data
        PERFORM build_new_prod_data USING ls_basic_product
                                          ls_ver_status
                                 CHANGING ls_prod_data.

        APPEND ls_prod_data TO pt_prod_data[].
      ENDIF.  " READ TABLE ls_reg_data-zarn_reg_hdr[] INTO ls_reg_hdr
    ENDIF.  " READ TABLE pt_reg_data INTO ls_reg_data
  ENDLOOP.  " LOOP AT pt_key_rmode INTO ls_key_rmode

ENDFORM.  " BUILD_R3_PRODUCT_DATA
*&---------------------------------------------------------------------*
*&      Form  REG_ONNCAT_ALV
*&---------------------------------------------------------------------*
FORM reg_onncat_alv .


  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
    ls_fieldcat TYPE lvc_s_fcat,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo,
    ls_variant  TYPE disvariant,
    lt_filter   TYPE lvc_t_filt,
    ls_filter   TYPE lvc_s_filt.



  IF go_reg_con_onlcat_alv IS INITIAL.
*   Regional RRP
    CREATE OBJECT go_roccc08
      EXPORTING
        container_name = gc_roccc08.

* Create ALV grid
    CREATE OBJECT go_reg_con_onlcat_alv
      EXPORTING
        i_parent = go_roccc08.

    PERFORM alv_build_fieldcat_reg_onlcat USING gc_alv_reg_onlcat_ui
                                        CHANGING lt_fieldcat.


*  Exclude all edit functions in this example since we do not need them:
    PERFORM exclude_tb_functions_edit USING  gc_alv_reg_onlcat_ui CHANGING lt_exclude.




*   Variant management
    ls_variant-handle   = 'RCAT'.

    PERFORM alv_variant_default CHANGING ls_variant.

    CLEAR ls_layout.
    ls_layout-cwidth_opt = abap_true.
    ls_layout-sel_mode   = abap_true.
    ls_layout-zebra      = abap_true.



    CALL METHOD go_reg_con_onlcat_alv->set_table_for_first_display
      EXPORTING
        i_bypassing_buffer            = abap_true
        is_variant                    = ls_variant
        i_save                        = gc_save
        is_layout                     = ls_layout
        it_toolbar_excluding          = lt_exclude
      CHANGING
        it_outtab                     = gt_zarn_reg_onlcat
        it_fieldcatalog               = lt_fieldcat
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.

    IF sy-subrc IS NOT INITIAL.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.


* set editable cells to ready for input
    CALL METHOD go_reg_con_onlcat_alv->set_ready_for_input
      EXPORTING
        i_ready_for_input = 1.

    CALL METHOD go_reg_con_onlcat_alv->register_edit_event
      EXPORTING
        i_event_id = cl_gui_alv_grid=>mc_evt_enter.

    CALL METHOD go_reg_con_onlcat_alv->register_edit_event
      EXPORTING
        i_event_id = cl_gui_alv_grid=>mc_evt_modified.


    SET HANDLER go_event_receiver_edit->handle_data_changed FOR go_reg_con_onlcat_alv.
    SET HANDLER go_event_receiver_edit->handle_toolbar      FOR go_reg_con_onlcat_alv.
    SET HANDLER go_event_receiver_edit->handle_user_command FOR go_reg_con_onlcat_alv.

*-- Register F4 event
    PERFORM register_f4_event_onlcat_alv USING go_reg_con_onlcat_alv.

  ELSE.
    PERFORM refresh_reg_alv USING go_reg_con_onlcat_alv lt_fieldcat.
  ENDIF.





ENDFORM.  " REG_ONNCAT_ALV
*&---------------------------------------------------------------------*
*&      Form  ALV_BUILD_FIELDCAT_REG_ONLCAT
*&---------------------------------------------------------------------*
FORM alv_build_fieldcat_reg_onlcat USING pv_structure
                                CHANGING pt_fieldcat TYPE lvc_t_fcat.

  FIELD-SYMBOLS <ls_fcat> TYPE lvc_s_fcat.


  CALL FUNCTION 'LVC_FIELDCATALOG_MERGE'
    EXPORTING
      i_structure_name = pv_structure
    CHANGING
      ct_fieldcat      = pt_fieldcat.

  LOOP AT pt_fieldcat ASSIGNING <ls_fcat>.

    <ls_fcat>-col_opt = abap_true.


    CASE <ls_fcat>-fieldname.
      WHEN gc_idno.
        <ls_fcat>-no_out = abap_true.
        <ls_fcat>-edit   = abap_false.

      WHEN 'BUNIT'.
        <ls_fcat>-edit    = abap_false.
        <ls_fcat>-col_pos = '1'.

      WHEN 'CATALOG_TYPE'.
        <ls_fcat>-edit    = abap_false.
        <ls_fcat>-col_pos = '2'.

      WHEN 'SEQNO'.
        <ls_fcat>-edit    = abap_false.
        <ls_fcat>-col_pos = '3'.

      WHEN 'ONL_CATALOG'.
        <ls_fcat>-edit    = abap_false.
        <ls_fcat>-col_pos = '4'.


      WHEN 'CATEGORY_1'.
        <ls_fcat>-edit    = abap_true.
        <ls_fcat>-col_pos = '5'.
        <ls_fcat>-tooltip = <ls_fcat>-scrtext_l =  <ls_fcat>-reptext = 'Category 1'.
        <ls_fcat>-scrtext_s = 'Cat 1'.
        <ls_fcat>-scrtext_m = 'Category 1'.

      WHEN 'CAT_DESCRIPTION_1'.
        <ls_fcat>-edit    = abap_false.
        <ls_fcat>-col_pos = '6'.
        <ls_fcat>-tooltip = <ls_fcat>-scrtext_l =  <ls_fcat>-reptext = 'Category Description 1'.
        <ls_fcat>-scrtext_s = 'Cat Desc 1'.
        <ls_fcat>-scrtext_m = 'Cat Desc 1'.

      WHEN 'CATEGORY_2'.
        <ls_fcat>-edit    = abap_true.
        <ls_fcat>-col_pos = '7'.
        <ls_fcat>-tooltip = <ls_fcat>-scrtext_l =  <ls_fcat>-reptext = 'Category 2'.
        <ls_fcat>-scrtext_s = 'Cat 2'.
        <ls_fcat>-scrtext_m = 'Category 2'.

      WHEN 'CAT_DESCRIPTION_2'.
        <ls_fcat>-edit    = abap_false.
        <ls_fcat>-col_pos = '8'.
        <ls_fcat>-tooltip = <ls_fcat>-scrtext_l =  <ls_fcat>-reptext = 'Category Description 2'.
        <ls_fcat>-scrtext_s = 'Cat Desc 2'.
        <ls_fcat>-scrtext_m = 'Cat Desc 2'.

      WHEN 'CATEGORY_3'.
        <ls_fcat>-edit    = abap_true.
        <ls_fcat>-col_pos = '9'.
        <ls_fcat>-tooltip = <ls_fcat>-scrtext_l =  <ls_fcat>-reptext = 'Category 3'.
        <ls_fcat>-scrtext_s = 'Cat 3'.
        <ls_fcat>-scrtext_m = 'Category 3'.

      WHEN 'CAT_DESCRIPTION_3'.
        <ls_fcat>-edit    = abap_false.
        <ls_fcat>-col_pos = '10'.
        <ls_fcat>-tooltip = <ls_fcat>-scrtext_l =  <ls_fcat>-reptext = 'Category Description 3'.
        <ls_fcat>-scrtext_s = 'Cat Desc 3'.
        <ls_fcat>-scrtext_m = 'Cat Desc 3'.

      WHEN 'CATEGORY_4'.
        <ls_fcat>-edit    = abap_true.
        <ls_fcat>-col_pos = '11'.
        <ls_fcat>-no_out  = abap_true.
        <ls_fcat>-tooltip = <ls_fcat>-scrtext_l =  <ls_fcat>-reptext = 'Category 4'.
        <ls_fcat>-scrtext_s = 'Cat 4'.
        <ls_fcat>-scrtext_m = 'Category 4'.

      WHEN 'CAT_DESCRIPTION_4'.
        <ls_fcat>-edit    = abap_false.
        <ls_fcat>-col_pos = '12'.
        <ls_fcat>-no_out  = abap_true.
        <ls_fcat>-tooltip = <ls_fcat>-scrtext_l =  <ls_fcat>-reptext = 'Category Description 4'.
        <ls_fcat>-scrtext_s = 'Cat Desc 4'.
        <ls_fcat>-scrtext_m = 'Cat Desc 4'.

      WHEN 'CATEGORY_5'.
        <ls_fcat>-edit    = abap_true.
        <ls_fcat>-col_pos = '13'.
        <ls_fcat>-no_out  = abap_true.
        <ls_fcat>-tooltip = <ls_fcat>-scrtext_l =  <ls_fcat>-reptext = 'Category 5'.
        <ls_fcat>-scrtext_s = 'Cat 5'.
        <ls_fcat>-scrtext_m = 'Category 5'.

      WHEN 'CAT_DESCRIPTION_5'.
        <ls_fcat>-edit    = abap_false.
        <ls_fcat>-col_pos = '14'.
        <ls_fcat>-no_out  = abap_true.
        <ls_fcat>-tooltip = <ls_fcat>-scrtext_l =  <ls_fcat>-reptext = 'Category Description 5'.
        <ls_fcat>-scrtext_s = 'Cat Desc 5'.
        <ls_fcat>-scrtext_m = 'Cat Desc 5'.

      WHEN 'CATEGORY_6'.
        <ls_fcat>-edit    = abap_true.
        <ls_fcat>-col_pos = '15'.
        <ls_fcat>-no_out  = abap_true.
        <ls_fcat>-tooltip = <ls_fcat>-scrtext_l =  <ls_fcat>-reptext = 'Category 6'.
        <ls_fcat>-scrtext_s = 'Cat 6'.
        <ls_fcat>-scrtext_m = 'Category 6'.

      WHEN 'CAT_DESCRIPTION_6'.
        <ls_fcat>-edit    = abap_false.
        <ls_fcat>-col_pos = '16'.
        <ls_fcat>-no_out  = abap_true.
        <ls_fcat>-tooltip = <ls_fcat>-scrtext_l =  <ls_fcat>-reptext = 'Category Description 6'.
        <ls_fcat>-scrtext_s = 'Cat Desc 6'.
        <ls_fcat>-scrtext_m = 'Cat Desc 6'.

      WHEN OTHERS.
        DELETE pt_fieldcat.

    ENDCASE.
  ENDLOOP.

ENDFORM.  " ALV_BUILD_FIELDCAT_REG_ONLCAT
*&---------------------------------------------------------------------*
*&      Form  GET_ONLCAT_DESCRIPTIONS
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
FORM get_onlcat_descriptions CHANGING fct_zarn_reg_onlcat TYPE ztarn_reg_onlcat_ui.

  DATA: ls_onl_category LIKE LINE OF gt_onl_category.

  FIELD-SYMBOLS: <ls_zarn_reg_onlcat> LIKE LINE OF gt_zarn_reg_onlcat.

  LOOP AT fct_zarn_reg_onlcat ASSIGNING <ls_zarn_reg_onlcat>.


    CLEAR ls_onl_category.
    READ TABLE gt_onl_category[] INTO ls_onl_category
    WITH KEY onl_catalog = <ls_zarn_reg_onlcat>-onl_catalog
             onl_level   = '1'
             category    = <ls_zarn_reg_onlcat>-category_1.
    IF sy-subrc = 0.
      <ls_zarn_reg_onlcat>-cat_description_1 = ls_onl_category-cat_description.
    ENDIF.


    CLEAR ls_onl_category.
    READ TABLE gt_onl_category[] INTO ls_onl_category
    WITH KEY onl_catalog = <ls_zarn_reg_onlcat>-onl_catalog
             onl_level   = '2'
             category    = <ls_zarn_reg_onlcat>-category_2.
    IF sy-subrc = 0.
      <ls_zarn_reg_onlcat>-cat_description_2 = ls_onl_category-cat_description.
    ENDIF.


    CLEAR ls_onl_category.
    READ TABLE gt_onl_category[] INTO ls_onl_category
    WITH KEY onl_catalog = <ls_zarn_reg_onlcat>-onl_catalog
             onl_level   = '3'
             category    = <ls_zarn_reg_onlcat>-category_3.
    IF sy-subrc = 0.
      <ls_zarn_reg_onlcat>-cat_description_3 = ls_onl_category-cat_description.
    ENDIF.

    CLEAR ls_onl_category.
    READ TABLE gt_onl_category[] INTO ls_onl_category
    WITH KEY onl_catalog = <ls_zarn_reg_onlcat>-onl_catalog
             onl_level   = '4'
             category    = <ls_zarn_reg_onlcat>-category_4.
    IF sy-subrc = 0.
      <ls_zarn_reg_onlcat>-cat_description_4 = ls_onl_category-cat_description.
    ENDIF.


    CLEAR ls_onl_category.
    READ TABLE gt_onl_category[] INTO ls_onl_category
    WITH KEY onl_catalog = <ls_zarn_reg_onlcat>-onl_catalog
             onl_level   = '5'
             category    = <ls_zarn_reg_onlcat>-category_5.
    IF sy-subrc = 0.
      <ls_zarn_reg_onlcat>-cat_description_5 = ls_onl_category-cat_description.
    ENDIF.

    CLEAR ls_onl_category.
    READ TABLE gt_onl_category[] INTO ls_onl_category
    WITH KEY onl_catalog = <ls_zarn_reg_onlcat>-onl_catalog
             onl_level   = '6'
             category    = <ls_zarn_reg_onlcat>-category_6.
    IF sy-subrc = 0.
      <ls_zarn_reg_onlcat>-cat_description_6 = ls_onl_category-cat_description.
    ENDIF.

  ENDLOOP.

ENDFORM.  " GET_ONLCAT_DESCRIPTIONS
*&---------------------------------------------------------------------*
*&      Form  CHECK_LATEST_REGULAR_VENDOR
*&---------------------------------------------------------------------*
* Check if selectedregular vendor has the latest last changed date
*----------------------------------------------------------------------*
FORM check_latest_regular_vendor USING fu_s_mod_rows_pir TYPE zsarn_reg_pir_ui
                                       fu_s_reg_pir      TYPE zsarn_reg_pir_ui
                                       fu_t_zarn_reg_pir TYPE ztarn_reg_pir_ui
                              CHANGING fc_v_pir_error    TYPE flag.


  DATA: lt_zarn_reg_pir       TYPE ztarn_reg_pir_ui,
        ls_zarn_reg_pir       TYPE zsarn_reg_pir_ui,
        lv_relif_pir_chg_date TYPE zarn_pir_last_changed_dats.



  CLEAR: fc_v_pir_error.

  lt_zarn_reg_pir[] = fu_t_zarn_reg_pir[].


* Step1.  System will select the “PIR Change date” for the PIR record marked as Regular vendor.
  CLEAR: ls_zarn_reg_pir, lv_relif_pir_chg_date.
  READ TABLE lt_zarn_reg_pir[] INTO ls_zarn_reg_pir
  WITH KEY idno         = fu_s_mod_rows_pir-idno
           lifnr        = fu_s_mod_rows_pir-lifnr
           pir_rel_eina = abap_true.
  IF sy-subrc EQ 0.
    lv_relif_pir_chg_date = ls_zarn_reg_pir-pir_last_changed_date.
    CLEAR: ls_zarn_reg_pir.
  ELSE.
    RETURN.
  ENDIF.



  DELETE lt_zarn_reg_pir WHERE pir_rel_eina IS INITIAL.
  IF lt_zarn_reg_pir[] IS INITIAL.
    fc_v_pir_error = abap_true.
  ELSE.

* Step2. Then system will select the “PIR Change date” from all the other PIR records marked as “EINA Flag”.
* Get Latest PIR Chaned date where EINA is ticked
    SORT lt_zarn_reg_pir[] BY pir_last_changed_date DESCENDING.
    CLEAR ls_zarn_reg_pir.
    ls_zarn_reg_pir = lt_zarn_reg_pir[ 1 ].

* Step3. Compare the date in step1 and the dates found in Step2.
* If the date in Step1 is not the latest date then system will show the confirmation popup
    IF lv_relif_pir_chg_date LT ls_zarn_reg_pir-pir_last_changed_date.
      fc_v_pir_error = abap_true.
    ENDIF.

  ENDIF.  " IF lt_zarn_reg_pir[] IS INITIAL


ENDFORM.  " CHECK_LATEST_REGULAR_VENDOR
*&---------------------------------------------------------------------*
*&      Form  CHECK_CONDITION_GROUP
*&---------------------------------------------------------------------*
* Check Condition Group
*----------------------------------------------------------------------*
FORM check_condition_group USING fu_s_mod_rows_pir TYPE zsarn_reg_pir_ui
                        CHANGING fc_v_pir_error    TYPE flag.


  DATA: lt_a718 TYPE STANDARD TABLE OF a718.




  CLEAR: fc_v_pir_error.

  IF fu_s_mod_rows_pir-lifnr IS INITIAL OR fu_s_mod_rows_pir-lifnr EQ 'MULTIPLE'.
    RETURN.
  ENDIF.


* Get Vendor/Purch.Org./Cond. Grp
  SELECT *
    FROM a718
    INTO CORRESPONDING FIELDS OF TABLE lt_a718
   WHERE lifnr = fu_s_mod_rows_pir-lifnr.
*     AND ekorg = '9999'.
  IF sy-subrc NE 0.
    fc_v_pir_error = abap_true.
    RETURN.
  ENDIF.

  DELETE lt_a718 WHERE ekkol IS INITIAL.
  IF lt_a718[] IS INITIAL.
    fc_v_pir_error = abap_true.
    RETURN.
  ENDIF.


  READ TABLE lt_a718 TRANSPORTING NO FIELDS
  WITH KEY ekkol = fu_s_mod_rows_pir-ekkol.
  IF sy-subrc NE 0.
    fc_v_pir_error = abap_true.
  ENDIF.

ENDFORM.  " CHECK_CONDITION_GROUP
*&---------------------------------------------------------------------*
*&      Form  DISPLAY_STOCK_POPUP
*&---------------------------------------------------------------------*
* Get Open PO/SO and display Stock Report
*----------------------------------------------------------------------*
FORM display_stock_popup.

  DATA ls_old_issue_uom LIKE LINE OF gt_zmd_host_sap_itab.
  DATA ls_host_sap      LIKE LINE OF gt_zmd_host_sap_itab.
  DATA ls_open_po       TYPE ty_open_po.
  DATA ls_open_so       TYPE ty_open_so.
  DATA ls_open_po_so    TYPE ty_open_po_so.
  DATA lt_open_po_so    TYPE TABLE OF ty_open_po_so.
  DATA lv_issue_uom     TYPE meinh.
  DATA lv_host_iss_uom  TYPE meinh.
  DATA lv_hostprd_found TYPE boole_d.
  DATA lv_noof_hostprd  TYPE i.

  CLEAR: gt_open_po, gt_open_po_disp, gt_open_so, gt_open_so_disp, gt_open_po_so_disp,
         gt_open_stock.
  REFRESH: gt_open_po[], gt_open_po_disp[], gt_open_so[], gt_open_so_disp[], gt_open_po_so_disp[],
           gt_open_stock[].

  READ TABLE gt_zmd_host_sap_itab INTO ls_old_issue_uom WITH KEY latest = abap_true.
  IF sy-subrc EQ 0.

    " Get open POs
    PERFORM get_open_po_lines USING ls_old_issue_uom-matnr
                              CHANGING gt_open_po.

    " Get open SOs
    PERFORM get_open_so_lines USING ls_old_issue_uom-matnr
                              CHANGING gt_open_so.

*      " Prepare global table for used POs/SOs to be displayed in popup
    PERFORM set_open_po_so_table USING ls_old_issue_uom.

    " Get Open WMS stock
    PERFORM get_stock_for_wms_product USING  ls_old_issue_uom-product
                                      CHANGING gt_open_stock.

*      IF gt_open_po_so_disp IS NOT INITIAL OR gt_open_stock IS NOT INITIAL.
    IF gt_open_stock IS NOT INITIAL.
      gv_revert_issue_uom = abap_true.
      " show popup
      CALL SCREEN 9001 STARTING AT 18 15.
    ENDIF.

  ENDIF. " READ TABLE gt_zmd_host_sap_itab for latest product

ENDFORM.  " DISPLAY_STOCK_POPUP
*&---------------------------------------------------------------------*
*&      Form  TEXT_DESCR_PRFAM
*&---------------------------------------------------------------------*
* text editor for Descriptions
*----------------------------------------------------------------------*
FORM text_descr_prfam .

* Create text editor Object - Cust Short Descr - Nat
  PERFORM create_text_editor USING space " read only
                                   gc_rhtxt03
                                   cl_gui_textedit=>wordwrap_at_windowborder " wordwrap mode
                                   290                                       " max characters
                                   0                                         " statusbar mode
                                   0                                         " toolbar mode
                          CHANGING go_reg_editor_desc_n_sh
                                   go_rhtxt03.

* Create text editor Object - Cust Med Descr - Nat
  PERFORM create_text_editor USING space " read only
                                   gc_rhtxt04
                                   cl_gui_textedit=>wordwrap_at_windowborder " wordwrap mode
                                   space                                     " max characters
                                   0                                         " statusbar mode
                                   0                                         " toolbar mode
                          CHANGING go_reg_editor_desc_n_md
                                   go_rhtxt04.

* Create text editor Object - Cust Long Descr - Nat
  PERFORM create_text_editor USING space " read only
                                   gc_rhtxt05
                                   cl_gui_textedit=>wordwrap_at_windowborder " wordwrap mode
                                   space                                     " max characters
                                   0                                         " statusbar mode
                                   0                                         " toolbar mode
                          CHANGING go_reg_editor_desc_n_lg
                                   go_rhtxt05.

* Create text editor Object - Cust Short Descr - Reg
  PERFORM create_text_editor USING gv_edit_toggle
                                   gc_rhtxt06
                                   cl_gui_textedit=>wordwrap_at_windowborder " wordwrap mode
                                   290                                       " max characters
                                   0                                         " statusbar mode
                                   0                                         " toolbar mode
                          CHANGING go_reg_editor_desc_r_sh
                                   go_rhtxt06.

* Create text editor Object - Cust Med Descr - Reg
  PERFORM create_text_editor USING gv_edit_toggle
                                   gc_rhtxt07
                                   cl_gui_textedit=>wordwrap_at_windowborder " wordwrap mode
                                   space                                     " max characters
                                   0                                         " statusbar mode
                                   0                                         " toolbar mode
                          CHANGING go_reg_editor_desc_r_md
                                   go_rhtxt07.

* Create text editor Object - Cust Long Descr - Reg
  PERFORM create_text_editor USING gv_edit_toggle
                                   gc_rhtxt08
                                   cl_gui_textedit=>wordwrap_at_windowborder " wordwrap mode
                                   space                                     " max characters
                                   0                                         " statusbar mode
                                   0                                         " toolbar mode
                          CHANGING go_reg_editor_desc_r_lg
                                   go_rhtxt08.


* Create text editor Object - Ingredient Statement - Nat
  PERFORM create_text_editor USING space " read only
                                   gc_rhtxt09
                                   cl_gui_textedit=>wordwrap_at_windowborder " wordwrap mode
                                   space                                     " max characters
                                   0                                         " statusbar mode
                                   0                                         " toolbar mode
                          CHANGING go_reg_editor_ingr_stmt_n
                                   go_rhtxt09.

  " Regional Ingredients Statement
  PERFORM create_text_editor USING space " read only
                                   gc_reg_ingr_stmt
                                   cl_gui_textedit=>wordwrap_at_windowborder " wordwrap mode
                                   space                                     " max characters
                                   0                                         " statusbar mode
                                   0                                         " toolbar mode
                          CHANGING go_reg_editor_ingr_stmt_reg
                                   go_reg_ingr_stmt_container.

* Ingredient Statement - Regional override
  PERFORM create_text_editor USING gv_edit_toggle
                                   gc_rhtxt10
                                   cl_gui_textedit=>wordwrap_at_windowborder " wordwrap mode
                                   space                                     " max characters
                                   0                                         " statusbar mode
                                   0                                         " toolbar mode
                          CHANGING go_reg_editor_ingr_stmt_r
                                   go_rhtxt10.

* Create text editor Object - Range Detail - Reg
  PERFORM create_text_editor USING gv_edit_toggle
                                   gc_rhtxt11
                                   cl_gui_textedit=>wordwrap_at_windowborder " wordwrap mode
                                   space                                     " max characters
                                   0                                         " statusbar mode
                                   0                                         " toolbar mode
                          CHANGING go_reg_editor_range_det_r
                                   go_rhtxt11.

ENDFORM.  " TEXT_DESCR_PRFAM
*&---------------------------------------------------------------------*
*&      Form  CREATE_TEXT_EDITOR
*&---------------------------------------------------------------------*
* Create text editor Object
*----------------------------------------------------------------------*
FORM create_text_editor USING fu_v_edit_toggle TYPE int4
                              fu_v_cc_name     TYPE scrfname
                              fu_v_wordwrap    TYPE i
                              fu_v_max_char    TYPE i
                              fu_v_statusbar   TYPE i
                              fu_v_toolbar     TYPE i
                     CHANGING fc_o_text_editor TYPE REF TO cl_gui_textedit
                              fc_o_container   TYPE REF TO cl_gui_custom_container.

  DATA:
    lv_mode           TYPE i,
    lv_wordwrap_pos   TYPE i,
    lv_wrap_linebreak TYPE i VALUE 1,
    lv_wordwrap_mode  TYPE i.

  lv_wordwrap_mode =  lv_wordwrap_mode.

  IF fu_v_cc_name = gc_rhtxt08.
    lv_wrap_linebreak = 0.
    lv_wordwrap_mode = 0.
  ENDIF.

  IF fc_o_text_editor IS INITIAL.

    CREATE OBJECT fc_o_container
      EXPORTING
        container_name = fu_v_cc_name.

*   create calls constructor, which initializes, creats and links
*   a TextEdit Control
    CREATE OBJECT fc_o_text_editor
      EXPORTING
        style                      = 0
        parent                     = fc_o_container
        wordwrap_mode              = lv_wordwrap_mode "fu_v_wordwrap
        max_number_chars           = fu_v_max_char
**        wordwrap_position        = lv_wordwrap_pos
        wordwrap_to_linebreak_mode = lv_wrap_linebreak "1
      EXCEPTIONS
        OTHERS                     = 1.

    IF sy-subrc NE 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.
*   remove status bar for space !
    CALL METHOD fc_o_text_editor->set_statusbar_mode
      EXPORTING
        statusbar_mode         = fu_v_statusbar
      EXCEPTIONS
        error_cntl_call_method = 1
        invalid_parameter      = 2
        OTHERS                 = 3.

*   remove toolbar - as per users request !
    CALL METHOD fc_o_text_editor->set_toolbar_mode
      EXPORTING
        toolbar_mode           = fu_v_toolbar
      EXCEPTIONS
        error_cntl_call_method = 1
        invalid_parameter      = 2
        OTHERS                 = 3.

  ELSE.
*   opposite of ALV grid edit mode :-(
    IF fu_v_edit_toggle IS NOT INITIAL.
      CLEAR lv_mode.
    ELSE.
      ADD 1 TO lv_mode.
    ENDIF.
*   toggle edit/display
    CALL METHOD fc_o_text_editor->set_readonly_mode
      EXPORTING
        readonly_mode = lv_mode.
  ENDIF.



ENDFORM.  " CREATE_TEXT_EDITOR
*&---------------------------------------------------------------------*
*&      Form  SET_TEXT_EDITOR_DESCR
*&---------------------------------------------------------------------*
* Set Descriptions in text editors
*----------------------------------------------------------------------*
FORM set_text_editor_descr.


* Cust Short Descr - Nat
  CALL METHOD go_reg_editor_desc_n_sh->set_textstream
    EXPORTING
      text   = CONV #( zarn_products_ex-fs_cust_short_descr )
    EXCEPTIONS
      OTHERS = 1.
  IF sy-subrc NE 0.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
               WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.


* Cust Med Descr - Nat
  CALL METHOD go_reg_editor_desc_n_md->set_textstream
    EXPORTING
      text   = gs_prod_str-fs_cust_medium_descr
    EXCEPTIONS
      OTHERS = 1.
  IF sy-subrc NE 0.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
               WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.

* Cust Long Descr - Nat
  CALL METHOD go_reg_editor_desc_n_lg->set_textstream
    EXPORTING
      text   = gs_prod_str-fs_cust_long_descr
    EXCEPTIONS
      OTHERS = 1.
  IF sy-subrc NE 0.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
               WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.


* Cust Short Descr - Reg
  CALL METHOD go_reg_editor_desc_r_sh->set_textstream
    EXPORTING
      text   = CONV #( zarn_reg_hdr-fs_cust_short_descr )
    EXCEPTIONS
      OTHERS = 1.
  IF sy-subrc NE 0.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
               WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.


* Cust Med Descr - Reg
  CALL METHOD go_reg_editor_desc_r_md->set_textstream
    EXPORTING
      text   = gs_reg_str-fs_cust_medium_descr
    EXCEPTIONS
      OTHERS = 1.
  IF sy-subrc NE 0.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
               WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.


* Cust Long Descr - Reg
  CALL METHOD go_reg_editor_desc_r_lg->set_textstream
    EXPORTING
      text   = gs_reg_str-fs_cust_long_descr
    EXCEPTIONS
      OTHERS = 1.
  IF sy-subrc NE 0.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
               WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.


* Ingredient Statement - National
  CALL METHOD go_reg_editor_ingr_stmt_n->set_textstream
    EXPORTING
      text   = gs_prod_str-ingredient_statement
    EXCEPTIONS
      OTHERS = 1.
  IF sy-subrc NE 0.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
               WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.

* Ingredient Statement - Regional
  go_reg_editor_ingr_stmt_reg->set_textstream(
    EXPORTING
      text   = gs_reg_str-reg_ingredient_statement
    EXCEPTIONS
      OTHERS = 1 ).
  IF sy-subrc NE 0.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
               WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.

* Ingredient Statement - Regional override
  CALL METHOD go_reg_editor_ingr_stmt_r->set_textstream
    EXPORTING
      text   = gs_reg_str-ingredient_statement
    EXCEPTIONS
      OTHERS = 1.
  IF sy-subrc NE 0.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
               WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.

* Range Detail - Reg
  CALL METHOD go_reg_editor_range_det_r->set_textstream
    EXPORTING
      text   = gs_reg_str-range_detail
    EXCEPTIONS
      OTHERS = 1.
  IF sy-subrc NE 0.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
               WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.

ENDFORM.  " SET_TEXT_EDITOR_DESCR
*&---------------------------------------------------------------------*
*&      Form  GET_TEXT_EDITOR_DESCR
*&---------------------------------------------------------------------*
* Get Descriptions from text editors
*----------------------------------------------------------------------*
FORM get_text_editor_descr CHANGING fc_t_reg_hdr TYPE ztarn_reg_hdr
                                    fc_t_reg_str TYPE ztarn_reg_str.

  DATA: lv_string   TYPE string,
        lv_modified TYPE i,
        ls_reg_str  TYPE zarn_reg_str.

  FIELD-SYMBOLS: <ls_reg_hdr> TYPE zarn_reg_hdr,
                 <ls_reg_str> TYPE zarn_reg_str.

  ASSIGN fc_t_reg_hdr[ 1 ] TO <ls_reg_hdr>.
  ASSIGN fc_t_reg_str[ 1 ] TO <ls_reg_str>.
  IF <ls_reg_str> IS NOT ASSIGNED.
    ls_reg_str-mandt = sy-mandt.
    ls_reg_str-idno  = zarn_reg_hdr-idno.
    APPEND ls_reg_str TO fc_t_reg_str[] ASSIGNING <ls_reg_str>.
  ENDIF.

  IF <ls_reg_hdr> IS ASSIGNED.
* Cust Short Descr - Reg
    PERFORM get_string_from_text_edit USING go_reg_editor_desc_r_sh
                                   CHANGING <ls_reg_hdr>-fs_cust_short_descr.
  ENDIF.

  IF <ls_reg_str> IS ASSIGNED.

* Cust Medium Descr - Reg
    PERFORM get_string_from_text_edit USING go_reg_editor_desc_r_md
                                    CHANGING <ls_reg_str>-fs_cust_medium_descr.
* DSDSS-2385 - ensure no CR characters are embedded
    REPLACE ALL OCCURRENCES OF cl_abap_char_utilities=>cr_lf   IN <ls_reg_str>-fs_cust_medium_descr WITH ''.


* Cust Long Descr - Reg
    PERFORM get_string_from_text_edit USING go_reg_editor_desc_r_lg
                                    CHANGING <ls_reg_str>-fs_cust_long_descr.

* Ingredient Statement - Reg
    PERFORM get_string_from_text_edit USING go_reg_editor_ingr_stmt_r
                                     CHANGING <ls_reg_str>-ingredient_statement.

* Range Detail - Reg
    PERFORM get_string_from_text_edit USING go_reg_editor_range_det_r
                                     CHANGING <ls_reg_str>-range_detail.

  ENDIF.

  " Delete the record if none of the descriptions populated. Otherwise, it would give incorrect message
  " saying something changed even though nothing changed.
  IF <ls_reg_str>-fs_cust_medium_descr      IS INITIAL AND
     <ls_reg_str>-fs_cust_long_descr        IS INITIAL AND
     <ls_reg_str>-ingredient_statement      IS INITIAL AND
     <ls_reg_str>-range_detail              IS INITIAL AND
     <ls_reg_str>-reg_ingredient_statement  IS INITIAL.
    DELETE fc_t_reg_str INDEX 1.
  ENDIF.

ENDFORM.

FORM get_string_from_text_edit USING io_textedit TYPE REF TO cl_gui_textedit
                              CHANGING cv_string.

  DATA: "lv_string   TYPE string,
    lv_modified       TYPE i.

  "gv_string has to be declared as global variable because if declared
  "as local, program is short dumping, saying:

  "SYSTEM_POINTER_PENDING:  Local result variable used in a control/automation call
  "Local variables cannot be used to add a return value in a call to a
  "control or an automation server...

  cl_gui_cfw=>flush( ).

  CLEAR: gv_string, lv_modified.
  CALL METHOD io_textedit->get_textstream
    EXPORTING
      only_when_modified = 1
    IMPORTING
      text               = gv_string
      is_modified        = lv_modified
    EXCEPTIONS
      OTHERS             = 1.

  "after get_textstream the GV_STRING is still empty
  "however after FLUSH the value will appear magically in GV_STRING :-)

  cl_gui_cfw=>flush( ).

  "this was inspired by the code of GET_TEXT_AS_STREAM of the same class

  IF lv_modified NE 0.
    cv_string = gv_string.
  ENDIF.

ENDFORM.

*&---------------------------------------------------------------------*
*&      Form  CREATE_ZS_ZG_EAN_111
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
FORM create_zs_zg_ean_111 .


  DATA: ls_zarn_reg_ean TYPE zsarn_reg_ean_ui,
        lv_ean11        TYPE ean11,

        lt_zs_zg_ean    TYPE ztarn_reg_ean_ui,
        ls_zs_zg_ean    TYPE zsarn_reg_ean_ui.


  CONSTANTS: lc_sales_org  TYPE vkorg  VALUE '1000',
             lc_prefix_brc TYPE char02 VALUE '02',
             lc_prefix_sng TYPE char03 VALUE '020',
             lc_filler_brc TYPE char05 VALUE '00000',

             lc_eantp_brc  TYPE numtp VALUE 'ZS',
             lc_eantp_sng  TYPE numtp VALUE 'ZG',
             lc_eantp_plu  TYPE numtp VALUE 'ZP'.

  CLEAR: lt_zs_zg_ean[].

*  IF zarn_reg_hdr-scagr IS NOT INITIAL.            "--CI18-124 JKH 06.07.2017
  IF zarn_reg_hdr-scagr       EQ 'YES' AND          "++CI18-124 JKH 06.07.2017
     zarn_reg_hdr-plu_article IS NOT INITIAL.       "++CI18-124 JKH 06.07.2017
    LOOP AT gt_zarn_reg_ean[] INTO ls_zarn_reg_ean
      WHERE eantp = lc_eantp_plu.  " 'ZP'.

      lv_ean11 = ls_zarn_reg_ean-ean11.


      " add ZS
      CLEAR ls_zs_zg_ean.
      ls_zs_zg_ean = ls_zarn_reg_ean.

*           automatically add the corresponding ZS barcode
      ls_zs_zg_ean-eantp = lc_eantp_brc. "'ZS'.
*           ZS barcode format = 02 + 5-digit ZP PLU they have entered + 00000 + C (check digit)
      ls_zs_zg_ean-ean11 = lc_prefix_brc && lv_ean11(5) && lc_filler_brc.
      PERFORM convert_ean_with_checkdigit   USING ls_zs_zg_ean-eantp
                                         CHANGING  ls_zs_zg_ean-ean11.
      PERFORM convert_ean_with_eantp USING ls_zs_zg_ean-eantp CHANGING ls_zs_zg_ean-ean11.

      READ TABLE gt_zarn_reg_ean TRANSPORTING NO FIELDS
      WITH KEY idno  = ls_zs_zg_ean-idno
               ean11 = ls_zs_zg_ean-ean11.
      IF sy-subrc NE 0.
*           add to table
        APPEND ls_zs_zg_ean TO gt_zarn_reg_ean.
      ENDIF.



      " add ZG
      CLEAR ls_zs_zg_ean.
      ls_zs_zg_ean = ls_zarn_reg_ean.

*           New barcode type for Shop N Go = ZG
      ls_zs_zg_ean-eantp = lc_eantp_sng. " 'ZG'.
*           Format (9-digits) = 020 + 5 digit PLU + Check Digit
      ls_zs_zg_ean-ean11 = lc_prefix_sng && lv_ean11(5).
      PERFORM convert_ean_with_checkdigit  USING ls_zs_zg_ean-eantp
                                        CHANGING ls_zs_zg_ean-ean11.
      PERFORM convert_ean_with_eantp USING ls_zs_zg_ean-eantp CHANGING ls_zs_zg_ean-ean11.

      READ TABLE gt_zarn_reg_ean TRANSPORTING NO FIELDS
      WITH KEY idno  = ls_zs_zg_ean-idno
               ean11 = ls_zs_zg_ean-ean11.
      IF sy-subrc NE 0.
*           add to table
        APPEND ls_zs_zg_ean TO gt_zarn_reg_ean.
      ENDIF.


    ENDLOOP.
  ENDIF.  " IF zarn_reg_hdr-scagr = 'YES'.



ENDFORM.
*//START insert Charm 9000003206
FORM set_days_111.


  IF zarn_reg_hdr-zzbbdays_bypass EQ abap_true.
    CLEAR: zarn_reg_hdr-zzsell, zarn_reg_hdr-zzuse.
  ELSE.
    READ TABLE gt_zarn_reg_hdr ASSIGNING FIELD-SYMBOL(<ls_reg_hdr>) WITH KEY idno     = zarn_reg_hdr-idno
                                                                             matnr    = zarn_reg_hdr-matnr.
    IF sy-subrc EQ 0.
      zarn_reg_hdr-zzsell = <ls_reg_hdr>-zzsell.
      zarn_reg_hdr-zzuse  = <ls_reg_hdr>-zzuse .
    ENDIF.
  ENDIF.

ENDFORM.
*\\END insert Charm 9000003206
*&---------------------------------------------------------------------*
*&      Form  REG_ALLERG_OVR_ALV
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM reg_allerg_ovr_alv .
  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
    ls_fieldcat TYPE lvc_s_fcat,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo,
    ls_variant  TYPE disvariant,
    lt_filter   TYPE lvc_t_filt,
    ls_filter   TYPE lvc_s_filt.

  IF go_reg_allerg_alv IS INITIAL.
*   Regional Allergen & Ingredient override
    CREATE OBJECT go_rconcc07
      EXPORTING
        container_name = gc_rconcc07.

* Create ALV grid
    CREATE OBJECT go_reg_allerg_alv
      EXPORTING
        i_parent = go_rconcc07.

    PERFORM alv_build_fieldcat USING gc_alv_reg_allerg_ui  CHANGING lt_fieldcat.
    PERFORM enrich_reg_allerg_ovr_fieldcat CHANGING lt_fieldcat.

*  Exclude all edit functions in this example since we do not need them:
    PERFORM exclude_tb_functions_edit USING  gc_alv_reg_allerg_ui CHANGING lt_exclude.

*   Variant management
    ls_variant-handle   = 'AOVR'.

    PERFORM alv_variant_default CHANGING ls_variant.

    CLEAR ls_layout.
    ls_layout-cwidth_opt = abap_false.
    ls_layout-sel_mode   = abap_true.
    ls_layout-zebra      = abap_true.

    CALL METHOD go_reg_allerg_alv->set_table_for_first_display
      EXPORTING
        i_bypassing_buffer            = abap_true
        is_variant                    = ls_variant
        i_save                        = gc_save
        is_layout                     = ls_layout
        it_toolbar_excluding          = lt_exclude
      CHANGING
        it_outtab                     = gt_zarn_reg_allerg
        it_fieldcatalog               = lt_fieldcat
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.

    IF sy-subrc IS NOT INITIAL.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

* set editable cells to ready for input
    CALL METHOD go_reg_allerg_alv->set_ready_for_input
      EXPORTING
        i_ready_for_input = 1.

    CALL METHOD go_reg_allerg_alv->register_edit_event
      EXPORTING
        i_event_id = cl_gui_alv_grid=>mc_evt_enter.

    CALL METHOD go_reg_allerg_alv->register_edit_event
      EXPORTING
        i_event_id = cl_gui_alv_grid=>mc_evt_modified.

    DATA(lo_reg_allergen_ui) = lcl_reg_allergen_ui=>get_instance( ).
    SET HANDLER lo_reg_allergen_ui->handle_data_changed FOR go_reg_allerg_alv.

  ELSE.
    PERFORM refresh_reg_alv USING go_reg_allerg_alv lt_fieldcat.
  ENDIF.

ENDFORM.

FORM reg_online_alv .
  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo,
    ls_variant  TYPE disvariant.

  DATA: ls_cellcolor TYPE lvc_s_scol. "required for color

**  PERFORM populate_banner_descriptions.

  IF go_reg_onl_alv  IS INITIAL.
*   Regional Banner
    CREATE OBJECT go_ronl01
      EXPORTING
        container_name = gc_ronlcc01.

* Create ALV grid
    CREATE OBJECT go_reg_onl_alv
      EXPORTING
        i_parent = go_ronl01.

    PERFORM alv_build_fieldcat USING  gc_alv_reg_online CHANGING lt_fieldcat.

*   Exclude all edit functions in this example since we do not need them:
    PERFORM exclude_tb_functions_edit USING  gc_alv_reg_online CHANGING lt_exclude.

* Color:
* 1 Blue
* 2 Purple
* 3 Yellow
* 4 Lime green
* 5 Green
* 6 Red
* 7 Orange

    IF gt_zarn_online_desc IS INITIAL.
      APPEND INITIAL LINE TO gt_zarn_online_desc ASSIGNING FIELD-SYMBOL(<ls_onl_desc>). <ls_onl_desc>-fielddesc = 'Brand'.
      ls_cellcolor-fname = 'FIELDDESC'.
      ls_cellcolor-color-col = '3'.
      ls_cellcolor-color-int = '1'.
      ls_cellcolor-color-inv = '0'.
      APPEND ls_cellcolor TO <ls_onl_desc>-cellcolor.
      APPEND INITIAL LINE TO gt_zarn_online_desc ASSIGNING <ls_onl_desc>. <ls_onl_desc>-fielddesc = ''.
      APPEND ls_cellcolor TO <ls_onl_desc>-cellcolor.

      APPEND INITIAL LINE TO gt_zarn_online_desc ASSIGNING <ls_onl_desc>. <ls_onl_desc>-fielddesc = 'Short Description'.
      ls_cellcolor-color-col = '2'.
      APPEND ls_cellcolor TO <ls_onl_desc>-cellcolor.
      APPEND INITIAL LINE TO gt_zarn_online_desc ASSIGNING <ls_onl_desc>. <ls_onl_desc>-fielddesc = ''.
      APPEND ls_cellcolor TO <ls_onl_desc>-cellcolor.

      APPEND INITIAL LINE TO gt_zarn_online_desc ASSIGNING <ls_onl_desc>. <ls_onl_desc>-fielddesc = 'Medium Description'.
      ls_cellcolor-color-col = '1'.   "Blue
      APPEND ls_cellcolor TO <ls_onl_desc>-cellcolor.
      APPEND INITIAL LINE TO gt_zarn_online_desc ASSIGNING <ls_onl_desc>. <ls_onl_desc>-fielddesc = ''.
      APPEND ls_cellcolor TO <ls_onl_desc>-cellcolor.

      APPEND INITIAL LINE TO gt_zarn_online_desc ASSIGNING <ls_onl_desc>. <ls_onl_desc>-fielddesc = 'Size & Units (use "pk" for multi-packs)'.
      ls_cellcolor-color-col = '2'.
      APPEND ls_cellcolor TO <ls_onl_desc>-cellcolor.
      APPEND INITIAL LINE TO gt_zarn_online_desc ASSIGNING <ls_onl_desc>. <ls_onl_desc>-fielddesc = 'Multipack Text (only for multi-packs)'.
      ls_cellcolor-color-col = '6'.
      APPEND ls_cellcolor TO <ls_onl_desc>-cellcolor.
      APPEND INITIAL LINE TO gt_zarn_online_desc ASSIGNING <ls_onl_desc>. <ls_onl_desc>-fielddesc = ''.
      APPEND ls_cellcolor TO <ls_onl_desc>-cellcolor.

      APPEND INITIAL LINE TO gt_zarn_online_desc ASSIGNING <ls_onl_desc>. <ls_onl_desc>-fielddesc = 'Marketing Text'.
      ls_cellcolor-color-col = '7'.
      APPEND ls_cellcolor TO <ls_onl_desc>-cellcolor.
      APPEND INITIAL LINE TO gt_zarn_online_desc ASSIGNING <ls_onl_desc>. <ls_onl_desc>-fielddesc = ''.
      APPEND ls_cellcolor TO <ls_onl_desc>-cellcolor.
      APPEND INITIAL LINE TO gt_zarn_online_desc ASSIGNING <ls_onl_desc>. <ls_onl_desc>-fielddesc = ''.
      APPEND ls_cellcolor TO <ls_onl_desc>-cellcolor.
      APPEND INITIAL LINE TO gt_zarn_online_desc ASSIGNING <ls_onl_desc>. <ls_onl_desc>-fielddesc = ''.
      APPEND ls_cellcolor TO <ls_onl_desc>-cellcolor.
      APPEND INITIAL LINE TO gt_zarn_online_desc ASSIGNING <ls_onl_desc>. <ls_onl_desc>-fielddesc = ''.
      APPEND ls_cellcolor TO <ls_onl_desc>-cellcolor.
      APPEND INITIAL LINE TO gt_zarn_online_desc ASSIGNING <ls_onl_desc>. <ls_onl_desc>-fielddesc = ''.
      APPEND ls_cellcolor TO <ls_onl_desc>-cellcolor.

      APPEND INITIAL LINE TO gt_zarn_online_desc ASSIGNING <ls_onl_desc>. <ls_onl_desc>-fielddesc = 'Height'.
      ls_cellcolor-color-col = '0'.
      APPEND ls_cellcolor TO <ls_onl_desc>-cellcolor.
      APPEND INITIAL LINE TO gt_zarn_online_desc ASSIGNING <ls_onl_desc>. <ls_onl_desc>-fielddesc = ''.
      APPEND ls_cellcolor TO <ls_onl_desc>-cellcolor.

    ENDIF.

***   Variant management
**    ls_variant-handle   = 'RBAN'.
**    PERFORM alv_variant_default CHANGING ls_variant.

    ls_layout-no_headers = abap_true.
    ls_layout-edit       = abap_false.
    ls_layout-cwidth_opt = abap_false.
    ls_layout-sel_mode   = ''.
    ls_layout-no_hgridln = abap_true.
    ls_layout-no_rowmark = abap_true.
    ls_layout-zebra      = abap_false.
    ls_layout-ctab_fname = 'CELLCOLOR'.

    DATA: ls_rowno TYPE lvc_s_roid.
    ls_rowno-row_id = 15.

    CALL METHOD go_reg_onl_alv->set_table_for_first_display
      EXPORTING
        is_variant                    = ls_variant
        i_save                        = gc_save
        is_layout                     = ls_layout
        it_toolbar_excluding          = lt_exclude
      CHANGING
        it_outtab                     = gt_zarn_online_desc
        it_fieldcatalog               = lt_fieldcat
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.

    IF sy-subrc IS NOT INITIAL.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

    go_reg_onl_alv->set_current_cell_via_id(
      EXPORTING
*        is_row_id    =  1   " Row
*        is_column_id =  1   " Column
        is_row_no    = ls_rowno  " Numeric Row ID
    ).

  ELSE.
    PERFORM refresh_reg_alv USING go_reg_onl_alv lt_fieldcat.
  ENDIF.
ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  NAT_NET_QTY_ALV
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM nat_net_qty_alv .

  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo.

  IF go_nat_net_qty_alv IS INITIAL.
* Net quantity
    CREATE OBJECT go_nncc19
      EXPORTING
        container_name = gc_nncc19.

* Create ALV grid
    CREATE OBJECT go_nat_net_qty_alv
      EXPORTING
        i_parent = go_nncc19.


*Call GRID
    PERFORM alv_build_fieldcat USING gc_alv_nat_net_qty  CHANGING lt_fieldcat.
** Exclude all edit functions in this example since we do not need them:
    PERFORM exclude_tb_functions_display CHANGING lt_exclude.

    ls_layout-cwidth_opt = abap_true.
    ls_layout-zebra = abap_true.
    ls_layout-no_toolbar = abap_true.


    CALL METHOD go_nat_net_qty_alv->set_table_for_first_display
      EXPORTING
        is_layout                     = ls_layout
        it_toolbar_excluding          = lt_exclude
      CHANGING
        it_outtab                     = gt_zarn_net_qty
        it_fieldcatalog               = lt_fieldcat
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.
    IF sy-subrc IS NOT INITIAL.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

  ELSE.

    CALL METHOD go_nat_net_qty_alv->refresh_table_display.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  NAT_SERVE_SIZE_ALV
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM nat_serve_size_alv .

  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo.

  IF go_nat_serve_size_alv IS INITIAL.
* Net quantity
    CREATE OBJECT go_nscc20
      EXPORTING
        container_name = gc_nscc20.

* Create ALV grid
    CREATE OBJECT go_nat_serve_size_alv
      EXPORTING
        i_parent = go_nscc20.


*Call GRID
    PERFORM alv_build_fieldcat USING gc_alv_nat_serve_size  CHANGING lt_fieldcat.
** Exclude all edit functions in this example since we do not need them:
    PERFORM exclude_tb_functions_display CHANGING lt_exclude.

    ls_layout-cwidth_opt = abap_true.
    ls_layout-zebra = abap_true.
    ls_layout-no_toolbar = abap_true.


    CALL METHOD go_nat_serve_size_alv->set_table_for_first_display
      EXPORTING
        is_layout                     = ls_layout
        it_toolbar_excluding          = lt_exclude
      CHANGING
        it_outtab                     = gt_zarn_serve_size
        it_fieldcatalog               = lt_fieldcat
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.
    IF sy-subrc IS NOT INITIAL.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

  ELSE.

    CALL METHOD go_nat_serve_size_alv->refresh_table_display.
  ENDIF.

ENDFORM.

FORM check_online_tab_active.
* check if the toggle get characteristics from Arena is active - if not hide the Online Tab

  DATA: lo_ftf_store TYPE REF TO zif_ftf_store.

* Tab is no longer dependent on the toggle
  EXIT.

  IF gv_online_tab_switch IS INITIAL.

    TRY.
*   Feature Toggle for "Article Characteristics from Arena"
        lo_ftf_store = zcl_ftf_store_factory=>get_instance( )->get_feature(
                          iv_store     = '1000'       "Store
                          iv_appl_area = 'ZFSA_SAW'   "Sales Activation Workbench (SAW)
                          iv_feature   = 'CA' ).      "Article Characteristics from Arena

*   Is the toggle active?
        IF lo_ftf_store->is_active( )."Yes
          gv_online_tab_switch = 'A'.
        ELSE.
          gv_online_tab_switch = 'I'.
        ENDIF.
      CATCH zcx_object_not_found.
        gv_online_tab_switch = 'I'.
    ENDTRY.
  ENDIF.

  IF gv_online_tab_switch = 'I'.
    LOOP AT SCREEN INTO gs_screen.
      CHECK gs_screen-name = 'TS_REG_TAB4'.
      gs_screen-input     = 0.
      gs_screen-invisible = 1.
      MODIFY SCREEN FROM gs_screen.
      EXIT.
    ENDLOOP.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  CHECK_REG_NAT
*&---------------------------------------------------------------------*
*       Compare National and Regional values
*----------------------------------------------------------------------*
*-- >>> B-03401: Arena: Regional Data Update from PIM
*----------------------------------------------------------------------*
FORM check_reg_nat  CHANGING pt_reg_hdr TYPE ztarn_reg_hdr
                             pt_reg_str TYPE ztarn_reg_str.
  DATA: lt_empty_table TYPE STANDARD TABLE OF ty_line.

*-- Get the 1st record from the tables
  ASSIGN pt_reg_hdr[ 1 ] TO FIELD-SYMBOL(<ls_reg_hdr>).
  IF sy-subrc NE 0.
    EXIT."No Regional header values found
  ENDIF.
  ASSIGN pt_reg_str[ 1 ] TO FIELD-SYMBOL(<ls_reg_str>).
  IF sy-subrc NE 0.
    EXIT."No Regional string values found
  ENDIF.

  DATA(lv_brand_id)             = VALUE #( gs_prod_data_o-zarn_products[ 1 ]-brand_id OPTIONAL ).
  DATA(lv_fs_cust_medium_descr) = VALUE #( gs_prod_data_o-zarn_prod_str[ 1 ]-fs_cust_medium_descr OPTIONAL ).
  DATA(lv_fs_cust_short_descr)  = VALUE #( gs_prod_data_o-zarn_products_ex[ 1 ]-fs_cust_short_descr OPTIONAL ).
  DATA(lv_fs_cust_long_descr)   = VALUE #( gs_prod_data_o-zarn_prod_str[ 1 ]-fs_cust_long_descr OPTIONAL ).

*-- Compare the Regional values to the National Values.
*-- If they are the same, - clear the regional value
*--------------------------------------------------------------------*

*--------------------------------------------------------------------*
*-- Brand Description
*-- - There is no national equivalent

*--------------------------------------------------------------------*
*-- Brand Id
  IF <ls_reg_hdr>-brand_override = lv_brand_id
  AND <ls_reg_hdr>-brand_override IS NOT INITIAL.
    CLEAR : zarn_reg_hdr-brand_override, "Update for Screen
            <ls_reg_hdr>-brand_override.
    MODIFY gt_zarn_reg_hdr  FROM zarn_reg_hdr TRANSPORTING brand_override WHERE idno = zarn_reg_hdr-idno.

*-- Regional value has been removed as it matches the Natiaonal value: & &
    DATA(lv_msg_str) = gs_prod_data_o-zarn_products[ 1 ]-brand_id.
    CONCATENATE '"' lv_msg_str '"' INTO lv_msg_str.
    MESSAGE w135 WITH 'Brand ID' lv_msg_str.

  ENDIF.

*--------------------------------------------------------------------*
*-- Short/Retail Unit Description
*-- - Regional value is used to populate the description in MARA - and Arena will give an error if blanked out

*  IF <ls_reg_hdr>-retail_unit_desc = gs_prod_data_o-zarn_products[ 1 ]-fs_short_descr.
*    CLEAR <ls_reg_hdr>-retail_unit_desc.
*  ENDIF.

*--------------------------------------------------------------------*
*-- Medium Description
  IF <ls_reg_str>-fs_cust_medium_descr = lv_fs_cust_medium_descr
  AND <ls_reg_str>-fs_cust_medium_descr IS NOT INITIAL.


* gv_data_change
* gs_reg_data_db-zarn_reg_str.
    CLEAR : <ls_reg_str>-fs_cust_medium_descr,
            gs_reg_str-fs_cust_medium_descr.
    MODIFY gt_zarn_reg_str  FROM gs_reg_str TRANSPORTING fs_cust_medium_descr WHERE idno = gs_reg_str-idno.

*-- Remove the Description from the screen field
    CALL METHOD go_reg_editor_desc_r_md->set_text_as_r3table
      EXPORTING
        table  = lt_empty_table[]
      EXCEPTIONS
        OTHERS = 1.
    IF sy-subrc NE 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ELSE.
*-- Regional value has been removed as it matches the Natiaonal value: & &
      lv_msg_str = gs_prod_data_o-zarn_prod_str[ 1 ]-fs_cust_medium_descr.
      CONCATENATE '"' lv_msg_str '"' INTO lv_msg_str.
      MESSAGE w135 WITH 'Medium Description' lv_msg_str.
    ENDIF.
  ENDIF.

*--------------------------------------------------------------------*
*-- Size & Units (Short desc)
  IF <ls_reg_hdr>-fs_cust_short_descr = lv_fs_cust_short_descr
  AND <ls_reg_hdr>-fs_cust_short_descr IS NOT INITIAL.
    CLEAR : <ls_reg_hdr>-fs_cust_short_descr,
             zarn_reg_hdr-fs_cust_short_descr.
    MODIFY gt_zarn_reg_hdr  FROM zarn_reg_hdr TRANSPORTING fs_cust_short_descr WHERE idno = zarn_reg_hdr-idno.

*-- Remove the Description from the screen field
    CALL METHOD go_reg_editor_desc_r_sh->set_text_as_r3table
      EXPORTING
        table  = lt_empty_table[]
      EXCEPTIONS
        OTHERS = 1.
    IF sy-subrc NE 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ELSE.
*-- Regional value has been removed as it matches the Natiaonal value: & &
      lv_msg_str = gs_prod_data_o-zarn_products_ex[ 1 ]-fs_cust_short_descr.
      CONCATENATE '"' lv_msg_str '"' INTO lv_msg_str.
      MESSAGE w135 WITH 'Size & Units' lv_msg_str.
    ENDIF.
  ENDIF.

*--------------------------------------------------------------------*
*-- Marketing Text (Long desc)
  IF <ls_reg_str>-fs_cust_long_descr = lv_fs_cust_long_descr
  AND <ls_reg_str>-fs_cust_long_descr IS NOT INITIAL.
    CLEAR : <ls_reg_str>-fs_cust_long_descr,
            gs_reg_str-fs_cust_long_descr.
    MODIFY gt_zarn_reg_str  FROM gs_reg_str TRANSPORTING fs_cust_long_descr WHERE idno = gs_reg_str-idno.

*-- Remove the Description from the screen field
    CALL METHOD go_reg_editor_desc_r_lg->set_text_as_r3table
      EXPORTING
        table  = lt_empty_table[]
      EXCEPTIONS
        OTHERS = 1.
    IF sy-subrc NE 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ELSE.
*-- Regional value has been removed as it matches the National value: & &
      lv_msg_str = gs_prod_data_o-zarn_prod_str[ 1 ]-fs_cust_long_descr.
      CONCATENATE '"' lv_msg_str '"' INTO lv_msg_str.
      MESSAGE w135 WITH 'Marketing Text' lv_msg_str.
    ENDIF.
  ENDIF.

ENDFORM.

FORM enrich_regional_uom_alv_catlog CHANGING ct_fieldcat TYPE lvc_t_fcat.

  DATA: lv_set_column_pos TYPE flag,
        lv_field_name     TYPE lvc_fname.

  LOOP AT ct_fieldcat ASSIGNING FIELD-SYMBOL(<ls_fieldcat>).

    CLEAR: lv_set_column_pos.
    CASE <ls_fieldcat>-fieldname.
      WHEN 'NET_WEIGHT_VALUE'.
        <ls_fieldcat>-coltext   = 'REG NetWt'(213).
      WHEN 'NAT_NET_WEIGHT_VALUE'.
        <ls_fieldcat>-coltext   = 'NAT NetWt'(214).
        lv_set_column_pos = abap_true.
      WHEN 'GROSS_WEIGHT_VALUE'.
        <ls_fieldcat>-coltext   = 'REG GrossWt'(215).
      WHEN 'NAT_GROSS_WEIGHT_VALUE'.
        <ls_fieldcat>-coltext   = 'NAT GrossWt'(216).
        lv_set_column_pos = abap_true.
      WHEN 'HEIGHT_VALUE'.
        <ls_fieldcat>-coltext   = 'REG Height'(217).
      WHEN 'NAT_HEIGHT_VALUE'.
        <ls_fieldcat>-coltext   = 'NAT Height'(218).
        lv_set_column_pos = abap_true.
      WHEN 'WIDTH_VALUE'.
        <ls_fieldcat>-coltext   = 'REG Width'(219).
      WHEN 'NAT_WIDTH_VALUE'.
        <ls_fieldcat>-coltext   = 'NAT Width'(220).
        lv_set_column_pos = abap_true.
      WHEN 'DEPTH_VALUE'.
        <ls_fieldcat>-coltext   = 'REG Depth'(221).
      WHEN 'NAT_DEPTH_VALUE'.
        <ls_fieldcat>-coltext   = 'NAT Depth'(222).
        lv_set_column_pos = abap_true.
    ENDCASE.

    " Regional and National columns should be next to each other. The names have naming convention hence
    " generic coding is written. In case naming convention not followed, handle them separately.
    IF lv_set_column_pos IS NOT INITIAL.
      lv_field_name = <ls_fieldcat>-fieldname+4.
      READ TABLE ct_fieldcat INTO DATA(ls_fieldcat) WITH KEY fieldname = lv_field_name.
      IF sy-subrc = 0.
        <ls_fieldcat>-col_pos = ls_fieldcat-col_pos - 1.
      ENDIF.
    ENDIF.

  ENDLOOP.

ENDFORM.

FORM map_regional_uom USING it_reg_uom     TYPE ztarn_reg_uom
                            it_nat_alv_uom TYPE tt_nat_alv_uom
                   CHANGING ct_reg_alv_uom TYPE ztarn_reg_uom_ui.

  LOOP AT it_reg_uom ASSIGNING FIELD-SYMBOL(<ls_reg_uom>).
    APPEND INITIAL LINE TO ct_reg_alv_uom ASSIGNING FIELD-SYMBOL(<ls_reg_alv_uom>).
    MOVE-CORRESPONDING <ls_reg_uom> TO <ls_reg_alv_uom>.
  ENDLOOP.

ENDFORM.

*&---------------------------------------------------------------------*
*&      Form  NAT_ONLCAT_ALV
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM nat_onlcat_alv .

  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo.

  IF gv_feature_a4_enabled EQ abap_false.
    LOOP AT SCREEN INTO gs_screen.
      IF gs_screen-name EQ 'GV_NAT_PB_11'
      OR gs_screen-name EQ 'ONLINE_CATEGORIES'
      OR gs_screen-name EQ 'NSS11'.
        gs_screen-input = 0.
        gs_screen-output = 0.
        gs_screen-invisible = 1.
        MODIFY SCREEN FROM gs_screen.
      ENDIF.
    ENDLOOP.
  ELSE.

    IF go_nat_onlcat_alv IS INITIAL.
* Net quantity
      CREATE OBJECT go_nscc21
        EXPORTING
          container_name = gc_nscc21.

* Create ALV grid
      CREATE OBJECT go_nat_onlcat_alv
        EXPORTING
          i_parent = go_nscc21.


*Call GRID
      PERFORM alv_build_fieldcat USING gc_alv_nat_onlcat CHANGING lt_fieldcat.
** Exclude all edit functions in this example since we do not need them:
      PERFORM exclude_tb_functions_display CHANGING lt_exclude.

      ls_layout-cwidth_opt = abap_true.
      ls_layout-zebra = abap_true.
      ls_layout-no_toolbar = abap_true.


      CALL METHOD go_nat_onlcat_alv->set_table_for_first_display
        EXPORTING
          is_layout                     = ls_layout
          it_toolbar_excluding          = lt_exclude
        CHANGING
          it_outtab                     = gt_zarn_onlcat
          it_fieldcatalog               = lt_fieldcat
        EXCEPTIONS
          invalid_parameter_combination = 1
          program_error                 = 2
          too_many_lines                = 3
          OTHERS                        = 4.
      IF sy-subrc IS NOT INITIAL.
        MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                   WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
      ENDIF.

    ELSE.

      CALL METHOD go_nat_onlcat_alv->refresh_table_display.
    ENDIF.
  ENDIF.

ENDFORM.

FORM nat_benefits_alv .

  DATA:
    lt_fieldcat TYPE lvc_t_fcat,
    lt_exclude  TYPE ui_functions,
    ls_layout   TYPE lvc_s_layo.

  IF gv_feature_a4_enabled EQ abap_false.
    LOOP AT SCREEN INTO gs_screen.
      IF gs_screen-name EQ 'GV_NAT_PB_12'
      OR gs_screen-name EQ 'TRADE_ITEM_BENEFITS'
      OR gs_screen-name EQ 'NSS12'.
        gs_screen-input = 0.
        gs_screen-output = 0.
        gs_screen-invisible = 1.
        MODIFY SCREEN FROM gs_screen.
      ENDIF.
    ENDLOOP.
  ELSE.

    IF go_nat_benefits_alv IS INITIAL.
* Net quantity
      CREATE OBJECT go_nscc22
        EXPORTING
          container_name = gc_nscc22.

* Create ALV grid
      CREATE OBJECT go_nat_benefits_alv
        EXPORTING
          i_parent = go_nscc22.


*Call GRID
      PERFORM alv_build_fieldcat USING gc_alv_nat_benefits CHANGING lt_fieldcat.
** Exclude all edit functions in this example since we do not need them:
      PERFORM exclude_tb_functions_display CHANGING lt_exclude.

      ls_layout-cwidth_opt = abap_true.
      ls_layout-zebra = abap_true.
      ls_layout-no_toolbar = abap_true.


      CALL METHOD go_nat_benefits_alv->set_table_for_first_display
        EXPORTING
          is_layout                     = ls_layout
          it_toolbar_excluding          = lt_exclude
        CHANGING
          it_outtab                     = gt_zarn_benefits
          it_fieldcatalog               = lt_fieldcat
        EXCEPTIONS
          invalid_parameter_combination = 1
          program_error                 = 2
          too_many_lines                = 3
          OTHERS                        = 4.
      IF sy-subrc IS NOT INITIAL.
        MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                   WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
      ENDIF.

    ELSE.

      CALL METHOD go_nat_benefits_alv->refresh_table_display.
    ENDIF.
  ENDIF.

ENDFORM.
