*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_CONVERSION_EXITTOP.          " Global Data
  INCLUDE LZARN_CONVERSION_EXITUXX.          " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_CONVERSION_EXITF...          " Subroutines
* INCLUDE LZARN_CONVERSION_EXITO...          " PBO-Modules
* INCLUDE LZARN_CONVERSION_EXITI...          " PAI-Modules
* INCLUDE LZARN_CONVERSION_EXITE...          " Events
* INCLUDE LZARN_CONVERSION_EXITP...          " Local class implement.
* INCLUDE LZARN_CONVERSION_EXITT99.          " ABAP Unit tests
