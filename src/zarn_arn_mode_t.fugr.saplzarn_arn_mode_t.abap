* regenerated at 02.05.2016 17:33:46 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_ARN_MODE_TTOP.               " Global Data
  INCLUDE LZARN_ARN_MODE_TUXX.               " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_ARN_MODE_TF...               " Subroutines
* INCLUDE LZARN_ARN_MODE_TO...               " PBO-Modules
* INCLUDE LZARN_ARN_MODE_TI...               " PAI-Modules
* INCLUDE LZARN_ARN_MODE_TE...               " Events
* INCLUDE LZARN_ARN_MODE_TP...               " Local class implement.
* INCLUDE LZARN_ARN_MODE_TT99.               " ABAP Unit tests
  INCLUDE LZARN_ARN_MODE_TF00                     . " subprograms
  INCLUDE LZARN_ARN_MODE_TI00                     . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
