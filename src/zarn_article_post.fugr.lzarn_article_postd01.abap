*&---------------------------------------------------------------------*
*&  Include           LZARN_ARTICLE_POSTD01
*&---------------------------------------------------------------------*

CLASS lcl_article_post_helper DEFINITION FINAL CREATE PRIVATE.

  PUBLIC SECTION.
    CLASS-METHODS: get_instance RETURNING VALUE(ro_instance) TYPE REF TO lcl_article_post_helper.
    METHODS: add_message     IMPORTING
                               iv_msgty     TYPE symsgty DEFAULT sy-msgty
                               iv_msgid     TYPE symsgid DEFAULT sy-msgid
                               iv_msgno     TYPE symsgno DEFAULT sy-msgno
                               iv_msgv1     TYPE symsgv DEFAULT sy-msgv1
                               iv_msgv2     TYPE symsgv DEFAULT sy-msgv2
                               iv_msgv3     TYPE symsgv DEFAULT sy-msgv3
                               iv_msgv4     TYPE symsgv DEFAULT sy-msgv4
                               iv_batch     TYPE flag DEFAULT space
                               is_bapiret2  TYPE bapiret2 OPTIONAL
                               is_idno_data TYPE zsarn_idno_data OPTIONAL
                             CHANGING
                               ct_bal_msg   TYPE bal_t_msg,
      post_layout_module IMPORTING iv_batch       TYPE flag
                                   is_idno_data   TYPE zsarn_idno_data
                                   it_reg_cluster TYPE ztarn_reg_cluster
                         CHANGING  ct_bal_msg     TYPE bal_t_msg.

  PRIVATE SECTION.
    TYPES: BEGIN OF ts_article_layout,
             article       TYPE matnr,
             layout_module TYPE laygr,
             unit          TYPE meinh,
             cluster       TYPE zmd_e_cluster,
             sales_org     TYPE vkorg,
           END   OF ts_article_layout,
           tt_article_layout TYPE SORTED TABLE OF ts_article_layout WITH NON-UNIQUE KEY article layout_module.

    CLASS-DATA: so_singleton TYPE REF TO lcl_article_post_helper.

ENDCLASS.
