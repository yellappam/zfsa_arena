* regenerated at 02.05.2016 16:25:10 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_ALCSTRDES_TTOP.              " Global Data
  INCLUDE LZARN_ALCSTRDES_TUXX.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_ALCSTRDES_TF...              " Subroutines
* INCLUDE LZARN_ALCSTRDES_TO...              " PBO-Modules
* INCLUDE LZARN_ALCSTRDES_TI...              " PAI-Modules
* INCLUDE LZARN_ALCSTRDES_TE...              " Events
* INCLUDE LZARN_ALCSTRDES_TP...              " Local class implement.
* INCLUDE LZARN_ALCSTRDES_TT99.              " ABAP Unit tests
  INCLUDE LZARN_ALCSTRDES_TF00                    . " subprograms
  INCLUDE LZARN_ALCSTRDES_TI00                    . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
