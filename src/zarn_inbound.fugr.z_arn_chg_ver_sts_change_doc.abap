*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Functional Specification 3136
* DATE WRITTEN     # 31032016
* SAP VERSION      # 731
* TYPE             # Function Module
* AUTHOR           # C89991021, Ashish Kumar
*-----------------------------------------------------------------------
* TITLE            # Change document create
* PURPOSE          # Maintain Change documents for version status
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------
FUNCTION z_arn_chg_ver_sts_change_doc.
*"----------------------------------------------------------------------
*"*"Update Function Module:
*"
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(IT_VER_STATUS) TYPE  ZTARN_VER_STATUS
*"     VALUE(IT_VER_STATUS_OLD) TYPE  ZTARN_VER_STATUS OPTIONAL
*"     VALUE(IV_CHGID) TYPE  CDCHNGIND
*"----------------------------------------------------------------------

  DATA: lv_objectid           TYPE cdhdr-objectid,
        lt_xver_status        TYPE STANDARD TABLE OF yzarn_ver_status,
        lt_yver_status        TYPE STANDARD TABLE OF yzarn_ver_status,
        ls_xver_status        TYPE yzarn_ver_status,
        ls_yver_status        TYPE yzarn_ver_status,
        ls_arn_ver_status     LIKE LINE OF it_ver_status,
        ls_arn_ver_status_old LIKE LINE OF it_ver_status_old,
        lt_xcontrol           TYPE STANDARD TABLE OF yzarn_control,
        lt_ycontrol           TYPE STANDARD TABLE OF yzarn_control,
        ls_xcontrol           TYPE yzarn_control,
        ls_ycontrol           TYPE yzarn_control,
        lt_xprd_version       TYPE STANDARD TABLE OF yzarn_prd_version,
        lt_yprd_version       TYPE STANDARD TABLE OF yzarn_prd_version,
        ls_xprd_version       TYPE yzarn_prd_version,
        ls_yprd_version       TYPE yzarn_prd_version,
        xzarn_reg_banner      TYPE STANDARD TABLE OF yzarn_reg_banner,
        yzarn_reg_banner      TYPE STANDARD TABLE OF yzarn_reg_banner,
        xzarn_reg_ean         TYPE STANDARD TABLE OF yzarn_reg_ean,
        yzarn_reg_ean         TYPE STANDARD TABLE OF yzarn_reg_ean,
        xzarn_reg_hdr         TYPE STANDARD TABLE OF yzarn_reg_hdr,
        yzarn_reg_hdr         TYPE STANDARD TABLE OF yzarn_reg_hdr,
        xzarn_reg_hsno        TYPE STANDARD TABLE OF yzarn_reg_hsno,
        yzarn_reg_hsno        TYPE STANDARD TABLE OF yzarn_reg_hsno,
        xzarn_reg_pir         TYPE STANDARD TABLE OF yzarn_reg_pir,
        yzarn_reg_pir         TYPE STANDARD TABLE OF yzarn_reg_pir,
        xzarn_reg_prfam       TYPE STANDARD TABLE OF yzarn_reg_prfam,
        yzarn_reg_prfam       TYPE STANDARD TABLE OF yzarn_reg_prfam,
        xzarn_reg_txt         TYPE STANDARD TABLE OF yzarn_reg_txt,
        yzarn_reg_txt         TYPE STANDARD TABLE OF yzarn_reg_txt,
        xzarn_reg_uom         TYPE STANDARD TABLE OF yzarn_reg_uom,
        yzarn_reg_uom         TYPE STANDARD TABLE OF yzarn_reg_uom,
        lt_xzarn_reg_artlink  TYPE STANDARD TABLE OF yzarn_reg_artlink,
        lt_yzarn_reg_artlink  TYPE STANDARD TABLE OF yzarn_reg_artlink,
        lt_xzarn_reg_onlcat   TYPE STANDARD TABLE OF yzarn_reg_onlcat,
        lt_yzarn_reg_onlcat   TYPE STANDARD TABLE OF yzarn_reg_onlcat,
        lt_xzarn_reg_str      TYPE yzarn_reg_strs,
        lt_yzarn_reg_str      TYPE yzarn_reg_strs,
        lt_cdtxt              TYPE isu_cdtxt,

        xzarn_reg_lst_prc     TYPE STANDARD TABLE OF yzarn_reg_lst_prc,
        yzarn_reg_lst_prc     TYPE STANDARD TABLE OF yzarn_reg_lst_prc,
        xzarn_reg_std_ter     TYPE STANDARD TABLE OF yzarn_reg_std_ter,
        yzarn_reg_std_ter     TYPE STANDARD TABLE OF yzarn_reg_std_ter,
        xzarn_reg_dc_sell     TYPE STANDARD TABLE OF yzarn_reg_dc_sell,
        yzarn_reg_dc_sell     TYPE STANDARD TABLE OF yzarn_reg_dc_sell,
        xzarn_reg_rrp         TYPE STANDARD TABLE OF yzarn_reg_rrp,
        yzarn_reg_rrp         TYPE STANDARD TABLE OF yzarn_reg_rrp.


  LOOP AT it_ver_status INTO ls_arn_ver_status.

    lv_objectid = ls_arn_ver_status-idno.

    FREE lt_xver_status.
    CLEAR ls_xver_status.

    MOVE-CORRESPONDING ls_arn_ver_status TO ls_xver_status.
*    ls_xver_status-kz = iv_chgid.
    APPEND ls_xver_status TO lt_xver_status[].

    IF it_ver_status_old[] IS NOT INITIAL .
      READ TABLE it_ver_status_old INTO ls_arn_ver_status_old WITH KEY idno = ls_arn_ver_status-idno.
      IF sy-subrc EQ 0.
        FREE lt_yver_status.
        CLEAR ls_yver_status.

        MOVE-CORRESPONDING ls_arn_ver_status_old TO ls_yver_status.
*        ls_yver_status-kz = iv_chgid.
        APPEND ls_yver_status TO lt_yver_status[].
      ENDIF.
    ENDIF.

** Create Change Document
    CALL FUNCTION 'ZARN_CTRL_REGNL_WRITE_DOC_CUST'
      EXPORTING
        objectid                   = lv_objectid
        tcode                      = sy-tcode
        utime                      = sy-uzeit
        udate                      = sy-datum
        username                   = sy-uname
**         PLANNED_CHANGE_NUMBER      = ' '
        object_change_indicator    = iv_chgid
**         PLANNED_OR_REAL_CHANGES    = ' '
**         NO_CHANGE_POINTERS         = ' '
        upd_icdtxt_zarn_ctrl_regnl = ' '
        upd_zarn_control           = ' '
        upd_zarn_prd_version       = ' '
        upd_zarn_reg_artlink       = ' '
        upd_zarn_reg_banner        = ' '
        upd_zarn_reg_dc_sell       = ' '
        upd_zarn_reg_ean           = ' '
        upd_zarn_reg_hdr           = ' '
        upd_zarn_reg_hsno          = ' '
        upd_zarn_reg_lst_prc       = ' '
        upd_zarn_reg_onlcat        = ' '
        upd_zarn_reg_pir           = ' '
        upd_zarn_reg_prfam         = ' '
        upd_zarn_reg_rrp           = ' '
        upd_zarn_reg_std_ter       = ' '
        upd_zarn_reg_str           = ' '
        xzarn_reg_str              = lt_xzarn_reg_str
        yzarn_reg_str              = lt_yzarn_reg_str
        upd_zarn_reg_txt           = ' '
        upd_zarn_reg_uom           = ' '
        upd_zarn_ver_status        = iv_chgid
      TABLES
        icdtxt_zarn_ctrl_regnl     = lt_cdtxt[]
        xzarn_control              = lt_xcontrol[]           " lt_control[]
        yzarn_control              = lt_ycontrol[]          " lt_control_old[]
        xzarn_prd_version          = lt_xprd_version[]       " lt_prd_version[]
        yzarn_prd_version          = lt_yprd_version[]       " lt_prd_version_old[]
        xzarn_reg_artlink          = lt_xzarn_reg_artlink[]
        yzarn_reg_artlink          = lt_yzarn_reg_artlink[]
        xzarn_reg_banner           = xzarn_reg_banner[]
        yzarn_reg_banner           = yzarn_reg_banner[]
        xzarn_reg_dc_sell          = xzarn_reg_dc_sell[]
        yzarn_reg_dc_sell          = yzarn_reg_dc_sell[]
        xzarn_reg_ean              = xzarn_reg_ean[]
        yzarn_reg_ean              = yzarn_reg_ean[]
        xzarn_reg_hdr              = xzarn_reg_hdr[]
        yzarn_reg_hdr              = yzarn_reg_hdr[]
        xzarn_reg_hsno             = xzarn_reg_hsno[]
        yzarn_reg_hsno             = yzarn_reg_hsno[]
        xzarn_reg_lst_prc          = xzarn_reg_lst_prc[]
        yzarn_reg_lst_prc          = yzarn_reg_lst_prc[]
        xzarn_reg_onlcat           = lt_xzarn_reg_onlcat[]
        yzarn_reg_onlcat           = lt_yzarn_reg_onlcat[]
        xzarn_reg_pir              = xzarn_reg_pir[]
        yzarn_reg_pir              = yzarn_reg_pir[]
        xzarn_reg_prfam            = xzarn_reg_prfam[]
        yzarn_reg_prfam            = yzarn_reg_prfam[]
        xzarn_reg_rrp              = xzarn_reg_rrp[]
        yzarn_reg_rrp              = yzarn_reg_rrp[]
        xzarn_reg_std_ter          = xzarn_reg_std_ter[]
        yzarn_reg_std_ter          = yzarn_reg_std_ter[]
        xzarn_reg_txt              = xzarn_reg_txt[]
        yzarn_reg_txt              = yzarn_reg_txt[]
        xzarn_reg_uom              = xzarn_reg_uom[]
        yzarn_reg_uom              = yzarn_reg_uom[]
        xzarn_ver_status           = lt_xver_status[]       " lt_ver_status[]
        yzarn_ver_status           = lt_yver_status[].       " lt_ver_status_old[].

  ENDLOOP.

ENDFUNCTION.
