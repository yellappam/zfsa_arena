FUNCTION ZARN_READ_UOM_CATEGORY .
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(IV_UOM_CATEGORY) TYPE  ZARN_UOM_CATEGORY OPTIONAL
*"  EXPORTING
*"     REFERENCE(IT_UOM_CAT) TYPE  ZTARN_UOM_CAT
*"----------------------------------------------------------------------

* get all value for a UOM Category if asked ?
  IF iv_uom_category IS NOT INITIAL.
    SELECT * INTO TABLE it_uom_cat
      FROM zarn_uom_cat
      WHERE uom_category EQ iv_uom_category.
  ELSE.
* Otherwise, get all values for a UOM Category where it is not SPACE !
    SELECT * INTO TABLE it_uom_cat
      FROM zarn_uom_cat
      WHERE uom_category NE iv_uom_category.
  ENDIF.


ENDFUNCTION.
