* regenerated at 05.05.2016 16:17:37 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_ALCSTRCOD_TTOP.              " Global Data
  INCLUDE LZARN_ALCSTRCOD_TUXX.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_ALCSTRCOD_TF...              " Subroutines
* INCLUDE LZARN_ALCSTRCOD_TO...              " PBO-Modules
* INCLUDE LZARN_ALCSTRCOD_TI...              " PAI-Modules
* INCLUDE LZARN_ALCSTRCOD_TE...              " Events
* INCLUDE LZARN_ALCSTRCOD_TP...              " Local class implement.
* INCLUDE LZARN_ALCSTRCOD_TT99.              " ABAP Unit tests
  INCLUDE LZARN_ALCSTRCOD_TF00                    . " subprograms
  INCLUDE LZARN_ALCSTRCOD_TI00                    . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
