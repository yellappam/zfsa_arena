interface ZIF_ARN_INITIATION_C
  public .


  constants GC_CENTRAL type ZARN_INITIATION value 'C' ##NO_TEXT.
  constants GC_LOCAL type ZARN_INITIATION value 'L' ##NO_TEXT.
  constants GC_DEPLOYMENT type ZARN_INITIATION value 'D' ##NO_TEXT.
  constants GC_LOCAL_RANGE type ZARN_INITIATION value 'S' ##NO_TEXT.
endinterface.
