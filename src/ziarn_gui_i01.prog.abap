*&---------------------------------------------------------------------*
*&  Include           ZIARN_GUI_I01
*&---------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*&      Module  USER_COMMAND_0100  INPUT
*&---------------------------------------------------------------------*
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 17.07.2018
* CHANGE No.       # SUP-114 aRena P1 issue? (MCH. Gilmours Online)
* DESCRIPTION      # The purpose of the proposed change is to influence the
*                    following 3 field values based on the Merchandise Category
*                    field changes in the AReNa Gui screen in the Regional Data tab;
*                    - Article Type in the Basic Data
*                    -  Host Product Type in the Non -SAP
*                    - AS400 Sub-Department in the Non -SAP
* WHO              # C90007074, Jos van Gysen
*-----------------------------------------------------------------------
* DATE             # 25/11/2019
* CHANGE No.       # Charm 9000006356; Jira CIP-148
* DESCRIPTION      # When saving regional data call new routine to save
*                  # that includes logic to post data when there are
*                  # no outstanding approvals. Prior to this change the
*                  # posting did not occur after auto approval.
*                  # Replaced calls to routine PROCESS_REGIONAL_DATA
*                  # with 'save' with call to new routine where relevant.
* WHO              # C90001929, Aruna Mackenjee
*-----------------------------------------------------------------------

MODULE user_command_0100 INPUT.

  DATA lv_error   TYPE boole_d.
  DATA lv_answer  TYPE char1.

  DATA: lt_rows   TYPE lvc_t_row.

  gv_save_okcode = gv_okcode.
  CLEAR: gv_okcode.

  IF go_dialogbox_container IS NOT INITIAL AND
     gv_save_okcode(3)      NE     gc_ts_prefix. "required on TAB toggle
*   release validation dialog box
    CALL METHOD go_dialogbox_container->free.
    FREE go_dialogbox_container.
  ENDIF.

  IF go_dialogbox_cont_gtin IS NOT INITIAL AND
     gv_save_okcode(3)      NE     gc_ts_prefix. "required on TAB toggle
*   release multiple GTIN dialog box
    CALL METHOD go_dialogbox_cont_gtin->free.
    FREE go_dialogbox_cont_gtin.
  ENDIF.

  IF go_dialogbox_chg_category IS NOT INITIAL.
*   release change category change listing dialog box
    CALL METHOD go_dialogbox_chg_category->free.
    FREE go_dialogbox_chg_category.
  ENDIF.

* capture if data changed in any one variable on the dynpro
  IF gs_prod_data-idno IS NOT INITIAL AND
     sy-datar          IS NOT INITIAL AND
     gv_display        IS     INITIAL.
    gv_data_change = sy-datar.
  ENDIF.

  CASE gv_save_okcode.
*
    WHEN 'SIZE'.
*     resize the docking container
*     calculate expand size
      IF gv_extension IS INITIAL OR
         gv_extension EQ gc_extension.
        gv_extension = ( p_ratio * 20 ).
        gv_text_size-text      = TEXT-i01.
        gv_text_size-icon_text = TEXT-i01.
      ELSE.
        gv_extension = gc_extension.
        gv_text_size-text      = TEXT-i02.
        gv_text_size-icon_text = TEXT-i02.
      ENDIF.

      CALL METHOD go_docking_alv->set_extension
        EXPORTING
          extension = gv_extension.  " Max 2000
*
    WHEN 'BACK' OR
         'EXIT'  OR
         'CANC'.
*     check ALV changes
*      PERFORM check_alv_data_change.
*     popup to check if data loss ??
      PERFORM check_data_loss.

*
    WHEN 'BACKTOLIST'.
      " go back to worklist screen in case of Enrichment/Approval
      PERFORM back_to_worklist USING p_ratio.

*
    WHEN 'SAVE'.
*     check ALV changes
      PERFORM check_alv_data_change.
      PERFORM align_regional_cluster.
      PERFORM check_duplicates CHANGING lv_error.

      CHECK lv_error IS INITIAL.


*      PERFORM process_regional_data
*        USING abap_true.
      PERFORM save_regional_data.

*      gs_reg_data_db = gs_reg_data_save.


*
    WHEN 'CHEK'.
**     check ALV changes
      PERFORM check_alv_data_change.


      CLEAR lt_rows[].
*   get selected row
      CALL METHOD go_worklist_alv->get_selected_rows
        IMPORTING
          et_index_rows = lt_rows.

      IF lt_rows[] IS INITIAL AND gs_prod_data_o IS INITIAL.
        MESSAGE e012 DISPLAY LIKE 'I'.
        RETURN.
      ENDIF.


      IF lt_rows[] IS NOT INITIAL.
        PERFORM worklist_validation USING lt_rows[].
      ELSE.
        PERFORM process_regional_data
          USING abap_false.
      ENDIF.

    WHEN 'PREV'.
**    Scroll_article .
      IF gv_row_total GT 1 AND
         gv_row_index GT 1.

        " switch to new record after checking data change
        PERFORM switch_mode_or_record CHANGING lv_answer.

        IF lv_answer NE gc_cancel.
          SUBTRACT 1 FROM gv_row_index.
*       Go back to using the default display
          gv_display = gv_display_default.
          PERFORM process_worklist_to_screen USING gt_rows.
*        IF  gv_nat_approved IS INITIAL.
*          ADD 1 TO gv_row_index.
*        ENDIF.
          PERFORM check_regional_tab.
        ENDIF.

      ENDIF.
*
    WHEN 'NEXT'.

**    Scroll_article .
      IF gv_row_total GT 1 AND
         gv_row_index LT gv_row_total.

        " switch to new record after checking data change
        PERFORM switch_mode_or_record CHANGING lv_answer.

        IF lv_answer NE gc_cancel.
          ADD 1 TO gv_row_index.
*       Go back to using the default display
          gv_display = gv_display_default.
          PERFORM process_worklist_to_screen USING gt_rows.
*        IF  gv_nat_approved IS INITIAL.
*          SUBTRACT 1 FROM gv_row_index.
*        ENDIF.
          PERFORM check_regional_tab.
        ENDIF.

      ENDIF.

*
    WHEN 'ENRICH'.

      CLEAR lt_rows[].
*   get selected row
      CALL METHOD go_worklist_alv->get_selected_rows
        IMPORTING
          et_index_rows = lt_rows.

      IF lt_rows[] IS NOT INITIAL.
        RETURN.
      ENDIF.

      PERFORM lock_regional_data_current.
      PERFORM process_worklist_to_screen USING gt_rows.

    WHEN 'DISPLAY'.

      CLEAR lt_rows[].
*   get selected row
      CALL METHOD go_worklist_alv->get_selected_rows
        IMPORTING
          et_index_rows = lt_rows.

      IF lt_rows[] IS NOT INITIAL.
        RETURN.
      ENDIF.

      PERFORM switch_mode_or_record CHANGING lv_answer.

      IF lv_answer NE gc_cancel.
        gv_display_default = abap_true.
        gv_display = gv_display_default.
        lcl_controller=>get_instance( )->set_display( iv_display = gv_display ).
        PERFORM process_worklist_to_screen USING gt_rows.
      ENDIF.
*________________________________________________________________
    WHEN 'NPB00'.

      IF gv_appr_nat_okcode IS NOT INITIAL.
        CLEAR gv_appr_nat_okcode.
        gv_appr_screen_no = '0700'.
        WRITE icon_collapse  TO gv_nat_pb_00 AS ICON.
      ELSE.
        gv_appr_nat_okcode = gv_save_okcode.
        gv_appr_screen_no = '0702'.
        WRITE icon_expand  TO gv_nat_pb_00 AS ICON.
      ENDIF.

*
    WHEN 'NPB01'.
      " Basic Info
      IF gv_nat_okcode IS NOT INITIAL.
        CLEAR gv_nat_okcode.
        gv_screen_no = '0201'.
        WRITE icon_collapse  TO gv_nat_pb_01 AS ICON.
      ELSE.
        gv_nat_okcode = gv_save_okcode.
        gv_screen_no = '0202'.

        WRITE icon_expand  TO gv_nat_pb_01 AS ICON.
      ENDIF.
*
    WHEN 'NPB02'.
      " UOM
      IF gv_nat_okcode_uom IS NOT INITIAL.
        CLEAR gv_nat_okcode_uom.
        gv_uom_rscreen_no = '0704'.
        WRITE icon_collapse  TO gv_nat_pb_02 AS ICON.
      ELSE.
        gv_nat_okcode_uom = gv_save_okcode.
        gv_uom_rscreen_no = '0705'.

        WRITE icon_expand  TO gv_nat_pb_02 AS ICON.
      ENDIF.


    WHEN 'NPB03'.
      " List Price
      IF gv_nat_okcode_lp IS NOT INITIAL.
        CLEAR gv_nat_okcode_lp.
        gv_lp_rscreen_no = '0706'.
        WRITE icon_collapse  TO gv_nat_pb_03 AS ICON.
      ELSE.
        gv_nat_okcode_lp = gv_save_okcode.
        gv_lp_rscreen_no = '0707'.

        WRITE icon_expand  TO gv_nat_pb_03 AS ICON.
      ENDIF.
*
    WHEN 'NPB04'.
*     " PIR
      IF gv_nat_okcode_pir IS NOT INITIAL.
        " refresh comm. table on opening this screen...
        IF gt_zarn_pir_comm_ch[] IS NOT INITIAL.
          gt_zarn_pir_comm_ui[] = gt_zarn_pir_comm_ch[].
        ENDIF.

        CLEAR gv_nat_okcode_pir.
        gv_pir_rscreen_no = '0708'.
        WRITE icon_collapse  TO gv_nat_pb_04 AS ICON.
      ELSE.
        gv_nat_okcode_pir = gv_save_okcode.
        gv_pir_rscreen_no = '0709'.

        WRITE icon_expand  TO gv_nat_pb_04 AS ICON.
      ENDIF.
*
    WHEN 'NPB05'.
*     food safety info
      IF gv_nat_okcode_fsi IS INITIAL.
        gv_nat_okcode_fsi = gv_save_okcode.
        gv_fsi_rscreen_no = '0710'.
        WRITE icon_collapse  TO gv_nat_pb_05 AS ICON.
      ELSE.
        CLEAR gv_nat_okcode_fsi.
        gv_fsi_rscreen_no = '0711'.

        WRITE icon_expand  TO gv_nat_pb_05 AS ICON.
      ENDIF.
*
    WHEN 'NPB06'.
*     Consumer Info
      IF gv_nat_okcode_ci IS INITIAL.
        gv_nat_okcode_ci = gv_save_okcode.
        gv_ci_rscreen_no = '0712'.
        WRITE icon_collapse  TO gv_nat_pb_06 AS ICON.
      ELSE.
        CLEAR gv_nat_okcode_ci.
        gv_ci_rscreen_no = '0713'.

        WRITE icon_expand  TO gv_nat_pb_06 AS ICON.
      ENDIF.
*
    WHEN 'NPB07'.
*     Nutritional/Ingredient Info
      IF gv_nat_okcode_nai IS INITIAL.
        gv_nat_okcode_nai = gv_save_okcode.
        gv_nai_rscreen_no = '0714'.
        WRITE icon_collapse  TO gv_nat_pb_07 AS ICON.
      ELSE.
        CLEAR gv_nat_okcode_nai.
        gv_nai_rscreen_no = '0715'.
        WRITE icon_expand  TO gv_nat_pb_07 AS ICON.
      ENDIF.
*
    WHEN 'NPB08'.
*     Additional Info
      IF gv_nat_okcode_ai IS INITIAL.
        gv_nat_okcode_ai = gv_save_okcode.
        gv_ai_rscreen_no = '0716'.
        WRITE icon_collapse  TO gv_nat_pb_08 AS ICON.
      ELSE.
        CLEAR gv_nat_okcode_ai.
        gv_ai_rscreen_no = '0717'.
        WRITE icon_expand  TO gv_nat_pb_08 AS ICON.
      ENDIF.
*
    WHEN 'NPB09'.
*     Dangerous goods Info
      IF gv_nat_okcode_dgi IS INITIAL.
        gv_nat_okcode_dgi = gv_save_okcode.
        gv_dgi_rscreen_no = '0718'.
        WRITE icon_collapse  TO gv_nat_pb_09 AS ICON.
      ELSE.
        CLEAR gv_nat_okcode_dgi.
        gv_dgi_rscreen_no = '0719'.
        WRITE icon_expand  TO gv_nat_pb_09 AS ICON.
      ENDIF.
*
    WHEN 'NPB10'.
*     Ingredient information
      IF gv_nat_okcode_ingr IS INITIAL.
        gv_nat_okcode_ingr = gv_save_okcode.
        gv_ingr_rscreen_no = '0720'.
        WRITE icon_collapse  TO gv_nat_pb_10 AS ICON.
      ELSE.
        CLEAR gv_nat_okcode_ingr.
        gv_ingr_rscreen_no = '0721'.
        WRITE icon_expand  TO gv_nat_pb_10 AS ICON.
      ENDIF.
*
    WHEN 'NPB11'.
*     Online Categories
      IF gv_nat_okcode_onlcat IS INITIAL.
        gv_nat_okcode_onlcat = gv_save_okcode.
        gv_onlcat_rscreen_no = '0722'.
        WRITE icon_collapse  TO gv_nat_pb_11 AS ICON.
      ELSE.
        CLEAR gv_nat_okcode_onlcat.
        gv_onlcat_rscreen_no = '0723'.
        WRITE icon_expand  TO gv_nat_pb_11 AS ICON.
      ENDIF.
*
    WHEN 'NPB12'.
*     Trade Item Benefits
      IF gv_nat_okcode_benefit IS INITIAL.
        gv_nat_okcode_benefit = gv_save_okcode.
        gv_benefit_rscreen_no = '0724'.
        WRITE icon_collapse  TO gv_nat_pb_12 AS ICON.
      ELSE.
        CLEAR gv_nat_okcode_benefit.
        gv_benefit_rscreen_no = '0725'.
        WRITE icon_expand  TO gv_nat_pb_12 AS ICON.
      ENDIF.
*
    WHEN 'RPB00'.

      IF gv_appr_reg_okcode IS NOT INITIAL.
        CLEAR gv_appr_reg_okcode.
        gv_appr_rscreen_no = '0701'.
        WRITE icon_collapse  TO gv_reg_pb_00 AS ICON.
        gv_appr_syucomm = sy-ucomm.
      ELSE.
        gv_appr_reg_okcode = gv_save_okcode.
        gv_appr_rscreen_no = '0702'.

        WRITE icon_expand  TO gv_reg_pb_00 AS ICON.
      ENDIF.
*
    WHEN 'RPB01'.

      IF gv_reg_okcode IS NOT INITIAL.
        CLEAR gv_reg_okcode.
        gv_rscreen_no = '0203'.
        WRITE icon_collapse  TO gv_reg_pb_01 AS ICON.
      ELSE.
        gv_reg_okcode = gv_save_okcode.
        gv_rscreen_no = '0204'.

        WRITE icon_expand  TO gv_reg_pb_01 AS ICON.
      ENDIF.
*
    WHEN 'CLOGS'.

      CLEAR lt_rows[].
*   get selected row
      CALL METHOD go_worklist_alv->get_selected_rows
        IMPORTING
          et_index_rows = lt_rows.

      IF lt_rows[] IS INITIAL AND zarn_products-idno IS INITIAL.
        MESSAGE e012 DISPLAY LIKE 'I'.
        RETURN.
      ENDIF.

*     Call change document report with selection
      PERFORM change_documents_display
        USING lt_rows
              zarn_products-idno
              gv_save_okcode.


*
    WHEN 'DELT'.

      CLEAR lt_rows[].
*   get selected row
      CALL METHOD go_worklist_alv->get_selected_rows
        IMPORTING
          et_index_rows = lt_rows.

      IF lt_rows[] IS INITIAL AND zarn_products-fan_id IS INITIAL.
        MESSAGE e012 DISPLAY LIKE 'I'.
        RETURN.
      ENDIF.


*     Call Delta Analysis report - show selection screen
      PERFORM delta_analysis_display
        USING lt_rows
              zarn_products-fan_id.


*
    WHEN 'RMEK1'.
      PERFORM call_transaction USING 'MEK1'.
*
    WHEN 'RVKP5'.
      PERFORM call_transaction USING 'VKP5'.
*
    WHEN 'RMM42'.
      PERFORM call_mm42_bdc.
*
    WHEN 'SIMU'.  " Simulate values
      PERFORM simulate_standard_terms.
*
*    WHEN 'DEF_BANNER_RET'.
*      PERFORM validate_catman CHANGING lv_error.
*
*      IF lv_error IS INITIAL.
*      PERFORM default_banners USING gc_banner_def_ret.
*      ENDIF.
**
*    WHEN 'DEF_BANNER_GIL'.
*      PERFORM default_banners USING gc_banner_def_gil.

*
    WHEN 'CALL_MM43'.
      PERFORM call_mm43.

* Approvals user commands moved to here instead of own screens
*
    WHEN 'APPR_NAPPR'.
      PERFORM approvals_update2 USING co_appr_called_from_nat space space zif_arn_approval_status=>gc_arn_appr_status_approved.
*
    WHEN 'APPR_NREJE'.
      PERFORM approvals_update2 USING co_appr_called_from_nat space space zif_arn_approval_status=>gc_arn_appr_status_rejected.
*
    WHEN 'APPR_NSHOWCHG'.
      PERFORM appr_highlight_changed_fields2 USING co_appr_called_from_nat space.
*
    WHEN 'APPR_RAPPR'.
      PERFORM approvals_update2 USING co_appr_called_from_reg space space zif_arn_approval_status=>gc_arn_appr_status_approved.
*
    WHEN 'APPR_RREJE'.
      PERFORM approvals_update2 USING co_appr_called_from_reg space space zif_arn_approval_status=>gc_arn_appr_status_rejected.
*
    WHEN 'APPR_SHOWCHG'.
      PERFORM appr_highlight_changed_fields2 USING co_appr_called_from_reg space.
*
    WHEN 'APPR_EDIT'.
      IF gv_display EQ abap_true.
        gv_display = abap_false.
      ELSE.
        gv_display = abap_true.
      ENDIF.

    WHEN 'REG_CLUSTER'.
      PERFORM change_reg_cluster_icon_0112.

    WHEN OTHERS.

  ENDCASE.

  CLEAR: gv_save_okcode, gv_okcode, sy-ucomm.

  CALL METHOD cl_gui_cfw=>flush.


ENDMODULE.                 " USER_COMMAND_0100  INPUT

*&---------------------------------------------------------------------*
*&      Module  USER_COMMAND_0703  INPUT
*&---------------------------------------------------------------------*
MODULE user_command_0703 INPUT.

  PERFORM appr_user_command_popup.

ENDMODULE.                 " USER_COMMAND_0703  INPUT

*&---------------------------------------------------------------------*
*&      Module  GET_MC_TEXT  INPUT
*&---------------------------------------------------------------------*
MODULE get_mc_text INPUT.
  IF zarn_reg_hdr-matkl IS INITIAL.
    CLEAR gs_text-wgbez.
    RETURN.
  ENDIF.

  SELECT SINGLE wgbez INTO gs_text-wgbez FROM t023t
         WHERE spras = sy-langu
           AND matkl = zarn_reg_hdr-matkl.
  IF sy-subrc IS NOT INITIAL.
    CLEAR gs_text-wgbez.
  ENDIF.

*** SUP-114 "aRena P1 issue? (MCH. Gilmours Online)"
*** get the article type from the merchandising category reference article
**  SELECT SINGLE mara~mtart INTO zarn_reg_hdr-mtart
**    FROM t023
**    INNER JOIN mara
**    ON mara~matnr = t023~wwgda
**    WHERE t023~matkl =  zarn_reg_hdr-matkl.
**
*** get the AS400 sub department
**  SELECT SINGLE subdep INTO zarn_reg_hdr-zzas4subdept
**    FROM zmd_mc_subdep
**      WHERE matkl = zarn_reg_hdr-matkl.
**
**  IF sy-subrc = 0.
***   get the host product type
**    SELECT SINGLE zzprdtype FROM zmd_host_prdtyp INTO zarn_reg_hdr-zzprdtype
**      WHERE wwgha = zarn_reg_hdr-zzas4subdept.
**  ENDIF.

ENDMODULE.                 " GET_MC_TEXT  INPUT
*&---------------------------------------------------------------------*
*&      Module  GET_VENDOR_TEXT  INPUT
*&---------------------------------------------------------------------*

MODULE get_vendor_text INPUT.
  IF zarn_reg_pir-lifnr IS INITIAL.
    CLEAR gs_text-name1.
  ENDIF.

  SELECT SINGLE name1 INTO gs_text-name1 FROM lfa1
         WHERE lifnr = zarn_reg_pir-lifnr.
  IF sy-subrc IS NOT INITIAL.
    CLEAR gs_text-name1.
  ENDIF.


ENDMODULE.                 " GET_VENDOR_TEXT  INPUT
*&---------------------------------------------------------------------*
*&      Module  USER_COMMAND_0111  INPUT
*&---------------------------------------------------------------------*
MODULE user_command_0111 INPUT.


** DEL Begin of Change ONLD-835 JKH 27.04.2017
*  IF sy-ucomm = 'PRFTC'.
*    PERFORM call_transaction USING 'PRFAM'.
*
*  ELSEIF sy-ucomm = 'ZSD_PRF'.
*    PERFORM call_transaction USING 'ZSD_PRFAM'.
*
*  ENDIF.
*
*  IF sy-ucomm EQ 'PRF'.  " radiobutton
**   check fields
*    IF gv_new IS NOT INITIAL.
*      zarn_reg_prfam-prfam_status = 1.
*    ELSEIF gv_existing IS NOT INITIAL.
*      zarn_reg_prfam-prfam_status = 2.
*    ELSEIF gv_change IS NOT INITIAL.
*      zarn_reg_prfam-prfam_status = 3.
*    ENDIF.
*
*    gv_prfam_done_text = text-039. "Done
*    CLEAR zarn_reg_prfam-prfam_done_flag.
*
*  ELSEIF sy-ucomm EQ 'PRFAM'.  "Done Pushbutton
*
*    IF gv_prfam_done_text EQ text-039.
*      WRITE icon_locked TO gv_prfam_done_text.
*      gv_prfam_done_text = gv_prfam_done_text && text-039. "Done
*      zarn_reg_prfam-prfam_done_flag = abap_true.
*    ELSE.
*      gv_prfam_done_text = text-039. "Done
*      CLEAR zarn_reg_prfam-prfam_done_flag.
*    ENDIF.
*
*  ENDIF.
** DEL End of Change ONLD-835 JKH 27.04.2017


ENDMODULE.                 " USER_COMMAND_0111  INPUT
*&---------------------------------------------------------------------*
*&      Module  GET_ATTRIBUTE  INPUT
*&---------------------------------------------------------------------*
MODULE get_attribute INPUT.

* INS Begin of Change CTS-160 JKH 10.01.2017
* Brand field compulsory+default 'OTHM'
  IF gv_display            IS INITIAL     AND
     zarn_reg_hdr-idno     IS NOT INITIAL AND
     zarn_reg_hdr-brand_id IS INITIAL.

    zarn_reg_hdr-brand_id = zcl_constants=>gc_brand_id_default.    " 'OTHM'.

* Brand Id is mandatory, defaulted to 'OTHM'
    MESSAGE s124.
  ENDIF.
* INS End of Change CTS-160 JKH 10.01.2017


* INC5348804 - Stop Defaulting of Private Label based on Brand

* DEL Begin of Change INC5348804 Jitin 31.10.2016
*  IF gv_display IS INITIAL.  " ++INC5321987 JKH 03.11.2016
** Regional header attribute Ctrld/Prv Label Rebate
*    PERFORM default_regional_attr_zzattr6
*      USING zarn_reg_hdr-brand_id
*      CHANGING zarn_reg_hdr-zzattr6
*               gv_zattr6_def.
*  ENDIF.
* DEL End of Change INC5348804 Jitin 31.10.2016


ENDMODULE.                 " GET_ATTRIBUTE  INPUT

MODULE check_height INPUT.

  IF zarn_reg_hdr-onl_pres_height_ddn = 'O'.
    IF zarn_reg_hdr-onl_pres_height = 0.
      MESSAGE w152.
    ENDIF.
  ELSE.
    CLEAR zarn_reg_hdr-onl_pres_height.
  ENDIF.

**  zarn_reg_hdr-onl_pres_height = get_online_dimension( EXPORTING  iv_article = zarn_reg_hdr-matnr
**                                                                  iv_idno    = zarn_reg_hdr-idno
**                                                                  iv_onl_pres_height_ddn = zarn_reg_hdr-onl_pres_height_ddn
**                                                                  iv_onl_pres_height = zarn_reg_hdr-onl_pres_height ).


ENDMODULE.

*&---------------------------------------------------------------------*
*&      Module  SET_ZZTKTPRNT  INPUT
*&---------------------------------------------------------------------*
MODULE set_zztktprnt INPUT.

*  IF zarn_reg_hdr-idno IS NOT INITIAL.             " ++JKH 01.11.2016
** Ticket Print flag
*    IF gv_zztktprnt IS INITIAL.
*      zarn_reg_hdr-zztktprnt = 'N'.
*    ELSEIF gv_zztktprnt = abap_true.
*      CLEAR zarn_reg_hdr-zztktprnt.
*    ENDIF.
*  ENDIF.                                           " ++JKH 01.11.2016

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  GET_AS4SUB_DEPT_TEXT  INPUT
*&---------------------------------------------------------------------*
MODULE get_as4sub_dept_text INPUT.

  IF zarn_reg_hdr-zzas4subdept IS INITIAL.
    CLEAR  gs_text-wwghb.
    RETURN.
  ENDIF.

* Host Art Hierarchy Vs Host depart mapping,
  SELECT SINGLE wwghb INTO gs_text-wwghb
              FROM zmd_host_prdtyp
             WHERE wwgha = zarn_reg_hdr-zzas4subdept.
  IF sy-subrc IS NOT INITIAL.
    CLEAR  gs_text-wwghb.
  ENDIF.

ENDMODULE.                 " GET_AS4SUB_DEPT_TEXT  INPUT
*&---------------------------------------------------------------------*
*&      Module  DEFAULT_LOGIC_SCREEN_0112  INPUT
*&---------------------------------------------------------------------*
*MODULE default_logic_screen_0112 INPUT.
*  PERFORM default_logic_screen_0112.
*ENDMODULE.                 " DEFAULT_LOGIC_SCREEN_0112  INPUT
*&---------------------------------------------------------------------*
*&      Module  USER_COMMAND_0710  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE user_command_0710 INPUT.

*  BREAK-POINT.
ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  CHECK_SEASON  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE check_season INPUT.

  CHECK ( zarn_reg_hdr-saiso IS NOT INITIAL OR zarn_reg_hdr-saisj IS NOT INITIAL ).

  IF gv_display IS INITIAL.
    CALL FUNCTION 'SEASON_YEAR_CHECK_FOR_MARA'
      EXPORTING
        isaiso                 = zarn_reg_hdr-saiso
        iyear                  = zarn_reg_hdr-saisj
*       isaity                 = wmara_saity
      EXCEPTIONS
        season_year_not_exists = 1
        OTHERS                 = 2.
    IF sy-subrc NE 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
        WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.
  ENDIF.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  USER_COMMAND_0112  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE user_command_0112 INPUT.

  DATA ls_zarn_reg_banner LIKE LINE OF gt_zarn_reg_banner.
  DATA: lv_force_update TYPE flag.

  CASE sy-ucomm.

    WHEN 'DEF_BANNER_RET'.

      lv_force_update = gv_banner_def_ret.
      PERFORM default_banners USING gc_banner_def_ret lv_force_update.
      PERFORM default_logic_screen_0112.

    WHEN 'DEF_BANNER_GIL'.
      PERFORM default_banners USING gc_banner_def_gil space.
      PERFORM default_logic_screen_0112.

      " SOS select
    WHEN 'ONSOS_SEL'.
      PERFORM default_banners USING gc_banner_def_ret space.
      IF gv_banner_def_gil IS NOT INITIAL.
        PERFORM default_banners USING gc_banner_def_gil space.
      ENDIF.
      PERFORM default_logic_screen_0112.

      " LNI DC select
    WHEN 'ONLNI_SEL'.
      PERFORM default_banners USING gc_banner_def_ret space.
      IF gv_banner_def_gil IS NOT INITIAL.
        PERFORM default_banners USING gc_banner_def_gil space.
      ENDIF.
      PERFORM default_logic_screen_0112.

      " UNI DC select
    WHEN 'ONUNI_SEL'.
      PERFORM default_banners USING gc_banner_def_ret space.
      IF gv_banner_def_gil IS NOT INITIAL.
        PERFORM default_banners USING gc_banner_def_gil space.
      ENDIF.
      PERFORM default_logic_screen_0112.

      " RETAIL category manager select
    WHEN 'ONRCM_SEL'.
      CLEAR ls_zarn_reg_banner.
      LOOP AT gt_zarn_reg_banner INTO ls_zarn_reg_banner
        WHERE banner EQ zcl_constants=>gc_banner_4000
        OR banner EQ zcl_constants=>gc_banner_5000
        OR banner EQ zcl_constants=>gc_banner_6000.

        EXIT.
      ENDLOOP.

      " in case of initial only
      IF ls_zarn_reg_banner IS INITIAL.
        PERFORM default_banners USING gc_banner_def_ret space.
        PERFORM default_logic_screen_0112.
      ENDIF.

      " WHOLESALE category manager select
    WHEN 'ONWCM_SEL'.
      IF gv_banner_def_gil IS NOT INITIAL.
        CLEAR ls_zarn_reg_banner.
        LOOP AT gt_zarn_reg_banner INTO ls_zarn_reg_banner
          WHERE banner EQ zcl_constants=>gc_banner_3000.
          EXIT.
        ENDLOOP.

        " in case of initial only
        IF ls_zarn_reg_banner IS INITIAL.
          PERFORM default_banners USING gc_banner_def_gil space.
          PERFORM default_logic_screen_0112.
        ENDIF.
      ENDIF.


* INS Begin of Change ONLD-835 JKH 27.04.2017
    WHEN 'PRFTC'.
      PERFORM call_transaction USING 'PRFAM'.

    WHEN 'ZSD_PRF'.
      PERFORM call_transaction USING 'ZSD_PRFAM'.

    WHEN 'PRF'.  " radiobutton
*   check fields
      IF gv_new IS NOT INITIAL.
        zarn_reg_prfam-prfam_status = 1.
      ELSEIF gv_existing IS NOT INITIAL.
        zarn_reg_prfam-prfam_status = 2.
      ELSEIF gv_change IS NOT INITIAL.
        zarn_reg_prfam-prfam_status = 3.
      ENDIF.

      gv_prfam_done_text = TEXT-039. "Done
      CLEAR zarn_reg_prfam-prfam_done_flag.

    WHEN 'PRFAM'.  "Done Pushbutton

      IF gv_prfam_done_text EQ TEXT-039.
        WRITE icon_locked TO gv_prfam_done_text.
        gv_prfam_done_text = gv_prfam_done_text && TEXT-039. "Done
        zarn_reg_prfam-prfam_done_flag = abap_true.
      ELSE.
        gv_prfam_done_text = TEXT-039. "Done
        CLEAR zarn_reg_prfam-prfam_done_flag.
      ENDIF.
* INS End of Change ONLD-835 JKH 27.04.2017

    WHEN OTHERS.

  ENDCASE.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  CHECK_MTART  INPUT
*&---------------------------------------------------------------------*
MODULE check_mtart INPUT.

  PERFORM check_mtart.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  USER_COMMAND_9001  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE user_command_9001 INPUT.

  CASE sy-ucomm.
    WHEN 'CANCEL'.
      CLEAR: gv_no_refresh.
      FREE: gt_open_po[], gt_open_po_disp[], gt_open_so[],
            gt_open_so_disp[], gt_open_po_so_disp[], gt_open_stock[].

      gv_revert_issue_uom = abap_true.

      LEAVE TO SCREEN 0.

    WHEN 'APPROVE'.
      CLEAR: gv_no_refresh, gv_revert_issue_uom.
      FREE: gt_open_po[], gt_open_po_disp[], gt_open_so[],
            gt_open_so_disp[], gt_open_po_so_disp[], gt_open_stock[].

      " Check authorisation object for MD change approval
      LEAVE TO SCREEN 0.

    WHEN OTHERS.
      SET CURSOR FIELD 'CANCEL'.
      gv_no_refresh = abap_true.

  ENDCASE.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  STATUS_9002  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE status_9002 OUTPUT.

  SET TITLEBAR 'CHANGE_NON_SAP_UOM'.
  SET PF-STATUS 'DUMMY'.

  SET CURSOR FIELD 'CANCEL'.

  PERFORM approve_button_auth.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  USER_COMMAND_9002  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE user_command_9002 INPUT.

  CASE sy-ucomm.
    WHEN 'CANCEL'.
      gv_revert_issue_uom = abap_true.
      CLEAR gv_no_refresh.
      LEAVE TO SCREEN 0.

    WHEN 'APPROVE'.
      CLEAR: gv_no_refresh, gv_revert_issue_uom.
      " Check authorisation object for MD change approval
      LEAVE TO SCREEN 0.

    WHEN OTHERS.
      SET CURSOR FIELD 'CANCEL'.
      gv_no_refresh = abap_true.

  ENDCASE.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  CHECK_NETT_CONT  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE check_nett_cont INPUT.

* Net Contents must be filled
  IF zarn_reg_hdr-inhal IS INITIAL.
    MESSAGE e132.
    RETURN.
  ENDIF.    "++CI18-124 JKH 29.06.2017

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  CHECK_NETT_UOM  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE check_nett_uom INPUT.

  CONSTANTS:
    lc_dimid_mass    LIKE t006-dimid VALUE 'MASS',   "Masse bzw. Gewicht
    lc_dimid_length  LIKE t006-dimid VALUE 'LENGTH', "Länge
    lc_dimid_volume  LIKE t006-dimid VALUE 'VOLUME', "Volumen
    lc_dimid_dimless LIKE t006-dimid VALUE 'AAAADL', "dimlos RWA/270798
    lc_dimid_surface LIKE t006-dimid VALUE 'SURFAC'. "Fläche Note403416

  " NETT_CONTENT CROSS CHECK -> NETT_UOM must be filled...
*  IF zarn_reg_hdr-inhal IS NOT INITIAL AND zarn_reg_hdr-inhme IS INITIAL.
*    SET CURSOR FIELD zarn_reg_hdr-inhme.
*    MESSAGE e071.
*    RETURN.
*  ENDIF.    "--CI18-124 JKH 29.06.2017


* Net Contents Unit must be filled
  IF zarn_reg_hdr-inhme IS INITIAL.
    SET CURSOR FIELD zarn_reg_hdr-inhme.
    MESSAGE e071.
    RETURN.
  ENDIF.    "++CI18-124 JKH 29.06.2017


  " NET_UOM should be cross checked with T006-MSEHI.
  IF zarn_reg_hdr-inhme IS NOT INITIAL.

    IF gt_t006 IS INITIAL.
      SELECT * FROM t006 INTO TABLE gt_t006
         WHERE dimid = lc_dimid_mass
            OR dimid = lc_dimid_volume
            OR dimid = lc_dimid_length
            OR dimid = lc_dimid_dimless
            OR dimid = lc_dimid_surface.
    ENDIF.

    READ TABLE gt_t006 TRANSPORTING NO FIELDS WITH KEY msehi = zarn_reg_hdr-inhme.
    IF sy-subrc NE 0.
      SET CURSOR FIELD zarn_reg_hdr-inhme.
      MESSAGE e072 WITH zarn_reg_hdr-inhme.
      RETURN.
    ENDIF.
  ENDIF.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  NETT_INHME_POV  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE nett_inhme_pov INPUT.

  DATA: lv_dsplay TYPE symarky.

  CALL FUNCTION 'MARA_INHME_HELP'
    EXPORTING
      display = lv_dsplay
    IMPORTING
      inhme   = zarn_reg_hdr-inhme.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  USER_COMMAND_0115  INPUT
*&---------------------------------------------------------------------*
MODULE user_command_0115 INPUT.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  115_LAUNCHP_DROPDOWN  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE 115_launchp_dropdown INPUT.

  DATA: BEGIN OF ls_launchp_dropdown,
          domvalue_l TYPE domvalue_l,
          ddtext     TYPE ddtext,
        END OF ls_launchp_dropdown,
        lt_launchp_dropdown LIKE TABLE OF ls_launchp_dropdown.

  REFRESH lt_launchp_dropdown.
  IF lt_launchp_dropdown IS INITIAL.
    LOOP AT gt_zarn_launchp_t INTO DATA(ls_zarn_launchp).
      ls_launchp_dropdown-domvalue_l = ls_zarn_launchp-npd_launch_priority.
      ls_launchp_dropdown-ddtext     = ls_zarn_launchp-launch_prio_desc.
      APPEND ls_launchp_dropdown TO lt_launchp_dropdown.
    ENDLOOP.
  ENDIF.

  CALL FUNCTION 'F4IF_INT_TABLE_VALUE_REQUEST'
    EXPORTING
      retfield  = 'DOMVALUE_L'
      dynpprog  = sy-repid
      dynpnr    = sy-dynnr
      value_org = 'S'
    TABLES
      value_tab = lt_launchp_dropdown.

  IF sy-subrc <> 0.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
    WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.

ENDMODULE.

*&---------------------------------------------------------------------*
*&      Module  115_DELCODE_DROPDOWN  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE 115_delcode_dropdown INPUT.

  DATA: BEGIN OF ls_delcode_dropdown,
          domvalue_l TYPE domvalue_l,
          ddtext     TYPE ddtext,
        END OF ls_delcode_dropdown,
        lt_delcode_dropdown LIKE TABLE OF ls_delcode_dropdown.

  IF lt_delcode_dropdown IS INITIAL.
    LOOP AT gt_zarn_npd_del_t INTO DATA(ls_zarn_delcode).
      ls_delcode_dropdown-domvalue_l = ls_zarn_delcode-del_code.
      ls_delcode_dropdown-ddtext     = ls_zarn_delcode-del_code_desc.
      APPEND ls_delcode_dropdown TO lt_delcode_dropdown.
    ENDLOOP.

    CALL FUNCTION 'F4IF_INT_TABLE_VALUE_REQUEST'
      EXPORTING
        retfield  = 'DOMVALUE_L'
        value_org = 'S'
      TABLES
        value_tab = lt_delcode_dropdown.

    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
      WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.
  ENDIF.

ENDMODULE.

*&---------------------------------------------------------------------*
*&      Module  115_ORDERAPPROACH_DROPDOWN  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE 115_orderapproach_dropdown INPUT.

  DATA: BEGIN OF ls_orderapproach_dropdown,
          domvalue_l TYPE domvalue_l,
          ddtext     TYPE ddtext,
        END OF ls_orderapproach_dropdown,
        lt_orderapproach_dropdown LIKE TABLE OF ls_orderapproach_dropdown.

  IF lt_orderapproach_dropdown IS INITIAL.
    LOOP AT gt_zarn_order_app_t INTO DATA(ls_zarn_orderapproach).
      ls_orderapproach_dropdown-domvalue_l = ls_zarn_orderapproach-order_approach.
      ls_orderapproach_dropdown-ddtext     = ls_zarn_orderapproach-order_appr_desc.
      APPEND ls_orderapproach_dropdown TO lt_orderapproach_dropdown.
    ENDLOOP.

    CALL FUNCTION 'F4IF_INT_TABLE_VALUE_REQUEST'
      EXPORTING
        retfield  = 'DOMVALUE_L'
        value_org = 'S'
      TABLES
        value_tab = lt_orderapproach_dropdown.

    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
      WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.
  ENDIF.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  115_DATE_VALIDATION INPUT
*&---------------------------------------------------------------------*
*       Dates should not be in the past
*----------------------------------------------------------------------*
MODULE 115_date_validation INPUT.

* Retrieve current copy of database ZARN_REG_SC
* Check should only occur when user enters a new value and not try and
* validate an existing value that has been previously saved
  SELECT SINGLE * FROM zarn_reg_sc
    INTO @DATA(ls_zarn_reg_sc_db)
    WHERE idno = @zarn_reg_sc-idno.

  IF ls_zarn_reg_sc_db-launch_date NE zarn_reg_sc-launch_date
    AND zarn_reg_sc-launch_date < sy-datum
    AND zarn_reg_sc-launch_date IS NOT INITIAL.
    SET CURSOR FIELD 'ZARN_REG_SC-LAUNCH_DATE'.
    MESSAGE e136.
    RETURN.
  ENDIF.

  IF ls_zarn_reg_sc_db-dc_ord_date NE zarn_reg_sc-dc_ord_date
    AND zarn_reg_sc-dc_ord_date < sy-datum
    AND zarn_reg_sc-dc_ord_date IS NOT INITIAL.
    SET CURSOR FIELD 'ZARN_REG_SC-DC_ORD_DATE'.
    MESSAGE e136.
    RETURN.
  ENDIF.

  IF ls_zarn_reg_sc_db-dc_gr_due_date NE zarn_reg_sc-dc_gr_due_date
  AND zarn_reg_sc-dc_gr_due_date < sy-datum
  AND zarn_reg_sc-dc_gr_due_date IS NOT INITIAL.
    SET CURSOR FIELD 'ZARN_REG_SC-DC_GR_DUE_DATE'.
    MESSAGE e136.
    RETURN.
  ENDIF.

  IF ls_zarn_reg_sc_db-dc_stk_rel_date NE zarn_reg_sc-dc_stk_rel_date
  AND zarn_reg_sc-dc_stk_rel_date < sy-datum
  AND zarn_reg_sc-dc_stk_rel_date IS NOT INITIAL.
    SET CURSOR FIELD 'ZARN_REG_SC-DC_STK_REL_DATE'.
    MESSAGE e136.
    RETURN.
  ENDIF.

  IF ls_zarn_reg_sc_db-store_po_date NE zarn_reg_sc-store_po_date
  AND zarn_reg_sc-store_po_date < sy-datum
  AND zarn_reg_sc-store_po_date IS NOT INITIAL.
    SET CURSOR FIELD 'ZARN_REG_SC-STORE_PO_DATE'.
    MESSAGE e136.
    RETURN.
  ENDIF.

  IF ls_zarn_reg_sc_db-first_promo_date NE zarn_reg_sc-first_promo_date
  AND zarn_reg_sc-first_promo_date < sy-datum
  AND zarn_reg_sc-first_promo_date IS NOT INITIAL.
    SET CURSOR FIELD 'ZARN_REG_SC-FIRST_PROMO_DATE'.
    MESSAGE e136.
    RETURN.
  ENDIF.

  IF ls_zarn_reg_sc_db-store_comms_date NE zarn_reg_sc-store_comms_date
  AND zarn_reg_sc-store_comms_date < sy-datum
  AND zarn_reg_sc-store_comms_date IS NOT INITIAL.
    SET CURSOR FIELD 'ZARN_REG_SC-LAUNCH_DATE'.
    MESSAGE w136.
  ENDIF.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  PBO_0112  OUTPUT
*&---------------------------------------------------------------------*
* Regional Banner
*----------------------------------------------------------------------*
MODULE pbo_0112 OUTPUT.
  PERFORM banner_and_cluster_alv.
ENDMODULE.
