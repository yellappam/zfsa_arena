*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_ARTICLE_LINK_TOP
*&---------------------------------------------------------------------*


TABLES: mara, makt, zarn_reg_artlink.

TYPES:
  BEGIN OF ty_s_mara,
    matnr TYPE matnr,
    matkl TYPE matkl,
    attyp TYPE attyp,
    mtart TYPE mtart,
    maktx TYPE maktx,
  END OF ty_s_mara .
TYPES:
  ty_t_mara TYPE SORTED TABLE OF ty_s_mara WITH UNIQUE KEY matnr .


DATA: gt_reg_artlink TYPE ztarn_reg_artlink,
      gt_mara        TYPE ty_t_mara,
      gt_hdr_scn_t   TYPE ztarn_hdr_scn_t,
      gt_link_scn_t  TYPE ztarn_link_scn_t,
      gt_output      TYPE ztarn_article_link_alv,

      go_docking     TYPE REF TO cl_gui_docking_container,  "Custom container instance reference
      go_alvgrid     TYPE REF TO cl_gui_alv_grid.
