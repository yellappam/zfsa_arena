FUNCTION zarn_shlp_exit_gln.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  TABLES
*"      SHLP_TAB TYPE  SHLP_DESCT
*"      RECORD_TAB STRUCTURE  SEAHLPRES
*"  CHANGING
*"     VALUE(SHLP) TYPE  SHLP_DESCR
*"     VALUE(CALLCONTROL) LIKE  DDSHF4CTRL STRUCTURE  DDSHF4CTRL
*"----------------------------------------------------------------------

  TYPES: BEGIN OF ty_vend,
           lifnr TYPE lfa1-lifnr,
           name1 TYPE lfa1-name1,
           name2 TYPE lfa1-name2,
           bbbnr TYPE lfa1-bbbnr,
           bbsnr TYPE lfa1-bbsnr,
           bubkz TYPE lfa1-bubkz,
           bukrs TYPE lfb1-bukrs,
         END OF ty_vend.


  DATA: lv_gln    TYPE zarn_gln,
        lt_vend   TYPE STANDARD TABLE OF ty_vend,
        ls_record LIKE LINE OF record_tab,
        lv_bbbnr  TYPE char7,
        lv_bbsnr  TYPE char5,
        lv_bubkz  TYPE char1.

  RANGES: lr_lifnr FOR lfa1-lifnr,
          lr_bbbnr FOR lv_bbbnr,
          lr_bbsnr FOR lv_bbsnr,
          lr_bubkz FOR lv_bubkz,
          lr_bukrs FOR lfb1-bukrs.

  FIELD-SYMBOLS: <ls_shlp_tab>   LIKE LINE OF shlp_tab,
                 <ls_interface>  LIKE LINE OF <ls_shlp_tab>-interface,
                 <ls_record_tab> LIKE LINE OF record_tab,
                 <ls_vend>       LIKE LINE OF lt_vend.

  IF callcontrol-step EQ 'SELECT'.
    REFRESH record_tab.
    LOOP AT shlp_tab ASSIGNING <ls_shlp_tab>.
      LOOP AT <ls_shlp_tab>-interface ASSIGNING <ls_interface>.
        CASE <ls_interface>-shlpfield.
          WHEN 'GLN'.

            IF NOT <ls_interface>-value+0(7) IS INITIAL.
              lr_bbbnr-sign = 'I'.
              IF <ls_interface>-value CS '*'.
                lr_bbbnr-option = 'CP'.
              ELSE.
                lr_bbbnr-option = 'EQ'.
              ENDIF.
              lr_bbbnr-low = <ls_interface>-value+0(7).
              APPEND lr_bbbnr.
            ENDIF.

            IF NOT <ls_interface>-value+7(5) IS INITIAL.
              lr_bbsnr-sign = 'I'.
              IF <ls_interface>-value CS '*'.
                lr_bbsnr-option = 'CP'.
              ELSE.
                lr_bbsnr-option = 'EQ'.
              ENDIF.
              lr_bbsnr-low = <ls_interface>-value+7(5).
              APPEND lr_bbsnr.
            ENDIF.

            IF NOT <ls_interface>-value+12(1) IS INITIAL.
              lr_bubkz-sign = 'I'.
              IF <ls_interface>-value CS '*'.
                lr_bubkz-option = 'CP'.
              ELSE.
                lr_bubkz-option = 'EQ'.
              ENDIF.
              lr_bubkz-low = <ls_interface>-value+12(1).
              APPEND lr_bubkz.
            ENDIF.
          WHEN 'LIFNR'.
            IF NOT <ls_interface>-value IS INITIAL.
              lr_lifnr-sign = 'I'.
              IF <ls_interface>-value CS '*'.
                lr_lifnr-option = 'CP'.
              ELSE.
                lr_lifnr-option = 'EQ'.
              ENDIF.
              lr_lifnr-low = <ls_interface>-value.
              CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'
                EXPORTING
                  input  = lr_lifnr-low
                IMPORTING
                  output = lr_lifnr-low.
              APPEND lr_lifnr.
            ENDIF.
          WHEN 'BUKRS'.
            IF NOT <ls_interface>-value IS INITIAL.
              lr_bukrs-sign = 'I'.
              IF <ls_interface>-value CS '*'.
                lr_bukrs-option = 'CP'.
              ELSE.
                lr_bukrs-option = 'EQ'.
              ENDIF.
              lr_bukrs-low = <ls_interface>-value.
              APPEND lr_bukrs.
            ENDIF.
        ENDCASE.
      ENDLOOP.
    ENDLOOP.

    SELECT l~lifnr l~name1 l~name2 l~bbbnr l~bbsnr l~bubkz b~bukrs
      INTO CORRESPONDING FIELDS OF TABLE lt_vend
      FROM lfa1 AS l
      INNER JOIN lfb1 AS b
        ON b~lifnr EQ l~lifnr
      WHERE l~lifnr IN lr_lifnr
      AND   l~bbbnr IN lr_bbbnr
      AND   l~bbsnr IN lr_bbsnr
      AND   l~bubkz IN lr_bubkz
      AND   b~bukrs IN lr_bukrs.
    DELETE lt_vend WHERE bbbnr IS INITIAL.
    LOOP AT lt_vend ASSIGNING <ls_vend>.
      CLEAR ls_record-string.
      CONCATENATE
        <ls_vend>-bbbnr
        <ls_vend>-bbsnr
        <ls_vend>-bubkz
        INTO lv_gln.
      ls_record-string+0(13)  = lv_gln.
      ls_record-string+13(4)  = <ls_vend>-bukrs.
      ls_record-string+17(10) = <ls_vend>-lifnr.
      ls_record-string+27(35) = <ls_vend>-name1.
      ls_record-string+62(35) = <ls_vend>-name2.
      APPEND ls_record TO record_tab.
    ENDLOOP.
    callcontrol-step = 'DISP'.
  ENDIF.

ENDFUNCTION.
