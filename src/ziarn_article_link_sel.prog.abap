*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_ARTICLE_LINK_SEL
*&---------------------------------------------------------------------*

SELECTION-SCREEN BEGIN OF BLOCK b1 WITH FRAME TITLE text-001.

SELECT-OPTIONS: s_scn   FOR zarn_reg_artlink-hdr_scn,
                s_matnr FOR mara-matnr,
                s_maktx FOR makt-maktg,
                s_matkl FOR mara-matkl,
                s_mtart FOR mara-mtart,
                s_attyp FOR mara-attyp.

SELECTION-SCREEN END OF BLOCK b1.


AT SELECTION-SCREEN.

  IF s_scn[]   IS INITIAL AND
     s_matnr[] IS INITIAL AND
     s_maktx[] IS INITIAL AND
     s_matkl[] IS INITIAL AND
     s_mtart[] IS INITIAL AND
     s_attyp[] IS INITIAL.
    MESSAGE e122.
  ENDIF.
