*&---------------------------------------------------------------------*
*& Report  ZRARN_REG_SYNCUP
*&
*&---------------------------------------------------------------------*
*&
*&
*&---------------------------------------------------------------------*
*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Functional Specification 3136
* DATE WRITTEN     # 04052016
* SAP VERSION      # 731
* TYPE             # Executable Report
* AUTHOR           # C89991021, Ashish Kumar
*-----------------------------------------------------------------------
* TITLE            # Regional Table Sync up
* PURPOSE          # This program Syncs the Regional Tables from National
*                  # Tables and SAP master data tables based on customising
*                  # settings defining the fields to be updated...
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      # Tcode: ZARN_REG_SYNCUP
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 24.04.2018
* CHANGE No.       # ChaRM DE0K917906; JIRA CI18-51 Pay By Scan Changes
* DESCRIPTION      # Pay by Scan should be set not only at the banner
*                  # level but also at the individual store level. The
*                  # previous implementation was to allow it to be set
*                  # only at the banner level on MVKE-MVGR3. The new
*                  # implementation is to add custom field ZZ_PBS to MARC.
*                  # 1. Removed MVGR3 related logic
*                  # 2. Added logic for MARC-ZZ_PBS.
* WHO              # I00600343, Fengyu Li
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------
REPORT zrarn_reg_syncup.

INCLUDE zarn_reg_syncup_top.
INCLUDE zarn_reg_syncup_scr.
INCLUDE zarn_reg_syncup_migrate.
INCLUDE zarn_reg_syncup_bau.
INCLUDE zarn_reg_syncup_main.
