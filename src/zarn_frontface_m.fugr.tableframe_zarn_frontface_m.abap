*---------------------------------------------------------------------*
*    program for:   TABLEFRAME_ZARN_FRONTFACE_M
*   generation date: 24.08.2018 at 13:21:17
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
FUNCTION TABLEFRAME_ZARN_FRONTFACE_M   .

  PERFORM TABLEFRAME TABLES X_HEADER X_NAMTAB DBA_SELLIST DPL_SELLIST
                            EXCL_CUA_FUNCT
                     USING  CORR_NUMBER VIEW_ACTION VIEW_NAME.

ENDFUNCTION.
