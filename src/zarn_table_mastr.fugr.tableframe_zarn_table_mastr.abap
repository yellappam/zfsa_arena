*---------------------------------------------------------------------*
*    program for:   TABLEFRAME_ZARN_TABLE_MASTR
*   generation date: 05.05.2016 at 10:20:41 by user C90001363
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
FUNCTION TABLEFRAME_ZARN_TABLE_MASTR   .

  PERFORM TABLEFRAME TABLES X_HEADER X_NAMTAB DBA_SELLIST DPL_SELLIST
                            EXCL_CUA_FUNCT
                     USING  CORR_NUMBER VIEW_ACTION VIEW_NAME.

ENDFUNCTION.
