* regenerated at 19.07.2016 11:09:55
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_PROD_UOM_TTOP.               " Global Data
  INCLUDE LZARN_PROD_UOM_TUXX.               " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_PROD_UOM_TF...               " Subroutines
* INCLUDE LZARN_PROD_UOM_TO...               " PBO-Modules
* INCLUDE LZARN_PROD_UOM_TI...               " PAI-Modules
* INCLUDE LZARN_PROD_UOM_TE...               " Events
* INCLUDE LZARN_PROD_UOM_TP...               " Local class implement.
* INCLUDE LZARN_PROD_UOM_TT99.               " ABAP Unit tests
  INCLUDE LZARN_PROD_UOM_TF00                     . " subprograms
  INCLUDE LZARN_PROD_UOM_TI00                     . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
