*-----------------------------------------------------------------------
* PROJECT          # Online
* SPECIFICATION    # FD 3160 - Mass approval of change documents
* DATE WRITTEN     # 11.01.2017
* SAP VERSION      # 740
* TYPE             # Program
* AUTHOR           # C90001363, Jitin Kharbanda
*-----------------------------------------------------------------------
* TITLE            # Mass Approval of Change Categories
* PURPOSE          # Support mass approval of change categories for multiple
*                  # Arena Records
*                  #
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      #
*                  #
*-----------------------------------------------------------------------
* CALLS TO         #
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------
REPORT zrarn_approvals_mass MESSAGE-ID zarena_msg.


INCLUDE ziarn_approvals_mass_top.
INCLUDE ziarn_approvals_mass_sel.
INCLUDE ziarn_approvals_mass_class.
INCLUDE ziarn_approvals_mass_i01.
INCLUDE ziarn_approvals_mass_o01.
INCLUDE ziarn_approvals_mass_f01.
INCLUDE ziarn_approvals_mass_f02.
INCLUDE ziarn_approvals_mass_main.
