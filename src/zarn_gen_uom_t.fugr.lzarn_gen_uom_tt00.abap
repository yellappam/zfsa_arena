*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 05.05.2016 at 16:36:52 by user C90001363
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_GEN_UOM_T..................................*
DATA:  BEGIN OF STATUS_ZARN_GEN_UOM_T                .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_GEN_UOM_T                .
CONTROLS: TCTRL_ZARN_GEN_UOM_T
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_GEN_UOM_T                .
TABLES: ZARN_GEN_UOM_T                 .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
