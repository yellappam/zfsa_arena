*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 24.02.2017 at 10:28:23
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_REL_MATRIX.................................*
DATA:  BEGIN OF STATUS_ZARN_REL_MATRIX               .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_REL_MATRIX               .
CONTROLS: TCTRL_ZARN_REL_MATRIX
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_REL_MATRIX               .
TABLES: ZARN_REL_MATRIX                .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
