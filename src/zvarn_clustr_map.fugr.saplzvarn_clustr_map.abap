* regenerated at 24.06.2020 13:53:36
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZVARN_CLUSTR_MAPTOP.              " Global Declarations
  INCLUDE LZVARN_CLUSTR_MAPUXX.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZVARN_CLUSTR_MAPF...              " Subroutines
* INCLUDE LZVARN_CLUSTR_MAPO...              " PBO-Modules
* INCLUDE LZVARN_CLUSTR_MAPI...              " PAI-Modules
* INCLUDE LZVARN_CLUSTR_MAPE...              " Events
* INCLUDE LZVARN_CLUSTR_MAPP...              " Local class implement.
* INCLUDE LZVARN_CLUSTR_MAPT99.              " ABAP Unit tests
  INCLUDE LZVARN_CLUSTR_MAPF00                    . " subprograms
  INCLUDE LZVARN_CLUSTR_MAPI00                    . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules

INCLUDE lzvarn_clustr_mapf01.
