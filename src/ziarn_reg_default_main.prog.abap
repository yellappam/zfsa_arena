*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_REG_DEFAULT_MAIN
*&---------------------------------------------------------------------*



START-OF-SELECTION.

* Execute main process
  PERFORM execute CHANGING sy-subrc.

END-OF-SELECTION.

* Write Log
  PERFORM write_log.
