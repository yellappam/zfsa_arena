*&---------------------------------------------------------------------*
*&  Include           ZARN_HYBRIS_GET_DEF
*&---------------------------------------------------------------------*

CONSTANTS: co_arena_inbound TYPE zarn_id_type VALUE 'M'.

DATA: gt_hybris     TYPE STANDARD TABLE OF zarn_hybris,
      gt_hybris_res TYPE STANDARD TABLE OF zarn_hybris_result.
