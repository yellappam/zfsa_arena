* regenerated at 05.05.2016 15:42:40 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_PREPSTATE_TTOP.              " Global Data
  INCLUDE LZARN_PREPSTATE_TUXX.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_PREPSTATE_TF...              " Subroutines
* INCLUDE LZARN_PREPSTATE_TO...              " PBO-Modules
* INCLUDE LZARN_PREPSTATE_TI...              " PAI-Modules
* INCLUDE LZARN_PREPSTATE_TE...              " Events
* INCLUDE LZARN_PREPSTATE_TP...              " Local class implement.
* INCLUDE LZARN_PREPSTATE_TT99.              " ABAP Unit tests
  INCLUDE LZARN_PREPSTATE_TF00                    . " subprograms
  INCLUDE LZARN_PREPSTATE_TI00                    . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
