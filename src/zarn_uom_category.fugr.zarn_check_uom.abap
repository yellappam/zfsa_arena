FUNCTION ZARN_CHECK_UOM.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(LOWER_UOM) TYPE  MEINH
*"     VALUE(HIGHER_UOM) TYPE  MEINH
*"  EXPORTING
*"     REFERENCE(RESULT) TYPE  CHAR005
*"  EXCEPTIONS
*"      INVALID_INPUT
*"----------------------------------------------------------------------
DATA: lv_msehi1 TYPE t006-msehi,
      lv_msehi2 TYPE t006-msehi,
      lv_seqlow TYPE SQNUR,
      lv_seqhgh TYPE SQNUR.

SELECT SINGLE msehi FROM t006 into lv_msehi1 where msehi = LOWER_UOM.
SELECT SINGLE msehi FROM t006 into lv_msehi2 where msehi = HIGHER_UOM.

IF lv_msehi1 IS INITIAL OR lv_msehi2 IS INITIAL.
  MESSAGE e100(ZARENA_BUSINESS_RULE) RAISING INVALID_INPUT.
ENDIF.

SELECT SINGLE CAT_SEQNO FROM ZARN_UOM_CAT INTO lv_seqlow
  WHERE UOM = LOWER_UOM.

SELECT SINGLE CAT_SEQNO FROM ZARN_UOM_CAT INTO lv_seqhgh
  WHERE UOM = HIGHER_UOM.


  IF lv_seqlow LT lv_seqhgh.
    RESULT = 'TRUE'.
  ELSEIF lv_seqlow = lv_seqhgh.
    RESULT = 'EQUAL'.
  ELSE.
    RESULT = 'FALSE'.
  ENDIF.

CLEAR: lv_msehi1, lv_msehi2, lv_seqlow, lv_seqhgh.




ENDFUNCTION.
