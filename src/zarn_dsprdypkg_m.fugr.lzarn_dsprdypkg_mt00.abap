*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 24.08.2018 at 13:15:25
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_DSPRDYPKG..................................*
DATA:  BEGIN OF STATUS_ZARN_DSPRDYPKG                .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_DSPRDYPKG                .
CONTROLS: TCTRL_ZARN_DSPRDYPKG
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_DSPRDYPKG                .
TABLES: *ZARN_DSPRDYPKG_T              .
TABLES: ZARN_DSPRDYPKG                 .
TABLES: ZARN_DSPRDYPKG_T               .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
