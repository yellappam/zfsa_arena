FUNCTION-POOL zarn_article_post MESSAGE-ID zarena_msg.
*-----------------------------------------------------------------------
* DATE             # 06.06.2018
* CHANGE No.       # ChaRM 9000003675; JIRA CI18-506 NEW Range Status flag
* DESCRIPTION      # New Range Flag, Range Availability and Range Detail
*                  # fields added in Arena and on material master in Listing
*                  # tab and Basic Data Text tab (materiallongtext).
* WHO              # C90005557, Tessa Newman
*-----------------------------------------------------------------------

CONSTANTS:

* Ranges
  BEGIN OF gc_range,
    sign_i    TYPE rsparams-sign   VALUE 'I',
    option_eq TYPE rsparams-option VALUE 'EQ',
    option_bt TYPE rsparams-option VALUE 'BT',
    option_cp TYPE rsparams-option VALUE 'CP',
  END OF gc_range,

* Message
  BEGIN OF gc_message,
    error   TYPE sy-msgty        VALUE 'E',
    success TYPE sy-msgty        VALUE 'S',
    warning TYPE sy-msgty        VALUE 'W',
    info    TYPE sy-msgty        VALUE 'I',
    id      TYPE sy-msgid        VALUE 'ZARENA_MSG',
  END OF gc_message,

* Status
  BEGIN OF gc_status,
    apr TYPE zarn_version_status VALUE 'APR',
    cre TYPE zarn_version_status VALUE 'CRE',
  END OF gc_status,

* TVARVC
  BEGIN OF gc_tvarvc,
    vkorg_uni    TYPE rvari_vnam  VALUE 'ZC_REF_VKORG_VTREFS',
    vkorg_lni    TYPE rvari_vnam  VALUE 'ZMD_VKORG_REF_SITES_LNI',
    eine_ekorg   TYPE rvari_vnam  VALUE 'ZARN_ARTICLE_POST_EINE_EKORG',
    uom_hyb_code TYPE rvari_vnam  VALUE 'ZARN_GUI_UOM_ADDITION_SCENARIO',
    type_s       TYPE rsscr_kind  VALUE 'S',
    type_p       TYPE rsscr_kind  VALUE 'P',
  END OF gc_tvarvc,

* Banner
  BEGIN OF gc_banner,
    rfst TYPE werks_d     VALUE 'RFST',
    rfdc TYPE werks_d     VALUE 'RFDC',
    rfgl TYPE werks_d     VALUE 'RFGL',
    rfnw TYPE werks_d     VALUE 'RFNW',
    rfps TYPE werks_d     VALUE 'RFPS',
    rfsq TYPE werks_d     VALUE 'RFSQ',
    rlgl TYPE werks_d     VALUE 'RLGL',
    rlnw TYPE werks_d     VALUE 'RLNW',
    rlps TYPE werks_d     VALUE 'RLPS',
    rlsq TYPE werks_d     VALUE 'RLSQ',
    1000 TYPE werks_d     VALUE '1000',
    3000 TYPE werks_d     VALUE '3000',
    4000 TYPE werks_d     VALUE '4000',
    5000 TYPE werks_d     VALUE '5000',
    6000 TYPE werks_d     VALUE '6000',
  END OF gc_banner,

* BAPI constants
  BEGIN OF gc_bapi,
    func_005 TYPE msgfn      VALUE '005',
    func_z01 TYPE msgfn      VALUE 'Z01',
    material LIKE thead-tdobject VALUE 'MATERIAL',
    basictxt LIKE thead-tdname VALUE 'GRUN',
  END OF gc_bapi,

* UOM Category
  BEGIN OF gc_uom_cat,
    inner   TYPE zarn_uom_category VALUE 'INNER',
    shipper TYPE zarn_uom_category VALUE 'SHIPPER',
    retail  TYPE zarn_uom_category VALUE 'RETAIL',
    layer   TYPE zarn_uom_category VALUE 'LAYER',
    pallet  TYPE zarn_uom_category VALUE 'PALLET',
  END OF gc_uom_cat,


  " AReNa Filter value for BADI implementations
  gc_arn_badi_callmode TYPE wrfcallmode       VALUE '14'.

TYPES: BEGIN OF ts_cluster_map,
         field_name TYPE fieldname,
         cluster    TYPE zmd_e_cluster,
       END   OF ts_cluster_map,
       tt_cluster_map TYPE SORTED TABLE OF ts_cluster_map WITH UNIQUE KEY field_name.

TYPES: ty_s_ver_status  TYPE zarn_ver_status,
       ty_t_ver_status  TYPE SORTED TABLE OF ty_s_ver_status WITH UNIQUE KEY idno,

       ty_s_prd_version TYPE zarn_prd_version,
       ty_t_prd_version TYPE SORTED TABLE OF ty_s_prd_version WITH UNIQUE KEY idno version,

       ty_s_reg_hdr     TYPE zarn_reg_hdr,
       ty_t_reg_hdr     TYPE SORTED TABLE OF ty_s_reg_hdr WITH UNIQUE KEY idno,

       ty_s_products    TYPE zarn_products,
       ty_t_products    TYPE SORTED TABLE OF ty_s_products WITH UNIQUE KEY idno version,

       ty_t_key         TYPE SORTED TABLE OF zsarn_key     WITH NON-UNIQUE KEY idno,
       ty_t_reg_key     TYPE SORTED TABLE OF zsarn_reg_key WITH NON-UNIQUE KEY idno,

       ty_s_mara        TYPE mara,
       ty_t_mara        TYPE SORTED TABLE OF ty_s_mara WITH UNIQUE KEY matnr,

       ty_s_marm        TYPE marm,
       ty_t_marm        TYPE SORTED TABLE OF ty_s_marm WITH UNIQUE KEY matnr meinh,

       ty_s_mean        TYPE mean,
       ty_t_mean        TYPE SORTED TABLE OF ty_s_mean WITH UNIQUE KEY matnr meinh lfnum,

       ty_s_maw1        TYPE maw1,
       ty_t_maw1        TYPE SORTED TABLE OF ty_s_maw1 WITH UNIQUE KEY matnr,

       ty_s_eina        TYPE eina,
       ty_t_eina        TYPE SORTED TABLE OF ty_s_eina WITH NON-UNIQUE KEY matnr lifnr,

       ty_s_eine        TYPE eine,
       ty_t_eine        TYPE SORTED TABLE OF ty_s_eine WITH NON-UNIQUE KEY infnr ekorg,

       ty_s_lfa1        TYPE lfa1,
       ty_t_lfa1        TYPE SORTED TABLE OF ty_s_lfa1 WITH UNIQUE KEY lifnr,

       ty_s_lfm1        TYPE lfm1,
       ty_t_lfm1        TYPE SORTED TABLE OF ty_s_lfm1 WITH UNIQUE KEY lifnr ekorg,

       ty_s_host_sap    TYPE zmd_host_sap,
       ty_t_host_sap    TYPE SORTED TABLE OF ty_s_host_sap WITH UNIQUE KEY matnr product,

       ty_s_uom_cat     TYPE zarn_uom_cat,
       ty_t_uom_cat     TYPE SORTED TABLE OF ty_s_uom_cat WITH UNIQUE KEY uom_category seqno uom,

       ty_t_coo_t       TYPE SORTED TABLE OF zarn_coo_t WITH UNIQUE KEY country_of_origin spras,
       ty_t_gen_uom_t   TYPE SORTED TABLE OF zarn_gen_uom_t WITH UNIQUE KEY gen_uom spras,

* Article Hierarchy
       ty_s_matgrp_hier TYPE wrf_matgrp_hier,

* Hierarchy-Product Assignment Table
       ty_s_matgrp_prod TYPE wrf_matgrp_prod,
       ty_t_matgrp_prod TYPE wrf_t_matgrp_prod,

* Article Hierarchy Data
       ty_s_art_hier    TYPE zsarn_art_hier,
       ty_t_art_hier    TYPE ztarn_art_hier,

       BEGIN OF ty_s_tvarvc_uom,
         value TYPE tvarv_val,
       END OF ty_s_tvarvc_uom,

* Units of Measurement
       ty_s_t006  TYPE t006,
       ty_t_t006  TYPE SORTED TABLE OF ty_s_t006 WITH UNIQUE KEY msehi,

* Countries
       ty_s_t005  TYPE t005,
       ty_t_t005  TYPE SORTED TABLE OF ty_s_t005 WITH UNIQUE KEY land1,

* Currency
       ty_s_tcurc TYPE tcurc,
       ty_t_tcurc TYPE SORTED TABLE OF ty_s_tcurc WITH UNIQUE KEY waers,


* FAN ID
       BEGIN OF ty_s_fan_id,
         idno   TYPE zarn_idno,
         fan_id TYPE zarn_fan_id,
       END OF ty_s_fan_id,
       ty_t_fan_id    TYPE SORTED TABLE OF ty_s_fan_id WITH NON-UNIQUE KEY idno,

* Article Types
       ty_s_t134      TYPE t134,
       ty_t_t134      TYPE SORTED TABLE OF ty_s_t134 WITH NON-UNIQUE KEY mtart,


* IDNO Data
       ty_s_idno_data TYPE zsarn_idno_data,
       ty_t_idno_data TYPE SORTED TABLE OF ty_s_idno_data WITH UNIQUE KEY idno.
