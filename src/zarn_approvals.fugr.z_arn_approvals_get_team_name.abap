FUNCTION z_arn_approvals_get_team_name.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(APPROVAL_TEAM) TYPE  ZARN_TEAM_CODE
*"  EXPORTING
*"     REFERENCE(APPROVAL_TEAM_NAME) TYPE  ZARN_TEAM_NAME
*"----------------------------------------------------------------------

  STATICS: lt_arn_teams TYPE HASHED TABLE OF zarn_teams WITH UNIQUE KEY team_code.

  FIELD-SYMBOLS: <ls_arn_team> LIKE LINE OF lt_arn_teams.

  IF lt_arn_teams[] IS INITIAL.
    SELECT *
      INTO TABLE lt_arn_teams
      FROM zarn_teams.
  ENDIF.

  CLEAR approval_team_name.

  READ TABLE lt_arn_teams ASSIGNING <ls_arn_team> WITH TABLE KEY team_code = approval_team.
  IF sy-subrc EQ 0.
    approval_team_name = <ls_arn_team>-team_name.
  ENDIF.

ENDFUNCTION.
