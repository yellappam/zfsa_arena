class ZCL_ARENA_HELPER definition
  public
  final
  create private .

public section.

  class-methods GET_INSTANCE
    returning
      value(RO_INSTANCE) type ref to ZCL_ARENA_HELPER .
  methods POST_LAYOUT_MODULE
    importing
      !IV_ARTICLE type MATNR
      !IT_REG_CLUSTER type ZTARN_REG_CLUSTER
    exporting
      !ET_MESSAGE type BAPIRET2_T .
protected section.
private section.

  types:
    BEGIN OF ts_article_layout,
      article       TYPE matnr,
      layout_module TYPE laygr,
      unit          TYPE meinh,
      cluster       TYPE zmd_e_cluster,
      sales_org     TYPE vkorg,
    END   OF ts_article_layout .
  types:
    tt_article_layout TYPE SORTED TABLE OF ts_article_layout WITH NON-UNIQUE KEY article sales_org cluster layout_module .
  types:
    BEGIN OF ts_cluster_map,
      field_name TYPE fieldname,
      cluster    TYPE zmd_e_cluster,
    END   OF ts_cluster_map .
  types:
    tt_cluster_map TYPE SORTED TABLE OF ts_cluster_map WITH UNIQUE KEY field_name .

  class-data SO_SINGLETON type ref to ZCL_ARENA_HELPER .
  data MT_ARTICLE_LAYOUT type TT_ARTICLE_LAYOUT .

  methods ASSIGN_LAYOUT_MODULE
    importing
      !IV_ARTICLE type MATNR
      !IV_LAYOUT_MODULE type LAYGR
      !IV_BANNER type VKORG
      !IV_CLUSTER type ZMD_E_CLUSTER
    changing
      !CT_MESSAGE type BAPIRET2_T .
  methods READ_ARTICLE_LAYOUT
    importing
      !IV_ARTICLE type MATNR .
  methods ADD_END_OF_CLUSTER_PROCESSING
    importing
      !IV_CLUSTER type ZMD_E_CLUSTER
      !IV_BANNER type VKORG optional
    changing
      !CT_MESSAGE type BAPIRET2_T .
ENDCLASS.



CLASS ZCL_ARENA_HELPER IMPLEMENTATION.


METHOD add_end_of_cluster_processing.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 31.07.2020
* SYSTEM           # DE0
* SPECIFICATION    # SSM-1 NW Range-ZARN_GUI CHANGE
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Add end of cluster processing message
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  MESSAGE i157 WITH iv_cluster iv_banner INTO zcl_message_services=>sv_msg_dummy.
  zcl_message_services=>add_message_to_bapiret_tab( CHANGING ct_return = ct_message ).

ENDMETHOD.


METHOD assign_layout_module.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 27.07.2020
* SYSTEM           # DE0
* SPECIFICATION    # SSM-1: NW Range-ZARN_GUI CHANGE
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Assign article to layout module
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  DATA: lv_delete_others TYPE flag,
        lv_layout_exists TYPE flag,
        lo_x_error       TYPE REF TO zcx_bapi_exception,
        lt_layout_module TYPE zif_layout_module=>tt_instance.

  " AReNa cluster range is always considered source of truth. Accordingly, needs to remove
  " all other layout assignments within same cluster
  LOOP AT mt_article_layout INTO DATA(ls_article_layout) WHERE article       = iv_article
                                                           AND sales_org     = iv_banner
                                                           AND cluster       = iv_cluster.

    IF ls_article_layout-layout_module <> iv_layout_module.
      lv_delete_others = abap_true.
    ELSE.
      lv_layout_exists = abap_true.
    ENDIF.
  ENDLOOP.
  IF lv_delete_others = abap_false AND
     ( lv_layout_exists = abap_true OR
       iv_layout_module IS INITIAL ).
    RETURN. " No action required
  ENDIF.

  MESSAGE i156 WITH iv_cluster iv_banner INTO zcl_message_services=>sv_msg_dummy.
  zcl_message_services=>add_message_to_bapiret_tab( CHANGING ct_return = ct_message ).

  " Step 1: Delete existing assignments within same cluster and set to delete
  DATA(lo_factory) = zcl_layout_module_factory=>get_instance( ).
  LOOP AT mt_article_layout INTO ls_article_layout WHERE article       = iv_article
                                                     AND sales_org     = iv_banner
                                                     AND cluster       = iv_cluster
                                                     AND layout_module <> iv_layout_module.
    TRY.
        DATA(lo_layout_module) = lo_factory->get_layout_module( ls_article_layout-layout_module ).
        lo_layout_module->lock( ).
        DATA(lo_item) = lo_layout_module->get_item( iv_article = iv_article ).
        lo_item->set_delete( ).
        APPEND lo_layout_module TO lt_layout_module.
        MESSAGE s147 WITH iv_article ls_article_layout-layout_module INTO zcl_message_services=>sv_msg_dummy.
        zcl_message_services=>add_message_to_bapiret_tab( CHANGING ct_return = ct_message ).
      CATCH zcx_object_not_found zcx_layout_module INTO lo_x_error.
        APPEND LINES OF lo_x_error->mt_bapi_return TO ct_message.
        add_end_of_cluster_processing( EXPORTING iv_cluster = iv_cluster
                                                 iv_banner  = iv_banner
                                       CHANGING  ct_message = ct_message ).
        RETURN.
    ENDTRY.
  ENDLOOP.

  " Step 2: Create new layout assignment. Layout upload model is used to have same validations
  IF lv_layout_exists = abap_false AND
     iv_layout_module IS NOT INITIAL.
    DATA(lo_model) = zcl_layout_module_upload_model=>get_instance( ).
    lo_model->execute_by_selection( EXPORTING it_article       = VALUE #( sign = 'I' option = 'EQ' ( low = iv_article ) )
                                              iv_layout_module = iv_layout_module
                                    IMPORTING et_message       = DATA(lt_message_model_error) ).
    APPEND LINES OF lt_message_model_error TO ct_message.
    IF zcl_message_services=>contains_error( it_bapi_return = lt_message_model_error ).
      add_end_of_cluster_processing( EXPORTING iv_cluster = iv_cluster
                                               iv_banner  = iv_banner
                                     CHANGING  ct_message = ct_message ).
      RETURN.
    ENDIF.
  ENDIF.

  " Step 3: Save all changes together
  LOOP AT lt_layout_module INTO lo_layout_module.
    TRY.
        lo_layout_module->save( ).
        lo_layout_module->reset( ).
      CATCH zcx_layout_module INTO lo_x_error.
        APPEND LINES OF lo_x_error->mt_bapi_return TO ct_message.
        add_end_of_cluster_processing( EXPORTING iv_cluster = iv_cluster
                                                 iv_banner  = iv_banner
                                       CHANGING  ct_message = ct_message ).
        RETURN.
    ENDTRY.
  ENDLOOP.

  add_end_of_cluster_processing( EXPORTING iv_cluster = iv_cluster
                                           iv_banner  = iv_banner
                                 CHANGING  ct_message = ct_message ).

ENDMETHOD.


METHOD get_instance.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 27.07.2020
* SYSTEM           # DE0
* SPECIFICATION    # SSM-1: NW Range-ZARN_GUI CHANGE
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Returns singleton instance
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  IF so_singleton IS NOT BOUND.
    so_singleton = NEW #( ).
  ENDIF.
  ro_instance = so_singleton.

ENDMETHOD.


METHOD post_layout_module.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 27.07.2020
* SYSTEM           # DE0
* SPECIFICATION    # SSM-1: NW Range-ZARN_GUI CHANGE
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Posts Layout Module
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  DATA: lv_field_name          TYPE fieldname,
        lv_cluster_range_field TYPE fieldname,
        lt_cluster_map         TYPE tt_cluster_map.

  FIELD-SYMBOLS: <lv_cluster_range> TYPE zmd_e_cluster_range.

  CHECK iv_article     IS NOT INITIAL AND
        it_reg_cluster IS NOT INITIAL.

  me->read_article_layout( iv_article = iv_article ).

  SELECT field_name,
         cluster_name
    FROM zarn_cluster_map
    INTO TABLE @lt_cluster_map
    WHERE arena_table = 'ZARN_REG_CLUSTER'.
  IF sy-subrc <> 0.
    RETURN.
  ENDIF.

  " For each range value, determine the cluster, layout module and assign it
  DATA(lo_layout_service) = zcl_layout_module_services=>get_instance( ).

  LOOP AT it_reg_cluster INTO DATA(ls_reg_cluster).

    DO 4 TIMES.

      lv_field_name = |CLUSTER{ sy-index }_RANGE|.
      lv_cluster_range_field = |LS_REG_CLUSTER-{ lv_field_name }|.
      ASSIGN (lv_cluster_range_field) TO <lv_cluster_range>.
      IF sy-subrc <> 0 AND <lv_cluster_range> IS INITIAL.
        CONTINUE.
      ENDIF.

      " Step 1: Determine Cluster
      READ TABLE lt_cluster_map INTO DATA(ls_cluster_map) WITH KEY field_name = lv_field_name.
      IF sy-subrc <> 0.
        MESSAGE e155 WITH lv_field_name INTO zcl_message_services=>sv_msg_dummy.
        zcl_message_services=>add_message_to_bapiret_tab( CHANGING ct_return = et_message ).
        CONTINUE.
      ENDIF.

      " Step 2: Determine Layout Module
      IF <lv_cluster_range> IS NOT INITIAL.
        TRY.
            DATA(lv_layout_module) = lo_layout_service->get_layout_module( iv_sales_org     = ls_reg_cluster-banner
                                                                           iv_cluster       = ls_cluster_map-cluster
                                                                           iv_cluster_range = <lv_cluster_range> ).
          CATCH zcx_layout_module INTO DATA(lo_x_error).
            APPEND LINES OF lo_x_error->mt_bapi_return TO et_message.
            CONTINUE.
        ENDTRY.
      ELSE.
        CLEAR: lv_layout_module.
      ENDIF.

      " Step 3: Post Assignment
      me->assign_layout_module( EXPORTING iv_article       = iv_article
                                          iv_banner        = ls_reg_cluster-banner
                                          iv_cluster       = ls_cluster_map-cluster
                                          iv_layout_module = lv_layout_module
                                CHANGING  ct_message       = et_message ).

    ENDDO.

  ENDLOOP.

ENDMETHOD.


METHOD read_article_layout.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 27.07.2020
* SYSTEM           # DE0
* SPECIFICATION    # SSM-1: NW Range-ZARN_GUI CHANGE
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          #
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  SELECT m~matnr,
         m~laygr,
         m~meinh,
         w~assordimval1,
         w~vkorg
    FROM malg AS m
    INNER JOIN wrs1 AS w ON w~laygr = m~laygr
    INTO TABLE @mt_article_layout
    WHERE matnr = @iv_article.

ENDMETHOD.
ENDCLASS.
