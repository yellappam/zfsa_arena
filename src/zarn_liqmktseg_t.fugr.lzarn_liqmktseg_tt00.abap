*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 05.05.2016 at 16:03:49 by user C90001363
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_LIQMKTSEG_T................................*
DATA:  BEGIN OF STATUS_ZARN_LIQMKTSEG_T              .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_LIQMKTSEG_T              .
CONTROLS: TCTRL_ZARN_LIQMKTSEG_T
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_LIQMKTSEG_T              .
TABLES: ZARN_LIQMKTSEG_T               .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
