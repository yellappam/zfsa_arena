*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_DATA_RECONCILIATION_SUB2
*&---------------------------------------------------------------------*

*&---------------------------------------------------------------------*
*&      Form  DISPLAY_ALV_9001
*&---------------------------------------------------------------------*
* Display ALV 9001
*----------------------------------------------------------------------*
FORM display_alv_9001.


  CONSTANTS: lc_a        TYPE char01 VALUE 'A'.

* Declaration
  DATA: "lo_docking       TYPE REF TO cl_gui_docking_container,  "Custom container instance reference
    lo_container  TYPE REF TO cl_gui_custom_container,    "Custom container instance reference
    lt_fieldcat   TYPE        lvc_t_fcat,                 "Field catalog
    ls_layout     TYPE        lvc_s_layo ,                "Layout structure
*        lt_toolbar       TYPE        ui_functions,              "Toolbar Function
*        lo_event_handler TYPE REF TO lcl_event_handler,         "Event handling
    ls_disvariant TYPE        disvariant,                 "Variant
    lo_alvgrid    TYPE REF TO cl_gui_alv_grid.


  IF go_alvgrid_9001 IS INITIAL .


*   Create custom container
    CREATE OBJECT go_container_9001
      EXPORTING
        container_name              = 'CC_9001'
      EXCEPTIONS
        cntl_error                  = 1
        cntl_system_error           = 2
        create_error                = 3
        lifetime_error              = 4
        lifetime_dynpro_dynpro_link = 5
        OTHERS                      = 6.
    IF sy-subrc NE 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.


**   Create docking container
*    CREATE OBJECT lo_docking
*      EXPORTING
*        parent = cl_gui_container=>screen0
**       ratio  = 90  " 90% yet not full-screen size
*      EXCEPTIONS
*        others = 6.
*    IF sy-subrc NE 0.
*      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
*                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
*    ENDIF.

**   Full size screen for ALV
*    CALL METHOD lo_docking->set_extension
*      EXPORTING
*        extension  = 99999  " full-screen size !!!
*      EXCEPTIONS
*        cntl_error = 1
*        OTHERS     = 2.
*    IF sy-subrc NE 0.
*      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
*                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
*    ENDIF.

* Create ALV grid
    CREATE OBJECT go_alvgrid_9001
      EXPORTING
        i_parent = go_container_9001
      EXCEPTIONS
        OTHERS   = 5.
    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.


*   Preparing field catalog
    PERFORM prepare_fcat_9001 CHANGING lt_fieldcat[].

*   Color Grid
    PERFORM color_grid_9001.

*   Preparing layout structure
    PERFORM prepare_layout_9001 CHANGING ls_layout.

* Prepare Variant
    ls_disvariant-report    = sy-repid.
    ls_disvariant-username  = sy-uname.

*   ALV Display
    CALL METHOD go_alvgrid_9001->set_table_for_first_display
      EXPORTING
        i_bypassing_buffer            = abap_true
*       i_structure_name              = gc_alv_8000-struct
        is_variant                    = ls_disvariant
        i_save                        = lc_a
        is_layout                     = ls_layout
*       it_toolbar_excluding          = lt_toolbar
      CHANGING
        it_outtab                     = gt_fan_exist[]
        it_fieldcatalog               = lt_fieldcat[]
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.


  ELSE.

*   ALV Refresh Display
    CALL METHOD go_alvgrid_9001->refresh_table_display
      EXPORTING
*       is_stable      = ls_stable
        i_soft_refresh = abap_true
      EXCEPTIONS
        finished       = 1
        OTHERS         = 2.
  ENDIF.




ENDFORM.                    " DISPLAY_ALV_9001
*&---------------------------------------------------------------------*
*&      Form  PREPARE_FCAT_9001
*&---------------------------------------------------------------------*
*   Preparing field catalog
*----------------------------------------------------------------------*
FORM prepare_fcat_9001  CHANGING fc_t_fieldcat TYPE lvc_t_fcat.

* Declaration
  DATA: lt_fieldcat TYPE lvc_t_fcat,
        lv_desc     TYPE char40.


  FIELD-SYMBOLS : <ls_fieldcat>    TYPE lvc_s_fcat.

* Prepare field catalog for ALV display by merging structure
  REFRESH: lt_fieldcat[].
  CALL FUNCTION 'LVC_FIELDCATALOG_MERGE'
    EXPORTING
      i_structure_name       = 'ZSARN_DATA_RECO_FAN_EXIST'
    CHANGING
      ct_fieldcat            = lt_fieldcat[]
    EXCEPTIONS
      inconsistent_interface = 1
      program_error          = 2
      OTHERS                 = 3.
  IF sy-subrc NE 0.
    REFRESH: fc_t_fieldcat.
  ENDIF.





* Looping to make the output field
  LOOP AT lt_fieldcat ASSIGNING <ls_fieldcat>.


    <ls_fieldcat>-tooltip = <ls_fieldcat>-scrtext_l.
    <ls_fieldcat>-col_opt = abap_true.

    lv_desc = <ls_fieldcat>-scrtext_s.

    CASE <ls_fieldcat>-fieldname.
      WHEN 'FAN'.            "<ls_fieldcat>-coltext = 'K:' && lv_desc.
      WHEN 'IDNO'.           "<ls_fieldcat>-coltext = 'K:' && lv_desc.
      WHEN 'CURRENT_VER'.
        <ls_fieldcat>-no_out = abap_true.
        "<ls_fieldcat>-coltext = 'K:' && lv_desc.
      WHEN 'NAT_FAN'.        "<ls_fieldcat>-coltext = 'N:' && lv_desc.
      WHEN 'SAP_FAN'.        "<ls_fieldcat>-coltext = 'E:' && lv_desc.
      WHEN 'MATNR_NI'.       "<ls_fieldcat>-coltext = 'N:' && lv_desc.
      WHEN 'MATNR_REG'.      "<ls_fieldcat>-coltext = 'R:' && lv_desc.
      WHEN 'MATNR_SAP'.      "<ls_fieldcat>-coltext = 'E:' && lv_desc.
      WHEN 'OVR_STATUS'.     "<ls_fieldcat>-coltext = 'S:' && 'Ovr Stat'.
      WHEN 'STATUS_FAN'.     "<ls_fieldcat>-coltext = 'S:' && 'Fan'.
      WHEN 'STATUS_BASIC1'.  "<ls_fieldcat>-coltext = 'S:' && 'Basic1'.
      WHEN 'STATUS_BASIC2'.  "<ls_fieldcat>-coltext = 'S:' && 'Basic2'.
      WHEN 'STATUS_ATTR'.    "<ls_fieldcat>-coltext = 'S:' && 'Attr'.
      WHEN 'STATUS_UOM'.     "<ls_fieldcat>-coltext = 'S:' && 'UOM'.
      WHEN 'STATUS_EAN'.     "<ls_fieldcat>-coltext = 'S:' && 'EAN'.
      WHEN 'STATUS_PIR'.     "<ls_fieldcat>-coltext = 'S:' && 'PIR'.
      WHEN 'STATUS_BANNER'.  "<ls_fieldcat>-coltext = 'S:' && 'Banner'.


      WHEN OTHERS.
        DELETE lt_fieldcat WHERE fieldname = <ls_fieldcat>-fieldname.

    ENDCASE.

  ENDLOOP.

  fc_t_fieldcat[] = lt_fieldcat[].

ENDFORM.                    " PREPARE_FCAT_9001
*&---------------------------------------------------------------------*
*&      Form  COLOR_GRID_9001
*&---------------------------------------------------------------------*
*   Color Grid
*----------------------------------------------------------------------*
FORM color_grid_9001.

  DATA: col    TYPE lvc_s_scol,
        coltab TYPE lvc_t_scol,
        color  TYPE lvc_s_colo.


  FIELD-SYMBOLS: <ls_fan_exist>     TYPE ty_s_fan_exist.

  LOOP AT gt_fan_exist[] ASSIGNING <ls_fan_exist>.

    CLEAR: coltab[].

    CLEAR col. col-fname = 'FAN'.                col-color-col = '7'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'IDNO'.               col-color-col = '7'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'CURRENT_VER'.        col-color-col = '7'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'NAT_FAN'.            col-color-col = '1'. col-color-int = '1'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_FAN'.            col-color-col = '4'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'MATNR_NI'.           col-color-col = '1'. col-color-int = '1'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'MATNR_REG'.          col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'MATNR_SAP'.          col-color-col = '4'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'OVR_STATUS'.         col-color-col = '3'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS_FAN'.         col-color-col = '3'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS_BASIC1'.      col-color-col = '3'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS_BASIC2'.      col-color-col = '3'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS_ATTR'.        col-color-col = '3'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS_UOM'.         col-color-col = '3'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS_EAN'.         col-color-col = '3'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS_PIR'.         col-color-col = '3'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS_BANNER'.      col-color-col = '3'. APPEND col TO coltab[].


    <ls_fan_exist>-color = coltab[].

  ENDLOOP.

ENDFORM.                    " COLOR_GRID_9001
*&---------------------------------------------------------------------*
*&      Form  PREPARE_LAYOUT_9001
*&---------------------------------------------------------------------*
*       Field Catalog for STO Display
*----------------------------------------------------------------------*
FORM prepare_layout_9001 CHANGING fc_s_layout    TYPE lvc_s_layo.

  DATA: lv_count TYPE char15,
        lv_lines TYPE i.

  fc_s_layout-ctab_fname = 'COLOR'.
  fc_s_layout-sel_mode   = 'A'.


  CLEAR lv_lines.
  DESCRIBE TABLE gt_fan_exist[] LINES lv_lines.


  lv_count = lv_lines.
  CONDENSE lv_count NO-GAPS.

  CONCATENATE TEXT-003 lv_count
         INTO fc_s_layout-grid_title
 SEPARATED BY space.



ENDFORM.                    " PREPARE_LAYOUT_9001
*&---------------------------------------------------------------------*
*&      Form  DISPLAY_ALV_9002
*&---------------------------------------------------------------------*
* Display ALV 9002
*----------------------------------------------------------------------*
FORM display_alv_9002.


  CONSTANTS: lc_a        TYPE char01 VALUE 'A'.

* Declaration
  DATA: "lo_docking       TYPE REF TO cl_gui_docking_container,  "Custom container instance reference
    lo_container  TYPE REF TO cl_gui_custom_container,    "Custom container instance reference
    lt_fieldcat   TYPE        lvc_t_fcat,                 "Field catalog
    ls_layout     TYPE        lvc_s_layo ,                "Layout structure
*        lt_toolbar       TYPE        ui_functions,              "Toolbar Function
*        lo_event_handler TYPE REF TO lcl_event_handler,         "Event handling
    ls_disvariant TYPE        disvariant,                 "Variant
    lo_alvgrid    TYPE REF TO cl_gui_alv_grid.


  IF go_alvgrid_9002 IS INITIAL .


*   Create custom container
    CREATE OBJECT go_container_9002
      EXPORTING
        container_name              = 'CC_9002'
      EXCEPTIONS
        cntl_error                  = 1
        cntl_system_error           = 2
        create_error                = 3
        lifetime_error              = 4
        lifetime_dynpro_dynpro_link = 5
        OTHERS                      = 6.
    IF sy-subrc NE 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.


**   Create docking container
*    CREATE OBJECT lo_docking
*      EXPORTING
*        parent = cl_gui_container=>screen0
**       ratio  = 90  " 90% yet not full-screen size
*      EXCEPTIONS
*        others = 6.
*    IF sy-subrc NE 0.
*      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
*                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
*    ENDIF.

**   Full size screen for ALV
*    CALL METHOD lo_docking->set_extension
*      EXPORTING
*        extension  = 99999  " full-screen size !!!
*      EXCEPTIONS
*        cntl_error = 1
*        OTHERS     = 2.
*    IF sy-subrc NE 0.
*      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
*                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
*    ENDIF.

* Create ALV grid
    CREATE OBJECT go_alvgrid_9002
      EXPORTING
        i_parent = go_container_9002
      EXCEPTIONS
        OTHERS   = 5.
    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.


*   Preparing field catalog
    PERFORM prepare_fcat_9002 CHANGING lt_fieldcat[].

*   Color Grid
    PERFORM color_grid_9002.

*   Preparing layout structure
    PERFORM prepare_layout_9002 CHANGING ls_layout.

* Prepare Variant
    ls_disvariant-report    = sy-repid.
    ls_disvariant-username  = sy-uname.

*   ALV Display
    CALL METHOD go_alvgrid_9002->set_table_for_first_display
      EXPORTING
        i_bypassing_buffer            = abap_true
*       i_structure_name              = gc_alv_8000-struct
        is_variant                    = ls_disvariant
        i_save                        = lc_a
        is_layout                     = ls_layout
*       it_toolbar_excluding          = lt_toolbar
      CHANGING
        it_outtab                     = gt_reco_basic1[]
        it_fieldcatalog               = lt_fieldcat[]
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.


  ELSE.

*   ALV Refresh Display
    CALL METHOD go_alvgrid_9002->refresh_table_display
      EXPORTING
*       is_stable      = ls_stable
        i_soft_refresh = abap_true
      EXCEPTIONS
        finished       = 1
        OTHERS         = 2.
  ENDIF.



ENDFORM.                    " DISPLAY_ALV_9002
*&---------------------------------------------------------------------*
*&      Form  PREPARE_FCAT_9002
*&---------------------------------------------------------------------*
*   Preparing field catalog
*----------------------------------------------------------------------*
FORM prepare_fcat_9002  CHANGING fc_t_fieldcat TYPE lvc_t_fcat.

* Declaration
  DATA: lt_fieldcat  TYPE lvc_t_fcat,
        lv_desc      TYPE char40,
        lv_desc_stat TYPE char40.


  FIELD-SYMBOLS : <ls_fieldcat>    TYPE lvc_s_fcat.

* Prepare field catalog for ALV display by merging structure
  REFRESH: lt_fieldcat[].
  CALL FUNCTION 'LVC_FIELDCATALOG_MERGE'
    EXPORTING
      i_structure_name       = 'ZSARN_DATA_RECO_BASIC1'
    CHANGING
      ct_fieldcat            = lt_fieldcat[]
    EXCEPTIONS
      inconsistent_interface = 1
      program_error          = 2
      OTHERS                 = 3.
  IF sy-subrc NE 0.
    REFRESH: fc_t_fieldcat.
  ENDIF.





* Looping to make the output field
  LOOP AT lt_fieldcat ASSIGNING <ls_fieldcat>.


    <ls_fieldcat>-tooltip = <ls_fieldcat>-scrtext_l.
    <ls_fieldcat>-col_opt = abap_true.

    lv_desc = <ls_fieldcat>-scrtext_s.


    IF <ls_fieldcat>-fieldname CS 'SAP_'.
      <ls_fieldcat>-coltext = 'E:' && lv_desc.
      lv_desc_stat  = <ls_fieldcat>-scrtext_s.
    ELSEIF <ls_fieldcat>-fieldname+0(6) EQ 'STATUS'.
      <ls_fieldcat>-coltext = 'S:' && lv_desc_stat.
    ELSE.
      <ls_fieldcat>-coltext = 'R:' && lv_desc.
    ENDIF.

    CASE <ls_fieldcat>-fieldname.
      WHEN 'FAN'.                             <ls_fieldcat>-coltext+0(1) = 'K:'.
      WHEN 'CURRENT_VER'.
        <ls_fieldcat>-coltext+0(1) = 'K:'.
        <ls_fieldcat>-no_out = abap_true.
      WHEN 'IDNO'.                            <ls_fieldcat>-coltext+0(1) = 'K:'.
      WHEN 'OVR_STATUS'.                      <ls_fieldcat>-coltext+0(1) = 'K:'.

      WHEN 'MATERIAL'.
      WHEN 'SAP_MATERIAL'.
      WHEN 'STATUS1'.
      WHEN 'MATL_DESC'.
      WHEN 'SAP_MATL_DESC'.
      WHEN 'STATUS2'.
      WHEN 'CONT_UNIT'.
      WHEN 'SAP_CONT_UNIT'.
      WHEN 'STATUS3'.
      WHEN 'NET_CONT'.
      WHEN 'SAP_NET_CONT'.
      WHEN 'STATUS4'.
      WHEN 'BASE_UOM'.
      WHEN 'SAP_BASE_UOM'.
      WHEN 'STATUS5'.
      WHEN 'PO_UNIT'.
      WHEN 'SAP_PO_UNIT'.
      WHEN 'STATUS6'.
      WHEN 'ITEM_CAT'.
      WHEN 'SAP_ITEM_CAT'.
      WHEN 'STATUS7'.
      WHEN 'NET_WEIGHT'.
      WHEN 'SAP_NET_WEIGHT'.
      WHEN 'STATUS8'.
      WHEN 'TEMP_CONDS'.
      WHEN 'SAP_TEMP_CONDS'.
      WHEN 'STATUS9'.
      WHEN 'SEASON'.
      WHEN 'SAP_SEASON'.
      WHEN 'STATUS10'.
      WHEN 'PROD_HIER'.
      WHEN 'SAP_PROD_HIER'.
      WHEN 'STATUS11'.
      WHEN 'MINREMLIFE'.
      WHEN 'SAP_MINREMLIFE'.
      WHEN 'STATUS12'.
      WHEN 'SAESON_YR'.
      WHEN 'SAP_SAESON_YR'.
      WHEN 'STATUS13'.
      WHEN 'PUR_STATUS'.
      WHEN 'SAP_PUR_STATUS'.
      WHEN 'STATUS14'.
      WHEN 'SAL_STATUS'.
      WHEN 'SAP_SAL_STATUS'.
      WHEN 'STATUS15'.
      WHEN 'PVALIDFROM'.
      WHEN 'SAP_PVALIDFROM'.
      WHEN 'STATUS16'.
      WHEN 'SVALIDFROM'.
      WHEN 'SAP_SVALIDFROM'.
      WHEN 'STATUS17'.
      WHEN 'CREATION_STATUS'.
      WHEN 'SAP_CREATION_STATUS'.
      WHEN 'STATUS18'.
      WHEN 'BRAND_ID'.
      WHEN 'SAP_BRAND_ID'.
      WHEN 'STATUS19'.
      WHEN 'COUNTRYORI'.              <ls_fieldcat>-coltext+0(1) = 'N:'.
      WHEN 'SAP_COUNTRYORI'.
      WHEN 'STATUS20'.
      WHEN 'SALES_UNIT'.
      WHEN 'SAP_SALES_UNIT'.
      WHEN 'STATUS21'.
      WHEN 'ISSUE_UNIT'.
      WHEN 'SAP_ISSUE_UNIT'.
      WHEN 'STATUS22'.
      WHEN 'MATL_GROUP'.
      WHEN 'SAP_MATL_GROUP'.
      WHEN 'STATUS23'.
      WHEN 'MATL_TYPE'.
      WHEN 'SAP_MATL_TYPE'.
      WHEN 'STATUS24'.
      WHEN 'MATL_CAT'.
      WHEN 'SAP_MATL_CAT'.
      WHEN 'STATUS25'.


      WHEN OTHERS.
        DELETE lt_fieldcat WHERE fieldname = <ls_fieldcat>-fieldname.

    ENDCASE.

  ENDLOOP.

  fc_t_fieldcat[] = lt_fieldcat[].

ENDFORM.                    " PREPARE_FCAT_9002
*&---------------------------------------------------------------------*
*&      Form  COLOR_GRID_9002
*&---------------------------------------------------------------------*
*   Color Grid
*----------------------------------------------------------------------*
FORM color_grid_9002.

  DATA: col    TYPE lvc_s_scol,
        coltab TYPE lvc_t_scol,
        color  TYPE lvc_s_colo.


  FIELD-SYMBOLS: <ls_reco_basic1>     TYPE zsarn_data_reco_basic1.

  LOOP AT gt_reco_basic1[] ASSIGNING <ls_reco_basic1>.

    CLEAR: coltab[].

    CLEAR col. col-fname = 'FAN'.                  col-color-col = '7'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'CURRENT_VER'.          col-color-col = '7'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'IDNO'.                 col-color-col = '7'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'OVR_STATUS'.           col-color-col = '7'. APPEND col TO coltab[].



    CLEAR col. col-fname = 'MATERIAL'.             col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_MATERIAL'.         col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS1'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'MATL_DESC'.            col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_MATL_DESC'.        col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS2'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'CONT_UNIT'.            col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_CONT_UNIT'.        col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS3'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'NET_CONT'.             col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_NET_CONT'.         col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS4'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'BASE_UOM'.             col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_BASE_UOM'.         col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS5'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'PO_UNIT'.              col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_PO_UNIT'.          col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS6'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ITEM_CAT'.             col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ITEM_CAT'.         col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS7'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'NET_WEIGHT'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_NET_WEIGHT'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS8'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'TEMP_CONDS'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_TEMP_CONDS'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS9'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'SEASON'.               col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_SEASON'.           col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS10'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'PROD_HIER'.            col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_PROD_HIER'.        col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS11'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'MINREMLIFE'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_MINREMLIFE'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS12'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'SAESON_YR'.            col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_SAESON_YR'.        col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS13'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'PUR_STATUS'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_PUR_STATUS'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS14'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'SAL_STATUS'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_SAL_STATUS'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS15'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'PVALIDFROM'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_PVALIDFROM'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS16'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'SVALIDFROM'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_SVALIDFROM'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS17'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'CREATION_STATUS'.      col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_CREATION_STATUS'.  col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS18'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'BRAND_ID'.             col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_BRAND_ID'.         col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS19'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'COUNTRYORI'.           col-color-col = '1'. col-color-int = '1'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_COUNTRYORI'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS20'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'SALES_UNIT'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_SALES_UNIT'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS21'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ISSUE_UNIT'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ISSUE_UNIT'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS22'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'MATL_GROUP'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_MATL_GROUP'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS23'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'MATL_TYPE'.            col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_MATL_TYPE'.        col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS24'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'MATL_CAT'.             col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_MATL_CAT'.         col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS25'.             col-color-col = '3'. APPEND col TO coltab[].


    <ls_reco_basic1>-color = coltab[].

  ENDLOOP.

ENDFORM.                    " COLOR_GRID_9002
*&---------------------------------------------------------------------*
*&      Form  PREPARE_LAYOUT_9002
*&---------------------------------------------------------------------*
*       Field Catalog for STO Display
*----------------------------------------------------------------------*
FORM prepare_layout_9002 CHANGING fc_s_layout    TYPE lvc_s_layo.


  DATA: lv_count TYPE char15.

  fc_s_layout-ctab_fname = 'COLOR'.
  fc_s_layout-sel_mode   = 'A'.

  lv_count = gv_count_9002.
  CONDENSE lv_count NO-GAPS.

  CONCATENATE TEXT-002 lv_count
         INTO fc_s_layout-grid_title
 SEPARATED BY space.


ENDFORM.                    " PREPARE_LAYOUT_9002
*&---------------------------------------------------------------------*
*&      Form  DISPLAY_ALV_9003
*&---------------------------------------------------------------------*
* Display ALV 9003
*----------------------------------------------------------------------*
FORM display_alv_9003.


  CONSTANTS: lc_a        TYPE char01 VALUE 'A'.

* Declaration
  DATA: "lo_docking       TYPE REF TO cl_gui_docking_container,  "Custom container instance reference
    lo_container  TYPE REF TO cl_gui_custom_container,    "Custom container instance reference
    lt_fieldcat   TYPE        lvc_t_fcat,                 "Field catalog
    ls_layout     TYPE        lvc_s_layo ,                "Layout structure
*        lt_toolbar       TYPE        ui_functions,              "Toolbar Function
*        lo_event_handler TYPE REF TO lcl_event_handler,         "Event handling
    ls_disvariant TYPE        disvariant,                 "Variant
    lo_alvgrid    TYPE REF TO cl_gui_alv_grid.


  IF go_alvgrid_9003 IS INITIAL .


*   Create custom container
    CREATE OBJECT go_container_9003
      EXPORTING
        container_name              = 'CC_9003'
      EXCEPTIONS
        cntl_error                  = 1
        cntl_system_error           = 2
        create_error                = 3
        lifetime_error              = 4
        lifetime_dynpro_dynpro_link = 5
        OTHERS                      = 6.
    IF sy-subrc NE 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.


**   Create docking container
*    CREATE OBJECT lo_docking
*      EXPORTING
*        parent = cl_gui_container=>screen0
**       ratio  = 90  " 90% yet not full-screen size
*      EXCEPTIONS
*        others = 6.
*    IF sy-subrc NE 0.
*      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
*                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
*    ENDIF.

**   Full size screen for ALV
*    CALL METHOD lo_docking->set_extension
*      EXPORTING
*        extension  = 99999  " full-screen size !!!
*      EXCEPTIONS
*        cntl_error = 1
*        OTHERS     = 2.
*    IF sy-subrc NE 0.
*      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
*                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
*    ENDIF.

* Create ALV grid
    CREATE OBJECT go_alvgrid_9003
      EXPORTING
        i_parent = go_container_9003
      EXCEPTIONS
        OTHERS   = 5.
    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.


*   Preparing field catalog
    PERFORM prepare_fcat_9003 CHANGING lt_fieldcat[].

*   Color Grid
    PERFORM color_grid_9003.

*   Preparing layout structure
    PERFORM prepare_layout_9003 CHANGING ls_layout.

* Prepare Variant
    ls_disvariant-report    = sy-repid.
    ls_disvariant-username  = sy-uname.

*   ALV Display
    CALL METHOD go_alvgrid_9003->set_table_for_first_display
      EXPORTING
        i_bypassing_buffer            = abap_true
*       i_structure_name              = gc_alv_8000-struct
        is_variant                    = ls_disvariant
        i_save                        = lc_a
        is_layout                     = ls_layout
*       it_toolbar_excluding          = lt_toolbar
      CHANGING
        it_outtab                     = gt_reco_basic2[]
        it_fieldcatalog               = lt_fieldcat[]
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.


  ELSE.

*   ALV Refresh Display
    CALL METHOD go_alvgrid_9003->refresh_table_display
      EXPORTING
*       is_stable      = ls_stable
        i_soft_refresh = abap_true
      EXCEPTIONS
        finished       = 1
        OTHERS         = 2.
  ENDIF.



ENDFORM.                    " DISPLAY_ALV_9003
*&---------------------------------------------------------------------*
*&      Form  PREPARE_FCAT_9003
*&---------------------------------------------------------------------*
*   Preparing field catalog
*----------------------------------------------------------------------*
FORM prepare_fcat_9003  CHANGING fc_t_fieldcat TYPE lvc_t_fcat.

* Declaration
  DATA: lt_fieldcat  TYPE lvc_t_fcat,
        lv_desc      TYPE char40,
        lv_desc_stat TYPE char40.


  FIELD-SYMBOLS : <ls_fieldcat>    TYPE lvc_s_fcat.

* Prepare field catalog for ALV display by merging structure
  REFRESH: lt_fieldcat[].
  CALL FUNCTION 'LVC_FIELDCATALOG_MERGE'
    EXPORTING
      i_structure_name       = 'ZSARN_DATA_RECO_BASIC2'
    CHANGING
      ct_fieldcat            = lt_fieldcat[]
    EXCEPTIONS
      inconsistent_interface = 1
      program_error          = 2
      OTHERS                 = 3.
  IF sy-subrc NE 0.
    REFRESH: fc_t_fieldcat.
  ENDIF.





* Looping to make the output field
  LOOP AT lt_fieldcat ASSIGNING <ls_fieldcat>.


    <ls_fieldcat>-tooltip = <ls_fieldcat>-scrtext_l.
    <ls_fieldcat>-col_opt = abap_true.

    lv_desc = <ls_fieldcat>-scrtext_s.


    IF <ls_fieldcat>-fieldname CS 'SAP_'.
      <ls_fieldcat>-coltext = 'E:' && lv_desc.
      lv_desc_stat  = <ls_fieldcat>-scrtext_s.
    ELSEIF <ls_fieldcat>-fieldname+0(6) EQ 'STATUS'.
      <ls_fieldcat>-coltext = 'S:' && lv_desc_stat.
    ELSE.
      <ls_fieldcat>-coltext = 'R:' && lv_desc.
    ENDIF.

    CASE <ls_fieldcat>-fieldname.
      WHEN 'FAN'.                       <ls_fieldcat>-coltext+0(1) = 'K:'.
      WHEN 'CURRENT_VER'.
        <ls_fieldcat>-coltext+0(1) = 'K:'.
        <ls_fieldcat>-no_out = abap_true.
      WHEN 'IDNO'.                      <ls_fieldcat>-coltext+0(1) = 'K:'.
      WHEN 'OVR_STATUS'.                <ls_fieldcat>-coltext+0(1) = 'K:'.

      WHEN 'MATERIAL'.
      WHEN 'SAP_MATERIAL'.
      WHEN 'STATUS1'.
      WHEN 'ZZFAN'.                     <ls_fieldcat>-coltext+0(1) = 'N:'.
      WHEN 'SAP_ZZFAN'.
      WHEN 'STATUS2'.
*      WHEN 'ZZSELLING_ONLY'.                 " --ONED-217 JKH 24.11.2016
*      WHEN 'SAP_ZZSELLING_ONLY'.             " --ONED-217 JKH 24.11.2016
      WHEN 'ZZBUY_SELL'.                      " ++ONED-217 JKH 24.11.2016
      WHEN 'SAP_ZZBUY_SELL'.                  " ++ONED-217 JKH 24.11.2016
      WHEN 'STATUS3'.
      WHEN 'ZZSELL'.
      WHEN 'SAP_ZZSELL'.
      WHEN 'STATUS4'.
      WHEN 'ZZUSE'.
      WHEN 'SAP_ZZUSE'.
      WHEN 'STATUS5'.
      WHEN 'ZZTKTPRNT'.
      WHEN 'SAP_ZZTKTPRNT'.
      WHEN 'STATUS6'.
      WHEN 'ZZPEDESC_FLAG'.
      WHEN 'SAP_ZZPEDESC_FLAG'.
      WHEN 'STATUS7'.

* DEL Begin of Change INC5348789 JKH 31.10.2016
*      WHEN 'ZZUNNUM'.
*      WHEN 'SAP_ZZUNNUM'.
*      WHEN 'STATUS8'.
*      WHEN 'ZZPACKGRP'.
*      WHEN 'SAP_ZZPACKGRP'.
*      WHEN 'STATUS9'.
*      WHEN 'ZZHCLS'.
*      WHEN 'SAP_ZZHCLS'.
*      WHEN 'STATUS10'.
*      WHEN 'ZZHSNO'.
*      WHEN 'SAP_ZZHSNO'.
*      WHEN 'STATUS11'.
*      WHEN 'ZZHARD'.
*      WHEN 'SAP_ZZHARD'.
*      WHEN 'STATUS12'.
* DEL End of Change INC5348789 JKH 31.10.2016



*      WHEN 'ZZPREP'.
*      WHEN 'SAP_ZZPREP'.
*      WHEN 'STATUS13'.
      WHEN 'ZZSTRD'.
      WHEN 'SAP_ZZSTRD'.
      WHEN 'STATUS14'.
      WHEN 'ZZSCO_WEIGHED_FLAG'.
      WHEN 'SAP_ZZSCO_WEIGHED_FLAG'.
      WHEN 'STATUS15'.
      WHEN 'ZZSTRG'.
      WHEN 'SAP_ZZSTRG'.
      WHEN 'STATUS16'.
      WHEN 'ZZLABNUM'.
      WHEN 'SAP_ZZLABNUM'.
      WHEN 'STATUS17'.
      WHEN 'ZZPKDDT'.
      WHEN 'SAP_ZZPKDDT'.
      WHEN 'STATUS18'.
      WHEN 'ZZMANDST'.
      WHEN 'SAP_ZZMANDST'.
      WHEN 'STATUS19'.
      WHEN 'ZZWNMSG'.
      WHEN 'SAP_ZZWNMSG'.
      WHEN 'STATUS20'.
      WHEN 'ZZPRDTYPE'.
      WHEN 'SAP_ZZPRDTYPE'.
      WHEN 'STATUS21'.
      WHEN 'ZZAS4SUBDEPT'.
      WHEN 'SAP_ZZAS4SUBDEPT'.
      WHEN 'STATUS22'.
      WHEN 'ZZVAR_WT_FLAG'.
      WHEN 'SAP_ZZVAR_WT_FLAG'.
      WHEN 'STATUS23'.
      WHEN 'ZZ_UNI_DC'.
      WHEN 'SAP_ZZ_UNI_DC'.
      WHEN 'STATUS24'.
      WHEN 'ZZ_LNI_DC'.
      WHEN 'SAP_ZZ_LNI_DC'.
      WHEN 'STATUS25'.
      WHEN 'ZZLNI_REPACK'.
      WHEN 'SAP_ZZLNI_REPACK'.
      WHEN 'STATUS26'.
      WHEN 'ZZLNI_BULK'.
      WHEN 'SAP_ZZLNI_BULK'.
      WHEN 'STATUS27'.
      WHEN 'ZZLNI_PRBOLY'.
      WHEN 'SAP_ZZLNI_PRBOLY'.
      WHEN 'STATUS28'.




      WHEN OTHERS.
        DELETE lt_fieldcat WHERE fieldname = <ls_fieldcat>-fieldname.

    ENDCASE.

  ENDLOOP.

  fc_t_fieldcat[] = lt_fieldcat[].

ENDFORM.                    " PREPARE_FCAT_9003
*&---------------------------------------------------------------------*
*&      Form  COLOR_GRID_9003
*&---------------------------------------------------------------------*
*   Color Grid
*----------------------------------------------------------------------*
FORM color_grid_9003.

  DATA: col    TYPE lvc_s_scol,
        coltab TYPE lvc_t_scol,
        color  TYPE lvc_s_colo.


  FIELD-SYMBOLS: <ls_reco_basic2>     TYPE zsarn_data_reco_basic2.

  LOOP AT gt_reco_basic2[] ASSIGNING <ls_reco_basic2>.

    CLEAR: coltab[].

    CLEAR col. col-fname = 'FAN'.                   col-color-col = '7'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'CURRENT_VER'.           col-color-col = '7'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'IDNO'.                  col-color-col = '7'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'OVR_STATUS'.            col-color-col = '7'. APPEND col TO coltab[].



    CLEAR col. col-fname = 'MATERIAL'.              col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_MATERIAL'.          col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS1'.               col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZFAN'.                 col-color-col = '1'. col-color-int = '1'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZFAN'.             col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS2'.               col-color-col = '3'. APPEND col TO coltab[].

*    CLEAR col. col-fname = 'ZZSELLING_ONLY'.        col-color-col = '5'. APPEND col TO coltab[].    " --ONED-217 JKH 24.11.2016
*    CLEAR col. col-fname = 'SAP_ZZSELLING_ONLY'.    col-color-col = '4'. APPEND col TO coltab[].    " --ONED-217 JKH 24.11.2016


    CLEAR col. col-fname = 'ZZBUY_SELL'.            col-color-col = '5'. APPEND col TO coltab[].    " ++ONED-217 JKH 24.11.2016
    CLEAR col. col-fname = 'SAP_ZZBUY_SELL'.        col-color-col = '4'. APPEND col TO coltab[].    " ++ONED-217 JKH 24.11.2016
    CLEAR col. col-fname = 'STATUS3'.               col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZSELL'.                col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZSELL'.            col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS4'.               col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZUSE'.                 col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZUSE'.             col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS5'.               col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZTKTPRNT'.             col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZTKTPRNT'.         col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS6'.               col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZPEDESC_FLAG'.         col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZPEDESC_FLAG'.     col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS7'.               col-color-col = '3'. APPEND col TO coltab[].


* DEL Begin of Change INC5348789 JKH 31.10.2016
*    CLEAR col. col-fname = 'ZZUNNUM'.               col-color-col = '5'. APPEND col TO coltab[].
*    CLEAR col. col-fname = 'SAP_ZZUNNUM'.           col-color-col = '4'. APPEND col TO coltab[].
*    CLEAR col. col-fname = 'STATUS8'.               col-color-col = '3'. APPEND col TO coltab[].
*
*    CLEAR col. col-fname = 'ZZPACKGRP'.             col-color-col = '5'. APPEND col TO coltab[].
*    CLEAR col. col-fname = 'SAP_ZZPACKGRP'.         col-color-col = '4'. APPEND col TO coltab[].
*    CLEAR col. col-fname = 'STATUS9'.               col-color-col = '3'. APPEND col TO coltab[].
*
*    CLEAR col. col-fname = 'ZZHCLS'.                col-color-col = '5'. APPEND col TO coltab[].
*    CLEAR col. col-fname = 'SAP_ZZHCLS'.            col-color-col = '4'. APPEND col TO coltab[].
*    CLEAR col. col-fname = 'STATUS10'.              col-color-col = '3'. APPEND col TO coltab[].
*
*    CLEAR col. col-fname = 'ZZHSNO'.                col-color-col = '5'. APPEND col TO coltab[].
*    CLEAR col. col-fname = 'SAP_ZZHSNO'.            col-color-col = '4'. APPEND col TO coltab[].
*    CLEAR col. col-fname = 'STATUS11'.              col-color-col = '3'. APPEND col TO coltab[].
*
*    CLEAR col. col-fname = 'ZZHARD'.                col-color-col = '5'. APPEND col TO coltab[].
*    CLEAR col. col-fname = 'SAP_ZZHARD'.            col-color-col = '4'. APPEND col TO coltab[].
*    CLEAR col. col-fname = 'STATUS12'.              col-color-col = '3'. APPEND col TO coltab[].
* DEL End of Change INC5348789 JKH 31.10.2016



*    CLEAR col. col-fname = 'ZZPREP'.                col-color-col = '5'. APPEND col TO coltab[].
*    CLEAR col. col-fname = 'SAP_ZZPREP'.            col-color-col = '4'. APPEND col TO coltab[].
*    CLEAR col. col-fname = 'STATUS13'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZSTRD'.                col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZSTRD'.            col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS14'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZSCO_WEIGHED_FLAG'.     col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZSCO_WEIGHED_FLAG'. col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS15'.               col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZSTRG'.                col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZSTRG'.            col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS16'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZLABNUM'.              col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZLABNUM'.          col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS17'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZPKDDT'.               col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZPKDDT'.           col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS18'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZMANDST'.              col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZMANDST'.          col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS19'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZWNMSG'.               col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZWNMSG'.           col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS20'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZPRDTYPE'.             col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZPRDTYPE'.         col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS21'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZAS4SUBDEPT'.          col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZAS4SUBDEPT'.      col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS22'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZVAR_WT_FLAG'.         col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZVAR_WT_FLAG'.     col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS23'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZ_UNI_DC'.             col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZ_UNI_DC'.         col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS24'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZ_LNI_DC'.             col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZ_LNI_DC'.         col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS25'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZLNI_REPACK'.          col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZLNI_REPACK'.      col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS26'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZLNI_BULK'.            col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZLNI_BULK'.        col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS27'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZLNI_PRBOLY'.          col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZLNI_PRBOLY'.      col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS28'.              col-color-col = '3'. APPEND col TO coltab[].





    <ls_reco_basic2>-color = coltab[].

  ENDLOOP.

ENDFORM.                    " COLOR_GRID_9003
*&---------------------------------------------------------------------*
*&      Form  PREPARE_LAYOUT_9003
*&---------------------------------------------------------------------*
*       Field Catalog for STO Display
*----------------------------------------------------------------------*
FORM prepare_layout_9003 CHANGING fc_s_layout    TYPE lvc_s_layo.

  DATA: lv_count TYPE char15.

  fc_s_layout-ctab_fname = 'COLOR'.
  fc_s_layout-sel_mode   = 'A'.

  lv_count = gv_count_9003.
  CONDENSE lv_count NO-GAPS.

  CONCATENATE TEXT-002 lv_count
         INTO fc_s_layout-grid_title
 SEPARATED BY space.


ENDFORM.                    " PREPARE_LAYOUT_9003
*&---------------------------------------------------------------------*
*&      Form  DISPLAY_ALV_9004
*&---------------------------------------------------------------------*
* Display ALV 9004
*----------------------------------------------------------------------*
FORM display_alv_9004.


  CONSTANTS: lc_a        TYPE char01 VALUE 'A'.

* Declaration
  DATA: "lo_docking       TYPE REF TO cl_gui_docking_container,  "Custom container instance reference
    lo_container  TYPE REF TO cl_gui_custom_container,    "Custom container instance reference
    lt_fieldcat   TYPE        lvc_t_fcat,                 "Field catalog
    ls_layout     TYPE        lvc_s_layo ,                "Layout structure
*        lt_toolbar       TYPE        ui_functions,              "Toolbar Function
*        lo_event_handler TYPE REF TO lcl_event_handler,         "Event handling
    ls_disvariant TYPE        disvariant,                 "Variant
    lo_alvgrid    TYPE REF TO cl_gui_alv_grid.




  IF go_alvgrid_9004 IS INITIAL .


*   Create custom container
    CREATE OBJECT go_container_9004
      EXPORTING
        container_name              = 'CC_9004'
      EXCEPTIONS
        cntl_error                  = 1
        cntl_system_error           = 2
        create_error                = 3
        lifetime_error              = 4
        lifetime_dynpro_dynpro_link = 5
        OTHERS                      = 6.
    IF sy-subrc NE 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.


**   Create docking container
*    CREATE OBJECT lo_docking
*      EXPORTING
*        parent = cl_gui_container=>screen0
**       ratio  = 90  " 90% yet not full-screen size
*      EXCEPTIONS
*        others = 6.
*    IF sy-subrc NE 0.
*      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
*                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
*    ENDIF.

**   Full size screen for ALV
*    CALL METHOD lo_docking->set_extension
*      EXPORTING
*        extension  = 99999  " full-screen size !!!
*      EXCEPTIONS
*        cntl_error = 1
*        OTHERS     = 2.
*    IF sy-subrc NE 0.
*      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
*                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
*    ENDIF.

* Create ALV grid
    CREATE OBJECT go_alvgrid_9004
      EXPORTING
        i_parent = go_container_9004
      EXCEPTIONS
        OTHERS   = 5.
    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.


*   Preparing field catalog
    PERFORM prepare_fcat_9004 CHANGING lt_fieldcat[].

*   Color Grid
    PERFORM color_grid_9004.

*   Preparing layout structure
    PERFORM prepare_layout_9004 CHANGING ls_layout.

* Prepare Variant
    ls_disvariant-report    = sy-repid.
    ls_disvariant-username  = sy-uname.

*   ALV Display
    CALL METHOD go_alvgrid_9004->set_table_for_first_display
      EXPORTING
        i_bypassing_buffer            = abap_true
*       i_structure_name              = gc_alv_8000-struct
        is_variant                    = ls_disvariant
        i_save                        = lc_a
        is_layout                     = ls_layout
*       it_toolbar_excluding          = lt_toolbar
      CHANGING
        it_outtab                     = gt_reco_uom[]
        it_fieldcatalog               = lt_fieldcat[]
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.


  ELSE.

*   ALV Refresh Display
    CALL METHOD go_alvgrid_9004->refresh_table_display
      EXPORTING
*       is_stable      = ls_stable
        i_soft_refresh = abap_true
      EXCEPTIONS
        finished       = 1
        OTHERS         = 2.
  ENDIF.




ENDFORM.                    " DISPLAY_ALV_9004
*&---------------------------------------------------------------------*
*&      Form  PREPARE_FCAT_9004
*&---------------------------------------------------------------------*
*   Preparing field catalog
*----------------------------------------------------------------------*
FORM prepare_fcat_9004  CHANGING fc_t_fieldcat TYPE lvc_t_fcat.

* Declaration
  DATA: lt_fieldcat  TYPE lvc_t_fcat,
        lv_desc      TYPE char40,
        lv_desc_stat TYPE char40.


  FIELD-SYMBOLS : <ls_fieldcat>    TYPE lvc_s_fcat.

* Prepare field catalog for ALV display by merging structure
  REFRESH: lt_fieldcat[].
  CALL FUNCTION 'LVC_FIELDCATALOG_MERGE'
    EXPORTING
      i_structure_name       = 'ZSARN_DATA_RECO_UOM'
    CHANGING
      ct_fieldcat            = lt_fieldcat[]
    EXCEPTIONS
      inconsistent_interface = 1
      program_error          = 2
      OTHERS                 = 3.
  IF sy-subrc NE 0.
    REFRESH: fc_t_fieldcat.
  ENDIF.





* Looping to make the output field
  LOOP AT lt_fieldcat ASSIGNING <ls_fieldcat>.


    <ls_fieldcat>-tooltip = <ls_fieldcat>-scrtext_l.
    <ls_fieldcat>-col_opt = abap_true.


    lv_desc = <ls_fieldcat>-scrtext_s.


    IF <ls_fieldcat>-fieldname CS 'SAP_'.
      <ls_fieldcat>-coltext = 'E:' && lv_desc.
      lv_desc_stat  = <ls_fieldcat>-scrtext_s.
    ELSEIF <ls_fieldcat>-fieldname+0(6) EQ 'STATUS'.
      <ls_fieldcat>-coltext = 'S:' && lv_desc_stat.
    ELSE.
      <ls_fieldcat>-coltext = 'R:' && lv_desc.
    ENDIF.


    CASE <ls_fieldcat>-fieldname.
      WHEN 'FAN'.                             <ls_fieldcat>-coltext+0(1) = 'K:'.
      WHEN 'CURRENT_VER'.
        <ls_fieldcat>-coltext+0(1) = 'K:'.
        <ls_fieldcat>-no_out = abap_true.
      WHEN 'IDNO'.                            <ls_fieldcat>-coltext+0(1) = 'K:'.
      WHEN 'UOM_CATEGORY'.                    <ls_fieldcat>-coltext+0(1) = 'K:'.
      WHEN 'PIM_UOM_CODE'.                    <ls_fieldcat>-coltext+0(1) = 'K:'.
      WHEN 'HYBRIS_INTERNAL_CODE'.            <ls_fieldcat>-coltext+0(1) = 'K:'.
      WHEN 'LOWER_UOM'.                       <ls_fieldcat>-coltext+0(1) = 'K:'.
      WHEN 'LOWER_CHILD_UOM'.                 <ls_fieldcat>-coltext+0(1) = 'K:'.
      WHEN 'OVR_STATUS'.                      <ls_fieldcat>-coltext+0(1) = 'K:'.
      WHEN 'MATERIAL'.
      WHEN 'SAP_MATERIAL'.
      WHEN 'STATUS1'.
      WHEN 'ALT_UNIT'.
      WHEN 'SAP_ALT_UNIT'.
      WHEN 'STATUS2'.
      WHEN 'NUMERATOR'.
      WHEN 'SAP_NUMERATOR'.
      WHEN 'STATUS3'.
      WHEN 'DENOMINATR'.
      WHEN 'SAP_DENOMINATR'.
      WHEN 'STATUS4'.
      WHEN 'EAN_UPC'.
      WHEN 'SAP_EAN_UPC'.
      WHEN 'STATUS5'.
      WHEN 'EAN_CAT'.
      WHEN 'SAP_EAN_CAT'.
      WHEN 'STATUS6'.
      WHEN 'LENGTH'.
      WHEN 'SAP_LENGTH'.
      WHEN 'STATUS7'.
      WHEN 'WIDTH'.
      WHEN 'SAP_WIDTH'.
      WHEN 'STATUS8'.
      WHEN 'HEIGHT'.
      WHEN 'SAP_HEIGHT'.
      WHEN 'STATUS9'.
      WHEN 'UNIT_DIM'.
      WHEN 'SAP_UNIT_DIM'.
      WHEN 'STATUS10'.
      WHEN 'GROSS_WT'.
      WHEN 'SAP_GROSS_WT'.
      WHEN 'STATUS11'.
      WHEN 'UNIT_OF_WT'.
      WHEN 'SAP_UNIT_OF_WT'.
      WHEN 'STATUS12'.
      WHEN 'SUB_UOM'.
      WHEN 'SAP_SUB_UOM'.
      WHEN 'STATUS13'.

      WHEN OTHERS.
        DELETE lt_fieldcat WHERE fieldname = <ls_fieldcat>-fieldname.

    ENDCASE.

  ENDLOOP.

  fc_t_fieldcat[] = lt_fieldcat[].

ENDFORM.                    " PREPARE_FCAT_9004
*&---------------------------------------------------------------------*
*&      Form  COLOR_GRID_9004
*&---------------------------------------------------------------------*
*   Color Grid
*----------------------------------------------------------------------*
FORM color_grid_9004.

  DATA: col    TYPE lvc_s_scol,
        coltab TYPE lvc_t_scol,
        color  TYPE lvc_s_colo.


  FIELD-SYMBOLS: <ls_reco_uom>     TYPE zsarn_data_reco_uom.

  LOOP AT gt_reco_uom[] ASSIGNING <ls_reco_uom>.

    CLEAR: coltab[].

    CLEAR col. col-fname = 'FAN'.                  col-color-col = '7'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'CURRENT_VER'.          col-color-col = '7'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'IDNO'.                 col-color-col = '7'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'UOM_CATEGORY'.         col-color-col = '7'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'PIM_UOM_CODE'.         col-color-col = '7'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'HYBRIS_INTERNAL_CODE'. col-color-col = '7'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'LOWER_UOM'.            col-color-col = '7'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'LOWER_CHILD_UOM'.      col-color-col = '7'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'OVR_STATUS'.           col-color-col = '7'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'MATERIAL'.             col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_MATERIAL'.         col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS1'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ALT_UNIT'.             col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ALT_UNIT'.         col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS2'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'NUMERATOR'.            col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_NUMERATOR'.        col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS3'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'DENOMINATR'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_DENOMINATR'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS4'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'EAN_UPC'.              col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_EAN_UPC'.          col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS5'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'EAN_CAT'.              col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_EAN_CAT'.          col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS6'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'LENGTH'.               col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_LENGTH'.           col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS7'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'WIDTH'.                col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_WIDTH'.            col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS8'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'HEIGHT'.               col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_HEIGHT'.           col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS9'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'UNIT_DIM'.             col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_UNIT_DIM'.         col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS10'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'GROSS_WT'.             col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_GROSS_WT'.         col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS11'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'UNIT_OF_WT'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_UNIT_OF_WT'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS12'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'SUB_UOM'.              col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_SUB_UOM'.          col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS13'.             col-color-col = '3'. APPEND col TO coltab[].




    <ls_reco_uom>-color = coltab[].

  ENDLOOP.

ENDFORM.                    " COLOR_GRID_9004
*&---------------------------------------------------------------------*
*&      Form  PREPARE_LAYOUT_9004
*&---------------------------------------------------------------------*
*       Field Catalog for STO Display
*----------------------------------------------------------------------*
FORM prepare_layout_9004 CHANGING fc_s_layout    TYPE lvc_s_layo.

  DATA: lv_count TYPE char15.

  fc_s_layout-ctab_fname = 'COLOR'.
  fc_s_layout-sel_mode   = 'A'.

  lv_count = gv_count_9004.
  CONDENSE lv_count NO-GAPS.

  CONCATENATE TEXT-002 lv_count
         INTO fc_s_layout-grid_title
 SEPARATED BY space.


ENDFORM.                    " PREPARE_LAYOUT_9004
*&---------------------------------------------------------------------*
*&      Form  DISPLAY_ALV_9005
*&---------------------------------------------------------------------*
* Display ALV 9005
*----------------------------------------------------------------------*
FORM display_alv_9005.


  CONSTANTS: lc_a        TYPE char01 VALUE 'A'.

* Declaration
  DATA: "lo_docking       TYPE REF TO cl_gui_docking_container,  "Custom container instance reference
    lo_container  TYPE REF TO cl_gui_custom_container,    "Custom container instance reference
    lt_fieldcat   TYPE        lvc_t_fcat,                 "Field catalog
    ls_layout     TYPE        lvc_s_layo ,                "Layout structure
*        lt_toolbar       TYPE        ui_functions,              "Toolbar Function
*        lo_event_handler TYPE REF TO lcl_event_handler,         "Event handling
    ls_disvariant TYPE        disvariant,                 "Variant
    lo_alvgrid    TYPE REF TO cl_gui_alv_grid.


  IF go_alvgrid_9005 IS INITIAL .


*   Create custom container
    CREATE OBJECT go_container_9005
      EXPORTING
        container_name              = 'CC_9005'
      EXCEPTIONS
        cntl_error                  = 1
        cntl_system_error           = 2
        create_error                = 3
        lifetime_error              = 4
        lifetime_dynpro_dynpro_link = 5
        OTHERS                      = 6.
    IF sy-subrc NE 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.


**   Create docking container
*    CREATE OBJECT lo_docking
*      EXPORTING
*        parent = cl_gui_container=>screen0
**       ratio  = 90  " 90% yet not full-screen size
*      EXCEPTIONS
*        others = 6.
*    IF sy-subrc NE 0.
*      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
*                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
*    ENDIF.

**   Full size screen for ALV
*    CALL METHOD lo_docking->set_extension
*      EXPORTING
*        extension  = 99999  " full-screen size !!!
*      EXCEPTIONS
*        cntl_error = 1
*        OTHERS     = 2.
*    IF sy-subrc NE 0.
*      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
*                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
*    ENDIF.

* Create ALV grid
    CREATE OBJECT go_alvgrid_9005
      EXPORTING
        i_parent = go_container_9005
      EXCEPTIONS
        OTHERS   = 5.
    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.


*   Preparing field catalog
    PERFORM prepare_fcat_9005 CHANGING lt_fieldcat[].

*   Color Grid
    PERFORM color_grid_9005.

*   Preparing layout structure
    PERFORM prepare_layout_9005 CHANGING ls_layout.

* Prepare Variant
    ls_disvariant-report    = sy-repid.
    ls_disvariant-username  = sy-uname.

*   ALV Display
    CALL METHOD go_alvgrid_9005->set_table_for_first_display
      EXPORTING
        i_bypassing_buffer            = abap_true
*       i_structure_name              = gc_alv_8000-struct
        is_variant                    = ls_disvariant
        i_save                        = lc_a
        is_layout                     = ls_layout
*       it_toolbar_excluding          = lt_toolbar
      CHANGING
        it_outtab                     = gt_reco_ean[]
        it_fieldcatalog               = lt_fieldcat[]
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.


  ELSE.

*   ALV Refresh Display
    CALL METHOD go_alvgrid_9005->refresh_table_display
      EXPORTING
*       is_stable      = ls_stable
        i_soft_refresh = abap_true
      EXCEPTIONS
        finished       = 1
        OTHERS         = 2.
  ENDIF.



ENDFORM.                    " DISPLAY_ALV_9005
*&---------------------------------------------------------------------*
*&      Form  PREPARE_FCAT_9005
*&---------------------------------------------------------------------*
*   Preparing field catalog
*----------------------------------------------------------------------*
FORM prepare_fcat_9005  CHANGING fc_t_fieldcat TYPE lvc_t_fcat.

* Declaration
  DATA: lt_fieldcat  TYPE lvc_t_fcat,
        lv_desc      TYPE char40,
        lv_desc_stat TYPE char40.


  FIELD-SYMBOLS : <ls_fieldcat>    TYPE lvc_s_fcat.

* Prepare field catalog for ALV display by merging structure
  REFRESH: lt_fieldcat[].
  CALL FUNCTION 'LVC_FIELDCATALOG_MERGE'
    EXPORTING
      i_structure_name       = 'ZSARN_DATA_RECO_EAN'
    CHANGING
      ct_fieldcat            = lt_fieldcat[]
    EXCEPTIONS
      inconsistent_interface = 1
      program_error          = 2
      OTHERS                 = 3.
  IF sy-subrc NE 0.
    REFRESH: fc_t_fieldcat.
  ENDIF.





* Looping to make the output field
  LOOP AT lt_fieldcat ASSIGNING <ls_fieldcat>.


    <ls_fieldcat>-tooltip = <ls_fieldcat>-scrtext_l.
    <ls_fieldcat>-col_opt = abap_true.


    lv_desc = <ls_fieldcat>-scrtext_s.


    IF <ls_fieldcat>-fieldname CS 'SAP_'.
      <ls_fieldcat>-coltext = 'E:' && lv_desc.
      lv_desc_stat  = <ls_fieldcat>-scrtext_s.
    ELSEIF <ls_fieldcat>-fieldname+0(6) EQ 'STATUS'.
      <ls_fieldcat>-coltext = 'S:' && lv_desc_stat.
    ELSE.
      <ls_fieldcat>-coltext = 'R:' && lv_desc.
    ENDIF.


    CASE <ls_fieldcat>-fieldname.
      WHEN 'FAN'.                          <ls_fieldcat>-coltext+0(1) = 'K:'.
      WHEN 'CURRENT_VER'.
        <ls_fieldcat>-coltext+0(1) = 'K:'.
        <ls_fieldcat>-no_out = abap_true.
      WHEN 'IDNO'.                         <ls_fieldcat>-coltext+0(1) = 'K:'.
*      WHEN 'MEINH'.
*      WHEN 'EAN11'.
*      WHEN 'EANTP'.
*      WHEN 'HPEAN'.
      WHEN 'OVR_STATUS'.                   <ls_fieldcat>-coltext+0(1) = 'K:'.
      WHEN 'MATERIAL'.
      WHEN 'SAP_MATERIAL'.
      WHEN 'STATUS1'.
      WHEN 'UNIT'.
      WHEN 'SAP_UNIT'.
      WHEN 'STATUS2'.
      WHEN 'EAN_UPC'.
      WHEN 'SAP_EAN_UPC'.
      WHEN 'STATUS3'.
      WHEN 'EAN_CAT'.
      WHEN 'SAP_EAN_CAT'.
      WHEN 'STATUS4'.
      WHEN 'HPEAN'.
      WHEN 'SAP_HPEAN'.
      WHEN 'STATUS5'.

      WHEN OTHERS.
        DELETE lt_fieldcat WHERE fieldname = <ls_fieldcat>-fieldname.

    ENDCASE.

  ENDLOOP.

  fc_t_fieldcat[] = lt_fieldcat[].

ENDFORM.                    " PREPARE_FCAT_9005
*&---------------------------------------------------------------------*
*&      Form  COLOR_GRID_9005
*&---------------------------------------------------------------------*
*   Color Grid
*----------------------------------------------------------------------*
FORM color_grid_9005.

  DATA: col    TYPE lvc_s_scol,
        coltab TYPE lvc_t_scol,
        color  TYPE lvc_s_colo.


  FIELD-SYMBOLS: <ls_reco_ean>     TYPE zsarn_data_reco_ean.

  LOOP AT gt_reco_ean[] ASSIGNING <ls_reco_ean>.

    CLEAR: coltab[].

    CLEAR col. col-fname = 'FAN'.                  col-color-col = '7'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'CURRENT_VER'.          col-color-col = '7'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'IDNO'.                 col-color-col = '7'. APPEND col TO coltab[].
*    CLEAR col. col-fname = 'MEINH'.                col-color-col = '7'. APPEND col TO coltab[].
*    CLEAR col. col-fname = 'EAN11'.                col-color-col = '7'. APPEND col TO coltab[].
*    CLEAR col. col-fname = 'EANTP'.                col-color-col = '7'. APPEND col TO coltab[].
*    CLEAR col. col-fname = 'HPEAN'.                col-color-col = '7'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'OVR_STATUS'.           col-color-col = '7'. APPEND col TO coltab[].



    CLEAR col. col-fname = 'MATERIAL'.             col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_MATERIAL'.         col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS1'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'UNIT'.                 col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_UNIT'.             col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS2'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'EAN_UPC'.              col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_EAN_UPC'.          col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS3'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'EAN_CAT'.              col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_EAN_CAT'.          col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS4'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'HPEAN'.                col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_HPEAN'.            col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS5'.              col-color-col = '3'. APPEND col TO coltab[].

    <ls_reco_ean>-color = coltab[].

  ENDLOOP.

ENDFORM.                    " COLOR_GRID_9005
*&---------------------------------------------------------------------*
*&      Form  PREPARE_LAYOUT_9005
*&---------------------------------------------------------------------*
*       Field Catalog for STO Display
*----------------------------------------------------------------------*
FORM prepare_layout_9005 CHANGING fc_s_layout    TYPE lvc_s_layo.

  DATA: lv_count TYPE char15.

  fc_s_layout-ctab_fname = 'COLOR'.
  fc_s_layout-sel_mode   = 'A'.

  lv_count = gv_count_9005.
  CONDENSE lv_count NO-GAPS.

  CONCATENATE TEXT-002 lv_count
         INTO fc_s_layout-grid_title
 SEPARATED BY space.


ENDFORM.                    " PREPARE_LAYOUT_9005
*&---------------------------------------------------------------------*
*&      Form  DISPLAY_ALV_9006
*&---------------------------------------------------------------------*
* Display ALV 9006
*----------------------------------------------------------------------*
FORM display_alv_9006.


  CONSTANTS: lc_a        TYPE char01 VALUE 'A'.

* Declaration
  DATA: "lo_docking       TYPE REF TO cl_gui_docking_container,  "Custom container instance reference
    lo_container  TYPE REF TO cl_gui_custom_container,    "Custom container instance reference
    lt_fieldcat   TYPE        lvc_t_fcat,                 "Field catalog
    ls_layout     TYPE        lvc_s_layo ,                "Layout structure
*        lt_toolbar       TYPE        ui_functions,              "Toolbar Function
*        lo_event_handler TYPE REF TO lcl_event_handler,         "Event handling
    ls_disvariant TYPE        disvariant,                 "Variant
    lo_alvgrid    TYPE REF TO cl_gui_alv_grid.


* Filter Display Data for screen 9006
  PERFORM filter_display_data_9006 USING gt_reco_pir[]
                                         gv_pir_rel_9006
                                CHANGING gt_output_9006[].


*   Color Grid
  PERFORM color_grid_9006 CHANGING gt_output_9006[].




  IF go_alvgrid_9006 IS INITIAL .


*   Create custom container
    CREATE OBJECT go_container_9006
      EXPORTING
        container_name              = 'CC_9006'
      EXCEPTIONS
        cntl_error                  = 1
        cntl_system_error           = 2
        create_error                = 3
        lifetime_error              = 4
        lifetime_dynpro_dynpro_link = 5
        OTHERS                      = 6.
    IF sy-subrc NE 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.


**   Create docking container
*    CREATE OBJECT lo_docking
*      EXPORTING
*        parent = cl_gui_container=>screen0
**       ratio  = 90  " 90% yet not full-screen size
*      EXCEPTIONS
*        others = 6.
*    IF sy-subrc NE 0.
*      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
*                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
*    ENDIF.

**   Full size screen for ALV
*    CALL METHOD lo_docking->set_extension
*      EXPORTING
*        extension  = 99999  " full-screen size !!!
*      EXCEPTIONS
*        cntl_error = 1
*        OTHERS     = 2.
*    IF sy-subrc NE 0.
*      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
*                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
*    ENDIF.

* Create ALV grid
    CREATE OBJECT go_alvgrid_9006
      EXPORTING
        i_parent = go_container_9006
      EXCEPTIONS
        OTHERS   = 5.
    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.


*   Preparing field catalog
    PERFORM prepare_fcat_9006 CHANGING lt_fieldcat[].

*   Preparing layout structure
    PERFORM prepare_layout_9006 CHANGING ls_layout.

* Prepare Variant
    ls_disvariant-report    = sy-repid.
    ls_disvariant-username  = sy-uname.

*   ALV Display
    CALL METHOD go_alvgrid_9006->set_table_for_first_display
      EXPORTING
        i_bypassing_buffer            = abap_true
*       i_structure_name              = gc_alv_8000-struct
        is_variant                    = ls_disvariant
        i_save                        = lc_a
        is_layout                     = ls_layout
*       it_toolbar_excluding          = lt_toolbar
      CHANGING
        it_outtab                     = gt_output_9006[]
        it_fieldcatalog               = lt_fieldcat[]
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.


  ELSE.

*   ALV Refresh Display
    CALL METHOD go_alvgrid_9006->refresh_table_display
      EXPORTING
*       is_stable      = ls_stable
        i_soft_refresh = abap_true
      EXCEPTIONS
        finished       = 1
        OTHERS         = 2.
  ENDIF.



ENDFORM.                    " DISPLAY_ALV_9006
*&---------------------------------------------------------------------*
*&      Form  FILTER_DISPLAY_DATA_9006
*&---------------------------------------------------------------------*
* Filter Display Data for screen 9006
*----------------------------------------------------------------------*
FORM filter_display_data_9006 USING fc_t_reco_pir TYPE ty_t_reco_pir
                                    fc_v_pir_rel  TYPE xfeld
                           CHANGING fc_t_output   TYPE ty_t_reco_pir.


  fc_t_output[] = fc_t_reco_pir[].

  IF fc_v_pir_rel IS NOT INITIAL.
    DELETE fc_t_output[] WHERE pir_rel_eina IS INITIAL
                           AND pir_rel_eine IS INITIAL.
  ENDIF.


ENDFORM.                    " FILTER_DISPLAY_DATA_9006
*&---------------------------------------------------------------------*
*&      Form  PREPARE_FCAT_9006
*&---------------------------------------------------------------------*
*   Preparing field catalog
*----------------------------------------------------------------------*
FORM prepare_fcat_9006  CHANGING fc_t_fieldcat TYPE lvc_t_fcat.

* Declaration
  DATA: lt_fieldcat  TYPE lvc_t_fcat,
        lv_desc      TYPE char40,
        lv_desc_stat TYPE char40.


  FIELD-SYMBOLS : <ls_fieldcat>    TYPE lvc_s_fcat.

* Prepare field catalog for ALV display by merging structure
  REFRESH: lt_fieldcat[].
  CALL FUNCTION 'LVC_FIELDCATALOG_MERGE'
    EXPORTING
      i_structure_name       = 'ZSARN_DATA_RECO_PIR'
    CHANGING
      ct_fieldcat            = lt_fieldcat[]
    EXCEPTIONS
      inconsistent_interface = 1
      program_error          = 2
      OTHERS                 = 3.
  IF sy-subrc NE 0.
    REFRESH: fc_t_fieldcat.
  ENDIF.





* Looping to make the output field
  LOOP AT lt_fieldcat ASSIGNING <ls_fieldcat>.


    <ls_fieldcat>-tooltip = <ls_fieldcat>-scrtext_l.
    <ls_fieldcat>-col_opt = abap_true.


    lv_desc = <ls_fieldcat>-scrtext_s.


    IF <ls_fieldcat>-fieldname CS 'SAP_'.
      <ls_fieldcat>-coltext = 'E:' && lv_desc.
      lv_desc_stat  = <ls_fieldcat>-scrtext_s.
    ELSEIF <ls_fieldcat>-fieldname+0(6) EQ 'STATUS'.
      <ls_fieldcat>-coltext = 'S:' && lv_desc_stat.
    ELSEIF <ls_fieldcat>-fieldname+0(3) EQ 'RO_'.
      <ls_fieldcat>-coltext = 'RO:' && lv_desc.
    ELSEIF <ls_fieldcat>-fieldname+0(5) EQ 'CALC_'.
      <ls_fieldcat>-coltext = 'RC:' && lv_desc.
    ELSE.
      <ls_fieldcat>-coltext = 'R:' && lv_desc.
    ENDIF.


    CASE <ls_fieldcat>-fieldname.
      WHEN 'FAN'.                             <ls_fieldcat>-coltext+0(1) = 'K:'.
      WHEN 'CURRENT_VER'.
        <ls_fieldcat>-coltext+0(1) = 'K:'.
        <ls_fieldcat>-no_out = abap_true.
      WHEN 'IDNO'.                            <ls_fieldcat>-coltext+0(1) = 'K:'.
      WHEN 'ORDER_UOM_PIM'.                   <ls_fieldcat>-coltext+0(1) = 'K:'.
        "<ls_fieldcat>-no_out = abap_true.
      WHEN 'HYBRIS_INTERNAL_CODE'.            <ls_fieldcat>-coltext+0(1) = 'K:'.
        "<ls_fieldcat>-no_out = abap_true.
      WHEN 'GLN_NO'.                          <ls_fieldcat>-coltext+0(1) = 'K:'.
        "<ls_fieldcat>-no_out = abap_true.
      WHEN 'LOWER_UOM'.                       <ls_fieldcat>-coltext+0(1) = 'K:'.
        "<ls_fieldcat>-no_out = abap_true.
      WHEN 'LOWER_CHILD_UOM'.                 <ls_fieldcat>-coltext+0(1) = 'K:'.
        "<ls_fieldcat>-no_out = abap_true.
      WHEN 'PIR_REL_EINA'.                    <ls_fieldcat>-coltext+0(1) = 'K:'.
      WHEN 'PIR_REL_EINE'.                    <ls_fieldcat>-coltext+0(1) = 'K:'.
      WHEN 'OVR_STATUS'.                      <ls_fieldcat>-coltext+0(1) = 'K:'.

      WHEN 'MATERIAL'.
      WHEN 'SAP_MATERIAL'.
      WHEN 'STATUS1'.
      WHEN 'VENDOR'.
      WHEN 'SAP_VENDOR'.
      WHEN 'STATUS2'.
      WHEN 'PO_UNIT'.
      WHEN 'SAP_PO_UNIT'.
      WHEN 'STATUS3'.
      WHEN 'CALC_UMREZ'.
        <ls_fieldcat>-tooltip = <ls_fieldcat>-tooltip && '(EINA & BUOM conv.)'.
      WHEN 'SAP_UMREZ'.
      WHEN 'STATUS4'.
      WHEN 'CALC_UMREN'.
        <ls_fieldcat>-tooltip = <ls_fieldcat>-tooltip && '(EINA & BUOM conv.)'.
      WHEN 'SAP_UMREN'.
      WHEN 'STATUS5'.
      WHEN 'VEND_MAT'.
      WHEN 'SAP_VEND_MAT'.
      WHEN 'STATUS6'.
      WHEN 'BASE_UOM'.
      WHEN 'SAP_BASE_UOM'.
      WHEN 'STATUS7'.
      WHEN 'VAR_ORD_UN'.
      WHEN 'SAP_VAR_ORD_UN'.
      WHEN 'STATUS8'.
      WHEN 'SUPPL_FROM'.
      WHEN 'SAP_SUPPL_FROM'.
      WHEN 'STATUS9'.
      WHEN 'SUPPL_TO'.
      WHEN 'SAP_SUPPL_TO'.
      WHEN 'STATUS10'.
      WHEN 'NORM_VEND'.
      WHEN 'SAP_NORM_VEND'.
      WHEN 'STATUS11'.


      WHEN 'PURCH_ORG'.
      WHEN 'SAP_PURCH_ORG'.
      WHEN 'STATUS12'.
      WHEN 'INFO_TYPE'.
      WHEN 'SAP_INFO_TYPE'.
      WHEN 'STATUS13'.
*      WHEN 'PLANT'.
*      WHEN 'SAP_PLANT'.
*      WHEN 'STATUS14'.
*      WHEN 'DELETE_IND'.
*      WHEN 'SAP_DELETE_IND'.
*      WHEN 'STATUS15'.
      WHEN 'PUR_GROUP'.
      WHEN 'SAP_PUR_GROUP'.
      WHEN 'STATUS16'.
      WHEN 'CURRENCY'.
      WHEN 'SAP_CURRENCY'.
      WHEN 'STATUS17'.
      WHEN 'MIN_PO_QTY'.     CLEAR: <ls_fieldcat>-quantity, <ls_fieldcat>-qfieldname.
      WHEN 'SAP_MIN_PO_QTY'. CLEAR: <ls_fieldcat>-quantity, <ls_fieldcat>-qfieldname.
      WHEN 'STATUS18'.
      WHEN 'NRM_PO_QTY'.     CLEAR: <ls_fieldcat>-quantity, <ls_fieldcat>-qfieldname.
      WHEN 'SAP_NRM_PO_QTY'. CLEAR: <ls_fieldcat>-quantity, <ls_fieldcat>-qfieldname.
      WHEN 'STATUS19'.
      WHEN 'PLND_DELRY'.
      WHEN 'SAP_PLND_DELRY'.
      WHEN 'STATUS20'.
*      WHEN 'OVERDELTOL'.
*      WHEN 'SAP_OVERDELTOL'.
*      WHEN 'STATUS21'.
*      WHEN 'UNDER_TOL'.
*      WHEN 'SAP_UNDER_TOL'.
*      WHEN 'STATUS22'.
      WHEN 'NET_PRICE'.
      WHEN 'SAP_NET_PRICE'.
      WHEN 'STATUS23'.
      WHEN 'PRICE_UNIT'.
      WHEN 'SAP_PRICE_UNIT'.
      WHEN 'STATUS24'.
      WHEN 'ORDERPR_UN'.
      WHEN 'SAP_ORDERPR_UN'.
      WHEN 'STATUS25'.
      WHEN 'PRICE_DATE'.
      WHEN 'SAP_PRICE_DATE'.
      WHEN 'STATUS26'.
      WHEN 'CALC_BPUMZ'.
        <ls_fieldcat>-tooltip = <ls_fieldcat>-tooltip && '(EINA & EINE conv.)'.
      WHEN 'SAP_BPUMZ'.
      WHEN 'STATUS27'.
      WHEN 'CALC_BPUMN'.
        <ls_fieldcat>-tooltip = <ls_fieldcat>-tooltip && '(EINA & EINE conv.)'.
      WHEN 'SAP_BPUMN'.
      WHEN 'STATUS28'.
*      WHEN 'EFF_PRICE'.
*      WHEN 'SAP_EFF_PRICE'.
*      WHEN 'STATUS29'.
      WHEN 'COND_GROUP'.
      WHEN 'SAP_COND_GROUP'.
      WHEN 'STATUS30'.
      WHEN 'TAX_CODE'.
      WHEN 'SAP_TAX_CODE'.
      WHEN 'STATUS31'.

      WHEN 'RO_CURRENCY'.
      WHEN 'RO_NET_PRICE'.
      WHEN 'RO_PRICE_UNIT'.
      WHEN 'RO_ORDERPR_UN'.
      WHEN 'RO_PRICE_DATE'.
      WHEN 'RO_EFF_PRICE'.

      WHEN OTHERS.
        DELETE lt_fieldcat WHERE fieldname = <ls_fieldcat>-fieldname.

    ENDCASE.

  ENDLOOP.

  fc_t_fieldcat[] = lt_fieldcat[].

ENDFORM.                    " PREPARE_FCAT_9006
*&---------------------------------------------------------------------*
*&      Form  COLOR_GRID_9006
*&---------------------------------------------------------------------*
*   Color Grid
*----------------------------------------------------------------------*
FORM color_grid_9006 CHANGING fc_t_output TYPE ztarn_data_reco_pir.

  DATA: col    TYPE lvc_s_scol,
        coltab TYPE lvc_t_scol,
        color  TYPE lvc_s_colo.



  FIELD-SYMBOLS: <ls_reco_pir>     TYPE zsarn_data_reco_pir.

  LOOP AT fc_t_output[] ASSIGNING <ls_reco_pir>.

    CLEAR: coltab[].

    CLEAR col. col-fname = 'FAN'.                  col-color-col = '7'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'CURRENT_VER'.          col-color-col = '7'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'IDNO'.                 col-color-col = '7'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'ORDER_UOM_PIM'.        col-color-col = '7'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'HYBRIS_INTERNAL_CODE'. col-color-col = '7'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'GLN_NO'.               col-color-col = '7'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'LOWER_UOM'.            col-color-col = '7'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'LOWER_CHILD_UOM'.      col-color-col = '7'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'PIR_REL_EINA'.         col-color-col = '7'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'PIR_REL_EINE'.         col-color-col = '7'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'OVR_STATUS'.           col-color-col = '7'. APPEND col TO coltab[].



    CLEAR col. col-fname = 'MATERIAL'.             col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_MATERIAL'.         col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS1'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'MATERIAL'.             col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_MATERIAL'.         col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS1'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'VENDOR'.               col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_VENDOR'.           col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS2'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'PO_UNIT'.              col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_PO_UNIT'.          col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS3'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'CALC_UMREZ'.                col-color-col = '6'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_UMREZ'.            col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS4'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'CALC_UMREN'.                col-color-col = '6'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_UMREN'.            col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS5'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'VEND_MAT'.             col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_VEND_MAT'.         col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS6'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'BASE_UOM'.             col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_BASE_UOM'.         col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS7'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'VAR_ORD_UN'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_VAR_ORD_UN'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS8'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'SUPPL_FROM'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_SUPPL_FROM'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS9'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'SUPPL_TO'.             col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_SUPPL_TO'.         col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS10'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'NORM_VEND'.            col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_NORM_VEND'.        col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS11'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'PURCH_ORG'.            col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_PURCH_ORG'.        col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS12'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'INFO_TYPE'.            col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_INFO_TYPE'.        col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS13'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'PLANT'.                col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_PLANT'.            col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS14'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'DELETE_IND'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_DELETE_IND'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS15'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'PUR_GROUP'.            col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_PUR_GROUP'.        col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS16'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'CURRENCY'.             col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_CURRENCY'.         col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS17'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'MIN_PO_QTY'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_MIN_PO_QTY'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS18'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'NRM_PO_QTY'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_NRM_PO_QTY'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS19'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'PLND_DELRY'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_PLND_DELRY'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS20'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'OVERDELTOL'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_OVERDELTOL'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS21'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'UNDER_TOL'.            col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_UNDER_TOL'.        col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS22'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'NET_PRICE'.            col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_NET_PRICE'.        col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS23'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'PRICE_UNIT'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_PRICE_UNIT'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS24'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ORDERPR_UN'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ORDERPR_UN'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS25'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'PRICE_DATE'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_PRICE_DATE'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS26'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'CALC_BPUMZ'.                col-color-col = '6'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_BPUMZ'.            col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS27'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'CALC_BPUMN'.                col-color-col = '6'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_BPUMN'.            col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS28'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'EFF_PRICE'.            col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_EFF_PRICE'.        col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS29'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'COND_GROUP'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_COND_GROUP'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS30'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'TAX_CODE'.             col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_TAX_CODE'.         col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS31'.             col-color-col = '3'. APPEND col TO coltab[].


    CLEAR col. col-fname = 'RO_CURRENCY'.          col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'RO_NET_PRICE'.         col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'RO_PRICE_UNIT'.        col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'RO_ORDERPR_UN'.        col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'RO_PRICE_DATE'.        col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'RO_EFF_PRICE'.         col-color-col = '5'. APPEND col TO coltab[].


    <ls_reco_pir>-color = coltab[].

  ENDLOOP.

ENDFORM.                    " COLOR_GRID_9006
*&---------------------------------------------------------------------*
*&      Form  PREPARE_LAYOUT_9006
*&---------------------------------------------------------------------*
*       Field Catalog for STO Display
*----------------------------------------------------------------------*
FORM prepare_layout_9006 CHANGING fc_s_layout    TYPE lvc_s_layo.

  DATA: lv_count TYPE char15.

  fc_s_layout-ctab_fname = 'COLOR'.
  fc_s_layout-sel_mode   = 'A'.

  lv_count = gv_count_9006.
  CONDENSE lv_count NO-GAPS.

  CONCATENATE TEXT-002 lv_count
         INTO fc_s_layout-grid_title
 SEPARATED BY space.


ENDFORM.                    " PREPARE_LAYOUT_9006
*&---------------------------------------------------------------------*
*&      Form  DISPLAY_ALV_9007
*&---------------------------------------------------------------------*
* Display ALV 9007
*----------------------------------------------------------------------*
FORM display_alv_9007.


  CONSTANTS: lc_a        TYPE char01 VALUE 'A'.

* Declaration
  DATA: "lo_docking       TYPE REF TO cl_gui_docking_container,  "Custom container instance reference
    lo_container  TYPE REF TO cl_gui_custom_container,    "Custom container instance reference
    lt_fieldcat   TYPE        lvc_t_fcat,                 "Field catalog
    ls_layout     TYPE        lvc_s_layo ,                "Layout structure
*        lt_toolbar       TYPE        ui_functions,              "Toolbar Function
*        lo_event_handler TYPE REF TO lcl_event_handler,         "Event handling
    ls_disvariant TYPE        disvariant,                 "Variant
    lo_alvgrid    TYPE REF TO cl_gui_alv_grid.


  IF go_alvgrid_9007 IS INITIAL .


*   Create custom container
    CREATE OBJECT go_container_9007
      EXPORTING
        container_name              = 'CC_9007'
      EXCEPTIONS
        cntl_error                  = 1
        cntl_system_error           = 2
        create_error                = 3
        lifetime_error              = 4
        lifetime_dynpro_dynpro_link = 5
        OTHERS                      = 6.
    IF sy-subrc NE 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.


**   Create docking container
*    CREATE OBJECT lo_docking
*      EXPORTING
*        parent = cl_gui_container=>screen0
**       ratio  = 90  " 90% yet not full-screen size
*      EXCEPTIONS
*        others = 6.
*    IF sy-subrc NE 0.
*      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
*                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
*    ENDIF.

**   Full size screen for ALV
*    CALL METHOD lo_docking->set_extension
*      EXPORTING
*        extension  = 99999  " full-screen size !!!
*      EXCEPTIONS
*        cntl_error = 1
*        OTHERS     = 2.
*    IF sy-subrc NE 0.
*      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
*                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
*    ENDIF.

* Create ALV grid
    CREATE OBJECT go_alvgrid_9007
      EXPORTING
        i_parent = go_container_9007
      EXCEPTIONS
        OTHERS   = 5.
    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.


*   Preparing field catalog
    PERFORM prepare_fcat_9007 CHANGING lt_fieldcat[].

*   Color Grid
    PERFORM color_grid_9007.

*   Preparing layout structure
    PERFORM prepare_layout_9007 CHANGING ls_layout.

* Prepare Variant
    ls_disvariant-report    = sy-repid.
    ls_disvariant-username  = sy-uname.

*   ALV Display
    CALL METHOD go_alvgrid_9007->set_table_for_first_display
      EXPORTING
        i_bypassing_buffer            = abap_true
*       i_structure_name              = gc_alv_8000-struct
        is_variant                    = ls_disvariant
        i_save                        = lc_a
        is_layout                     = ls_layout
*       it_toolbar_excluding          = lt_toolbar
      CHANGING
        it_outtab                     = gt_reco_banner[]
        it_fieldcatalog               = lt_fieldcat[]
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.


  ELSE.

*   ALV Refresh Display
    CALL METHOD go_alvgrid_9007->refresh_table_display
      EXPORTING
*       is_stable      = ls_stable
        i_soft_refresh = abap_true
      EXCEPTIONS
        finished       = 1
        OTHERS         = 2.
  ENDIF.



ENDFORM.                    " DISPLAY_ALV_9007
*&---------------------------------------------------------------------*
*&      Form  PREPARE_FCAT_9007
*&---------------------------------------------------------------------*
*   Preparing field catalog
*----------------------------------------------------------------------*
FORM prepare_fcat_9007  CHANGING fc_t_fieldcat TYPE lvc_t_fcat.

* Declaration
  DATA: lt_fieldcat  TYPE lvc_t_fcat,
        lv_desc      TYPE char40,
        lv_desc_stat TYPE char40.


  FIELD-SYMBOLS : <ls_fieldcat>    TYPE lvc_s_fcat.

* Prepare field catalog for ALV display by merging structure
  REFRESH: lt_fieldcat[].
  CALL FUNCTION 'LVC_FIELDCATALOG_MERGE'
    EXPORTING
      i_structure_name       = 'ZSARN_DATA_RECO_BANNER'
    CHANGING
      ct_fieldcat            = lt_fieldcat[]
    EXCEPTIONS
      inconsistent_interface = 1
      program_error          = 2
      OTHERS                 = 3.
  IF sy-subrc NE 0.
    REFRESH: fc_t_fieldcat.
  ENDIF.





* Looping to make the output field
  LOOP AT lt_fieldcat ASSIGNING <ls_fieldcat>.


    <ls_fieldcat>-tooltip = <ls_fieldcat>-scrtext_l.
    <ls_fieldcat>-col_opt = abap_true.


    lv_desc = <ls_fieldcat>-scrtext_s.


    IF <ls_fieldcat>-fieldname CS 'SAP_'.
      <ls_fieldcat>-coltext = 'E:' && lv_desc.
      lv_desc_stat  = <ls_fieldcat>-scrtext_s.
    ELSEIF <ls_fieldcat>-fieldname+0(6) EQ 'STATUS'.
      <ls_fieldcat>-coltext = 'S:' && lv_desc_stat.
    ELSE.
      <ls_fieldcat>-coltext = 'R:' && lv_desc.
    ENDIF.


    CASE <ls_fieldcat>-fieldname.
      WHEN 'FAN'.                         <ls_fieldcat>-coltext+0(1) = 'K:'.
      WHEN 'CURRENT_VER'.
        <ls_fieldcat>-coltext+0(1) = 'K:'.
        <ls_fieldcat>-no_out = abap_true.
      WHEN 'IDNO'.                        <ls_fieldcat>-coltext+0(1) = 'K:'.
      WHEN 'BANNER'.                      <ls_fieldcat>-coltext+0(1) = 'K:'.
      WHEN 'OVR_STATUS'.                  <ls_fieldcat>-coltext+0(1) = 'K:'.

      WHEN 'MATERIAL'.
      WHEN 'SAP_MATERIAL'.
      WHEN 'STATUS1'.
*      WHEN 'SALES_ORG'.
*      WHEN 'SAP_SALES_ORG'.
*      WHEN 'STATUS2'.
*      WHEN 'DISTR_CHAN'.
*      WHEN 'SAP_DISTR_CHAN'.
*      WHEN 'STATUS3'.
      WHEN 'UNI_SUP_SOURCE'.
      WHEN 'SAP_UNI_SUP_SOURCE'.
      WHEN 'STATUS4'.
      WHEN 'LNI_SUP_SOURCE'.
      WHEN 'SAP_LNI_SUP_SOURCE'.
      WHEN 'STATUS5'.
      WHEN 'MATL_STATS'.
      WHEN 'SAP_MATL_STATS'.
      WHEN 'STATUS6'.
*      WHEN 'CASH_DISC'.
*      WHEN 'SAP_CASH_DISC'.
*      WHEN 'STATUS7'.

*      WHEN 'SAL_STATUS'.
*      WHEN 'SAP_SAL_STATUS'.      "--IR5047357 JKH 16.08.2016
*      WHEN 'STATUS8'.
*      WHEN 'VALID_FROM'.
*      WHEN 'SAP_VALID_FROM'.      "--IR5047357 JKH 16.08.2016
*      WHEN 'STATUS9'.

      WHEN 'MIN_ORDER'.
      WHEN 'SAP_MIN_ORDER'.
      WHEN 'STATUS10'.
      WHEN 'SALES_UNIT'.
      WHEN 'SAP_SALES_UNIT'.
      WHEN 'STATUS11'.
*      WHEN 'ITEM_CAT'.
*      WHEN 'SAP_ITEM_CAT'.
*      WHEN 'STATUS12'.
      WHEN 'PROD_HIER'.
      WHEN 'SAP_PROD_HIER'.
      WHEN 'STATUS13'.
      WHEN 'ACCT_ASSGT'.
      WHEN 'SAP_ACCT_ASSGT'.
      WHEN 'STATUS14'.
      WHEN 'ASSORT_LEV'.
      WHEN 'SAP_ASSORT_LEV'.
      WHEN 'STATUS15'.
*      WHEN 'MAT_PR_GRP'.
*      WHEN 'SAP_MAT_PR_GRP'.
*      WHEN 'STATUS16'.
*      WHEN 'LI_PROC_ST'.
*      WHEN 'SAP_LI_PROC_ST'.
*      WHEN 'STATUS17'.
*      WHEN 'LI_PROC_DC'.
*      WHEN 'SAP_LI_PROC_DC'.
*      WHEN 'STATUS18'.
*      WHEN 'LIST_ST_FR'.
*      WHEN 'SAP_LIST_ST_FR'.
*      WHEN 'STATUS19'.
*      WHEN 'LIST_ST_TO'.
*      WHEN 'SAP_LIST_ST_TO'.
*      WHEN 'STATUS20'.
*      WHEN 'LIST_DC_FR'.
*      WHEN 'SAP_LIST_DC_FR'.
*      WHEN 'STATUS21'.
*      WHEN 'LIST_DC_TO'.
*      WHEN 'SAP_LIST_DC_TO'.
*      WHEN 'STATUS22'.
*      WHEN 'SELL_ST_FR'.
*      WHEN 'SAP_SELL_ST_FR'.
*      WHEN 'STATUS23'.
*      WHEN 'SELL_ST_TO'.
*      WHEN 'SAP_SELL_ST_TO'.
*      WHEN 'STATUS24'.
*      WHEN 'SELL_DC_FR'.
*      WHEN 'SAP_SELL_DC_FR'.
*      WHEN 'STATUS25'.
*      WHEN 'SELL_DC_TO'.
*      WHEN 'SAP_SELL_DC_TO'.
*      WHEN 'STATUS26'.
      WHEN 'ZZCATMAN'.
      WHEN 'SAP_ZZCATMAN'.
      WHEN 'STATUS27'.
      WHEN 'WLK2_NO_REP_KEY'.
      WHEN 'SAP_WLK2_NO_REP_KEY'.
      WHEN 'STATUS28'.
      WHEN 'WLK2_PRICE_REQD'.
      WHEN 'SAP_WLK2_PRICE_REQD'.
      WHEN 'STATUS29'.
*      WHEN 'WLK2_SELL_ST_FR'.
*      WHEN 'SAP_WLK2_SELL_ST_FR'.
*      WHEN 'STATUS30'.
*      WHEN 'WLK2_SELL_ST_TO'.
*      WHEN 'SAP_WLK2_SELL_ST_TO'.
*      WHEN 'STATUS31'.
*      WHEN 'WLK2_SAL_STATUS'.
*      WHEN 'SAP_WLK2_SAL_STATUS'.
*      WHEN 'STATUS32'.
*      WHEN 'WLK2_VALID_FROM'.
*      WHEN 'SAP_WLK2_VALID_FROM'.
*      WHEN 'STATUS33'.
      WHEN 'WLK2_DISC_ALLWD'.
      WHEN 'SAP_WLK2_DISC_ALLWD'.
      WHEN 'STATUS34'.
      WHEN 'WLK2_SCALES_GRP'.
      WHEN 'SAP_WLK2_SCALES_GRP'.
      WHEN 'STATUS35'.

      WHEN 'ZZ_PBS'.
        <ls_fieldcat>-coltext+2 = 'PayByScan'.
      WHEN 'SAP_ZZ_PBS'.
        <ls_fieldcat>-coltext+2 = 'PayByScan'.
      WHEN 'STATUS36'.

      WHEN 'ZZONLINE_STATUS'.                           "++ONLD-822 JKH 17.02.2017
      WHEN 'SAP_ZZONLINE_STATUS'.
      WHEN 'STATUS37'.

      WHEN OTHERS.
        DELETE lt_fieldcat WHERE fieldname = <ls_fieldcat>-fieldname.

    ENDCASE.

  ENDLOOP.

  fc_t_fieldcat[] = lt_fieldcat[].

ENDFORM.                    " PREPARE_FCAT_9007
*&---------------------------------------------------------------------*
*&      Form  COLOR_GRID_9007
*&---------------------------------------------------------------------*
*   Color Grid
*----------------------------------------------------------------------*
FORM color_grid_9007.

  DATA: col    TYPE lvc_s_scol,
        coltab TYPE lvc_t_scol,
        color  TYPE lvc_s_colo.


  FIELD-SYMBOLS: <ls_reco_banner>     TYPE zsarn_data_reco_banner.

  LOOP AT gt_reco_banner[] ASSIGNING <ls_reco_banner>.

    CLEAR: coltab[].

    CLEAR col. col-fname = 'FAN'.                  col-color-col = '7'.  APPEND col TO coltab[].
    CLEAR col. col-fname = 'CURRENT_VER'.          col-color-col = '7'.  APPEND col TO coltab[].
    CLEAR col. col-fname = 'IDNO'.                 col-color-col = '7'.  APPEND col TO coltab[].
    CLEAR col. col-fname = 'BANNER'.               col-color-col = '7'.  APPEND col TO coltab[].
    CLEAR col. col-fname = 'OVR_STATUS'.           col-color-col = '7'.  APPEND col TO coltab[].



    CLEAR col. col-fname = 'MATERIAL'.             col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_MATERIAL'.         col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS1'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'MATERIAL'.             col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_MATERIAL'.         col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS1'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'SALES_ORG'.            col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_SALES_ORG'.        col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS2'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'DISTR_CHAN'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_DISTR_CHAN'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS3'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'UNI_SUP_SOURCE'.       col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_UNI_SUP_SOURCE'.   col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS4'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'LNI_SUP_SOURCE'.       col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_LNI_SUP_SOURCE'.   col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS5'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'MATL_STATS'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_MATL_STATS'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS6'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'CASH_DISC'.            col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_CASH_DISC'.        col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS7'.              col-color-col = '3'. APPEND col TO coltab[].

*     "--IR5047357 JKH 16.08.2016
*    CLEAR col. col-fname = 'SAL_STATUS'.           col-color-col = '5'. APPEND col TO coltab[].
*    CLEAR col. col-fname = 'SAP_SAL_STATUS'.       col-color-col = '4'. APPEND col TO coltab[].
*    CLEAR col. col-fname = 'STATUS8'.              col-color-col = '3'. APPEND col TO coltab[].
*
*      "--IR5047357 JKH 16.08.2016
*    CLEAR col. col-fname = 'VALID_FROM'.           col-color-col = '5'. APPEND col TO coltab[].
*    CLEAR col. col-fname = 'SAP_VALID_FROM'.       col-color-col = '4'. APPEND col TO coltab[].
*    CLEAR col. col-fname = 'STATUS9'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'MIN_ORDER'.            col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_MIN_ORDER'.        col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS10'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'SALES_UNIT'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_SALES_UNIT'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS11'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ITEM_CAT'.             col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ITEM_CAT'.         col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS12'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'PROD_HIER'.            col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_PROD_HIER'.        col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS13'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ACCT_ASSGT'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ACCT_ASSGT'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS14'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ASSORT_LEV'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ASSORT_LEV'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS15'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'MAT_PR_GRP'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_MAT_PR_GRP'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS16'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'LI_PROC_ST'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_LI_PROC_ST'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS17'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'LI_PROC_DC'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_LI_PROC_DC'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS18'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'LIST_ST_FR'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_LIST_ST_FR'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS19'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'LIST_ST_TO'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_LIST_ST_TO'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS20'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'LIST_DC_FR'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_LIST_DC_FR'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS21'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'LIST_DC_TO'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_LIST_DC_TO'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS22'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'SELL_ST_FR'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_SELL_ST_FR'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS23'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'SELL_ST_TO'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_SELL_ST_TO'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS24'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'SELL_DC_FR'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_SELL_DC_FR'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS25'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'SELL_DC_TO'.           col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_SELL_DC_TO'.       col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS26'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZCATMAN'.             col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZCATMAN'.         col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS27'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'WLK2_NO_REP_KEY'.      col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_WLK2_NO_REP_KEY'.  col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS28'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'WLK2_PRICE_REQD'.      col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_WLK2_PRICE_REQD'.  col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS29'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'WLK2_SELL_ST_FR'.      col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_WLK2_SELL_ST_FR'.  col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS30'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'WLK2_SELL_ST_TO'.      col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_WLK2_SELL_ST_TO'.  col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS31'.             col-color-col = '3'. APPEND col TO coltab[].

*     "--IR5047357 JKH 16.08.2016
*    CLEAR col. col-fname = 'WLK2_SAL_STATUS'.      col-color-col = '5'. APPEND col TO coltab[].
*    CLEAR col. col-fname = 'SAP_WLK2_SAL_STATUS'.  col-color-col = '4'. APPEND col TO coltab[].
*    CLEAR col. col-fname = 'STATUS32'.             col-color-col = '3'. APPEND col TO coltab[].
*
*     "--IR5047357 JKH 16.08.2016
*    CLEAR col. col-fname = 'WLK2_VALID_FROM'.      col-color-col = '5'. APPEND col TO coltab[].
*    CLEAR col. col-fname = 'SAP_WLK2_VALID_FROM'.  col-color-col = '4'. APPEND col TO coltab[].
*    CLEAR col. col-fname = 'STATUS33'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'WLK2_DISC_ALLWD'.      col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_WLK2_DISC_ALLWD'.  col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS34'.             col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'WLK2_SCALES_GRP'.      col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_WLK2_SCALES_GRP'.  col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS35'.             col-color-col = '3'. APPEND col TO coltab[].


    " ++CI17-343 JKH 09.11.2016
    CLEAR col. col-fname = 'ZZ_PBS'.               col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZ_PBS'.           col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS36'.             col-color-col = '3'. APPEND col TO coltab[].

    " ++ONLD-822 JKH 17.02.2017
    CLEAR col. col-fname = 'ZZONLINE_STATUS'.      col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZONLINE_STATUS'.  col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS37'.             col-color-col = '3'. APPEND col TO coltab[].


    <ls_reco_banner>-color = coltab[].

  ENDLOOP.

ENDFORM.                    " COLOR_GRID_9007
*&---------------------------------------------------------------------*
*&      Form  PREPARE_LAYOUT_9007
*&---------------------------------------------------------------------*
*       Field Catalog for STO Display
*----------------------------------------------------------------------*
FORM prepare_layout_9007 CHANGING fc_s_layout    TYPE lvc_s_layo.

  DATA: lv_count TYPE char15.

  fc_s_layout-ctab_fname = 'COLOR'.
  fc_s_layout-sel_mode   = 'A'.

  lv_count = gv_count_9007.
  CONDENSE lv_count NO-GAPS.

  CONCATENATE TEXT-002 lv_count
         INTO fc_s_layout-grid_title
 SEPARATED BY space.


ENDFORM.                    " PREPARE_LAYOUT_9007
*&---------------------------------------------------------------------*
*&      Form  DISPLAY_ALV_9008
*&---------------------------------------------------------------------*
* Display ALV 9008
*----------------------------------------------------------------------*
FORM display_alv_9008.


  CONSTANTS: lc_a        TYPE char01 VALUE 'A'.

* Declaration
  DATA: "lo_docking       TYPE REF TO cl_gui_docking_container,  "Custom container instance reference
    lo_container  TYPE REF TO cl_gui_custom_container,    "Custom container instance reference
    lt_fieldcat   TYPE        lvc_t_fcat,                 "Field catalog
    ls_layout     TYPE        lvc_s_layo ,                "Layout structure
*        lt_toolbar       TYPE        ui_functions,              "Toolbar Function
*        lo_event_handler TYPE REF TO lcl_event_handler,         "Event handling
    ls_disvariant TYPE        disvariant,                 "Variant
    lo_alvgrid    TYPE REF TO cl_gui_alv_grid.


  IF go_alvgrid_9008 IS INITIAL .


*   Create custom container
    CREATE OBJECT go_container_9008
      EXPORTING
        container_name              = 'CC_9008'
      EXCEPTIONS
        cntl_error                  = 1
        cntl_system_error           = 2
        create_error                = 3
        lifetime_error              = 4
        lifetime_dynpro_dynpro_link = 5
        OTHERS                      = 6.
    IF sy-subrc NE 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.


**   Create docking container
*    CREATE OBJECT lo_docking
*      EXPORTING
*        parent = cl_gui_container=>screen0
**       ratio  = 90  " 90% yet not full-screen size
*      EXCEPTIONS
*        others = 6.
*    IF sy-subrc NE 0.
*      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
*                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
*    ENDIF.

**   Full size screen for ALV
*    CALL METHOD lo_docking->set_extension
*      EXPORTING
*        extension  = 99999  " full-screen size !!!
*      EXCEPTIONS
*        cntl_error = 1
*        OTHERS     = 2.
*    IF sy-subrc NE 0.
*      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
*                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
*    ENDIF.

* Create ALV grid
    CREATE OBJECT go_alvgrid_9008
      EXPORTING
        i_parent = go_container_9008
      EXCEPTIONS
        OTHERS   = 5.
    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.


*   Preparing field catalog
    PERFORM prepare_fcat_9008 CHANGING lt_fieldcat[].

*   Color Grid
    PERFORM color_grid_9008.

*   Preparing layout structure
    PERFORM prepare_layout_9008 CHANGING ls_layout.

* Prepare Variant
    ls_disvariant-report    = sy-repid.
    ls_disvariant-username  = sy-uname.

*   ALV Display
    CALL METHOD go_alvgrid_9008->set_table_for_first_display
      EXPORTING
        i_bypassing_buffer            = abap_true
*       i_structure_name              = gc_alv_8000-struct
        is_variant                    = ls_disvariant
        i_save                        = lc_a
        is_layout                     = ls_layout
*       it_toolbar_excluding          = lt_toolbar
      CHANGING
        it_outtab                     = gt_reco_attr[]
        it_fieldcatalog               = lt_fieldcat[]
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.


  ELSE.

*   ALV Refresh Display
    CALL METHOD go_alvgrid_9008->refresh_table_display
      EXPORTING
*       is_stable      = ls_stable
        i_soft_refresh = abap_true
      EXCEPTIONS
        finished       = 1
        OTHERS         = 2.
  ENDIF.



ENDFORM.                    " DISPLAY_ALV_9008
*&---------------------------------------------------------------------*
*&      Form  PREPARE_FCAT_9008
*&---------------------------------------------------------------------*
*   Preparing field catalog
*----------------------------------------------------------------------*
FORM prepare_fcat_9008  CHANGING fc_t_fieldcat TYPE lvc_t_fcat.

* Declaration
  DATA: lt_fieldcat  TYPE lvc_t_fcat,
        lv_desc      TYPE char40,
        lv_desc_stat TYPE char40.


  FIELD-SYMBOLS : <ls_fieldcat>    TYPE lvc_s_fcat.

* Prepare field catalog for ALV display by merging structure
  REFRESH: lt_fieldcat[].
  CALL FUNCTION 'LVC_FIELDCATALOG_MERGE'
    EXPORTING
      i_structure_name       = 'ZSARN_DATA_RECO_ATTRIBUTES'
    CHANGING
      ct_fieldcat            = lt_fieldcat[]
    EXCEPTIONS
      inconsistent_interface = 1
      program_error          = 2
      OTHERS                 = 3.
  IF sy-subrc NE 0.
    REFRESH: fc_t_fieldcat.
  ENDIF.





* Looping to make the output field
  LOOP AT lt_fieldcat ASSIGNING <ls_fieldcat>.


    <ls_fieldcat>-tooltip = <ls_fieldcat>-scrtext_l.
    <ls_fieldcat>-col_opt = abap_true.


    lv_desc = <ls_fieldcat>-scrtext_s.


    IF <ls_fieldcat>-fieldname CS 'SAP_'.
      <ls_fieldcat>-coltext = 'E:' && lv_desc.
      lv_desc_stat  = <ls_fieldcat>-scrtext_s.
    ELSEIF <ls_fieldcat>-fieldname+0(6) EQ 'STATUS'.
      <ls_fieldcat>-coltext = 'S:' && lv_desc_stat.
    ELSE.
      <ls_fieldcat>-coltext = 'R:' && lv_desc.
    ENDIF.


    CASE <ls_fieldcat>-fieldname.
      WHEN 'FAN'.                  <ls_fieldcat>-coltext+0(1) = 'K:'.
      WHEN 'CURRENT_VER'.
        <ls_fieldcat>-coltext+0(1) = 'K:'.
        <ls_fieldcat>-no_out = abap_true.
      WHEN 'IDNO'.                 <ls_fieldcat>-coltext+0(1) = 'K:'.
      WHEN 'OVR_STATUS'.           <ls_fieldcat>-coltext+0(1) = 'K:'.
      WHEN 'MATERIAL'.
      WHEN 'SAP_MATERIAL'.
      WHEN 'STATUS1'.
      WHEN 'ZZATTR1'.
      WHEN 'SAP_ZZATTR1'.
      WHEN 'STATUS2'.
      WHEN 'ZZATTR2'.
      WHEN 'SAP_ZZATTR2'.
      WHEN 'STATUS3'.
      WHEN 'ZZATTR3'.
      WHEN 'SAP_ZZATTR3'.
      WHEN 'STATUS4'.
      WHEN 'ZZATTR4'.
      WHEN 'SAP_ZZATTR4'.
      WHEN 'STATUS5'.
      WHEN 'ZZATTR5'.
      WHEN 'SAP_ZZATTR5'.
      WHEN 'STATUS6'.
      WHEN 'ZZATTR6'.
      WHEN 'SAP_ZZATTR6'.
      WHEN 'STATUS7'.
      WHEN 'ZZATTR7'.
      WHEN 'SAP_ZZATTR7'.
      WHEN 'STATUS8'.
      WHEN 'ZZATTR8'.
      WHEN 'SAP_ZZATTR8'.
      WHEN 'STATUS9'.
      WHEN 'ZZATTR9'.
      WHEN 'SAP_ZZATTR9'.
      WHEN 'STATUS10'.
      WHEN 'ZZATTR10'.
      WHEN 'SAP_ZZATTR10'.
      WHEN 'STATUS11'.
      WHEN 'ZZATTR11'.
      WHEN 'SAP_ZZATTR11'.
      WHEN 'STATUS12'.
      WHEN 'ZZATTR12'.
      WHEN 'SAP_ZZATTR12'.
      WHEN 'STATUS13'.
      WHEN 'ZZATTR13'.
      WHEN 'SAP_ZZATTR13'.
      WHEN 'STATUS14'.
      WHEN 'ZZATTR14'.
      WHEN 'SAP_ZZATTR14'.
      WHEN 'STATUS15'.
      WHEN 'ZZATTR15'.
      WHEN 'SAP_ZZATTR15'.
      WHEN 'STATUS16'.
      WHEN 'ZZATTR16'.
      WHEN 'SAP_ZZATTR16'.
      WHEN 'STATUS17'.
      WHEN 'ZZATTR17'.
      WHEN 'SAP_ZZATTR17'.
      WHEN 'STATUS18'.
      WHEN 'ZZATTR18'.
      WHEN 'SAP_ZZATTR18'.
      WHEN 'STATUS19'.
      WHEN 'ZZATTR19'.
      WHEN 'SAP_ZZATTR19'.
      WHEN 'STATUS20'.
      WHEN 'ZZATTR20'.
      WHEN 'SAP_ZZATTR20'.
      WHEN 'STATUS21'.
      WHEN 'ZZATTR21'.
      WHEN 'SAP_ZZATTR21'.
      WHEN 'STATUS22'.
      WHEN 'ZZATTR22'.
      WHEN 'SAP_ZZATTR22'.
      WHEN 'STATUS23'.
      WHEN 'ZZATTR23'.
      WHEN 'SAP_ZZATTR23'.
      WHEN 'STATUS24'.





      WHEN OTHERS.
        DELETE lt_fieldcat WHERE fieldname = <ls_fieldcat>-fieldname.

    ENDCASE.

  ENDLOOP.

  fc_t_fieldcat[] = lt_fieldcat[].

ENDFORM.                    " PREPARE_FCAT_9008
*&---------------------------------------------------------------------*
*&      Form  COLOR_GRID_9008
*&---------------------------------------------------------------------*
*   Color Grid
*----------------------------------------------------------------------*
FORM color_grid_9008.

  DATA: col    TYPE lvc_s_scol,
        coltab TYPE lvc_t_scol,
        color  TYPE lvc_s_colo.


  FIELD-SYMBOLS: <ls_reco_attr>     TYPE zsarn_data_reco_attributes.

  LOOP AT gt_reco_attr[] ASSIGNING <ls_reco_attr>.

    CLEAR: coltab[].

    CLEAR col. col-fname = 'FAN'.                   col-color-col = '7'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'CURRENT_VER'.           col-color-col = '7'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'IDNO'.                  col-color-col = '7'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'OVR_STATUS'.            col-color-col = '7'. APPEND col TO coltab[].



    CLEAR col. col-fname = 'MATERIAL'.              col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_MATERIAL'.          col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS1'.               col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZATTR1'.               col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZATTR1'.           col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS2'.               col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZATTR2'.               col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZATTR2'.           col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS3'.               col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZATTR3'.               col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZATTR3'.           col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS4'.               col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZATTR4'.               col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZATTR4'.           col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS5'.               col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZATTR5'.               col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZATTR5'.           col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS6'.               col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZATTR6'.               col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZATTR6'.           col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS7'.               col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZATTR7'.               col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZATTR7'.           col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS8'.               col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZATTR8'.               col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZATTR8'.           col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS9'.               col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZATTR9'.               col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZATTR9'.           col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS10'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZATTR10'.              col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZATTR10'.          col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS11'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZATTR11'.              col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZATTR11'.          col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS12'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZATTR12'.              col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZATTR12'.          col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS13'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZATTR13'.              col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZATTR13'.          col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS14'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZATTR14'.              col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZATTR14'.          col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS15'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZATTR15'.              col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZATTR15'.          col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS16'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZATTR16'.              col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZATTR16'.          col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS17'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZATTR17'.              col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZATTR17'.          col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS18'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZATTR18'.              col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZATTR18'.          col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS19'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZATTR19'.              col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZATTR19'.          col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS20'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZATTR20'.              col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZATTR20'.          col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS21'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZATTR21'.              col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZATTR21'.          col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS22'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZATTR22'.              col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZATTR22'.          col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS23'.              col-color-col = '3'. APPEND col TO coltab[].

    CLEAR col. col-fname = 'ZZATTR23'.              col-color-col = '5'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'SAP_ZZATTR23'.          col-color-col = '4'. APPEND col TO coltab[].
    CLEAR col. col-fname = 'STATUS24'.              col-color-col = '3'. APPEND col TO coltab[].


    <ls_reco_attr>-color = coltab[].

  ENDLOOP.

ENDFORM.                    " COLOR_GRID_9008
*&---------------------------------------------------------------------*
*&      Form  PREPARE_LAYOUT_9008
*&---------------------------------------------------------------------*
*       Field Catalog for STO Display
*----------------------------------------------------------------------*
FORM prepare_layout_9008 CHANGING fc_s_layout    TYPE lvc_s_layo.

  DATA: lv_count TYPE char15.

  fc_s_layout-ctab_fname = 'COLOR'.
  fc_s_layout-sel_mode   = 'A'.

  lv_count = gv_count_9008.
  CONDENSE lv_count NO-GAPS.

  CONCATENATE TEXT-002 lv_count
         INTO fc_s_layout-grid_title
 SEPARATED BY space.


ENDFORM.                    " PREPARE_LAYOUT_9008
