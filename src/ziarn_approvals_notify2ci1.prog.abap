*&---------------------------------------------------------------------*
*&  Include           ZIARN_APPROVALS_NOTIFY2CI1
*&---------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*&       Class (Implementation)  lcl_approvals_notify
*&---------------------------------------------------------------------*
*        Text
*----------------------------------------------------------------------*
CLASS lcl_approvals_notify IMPLEMENTATION.

  METHOD constructor.

    me->ms_control = is_control.
    me->ms_selections = is_selections.

* Read/load customising
    me->read_cust( ).

  ENDMETHOD.

  METHOD run.

* Adjust selections
    me->adjust( ).

* Read pending approvals
    IF me->query( ).

* Package approvals
      me->package( IMPORTING et_recipient_approvals = DATA(lt_recipient_approvals) ).

* Process the packages
      me->process( lt_recipient_approvals ).

    ENDIF.

* Finalise actions
    me->finalise( ).

  ENDMETHOD.

  METHOD read_approvals.

* Read pending approvals. If IDNO is passed then we ignore if notification has already been sent
    me->mt_pending_approvals = zcl_arn_approval_backend=>get_pending_approvals( it_idno = me->ms_selections-idno
                                                                                it_appr_updated_on = me->ms_selections-updated_on ).
    IF me->mt_pending_approvals IS NOT  INITIAL.
      rv_loaded = abap_true.
    ELSE.
*   No pending approvals found
      MESSAGE i100(zarena_msg) INTO sy-msgli.
      me->write_log( iv_message = CONV #( sy-msgli ) ).
    ENDIF.

  ENDMETHOD.

  METHOD read_regional.
* Reads out regional data

    CHECK me->mt_pending_approvals IS NOT INITIAL.

    DATA(lt_idno) = me->mt_pending_approvals.
    DELETE ADJACENT DUPLICATES FROM lt_idno COMPARING idno.

* Regional header
    SELECT *
      FROM zarn_reg_hdr
      INTO TABLE me->mt_reg_hdr
      FOR ALL ENTRIES IN lt_idno
      WHERE idno = lt_idno-idno.

* Regional banner
    SELECT *
      FROM zarn_reg_banner
      INTO TABLE me->mt_reg_banner
      FOR ALL ENTRIES IN lt_idno
      WHERE idno = lt_idno-idno.

  ENDMETHOD.

  METHOD read_changes.

    DATA: lt_chg_hdr LIKE me->mt_chg_hdr.

    CHECK me->mt_pending_approvals IS NOT INITIAL.

* Change header...with guid
    SELECT a~chgid a~idno a~chg_category a~chg_area a~national_ver_id
           b~guid
      FROM zarn_cc_hdr AS a
      INNER JOIN zarn_approval_ch AS b ON b~chgid = a~chgid
      INTO CORRESPONDING FIELDS OF TABLE me->mt_chg_hdr
      FOR ALL ENTRIES IN me->mt_pending_approvals
      WHERE b~guid = me->mt_pending_approvals-guid AND
            a~idno = me->mt_pending_approvals-idno.
    IF sy-subrc = 0.

      lt_chg_hdr =  me->mt_chg_hdr.
      DELETE ADJACENT DUPLICATES FROM lt_chg_hdr COMPARING chgid idno.

* Change detail(items)
      SELECT * FROM zarn_cc_det
        INTO TABLE me->mt_chg_det
        FOR ALL ENTRIES IN lt_chg_hdr
        WHERE chgid = lt_chg_hdr-chgid AND
              idno  = lt_chg_hdr-idno.
    ENDIF.


  ENDMETHOD.

  METHOD get_idno_from_fanid.

    DATA: ls_idno TYPE ty_s_idno_range.

    CHECK me->ms_selections-fanid IS NOT INITIAL.
    SELECT DISTINCT idno
      FROM zarn_products
      INTO TABLE @DATA(lt_idno)
      WHERE fan_id IN @me->ms_selections-fanid.
    IF sy-subrc = 0.
* Adjust the selection ids
      ls_idno-sign = 'I'.
      ls_idno-option = 'EQ'.
      LOOP AT lt_idno ASSIGNING FIELD-SYMBOL(<ls_idno>).
        ls_idno-low = <ls_idno>-idno.
        APPEND ls_idno TO me->ms_selections-idno.
      ENDLOOP.
    ENDIF.

  ENDMETHOD.

  METHOD adjust.

* If we have a fan id selection then we get the corresponding arena ids first
    IF me->ms_selections-fanid IS NOT INITIAL.
      me->get_idno_from_fanid( ).
    ENDIF.

  ENDMETHOD.

  METHOD read_cust.

    SELECT *
      INTO TABLE me->mt_category
      FROM zmd_category.

    SELECT *
      INTO TABLE me->mt_dc_buyer
      FROM zmd_dc_buyer.

    SELECT *
      INTO TABLE me->mt_dc_by_cm
      FROM zmd_dc_by_cm_ass.

    SELECT *
      INTO TABLE me->mt_teams
      FROM zarn_teams
      WHERE display_order GE 1.

  ENDMETHOD.

  METHOD display_log.
* Display the log

* Setup the ALV for log display
    me->setup_alv( ).

* Display it
    me->mo_alv_table->display( ).

  ENDMETHOD.

  METHOD write_log.

    DATA: ls_log TYPE ty_log.


    ls_log-message = iv_message.
    IF iv_type IS NOT INITIAL.
      ls_log-type = iv_type.
    ELSE.
      ls_log-type = sy-msgty.
    ENDIF.
    IF iv_id IS NOT INITIAL.
      ls_log-id = iv_id.
    ELSE.
      ls_log-id = sy-msgid.
    ENDIF.
    IF iv_number IS NOT INITIAL.
      ls_log-number = iv_number.
    ELSE.
      ls_log-number = sy-msgno.
    ENDIF.

* Traffic light
    CASE ls_log-type.
      WHEN 'S' OR 'I'.
        ls_log-traffic_light = icon_green_light.
      WHEN 'W'.
        ls_log-traffic_light = icon_yellow_light.
      WHEN 'A' OR 'E' OR 'X'.
        ls_log-traffic_light = icon_red_light.
    ENDCASE.

    INSERT ls_log INTO TABLE me->mt_log.

  ENDMETHOD.

  METHOD validate.
* Validate approval


  ENDMETHOD.

  METHOD package.
* Package approvals according to recipient

    DATA: ls_recipient TYPE zsarn_user_approvals,
          ls_reg_hdr   TYPE zarn_reg_hdr.

    LOOP AT me->mt_pending_approvals ASSIGNING FIELD-SYMBOL(<ls_approval>).

* Get the regional header for each group of idno
      IF <ls_approval>-idno <> ls_reg_hdr-idno.
        TRY.
            ls_reg_hdr = me->mt_reg_hdr[ idno = <ls_approval>-idno ].
          CATCH cx_sy_itab_line_not_found.
*   No category manager determined for ID &1. Regional header missing.
            MESSAGE e111(zarena_msg) WITH <ls_approval>-idno INTO DATA(lv_msgli).
            write_log( iv_message = CONV #( lv_msgli ) ).
* Don't bother processing any further approvals for this idno
            DELETE me->mt_pending_approvals WHERE idno = <ls_approval>-idno.
            CONTINUE.
        ENDTRY.
      ENDIF.

* Determine the category manager
      DATA(lt_catman) = me->determine_catman( is_approval = <ls_approval> is_reg_hdr = ls_reg_hdr ).
      LOOP AT lt_catman ASSIGNING FIELD-SYMBOL(<lv_catman>).
* Get the team user for the category manager
        DATA(ls_team_user) = me->get_team_recipient( iv_team_code = <ls_approval>-team_code iv_catman = <lv_catman> ).
        IF ls_team_user IS NOT INITIAL.
* Add the approvals by team
          READ TABLE et_recipient_approvals ASSIGNING FIELD-SYMBOL(<ls_recipient>)
           WITH KEY team_code = ls_team_user-team_code
                    username  = ls_team_user-user.
          IF sy-subrc <> 0.
            CLEAR ls_recipient.
            ls_recipient-team_code = ls_team_user-team_code.
            ls_recipient-username = ls_team_user-user.
            INSERT: <ls_approval> INTO TABLE ls_recipient-approvals,
                    ls_recipient  INTO TABLE et_recipient_approvals.
          ELSE.
            INSERT <ls_approval> INTO TABLE <ls_recipient>-approvals.
          ENDIF.
        ENDIF.
      ENDLOOP.
* No category manager determined for this approval
      IF sy-subrc <> 0.
*   No category manager determined for ID &1 Change &2 Approval &3
        MESSAGE e110(zarena_msg) WITH <ls_approval>-idno <ls_approval>-chg_category <ls_approval>-guid INTO lv_msgli.
        me->write_log( iv_message = CONV #( lv_msgli ) ).
      ENDIF.

    ENDLOOP.

  ENDMETHOD.

  METHOD process.
* Process the recipient packages

    LOOP AT it_recipient_approvals ASSIGNING FIELD-SYMBOL(<ls_recipient_approval>).

* Send the notification
      IF me->send( <ls_recipient_approval> ).
* Process updates(Notification status, commit)
        me->update( <ls_recipient_approval>-approvals  ).
      ENDIF.

    ENDLOOP.

  ENDMETHOD.

  METHOD finalise.

    me->adjust_log( ).

  ENDMETHOD.

  METHOD send.

    DATA(ls_approval) = is_recipient_approval.

* Apply sort rules(We're missing change area)
    SORT ls_approval-approvals BY idno chg_category.

* Bang the email
    CALL FUNCTION 'Z_ARN_APPROVALS_EMAIL_TEAM2'
      EXPORTING
        iv_team_code              = ls_approval-team_code
        iv_user                   = ls_approval-username
        it_approvals              = ls_approval-approvals
      EXCEPTIONS
        email_problem             = 1
        no_users_in_team          = 2
        invalid_notification_type = 3
        team_not_found            = 4
        no_email_for_user         = 5
        no_user_for_team          = 6
        OTHERS                    = 7.
    IF sy-subrc = 0.
      ADD 1 TO me->mv_send_cnt.
      rv_sent = abap_true.
    ELSE.
      ADD 1 TO me->mv_send_err_cnt.
* Re-issue message for text
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
        WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4 INTO DATA(lv_message).
      me->write_log( iv_message = CONV #( lv_message ) ).
    ENDIF.

  ENDMETHOD.


  METHOD update.
* Update the approvals status and history


* Update the notifications status
    IF me->ms_control-test = abap_false."Not in test
      IF me->ms_control-consum = abap_true. "Update the notification status
        IF it_approvals IS NOT INITIAL.
* Update the notification status
          NEW zcl_arn_approval_backend( )->update_notification_status( it_approvals ).
        ENDIF.
      ENDIF.
* Commit the send request and optionally the approval notification status update
      COMMIT WORK.

* Rollback in test mode
    ELSE.
      ROLLBACK WORK.
    ENDIF.

  ENDMETHOD.

  METHOD setup_alv.

    TRY.
* Get the ALV
        cl_salv_table=>factory(
          IMPORTING
            r_salv_table   = me->mo_alv_table
          CHANGING
            t_table        = me->mt_log
        ).

* Get the ALV Function
        DATA(lo_functions) = me->mo_alv_table->get_functions( ).

        lo_functions->set_all( abap_true ).

        DATA(lo_display) = me->mo_alv_table->get_display_settings( ).

* Set the ALV Header (including some stats when testing)
        lo_display->set_list_header( 'Arena Approval Notification'(002) ).

* Build the header
        DATA(lo_header) = me->build_alv_header( ).

* Set top-of-list
        me->mo_alv_table->set_top_of_list( lo_header ).
        me->mo_alv_table->set_top_of_list_print( lo_header ).

* Columns stuff
        DATA(lo_columns) = me->mo_alv_table->get_columns( ).


* Optimise the columns
        lo_columns->set_optimize( cl_salv_display_settings=>true ).

      CATCH cx_salv_msg cx_salv_data_error cx_salv_not_found ##NO_HANDLER."
* Oops
    ENDTRY.

  ENDMETHOD.

  METHOD adjust_log.

* Add in a notification total message
*   &1 Notifcations were sent successfully
    IF me->mv_send_cnt > 0.
      MESSAGE s112(zarena_msg) WITH me->mv_send_cnt INTO DATA(lv_msg).
    ELSE.
      MESSAGE w112(zarena_msg) WITH me->mv_send_cnt INTO lv_msg.
    ENDIF.
    me->write_log( iv_message = CONV #( lv_msg ) ).

  ENDMETHOD.

  METHOD build_alv_header.
* Testing charm

    ro_header = NEW cl_salv_form_layout_grid( ).

*  Some info
    DATA(lo_flow) = ro_header->create_flow( row = 1  column = 1 ).
    lo_flow->create_text( text = 'Notifications Sent:'(003) ).
    lo_flow = ro_header->create_flow( row = 1  column = 2 ).
    lo_flow->create_text( text =  me->mv_send_cnt ).
    lo_flow = ro_header->create_flow( row = 2  column = 1 ).
    lo_flow->create_text( text = 'Notification Send Errors:'(004) ).
    lo_flow = ro_header->create_flow( row = 2  column = 2 ).
    lo_flow->create_text( text = me->mv_send_err_cnt ).
    lo_flow = ro_header->create_flow( row = 3  column = 1 ).
    lo_flow->create_text( text = 'Simluation Mode:'(006) ).
    lo_flow = ro_header->create_flow( row = 3  column = 2 ).
    lo_flow->create_text( text = me->ms_control-test ).

* Blank
    lo_flow = ro_header->create_flow( row = 4  column = 1 ).
    lo_flow->create_text( text = '' ).

  ENDMETHOD.

  METHOD determine_catman.
* Determines the category manager based on some rules


* 1: Catman by change
    rt_catman = me->catman_by_change(
                   is_approval = is_approval
                   is_reg_hdr  = is_reg_hdr
                ).
    CHECK rt_catman IS INITIAL.

* 2: Catman by host
    rt_catman = me->catman_by_host(
                    is_approval = is_approval
                    is_reg_hdr  = is_reg_hdr
                ).
    CHECK rt_catman IS INITIAL.

* 3: Default fallback for catman
    rt_catman = me->catman_by_default(
                    is_approval = is_approval
                    is_reg_hdr  = is_reg_hdr
                ).

  ENDMETHOD.

  METHOD catman_by_change.

    DATA: lv_wholesale_found TYPE abap_bool,
          lv_retail_found    TYPE abap_bool.

* 2: Banner relevant change should go to banner relevant manager
    LOOP AT me->mt_chg_hdr ASSIGNING FIELD-SYMBOL(<ls_chg_hdr>) USING KEY guid
      WHERE guid = is_approval-guid.
      TRY.
          DATA(ls_chg_det) =
            me->mt_chg_det[ chgid = <ls_chg_hdr>-chgid idno = <ls_chg_hdr>-idno chg_category = <ls_chg_hdr>-chg_category chg_table = 'ZARN_REG_BANNER' ].
* If it's wholesale/gilmours then gilmours catman
          IF ( lv_wholesale_found = abap_false ) AND
             ( zcl_sales_org_services=>is_wholesale_banner( CONV #( ls_chg_det-reference_data ) ) ).
            lv_wholesale_found = abap_true.
            IF is_reg_hdr-gil_zzcatman IS NOT INITIAL.
              INSERT is_reg_hdr-gil_zzcatman INTO TABLE rt_catman.
            ENDIF.
            CONTINUE."No need to check retail
          ENDIF.
* Check if it's retail catman
          IF ( lv_retail_found = abap_false ) AND
             ( zcl_sales_org_services=>is_retail_banner( CONV #( ls_chg_det-reference_data ) ) ).
            lv_retail_found = abap_true.
            IF is_reg_hdr-ret_zzcatman IS NOT INITIAL.
              INSERT is_reg_hdr-ret_zzcatman INTO TABLE rt_catman.
            ENDIF.
          ENDIF.
* If we've come up with both then stop looking
          IF lv_retail_found = abap_true AND lv_wholesale_found = abap_true.
            EXIT.
          ENDIF.
        CATCH cx_sy_itab_line_not_found.
      ENDTRY.
    ENDLOOP.

  ENDMETHOD.

  METHOD catman_by_host.

* Determine the category manager based on host product
    LOOP AT me->mt_reg_banner ASSIGNING FIELD-SYMBOL(<ls_banner>)
      WHERE idno = is_approval-idno .
* Is it wholesale?
      IF zcl_sales_org_services=>is_wholesale_banner( <ls_banner>-banner ).
        DATA(lv_wholesale) = abap_true.
      ELSE.
        DATA(lv_retail) = abap_true.
      ENDIF.
    ENDLOOP.
    CHECK sy-subrc = 0.
* Rules of retail and wholesale
    IF lv_retail = abap_true AND lv_wholesale = abap_true.
* It's a host product then we flick it to retail
      IF me->mt_reg_hdr[ idno = is_approval-idno ]-host_product = abap_true.
        IF is_reg_hdr-ret_zzcatman IS NOT INITIAL.
          INSERT is_reg_hdr-ret_zzcatman INTO TABLE rt_catman.
        ENDIF.
* Not a host product then we flick it to wholesale
      ELSE.
        IF is_reg_hdr-gil_zzcatman IS NOT INITIAL.
          INSERT is_reg_hdr-gil_zzcatman INTO TABLE rt_catman.
        ENDIF.
      ENDIF.
* To wholesale
    ELSEIF lv_wholesale = abap_true.
      IF is_reg_hdr-gil_zzcatman IS NOT INITIAL.
        INSERT is_reg_hdr-gil_zzcatman INTO TABLE rt_catman.
      ENDIF.
* To retail
    ELSE.
      IF is_reg_hdr-ret_zzcatman IS NOT INITIAL.
        INSERT is_reg_hdr-ret_zzcatman INTO TABLE rt_catman.
      ENDIF.
    ENDIF.

  ENDMETHOD.

  METHOD catman_by_default.


* Default to header catman
*    IF me->mt_reg_banner IS INITIAL.
    IF is_reg_hdr-gil_zzcatman IS NOT INITIAL.
      INSERT is_reg_hdr-gil_zzcatman INTO TABLE rt_catman.
    ENDIF.
    IF is_reg_hdr-ret_zzcatman IS NOT INITIAL.
      INSERT is_reg_hdr-ret_zzcatman INTO TABLE rt_catman.
    ENDIF.
*    ENDIF.

  ENDMETHOD.

  METHOD get_team_recipient.

    FIELD-SYMBOLS: <lv_field>       TYPE any.

    DATA: lv_table     TYPE char40,
          lv_field     TYPE char40,
          lv_fieldname TYPE char40,
          lv_username  TYPE xubname.


* Get the team detail
    TRY.
        DATA(ls_team) = VALUE #( me->mt_teams[ team_code = iv_team_code ] ).
      CATCH cx_sy_itab_line_not_found.
* New message....no team
    ENDTRY.

    SPLIT ls_team-fieldtoemail AT '-' INTO lv_table lv_field.
    CASE lv_table.
      WHEN 'ZMD_CATEGORY'.
        READ TABLE me->mt_category ASSIGNING FIELD-SYMBOL(<ls_md_category>) WITH TABLE KEY role_id = iv_catman.
        IF sy-subrc EQ 0.
          CONCATENATE '<ls_md_category>-' lv_field INTO lv_fieldname.
          UNASSIGN <lv_field>.
          ASSIGN (lv_fieldname) TO <lv_field>.
          IF <lv_field> IS ASSIGNED.
            lv_username = <lv_field>.
          ENDIF.
        ENDIF.
      WHEN 'ZMD_DC_BUYER'.
        LOOP AT me->mt_dc_by_cm ASSIGNING FIELD-SYMBOL(<ls_md_dc_by_cm>) WHERE cm_code EQ iv_catman.
          CHECK lv_username IS INITIAL.
          READ TABLE me->mt_dc_buyer ASSIGNING FIELD-SYMBOL(<ls_md_dc_buyer>) WITH TABLE KEY dc_buyer_code = <ls_md_dc_by_cm>-dc_buyer_code.
          IF sy-subrc EQ 0.
            lv_username = <ls_md_dc_buyer>-dc_buyer_user.
          ENDIF.
        ENDLOOP.
    ENDCASE.
    IF lv_username IS NOT INITIAL.
      rs_team_user-team_code = iv_team_code.
      rs_team_user-user      = lv_username.
    ENDIF.

  ENDMETHOD.

  METHOD query.

* Load the pending approvals
    rv_success = me->read_approvals( ).
    IF rv_success = abap_true.
* Read the changes
      me->read_changes( ).
* Read the regional data
      me->read_regional( ).
    ENDIF.


  ENDMETHOD.

ENDCLASS.               "lcl_approvals_notify
