* regenerated at 19.05.2020 10:08:56
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZVARN_REG_UOM_MCTOP.              " Global Declarations
  INCLUDE LZVARN_REG_UOM_MCUXX.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZVARN_REG_UOM_MCF...              " Subroutines
* INCLUDE LZVARN_REG_UOM_MCO...              " PBO-Modules
* INCLUDE LZVARN_REG_UOM_MCI...              " PAI-Modules
* INCLUDE LZVARN_REG_UOM_MCE...              " Events
* INCLUDE LZVARN_REG_UOM_MCP...              " Local class implement.
* INCLUDE LZVARN_REG_UOM_MCT99.              " ABAP Unit tests
  INCLUDE LZVARN_REG_UOM_MCF00                    . " subprograms
  INCLUDE LZVARN_REG_UOM_MCI00                    . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules

include LZVARN_REG_UOM_MCF01.
