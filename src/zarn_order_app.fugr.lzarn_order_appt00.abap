*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 11.12.2018 at 09:30:09
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_ORDER_APP_T................................*
DATA:  BEGIN OF STATUS_ZARN_ORDER_APP_T              .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_ORDER_APP_T              .
CONTROLS: TCTRL_ZARN_ORDER_APP_T
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_ORDER_APP_T              .
TABLES: ZARN_ORDER_APP_T               .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
