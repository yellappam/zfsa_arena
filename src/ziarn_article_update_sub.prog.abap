*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_ARTICLE_UPDATE_SUB
*&---------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*&      Form  BUILD_KEY
*&---------------------------------------------------------------------*
* Build Key table
*----------------------------------------------------------------------*
FORM build_key USING fu_v_idno TYPE zarn_idno
                     fu_v_ver  TYPE zarn_version
            CHANGING fc_t_key  TYPE ztarn_key.
  DATA: ls_key TYPE zsarn_key.

  CLEAR ls_key.
  ls_key-idno    = fu_v_idno.
  ls_key-version = fu_v_ver.
  INSERT ls_key INTO TABLE fc_t_key[].

ENDFORM.                    " BUILD_KEY
*&---------------------------------------------------------------------*
*&      Form  ARTICLE_POSTING
*&---------------------------------------------------------------------*
* Article Posting as Batch Program
*----------------------------------------------------------------------*
FORM article_posting  USING    fu_t_key     TYPE ztarn_key
                               fu_r_status  TYPE ztarn_status_range
                               fu_r_frmdat  TYPE ztarn_date_range
                               fu_v_batch   TYPE flag
                      CHANGING fc_t_message TYPE bal_t_msg.

  DATA: lv_post_dest  TYPE rfcdes-rfcdest,
        lv_own_logsys TYPE tbdls-logsys,
        lv_rfcchk     TYPE boolean,
        ls_tvarvc     TYPE tvarvc.

* Get RFC Connection from TVARVC
  CLEAR: lv_post_dest,
         ls_tvarvc.
  SELECT SINGLE *
    INTO ls_tvarvc
    FROM tvarvc
    WHERE name EQ gc_stvarv_post_dest
    AND   type EQ gc_type_p.
  IF sy-subrc EQ 0 AND ls_tvarvc-low IS NOT INITIAL.
* If RFC Dest if found from TVARVC
    lv_post_dest = ls_tvarvc-low.

    CALL FUNCTION 'CHECK_RFC_DESTINATION'
      EXPORTING
        i_destination              = lv_post_dest
      IMPORTING
        e_user_password_incomplete = lv_rfcchk.

  ENDIF.  " IF sy-subrc EQ 0 AND ls_tvarvc-low IS NOT INITIAL


  IF lv_post_dest IS INITIAL OR lv_rfcchk = abap_true.
    CALL FUNCTION 'OWN_LOGICAL_SYSTEM_GET'
      IMPORTING
        own_logical_system             = lv_own_logsys
      EXCEPTIONS
        own_logical_system_not_defined = 1
        OTHERS                         = 2.

    lv_post_dest = lv_own_logsys.
  ENDIF.  " IF lv_post_dest IS INITIAL


  IF lv_post_dest IS NOT INITIAL.
* Call Article Posting FM as Batch Program
    CALL FUNCTION 'ZARN_ARTICLE_POSTING' DESTINATION lv_post_dest
      EXPORTING
        it_key           = fu_t_key[]
        ir_status        = fu_r_status[]
        ir_frmdat        = fu_r_frmdat[]
        iv_batch         = fu_v_batch
        iv_display_log   = space
        iv_no_change_doc = space
      IMPORTING
        et_message       = fc_t_message[].
  ELSE.
    MESSAGE 'RFC Destination is not open' TYPE 'S' DISPLAY LIKE 'E'.
    EXIT.
  ENDIF.


ENDFORM.                    " ARTICLE_POSTING
*&---------------------------------------------------------------------*
*&      Form  MAINTAIN_SLG1_LOG
*&---------------------------------------------------------------------*
* Maintain SLG1 Log
*----------------------------------------------------------------------*
FORM maintain_slg1_log USING fu_t_message     TYPE bal_t_msg
                             fu_v_display_log TYPE boole_d
                    CHANGING fc_v_log_no      TYPE balognr.

  DATA:   ls_s_log      TYPE bal_s_log,
          ls_log_handle TYPE balloghndl,
          lt_message    TYPE bal_t_msg,
          ls_message    TYPE bal_s_msg.

  lt_message[] = fu_t_message[].

  ls_s_log-object    = 'ZARN'.
  ls_s_log-subobject = 'ZARN_ART_POST'.
  ls_s_log-aldate    = sy-datum.
  ls_s_log-altime    = sy-uzeit.
  ls_s_log-aluser    = sy-uname.
  ls_s_log-altcode   = sy-tcode.

* Create Log
  PERFORM create_log USING ls_s_log
                  CHANGING ls_log_handle.


  LOOP AT lt_message INTO ls_message.
* Write Message to Log
    PERFORM write_msg USING ls_log_handle
                   CHANGING ls_message.

  ENDLOOP.

* Save and Display the log
  PERFORM save_and_display_log USING ls_log_handle
                                     abap_true         " Save
                                     fu_v_display_log  " Display
                            CHANGING fc_v_log_no.


ENDFORM.                    " MAINTAIN_SLG1_LOG
*&---------------------------------------------------------------------*
*&      Form  CREATE_LOG
*&---------------------------------------------------------------------*
* Create Log
*----------------------------------------------------------------------*
FORM create_log USING fu_s_log        TYPE bal_s_log
             CHANGING fc_s_log_handle TYPE balloghndl.

  CLEAR fc_s_log_handle.
  CALL FUNCTION 'BAL_LOG_CREATE'
    EXPORTING
      i_s_log                 = fu_s_log
    IMPORTING
      e_log_handle            = fc_s_log_handle
    EXCEPTIONS
      log_header_inconsistent = 1
      OTHERS                  = 2.
  IF sy-subrc <> 0.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
            WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.


ENDFORM.                    " CREATE_LOG
*&---------------------------------------------------------------------*
*&      Form  WRITE_MSG
*&---------------------------------------------------------------------*
* Write Message to Log
*----------------------------------------------------------------------*
FORM write_msg  USING fu_s_log_handle TYPE balloghndl
             CHANGING fc_s_message    TYPE bal_s_msg.


  CALL FUNCTION 'BAL_LOG_MSG_ADD'
    EXPORTING
      i_log_handle     = fu_s_log_handle
      i_s_msg          = fc_s_message
    EXCEPTIONS
      log_not_found    = 1
      msg_inconsistent = 2
      log_is_full      = 3
      OTHERS           = 4.

  IF sy-subrc <> 0.
    WRITE : 'Write to Log Failed'(001).
  ENDIF.

ENDFORM.                    " WRITE_MSG
*&---------------------------------------------------------------------*
*&      Form  SAVE_AND_DISPLAY_LOG
*&---------------------------------------------------------------------*
* Save and Display the log
*----------------------------------------------------------------------*
FORM save_and_display_log  USING fu_s_log_handle TYPE balloghndl
                                 fi_v_save       TYPE boole_d
                                 fi_v_display    TYPE boole_d
                        CHANGING fc_v_log_no     TYPE balognr.

  DATA: lt_log_handle TYPE bal_t_logh,
        lt_lognumbers TYPE bal_t_lgnm,
        ls_lognumbers TYPE bal_s_lgnm.

  DATA: ls_display_profile TYPE bal_s_prof,
        ls_fcat            TYPE bal_s_fcat,
        lv_msg             TYPE string.


  APPEND fu_s_log_handle TO lt_log_handle.

  IF fi_v_save = abap_true.

    REFRESH: lt_lognumbers[].
    CALL FUNCTION 'BAL_DB_SAVE'
      EXPORTING
        i_t_log_handle   = lt_log_handle
      IMPORTING
        e_new_lognumbers = lt_lognumbers[]
      EXCEPTIONS
        log_not_found    = 1
        save_not_allowed = 2
        numbering_error  = 3
        OTHERS           = 4.
    IF sy-subrc <> 0.

      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
              WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ELSE.

      CLEAR ls_lognumbers.
      READ TABLE lt_lognumbers[] INTO ls_lognumbers
      WITH KEY log_handle = fu_s_log_handle.
      IF sy-subrc = 0.
        fc_v_log_no = ls_lognumbers-lognumber.
      ENDIF.

    ENDIF.

  ENDIF.

  IF fi_v_display = abap_true.

    " Get the display profile because want to adjust the
    " field catalog so that site, article can be output as
    " columns of the log
    CALL FUNCTION 'BAL_DSP_PROFILE_SINGLE_LOG_GET'
      IMPORTING
        e_s_display_profile = ls_display_profile.

    " Add to field catalog - will display as additional
    " columns in the log
    ls_fcat-ref_table = 'ZSARN_SLG1_LOG_PARAMS_ART_POST'.
    ls_fcat-ref_field = 'IDNO'.
    APPEND ls_fcat TO ls_display_profile-mess_fcat.

    ls_fcat-ref_table = 'ZSARN_SLG1_LOG_PARAMS_ART_POST'.
    ls_fcat-ref_field = 'VERSION'.
    APPEND ls_fcat TO ls_display_profile-mess_fcat.

    ls_fcat-ref_table = 'ZSARN_SLG1_LOG_PARAMS_ART_POST'.
    ls_fcat-ref_field = 'FANID'.
    APPEND ls_fcat TO ls_display_profile-mess_fcat.

    ls_fcat-ref_table = 'ZSARN_SLG1_LOG_PARAMS_ART_POST'.
    ls_fcat-ref_field = 'NAT_STATUS'.
    APPEND ls_fcat TO ls_display_profile-mess_fcat.

    ls_fcat-ref_table = 'ZSARN_SLG1_LOG_PARAMS_ART_POST'.
    ls_fcat-ref_field = 'REG_STATUS'.
    APPEND ls_fcat TO ls_display_profile-mess_fcat.

    ls_fcat-ref_table = 'ZSARN_SLG1_LOG_PARAMS_ART_POST'.
    ls_fcat-ref_field = 'SAPARTICLE'.
    APPEND ls_fcat TO ls_display_profile-mess_fcat.

    ls_fcat-ref_table = 'ZSARN_SLG1_LOG_PARAMS_ART_POST'.
    ls_fcat-ref_field = 'BATCH_MODE'.
    APPEND ls_fcat TO ls_display_profile-mess_fcat.





    ls_display_profile-disvariant-report = sy-repid.
    ls_display_profile-disvariant-handle = 'LOG' .
    ls_display_profile-cwidth_opt        = abap_true.


    CALL FUNCTION 'BAL_DSP_LOG_DISPLAY'
      EXPORTING
        i_s_display_profile  = ls_display_profile
        i_t_log_handle       = lt_log_handle
      EXCEPTIONS
        profile_inconsistent = 1
        internal_error       = 2
        no_data_available    = 3
        no_authority         = 4
        OTHERS               = 5.
    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
              WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

  ENDIF.

ENDFORM.                    " SAVE_AND_DISPLAY_LOG
