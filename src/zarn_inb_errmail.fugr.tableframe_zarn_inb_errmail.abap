*---------------------------------------------------------------------*
*    program for:   TABLEFRAME_ZARN_INB_ERRMAIL
*   generation date: 16.05.2016 at 09:39:13 by user C90001363
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
FUNCTION TABLEFRAME_ZARN_INB_ERRMAIL   .

  PERFORM TABLEFRAME TABLES X_HEADER X_NAMTAB DBA_SELLIST DPL_SELLIST
                            EXCL_CUA_FUNCT
                     USING  CORR_NUMBER VIEW_ACTION VIEW_NAME.

ENDFUNCTION.
