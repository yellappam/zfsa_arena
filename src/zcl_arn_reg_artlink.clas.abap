CLASS zcl_arn_reg_artlink DEFINITION
  PUBLIC
  CREATE PUBLIC .

  PUBLIC SECTION.

    TYPES:
      BEGIN OF ty_s_mara,
        matnr TYPE matnr,
        matkl TYPE matkl,
        attyp TYPE attyp,
        mtart TYPE mtart,
        maktx TYPE maktx,
      END OF ty_s_mara .
    TYPES:
      ty_t_mara TYPE SORTED TABLE OF ty_s_mara WITH UNIQUE KEY matnr .

    CLASS-DATA st_hdr_scn_t TYPE ztarn_hdr_scn_t .
    CLASS-DATA st_link_scn_t TYPE ztarn_link_scn_t .
    CLASS-DATA st_art_link TYPE ztarn_art_link .
    DATA mt_reg_artlink TYPE ztarn_reg_artlink .
    DATA mt_reg_artlink_ui TYPE ztarn_reg_artlink_ui .
    DATA mt_mara TYPE ty_t_mara .

    CLASS-METHODS get_hdr_scn_t
      RETURNING
        VALUE(rt_hdr_scn_t) TYPE ztarn_hdr_scn_t .
    CLASS-METHODS get_link_scn_t
      RETURNING
        VALUE(rt_link_scn_t) TYPE ztarn_link_scn_t .
    CLASS-METHODS get_article_det
      IMPORTING
        !iv_matnr             TYPE matnr
      RETURNING
        VALUE(rs_article_det) TYPE ty_s_mara .
    CLASS-METHODS get_art_link
      RETURNING
        VALUE(rt_art_link) TYPE ztarn_art_link .
    CLASS-METHODS get_article_linking_data
      IMPORTING
        !iv_hdr_scn        TYPE zarn_hdr_scn
      RETURNING
        VALUE(rs_art_link) TYPE zarn_art_link .
    METHODS get_reg_artlink
      IMPORTING
        !iv_matnr       TYPE matnr
      EXPORTING
        !et_reg_artlink TYPE ztarn_reg_artlink .
    METHODS get_reg_artlink_idno
      IMPORTING
        !iv_idno        TYPE zarn_idno OPTIONAL
        !it_idno        TYPE ztarn_reg_key OPTIONAL
      EXPORTING
        !et_reg_artlink TYPE ztarn_reg_artlink .
    METHODS build_reg_artlink_ui
      IMPORTING
        !it_reg_artlink    TYPE ztarn_reg_artlink
      EXPORTING
        !et_reg_artlink_ui TYPE ztarn_reg_artlink_ui .
protected section.
private section.
ENDCLASS.



CLASS ZCL_ARN_REG_ARTLINK IMPLEMENTATION.


  METHOD build_reg_artlink_ui.

    DATA: lt_reg_artlink TYPE ztarn_reg_artlink,
          ls_hdr_mara    TYPE ty_s_mara,
          ls_link_mara   TYPE ty_s_mara,
          ls_hdr_scn_t   TYPE zarn_hdr_scn_t,
          ls_link_scn_t  TYPE zarn_link_scn_t.

    FIELD-SYMBOLS: <ls_reg_artlink_ui> TYPE zsarn_reg_artlink_ui.


    CLEAR: me->mt_mara, me->mt_reg_artlink_ui, et_reg_artlink_ui.


    lt_reg_artlink = it_reg_artlink.

    CHECK lt_reg_artlink IS NOT INITIAL.

    SELECT a~matnr a~matkl a~attyp a~mtart b~maktx
      INTO CORRESPONDING FIELDS OF TABLE me->mt_mara
      FROM mara AS a
      JOIN makt AS b
      ON a~matnr = b~matnr
      FOR ALL ENTRIES IN lt_reg_artlink
      WHERE ( a~matnr = lt_reg_artlink-hdr_matnr OR a~matnr = lt_reg_artlink-link_matnr )
        AND b~spras = sy-langu.



    MOVE-CORRESPONDING lt_reg_artlink TO me->mt_reg_artlink_ui.

    LOOP AT me->mt_reg_artlink_ui ASSIGNING <ls_reg_artlink_ui>.
      CLEAR: ls_hdr_mara, ls_link_mara.

      TRY.
          ls_hdr_mara  = me->mt_mara[ matnr = <ls_reg_artlink_ui>-hdr_matnr ].
        CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
      ENDTRY.

      TRY.
          ls_link_mara = me->mt_mara[ matnr = <ls_reg_artlink_ui>-link_matnr ].
        CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
      ENDTRY.

      <ls_reg_artlink_ui>-hdr_maktx  = ls_hdr_mara-maktx.
      <ls_reg_artlink_ui>-hdr_attyp  = ls_hdr_mara-attyp.
      <ls_reg_artlink_ui>-hdr_mtart  = ls_hdr_mara-mtart.
      <ls_reg_artlink_ui>-hdr_matkl  = ls_hdr_mara-matkl.
      <ls_reg_artlink_ui>-link_maktx = ls_link_mara-maktx.
      <ls_reg_artlink_ui>-link_attyp = ls_link_mara-attyp.
      <ls_reg_artlink_ui>-link_mtart = ls_link_mara-mtart.
      <ls_reg_artlink_ui>-link_matkl = ls_link_mara-matkl.


      CLEAR ls_hdr_scn_t.
      TRY.
          ls_hdr_scn_t = st_hdr_scn_t[ hdr_scn = <ls_reg_artlink_ui>-hdr_scn spras = sy-langu ].
          <ls_reg_artlink_ui>-hdr_scn_drdn+0(2)  = <ls_reg_artlink_ui>-hdr_scn.
          <ls_reg_artlink_ui>-hdr_scn_drdn+2(1)  = space.
          <ls_reg_artlink_ui>-hdr_scn_drdn+3(20) = ls_hdr_scn_t-hdr_scn_text.
        CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
          <ls_reg_artlink_ui>-hdr_scn_drdn = <ls_reg_artlink_ui>-hdr_scn.
      ENDTRY.

      IF <ls_reg_artlink_ui>-hdr_scn IS INITIAL.
        CLEAR: <ls_reg_artlink_ui>-hdr_scn, <ls_reg_artlink_ui>-hdr_scn_drdn.
      ENDIF.



      CLEAR ls_link_scn_t.
      TRY.
          ls_link_scn_t = st_link_scn_t[ link_scn = <ls_reg_artlink_ui>-link_scn spras = sy-langu ].
          <ls_reg_artlink_ui>-link_scn_drdn+0(2)  = <ls_reg_artlink_ui>-link_scn.
          <ls_reg_artlink_ui>-link_scn_drdn+2(1)  = space.
          <ls_reg_artlink_ui>-link_scn_drdn+3(20) = ls_link_scn_t-link_scn_text.
        CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
          <ls_reg_artlink_ui>-link_scn_drdn = <ls_reg_artlink_ui>-link_scn.
      ENDTRY.

      IF <ls_reg_artlink_ui>-link_scn IS INITIAL.
        CLEAR: <ls_reg_artlink_ui>-link_scn, <ls_reg_artlink_ui>-link_scn_drdn.
      ENDIF.


    ENDLOOP.


    et_reg_artlink_ui = me->mt_reg_artlink_ui.


  ENDMETHOD.


  METHOD get_article_det.

    SELECT SINGLE  a~matnr a~matkl a~attyp a~mtart b~maktx
      INTO CORRESPONDING FIELDS OF rs_article_det
      FROM mara AS a
      JOIN makt AS b
      ON a~matnr = b~matnr
      WHERE a~matnr = iv_matnr
        AND b~spras = sy-langu.

  ENDMETHOD.


  METHOD get_article_linking_data.

    get_art_link( ).


    TRY.
        rs_art_link = st_art_link[ hdr_scn = iv_hdr_scn ].
      CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
    ENDTRY.


  ENDMETHOD.


  METHOD get_art_link..

    IF st_art_link IS INITIAL.
      SELECT * FROM zarn_art_link
        INTO TABLE st_art_link.
    ENDIF.
    IF rt_art_link IS REQUESTED.
      rt_art_link = st_art_link.
    ENDIF.



  ENDMETHOD.


  METHOD get_hdr_scn_t.

    IF st_hdr_scn_t[] IS INITIAL.
      SELECT * FROM zarn_hdr_scn_t
        INTO TABLE st_hdr_scn_t[]
        WHERE spras = sy-langu.
    ENDIF.

    IF rt_hdr_scn_t IS REQUESTED.
      rt_hdr_scn_t = st_hdr_scn_t[].
    ENDIF.

  ENDMETHOD.


  METHOD get_link_scn_t.

    IF st_link_scn_t[] IS INITIAL.
      SELECT * FROM zarn_link_scn_t
        INTO TABLE st_link_scn_t[]
        WHERE spras = sy-langu.
    ENDIF.

    IF rt_link_scn_t IS REQUESTED.
      rt_link_scn_t = st_link_scn_t[].
    ENDIF.

  ENDMETHOD.


  METHOD get_reg_artlink.

    DATA: ls_hdr_mara   TYPE ty_s_mara,
          ls_link_mara  TYPE ty_s_mara,
          ls_hdr_scn_t  TYPE zarn_hdr_scn_t,
          ls_link_scn_t TYPE zarn_link_scn_t.

    FIELD-SYMBOLS: <ls_reg_artlink_ui> TYPE zsarn_reg_artlink_ui.


    CLEAR: me->mt_reg_artlink, et_reg_artlink.

    CHECK iv_matnr IS NOT INITIAL.

      SELECT * FROM zarn_reg_artlink
        INTO TABLE me->mt_reg_artlink
        WHERE hdr_matnr  = iv_matnr
           OR link_matnr = iv_matnr
        ORDER BY PRIMARY KEY.    "S/4 HANA Upgrade


    et_reg_artlink = me->mt_reg_artlink.



  ENDMETHOD.


  METHOD get_reg_artlink_idno.

    DATA: ls_hdr_mara   TYPE ty_s_mara,
          ls_link_mara  TYPE ty_s_mara,
          ls_hdr_scn_t  TYPE zarn_hdr_scn_t,
          ls_link_scn_t TYPE zarn_link_scn_t,
          lv_matnr      TYPE matnr,
          lt_reg_hdr    TYPE ztarn_reg_hdr.

    FIELD-SYMBOLS: <ls_reg_artlink_ui> TYPE zsarn_reg_artlink_ui.


    CLEAR: me->mt_reg_artlink, et_reg_artlink.

    IF iv_idno IS INITIAL AND it_idno IS INITIAL.
      RETURN.
    ENDIF.


    IF it_idno IS NOT INITIAL.
      SELECT idno matnr
        INTO CORRESPONDING FIELDS OF TABLE lt_reg_hdr
        FROM zarn_reg_hdr
        FOR ALL ENTRIES IN it_idno
        WHERE idno = it_idno-idno.
      IF sy-subrc NE 0.
        RETURN.
      ELSE.
        DELETE lt_reg_hdr WHERE matnr IS INITIAL.
      ENDIF.

      IF lt_reg_hdr IS INITIAL.
        RETURN.
      ELSE.
        SELECT * FROM zarn_reg_artlink
        INTO TABLE me->mt_reg_artlink
        FOR ALL ENTRIES IN lt_reg_hdr
        WHERE hdr_matnr  = lt_reg_hdr-matnr
           OR link_matnr = lt_reg_hdr-matnr.
      ENDIF.

      et_reg_artlink = me->mt_reg_artlink.
      RETURN.
    ENDIF.





    IF iv_idno IS NOT INITIAL.
      CLEAR lv_matnr.
      SELECT SINGLE matnr INTO lv_matnr
        FROM zarn_reg_hdr
        WHERE idno = iv_idno.
      IF sy-subrc = 0.
        SELECT * FROM zarn_reg_artlink
          INTO TABLE me->mt_reg_artlink
          WHERE hdr_matnr  = lv_matnr
             OR link_matnr = lv_matnr.
      ENDIF.

      et_reg_artlink = me->mt_reg_artlink.
      RETURN.
    ENDIF.


  ENDMETHOD.
ENDCLASS.
