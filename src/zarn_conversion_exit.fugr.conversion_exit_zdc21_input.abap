*-----------------------------------------------------------------------
* PROJECT          # AReNa
* SPECIFICATION    #
* DATE WRITTEN     # 26.07.2016
* SYSTEM           # DE0
* SAP VERSION      # ECC6
* TYPE             # Conversion Exit
* AUTHOR           # C90003202, Howard Chow
*-----------------------------------------------------------------------
* PURPOSE          # Conversion exit input for domain ZARN_DEC21_14_D
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------
FUNCTION CONVERSION_EXIT_ZDC21_INPUT.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(INPUT)
*"  EXPORTING
*"     REFERENCE(OUTPUT)
*"----------------------------------------------------------------------

  DATA:
    lv_output   TYPE zarn_retail_price.


* copy input to variable with domain ZARN_DEC21_14_D to get full decimal
  lv_output = input.
  output    = lv_output.


ENDFUNCTION.
