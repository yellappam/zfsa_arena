*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 21.11.2016 at 14:10:29
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_ART_LINK...................................*
DATA:  BEGIN OF STATUS_ZARN_ART_LINK                 .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_ART_LINK                 .
CONTROLS: TCTRL_ZARN_ART_LINK
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_ART_LINK                 .
TABLES: ZARN_ART_LINK                  .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
