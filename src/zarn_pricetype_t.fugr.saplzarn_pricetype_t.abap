* regenerated at 25.05.2016 12:13:11 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_PRICETYPE_TTOP.              " Global Data
  INCLUDE LZARN_PRICETYPE_TUXX.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_PRICETYPE_TF...              " Subroutines
* INCLUDE LZARN_PRICETYPE_TO...              " PBO-Modules
* INCLUDE LZARN_PRICETYPE_TI...              " PAI-Modules
* INCLUDE LZARN_PRICETYPE_TE...              " Events
* INCLUDE LZARN_PRICETYPE_TP...              " Local class implement.
* INCLUDE LZARN_PRICETYPE_TT99.              " ABAP Unit tests
  INCLUDE LZARN_PRICETYPE_TF00                    . " subprograms
  INCLUDE LZARN_PRICETYPE_TI00                    . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
