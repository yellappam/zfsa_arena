class ZCL_MASTER_PRODUCT_WITH_PRICE definition
  public
  create public .

public section.

  interfaces ZII_MASTER_PRODUCT_WITH_PRICE .
protected section.
private section.
ENDCLASS.



CLASS ZCL_MASTER_PRODUCT_WITH_PRICE IMPLEMENTATION.


METHOD zii_master_product_with_price~master_product_with_price_crea.
*** **** INSERT IMPLEMENTATION HERE **** ***

  DATA: lt_return          TYPE bal_t_msg,
        ls_return          TYPE bal_s_msg,
        lt_product         TYPE zmaster_products_product_tab,
        ls_admin           TYPE zmaster_products_admin,
        lt_product_error   TYPE zmaster_products_product_tab,
        lv_slg1_log_no     TYPE balognr.

** For debugging to check data from PI - uncomment the following loop
** and debug through SM50
*  WHILE 1 = 1.
*  ENDWHILE.


  lt_product[] = input-master_product_with_price_crea-product[].
  ls_admin     = input-master_product_with_price_crea-admin.


* AReNa: Inbound Data Processing from PI
  REFRESH: lt_return[].
  CALL FUNCTION 'ZARN_INBOUND_DATA_PROCESSING'
    EXPORTING
      it_product       = lt_product[]
      is_admin         = ls_admin
      iv_id_type       = 'H'
    IMPORTING
      et_return        = lt_return[]
      et_product_error = lt_product_error[]
      ev_slg1_log_no   = lv_slg1_log_no.

* PC 7/12/16 - Do not raise an exception here. It is very possible that multiple
* products were sent and some updates were successful and some errored. Errors are
* already handled internally within the application. The report RSXMB_RESTART_MESSAGES
* should therefore not try and reprocess, otherwise we end up with products updated
* multiple times and the version of the product increasing by the amount of retry
* attempts of the report RSXMB_RESTART_MESSAGES variant
* Raise Exception to PI in case of any error
*  IF lt_return[] IS NOT INITIAL.
*    CLEAR ls_return.
*    READ TABLE lt_return[] INTO ls_return
*    WITH KEY msgty = 'E'.
*    IF sy-subrc = 0.
*      RAISE EXCEPTION TYPE zcx_master_product_with_price.
*    ENDIF.
*  ENDIF.

ENDMETHOD.
ENDCLASS.
