*----------------------------------------------------------------------*
***INCLUDE LZARN_ARTICLE_POSTF01 .
*----------------------------------------------------------------------*
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 13.04.2018
* CHANGE No.       # Charm 9000003482; Jira CI18-554
* DESCRIPTION      # CI18-554 Recipe Management - Ingredient Changes
*                  # New regional table for Allergen Type overrides and
*                  # fields for Ingredients statement overrides in Arena.
*                  # New Ingredients Type field in the Scales block in
*                  # Arena and NONSAP tab in material master.
* WHO              # C90005557, Tessa Newman
*-----------------------------------------------------------------------
* DATE             # 24.04.2018
* CHANGE No.       # ChaRM DE0K917906; JIRA CI18-51 Pay By Scan Changes
* DESCRIPTION      # Pay by Scan should be set not only at the banner
*                  # level but also at the individual store level. The
*                  # previous implementation was to allow it to be set
*                  # only at the banner level on MVKE-MVGR3. The new
*                  # implementation is to add custom field ZZ_PBS to MARC.
*                  # 1. Removed all the logic that is related to matl_grp_3.
*                  # 2. Added ZZ_PBS to plant data enhancement structure
*                  #    ZMD_S_BAPI_PLANTEXT & ZMD_S_BAPI_PLANTEXTX.
* WHO              # I00600343, Fengyu Li
*-----------------------------------------------------------------------
* DATE             # 06.06.2018
* CHANGE No.       # ChaRM 9000003675; JIRA CI18-506 NEW Range Status flag
* DESCRIPTION      # New Range Flag, Range Availability and Range Detail
*                  # fields added in Arena and on material master in Listing
*                  # tab and Basic Data Text tab
* WHO              # C90005557, Tessa Newman
*-----------------------------------------------------------------------
* DATE             # 26.06.2020
* CHANGE No.       # 9000007255: SSM-1 NW Range Policy ZARN_GUI
* DESCRIPTION      # 1. Post Layout Module assignment for article
*                  # 2. Code clean-up
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* DATE             # 26.06.2020
* CHANGE No.       # SSM-1: NW Range Policy ZARN_GUI change
* DESCRIPTION      # Post layout module assignment for change scenario
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
*&---------------------------------------------------------------------*
*&      Form  VALIDATE_KEY
*&---------------------------------------------------------------------*
* Validate Provided IDNO and Version
*----------------------------------------------------------------------*
FORM validate_key USING fu_t_key        TYPE ty_t_key
                        fu_v_batch      TYPE flag
               CHANGING fc_t_ver_status TYPE ty_t_ver_status
                        fc_t_key_valid  TYPE ty_t_key
                        fc_t_fan_id     TYPE ty_t_fan_id
                        fc_t_message    TYPE bal_t_msg
                        fc_v_error      TYPE flag.

  DATA: lt_key        TYPE ty_t_key,
        ls_key        TYPE zsarn_key,
        lt_key_valid  TYPE ty_t_key,
        ls_key_valid  TYPE zsarn_key,
        ls_ver_status TYPE ty_s_ver_status,
        ls_fan_id     TYPE ty_s_fan_id,

        ls_log_params TYPE zsarn_slg1_log_params_art_post,

        lv_tabix      TYPE sy-tabix.

  CLEAR : fc_t_ver_status[], fc_t_key_valid[].


  lt_key[] = fu_t_key[].
  DELETE ADJACENT DUPLICATES FROM lt_key[] COMPARING idno.
  DELETE lt_key[] WHERE idno IS INITIAL.

* Get FAN ID
  IF lt_key[] IS NOT INITIAL.
    CLEAR fc_t_fan_id[].
    SELECT idno fan_id FROM zarn_products
    INTO CORRESPONDING FIELDS OF TABLE fc_t_fan_id[]
    FOR ALL ENTRIES IN lt_key[]
      WHERE idno    = lt_key-idno
        AND version = lt_key-version.
  ENDIF.


* Check for Missing IDNOs and Versions
  LOOP AT fu_t_key[] INTO ls_key.
    lv_tabix = sy-tabix.

    IF ls_key-idno IS INITIAL OR ls_key-version IS INITIAL.

* Get FAN ID
      IF ls_key-idno IS NOT INITIAL.
        CLEAR ls_fan_id.
        READ TABLE fc_t_fan_id[] INTO ls_fan_id
        WITH TABLE KEY idno = ls_key-idno.
      ENDIF.

* Build Return Message

      CLEAR: ls_log_params.

      ls_log_params-idno    = ls_key-idno.
      ls_log_params-fanid   = ls_fan_id-fan_id.
      ls_log_params-version = ls_key-version.
      ls_log_params-batch_mode = fu_v_batch.

* Key Line '&' : IDNO and Current Version are required
      PERFORM build_message USING gc_message-error
                                  gc_message-id
                                  '021'
                                  lv_tabix
                                  space
                                  space
                                  space
                                  ls_log_params
                         CHANGING fc_t_message[].

      IF 1 = 2. MESSAGE e021(zarena_msg). ENDIF.
    ELSE.
      INSERT ls_key INTO TABLE lt_key_valid[].
    ENDIF.
  ENDLOOP.  " LOOP AT fu_t_key[] INTO ls_key

  IF lt_key_valid[] IS INITIAL.
    fc_v_error = abap_true.
    EXIT.
  ENDIF.

* Get Version status Details
  CLEAR: fc_t_ver_status[].
  SELECT * FROM zarn_ver_status
    INTO CORRESPONDING FIELDS OF TABLE fc_t_ver_status[]
    FOR ALL ENTRIES IN lt_key_valid[]
    WHERE idno        = lt_key_valid-idno
      AND current_ver = lt_key_valid-version.


* Check for Missing IDNOs and Versions
  LOOP AT lt_key_valid[] INTO ls_key_valid.
* Check if IDNO Version is valid
    CLEAR ls_ver_status.
    READ TABLE fc_t_ver_status[] INTO ls_ver_status
    WITH TABLE KEY idno = ls_key_valid-idno.
    IF sy-subrc NE 0.

* Get FAN ID
      IF ls_key_valid-idno IS NOT INITIAL.
        CLEAR ls_fan_id.
        READ TABLE fc_t_fan_id[] INTO ls_fan_id
        WITH TABLE KEY idno = ls_key_valid-idno.
      ENDIF.

* Build Return Message

      CLEAR: ls_log_params.

      ls_log_params-idno    = ls_key_valid-idno.
      ls_log_params-fanid   = ls_fan_id-fan_id.
      ls_log_params-version = ls_key_valid-version.
      ls_log_params-batch_mode = fu_v_batch.

* IDNO with Current Version doesn't exist
      PERFORM build_message USING gc_message-error
                                  gc_message-id
                                  '022'
                                  space
                                  space
                                  space
                                  space
                                  ls_log_params
                         CHANGING fc_t_message[].

      IF 1 = 2. MESSAGE e022(zarena_msg). ENDIF.
      fc_v_error = abap_true.
    ELSE.
      INSERT ls_key_valid INTO TABLE fc_t_key_valid[].
    ENDIF.
  ENDLOOP.  " LOOP AT fu_t_key[] INTO ls_key

ENDFORM.                    " VALIDATE_KEY
*&---------------------------------------------------------------------*
*&      Form  BUILD_MESSAGE
*&---------------------------------------------------------------------*
* Build Return Message
*----------------------------------------------------------------------*
FORM build_message USING fu_v_type       TYPE sy-msgty
                         fu_v_id         TYPE sy-msgid
                         fu_v_number     "TYPE sy-msgno
                         fu_v_message_v1
                         fu_v_message_v2
                         fu_v_message_v3
                         fu_v_message_v4
                         fu_s_log_params TYPE zsarn_slg1_log_params_art_post
                CHANGING fc_t_message    TYPE bal_t_msg.

  DATA: ls_message       TYPE bal_s_msg,
        lv_context_value TYPE string.

  CLEAR ls_message.
  ls_message-msgty  = fu_v_type.
  ls_message-msgid  = fu_v_id.
  ls_message-msgno  = fu_v_number.
  ls_message-msgv1  = fu_v_message_v1.
  ls_message-msgv2  = fu_v_message_v2.
  ls_message-msgv3  = fu_v_message_v3.
  ls_message-msgv4  = fu_v_message_v4.

  lv_context_value           = fu_s_log_params.
  ls_message-context-tabname = 'ZSARN_SLG1_LOG_PARAMS_ART_POST'.
  ls_message-context-value   = lv_context_value.

  APPEND ls_message TO fc_t_message[].

ENDFORM.                    " BUILD_MESSAGE
*&---------------------------------------------------------------------*
*&      Form  CHECK_APPROVAL_STATUS
*&---------------------------------------------------------------------*
* Check Approval Status - if status is 'APR' in National /Regional Tables
*----------------------------------------------------------------------*
FORM check_approval_status USING fu_t_key         TYPE ty_t_key
                                 fu_r_status      TYPE ztarn_status_range
                                 fu_t_fan_id      TYPE ty_t_fan_id
                                 fu_v_batch       TYPE flag
                        CHANGING fc_t_prd_version TYPE ty_t_prd_version
                                 fc_t_reg_hdr     TYPE ty_t_reg_hdr
                                 fc_t_fan_id      TYPE ty_t_fan_id
                                 fc_t_key_valid   TYPE ty_t_key
                                 fc_t_message     TYPE bal_t_msg
                                 fc_v_error       TYPE flag.

  DATA: ls_prd_version TYPE ty_s_prd_version,
        ls_reg_hdr     TYPE ty_s_reg_hdr,
        ls_fan_id      TYPE ty_s_fan_id,

        ls_key         TYPE zsarn_key,
        "ls_key_valid      TYPE zsarn_key,

        ls_log_params  TYPE zsarn_slg1_log_params_art_post,

        lv_tabix       TYPE sy-tabix.

  IF fu_t_key[] IS INITIAL.
    EXIT.
  ENDIF.

  CLEAR : fc_t_prd_version[], fc_t_reg_hdr[], fc_t_key_valid[].

* Get Product Version details to validate approval status
  SELECT * FROM zarn_prd_version
    INTO CORRESPONDING FIELDS OF TABLE fc_t_prd_version[]
    FOR ALL ENTRIES IN fu_t_key[]
    WHERE idno           EQ fu_t_key-idno
      AND version        EQ fu_t_key-version.
*      AND version_status IN fu_r_status[].


* Get Regional Header details to validate approval status
  SELECT * FROM zarn_reg_hdr
    INTO CORRESPONDING FIELDS OF TABLE fc_t_reg_hdr[]
    FOR ALL ENTRIES IN fu_t_key[]
    WHERE idno   EQ fu_t_key-idno.
*      AND status IN fu_r_status[].

* Check for Missing IDNOs and Versions
  LOOP AT fu_t_key[] INTO ls_key.
    lv_tabix = sy-tabix.

* Get FAN ID
    IF ls_key-idno IS NOT INITIAL.
      CLEAR ls_fan_id.
      READ TABLE fu_t_fan_id[] INTO ls_fan_id
      WITH TABLE KEY idno = ls_key-idno.
    ENDIF.

* Check 'APR' status in Product Version - National data status
    CLEAR ls_prd_version.
    READ TABLE fc_t_prd_version[] INTO ls_prd_version
    WITH TABLE KEY idno    = ls_key-idno
                   version = ls_key-version.


* Check 'APR' status in Regional Header - Regional data status
    CLEAR ls_reg_hdr.
    READ TABLE fc_t_reg_hdr[] INTO ls_reg_hdr
    WITH TABLE KEY idno    = ls_key-idno.


* Either National or Regional status must be 'APR' to create article
    IF ls_prd_version-version_status IN fu_r_status[] OR
       ls_reg_hdr-status             IN fu_r_status[].
      INSERT ls_key INTO TABLE fc_t_key_valid[].
    ELSE.

* Build Return Message

      CLEAR: ls_log_params.

      ls_log_params-idno        = ls_key-idno.
      ls_log_params-fanid       = ls_fan_id-fan_id.
      ls_log_params-version     = ls_key-version.
      ls_log_params-nat_status  = ls_prd_version-version_status.
      ls_log_params-reg_status  = ls_reg_hdr-status.
      ls_log_params-saparticle  = ls_reg_hdr-matnr.
      ls_log_params-batch_mode  = fu_v_batch.

* Article can't be created/updated for this version
      PERFORM build_message USING gc_message-error
                                  gc_message-id
                                  '023'
                                  space
                                  space
                                  space
                                  space
                                  ls_log_params
                         CHANGING fc_t_message[].

      IF 1 = 2. MESSAGE e023(zarena_msg). ENDIF.
      fc_v_error = abap_true.

    ENDIF.

  ENDLOOP.  " LOOP AT fu_t_key[] INTO ls_key

ENDFORM.                    " CHECK_APPROVAL_STATUS
*&---------------------------------------------------------------------*
*&      Form  GET_APPROVED_KEYS
*&---------------------------------------------------------------------*
* Get Approved Keys - if status is 'APR' in National /Regional Tables
*----------------------------------------------------------------------*
FORM get_approved_keys USING fu_r_status      TYPE ztarn_status_range
                             fu_r_frmdat      TYPE ztarn_date_range
                             fu_v_batch       TYPE flag
                    CHANGING fc_t_ver_status  TYPE ty_t_ver_status
                             fc_t_prd_version TYPE ty_t_prd_version
                             fc_t_reg_hdr     TYPE ty_t_reg_hdr
                             fc_t_fan_id      TYPE ty_t_fan_id
                             fc_t_key_valid   TYPE ty_t_key
                             fc_t_message     TYPE bal_t_msg
                             fc_v_error       TYPE flag.

  DATA: lt_key         TYPE ty_t_key,
        ls_key         TYPE zsarn_key,
        ls_key_valid   TYPE zsarn_key,
        ls_ver_status  TYPE ty_s_ver_status,
        ls_prd_version TYPE ty_s_prd_version,
        ls_reg_hdr     TYPE ty_s_reg_hdr,
        ls_fan_id      TYPE ty_s_fan_id,

        ls_log_params  TYPE zsarn_slg1_log_params_art_post.


  CLEAR : fc_t_ver_status[], fc_t_prd_version[], fc_t_reg_hdr[], fc_t_key_valid[].

  CLEAR lt_key[].

  SELECT a~*
    FROM zarn_prd_version AS a
    JOIN zarn_ver_status AS b
    ON   a~idno    = b~idno
    AND  a~version = b~current_ver
    INTO CORRESPONDING FIELDS OF TABLE @fc_t_prd_version[]
    WHERE a~version_status IN @fu_r_status[]
      AND a~changed_on     IN @fu_r_frmdat[].
  IF sy-subrc = 0.
* Get Approved IDNO for National
    LOOP AT fc_t_prd_version[] INTO ls_prd_version.
      CLEAR ls_key.
      ls_key-idno = ls_prd_version-idno.
      INSERT ls_key INTO TABLE lt_key[].
    ENDLOOP.
  ENDIF.

* Get Regional Header details to validate approval status
  SELECT * FROM zarn_reg_hdr                           "#EC CI_NOFIELD.
    INTO CORRESPONDING FIELDS OF TABLE fc_t_reg_hdr[]
    WHERE status IN fu_r_status[]
      AND laeda  IN fu_r_frmdat[].
  IF sy-subrc = 0.
* Get Approved IDNO for Regional
    LOOP AT fc_t_reg_hdr[] INTO ls_reg_hdr.
      CLEAR ls_key.
      ls_key-idno = ls_reg_hdr-idno.
      INSERT ls_key INTO TABLE lt_key[].
    ENDLOOP.
  ENDIF.

  IF lt_key[] IS NOT INITIAL.
    DELETE ADJACENT DUPLICATES FROM lt_key[] COMPARING ALL FIELDS.
  ENDIF.

  IF lt_key[] IS NOT INITIAL.

* Get Version status Details
    SELECT * FROM zarn_ver_status
      INTO CORRESPONDING FIELDS OF TABLE fc_t_ver_status[]
      FOR ALL ENTRIES IN lt_key[]
      WHERE idno        = lt_key-idno.
    IF sy-subrc = 0.

* Get FAN ID
      CLEAR fc_t_fan_id[].
      SELECT idno fan_id FROM zarn_products
      INTO CORRESPONDING FIELDS OF TABLE fc_t_fan_id[]
      FOR ALL ENTRIES IN fc_t_ver_status[]
        WHERE idno    = fc_t_ver_status-idno
          AND version = fc_t_ver_status-current_ver.

* Validate Product Version Data
      LOOP AT fc_t_prd_version[] INTO ls_prd_version.

* Get FAN ID
        IF ls_prd_version-idno IS NOT INITIAL.
          CLEAR ls_fan_id.
          READ TABLE fc_t_fan_id[] INTO ls_fan_id
          WITH TABLE KEY idno = ls_prd_version-idno.
        ENDIF.

* Get Regional Header
        CLEAR ls_reg_hdr.
        READ TABLE fc_t_reg_hdr[] INTO ls_reg_hdr
        WITH TABLE KEY idno = ls_prd_version-idno.


* Check if Retreived Approved Version from PRD_VERSION is current Version or not
        CLEAR ls_ver_status.
        READ TABLE fc_t_ver_status[] INTO ls_ver_status
        WITH TABLE KEY idno = ls_prd_version-idno.
        IF sy-subrc = 0.
          IF ls_prd_version-version EQ ls_ver_status-current_ver.
* If Retreived Approved Version is current Version then add to Valid Key
            CLEAR ls_key_valid.
            ls_key_valid-idno    = ls_ver_status-idno.
            ls_key_valid-version = ls_ver_status-current_ver.
            INSERT ls_key_valid INTO TABLE fc_t_key_valid[].

          ELSE.

* If Retreived Approved Version is NOT current Version then ERROR

            CLEAR ls_key_valid.
            READ TABLE fc_t_key_valid[] INTO ls_key_valid
            WITH KEY idno = ls_prd_version-idno.
            IF sy-subrc = 0.
              DELETE fc_t_key_valid[] INDEX sy-tabix.


* Build Return Message

              CLEAR: ls_log_params.

              ls_log_params-idno        = ls_prd_version-idno.
              ls_log_params-fanid       = ls_fan_id-fan_id.
              ls_log_params-version     = ls_prd_version-version.
              ls_log_params-nat_status  = ls_prd_version-version_status.
              ls_log_params-reg_status  = ls_reg_hdr-status.
              ls_log_params-saparticle  = ls_reg_hdr-matnr.
              ls_log_params-batch_mode  = fu_v_batch.

* Multiple Approved Version Exist for FAN ID, Article can't be created
              PERFORM build_message USING gc_message-error
                                          gc_message-id
                                          '026'
                                          space
                                          space
                                          space
                                          space
                                          ls_log_params
                                 CHANGING fc_t_message[].

              IF 1 = 2. MESSAGE e026(zarena_msg). ENDIF.
              fc_v_error = abap_true.
            ELSE.

* Build Return Message

              CLEAR: ls_log_params.

              ls_log_params-idno        = ls_prd_version-idno.
              ls_log_params-fanid       = ls_fan_id-fan_id.
              ls_log_params-version     = ls_prd_version-version.
              ls_log_params-nat_status  = ls_prd_version-version_status.
              ls_log_params-reg_status  = ls_reg_hdr-status.
              ls_log_params-saparticle  = ls_reg_hdr-matnr.
              ls_log_params-batch_mode  = fu_v_batch.

* Approved Version is not the Current Version '&'
              PERFORM build_message USING gc_message-error
                                          gc_message-id
                                          '025'
                                          ls_ver_status-current_ver
                                          space
                                          space
                                          space
                                          ls_log_params
                                 CHANGING fc_t_message[].

              IF 1 = 2. MESSAGE e025(zarena_msg). ENDIF.
              fc_v_error = abap_true.
            ENDIF.

          ENDIF.  " IF ls_prd_version-version EQ ls_ver_status-current_ver
        ELSE.
* Build Return Message

          CLEAR: ls_log_params.

          ls_log_params-idno        = ls_prd_version-idno.
          ls_log_params-fanid       = ls_fan_id-fan_id.
*    ls_log_params-version     =
*    ls_log_params-nat_status  =
          ls_log_params-reg_status  = ls_reg_hdr-status.
          ls_log_params-saparticle  = ls_reg_hdr-matnr.
          ls_log_params-batch_mode  = fu_v_batch.

* No Current Version Exist
          PERFORM build_message USING gc_message-error
                                      gc_message-id
                                      '027'
                                      space
                                      space
                                      space
                                      space
                                      ls_log_params
                             CHANGING fc_t_message[].

          IF 1 = 2. MESSAGE e027(zarena_msg). ENDIF.
          fc_v_error = abap_true.
        ENDIF.
      ENDLOOP.  " LOOP AT fc_t_prd_version[] INTO ls_prd_version

* Get Current Version for Regional Header Data
      LOOP AT fc_t_reg_hdr[] INTO ls_reg_hdr.

* Get FAN ID
        IF ls_reg_hdr-idno IS NOT INITIAL.
          CLEAR ls_fan_id.
          READ TABLE fc_t_fan_id[] INTO ls_fan_id
          WITH TABLE KEY idno = ls_reg_hdr-idno.
        ENDIF.

* Check if Retreived Approved Version from PRD_VERSION is current Version or not
        CLEAR ls_ver_status.
        READ TABLE fc_t_ver_status[] INTO ls_ver_status
        WITH TABLE KEY idno = ls_reg_hdr-idno.
        IF sy-subrc = 0.
* If Retreived Approved Version is current Version then add to Valid Key
          CLEAR ls_key_valid.
          READ TABLE fc_t_key_valid[] INTO ls_key_valid
          WITH KEY idno = ls_reg_hdr-idno.
          IF sy-subrc NE 0.
            ls_key_valid-idno    = ls_ver_status-idno.
            ls_key_valid-version = ls_ver_status-current_ver.
            INSERT ls_key_valid INTO TABLE fc_t_key_valid[].
          ENDIF.
        ELSE.
* Build Return Message

          CLEAR: ls_log_params.

          ls_log_params-idno        = ls_reg_hdr-idno.
          ls_log_params-fanid       = ls_fan_id-fan_id.
          ls_log_params-reg_status  = ls_reg_hdr-status.
          ls_log_params-saparticle  = ls_reg_hdr-matnr.
          ls_log_params-batch_mode  = fu_v_batch.

* No Current Version Exist
          PERFORM build_message USING gc_message-error
                                      gc_message-id
                                      '027'
                                      space
                                      space
                                      space
                                      space
                                      ls_log_params
                             CHANGING fc_t_message[].

          IF 1 = 2. MESSAGE e027(zarena_msg). ENDIF.
          fc_v_error = abap_true.
        ENDIF.

      ENDLOOP.  " LOOP AT fc_t_reg_hdr[] INTO ls_reg_hdr


    ELSE.

* Build Return Message

      CLEAR: ls_log_params.
      ls_log_params-batch_mode  = fu_v_batch.

* No Current Version Exist
      PERFORM build_message USING gc_message-error
                                  gc_message-id
                                  '027'
                                  space
                                  space
                                  space
                                  space
                                  ls_log_params
                         CHANGING fc_t_message[].

      IF 1 = 2. MESSAGE e027(zarena_msg). ENDIF.
      fc_v_error = abap_true.
    ENDIF.  " Get Version status Details

  ELSE.

* Build Return Message

    CLEAR: ls_log_params.
    ls_log_params-batch_mode  = fu_v_batch.

* No Article to Create
    PERFORM build_message USING gc_message-warning
                                gc_message-id
                                '024'
                                space
                                space
                                space
                                space
                                ls_log_params
                       CHANGING fc_t_message[].

    IF 1 = 2. MESSAGE e024(zarena_msg). ENDIF.
  ENDIF.  " IF lt_key[] IS NOT INITIAL

ENDFORM.                    " GET_APPROVED_KEYS
*&---------------------------------------------------------------------*
*&      Form  BUILD_IDNO_DATA
*&---------------------------------------------------------------------*
* Build IDNO Data
*----------------------------------------------------------------------*
FORM build_idno_data USING fu_t_key         TYPE ty_t_key
                           fu_t_ver_status  TYPE ty_t_ver_status
                           fu_t_prd_version TYPE ty_t_prd_version
                           fu_t_fan_id      TYPE ty_t_fan_id
                           fu_t_reg_hdr     TYPE ty_t_reg_hdr
                  CHANGING fc_t_idno_data   TYPE ty_t_idno_data
                           fc_t_products    TYPE ty_t_products
                           fc_t_mara        TYPE ty_t_mara.

  DATA: ls_key         TYPE zsarn_key,
        ls_ver_status  TYPE ty_s_ver_status,
        ls_prd_version TYPE ty_s_prd_version,
        ls_fan_id      TYPE ty_s_fan_id,
        ls_reg_hdr     TYPE ty_s_reg_hdr,
        ls_idno_data   TYPE ty_s_idno_data,
        ls_mara        TYPE ty_s_mara.


  IF fu_t_key[] IS INITIAL.
    EXIT.
  ENDIF.

  CLEAR: fc_t_idno_data[], fc_t_products[], fc_t_mara[].


* Get Product Details to get MATNR for entered IDNOs
  SELECT idno fan_id
      INTO CORRESPONDING FIELDS OF TABLE fc_t_products[]
    FROM zarn_products
      FOR ALL ENTRIES IN fu_t_key[]
      WHERE idno    EQ fu_t_key-idno
        AND version EQ fu_t_key-version.

  IF fc_t_products[] IS NOT INITIAL.

    DELETE fc_t_products[] WHERE fan_id IS INITIAL.

    IF fc_t_products[] IS NOT INITIAL.
      SELECT matnr meins zzfan zzlni_repack zzlni_bulk zzlni_prboly
        FROM mara
        INTO CORRESPONDING FIELDS OF TABLE fc_t_mara[]
        FOR ALL ENTRIES IN fc_t_products[]
        WHERE zzfan = fc_t_products-fan_id.
    ENDIF.
  ENDIF.



  LOOP AT fu_t_key[] INTO ls_key.

* Get FAN ID
    CLEAR ls_fan_id.
    READ TABLE fu_t_fan_id[] INTO ls_fan_id
    WITH TABLE KEY idno = ls_key-idno.

* Get Regional Header
    CLEAR ls_reg_hdr.
    READ TABLE fu_t_reg_hdr[] INTO ls_reg_hdr
    WITH TABLE KEY idno = ls_key-idno.

* Get Version status
    CLEAR ls_ver_status.
    READ TABLE fu_t_ver_status[] INTO ls_ver_status
    WITH TABLE KEY idno = ls_key-idno.

* Get Product Version
    CLEAR ls_prd_version.
    READ TABLE fu_t_prd_version[] INTO ls_prd_version
    WITH TABLE KEY idno    = ls_key-idno
                   version = ls_key-version.


    CLEAR ls_idno_data.
    READ TABLE fc_t_idno_data[] INTO ls_idno_data
    WITH TABLE KEY idno = ls_key-idno.
    IF sy-subrc NE 0.
      ls_idno_data-idno         = ls_key-idno.
      ls_idno_data-current_ver  = ls_ver_status-current_ver.
      ls_idno_data-previous_ver = ls_ver_status-previous_ver.
      ls_idno_data-latest_ver   = ls_ver_status-latest_ver.
      ls_idno_data-article_ver  = ls_ver_status-article_ver.
      ls_idno_data-fan_id       = ls_fan_id-fan_id.
      ls_idno_data-nat_status   = ls_prd_version-version_status.
      ls_idno_data-reg_status   = ls_reg_hdr-status.


      CLEAR ls_mara.
      READ TABLE fc_t_mara[] INTO ls_mara
      WITH KEY zzfan = ls_fan_id-fan_id.
      IF sy-subrc NE 0.
        ls_idno_data-sap_article  = ls_fan_id-fan_id.
      ELSE.
        ls_idno_data-sap_article  = ls_mara-matnr.
      ENDIF.


      CALL FUNCTION 'CONVERSION_EXIT_MATN1_INPUT'
        EXPORTING
          input        = ls_idno_data-sap_article
        IMPORTING
          output       = ls_idno_data-sap_article
        EXCEPTIONS
          length_error = 1
          OTHERS       = 2.

      INSERT ls_idno_data INTO TABLE fc_t_idno_data[].
    ENDIF.


  ENDLOOP.


ENDFORM.                    " BUILD_IDNO_DATA
*&---------------------------------------------------------------------*
*&      Form  GET_NAT_REG_DATA
*&---------------------------------------------------------------------*
* Get National and Regional Data
*----------------------------------------------------------------------*
FORM get_nat_reg_data USING fu_t_idno_data     TYPE ty_t_idno_data
                   CHANGING fc_t_prod_data     TYPE ztarn_prod_data
                            fc_t_reg_data      TYPE ztarn_reg_data
                            fc_s_prod_data_all TYPE zsarn_prod_data
                            fc_s_reg_data_all  TYPE zsarn_reg_data.


  DATA: ls_idno_data     TYPE ty_s_idno_data,
        lt_key           TYPE ztarn_key,
        ls_key           TYPE zsarn_key,
        lt_reg_key       TYPE ztarn_reg_key,
        ls_reg_key       TYPE zsarn_reg_key,
        ls_prod_data_all TYPE zsarn_prod_data,
        ls_reg_data_all  TYPE zsarn_reg_data,
        lt_prod_data     TYPE ztarn_prod_data,
        lt_reg_data      TYPE ztarn_reg_data.



  LOOP AT fu_t_idno_data[] INTO ls_idno_data.

    CLEAR ls_key.
    ls_key-idno    = ls_idno_data-idno.
    ls_key-version = ls_idno_data-current_ver.
    APPEND ls_key TO lt_key[].

    CLEAR ls_reg_key.
    ls_reg_key-idno = ls_idno_data-idno.
    APPEND ls_reg_key TO lt_reg_key[].

  ENDLOOP.


* Get National Data
  CLEAR lt_prod_data[].
  CALL FUNCTION 'ZARN_READ_NATIONAL_DATA'
    EXPORTING
      it_key      = lt_key[]
    IMPORTING
      et_data     = lt_prod_data[]
      es_data_all = ls_prod_data_all.


* Get Regional Data
  CLEAR lt_reg_data[].
  CALL FUNCTION 'ZARN_READ_REGIONAL_DATA'
    EXPORTING
      it_key      = lt_reg_key[]
    IMPORTING
      et_data     = lt_reg_data[]
      es_data_all = ls_reg_data_all.


  fc_t_prod_data[]     = lt_prod_data[].
  fc_t_reg_data[]      = lt_reg_data[].

  fc_s_prod_data_all   = ls_prod_data_all.
  fc_s_reg_data_all    = ls_reg_data_all.


ENDFORM.                    " GET_NAT_REG_DATA
*&---------------------------------------------------------------------*
*&      Form  GET_SAP_DATA
*&---------------------------------------------------------------------*
* Get SAP data from DB
*----------------------------------------------------------------------*
FORM get_sap_data USING fu_t_idno_data       TYPE ty_t_idno_data
                        fu_s_prod_data_all   TYPE zsarn_prod_data
                        fu_s_reg_data_all    TYPE zsarn_reg_data
                        fu_t_mara            TYPE ty_t_mara
               CHANGING fc_t_marm            TYPE ty_t_marm
                        fc_t_mean            TYPE ty_t_mean
                        fc_t_eina            TYPE ty_t_eina
                        fc_t_eine            TYPE ty_t_eine
                        fc_t_eine_site       TYPE ty_t_eine       "++3181 JKH 19.05.2017
                        fc_t_maw1            TYPE ty_t_maw1
                        fc_t_host_sap        TYPE ty_t_host_sap
                        fc_t_t006            TYPE ty_t_t006
                        fc_t_t005            TYPE ty_t_t005
                        fc_t_tcurc           TYPE ty_t_tcurc
                        fc_t_coo_t           TYPE ty_t_coo_t
                        fc_t_gen_uom_t       TYPE ty_t_gen_uom_t
                        fc_t_t134            TYPE ty_t_t134

*>>>IS 2019/06 usage of TVARVC decommissioned - instead use new config table
                        "fc_t_tvarvc_ekorg    TYPE tvarvc_t
                        fc_t_pir_ekorg       TYPE zarn_t_pir_pur_cnf
*<<<IS 2019/06 usage of TVARVC decommissioned - instead use new config table
                        fc_v_ekorg_main      TYPE ekorg
                        fc_t_tvarvc          TYPE tvarvc_t
                        fc_t_tvarvc_hyb_code TYPE tvarvc_t
                        fc_t_tvta            TYPE tvta_tt
                        fc_t_uom_cat         TYPE ty_t_uom_cat
                        fc_s_matgrp_hier     TYPE ty_s_matgrp_hier
                        fc_t_matgrp_prod     TYPE ty_t_matgrp_prod
                        fc_t_art_hier        TYPE ty_t_art_hier.

  DATA: ls_prod_data_all TYPE zsarn_prod_data,
        ls_reg_data_all  TYPE zsarn_reg_data,
        lt_rng_ekorg     TYPE RANGE OF ekorg.

  ls_prod_data_all = fu_s_prod_data_all.
  ls_reg_data_all  = fu_s_reg_data_all.

  IF ls_prod_data_all-zarn_products[] IS NOT INITIAL.
    DELETE ls_prod_data_all-zarn_products[] WHERE fan_id IS INITIAL.
  ENDIF.

*>>>IS 2019/06 usage of PIR TVARVC decommissioned - instead use new config table

  SELECT * FROM zarn_pir_pur_cnf INTO TABLE fc_t_pir_ekorg.
  IF sy-subrc NE 0.
    "tbd error
  ENDIF.

  TRY.
      fc_v_ekorg_main = fc_t_pir_ekorg[ main_ekorg = abap_true ]-ekorg.

    CATCH cx_sy_itab_line_not_found.
      "tbd error
  ENDTRY.

  lt_rng_ekorg = VALUE #( FOR ls_ekorg IN fc_t_pir_ekorg ( low = ls_ekorg-ekorg sign = 'I' option = 'EQ' ) ).

* Sales Org For UNI/LNI from TVARVC
  SELECT name type numb sign opti low high FROM tvarvc
    INTO CORRESPONDING FIELDS OF TABLE fc_t_tvarvc[]
    WHERE name = gc_tvarvc-vkorg_uni
       OR name = gc_tvarvc-vkorg_lni
      AND type = gc_tvarvc-type_s.

* Hybris Code for REG_UOM additional Scenario
  SELECT name type numb sign opti low high FROM tvarvc
    INTO CORRESPONDING FIELDS OF TABLE fc_t_tvarvc_hyb_code[]
    WHERE name = gc_tvarvc-uom_hyb_code
      AND type = gc_tvarvc-type_s.

  IF fu_t_idno_data[] IS NOT INITIAL.

    SELECT matnr meinh umrez umren eannr ean11 numtp laeng
           breit hoehe meabm volum voleh brgew gewei mesub
    FROM marm
    INTO CORRESPONDING FIELDS OF TABLE fc_t_marm[]
    FOR ALL ENTRIES IN fu_t_idno_data[]
    WHERE matnr = fu_t_idno_data-sap_article.


    SELECT matnr meinh lfnum ean11 eantp hpean
    FROM mean
    INTO CORRESPONDING FIELDS OF TABLE fc_t_mean[]
    FOR ALL ENTRIES IN fu_t_idno_data[]
    WHERE matnr = fu_t_idno_data-sap_article.


    SELECT matnr wausm
    FROM maw1
    INTO CORRESPONDING FIELDS OF TABLE fc_t_maw1[]
    FOR ALL ENTRIES IN fu_t_idno_data[]
    WHERE matnr = fu_t_idno_data-sap_article.


    SELECT * FROM zmd_host_sap
    INTO CORRESPONDING FIELDS OF TABLE fc_t_host_sap[]
    FOR ALL ENTRIES IN fu_t_idno_data[]
    WHERE matnr = fu_t_idno_data-sap_article
      AND deleted EQ space.

    SELECT * FROM eina
    INTO CORRESPONDING FIELDS OF TABLE fc_t_eina[]
    FOR ALL ENTRIES IN fu_t_idno_data[]
    WHERE matnr = fu_t_idno_data-sap_article.
    IF sy-subrc = 0.

      SELECT * FROM eine
      INTO CORRESPONDING FIELDS OF TABLE fc_t_eine[]
      FOR ALL ENTRIES IN fc_t_eina[]
      WHERE infnr = fc_t_eina-infnr
*>>> IS 2019/06 configurable PIR Purch. org.
        AND ekorg IN lt_rng_ekorg
        "AND ekorg = fc_v_ekorg_main                         " '9999'
*<<< IS 2019/06 configurable PIR Purch. org.
        AND werks = space.

      SELECT * FROM eine
      INTO CORRESPONDING FIELDS OF TABLE fc_t_eine_site[]                     "++3181 JKH 19.05.2017
      FOR ALL ENTRIES IN fc_t_eina[]
      WHERE infnr EQ fc_t_eina-infnr
        AND werks NE space.


    ENDIF.

  ENDIF.   " IF fu_t_idno_data[] IS NOT INITIAL

  SELECT * FROM zarn_uom_cat
    INTO CORRESPONDING FIELDS OF TABLE fc_t_uom_cat.

  SELECT * FROM zarn_coo_t
    INTO CORRESPONDING FIELDS OF TABLE fc_t_coo_t
    WHERE spras = sy-langu.

  SELECT * FROM zarn_gen_uom_t
    INTO CORRESPONDING FIELDS OF TABLE fc_t_gen_uom_t
    WHERE spras = sy-langu.

  SELECT msehi isocode FROM t006
    INTO CORRESPONDING FIELDS OF TABLE fc_t_t006[].

  SELECT land1 intca FROM t005
    INTO CORRESPONDING FIELDS OF TABLE fc_t_t005[].

  SELECT waers isocd FROM tcurc
  INTO CORRESPONDING FIELDS OF TABLE fc_t_tcurc[].

  SELECT mtart vmtpo FROM t134
    INTO CORRESPONDING FIELDS OF TABLE fc_t_t134[].

  SELECT vkorg vtweg spart FROM tvta
    INTO CORRESPONDING FIELDS OF TABLE fc_t_tvta[].



* Article Hierarchy Properties
  SELECT SINGLE * FROM wrf_matgrp_hier INTO fc_s_matgrp_hier WHERE status = '02'.
  IF sy-subrc = 0.

* Article Hierarchy Node
    SELECT a~hier_id a~node a~date_from a~date_to a~tree_level a~parent a~catflg
           b~ltext b~ltextg
      INTO CORRESPONDING FIELDS OF TABLE fc_t_art_hier[]
      FROM wrf_matgrp_struc AS a
      JOIN wrf_matgrp_strct AS b
      ON a~hier_id = b~hier_id AND
         a~node    = b~node
      WHERE a~hier_id   EQ fc_s_matgrp_hier-hier_id
        AND a~date_from LE sy-datum
        AND a~date_to   GE sy-datum
        AND b~spras     EQ sy-langu.

    IF fu_t_idno_data[] IS NOT INITIAL.
* Hierarchy-Product Assignment Table
      SELECT matnr hier_id node date_from date_to strategy mainflg hiernode1 hiernode2 hiernode3 deleteflag
        INTO CORRESPONDING FIELDS OF TABLE fc_t_matgrp_prod[]
        FROM wrf_matgrp_prod
        FOR ALL ENTRIES IN fu_t_idno_data[]
        WHERE matnr   = fu_t_idno_data-sap_article
          AND hier_id = fc_s_matgrp_hier-hier_id.
    ENDIF.

  ENDIF.


ENDFORM.                    " GET_SAP_DATA
*&---------------------------------------------------------------------*
*&      Form  BUILD_MAPPING_DATA
*&---------------------------------------------------------------------*
* Build Mapping Data and Post Article
*----------------------------------------------------------------------*
FORM build_mapping_data_and_post USING fu_t_idno_data       TYPE ty_t_idno_data
                                       fu_t_prod_data       TYPE ztarn_prod_data
                                       fu_t_reg_data        TYPE ztarn_reg_data
                                       fu_r_status          TYPE ztarn_status_range
                                       fu_t_mara            TYPE ty_t_mara
                                       fu_t_marm            TYPE ty_t_marm
                                       fu_t_mean            TYPE ty_t_mean
                                       fu_t_eina            TYPE ty_t_eina
                                       fu_t_eine            TYPE ty_t_eine
                                       fu_t_eine_site       TYPE ty_t_eine                   "++3181 JKH 19.05.2017
                                       fu_t_maw1            TYPE ty_t_maw1
                                       fu_t_host_sap        TYPE ty_t_host_sap
                                       fu_t_t006            TYPE ty_t_t006
                                       fu_t_t005            TYPE ty_t_t005
                                       fu_t_tcurc           TYPE ty_t_tcurc
                                       fu_t_coo_t           TYPE ty_t_coo_t
                                       fu_t_gen_uom_t       TYPE ty_t_gen_uom_t
                                       fu_t_t134            TYPE ty_t_t134
                                       fu_t_pir_ekorg       TYPE zarn_t_pir_pur_cnf "IS 2019/06
                                       "fu_t_tvarvc_ekorg    TYPE tvarvc_t                  "IS 2019/06
                                       fu_t_tvarvc          TYPE tvarvc_t
                                       fu_t_tvarvc_hyb_code TYPE tvarvc_t
                                       fu_t_tvta            TYPE tvta_tt
                                       fu_t_uom_cat         TYPE ty_t_uom_cat
                                       fu_s_matgrp_hier     TYPE ty_s_matgrp_hier
                                       fu_t_matgrp_prod     TYPE ty_t_matgrp_prod
                                       fu_t_art_hier        TYPE ty_t_art_hier
                                       fu_v_ekorg_main      TYPE ekorg
                                       fu_v_batch           TYPE flag
                              CHANGING fc_t_message         TYPE bal_t_msg
                                       fc_v_error           TYPE flag
                                       fc_t_idno_data_err   TYPE ty_t_idno_data
                                       fc_t_idno_data_suc   TYPE ty_t_idno_data.

  DATA: ls_idno_data             TYPE ty_s_idno_data,
        ls_prod_data             TYPE zsarn_prod_data,
        ls_reg_data              TYPE zsarn_reg_data,

        lv_nat_map               TYPE flag,
        lv_reg_map               TYPE flag,
        lv_arena_success         TYPE char1,

        lt_mara                  TYPE mara_tab,
        lt_marm                  TYPE marm_tab,
        lt_mean                  TYPE mean_tab,
        lt_eina                  TYPE mmpr_eina,
        lt_eine                  TYPE mmpr_eine,
        lt_maw1                  TYPE wrf_maw1_tty,
        lt_host_sap              TYPE ztmd_host_sap,
        lt_t006                  TYPE tt_t006,
        lt_t005                  TYPE wgds_countries_tty,
        lt_tcurc                 TYPE fiappt_t_tcurc_cbr,
        lt_coo_t                 TYPE ztarn_coo_t,
        lt_gen_uom_t             TYPE ztarn_gen_uom_t,
        lt_t134                  TYPE wrf_t134_tty,
        "lt_tvarvc_ekorg          TYPE tvarvc_t,          "2019/06
        lt_tvarvc                TYPE tvarvc_t,
        lt_tvarvc_hyb_code       TYPE tvarvc_t,
        lt_tvta                  TYPE tvta_tt,
        lt_uom_cat               TYPE ztarn_uom_cat,
        ls_matgrp_hier           TYPE wrf_matgrp_hier,
        lt_matgrp_prod           TYPE wrf_t_matgrp_prod,
        lt_art_hier              TYPE ztarn_art_hier,
        ls_log_params            TYPE zsarn_slg1_log_params_art_post,
        ls_pir_key               TYPE zsarn_key,
        lt_eine_unit_error       TYPE STANDARD TABLE OF wrfbapieine,           "++IR5101117 JKH 25.08.2016
        ls_eine_unit_error       TYPE wrfbapieine,                             "++IR5101117 JKH 25.08.2016
        ls_headdata              TYPE bapie1mathead,
        ls_hierarchy_data        TYPE bapi_wrf_hier_change_head,
        lt_return                TYPE wty_bapireturn1_tab,
        ls_return                TYPE bapireturn1,
        lt_errorkeys             TYPE STANDARD TABLE OF wrfvalkey,
        lt_variantskeys          TYPE STANDARD TABLE OF bapie1varkey,
        lt_characteristicvalue   TYPE STANDARD TABLE OF bapie1ausprt,
        lt_characteristicvaluex  TYPE STANDARD TABLE OF bapie1ausprtx,
        lt_clientdata            TYPE STANDARD TABLE OF bapie1marart,
        lt_clientdatax           TYPE STANDARD TABLE OF bapie1marartx,
        lt_clientext             TYPE STANDARD TABLE OF bapie1maraextrt,
        lt_clientextx            TYPE STANDARD TABLE OF bapie1maraextrtx,
        lt_addnlclientdata       TYPE STANDARD TABLE OF bapie1maw1rt,
        lt_addnlclientdatax      TYPE STANDARD TABLE OF bapie1maw1rtx,
        lt_materialdescription   TYPE STANDARD TABLE OF bapie1maktrt,
        lt_plantdata             TYPE STANDARD TABLE OF bapie1marcrt,
        lt_plantdatax            TYPE STANDARD TABLE OF bapie1marcrtx,
        lt_plantext              TYPE STANDARD TABLE OF bapie1marcextrt,
        lt_plantextx             TYPE STANDARD TABLE OF bapie1marcextrtx,
        lt_forecastparameters    TYPE STANDARD TABLE OF bapie1mpoprt,
        lt_forecastparametersx   TYPE STANDARD TABLE OF bapie1mpoprtx,
        lt_forecastvalues        TYPE STANDARD TABLE OF bapie1mprwrt,
        lt_totalconsumption      TYPE STANDARD TABLE OF bapie1mvegrt,
        lt_unplndconsumption     TYPE STANDARD TABLE OF bapie1mveurt,
        lt_planningdata          TYPE STANDARD TABLE OF bapie1mpgdrt,
        lt_planningdatax         TYPE STANDARD TABLE OF bapie1mpgdrtx,
        lt_storagelocationdata   TYPE STANDARD TABLE OF bapie1mardrt,
        lt_storagelocationdatax  TYPE STANDARD TABLE OF bapie1mardrtx,
        lt_storagelocationext    TYPE STANDARD TABLE OF bapie1mardextrt,
        lt_storagelocationextx   TYPE STANDARD TABLE OF bapie1mardextrtx,
        lt_unitsofmeasure        TYPE STANDARD TABLE OF bapie1marmrt,
        lt_unitsofmeasurex       TYPE STANDARD TABLE OF bapie1marmrtx,
        lt_unitofmeasuretexts    TYPE STANDARD TABLE OF bapie1mamtrt,
        lt_internationalartnos   TYPE STANDARD TABLE OF bapie1meanrt,
        lt_vendorean             TYPE STANDARD TABLE OF bapie1mleart,
        lt_layoutmoduleassgmt    TYPE STANDARD TABLE OF bapie1malgrt,
        lt_layoutmoduleassgmtx   TYPE STANDARD TABLE OF bapie1malgrtx,
        lt_taxclassifications    TYPE STANDARD TABLE OF bapie1mlanrt,
        lt_valuationdata         TYPE STANDARD TABLE OF bapie1mbewrt,
        lt_valuationdatax        TYPE STANDARD TABLE OF bapie1mbewrtx,
        lt_valuationext          TYPE STANDARD TABLE OF bapie1mbewextrt,
        lt_valuationextx         TYPE STANDARD TABLE OF bapie1mbewextrtx,
        lt_warehousenumberdata   TYPE STANDARD TABLE OF bapie1mlgnrt,
        lt_warehousenumberdatax  TYPE STANDARD TABLE OF bapie1mlgnrtx,
        lt_warehousenumberext    TYPE STANDARD TABLE OF bapie1mlgnextrt,
        lt_warehousenumberextx   TYPE STANDARD TABLE OF bapie1mlgnextrtx,
        lt_storagetypedata       TYPE STANDARD TABLE OF bapie1mlgtrt,
        lt_storagetypedatax      TYPE STANDARD TABLE OF bapie1mlgtrtx,
        lt_storagetypeext        TYPE STANDARD TABLE OF bapie1mlgtextrt,
        lt_storagetypeextx       TYPE STANDARD TABLE OF bapie1mlgtextrtx,
        lt_salesdata             TYPE STANDARD TABLE OF bapie1mvkert,
        lt_salesdatax            TYPE STANDARD TABLE OF bapie1mvkertx,
        lt_salesext              TYPE STANDARD TABLE OF bapie1mvkeextrt,
        lt_salesextx             TYPE STANDARD TABLE OF bapie1mvkeextrtx,
        lt_posdata               TYPE STANDARD TABLE OF bapie1wlk2rt,
        lt_posdatax              TYPE STANDARD TABLE OF bapie1wlk2rtx,
        lt_posext                TYPE STANDARD TABLE OF bapie1wlk2extrt,
        lt_posextx               TYPE STANDARD TABLE OF bapie1wlk2extrtx,
        lt_materiallongtext      TYPE STANDARD TABLE OF bapie1mltxrt,
        lt_plantkeys             TYPE STANDARD TABLE OF bapie1wrkkey,
        lt_storagelocationkeys   TYPE STANDARD TABLE OF bapie1lgokey,
        lt_distrchainkeys        TYPE STANDARD TABLE OF bapie1vtlkey,
        lt_warehousenokeys       TYPE STANDARD TABLE OF bapie1lgnkey,
        lt_storagetypekeys       TYPE STANDARD TABLE OF bapie1lgtkey,
        lt_valuationtypekeys     TYPE STANDARD TABLE OF bapie1bwakey,
        lt_importextension       TYPE STANDARD TABLE OF bapiparmatex,
        lt_inforecord_general    TYPE STANDARD TABLE OF bapieina,
        lt_inforecord_purchorg   TYPE STANDARD TABLE OF wrfbapieine,
        lt_source_list           TYPE STANDARD TABLE OF eordu,
        lt_additionaldata        TYPE STANDARD TABLE OF bapie1wta01,
        lt_calculationitemin     TYPE STANDARD TABLE OF bapicalcitemin,
        lt_calculationiteminx    TYPE STANDARD TABLE OF bapicalciteminx,
        lt_calculationextin      TYPE STANDARD TABLE OF bapiparex,
        lt_calculationitemout    TYPE STANDARD TABLE OF bapicalcitemout,
        lt_recipientparameters   TYPE STANDARD TABLE OF bapi_wrpl_import,
        lt_recipientparametersx  TYPE STANDARD TABLE OF bapi_wrpl_importx,
        lt_vendormatheader       TYPE STANDARD TABLE OF bapi_vendor_mat_header,
        lt_vendormatcharvalues   TYPE STANDARD TABLE OF bapi_vendor_mat_char_valu,
        lt_listingconditions     TYPE STANDARD TABLE OF wlk1_ueb,
        lt_bomheader             TYPE STANDARD TABLE OF wstr_in,
        lt_bompositions          TYPE STANDARD TABLE OF wrfstpob,
        lt_priceconditions       TYPE STANDARD TABLE OF edidd,
        lt_hierarchy_items       TYPE STANDARD TABLE OF bapi_wrf_hier_change_items,
        lt_followupmatdata       TYPE STANDARD TABLE OF wrf_folup_typ_a,
        lt_materialdatax         TYPE STANDARD TABLE OF wrfmatdatax,
        lt_reg_cluster           TYPE ztarn_reg_cluster,
        lt_calp_vb               TYPE calp_vb_tab,
        lv_main_data_error       TYPE boolean,
        lv_additional_data_error TYPE boolean,
        ls_bapiret2              TYPE bapiret2.

  lt_mara[]            = fu_t_mara[].
  lt_marm[]            = fu_t_marm[].
  lt_mean[]            = fu_t_mean[].
  lt_eina[]            = fu_t_eina[].
  lt_eine[]            = fu_t_eine[].
  lt_maw1[]            = fu_t_maw1[].
  lt_host_sap[]        = fu_t_host_sap[].
  lt_t006[]            = fu_t_t006[].
  lt_t005[]            = fu_t_t005[].
  lt_tcurc[]           = fu_t_tcurc[].
  lt_coo_t[]           = fu_t_coo_t[].
  lt_gen_uom_t[]       = fu_t_gen_uom_t[].
  lt_t134[]            = fu_t_t134[].
  "lt_tvarvc_ekorg[]    = fu_t_tvarvc_ekorg[].        "IS 2019/06
  lt_tvarvc[]          = fu_t_tvarvc[].
  lt_tvarvc_hyb_code[] = fu_t_tvarvc_hyb_code[].
  lt_tvta[]            = fu_t_tvta[].
  lt_uom_cat[]         = fu_t_uom_cat[].
  ls_matgrp_hier       = fu_s_matgrp_hier.
  lt_matgrp_prod[]     = fu_t_matgrp_prod[].
  lt_art_hier[]        = fu_t_art_hier[].


  CLEAR: fc_t_idno_data_err[],fc_t_idno_data_suc[].

  DATA(lo_helper) = lcl_article_post_helper=>get_instance( ).
  LOOP AT fu_t_idno_data INTO ls_idno_data.

    CLEAR : ls_headdata,
            ls_hierarchy_data,
            lt_return[],
            lt_errorkeys[],
            lt_variantskeys[],
            lt_characteristicvalue[],
            lt_characteristicvaluex[],
            lt_clientdata[],
            lt_clientdatax[],
            lt_clientext[],
            lt_clientextx[],
            lt_addnlclientdata[],
            lt_addnlclientdatax[],
            lt_materialdescription[],
            lt_plantdata[],
            lt_plantdatax[],
            lt_plantext[],
            lt_plantextx[],
            lt_forecastparameters[],
            lt_forecastparametersx[],
            lt_forecastvalues[],
            lt_totalconsumption[],
            lt_unplndconsumption[],
            lt_planningdata[],
            lt_planningdatax[],
            lt_storagelocationdata[],
            lt_storagelocationdatax[],
            lt_storagelocationext[],
            lt_storagelocationextx[],
            lt_unitsofmeasure[],
            lt_unitsofmeasurex[],
            lt_unitofmeasuretexts[],
            lt_internationalartnos[],
            lt_vendorean[],
            lt_layoutmoduleassgmt[],
            lt_layoutmoduleassgmtx[],
            lt_taxclassifications[],
            lt_valuationdata[],
            lt_valuationdatax[],
            lt_valuationext[],
            lt_valuationextx[],
            lt_warehousenumberdata[],
            lt_warehousenumberdatax[],
            lt_warehousenumberext[],
            lt_warehousenumberextx[],
            lt_storagetypedata[],
            lt_storagetypedatax[],
            lt_storagetypeext[],
            lt_storagetypeextx[],
            lt_salesdata[],
            lt_salesdatax[],
            lt_salesext[],
            lt_salesextx[],
            lt_posdata[],
            lt_posdatax[],
            lt_posext[],
            lt_posextx[],
            lt_materiallongtext[],
            lt_plantkeys[],
            lt_storagelocationkeys[],
            lt_distrchainkeys[],
            lt_warehousenokeys[],
            lt_storagetypekeys[],
            lt_valuationtypekeys[],
            lt_importextension[],
            lt_inforecord_general[],
            lt_inforecord_purchorg[],
            lt_eine_unit_error[],           "++IR5101117 JKH 25.08.2016
            lt_source_list[],
            lt_additionaldata[],
            lt_calculationitemin[],
            lt_calculationiteminx[],
            lt_calculationextin[],
            lt_calculationitemout[],
            lt_recipientparameters[],
            lt_recipientparametersx[],
            lt_vendormatheader[],
            lt_vendormatcharvalues[],
            lt_listingconditions[],
            lt_bomheader[],
            lt_bompositions[],
            lt_priceconditions[],
            lt_hierarchy_items[],
            lt_followupmatdata[],
            lt_materialdatax[],

            lt_calp_vb[].

    CLEAR: lv_nat_map, lv_reg_map.

    CLEAR fc_v_error.

    CLEAR ls_prod_data.
    READ TABLE fu_t_prod_data[] INTO ls_prod_data
    WITH KEY idno = ls_idno_data-idno
          version = ls_idno_data-current_ver.

    CLEAR ls_reg_data.
    READ TABLE fu_t_reg_data[] INTO ls_reg_data
    WITH KEY idno = ls_idno_data-idno.


* Build Mapping Data for Article Posting
    CALL FUNCTION 'ZARN_MAP_ARTICLE_POST_DATA'
      EXPORTING
        is_idno_data            = ls_idno_data
        is_prod_data            = ls_prod_data
        is_reg_data             = ls_reg_data
        ir_status               = fu_r_status[]
        it_mara                 = lt_mara[]
        it_marm                 = lt_marm[]
        it_mean                 = lt_mean[]
        it_eina                 = lt_eina[]
        it_eine                 = lt_eine[]
        it_maw1                 = lt_maw1[]
        it_host_sap             = lt_host_sap[]
        it_t006                 = lt_t006[]
        it_t005                 = lt_t005[]
        it_tcurc                = lt_tcurc[]
        it_coo_t                = lt_coo_t[]
        it_gen_uom_t            = lt_gen_uom_t[]
        it_t134                 = lt_t134[]
        it_pir_ekorg            = fu_t_pir_ekorg     "IS 2019/06
        "it_tvarvc_ekorg         = lt_tvarvc_ekorg[]  "IS 2019/06
        it_tvarvc               = lt_tvarvc[]
        it_tvarvc_hyb_code      = lt_tvarvc_hyb_code[]
        it_tvta                 = lt_tvta[]
        it_uom_cat              = lt_uom_cat[]
        is_matgrp_hier          = ls_matgrp_hier
        it_matgrp_prod          = lt_matgrp_prod[]
        it_art_hier             = lt_art_hier[]
        iv_batch                = fu_v_batch
      IMPORTING
        es_pir_key              = ls_pir_key
        ev_nat_map              = lv_nat_map
        ev_reg_map              = lv_reg_map
        es_headdata             = ls_headdata
        es_hierarchy_data       = ls_hierarchy_data
        et_errorkeys            = lt_errorkeys[]
        et_variantskeys         = lt_variantskeys[]
        et_characteristicvalue  = lt_characteristicvalue[]
        et_characteristicvaluex = lt_characteristicvaluex[]
        et_clientdata           = lt_clientdata[]
        et_clientdatax          = lt_clientdatax[]
        et_clientext            = lt_clientext[]
        et_clientextx           = lt_clientextx[]
        et_addnlclientdata      = lt_addnlclientdata[]
        et_addnlclientdatax     = lt_addnlclientdatax[]
        et_materialdescription  = lt_materialdescription[]
        et_plantdata            = lt_plantdata[]
        et_plantdatax           = lt_plantdatax[]
        et_plantext             = lt_plantext[]
        et_plantextx            = lt_plantextx[]
        et_forecastparameters   = lt_forecastparameters[]
        et_forecastparametersx  = lt_forecastparametersx[]
        et_forecastvalues       = lt_forecastvalues[]
        et_totalconsumption     = lt_totalconsumption[]
        et_unplndconsumption    = lt_unplndconsumption[]
        et_planningdata         = lt_planningdata[]
        et_planningdatax        = lt_planningdatax[]
        et_storagelocationdata  = lt_storagelocationdata[]
        et_storagelocationdatax = lt_storagelocationdatax[]
        et_storagelocationext   = lt_storagelocationext[]
        et_storagelocationextx  = lt_storagelocationextx[]
        et_unitsofmeasure       = lt_unitsofmeasure[]
        et_unitsofmeasurex      = lt_unitsofmeasurex[]
        et_unitofmeasuretexts   = lt_unitofmeasuretexts[]
        et_internationalartnos  = lt_internationalartnos[]
        et_vendorean            = lt_vendorean[]
        et_layoutmoduleassgmt   = lt_layoutmoduleassgmt[]
        et_layoutmoduleassgmtx  = lt_layoutmoduleassgmtx[]
        et_taxclassifications   = lt_taxclassifications[]
        et_valuationdata        = lt_valuationdata[]
        et_valuationdatax       = lt_valuationdatax[]
        et_valuationext         = lt_valuationext[]
        et_valuationextx        = lt_valuationextx[]
        et_warehousenumberdata  = lt_warehousenumberdata[]
        et_warehousenumberdatax = lt_warehousenumberdatax[]
        et_warehousenumberext   = lt_warehousenumberext[]
        et_warehousenumberextx  = lt_warehousenumberextx[]
        et_storagetypedata      = lt_storagetypedata[]
        et_storagetypedatax     = lt_storagetypedatax[]
        et_storagetypeext       = lt_storagetypeext[]
        et_storagetypeextx      = lt_storagetypeextx[]
        et_salesdata            = lt_salesdata[]
        et_salesdatax           = lt_salesdatax[]
        et_salesext             = lt_salesext[]
        et_salesextx            = lt_salesextx[]
        et_posdata              = lt_posdata[]
        et_posdatax             = lt_posdatax[]
        et_posext               = lt_posext[]
        et_posextx              = lt_posextx[]
        et_materiallongtext     = lt_materiallongtext[]
        et_plantkeys            = lt_plantkeys[]
        et_storagelocationkeys  = lt_storagelocationkeys[]
        et_distrchainkeys       = lt_distrchainkeys[]
        et_warehousenokeys      = lt_warehousenokeys[]
        et_storagetypekeys      = lt_storagetypekeys[]
        et_valuationtypekeys    = lt_valuationtypekeys[]
        et_importextension      = lt_importextension[]
        et_inforecord_general   = lt_inforecord_general[]
        et_inforecord_purchorg  = lt_inforecord_purchorg[]
        et_eine_unit_error      = lt_eine_unit_error[]          "++IR5101117 JKH 25.08.2016
        et_source_list          = lt_source_list[]
        et_additionaldata       = lt_additionaldata[]
        et_calculationitemin    = lt_calculationitemin[]
        et_calculationiteminx   = lt_calculationiteminx[]
        et_calculationextin     = lt_calculationextin[]
        et_calculationitemout   = lt_calculationitemout[]
        et_recipientparameters  = lt_recipientparameters[]
        et_recipientparametersx = lt_recipientparametersx[]
        et_vendormatheader      = lt_vendormatheader[]
        et_vendormatcharvalues  = lt_vendormatcharvalues[]
        et_listingconditions    = lt_listingconditions[]
        et_bomheader            = lt_bomheader[]
        et_bompositions         = lt_bompositions[]
        et_priceconditions      = lt_priceconditions[]
        et_hierarchy_items      = lt_hierarchy_items[]
        et_followupmatdata      = lt_followupmatdata[]
        et_materialdatax        = lt_materialdatax[]
        et_calp_vb              = lt_calp_vb[].


* Set Flag for Article Posting as TRUE
    CALL FUNCTION 'ZARN_SET_GET_ARTICLE_POST'
      EXPORTING
        iv_mode                = 'W'
        iv_arena               = abap_true
        is_pir_key             = ls_pir_key
        it_inforecord_general  = lt_inforecord_general[]
        it_inforecord_purchorg = lt_inforecord_purchorg[].

* Post Article into SAP
    PERFORM post_article_sap USING ls_idno_data
                                   ls_pir_key
                                   ls_prod_data
                                   ls_reg_data
                                   ls_headdata
                                   ls_hierarchy_data
                                   lt_errorkeys[]
                                   lt_variantskeys[]
                                   lt_characteristicvalue[]
                                   lt_characteristicvaluex[]
                                   lt_clientdata[]
                                   lt_clientdatax[]
                                   lt_clientext[]
                                   lt_clientextx[]
                                   lt_addnlclientdata[]
                                   lt_addnlclientdatax[]
                                   lt_materialdescription[]
                                   lt_plantdata[]
                                   lt_plantdatax[]
                                   lt_plantext[]
                                   lt_plantextx[]
                                   lt_forecastparameters[]
                                   lt_forecastparametersx[]
                                   lt_forecastvalues[]
                                   lt_totalconsumption[]
                                   lt_unplndconsumption[]
                                   lt_planningdata[]
                                   lt_planningdatax[]
                                   lt_storagelocationdata[]
                                   lt_storagelocationdatax[]
                                   lt_storagelocationext[]
                                   lt_storagelocationextx[]
                                   lt_unitsofmeasure[]
                                   lt_unitsofmeasurex[]
                                   lt_unitofmeasuretexts[]
                                   lt_internationalartnos[]
                                   lt_vendorean[]
                                   lt_layoutmoduleassgmt[]
                                   lt_layoutmoduleassgmtx[]
                                   lt_taxclassifications[]
                                   lt_valuationdata[]
                                   lt_valuationdatax[]
                                   lt_valuationext[]
                                   lt_valuationextx[]
                                   lt_warehousenumberdata[]
                                   lt_warehousenumberdatax[]
                                   lt_warehousenumberext[]
                                   lt_warehousenumberextx[]
                                   lt_storagetypedata[]
                                   lt_storagetypedatax[]
                                   lt_storagetypeext[]
                                   lt_storagetypeextx[]
                                   lt_salesdata[]
                                   lt_salesdatax[]
                                   lt_salesext[]
                                   lt_salesextx[]
                                   lt_posdata[]
                                   lt_posdatax[]
                                   lt_posext[]
                                   lt_posextx[]
                                   lt_materiallongtext[]
                                   lt_plantkeys[]
                                   lt_storagelocationkeys[]
                                   lt_distrchainkeys[]
                                   lt_warehousenokeys[]
                                   lt_storagetypekeys[]
                                   lt_valuationtypekeys[]
                                   lt_importextension[]
                                   lt_inforecord_general[]
                                   lt_inforecord_purchorg[]
                                   lt_source_list[]
                                   lt_additionaldata[]
                                   lt_calculationitemin[]
                                   lt_calculationiteminx[]
                                   lt_calculationextin[]
                                   lt_calculationitemout[]
                                   lt_recipientparameters[]
                                   lt_recipientparametersx[]
                                   lt_vendormatheader[]
                                   lt_vendormatcharvalues[]
                                   lt_listingconditions[]
                                   lt_bomheader[]
                                   lt_bompositions[]
                                   lt_priceconditions[]
                                   lt_hierarchy_items[]
                                   lt_followupmatdata[]
                                   lt_materialdatax[]
                          CHANGING lt_return[]
                                   lv_main_data_error
                                   lv_additional_data_error.

    CLEAR fc_v_error.
    LOOP AT lt_return[] INTO ls_return.

      MOVE-CORRESPONDING ls_return TO ls_bapiret2.
      lo_helper->add_message( EXPORTING is_idno_data = ls_idno_data
                                        is_bapiret2  = ls_bapiret2
                                        iv_batch     = fu_v_batch
                              CHANGING  ct_bal_msg   = fc_t_message ).

      IF ls_return-type = gc_message-error.
        fc_v_error = abap_true.
      ENDIF.

    ENDLOOP.

    LOOP AT lt_eine_unit_error[] INTO ls_eine_unit_error.

      MESSAGE w074 WITH ls_eine_unit_error-material ls_eine_unit_error-vendor ls_eine_unit_error-orderpr_un
                   INTO zcl_message_services=>sv_msg_dummy.
      lo_helper->add_message( EXPORTING is_idno_data = ls_idno_data
                                        iv_batch     = fu_v_batch
                              CHANGING  ct_bal_msg   = fc_t_message ).

    ENDLOOP.

    IF fc_v_error = abap_true.
      " Rollback
      CALL FUNCTION 'BAPI_TRANSACTION_ROLLBACK'.
    ELSE.
      " Commit
      CALL FUNCTION 'BAPI_TRANSACTION_COMMIT'
        EXPORTING
          wait = abap_true.

      PERFORM update_pir_all_sites USING ls_reg_data-zarn_reg_pir[]
                                         fu_t_eina
                                         fu_t_eine_site
                                         ls_idno_data
                                         fu_v_batch
                                CHANGING fc_t_message[].

      PERFORM post_conditions USING lt_calp_vb[]
                                    lt_inforecord_general[]
                                    ls_idno_data
                                    fu_v_batch
                           CHANGING fc_t_message[]
                                    fc_v_error.
    ENDIF.

    lt_reg_cluster = ls_reg_data-zarn_reg_cluster.
    PERFORM add_deleted_cluster USING    ls_reg_data-idno
                                CHANGING lt_reg_cluster.
    lcl_article_post_helper=>get_instance( )->post_layout_module( EXPORTING iv_batch       = fu_v_batch
                                                                            is_idno_data   = ls_idno_data
                                                                            it_reg_cluster = lt_reg_cluster
                                                                  CHANGING  ct_bal_msg     =  fc_t_message ).

    CLEAR lv_arena_success.
* Get Flag for Article Sucessfully Posted
    CALL FUNCTION 'ZARN_SET_GET_ARTICLE_POST'
      EXPORTING
        iv_mode          = 'R'
      IMPORTING
        ev_arena_success = lv_arena_success.

    ls_idno_data-post_error = fc_v_error.

* If Article is created/updated and later article changes fails,
* then also consider as success as article is partialy created/updated
    IF lv_arena_success = abap_true.
      APPEND ls_idno_data TO fc_t_idno_data_suc[].
    ELSE.
      APPEND ls_idno_data TO fc_t_idno_data_err[].
    ENDIF.

* Set Flag for Article Posting as FALSE
    CALL FUNCTION 'ZARN_SET_GET_ARTICLE_POST'
      EXPORTING
        iv_mode  = 'W'
        iv_arena = space.

  ENDLOOP.  " LOOP AT fu_t_idno_data INTO ls_idno_data.

ENDFORM.                    " BUILD_MAPPING_DATA
*&---------------------------------------------------------------------*
*&      Form  MAP_COMMON_DATA
*&---------------------------------------------------------------------*
* Common Mapping Data for National/Regional
*----------------------------------------------------------------------*
FORM map_common_data  USING    fu_s_idno_data           TYPE ty_s_idno_data
                               fu_s_prod_data           TYPE zsarn_prod_data
                               fu_s_reg_data            TYPE zsarn_reg_data
                               fu_t_mara                TYPE ty_t_mara
                               "fu_t_marm                TYPE ty_t_marm
                               "fu_t_mean                TYPE ty_t_mean
                               "fu_t_t006                TYPE ty_t_t006
                               fu_t_t005                TYPE ty_t_t005
                               fu_t_coo_t               TYPE ty_t_coo_t
                               "fu_t_gen_uom_t           TYPE ty_t_gen_uom_t
                               "fu_t_t134                TYPE ty_t_t134
                               "fu_t_tvarvc              TYPE tvarvc_t
                               fu_r_status              TYPE ztarn_status_range
                               fu_s_matgrp_hier         TYPE ty_s_matgrp_hier
                      CHANGING fc_s_headdata            TYPE bapie1mathead
                               fc_s_hierarchy_data      TYPE bapi_wrf_hier_change_head
                               fc_v_art_mode            TYPE char1
                               fc_t_clientdata          TYPE bapie1marart_tab
                               fc_t_clientdatax         TYPE bapie1marartx_tab
*                               fc_t_clientext           TYPE bapie1maraextrt_tab
*                               fc_t_clientextx          TYPE bapie1maraextrtx_tab
                               fc_t_addnlclientdata     TYPE bapie1maw1rt_tab
                               fc_t_addnlclientdatax    TYPE bapie1maw1rtx_tab.
*                               fc_t_unitsofmeasure      TYPE bapie1marmrt_tab
*                               fc_t_unitsofmeasurex     TYPE bapie1marmrtx_tab.


  DATA: ls_products TYPE zarn_products,
        ls_reg_hdr  TYPE zarn_reg_hdr,

        ls_reg_pir  TYPE zarn_reg_pir,
        ls_pir      TYPE zarn_pir,

        ls_mara     TYPE ty_s_mara,
        ls_coo_t    TYPE zarn_coo_t,
        ls_t005     TYPE ty_s_t005,

        BEGIN OF ls_data,
          field1 TYPE bapie1maraextrtx-field1,
          field2 TYPE bapie1maraextrtx-field2,
          field3 TYPE bapie1maraextrtx-field3,
          field4 TYPE bapie1maraextrtx-field4,
        END OF ls_data,


        ls_clientdata       TYPE bapie1marart,
        ls_clientdatax      TYPE bapie1marartx,
        ls_addnlclientdata  TYPE bapie1maw1rt,
        ls_addnlclientdatax TYPE bapie1maw1rtx.



  CLEAR: ls_products.
  READ TABLE fu_s_prod_data-zarn_products[] INTO ls_products INDEX 1.

* Regional Header
  CLEAR: ls_reg_hdr.
  READ TABLE fu_s_reg_data-zarn_reg_hdr[] INTO ls_reg_hdr INDEX 1.


**********************************************************************************

*** HEADDATA
  fc_s_headdata-material = fu_s_idno_data-sap_article.

  CLEAR ls_mara.
  READ TABLE fu_t_mara[] INTO ls_mara
  WITH KEY matnr = fu_s_idno_data-sap_article.
  IF sy-subrc = 0.
    fc_v_art_mode = 'U'.  " Update
  ELSE.
    fc_v_art_mode = 'C'.  " Create
  ENDIF.

  CALL FUNCTION 'CONVERSION_EXIT_MATN1_INPUT'
    EXPORTING
      input        = fc_s_headdata-material
    IMPORTING
      output       = fc_s_headdata-material
    EXCEPTIONS
      length_error = 1
      OTHERS       = 2.


  IF fc_v_art_mode = 'C'.  " Create

    fc_s_headdata-matl_group = ls_reg_hdr-matkl.
    fc_s_headdata-matl_type  = ls_reg_hdr-mtart.
    fc_s_headdata-matl_cat   = ls_reg_hdr-attyp.
  ENDIF.

  fc_s_headdata-function   = gc_bapi-func_z01.   " '0Z01'
  fc_s_headdata-basic_view = abap_true.
  fc_s_headdata-list_view  = abap_true.
  fc_s_headdata-sales_view = abap_true.
  fc_s_headdata-logdc_view = abap_true.
  fc_s_headdata-logst_view = abap_true.
  fc_s_headdata-pos_view   = abap_true.

**********************************************************************************

  CLEAR fc_s_hierarchy_data.
  fc_s_hierarchy_data-hier_id       = fu_s_matgrp_hier-hier_id.
  fc_s_hierarchy_data-log_flag      = abap_true.
  fc_s_hierarchy_data-status        = fu_s_matgrp_hier-status.
  fc_s_hierarchy_data-salesorg      = fu_s_matgrp_hier-vkorg.
  fc_s_hierarchy_data-distr_chan    = fu_s_matgrp_hier-vtweg.
  fc_s_hierarchy_data-bw_flag       = fu_s_matgrp_hier-bwflg.
  fc_s_hierarchy_data-mulitple_flag = fu_s_matgrp_hier-mltpflg.


**********************************************************************************

*** CLIENTDATA
  CLEAR ls_clientdata.
  ls_clientdata-function       = gc_bapi-func_005.    " '005'.
  ls_clientdata-material       = fc_s_headdata-material.


* Regional PIR
  CLEAR ls_reg_pir.
  LOOP AT fu_s_reg_data-zarn_reg_pir[] INTO ls_reg_pir
    WHERE idno  EQ fu_s_prod_data-idno
      AND relif EQ abap_true.
    EXIT.
  ENDLOOP.


* If Regional PIR is found
  CLEAR: ls_coo_t, ls_t005.
  IF ls_reg_pir-order_uom_pim        IS NOT INITIAL AND
     ls_reg_pir-hybris_internal_code IS NOT INITIAL.
* National PIR
    CLEAR ls_pir.
    READ TABLE fu_s_prod_data-zarn_pir INTO ls_pir
    WITH KEY idno                 = fu_s_prod_data-idno
             version              = fu_s_prod_data-version
             uom_code             = ls_reg_pir-order_uom_pim
             hybris_internal_code = ls_reg_pir-hybris_internal_code.

* Country of Origin Text
    IF ls_pir-country_of_origin IS NOT INITIAL.
      READ TABLE fu_t_coo_t INTO ls_coo_t
      WITH TABLE KEY country_of_origin = ls_pir-country_of_origin
                     spras             = sy-langu.
      IF ls_coo_t-sap_coo IS NOT INITIAL.
* Countries
        READ TABLE fu_t_t005[] INTO ls_t005
        WITH TABLE KEY land1 = ls_coo_t-sap_coo.
      ENDIF.
    ENDIF.
  ENDIF.  " If Regional PIR is found



  IF fc_v_art_mode = 'C'.  " Create
    ls_clientdata-created_on   = sy-datum.
    ls_clientdata-created_by   = sy-uname.
    ls_clientdata-valid_from   = sy-datum.

  ELSEIF fc_v_art_mode = 'U'.  " Update
    ls_clientdata-last_chnge     = sy-datum.
    ls_clientdata-changed_by     = sy-uname.
  ENDIF.

* If Regional Data is Approved, perform mapping for Regional data only
  IF fu_s_idno_data-reg_status IN fu_r_status[] OR fu_r_status[] IS INITIAL.
    ls_clientdata-countryori     = ls_coo_t-sap_coo.
    ls_clientdata-countryori_iso = ls_t005-intca.
  ENDIF.

  APPEND ls_clientdata TO fc_t_clientdata[].


*** CLIENTDATAX
  CLEAR ls_clientdatax.
  ls_clientdata-function       = gc_bapi-func_005.    " '005'.
  ls_clientdatax-material       = fc_s_headdata-material.

  IF fc_v_art_mode = 'C'.  " Create
    ls_clientdatax-created_on   = abap_true.
    ls_clientdatax-created_by   = abap_true.
    ls_clientdatax-valid_from   = abap_true.

  ELSEIF fc_v_art_mode = 'U'.  " Update
    ls_clientdatax-last_chnge     = abap_true.
    ls_clientdatax-changed_by     = abap_true.
  ENDIF.

* If Regional Data is Approved, perform mapping for Regional data only
  IF fu_s_idno_data-reg_status IN fu_r_status[] OR fu_r_status[] IS INITIAL.
    IF ls_coo_t-sap_coo IS NOT INITIAL.
      ls_clientdatax-countryori     = abap_true.
      ls_clientdatax-countryori_iso = abap_true.
    ENDIF.
  ENDIF.

  APPEND ls_clientdatax TO fc_t_clientdatax[].


*** ADDNLCLIENTDATA
  CLEAR ls_addnlclientdata.
  ls_addnlclientdata-function = gc_bapi-func_005.    " '005'.
  ls_addnlclientdata-material = fc_s_headdata-material.


  IF fc_v_art_mode = 'C'.  " Create
    ls_addnlclientdata-li_proc_st = 'B1'.
    ls_addnlclientdata-li_proc_dc = 'B1'.
    ls_addnlclientdata-list_st_fr = sy-datum.
    ls_addnlclientdata-list_st_to = '99991231'.
    ls_addnlclientdata-list_dc_fr = sy-datum.
    ls_addnlclientdata-list_dc_to = '99991231'.
    ls_addnlclientdata-sell_st_fr = sy-datum.
    ls_addnlclientdata-sell_st_to = '99991231'.
    ls_addnlclientdata-sell_dc_fr = sy-datum.
    ls_addnlclientdata-sell_dc_to = '99991231'.
  ENDIF.

* If Regional Data is Approved, perform mapping for Regional data only
  IF fu_s_idno_data-reg_status IN fu_r_status[] OR fu_r_status[] IS INITIAL.
    ls_addnlclientdata-countryori     = ls_coo_t-sap_coo.
    ls_addnlclientdata-countryori_iso = ls_t005-intca.
  ENDIF.

  APPEND ls_addnlclientdata TO fc_t_addnlclientdata[].


*** ADDNLCLIENTDATAX
  CLEAR ls_addnlclientdatax.
  ls_addnlclientdatax-function = gc_bapi-func_005.    " '005'.
  ls_addnlclientdatax-material = fc_s_headdata-material.

  IF fc_v_art_mode = 'C'.  " Create
    ls_addnlclientdatax-li_proc_st = abap_true.
    ls_addnlclientdatax-li_proc_dc = abap_true.
    ls_addnlclientdatax-list_st_fr = abap_true.
    ls_addnlclientdatax-list_st_to = abap_true.
    ls_addnlclientdatax-list_dc_fr = abap_true.
    ls_addnlclientdatax-list_dc_to = abap_true.
    ls_addnlclientdatax-sell_st_fr = abap_true.
    ls_addnlclientdatax-sell_st_to = abap_true.
    ls_addnlclientdatax-sell_dc_fr = abap_true.
    ls_addnlclientdatax-sell_dc_to = abap_true.
  ENDIF.

* If Regional Data is Approved, perform mapping for Regional data only
  IF fu_s_idno_data-reg_status IN fu_r_status[] OR fu_r_status[] IS INITIAL.
    IF ls_coo_t-sap_coo IS NOT INITIAL.
      ls_addnlclientdatax-countryori     = abap_true.
      ls_addnlclientdatax-countryori_iso = abap_true.
    ENDIF.
  ENDIF.

  APPEND ls_addnlclientdatax TO fc_t_addnlclientdatax[].


ENDFORM.                    " MAP_COMMON_DATA

*&---------------------------------------------------------------------*
*&      Form  MAP_NATIONAL_DATA
*&---------------------------------------------------------------------*
* Map National Data
*----------------------------------------------------------------------*
FORM map_national_data  USING    fu_s_idno_data           TYPE ty_s_idno_data
                                 fu_s_prod_data           TYPE zsarn_prod_data
                                 fu_s_reg_data            TYPE zsarn_reg_data
                                 fu_v_art_mode            TYPE char1
                                 fu_t_mara                TYPE ty_t_mara
                        CHANGING fc_s_headdata            TYPE bapie1mathead
                                 fc_t_clientext           TYPE bapie1maraextrt_tab
                                 fc_t_clientextx          TYPE bapie1maraextrtx_tab
                                 fc_s_bapi_clientext      TYPE zmd_s_bapi_clientext
                                 fc_s_bapi_clientextx     TYPE zmd_s_bapi_clientextx.


  DATA: ls_products        TYPE zarn_products,
        ls_mara            TYPE ty_s_mara,
        ls_bapi_clientext  TYPE zmd_s_bapi_clientext,
        ls_bapi_clientextx TYPE zmd_s_bapi_clientextx,
        lv_string          TYPE string,

        BEGIN OF ls_data,
          field1 TYPE bapie1maraextrtx-field1,
          field2 TYPE bapie1maraextrtx-field2,
          field3 TYPE bapie1maraextrtx-field3,
          field4 TYPE bapie1maraextrtx-field4,
        END OF ls_data,


        ls_clientext  TYPE bapie1maraextrt,
        ls_clientextx TYPE bapie1maraextrtx.


  FIELD-SYMBOLS:

    <ls_clientext>  TYPE bapie1maraextrt,
    <ls_clientextx> TYPE bapie1maraextrtx.


  IF <ls_clientext>           IS ASSIGNED. UNASSIGN <ls_clientext>.           ENDIF.
  IF <ls_clientextx>          IS ASSIGNED. UNASSIGN <ls_clientextx>.          ENDIF.

* Products
  CLEAR: ls_products.
  READ TABLE fu_s_prod_data-zarn_products[] INTO ls_products INDEX 1.

*** CLIENTEXT

  CLEAR: ls_bapi_clientext.
  ls_bapi_clientext-function = gc_bapi-func_005.    " gc_bapi-func_005.    " '005'.
  ls_bapi_clientext-material = fc_s_headdata-material.


*** CLIENTEXTX
  CLEAR: ls_bapi_clientextx.
  ls_bapi_clientext-function = gc_bapi-func_005.    " gc_bapi-func_005.    " '005'.
  ls_bapi_clientext-material = fc_s_headdata-material.

  ls_bapi_clientext-zzfan     = ls_products-fan_id.
  ls_bapi_clientextx-zzfan     = abap_true.


  CLEAR ls_mara.
  READ TABLE fu_t_mara[] INTO ls_mara
  WITH TABLE KEY matnr = fc_s_headdata-material.


  fc_s_bapi_clientext  = ls_bapi_clientext.
  fc_s_bapi_clientextx = ls_bapi_clientextx.

  CLEAR lv_string.
  cl_abap_container_utilities=>fill_container_c(
    EXPORTING im_value = ls_bapi_clientext-data
    IMPORTING ex_container = lv_string ).

  CLEAR ls_data.
  ls_data-field1(30) = 'ZMD_S_BAPI_CLIENTEXT'.
  ls_data+30 = lv_string.

*** CLIENTEXT
  READ TABLE fc_t_clientext[] ASSIGNING <ls_clientext>
  WITH KEY material = fc_s_headdata-material.
  IF sy-subrc = 0.
    <ls_clientext>-function = gc_bapi-func_005.    " gc_bapi-func_005.    " '005'.
    <ls_clientext>-material = fc_s_headdata-material.
    <ls_clientext>-field1   = ls_data-field1.
    <ls_clientext>-field2   = ls_data-field2.
    <ls_clientext>-field3   = ls_data-field3.
    <ls_clientext>-field4   = ls_data-field4.
  ELSE.
    ls_clientext-function = gc_bapi-func_005.    " gc_bapi-func_005.    " '005'.
    ls_clientext-material = fc_s_headdata-material.
    ls_clientext-field1   = ls_data-field1.
    ls_clientext-field2   = ls_data-field2.
    ls_clientext-field3   = ls_data-field3.
    ls_clientext-field4   = ls_data-field4.
    APPEND ls_clientext TO fc_t_clientext[].
  ENDIF.


  CLEAR lv_string.
  cl_abap_container_utilities=>fill_container_c(
    EXPORTING im_value = ls_bapi_clientextx-datax
    IMPORTING ex_container = lv_string ).

  CLEAR ls_data.
  ls_data-field1(30) = 'ZMD_S_BAPI_CLIENTEXTX'.
  ls_data+30 = lv_string.

*** CLIENTEXTX
  READ TABLE fc_t_clientextx[] ASSIGNING <ls_clientextx>
  WITH KEY material = fc_s_headdata-material.
  IF sy-subrc = 0.
    <ls_clientextx>-function = gc_bapi-func_005.    " gc_bapi-func_005.    " '005'.
    <ls_clientextx>-material = fc_s_headdata-material.
    <ls_clientextx>-field1   = ls_data-field1.
    <ls_clientextx>-field2   = ls_data-field2.
    <ls_clientextx>-field3   = ls_data-field3.
    <ls_clientextx>-field4   = ls_data-field4.
  ELSE.
    ls_clientextx-function = gc_bapi-func_005.    " gc_bapi-func_005.    " '005'.
    ls_clientextx-material = fc_s_headdata-material.
    ls_clientextx-field1   = ls_data-field1.
    ls_clientextx-field2   = ls_data-field2.
    ls_clientextx-field3   = ls_data-field3.
    ls_clientextx-field4   = ls_data-field4.
    APPEND ls_clientextx TO fc_t_clientextx[].
  ENDIF.

ENDFORM.                    " MAP_NATIONAL_DATA
*&---------------------------------------------------------------------*
*&      Form  MAP_REGIONAL_DATA
*&---------------------------------------------------------------------*
* Map Regional Data
*----------------------------------------------------------------------*
FORM map_regional_data  USING    fu_s_idno_data            TYPE ty_s_idno_data
                                 fu_s_prod_data            TYPE zsarn_prod_data
                                 fu_s_reg_data             TYPE zsarn_reg_data
                                 fu_v_art_mode             TYPE char1
                                 fu_t_mara                 TYPE ty_t_mara
                                 fu_t_marm                 TYPE ty_t_marm
                                 "fu_t_mean                 TYPE ty_t_mean
                                 fu_t_eina                 TYPE ty_t_eina
                                 fu_t_eine                 TYPE ty_t_eine
                                 fu_t_lfm1                 TYPE ty_t_lfm1
                                 "fu_t_maw1                 TYPE ty_t_maw1
                                 fu_t_host_sap             TYPE ty_t_host_sap
                                 fu_t_t006                 TYPE ty_t_t006
                                 fu_t_t005                 TYPE ty_t_t005
                                 fu_t_tcurc                TYPE ty_t_tcurc
                                 fu_t_coo_t                TYPE ty_t_coo_t
                                 fu_t_gen_uom_t            TYPE ty_t_gen_uom_t
                                 fu_t_t134                 TYPE ty_t_t134
                                 "fu_t_tvarvc_ekorg         TYPE tvarvc_t             "IS 2019/06
                                 fu_t_pir_ekorg            TYPE zarn_t_pir_pur_cnf "IS 2019/06
                                 fu_t_tvarvc               TYPE tvarvc_t
                                 fu_t_tvarvc_hyb_code      TYPE tvarvc_t
                                 fu_t_tvta                 TYPE tvta_tt
                                 fu_t_uom_cat              TYPE ty_t_uom_cat
                                 fu_s_matgrp_hier          TYPE ty_s_matgrp_hier
                                 fu_t_matgrp_prod          TYPE ty_t_matgrp_prod
                                 fu_t_art_hier             TYPE ty_t_art_hier
                                 fu_v_ekorg_main           TYPE ekorg
                        CHANGING fc_s_headdata             TYPE bapie1mathead
                                 fc_t_clientdata           TYPE bapie1marart_tab
                                 fc_t_clientdatax          TYPE bapie1marartx_tab
                                 fc_t_clientext            TYPE bapie1maraextrt_tab
                                 fc_t_clientextx           TYPE bapie1maraextrtx_tab
                                 fc_s_bapi_clientext       TYPE zmd_s_bapi_clientext
                                 fc_s_bapi_clientextx      TYPE zmd_s_bapi_clientextx
                                 fc_t_addnlclientdata      TYPE bapie1maw1rt_tab
                                 fc_t_addnlclientdatax     TYPE bapie1maw1rtx_tab
                                 fc_t_materialdescription  TYPE bapie1maktrt_tab
                                 fc_t_plantdata            TYPE bapie1marcrt_tab
                                 fc_t_plantdatax           TYPE bapie1marcrtx_tab
                                 fc_t_plantext             TYPE bapie1marcextrt_tab               "++ONLD-822 JKH 12.01.2017
                                 fc_t_plantextx            TYPE bapie1marcextrtx_tab              "++ONLD-822 JKH 12.01.2017
                                 fc_t_bapi_plantext        TYPE zmd_t_bapi_plantext               "++ONLD-822 JKH 12.01.2017
                                 fc_t_internationalartnos  TYPE bapie1meanrt_tab
                                 fc_t_taxclassifications   TYPE bapie1mlanrt_tab
                                 fc_t_valuationdata        TYPE bapie1mbewrt_tab
                                 fc_t_valuationdatax       TYPE bapie1mbewrtx_tab
                                 fc_t_warehousenumberdata  TYPE bapie1mlgnrt_tab                  "++ONLD-822 JKH 12.01.2017
                                 fc_t_warehousenumberdatax TYPE bapie1mlgnrtx_tab                 "++ONLD-822 JKH 12.01.2017
                                 fc_t_salesdata            TYPE bapie1mvkert_tab
                                 fc_t_salesdatax           TYPE bapie1mvkertx_tab
                                 fc_t_salesext             TYPE bapie1mvkeextrt_tab
                                 fc_t_salesextx            TYPE bapie1mvkeextrtx_tab
                                 fc_s_bapi_salesext        TYPE zmd_s_bapi_salesext
                                 fc_s_bapi_salesextx       TYPE zmd_s_bapi_salesextx
                                 fc_t_bapi_salesext        TYPE zmd_t_bapi_salesext
                                 fc_t_posdata              TYPE bapie1wlk2rt_tab
                                 fc_t_posdatax             TYPE bapie1wlk2rtx_tab
                                 fc_t_materiallongtext     TYPE bapie1mltxrt_tab
                                 fc_t_plantkeys            TYPE bapie1wrkkey_tab
                                 fc_t_storagelocationkeys  TYPE bapie1lgokey_tab
                                 fc_t_distrchainkeys       TYPE bapie1vtlkey_tab
                                 fc_t_valuationtypekeys    TYPE bapie1bwakey_tab
                                 fc_t_importextension      TYPE wrf_bapiparmatex_tty
                                 fc_t_legacy_struct        TYPE ztarn_legacy_struct
                                 fc_t_inforecord_general   TYPE wrf_bapieina_tty
                                 fc_t_inforecord_purchorg  TYPE wrf_bapieine_tty
                                 fc_t_eine_unit_error      TYPE wrf_bapieine_tty    "++IR5101117 JKH 25.08.2016
                                 fc_t_hierarchy_items      TYPE bapi_wrf_hier_ch_items_tty
                                 fc_t_calp_vb              TYPE calp_vb_tab
                                 fc_t_unitsofmeasure       TYPE bapie1marmrt_tab
                                 fc_t_unitsofmeasurex      TYPE bapie1marmrtx_tab
                                 fc_s_pir_key              TYPE zsarn_key.


  DATA: ls_products        TYPE zarn_products,
        ls_reg_hdr         TYPE zarn_reg_hdr,
        lt_reg_pir_vend    TYPE ztarn_reg_pir,
        ls_reg_pir_vend    TYPE zarn_reg_pir,
        ls_reg_pir         TYPE zarn_reg_pir,
        ls_reg_pir_po      TYPE zarn_reg_pir,
        ls_reg_uom         TYPE zarn_reg_uom,
        ls_reg_ean         TYPE zarn_reg_ean,
        ls_reg_banner      TYPE zarn_reg_banner,
        ls_clientdata_tmp  TYPE bapie1marart,
        ls_mara            TYPE ty_s_mara,
        ls_marm            TYPE ty_s_marm,
        ls_eina            TYPE ty_s_eina,
        ls_eine            TYPE ty_s_eine,
        ls_lfm1            TYPE ty_s_lfm1,
        ls_gen_uom_t       TYPE zarn_gen_uom_t,
        ls_t006            TYPE ty_s_t006,
        ls_tcurc           TYPE ty_s_tcurc,
        ls_t134            TYPE ty_s_t134,
        "ls_tvarvc_ekorg    TYPE tvarvc,          "IS 2019/06
        lt_pir_ekorg       LIKE fu_t_pir_ekorg,             "IS 2019/06
        ls_tvarvc          TYPE tvarvc,
        ls_tvta            TYPE tvta,
        lv_werks           TYPE werks_d,
        ls_matgrp_prod     TYPE ty_s_matgrp_prod,
        lv_po_qty          TYPE zarn_num_base_units,
        lv_price_qty       TYPE zarn_num_base_units,
        lv_fraction        TYPE f,
        lv_nomi            TYPE dzaehl,
        lv_deno            TYPE nennr,
        lv_nr_scenario     TYPE flag,
        lv_tabix           TYPE sy-tabix,
*        lv_no_uom          TYPE flag,
        lv_uom_conv_err    TYPE flag,

        lt_host_sap        TYPE ty_t_host_sap,

        ls_bapi_clientext  TYPE zmd_s_bapi_clientext,
        ls_bapi_clientextx TYPE zmd_s_bapi_clientextx,
        ls_bapi_plantext   TYPE zmd_s_bapi_plantext,                    "++ONLD-822 JKH 12.01.2017
        ls_bapi_salesext   TYPE zmd_s_bapi_salesext,
        ls_bapi_salesextx  TYPE zmd_s_bapi_salesextx,
        lv_string          TYPE string,

        ls_calp_vb         TYPE calp_vb,

        BEGIN OF ls_data,
          field1 TYPE bapie1maraextrtx-field1,
          field2 TYPE bapie1maraextrtx-field2,
          field3 TYPE bapie1maraextrtx-field3,
          field4 TYPE bapie1maraextrtx-field4,
        END OF ls_data,

        BEGIN OF ls_data_sales,
          field1 TYPE bapie1mvkeextrtx-field1,
          field2 TYPE bapie1mvkeextrtx-field2,
          field3 TYPE bapie1mvkeextrtx-field3,
          field4 TYPE bapie1mvkeextrtx-field4,
        END OF ls_data_sales,

        BEGIN OF ls_data_import,
          field1 TYPE bapiparmatex-field1,
          field2 TYPE bapiparmatex-field2,
          field3 TYPE bapiparmatex-field3,
          field4 TYPE bapiparmatex-field4,
        END OF ls_data_import,

        ls_clientdata               TYPE bapie1marart,
        ls_clientdatax              TYPE bapie1marartx,
        ls_clientext                TYPE bapie1maraextrt,
        ls_clientextx               TYPE bapie1maraextrtx,
        ls_addnlclientdata          TYPE bapie1maw1rt,
        ls_addnlclientdatax         TYPE bapie1maw1rtx,
        ls_materialdescription      TYPE bapie1maktrt,
        ls_plantdata                TYPE bapie1marcrt,
        ls_plantdatax               TYPE bapie1marcrtx,
        ls_plantext                 TYPE bapie1marcextrt,                    "++ONLD-822 JKH 12.01.2017
        ls_plantextx                TYPE bapie1marcextrtx,                   "++ONLD-822 JKH 12.01.2017
        ls_internationalartnos      TYPE bapie1meanrt,
        ls_taxclassifications       TYPE bapie1mlanrt,
        ls_valuationdata            TYPE bapie1mbewrt,
        ls_valuationdatax           TYPE bapie1mbewrtx,
        ls_warehousenumberdata      TYPE bapie1mlgnrt,                      "++ONLD-822 JKH 12.01.2017
        ls_warehousenumberdatax     TYPE bapie1mlgnrtx,                     "++ONLD-822 JKH 12.01.2017
        ls_salesdata                TYPE bapie1mvkert,
        ls_salesdatax               TYPE bapie1mvkertx,
        ls_salesext                 TYPE bapie1mvkeextrt,
        ls_salesextx                TYPE bapie1mvkeextrtx,
        ls_posdata                  TYPE bapie1wlk2rt,
        ls_posdatax                 TYPE bapie1wlk2rtx,
        ls_plantkeys                TYPE bapie1wrkkey,
        ls_distrchainkeys           TYPE bapie1vtlkey,
        ls_valuationtypekeys        TYPE bapie1bwakey,
        ls_inforecord_general       TYPE bapieina,
        ls_inforecord_purchorg      TYPE wrfbapieine,
        ls_inforecord_purchorg_main TYPE wrfbapieine,
        ls_hierarchy_items          TYPE bapi_wrf_hier_change_items,
        ls_unitsofmeasure           TYPE bapie1marmrt,
        ls_unitsofmeasurex          TYPE bapie1marmrtx.


  FIELD-SYMBOLS: <ls_clientdata>          TYPE bapie1marart,
                 <ls_clientdatax>         TYPE bapie1marartx,
                 <ls_clientext>           TYPE bapie1maraextrt,
                 <ls_clientextx>          TYPE bapie1maraextrtx,
                 <ls_addnlclientdata>     TYPE bapie1maw1rt,
                 <ls_addnlclientdatax>    TYPE bapie1maw1rtx,
                 <ls_materialdescription> TYPE bapie1maktrt,
                 <ls_plantdata>           TYPE bapie1marcrt,
                 <ls_plantdatax>          TYPE bapie1marcrtx,
                 <ls_internationalartnos> TYPE bapie1meanrt,
                 <ls_taxclassifications>  TYPE bapie1mlanrt,
                 <ls_valuationdata>       TYPE bapie1mbewrt,
                 <ls_valuationdatax>      TYPE bapie1mbewrtx,
                 <ls_salesdata>           TYPE bapie1mvkert,
                 <ls_salesdatax>          TYPE bapie1mvkertx,
                 <ls_salesext>            TYPE bapie1mvkeextrt,
                 <ls_salesextx>           TYPE bapie1mvkeextrtx,
                 <ls_posdata>             TYPE bapie1wlk2rt,
                 <ls_posdatax>            TYPE bapie1wlk2rtx,
                 <ls_plantkeys>           TYPE bapie1wrkkey,
                 <ls_storagelocationkeys> TYPE bapie1lgokey,
                 <ls_distrchainkeys>      TYPE bapie1vtlkey,
                 <ls_valuationtypekeys>   TYPE bapie1bwakey,
                 <ls_importextension>     TYPE bapiparmatex,
                 <ls_inforecord_general>  TYPE bapieina,
                 <ls_inforecord_purchorg> TYPE wrfbapieine.

  IF <ls_clientdata>          IS ASSIGNED. UNASSIGN <ls_clientdata>.          ENDIF.
  IF <ls_clientdatax>         IS ASSIGNED. UNASSIGN <ls_clientdatax>.         ENDIF.
  IF <ls_clientext>           IS ASSIGNED. UNASSIGN <ls_clientext>.           ENDIF.
  IF <ls_clientextx>          IS ASSIGNED. UNASSIGN <ls_clientextx>.          ENDIF.
  IF <ls_addnlclientdata>     IS ASSIGNED. UNASSIGN <ls_addnlclientdata>.     ENDIF.
  IF <ls_addnlclientdatax>    IS ASSIGNED. UNASSIGN <ls_addnlclientdatax>.    ENDIF.
  IF <ls_materialdescription> IS ASSIGNED. UNASSIGN <ls_materialdescription>. ENDIF.
  IF <ls_plantdata>           IS ASSIGNED. UNASSIGN <ls_plantdata>.           ENDIF.
  IF <ls_plantdatax>          IS ASSIGNED. UNASSIGN <ls_plantdatax>.          ENDIF.
  IF <ls_internationalartnos> IS ASSIGNED. UNASSIGN <ls_internationalartnos>. ENDIF.
  IF <ls_taxclassifications>  IS ASSIGNED. UNASSIGN <ls_taxclassifications>.  ENDIF.
  IF <ls_valuationdata>       IS ASSIGNED. UNASSIGN <ls_valuationdata>.       ENDIF.
  IF <ls_valuationdatax>      IS ASSIGNED. UNASSIGN <ls_valuationdatax>.      ENDIF.
  IF <ls_salesdata>           IS ASSIGNED. UNASSIGN <ls_salesdata>.           ENDIF.
  IF <ls_salesdatax>          IS ASSIGNED. UNASSIGN <ls_salesdatax>.          ENDIF.
  IF <ls_salesext>            IS ASSIGNED. UNASSIGN <ls_salesext>.            ENDIF.
  IF <ls_salesextx>           IS ASSIGNED. UNASSIGN <ls_salesextx>.           ENDIF.
  IF <ls_posdata>             IS ASSIGNED. UNASSIGN <ls_posdata>.             ENDIF.
  IF <ls_posdatax>            IS ASSIGNED. UNASSIGN <ls_posdatax>.            ENDIF.
  IF <ls_plantkeys>           IS ASSIGNED. UNASSIGN <ls_plantkeys>.           ENDIF.
  IF <ls_storagelocationkeys> IS ASSIGNED. UNASSIGN <ls_storagelocationkeys>. ENDIF.
  IF <ls_distrchainkeys>      IS ASSIGNED. UNASSIGN <ls_distrchainkeys>.      ENDIF.
  IF <ls_valuationtypekeys>   IS ASSIGNED. UNASSIGN <ls_valuationtypekeys>.   ENDIF.
  IF <ls_importextension>     IS ASSIGNED. UNASSIGN <ls_importextension>.     ENDIF.
  IF <ls_inforecord_general>  IS ASSIGNED. UNASSIGN <ls_inforecord_general>.  ENDIF.
  IF <ls_inforecord_purchorg> IS ASSIGNED. UNASSIGN <ls_inforecord_purchorg>. ENDIF.

* Products
  CLEAR: ls_products.
  READ TABLE fu_s_prod_data-zarn_products[] INTO ls_products INDEX 1.

* Regional Header
  CLEAR: ls_reg_hdr.
  READ TABLE fu_s_reg_data-zarn_reg_hdr[] INTO ls_reg_hdr INDEX 1.

* MARA
  CLEAR ls_mara.
  READ TABLE fu_t_mara[] INTO ls_mara
  WITH KEY zzfan = ls_products-fan_id.

**********************************************************************************

*** CLIENTDATA


  CLEAR ls_clientdata_tmp.


* BASE_UOM
  CLEAR: ls_reg_uom, ls_t006.
  READ TABLE fu_s_reg_data-zarn_reg_uom[] INTO ls_reg_uom
  WITH KEY idno      = fu_s_reg_data-idno
           unit_base = abap_true.
  IF sy-subrc = 0.

    ls_clientdata_tmp-base_uom = ls_reg_uom-meinh.

* BASE_UOM_ISO
    READ TABLE fu_t_t006[] INTO ls_t006
    WITH TABLE KEY msehi = ls_reg_uom-meinh.
    IF sy-subrc = 0.
      ls_clientdata_tmp-base_uom_iso = ls_t006-isocode.
    ENDIF.
  ENDIF.


* PO_UNIT
  CLEAR: ls_reg_uom, ls_t006.
  LOOP AT  fu_s_reg_data-zarn_reg_uom[] INTO ls_reg_uom
    WHERE idno        EQ fu_s_reg_data-idno
      AND unit_purord EQ abap_true
      AND unit_base   NE abap_true.

    ls_clientdata_tmp-po_unit = ls_reg_uom-meinh.

* PO_UNIT_ISO
    READ TABLE fu_t_t006[] INTO ls_t006
    WITH TABLE KEY msehi = ls_reg_uom-meinh.
    IF sy-subrc = 0.
      ls_clientdata_tmp-po_unit_iso = ls_t006-isocode.
    ENDIF.

    EXIT.
  ENDLOOP.



* CONT_UNIT
  ls_clientdata_tmp-cont_unit = ls_reg_hdr-inhme.

* CONT_UNIT_ISO
  CLEAR: ls_t006.
  READ TABLE fu_t_t006[] INTO ls_t006
  WITH TABLE KEY msehi = ls_reg_hdr-inhme.
  IF sy-subrc = 0.
    ls_clientdata_tmp-cont_unit_iso = ls_t006-isocode.
  ENDIF.

* ITEM_CAT
  CLEAR ls_t134.
  READ TABLE fu_t_t134[] INTO ls_t134
  WITH TABLE KEY mtart = ls_reg_hdr-mtart.


*** CLIENTDATA
  READ TABLE fc_t_clientdata[] ASSIGNING <ls_clientdata>
  WITH KEY material = fc_s_headdata-material.
  IF sy-subrc = 0.
    <ls_clientdata>-function       = gc_bapi-func_005.    " '005'.
    <ls_clientdata>-material       = fc_s_headdata-material.

    IF fu_v_art_mode = 'C'.  " Create
      <ls_clientdata>-base_uom       = ls_clientdata_tmp-base_uom.
      <ls_clientdata>-base_uom_iso   = ls_clientdata_tmp-base_uom_iso.
      <ls_clientdata>-item_cat       = ls_t134-vmtpo.
    ENDIF.


    <ls_clientdata>-cont_unit      = ls_clientdata_tmp-cont_unit.
    <ls_clientdata>-cont_unit_iso  = ls_clientdata_tmp-cont_unit_iso.
    <ls_clientdata>-net_cont       = ls_reg_hdr-inhal.
    <ls_clientdata>-po_unit        = ls_clientdata_tmp-po_unit.
    <ls_clientdata>-po_unit_iso    = ls_clientdata_tmp-po_unit_iso.
    <ls_clientdata>-net_weight     = ls_reg_hdr-retail_net_weight.
    <ls_clientdata>-temp_conds     = ls_reg_hdr-storage_temp.
    <ls_clientdata>-season         = ls_reg_hdr-saiso.
    <ls_clientdata>-prod_hier      = ls_reg_hdr-prdha.
    <ls_clientdata>-minremlife     = ls_reg_hdr-mhdrz.
    <ls_clientdata>-saeson_yr      = ls_reg_hdr-saisj.
    <ls_clientdata>-pur_status     = ls_reg_hdr-mstae.
    <ls_clientdata>-sal_status     = ls_reg_hdr-mstav.
    <ls_clientdata>-pvalidfrom     = ls_reg_hdr-mstde.
    <ls_clientdata>-svalidfrom     = ls_reg_hdr-mstdv.
    <ls_clientdata>-creation_status = ls_reg_hdr-bstat.
    <ls_clientdata>-brand_id        = ls_reg_hdr-brand_id.

  ELSE.
    ls_clientdata-function       = gc_bapi-func_005.    " '005'.
    ls_clientdata-material       = fc_s_headdata-material.

    IF fu_v_art_mode = 'C'.  " Create
      ls_clientdata-base_uom       = ls_clientdata_tmp-base_uom.
      ls_clientdata-base_uom_iso   = ls_clientdata_tmp-base_uom_iso.
      ls_clientdata-item_cat       = ls_t134-vmtpo.
    ENDIF.

    ls_clientdata-cont_unit      = ls_clientdata_tmp-cont_unit.
    ls_clientdata-cont_unit_iso  = ls_clientdata_tmp-cont_unit_iso.
    ls_clientdata-net_cont       = ls_reg_hdr-inhal.
    ls_clientdata-po_unit        = ls_clientdata_tmp-po_unit.
    ls_clientdata-po_unit_iso    = ls_clientdata_tmp-po_unit_iso.
    ls_clientdata-net_weight     = ls_reg_hdr-retail_net_weight.
    ls_clientdata-temp_conds     = ls_reg_hdr-storage_temp.
    ls_clientdata-season         = ls_reg_hdr-saiso.
    ls_clientdata-prod_hier      = ls_reg_hdr-prdha.
    ls_clientdata-minremlife     = ls_reg_hdr-mhdrz.
    ls_clientdata-saeson_yr      = ls_reg_hdr-saisj.
    ls_clientdata-pur_status     = ls_reg_hdr-mstae.
    ls_clientdata-sal_status     = ls_reg_hdr-mstav.
    ls_clientdata-pvalidfrom     = ls_reg_hdr-mstde.
    ls_clientdata-svalidfrom     = ls_reg_hdr-mstdv.
    ls_clientdata-creation_status = ls_reg_hdr-bstat.
    ls_clientdata-brand_id        = ls_reg_hdr-brand_id.
    APPEND ls_clientdata TO fc_t_clientdata[].
  ENDIF.


*** CLIENTDATAX
  READ TABLE fc_t_clientdatax[] ASSIGNING <ls_clientdatax>
  WITH KEY material = fc_s_headdata-material.
  IF sy-subrc = 0.

    <ls_clientdatax>-function      = gc_bapi-func_005.    " '005'.
    <ls_clientdatax>-material      = fc_s_headdata-material.

    IF fu_v_art_mode = 'C'.  " Create
      <ls_clientdatax>-base_uom       = abap_true.
      <ls_clientdatax>-base_uom_iso   = abap_true.
      <ls_clientdatax>-item_cat       = abap_true.
    ENDIF.

    <ls_clientdatax>-cont_unit      = abap_true.
    <ls_clientdatax>-cont_unit_iso  = abap_true.
    <ls_clientdatax>-net_cont       = abap_true.
    <ls_clientdatax>-po_unit        = abap_true.
    <ls_clientdatax>-po_unit_iso    = abap_true.
    <ls_clientdatax>-net_weight     = abap_true.
    <ls_clientdatax>-temp_conds     = abap_true.
    <ls_clientdatax>-season         = abap_true.
    <ls_clientdatax>-prod_hier      = abap_true.
    <ls_clientdatax>-minremlife     = abap_true.
    <ls_clientdatax>-saeson_yr      = abap_true.
    <ls_clientdatax>-pur_status     = abap_true.
    <ls_clientdatax>-sal_status     = abap_true.
    <ls_clientdatax>-pvalidfrom     = abap_true.
    <ls_clientdatax>-svalidfrom     = abap_true.
    <ls_clientdatax>-creation_status = abap_true.
    <ls_clientdatax>-brand_id        = abap_true.
  ELSE.
    ls_clientdatax-function      = gc_bapi-func_005.    " '005'.
    ls_clientdatax-material      = fc_s_headdata-material.

    IF fu_v_art_mode = 'C'.  " Create
      ls_clientdatax-base_uom       = abap_true.
      ls_clientdatax-base_uom_iso   = abap_true.
      ls_clientdatax-item_cat       = abap_true.
    ENDIF.

    ls_clientdatax-cont_unit      = abap_true.
    ls_clientdatax-cont_unit_iso  = abap_true.
    ls_clientdatax-net_cont       = abap_true.
    ls_clientdatax-po_unit        = abap_true.
    ls_clientdatax-po_unit_iso    = abap_true.
    ls_clientdatax-net_weight     = abap_true.
    ls_clientdatax-temp_conds     = abap_true.
    ls_clientdatax-season         = abap_true.
    ls_clientdatax-prod_hier      = abap_true.
    ls_clientdatax-minremlife     = abap_true.
    ls_clientdatax-saeson_yr      = abap_true.
    ls_clientdatax-pur_status     = abap_true.
    ls_clientdatax-sal_status     = abap_true.
    ls_clientdatax-pvalidfrom     = abap_true.
    ls_clientdatax-svalidfrom     = abap_true.
    ls_clientdatax-creation_status = abap_true.
    ls_clientdatax-brand_id        = abap_true.
    APPEND ls_clientdatax TO fc_t_clientdatax[].
  ENDIF.


**********************************************************************************

*** CLIENTEXT


  ls_bapi_clientext  = fc_s_bapi_clientext.
  ls_bapi_clientextx = fc_s_bapi_clientextx.
  ls_bapi_clientext-function = gc_bapi-func_005.    " '005'.
  ls_bapi_clientext-material = fc_s_headdata-material.
  ls_bapi_clientext-zzprdtype      = ls_reg_hdr-zzprdtype.
*  ls_bapi_clientext-zzselling_only = ls_reg_hdr-zzselling_only.         "--ONED-217 JKH 24.11.2016
  ls_bapi_clientext-zzbuy_sell     = ls_reg_hdr-zzbuy_sell.              "++ONED-217 JKH 24.11.2016
  ls_bapi_clientext-zzas4subdept   = ls_reg_hdr-zzas4subdept.
  ls_bapi_clientext-zzvar_wt_flag  = ls_reg_hdr-zzvar_wt_flag.
  ls_bapi_clientext-zz_uni_dc      = ls_reg_hdr-zz_uni_dc.
  ls_bapi_clientext-zz_lni_dc      = ls_reg_hdr-zz_lni_dc.
  ls_bapi_clientext-zzsell         = ls_reg_hdr-zzsell.
  ls_bapi_clientext-zzuse          = ls_reg_hdr-zzuse.
  ls_bapi_clientext-zztktprnt      = ls_reg_hdr-zztktprnt.
  ls_bapi_clientext-zzpedesc_flag  = ls_reg_hdr-zzpedesc_flag.
  ls_bapi_clientext-zzattr1        = ls_reg_hdr-zzattr1.
  ls_bapi_clientext-zzattr2        = ls_reg_hdr-zzattr2.
  ls_bapi_clientext-zzattr3        = ls_reg_hdr-zzattr3.
  ls_bapi_clientext-zzattr4        = ls_reg_hdr-zzattr4.
  ls_bapi_clientext-zzattr5        = ls_reg_hdr-zzattr5.
  ls_bapi_clientext-zzattr6        = ls_reg_hdr-zzattr6.
  ls_bapi_clientext-zzattr7        = ls_reg_hdr-zzattr7.
  ls_bapi_clientext-zzattr8        = ls_reg_hdr-zzattr8.
  ls_bapi_clientext-zzattr9        = ls_reg_hdr-zzattr9.
  ls_bapi_clientext-zzattr10       = ls_reg_hdr-zzattr10.
  ls_bapi_clientext-zzattr11       = ls_reg_hdr-zzattr11.
  ls_bapi_clientext-zzattr12       = ls_reg_hdr-zzattr12.
  ls_bapi_clientext-zzattr13       = ls_reg_hdr-zzattr13.
  ls_bapi_clientext-zzattr14       = ls_reg_hdr-zzattr14.
  ls_bapi_clientext-zzattr15       = ls_reg_hdr-zzattr15.
  ls_bapi_clientext-zzattr16       = ls_reg_hdr-zzattr16.
  ls_bapi_clientext-zzattr17       = ls_reg_hdr-zzattr17.
  ls_bapi_clientext-zzattr18       = ls_reg_hdr-zzattr18.
  ls_bapi_clientext-zzattr19       = ls_reg_hdr-zzattr19.
  ls_bapi_clientext-zzattr20       = ls_reg_hdr-zzattr20.
  ls_bapi_clientext-zzattr21       = ls_reg_hdr-zzattr21.
  ls_bapi_clientext-zzattr22       = ls_reg_hdr-zzattr22.
  ls_bapi_clientext-zzattr23       = ls_reg_hdr-zzattr23.
  ls_bapi_clientext-zzstrd         = ls_reg_hdr-zzstrd.
  ls_bapi_clientext-zzsco_weighed_flag = ls_reg_hdr-zzsco_weighed_flag.
  ls_bapi_clientext-zzstrg         = ls_reg_hdr-zzstrg.
  ls_bapi_clientext-zzlabnum       = ls_reg_hdr-zzlabnum.
  ls_bapi_clientext-zzpkddt        = ls_reg_hdr-zzpkddt.
  ls_bapi_clientext-zzmandst       = ls_reg_hdr-zzmandst.
  ls_bapi_clientext-zzwnmsg        = ls_reg_hdr-zzwnmsg.
  ls_bapi_clientext-zzingretype    = ls_reg_hdr-zzingretype.
  ls_bapi_clientext-zzrange_flag   = ls_reg_hdr-zzrange_flag.
  ls_bapi_clientext-zzrange_avail  = ls_reg_hdr-zzrange_avail.

  IF ls_reg_hdr-zz_lni_dc = abap_true AND ls_mara-zzlni_repack IS INITIAL.

* Get Legacy UOM  - REPACK
    PERFORM get_legacy_uom USING fu_s_reg_data
                                 fu_t_uom_cat[]
                                 ls_reg_hdr-leg_repack
                        CHANGING ls_bapi_clientext-zzlni_repack.

* Get Legacy UOM  - BULK
    PERFORM get_legacy_uom USING fu_s_reg_data
                                 fu_t_uom_cat[]
                                 ls_reg_hdr-leg_bulk
                        CHANGING ls_bapi_clientext-zzlni_bulk.

* PR BOLY
    ls_bapi_clientext-zzlni_prboly = ls_reg_hdr-leg_prboly.
  ENDIF.

  ls_bapi_clientextx-function = gc_bapi-func_005.    " '005'.
  ls_bapi_clientextx-material = fc_s_headdata-material.

  ls_bapi_clientextx-zzprdtype          = abap_true.
*  ls_bapi_clientextx-zzselling_only     = abap_true.            "--ONED-217 JKH 24.11.2016
  ls_bapi_clientextx-zzbuy_sell         = abap_true.             "++ONED-217 JKH 24.11.2016
  ls_bapi_clientextx-zzas4subdept       = abap_true.
  ls_bapi_clientextx-zzvar_wt_flag      = abap_true.
  ls_bapi_clientextx-zz_uni_dc          = abap_true.
  ls_bapi_clientextx-zz_lni_dc          = abap_true.
  ls_bapi_clientextx-zzsell             = abap_true.
  ls_bapi_clientextx-zzuse              = abap_true.
  ls_bapi_clientextx-zztktprnt          = abap_true.
  ls_bapi_clientextx-zzpedesc_flag      = abap_true.
  ls_bapi_clientextx-zzattr1            = abap_true.
  ls_bapi_clientextx-zzattr2            = abap_true.
  ls_bapi_clientextx-zzattr3            = abap_true.
  ls_bapi_clientextx-zzattr4            = abap_true.
  ls_bapi_clientextx-zzattr5            = abap_true.
  ls_bapi_clientextx-zzattr6            = abap_true.
  ls_bapi_clientextx-zzattr7            = abap_true.
  ls_bapi_clientextx-zzattr8            = abap_true.
  ls_bapi_clientextx-zzattr9            = abap_true.
  ls_bapi_clientextx-zzattr10           = abap_true.
  ls_bapi_clientextx-zzattr11           = abap_true.
  ls_bapi_clientextx-zzattr12           = abap_true.
  ls_bapi_clientextx-zzattr13           = abap_true.
  ls_bapi_clientextx-zzattr14           = abap_true.
  ls_bapi_clientextx-zzattr15           = abap_true.
  ls_bapi_clientextx-zzattr16           = abap_true.
  ls_bapi_clientextx-zzattr17           = abap_true.
  ls_bapi_clientextx-zzattr18           = abap_true.
  ls_bapi_clientextx-zzattr19           = abap_true.
  ls_bapi_clientextx-zzattr20           = abap_true.
  ls_bapi_clientextx-zzattr21           = abap_true.
  ls_bapi_clientextx-zzattr22           = abap_true.
  ls_bapi_clientextx-zzattr23           = abap_true.
  ls_bapi_clientextx-zzstrd             = abap_true.
  ls_bapi_clientextx-zzsco_weighed_flag = abap_true.
  ls_bapi_clientextx-zzstrg             = abap_true.
  ls_bapi_clientextx-zzlabnum           = abap_true.
  ls_bapi_clientextx-zzpkddt            = abap_true.
  ls_bapi_clientextx-zzmandst           = abap_true.
  ls_bapi_clientextx-zzwnmsg            = abap_true.
  ls_bapi_clientextx-zzingretype        = abap_true.
  ls_bapi_clientextx-zzrange_flag       = abap_true.
  ls_bapi_clientextx-zzrange_avail      = abap_true.

  IF ls_reg_hdr-zz_lni_dc = abap_true AND ls_mara-zzlni_repack IS INITIAL.
    ls_bapi_clientextx-zzlni_repack = abap_true.
    ls_bapi_clientextx-zzlni_bulk   = abap_true.
    ls_bapi_clientextx-zzlni_prboly = abap_true.
  ENDIF.

  fc_s_bapi_clientext  = ls_bapi_clientext.
  fc_s_bapi_clientextx = ls_bapi_clientextx.

  CLEAR lv_string.
  cl_abap_container_utilities=>fill_container_c(
    EXPORTING im_value = ls_bapi_clientext-data
    IMPORTING ex_container = lv_string ).

  CLEAR ls_data.
  ls_data-field1(30) = 'ZMD_S_BAPI_CLIENTEXT'.
  ls_data+30 = lv_string.

*** CLIENTEXT
  READ TABLE fc_t_clientext[] ASSIGNING <ls_clientext>
  WITH KEY material = fc_s_headdata-material.
  IF sy-subrc = 0.
    <ls_clientext>-function = gc_bapi-func_005.    " '005'.
    <ls_clientext>-material = fc_s_headdata-material.
    <ls_clientext>-field1   = ls_data-field1.
    <ls_clientext>-field2   = ls_data-field2.
    <ls_clientext>-field3   = ls_data-field3.
    <ls_clientext>-field4   = ls_data-field4.
  ELSE.
    ls_clientext-function = gc_bapi-func_005.    " '005'.
    ls_clientext-material = fc_s_headdata-material.
    ls_clientext-field1   = ls_data-field1.
    ls_clientext-field2   = ls_data-field2.
    ls_clientext-field3   = ls_data-field3.
    ls_clientext-field4   = ls_data-field4.
    APPEND ls_clientext TO fc_t_clientext[].
  ENDIF.


  CLEAR lv_string.
  cl_abap_container_utilities=>fill_container_c(
    EXPORTING im_value = ls_bapi_clientextx-datax
    IMPORTING ex_container = lv_string ).

  CLEAR ls_data.
  ls_data-field1(30) = 'ZMD_S_BAPI_CLIENTEXTX'.
  ls_data+30 = lv_string.

*** CLIENTEXTX
  READ TABLE fc_t_clientextx[] ASSIGNING <ls_clientextx>
  WITH KEY material = fc_s_headdata-material.
  IF sy-subrc = 0.
    <ls_clientextx>-function = gc_bapi-func_005.    " '005'.
    <ls_clientextx>-material = fc_s_headdata-material.
    <ls_clientextx>-field1   = ls_data-field1.
    <ls_clientextx>-field2   = ls_data-field2.
    <ls_clientextx>-field3   = ls_data-field3.
    <ls_clientextx>-field4   = ls_data-field4.
  ELSE.
    ls_clientextx-function = gc_bapi-func_005.    " '005'.
    ls_clientextx-material = fc_s_headdata-material.
    ls_clientextx-field1   = ls_data-field1.
    ls_clientextx-field2   = ls_data-field2.
    ls_clientextx-field3   = ls_data-field3.
    ls_clientextx-field4   = ls_data-field4.
    APPEND ls_clientextx TO fc_t_clientextx[].
  ENDIF.


**********************************************************************************

*** ADDNLCLIENTDATA


* SALES_UNIT
  CLEAR ls_reg_uom.
  LOOP AT fu_s_reg_data-zarn_reg_uom[] INTO ls_reg_uom
  WHERE idno       EQ fu_s_reg_data-idno
    AND sales_unit EQ abap_true
    AND unit_base  NE abap_true.
    EXIT.
  ENDLOOP.

  IF ls_reg_uom IS NOT INITIAL.
    ls_addnlclientdata-sales_unit = ls_reg_uom-meinh.

* SALES_UNIT_ISO
    READ TABLE fu_t_t006[] INTO ls_t006
    WITH TABLE KEY msehi = ls_reg_uom-meinh.
    IF sy-subrc = 0.
      ls_addnlclientdata-sales_unit_iso = ls_t006-isocode.
    ENDIF.
  ENDIF.

* ISSUE_UNIT
  CLEAR ls_reg_uom.
  LOOP AT fu_s_reg_data-zarn_reg_uom[] INTO ls_reg_uom
  WHERE idno       EQ fu_s_reg_data-idno
    AND issue_unit EQ abap_true
    AND unit_base  NE abap_true.
    EXIT.
  ENDLOOP.

  IF ls_reg_uom IS NOT INITIAL.
    ls_addnlclientdata-issue_unit = ls_reg_uom-meinh.

* ISSUE_UNIT_ISO
    READ TABLE fu_t_t006[] INTO ls_t006
    WITH TABLE KEY msehi = ls_reg_uom-meinh.
    IF sy-subrc = 0.
      ls_addnlclientdata-issue_unit_iso = ls_t006-isocode.
    ENDIF.
  ENDIF.

*** ADDNLCLIENTDATA
  READ TABLE fc_t_addnlclientdata[] ASSIGNING <ls_addnlclientdata>
  WITH KEY material = fc_s_headdata-material.
  IF sy-subrc = 0.
    <ls_addnlclientdata>-function       = gc_bapi-func_005.    " '005'.
    <ls_addnlclientdata>-material       = fc_s_headdata-material.
    <ls_addnlclientdata>-sales_unit     = ls_addnlclientdata-sales_unit.
    <ls_addnlclientdata>-sales_unit_iso = ls_addnlclientdata-sales_unit_iso.
    <ls_addnlclientdata>-issue_unit     = ls_addnlclientdata-issue_unit.
    <ls_addnlclientdata>-issue_unit_iso = ls_addnlclientdata-issue_unit_iso.
  ELSE.
    ls_addnlclientdata-function       = gc_bapi-func_005.    " '005'.
    ls_addnlclientdata-material       = fc_s_headdata-material.

    APPEND ls_addnlclientdata TO fc_t_addnlclientdata[].
  ENDIF.

*** ADDNLCLIENTDATAX
  READ TABLE fc_t_addnlclientdatax[] ASSIGNING <ls_addnlclientdatax>
  WITH KEY material = fc_s_headdata-material.
  IF sy-subrc = 0.
    <ls_addnlclientdatax>-function       = gc_bapi-func_005.    " '005'.
    <ls_addnlclientdatax>-material       = fc_s_headdata-material.
    <ls_addnlclientdatax>-sales_unit     = abap_true.
    <ls_addnlclientdatax>-sales_unit_iso = abap_true.
    <ls_addnlclientdatax>-issue_unit     = abap_true.
    <ls_addnlclientdatax>-issue_unit_iso = abap_true.
  ELSE.
    ls_addnlclientdatax-function       = gc_bapi-func_005.    " '005'.
    ls_addnlclientdatax-material       = fc_s_headdata-material.
    ls_addnlclientdatax-sales_unit     = abap_true.
    ls_addnlclientdatax-sales_unit_iso = abap_true.
    ls_addnlclientdatax-issue_unit     = abap_true.
    ls_addnlclientdatax-issue_unit_iso = abap_true.
    APPEND ls_addnlclientdatax TO fc_t_addnlclientdatax[].
  ENDIF.


**********************************************************************************

*** MATERIALDESCRIPTION
  READ TABLE fc_t_materialdescription[] ASSIGNING <ls_materialdescription>
  WITH KEY material = fc_s_headdata-material.
  IF sy-subrc = 0.
    <ls_materialdescription>-function  = gc_bapi-func_005.    " '005'.
    <ls_materialdescription>-material  = fc_s_headdata-material.
    <ls_materialdescription>-langu     = sy-langu.
    <ls_materialdescription>-langu_iso = sy-langu.
    <ls_materialdescription>-matl_desc = ls_reg_hdr-retail_unit_desc.

  ELSE.
    ls_materialdescription-function  = gc_bapi-func_005.    " '005'.
    ls_materialdescription-material  = fc_s_headdata-material.
    ls_materialdescription-langu     = sy-langu.
    ls_materialdescription-langu_iso = sy-langu.
    ls_materialdescription-matl_desc = ls_reg_hdr-retail_unit_desc.

    APPEND ls_materialdescription TO fc_t_materialdescription[].
  ENDIF.

**********************************************************************************

*** PLANTDATA
*** PLANTDATAX

* Fill Plant Data - RFST
  CLEAR: ls_plantdata, ls_plantdatax.
  PERFORM fill_plantdata USING gc_bapi-func_005    " '005'
                               fc_s_headdata-material
                               gc_banner-rfst   " 'RFST'
                               ls_reg_hdr-bwscl
                               fu_v_art_mode
                      CHANGING ls_plantdata
                               ls_plantdatax.

  APPEND ls_plantdata TO fc_t_plantdata[].
  APPEND ls_plantdatax TO fc_t_plantdatax[].

  LOOP AT fu_t_tvarvc INTO ls_tvarvc.

    CLEAR lv_werks.
    lv_werks = ls_tvarvc-high.

    CLEAR: ls_plantdata, ls_plantdatax.

    CLEAR ls_reg_banner.
    READ TABLE fu_s_reg_data-zarn_reg_banner[] INTO ls_reg_banner
    WITH KEY idno   = fu_s_reg_data-idno
             banner = ls_tvarvc-low.
    IF sy-subrc = 0.
      CASE ls_tvarvc-name.
        WHEN gc_tvarvc-vkorg_uni.  " ZC_REF_VKORG_VTREFS
* Fill Plant Data - RFXX
          PERFORM fill_plantdata USING gc_bapi-func_005    " '005'
                                       fc_s_headdata-material
                                       lv_werks
                                       ls_reg_banner-source_uni
                                       fu_v_art_mode
                              CHANGING ls_plantdata
                                       ls_plantdatax.

          APPEND ls_plantdata TO fc_t_plantdata[].
          APPEND ls_plantdatax TO fc_t_plantdatax[].

** Fill Plant Ext - RFXX
          PERFORM fill_plantext  USING gc_bapi-func_005    " '005'
                                       fc_s_headdata-material
                                       lv_werks
                                       ls_reg_banner
                                       fu_v_art_mode
                              CHANGING ls_plantext
                                       ls_plantextx
                                       ls_bapi_plantext.

          APPEND ls_plantext  TO fc_t_plantext[].
          APPEND ls_plantextx TO fc_t_plantextx[].
          APPEND ls_bapi_plantext TO fc_t_bapi_plantext[].
* INS End of Change ONLD-822 JKH 12.01.2017



        WHEN gc_tvarvc-vkorg_lni.  " ZMD_VKORG_REF_SITES_LNI
* Fill Plant Data - RLXX
          PERFORM fill_plantdata USING gc_bapi-func_005    " '005'
                                       fc_s_headdata-material
                                       lv_werks
                                       ls_reg_banner-source_lni
                                       fu_v_art_mode
                              CHANGING ls_plantdata
                                       ls_plantdatax.

          APPEND ls_plantdata TO fc_t_plantdata[].
          APPEND ls_plantdatax TO fc_t_plantdatax[].

** Fill Plant Ext - RLXX
          PERFORM fill_plantext  USING gc_bapi-func_005    " '005'
                                       fc_s_headdata-material
                                       lv_werks
                                       ls_reg_banner
                                       fu_v_art_mode
                              CHANGING ls_plantext
                                       ls_plantextx
                                       ls_bapi_plantext.

          APPEND ls_plantext  TO fc_t_plantext[].
          APPEND ls_plantextx TO fc_t_plantextx[].
          APPEND ls_bapi_plantext TO fc_t_bapi_plantext[].
* INS End of Change ONLD-822 JKH 17.02.2017

      ENDCASE.
    ENDIF.  " READ TABLE fu_s_reg_data-zarn_reg_banner[] INTO ls_reg_banner
  ENDLOOP.  " LOOP AT fu_t_tvarvc INTO ls_tvarvc

* RFDC - if not created
  CLEAR ls_plantdata.
  READ TABLE fc_t_plantdata[] INTO ls_plantdata
  WITH KEY material = fc_s_headdata-material
           plant    = gc_banner-rfdc.   " 'RFDC'.
  IF sy-subrc NE 0.

    CLEAR ls_reg_banner.
    READ TABLE fu_s_reg_data-zarn_reg_banner[] INTO ls_reg_banner
    WITH KEY idno   = fu_s_reg_data-idno
             banner = gc_banner-1000.                       " '1000'.


* Fill Plant Data - RFST
    CLEAR: ls_plantdata, ls_plantdatax.
    PERFORM fill_plantdata USING gc_bapi-func_005    " '005'
                                 fc_s_headdata-material
                                 gc_banner-rfdc   " 'RFDC'
                                 ls_reg_banner-source_uni
                                 fu_v_art_mode
                        CHANGING ls_plantdata
                                 ls_plantdatax.

    APPEND ls_plantdata TO fc_t_plantdata[].
    APPEND ls_plantdatax TO fc_t_plantdatax[].

  ENDIF.


**********************************************************************************

*** SALESDATA


  LOOP AT fu_s_reg_data-zarn_reg_banner[] INTO ls_reg_banner
                       WHERE banner IS NOT INITIAL.

    LOOP AT fu_t_tvta INTO ls_tvta WHERE vkorg+0(1) = ls_reg_banner-banner+0(1).


*** SALESDATA
      CLEAR: ls_salesdata, ls_salesdatax.
      ls_salesdata-function   = gc_bapi-func_005.    " '005'
      ls_salesdata-material   = fc_s_headdata-material.
      ls_salesdata-sales_org  = ls_tvta-vkorg.
      ls_salesdata-distr_chan = ls_tvta-vtweg.

      IF fu_v_art_mode = 'C'.  " Create
        DATA(lv_new_mvke) = abap_true.
      ELSE.
        SELECT COUNT( * )  FROM mvke INTO sy-dbcnt   "#EC CI_SEL_NESTED
          WHERE matnr = ls_salesdata-material
          AND   vkorg = ls_salesdata-sales_org
          AND   vtweg = ls_salesdata-distr_chan.
        IF sy-dbcnt = 0.
          lv_new_mvke = abap_true.
        ENDIF.
      ENDIF.

      ls_salesdata-matl_stats = ls_reg_banner-versg.
      ls_salesdata-cash_disc  = abap_true.
*      ls_salesdata-sal_status = ls_reg_banner-vmsta.  "--IR5047357 JKH 16.08.2016
*      ls_salesdata-valid_from = ls_reg_banner-vmstd.  "--IR5047357 JKH 16.08.2016

      IF ls_tvta-vkorg = gc_banner-1000 AND ls_tvta-vtweg = '10'.
        ls_salesdata-min_order = ls_reg_banner-aumng.
      ELSEIF ls_tvta-vkorg NE gc_banner-1000.
        ls_salesdata-min_order = ls_reg_banner-aumng.
      ENDIF.

      ls_salesdata-sales_unit = ls_reg_banner-vrkme.


* SALES_UNIT_ISO
      CLEAR ls_t006.
      READ TABLE fu_t_t006[] INTO ls_t006
      WITH TABLE KEY msehi = ls_reg_banner-vrkme.
      IF sy-subrc = 0.
        ls_salesdata-sales_unit_iso = ls_t006-isocode.
      ENDIF.

* ITEM_CAT
      IF lv_new_mvke = abap_true.
        CLEAR ls_t134.
        READ TABLE fu_t_t134[] INTO ls_t134
        WITH TABLE KEY mtart = ls_reg_hdr-mtart.
        IF sy-subrc = 0.
          ls_salesdata-item_cat = ls_t134-vmtpo.
        ENDIF.
      ENDIF.

      ls_salesdata-prod_hier  = ls_reg_banner-prodh.
      ls_salesdata-acct_assgt = ls_reg_banner-ktgrm.
      ls_salesdata-assort_lev = ls_reg_banner-sstuf.

*      IF fu_v_art_mode = 'C'.  " Create
      IF lv_new_mvke = abap_true.
        ls_salesdata-mat_pr_grp = '01'.
        ls_salesdata-li_proc_st = 'B1'.
        ls_salesdata-li_proc_dc = 'B1'.
        ls_salesdata-list_st_fr = sy-datum.
        ls_salesdata-list_st_to = '99991231'.
        ls_salesdata-list_dc_fr = sy-datum.
        ls_salesdata-list_dc_to = '99991231'.
        ls_salesdata-sell_st_fr = sy-datum.
        ls_salesdata-sell_st_to = '99991231'.
        ls_salesdata-sell_dc_fr = sy-datum.
        ls_salesdata-sell_dc_to = '99991231'.
      ENDIF.

      APPEND ls_salesdata TO fc_t_salesdata[].

*** SALESDATAX
      ls_salesdatax-function   = gc_bapi-func_005.    " '005'
      ls_salesdatax-material   = fc_s_headdata-material.
      ls_salesdatax-sales_org  = ls_tvta-vkorg.
      ls_salesdatax-distr_chan = ls_tvta-vtweg.

      ls_salesdatax-matl_stats = abap_true.
      ls_salesdatax-cash_disc  = abap_true.
*      ls_salesdatax-sal_status = abap_true.  "--IR5047357 JKH 16.08.2016
*      ls_salesdatax-valid_from = abap_true.  "--IR5047357 JKH 16.08.2016

      IF ls_tvta-vkorg = gc_banner-1000 AND ls_tvta-vtweg = '10'.
        ls_salesdatax-min_order = abap_true.
      ELSEIF ls_tvta-vkorg NE gc_banner-1000.
        ls_salesdatax-min_order = abap_true.
      ENDIF.

      ls_salesdatax-sales_unit     = abap_true.
      ls_salesdatax-sales_unit_iso = abap_true.
      IF lv_new_mvke = abap_true.
        ls_salesdatax-item_cat       = abap_true.
      ENDIF.
      ls_salesdatax-prod_hier      = abap_true.
      ls_salesdatax-acct_assgt     = abap_true.
      ls_salesdatax-assort_lev     = abap_true.


*      IF fu_v_art_mode = 'C'.  " Create
      IF lv_new_mvke = abap_true.
        ls_salesdatax-mat_pr_grp = abap_true.
        ls_salesdatax-li_proc_st = abap_true.
        ls_salesdatax-li_proc_dc = abap_true.
        ls_salesdatax-list_st_fr = abap_true.
        ls_salesdatax-list_st_to = abap_true.
        ls_salesdatax-list_dc_fr = abap_true.
        ls_salesdatax-list_dc_to = abap_true.
        ls_salesdatax-sell_st_fr = abap_true.
        ls_salesdatax-sell_st_to = abap_true.
        ls_salesdatax-sell_dc_fr = abap_true.
        ls_salesdatax-sell_dc_to = abap_true.
      ENDIF.

      APPEND ls_salesdatax TO fc_t_salesdatax[].

**********************************************************************************

*** SALESEXT
      CLEAR: ls_bapi_salesext.
      ls_bapi_salesext-function   = gc_bapi-func_005.    " '005'
      ls_bapi_salesext-material   = fc_s_headdata-material.
      ls_bapi_salesext-sales_org  = ls_tvta-vkorg.
      ls_bapi_salesext-distr_chan = ls_tvta-vtweg.

      ls_bapi_salesext-zzcatman = ls_reg_banner-zzcatman.

      fc_s_bapi_salesext = ls_bapi_salesext.

      APPEND fc_s_bapi_salesext TO fc_t_bapi_salesext[].

      CLEAR lv_string.
      cl_abap_container_utilities=>fill_container_c(
        EXPORTING im_value = ls_bapi_salesext-data
        IMPORTING ex_container = lv_string ).

      CLEAR ls_data_sales.
      ls_data_sales-field1(30) = 'ZMD_S_BAPI_SALESEXT'.
      ls_data_sales+30 = lv_string.

      CLEAR ls_salesext.
      ls_salesext-function   = gc_bapi-func_005.    " gc_bapi-func_005.    " '005'.
      ls_salesext-material   = fc_s_headdata-material.
      ls_salesext-sales_org  = ls_tvta-vkorg.
      ls_salesext-distr_chan = ls_tvta-vtweg.
      ls_salesext-field1     = ls_data_sales-field1.
      ls_salesext-field2     = ls_data_sales-field2.
      ls_salesext-field3     = ls_data_sales-field3.
      ls_salesext-field4     = ls_data_sales-field4.

      APPEND ls_salesext TO fc_t_salesext[].


*** SALESEXTX
      CLEAR: ls_bapi_salesextx.
      ls_bapi_salesextx-function   = gc_bapi-func_005.    " '005'
      ls_bapi_salesextx-material   = fc_s_headdata-material.
      ls_bapi_salesextx-sales_org  = ls_tvta-vkorg.
      ls_bapi_salesextx-distr_chan = ls_tvta-vtweg.

      ls_bapi_salesextx-zzcatman = abap_true.

      fc_s_bapi_salesextx = ls_bapi_salesextx.


      CLEAR lv_string.
      cl_abap_container_utilities=>fill_container_c(
        EXPORTING im_value = ls_bapi_salesextx-datax
        IMPORTING ex_container = lv_string ).

      CLEAR ls_data_sales.
      ls_data_sales-field1(30) = 'ZMD_S_BAPI_SALESEXTX'.
      ls_data_sales+30 = lv_string.

      CLEAR ls_salesextx.
      ls_salesextx-function   = gc_bapi-func_005.    " gc_bapi-func_005.    " '005'.
      ls_salesextx-material   = fc_s_headdata-material.
      ls_salesextx-sales_org  = ls_tvta-vkorg.
      ls_salesextx-distr_chan = ls_tvta-vtweg.
      ls_salesextx-field1     = ls_data_sales-field1.
      ls_salesextx-field2     = ls_data_sales-field2.
      ls_salesextx-field3     = ls_data_sales-field3.
      ls_salesextx-field4     = ls_data_sales-field4.

      APPEND ls_salesextx TO fc_t_salesextx[].


**********************************************************************************

*** POSDATA
      CLEAR: ls_posdata, ls_posdatax.
      ls_posdata-function     = gc_bapi-func_005.    " '005'
      ls_posdata-material     = fc_s_headdata-material.
      ls_posdata-sales_org    = ls_tvta-vkorg.
      ls_posdata-distr_chan   = ls_tvta-vtweg.

      ls_posdata-no_rep_key   = ls_reg_banner-qty_required.
      ls_posdata-price_reqd   = ls_reg_banner-prerf.
*      ls_posdata-sal_status   = ls_reg_banner-vmsta.  "--IR5047357 JKH 16.08.2016
*      ls_posdata-valid_from   = ls_reg_banner-vmstd.  "--IR5047357 JKH 16.08.2016
      ls_posdata-disc_allwd   = ls_reg_banner-pns_tpr_flag.
      ls_posdata-scales_grp   = ls_reg_banner-scagr.

      IF fu_v_art_mode = 'C'.  " Create
        ls_posdata-sell_st_fr = sy-datum.
        ls_posdata-sell_st_to = '99991231'.
      ENDIF.
      APPEND ls_posdata TO fc_t_posdata[].


*** POSDATAX
      ls_posdatax-function     = gc_bapi-func_005.    " '005'
      ls_posdatax-material     = fc_s_headdata-material.
      ls_posdatax-sales_org    = ls_tvta-vkorg.
      ls_posdatax-distr_chan   = ls_tvta-vtweg.

      ls_posdatax-no_rep_key   = abap_true.
      ls_posdatax-price_reqd   = abap_true.
*      ls_posdatax-sal_status   = abap_true.  "--IR5047357 JKH 16.08.2016
*      ls_posdatax-valid_from   = abap_true.  "--IR5047357 JKH 16.08.2016
      ls_posdatax-disc_allwd   = abap_true.
      ls_posdatax-scales_grp   = abap_true.

      IF fu_v_art_mode = 'C'.  " Create
        ls_posdatax-sell_st_fr = abap_true.
        ls_posdatax-sell_st_to = abap_true.
      ENDIF.
      APPEND ls_posdatax TO fc_t_posdatax[].

**********************************************************************************

*** DISTRCHAINKEYS
      CLEAR ls_distrchainkeys.
      ls_distrchainkeys-function   = gc_bapi-func_005.    " '005'
      ls_distrchainkeys-material   = fc_s_headdata-material.
      ls_distrchainkeys-sales_org  = ls_tvta-vkorg.
      ls_distrchainkeys-distr_chan = ls_tvta-vtweg.

      APPEND ls_distrchainkeys TO fc_t_distrchainkeys[].



    ENDLOOP.  " LOOP AT fu_t_tvta INTO ls_tvta.
  ENDLOOP.  " LOOP AT fu_s_reg_data-zarn_reg_banner[] INTO ls_reg_banner.



**********************************************************************************

*** MATERIAL LONGTEXT

  DATA: lt_tlines TYPE tline_t.
  DATA(lv_range_det_str) = VALUE #( fu_s_reg_data-zarn_reg_str[ 1 ]-range_detail OPTIONAL ).

  IF lv_range_det_str IS NOT INITIAL.

    CALL FUNCTION 'VB_CP_CONVERT_STRING_2_ITF'
      EXPORTING
        i_string = lv_range_det_str
      TABLES
        et_table = lt_tlines.

    fc_t_materiallongtext = VALUE #( FOR ls_tline IN lt_tlines ( function =  gc_bapi-func_005
                                                                 applobject = gc_bapi-material
                                                                 text_name = fc_s_headdata-material
                                                                 text_id = gc_bapi-basictxt
                                                                 langu = sy-langu
                                                                "langu_iso = ls_t002-laiso
                                                                 format_col = ls_tline-tdformat
                                                                 text_line = ls_tline-tdline ) ).
  ELSE.
    fc_t_materiallongtext = VALUE #( ( function =  gc_bapi-func_005
                                       applobject = gc_bapi-material
                                       text_name = fc_s_headdata-material
                                       text_id = gc_bapi-basictxt
                                       langu = sy-langu
                                      "langu_iso = ls_t002-laiso
                                       format_col = '*'
                                       text_line = abap_false ) ).

  ENDIF.

**********************************************************************************

* Maintain Entries for Each MARC Created/Updated
  LOOP AT fc_t_plantdata[] INTO ls_plantdata.


*** VALUATIONDATA
    CLEAR ls_valuationdata.
    ls_valuationdata-function   = gc_bapi-func_005.    " '005'
    ls_valuationdata-material   = fc_s_headdata-material.
    ls_valuationdata-val_area   = ls_plantdata-plant.
    ls_valuationdata-val_type   = space.
*  ls_valuationdata-price_ctrl =
*  ls_valuationdata-price_unit =
*  ls_valuationdata-val_class  =
    APPEND ls_valuationdata TO fc_t_valuationdata[].


*** VALUATIONDATAX
    CLEAR ls_valuationdatax.
    ls_valuationdatax-function   = gc_bapi-func_005.    " '005'
    ls_valuationdatax-material   = fc_s_headdata-material.
    ls_valuationdatax-val_area   = ls_plantdata-plant.
    ls_valuationdatax-val_type   = space.
*  ls_valuationdatax-price_ctrl = abap_true.
*  ls_valuationdatax-price_unit = abap_true.
*  ls_valuationdatax-val_class  = abap_true.
    APPEND ls_valuationdatax TO fc_t_valuationdatax[].

**********************************************************************************

*** PLANTKEYS
    CLEAR ls_plantkeys.
    ls_plantkeys-function   = gc_bapi-func_005.    " '005'
    ls_plantkeys-material   = fc_s_headdata-material.
    ls_plantkeys-plant      = ls_plantdata-plant.
    APPEND ls_plantkeys TO fc_t_plantkeys[].

**********************************************************************************

*** VALUATIONTYPEKEYS
    CLEAR ls_valuationtypekeys.
    ls_valuationtypekeys-function   = gc_bapi-func_005.    " '005'
    ls_valuationtypekeys-material   = fc_s_headdata-material.
    ls_valuationtypekeys-val_area   = ls_plantdata-plant.
    ls_valuationtypekeys-val_type   = space.

    APPEND ls_valuationtypekeys TO fc_t_valuationtypekeys[].


  ENDLOOP.   " LOOP AT fc_t_plantdata[] INTO ls_plantdata.
**********************************************************************************

* INS Begin of Change ONLD-822 JKH 12.01.2017

*** WAREHOUSENUMBERDATA
*** WAREHOUSENUMBERDATAX

* If any of the ARENA’s Banner has the value 1 then system will extend the
* Article Master Warehouse Management View for a 'RFDC' to
* Warehouse Number '900' Foodstuffs Online (LeanWM).
* Table MLGN-MATNR will be equal to the Article marked as Online and
* MLGN-LGNUM equal to '900'.
  CLEAR ls_reg_banner.
  READ TABLE fu_s_reg_data-zarn_reg_banner[] INTO ls_reg_banner
  WITH KEY idno   = fu_s_reg_data-idno
           online_status = '01'.   " Ranged for Online
  IF sy-subrc = 0.

    CLEAR ls_warehousenumberdata.
    ls_warehousenumberdata-function = gc_bapi-func_005.    " '005'
    ls_warehousenumberdata-material = fc_s_headdata-material.
    ls_warehousenumberdata-whse_no  = '900'.

    APPEND ls_warehousenumberdata TO fc_t_warehousenumberdata[].

    CLEAR ls_warehousenumberdatax.
    ls_warehousenumberdatax-function = gc_bapi-func_005.    " '005'
    ls_warehousenumberdatax-material = fc_s_headdata-material.
    ls_warehousenumberdatax-whse_no  = '900'.

    APPEND ls_warehousenumberdatax TO fc_t_warehousenumberdatax[].

  ENDIF.
* INS End of Change ONLD-822 JKH 12.01.2017

**********************************************************************************
*** IMPORTEXTENSION

  CLEAR: lt_host_sap[].
* Build Host Data in IMPORTEXTENSION - Create Product
  PERFORM build_importextension_create USING fu_s_idno_data
                                             fu_s_prod_data
                                             fu_s_reg_data
                                             fu_v_art_mode
                                             fc_s_headdata
                                             fu_t_marm[]
                                             fu_t_uom_cat[]
                                             fu_t_host_sap[]
                                    CHANGING fc_t_importextension[]
                                             lt_host_sap[]
                                             fc_t_legacy_struct[].


* Build Host Data in IMPORTEXTENSION - Update Product
  PERFORM build_importextension_update USING fu_s_idno_data
                                             fu_s_prod_data
                                             fu_s_reg_data
                                             fu_v_art_mode
                                             fc_s_headdata
                                             "fu_t_maw1[]
                                             lt_host_sap[]
                                             fc_t_addnlclientdata[]
                                    CHANGING fc_t_importextension[]
                                             fc_t_legacy_struct[].






**********************************************************************************

*** TAXCLASSIFICATIONS

  IF fu_v_art_mode = 'C'.  " Create

    CLEAR ls_taxclassifications.
    ls_taxclassifications-function   = gc_bapi-func_005.    " '005'
    ls_taxclassifications-material   = fc_s_headdata-material.

    APPEND ls_taxclassifications TO fc_t_taxclassifications[].
  ENDIF.

**********************************************************************************

*** HIERARCHY_DATA

  IF ls_reg_hdr-article_hierarchy IS NOT INITIAL.

    CLEAR ls_hierarchy_items.
    ls_hierarchy_items-matnr     = fc_s_headdata-material.
    ls_hierarchy_items-node      = ls_reg_hdr-article_hierarchy.
    ls_hierarchy_items-date_from = sy-datum.
    ls_hierarchy_items-date_to   = '99991231'.
    ls_hierarchy_items-mainflg   = abap_true.
    ls_hierarchy_items-change    = 'I'.

    IF fu_v_art_mode = 'C'.   " Create
* For Article Creation, create article hierarchy
      APPEND ls_hierarchy_items TO fc_t_hierarchy_items[].

    ELSEIF fu_v_art_mode = 'U'.   " Update
* For Article Updation,

* Check if any node exist for this article
      CLEAR ls_matgrp_prod.
      LOOP AT fu_t_matgrp_prod[] INTO ls_matgrp_prod
      WHERE matnr   = fc_s_headdata-material
        AND hier_id = fu_s_matgrp_hier-hier_id.
        EXIT.
      ENDLOOP.

      IF ls_matgrp_prod IS NOT INITIAL.

* If any node doesn't exist then, check if required node exist
        CLEAR ls_matgrp_prod.
        READ TABLE fu_t_matgrp_prod[] INTO ls_matgrp_prod
        WITH KEY matnr   = fc_s_headdata-material
                 hier_id = fu_s_matgrp_hier-hier_id
                 node    = ls_reg_hdr-article_hierarchy.
        IF sy-subrc NE 0.
* If not found then, create article hierarchy without mainflag
          ls_hierarchy_items-mainflg   = space.
          APPEND ls_hierarchy_items TO fc_t_hierarchy_items[].
        ENDIF.
      ELSE.

* If any node doesn't exist then, create article hierarchy
        APPEND ls_hierarchy_items TO fc_t_hierarchy_items[].
      ENDIF.


    ENDIF.

  ENDIF.  " IF ls_reg_hdr-article_hierarchy IS NOT INITIAL



**********************************************************************************

** National GTIN Variants are already added in National 'UNITSOFMEASURE'

*** INTERNATIONALARTNOS
  CLEAR ls_internationalartnos.
  ls_internationalartnos-function = gc_bapi-func_005.    " '005'.
  ls_internationalartnos-material = fc_s_headdata-material.

  CLEAR ls_reg_ean.
  LOOP AT fu_s_reg_data-zarn_reg_ean[] INTO ls_reg_ean
    WHERE idno  = fu_s_prod_data-idno
      AND meinh IS NOT INITIAL.

* UNIT
    ls_internationalartnos-unit = ls_reg_ean-meinh.

* UNIT_ISO - ISOCODE - Units of Measurement
    CLEAR ls_t006.
    READ TABLE fu_t_t006[] INTO ls_t006
    WITH TABLE KEY msehi = ls_reg_ean-meinh.
    IF sy-subrc = 0.
      ls_internationalartnos-unit_iso = ls_t006-isocode.
    ENDIF.


*    IF ls_reg_uom-uom_category NE gc_uom_cat-layer  AND         " LAYER
*       ls_reg_uom-uom_category NE gc_uom_cat-pallet.            " PALLET

* EAN_UPC / EAN_CAT
    ls_internationalartnos-ean_upc  = ls_reg_ean-ean11.
    ls_internationalartnos-ean_cat  = ls_reg_ean-eantp.
*    ENDIF.

    APPEND ls_internationalartnos TO fc_t_internationalartnos[].
  ENDLOOP.



**********************************************************************************


*** UNITSOFMEASURE

  SORT fu_s_reg_data-zarn_reg_uom[] BY idno uom_category.

  CLEAR ls_reg_uom.
  LOOP AT fu_s_reg_data-zarn_reg_uom[] INTO ls_reg_uom
    WHERE meinh IS NOT INITIAL.

    CLEAR ls_unitsofmeasure.
    ls_unitsofmeasure-function = gc_bapi-func_005.    " '005'.
    ls_unitsofmeasure-material = fc_s_headdata-material.
    ls_unitsofmeasure-alt_unit = ls_reg_uom-meinh.




*** UNITSOFMEASUREX
    CLEAR ls_unitsofmeasurex.
    ls_unitsofmeasurex-function = gc_bapi-func_005.    " '005'.
    ls_unitsofmeasurex-material = fc_s_headdata-material.
    ls_unitsofmeasurex-alt_unit = ls_reg_uom-meinh.



* ISOCODE - Units of Measurement
    CLEAR ls_t006.
    READ TABLE fu_t_t006[] INTO ls_t006
    WITH TABLE KEY msehi = ls_reg_uom-meinh.
    IF sy-subrc = 0.
      ls_unitsofmeasure-alt_unit_iso = ls_t006-isocode.

      ls_unitsofmeasurex-alt_unit_iso = ls_t006-isocode.
    ENDIF.




* NUMERATOR/DENOMINATOR

    ls_unitsofmeasure-denominatr = '1'.

    TRANSLATE ls_reg_uom-uom_category TO UPPER CASE.


    IF ls_reg_uom-num_base_units GT '99999'.
      ls_unitsofmeasure-numerator  = '99999'.
    ELSE.
      ls_unitsofmeasure-numerator  = ls_reg_uom-num_base_units.
    ENDIF.

    IF ls_reg_uom-factor_of_base_units GT '99999'.
      ls_unitsofmeasure-denominatr = '99999'.
    ELSE.
      ls_unitsofmeasure-denominatr = ls_reg_uom-factor_of_base_units.
    ENDIF.



    ls_unitsofmeasure-length     = ls_reg_uom-depth_value.
    ls_unitsofmeasure-width      = ls_reg_uom-width_value.
    ls_unitsofmeasure-height     = ls_reg_uom-height_value.
    ls_unitsofmeasure-gross_wt   = ls_reg_uom-gross_weight_value.

* UNIT_OF_WT
    CLEAR ls_gen_uom_t.
    READ TABLE fu_t_gen_uom_t[] INTO ls_gen_uom_t
    WITH TABLE KEY gen_uom = ls_reg_uom-gross_weight_uom
                   spras   = sy-langu.
    IF sy-subrc = 0.
      ls_unitsofmeasure-unit_of_wt = ls_gen_uom_t-sap_uom.

      ls_unitsofmeasurex-unit_of_wt = abap_true.
* UNIT_OF_WT_ISO
      CLEAR ls_t006.
      READ TABLE fu_t_t006[] INTO ls_t006
      WITH TABLE KEY msehi = ls_gen_uom_t-sap_uom.
      IF sy-subrc = 0.
        ls_unitsofmeasure-unit_of_wt_iso = ls_t006-isocode.

        ls_unitsofmeasurex-unit_of_wt_iso = abap_true.
      ENDIF.
    ENDIF.

* INS Begin of Change INC5409316 JKH 17.11.2016
    zcl_arn_gui_load=>unit_conversion_simple(
          EXPORTING input    = ls_unitsofmeasure-length
                    unit_in  = ls_unitsofmeasure-unit_dim
                    unit_out = 'MM'
          IMPORTING output   = ls_unitsofmeasure-length
                    ev_error = lv_uom_conv_err ).


    zcl_arn_gui_load=>unit_conversion_simple(
          EXPORTING input    = ls_unitsofmeasure-width
                    unit_in  = ls_unitsofmeasure-unit_dim
                    unit_out = 'MM'
          IMPORTING output   = ls_unitsofmeasure-width
                    ev_error = lv_uom_conv_err ).


    zcl_arn_gui_load=>unit_conversion_simple(
          EXPORTING input    = ls_unitsofmeasure-height
                    unit_in  = ls_unitsofmeasure-unit_dim
                    unit_out = 'MM'
          IMPORTING output   = ls_unitsofmeasure-height
                    ev_error = lv_uom_conv_err ).
    IF lv_uom_conv_err IS INITIAL.
      ls_reg_uom-height_uom = 'MMT'.
    ENDIF.

*VOLUME
    TRY.
        ls_unitsofmeasure-volume = ls_unitsofmeasure-length *
                                   ls_unitsofmeasure-width  *
                                   ls_unitsofmeasure-height.
        ls_unitsofmeasurex-volume   = abap_true.

*VOLUMEUNIT
        ls_unitsofmeasure-volumeunit  = 'MMQ'.
        ls_unitsofmeasurex-volumeunit = abap_true.

*VOLUMEUNIT_ISO
        ls_unitsofmeasure-volumeunit_iso  = ''.
        ls_unitsofmeasurex-volumeunit_iso = abap_true.
      CATCH cx_sy_arithmetic_overflow.
* Do nothing, neither volume nor Unit will be posted by AReNa
* Volume and Unit will be calculated and Posted by SAP itself
    ENDTRY.
* INS End of Change INC5409316 JKH 17.11.2016


* UNIT_DIM
    CLEAR ls_gen_uom_t.
    READ TABLE fu_t_gen_uom_t[] INTO ls_gen_uom_t
    WITH TABLE KEY gen_uom = ls_reg_uom-height_uom
                   spras   = sy-langu.
    IF sy-subrc = 0.
      ls_unitsofmeasure-unit_dim = ls_gen_uom_t-sap_uom.

      ls_unitsofmeasurex-unit_dim = abap_true.

* UNIT_DIM_ISO
      CLEAR ls_t006.
      READ TABLE fu_t_t006[] INTO ls_t006
      WITH TABLE KEY msehi = ls_gen_uom_t-sap_uom.
      IF sy-subrc = 0.
        ls_unitsofmeasure-unit_dim_iso = ls_t006-isocode.

        ls_unitsofmeasurex-unit_dim_iso = abap_true.
      ENDIF.
    ENDIF.

    ls_unitsofmeasurex-numerator  = abap_true.
    ls_unitsofmeasurex-denominatr = abap_true.

    ls_unitsofmeasurex-length   = abap_true.
    ls_unitsofmeasurex-width    = abap_true.
    ls_unitsofmeasurex-height   = abap_true.
    ls_unitsofmeasurex-gross_wt = abap_true.

    IF ls_reg_uom-uom_category NE gc_uom_cat-layer  AND         " LAYER
       ls_reg_uom-uom_category NE gc_uom_cat-pallet.            " PALLET

* EAN_UPC / EAN_CAT
      CLEAR ls_reg_ean.
      READ TABLE fu_s_reg_data-zarn_reg_ean[] INTO ls_reg_ean
        WITH KEY idno  = fu_s_prod_data-idno
                 meinh = ls_reg_uom-meinh
                 hpean = abap_true.
      IF sy-subrc NE 0.

      ELSE.
        ls_unitsofmeasure-ean_upc = ls_reg_ean-ean11.
        ls_unitsofmeasure-ean_cat = ls_reg_ean-eantp.

        ls_unitsofmeasurex-ean_upc    = abap_true.
        ls_unitsofmeasurex-ean_cat    = abap_true.
      ENDIF.

    ENDIF.



* SUB_UOM
    ls_unitsofmeasure-sub_uom = ls_reg_uom-lower_meinh.

    ls_unitsofmeasurex-sub_uom     = abap_true.

* SUB_UOM_ISO
    CLEAR ls_t006.
    READ TABLE fu_t_t006[] INTO ls_t006
    WITH TABLE KEY msehi = ls_reg_uom-lower_meinh.
    IF sy-subrc = 0.
      ls_unitsofmeasure-sub_uom_iso = ls_t006-isocode.

      ls_unitsofmeasurex-sub_uom_iso = abap_true.
    ENDIF.



*    IF lv_no_uom IS INITIAL.
    APPEND ls_unitsofmeasure TO fc_t_unitsofmeasure[].
    APPEND ls_unitsofmeasurex TO fc_t_unitsofmeasurex[].
*    ENDIF.


  ENDLOOP.  " LOOP AT fu_s_reg_data-zarn_reg_uom[] INTO ls_reg_uom.

**********************************************************************************

*>>>IS 2019/06 creating multiple EINE for different EKORGs

  DATA(lo_pir_purch_org) = NEW zcl_arn_pir_utils( ).

  "restrict relevant EINE based on additional criteria
  CLEAR lt_pir_ekorg.
  LOOP AT fu_t_pir_ekorg ASSIGNING FIELD-SYMBOL(<ls_pir_ekorg>).
    IF <ls_pir_ekorg>-additional_condition IS INITIAL
      OR lo_pir_purch_org->is_ekorg_relevant( iv_ekorg = <ls_pir_ekorg>-ekorg
                                              is_reg_hdr = ls_reg_hdr ).

      APPEND <ls_pir_ekorg> TO lt_pir_ekorg.
    ENDIF.
  ENDLOOP.

*<<< IS 2019/06

*** INFORECORD_GENERAL

  lt_reg_pir_vend[] = fu_s_reg_data-zarn_reg_pir[].

  DELETE lt_reg_pir_vend[] WHERE pir_rel_eine IS INITIAL.

  SORT fu_s_reg_data-zarn_reg_pir[] BY idno lifnr   ASCENDING
                                       pir_rel_eina DESCENDING.


  CLEAR: ls_calp_vb, fc_s_pir_key.

  LOOP AT lt_reg_pir_vend[] INTO ls_reg_pir_vend.

    fc_s_pir_key-idno    = ls_reg_pir_vend-idno.
    fc_s_pir_key-version = fu_s_prod_data-version.

    CLEAR lv_nr_scenario.

    LOOP AT fu_s_reg_data-zarn_reg_pir[] INTO ls_reg_pir
         WHERE idno  = ls_reg_pir_vend-idno
           AND lifnr = ls_reg_pir_vend-lifnr
           AND ( pir_rel_eina = abap_true OR pir_rel_eine = abap_true ).

* Create/Update EINA only where PIR Relevant General is True
      IF ls_reg_pir-pir_rel_eina = abap_true.

*** INFORECORD_GENERAL
        CLEAR ls_inforecord_general.

*ls_inforecord_general-INFO_REC =
        ls_inforecord_general-material     = fc_s_headdata-material.
        ls_inforecord_general-vendor       = ls_reg_pir-lifnr.
        ls_inforecord_general-var_ord_un   = ls_reg_pir-vabme.
        ls_inforecord_general-suppl_from   = ls_reg_pir-lifab.
        ls_inforecord_general-suppl_to     = ls_reg_pir-lifbi.
        ls_inforecord_general-vend_mat     = ls_reg_pir-idnlf.

        ls_inforecord_general-po_unit      = ls_reg_pir-bprme.

* PO_UNIT_ISO
        CLEAR ls_t006.
        READ TABLE fu_t_t006[] INTO ls_t006
        WITH TABLE KEY msehi = ls_reg_uom-meinh.
        IF sy-subrc = 0.
          ls_inforecord_general-po_unit_iso  = ls_t006-isocode.
        ENDIF.


        CLEAR ls_eina.
        READ TABLE fu_t_eina INTO ls_eina
        WITH KEY matnr = fc_s_headdata-material
                 lifnr = ls_reg_pir-lifnr.
        IF sy-subrc = 0.
          ls_inforecord_general-created_at = ls_eina-erdat.
          ls_inforecord_general-created_by = ls_eina-ernam.

          ls_inforecord_general-mat_grp    = ls_eina-matkl.
          ls_inforecord_general-delete_ind = ls_eina-loekz.
          ls_inforecord_general-short_text = ls_eina-txz01.
          ls_inforecord_general-sorted_by  = ls_eina-sortl.
          ls_inforecord_general-sales_pers = ls_eina-verkf.
          ls_inforecord_general-telephone  = ls_eina-telf1.
          ls_inforecord_general-reminder1  = ls_eina-mahn1.
          ls_inforecord_general-reminder2  = ls_eina-mahn2.
          ls_inforecord_general-reminder3  = ls_eina-mahn3.
          ls_inforecord_general-cert_no    = ls_eina-urznr.
          ls_inforecord_general-cert_valid = ls_eina-urzdt.
          ls_inforecord_general-cert_ctry  = ls_eina-urzla.
          ls_inforecord_general-cert_type  = ls_eina-urztp.
          ls_inforecord_general-cust_no    = ls_eina-urzzt.
          ls_inforecord_general-region     = ls_eina-regio.
          ls_inforecord_general-vend_part  = ls_eina-ltsnr.
          ls_inforecord_general-sort_no    = ls_eina-ltssf.
          ls_inforecord_general-vend_matg  = ls_eina-wglif.
          ls_inforecord_general-back_agree = ls_eina-rueck.
          ls_inforecord_general-pre_vendor = ls_eina-kolif.
          ls_inforecord_general-points     = ls_eina-anzpu.
          ls_inforecord_general-point_unit = ls_eina-punei.

        ELSE.
          ls_inforecord_general-created_at = sy-datum.
          ls_inforecord_general-created_by = sy-uname.
        ENDIF.

* Check if RELIF is 'X' for any UOM of current Vendor then consider it as regular vendor
        CLEAR ls_reg_pir_po.
        LOOP AT fu_s_reg_data-zarn_reg_pir[] INTO ls_reg_pir_po
          WHERE idno  = ls_reg_pir-idno
            AND lifnr = ls_reg_pir-lifnr
            AND relif = abap_true.
          EXIT.
        ENDLOOP.

        IF ls_reg_pir_po IS INITIAL.
          ls_inforecord_general-norm_vend    = space.
        ELSE.
          ls_inforecord_general-norm_vend    = abap_true.
        ENDIF.

        CLEAR ls_reg_uom.
        READ TABLE fu_s_reg_data-zarn_reg_uom[] INTO ls_reg_uom
        WITH KEY  idno                 = ls_reg_pir-idno
                  pim_uom_code         = ls_reg_pir-order_uom_pim
                  hybris_internal_code = space
                  lower_uom            = ls_reg_pir-lower_uom
                  lower_child_uom      = ls_reg_pir-lower_child_uom.
        IF sy-subrc = 0.
          ls_inforecord_general-conv_num1 = ls_reg_uom-num_base_units.
          ls_inforecord_general-conv_den1 = ls_reg_uom-factor_of_base_units.
        ENDIF.

        CLEAR ls_reg_uom.
        READ TABLE fu_s_reg_data-zarn_reg_uom[] INTO ls_reg_uom
        WITH KEY idno      = fu_s_reg_data-idno
                 unit_base = abap_true.
        IF sy-subrc = 0.

          ls_inforecord_general-base_uom     = ls_reg_uom-meinh.

* BASE_UOM_ISO
          CLEAR ls_t006.
          READ TABLE fu_t_t006[] INTO ls_t006
          WITH TABLE KEY msehi = ls_reg_uom-meinh.
          IF sy-subrc = 0.
            ls_inforecord_general-base_uom_iso  = ls_t006-isocode.
          ENDIF.
        ENDIF.


        APPEND ls_inforecord_general TO fc_t_inforecord_general[].

*** CALP_VB
        ls_calp_vb-bstme   = ls_reg_pir-bprme.
        ls_calp_vb-meins   = ls_reg_pir-bprme.
      ENDIF.  " IF ls_reg_pir-po_unit = abap_true

**********************************************************************************


* Create/Update EINE only where PIR Relevant Purch Org is True
      IF ls_reg_pir-pir_rel_eine = abap_true.

*** INFORECORD_PURCHORG

        CLEAR ls_inforecord_purchorg.

*    ls_inforecord_purchorg-INFO_REC =
        ls_inforecord_purchorg-material   = fc_s_headdata-material.
*        ls_inforecord_purchorg-purch_org  = '9999'.
*        ls_inforecord_purchorg-created_at = sy-datum.
*        ls_inforecord_purchorg-created_by = sy-uname.

        ls_inforecord_purchorg-vendor     = ls_reg_pir-lifnr.
        ls_inforecord_purchorg-info_type  = ls_reg_pir-esokz.
        ls_inforecord_purchorg-delete_ind = ls_reg_pir-loekz.
        ls_inforecord_purchorg-pur_group  = ls_reg_pir-ekgrp.
        ls_inforecord_purchorg-min_po_qty = ls_reg_pir-minbm.
        ls_inforecord_purchorg-nrm_po_qty = ls_reg_pir-norbm.
        ls_inforecord_purchorg-plnd_delry = ls_reg_pir-aplfz.
        ls_inforecord_purchorg-cond_group = ls_reg_pir-ekkol.
        ls_inforecord_purchorg-tax_code   = ls_reg_pir-mwskz.
*        ls_inforecord_purchorg-last_po    = ls_reg_pir-datlb.
        ls_inforecord_purchorg-overdeltol = ls_reg_pir-uebto.
        ls_inforecord_purchorg-under_tol  = ls_reg_pir-untto.

        ls_inforecord_purchorg-currency   = ls_reg_pir-waers.
        ls_inforecord_purchorg-net_price  = ls_reg_pir-netpr.
        ls_inforecord_purchorg-price_unit = ls_reg_pir-peinh.
        ls_inforecord_purchorg-eff_price  = ls_reg_pir-netpr.
        ls_inforecord_purchorg-orderpr_un = ls_reg_pir-bprme.
        ls_inforecord_purchorg-price_date = ls_reg_pir-prdat.

        IF ls_inforecord_purchorg-price_date LT sy-datum.
          ls_inforecord_purchorg-price_date = '99991231'.
        ENDIF.

* ORDERPR_UN_ISO
        CLEAR ls_t006.
        READ TABLE fu_t_t006[] INTO ls_t006
        WITH TABLE KEY msehi = ls_reg_pir-bprme.
        IF sy-subrc = 0.
          ls_inforecord_purchorg-orderpr_un_iso  = ls_t006-isocode.
        ENDIF.

* CURRENCY_ISO
        CLEAR ls_tcurc.
        READ TABLE fu_t_tcurc[] INTO ls_tcurc
        WITH TABLE KEY waers = ls_reg_pir-waers.
        IF sy-subrc = 0.
          ls_inforecord_purchorg-currency_iso  = ls_tcurc-isocd.
        ENDIF.



* CONV_NUM1 / CONV_DEN1

        CLEAR : lv_po_qty, lv_price_qty, lv_fraction, lv_nomi, lv_deno.

* PIR_REL_EINA QTY - NUM_BASE_UNITS
        CLEAR ls_reg_pir_po.
        READ TABLE fu_s_reg_data-zarn_reg_pir[] INTO ls_reg_pir_po
        WITH KEY idno          = fu_s_reg_data-idno
                 lifnr         = ls_reg_pir-lifnr
                 pir_rel_eina  = abap_true.
        IF sy-subrc = 0.

          CLEAR ls_reg_uom.
          READ TABLE fu_s_reg_data-zarn_reg_uom[] INTO ls_reg_uom
          WITH KEY  idno                 = ls_reg_pir_po-idno
                    pim_uom_code         = ls_reg_pir_po-order_uom_pim
                    hybris_internal_code = space
                    lower_uom            = ls_reg_pir_po-lower_uom
                    lower_child_uom      = ls_reg_pir_po-lower_child_uom.
          IF sy-subrc = 0.

            IF ls_reg_uom-num_base_units IS INITIAL.
              ls_reg_uom-num_base_units = 1.
            ENDIF.

            IF ls_reg_uom-factor_of_base_units IS INITIAL.
              ls_reg_uom-factor_of_base_units = 1.
            ENDIF.

            lv_po_qty = ls_reg_uom-num_base_units / ls_reg_uom-factor_of_base_units.
          ENDIF.

        ENDIF.

* PRICE_UNIT QTY - NUM_BASE_UNITS
        CLEAR ls_reg_uom.
        READ TABLE fu_s_reg_data-zarn_reg_uom[] INTO ls_reg_uom
        WITH KEY  idno                 = ls_reg_pir-idno
                  pim_uom_code         = ls_reg_pir-order_uom_pim
                  hybris_internal_code = space
                  lower_uom            = ls_reg_pir-lower_uom
                  lower_child_uom      = ls_reg_pir-lower_child_uom.
        IF sy-subrc = 0.

          IF ls_reg_uom-num_base_units IS INITIAL.
            ls_reg_uom-num_base_units = 1.
          ENDIF.

          IF ls_reg_uom-factor_of_base_units IS INITIAL.
            ls_reg_uom-factor_of_base_units = 1.
          ENDIF.

          lv_price_qty = ls_reg_uom-num_base_units / ls_reg_uom-factor_of_base_units.
        ENDIF.

        ls_inforecord_purchorg-created_at = sy-datum.
        ls_inforecord_purchorg-created_by = sy-uname.

* Rounding Profile and 'Indicator: GR-Based Invoice Verification'
* Only on EINE Creation, if EINE already exist then not updated
        CLEAR ls_lfm1.
        READ TABLE fu_t_lfm1 INTO ls_lfm1
        WITH TABLE KEY lifnr = ls_reg_pir-lifnr
                       ekorg = fu_v_ekorg_main.
        IF sy-subrc = 0.
          ls_inforecord_purchorg-round_prof = ls_lfm1-rdprf.
          ls_inforecord_purchorg-gr_basediv = ls_lfm1-webre.

*          Article Posting: Confirmation control defaulting, post from vendor on creation
          ls_inforecord_purchorg-conf_ctrl  = ls_lfm1-bstae.  "++INC 5741756/5746577 JKH 17.03.2017
        ENDIF.

* 3159 - FOBL
* If EINE exist for given article/vendor then don't update EINE price fields and Order Unit

*>>> IS 2019/06 multiple changes in following code to enable creating / updating multiple EINE
*based on config in ZARN_PIR_PUR_CNF


        ls_inforecord_purchorg_main = ls_inforecord_purchorg.

        LOOP AT lt_pir_ekorg ASSIGNING <ls_pir_ekorg>.

          ls_inforecord_purchorg = ls_inforecord_purchorg_main.

          IF <ls_pir_ekorg>-main_ekorg IS INITIAL.
            "defaulting some fields if EKORG <> 9999
            lo_pir_purch_org->default_fields_ekorg( EXPORTING iv_lifnr    = ls_reg_pir-lifnr
                                                              iv_ekorg    = <ls_pir_ekorg>-ekorg
                                                              iv_base_uom = ls_clientdata_tmp-base_uom
                                                   CHANGING cs_inforec_purchorg = ls_inforecord_purchorg ).

          ENDIF.

          CLEAR ls_eina.
          READ TABLE fu_t_eina INTO ls_eina
          WITH KEY matnr = fc_s_headdata-material
                   lifnr = ls_reg_pir-lifnr.
          IF sy-subrc = 0.

            CLEAR ls_eine.
            READ TABLE fu_t_eine INTO ls_eine
            WITH KEY infnr = ls_eina-infnr
                     ekorg = <ls_pir_ekorg>-ekorg.
            IF sy-subrc = 0.

              "if such EINE exists, takeover most of the fields from it

              ls_inforecord_purchorg-created_at      = ls_eina-erdat.
              ls_inforecord_purchorg-created_by      = ls_eina-ernam.

              ls_inforecord_purchorg-plant           = ls_eine-werks.
              ls_inforecord_purchorg-bonus           = ls_eine-bonus.
              ls_inforecord_purchorg-bonus_qty       = ls_eine-mgbon.
              ls_inforecord_purchorg-unlimited       = ls_eine-uebtk.
              ls_inforecord_purchorg-quotation       = ls_eine-angnr.
              ls_inforecord_purchorg-quot_date       = ls_eine-angdt.
              ls_inforecord_purchorg-rfq_no          = ls_eine-anfnr.
              ls_inforecord_purchorg-rfq_item        = ls_eine-anfps.
              ls_inforecord_purchorg-rfq_canc        = ls_eine-abskz.
              ls_inforecord_purchorg-amort_from      = ls_eine-amodv.
              ls_inforecord_purchorg-amort_to        = ls_eine-amodb.
              ls_inforecord_purchorg-amort_qty       = ls_eine-amobm.
              ls_inforecord_purchorg-amort_val       = ls_eine-amobw.
              ls_inforecord_purchorg-amort_tqty      = ls_eine-amoam.
              ls_inforecord_purchorg-amort_tval      = ls_eine-amoaw.
              ls_inforecord_purchorg-amort_res       = ls_eine-amors.
              ls_inforecord_purchorg-doc_cat         = ls_eine-bstyp.
              ls_inforecord_purchorg-po_number       = ls_eine-ebeln.
              ls_inforecord_purchorg-po_item         = ls_eine-ebelp.
              ls_inforecord_purchorg-last_po         = ls_eine-datlb.
              ls_inforecord_purchorg-no_mattext      = ls_eine-mtxno.
              ls_inforecord_purchorg-no_disct        = ls_eine-sktof.
              ls_inforecord_purchorg-ackn_reqd       = ls_eine-kzabs.
              ls_inforecord_purchorg-val_type        = ls_eine-bwtar.
              ls_inforecord_purchorg-bon_grp1        = ls_eine-ebonu.
              ls_inforecord_purchorg-shipping        = ls_eine-evers.
              ls_inforecord_purchorg-exp_imp_p       = ls_eine-exprf.
              ls_inforecord_purchorg-conf_ctrl       = ls_eine-bstae.
              ls_inforecord_purchorg-pricedate       = ls_eine-meprf.
              ls_inforecord_purchorg-incoterms1      = ls_eine-inco1.
              ls_inforecord_purchorg-incoterms2      = ls_eine-inco2.
              ls_inforecord_purchorg-no_aut_gr       = ls_eine-xersn.
              ls_inforecord_purchorg-bon_grp2        = ls_eine-ebon2.
              ls_inforecord_purchorg-bon_grp3        = ls_eine-ebon3.
              ls_inforecord_purchorg-no_bon_itm      = ls_eine-ebonf.
              ls_inforecord_purchorg-minremlife      = ls_eine-mhdrz.
              ls_inforecord_purchorg-version         = ls_eine-verid.
              ls_inforecord_purchorg-max_po_qty      = ls_eine-bstma.
              ls_inforecord_purchorg-unit_group      = ls_eine-megru.
              ls_inforecord_purchorg-bras_nbm        = ls_eine-j_1bnbm.
              ls_inforecord_purchorg-transport_chain = ls_eine-transport_chain.
              ls_inforecord_purchorg-staging_time    = ls_eine-staging_time.

* If EINE already exist then not updated
              ls_inforecord_purchorg-round_prof      = ls_eine-rdprf.
              ls_inforecord_purchorg-gr_basediv      = ls_eine-webre.

              IF ls_reg_pir-bprme IS INITIAL.
                lv_nr_scenario = abap_true.
              ELSE.

                CLEAR ls_marm.
                READ TABLE fu_t_marm[] INTO ls_marm
                WITH KEY matnr = fc_s_headdata-material
                         meinh = ls_reg_pir-bprme.
                IF sy-subrc = 0.
                  lv_nr_scenario = abap_true.
                ENDIF.  " READ TABLE fu_t_marm[] INTO ls_marm
              ENDIF.  " IF ls_reg_pir-bprme IS INITIAL

              IF lv_nr_scenario = abap_true.
                ls_inforecord_purchorg-currency   = ls_eine-waers.
                ls_inforecord_purchorg-net_price  = ls_eine-netpr.
                ls_inforecord_purchorg-price_unit = ls_eine-peinh.
                ls_inforecord_purchorg-eff_price  = ls_eine-netpr.
                ls_inforecord_purchorg-orderpr_un = ls_eine-bprme.
                ls_inforecord_purchorg-price_date = ls_eine-prdat.

* ORDERPR_UN_ISO
                CLEAR ls_t006.
                READ TABLE fu_t_t006[] INTO ls_t006
                WITH TABLE KEY msehi = ls_eine-bprme.
                IF sy-subrc = 0.
                  ls_inforecord_purchorg-orderpr_un_iso  = ls_t006-isocode.
                ENDIF.

* CURRENCY_ISO
                CLEAR ls_tcurc.
                READ TABLE fu_t_tcurc[] INTO ls_tcurc
                WITH TABLE KEY waers = ls_eine-waers.
                IF sy-subrc = 0.
                  ls_inforecord_purchorg-currency_iso  = ls_tcurc-isocd.
                ENDIF.

* PRICE_UNIT QTY - NUM_BASE_UNITS

                CLEAR ls_reg_pir_po.
                READ TABLE fu_s_reg_data-zarn_reg_pir[] INTO ls_reg_pir_po
                WITH KEY idno   = fu_s_reg_data-idno
                         lifnr  = ls_reg_pir-lifnr
                         bprme  = ls_eine-bprme.
                IF sy-subrc = 0.

                  CLEAR ls_reg_uom.
                  READ TABLE fu_s_reg_data-zarn_reg_uom[] INTO ls_reg_uom
                  WITH KEY  idno                 = ls_reg_pir_po-idno
                            pim_uom_code         = ls_reg_pir_po-order_uom_pim
                            hybris_internal_code = space
                            lower_uom            = ls_reg_pir_po-lower_uom
                            lower_child_uom      = ls_reg_pir_po-lower_child_uom.
                  IF sy-subrc = 0.

                    IF ls_reg_uom-num_base_units IS INITIAL.
                      ls_reg_uom-num_base_units = 1.
                    ENDIF.

                    IF ls_reg_uom-factor_of_base_units IS INITIAL.
                      ls_reg_uom-factor_of_base_units = 1.
                    ENDIF.

                    lv_price_qty = ls_reg_uom-num_base_units / ls_reg_uom-factor_of_base_units.
                  ENDIF.

                ENDIF.

              ENDIF.  " IF lv_nr_scenario = abap_true.

            ENDIF.

          ENDIF.

* Avoid divide by zero
          IF lv_price_qty IS NOT INITIAL.
* Get fraction of PO_UNIT / PRICE_UNIT
            lv_fraction = lv_po_qty / lv_price_qty.

            CALL FUNCTION 'CONVERT_TO_FRACTION'
              EXPORTING
                input       = lv_fraction
              IMPORTING
                nominator   = lv_nomi
                denominator = lv_deno.

            ls_inforecord_purchorg-conv_num1  = lv_nomi.
            ls_inforecord_purchorg-conv_den1  = lv_deno.


            ls_inforecord_purchorg-purch_org  = <ls_pir_ekorg>-ekorg.

            APPEND ls_inforecord_purchorg TO fc_t_inforecord_purchorg[].
          ENDIF.

*** CALP_VB
          IF <ls_pir_ekorg>-create_pb00 = abap_true
            AND lv_nr_scenario IS INITIAL.

            ls_calp_vb-matnr   = fc_s_headdata-material.
            ls_calp_vb-vrkme   = ls_inforecord_purchorg-orderpr_un.         "ls_reg_pir-bprme.
            ls_calp_vb-esokz   = ls_inforecord_purchorg-info_type.          "ls_reg_pir-esokz.
            ls_calp_vb-vkkab   = ls_reg_pir-datlb.
            ls_calp_vb-vkkbi   = ls_inforecord_purchorg-price_date.         "ls_reg_pir-prdat.

            IF ls_calp_vb-vkkbi LT sy-datum.
              ls_calp_vb-vkkbi   = '99991231'.
            ENDIF.

            ls_calp_vb-lifnr   = ls_inforecord_purchorg-vendor.             "ls_reg_pir-lifnr.
            ls_calp_vb-waers   = ls_inforecord_purchorg-currency.           "ls_reg_pir-waers.
            ls_calp_vb-kbetr   = ls_inforecord_purchorg-net_price.          "ls_reg_pir-netpr.
            ls_calp_vb-kpein   = ls_inforecord_purchorg-price_unit.         "ls_reg_pir-peinh.
            ls_calp_vb-kmein   = ls_inforecord_purchorg-orderpr_un.         "ls_reg_pir-bprme.
            ls_calp_vb-kappl   = 'M'.
            ls_calp_vb-kschl   = 'PB00'.
            ls_calp_vb-kotabnr = '018'.

            ls_calp_vb-ekorg  = <ls_pir_ekorg>-ekorg.
            APPEND ls_calp_vb TO fc_t_calp_vb[].

          ENDIF.
        ENDLOOP.
      ENDIF.   "  IF ls_reg_pir-pir_rel_eine = abap_true

*<<< IS 2019/06 multiple changes in code to enable creating / updating multiple EINE

    ENDLOOP.  " LOOP AT fu_s_reg_data-zarn_reg_pir INTO ls_reg_pir
  ENDLOOP.  " LOOP AT lt_reg_pir_vend[] INTO ls_reg_pir_vend


* INS Begin of IR5101117 JKH 25.08.2016
* Article/PIR posting failing due to incorrect EINE record
* - check EINE unit in MARM and UOMs to be created before creating PIR

  CLEAR fc_t_eine_unit_error[].
  LOOP AT fc_t_inforecord_purchorg[] INTO ls_inforecord_purchorg.

    lv_tabix = sy-tabix.

* Check in MARM
    CLEAR ls_marm.
    READ TABLE fu_t_marm[] INTO ls_marm
    WITH KEY matnr = ls_inforecord_purchorg-material
             meinh = ls_inforecord_purchorg-orderpr_un.
    IF sy-subrc NE 0.

* Check in UOMs to be created/modified
      CLEAR ls_unitsofmeasure.
      READ TABLE fc_t_unitsofmeasure[] INTO ls_unitsofmeasure
      WITH KEY material = ls_inforecord_purchorg-material
               alt_unit = ls_inforecord_purchorg-orderpr_un.
      IF sy-subrc NE 0.

* If EINE unit is not found in MARM and UOMs, then don't create PIR/Conditions
* for that article/vendor

        APPEND ls_inforecord_purchorg TO fc_t_eine_unit_error[].


* Delete EINE
        DELETE fc_t_inforecord_purchorg[] WHERE material = ls_inforecord_purchorg-material
                                            AND vendor   = ls_inforecord_purchorg-vendor.
* Delete EINA
        DELETE fc_t_inforecord_general[] WHERE material = ls_inforecord_purchorg-material
                                           AND vendor   = ls_inforecord_purchorg-vendor.
* Delete Conditions
        DELETE fc_t_calp_vb[] WHERE matnr = ls_inforecord_purchorg-material
                                AND lifnr = ls_inforecord_purchorg-vendor.

      ENDIF.
    ENDIF.  " READ TABLE fu_t_marm[] INTO ls_marm
  ENDLOOP.
* INS End of IR5101117 JKH 25.08.2016


ENDFORM.                    " MAP_REGIONAL_DATA
*&---------------------------------------------------------------------*
*&      Form  POST_ARTICLE_SAP
*&---------------------------------------------------------------------*
* Post Article into SAP
*----------------------------------------------------------------------*
FORM post_article_sap    USING  fu_s_idno_data             TYPE ty_s_idno_data
                                fu_s_pir_key               TYPE zsarn_key
                                fu_s_prod_data             TYPE zsarn_prod_data
                                fu_s_reg_data              TYPE zsarn_reg_data
                                fu_s_headdata              TYPE bapie1mathead
                                fu_s_hierarchy_data        TYPE bapi_wrf_hier_change_head
                                fu_t_errorkeys             TYPE wdsd_valkey_tty
                                fu_t_variantskeys          TYPE bapie1varkey_tab
                                fu_t_characteristicvalue   TYPE bapie1ausprt_tab
                                fu_t_characteristicvaluex  TYPE bapie1ausprtx_tab
                                fu_t_clientdata            TYPE bapie1marart_tab
                                fu_t_clientdatax           TYPE bapie1marartx_tab
                                fu_t_clientext             TYPE bapie1maraextrt_tab
                                fu_t_clientextx            TYPE bapie1maraextrtx_tab
                                fu_t_addnlclientdata       TYPE bapie1maw1rt_tab
                                fu_t_addnlclientdatax      TYPE bapie1maw1rtx_tab
                                fu_t_materialdescription   TYPE bapie1maktrt_tab
                                fu_t_plantdata             TYPE bapie1marcrt_tab
                                fu_t_plantdatax            TYPE bapie1marcrtx_tab
                                fu_t_plantext              TYPE bapie1marcextrt_tab
                                fu_t_plantextx             TYPE bapie1marcextrtx_tab
                                fu_t_forecastparameters    TYPE bapie1mpoprt_tab
                                fu_t_forecastparametersx   TYPE bapie1mpoprtx_tab
                                fu_t_forecastvalues        TYPE bapie1mprwrt_tab
                                fu_t_totalconsumption      TYPE bapie1mvegrt_tab
                                fu_t_unplndconsumption     TYPE bapie1mveurt_tab
                                fu_t_planningdata          TYPE bapie1mpgdrt_tab
                                fu_t_planningdatax         TYPE bapie1mpgdrtx_tab
                                fu_t_storagelocationdata   TYPE bapie1mardrt_tab
                                fu_t_storagelocationdatax  TYPE bapie1mardrtx_tab
                                fu_t_storagelocationext    TYPE bapie1mardextrt_tab
                                fu_t_storagelocationextx   TYPE bapie1mardextrtx_tab
                                fu_t_unitsofmeasure        TYPE bapie1marmrt_tab
                                fu_t_unitsofmeasurex       TYPE bapie1marmrtx_tab
                                fu_t_unitofmeasuretexts    TYPE bapie1mamtrt_tab
                                fu_t_internationalartnos   TYPE bapie1meanrt_tab
                                fu_t_vendorean             TYPE bapie1mleart_tab
                                fu_t_layoutmoduleassgmt    TYPE wstn_bapie1malgrt_tab
                                fu_t_layoutmoduleassgmtx   TYPE wstn_bapie1malgrtx_tab
                                fu_t_taxclassifications    TYPE bapie1mlanrt_tab
                                fu_t_valuationdata         TYPE bapie1mbewrt_tab
                                fu_t_valuationdatax        TYPE bapie1mbewrtx_tab
                                fu_t_valuationext          TYPE bapie1mbewextrt_tab
                                fu_t_valuationextx         TYPE bapie1mbewextrtx_tab
                                fu_t_warehousenumberdata   TYPE bapie1mlgnrt_tab
                                fu_t_warehousenumberdatax  TYPE bapie1mlgnrtx_tab
                                fu_t_warehousenumberext    TYPE bapie1mlgnextrt_tab
                                fu_t_warehousenumberextx   TYPE bapie1mlgnextrtx_tab
                                fu_t_storagetypedata       TYPE bapie1mlgtrt_tab
                                fu_t_storagetypedatax      TYPE bapie1mlgtrtx_tab
                                fu_t_storagetypeext        TYPE bapie1mlgtextrt_tab
                                fu_t_storagetypeextx       TYPE bapie1mlgtextrtx_tab
                                fu_t_salesdata             TYPE bapie1mvkert_tab
                                fu_t_salesdatax            TYPE bapie1mvkertx_tab
                                fu_t_salesext              TYPE bapie1mvkeextrt_tab
                                fu_t_salesextx             TYPE bapie1mvkeextrtx_tab
                                fu_t_posdata               TYPE bapie1wlk2rt_tab
                                fu_t_posdatax              TYPE bapie1wlk2rtx_tab
                                fu_t_posext                TYPE bapie1wlk2extrt_tab
                                fu_t_posextx               TYPE bapie1wlk2extrtx_tab
                                fu_t_materiallongtext      TYPE bapie1mltxrt_tab
                                fu_t_plantkeys             TYPE bapie1wrkkey_tab
                                fu_t_storagelocationkeys   TYPE bapie1lgokey_tab
                                fu_t_distrchainkeys        TYPE bapie1vtlkey_tab
                                fu_t_warehousenokeys       TYPE bapie1lgnkey_tab
                                fu_t_storagetypekeys       TYPE bapie1lgtkey_tab
                                fu_t_valuationtypekeys     TYPE bapie1bwakey_tab
                                fu_t_importextension       TYPE wrf_bapiparmatex_tty
                                fu_t_inforecord_general    TYPE wrf_bapieina_tty
                                fu_t_inforecord_purchorg   TYPE wrf_bapieine_tty
                                fu_t_source_list           TYPE wrf_eordu_tty
                                fu_t_additionaldata        TYPE wrf_bapie1wta01_tty
                                fu_t_calculationitemin     TYPE wrf_bapicalcitemin_tty
                                fu_t_calculationiteminx    TYPE wrf_bapicalciteminx_tty
                                fu_t_calculationextin      TYPE bapiparex_tab
                                fu_t_calculationitemout    TYPE wrf_bapicalcitemout_tty
                                fu_t_recipientparameters   TYPE wrf_bapi_wrpl_import_tty
                                fu_t_recipientparametersx  TYPE wrf_bapi_wrpl_importx_tty
                                fu_t_vendormatheader       TYPE wrf_bapi_vendor_mat_header_tty
                                fu_t_vendormatcharvalues   TYPE wrf_bapi_vendor_mcv_tty
                                fu_t_listingconditions     TYPE wstn_wlk1_ueb_tab
                                fu_t_bomheader             TYPE wrf_wstr_in_tty
                                fu_t_bompositions          TYPE wrf_stpob_tty
                                fu_t_priceconditions       TYPE edidd_tt
                                fu_t_hierarchy_items       TYPE bapi_wrf_hier_ch_items_tty
                                fu_t_followupmatdata       TYPE wrf_folup_typ_a_tty
                                fu_t_materialdatax         TYPE wrf_matdatax_tty
                       CHANGING fc_t_return                TYPE wty_bapireturn1_tab
                                fc_v_main_data_error       TYPE boolean
                                fc_v_additional_data_error TYPE boolean.


  CLEAR: fc_t_return[], fc_v_main_data_error, fc_v_additional_data_error.
  CALL FUNCTION 'WRF_MATERIAL_MAINTAINDATA_RT'
    EXPORTING
      headdata                 = fu_s_headdata
      hierarchy_data           = fu_s_hierarchy_data
*     ORGANIZATIONAL_LEVEL     =
*     SAPGUI_PROGRESS          = ' '
*     PROGRESS_STEP_ALL        = 0
*     PROGRESS_STEP_ACT        = 0
*     EXTERNAL_LOG_NUMBER      = ' '
      badi_callmode            = gc_arn_badi_callmode  " '14'
*     SEND_ERROR_MESSAGE       = ' '
*     DEQUEUE_ALL              = ' '
    IMPORTING
      ev_main_data_error       = fc_v_main_data_error
      ev_additional_data_error = fc_v_additional_data_error
    TABLES
      return                   = fc_t_return[]
      errorkeys                = fu_t_errorkeys[]
      variantskeys             = fu_t_variantskeys[]
      characteristicvalue      = fu_t_characteristicvalue[]
      characteristicvaluex     = fu_t_characteristicvaluex[]
      clientdata               = fu_t_clientdata[]
      clientdatax              = fu_t_clientdatax[]
      clientext                = fu_t_clientext[]
      clientextx               = fu_t_clientextx[]
      addnlclientdata          = fu_t_addnlclientdata[]
      addnlclientdatax         = fu_t_addnlclientdatax[]
      materialdescription      = fu_t_materialdescription[]
      plantdata                = fu_t_plantdata[]
      plantdatax               = fu_t_plantdatax[]
      plantext                 = fu_t_plantext[]
      plantextx                = fu_t_plantextx[]
      forecastparameters       = fu_t_forecastparameters[]
      forecastparametersx      = fu_t_forecastparametersx[]
      forecastvalues           = fu_t_forecastvalues[]
      totalconsumption         = fu_t_totalconsumption[]
      unplndconsumption        = fu_t_unplndconsumption[]
      planningdata             = fu_t_planningdata[]
      planningdatax            = fu_t_planningdatax[]
      storagelocationdata      = fu_t_storagelocationdata[]
      storagelocationdatax     = fu_t_storagelocationdatax[]
      storagelocationext       = fu_t_storagelocationext[]
      storagelocationextx      = fu_t_storagelocationextx[]
      unitsofmeasure           = fu_t_unitsofmeasure[]
      unitsofmeasurex          = fu_t_unitsofmeasurex[]
      unitofmeasuretexts       = fu_t_unitofmeasuretexts[]
      internationalartnos      = fu_t_internationalartnos[]
      vendorean                = fu_t_vendorean[]
      layoutmoduleassgmt       = fu_t_layoutmoduleassgmt[]
      layoutmoduleassgmtx      = fu_t_layoutmoduleassgmtx[]
      taxclassifications       = fu_t_taxclassifications[]
      valuationdata            = fu_t_valuationdata[]
      valuationdatax           = fu_t_valuationdatax[]
      valuationext             = fu_t_valuationext[]
      valuationextx            = fu_t_valuationextx[]
      warehousenumberdata      = fu_t_warehousenumberdata[]
      warehousenumberdatax     = fu_t_warehousenumberdatax[]
      warehousenumberext       = fu_t_warehousenumberext[]
      warehousenumberextx      = fu_t_warehousenumberextx[]
      storagetypedata          = fu_t_storagetypedata[]
      storagetypedatax         = fu_t_storagetypedatax[]
      storagetypeext           = fu_t_storagetypeext[]
      storagetypeextx          = fu_t_storagetypeextx[]
      salesdata                = fu_t_salesdata[]
      salesdatax               = fu_t_salesdatax[]
      salesext                 = fu_t_salesext[]
      salesextx                = fu_t_salesextx[]
      posdata                  = fu_t_posdata[]
      posdatax                 = fu_t_posdatax[]
      posext                   = fu_t_posext[]
      posextx                  = fu_t_posextx[]
      materiallongtext         = fu_t_materiallongtext[]
      plantkeys                = fu_t_plantkeys[]
      storagelocationkeys      = fu_t_storagelocationkeys[]
      distrchainkeys           = fu_t_distrchainkeys[]
      warehousenokeys          = fu_t_warehousenokeys[]
      storagetypekeys          = fu_t_storagetypekeys[]
      valuationtypekeys        = fu_t_valuationtypekeys[]
      importextension          = fu_t_importextension[]
      inforecord_general       = fu_t_inforecord_general[]
      inforecord_purchorg      = fu_t_inforecord_purchorg[]
      source_list              = fu_t_source_list[]
      additionaldata           = fu_t_additionaldata[]
      calculationitemin        = fu_t_calculationitemin[]
      calculationiteminx       = fu_t_calculationiteminx[]
      calculationextin         = fu_t_calculationextin[]
      calculationitemout       = fu_t_calculationitemout[]
      recipientparameters      = fu_t_recipientparameters[]
      recipientparametersx     = fu_t_recipientparametersx[]
      vendormatheader          = fu_t_vendormatheader[]
      vendormatcharvalues      = fu_t_vendormatcharvalues[]
      listingconditions        = fu_t_listingconditions[]
      bomheader                = fu_t_bomheader[]
      bompositions             = fu_t_bompositions[]
      priceconditions          = fu_t_priceconditions[]
      hierarchy_items          = fu_t_hierarchy_items[]
      followupmatdata          = fu_t_followupmatdata[]
      materialdatax            = fu_t_materialdatax[].

ENDFORM.                    " POST_ARTICLE_SAP
*&---------------------------------------------------------------------*
*&      Form  POST_CONDITIONS
*&---------------------------------------------------------------------*
* Post Conditions
*----------------------------------------------------------------------*
FORM post_conditions USING fu_t_calp_vb               TYPE calp_vb_tab
                           fu_t_inforecord_general    TYPE wrf_bapieina_tty
                           fu_s_idno_data             TYPE ty_s_idno_data
                           fu_v_batch                 TYPE flag
                  CHANGING fc_t_message               TYPE bal_t_msg
                           fc_v_error                 TYPE flag.

  DATA: lv_error      TYPE flag,
        ls_log_params TYPE zsarn_slg1_log_params_art_post,
        ls_calp_vb    TYPE calp_vb,
*        ls_inforecord_general TYPE bapieina,
        lt_calp_err   TYPE STANDARD TABLE OF erro,
        ls_calp_err   TYPE erro,
        lv_tabix      TYPE sy-tabix.


* Don't Post conditions for records for which PIR is not posted
  LOOP AT fu_t_calp_vb INTO ls_calp_vb.
    lv_tabix = sy-tabix.

    READ TABLE fu_t_inforecord_general[] TRANSPORTING NO FIELDS
    WITH KEY material = ls_calp_vb-matnr
             vendor   = ls_calp_vb-lifnr.
    IF sy-subrc NE 0.
      DELETE fu_t_calp_vb[] INDEX lv_tabix.
    ENDIF.
  ENDLOOP.  " LOOP AT fu_t_calp_vb INTO ls_calp_vb

  IF fu_t_calp_vb[] IS INITIAL.
    EXIT.
  ENDIF.


* Post all conditions in one step to avoid short dump
* because of duplicate key's concerning unit of measure
* i.e. when posting into A018
  CLEAR: lt_calp_err[], lv_error.
  CALL FUNCTION 'SALES_CONDITIONS_POSTE'
    EXPORTING
      pi_commit_work         = ' '
    TABLES
      px_t_calp_vb           = fu_t_calp_vb[]
      pe_t_erro              = lt_calp_err[]
    EXCEPTIONS
      no_records_for_posting = 1
      OTHERS                 = 2.
  IF sy-subrc = 0.

    LOOP AT lt_calp_err[] INTO ls_calp_err.
* Build Return Message
      CLEAR: ls_log_params.

      ls_log_params-idno        = fu_s_idno_data-idno.
      ls_log_params-fanid       = fu_s_idno_data-fan_id.
      ls_log_params-version     = fu_s_idno_data-current_ver.
      ls_log_params-nat_status  = fu_s_idno_data-nat_status.
      ls_log_params-reg_status  = fu_s_idno_data-reg_status.
      ls_log_params-saparticle  = fu_s_idno_data-sap_article.
      ls_log_params-batch_mode  = fu_v_batch.


      PERFORM build_message USING ls_calp_err-msgty
                                  ls_calp_err-msgid
                                  ls_calp_err-msgno
                                  ls_calp_err-msgv1
                                  ls_calp_err-msgv2
                                  ls_calp_err-msgv3
                                  ls_calp_err-msgv4
                                  ls_log_params
                         CHANGING fc_t_message[].

      IF ls_calp_err-msgty = gc_message-error.
        lv_error = abap_true.
      ENDIF.
    ENDLOOP.

  ELSE.
    lv_error = abap_true.
  ENDIF.

  IF lv_error = abap_true.
    ROLLBACK WORK.
  ELSE.
    COMMIT WORK AND WAIT.
  ENDIF.

  fc_v_error = lv_error.


ENDFORM.                    " POST_CONDITIONS
*&---------------------------------------------------------------------*
*&      Form  FILL_PLANTDATA
*&---------------------------------------------------------------------*
* Fill Plant Data
*----------------------------------------------------------------------*
FORM fill_plantdata USING fu_v_function   TYPE msgfn
                          fu_v_material   TYPE matnr
                          fu_v_plant      TYPE werks_d
                          fu_v_source     TYPE bwscl
                          fu_v_art_mode   TYPE char1
                 CHANGING fc_s_plantdata  TYPE bapie1marcrt
                          fc_s_plantdatax TYPE bapie1marcrtx.



* PLANTDATA - RFXX/RLXX
  CLEAR fc_s_plantdata.
  fc_s_plantdata-function       = fu_v_function.
  fc_s_plantdata-material       = fu_v_material.
  fc_s_plantdata-plant          = fu_v_plant.

  IF fu_v_art_mode = 'C'.  " Create
*  fc_s_plantdata-abc_id         =
*  fc_s_plantdata-pur_group      =
*  fc_s_plantdata-mrp_type       =
*  fc_s_plantdata-plnd_delry     =
*  fc_s_plantdata-availcheck     =
*  fc_s_plantdata-auto_p_ord     =
*  fc_s_plantdata-cc_ph_inv      =
*  fc_s_plantdata-sloc_exprc     =
  ENDIF.

*  fc_s_plantdata-issue_unit     =
*  fc_s_plantdata-issue_unit_iso =
*  fc_s_plantdata-neg_stocks     =

  IF fu_v_source IS NOT INITIAL.
    fc_s_plantdata-sup_source     = fu_v_source.
  ELSE.
    fc_s_plantdata-sup_source     = '1'.
  ENDIF.

  fc_s_plantdata-regionorig = space.





* PLANTDATAX - RFXX/RLXX
  CLEAR fc_s_plantdatax.
  fc_s_plantdatax-function       = fu_v_function.
  fc_s_plantdatax-material       = fu_v_material.
  fc_s_plantdatax-plant          = fu_v_plant.

  IF fu_v_art_mode = 'C'.  " Create
*  fc_s_plantdatax-abc_id         = abap_true.
*  fc_s_plantdatax-pur_group      = abap_true.
*  fc_s_plantdatax-mrp_type       = abap_true.
*  fc_s_plantdatax-plnd_delry     = abap_true.
*  fc_s_plantdatax-availcheck     = abap_true.
*  fc_s_plantdatax-auto_p_ord     = abap_true.
*  fc_s_plantdatax-cc_ph_inv      = abap_true.
*  fc_s_plantdatax-sloc_exprc     = abap_true.

  ENDIF.


*  fc_s_plantdatax-issue_unit     = abap_true.
*  fc_s_plantdatax-issue_unit_iso = abap_true.
*  fc_s_plantdatax-neg_stocks     = abap_true.

  fc_s_plantdatax-sup_source     = abap_true.

  fc_s_plantdatax-regionorig     = abap_true.


ENDFORM.                    " FILL_PLANTDATA
*&---------------------------------------------------------------------*
*&      Form  FILL_PLANTEXT
*&---------------------------------------------------------------------*
* Fill Plant Data
*----------------------------------------------------------------------*
FORM fill_plantext  USING fu_v_function      TYPE msgfn
                          fu_v_material      TYPE matnr
                          fu_v_plant         TYPE werks_d
                          fu_s_reg_banner    TYPE zarn_reg_banner
                          fu_v_art_mode      TYPE char1
                 CHANGING fc_s_plantext      TYPE bapie1marcextrt
                          fc_s_plantextx     TYPE bapie1marcextrtx
                          fc_s_bapi_plantext TYPE zmd_s_bapi_plantext.

  DATA: ls_plantext       TYPE bapie1marcextrt,
        ls_plantextx      TYPE bapie1marcextrtx,
        ls_bapi_plantext  TYPE zmd_s_bapi_plantext,
        ls_bapi_plantextx TYPE zmd_s_bapi_plantextx,
        lv_string         TYPE string,

        BEGIN OF ls_data_plant,
          field1 TYPE bapie1marcextrtx-field1,
          field2 TYPE bapie1marcextrtx-field2,
          field3 TYPE bapie1marcextrtx-field3,
          field4 TYPE bapie1marcextrtx-field4,
        END OF ls_data_plant.

  CLEAR: fc_s_plantext,  fc_s_plantextx,  fc_s_bapi_plantext.

*  EXIT.

*** PLANTEXT   - RFXX
  CLEAR: ls_bapi_plantext.

  ls_bapi_plantext-function       = fu_v_function.
  ls_bapi_plantext-material       = fu_v_material.
  ls_bapi_plantext-plant          = fu_v_plant.

  ls_bapi_plantext-zzonline_status = fu_s_reg_banner-online_status.
  ls_bapi_plantext-zz_pbs          = fu_s_reg_banner-pbs_code.

  CLEAR lv_string.
  cl_abap_container_utilities=>fill_container_c(
    EXPORTING im_value = ls_bapi_plantext-data
    IMPORTING ex_container = lv_string ).

  CLEAR ls_data_plant.
  ls_data_plant-field1(30) = 'ZMD_S_BAPI_PLANTEXT'.
  ls_data_plant+30 = lv_string.

  CLEAR ls_plantext.
  ls_plantext-function   = fu_v_function.
  ls_plantext-material   = fu_v_material.
  ls_plantext-plant      = fu_v_plant.
  ls_plantext-field1     = ls_data_plant-field1.
  ls_plantext-field2     = ls_data_plant-field2.
  ls_plantext-field3     = ls_data_plant-field3.
  ls_plantext-field4     = ls_data_plant-field4.


*** PLANTEXTX  - RFXX
  CLEAR: ls_bapi_plantextx.

  ls_bapi_plantextx-function       = fu_v_function.
  ls_bapi_plantextx-material       = fu_v_material.
  ls_bapi_plantextx-plant          = fu_v_plant.

  ls_bapi_plantextx-zzonline_status = abap_true.
  ls_bapi_plantextx-zz_pbs          = abap_true.

  CLEAR lv_string.
  cl_abap_container_utilities=>fill_container_c(
    EXPORTING im_value = ls_bapi_plantextx-datax
    IMPORTING ex_container = lv_string ).

  CLEAR ls_data_plant.
  ls_data_plant-field1(30) = 'ZMD_S_BAPI_PLANTEXTX'.
  ls_data_plant+30 = lv_string.

  CLEAR ls_plantextx.
  ls_plantextx-function   = fu_v_function.
  ls_plantextx-material   = fu_v_material.
  ls_plantextx-plant      = fu_v_plant.
  ls_plantextx-field1     = ls_data_plant-field1.
  ls_plantextx-field2     = ls_data_plant-field2.
  ls_plantextx-field3     = ls_data_plant-field3.
  ls_plantextx-field4     = ls_data_plant-field4.


  fc_s_plantext      = ls_plantext.
  fc_s_plantextx     = ls_plantextx.
  fc_s_bapi_plantext = ls_bapi_plantext.


ENDFORM.                    " FILL_PLANTEXT
*&---------------------------------------------------------------------*
*&      Form  BUILD_IMPORTEXTENSION_CREATE
*&---------------------------------------------------------------------*
* Build Host Data in IMPORTEXTENSION - Create Product
*----------------------------------------------------------------------*
FORM build_importextension_create USING fu_s_idno_data           TYPE ty_s_idno_data
                                        fu_s_prod_data           TYPE zsarn_prod_data
                                        fu_s_reg_data            TYPE zsarn_reg_data
                                        fu_v_art_mode            TYPE char1
                                        fu_s_headdata            TYPE bapie1mathead
                                        fu_t_marm                TYPE ty_t_marm
                                        fu_t_uom_cat             TYPE ty_t_uom_cat
                                        fu_t_host_sap            TYPE ty_t_host_sap
                               CHANGING fc_t_importextension     TYPE wrf_bapiparmatex_tty
                                        fc_t_host_sap_all        TYPE ty_t_host_sap
                                        fc_t_legacy_struct       TYPE ztarn_legacy_struct.


  DATA: ls_products       TYPE zarn_products,
        ls_reg_hdr        TYPE zarn_reg_hdr,
        ls_reg_uom        TYPE zarn_reg_uom,
        ls_reg_banner     TYPE zarn_reg_banner,

        ls_marm           TYPE ty_s_marm,
        ls_host_sap       TYPE ty_s_host_sap,
        ls_host_sap_new   TYPE ty_s_host_sap,

        ls_bapi_importext TYPE zarn_legacy_struct,

        BEGIN OF ls_data_import,
          field1 TYPE bapiparmatex-field1,
          field2 TYPE bapiparmatex-field2,
          field3 TYPE bapiparmatex-field3,
          field4 TYPE bapiparmatex-field4,
        END OF ls_data_import,

        ls_importextension TYPE bapiparmatex,
        lv_string          TYPE string.

  DATA: lv_process_host             TYPE xfeld,
        lv_badi_create_host_product TYPE flag,
        lv_host_retail_uom          TYPE meins,
        lv_host_repack_uom          TYPE meins,
        lv_host_bulk_uom            TYPE meins,
        lv_host_layer_uom           TYPE meins,
        lv_host_pallet_uom          TYPE meins,
        lv_len                      TYPE i,
        lv_desc_text                TYPE i      VALUE 26.


* Products
  CLEAR: ls_products.
  READ TABLE fu_s_prod_data-zarn_products[] INTO ls_products INDEX 1.

* Regional Header
  CLEAR: ls_reg_hdr.
  READ TABLE fu_s_reg_data-zarn_reg_hdr[] INTO ls_reg_hdr INDEX 1.


  IF ls_reg_hdr-leg_retail IS INITIAL OR
     ls_reg_hdr-leg_bulk   IS INITIAL OR
     ls_reg_hdr-leg_repack IS INITIAL.
    EXIT.
  ENDIF.

  CLEAR : fc_t_host_sap_all[].

  LOOP AT fu_t_host_sap[] INTO ls_host_sap
    WHERE matnr = fu_s_headdata-material.
    APPEND ls_host_sap TO fc_t_host_sap_all[].
  ENDLOOP.




  "process host data only IF
  "a) 1000 with Category manager exists OR
  "b) 4000/5000/6000 with Category manager and Assort. Grade exist

  " Get the assortment ranges to be excluded for host data processing
  DATA(lt_tvarvc) = VALUE tvarvc_t( ( name = zif_tvarvc_name_c=>gc_host_assort_range_exclude ) ).
  zcl_stvarv_constant=>get_stvarv_constant(
    CHANGING
      xt_stvarv_constant = lt_tvarvc ).
  DATA(lt_sstuf_r) = VALUE ztmd_sstuf_range( FOR wa IN lt_tvarvc
                                             WHERE ( type = 'S' AND opti IS NOT INITIAL )
                                             ( sign   = wa-sign
                                               option = wa-opti
                                               low    = wa-low
                                               high   = wa-high )
                                           ).

  CLEAR ls_reg_banner.
  LOOP AT fu_s_reg_data-zarn_reg_banner[] INTO ls_reg_banner
    WHERE idno = fu_s_reg_data-idno.

    CASE ls_reg_banner-banner.
      WHEN zcl_constants=>gc_banner_1000.
        IF ls_reg_banner-zzcatman IS NOT INITIAL AND ls_reg_hdr-zz_uni_dc EQ abap_true.
          "if exists 1000 with Catman and UNI DC flag = X, proceed
          lv_process_host = abap_true.
          EXIT.
        ENDIF.
      WHEN  zcl_constants=>gc_banner_4000
         OR zcl_constants=>gc_banner_5000
         OR zcl_constants=>gc_banner_6000.
        "if exists Retail Banner with Catman and Assortment (other than the excluded ranges), proceed
        IF ls_reg_banner-zzcatman IS NOT INITIAL     AND
           ls_reg_banner-sstuf IS NOT INITIAL AND
           ls_reg_banner-sstuf NOT IN lt_sstuf_r[].
          lv_process_host = abap_true.
          EXIT.
        ENDIF.
    ENDCASE.

  ENDLOOP.  " LOOP AT fu_s_reg_data-zarn_reg_banner[] INTO ls_reg_banner


  CHECK lv_process_host EQ abap_true.




* Check whether there is any need to create HOST product?
  "1st check - UOM Check
  CLEAR: ls_reg_uom, lv_badi_create_host_product.
  LOOP AT fu_s_reg_data-zarn_reg_uom[] INTO ls_reg_uom
    WHERE idno           = fu_s_reg_data-idno
      AND ( uom_category = gc_uom_cat-inner   OR    "Check inner uom
            uom_category = gc_uom_cat-shipper OR    "Check shipper uom
            uom_category = gc_uom_cat-retail )      "Check retail uom
      AND meinh IS NOT INITIAL.  "++IR5025734 JKH 03.08.2016

    CLEAR ls_marm.
    READ TABLE fu_t_marm[] INTO ls_marm
    WITH TABLE KEY matnr = fu_s_headdata-material
                   meinh = ls_reg_uom-meinh.
    IF sy-subrc NE 0.
      lv_badi_create_host_product = zcl_constants=>gc_true.
      EXIT.
    ENDIF.
  ENDLOOP.



  "2nd check - VKORG Check - When new line items are added
  IF lv_badi_create_host_product IS INITIAL.
* Check any existing entry in host table exist.
    CLEAR: ls_host_sap.
    "Read HOST Table entry
    READ TABLE fu_t_host_sap[] INTO ls_host_sap
    WITH KEY matnr = fu_s_headdata-material.
    IF sy-subrc NE 0. "No product exist & there is pack size changes thus create product
      lv_badi_create_host_product = zcl_constants=>gc_true.
    ENDIF.
  ENDIF.


  "Product creation is required or not?
  CHECK NOT lv_badi_create_host_product  IS INITIAL.



  CLEAR : lv_host_retail_uom, lv_host_repack_uom, lv_host_bulk_uom,
          lv_host_layer_uom, lv_host_pallet_uom.




  TRANSLATE: ls_reg_hdr-leg_retail TO UPPER CASE.
  TRANSLATE: ls_reg_hdr-leg_bulk   TO UPPER CASE.
  TRANSLATE: ls_reg_hdr-leg_repack TO UPPER CASE.


* Get Legacy UOM  - RETAIL_UOM - retail
  PERFORM get_legacy_uom USING fu_s_reg_data
                               fu_t_uom_cat[]
                               ls_reg_hdr-leg_retail
                      CHANGING lv_host_retail_uom.

* Get Legacy UOM  - REPACK_UOM - inner
  PERFORM get_legacy_uom USING fu_s_reg_data
                               fu_t_uom_cat[]
                               ls_reg_hdr-leg_repack
                      CHANGING lv_host_repack_uom.

* Get Legacy UOM  - BULK_UOM - shipper
  PERFORM get_legacy_uom USING fu_s_reg_data
                               fu_t_uom_cat[]
                               ls_reg_hdr-leg_bulk
                      CHANGING lv_host_bulk_uom.

* Get Legacy UOM  - LAYER_UOM - layer
  PERFORM get_legacy_uom USING fu_s_reg_data
                               fu_t_uom_cat[]
                               gc_uom_cat-layer
                      CHANGING lv_host_layer_uom.

* Get Legacy UOM  - PALLET_UOM - pallet
  PERFORM get_legacy_uom USING fu_s_reg_data
                               fu_t_uom_cat[]
                               gc_uom_cat-pallet
                      CHANGING lv_host_pallet_uom.



  CLEAR : ls_host_sap_new.
  ls_host_sap_new-matnr         = fu_s_headdata-material.
  ls_host_sap_new-retail_uom    = lv_host_retail_uom.
  ls_host_sap_new-inner_uom     = lv_host_repack_uom.
  ls_host_sap_new-shipper_uom   = lv_host_bulk_uom.
  ls_host_sap_new-layer_uom     = lv_host_layer_uom.
  ls_host_sap_new-pallet_uom    = lv_host_pallet_uom.
  ls_host_sap_new-prboly        = ls_reg_hdr-leg_prboly.

  ls_host_sap_new-hostdescription = ls_reg_hdr-retail_unit_desc.

  CLEAR lv_len.
  lv_len = strlen( ls_host_sap_new-hostdescription ).

  IF lv_len GT ( lv_desc_text - 1 ).

    WHILE lv_desc_text GE 0.
      lv_desc_text = lv_desc_text - 1.
      IF ls_host_sap_new-hostdescription+lv_desc_text(1) IS NOT INITIAL.
        CLEAR ls_host_sap_new-hostdescription+lv_desc_text(1).
      ELSE.
        EXIT.
      ENDIF.
    ENDWHILE.

  ENDIF.



  CLEAR : ls_host_sap.
  "Read HOST Table entry
  READ TABLE fu_t_host_sap[] INTO ls_host_sap
  WITH KEY matnr = fu_s_headdata-material
           latest = zcl_constants=>gc_true.
  IF sy-subrc EQ 0.
    " Don't set latest indicator in case of additional (not 1st) CPQ product
    " when already latest product exits
    ls_host_sap_new-latest        = zcl_constants=>gc_false.
    ls_host_sap_new-cpqprd        = ls_host_sap-product.
  ELSE.
    "No value exists, thus create a new entry in HOST table without comparing
    ls_host_sap_new-latest        = zcl_constants=>gc_true.
  ENDIF.


**--Get new product Code only if required otherwise skip.
*  CALL FUNCTION 'ZMD_GET_HOST_PRODUCT_CODE'
*    EXPORTING
*      iv_matnr    = fu_s_headdata-material
*    IMPORTING
*      ev_product  = ls_host_sap_new-product
*    EXCEPTIONS
*      error_found = 1
*      OTHERS      = 2.



  INSERT ls_host_sap_new INTO TABLE fc_t_host_sap_all[].



  CLEAR ls_bapi_importext.
  ls_bapi_importext-structure   = 'ZARN_LEGACY_STRUCT'.
  ls_bapi_importext-material    = fu_s_headdata-material.


  MOVE-CORRESPONDING ls_host_sap_new TO ls_bapi_importext-data.


  ls_bapi_importext-ersda = sy-datum.
  ls_bapi_importext-erzet = sy-uzeit.
  ls_bapi_importext-ernam = sy-uname.


  APPEND ls_bapi_importext TO fc_t_legacy_struct[].


  CLEAR lv_string.
  cl_abap_container_utilities=>fill_container_c(
    EXPORTING im_value = ls_bapi_importext-data
    IMPORTING ex_container = lv_string ).

  CLEAR ls_data_import.
  ls_data_import-field1(30) = 'ZARN_LEGACY_STRUCT'.
  ls_data_import+30 = lv_string.



  CLEAR ls_importextension.
  ls_importextension-structure  = 'ZARN_LEGACY_STRUCT'.
  ls_importextension-material   = fu_s_headdata-material.
  ls_importextension-field1     = ls_data_import-field1.
  ls_importextension-field2     = ls_data_import-field2.
  ls_importextension-field3     = ls_data_import-field3.
  ls_importextension-field4     = ls_data_import-field4.

  APPEND ls_importextension TO fc_t_importextension[].




ENDFORM.                    " BUILD_IMPORTEXTENSION_CREATE
*&---------------------------------------------------------------------*
*&      Form  GET_LEGACY_UOM
*&---------------------------------------------------------------------*
* Get Legacy UOM
*----------------------------------------------------------------------*
FORM get_legacy_uom USING fu_s_reg_data TYPE zsarn_reg_data
                          fu_t_uom_cat  TYPE ty_t_uom_cat
                          fu_v_uom_cat  TYPE zarn_uom_category
                 CHANGING fc_v_meins    TYPE meins.

  DATA: ls_reg_uom TYPE zarn_reg_uom,

        lt_uom_cat TYPE ty_t_uom_cat,
        ls_uom_cat TYPE ty_s_uom_cat,

        lv_lines   TYPE sy-tabix.

  CLEAR fc_v_meins.

  lt_uom_cat[] = fu_t_uom_cat[].
  DELETE lt_uom_cat[] WHERE uom_category NE fu_v_uom_cat.



  LOOP AT lt_uom_cat[] INTO ls_uom_cat.
    lv_lines = sy-tabix.

    CLEAR ls_reg_uom.
    READ TABLE fu_s_reg_data-zarn_reg_uom[] INTO ls_reg_uom
        WITH KEY idno         = fu_s_reg_data-idno
                 uom_category = fu_v_uom_cat
                 meinh        = ls_uom_cat-uom. "++IR5025734 JKH 03.08.2016
    IF sy-subrc NE 0.
      DELETE lt_uom_cat[] INDEX lv_lines.
    ELSE.
      fc_v_meins  = ls_reg_uom-meinh.
    ENDIF.
  ENDLOOP.




ENDFORM.                    " GET_LEGACY_UOM
*&---------------------------------------------------------------------*
*&      Form  BUILD_IMPORTEXTENSION_UPDATE
*&---------------------------------------------------------------------*
* Build Host Data in IMPORTEXTENSION - Update Product
*----------------------------------------------------------------------*
FORM build_importextension_update USING fu_s_idno_data           TYPE ty_s_idno_data
                                        fu_s_prod_data           TYPE zsarn_prod_data
                                        fu_s_reg_data            TYPE zsarn_reg_data
                                        fu_v_art_mode            TYPE char1
                                        fu_s_headdata            TYPE bapie1mathead
                                        "fu_t_maw1                TYPE ty_t_maw1
                                        fu_t_host_sap            TYPE ty_t_host_sap
                                        fu_t_addnlclientdata     TYPE bapie1maw1rt_tab
                               CHANGING fc_t_importextension     TYPE wrf_bapiparmatex_tty
                                        fc_t_legacy_struct       TYPE ztarn_legacy_struct.


  DATA: ls_addnlclientdata     TYPE bapie1maw1rt,
        ls_host_sap_latest_old TYPE ty_s_host_sap,
        ls_host_sap_latest_new TYPE ty_s_host_sap,
        ls_host_sap            TYPE ty_s_host_sap,

        lv_issue_uom           TYPE meinh,
        lv_count_uom           TYPE i,

        ls_bapi_importext      TYPE zarn_legacy_struct,

        BEGIN OF ls_data_import,
          field1 TYPE bapiparmatex-field1,
          field2 TYPE bapiparmatex-field2,
          field3 TYPE bapiparmatex-field3,
          field4 TYPE bapiparmatex-field4,
        END OF ls_data_import,

        ls_importextension TYPE bapiparmatex,
        lv_string          TYPE string.


  FIELD-SYMBOLS : <ls_bapi_importext>      TYPE zarn_legacy_struct.


  CLEAR ls_addnlclientdata.
  READ TABLE fu_t_addnlclientdata[] INTO ls_addnlclientdata INDEX 1.
  IF sy-subrc = 0 AND ls_addnlclientdata-material = fu_s_headdata-material.

* find latest product in existing DB + new created
    READ TABLE fu_t_host_sap[] INTO ls_host_sap
    WITH KEY matnr  = fu_s_headdata-material
             latest = abap_true.
    IF sy-subrc = 0 AND ls_host_sap-product IS NOT INITIAL.

      CLEAR ls_host_sap_latest_old.
      ls_host_sap_latest_old = ls_host_sap.


* Get Issue Unit
      CLEAR lv_issue_uom.
      IF ls_host_sap-prboly = 'B'.
        lv_issue_uom = ls_host_sap-shipper_uom.
      ELSE.
        lv_issue_uom = ls_host_sap-inner_uom.
      ENDIF.

* If Issue Unit in host sap for latest product is same as new issue unit in MAW1 (addnlclientdata)
      " then do nothing
* If Issue Unit in host sap for latest product is not same as new issue unit in MAW1 (addnlclientdata)
      " then update latest product in host and unckeck existing latest product
      IF lv_issue_uom NE ls_addnlclientdata-issue_unit.

* Count products with same issue unit in host sap as new issue unit in MAW1 (addnlclientdata)
        CLEAR: lv_count_uom, ls_host_sap, ls_host_sap_latest_new.
        LOOP AT fu_t_host_sap[] INTO ls_host_sap
          WHERE matnr EQ fu_s_headdata-material.

* Get Issue Unit
          CLEAR lv_issue_uom.
          IF ls_host_sap-prboly = 'B'.
            lv_issue_uom = ls_host_sap-shipper_uom.
          ELSE.
            lv_issue_uom = ls_host_sap-inner_uom.
          ENDIF.

* Check if multiple pro
          IF lv_issue_uom EQ ls_addnlclientdata-issue_unit.
            lv_count_uom = lv_count_uom + 1.
            ls_host_sap_latest_new = ls_host_sap.
          ENDIF.

        ENDLOOP.  " LOOP AT fu_t_host_sap[] INTO ls_host_sap

* If exactly ONE product exist with same issue unit in host sap as new issue unit in MAW1 (addnlclientdata)
        " then only update latest product in host and unckeck existing latest product
        IF lv_count_uom EQ 1.

* Unckeck existing latest product
          CLEAR ls_host_sap_latest_old-latest.

          CLEAR ls_bapi_importext.
          ls_bapi_importext-structure   = 'ZARN_LEGACY_STRUCT'.
          ls_bapi_importext-material    = fu_s_headdata-material.

          MOVE-CORRESPONDING ls_host_sap_latest_old TO ls_bapi_importext-data.

*          ls_bapi_importext-ersda = sy-datum.
*          ls_bapi_importext-erzet = sy-uzeit.
*          ls_bapi_importext-ernam = sy-uname.



          READ TABLE fc_t_legacy_struct[] ASSIGNING <ls_bapi_importext>
          WITH KEY material = ls_bapi_importext-material
                   product  = ls_bapi_importext-product.
          IF sy-subrc = 0.
            <ls_bapi_importext> = ls_bapi_importext.
          ELSE.
            APPEND ls_bapi_importext TO fc_t_legacy_struct[].
          ENDIF.


          CLEAR lv_string.
          cl_abap_container_utilities=>fill_container_c(
            EXPORTING im_value = ls_bapi_importext-data
            IMPORTING ex_container = lv_string ).

          CLEAR ls_data_import.
          ls_data_import-field1(30) = 'ZARN_LEGACY_STRUCT'.
          ls_data_import+30 = lv_string.



          CLEAR ls_importextension.
          ls_importextension-structure  = 'ZARN_LEGACY_STRUCT'.
          ls_importextension-material   = fu_s_headdata-material.
          ls_importextension-field1     = ls_data_import-field1.
          ls_importextension-field2     = ls_data_import-field2.
          ls_importextension-field3     = ls_data_import-field3.
          ls_importextension-field4     = ls_data_import-field4.

          APPEND ls_importextension TO fc_t_importextension[].




* Update latest product in host
          ls_host_sap_latest_new-latest = abap_true.


          CLEAR ls_bapi_importext.
          ls_bapi_importext-structure   = 'ZARN_LEGACY_STRUCT'.
          ls_bapi_importext-material    = fu_s_headdata-material.

          MOVE-CORRESPONDING ls_host_sap_latest_new TO ls_bapi_importext-data.

*          ls_bapi_importext-ersda = sy-datum.
*          ls_bapi_importext-erzet = sy-uzeit.
*          ls_bapi_importext-ernam = sy-uname.




          READ TABLE fc_t_legacy_struct[] ASSIGNING <ls_bapi_importext>
          WITH KEY material = ls_bapi_importext-material
                   product  = ls_bapi_importext-product.
          IF sy-subrc = 0.
            <ls_bapi_importext> = ls_bapi_importext.
          ELSE.
            APPEND ls_bapi_importext TO fc_t_legacy_struct[].
          ENDIF.


          CLEAR lv_string.
          cl_abap_container_utilities=>fill_container_c(
            EXPORTING im_value = ls_bapi_importext-data
            IMPORTING ex_container = lv_string ).

          CLEAR ls_data_import.
          ls_data_import-field1(30) = 'ZARN_LEGACY_STRUCT'.
          ls_data_import+30 = lv_string.



          CLEAR ls_importextension.
          ls_importextension-structure  = 'ZARN_LEGACY_STRUCT'.
          ls_importextension-material   = fu_s_headdata-material.
          ls_importextension-field1     = ls_data_import-field1.
          ls_importextension-field2     = ls_data_import-field2.
          ls_importextension-field3     = ls_data_import-field3.
          ls_importextension-field4     = ls_data_import-field4.

          APPEND ls_importextension TO fc_t_importextension[].




        ENDIF.



      ENDIF.  " IF lv_issue_uom NE ls_addnlclientdata-issue_unit

    ENDIF.  " if latest found
  ENDIF.  " IF sy-subrc = 0 AND ls_addnlclientdata-material = fu_s_headdata-material




* Add other Host Products - wont be impacted
  LOOP AT fu_t_host_sap[] INTO ls_host_sap.

    CLEAR ls_bapi_importext.
    READ TABLE fc_t_legacy_struct[] INTO ls_bapi_importext
    WITH KEY material = ls_host_sap-matnr
             product  = ls_host_sap-product.
    IF sy-subrc NE 0.
      CLEAR ls_bapi_importext.
      ls_bapi_importext-structure   = 'ZARN_LEGACY_STRUCT'.
      ls_bapi_importext-material    = ls_host_sap-matnr.

      MOVE-CORRESPONDING ls_host_sap TO ls_bapi_importext-data.

      APPEND ls_bapi_importext TO fc_t_legacy_struct[].
    ENDIF.
  ENDLOOP.





  CLEAR : fc_t_importextension[].
  LOOP AT fc_t_legacy_struct[] INTO ls_bapi_importext.

    CLEAR lv_string.
    cl_abap_container_utilities=>fill_container_c(
      EXPORTING im_value = ls_bapi_importext-data
      IMPORTING ex_container = lv_string ).

    CLEAR ls_data_import.
    ls_data_import-field1(30) = 'ZARN_LEGACY_STRUCT'.
    ls_data_import+30 = lv_string.



    CLEAR ls_importextension.
    ls_importextension-structure  = 'ZARN_LEGACY_STRUCT'.
    ls_importextension-material   = fu_s_headdata-material.
    ls_importextension-field1     = ls_data_import-field1.
    ls_importextension-field2     = ls_data_import-field2.
    ls_importextension-field3     = ls_data_import-field3.
    ls_importextension-field4     = ls_data_import-field4.

    APPEND ls_importextension TO fc_t_importextension[].
  ENDLOOP.



ENDFORM.                    " BUILD_IMPORTEXTENSION_UPDATE
*&---------------------------------------------------------------------*
*&      Form  UPDATE_NAT_REG_STATUS_VERSION
*&---------------------------------------------------------------------*
* Update National/Regional status and Version
*----------------------------------------------------------------------*
FORM update_nat_reg_status_version USING fu_t_prd_version    TYPE ty_t_prd_version
                                         fu_t_ver_status     TYPE ty_t_ver_status
                                         fu_t_reg_hdr        TYPE ztarn_reg_hdr
                                         fu_t_idno_data_suc  TYPE ty_t_idno_data
                                         fu_r_status         TYPE ztarn_status_range
                                         fu_v_error          TYPE flag
                                         fu_v_no_change_doc  TYPE flag.


  DATA: lt_prd_version_o TYPE ztarn_prd_version,
        lt_ver_status_o  TYPE ztarn_ver_status,
        lt_reg_hdr_o     TYPE ty_t_reg_hdr,
        lt_prd_version_n TYPE ztarn_prd_version,
        lt_ver_status_n  TYPE ztarn_ver_status,
        lt_reg_hdr_n     TYPE ztarn_reg_hdr,

        ls_prd_version_o TYPE zarn_prd_version,
        ls_ver_status_o  TYPE zarn_ver_status,
        ls_reg_hdr_o     TYPE ty_s_reg_hdr,
        ls_prd_version_n TYPE zarn_prd_version,
        ls_ver_status_n  TYPE zarn_ver_status,
        ls_reg_hdr_n     TYPE zarn_reg_hdr,

        lt_idno_data_suc TYPE ty_t_idno_data,
        ls_idno_data_suc TYPE ty_s_idno_data,
        lr_status        TYPE ztarn_status_range,

        lt_reg_data_o    TYPE ztarn_reg_data,
        lt_reg_data      TYPE ztarn_reg_data,
        ls_reg_data      TYPE zsarn_reg_data,

        lt_prod_data     TYPE ztarn_prod_data,
        ls_prod_data     TYPE zsarn_prod_data,

        lv_change        TYPE flag.



  lr_status[]        = fu_r_status[].
  lt_idno_data_suc[] = fu_t_idno_data_suc[].

  LOOP AT lt_idno_data_suc[] INTO ls_idno_data_suc.

    CLEAR ls_prod_data.
    ls_prod_data-idno = ls_idno_data_suc-idno.
    APPEND ls_prod_data TO lt_prod_data[].


* Create Old/New Version status table
    CLEAR lv_change.
    CLEAR ls_ver_status_o.
    READ TABLE fu_t_ver_status[] INTO ls_ver_status_o
    WITH TABLE KEY idno = ls_idno_data_suc-idno.
    IF sy-subrc = 0.

      APPEND ls_ver_status_o TO lt_ver_status_o[].

      CLEAR ls_ver_status_n.
      ls_ver_status_n = ls_ver_status_o.

      ls_ver_status_n-article_ver  = ls_idno_data_suc-current_ver.
      ls_ver_status_n-changed_on   = sy-datum.
      ls_ver_status_n-changed_at   = sy-uzeit.
      ls_ver_status_n-changed_by   = sy-uname.

      APPEND ls_ver_status_n TO lt_ver_status_n[].

*    ELSE.
*      CONTINUE.
    ENDIF.


* Update status only if article creation/updation is fully successful, not partial
    CLEAR lv_change.
    IF ls_idno_data_suc-post_error IS INITIAL.

* Create Old/New Product Version table
      CLEAR ls_prd_version_o.
      READ TABLE fu_t_prd_version[] INTO ls_prd_version_o
      WITH TABLE KEY idno    = ls_idno_data_suc-idno
                     version = ls_idno_data_suc-current_ver.
      IF sy-subrc = 0.

        IF ls_prd_version_o-version_status IN lr_status[].

          APPEND ls_prd_version_o TO lt_prd_version_o[].

          CLEAR ls_prd_version_n.
          ls_prd_version_n = ls_prd_version_o.

* Update version and dates only when old version if not CRE
* Else nothing is updated in PRD_VERSION thus change date/time must not be updated
          IF ls_prd_version_n-version_status NE 'CRE'.
            ls_prd_version_n-version_status = 'CRE'.
            ls_prd_version_n-changed_on     = sy-datum.
            ls_prd_version_n-changed_at     = sy-uzeit.
            ls_prd_version_n-changed_by     = sy-uname.
          ENDIF.

* PC 7/12/16 In Arena SLA reporting we need to be able to track when a product version
* is first posted to the article master. We therefore introduce posted_xx fields
          IF ls_prd_version_o-posted_on IS INITIAL.
            ls_prd_version_n-posted_on = sy-datum.
            ls_prd_version_n-posted_at = sy-uzeit.
            ls_prd_version_n-posted_by = sy-uname.
          ENDIF.

          APPEND ls_prd_version_n TO lt_prd_version_n[].

        ENDIF.  " IF ls_prd_version_o-version_status IN lr_status[]

*    ELSE.
*      CONTINUE.
      ENDIF.

    ENDIF.  " IF ls_idno_data_suc-post_error IS INITIAL


* Create Old/New Regional Header table
    CLEAR lv_change.
    CLEAR: lt_reg_hdr_o[], lt_reg_hdr_n[].
    CLEAR ls_reg_hdr_o.
    READ TABLE fu_t_reg_hdr[] INTO ls_reg_hdr_o
    WITH KEY idno    = ls_idno_data_suc-idno.
    IF sy-subrc = 0.

      IF ls_reg_hdr_o-status IN lr_status[].

        INSERT ls_reg_hdr_o INTO TABLE lt_reg_hdr_o[].
        CLEAR ls_reg_data.
        ls_reg_data-idno         = ls_idno_data_suc-idno.
        ls_reg_data-zarn_reg_hdr = lt_reg_hdr_o[].
        APPEND ls_reg_data TO lt_reg_data_o[].


        CLEAR ls_reg_hdr_n.
        ls_reg_hdr_n = ls_reg_hdr_o.

        IF ls_reg_hdr_n-version NE ls_idno_data_suc-current_ver.
          ls_reg_hdr_n-version = ls_idno_data_suc-current_ver.
          lv_change = abap_true.
        ENDIF.

        IF ls_reg_hdr_n-matnr NE ls_idno_data_suc-sap_article.
          ls_reg_hdr_n-matnr = ls_idno_data_suc-sap_article.
          lv_change = abap_true.
        ENDIF.

* Update status only if article creation/updation is fully successful, not partial
        IF ls_idno_data_suc-post_error IS INITIAL.
          IF ls_reg_hdr_n-status NE 'CRE'.
            ls_reg_hdr_n-status = 'CRE'.
            lv_change = abap_true.
          ENDIF.
        ENDIF.

        IF lv_change = abap_true.
          ls_reg_hdr_n-laeda       = sy-datum.
          ls_reg_hdr_n-eruet       = sy-uzeit.
          ls_reg_hdr_n-aenam       = sy-uname.
        ENDIF.

        INSERT ls_reg_hdr_n INTO TABLE lt_reg_hdr_n[].


        CLEAR ls_reg_data.
        ls_reg_data-idno         = ls_idno_data_suc-idno.
        ls_reg_data-zarn_reg_hdr = lt_reg_hdr_n[].
        APPEND ls_reg_data TO lt_reg_data[].



      ENDIF.  " IF ls_reg_hdr_o-version_status IN lr_status[]

*    ELSE.
*      CONTINUE.
    ENDIF.

  ENDLOOP.  " LOOP AT lt_idno_data_suc[] INTO ls_idno_data_suc




* Update Version and Control tables
  CALL FUNCTION 'ZARN_VERSION_CONTROL_UPDATE'
    EXPORTING
*     it_control     =
      it_prd_version = lt_prd_version_n[]
      it_ver_status  = lt_ver_status_n[].


* Update Regional Tables - Regional Header
  CALL FUNCTION 'ZARN_REGIONAL_DATA_UPDATE'
    EXPORTING
      it_reg_data = lt_reg_data[].


* Write Change Document for Control and Regional Data
  IF fu_v_no_change_doc EQ space.

    CALL FUNCTION 'ZARN_CTRL_REGNL_CHANGE_DOC'
      EXPORTING
*       IT_CONTROL         =
        it_prd_version     = lt_prd_version_n[]
        it_ver_status      = lt_ver_status_n[]
        it_reg_data        = lt_reg_data[]
        it_prod_data       = lt_prod_data[]
*       IT_CONTROL_OLD     =
        it_prd_version_old = lt_prd_version_o[]
        it_ver_status_old  = lt_ver_status_o[]
        it_reg_data_old    = lt_reg_data_o[]
*       IV_CI_CONTROL      = ' '
        iv_ci_prd_version  = 'X'
        iv_ci_ver_status   = 'X'
        iv_ci_reg_hdr      = 'X'.
  ENDIF.




ENDFORM.                    " UPDATE_NAT_REG_STATUS_VERSION
*&---------------------------------------------------------------------*
*&      Form  GET_LOWER_CHILD_UOM
*&---------------------------------------------------------------------*
* Get lower uom and lower child uom
*----------------------------------------------------------------------*
FORM get_lower_child_uom  USING    fu_s_idno_data       TYPE ty_s_idno_data
                                   fu_s_reg_uom         TYPE zarn_reg_uom
                                   fu_t_uom_variant     TYPE ztarn_uom_variant
                                   fu_t_gtin_var        TYPE ztarn_gtin_var
                                   fu_t_pir             TYPE ztarn_pir
                          CHANGING fc_v_lower_uom       TYPE zarn_lower_uom
                                   fc_v_lower_child_uom TYPE zarn_lower_child_uom.


  DATA: ls_uom_variant TYPE zarn_uom_variant,
        ls_gtin_var    TYPE zarn_gtin_var,
        ls_pir         TYPE zarn_pir.

  CLEAR : fc_v_lower_uom, fc_v_lower_child_uom.

  TRANSLATE fu_s_reg_uom-uom_category TO UPPER CASE.

  CASE fu_s_reg_uom-uom_category.

    WHEN gc_uom_cat-inner OR gc_uom_cat-shipper OR gc_uom_cat-retail.

      CLEAR ls_uom_variant.
      READ TABLE fu_t_uom_variant[] INTO ls_uom_variant
      WITH KEY idno     = fu_s_idno_data-idno
               version  = fu_s_idno_data-current_ver
               uom_code = fu_s_reg_uom-pim_uom_code.
      IF sy-subrc = 0 AND ls_uom_variant-child_gtin IS NOT INITIAL.

        CLEAR ls_gtin_var.
        READ TABLE fu_t_gtin_var[] INTO ls_gtin_var
        WITH KEY idno      = fu_s_idno_data-idno
                 version   = fu_s_idno_data-current_ver
                 gtin_code = ls_uom_variant-child_gtin.
        IF sy-subrc = 0.
* Lower UOM
          fc_v_lower_uom = ls_gtin_var-uom_code.



          "" Process same for lower child uom

          CLEAR ls_uom_variant.
          READ TABLE fu_t_uom_variant[] INTO ls_uom_variant
          WITH KEY idno     = fu_s_idno_data-idno
                   version  = fu_s_idno_data-current_ver
                   uom_code = ls_gtin_var-uom_code.
          IF sy-subrc = 0 AND ls_uom_variant-child_gtin IS NOT INITIAL.
            CLEAR ls_gtin_var.
            READ TABLE fu_t_gtin_var[] INTO ls_gtin_var
            WITH KEY idno      = fu_s_idno_data-idno
                     version   = fu_s_idno_data-current_ver
                     gtin_code = ls_uom_variant-child_gtin.
            IF sy-subrc = 0.
* Lower Child UOM
              fc_v_lower_child_uom = ls_gtin_var-uom_code.
            ENDIF.  " READ TABLE fu_t_gtin_var[] INTO ls_gtin_var - lower child

          ENDIF.  " READ TABLE fu_t_uom_variant[] INTO ls_uom_variant - lower child
        ENDIF.   " READ TABLE fu_t_gtin_var[] INTO ls_gtin_var - lower
      ENDIF.  " READ TABLE fu_t_uom_variant[] INTO ls_uom_variant - lower



    WHEN gc_uom_cat-layer OR gc_uom_cat-pallet.

      CLEAR ls_pir.
      READ TABLE fu_t_pir[] INTO ls_pir
      WITH KEY idno                  = fu_s_idno_data-idno
               version               = fu_s_idno_data-current_ver
               uom_code              = fu_s_reg_uom-pim_uom_code
               hybris_internal_code  = fu_s_reg_uom-hybris_internal_code.
      IF sy-subrc = 0.

        IF ls_pir-qty_per_pallet_layer IS NOT INITIAL OR
           ls_pir-qty_layers_per_pallet IS NOT INITIAL OR
           ls_pir-qty_units_per_pallet IS NOT INITIAL.

          "" Process same for lower child uom

          CLEAR ls_uom_variant.
          READ TABLE fu_t_uom_variant[] INTO ls_uom_variant
          WITH KEY idno     = fu_s_idno_data-idno
                   version  = fu_s_idno_data-current_ver
                   uom_code = fu_s_reg_uom-pim_uom_code.
          IF sy-subrc = 0 AND ls_uom_variant-child_gtin IS NOT INITIAL.
            CLEAR ls_gtin_var.
            READ TABLE fu_t_gtin_var[] INTO ls_gtin_var
            WITH KEY idno      = fu_s_idno_data-idno
                     version   = fu_s_idno_data-current_ver
                     gtin_code = ls_uom_variant-child_gtin.
            IF sy-subrc = 0.
* Lower Child UOM
              fc_v_lower_child_uom = ls_gtin_var-uom_code.
            ENDIF.  " READ TABLE fu_t_gtin_var[] INTO ls_gtin_var - lower child
          ENDIF.  " READ TABLE fu_t_uom_variant[] INTO ls_uom_variant - lower child
        ENDIF.  " ZARN_PIR Qty check - lower child

      ENDIF.    " READ TABLE fu_t_pir[] INTO ls_pir - lower child



  ENDCASE.

ENDFORM.                    " GET_LOWER_CHILD_UOM

*&---------------------------------------------------------------------*
*&      Form  POPUP_APPL_LOG
*&---------------------------------------------------------------------*
FORM popup_appl_log USING pt_message TYPE bal_t_msg.

  DATA: ls_log             TYPE bal_s_log,
        ls_display_profile TYPE bal_s_prof,
        lt_log_handle      TYPE bal_t_logh,
        lv_log_handle      TYPE balloghndl.

  FIELD-SYMBOLS: <ls_message> LIKE LINE OF pt_message.

  CHECK NOT pt_message[] IS INITIAL.

  ls_log-object    = 'ZARN'.
  ls_log-subobject = 'ZARN_ART_POST'.

  CALL FUNCTION 'BAL_LOG_CREATE'
    EXPORTING
      i_s_log                 = ls_log
    IMPORTING
      e_log_handle            = lv_log_handle
    EXCEPTIONS
      log_header_inconsistent = 1.

  IF sy-subrc EQ 0.
    CALL FUNCTION 'BAL_LOG_REFRESH'
      EXPORTING
        i_log_handle  = lv_log_handle
      EXCEPTIONS
        log_not_found = 1.
    LOOP AT pt_message ASSIGNING <ls_message>.
      CALL FUNCTION 'BAL_LOG_MSG_ADD'
        EXPORTING
          i_log_handle     = lv_log_handle
          i_s_msg          = <ls_message>
        EXCEPTIONS
          log_not_found    = 1
          msg_inconsistent = 2
          log_is_full      = 3.
    ENDLOOP.

    CALL FUNCTION 'BAL_DSP_PROFILE_POPUP_GET'
*     EXPORTING
*       START_COL                 = 5
*       START_ROW                 = 5
*       END_COL                   = 87
*       END_ROW                   = 25
      IMPORTING
        e_s_display_profile = ls_display_profile.

    CALL FUNCTION 'BAL_DSP_LOG_DISPLAY'
      EXPORTING
        i_s_display_profile  = ls_display_profile
        i_t_log_handle       = lt_log_handle
*       I_AMODAL             = ' '
      EXCEPTIONS
        profile_inconsistent = 1
        internal_error       = 2
        no_data_available    = 3
        no_authority         = 4.
  ENDIF.

ENDFORM.                    " POPUP_APPL_LOG
*&---------------------------------------------------------------------*
*&      Form  UPDATE_PIR_ALL_SITES
*&---------------------------------------------------------------------*
* Update Cond group for all sites
*----------------------------------------------------------------------*
FORM update_pir_all_sites USING fu_t_reg_pir   TYPE ztarn_reg_pir
                                fu_t_eina      TYPE ty_t_eina
                                fu_t_eine_site TYPE ty_t_eine
                                fu_s_idno_data TYPE ty_s_idno_data
                                fu_v_batch TYPE flag
                       CHANGING fc_t_message   TYPE bal_t_msg.

  DATA: lt_eina       TYPE ty_t_eina,
        lt_eine       TYPE ty_t_eine,
        lt_reg_pir    TYPE ztarn_reg_pir,
        ls_reg_pir    TYPE zarn_reg_pir,
        ls_return     TYPE bapireturn1,
        ls_log_params TYPE zsarn_slg1_log_params_art_post,

        ls_eina_new   TYPE eina,
        ls_eine_new   TYPE eine,
        ls_eina_old   TYPE eina,
        ls_eine_old   TYPE eine,
        ls_eina       TYPE eina,

        ls_weine      TYPE weine,
        lt_cdtxt      TYPE TABLE OF cdtxt,
        lt_xlfei      TYPE TABLE OF ulfei,
        lt_ylfei      TYPE TABLE OF ulfei,
        lv_objectid   TYPE cdhdr-objectid,
        lv_date       TYPE cdhdr-udate,
        lv_time       TYPE cdhdr-utime.



  IF fu_t_eina[] IS INITIAL.
    RETURN.
  ENDIF.

  lt_eina[]    = fu_t_eina[].
  lt_eine[]    = fu_t_eine_site[].
  lt_reg_pir[] = fu_t_reg_pir[].




  DELETE lt_reg_pir[] WHERE pir_rel_eine IS INITIAL.

  IF lt_reg_pir[] IS INITIAL.
    RETURN.
  ENDIF.


  LOOP AT lt_reg_pir[] INTO ls_reg_pir.

    CLEAR: ls_eina_new, ls_eine_new, ls_eina_old, ls_eine_old.

    READ TABLE lt_eina INTO ls_eina_old
    WITH TABLE KEY matnr = fu_s_idno_data-sap_article
                   lifnr = ls_reg_pir-lifnr.
    IF sy-subrc = 0.

      ls_eina_new = ls_eina_old.

      LOOP AT lt_eine INTO ls_eine_old
        WHERE infnr = ls_eina_old-infnr.

        ls_eine_new = ls_eine_old.

        IF ls_eine_old-ekkol NE ls_reg_pir-ekkol.
          ls_eine_new-ekkol = ls_reg_pir-ekkol.
        ELSE.
          CONTINUE.
        ENDIF.

* Update Inforecord
        CLEAR ls_eina.
        CALL FUNCTION 'ME_UPDATE_INFORECORD'
          EXPORTING
            xeina         = ls_eina_new
            xeine         = ls_eine_new
            yeina         = ls_eina_old
            yeine         = ls_eine_old
            reg_eina      = ls_eina
          EXCEPTIONS
            error_message = 1.
        IF sy-subrc = 0.

          COMMIT WORK AND WAIT.

****   use MEII function modules to create/change inforecords
*          CALL FUNCTION 'ME_MAINTAIN_INFORECORD'
*            EXPORTING
*              activity           = 'V'
*              i_eina             = ls_eina_new
*              i_eine             = ls_eine_new
*              o_eina             = ls_eina_old
*              o_eine             = ls_eine_old
*              i_no_suppose       = 'X'
*              i_no_material_read = 'X'
*              i_mt06e            = ls_mt06e
*              i_vorga            = 'A'
*            IMPORTING
*              e_eina             = ls_eina_new
*            EXCEPTIONS
*              error_message      = 1
*              OTHERS             = 2.
*          IF sy-subrc = 0.
*
*            CLEAR: lt_eina_db[].
*            CALL FUNCTION 'ME_POST_INFORECORD'
*              TABLES
*                t_eina_i      = lt_eina_db
*              EXCEPTIONS
*                error_message = 1
*                OTHERS        = 2.
*            IF sy-subrc = 0.
*
*              CALL FUNCTION 'BAPI_TRANSACTION_COMMIT'
*                EXPORTING
*                  wait = abap_true.
*
*
*
          CLEAR ls_return.
          ls_return-type       = 'S'.
          ls_return-id         = 'WRF_MATERIAL'.
          ls_return-number     = '035'.

          MESSAGE ID ls_return-id TYPE ls_return-type NUMBER ls_return-number
          WITH ls_eine_new-infnr ls_eine_new-ekorg ls_eine_new-esokz ls_eine_new-werks
          INTO ls_return-message.

          ls_return-message_v1 = ls_eine_new-infnr.
          ls_return-message_v2 = ls_eine_new-ekorg.
          ls_return-message_v3 = ls_eine_new-esokz.
          ls_return-message_v4 = ls_eine_new-werks.

* Build Return Message
          CLEAR: ls_log_params.

          ls_log_params-idno        = fu_s_idno_data-idno.
          ls_log_params-fanid       = fu_s_idno_data-fan_id.
          ls_log_params-version     = fu_s_idno_data-current_ver.
          ls_log_params-nat_status  = fu_s_idno_data-nat_status.
          ls_log_params-reg_status  = fu_s_idno_data-reg_status.
          ls_log_params-saparticle  = fu_s_idno_data-sap_article.
          ls_log_params-batch_mode  = fu_v_batch.


          PERFORM build_message USING ls_return-type
                                      ls_return-id
                                      ls_return-number
                                      ls_return-message_v1
                                      ls_return-message_v2
                                      ls_return-message_v3
                                      ls_return-message_v4
                                      ls_log_params
                             CHANGING fc_t_message[].


          lv_objectid = ls_eina_new-infnr. " Inforecord number
          lv_date     = sy-datum.
          lv_time     = sy-uzeit.

* Maintain Change Document
          CALL FUNCTION 'INFOSATZ_WRITE_DOCUMENT'
            EXPORTING
              objectid            = lv_objectid
              tcode               = sy-tcode
              utime               = lv_time
              udate               = lv_date
              username            = sy-uname
              upd_icdtxt_infosatz = space
              n_eina              = ls_eina_new
              o_yeina             = ls_eina_old
              upd_eina            = 'U'
              n_eine              = ls_eine_new
              o_yeine             = ls_eine_old
              o_weine             = ls_weine
              n_weine             = ls_weine
              upd_eine            = 'U'
              upd_lfei            = space
            TABLES
              icdtxt_infosatz     = lt_cdtxt
              xlfei               = lt_xlfei
              ylfei               = lt_ylfei.

*            ENDIF.  " IF sy-subrc = 0 - ME_MAINTAIN_INFORECORD  ++INC5272626 JKH 23.01.2017
*          ENDIF.  " IF sy-subrc = 0 - ME_POST_INFORECORD        ++INC5272626 JKH 23.01.2017
        ENDIF.  " IF sy-subrc = 0 - ME_UPDATE_INFORECORD



      ENDLOOP.  " LOOP AT lt_eine INTO ls_eine_old
    ENDIF.    " READ TABLE lt_eina INTO ls_eina_old
  ENDLOOP.  " LOOP AT lt_reg_pir[] INTO ls_reg_pir

ENDFORM.

FORM add_deleted_cluster    USING iv_idno        TYPE zarn_idno
                         CHANGING ct_reg_cluster TYPE ztarn_reg_cluster.

  " When the existing cluster range is deleted, we need to delete layout assignment as well. Get
  " the deletion indicator from change log and add blank cluster entry which would delete existing
  " assignments

  DATA: lv_created_on_start TYPE zarn_chg_cat_cre_on,
        lv_created_on_end   TYPE zarn_chg_cat_cre_on.

  lv_created_on_start = |{ sy-datum }000000|.
  lv_created_on_end   = |{ sy-datum }235959|.

  SELECT i~chgid,
         i~idno,
         i~reference_data,
         i~chg_ind
    FROM zarn_cc_det AS i
    INNER JOIN zarn_cc_hdr AS h ON h~chgid        = i~chgid AND
                                   h~idno         = i~idno  AND
                                   h~chg_category = i~chg_category AND
                                   h~chg_area     = i~chg_area
    WHERE i~idno = @iv_idno
      AND h~chg_cat_cre_on BETWEEN @lv_created_on_start AND @lv_created_on_end
      AND i~chg_table = 'ZARN_REG_CLUSTER'
      AND i~chg_field = 'KEY'
    INTO TABLE @DATA(lt_cc_det)."#EC CI_SEL_NESTED

  " Keep only latest and deleted entries
  SORT lt_cc_det BY idno reference_data chgid DESCENDING.
  DELETE ADJACENT DUPLICATES FROM lt_cc_det COMPARING idno reference_data.
  DELETE lt_cc_det WHERE chg_ind <> 'D'.
  LOOP AT lt_cc_det INTO DATA(ls_cc_det).
    APPEND INITIAL LINE TO ct_reg_cluster ASSIGNING FIELD-SYMBOL(<ls_reg_cluster>).
    <ls_reg_cluster>-idno   = iv_idno.
    <ls_reg_cluster>-banner = ls_cc_det-reference_data.
  ENDLOOP.

ENDFORM.
