FUNCTION zarn_set_get_article_post.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(IV_MODE) TYPE  CHAR1
*"     REFERENCE(IV_ARENA) TYPE  CHAR1 OPTIONAL
*"     REFERENCE(IV_ARENA_SUCCESS) TYPE  CHAR1 OPTIONAL
*"     REFERENCE(IS_PIR_KEY) TYPE  ZSARN_KEY OPTIONAL
*"     REFERENCE(IT_INFORECORD_GENERAL) TYPE  WRF_BAPIEINA_TTY OPTIONAL
*"     REFERENCE(IT_INFORECORD_PURCHORG) TYPE  WRF_BAPIEINE_TTY
*"       OPTIONAL
*"     REFERENCE(IS_EINA_NEW) TYPE  EINA OPTIONAL
*"     REFERENCE(IS_EINE_NEW) TYPE  EINE OPTIONAL
*"     REFERENCE(IS_EINA_OLD) TYPE  EINA OPTIONAL
*"     REFERENCE(IS_EINE_OLD) TYPE  EINE OPTIONAL
*"  EXPORTING
*"     REFERENCE(EV_ARENA) TYPE  CHAR1
*"     REFERENCE(EV_ARENA_SUCCESS) TYPE  CHAR1
*"     REFERENCE(ES_PIR_KEY) TYPE  ZSARN_KEY
*"     REFERENCE(ET_INFORECORD_GENERAL) TYPE  WRF_BAPIEINA_TTY
*"     REFERENCE(ET_INFORECORD_PURCHORG) TYPE  WRF_BAPIEINE_TTY
*"     REFERENCE(ET_EINA_NEW) TYPE  MMPUR_T_EINA
*"     REFERENCE(ET_EINE_NEW) TYPE  MMPUR_T_EINE
*"     REFERENCE(ET_EINA_OLD) TYPE  MMPUR_T_EINA
*"     REFERENCE(ET_EINE_OLD) TYPE  MMPUR_T_EINE
*"----------------------------------------------------------------------

*-----------------------------------------------------------------------
* PROJECT          # Lightning stabilisation
* SPECIFICATION    # Feature 3133 - AReNA to ECC Update
* DATE WRITTEN     # 29/03/2016
* SAP VERSION      # SAPK-606 03 (SAP Enterprise Extension Retail)
* TYPE             #
* AUTHOR           # Jitin Kharbanda
*-----------------------------------------------------------------------
* TITLE            # Article Posting
* PURPOSE          # See FM ZARN_ARTICLE_POSTING for details
*                  # Function module to switch-on/off logic in
*                  # Article BADI Implementations
*-----------------------------------------------------------------------
* CALLED FROM      # DE1 system
*                  #
*-----------------------------------------------------------------------
* CALLS TO         #
*-----------------------------------------------------------------------

*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  STATICS: lv_arena_static          TYPE char1.
  STATICS: ls_pir_key_static        TYPE zsarn_key.
  STATICS: lt_pir_general_static    TYPE wrf_bapieina_tty.
  STATICS: lt_pir_purchorg_static   TYPE wrf_bapieine_tty.
  STATICS: lv_arena_succcess_static TYPE char1.

  STATICS: lt_eina_new              TYPE mmpur_t_eina.
  STATICS: lt_eine_new              TYPE mmpur_t_eine.
  STATICS: lt_eina_old              TYPE mmpur_t_eina.
  STATICS: lt_eine_old              TYPE mmpur_t_eine.

* default = nothing to return
  CLEAR ev_arena.

  CASE iv_mode.
    WHEN 'W'. "write
      lv_arena_static = iv_arena.


      IF iv_arena = 'X'.
        IF is_pir_key IS SUPPLIED.
          ls_pir_key_static        = is_pir_key.
        ENDIF.

        IF it_inforecord_general[] IS SUPPLIED.
          lt_pir_general_static[]  = it_inforecord_general[].
        ENDIF.

        IF it_inforecord_purchorg[] IS SUPPLIED.
          lt_pir_purchorg_static[] = it_inforecord_purchorg[].
        ENDIF.

        IF iv_arena_success IS SUPPLIED.
          lv_arena_succcess_static = iv_arena_success.
        ENDIF.

        IF is_eina_new IS SUPPLIED.
          APPEND is_eina_new TO lt_eina_new[].
        ENDIF.

        IF is_eine_new IS SUPPLIED.
          APPEND is_eine_new TO lt_eine_new[].
        ENDIF.

        IF is_eina_old IS SUPPLIED.
          APPEND is_eina_old TO lt_eina_old[].
        ENDIF.

        IF is_eine_old IS SUPPLIED.
          APPEND is_eine_old TO lt_eine_old[].
        ENDIF.

      ELSE.
        CLEAR: ls_pir_key_static, lt_pir_general_static[], lt_pir_purchorg_static[], lv_arena_succcess_static,
               lt_eina_new[], lt_eine_new[], lt_eina_old[], lt_eine_old[].

      ENDIF.

    WHEN 'R'. "Read
      ev_arena                 = lv_arena_static.
      ev_arena_success         = lv_arena_succcess_static.
      es_pir_key               = ls_pir_key_static.
      et_inforecord_general[]  = lt_pir_general_static[].
      et_inforecord_purchorg[] = lt_pir_purchorg_static[].
      et_eina_new[]            = lt_eina_new[].
      et_eine_new[]            = lt_eine_new[].
      et_eina_old[]            = lt_eina_old[].
      et_eine_old[]            = lt_eine_old[].


    WHEN OTHERS.

  ENDCASE.

ENDFUNCTION.
