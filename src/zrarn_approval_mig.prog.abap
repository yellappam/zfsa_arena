REPORT zrarn_approval_mig.
*
****************MIGRATION OBJECT ONLY****************
*"populates new redesigned AReNA approval tables
*"from old ones
*
*
*
*TYPES: BEGIN OF ty_s_mig.
*        INCLUDE TYPE zsarn_appr_report.
*TYPES: chgid   TYPE zarn_chg_id,
*       version TYPE zarn_version,
*       END OF ty_s_mig.
*
*DATA:
*  it_data             TYPE STANDARD TABLE OF ty_s_mig,
*  lt_cchdr            TYPE ztarn_cc_hdr,
*  lt_idno_skip_aa_reg TYPE ztarn_idno_key.
*
*TABLES zsarn_appr_report.
**-------------------------------------------------------------------*
**   selection screen statements
**-------------------------------------------------------------------*
**   (define your selection-screen here)
*SELECTION-SCREEN BEGIN OF BLOCK b1.
*
*SELECT-OPTIONS:
*     s_fan    FOR zsarn_appr_report-fan_id MATCHCODE OBJECT zmd_zzfan,
*     s_hid    FOR zsarn_appr_report-idno,
*     s_credat FOR zsarn_appr_report-date_in NO-EXTENSION OBLIGATORY,
*     s_catman FOR zsarn_appr_report-ret_zzcatman NO INTERVALS MATCHCODE OBJECT zhmd_role_id.
*
*SELECTION-SCREEN END OF BLOCK b1.
*
** !! the following comment MUST NOT BE CHANGED !!
**<QUERY_HEAD>
*
*
*
*START-OF-SELECTION.
*
**-------------------------------------------------------------------*
**   read data into IT_DATA
**-------------------------------------------------------------------*
**  (select your data here into internal table IT_DATA)
*  SELECT p~idno p~fan_id p~description r~matkl r~ret_zzcatman r~gil_zzcatman
*         c~date_in v~version_status v~release_status
*         r~status AS status_r r~release_status AS release_status_r
*         t~chg_category t~chg_area t~team_code t~upd_on t~status t~chgid
*         p~version
*            INTO CORRESPONDING FIELDS OF TABLE it_data
*            FROM zarn_control AS c
*            INNER JOIN zarn_prd_version AS v
*              ON  c~guid    EQ v~guid
*            INNER JOIN zarn_ver_status AS s " added
*              ON  s~idno         EQ v~idno
*              AND s~current_ver  EQ v~version
*            INNER JOIN zarn_products AS p
*              ON  p~idno    EQ v~idno
*              AND p~version EQ s~current_ver
*            INNER JOIN zarn_reg_hdr AS r
*              ON  r~idno    EQ p~idno
*            INNER JOIN zarn_cc_team AS t
*              ON t~idno     EQ r~idno
*             AND t~chg_area EQ 'R'
*            WHERE c~date_in          IN s_credat[]
*              AND p~fan_id           IN s_fan[]
*              AND p~fan_id           NE space
*              AND p~fsni_icare_value EQ abap_true
*              AND p~idno             IN s_hid[]
*              AND ( r~ret_zzcatman   IN s_catman
*              OR    r~gil_zzcatman   IN s_catman )
*              AND t~status           NE 'N'.
*
*  SELECT p~idno p~fan_id p~description r~matkl r~ret_zzcatman r~gil_zzcatman
*         c~date_in v~version_status v~release_status
*         r~status AS status_r r~release_status AS release_status_r
*         t~chg_category t~chg_area t~team_code t~upd_on t~status t~chgid
*         p~version
*            APPENDING CORRESPONDING FIELDS OF TABLE it_data
*            FROM zarn_control AS c
*            INNER JOIN zarn_prd_version AS v
*              ON  c~guid    EQ v~guid
*            INNER JOIN zarn_ver_status AS s " added
*              ON  s~idno         EQ v~idno
*              AND s~current_ver  EQ v~version
*            INNER JOIN zarn_products AS p
*              ON  p~idno    EQ v~idno
*              AND p~version EQ s~current_ver
*            INNER JOIN zarn_reg_hdr AS r
*              ON  r~idno    EQ p~idno
*            INNER JOIN zarn_cc_hdr AS h
*              ON  h~idno    EQ r~idno
*              AND h~national_ver_id EQ s~current_ver
*              AND h~chg_area EQ 'N'
*            INNER JOIN zarn_cc_team AS t
*              ON  t~chgid    EQ h~chgid
*              AND t~idno     EQ h~idno
*            WHERE c~date_in          IN s_credat[]
*              AND p~fan_id           IN s_fan[]
*              AND p~fan_id           NE space
*              AND p~fsni_icare_value EQ abap_true
*              AND p~idno             IN s_hid[]
*              AND ( r~ret_zzcatman   IN s_catman
*              OR    r~gil_zzcatman   IN s_catman )
*              AND t~status           NE 'N'.
*
*  "keep only pending or rejected records
*
*  DELETE it_data WHERE NOT ( status EQ 'P'
*                          OR status EQ 'R' ).
*
*  lt_cchdr = CORRESPONDING #( it_data MAPPING chg_category = chg_category
*                                              chgid        = chgid
*                                              idno         = idno
*                                              chg_area     = chg_area
*                                              national_ver_id = version  ).
*
*  SORT lt_cchdr BY idno chg_area chg_category chgid.
*  DELETE ADJACENT DUPLICATES FROM lt_cchdr COMPARING idno chg_area chg_category chgid.
*
*  lt_idno_skip_aa_reg = CORRESPONDING #( lt_cchdr ).
*
*  SORT  lt_idno_skip_aa_reg BY idno.
*  DELETE ADJACENT DUPLICATES FROM lt_idno_skip_aa_reg COMPARING idno.
*
*  DATA(lo_approval) = NEW zcl_arn_approval_backend( iv_approval_event = zcl_arn_approval_backend=>gc_event_inbound ).
*
*  lo_approval->build_approvals_from_chg_bulk( EXPORTING it_zarn_cc_hdr  = lt_cchdr
*                                                        it_idno_val_err = lt_idno_skip_aa_reg
*                                              IMPORTING et_zarn_approval = DATA(lt_approval)
*                                                        et_zarn_approval_ch = DATA(lt_appr_ch)
*                                                        et_after_appr_actions = DATA(lt_after_act)
*                                                        et_zarn_appr_hist = DATA(lt_appr_hist) ).
*
*  lo_approval->post_approvals( EXPORTING it_approval = lt_approval
*                                         it_approval_ch = lt_appr_ch
*                                         it_appr_hist = lt_appr_hist ).
*
*  COMMIT WORK AND WAIT.
*
*  "change all actions to synchronous, dont want to trigger whole bunch of async. RFCs...
*
*  DATA ls_after_act LIKE LINE OF lt_after_act.
*  ls_after_act-sync_async = 'S'.
*
*  MODIFY lt_after_act FROM ls_after_act TRANSPORTING sync_async
*    WHERE sync_async = 'A'.
*
*  lo_approval->call_after_appr_actions(  lt_after_act ).
*
*  COMMIT WORK AND WAIT.
*
*  WRITE 'Migration Done'.
