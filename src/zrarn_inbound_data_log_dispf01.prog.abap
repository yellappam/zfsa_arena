*----------------------------------------------------------------------*
***INCLUDE ZRARN_INBOUND_DATA_LOG_DISPF01 .
*----------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*&      Form  PREPARE_FCAT_9000
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
FORM prepare_fcat_9000  CHANGING fc_t_fieldcat TYPE lvc_t_fcat
                                 fc_v_sub_obj  TYPE balsubobj.


* Declaration
  DATA: lt_fieldcat TYPE lvc_t_fcat,
        ls_fieldcat TYPE lvc_s_fcat.

  FIELD-SYMBOLS : <ls_fieldcat>    TYPE lvc_s_fcat.

* Prepare field catalog for ALV display by merging structure
  REFRESH: lt_fieldcat[].
  CALL FUNCTION 'LVC_FIELDCATALOG_MERGE'
    EXPORTING
      i_structure_name       = 'ZSARN_INBOUND_DATA_LOG_DISPLAY'
    CHANGING
      ct_fieldcat            = lt_fieldcat[]
    EXCEPTIONS
      inconsistent_interface = 1
      program_error          = 2
      OTHERS                 = 3.
  IF sy-subrc NE 0.
    REFRESH: fc_t_fieldcat.
  ENDIF.



* Looping to make the output field
  LOOP AT lt_fieldcat ASSIGNING <ls_fieldcat>.


    <ls_fieldcat>-tooltip = <ls_fieldcat>-scrtext_l.
    <ls_fieldcat>-col_opt = abap_true.

    CASE <ls_fieldcat>-fieldname.
      WHEN 'MSGTY_ICON'.

      WHEN 'MESSAGE'.
        CLEAR <ls_fieldcat>-col_opt.
        <ls_fieldcat>-outputlen = 30.

      WHEN 'LOGNUMBER'.

      WHEN 'GUID'.
        IF fc_v_sub_obj = 'ZARN_ART_POST' OR fc_v_sub_obj = 'ZARN_PROXY'.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

      WHEN 'CREATED_ON'.

      WHEN 'CREATED_AT'.

      WHEN 'CREATED_BY'.

      WHEN 'FAN_ID'.
        IF fc_v_sub_obj = 'ZARN_ART_POST' OR fc_v_sub_obj = 'ZARN_PROXY'.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

      WHEN 'ARN_MODE'.
        IF fc_v_sub_obj = 'ZARN_ART_POST' OR fc_v_sub_obj = 'ZARN_PROXY'.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

      WHEN 'CODE'.
        IF fc_v_sub_obj = 'ZARN_ART_POST' OR fc_v_sub_obj = 'ZARN_PROXY'.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

      WHEN 'MATKL'.
        IF fc_v_sub_obj = 'ZARN_ART_POST' OR fc_v_sub_obj = 'ZARN_PROXY'.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

      WHEN 'RET_BCODE'.
        IF fc_v_sub_obj = 'ZARN_ART_POST' OR fc_v_sub_obj = 'ZARN_PROXY'.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

      WHEN 'INN_BCODE'.
        IF fc_v_sub_obj = 'ZARN_ART_POST' OR fc_v_sub_obj = 'ZARN_PROXY'.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

      WHEN 'SHIP_BCODE'.
        IF fc_v_sub_obj = 'ZARN_ART_POST' OR fc_v_sub_obj = 'ZARN_PROXY'.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

      WHEN 'VEND_NAME'.
        IF fc_v_sub_obj = 'ZARN_ART_POST' OR fc_v_sub_obj = 'ZARN_PROXY'.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

      WHEN 'NAME'.
        IF fc_v_sub_obj = 'ZARN_ART_POST' OR fc_v_sub_obj = 'ZARN_PROXY'.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

      WHEN 'DESCR'.
        IF fc_v_sub_obj = 'ZARN_ART_POST' OR fc_v_sub_obj = 'ZARN_PROXY'.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

      WHEN 'FS_DESCR_S'.
        IF fc_v_sub_obj = 'ZARN_ART_POST' OR fc_v_sub_obj = 'ZARN_PROXY'.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

      WHEN 'BRANDID'.
        IF fc_v_sub_obj = 'ZARN_ART_POST' OR fc_v_sub_obj = 'ZARN_PROXY'.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

      WHEN 'SUB_BRNDID'.
        IF fc_v_sub_obj = 'ZARN_ART_POST' OR fc_v_sub_obj = 'ZARN_PROXY'.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

      WHEN 'FS_BRANDID'.
        IF fc_v_sub_obj = 'ZARN_ART_POST' OR fc_v_sub_obj = 'ZARN_PROXY'.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

*      WHEN 'FS_BRN_DSC'.
*        IF fc_v_sub_obj = 'ZARN_ART_POST'.
*          <ls_fieldcat>-no_out = abap_true.
*        ENDIF.

      WHEN 'FS_SBBRNID'.
        IF fc_v_sub_obj = 'ZARN_ART_POST' OR fc_v_sub_obj = 'ZARN_PROXY'.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

*      WHEN 'FS_SBBRNDS'.
*        IF fc_v_sub_obj = 'ZARN_ART_POST'.
*          <ls_fieldcat>-no_out = abap_true.
*        ENDIF.




**      WHEN 'IDNO'.
**        IF fc_v_sub_obj = 'ZARN_INBOUND'.
**          <ls_fieldcat>-no_out = abap_true.
**        ENDIF.
**
**      WHEN 'VERSION'.
**        IF fc_v_sub_obj = 'ZARN_INBOUND'.
**          <ls_fieldcat>-no_out = abap_true.
**        ENDIF.
**
**      WHEN 'FANID'.
**        IF fc_v_sub_obj = 'ZARN_INBOUND'.
**          <ls_fieldcat>-no_out = abap_true.
**        ENDIF.
**
**      WHEN 'NAT_STATUS'.
**        IF fc_v_sub_obj = 'ZARN_INBOUND'.
**          <ls_fieldcat>-no_out = abap_true.
**        ENDIF.
**
**      WHEN 'REG_STATUS'.
**        IF fc_v_sub_obj = 'ZARN_INBOUND'.
**          <ls_fieldcat>-no_out = abap_true.
**        ENDIF.
**
**      WHEN 'SAPARTICLE'.
**        IF fc_v_sub_obj = 'ZARN_INBOUND'.
**          <ls_fieldcat>-no_out = abap_true.
**        ENDIF.
**
**      WHEN 'BATCH_MODE'.
**        IF fc_v_sub_obj = 'ZARN_INBOUND'.
**          <ls_fieldcat>-no_out = abap_true.
**        ENDIF.


      WHEN OTHERS.
        DELETE lt_fieldcat WHERE fieldname = <ls_fieldcat>-fieldname.

    ENDCASE.

  ENDLOOP.

  fc_t_fieldcat[] = lt_fieldcat[].

ENDFORM.                    " PREPARE_FCAT_8000
*&---------------------------------------------------------------------*
*&      Form  APPLY_FILTERS_ON_ALV
*&---------------------------------------------------------------------*
* Apply Filters on ALV
*----------------------------------------------------------------------*
FORM apply_filters_on_alv USING fu_r_fdesc  TYPE ty_r_fdesc
                                "fu_r_vend   TYPE ty_r_vend
                                fu_r_bcode  TYPE ty_r_bcode
                                fu_r_brand  TYPE ty_r_brand
                                fu_r_sbrand TYPE ty_r_sbrand
                       CHANGING fc_t_alv TYPE ztarn_inbound_data_log_display.

  DATA: ls_alv      TYPE zsarn_inbound_data_log_display,
        lv_tabix    TYPE sy-tabix,

        ls_r_fdesc  TYPE LINE OF ty_r_fdesc,
        ls_r_bcode  TYPE LINE OF ty_r_bcode,
        ls_r_brand  TYPE LINE OF ty_r_brand,
        ls_r_sbrand TYPE LINE OF ty_r_sbrand.


  LOOP AT fu_r_bcode INTO ls_r_bcode.
    lv_tabix = sy-tabix.

    TRANSLATE ls_r_bcode-low TO UPPER CASE.

    IF ls_r_bcode-option NE 'CP'.
      ls_r_bcode-option = 'CP'.
      CONCATENATE '*' ls_r_bcode-low '*' INTO ls_r_bcode-low.
    ENDIF.

    IF ls_r_bcode-high IS NOT INITIAL.
      CLEAR ls_r_bcode-high.
    ENDIF.

    MODIFY fu_r_bcode FROM ls_r_bcode INDEX lv_tabix.
  ENDLOOP.

  LOOP AT fu_r_brand INTO ls_r_brand.
    lv_tabix = sy-tabix.

    TRANSLATE ls_r_brand-low TO UPPER CASE.

    IF ls_r_brand-option NE 'CP'.
      ls_r_brand-option = 'CP'.
      CONCATENATE '*' ls_r_brand-low '*' INTO ls_r_brand-low.
    ENDIF.

    IF ls_r_brand-high IS NOT INITIAL.
      CLEAR ls_r_brand-high.
    ENDIF.

    MODIFY fu_r_brand FROM ls_r_brand INDEX lv_tabix.
  ENDLOOP.

  LOOP AT fu_r_sbrand INTO ls_r_sbrand.
    lv_tabix = sy-tabix.

    TRANSLATE ls_r_sbrand-low TO UPPER CASE.

    IF ls_r_sbrand-option NE 'CP'.
      ls_r_sbrand-option = 'CP'.
      CONCATENATE '*' ls_r_sbrand-low '*' INTO ls_r_sbrand-low.
    ENDIF.

    IF ls_r_sbrand-high IS NOT INITIAL.
      CLEAR ls_r_sbrand-high.
    ENDIF.

    MODIFY fu_r_sbrand FROM ls_r_sbrand INDEX lv_tabix.
  ENDLOOP.

  LOOP AT fu_r_fdesc INTO ls_r_fdesc.
    lv_tabix = sy-tabix.

    TRANSLATE ls_r_fdesc-low TO UPPER CASE.

    IF ls_r_fdesc-option NE 'CP'.
      ls_r_fdesc-option = 'CP'.
      CONCATENATE '*' ls_r_fdesc-low '*' INTO ls_r_fdesc-low.
    ENDIF.

    IF ls_r_fdesc-high IS NOT INITIAL.
      CLEAR ls_r_fdesc-high.
    ENDIF.

    MODIFY fu_r_fdesc FROM ls_r_fdesc INDEX lv_tabix.
  ENDLOOP.



  LOOP AT fc_t_alv[] INTO ls_alv.

    lv_tabix = sy-tabix.

*** NAME
*** DESCRIPTION
*** FS_SHORT_DESCR
    IF ls_alv-name NOT IN fu_r_fdesc[].
      IF ls_alv-descr NOT IN fu_r_fdesc[].
        IF ls_alv-fs_descr_s NOT IN fu_r_fdesc[].
          DELETE fc_t_alv[] INDEX lv_tabix.
          CONTINUE.
        ENDIF.
      ENDIF.
    ENDIF.


*** BRAND_ID
*** FS_BRAND_ID
    IF ls_alv-brandid NOT IN fu_r_brand[].
      IF ls_alv-fs_brandid NOT IN fu_r_brand[].
        DELETE fc_t_alv[] INDEX lv_tabix.
        CONTINUE.
      ENDIF.
    ENDIF.


*** SUB_BRAND_ID
*** FS_SUB_BRAND_ID
    IF ls_alv-sub_brndid NOT IN fu_r_sbrand[].
      IF ls_alv-fs_sbbrnid NOT IN fu_r_sbrand[].
        DELETE fc_t_alv[] INDEX lv_tabix.
        CONTINUE.
      ENDIF.
    ENDIF.

*** RETAIL_BCODE
*** INNER_BCODE
*** SHIPPER_BCODE
    IF ls_alv-ret_bcode NOT IN fu_r_bcode[].
      IF ls_alv-inn_bcode NOT IN fu_r_bcode[].
        IF ls_alv-ship_bcode NOT IN fu_r_bcode[].
          DELETE fc_t_alv[] INDEX lv_tabix.
          CONTINUE.
        ENDIF.
      ENDIF.
    ENDIF.


  ENDLOOP.  " LOOP AT fc_t_alv[] INTO ls_alv


ENDFORM.                    " APPLY_FILTERS_ON_ALV
