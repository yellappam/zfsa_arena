* regenerated at 12.03.2020 16:56:18
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_REG_ARTLINKTOP.              " Global Declarations
  INCLUDE LZARN_REG_ARTLINKUXX.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_REG_ARTLINKF...              " Subroutines
* INCLUDE LZARN_REG_ARTLINKO...              " PBO-Modules
* INCLUDE LZARN_REG_ARTLINKI...              " PAI-Modules
* INCLUDE LZARN_REG_ARTLINKE...              " Events
* INCLUDE LZARN_REG_ARTLINKP...              " Local class implement.
* INCLUDE LZARN_REG_ARTLINKT99.              " ABAP Unit tests
  INCLUDE LZARN_REG_ARTLINKF00                    . " subprograms
  INCLUDE LZARN_REG_ARTLINKI00                    . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
