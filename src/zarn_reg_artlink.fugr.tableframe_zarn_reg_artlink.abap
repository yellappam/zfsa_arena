*---------------------------------------------------------------------*
*    program for:   TABLEFRAME_ZARN_REG_ARTLINK
*   generation date: 08.04.2020 at 11:21:20
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
FUNCTION TABLEFRAME_ZARN_REG_ARTLINK   .

  PERFORM TABLEFRAME TABLES X_HEADER X_NAMTAB DBA_SELLIST DPL_SELLIST
                            EXCL_CUA_FUNCT
                     USING  CORR_NUMBER VIEW_ACTION VIEW_NAME.

ENDFUNCTION.
