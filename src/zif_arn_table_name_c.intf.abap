INTERFACE zif_arn_table_name_c
  PUBLIC .

  CONSTANTS: BEGIN OF gc_table_name,
               BEGIN OF national,
                 product_master        TYPE tabname16 VALUE 'ZARN_PRODUCTS',
                 product_master_string TYPE tabname16 VALUE 'ZARN_PROD_STR',
               END   OF national,
               BEGIN OF regional,
                 header        TYPE tabname16 VALUE 'ZARN_REG_HDR',
                 header_string TYPE tabname16 VALUE 'ZARN_REG_STR',
               END   OF regional,
             END   OF gc_table_name.
ENDINTERFACE.
