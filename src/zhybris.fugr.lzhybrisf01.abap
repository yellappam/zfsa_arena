*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Functional Specification 3122, 3131
* DATE WRITTEN     # 01 03 2016
* SAP VERSION      # 731
* TYPE             # Function
* AUTHOR           # C90001448, Greg van der Loeff
*-----------------------------------------------------------------------
* TITLE            # AReNa: Search Hybris for article
* PURPOSE          # AReNa: Search Hybris for article
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------
*----------------------------------------------------------------------*
***INCLUDE LZHYBRISF01 .
*----------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*&      Form  CLEANUP_GET
*&---------------------------------------------------------------------*
FORM cleanup_get.

  DATA: lt_arn_get_cleanup TYPE STANDARD TABLE OF zarn_get_cleanup,
        ls_arn_get_cleanup TYPE zarn_get_cleanup.

  FIELD-SYMBOLS: <ls_arn_get_cleanup> LIKE LINE OF lt_arn_get_cleanup.

* Cleanup any records for the user
  SELECT *
    INTO TABLE lt_arn_get_cleanup
    FROM zarn_get_cleanup
    WHERE uname EQ sy-uname.
  LOOP AT lt_arn_get_cleanup ASSIGNING <ls_arn_get_cleanup>.
*   Is anyone else looking at it ?
    SELECT SINGLE *
      INTO ls_arn_get_cleanup
      FROM zarn_get_cleanup
      WHERE idno  EQ <ls_arn_get_cleanup>-idno
      AND   uname NE <ls_arn_get_cleanup>-uname.
    IF sy-subrc NE 0.
*     Call function to do the deletion

    ENDIF.
*   And remove from the table
    DELETE FROM zarn_get_cleanup WHERE idno EQ <ls_arn_get_cleanup>-idno AND uname EQ <ls_arn_get_cleanup>-uname.
  ENDLOOP.

  COMMIT WORK AND WAIT.

ENDFORM.                    " CLEANUP_GET
*&---------------------------------------------------------------------*
*&      Form  HYBRIS_SET
*&---------------------------------------------------------------------*
FORM hybris_set USING pt_icare TYPE zarn_hybris_set_fields_tab
                      pv_commit type xfeld.   "IS 2019/05 enable no commit

  DATA: lo_hybris_set TYPE REF TO zmaster_co_product_take_up_req,
        ls_output	    TYPE zhybris_product_take_up1,
        ls_input      TYPE zmaster_product_with_price_cre,
        lt_product    TYPE zmaster_products_product_tab,
        ls_admin      TYPE zmaster_products_admin,
        lv_log_no     TYPE balognr,
        lo_root       TYPE REF TO cx_root,
        lv_msg        TYPE string.

  FIELD-SYMBOLS: <ls_icare> LIKE LINE OF pt_icare[].

  LOOP AT pt_icare ASSIGNING <ls_icare>.
    FREE lo_hybris_set.
    CLEAR: ls_input,
           ls_output.
    ls_output-product_take_up-gtin_code            = <ls_icare>-gtin.
    ls_output-product_take_up-fsnicategory_manager = <ls_icare>-icare.
    TRY.
        CREATE OBJECT lo_hybris_set.
        IF lo_hybris_set IS BOUND.
          CALL METHOD lo_hybris_set->product_take_up_request_out
            EXPORTING
              output = ls_output
            IMPORTING
              input  = ls_input.
          IF NOT ls_input IS INITIAL.
*       Push the updated product info into AReNa
            CALL FUNCTION 'ZARN_INBOUND_DATA_PROCESSING'
              EXPORTING
                it_product     = ls_input-master_product_with_price_crea-product[]
                is_admin       = ls_input-master_product_with_price_crea-admin
                iv_id_type     = 'I'
                iv_commit   = pv_commit
              IMPORTING
*               ET_RETURN      = ET_RETURN
*               ET_PRODUCT_ERROR = ET_PRODUCT_ERROR
                ev_slg1_log_no = lv_log_no.
          ENDIF.
        ENDIF.
      CATCH cx_root INTO lo_root.
    ENDTRY.
  ENDLOOP.

ENDFORM.                    " HYBRIS_SET
*&---------------------------------------------------------------------*
*&      Form  SPLIT_MSG
*&---------------------------------------------------------------------*
FORM split_msg  USING    pv_msg   TYPE string
                CHANGING pc_msgv1 TYPE symsgv
                         pc_msgv2 TYPE symsgv
                         pc_msgv3 TYPE symsgv
                         pc_msgv4 TYPE symsgv.

  DATA: lv_msg TYPE string.
  lv_msg = pv_msg.

  CLEAR: pc_msgv1,
         pc_msgv2,
         pc_msgv3,
         pc_msgv4.
  IF strlen( pv_msg ) GT 50.
    pc_msgv1 = lv_msg(50).
    SHIFT lv_msg LEFT BY 50 PLACES.
    IF strlen( lv_msg ) GT 50.
      pc_msgv2 = lv_msg(50).
      SHIFT lv_msg LEFT BY 50 PLACES.
      IF strlen( lv_msg ) GT 50.
        pc_msgv3 = lv_msg(50).
        SHIFT lv_msg LEFT BY 50 PLACES.
        IF strlen( lv_msg ) GT 50.
          pc_msgv4 = lv_msg(50).
          SHIFT lv_msg LEFT BY 50 PLACES.
        ELSE.
          pc_msgv4 = lv_msg.
        ENDIF.
      ELSE.
        pc_msgv3 = lv_msg.
      ENDIF.
    ELSE.
      pc_msgv2 = lv_msg.
    ENDIF.
  ELSE.
    pc_msgv1 = lv_msg.
  ENDIF.

ENDFORM.                    " SPLIT_MSG
*&---------------------------------------------------------------------*
*&      Form  SAVE_RESULTS_TO_TABLE
*&---------------------------------------------------------------------*
FORM save_results_to_table USING pv_run_id  TYPE zarn_run_id
                                 pv_run_seq TYPE zarn_run_seq
                                 pt_fan_ids TYPE  zarn_fan_id_t
                                 ps_results TYPE zmaster_product_with_price_cre.

  DATA: lv_string       TYPE string,
        lv_search       TYPE string,
        lv_timestamp    TYPE timestampl,
        lv_timestamp2   TYPE timestampl,
        lv_fan_id_count TYPE i,
        lv_fan_ids      TYPE string,
        lv_offset       TYPE i,
        lv_datum        TYPE datum,
        lv_uzeit        TYPE uzeit,
        lv_tzone        TYPE tznzone.

  FIELD-SYMBOLS: <ls_fan_id> LIKE LINE OF pt_fan_ids.

  CHECK pv_run_id IS NOT INITIAL.

  GET TIME STAMP FIELD lv_timestamp.

  CALL TRANSFORMATION id
    SOURCE data = ps_results
    RESULT XML lv_string.
  lv_fan_id_count = 0.
  LOOP AT pt_fan_ids ASSIGNING <ls_fan_id>.
    CONCATENATE
      '<FAN>'
      <ls_fan_id>-fan_id
      '</FAN>'
      INTO lv_search.
    CONDENSE lv_search NO-GAPS.
    FIND FIRST OCCURRENCE OF lv_search IN lv_string IN CHARACTER MODE.
    IF sy-subrc EQ 0.
      lv_fan_id_count = lv_fan_id_count + 1.
      IF lv_fan_ids IS INITIAL.
        lv_fan_ids = <ls_fan_id>-fan_id.
      ELSE.
        CONCATENATE
          lv_fan_ids
          <ls_fan_id>-fan_id
          INTO lv_fan_ids SEPARATED BY ','.
      ENDIF.
    ENDIF.
  ENDLOOP.
  FIND FIRST OCCURRENCE OF '<SAPTRANSMISSION_TIME>' IN lv_string IN CHARACTER MODE MATCH OFFSET lv_offset.
  IF lv_offset GT 0.
    lv_offset = lv_offset + strlen( '<SAPTRANSMISSION_TIME>' ).
    lv_search = lv_string+lv_offset.
    FIND FIRST OCCURRENCE OF '</SAPTRANSMISSION_TIME>' IN lv_search IN CHARACTER MODE MATCH OFFSET lv_offset.
    IF lv_offset GT 0.
      lv_search = lv_search(lv_offset).
      "2016-05-09T07:49:22Z
      CONCATENATE
        lv_search(4)
        lv_search+5(2)
        lv_search+8(2)
        INTO lv_datum.
      CONCATENATE
        lv_search+11(2)
        lv_search+14(2)
        lv_search+17(2)
        INTO lv_uzeit.
      lv_tzone = 'NZST'.
      TRY.
          CONVERT DATE lv_datum TIME lv_uzeit INTO TIME STAMP lv_timestamp2 TIME ZONE lv_tzone.
        CATCH cx_root.
      ENDTRY.
    ENDIF.
  ENDIF.
  UPDATE zarn_hybris
    SET hybris_result    = lv_string
        hybris_returned  = lv_timestamp
        sap_transmission = lv_timestamp2
        fan_id_count_msg = lv_fan_id_count
        fan_ids_msg      = lv_fan_ids
    WHERE run_id       EQ pv_run_id
    AND   run_sequence EQ pv_run_seq.

ENDFORM.                    " SAVE_RESULTS_TO_TABLE
*&---------------------------------------------------------------------*
*&      Form  SET_BEFORE_HYBRIS_CALL
*&---------------------------------------------------------------------*
FORM set_before_hybris_call USING pv_run_id  TYPE zarn_run_id
                                  pv_run_seq TYPE zarn_run_seq.

  DATA: lv_timestamp TYPE timestampl.

  CHECK pv_run_id IS NOT INITIAL.

  GET TIME STAMP FIELD lv_timestamp.

  UPDATE zarn_hybris
    SET before_hybris_call = lv_timestamp
    WHERE run_id       EQ pv_run_id
    AND   run_sequence EQ pv_run_seq.

ENDFORM.                    " SET_BEFORE_HYBRIS_CALL
*&---------------------------------------------------------------------*
*&      Form  SET_BEFORE_ARENA_CALL
*&---------------------------------------------------------------------*
FORM set_before_arena_call USING pv_run_id  TYPE zarn_run_id
                                 pv_run_seq TYPE zarn_run_seq.

  DATA: lv_timestamp TYPE timestampl.

  CHECK pv_run_id IS NOT INITIAL.

  GET TIME STAMP FIELD lv_timestamp.

  UPDATE zarn_hybris
    SET before_arena_call = lv_timestamp
    WHERE run_id       EQ pv_run_id
    AND   run_sequence EQ pv_run_seq.

ENDFORM.                    " SET_BEFORE_ARENA_CALL
*&---------------------------------------------------------------------*
*&      Form  set_after_arena_call
*&---------------------------------------------------------------------*
FORM set_after_arena_call USING pv_run_id  TYPE zarn_run_id
                                 pv_run_seq TYPE zarn_run_seq.

  DATA: lv_timestamp     TYPE timestampl,
        lt_fan_ids       TYPE zarn_fan_id_t,
        lt_fan_ids_arena TYPE zarn_fan_id_t,
        lv_fan_ids       TYPE string,
        lv_fan_id_count  TYPE i.

  FIELD-SYMBOLS: <ls_fan_id> LIKE LINE OF lt_fan_ids.

  CHECK pv_run_id IS NOT INITIAL.

  COMMIT WORK AND WAIT.

  GET TIME STAMP FIELD lv_timestamp.

  REFRESH lt_fan_ids.
  SELECT SINGLE fan_ids
    INTO lv_fan_ids
    FROM zarn_hybris
    WHERE run_id       EQ pv_run_id
    AND   run_sequence EQ pv_run_seq.
  SPLIT lv_fan_ids AT ',' INTO TABLE lt_fan_ids.

  IF NOT lt_fan_ids[] IS INITIAL.
    SELECT fan_id
      INTO TABLE lt_fan_ids_arena
      FROM zarn_products
      FOR ALL ENTRIES IN lt_fan_ids
      WHERE fan_id EQ lt_fan_ids-fan_id.
  ENDIF.
  CLEAR lv_fan_ids.
  lv_fan_id_count = 0.
  LOOP AT lt_fan_ids_arena ASSIGNING <ls_fan_id>.
    lv_fan_id_count = lv_fan_id_count + 1.
    IF lv_fan_ids IS INITIAL.
      lv_fan_ids = <ls_fan_id>-fan_id.
    ELSE.
      CONCATENATE
        lv_fan_ids
        <ls_fan_id>-fan_id
        INTO lv_fan_ids SEPARATED BY ','.
    ENDIF.
  ENDLOOP.

  UPDATE zarn_hybris
    SET after_arena_call   = lv_timestamp
        fan_id_count_arena = lv_fan_id_count
        fan_ids_arena      = lv_fan_ids
    WHERE run_id       EQ pv_run_id
    AND   run_sequence EQ pv_run_seq.

ENDFORM.                    "set_after_arena_call

*&---------------------------------------------------------------------*
*&      Form  log_messages
*&---------------------------------------------------------------------*
FORM log_message USING pv_message TYPE string.

  DATA: ls_log        TYPE bal_s_log,
        lv_log_handle TYPE balloghndl,
        lt_log_handle TYPE bal_t_logh,
        lv_text       TYPE text255.

  CLEAR: ls_log.
  ls_log-object    = 'ZARN'.
  ls_log-subobject = 'ZARN_PROXY'.

  lv_text = pv_message.

  CALL FUNCTION 'BAL_LOG_CREATE'
    EXPORTING
      i_s_log                 = ls_log
    IMPORTING
      e_log_handle            = lv_log_handle
    EXCEPTIONS
      log_header_inconsistent = 1.

  CALL FUNCTION 'BAL_LOG_MSG_ADD_FREE_TEXT'
    EXPORTING
      i_log_handle     = lv_log_handle
      i_msgty          = 'E'
      i_probclass      = '4'
      i_text           = lv_text
    EXCEPTIONS
      log_not_found    = 1
      msg_inconsistent = 2
      log_is_full      = 3.
  APPEND lv_log_handle TO lt_log_handle.
  CALL FUNCTION 'BAL_DB_SAVE'
    EXPORTING
      i_t_log_handle   = lt_log_handle
    EXCEPTIONS
      log_not_found    = 1
      save_not_allowed = 2
      numbering_error  = 3.
  CALL FUNCTION 'BAL_LOG_REFRESH'
    EXPORTING
      i_log_handle  = lv_log_handle
    EXCEPTIONS
      log_not_found = 1
      OTHERS        = 2.

ENDFORM.                    "log_messages

FORM call_external_system USING ps_result TYPE zmaster_product_with_price_cre.

  DATA: lo_root    TYPE REF TO cx_root,
        lv_msg     TYPE string,
        lo_nat_out TYPE REF TO zco_national_products_out,
        ls_output  TYPE znational_products.

  TRY.
      IF ps_result-master_product_with_price_crea-product[] IS NOT INITIAL.
        CREATE OBJECT lo_nat_out.
        IF lo_nat_out IS BOUND.
          CLEAR ls_output.
          ls_output-national_products-admin     = ps_result-master_product_with_price_crea-admin.
          ls_output-national_products-product[] = ps_result-master_product_with_price_crea-product[].
          CALL METHOD lo_nat_out->national_products_out
            EXPORTING
              output = ls_output.
        ENDIF.
      ENDIF.
    CATCH cx_root INTO lo_root.
      lv_msg = lo_root->get_text( ).
  ENDTRY.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  HYBRIS_SET_R3
*&---------------------------------------------------------------------*
* Call Proxy and build AReNa control data
*----------------------------------------------------------------------*
FORM hybris_set_r3  USING    ps_request      TYPE zproduct_staging_icare_reques4
                    CHANGING pcs_response    TYPE zproduct_staging_icare_respon4
                             pcs_control     TYPE zarn_control
                             pcs_prd_version TYPE zarn_prd_version
                             pcs_ver_status  TYPE zarn_ver_status
                             pct_message     TYPE bal_t_msg.

  DATA: lo_hybris_set  TYPE REF TO zco_product_staging_icare_out,
        ls_output	     TYPE zproduct_staging_icare_reques4,
        ls_input       TYPE zproduct_staging_icare_respon4,
        ls_admin       TYPE zproduct_staging_icare_respon3,
        ls_product     TYPE zproduct,
        lo_root        TYPE REF TO cx_root,
        lv_msg         TYPE string,
        ls_message     TYPE bal_s_msg,

        lt_ver_status  TYPE ztarn_ver_status,
        lt_prd_version TYPE ztarn_prd_version,
        lt_control     TYPE ztarn_control,
        ls_ver_status  TYPE zarn_ver_status,
        ls_prd_version TYPE zarn_prd_version,
        ls_control     TYPE zarn_control,


        lt_cdtxt       TYPE isu_cdtxt,
        lv_objectid    TYPE cdhdr-objectid.



  DATA:   lt_xcontrol     TYPE STANDARD TABLE OF yzarn_control,
          lt_ycontrol     TYPE STANDARD TABLE OF yzarn_control,

          lt_xprd_version TYPE STANDARD TABLE OF yzarn_prd_version,
          lt_yprd_version TYPE STANDARD TABLE OF yzarn_prd_version,

          lt_xver_status  TYPE STANDARD TABLE OF yzarn_ver_status,
          lt_yver_status  TYPE STANDARD TABLE OF yzarn_ver_status.


* Old Data
  IF pcs_ver_status IS NOT INITIAL.
    APPEND pcs_ver_status  TO  lt_yver_status[].
  ENDIF.

  IF pcs_prd_version IS NOT INITIAL.
    APPEND pcs_prd_version TO  lt_yprd_version[].
  ENDIF.

  IF pcs_control IS NOT INITIAL.
    APPEND pcs_control     TO  lt_ycontrol[].
  ENDIF.




  ls_output = ps_request.
  CLEAR pcs_response.


  TRY.
      CREATE OBJECT lo_hybris_set.
      IF lo_hybris_set IS BOUND.

        CLEAR ls_input.
        CALL METHOD lo_hybris_set->product_staging_icare_out
          EXPORTING
            output = ls_output
          IMPORTING
            input  = ls_input.
        IF NOT ls_input IS INITIAL.
          CLEAR ls_admin.
          ls_admin   = ls_input-product_staging_icare_response-admin.
          ls_product = ls_input-product_staging_icare_response-product_info-product.

          pcs_response = ls_input.


          CLEAR: ls_ver_status, ls_prd_version, ls_control.
          CLEAR: lt_ver_status[], lt_prd_version[], lt_control[].

          IF ls_product IS NOT INITIAL.
* Build Version Status Data
            IF pcs_ver_status IS NOT INITIAL.
              ls_ver_status = pcs_ver_status.

              ls_ver_status-previous_ver   = pcs_ver_status-current_ver.
              ls_ver_status-current_ver    = pcs_ver_status-latest_ver + 1.
              ls_ver_status-latest_ver     = pcs_ver_status-latest_ver + 1.

              ls_ver_status-changed_on     = sy-datum.
              ls_ver_status-changed_at     = sy-uzeit.
              ls_ver_status-changed_by     = sy-uname.
            ELSE.
              ls_ver_status-mandt          = sy-mandt.
              ls_ver_status-idno           = ls_product-code.
              ls_ver_status-current_ver    = 1.
              ls_ver_status-latest_ver     = 1.
            ENDIF.
            APPEND ls_ver_status TO lt_ver_status[].
            pcs_ver_status = ls_ver_status.

* Build Product Version Data
            ls_prd_version-mandt          = sy-mandt.
            ls_prd_version-idno           = ls_ver_status-idno.
            ls_prd_version-version        = ls_ver_status-latest_ver.
            ls_prd_version-id_type        = 'R'.
            ls_prd_version-guid           = ls_admin-national_guid.
            ls_prd_version-version_status = 'NEW'.
            APPEND ls_prd_version TO lt_prd_version[].
            pcs_prd_version = ls_prd_version.

* Build Control Data
            CLEAR ls_control.
            ls_control-mandt              = sy-mandt.
            ls_control-guid               = ls_admin-national_guid.
            ls_control-hybris_msgid       = ls_admin-sending_system_id.
            ls_control-external_reference = ls_admin-external_reference.
            ls_control-test_indicator     = ls_admin-test_indicator.
            ls_control-arn_mode           = ls_admin-operation_mode.
            ls_control-date_in            = sy-datum.
            ls_control-time_in            = sy-uzeit.
            ls_control-date_pi            = sy-datum.
            ls_control-time_pi            = sy-uzeit.
            ls_control-date_exp           = sy-datum.
            ls_control-time_exp           = sy-uzeit.
            ls_control-created_on         = sy-datum.
            ls_control-created_at         = sy-uzeit.
            ls_control-created_by         = sy-uname.
            APPEND ls_control TO lt_control[].
            pcs_control = ls_control.

* Update Version and Control tables
            CALL FUNCTION 'ZARN_VERSION_CONTROL_UPDATE'
              EXPORTING
                it_control     = lt_control[]
                it_prd_version = lt_prd_version[]
                it_ver_status  = lt_ver_status[].


* New Data
            lt_xcontrol[]     = lt_control[].
            lt_xprd_version[] = lt_prd_version[].
            lt_xver_status[]  = lt_ver_status[].

            CLEAR lv_objectid.
            lv_objectid = ls_ver_status-idno.


* Create Change Document
            CALL FUNCTION 'ZARN_CTRL_REGNL_WRITE_DOC_CUST'
              EXPORTING
                objectid                = lv_objectid
                tcode                   = sy-tcode
                utime                   = sy-uzeit
                udate                   = sy-datum
                username                = sy-uname
                object_change_indicator = 'U'
                upd_zarn_control        = 'U'
                upd_zarn_prd_version    = 'U'
                upd_zarn_ver_status     = 'U'
              TABLES
                icdtxt_zarn_ctrl_regnl  = lt_cdtxt[]
                xzarn_control           = lt_xcontrol[]           " lt_control[]
                yzarn_control           = lt_ycontrol[]           " lt_control_old[]
                xzarn_prd_version       = lt_xprd_version[]       " lt_prd_version[]
                yzarn_prd_version       = lt_yprd_version[]       " lt_prd_version_old[]
                xzarn_ver_status        = lt_xver_status[]        " lt_ver_status[]
                yzarn_ver_status        = lt_yver_status[].       " lt_ver_status_old[].

          ENDIF.  " IF ls_product IS NOT INITIAL
        ENDIF.  " ls_product
      ENDIF.  " IF lo_hybris_set IS BOUND.
    CATCH cx_root INTO lo_root.

      lv_msg = lo_root->if_message~get_text( ).
      CLEAR ls_message.
      ls_message-msgid = 'ZARENA_MSG'.
      ls_message-msgty = 'E'.
      ls_message-msgno = '000'.
      PERFORM split_msg USING lv_msg CHANGING ls_message-msgv1 ls_message-msgv2 ls_message-msgv3 ls_message-msgv4.
      APPEND ls_message TO pct_message.
      PERFORM log_message USING lv_msg.

      WHILE NOT lo_root->previous IS INITIAL.
        lo_root = lo_root->previous.
        lv_msg = lo_root->if_message~get_text( ).
        CLEAR ls_message.
        ls_message-msgid = 'ZARENA_MSG'.
        ls_message-msgty = 'E'.
        ls_message-msgno = '000'.
        PERFORM split_msg USING lv_msg CHANGING ls_message-msgv1 ls_message-msgv2 ls_message-msgv3 ls_message-msgv4.
        APPEND ls_message TO pct_message.
        PERFORM log_message USING lv_msg.
      ENDWHILE.


  ENDTRY.

ENDFORM.  " HYBRIS_SET_R3
