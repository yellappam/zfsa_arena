*&---------------------------------------------------------------------*
*&  Include           ZIARN_DATA_MIGRATION_SEL
*&---------------------------------------------------------------------*
SELECTION-SCREEN BEGIN OF BLOCK tab with FRAME TITLE text-tab.
PARAMETERS: r_qty RADIOBUTTON GROUP rg1,
            r_srv RADIOBUTTON GROUP rg1,
            r_bio RADIOBUTTON GROUP rg1.
SELECTION-SCREEN END OF BLOCK tab.

SELECTION-SCREEN BEGIN OF BLOCK sel WITH FRAME TITLE text-sel.
SELECT-OPTIONS: s_idno  FOR ls_zarn_products-idno.
*PARAMETERS: p_dir TYPE string DEFAULT 'C:\eclipse\upload.xlsx'.
SELECTION-SCREEN END OF BLOCK sel.

SELECTION-SCREEN BEGIN OF BLOCK opt WITH FRAME TITLE text-opt.
PARAMETERS: p_test TYPE flag DEFAULT 'X',
*            p_pass type flag default 'X',
            p_pksize(6) type n DEFAULT 20000.
SELECTION-SCREEN END OF BLOCK opt.

**----------------------------------------------------------------------*
**       VALIDATION
**----------------------------------------------------------------------*
*AT SELECTION-SCREEN ON VALUE-REQUEST FOR  p_dir.
*
*  REFRESH : lt_file_tab. CLEAR lt_file_tab.
*  CALL METHOD cl_gui_frontend_services=>file_open_dialog
*    EXPORTING
*      window_title            = 'Select 1 File Only'
*      default_extension       = '*.xlsx'
*      default_filename        = '*.xlsx'
*      initial_directory       = 'c:'
*      multiselection          = ' '
*    CHANGING
*      file_table              = lt_file_tab
*      rc                      = lv_subrc
*    EXCEPTIONS
*      file_open_dialog_failed = 1
*      cntl_error              = 2
*      error_no_gui            = 3
*      not_supported_by_gui    = 4
*      OTHERS                  = 5.
*
*  IF sy-subrc EQ 0.
**  File exists
*    READ TABLE lt_file_tab INTO ls_file_tab INDEX 1.   "Multiselection not allowed
*    IF sy-subrc = 0.
*      p_dir = ls_file_tab-filename.
*    ENDIF.
*  ELSE.
*    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
*               WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
*  ENDIF.
