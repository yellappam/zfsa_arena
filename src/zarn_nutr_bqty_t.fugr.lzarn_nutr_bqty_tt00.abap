*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 12.05.2016 at 16:32:50 by user C90001363
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_NUTR_BQTY_T................................*
DATA:  BEGIN OF STATUS_ZARN_NUTR_BQTY_T              .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_NUTR_BQTY_T              .
CONTROLS: TCTRL_ZARN_NUTR_BQTY_T
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_NUTR_BQTY_T              .
TABLES: ZARN_NUTR_BQTY_T               .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
