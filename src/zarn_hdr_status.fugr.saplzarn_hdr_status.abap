* regenerated at 23.02.2016 08:21:25 by  C90001448
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_HDR_STATUSTOP.               " Global Data
  INCLUDE LZARN_HDR_STATUSUXX.               " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_HDR_STATUSF...               " Subroutines
* INCLUDE LZARN_HDR_STATUSO...               " PBO-Modules
* INCLUDE LZARN_HDR_STATUSI...               " PAI-Modules
* INCLUDE LZARN_HDR_STATUSE...               " Events
* INCLUDE LZARN_HDR_STATUSP...               " Local class implement.
* INCLUDE LZARN_HDR_STATUST99.               " ABAP Unit tests
  INCLUDE LZARN_HDR_STATUSF00                     . " subprograms
  INCLUDE LZARN_HDR_STATUSI00                     . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
