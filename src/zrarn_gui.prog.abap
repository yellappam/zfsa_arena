*&---------------------------------------------------------------------*
*& Report  ZRARN_GUI
*&
*&---------------------------------------------------------------------*
*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Functional Specification 3125
* DATE WRITTEN     # 15 02 2016
* SAP VERSION      # 731
* TYPE             # Report Program
* AUTHOR           # C90002769, Rajesh Anumolu
*-----------------------------------------------------------------------
* TITLE            # AReNa: GUI for Enrichment
* PURPOSE          # AReNa: Enrichment of Regional Data
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 14/10/16
* CHANGE No.       # 3162
* DESCRIPTION      # Replace approvals selection screen to reflect new model
* WHO              # Paul Collins
*-----------------------------------------------------------------------
*-----------------------------------------------------------------------
* DATE             # 07/02/2018
* CHANGE No.       # Charm 9000003206
* DESCRIPTION      # BB date on Produce articles
* WHO              # Ashwaan Morris
*-----------------------------------------------------------------------
* DATE             # 24.04.2018
* CHANGE No.       # ChaRM DE0K917906; JIRA CI18-51 Pay By Scan Changes
* DESCRIPTION      # Pay by Scan should be set not only at the banner
*                  # level but also at the individual store level. The
*                  # previous implementation was to allow it to be set
*                  # only at the banner level on MVKE-MVGR3. The new
*                  # implementation is to add custom field ZZ_PBS to MARC.
*                  # 1. Removed MVGR3 related logic
*                  # 2. Added logic for ZZ_PBS.
* WHO              # I00600343, Fengyu Li
*-----------------------------------------------------------------------
* DATE             # 07.12.2018
* CHANGE No.       # 9000004661: CI19-294 LRA AReNa-Supply Chain Changes
* DESCRIPTION      # include SC team in AReNa processes,
*                  # i.e. article data review, enrichment and approval,
*                  # at least for DC-sourced articles.
*                  #
*                  # 1. Add Supply Chain tab. New screen/table created.
*                  # 2. New ALV to display UoM with different editable
*                  #    fields similar to basic article tab
* WHO              # C90012814, Brad Gorlicki
*-----------------------------------------------------------------------
* DATE             # 07.11.2019
* CHANGE No.       # CIP-148
* DESCRIPTION      # When both UNI and LNI DC flags are blank, reset
*                  # ZZVAR_WT_FLAG flag in regional header
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* DATE             # 24.06.2020
* CHANGE No.       # SSM-1 NW Range Policy ZARN_GUI
* DESCRIPTION      # Added Regional Cluster sub-screen 0116
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* DATE             # 04.08.2020
* CHANGE No.       # SSM-1:	ZARN_GUI - Layout Module Changes
* DESCRIPTION      # Added edit feature for Regional Cluster
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
INCLUDE ziarn_gui_top.                           " global Data
INCLUDE ziarn_gui_d03.                           " Class Definition
INCLUDE ziarn_gui_cl01.                          " Class definition
INCLUDE ziarn_gui_cl02.                          " Class definition

INCLUDE ziarn_gui_p03.                           " Class implementation

INCLUDE ziarn_gui_sel.                           " selection screen
INCLUDE ziarn_gui_main.                          " main processing logic
INCLUDE ziarn_gui_o01.                           " PBO-Modules
INCLUDE ziarn_gui_i01.                           " PAI-Modules
INCLUDE ziarn_gui_f01.                           " FORM-Routines
INCLUDE ziarn_gui_f02.                           " FORM-Routines for Approvals
INCLUDE ziarn_gui_f03.                           " FORM-Routines for Status Updates
INCLUDE ziarn_gui_f04.                           " FORM-Routines for Banner defaulting
INCLUDE ziarn_gui_ts01.                          " Tab Strip Routines
INCLUDE ziarn_gui_ts02.                          " Tab Strip Routines

INCLUDE ziarn_gui_i02.

INCLUDE ziarn_gui_f05.
