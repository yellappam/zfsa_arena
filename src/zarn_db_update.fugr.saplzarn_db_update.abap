*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_DB_UPDATETOP.                " Global Data
  INCLUDE LZARN_DB_UPDATEUXX.                " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_DB_UPDATEF...                " Subroutines
* INCLUDE LZARN_DB_UPDATEO...                " PBO-Modules
* INCLUDE LZARN_DB_UPDATEI...                " PAI-Modules
* INCLUDE LZARN_DB_UPDATEE...                " Events
* INCLUDE LZARN_DB_UPDATEP...                " Local class implement.
* INCLUDE LZARN_DB_UPDATET99.                " ABAP Unit tests

INCLUDE LZARN_DB_UPDATEF01.
