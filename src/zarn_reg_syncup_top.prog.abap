*&---------------------------------------------------------------------*
*&  Include           ZARN_REG_SYNCUP_TOP
*&---------------------------------------------------------------------*

  TYPES:
    BEGIN OF ty_s_main,
      zzfan   TYPE zmm_e_fan,
      version TYPE zarn_version,
    END OF ty_s_main,

    BEGIN OF ty_s_uom,
      zzfan        TYPE zmm_e_fan,
      hyb_uom_code TYPE zarn_uom_code,
      hyb_pir      TYPE zarn_hybris_internal_code,
      sap_uom_code TYPE meins,
    END OF ty_s_uom,

    BEGIN OF ty_s_pir,
      zzfan   TYPE zmm_e_fan,
      hyb_pir TYPE zarn_hybris_internal_code,
      sap_pir TYPE infnr,
    END OF ty_s_pir,

    BEGIN OF ty_s_pir_reg,
      idno     TYPE zarn_idno,
      version  TYPE zarn_version,
      uom_code TYPE zarn_uom_code,
      hyb_pir  TYPE zarn_hybris_internal_code,
      matnr    TYPE matnr,
      gln      TYPE zarn_gln,
      lifnr    TYPE lifnr,
    END OF ty_s_pir_reg,

    BEGIN OF ty_s_zzcatman,
      matnr    TYPE matnr,
      zzcatman TYPE zmd_e_catman,
    END OF ty_s_zzcatman,

    BEGIN OF ty_s_zzcatman_ret,
      matnr    TYPE matnr,
      vkorg    TYPE vkorg,zzcatman TYPE zmd_e_catman,
    END OF ty_s_zzcatman_ret,

    BEGIN OF ty_s_marc_sos,
      matnr           TYPE matnr,
      werks           TYPE werks_d,
      bwscl           TYPE bwscl,
      zzonline_status TYPE zmd_online_status,       "++ONLD-822 JKH 17.02.2017
      zz_pbs          TYPE zmd_e_pbs,
    END OF ty_s_marc_sos.

  TYPES:
    BEGIN OF ty_s_arn_products,
      fan_id                TYPE zmm_e_fan,
      idno                  TYPE zarn_idno,
      version               TYPE zarn_version,
      irradiated            TYPE zarn_irradiated,

* INS Begin of Change 3169 JKH 20.10.2016
      dgi_packing_group      TYPE zarn_dgi_packing_group,
      dgi_regulation_code    TYPE zarn_dgi_regulation_code,
      dgi_technical_name    TYPE zarn_dgi_technical_name,
      dgi_hazardous_code    TYPE zarn_dgi_hazardous_code,
      dgi_unn_code          TYPE zarn_dgi_unn_code,
      dgi_unn_shipping_name  TYPE zarn_dgi_unn_shipping_name,
      dgi_class              TYPE zarn_dgi_class,
      hsno_app              TYPE zarn_hsno_app,
      hsno_hsr              TYPE zarn_hsno_hsr,
      dangerous_good        TYPE zsarn_dangerous_good,
      hazardous_good        TYPE zsarn_hazardous_good,
      flash_pt_temp_val      TYPE zarn_flash_pt_temp_val,
      flash_pt_temp_uom      TYPE zarn_flash_pt_temp_uom,
* INS End of Change 3169 JKH 20.10.2016
    END OF ty_s_arn_products,

    BEGIN OF ty_s_arn_prd_ver,
      idno    TYPE zarn_idno,
      version  TYPE zarn_version,
    END OF ty_s_arn_prd_ver,

    BEGIN OF ty_s_wrf_matgrp_prod,
      matnr TYPE matnr,
      node  TYPE wrf_struc_node,
    END OF ty_s_wrf_matgrp_prod,

    BEGIN OF ty_s_maw1,
      matnr TYPE matnr,
      wvrkm  TYPE vrkme,
      wausm  TYPE ausme,
    END OF ty_s_maw1,

    BEGIN OF ty_s_reg_banner,
      matnr    TYPE matnr,
      vkorg    TYPE vkorg,
      vtweg    TYPE vtweg,
      zzcatman TYPE zmd_e_catman,
      sstuf    TYPE sstuf,
      vrkme    TYPE vrkme,
      prodh    TYPE prodh_d,
      versg    TYPE stgma,
      ktgrm    TYPE ktgrm,
      vmsta    TYPE vmsta,
      vmstd    TYPE vmstd,
      aumng    TYPE aumng,
    END OF ty_s_reg_banner,

    BEGIN OF ty_s_marm_idno,
      idno         TYPE zarn_idno,
      meinh        TYPE lrmei,
      umrez        TYPE umrez,
      umren        TYPE umren,
      mesub        TYPE mesub,
      matnr        TYPE matnr,
      uom_category TYPE zarn_uom_category,
      seqno        TYPE zarn_seqno,
      uom          TYPE meinh,
      cat_seqno    TYPE zarn_cat_seqno,
    END OF ty_s_marm_idno,

    BEGIN OF ty_s_uom_var_lower,
      idno                 TYPE zarn_idno,
      version              TYPE zarn_version,
      uom_category         TYPE zarn_uom_category,
      uom_code             TYPE zarn_uom_code,
      hybris_internal_code TYPE zarn_hybris_internal_code,
      lower_uom            TYPE zarn_lower_uom,
      lower_child_uom      TYPE zarn_lower_child_uom,
      meinh                TYPE zarn_sap_uom,
      lower_meinh          TYPE zarn_lower_sap_uom,
      product_uom          TYPE zarn_product_uom,
      base_unit            TYPE zarn_base_unit,
      consumer_unit        TYPE zarn_consumer_unit,
      despatch_unit        TYPE zarn_despatch_unit,
      an_invoice_unit      TYPE zarn_an_invoice_unit,
      cat_seqno            TYPE zarn_cat_seqno,
      seqno                TYPE zarn_seqno,
    END OF ty_s_uom_var_lower.

  TYPES: ty_t_uom_var_lower TYPE STANDARD TABLE OF ty_s_uom_var_lower,

         ty_s_marm          TYPE marm,

         ty_t_marm          TYPE SORTED TABLE OF ty_s_marm WITH UNIQUE KEY matnr meinh,

         ty_t_marm_idno     TYPE STANDARD TABLE OF ty_s_marm_idno,

         BEGIN OF ty_s_uom_variant_ui,
           include      TYPE zarn_uom_variant,
           uom_category TYPE zarn_uom_category,
           cat_seqno    TYPE zarn_cat_seqno,
           seqno        TYPE zarn_seqno,
         END OF ty_s_uom_variant_ui.

  TYPES:
    BEGIN OF ty_s_pir_vendor,
      bbbnr TYPE bbbnr,
      bbsnr TYPE bbsnr,
      bubkz TYPE bubkz,
      lifnr TYPE lifnr,
    END OF ty_s_pir_vendor,
    ty_t_pir_vendor TYPE SORTED TABLE OF ty_s_pir_vendor WITH NON-UNIQUE KEY bbbnr bbsnr bubkz,

    BEGIN OF ty_s_reg_pir,
      include TYPE zarn_reg_pir,
      matnr   TYPE matnr,
    END OF ty_s_reg_pir,

    ty_t_reg_pir TYPE SORTED TABLE OF ty_s_reg_pir WITH NON-UNIQUE KEY matnr. " lifnr bprme.

  TYPES ty_s_prod_uom_t TYPE zarn_prod_uom_t .

  TYPES ty_t_prod_uom_t TYPE SORTED TABLE OF ty_s_prod_uom_t WITH UNIQUE KEY product_uom .

  CONSTANTS:
    gc_cat_layer  TYPE zarn_uom_category VALUE 'LAYER',
    gc_cat_pallet TYPE zarn_uom_category VALUE 'PALLET'.


  DATA gt_pir_vendor                  TYPE ty_t_pir_vendor.
  DATA gt_main                        TYPE SORTED TABLE OF ty_s_main WITH NON-UNIQUE KEY zzfan.
  DATA gt_uom                         TYPE SORTED TABLE OF ty_s_uom WITH NON-UNIQUE KEY zzfan hyb_uom_code hyb_pir.
  DATA gt_pir                         TYPE SORTED TABLE OF ty_s_pir WITH NON-UNIQUE KEY zzfan hyb_pir.
  DATA gt_pir_not_found               TYPE SORTED TABLE OF ty_s_pir_reg WITH NON-UNIQUE KEY idno matnr.
  DATA gt_pir_not_found_all           TYPE SORTED TABLE OF ty_s_pir_reg WITH NON-UNIQUE KEY idno matnr.
  DATA gt_mara                        TYPE SORTED TABLE OF mara WITH NON-UNIQUE KEY matnr.
  DATA gt_gil_zzcatman                TYPE SORTED TABLE OF ty_s_zzcatman WITH NON-UNIQUE KEY matnr.
  DATA gt_ret_zzcatman                TYPE SORTED TABLE OF ty_s_zzcatman_ret WITH NON-UNIQUE KEY matnr vkorg.
  DATA gt_retdc_zzcatman              TYPE SORTED TABLE OF ty_s_zzcatman WITH NON-UNIQUE KEY matnr.
  DATA gt_host_sap                    TYPE SORTED TABLE OF zmd_host_sap WITH NON-UNIQUE KEY matnr.
  DATA gt_wlk2                        TYPE SORTED TABLE OF wlk2 WITH NON-UNIQUE KEY matnr vkorg vtweg werks.
  DATA gt_marc_sos                    TYPE SORTED TABLE OF ty_s_marc_sos WITH NON-UNIQUE KEY matnr werks.
  DATA gt_mean                        TYPE TABLE OF mean.
  DATA gt_arn_products                TYPE SORTED TABLE OF ty_s_arn_products WITH NON-UNIQUE KEY fan_id.
  DATA gt_arn_prd_ver                 TYPE SORTED TABLE OF ty_s_arn_prd_ver WITH NON-UNIQUE KEY idno version.
  DATA gt_arn_curr_prd_ver            TYPE SORTED TABLE OF ty_s_arn_prd_ver WITH NON-UNIQUE KEY idno.
  DATA gt_arn_growing                 TYPE SORTED TABLE OF zarn_growing WITH NON-UNIQUE KEY idno version growing_method.
  DATA gt_arn_diet_info               TYPE SORTED TABLE OF zarn_diet_info WITH NON-UNIQUE KEY idno version diet_type.
  DATA gt_arn_allergen                TYPE SORTED TABLE OF zarn_allergen WITH NON-UNIQUE KEY idno version.
  DATA gt_arn_ntr_claims              TYPE SORTED TABLE OF zarn_ntr_claims WITH NON-UNIQUE KEY idno version nutritional_claims nutritional_claims_elem.
  DATA gt_makt                        TYPE SORTED TABLE OF makt WITH NON-UNIQUE KEY matnr.
  DATA gt_wrf_matgrp_prod             TYPE SORTED TABLE OF ty_s_wrf_matgrp_prod WITH NON-UNIQUE KEY matnr.
  DATA gt_reg_banner                  TYPE SORTED TABLE OF ty_s_reg_banner WITH NON-UNIQUE KEY matnr.
  DATA gt_tvarv_ref_sites_uni         TYPE tvarvc_t.
  DATA gt_tvarv_ref_sites_lni         TYPE tvarvc_t.
  DATA gt_arn_reg_banner_all          TYPE ztarn_reg_banner.
  DATA gt_arn_reg_ean_all             TYPE ztarn_reg_ean.
  DATA gtr_numtp                      TYPE RANGE OF numtp.
  DATA gt_arn_reg_uom_all             TYPE ztarn_reg_uom.
  DATA gt_arn_uom_variant             TYPE SORTED TABLE OF zarn_uom_variant WITH NON-UNIQUE KEY idno version.
  DATA gt_arn_uom_variant_ui          TYPE TABLE OF ty_s_uom_variant_ui.
  DATA gt_arn_gtin_var                TYPE SORTED TABLE OF zarn_gtin_var WITH NON-UNIQUE KEY idno version gtin_code.
  DATA gt_maw1                        TYPE SORTED TABLE OF ty_s_maw1 WITH NON-UNIQUE KEY matnr.
  DATA gt_arn_pir                     TYPE SORTED TABLE OF zarn_pir  WITH NON-UNIQUE KEY idno version uom_code hybris_internal_code.
  DATA gt_arn_list_price              TYPE SORTED TABLE OF zarn_list_price WITH NON-UNIQUE KEY idno version uom_code gln.
*  DATA gt_arn_list_price              TYPE SORTED TABLE OF zarn_list_price WITH NON-UNIQUE KEY idno version uom_code gln.
  DATA gt_eina                        TYPE SORTED TABLE OF eina WITH NON-UNIQUE KEY infnr.
  DATA gt_eine                        TYPE SORTED TABLE OF eine WITH NON-UNIQUE KEY infnr.
  DATA gt_arn_reg_pir_all             TYPE ztarn_reg_pir.
  DATA gtr_layer_uoms                 TYPE RANGE OF meins.
  DATA gtr_pallets_uoms               TYPE RANGE OF meins.
  DATA gt_arn_ver_status              TYPE TABLE OF zarn_ver_status.
  DATA gt_arn_reg_hdr                 TYPE SORTED TABLE OF zarn_reg_hdr WITH NON-UNIQUE KEY idno.
  DATA gt_arn_reg_hdr_in_db           TYPE SORTED TABLE OF zarn_reg_hdr WITH NON-UNIQUE KEY idno.
  DATA gt_arn_reg_banner_in_db        TYPE SORTED TABLE OF zarn_reg_banner WITH NON-UNIQUE KEY idno banner.
  DATA gt_arn_reg_uom_in_db           TYPE SORTED TABLE OF zarn_reg_uom WITH NON-UNIQUE KEY idno
                                      uom_category pim_uom_code hybris_internal_code lower_uom lower_child_uom.
  DATA gt_arn_reg_ean_in_db           TYPE SORTED TABLE OF zarn_reg_ean WITH NON-UNIQUE KEY idno meinh ean11.
  DATA gt_arn_reg_pir_in_db           TYPE SORTED TABLE OF zarn_reg_pir WITH NON-UNIQUE KEY idno order_uom_pim
                                      hybris_internal_code.
  DATA gt_arn_reg_hdr_in_db_old       TYPE SORTED TABLE OF zarn_reg_hdr WITH NON-UNIQUE KEY idno.
  DATA gt_arn_reg_banner_in_db_old    TYPE SORTED TABLE OF zarn_reg_banner WITH NON-UNIQUE KEY idno banner.
  DATA gt_arn_reg_uom_in_db_old       TYPE SORTED TABLE OF zarn_reg_uom WITH NON-UNIQUE KEY idno
                                      uom_category pim_uom_code hybris_internal_code lower_uom lower_child_uom.
  DATA gt_arn_reg_ean_in_db_old       TYPE SORTED TABLE OF zarn_reg_ean WITH NON-UNIQUE KEY idno meinh ean11.
  DATA gt_arn_reg_pir_in_db_old       TYPE SORTED TABLE OF zarn_reg_pir WITH NON-UNIQUE KEY idno order_uom_pim
                                      hybris_internal_code.
  DATA gt_arn_ver_status_in_db        TYPE SORTED TABLE OF zarn_ver_status WITH NON-UNIQUE KEY idno.
  DATA gt_arn_ver_status_in_db_old    TYPE SORTED TABLE OF zarn_ver_status WITH NON-UNIQUE KEY idno.
  DATA gt_arn_sync                    TYPE SORTED TABLE OF zarn_sync WITH NON-UNIQUE KEY tabname fldname.
  DATA gv_fcode                       TYPE sy-ucomm.
  DATA gt_uom_cat                     TYPE ztarn_uom_cat.
  DATA gt_arn_prod_uom_t              TYPE SORTED TABLE OF zarn_prod_uom_t WITH UNIQUE KEY product_uom.
  DATA gt_marm                        TYPE ty_t_marm.
  DATA gt_marm_idno                   TYPE ty_t_marm_idno.
  DATA gt_arn_reg_uom_ui              TYPE ztarn_reg_uom_ui.
  DATA gt_arn_reg_uom_all_ui          TYPE ztarn_reg_uom_ui.
  DATA gt_arn_hsno                    TYPE SORTED TABLE OF zarn_hsno
                                           WITH NON-UNIQUE KEY idno hsno_code.         " ++3169 JKH 20.10.2016
  DATA gt_arn_reg_hsno_all            TYPE SORTED TABLE OF zarn_reg_hsno
                                           WITH NON-UNIQUE KEY idno hsno_code.         " ++3169 JKH 20.10.2016
  DATA gt_arn_reg_hsno_in_db          TYPE SORTED TABLE OF zarn_reg_hsno
                                           WITH NON-UNIQUE KEY idno hsno_code.         " ++3169 JKH 20.10.2016
  DATA gt_arn_reg_hsno_in_db_old      TYPE SORTED TABLE OF zarn_reg_hsno
                                           WITH NON-UNIQUE KEY idno hsno_code.         " ++3169 JKH 20.10.2016



  DATA: gv_name_text    TYPE ad_namtext.
