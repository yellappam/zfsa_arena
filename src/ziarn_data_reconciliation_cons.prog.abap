*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_DATA_RECONCILIATION_CONS
*&---------------------------------------------------------------------*

*&SPWIZARD: FUNCTION CODES FOR TABSTRIP 'TS_RECON'
CONSTANTS: BEGIN OF c_ts_recon,
             tab1 LIKE sy-ucomm VALUE 'TS_FAN',
             tab2 LIKE sy-ucomm VALUE 'TS_BASIC1',
             tab3 LIKE sy-ucomm VALUE 'TS_BASIC2',
             tab4 LIKE sy-ucomm VALUE 'TS_UOM',
             tab5 LIKE sy-ucomm VALUE 'TS_EAN',
             tab6 LIKE sy-ucomm VALUE 'TS_PIR',
             tab7 LIKE sy-ucomm VALUE 'TS_BANNER',
             tab8 LIKE sy-ucomm VALUE 'TS_ATTR',
           END OF c_ts_recon.

CONSTANTS:
* Mode
  BEGIN OF gc_mode,
    dsp TYPE char1 VALUE 'D',
    ecc TYPE char1 VALUE 'E',
  END OF gc_mode,

* TVARVC
  BEGIN OF gc_tvarvc,
    vkorg_uni         TYPE rvari_vnam  VALUE 'ZC_REF_VKORG_VTREFS',
    vkorg_lni         TYPE rvari_vnam  VALUE 'ZMD_VKORG_REF_SITES_LNI',
    eine_ekorg        TYPE rvari_vnam  VALUE 'ZARN_ARTICLE_POST_EINE_EKORG',
    def_eancat        TYPE rvari_vnam  VALUE 'ZARN_GUI_DEFAULT_EAN_CATEGORY',
    def_eancat_prefix TYPE rvari_vnam  VALUE 'ZARN_GUI_DEFLT_EAN_PREFIX_CATE',
    uom_hyb_code      TYPE rvari_vnam  VALUE 'ZARN_GUI_UOM_ADDITION_SCENARIO',
    type_s            TYPE rsscr_kind  VALUE 'S',
    type_p            TYPE rsscr_kind  VALUE 'P',
  END OF gc_tvarvc,


* Reconciliation status.
  BEGIN OF gc_status,
    matched   TYPE zarn_reco_status  VALUE space,
    unmatched TYPE zarn_reco_status  VALUE 'X',
    update    TYPE zarn_reco_status  VALUE 'U',
    insert    TYPE zarn_reco_status  VALUE 'I',
    delete    TYPE zarn_reco_status  VALUE 'D',
  END OF gc_status,


* Function Codea
  BEGIN OF gc_fcode,
    pir_rel_9006 TYPE sy-ucomm VALUE 'PIR_REL_9006',
  END OF gc_fcode.
