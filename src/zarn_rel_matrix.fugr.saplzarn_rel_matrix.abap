* regenerated at 24.02.2017 10:28:23
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_REL_MATRIXTOP.               " Global Data
  INCLUDE LZARN_REL_MATRIXUXX.               " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_REL_MATRIXF...               " Subroutines
* INCLUDE LZARN_REL_MATRIXO...               " PBO-Modules
* INCLUDE LZARN_REL_MATRIXI...               " PAI-Modules
* INCLUDE LZARN_REL_MATRIXE...               " Events
* INCLUDE LZARN_REL_MATRIXP...               " Local class implement.
* INCLUDE LZARN_REL_MATRIXT99.               " ABAP Unit tests
  INCLUDE LZARN_REL_MATRIXF00                     . " subprograms
  INCLUDE LZARN_REL_MATRIXI00                     . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
