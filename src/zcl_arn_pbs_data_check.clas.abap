class ZCL_ARN_PBS_DATA_CHECK definition
  public
  create public .

public section.

  types:
    BEGIN OF ty_banner_refsite,
      vkorg TYPE t001w-vkorg,
      werks TYPE t001w-werks,
    END OF ty_banner_refsite .
  types:
    ty_t_range_pbs_values TYPE RANGE OF zmd_e_pbs .
  types:
    ty_t_banner_refsite TYPE SORTED TABLE OF ty_banner_refsite WITH NON-UNIQUE KEY vkorg .
  types:
    ty_t_marc TYPE SORTED TABLE OF marc WITH UNIQUE KEY matnr werks .

  class-data ST_PBS_VALUES type TY_T_RANGE_PBS_VALUES .
  data MT_MARC type MARC_TT .
  constants MC_VKORG_1000 type VKORG value '1000' ##NO_TEXT.
  data MT_MVKE type MVKE_TT .
  data MS_T001W type T001W .
  constants MC_BWSCL_2 type BWSCL value '2' ##NO_TEXT.
  constants MC_BWSCL_1 type BWSCL value '1' ##NO_TEXT.
  constants MC_PBS_VALUES_TVARVC type RVARI_VNAM value 'ZMD_PBS_RESTRICTED_VALUES' ##NO_TEXT.
  class-data ST_BANNER_REFSITE type TY_T_BANNER_REFSITE .

  class-methods GET_PBS
    returning
      value(RT_PBS) type ZTARN_PBS_TEXT .
  class-methods GET_PBS_SINGLE
    importing
      !IV_PBS type ZMD_E_PBS
    returning
      value(RS_PBS) type ZSARN_PBS_TEXT .
  class-methods GET_PBS_VALUES_TVARVC .
  methods GET_MARC
    importing
      !IV_MATNR type MATNR
      !IV_VKORG type VKORG
      !IV_VTWEG type VTWEG
    returning
      value(RT_MARC) type MARC_TT .
  methods GET_MARC_PBS_ERROR
    importing
      !IV_MATNR type MATNR
      !IV_VKORG type VKORG
      !IV_VTWEG type VTWEG
      !IV_WERKS type T001W-WERKS optional
      !IV_BWSCL type BWSCL default '1'
      !IV_PBS_CODE type ZMD_E_PBS default 'PBS'
      !IT_MARC_DATA type MARC_TT optional
    exporting
      !ES_MARC type MARC
      !ET_MARC type MARC_TT
      !ES_BAPIRET2 type BAPIRET2
    returning
      value(RV_ERROR) type FLAG .
  methods GET_MVKE
    importing
      !IV_MATNR type MATNR
      !IV_WERKS type WERKS_D
    returning
      value(RT_MVKE) type MVKE_TT .
  methods GET_BANNER_FROM_SITE
    importing
      !IV_WERKS type WERKS_D
    returning
      value(RS_T001W) type T001W .
  methods GET_MVKE_PBS_ERROR
    importing
      !IV_MATNR type MATNR
      !IV_WERKS type WERKS_D
      !IV_BWSCL type BWSCL
      !IT_MVKE_DATA type MVKE_TT optional
    exporting
      !ES_MVKE type MVKE
    returning
      value(RV_ERROR) type FLAG .
  class-methods GET_BANNER_REFSITE
    importing
      !IV_VKORG type T001W-VKORG
    returning
      value(RS_BANNER_REFSITE) type TY_BANNER_REFSITE .
protected section.
private section.

  class-data ST_PBS type ZTARN_PBS_TEXT .
ENDCLASS.



CLASS ZCL_ARN_PBS_DATA_CHECK IMPLEMENTATION.


  METHOD get_banner_from_site.

    CLEAR ms_t001w.
    SELECT SINGLE * FROM t001w
      INTO ms_t001w
      WHERE werks = iv_werks.
    IF rs_t001w IS REQUESTED.
      rs_t001w  = ms_t001w.
    ENDIF.

  ENDMETHOD.


  METHOD get_banner_refsite.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 14.05.2018
* SYSTEM           # DE0
* SPECIFICATION    # CI18-348
* SAP VERSION      # SAP_BASIS  740 0012
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Get Banner Reference site
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

    DATA: ls_banner_refsite LIKE LINE OF st_banner_refsite.

    IF ( st_banner_refsite[] IS INITIAL ).

      SELECT sales_org,
             reference_site
        FROM zca_salesorg_ext
        INTO TABLE @st_banner_refsite.

    ENDIF.

    rs_banner_refsite = VALUE #( st_banner_refsite[ vkorg = iv_vkorg ] OPTIONAL ).

  ENDMETHOD.


  METHOD get_marc.

*  Return the MARC data

    SELECT a~* "matnr a~werks a~bwscl
      INTO CORRESPONDING FIELDS OF TABLE @me->mt_marc
      FROM marc AS a
      JOIN
      t001w AS b
      ON a~werks = b~werks
      WHERE a~matnr = @iv_matnr
        AND b~vkorg = @iv_vkorg
        AND b~vtweg = @iv_vtweg.
    IF rt_marc IS REQUESTED.
      rt_marc = me->mt_marc.
    ENDIF.


  ENDMETHOD.


  METHOD get_marc_pbs_error.

    DATA: lv_werks      LIKE iv_werks,
          lt_pbs_code_r TYPE ty_t_range_pbs_values,
          lt_marc_bwscl TYPE marc_tt,
          lt_marc_pbs   TYPE marc_tt,
          ls_marc_db    TYPE marc,
          ls_marc       TYPE marc.

    FIELD-SYMBOLS: <ls_marc> TYPE marc.

    CLEAR: rv_error,
           es_bapiret2.

    lv_werks = iv_werks.

    " Get banner reference site
    DATA(lv_banner_werks) = get_banner_refsite( iv_vkorg = iv_vkorg )-werks.
    IF ( lv_werks IS INITIAL ).
      " Site is not passed in, so we are working on banner
      lv_werks = lv_banner_werks.
    ENDIF.

    " PBS can't be set for DC Banner (site)
    IF ( iv_vkorg EQ mc_vkorg_1000 ) AND ( iv_pbs_code IS NOT INITIAL ).
      " Article &, Reference banner(&): PBS entry not allowed for DC Banner
      MESSAGE e166(zmd_msg) WITH iv_matnr
                                 lv_banner_werks
                            INTO zcl_message_services=>sv_msg_dummy.
      es_bapiret2 = zcl_message_services=>convert_message_to_bapiret( ).
      rv_error = abap_true.
      RETURN.
    ENDIF.

    DATA(lt_pbs) = get_pbs( ).
    lt_pbs_code_r = VALUE #( FOR wa IN lt_pbs sign = 'I' option = 'EQ' ( low = wa-pbs_code ) ).

    IF ( iv_pbs_code IS NOT INITIAL ) AND ( iv_pbs_code NOT IN lt_pbs_code_r ).
      " Invalid value & entered for PBS
      MESSAGE e575(zmd_msg) WITH iv_pbs_code INTO zcl_message_services=>sv_msg_dummy.
      es_bapiret2 = zcl_message_services=>convert_message_to_bapiret( ).
      rv_error = abap_true.
      RETURN.
    ENDIF.

    " Load article MARC records for all the sites under the banner
    get_marc( iv_matnr = iv_matnr
              iv_vkorg = iv_vkorg
              iv_vtweg = iv_vtweg ).

    SORT me->mt_marc BY matnr werks.
    ls_marc_db = VALUE #( me->mt_marc[ matnr = iv_matnr werks = lv_werks ] OPTIONAL ).

    " To set store PBS, SOS has to be set to '1' first
    IF ( lv_werks NE lv_banner_werks ) AND
       ( iv_pbs_code IS NOT INITIAL ) AND
       ( ls_marc_db-bwscl NE mc_bwscl_1 OR iv_bwscl NE mc_bwscl_1 ).
      " Message: Article &, store &: To mark PBS, SOS has to be set to 1 first
      MESSAGE e573(zmd_msg) WITH iv_matnr lv_werks INTO zcl_message_services=>sv_msg_dummy.
      es_bapiret2 = zcl_message_services=>convert_message_to_bapiret( ).
      rv_error = abap_true.
      RETURN.
    ENDIF.

    " PBS is marked, SoS must be '1'
    IF ( lv_werks NE lv_banner_werks ) AND
       ( iv_bwscl NE mc_bwscl_1 ) AND
       ( iv_pbs_code IS NOT INITIAL OR ls_marc_db-zz_pbs IS NOT INITIAL ).
      " Message: Article &, store &: PBS was marked, SOS must be 1
      MESSAGE e574(zmd_msg) WITH iv_matnr lv_werks INTO zcl_message_services=>sv_msg_dummy.
      es_bapiret2 = zcl_message_services=>convert_message_to_bapiret( ).
      rv_error = abap_true.
      RETURN.
    ENDIF.

    TRY.

        IF it_marc_data[] IS NOT INITIAL.

          LOOP AT me->mt_marc ASSIGNING <ls_marc>.
            CLEAR ls_marc.
            READ TABLE it_marc_data INTO ls_marc
              WITH KEY matnr = <ls_marc>-matnr
                       werks = <ls_marc>-werks.
            IF sy-subrc = 0.
              <ls_marc> = ls_marc.
            ENDIF.
          ENDLOOP.

        ENDIF.

        " To set banner PBS, SoS must be '1' for all stores under the banner
        IF ( iv_pbs_code IS NOT INITIAL ) AND
           ( iv_pbs_code NE ls_marc_db-zz_pbs ) AND
           ( lv_werks EQ lv_banner_werks ).

          CLEAR lt_marc_bwscl[].
          lt_marc_bwscl = VALUE #( FOR marc IN me->mt_marc WHERE ( matnr EQ iv_matnr AND
                                                                   bwscl NE mc_bwscl_1 ) ( marc ) ).
          IF ( lt_marc_bwscl[] IS NOT INITIAL ).

            SORT lt_marc_bwscl[] BY werks DESCENDING.
            es_marc   = VALUE #( lt_marc_bwscl[ 1 ] OPTIONAL ).
            et_marc[] = lt_marc_bwscl[].

            " Message: Article &, Ref Banner(&): PBS is not allowed as SOS is not 1 for site '&'
            MESSAGE e167(zmd_msg) WITH iv_matnr lv_banner_werks es_marc-werks INTO zcl_message_services=>sv_msg_dummy.
            es_bapiret2 = zcl_message_services=>convert_message_to_bapiret( ).
            rv_error = abap_true.
            RETURN.

          ENDIF.

        ENDIF.

        " Get banner PBS (exiting or new)
        CLEAR lt_marc_pbs[].
        lt_marc_pbs = VALUE #( FOR marc IN me->mt_marc WHERE ( matnr EQ iv_matnr AND
                                                               zz_pbs IS NOT INITIAL ) ( marc ) ).
        DATA(lv_banner_pbs) = VALUE #( lt_marc_pbs[ werks = lv_banner_werks ]-zz_pbs OPTIONAL ).

        " SoS can't be changed from '1' to other values while banner PBS exists
        IF ( iv_bwscl NE mc_bwscl_1 ) AND
           ( iv_bwscl NE ls_marc_db-bwscl ) AND
           ( lv_banner_pbs IS NOT INITIAL ).

          SORT lt_marc_pbs[] BY werks DESCENDING.
          es_marc   = VALUE #( lt_marc_pbs[ 1 ] OPTIONAL ).
          et_marc[] = lt_marc_pbs[].

          IF ( lv_werks EQ lv_banner_werks ).
            " Message: Reference banner(&) is marked as PBS, SOS must be 1
            MESSAGE e145(zmd_msg) WITH lv_banner_werks INTO zcl_message_services=>sv_msg_dummy.
          ELSE.
            " Message: Article &, store &: Reference banner(&) is marked as PBS, SOS must be 1
            MESSAGE e165(zmd_msg) WITH iv_matnr lv_werks lv_banner_werks INTO zcl_message_services=>sv_msg_dummy.
          ENDIF.

          es_bapiret2 = zcl_message_services=>convert_message_to_bapiret( ).
          rv_error = abap_true.
          RETURN.

        ENDIF.

        " Set store level PBS not allowed before banner PBS is set
*        IF ( iv_pbs_code IS NOT INITIAL ) AND
*           ( iv_pbs_code NE ls_marc_db-zz_pbs ) AND
*           ( lv_werks NE lv_banner_werks ) AND
*           ( lv_banner_pbs IS INITIAL ).
*          " Message: Article & Site &: Reference banner(&) PBS has to be set first
*          MESSAGE e573(zmd_msg) WITH iv_matnr lv_werks lv_banner_werks INTO zcl_message_services=>sv_msg_dummy.
*          es_bapiret2 = zcl_message_services=>convert_message_to_bapiret( ).
*          rv_error = abap_true.
*          RETURN.
*        ENDIF.

        " Removing banner PBS not allowed while store PBS still exists
*        IF ( iv_pbs_code IS INITIAL ) AND
*           ( iv_pbs_code NE ls_marc_db-zz_pbs ) AND
*           ( lv_werks EQ lv_banner_werks ) AND
*           ( lt_marc_pbs[] IS NOT INITIAL ).
*
*          SORT lt_marc_pbs[] BY werks DESCENDING.
*          es_marc   = VALUE #( lt_marc_pbs[ 1 ] OPTIONAL ).
*          et_marc[] = lt_marc_bwscl[].
*
*          " Message: Article & Site & still has PBS indicator set
*          MESSAGE e574(zmd_msg) WITH iv_matnr es_marc-werks INTO zcl_message_services=>sv_msg_dummy.
*          es_bapiret2 = zcl_message_services=>convert_message_to_bapiret( ).
*          rv_error = abap_true.
*          RETURN.
*
*        ENDIF.

      CATCH cx_sy_itab_line_not_found ##NO_HANDLER.

    ENDTRY.

  ENDMETHOD.


  METHOD GET_MVKE.

**  Return the MVKE data
*    SELECT a~*
*      INTO CORRESPONDING FIELDS OF TABLE @me->mt_mvke
*      FROM mvke AS a
*      JOIN
*      t001w AS b
*      ON  a~vkorg = b~vkorg
*      AND a~vtweg = b~vtweg
*      WHERE a~matnr = @iv_matnr
*        AND b~werks = @iv_werks.
*    IF rt_mvke IS REQUESTED.
*      rt_mvke = me->mt_mvke.
*    ENDIF.


  ENDMETHOD.


  METHOD GET_MVKE_PBS_ERROR.

*    DATA: lt_mvke TYPE mvke_tt,
*          ls_mvke TYPE mvke.
*
*
*    FIELD-SYMBOLS: <ls_mvke> TYPE mvke.
*
*    CLEAR rv_error.
*
*    get_pbs_values_tvarvc( ).
*
*    get_mvke( iv_matnr = iv_matnr
*              iv_werks = iv_werks ).
*
*    get_banner_from_site( iv_werks = iv_werks ).
*
*    TRY.
*
*        IF it_mvke_data[] IS NOT INITIAL.
*          LOOP AT me->mt_mvke ASSIGNING <ls_mvke>.
*            CLEAR ls_mvke.
*            READ TABLE it_mvke_data INTO ls_mvke
*              WITH KEY matnr = <ls_mvke>-matnr
*                       vkorg = <ls_mvke>-vkorg
*                       vtweg = <ls_mvke>-vtweg.
*            IF sy-subrc = 0.
*              <ls_mvke> = ls_mvke.
*            ENDIF.
*          ENDLOOP.
*        ENDIF.
*
*        CLEAR: lt_mvke.
*
*        TRY.
*            lt_mvke = VALUE #( FOR mvke IN me->mt_mvke WHERE ( matnr = iv_matnr       AND
*                                                               vkorg = ms_t001w-vkorg AND
*                                                               vtweg = ms_t001w-vtweg AND
*                                                               mvgr3 IN st_pbs_values[] ) ( mvke ) ).
*          CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
*        ENDTRY.
*
*        IF lt_mvke[] IS NOT INITIAL AND
*           ( iv_bwscl NE mc_bwscl_1 AND
*             iv_bwscl IS NOT INITIAL ).
*
*          SORT lt_mvke[] BY vkorg vtweg.
*
*          TRY.
*              es_mvke = lt_mvke[ matnr = iv_matnr
*                                     vkorg = ms_t001w-vkorg
*                                     vtweg = ms_t001w-vtweg ].
*            CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
*          ENDTRY.
*
*          rv_error = abap_true.
*        ENDIF.
*
*
*      CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
*    ENDTRY.


  ENDMETHOD.


  METHOD get_pbs.
*  Return the PBS with text

    IF st_pbs[] IS INITIAL.

      SELECT pbs_code  AS pbs_code
             name_text AS pbs_name_text
        INTO CORRESPONDING FIELDS OF TABLE st_pbs
        FROM zmd_pay_by_scant
        WHERE ( spras EQ sy-langu ).

    ENDIF.
    IF rt_pbs IS REQUESTED.
      rt_pbs = st_pbs.
    ENDIF.

  ENDMETHOD.


  METHOD get_pbs_single.

* Return single PBS(with text)

* Load
    get_pbs( ).

    TRY.
        rs_pbs = st_pbs[ pbs_code = iv_pbs ].
      CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
    ENDTRY.

  ENDMETHOD.


  METHOD GET_PBS_VALUES_TVARVC.
*
*    IF st_pbs_values[] IS INITIAL.
*      SELECT sign
*             opti AS option
*             low
*             high
*        INTO CORRESPONDING FIELDS OF TABLE st_pbs_values[]
*        FROM tvarvc
*        WHERE name = mc_pbs_values_tvarvc
*          AND type = 'S'.
*    ENDIF.


  ENDMETHOD.
ENDCLASS.
