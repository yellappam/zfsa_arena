*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Functional Specification 3113
* DATE WRITTEN     # 17112015
* SAP VERSION      # 731
* TYPE             # Function Module
* AUTHOR           # C90001363, Jitin Kharbanda
*-----------------------------------------------------------------------
* TITLE            # Update data into DB
* PURPOSE          # National Data Update from PI into DB
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------
FUNCTION zarn_national_data_update.
*"----------------------------------------------------------------------
*"*"Update Function Module:
*"
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(IT_PROD_DATA) TYPE  ZTARN_PROD_DATA
*"----------------------------------------------------------------------


  DATA: ls_prod_data     TYPE zsarn_prod_data,
        ls_prod_data_upd TYPE zsarn_prod_data,
        lv_itabname      TYPE char50,
        lt_dfies         TYPE dfies_tab,
        ls_dfies         TYPE dfies.

  FIELD-SYMBOLS: <table>     TYPE table,
                 <table_upd> TYPE table,
                 <table_str> TYPE any,
                 <table_val> TYPE any.


* Get Fields of the table
  REFRESH: lt_dfies[].
  CALL FUNCTION 'DDIF_FIELDINFO_GET'
    EXPORTING
      tabname        = 'ZSARN_PROD_DATA'
      langu          = sy-langu
      all_types      = abap_true
    TABLES
      dfies_tab      = lt_dfies[]
    EXCEPTIONS
      not_found      = 1
      internal_error = 2
      OTHERS         = 3.
  IF sy-subrc <> 0.
    EXIT.
  ENDIF.

* For each table -
* Collect the national table data for all products into one table
* so as to update DB tables at once for all products
  CLEAR ls_prod_data_upd.
  LOOP AT it_prod_data[] INTO ls_prod_data.
    LOOP AT lt_dfies[] INTO ls_dfies.

      IF ls_dfies-fieldname = 'IDNO' OR ls_dfies-fieldname = 'VERSION'.
        CONTINUE.
      ENDIF.

      IF <table>      IS ASSIGNED. UNASSIGN <table>.       ENDIF.
      IF <table_upd>  IS ASSIGNED. UNASSIGN <table_upd>.   ENDIF.

      CLEAR: lv_itabname.
      CONCATENATE 'LS_PROD_DATA-' ls_dfies-fieldname INTO lv_itabname.
      ASSIGN (lv_itabname) TO <table>.

      CLEAR: lv_itabname.
      CONCATENATE 'LS_PROD_DATA_UPD-' ls_dfies-fieldname INTO lv_itabname.
      ASSIGN (lv_itabname) TO <table_upd>.


      APPEND LINES OF <table> TO <table_upd>.

      DELETE FROM (ls_dfies-fieldname) WHERE idno    = ls_prod_data-idno    "#EC CI_IMUD_NESTED
                                         AND version = ls_prod_data-version.

    ENDLOOP.  " LOOP AT lt_dfies[] INTO ls_dfies.
  ENDLOOP.  " LOOP AT it_prod_data[] INTO ls_prod_data





* Update National Data
  LOOP AT lt_dfies[] INTO ls_dfies.

    IF ls_dfies-fieldname = 'IDNO' OR ls_dfies-fieldname = 'VERSION'.
      CONTINUE.
    ENDIF.

    IF <table_upd>  IS ASSIGNED. UNASSIGN <table_upd>.   ENDIF.

    CLEAR: lv_itabname.
    CONCATENATE 'LS_PROD_DATA_UPD-' ls_dfies-fieldname INTO lv_itabname.
    ASSIGN (lv_itabname) TO <table_upd>.

    IF <table_upd> IS NOT INITIAL.
      MODIFY (ls_dfies-fieldname) FROM TABLE <table_upd>.  "#EC CI_IMUD_NESTED
    ENDIF.

  ENDLOOP.  " LOOP AT lt_dfies[] INTO ls_dfies.



ENDFUNCTION.
