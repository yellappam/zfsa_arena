*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_ARTICLE_LINK_PBO
*&---------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*&      Module  STATUS_9000  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE status_9000 OUTPUT.

  CONSTANTS : lc_pf TYPE char20 VALUE 'ZPF_ARN_ALINK',
              lc_tb TYPE char20 VALUE 'ZTB_ARN_ALINK'.
* Set PF status and title bar
  SET PF-STATUS lc_pf.
  SET TITLEBAR  lc_tb.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  DISPLAY_ALV_9000  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE display_alv_9000 OUTPUT.

* Display ALV
  PERFORM display_alv.

ENDMODULE.
