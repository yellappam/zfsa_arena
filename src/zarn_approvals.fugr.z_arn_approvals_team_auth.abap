FUNCTION z_arn_approvals_team_auth.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(TEAM_CODE) TYPE  ZARN_TEAM_CODE
*"  EXPORTING
*"     REFERENCE(AUTHORIZED) TYPE  FLAG
*"----------------------------------------------------------------------

  IF sy-sysid EQ 'DE0' AND sy-uname EQ 'C90001448'.
    authorized = abap_true.
    EXIT.
  ENDIF.

  authorized = abap_false.

  AUTHORITY-CHECK OBJECT 'ZARN_TEAM'
           ID 'ZARN_TEAM' FIELD team_code.
  IF sy-subrc EQ 0.
    authorized = abap_true.
  ENDIF.

ENDFUNCTION.
