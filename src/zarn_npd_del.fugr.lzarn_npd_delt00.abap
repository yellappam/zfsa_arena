*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 11.12.2018 at 09:43:59
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_NPD_DEL_T..................................*
DATA:  BEGIN OF STATUS_ZARN_NPD_DEL_T                .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_NPD_DEL_T                .
CONTROLS: TCTRL_ZARN_NPD_DEL_T
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_NPD_DEL_T                .
TABLES: ZARN_NPD_DEL_T                 .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
