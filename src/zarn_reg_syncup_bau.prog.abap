*&---------------------------------------------------------------------*
*&  Include           ZARN_REG_SYNCUP_BAU
*&---------------------------------------------------------------------*
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 13.04.2018
* CHANGE No.       # Charm 9000003482; Jira CI18-554
* DESCRIPTION      # CI18-554 Recipe Management - Ingredient Changes
*                  # New regional table for Allergen Type overrides and
*                  # fields for Ingredients statement overrides in Arena.
*                  # New Ingredients Type field in the Scales block in
*                  # Arena and NONSAP tab in material master.
* WHO              # C90005557, Tessa Newman
*-----------------------------------------------------------------------
* DATE             # 06.06.2018
* CHANGE No.       # ChaRM 9000003675; JIRA CI18-506 NEW Range Status flag
* DESCRIPTION      # New Range Flag, Range Availability and Range Detail
*                  # fields added in Arena and on material master in Listing
*                  # tab and Basic Data Text tab (materiallongtext).
* WHO              # C90005557, Tessa Newman
*-----------------------------------------------------------------------
*&---------------------------------------------------------------------*
*&      Form  FILL_ARENA_TABLES_BAU
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM fill_arena_tables_bau .

  DATA ls_mara                  LIKE LINE OF gt_mara.
  DATA ls_main                  LIKE LINE OF gt_main.
  DATA ls_arn_products          LIKE LINE OF gt_arn_products.
  DATA ls_arn_prd_ver           LIKE LINE OF gt_arn_prd_ver.
  DATA ls_arn_reg_hdr           TYPE zarn_reg_hdr.
  DATA lt_arn_reg_banner        TYPE ztarn_reg_banner.
  DATA lt_arn_reg_uom           TYPE ztarn_reg_uom.
  DATA lt_arn_reg_uom_lay_pal   TYPE ztarn_reg_uom.
  DATA lt_arn_reg_ean           TYPE ztarn_reg_ean.
  DATA lt_arn_reg_pir           TYPE ztarn_reg_pir.
  DATA lt_arn_reg_hsno          TYPE ztarn_reg_hsno.
  DATA ls_arn_ver_status        TYPE zarn_ver_status.


  FREE: gt_arn_ver_status,gt_arn_reg_banner_all, gt_arn_reg_ean_all,
        gt_arn_reg_uom_all, gt_arn_reg_pir_all, gt_arn_reg_hdr, gt_arn_reg_uom_ui,
        gt_pir_not_found_all, gt_arn_reg_hsno_all.


  LOOP AT gt_mara INTO ls_mara.
    CLEAR: ls_arn_reg_hdr, ls_arn_products, ls_main, ls_arn_prd_ver, ls_arn_ver_status.
    FREE: lt_arn_reg_banner, lt_arn_reg_ean, lt_arn_reg_uom, lt_arn_reg_uom_lay_pal, lt_arn_reg_pir,
          gt_pir_not_found, lt_arn_reg_hsno.


**********************************************************************
*********************FILL REGIONAL HEADER TABLE***********************
**********************************************************************
    ls_arn_reg_hdr-mandt = sy-mandt.

    READ TABLE gt_arn_products INTO ls_arn_products WITH TABLE KEY fan_id = ls_mara-zzfan.
    IF sy-subrc EQ 0.

**   IDNO
      ls_arn_reg_hdr-idno = ls_arn_products-idno.


      " VERSION
      READ TABLE gt_main INTO ls_main WITH TABLE KEY zzfan = ls_mara-zzfan.
      IF sy-subrc EQ 0.
**      where ZARN_PRD_VERSION-VERSION=Version in the file & ZARN_PRD_VERSION-IDNO = ZARN_REG_HDR-IDNO
        READ TABLE gt_arn_prd_ver INTO ls_arn_prd_ver
          WITH TABLE KEY idno = ls_arn_products-idno version = ls_main-version.
        IF sy-subrc EQ 0.
          ls_arn_reg_hdr-version = ls_arn_prd_ver-version.
        ELSE.
**        ZARN_VER_STATUS-CURRENT_VER where ZARN_VER_STATUS-IDNO = ZARN_REG_HDR-IDNO
          READ TABLE gt_arn_curr_prd_ver INTO ls_arn_prd_ver
            WITH TABLE KEY idno = ls_arn_products-idno. " version = ls_main-version.
          IF sy-subrc EQ 0.
            ls_arn_reg_hdr-version = ls_arn_prd_ver-version.
          ENDIF.
        ENDIF.
      ENDIF.

    ELSE.
      " log error - MISSING IDNO
      CONTINUE.
    ENDIF.


    " Fill other header fields of Regional Header table
    PERFORM fill_reg_header_tbl_bau
      USING ls_mara
      CHANGING ls_arn_reg_hdr.

    INSERT ls_arn_reg_hdr INTO TABLE gt_arn_reg_hdr.

**********************************************************************
*********************FILL REGIONAL BANNER TABLE***********************
**********************************************************************

    " Fill other fields for Regional banner table
    PERFORM fill_reg_banner_bau
      USING ls_mara-matnr ls_arn_reg_hdr-idno
      CHANGING lt_arn_reg_banner.

    IF lt_arn_reg_banner[] IS NOT INITIAL.
      INSERT LINES OF lt_arn_reg_banner INTO TABLE gt_arn_reg_banner_all.
    ENDIF.

**********************************************************************
********** Fill REGIONAL UOM TABLE- RETAIL/INNER/SHIPPER**************
**********************************************************************
    FREE: gt_marm_idno[], gt_arn_reg_uom_ui[].

    PERFORM fill_marm_per_idno USING ls_mara-matnr ls_arn_reg_hdr-idno.

** Fill ZARN_REG_UOM for RETAIL/INNER/SHIPPER
    PERFORM fill_reg_uom_bau
      USING ls_mara ls_arn_reg_hdr-version ls_arn_reg_hdr-idno
      CHANGING lt_arn_reg_uom.

*    IF lt_arn_reg_uom[] IS NOT INITIAL.
*      INSERT LINES OF lt_arn_reg_uom INTO TABLE gt_arn_reg_uom_all.
*    ENDIF.

**********************************************************************
*************** Fill REGIONAL UOM TABLE- LAYER/PALLET*****************
**********************************************************************
** Fill ZARN_REG_UOM for LAYER and PALLET:
** Read all the ZARN_PIR-HYBRIS_INTERNAL_CODE
** where IDNO=ZARN_REG_HDR_IDNO & VERSION = ZARN_REG_HDR-VERSION
    PERFORM fill_reg_uom_lay_pal_bau
      USING ls_mara ls_arn_reg_hdr-version ls_arn_reg_hdr-idno
      CHANGING lt_arn_reg_uom_lay_pal.

*    IF lt_arn_reg_uom_lay_pal[] IS NOT INITIAL.
*      INSERT LINES OF lt_arn_reg_uom_lay_pal INTO TABLE gt_arn_reg_uom_all.
*    ENDIF.


    " fill obsolete UOMS, already existing UOMs which are not in National anymore
    " or Local Uoms which are added by user and may be posted
    PERFORM fill_reg_uom_obsolete_bau USING ls_mara ls_arn_reg_hdr-version ls_arn_reg_hdr-idno.


    " Fill global table for UOMs
    IF gt_arn_reg_uom_ui[] IS NOT INITIAL.
      APPEND LINES OF gt_arn_reg_uom_ui TO gt_arn_reg_uom_all_ui.
    ENDIF.

    " Set UOM flags
    PERFORM set_reg_uom_flags_bau USING ls_arn_reg_hdr-version ls_arn_reg_hdr-idno.

**********************************************************************
***************************Fill Regional EAN TABLE********************
**********************************************************************
**  Fill ZARN_REG_EAN EAN for Retail/Inner/Shipper in ZARN_REG_UOM-PIM_UOM_CODE
    PERFORM fill_reg_ean_bau
      USING ls_mara-matnr ls_arn_reg_hdr-idno
            gt_arn_reg_uom_all
      CHANGING lt_arn_reg_ean.

    IF lt_arn_reg_ean[] IS NOT INITIAL.
      INSERT LINES OF lt_arn_reg_ean INTO TABLE gt_arn_reg_ean_all.
    ENDIF.

**********************************************************************
*********************** Fill REGIONAL PIR TABLE***********************
**********************************************************************
**  Fill ZARN_REG_PIR table
    PERFORM fill_reg_pir_bau
      USING ls_mara ls_arn_reg_hdr-version ls_arn_reg_hdr-idno
      CHANGING lt_arn_reg_pir.

    IF lt_arn_reg_pir[] IS NOT INITIAL.
      INSERT LINES OF lt_arn_reg_pir INTO TABLE gt_arn_reg_pir_all.
    ENDIF.


**********************************************************************
*********************** Fill REGIONAL HSNO TABLE***********************
**********************************************************************
**  Fill ZARN_REG_HSNO table
    PERFORM fill_reg_hsno_bau
      USING ls_mara ls_arn_reg_hdr-version ls_arn_reg_hdr-idno
      CHANGING lt_arn_reg_hsno.

    IF lt_arn_reg_hsno[] IS NOT INITIAL.
      INSERT LINES OF lt_arn_reg_hsno INTO TABLE gt_arn_reg_hsno_all.
    ENDIF.


**  Fill ZARN_VER_STATUS table - Only CURRENT_VER If SYNC BACK successfully

    READ TABLE gt_arn_ver_status_in_db_old INTO ls_arn_ver_status
    WITH TABLE KEY idno = ls_arn_products-idno.


    ls_arn_ver_status-mandt = sy-mandt.
    ls_arn_ver_status-idno = ls_arn_reg_hdr-idno.

    ls_arn_ver_status-article_ver = ls_arn_reg_hdr-version.

    ls_arn_ver_status-changed_on = sy-datlo.

    ls_arn_ver_status-changed_at = sy-timlo.

    ls_arn_ver_status-changed_by = sy-uname.

    INSERT ls_arn_ver_status INTO TABLE gt_arn_ver_status.

  ENDLOOP.

  PERFORM get_not_found_pir_bau.



*  LOOP AT gt_arn_sync TRANSPORTING NO FIELDS
*  WHERE   tabname = 'ZARN_REG_HDR'
*     OR ( tabname = 'ZARN_REG_BANNER'
*          AND ( fldname = 'SOURCE_UNI' OR fldname = 'SOURCE_LNI' OR fldname = 'ZZCATMAN') ).
*    EXIT.
*  ENDLOOP.
*  IF sy-subrc NE 0.
*    CLEAR: gt_arn_reg_hdr_in_db_old[], gt_arn_reg_hdr[].
*    FREE : gt_arn_reg_hdr_in_db_old[], gt_arn_reg_hdr[].
*  ENDIF.

  LOOP AT gt_arn_sync TRANSPORTING NO FIELDS
  WHERE   tabname = 'ZARN_REG_BANNER'
     OR ( tabname = 'ZARN_REG_HDR'
          AND ( fldname = 'BWSCL' OR fldname = 'ZZ_UNI_DC' OR fldname = 'ZZ_LNI_DC' OR
                fldname = 'GIL_ZZCATMAN' OR fldname = 'RET_ZZCATMAN' ) )
     OR ( tabname = 'ZARN_REG_UOM'
          AND ( fldname = 'SALES_UNIT' ) ).
    EXIT.
  ENDLOOP.
  IF sy-subrc NE 0.
    CLEAR: gt_arn_reg_banner_in_db_old[], gt_arn_reg_banner_all[].
    FREE : gt_arn_reg_banner_in_db_old[], gt_arn_reg_banner_all[].
  ENDIF.


  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
  WITH KEY tabname = 'ZARN_REG_UOM'.
  IF sy-subrc NE 0.
    READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH KEY tabname = 'ZARN_REG_BANNER'
             fldname = 'VRKME'.
    IF sy-subrc NE 0.
      CLEAR: gt_arn_reg_uom_in_db_old[], gt_arn_reg_uom_all[].
      FREE : gt_arn_reg_uom_in_db_old[], gt_arn_reg_uom_all[].
    ENDIF.
  ENDIF.


  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
  WITH KEY tabname = 'ZARN_REG_EAN'.
  IF sy-subrc NE 0.
    CLEAR: gt_arn_reg_ean_in_db_old[], gt_arn_reg_ean_all[].
    FREE : gt_arn_reg_ean_in_db_old[], gt_arn_reg_ean_all[].
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
  WITH KEY tabname = 'ZARN_REG_PIR'.
  IF sy-subrc NE 0.
    CLEAR: gt_arn_reg_pir_in_db_old[], gt_arn_reg_pir_all[].
    FREE : gt_arn_reg_pir_in_db_old[], gt_arn_reg_pir_all[].
  ENDIF.

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
  WITH KEY tabname = 'ZARN_REG_HSNO'.
  IF sy-subrc NE 0.
    CLEAR: gt_arn_reg_hsno_in_db_old[], gt_arn_reg_hsno_all[].
    FREE : gt_arn_reg_hsno_in_db_old[], gt_arn_reg_hsno_all[].
  ENDIF.



  gt_arn_reg_hdr_in_db      = gt_arn_reg_hdr.
  gt_arn_reg_banner_in_db   = gt_arn_reg_banner_all.
  gt_arn_reg_uom_in_db      = gt_arn_reg_uom_all.
  gt_arn_reg_ean_in_db      = gt_arn_reg_ean_all.
  gt_arn_reg_pir_in_db      = gt_arn_reg_pir_all.
  gt_arn_ver_status_in_db   = gt_arn_ver_status.
  gt_arn_reg_hsno_in_db     = gt_arn_reg_hsno_all.





ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  FILL_REG_HEADER_TBL_BAU
*&---------------------------------------------------------------------*
* Fill other header fields of Regional Header table
*----------------------------------------------------------------------*
FORM fill_reg_header_tbl_bau USING fi_s_mara        TYPE mara
                          CHANGING fc_s_arn_reg_hdr TYPE zarn_reg_hdr.


  DATA ls_makt              LIKE LINE OF gt_makt.
  DATA ls_marc_sos          LIKE LINE OF gt_marc_sos.
  DATA ls_gil_zzcatman      LIKE LINE OF gt_gil_zzcatman.
  DATA ls_ret_zzcatman      LIKE LINE OF gt_ret_zzcatman.
  DATA ls_retdc_zzcatman    LIKE LINE OF gt_retdc_zzcatman.
  DATA ls_host_sap          LIKE LINE OF gt_host_sap.
  DATA ls_wlk2              LIKE LINE OF gt_wlk2.
  DATA ls_wrf_matgrp_prod   LIKE LINE OF gt_wrf_matgrp_prod.
  DATA ls_uom_cat           LIKE LINE OF gt_uom_cat.
  DATA ls_arn_ntr_claims    LIKE LINE OF gt_arn_ntr_claims.
  DATA ls_arn_products      LIKE LINE OF gt_arn_products.  " ++3169 JKH 20.10.2016
  DATA ls_arn_sync          TYPE zarn_sync.
  DATA ls_arn_reg_hdr_in_db LIKE LINE OF gt_arn_reg_hdr_in_db.
  DATA ls_arn_prd_ver           LIKE LINE OF gt_arn_prd_ver.
  DATA ls_main                  LIKE LINE OF gt_main.



  LOOP AT gt_arn_sync TRANSPORTING NO FIELDS
  WHERE   tabname = 'ZARN_REG_HDR'
     OR ( tabname = 'ZARN_REG_BANNER'
          AND ( fldname = 'SOURCE_UNI' OR fldname = 'SOURCE_LNI' OR fldname = 'ZZCATMAN') ).

    EXIT.
  ENDLOOP.

  IF sy-subrc NE 0.

    CLEAR ls_arn_reg_hdr_in_db.
    READ TABLE gt_arn_reg_hdr_in_db INTO ls_arn_reg_hdr_in_db
    WITH TABLE KEY idno = fc_s_arn_reg_hdr-idno.
    IF sy-subrc EQ 0.

      IF gt_arn_sync[] IS NOT INITIAL.
        ls_arn_reg_hdr_in_db-laeda = sy-datlo.
        ls_arn_reg_hdr_in_db-eruet = sy-timlo.
        ls_arn_reg_hdr_in_db-aenam = sy-uname.
      ENDIF.

      MOVE-CORRESPONDING ls_arn_reg_hdr_in_db TO fc_s_arn_reg_hdr.
    ENDIF.


    EXIT.
  ENDIF.

*  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
*  WITH KEY tabname = 'ZARN_REG_HDR'.
*  IF sy-subrc NE 0.
*
*    CLEAR ls_arn_reg_hdr_in_db.
*    READ TABLE gt_arn_reg_hdr_in_db INTO ls_arn_reg_hdr_in_db
*    WITH TABLE KEY idno = ls_arn_products-idno.
*    IF sy-subrc EQ 0.
*      MOVE-CORRESPONDING ls_arn_reg_hdr_in_db TO fc_s_arn_reg_hdr.
*    ENDIF.
*
*    EXIT.
*  ENDIF.




  CLEAR ls_arn_products.
  READ TABLE gt_arn_products INTO ls_arn_products
  WITH TABLE KEY fan_id = fi_s_mara-zzfan.

**   IDNO
  fc_s_arn_reg_hdr-idno = ls_arn_products-idno.



* If record exist in AReNa then build data using migrate version,
* else using BAU version
  CLEAR ls_arn_reg_hdr_in_db.
  READ TABLE gt_arn_reg_hdr_in_db INTO ls_arn_reg_hdr_in_db
  WITH TABLE KEY idno = ls_arn_products-idno.
  IF sy-subrc EQ 0.

    MOVE-CORRESPONDING ls_arn_reg_hdr_in_db TO fc_s_arn_reg_hdr.

    " MARA-MATNR where MARA-ZZFAN = ZARN_PRODUCTS-FAN_ID
    fc_s_arn_reg_hdr-matnr = fi_s_mara-matnr.

    LOOP AT gt_arn_sync INTO ls_arn_sync WHERE tabname = 'ZARN_REG_HDR'.
      CASE ls_arn_sync-fldname.
        WHEN 'MATKL'.
          " MATKL where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-matkl = fi_s_mara-matkl.

        WHEN 'MTART'.
          " MTART  where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-mtart = fi_s_mara-mtart.

        WHEN 'ATTYP'.
          " ATTYP  where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-attyp = fi_s_mara-attyp.

        WHEN 'STORAGE_TEMP'.
          " TEMPB  where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-storage_temp = fi_s_mara-tempb.

        WHEN 'RETAIL_NET_WEIGHT'.
          " NTGEW  where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-retail_net_weight = fi_s_mara-ntgew.

        WHEN 'RETAIL_NET_WEIGHT_UOM'.
          " GEWEI  where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-retail_net_weight_uom = fi_s_mara-gewei.

        WHEN 'RETAIL_UNIT_DESC'.
          READ TABLE gt_makt INTO ls_makt WITH TABLE KEY matnr = fi_s_mara-matnr.
          IF sy-subrc EQ 0.
            " MAKTX  where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
            fc_s_arn_reg_hdr-retail_unit_desc = ls_makt-maktx.
          ENDIF.

        WHEN 'PRDHA'.
          " PRDHA where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-prdha = fi_s_mara-prdha.

        WHEN 'BWSCL' OR 'ZZ_UNI_DC' OR 'ZZ_LNI_DC'.
          "BWSCL where WERKS=RFST & MARC-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          READ TABLE gt_marc_sos INTO ls_marc_sos
            WITH TABLE KEY matnr = fi_s_mara-matnr werks = 'RFST'.
          IF sy-subrc EQ 0.
            fc_s_arn_reg_hdr-bwscl = ls_marc_sos-bwscl.
          ENDIF.

**    ZZ_UNI_DC  where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-zz_uni_dc = fi_s_mara-zz_uni_dc.

**  ZZ_LNI_DC  where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-zz_lni_dc = fi_s_mara-zz_lni_dc.


        WHEN 'ZZPRDTYPE'.
          " ZZPRDTYPE where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-zzprdtype = fi_s_mara-zzprdtype.

        WHEN 'ZZSELLING_ONLY'.
          " ZZSELLING_ONLY where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-zzselling_only = fi_s_mara-zzselling_only.


        WHEN 'ZZBUY_SELL'.                                                   " ++ONED-217 JKH 24.11.2016
          " ZZBUY_SELL where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-zzbuy_sell = fi_s_mara-zzbuy_sell.

        WHEN 'ZZAS4SUBDEPT'.
          " ZZAS4SUBDEPT where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-zzas4subdept = fi_s_mara-zzas4subdept.

        WHEN 'ZZVAR_WT_FLAG'.
          " ZZVAR_WT_FLAG where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-zzvar_wt_flag = fi_s_mara-zzvar_wt_flag.

        WHEN 'GIL_ZZCATMAN' OR 'RET_ZZCATMAN'.
          CLEAR ls_gil_zzcatman.
          " ZZCATMAN where VKORG=3000 & VTWEG=20 &  MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          READ TABLE gt_gil_zzcatman INTO ls_gil_zzcatman WITH TABLE KEY matnr = fi_s_mara-matnr.
          IF sy-subrc EQ 0.
            fc_s_arn_reg_hdr-gil_zzcatman = ls_gil_zzcatman-zzcatman.
          ENDIF.

          " ZZCATMAN where VKORG=4000/5000/6000 & VTWEG=20
          " MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          CLEAR ls_ret_zzcatman.
          READ TABLE gt_ret_zzcatman INTO ls_ret_zzcatman
            WITH KEY matnr = fi_s_mara-matnr. " getting the first record for banner 4000/5000/6000 sorted already
          IF sy-subrc EQ 0 AND ls_ret_zzcatman-zzcatman IS NOT INITIAL.
            fc_s_arn_reg_hdr-ret_zzcatman = ls_ret_zzcatman-zzcatman.
            " If Blank then ZZCATMAN where VKORG=1000 & VTWEG=10
          ELSE.
            CLEAR ls_retdc_zzcatman.
            READ TABLE gt_retdc_zzcatman INTO ls_retdc_zzcatman WITH TABLE KEY matnr = fi_s_mara-matnr.
            IF sy-subrc EQ 0.
              fc_s_arn_reg_hdr-ret_zzcatman = ls_retdc_zzcatman-zzcatman.
            ENDIF.
          ENDIF.




        WHEN 'LEG_RETAIL' OR 'LEG_REPACK' OR 'LEG_BULK' OR 'LEG_PRBOLY'.
**********************************************************************
***************************** FILL UOM CATEGORY **********************
**********************************************************************
          READ TABLE gt_host_sap INTO ls_host_sap WITH TABLE KEY matnr = fi_s_mara-matnr.
          IF sy-subrc EQ 0.

**  UOM Category of RETAIL_UOM where LATEST=X  & MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
            CLEAR ls_uom_cat.
            READ TABLE gt_uom_cat INTO ls_uom_cat WITH KEY uom = ls_host_sap-retail_uom.
            IF sy-subrc EQ 0.
              fc_s_arn_reg_hdr-leg_retail = ls_uom_cat-uom_category.
            ENDIF.

**  UOM Category of INNER_UOM where LATEST=X  & MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
            CLEAR ls_uom_cat.
            READ TABLE gt_uom_cat INTO ls_uom_cat WITH KEY uom = ls_host_sap-inner_uom.
            IF sy-subrc EQ 0.
              fc_s_arn_reg_hdr-leg_repack = ls_uom_cat-uom_category.
            ENDIF.

**  UOM Category of SHIPPER_UOM where LATEST=X & MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
            CLEAR ls_uom_cat.
            READ TABLE gt_uom_cat INTO ls_uom_cat WITH KEY uom = ls_host_sap-shipper_uom.
            IF sy-subrc EQ 0.
              fc_s_arn_reg_hdr-leg_bulk = ls_uom_cat-uom_category.
            ENDIF.

**  PRBOLY where LATEST=X & & MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
            fc_s_arn_reg_hdr-leg_prboly = ls_host_sap-prboly.

          ENDIF.
**********************************************************************

        WHEN 'ZZSELL'.
**  ZZSELL Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-zzsell = fi_s_mara-zzsell.

        WHEN 'ZZUSE'.
**  ZZUSE Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-zzuse = fi_s_mara-zzuse.

        WHEN 'QTY_REQUIRED'.
**  KWDHT where KWDHT<> "" Where WLK2-MATNR = ZARN_REG_HDR-SAP_ARTICLE (First record)
          LOOP AT gt_wlk2 INTO ls_wlk2 WHERE matnr = fi_s_mara-matnr AND kwdht IS NOT INITIAL.
            fc_s_arn_reg_hdr-qty_required = ls_wlk2-kwdht.
            EXIT.
          ENDLOOP.

        WHEN 'PRERF'.
**  PRERF where PRERF<>"" Where WLK2-MATNR = ZARN_REG_HDR-SAP_ARTICLE (First record)
          LOOP AT gt_wlk2 INTO ls_wlk2 WHERE matnr = fi_s_mara-matnr AND prerf IS NOT INITIAL.
            fc_s_arn_reg_hdr-prerf = ls_wlk2-prerf.
            EXIT.
          ENDLOOP.

        WHEN 'PNS_TPR_FLAG'.
**  RBZUL where RBZUL<> "" & VKORG=5000 & VTWEG =20 &MATNR = ZARN_REG_HDR-SAP_ARTICLE
          READ TABLE gt_wlk2 INTO ls_wlk2 WITH TABLE KEY matnr = fi_s_mara-matnr vkorg = '5000'
            vtweg = '20' werks = ''.
          IF sy-subrc EQ 0 AND ls_wlk2-rbzul IS NOT INITIAL.
            fc_s_arn_reg_hdr-pns_tpr_flag = ls_wlk2-rbzul.
          ENDIF.

        WHEN 'SCAGR'.
**  SCAGR where SCAGR<> " " Where WLK2-MATNR = ZARN_REG_HDR-SAP_ARTICLE (First record)
          LOOP AT gt_wlk2 INTO ls_wlk2 WHERE matnr = fi_s_mara-matnr AND scagr IS NOT INITIAL.
            fc_s_arn_reg_hdr-scagr = ls_wlk2-scagr.
            EXIT.
          ENDLOOP.

        WHEN 'MSTAE'.
**   MSTAE Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-mstae = fi_s_mara-mstae.

        WHEN 'MSTDE'.
**  MSTDE Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-mstde = fi_s_mara-mstde.

        WHEN 'MSTAV'.
**  MSTAV Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-mstav = fi_s_mara-mstav.

        WHEN 'INHAL'.
** map INHAL
          fc_s_arn_reg_hdr-inhal = fi_s_mara-inhal.

        WHEN 'INHME'.
** map INHME
          fc_s_arn_reg_hdr-inhme = fi_s_mara-inhme.

        WHEN 'MSTDV'.
**  MSTDV Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-mstdv = fi_s_mara-mstdv.

        WHEN 'ZZTKTPRNT'.
**  ZZTKTPRNT Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-zztktprnt = fi_s_mara-zztktprnt.

        WHEN 'ZZPEDESC_FLAG'.
**  ZZPEDESC_FLAG Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-zzpedesc_flag = fi_s_mara-zzpedesc_flag.

        WHEN 'SAISJ'.
**  SAISJ Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-saisj = fi_s_mara-saisj.

        WHEN 'SAISO'.
**  SAISO Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-saiso = fi_s_mara-saiso.

        WHEN 'BSTAT'.
**  BSTAT Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-bstat = fi_s_mara-bstat.

        WHEN 'ZZATTR1'.
**  ZZATTR1 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-zzattr1 = fi_s_mara-zzattr1.

        WHEN 'ZZATTR2'.
**  ZZATTR2 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
*  fc_s_arn_reg_hdr-zzattr2 = fi_s_mara-zzattr2.
          READ TABLE gt_arn_growing TRANSPORTING NO FIELDS
            WITH TABLE KEY idno = fc_s_arn_reg_hdr-idno version = fc_s_arn_reg_hdr-version growing_method = 'ORGANIC'.
          IF sy-subrc EQ 0.
            fc_s_arn_reg_hdr-zzattr2 = abap_true.
          ENDIF.

        WHEN 'ZZATTR3'.
**  ZZATTR3 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
*  fc_s_arn_reg_hdr-zzattr3 = fi_s_mara-zzattr3.
          READ TABLE gt_arn_products TRANSPORTING NO FIELDS
            WITH KEY idno = fc_s_arn_reg_hdr-idno version = fc_s_arn_reg_hdr-version irradiated = abap_true.
          IF sy-subrc EQ 0.
            fc_s_arn_reg_hdr-zzattr3 = abap_true.
          ENDIF.

        WHEN 'ZZATTR4'.
**  ZZATTR4 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
*  fc_s_arn_reg_hdr-zzattr4 = fi_s_mara-zzattr4.
          READ TABLE gt_arn_diet_info TRANSPORTING NO FIELDS
            WITH KEY idno = fc_s_arn_reg_hdr-idno version = fc_s_arn_reg_hdr-version diet_type = 'KOSHER'.
          IF sy-subrc EQ 0.
            fc_s_arn_reg_hdr-zzattr4 = abap_true.
          ENDIF.

        WHEN 'ZZATTR5'.
**  ZZATTR5 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
*  fc_s_arn_reg_hdr-zzattr5 = fi_s_mara-zzattr5.
          READ TABLE gt_arn_allergen TRANSPORTING NO FIELDS WITH TABLE KEY idno = fc_s_arn_reg_hdr-idno version = fc_s_arn_reg_hdr-version.
          IF sy-subrc EQ 0.
            fc_s_arn_reg_hdr-zzattr5 = abap_true.
          ENDIF.

        WHEN 'ZZATTR6'.
**  ZZATTR6 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-zzattr6 = fi_s_mara-zzattr6.

        WHEN 'ZZATTR7'.
**  ZZATTR7 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
*  fc_s_arn_reg_hdr-zzattr7 = fi_s_mara-zzattr7.
          READ TABLE gt_arn_ntr_claims TRANSPORTING NO FIELDS WITH TABLE KEY idno = fc_s_arn_reg_hdr-idno version = fc_s_arn_reg_hdr-version
                                                                             nutritional_claims = 'LOW' nutritional_claims_elem = 'FAT'.
          IF sy-subrc EQ 0.
            fc_s_arn_reg_hdr-zzattr7 = abap_true.
          ENDIF.

        WHEN 'ZZATTR8'.
**  ZZATTR8 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-zzattr8 = fi_s_mara-zzattr8.

        WHEN 'ZZATTR9'.
**  ZZATTR9 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
*  fc_s_arn_reg_hdr-zzattr9 = fi_s_mara-zzattr9.
          READ TABLE gt_arn_diet_info TRANSPORTING NO FIELDS
            WITH KEY idno = fc_s_arn_reg_hdr-idno version = fc_s_arn_reg_hdr-version diet_type = 'HALAL'.
          IF sy-subrc EQ 0.
            fc_s_arn_reg_hdr-zzattr9 = abap_true.
          ENDIF.

        WHEN 'ZZATTR10'.
**  ZZATTR10 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
*  fc_s_arn_reg_hdr-zzattr10 = fi_s_mara-zzattr10.
          READ TABLE gt_arn_growing TRANSPORTING NO FIELDS
            WITH TABLE KEY idno = fc_s_arn_reg_hdr-idno version = fc_s_arn_reg_hdr-version growing_method = 'FREE_RANGE'.
          IF sy-subrc EQ 0.
            fc_s_arn_reg_hdr-zzattr10 = abap_true.
          ENDIF.

        WHEN 'ZZATTR11'.
**  ZZATTR11 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
*  fc_s_arn_reg_hdr-zzattr11 = fi_s_mara-zzattr11.
          READ TABLE gt_arn_diet_info TRANSPORTING NO FIELDS
            WITH KEY idno = fc_s_arn_reg_hdr-idno version = fc_s_arn_reg_hdr-version diet_type = 'FREE_FROM_GLUTEN'.
          IF sy-subrc EQ 0.
            fc_s_arn_reg_hdr-zzattr11 = abap_true.
          ENDIF.

        WHEN 'ZZATTR12'.
**  ZZATTR12 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-zzattr12 = fi_s_mara-zzattr12.

        WHEN 'ZZATTR13'.
**  ZZATTR13 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
*  fc_s_arn_reg_hdr-zzattr13 = fi_s_mara-zzattr13.
          LOOP AT gt_arn_ntr_claims INTO ls_arn_ntr_claims WHERE idno = fc_s_arn_reg_hdr-idno AND version = fc_s_arn_reg_hdr-version AND
            nutritional_claims      CS 'FREE' AND
            nutritional_claims_elem CP 'SUGAR*'.

            fc_s_arn_reg_hdr-zzattr13 = abap_true.
            EXIT.
          ENDLOOP.


        WHEN 'ZZATTR14'.
**  ZZATTR14 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
*  fc_s_arn_reg_hdr-zzattr14 = fi_s_mara-zzattr14.
          READ TABLE gt_arn_diet_info TRANSPORTING NO FIELDS
            WITH KEY idno = fc_s_arn_reg_hdr-idno version = fc_s_arn_reg_hdr-version diet_type = 'VEGAN'.
          IF sy-subrc EQ 0.
            fc_s_arn_reg_hdr-zzattr14 = abap_true.
          ENDIF.

        WHEN 'ZZATTR15'.
**  ZZATTR15 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-zzattr15 = fi_s_mara-zzattr15.

        WHEN 'ZZATTR16'.
**  ZZATTR16 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-zzattr16 = fi_s_mara-zzattr16.

        WHEN 'ZZATTR17'.
**  ZZATTR17 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-zzattr17 = fi_s_mara-zzattr17.

        WHEN 'ZZATTR18'.
**  ZZATTR18 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-zzattr18 = fi_s_mara-zzattr18.

        WHEN 'ZZATTR19'.
**  ZZATTR19 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-zzattr19 = fi_s_mara-zzattr19.

        WHEN 'ZZATTR20'.
**  ZZATTR20 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-zzattr20 = fi_s_mara-zzattr20.

        WHEN 'ZZATTR21'.
**  ZZATTR21 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-zzattr21 = fi_s_mara-zzattr21.

        WHEN 'ZZATTR22'.
**  ZZATTR22 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
*  fc_s_arn_reg_hdr-zzattr22 = fi_s_mara-zzattr22.
          READ TABLE gt_arn_ntr_claims TRANSPORTING NO FIELDS WITH TABLE KEY idno = fc_s_arn_reg_hdr-idno version = fc_s_arn_reg_hdr-version
                                                                             nutritional_claims = 'LOW' nutritional_claims_elem = 'CHOLESTEROL'.
          IF sy-subrc EQ 0.
            fc_s_arn_reg_hdr-zzattr22 = abap_true.
          ENDIF.

        WHEN 'ZZATTR23'.
**  ZZATTR23 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
*  fc_s_arn_reg_hdr-zzattr23 = fi_s_mara-zzattr23.
          READ TABLE gt_arn_diet_info TRANSPORTING NO FIELDS
            WITH KEY idno = fc_s_arn_reg_hdr-idno version = fc_s_arn_reg_hdr-version diet_type = 'VEGETARIAN'.
          IF sy-subrc EQ 0.
            fc_s_arn_reg_hdr-zzattr23 = abap_true.
          ENDIF.

        WHEN 'ZZSTRD'.
**    ZZSTRD WHERE MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-zzstrd = fi_s_mara-zzstrd.

        WHEN 'ZZSCO_WEIGHED_FLAG'.
**    ZZSCO_WEIGHED_FLAG WHERE MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-zzsco_weighed_flag = fi_s_mara-zzsco_weighed_flag.

        WHEN 'ZZSTRG'.
**    ZZSTRG WHERE MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-zzstrg = fi_s_mara-zzstrg.

        WHEN 'ZZLABNUM'.
**    ZZLABNUM WHERE MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-zzlabnum = fi_s_mara-zzlabnum.

        WHEN 'ZZPKDDT'.
**    ZZPKDDT WHERE MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-zzpkddt = fi_s_mara-zzpkddt.

        WHEN 'ZZMANDST'.
**    ZZMANDST WHERE MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-zzmandst = fi_s_mara-zzmandst.

        WHEN 'ZZWNMSG'.
**    ZZWNMSG  WHERE MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-zzwnmsg = fi_s_mara-zzwnmsg.

        WHEN 'BRAND_ID'.
* INS Begin of Change CTS-160 JKH 10.01.2017
          IF fi_s_mara-brand_id IS INITIAL.
            fc_s_arn_reg_hdr-brand_id = zcl_constants=>gc_brand_id_default.    " 'OTHM'.
          ELSE.
**    BRAND_ID Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
            fc_s_arn_reg_hdr-brand_id = fi_s_mara-brand_id.
          ENDIF.
* INS End of Change CTS-160 JKH 10.01.2017

**    BRAND_ID Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
*    fc_s_arn_reg_hdr-brand_id = fi_s_mara-brand_id.  "--CTS-160 JKH 10.01.2017


        WHEN 'MHDRZ'.
**    MHDRZ WHERE MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
          fc_s_arn_reg_hdr-mhdrz = fi_s_mara-mhdrz.

        WHEN 'ZZINRETYPE'.
          fc_s_arn_reg_hdr-zzingretype           = fi_s_mara-zzingretype.

        WHEN 'GILMOURS_WEB'.
**    IF MARA-PRDHA <> "" WHERE MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE THEN "X"
          IF fi_s_mara-prdha IS NOT INITIAL.
            fc_s_arn_reg_hdr-gilmours_web = zcl_constants=>gc_true.
          ENDIF.

        WHEN 'PLU_ARTICLE'.
**   IF COUNT(MEAN-EAN11 WHERE MEAN-EANTP='ZP' & MEAN-MATNR = ZARN_REG_HDR-SAP_ARTICLE)>0 THEN "X"
          READ TABLE gt_mean TRANSPORTING NO FIELDS WITH KEY matnr = fi_s_mara-matnr eantp = 'ZP'.
          IF sy-subrc EQ 0.
            fc_s_arn_reg_hdr-plu_article = zcl_constants=>gc_true.
          ENDIF.

        WHEN 'ARTICLE_HIERARCHY'.
**    NODE WHERE WRF_MATGRP_PROD-MATNR =  ZARN_REG_HDR-SAP_ARTICLE
          READ TABLE gt_wrf_matgrp_prod INTO ls_wrf_matgrp_prod WITH TABLE KEY matnr = fi_s_mara-matnr.
          IF sy-subrc EQ 0.
            fc_s_arn_reg_hdr-article_hierarchy = ls_wrf_matgrp_prod-node.
          ENDIF.





        WHEN 'DATA_ENRICHED'.
          fc_s_arn_reg_hdr-data_enriched = abap_true.

        WHEN 'ZZUNNUM'.
          fc_s_arn_reg_hdr-zzunnum               = ls_arn_products-dgi_unn_code.

        WHEN 'ZZPACKGRP'.
          fc_s_arn_reg_hdr-zzpackgrp             = ls_arn_products-dgi_packing_group.

        WHEN 'ZZHCLS'.
          fc_s_arn_reg_hdr-zzhcls                = ls_arn_products-dgi_class.

        WHEN 'ZZHSNO'.
          fc_s_arn_reg_hdr-zzhsno                = ls_arn_products-dgi_hazardous_code.

        WHEN 'ZZHARD'.
          fc_s_arn_reg_hdr-zzhard                = ls_arn_products-hazardous_good.

        WHEN 'DGI_REGULATION_CODE'.
          fc_s_arn_reg_hdr-dgi_regulation_code   = ls_arn_products-dgi_regulation_code.

        WHEN 'DGI_TECHNICAL_NAME'.
          fc_s_arn_reg_hdr-dgi_technical_name    = ls_arn_products-dgi_technical_name.

        WHEN 'DGI_UNN_SHIPPING_NAME'.
          fc_s_arn_reg_hdr-dgi_unn_shipping_name = ls_arn_products-dgi_unn_shipping_name.

        WHEN 'HSNO_APP'.
          fc_s_arn_reg_hdr-hsno_app              = ls_arn_products-hsno_app.

        WHEN 'HSNO_HSR'.
          fc_s_arn_reg_hdr-hsno_hsr              = ls_arn_products-hsno_hsr.

        WHEN 'FLASH_PT_TEMP_VAL'.
          fc_s_arn_reg_hdr-flash_pt_temp_val     = ls_arn_products-flash_pt_temp_val.

        WHEN 'FLASH_PT_TEMP_UOM'.
          fc_s_arn_reg_hdr-flash_pt_temp_uom     = ls_arn_products-flash_pt_temp_uom.

        WHEN 'DANGEROUS_GOOD'.
          fc_s_arn_reg_hdr-dangerous_good        = ls_arn_products-dangerous_good.

        WHEN 'ZZRANGE_FLAG'.
          fc_s_arn_reg_hdr-zzrange_flag           = fi_s_mara-zzrange_flag.

        WHEN 'ZZRANGE_AVAIL'.
          fc_s_arn_reg_hdr-zzrange_avail           = fi_s_mara-zzrange_avail.

      ENDCASE.



      LOOP AT gt_arn_sync TRANSPORTING NO FIELDS
      WHERE ( tabname = 'ZARN_REG_BANNER'  AND ( fldname = 'SOURCE_UNI' OR fldname = 'SOURCE_LNI' ) ).

        "BWSCL where WERKS=RFST & MARC-MATNR = ZARN_REG_HDR-SAP_ARTICLE
        READ TABLE gt_marc_sos INTO ls_marc_sos
          WITH TABLE KEY matnr = fi_s_mara-matnr werks = 'RFST'.
        IF sy-subrc EQ 0.
          fc_s_arn_reg_hdr-bwscl = ls_marc_sos-bwscl.
        ENDIF.

**    ZZ_UNI_DC  where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
        fc_s_arn_reg_hdr-zz_uni_dc = fi_s_mara-zz_uni_dc.

**  ZZ_LNI_DC  where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
        fc_s_arn_reg_hdr-zz_lni_dc = fi_s_mara-zz_lni_dc.

        EXIT.
      ENDLOOP.  " ZARN_REG_BANNER-SOURCE_UNI/SOURCE_LNI


      READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
      WITH TABLE KEY tabname = 'ZARN_REG_BANNER' fldname = 'ZZCATMAN'.
      IF sy-subrc = 0.

        CLEAR ls_gil_zzcatman.
        " ZZCATMAN where VKORG=3000 & VTWEG=20 &  MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
        READ TABLE gt_gil_zzcatman INTO ls_gil_zzcatman WITH TABLE KEY matnr = fi_s_mara-matnr.
        IF sy-subrc EQ 0.
          fc_s_arn_reg_hdr-gil_zzcatman = ls_gil_zzcatman-zzcatman.
        ENDIF.


        " ZZCATMAN where VKORG=4000/5000/6000 & VTWEG=20
        " MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
        CLEAR ls_ret_zzcatman.
        READ TABLE gt_ret_zzcatman INTO ls_ret_zzcatman
          WITH KEY matnr = fi_s_mara-matnr. " getting the first record for banner 4000/5000/6000 sorted already
        IF sy-subrc EQ 0 AND ls_ret_zzcatman-zzcatman IS NOT INITIAL.
          fc_s_arn_reg_hdr-ret_zzcatman = ls_ret_zzcatman-zzcatman.
          " If Blank then ZZCATMAN where VKORG=1000 & VTWEG=10
        ELSE.
          CLEAR ls_retdc_zzcatman.
          READ TABLE gt_retdc_zzcatman INTO ls_retdc_zzcatman WITH TABLE KEY matnr = fi_s_mara-matnr.
          IF sy-subrc EQ 0.
            fc_s_arn_reg_hdr-ret_zzcatman = ls_retdc_zzcatman-zzcatman.
          ENDIF.
        ENDIF.

      ENDIF.  " ZARN_REG_BANNER-ZZCATMAN



**      Current Date when changing the records
      fc_s_arn_reg_hdr-laeda = sy-datlo.

**      Current Time when changing the records
      fc_s_arn_reg_hdr-eruet = sy-timlo.

**      Current User when changing the records
      fc_s_arn_reg_hdr-aenam = sy-uname.



    ENDLOOP.  " LOOP AT gt_arn_sync INTO ls_arn_sync


  ELSE.  " READ TABLE gt_arn_reg_hdr_in_db INTO ls_arn_reg_hdr_in_db

    " MARA-MATNR where MARA-ZZFAN = ZARN_PRODUCTS-FAN_ID
    fc_s_arn_reg_hdr-matnr = fi_s_mara-matnr.

    " MIG
    fc_s_arn_reg_hdr-status = 'MIG'.

    " not to be updated
    fc_s_arn_reg_hdr-app_mode = ''.

    " not to be updated
    fc_s_arn_reg_hdr-release_status = ''.

    " MATKL where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-matkl = fi_s_mara-matkl.

    " MTART  where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-mtart = fi_s_mara-mtart.

    " ATTYP  where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-attyp = fi_s_mara-attyp.

    " TEMPB  where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-storage_temp = fi_s_mara-tempb.

    " NTGEW  where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-retail_net_weight = fi_s_mara-ntgew.

    " GEWEI  where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-retail_net_weight_uom = fi_s_mara-gewei.

    READ TABLE gt_makt INTO ls_makt WITH TABLE KEY matnr = fi_s_mara-matnr.
    IF sy-subrc EQ 0.
      " MAKTX  where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
      fc_s_arn_reg_hdr-retail_unit_desc = ls_makt-maktx.
    ENDIF.

    " PRDHA where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-prdha = fi_s_mara-prdha.

    "BWSCL where WERKS=RFST & MARC-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    READ TABLE gt_marc_sos INTO ls_marc_sos
      WITH TABLE KEY matnr = fi_s_mara-matnr werks = 'RFST'.
    IF sy-subrc EQ 0.
      fc_s_arn_reg_hdr-bwscl = ls_marc_sos-bwscl.
    ENDIF.

    " ZZPRDTYPE where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzprdtype = fi_s_mara-zzprdtype.

    " ZZSELLING_ONLY where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzselling_only = fi_s_mara-zzselling_only.

    " ZZBUY_SELL where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzbuy_sell = fi_s_mara-zzbuy_sell.         " ++ONED-217 JKH 24.11.2016

    " ZZAS4SUBDEPT where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzas4subdept = fi_s_mara-zzas4subdept.

    " ZZVAR_WT_FLAG where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzvar_wt_flag = fi_s_mara-zzvar_wt_flag.

    CLEAR ls_gil_zzcatman.
    " ZZCATMAN where VKORG=3000 & VTWEG=20 &  MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    READ TABLE gt_gil_zzcatman INTO ls_gil_zzcatman WITH TABLE KEY matnr = fi_s_mara-matnr.
    IF sy-subrc EQ 0.
      fc_s_arn_reg_hdr-gil_zzcatman = ls_gil_zzcatman-zzcatman.
    ENDIF.

    " ZZCATMAN where VKORG=4000/5000/6000 & VTWEG=20
    " MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    CLEAR ls_ret_zzcatman.
    READ TABLE gt_ret_zzcatman INTO ls_ret_zzcatman
      WITH KEY matnr = fi_s_mara-matnr. " getting the first record for banner 4000/5000/6000 sorted already
    IF sy-subrc EQ 0 AND ls_ret_zzcatman-zzcatman IS NOT INITIAL.
      fc_s_arn_reg_hdr-ret_zzcatman = ls_ret_zzcatman-zzcatman.
      " If Blank then ZZCATMAN where VKORG=1000 & VTWEG=10
    ELSE.
      CLEAR ls_retdc_zzcatman.
      READ TABLE gt_retdc_zzcatman INTO ls_retdc_zzcatman WITH TABLE KEY matnr = fi_s_mara-matnr.
      IF sy-subrc EQ 0.
        fc_s_arn_reg_hdr-ret_zzcatman = ls_retdc_zzcatman-zzcatman.
      ENDIF.
    ENDIF.

**    ZZ_UNI_DC  where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zz_uni_dc = fi_s_mara-zz_uni_dc.

**  ZZ_LNI_DC  where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zz_lni_dc = fi_s_mara-zz_lni_dc.

**********************************************************************
***************************** FILL UOM CATEGORY **********************
**********************************************************************
    READ TABLE gt_host_sap INTO ls_host_sap WITH TABLE KEY matnr = fi_s_mara-matnr.
    IF sy-subrc EQ 0.

**  UOM Category of RETAIL_UOM where LATEST=X  & MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
      CLEAR ls_uom_cat.
      READ TABLE gt_uom_cat INTO ls_uom_cat WITH KEY uom = ls_host_sap-retail_uom.
      IF sy-subrc EQ 0.
        fc_s_arn_reg_hdr-leg_retail = ls_uom_cat-uom_category.
      ENDIF.

**  UOM Category of INNER_UOM where LATEST=X  & MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
      CLEAR ls_uom_cat.
      READ TABLE gt_uom_cat INTO ls_uom_cat WITH KEY uom = ls_host_sap-inner_uom.
      IF sy-subrc EQ 0.
        fc_s_arn_reg_hdr-leg_repack = ls_uom_cat-uom_category.
      ENDIF.

**  UOM Category of SHIPPER_UOM where LATEST=X & MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
      CLEAR ls_uom_cat.
      READ TABLE gt_uom_cat INTO ls_uom_cat WITH KEY uom = ls_host_sap-shipper_uom.
      IF sy-subrc EQ 0.
        fc_s_arn_reg_hdr-leg_bulk = ls_uom_cat-uom_category.
      ENDIF.

**  PRBOLY where LATEST=X & & MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
      fc_s_arn_reg_hdr-leg_prboly = ls_host_sap-prboly.
    ENDIF.
**********************************************************************
**  ZZSELL Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzsell = fi_s_mara-zzsell.

**  ZZUSE Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzuse = fi_s_mara-zzuse.

**  KWDHT where KWDHT<> "" Where WLK2-MATNR = ZARN_REG_HDR-SAP_ARTICLE (First record)
    LOOP AT gt_wlk2 INTO ls_wlk2 WHERE matnr = fi_s_mara-matnr AND kwdht IS NOT INITIAL.
      fc_s_arn_reg_hdr-qty_required = ls_wlk2-kwdht.
      EXIT.
    ENDLOOP.

**  PRERF where PRERF<>"" Where WLK2-MATNR = ZARN_REG_HDR-SAP_ARTICLE (First record)
    LOOP AT gt_wlk2 INTO ls_wlk2 WHERE matnr = fi_s_mara-matnr AND prerf IS NOT INITIAL.
      fc_s_arn_reg_hdr-prerf = ls_wlk2-prerf.
      EXIT.
    ENDLOOP.

**  RBZUL where RBZUL<> "" & VKORG=5000 & VTWEG =20 &MATNR = ZARN_REG_HDR-SAP_ARTICLE
    READ TABLE gt_wlk2 INTO ls_wlk2 WITH TABLE KEY matnr = fi_s_mara-matnr vkorg = '5000'
      vtweg = '20' werks = ''.
    IF sy-subrc EQ 0 AND ls_wlk2-rbzul IS NOT INITIAL.
      fc_s_arn_reg_hdr-pns_tpr_flag = ls_wlk2-rbzul.
    ENDIF.

**  SCAGR where SCAGR<> " " Where WLK2-MATNR = ZARN_REG_HDR-SAP_ARTICLE (First record)
    LOOP AT gt_wlk2 INTO ls_wlk2 WHERE matnr = fi_s_mara-matnr AND scagr IS NOT INITIAL.
      fc_s_arn_reg_hdr-scagr = ls_wlk2-scagr.
      EXIT.
    ENDLOOP.

**   MSTAE Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-mstae = fi_s_mara-mstae.

**  MSTDE Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-mstde = fi_s_mara-mstde.

**  MSTAV Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-mstav = fi_s_mara-mstav.

** map INHAL
    fc_s_arn_reg_hdr-inhal = fi_s_mara-inhal.

** map INHME
    fc_s_arn_reg_hdr-inhme = fi_s_mara-inhme.

**  MSTDV Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-mstdv = fi_s_mara-mstdv.

**  ZZTKTPRNT Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zztktprnt = fi_s_mara-zztktprnt.

**  ZZPEDESC_FLAG Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzpedesc_flag = fi_s_mara-zzpedesc_flag.

**  SAISJ Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-saisj = fi_s_mara-saisj.

**  SAISO Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-saiso = fi_s_mara-saiso.

**  BSTAT Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-bstat = fi_s_mara-bstat.

**  ZZATTR1 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr1 = fi_s_mara-zzattr1.

**  ZZATTR2 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
*  fc_s_arn_reg_hdr-zzattr2 = fi_s_mara-zzattr2.
    READ TABLE gt_arn_growing TRANSPORTING NO FIELDS
      WITH TABLE KEY idno = fc_s_arn_reg_hdr-idno version = fc_s_arn_reg_hdr-version growing_method = 'ORGANIC'.
    IF sy-subrc EQ 0.
      fc_s_arn_reg_hdr-zzattr2 = abap_true.
    ENDIF.

**  ZZATTR3 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
*  fc_s_arn_reg_hdr-zzattr3 = fi_s_mara-zzattr3.
    READ TABLE gt_arn_products TRANSPORTING NO FIELDS
      WITH KEY idno = fc_s_arn_reg_hdr-idno version = fc_s_arn_reg_hdr-version irradiated = abap_true.
    IF sy-subrc EQ 0.
      fc_s_arn_reg_hdr-zzattr3 = abap_true.
    ENDIF.

**  ZZATTR4 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
*  fc_s_arn_reg_hdr-zzattr4 = fi_s_mara-zzattr4.
    READ TABLE gt_arn_diet_info TRANSPORTING NO FIELDS
      WITH KEY idno = fc_s_arn_reg_hdr-idno version = fc_s_arn_reg_hdr-version diet_type = 'KOSHER'.
    IF sy-subrc EQ 0.
      fc_s_arn_reg_hdr-zzattr4 = abap_true.
    ENDIF.

**  ZZATTR5 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
*  fc_s_arn_reg_hdr-zzattr5 = fi_s_mara-zzattr5.
    READ TABLE gt_arn_allergen TRANSPORTING NO FIELDS WITH TABLE KEY idno = fc_s_arn_reg_hdr-idno version = fc_s_arn_reg_hdr-version.
    IF sy-subrc EQ 0.
      fc_s_arn_reg_hdr-zzattr5 = abap_true.
    ENDIF.

**  ZZATTR6 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr6 = fi_s_mara-zzattr6.

**  ZZATTR7 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
*  fc_s_arn_reg_hdr-zzattr7 = fi_s_mara-zzattr7.
    READ TABLE gt_arn_ntr_claims TRANSPORTING NO FIELDS WITH TABLE KEY idno = fc_s_arn_reg_hdr-idno version = fc_s_arn_reg_hdr-version
                                                                       nutritional_claims = 'LOW' nutritional_claims_elem = 'FAT'.
    IF sy-subrc EQ 0.
      fc_s_arn_reg_hdr-zzattr7 = abap_true.
    ENDIF.

**  ZZATTR8 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr8 = fi_s_mara-zzattr8.

**  ZZATTR9 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
*  fc_s_arn_reg_hdr-zzattr9 = fi_s_mara-zzattr9.
    READ TABLE gt_arn_diet_info TRANSPORTING NO FIELDS
      WITH KEY idno = fc_s_arn_reg_hdr-idno version = fc_s_arn_reg_hdr-version diet_type = 'HALAL'.
    IF sy-subrc EQ 0.
      fc_s_arn_reg_hdr-zzattr9 = abap_true.
    ENDIF.

**  ZZATTR10 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
*  fc_s_arn_reg_hdr-zzattr10 = fi_s_mara-zzattr10.
    READ TABLE gt_arn_growing TRANSPORTING NO FIELDS
      WITH TABLE KEY idno = fc_s_arn_reg_hdr-idno version = fc_s_arn_reg_hdr-version growing_method = 'FREE_RANGE'.
    IF sy-subrc EQ 0.
      fc_s_arn_reg_hdr-zzattr10 = abap_true.
    ENDIF.

**  ZZATTR11 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
*  fc_s_arn_reg_hdr-zzattr11 = fi_s_mara-zzattr11.
    READ TABLE gt_arn_diet_info TRANSPORTING NO FIELDS
      WITH KEY idno = fc_s_arn_reg_hdr-idno version = fc_s_arn_reg_hdr-version diet_type = 'FREE_FROM_GLUTEN'.
    IF sy-subrc EQ 0.
      fc_s_arn_reg_hdr-zzattr11 = abap_true.
    ENDIF.

**  ZZATTR12 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr12 = fi_s_mara-zzattr12.

**  ZZATTR13 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
*  fc_s_arn_reg_hdr-zzattr13 = fi_s_mara-zzattr13.
    LOOP AT gt_arn_ntr_claims INTO ls_arn_ntr_claims WHERE idno = fc_s_arn_reg_hdr-idno AND version = fc_s_arn_reg_hdr-version AND
      nutritional_claims      CS 'FREE' AND
      nutritional_claims_elem CP 'SUGAR*'.

      fc_s_arn_reg_hdr-zzattr13 = abap_true.
      EXIT.
    ENDLOOP.


**  ZZATTR14 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
*  fc_s_arn_reg_hdr-zzattr14 = fi_s_mara-zzattr14.
    READ TABLE gt_arn_diet_info TRANSPORTING NO FIELDS
      WITH KEY idno = fc_s_arn_reg_hdr-idno version = fc_s_arn_reg_hdr-version diet_type = 'VEGAN'.
    IF sy-subrc EQ 0.
      fc_s_arn_reg_hdr-zzattr14 = abap_true.
    ENDIF.

**  ZZATTR15 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr15 = fi_s_mara-zzattr15.

**  ZZATTR16 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr16 = fi_s_mara-zzattr16.

**  ZZATTR17 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr17 = fi_s_mara-zzattr17.

**  ZZATTR18 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr18 = fi_s_mara-zzattr18.

**  ZZATTR19 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr19 = fi_s_mara-zzattr19.

**  ZZATTR20 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr20 = fi_s_mara-zzattr20.

**  ZZATTR21 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzattr21 = fi_s_mara-zzattr21.

**  ZZATTR22 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
*  fc_s_arn_reg_hdr-zzattr22 = fi_s_mara-zzattr22.
    READ TABLE gt_arn_ntr_claims TRANSPORTING NO FIELDS WITH TABLE KEY idno = fc_s_arn_reg_hdr-idno version = fc_s_arn_reg_hdr-version
                                                                       nutritional_claims = 'LOW' nutritional_claims_elem = 'CHOLESTEROL'.
    IF sy-subrc EQ 0.
      fc_s_arn_reg_hdr-zzattr22 = abap_true.
    ENDIF.

**  ZZATTR23 Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
*  fc_s_arn_reg_hdr-zzattr23 = fi_s_mara-zzattr23.
    READ TABLE gt_arn_diet_info TRANSPORTING NO FIELDS
      WITH KEY idno = fc_s_arn_reg_hdr-idno version = fc_s_arn_reg_hdr-version diet_type = 'VEGETARIAN'.
    IF sy-subrc EQ 0.
      fc_s_arn_reg_hdr-zzattr23 = abap_true.
    ENDIF.

**    ZZSTRD Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzstrd = fi_s_mara-zzstrd.

**    ZZSCO_WEIGHED_FLAG Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzsco_weighed_flag = fi_s_mara-zzsco_weighed_flag.

**    ZZSTRG Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzstrg = fi_s_mara-zzstrg.

**    ZZLABNUM Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzlabnum = fi_s_mara-zzlabnum.

**    ZZPKDDT Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzpkddt = fi_s_mara-zzpkddt.

**    ZZMANDST Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzmandst = fi_s_mara-zzmandst.

**    ZZWNMSG  Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzwnmsg = fi_s_mara-zzwnmsg.

* INS Begin of Change CTS-160 JKH 10.01.2017
    IF fi_s_mara-brand_id IS INITIAL.
      fc_s_arn_reg_hdr-brand_id = zcl_constants=>gc_brand_id_default.    " 'OTHM'.
    ELSE.
**    BRAND_ID Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
      fc_s_arn_reg_hdr-brand_id = fi_s_mara-brand_id.
    ENDIF.
* INS End of Change CTS-160 JKH 10.01.2017

**    BRAND_ID Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
*    fc_s_arn_reg_hdr-brand_id = fi_s_mara-brand_id.  "--CTS-160 JKH 10.01.2017

**    MHDRZ Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-mhdrz = fi_s_mara-mhdrz.
**    ZZINGRETYPE Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzingretype           = fi_s_mara-zzingretype.

**    ZZRANGE_FLAG Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzrange_flag          = fi_s_mara-zzrange_flag.

**    ZZRANGE_AVAIL Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE
    fc_s_arn_reg_hdr-zzrange_avail         = fi_s_mara-zzrange_avail.

**    If MARA-PRDHA <> "" Where MARA-MATNR = ZARN_REG_HDR-SAP_ARTICLE then "X"
    IF fi_s_mara-prdha IS NOT INITIAL.
      fc_s_arn_reg_hdr-gilmours_web = zcl_constants=>gc_true.
    ENDIF.

**   If Count(MEAN-EAN11 where MEAN-EANTP='ZP' & MEAN-MATNR = ZARN_REG_HDR-SAP_ARTICLE)>0 then "X"
    READ TABLE gt_mean TRANSPORTING NO FIELDS WITH KEY matnr = fi_s_mara-matnr eantp = 'ZP'.
    IF sy-subrc EQ 0.
      fc_s_arn_reg_hdr-plu_article = zcl_constants=>gc_true.
    ENDIF.

**    NODE Where WRF_MATGRP_PROD-MATNR =  ZARN_REG_HDR-SAP_ARTICLE
    READ TABLE gt_wrf_matgrp_prod INTO ls_wrf_matgrp_prod WITH TABLE KEY matnr = fi_s_mara-matnr.
    IF sy-subrc EQ 0.
      fc_s_arn_reg_hdr-article_hierarchy = ls_wrf_matgrp_prod-node.
    ENDIF.





* INS Begin of Change 3169 JKH 20.10.2016
    fc_s_arn_reg_hdr-data_enriched = abap_true.

    CLEAR ls_arn_products.
    READ TABLE gt_arn_products INTO ls_arn_products
    WITH TABLE KEY fan_id = fi_s_mara-zzfan.
    IF sy-subrc = 0.
      fc_s_arn_reg_hdr-zzunnum               = ls_arn_products-dgi_unn_code.
      fc_s_arn_reg_hdr-zzpackgrp             = ls_arn_products-dgi_packing_group.
      fc_s_arn_reg_hdr-zzhcls                = ls_arn_products-dgi_class.
      fc_s_arn_reg_hdr-zzhsno                = ls_arn_products-dgi_hazardous_code.
      fc_s_arn_reg_hdr-zzhard                = ls_arn_products-hazardous_good.

      fc_s_arn_reg_hdr-dgi_regulation_code   = ls_arn_products-dgi_regulation_code.
      fc_s_arn_reg_hdr-dgi_technical_name    = ls_arn_products-dgi_technical_name.
      fc_s_arn_reg_hdr-dgi_unn_shipping_name = ls_arn_products-dgi_unn_shipping_name.
      fc_s_arn_reg_hdr-hsno_app              = ls_arn_products-hsno_app.
      fc_s_arn_reg_hdr-hsno_hsr              = ls_arn_products-hsno_hsr.
      fc_s_arn_reg_hdr-flash_pt_temp_val     = ls_arn_products-flash_pt_temp_val.
      fc_s_arn_reg_hdr-flash_pt_temp_uom     = ls_arn_products-flash_pt_temp_uom.
      fc_s_arn_reg_hdr-dangerous_good        = ls_arn_products-dangerous_good.

    ENDIF.
* INS End of Change 3169 JKH 20.10.2016







**      Current Date when creating the records
    fc_s_arn_reg_hdr-ersda = sy-datlo.

**      Current Time when creating the records
    fc_s_arn_reg_hdr-erzet = sy-timlo.

**      Current UserId when creating the records
    fc_s_arn_reg_hdr-ernam = sy-uname.


  ENDIF.  " READ TABLE gt_arn_reg_hdr_in_db INTO ls_arn_reg_hdr_in_db

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  FILL_REG_BANNER_BAU
*&---------------------------------------------------------------------*
* Fill other fields for Regional banner table
*----------------------------------------------------------------------*
FORM fill_reg_banner_bau USING fi_v_matnr          TYPE matnr
                               fi_v_idno           TYPE zarn_idno
                      CHANGING fc_t_arn_reg_banner TYPE ztarn_reg_banner.


  DATA ls_tvarvc                TYPE tvarvc.
  DATA ls_marc_sos              LIKE LINE OF gt_marc_sos.
  DATA ls_arn_reg_banner        TYPE zarn_reg_banner.
  DATA ls_reg_banner            LIKE LINE OF gt_reg_banner.
  DATA lv_werks                 TYPE werks_d.
  DATA ls_wlk2                  LIKE LINE OF gt_wlk2.
  DATA ls_arn_sync              TYPE zarn_sync.
  DATA ls_arn_reg_banner_in_db  LIKE LINE OF gt_arn_reg_banner_in_db.


  LOOP AT gt_arn_sync TRANSPORTING NO FIELDS
  WHERE   tabname = 'ZARN_REG_BANNER'
     OR ( tabname = 'ZARN_REG_HDR'
          AND ( fldname = 'BWSCL' OR fldname = 'ZZ_UNI_DC' OR fldname = 'ZZ_LNI_DC' OR
                fldname = 'GIL_ZZCATMAN' OR fldname = 'RET_ZZCATMAN' ) )
     OR ( tabname = 'ZARN_REG_UOM'
          AND ( fldname = 'SALES_UNIT' ) ).

*    fc_t_arn_reg_banner[] = VALUE #( FOR banner IN gt_arn_reg_banner_in_db[]
*                                     WHERE ( idno = fi_v_idno ) ( banner ) ).

    EXIT.
  ENDLOOP.

  IF sy-subrc NE 0.
    EXIT.
  ENDIF.

*  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
*  WITH KEY tabname = 'ZARN_REG_BANNER'.
*  IF sy-subrc NE 0.
*    READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
*    WITH KEY tabname = 'ZARN_REG_UOM'
*             fldname = 'SALES_UNIT'.
*    IF sy-subrc NE 0.
*
**    fc_t_arn_reg_banner[] = VALUE #( FOR banner IN gt_arn_reg_banner_in_db[]
**                                     WHERE ( idno = fi_v_idno ) ( banner ) ).
*
*      EXIT.
*    ENDIF.
*  ENDIF.



  LOOP AT gt_reg_banner INTO ls_reg_banner WHERE matnr = fi_v_matnr.

    CLEAR ls_arn_reg_banner.

    MOVE-CORRESPONDING ls_reg_banner TO ls_arn_reg_banner.


    " fill data for all relevant banners
    ls_arn_reg_banner-mandt  = sy-mandt.
    ls_arn_reg_banner-idno   = fi_v_idno.
    ls_arn_reg_banner-banner = ls_reg_banner-vkorg.

* If record exist in AReNa then build data using migrate version,
* else using BAU version
    CLEAR ls_arn_reg_banner_in_db.
    READ TABLE gt_arn_reg_banner_in_db INTO ls_arn_reg_banner_in_db
    WITH TABLE KEY idno   = fi_v_idno
                   banner = ls_reg_banner-vkorg.
    IF sy-subrc = 0.

      MOVE-CORRESPONDING ls_arn_reg_banner_in_db TO ls_arn_reg_banner.

      CLEAR ls_wlk2.
      READ TABLE gt_wlk2 INTO ls_wlk2 WITH TABLE KEY matnr = fi_v_matnr vkorg = ls_reg_banner-vkorg
        vtweg = ls_reg_banner-vtweg werks = ''.

      LOOP AT gt_arn_sync INTO ls_arn_sync WHERE tabname = 'ZARN_REG_BANNER'.
        CASE ls_arn_sync-fldname.
          WHEN 'SOURCE_UNI' OR 'SOURCE_LNI'.

            " UNI SOS
            CLEAR: ls_marc_sos,ls_tvarvc.
            READ TABLE gt_tvarv_ref_sites_uni INTO ls_tvarvc WITH KEY low = ls_reg_banner-vkorg.
            IF sy-subrc EQ 0.
              CLEAR lv_werks.
              lv_werks = ls_tvarvc-high.
              CONDENSE lv_werks.
              READ TABLE gt_marc_sos INTO ls_marc_sos
                WITH TABLE KEY matnr = fi_v_matnr werks = lv_werks.
              IF sy-subrc EQ 0.
                ls_arn_reg_banner-source_uni = ls_marc_sos-bwscl.
              ENDIF.
            ENDIF.


            " LNI SOS
            CLEAR: ls_marc_sos,ls_tvarvc.
            READ TABLE gt_tvarv_ref_sites_lni INTO ls_tvarvc WITH KEY low = ls_reg_banner-vkorg.
            IF sy-subrc EQ 0.
              CLEAR lv_werks.
              lv_werks = ls_tvarvc-high.
              CONDENSE lv_werks.
              READ TABLE gt_marc_sos INTO ls_marc_sos
                WITH TABLE KEY matnr = fi_v_matnr werks = lv_werks.
              IF sy-subrc EQ 0.
                ls_arn_reg_banner-source_lni = ls_marc_sos-bwscl.
              ENDIF.
            ENDIF.

          WHEN 'ONLINE_STATUS'.             "++ONLD-822 JKH 17.02.2017
            " UNI SOS
            CLEAR: ls_marc_sos,ls_tvarvc.
            READ TABLE gt_tvarv_ref_sites_uni INTO ls_tvarvc WITH KEY low = ls_reg_banner-vkorg.
            IF sy-subrc EQ 0.
              CLEAR lv_werks.
              lv_werks = ls_tvarvc-high.
              CONDENSE lv_werks.
              READ TABLE gt_marc_sos INTO ls_marc_sos
                WITH TABLE KEY matnr = fi_v_matnr werks = lv_werks.
              IF sy-subrc EQ 0.
                ls_arn_reg_banner-online_status = ls_marc_sos-zzonline_status.
              ENDIF.
            ENDIF.


          WHEN 'ZZCATMAN'.
            " Category manager
**   ZZCATMAN where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
            ls_arn_reg_banner-zzcatman = ls_reg_banner-zzcatman.


          WHEN 'SSTUF'.
**    SSTUF where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
            ls_arn_reg_banner-sstuf  = ls_reg_banner-sstuf.

          WHEN 'VRKME'.
**  VRKME where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
            ls_arn_reg_banner-vrkme = ls_reg_banner-vrkme.

          WHEN 'PRODH'.
**    PRODH where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
            ls_arn_reg_banner-prodh = ls_reg_banner-prodh.

          WHEN 'VERSG'.
**    STGMA where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
            ls_arn_reg_banner-versg = ls_reg_banner-versg.

          WHEN 'KTGRM'.
**    KTGRM where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
            ls_arn_reg_banner-ktgrm = ls_reg_banner-ktgrm.

          WHEN 'QTY_REQUIRED'.
            IF ls_wlk2 IS NOT INITIAL.
*    KWDHT where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
              ls_arn_reg_banner-qty_required = ls_wlk2-kwdht.
            ENDIF.

          WHEN 'PRERF'.
            IF ls_wlk2 IS NOT INITIAL.
*    PRERF where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
              ls_arn_reg_banner-prerf = ls_wlk2-prerf.
            ENDIF.

          WHEN 'PNS_TPR_FLAG'.
            IF ls_wlk2 IS NOT INITIAL.
*    RBZUL where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
              ls_arn_reg_banner-pns_tpr_flag = ls_wlk2-rbzul.
            ENDIF.

          WHEN 'SCAGR'.
            IF ls_wlk2 IS NOT INITIAL.
*    scagr where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
              ls_arn_reg_banner-scagr = ls_wlk2-scagr.
            ENDIF.


          WHEN 'VMSTA'.
**    VMSTA where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
            ls_arn_reg_banner-vmsta = ls_reg_banner-vmsta.

          WHEN 'VMSTD'.
**  VMSTD where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
            ls_arn_reg_banner-vmstd = ls_reg_banner-vmstd.

          WHEN 'AUMNG'.
**  AUMNG where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
            ls_arn_reg_banner-aumng = ls_reg_banner-aumng.


          WHEN 'PBS_CODE'.

            " Get banner reference site
            DATA(lv_banner_werks) = zcl_arn_pbs_data_check=>get_banner_refsite( iv_vkorg = ls_reg_banner-vkorg )-werks.

            " Set banner PBS_CODE if found
            READ TABLE gt_marc_sos INTO ls_marc_sos
                 WITH KEY matnr = fi_v_matnr werks = lv_banner_werks.
            IF sy-subrc EQ 0.
              ls_arn_reg_banner-pbs_code = ls_marc_sos-zz_pbs.
            ENDIF.

        ENDCASE.

      ENDLOOP.  " LOOP AT gt_arn_sync INTO ls_arn_sync

      READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
      WITH KEY tabname = 'ZARN_REG_UOM'
               fldname = 'SALES_UNIT'.
      IF sy-subrc = 0.
**  VRKME where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
        ls_arn_reg_banner-vrkme = ls_reg_banner-vrkme.
      ENDIF.


      LOOP AT gt_arn_sync TRANSPORTING NO FIELDS
      WHERE ( tabname = 'ZARN_REG_HDR'  AND ( fldname = 'BWSCL' OR fldname = 'ZZ_UNI_DC' OR fldname = 'ZZ_LNI_DC' ) ).

        " UNI SOS
        CLEAR: ls_marc_sos,ls_tvarvc.
        READ TABLE gt_tvarv_ref_sites_uni INTO ls_tvarvc WITH KEY low = ls_reg_banner-vkorg.
        IF sy-subrc EQ 0.
          CLEAR lv_werks.
          lv_werks = ls_tvarvc-high.
          CONDENSE lv_werks.
          READ TABLE gt_marc_sos INTO ls_marc_sos
            WITH TABLE KEY matnr = fi_v_matnr werks = lv_werks.
          IF sy-subrc EQ 0.
            ls_arn_reg_banner-source_uni = ls_marc_sos-bwscl.
          ENDIF.
        ENDIF.

        " LNI SOS
        CLEAR: ls_marc_sos,ls_tvarvc.
        READ TABLE gt_tvarv_ref_sites_lni INTO ls_tvarvc WITH KEY low = ls_reg_banner-vkorg.
        IF sy-subrc EQ 0.
          CLEAR lv_werks.
          lv_werks = ls_tvarvc-high.
          CONDENSE lv_werks.
          READ TABLE gt_marc_sos INTO ls_marc_sos
            WITH TABLE KEY matnr = fi_v_matnr werks = lv_werks.
          IF sy-subrc EQ 0.
            ls_arn_reg_banner-source_lni = ls_marc_sos-bwscl.
          ENDIF.
        ENDIF.

        EXIT.
      ENDLOOP.   " ZARN_REG_HDR-BWSCL/ZZ_UNI_DC/ZZ_LNI_DC

      LOOP AT gt_arn_sync TRANSPORTING NO FIELDS
      WHERE ( tabname = 'ZARN_REG_HDR'  AND ( fldname = 'GIL_ZZCATMAN' OR fldname = 'RET_ZZCATMAN' ) ).

        " Category manager
**   ZZCATMAN where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
        ls_arn_reg_banner-zzcatman = ls_reg_banner-zzcatman.

        EXIT.
      ENDLOOP.   " ZARN_REG_HDR-GIL_ZZCATMAN/RET_ZZCATMAN


      APPEND ls_arn_reg_banner TO fc_t_arn_reg_banner.

    ELSE.  " READ TABLE gt_arn_reg_banner_in_db INTO ls_arn_reg_banner_in_db

      " UNI SOS
      CLEAR: ls_marc_sos,ls_tvarvc.
      READ TABLE gt_tvarv_ref_sites_uni INTO ls_tvarvc WITH KEY low = ls_reg_banner-vkorg.
      IF sy-subrc EQ 0.
        CLEAR lv_werks.
        lv_werks = ls_tvarvc-high.
        CONDENSE lv_werks.
        READ TABLE gt_marc_sos INTO ls_marc_sos
          WITH TABLE KEY matnr = fi_v_matnr werks = lv_werks.
        IF sy-subrc EQ 0.
          ls_arn_reg_banner-source_uni = ls_marc_sos-bwscl.
          ls_arn_reg_banner-pbs_code   = ls_marc_sos-zz_pbs.
          ls_arn_reg_banner-online_status = ls_marc_sos-zzonline_status.             "++ONLD-822 JKH 17.02.2017
        ENDIF.
      ENDIF.

      " LNI SOS
      CLEAR: ls_marc_sos,ls_tvarvc.
      READ TABLE gt_tvarv_ref_sites_lni INTO ls_tvarvc WITH KEY low = ls_reg_banner-vkorg.
      IF sy-subrc EQ 0.
        CLEAR lv_werks.
        lv_werks = ls_tvarvc-high.
        CONDENSE lv_werks.
        READ TABLE gt_marc_sos INTO ls_marc_sos
          WITH TABLE KEY matnr = fi_v_matnr werks = lv_werks.
        IF sy-subrc EQ 0.
          ls_arn_reg_banner-source_lni = ls_marc_sos-bwscl.
        ENDIF.
      ENDIF.

      " Category manager
**   ZZCATMAN where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
      ls_arn_reg_banner-zzcatman = ls_reg_banner-zzcatman.

**    SSTUF where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
      ls_arn_reg_banner-sstuf  = ls_reg_banner-sstuf.

**  VRKME where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
*        IF ls_reg_banner-vrkme IS NOT INITIAL.
      ls_arn_reg_banner-vrkme = ls_reg_banner-vrkme.
      "If Blank then MARA-MEINS" - NOT REQUIRED AS PER NEW LOGIC
*        ELSE.
*          CLEAR ls_mara-meins.
*          READ TABLE gt_mara INTO ls_mara TRANSPORTING meins WITH TABLE KEY matnr = fi_v_matnr.
*          IF sy-subrc EQ 0.
*            ls_arn_reg_banner-sales_unit = ls_mara-meins.
*          ENDIF.
*        ENDIF.

**    PRODH where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
      ls_arn_reg_banner-prodh = ls_reg_banner-prodh.


**    STGMA where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
      ls_arn_reg_banner-versg = ls_reg_banner-versg.

**    KTGRM where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
      ls_arn_reg_banner-ktgrm = ls_reg_banner-ktgrm.

      CLEAR ls_wlk2.
      READ TABLE gt_wlk2 INTO ls_wlk2 WITH TABLE KEY matnr = fi_v_matnr vkorg = ls_reg_banner-vkorg
        vtweg = ls_reg_banner-vtweg werks = ''.
      IF sy-subrc EQ 0.
*    KWDHT where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
        ls_arn_reg_banner-qty_required = ls_wlk2-kwdht.
*    PRERF where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
        ls_arn_reg_banner-prerf = ls_wlk2-prerf.
*    RBZUL where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
        ls_arn_reg_banner-pns_tpr_flag = ls_wlk2-rbzul.
*    scagr where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
        ls_arn_reg_banner-scagr = ls_wlk2-scagr.
      ENDIF.

**    VMSTA where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
      ls_arn_reg_banner-vmsta = ls_reg_banner-vmsta.

**  VMSTD where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
      ls_arn_reg_banner-vmstd = ls_reg_banner-vmstd.

**  AUMNG where ZARN_REG_BANNER-BANNER=VKORG & MVKE-MATNR = ZARN_REG_HDR-SAP_ARTICLE
      ls_arn_reg_banner-aumng = ls_reg_banner-aumng.


      APPEND ls_arn_reg_banner TO fc_t_arn_reg_banner.


    ENDIF.   " READ TABLE gt_arn_reg_banner_in_db INTO ls_arn_reg_banner_in_db

  ENDLOOP.  " LOOP AT gt_reg_banner INTO ls_reg_banner WHERE matnr = fi_v_matnr



*  " Regional BANNER update
*  IF gt_arn_reg_banner_in_db_old[] IS NOT INITIAL.
*    LOOP AT gt_arn_reg_banner_in_db_old INTO ls_arn_reg_banner
*      WHERE idno = fi_v_idno.
*
*      READ TABLE fc_t_arn_reg_banner TRANSPORTING NO FIELDS
*        WITH KEY idno   = ls_arn_reg_banner-idno
*                 banner = ls_arn_reg_banner-banner.
*      IF sy-subrc NE 0.
*        INSERT ls_arn_reg_banner INTO TABLE fc_t_arn_reg_banner.
*      ENDIF.
*    ENDLOOP.
*  ENDIF.



ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  FILL_REG_UOM_BAU
*&---------------------------------------------------------------------*
* Fill ZARN_REG_UOM for RETAIL/INNER/SHIPPER
*----------------------------------------------------------------------*
FORM fill_reg_uom_bau USING fi_s_mara        TYPE mara
                            fi_v_version     TYPE zarn_version
                            fi_v_idno        TYPE zarn_idno
                   CHANGING fc_t_arn_reg_uom TYPE ztarn_reg_uom.


  DATA ls_arn_reg_uom           LIKE LINE OF fc_t_arn_reg_uom.
  DATA ls_arn_uom_variant       LIKE LINE OF gt_arn_uom_variant.
  DATA ls_arn_uom_variant_child LIKE LINE OF gt_arn_uom_variant.
  DATA ls_arn_gtin_var_child    LIKE LINE OF gt_arn_gtin_var.
  DATA ls_uom                   LIKE LINE OF gt_uom.
  DATA ls_maw1                  LIKE LINE OF gt_maw1.
  DATA ls_arn_prod_uom_t        LIKE LINE OF gt_arn_prod_uom_t.
  DATA lt_mul_gtin      TYPE ty_t_marm_idno.
  DATA ls_mul_gtin      TYPE ty_s_marm_idno.
  DATA ls_mean          LIKE LINE OF gt_mean.
  DATA ls_arn_gtin_var  LIKE LINE OF gt_arn_gtin_var.
  DATA lv_gtin_found    TYPE boole_d.
  DATA:
    lt_marm_idno_tmp      TYPE ty_t_marm_idno,
    ls_marm_idno_tmp      TYPE ty_s_marm_idno,
    lt_reg_uom_ui_tmp     TYPE ztarn_reg_uom_ui,
    ls_reg_uom_ui_tmp     TYPE zsarn_reg_uom_ui,
    ls_reg_uom_tmp_ui     TYPE zsarn_reg_uom_ui,
    ls_uom_cat            LIKE LINE OF gt_uom_cat,
    lv_seqno              TYPE zarn_seqno,
    ls_arn_uom_variant_ui LIKE LINE OF gt_arn_uom_variant_ui,
    ls_arn_reg_uom_all    LIKE LINE OF gt_arn_reg_uom_all,
    lv_count              TYPE i,
    ls_marm_idno          LIKE LINE OF gt_marm_idno,
    lv_uom                TYPE meinh,
    lv_uom_conv_err       TYPE flag.
  DATA ls_arn_reg_uom_in_db  LIKE LINE OF gt_arn_reg_uom_in_db.


  CHECK gt_arn_uom_variant[] IS NOT INITIAL.

  FREE: fc_t_arn_reg_uom, gt_arn_uom_variant_ui.



  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
  WITH KEY tabname = 'ZARN_REG_UOM'.
  IF sy-subrc NE 0.
    READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH KEY tabname = 'ZARN_REG_BANNER'
             fldname = 'VRKME'.
    IF sy-subrc NE 0.

      LOOP AT gt_arn_reg_uom_in_db[] INTO ls_arn_reg_uom_in_db
        WHERE idno = fi_v_idno
          AND (    uom_category = 'RETAIL'
                OR uom_category = 'INNER'
                OR uom_category = 'SHIPPER' ).

        MOVE-CORRESPONDING ls_arn_reg_uom_in_db TO ls_arn_reg_uom.

        INSERT ls_arn_reg_uom INTO TABLE gt_arn_reg_uom_all.


        CLEAR ls_reg_uom_tmp_ui.
        MOVE-CORRESPONDING ls_arn_reg_uom TO ls_reg_uom_tmp_ui.

        CLEAR ls_uom_cat.
        READ TABLE gt_uom_cat INTO ls_uom_cat WITH KEY uom = ls_arn_reg_uom-meinh.
        IF sy-subrc EQ 0.
          ls_reg_uom_tmp_ui-cat_seqno = ls_uom_cat-cat_seqno.
          ls_reg_uom_tmp_ui-seqno = ls_uom_cat-seqno.
        ENDIF.

        INSERT ls_reg_uom_tmp_ui INTO TABLE gt_arn_reg_uom_ui.

      ENDLOOP.

      EXIT.
    ENDIF.
  ENDIF.






  " sort based on category seq.
  LOOP AT gt_arn_uom_variant INTO ls_arn_uom_variant WHERE idno = fi_v_idno.
    CLEAR ls_arn_uom_variant_ui.
    ls_arn_uom_variant_ui-include = ls_arn_uom_variant.
    CLEAR ls_uom_cat.

    CASE ls_arn_uom_variant-product_uom.
        " PRODUCT_UOM = EA/KGM then 'Retail'
      WHEN 'EA' OR 'KGM' OR 'KG'.
        ls_arn_uom_variant_ui-uom_category = 'RETAIL'.
        IF ls_arn_uom_variant-product_uom EQ 'KGM'.
          lv_uom = 'KG'.
        ELSE.
          lv_uom = ls_arn_uom_variant-product_uom.
        ENDIF.

        " PRODUCT_UOM = INN Then 'Inner'
      WHEN 'INN'.
        ls_arn_uom_variant_ui-uom_category = 'INNER'.
        lv_uom = ls_arn_uom_variant-product_uom.

        " PRODUCT_UOM = 'CS' Then 'Shipper'"
      WHEN 'CS'.
        ls_arn_uom_variant_ui-uom_category = 'SHIPPER'.
        lv_uom = 'CAS'.

      WHEN OTHERS.
    ENDCASE.

    READ TABLE gt_uom_cat INTO ls_uom_cat
      WITH KEY uom_category = ls_arn_uom_variant_ui-uom_category uom = lv_uom.
    IF sy-subrc EQ 0.
      ls_arn_uom_variant_ui-cat_seqno = ls_uom_cat-cat_seqno.
      ls_arn_uom_variant_ui-seqno = ls_uom_cat-seqno.
    ENDIF.
    INSERT ls_arn_uom_variant_ui INTO TABLE gt_arn_uom_variant_ui.

  ENDLOOP.  " LOOP AT gt_arn_uom_variant INTO ls_arn_uom_variant WHERE idno = fi_v_idno



  SORT gt_arn_uom_variant_ui BY cat_seqno seqno.

  LOOP AT gt_arn_uom_variant_ui INTO ls_arn_uom_variant_ui." WHERE idno = fi_v_idno. " AND version = fi_v_version.

    ls_arn_uom_variant = ls_arn_uom_variant_ui-include.

    CLEAR ls_arn_reg_uom.

    ls_arn_reg_uom-mandt = sy-mandt.

    " fill data for all relevant UOMs
    ls_arn_reg_uom-idno = fi_v_idno.
    ls_arn_reg_uom-hybris_internal_code = ''.

    " UOM_CODE where ZARN_UOM_VARIANT-IDNO=ZARN_REG_HDR-IDNO &
    " ZARN_UOM_VARIANT-VERSION = ZARN_REG_HDR-VERSION"
    ls_arn_reg_uom-pim_uom_code = ls_arn_uom_variant-uom_code.

    " If PRODUCT_UOM = EA/KGM then 'Retail' Elseif  PRODUCT_UOM = INN Then 'Inner'
    " ElseIf  PRODUCT_UOM = 'CS' Then 'Shipper'"


    CLEAR ls_arn_prod_uom_t.
    READ TABLE gt_arn_prod_uom_t INTO ls_arn_prod_uom_t
      WITH TABLE KEY product_uom = ls_arn_uom_variant-product_uom.
    IF sy-subrc EQ 0.
      ls_arn_reg_uom-uom_category = ls_arn_prod_uom_t-sap_uom_cat.
    ENDIF.

    CLEAR ls_arn_gtin_var_child.
    " ZARN_GTIN_VAR-UOM_CODE where ZARN_GTIN_VAR-GTIN_CODE = ZARN_UOM_VARIANT-CHILD_GTIN
    READ TABLE gt_arn_gtin_var INTO ls_arn_gtin_var_child
      WITH KEY idno = fi_v_idno version = fi_v_version gtin_code = ls_arn_uom_variant-child_gtin.
    IF sy-subrc EQ 0.
      ls_arn_reg_uom-lower_uom = ls_arn_gtin_var_child-uom_code.
    ENDIF.

    " ZARN_GTIN_VAR-UOM_CODE where ZARN_GTIN_VAR-GTIN_CODE = ZARN_UOM_VARIANT-CHILD_GTIN
    " & ZARN_UOM_VARIANT-UOM_CODE= ZARN_REG_UOM-LOWER_UOM
    CLEAR ls_arn_uom_variant_child.
    READ TABLE gt_arn_uom_variant INTO ls_arn_uom_variant_child
      WITH KEY idno = fi_v_idno version = fi_v_version uom_code = ls_arn_reg_uom-lower_uom.
    IF sy-subrc EQ 0.
      CLEAR ls_arn_gtin_var_child.
      READ TABLE gt_arn_gtin_var INTO ls_arn_gtin_var_child
         WITH KEY idno = fi_v_idno version = fi_v_version gtin_code = ls_arn_uom_variant_child-child_gtin.
      IF sy-subrc EQ 0.
        ls_arn_reg_uom-lower_child_uom = ls_arn_gtin_var_child-uom_code.
      ENDIF.
    ENDIF.



* If record exist in AReNa then build data using migrate version,
* else using BAU version
    CLEAR ls_arn_reg_uom_in_db.
    READ TABLE gt_arn_reg_uom_in_db INTO ls_arn_reg_uom_in_db
    WITH TABLE KEY idno                 = ls_arn_reg_uom-idno
                   uom_category         = ls_arn_reg_uom-uom_category
                   pim_uom_code         = ls_arn_reg_uom-pim_uom_code
                   hybris_internal_code = ls_arn_reg_uom-hybris_internal_code
                   lower_uom            = ls_arn_reg_uom-lower_uom
                   lower_child_uom      = ls_arn_reg_uom-lower_child_uom.
    IF sy-subrc EQ 0.

      ls_arn_reg_uom-net_weight_uom     = ls_arn_reg_uom_in_db-net_weight_uom.
      ls_arn_reg_uom-net_weight_value   = ls_arn_reg_uom_in_db-net_weight_value.
      ls_arn_reg_uom-gross_weight_uom   = ls_arn_reg_uom_in_db-gross_weight_uom.
      ls_arn_reg_uom-gross_weight_value = ls_arn_reg_uom_in_db-gross_weight_value.
      ls_arn_reg_uom-height_uom         = ls_arn_reg_uom_in_db-height_uom.
      ls_arn_reg_uom-height_value       = ls_arn_reg_uom_in_db-height_value.
      ls_arn_reg_uom-width_uom          = ls_arn_reg_uom_in_db-width_uom.
      ls_arn_reg_uom-width_value        = ls_arn_reg_uom_in_db-width_value.
      ls_arn_reg_uom-depth_uom          = ls_arn_reg_uom_in_db-depth_uom.
      ls_arn_reg_uom-depth_value        = ls_arn_reg_uom_in_db-depth_value.
      ls_arn_reg_uom-unit_base          = ls_arn_reg_uom_in_db-unit_base.
      ls_arn_reg_uom-unit_purord        = ls_arn_reg_uom_in_db-unit_purord.
      ls_arn_reg_uom-sales_unit         = ls_arn_reg_uom_in_db-sales_unit.
      ls_arn_reg_uom-issue_unit         = ls_arn_reg_uom_in_db-issue_unit.


* Get Number of Base Units
      ls_arn_reg_uom-num_base_units = ls_arn_uom_variant-num_base_units.
      IF ls_arn_reg_uom-num_base_units IS INITIAL.
        ls_arn_reg_uom-num_base_units = 1.
      ENDIF.

* Factor of Bsse Units
      ls_arn_reg_uom-factor_of_base_units = ls_arn_uom_variant-factor_of_base_units.
      IF ls_arn_reg_uom-factor_of_base_units IS INITIAL.
        ls_arn_reg_uom-factor_of_base_units = 1.
      ENDIF.


      LOOP AT gt_arn_sync TRANSPORTING NO FIELDS
      WHERE tabname = 'ZARN_REG_UOM'
        AND ( fldname = 'NET_WEIGHT_UOM' OR fldname = 'NET_WEIGHT_VALUE' ).

        TRY.
            ls_arn_reg_uom-net_weight_value     = ls_arn_uom_variant-net_weight_value.          " Net Weight Value
            ls_arn_reg_uom-net_weight_uom       = ls_arn_uom_variant-net_weight_uom.            " Net Weight UOM
          CATCH cx_sy_conversion_overflow.
            ls_arn_reg_uom-net_weight_uom     = ls_arn_reg_uom_in_db-net_weight_uom.
            ls_arn_reg_uom-net_weight_value   = ls_arn_reg_uom_in_db-net_weight_value.
        ENDTRY.

        EXIT.
      ENDLOOP.  " NET_WEIGHT_UOM/NET_WEIGHT_VALUE
*      IF sy-subrc NE 0.
*        ls_arn_reg_uom-net_weight_uom     = ls_arn_reg_uom_in_db-net_weight_uom.
*        ls_arn_reg_uom-net_weight_value   = ls_arn_reg_uom_in_db-net_weight_value.
*      ENDIF.


      LOOP AT gt_arn_sync TRANSPORTING NO FIELDS
      WHERE tabname = 'ZARN_REG_UOM'
        AND ( fldname = 'GROSS_WEIGHT_UOM' OR
              fldname = 'GROSS_WEIGHT_VALUE' OR
              fldname = 'HEIGHT_UOM' OR
              fldname = 'HEIGHT_VALUE' OR
              fldname = 'WIDTH_UOM' OR
              fldname = 'WIDTH_VALUE' OR
              fldname = 'DEPTH_UOM' OR
              fldname = 'DEPTH_VALUE' ).

        TRY.
            ls_arn_reg_uom-gross_weight_value   = ls_arn_uom_variant-gross_weight_value.            " Gross Weight Value
            ls_arn_reg_uom-gross_weight_uom     = ls_arn_uom_variant-gross_weight_uom.              " Gross Weight UOM
          CATCH cx_sy_conversion_overflow.
            ls_arn_reg_uom-gross_weight_uom   = ls_arn_reg_uom_in_db-gross_weight_uom.
            ls_arn_reg_uom-gross_weight_value = ls_arn_reg_uom_in_db-gross_weight_value.
        ENDTRY.

        TRY.
            ls_arn_reg_uom-height_value     = ls_arn_uom_variant-height_value.                  " Height Value
            ls_arn_reg_uom-height_uom       = ls_arn_uom_variant-height_uom.                    " Height UOM

* INS Begin of Change INC5409316 JKH 17.11.2016
* Convert Height into 'MM'
            zcl_arn_gui_load=>unit_conversion_simple(
                  EXPORTING input    = ls_arn_reg_uom-height_value
                            unit_in  = ls_arn_reg_uom-height_uom
                            unit_out = 'MM'
                  IMPORTING output   = ls_arn_reg_uom-height_value
                            ev_error = lv_uom_conv_err ).
            IF lv_uom_conv_err IS INITIAL.
              ls_arn_reg_uom-height_uom = 'MMT'.
            ENDIF.
* INS End of Change INC5409316 JKH 17.11.2016

          CATCH cx_sy_conversion_overflow.
            ls_arn_reg_uom-height_value     = ls_arn_reg_uom_in_db-height_value.
            ls_arn_reg_uom-height_uom       = ls_arn_reg_uom_in_db-height_uom.
        ENDTRY.

        TRY.
            ls_arn_reg_uom-width_value     = ls_arn_uom_variant-width_value.                    " Width Value
            ls_arn_reg_uom-width_uom       = ls_arn_uom_variant-width_uom.                      " Width UOM

* INS Begin of Change INC5409316 JKH 17.11.2016
* Convert width into 'MM'
            zcl_arn_gui_load=>unit_conversion_simple(
                  EXPORTING input    = ls_arn_reg_uom-width_value
                            unit_in  = ls_arn_reg_uom-width_uom
                            unit_out = 'MM'
                  IMPORTING output   = ls_arn_reg_uom-width_value
                            ev_error = lv_uom_conv_err ).
            IF lv_uom_conv_err IS INITIAL.
              ls_arn_reg_uom-width_uom  = 'MMT'.
            ENDIF.
* INS End of Change INC5409316 JKH 17.11.2016

          CATCH cx_sy_conversion_overflow.
            ls_arn_reg_uom-width_value     = ls_arn_reg_uom_in_db-width_value.
            ls_arn_reg_uom-width_uom       = ls_arn_reg_uom_in_db-width_uom.
        ENDTRY.

        TRY.
            ls_arn_reg_uom-depth_value     = ls_arn_uom_variant-depth_value.                    " Depth Value
            ls_arn_reg_uom-depth_uom       = ls_arn_uom_variant-depth_uom.                      " Depth UOM

* INS Begin of Change INC5409316 JKH 17.11.2016
* Convert depth into 'MM'
            zcl_arn_gui_load=>unit_conversion_simple(
                  EXPORTING input    = ls_arn_reg_uom-depth_value
                            unit_in  = ls_arn_reg_uom-depth_uom
                            unit_out = 'MM'
                  IMPORTING output   = ls_arn_reg_uom-depth_value
                            ev_error = lv_uom_conv_err ).
            IF lv_uom_conv_err IS INITIAL.
              ls_arn_reg_uom-depth_uom  = 'MMT'.
            ENDIF.
* INS End of Change INC5409316 JKH 17.11.2016

          CATCH cx_sy_conversion_overflow.
            ls_arn_reg_uom-depth_value     = ls_arn_reg_uom_in_db-depth_value.
            ls_arn_reg_uom-depth_uom       = ls_arn_reg_uom_in_db-depth_uom.
        ENDTRY.

        EXIT.
      ENDLOOP.
*      IF sy-subrc NE 0.
*        ls_arn_reg_uom-gross_weight_uom   = ls_arn_reg_uom_in_db-gross_weight_uom.
*        ls_arn_reg_uom-gross_weight_value = ls_arn_reg_uom_in_db-gross_weight_value.
*        ls_arn_reg_uom-height_uom         = ls_arn_reg_uom_in_db-height_uom.
*        ls_arn_reg_uom-height_value       = ls_arn_reg_uom_in_db-height_value.
*        ls_arn_reg_uom-width_uom          = ls_arn_reg_uom_in_db-width_uom.
*        ls_arn_reg_uom-width_value        = ls_arn_reg_uom_in_db-width_value.
*        ls_arn_reg_uom-depth_uom          = ls_arn_reg_uom_in_db-depth_uom.
*        ls_arn_reg_uom-depth_value        = ls_arn_reg_uom_in_db-depth_value.
*      ENDIF.


**    " check if the entry is in upload UOM file
*    READ TABLE gt_uom TRANSPORTING NO FIELDS
*      WITH KEY zzfan = fi_s_mara-zzfan hyb_uom_code = ls_arn_reg_uom-pim_uom_code hyb_pir = ''.
*    IF sy-subrc eq 0.
*


*    FILE-SAP_UOMCODE where FILE-HYBRIS_UOMCODE= ZARN_REG_UOM-PIM_UOM_CODE
      CLEAR ls_uom.
      READ TABLE gt_uom INTO ls_uom
        WITH TABLE KEY zzfan = fi_s_mara-zzfan hyb_uom_code = ls_arn_reg_uom-pim_uom_code
                       hyb_pir = ''.
      IF sy-subrc EQ 0.

        ls_arn_reg_uom-meinh = ls_uom-sap_uom_code.

**  FILE-SAP_UOMCODE where FILE-HYBRIS_UOMCODE= ZARN_REG_UOM-LOWER_UOM
        CLEAR: ls_uom, ls_arn_reg_uom-lower_meinh.
        READ TABLE gt_uom INTO ls_uom WITH TABLE KEY zzfan = fi_s_mara-zzfan hyb_uom_code = ls_arn_reg_uom-lower_uom
          hyb_pir = ''.
        IF sy-subrc EQ 0.
          ls_arn_reg_uom-lower_meinh = ls_uom-sap_uom_code.
        ENDIF.



        IF ls_arn_reg_uom-meinh IS NOT INITIAL AND ls_arn_reg_uom-lower_meinh IS NOT INITIAL.
          CLEAR ls_marm_idno .
          READ TABLE gt_marm_idno INTO ls_marm_idno
            WITH KEY meinh = ls_arn_reg_uom-meinh mesub = ls_arn_reg_uom-lower_meinh.
          IF sy-subrc NE 0.
            CLEAR ls_arn_reg_uom-meinh.
          ENDIF.
        ENDIF.

      ENDIF.




      IF ls_arn_reg_uom-meinh IS INITIAL.


        CLEAR: ls_arn_reg_uom_all, ls_arn_reg_uom-lower_meinh.
        READ TABLE gt_arn_reg_uom_all INTO ls_arn_reg_uom_all TRANSPORTING meinh
          WITH KEY idno = fi_v_idno pim_uom_code = ls_arn_reg_uom-lower_uom
          lower_uom = ls_arn_reg_uom-lower_child_uom.
        IF sy-subrc EQ 0.
          ls_arn_reg_uom-lower_meinh = ls_arn_reg_uom_all-meinh.
        ENDIF.


        " DEFAULTING LOGIC in case UOM missing in FILE
**********************************************************************
        lt_marm_idno_tmp[] = gt_marm_idno[].

        DELETE lt_marm_idno_tmp[] WHERE uom_category NE ls_arn_reg_uom-uom_category.

* Get the highest UOM at last so that If multiple UOM exist for the same conversion,
* then the highest is defaulted
        SORT lt_marm_idno_tmp[] BY seqno.

        IF ls_arn_reg_uom-uom_category NE 'RETAIL'.

* Check in MARM if UOM exist for the same conversion, get count
          CLEAR lv_count.
          CLEAR ls_marm_idno_tmp.
          FREE lt_mul_gtin[].

          LOOP AT lt_marm_idno_tmp INTO ls_marm_idno_tmp
           WHERE idno  = ls_arn_reg_uom-idno
             AND umrez = ls_arn_reg_uom-num_base_units
             AND umren = ls_arn_reg_uom-factor_of_base_units
             AND mesub = ls_arn_reg_uom-lower_meinh.

            INSERT ls_marm_idno_tmp INTO TABLE lt_mul_gtin.
            lv_count = lv_count + 1.

          ENDLOOP.

          IF lv_count GE 1.


            " check if the higest UOM's GTIN matches the national GTIN then set UOM else continue with the lower UOMs...
            SORT lt_mul_gtin BY seqno DESCENDING.
            CLEAR lv_gtin_found.

            LOOP AT lt_mul_gtin INTO ls_mul_gtin.
              LOOP AT gt_mean INTO ls_mean WHERE matnr = fi_s_mara-matnr AND meinh = ls_mul_gtin-meinh.
                CLEAR ls_arn_gtin_var.
                READ TABLE gt_arn_gtin_var INTO ls_arn_gtin_var
                  WITH KEY idno = fi_v_idno version = fi_v_version gtin_code = ls_mean-ean11.
                IF sy-subrc EQ 0.
                  lv_gtin_found = abap_true.
                  EXIT.
                ENDIF.
              ENDLOOP.
              IF lv_gtin_found EQ abap_true.
                EXIT.
              ENDIF.
            ENDLOOP.

            IF lv_gtin_found = abap_true.
              ls_arn_reg_uom-meinh = ls_mul_gtin-meinh.
            ELSE.
              ls_arn_reg_uom-meinh = ls_marm_idno_tmp-meinh.
            ENDIF.



          ELSE.



            ls_arn_reg_uom-relationship = 1.





* For MEINH, check if UOM already exist (either in DB or in new UOM),
* then get the next higher UOM
*        CLEAR: lt_reg_uom_tmp_ui[].
*          DATA ls_arn_reg_uom_temp LIKE LINE OF fc_t_arn_reg_uom.

            lt_reg_uom_ui_tmp[] = gt_arn_reg_uom_ui[].

            DELETE lt_reg_uom_ui_tmp[] WHERE uom_category NE ls_arn_reg_uom-uom_category.
            DELETE lt_reg_uom_ui_tmp[] WHERE meinh IS INITIAL.

* Get highest existing UOM
            SORT lt_reg_uom_ui_tmp[] BY seqno DESCENDING.
            CLEAR ls_reg_uom_ui_tmp.
            READ TABLE lt_reg_uom_ui_tmp[] INTO ls_reg_uom_ui_tmp INDEX 1.

* get highest UOM on the top
            SORT lt_marm_idno_tmp[] BY seqno DESCENDING.

* Get highest existing UOM from MARM
            CLEAR ls_marm_idno_tmp.
            READ TABLE lt_marm_idno_tmp[] INTO ls_marm_idno_tmp INDEX 1.

* If highest UOM is same in defaulted UOM and MARM then get the next higher UOM
* else, check the highest UOM in defaulted UOM and MARM and get the next higher UOM

            CLEAR lv_seqno.
            IF ls_reg_uom_ui_tmp-seqno = ls_marm_idno_tmp-seqno.
              lv_seqno = ls_reg_uom_ui_tmp-seqno + 1.
            ELSEIF ls_reg_uom_ui_tmp-seqno GT ls_marm_idno_tmp-seqno.
              lv_seqno = ls_reg_uom_ui_tmp-seqno + 1.
            ELSEIF ls_reg_uom_ui_tmp-seqno LT ls_marm_idno_tmp-seqno.
              lv_seqno = ls_marm_idno_tmp-seqno + 1.
            ENDIF.

            IF lv_seqno GT 0.
* Get next higher UOM to be defaulted
              CLEAR ls_uom_cat.
              READ TABLE gt_uom_cat[] INTO ls_uom_cat
              WITH KEY uom_category = ls_arn_reg_uom-uom_category
                       seqno = lv_seqno.
              IF sy-subrc = 0.
                ls_arn_reg_uom-meinh = ls_uom_cat-uom.
              ENDIF.
            ENDIF.



          ENDIF.  " READ TABLE lt_marm_idno INTO ls_marm_idno

          " RETAIL
        ELSE.

          IF ls_arn_uom_variant-product_uom EQ 'KGM'.
            lv_uom = 'KG'.
          ELSE.
            lv_uom = 'EA'.
          ENDIF.



* Check in MARM if UOM exist for the same conversion, get count
          CLEAR lv_count.
          CLEAR ls_marm_idno_tmp.
          LOOP AT lt_marm_idno_tmp INTO ls_marm_idno_tmp
           WHERE idno  = ls_arn_reg_uom-idno
             AND umrez = ls_arn_reg_uom-num_base_units
             AND umren = ls_arn_reg_uom-factor_of_base_units
             AND mesub = ls_arn_reg_uom-lower_meinh
             AND meinh = lv_uom.

            lv_count = lv_count + 1.
          ENDLOOP.

          IF lv_count GE 1.
            ls_arn_reg_uom-meinh = ls_marm_idno_tmp-meinh.
          ELSE.

            READ TABLE lt_marm_idno_tmp INTO ls_marm_idno_tmp
              WITH KEY idno  = ls_arn_reg_uom-idno meinh = lv_uom.
            IF sy-subrc NE 0.
              ls_arn_reg_uom-meinh = lv_uom.
            ENDIF.

          ENDIF.


        ENDIF. " ls_arn_reg_uom-uom_category NE 'RETAIL'.


      ENDIF.

**********************************************************************


      CLEAR: ls_arn_reg_uom_all, ls_arn_reg_uom-higher_level_units.
      READ TABLE gt_arn_reg_uom_all INTO ls_arn_reg_uom_all
        WITH KEY idno = fi_v_idno pim_uom_code = ls_arn_reg_uom-lower_uom
                 lower_uom = ls_arn_reg_uom-lower_child_uom.

      IF ls_arn_reg_uom_all-num_base_units IS INITIAL.
        ls_arn_reg_uom_all-num_base_units = 1.
      ENDIF.
      IF ls_arn_reg_uom_all-factor_of_base_units IS INITIAL.
        ls_arn_reg_uom_all-factor_of_base_units = 1.
      ENDIF.
      IF ls_arn_reg_uom-num_base_units IS INITIAL.
        ls_arn_reg_uom-num_base_units = 1.
      ENDIF.
      IF ls_arn_reg_uom-factor_of_base_units IS INITIAL.
        ls_arn_reg_uom-factor_of_base_units = 1.
      ENDIF.


      ls_arn_reg_uom-higher_level_units = ( ls_arn_reg_uom-num_base_units / ls_arn_reg_uom-factor_of_base_units ) /
                                          ( ls_arn_reg_uom_all-num_base_units / ls_arn_reg_uom_all-factor_of_base_units ) .




      READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
        WITH TABLE KEY tabname = 'ZARN_REG_UOM' fldname = 'UNIT_BASE'.
      IF sy-subrc EQ 0.
**  UNIT_BASE = "X" if above ZARN_REG_UOM-MEINH=MARA-MEINS
        CLEAR ls_arn_reg_uom-unit_base.
        IF ls_arn_reg_uom-meinh EQ fi_s_mara-meins.
          ls_arn_reg_uom-unit_base = abap_true.
        ENDIF.
*      ELSE.
*        ls_arn_reg_uom-unit_base          = ls_arn_reg_uom_in_db-unit_base.
      ENDIF.  " 'UNIT_BASE'


      READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
        WITH TABLE KEY tabname = 'ZARN_REG_UOM' fldname = 'UNIT_PURORD'.
      IF sy-subrc EQ 0.
        CLEAR ls_arn_reg_uom-unit_purord.
**  UNIT_PURORD = "X" if above ZARN_REG_UOM-MEINH=MARA-BSTME
**  OR If above ZARN_REG_UOM-MEINH=MARA-MEINS & if MARA-BSTME="" "" then ""X"""
        IF ( ls_arn_reg_uom-meinh EQ fi_s_mara-bstme AND fi_s_mara-bstme IS NOT INITIAL ) OR
          ( ls_arn_reg_uom-meinh EQ fi_s_mara-meins AND fi_s_mara-bstme IS INITIAL ).

          ls_arn_reg_uom-unit_purord = abap_true.
        ENDIF.
*      ELSE.
*        ls_arn_reg_uom-unit_purord        = ls_arn_reg_uom_in_db-unit_purord.
      ENDIF.  " 'UNIT_PURORD'



      LOOP AT gt_arn_sync TRANSPORTING NO FIELDS
      WHERE ( tabname = 'ZARN_REG_UOM' AND fldname = 'SALES_UNIT' )
         OR ( tabname = 'ZARN_REG_BANNER' AND fldname = 'VRKME' ).

**  SALES_UNIT = ""X"" if above ZARN_REG_UOM-MEINH=MAW1-WVRKM
        CLEAR: ls_maw1, ls_arn_reg_uom-sales_unit.
        READ TABLE gt_maw1 INTO ls_maw1 WITH TABLE KEY matnr = fi_s_mara-matnr.
        IF sy-subrc EQ 0 AND ls_maw1-wvrkm EQ ls_arn_reg_uom-meinh AND ls_maw1-wvrkm IS NOT INITIAL.
          ls_arn_reg_uom-sales_unit = abap_true.
**  If above ZARN_REG_UOM-MEINH=MARA-MEINS & if MAW1-WVRKM="" "" then ""X"""
        ELSEIF sy-subrc EQ 0 AND ls_maw1-wvrkm IS INITIAL AND ls_arn_reg_uom-meinh EQ fi_s_mara-meins.
          ls_arn_reg_uom-sales_unit = abap_true.
        ENDIF.

        EXIT.
      ENDLOOP.   " SALES_UNIT/VRKME
*      IF sy-subrc NE 0.
*        ls_arn_reg_uom-sales_unit         = ls_arn_reg_uom_in_db-sales_unit.
*      ENDIF.

      READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
        WITH TABLE KEY tabname = 'ZARN_REG_UOM' fldname = 'ISSUE_UNIT'.
      IF sy-subrc EQ 0.
**  ISSUE_UNIT = "X" if above ZARN_REG_UOM-MEINH=MAW1-WAUSM
        CLEAR: ls_maw1, ls_arn_reg_uom-issue_unit.
        READ TABLE gt_maw1 INTO ls_maw1 WITH TABLE KEY matnr = fi_s_mara-matnr.
        IF sy-subrc EQ 0 AND ls_maw1-wausm EQ ls_arn_reg_uom-meinh AND ls_maw1-wausm IS NOT INITIAL.
          ls_arn_reg_uom-issue_unit = abap_true.
**  If above ZARN_REG_UOM-MEINH=MARA-MEINS & if MAW1-WAUSM="" "" then ""X"""
        ELSEIF sy-subrc EQ 0 AND ls_maw1-wausm IS INITIAL AND ls_arn_reg_uom-meinh EQ fi_s_mara-meins.
          ls_arn_reg_uom-issue_unit = abap_true.
        ENDIF.
*      ELSE.
*        ls_arn_reg_uom-issue_unit         = ls_arn_reg_uom_in_db-issue_unit.
      ENDIF.  " 'ISSUE_UNIT'



***  System Date of Upload (No update)
*      ls_arn_reg_uom-ersda = sy-datlo.
*
***  System Date of Upload (No update)
*      ls_arn_reg_uom-erzet = sy-timlo.


*    INSERT ls_arn_reg_uom INTO TABLE fc_t_arn_reg_uom.
      INSERT ls_arn_reg_uom INTO TABLE gt_arn_reg_uom_all.


      CLEAR ls_reg_uom_tmp_ui.
      MOVE-CORRESPONDING ls_arn_reg_uom TO ls_reg_uom_tmp_ui.

      READ TABLE gt_uom_cat INTO ls_uom_cat WITH KEY uom = ls_arn_reg_uom-meinh.
      IF sy-subrc EQ 0.
        ls_reg_uom_tmp_ui-cat_seqno = ls_uom_cat-cat_seqno.
        ls_reg_uom_tmp_ui-seqno = ls_uom_cat-seqno.
      ENDIF.

      INSERT ls_reg_uom_tmp_ui INTO TABLE gt_arn_reg_uom_ui.


    ELSE.  " READ TABLE gt_arn_reg_uom_in_db INTO ls_arn_reg_uom_in_db


* Get Number of Base Units
      ls_arn_reg_uom-num_base_units = ls_arn_uom_variant-num_base_units.
* Factor of Bsse Units
      ls_arn_reg_uom-factor_of_base_units = ls_arn_uom_variant-factor_of_base_units.


      IF ls_arn_reg_uom-num_base_units IS INITIAL.
        ls_arn_reg_uom-num_base_units = 1.
      ENDIF.

      IF ls_arn_reg_uom-factor_of_base_units IS INITIAL.
        ls_arn_reg_uom-factor_of_base_units = 1.
      ENDIF.

      ls_arn_reg_uom-net_weight_uom       = ls_arn_uom_variant-net_weight_uom.         " Net Weight UOM
      ls_arn_reg_uom-net_weight_value     = ls_arn_uom_variant-net_weight_value.       " Net Weight Value
      ls_arn_reg_uom-gross_weight_uom     = ls_arn_uom_variant-gross_weight_uom.       " Gross Weight UOM
      ls_arn_reg_uom-gross_weight_value   = ls_arn_uom_variant-gross_weight_value.     " Gross Weight Value
      ls_arn_reg_uom-height_uom           = ls_arn_uom_variant-height_uom.             " Height UOM
      ls_arn_reg_uom-height_value         = ls_arn_uom_variant-height_value.           " Height Value
      ls_arn_reg_uom-width_uom            = ls_arn_uom_variant-width_uom.              " Width UOM
      ls_arn_reg_uom-width_value          = ls_arn_uom_variant-width_value.            " Width Value
      ls_arn_reg_uom-depth_uom            = ls_arn_uom_variant-depth_uom.              " Depth UOM
      ls_arn_reg_uom-depth_value          = ls_arn_uom_variant-depth_value.            " Depth Value

* INS Begin of Change INC5409316 JKH 17.11.2016
* Convert Height, width, depth into 'MM'
      zcl_arn_gui_load=>unit_conversion_simple(
            EXPORTING input    = ls_arn_reg_uom-height_value
                      unit_in  = ls_arn_reg_uom-height_uom
                      unit_out = 'MM'
            IMPORTING output   = ls_arn_reg_uom-height_value
                      ev_error = lv_uom_conv_err ).
      IF lv_uom_conv_err IS INITIAL.
        ls_arn_reg_uom-height_uom = 'MMT'.
      ENDIF.

      zcl_arn_gui_load=>unit_conversion_simple(
            EXPORTING input    = ls_arn_reg_uom-width_value
                      unit_in  = ls_arn_reg_uom-width_uom
                      unit_out = 'MM'
            IMPORTING output   = ls_arn_reg_uom-width_value
                      ev_error = lv_uom_conv_err ).
      IF lv_uom_conv_err IS INITIAL.
        ls_arn_reg_uom-width_uom  = 'MMT'.
      ENDIF.

      zcl_arn_gui_load=>unit_conversion_simple(
            EXPORTING input    = ls_arn_reg_uom-depth_value
                      unit_in  = ls_arn_reg_uom-depth_uom
                      unit_out = 'MM'
            IMPORTING output   = ls_arn_reg_uom-depth_value
                      ev_error = lv_uom_conv_err ).
      IF lv_uom_conv_err IS INITIAL.
        ls_arn_reg_uom-depth_uom  = 'MMT'.
      ENDIF.
* INS End of Change INC5409316 JKH 17.11.2016


*    FILE-SAP_UOMCODE where FILE-HYBRIS_UOMCODE= ZARN_REG_UOM-PIM_UOM_CODE
      CLEAR ls_uom.
      READ TABLE gt_uom INTO ls_uom
        WITH TABLE KEY zzfan = fi_s_mara-zzfan hyb_uom_code = ls_arn_reg_uom-pim_uom_code
        hyb_pir = ''.
      IF sy-subrc EQ 0.
        ls_arn_reg_uom-meinh = ls_uom-sap_uom_code.

**  FILE-SAP_UOMCODE where FILE-HYBRIS_UOMCODE= ZARN_REG_UOM-LOWER_UOM
        CLEAR: ls_uom, ls_arn_reg_uom-lower_meinh.
        READ TABLE gt_uom INTO ls_uom WITH TABLE KEY zzfan = fi_s_mara-zzfan hyb_uom_code = ls_arn_reg_uom-lower_uom
          hyb_pir = ''.
        IF sy-subrc EQ 0.
          ls_arn_reg_uom-lower_meinh = ls_uom-sap_uom_code.
        ENDIF.

        IF ls_arn_reg_uom-meinh IS NOT INITIAL AND ls_arn_reg_uom-lower_meinh IS NOT INITIAL.
          CLEAR ls_marm_idno .
          READ TABLE gt_marm_idno INTO ls_marm_idno
            WITH KEY meinh = ls_arn_reg_uom-meinh mesub = ls_arn_reg_uom-lower_meinh.
          IF sy-subrc NE 0.
            CLEAR ls_arn_reg_uom-meinh.
          ENDIF.
        ENDIF.

      ENDIF.

      IF ls_arn_reg_uom-meinh IS INITIAL.

        CLEAR: ls_arn_reg_uom_all, ls_arn_reg_uom-lower_meinh.
        READ TABLE gt_arn_reg_uom_all INTO ls_arn_reg_uom_all TRANSPORTING meinh
          WITH KEY idno = fi_v_idno pim_uom_code = ls_arn_reg_uom-lower_uom
          lower_uom = ls_arn_reg_uom-lower_child_uom.
        IF sy-subrc EQ 0.
          ls_arn_reg_uom-lower_meinh = ls_arn_reg_uom_all-meinh.
        ENDIF.

        " DEFAULTING LOGIC in case UOM missing in FILE
**********************************************************************
        lt_marm_idno_tmp[] = gt_marm_idno[].

        DELETE lt_marm_idno_tmp[] WHERE uom_category NE ls_arn_reg_uom-uom_category.

* Get the highest UOM at last so that If multiple UOM exist for the same conversion,
* then the highest is defaulted
        SORT lt_marm_idno_tmp[] BY seqno.

        IF ls_arn_reg_uom-uom_category NE 'RETAIL'.

* Check in MARM if UOM exist for the same conversion, get count
          CLEAR lv_count.
          CLEAR ls_marm_idno_tmp.
          FREE lt_mul_gtin[].

          LOOP AT lt_marm_idno_tmp INTO ls_marm_idno_tmp
           WHERE idno  = ls_arn_reg_uom-idno
             AND umrez = ls_arn_reg_uom-num_base_units
             AND umren = ls_arn_reg_uom-factor_of_base_units
             AND mesub = ls_arn_reg_uom-lower_meinh.

            INSERT ls_marm_idno_tmp INTO TABLE lt_mul_gtin.
            lv_count = lv_count + 1.

          ENDLOOP.

          IF lv_count GE 1.

            " check if the higest UOM's GTIN matches the national GTIN then set UOM else continue with the lower UOMs...
            SORT lt_mul_gtin BY seqno DESCENDING.
            CLEAR lv_gtin_found.

            LOOP AT lt_mul_gtin INTO ls_mul_gtin.
              LOOP AT gt_mean INTO ls_mean WHERE matnr = fi_s_mara-matnr AND meinh = ls_mul_gtin-meinh.
                CLEAR ls_arn_gtin_var.
                READ TABLE gt_arn_gtin_var INTO ls_arn_gtin_var
                  WITH KEY idno = fi_v_idno version = fi_v_version gtin_code = ls_mean-ean11.
                IF sy-subrc EQ 0.
                  lv_gtin_found = abap_true.
                  EXIT.
                ENDIF.
              ENDLOOP.
              IF lv_gtin_found EQ abap_true.
                EXIT.
              ENDIF.
            ENDLOOP.

            IF lv_gtin_found = abap_true.
              ls_arn_reg_uom-meinh = ls_mul_gtin-meinh.
            ELSE.
              ls_arn_reg_uom-meinh = ls_marm_idno_tmp-meinh.
            ENDIF.

          ELSE.

            ls_arn_reg_uom-relationship = 1.

* For MEINH, check if UOM already exist (either in DB or in new UOM),
* then get the next higher UOM
*        CLEAR: lt_reg_uom_tmp_ui[].
*          DATA ls_arn_reg_uom_temp LIKE LINE OF fc_t_arn_reg_uom.

            lt_reg_uom_ui_tmp[] = gt_arn_reg_uom_ui[].

            DELETE lt_reg_uom_ui_tmp[] WHERE uom_category NE ls_arn_reg_uom-uom_category.
            DELETE lt_reg_uom_ui_tmp[] WHERE meinh IS INITIAL.

* Get highest existing UOM
            SORT lt_reg_uom_ui_tmp[] BY seqno DESCENDING.
            CLEAR ls_reg_uom_ui_tmp.
            READ TABLE lt_reg_uom_ui_tmp[] INTO ls_reg_uom_ui_tmp INDEX 1.

* get highest UOM on the top
            SORT lt_marm_idno_tmp[] BY seqno DESCENDING.

* Get highest existing UOM from MARM
            CLEAR ls_marm_idno_tmp.
            READ TABLE lt_marm_idno_tmp[] INTO ls_marm_idno_tmp INDEX 1.

* If highest UOM is same in defaulted UOM and MARM then get the next higher UOM
* else, check the highest UOM in defaulted UOM and MARM and get the next higher UOM

            CLEAR lv_seqno.
            IF ls_reg_uom_ui_tmp-seqno = ls_marm_idno_tmp-seqno.
              lv_seqno = ls_reg_uom_ui_tmp-seqno + 1.
            ELSEIF ls_reg_uom_ui_tmp-seqno GT ls_marm_idno_tmp-seqno.
              lv_seqno = ls_reg_uom_ui_tmp-seqno + 1.
            ELSEIF ls_reg_uom_ui_tmp-seqno LT ls_marm_idno_tmp-seqno.
              lv_seqno = ls_marm_idno_tmp-seqno + 1.
            ENDIF.

            IF lv_seqno GT 0.
* Get next higher UOM to be defaulted
              CLEAR ls_uom_cat.
              READ TABLE gt_uom_cat[] INTO ls_uom_cat
              WITH KEY uom_category = ls_arn_reg_uom-uom_category
                       seqno = lv_seqno.
              IF sy-subrc = 0.
                ls_arn_reg_uom-meinh = ls_uom_cat-uom.
              ENDIF.
            ENDIF.

          ENDIF.  " READ TABLE lt_marm_idno INTO ls_marm_idno

          " RETAIL
        ELSE.

          IF ls_arn_uom_variant-product_uom EQ 'KGM'.
            lv_uom = 'KG'.
          ELSE.
            lv_uom = 'EA'.
          ENDIF.

* Check in MARM if UOM exist for the same conversion, get count
          CLEAR lv_count.
          CLEAR ls_marm_idno_tmp.
          LOOP AT lt_marm_idno_tmp INTO ls_marm_idno_tmp
           WHERE idno  = ls_arn_reg_uom-idno
             AND umrez = ls_arn_reg_uom-num_base_units
             AND umren = ls_arn_reg_uom-factor_of_base_units
             AND mesub = ls_arn_reg_uom-lower_meinh
             AND meinh = lv_uom.

            lv_count = lv_count + 1.
          ENDLOOP.

          IF lv_count GE 1.
            ls_arn_reg_uom-meinh = ls_marm_idno_tmp-meinh.
          ELSE.

            READ TABLE lt_marm_idno_tmp INTO ls_marm_idno_tmp
              WITH KEY idno  = ls_arn_reg_uom-idno meinh = lv_uom.
            IF sy-subrc NE 0.
              ls_arn_reg_uom-meinh = lv_uom.
            ENDIF.

          ENDIF.

        ENDIF. " ls_arn_reg_uom-uom_category NE 'RETAIL'.


      ENDIF.
**********************************************************************



      CLEAR: ls_arn_reg_uom_all, ls_arn_reg_uom-higher_level_units.
      READ TABLE gt_arn_reg_uom_all INTO ls_arn_reg_uom_all
        WITH KEY idno = fi_v_idno pim_uom_code = ls_arn_reg_uom-lower_uom
                 lower_uom = ls_arn_reg_uom-lower_child_uom.

      IF ls_arn_reg_uom_all-num_base_units IS INITIAL.
        ls_arn_reg_uom_all-num_base_units = 1.
      ENDIF.
      IF ls_arn_reg_uom_all-factor_of_base_units IS INITIAL.
        ls_arn_reg_uom_all-factor_of_base_units = 1.
      ENDIF.
      IF ls_arn_reg_uom-num_base_units IS INITIAL.
        ls_arn_reg_uom-num_base_units = 1.
      ENDIF.
      IF ls_arn_reg_uom-factor_of_base_units IS INITIAL.
        ls_arn_reg_uom-factor_of_base_units = 1.
      ENDIF.

      ls_arn_reg_uom-higher_level_units = ( ls_arn_reg_uom-num_base_units / ls_arn_reg_uom-factor_of_base_units ) /
                                          ( ls_arn_reg_uom_all-num_base_units / ls_arn_reg_uom_all-factor_of_base_units ) .

**  UNIT_BASE = "X" if above ZARN_REG_UOM-MEINH=MARA-MEINS
      CLEAR ls_arn_reg_uom-unit_base.
      IF ls_arn_reg_uom-meinh EQ fi_s_mara-meins.
        ls_arn_reg_uom-unit_base = abap_true.
      ENDIF.

      CLEAR ls_arn_reg_uom-unit_purord.
**  UNIT_PURORD = "X" if above ZARN_REG_UOM-MEINH=MARA-BSTME
**  OR If above ZARN_REG_UOM-MEINH=MARA-MEINS & if MARA-BSTME="" "" then ""X"""
      IF ( ls_arn_reg_uom-meinh EQ fi_s_mara-bstme AND fi_s_mara-bstme IS NOT INITIAL ) OR
        ( ls_arn_reg_uom-meinh EQ fi_s_mara-meins AND fi_s_mara-bstme IS INITIAL ).

        ls_arn_reg_uom-unit_purord = abap_true.
      ENDIF.

**  SALES_UNIT = ""X"" if above ZARN_REG_UOM-MEINH=MAW1-WVRKM
      CLEAR: ls_maw1, ls_arn_reg_uom-sales_unit.
      READ TABLE gt_maw1 INTO ls_maw1 WITH TABLE KEY matnr = fi_s_mara-matnr.
      IF sy-subrc EQ 0 AND ls_maw1-wvrkm EQ ls_arn_reg_uom-meinh AND ls_maw1-wvrkm IS NOT INITIAL.
        ls_arn_reg_uom-sales_unit = abap_true.
**  If above ZARN_REG_UOM-MEINH=MARA-MEINS & if MAW1-WVRKM="" "" then ""X"""
      ELSEIF sy-subrc EQ 0 AND ls_maw1-wvrkm IS INITIAL AND ls_arn_reg_uom-meinh EQ fi_s_mara-meins.
        ls_arn_reg_uom-sales_unit = abap_true.
      ENDIF.

**  ISSUE_UNIT = "X" if above ZARN_REG_UOM-MEINH=MAW1-WAUSM
      CLEAR: ls_maw1, ls_arn_reg_uom-issue_unit.
      READ TABLE gt_maw1 INTO ls_maw1 WITH TABLE KEY matnr = fi_s_mara-matnr.
      IF sy-subrc EQ 0 AND ls_maw1-wausm EQ ls_arn_reg_uom-meinh AND ls_maw1-wausm IS NOT INITIAL.
        ls_arn_reg_uom-issue_unit = abap_true.
**  If above ZARN_REG_UOM-MEINH=MARA-MEINS & if MAW1-WAUSM="" "" then ""X"""
      ELSEIF sy-subrc EQ 0 AND ls_maw1-wausm IS INITIAL AND ls_arn_reg_uom-meinh EQ fi_s_mara-meins.
        ls_arn_reg_uom-issue_unit = abap_true.
      ENDIF.


**  System Date of Upload (No update)
      ls_arn_reg_uom-ersda = sy-datlo.

**  System Date of Upload (No update)
      ls_arn_reg_uom-erzet = sy-timlo.

*    INSERT ls_arn_reg_uom INTO TABLE fc_t_arn_reg_uom.
      INSERT ls_arn_reg_uom INTO TABLE gt_arn_reg_uom_all.


      CLEAR ls_reg_uom_tmp_ui.
      MOVE-CORRESPONDING ls_arn_reg_uom TO ls_reg_uom_tmp_ui.

      READ TABLE gt_uom_cat INTO ls_uom_cat WITH KEY uom = ls_arn_reg_uom-meinh.
      IF sy-subrc EQ 0.
        ls_reg_uom_tmp_ui-cat_seqno = ls_uom_cat-cat_seqno.
        ls_reg_uom_tmp_ui-seqno = ls_uom_cat-seqno.
      ENDIF.

      INSERT ls_reg_uom_tmp_ui INTO TABLE gt_arn_reg_uom_ui.



    ENDIF.   " READ TABLE gt_arn_reg_uom_in_db INTO ls_arn_reg_uom_in_db

  ENDLOOP.  " LOOP AT gt_arn_uom_variant_ui INTO ls_arn_uom_variant_ui

*  APPEND LINES OF gt_arn_reg_uom_ui TO gt_arn_reg_uom_all_ui.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  FILL_REG_UOM_LAY_PAL_BAU
*&---------------------------------------------------------------------*
* Fill ZARN_REG_UOM for LAYER and PALLET:
* Read all the ZARN_PIR-HYBRIS_INTERNAL_CODE
* where IDNO=ZARN_REG_HDR_IDNO & VERSION = ZARN_REG_HDR-VERSION
*----------------------------------------------------------------------*
FORM fill_reg_uom_lay_pal_bau USING fi_s_mara        TYPE mara
                                    fi_v_version     TYPE zarn_version
                                    fi_v_idno        TYPE zarn_idno
                           CHANGING fc_t_arn_reg_uom TYPE ztarn_reg_uom.

  TYPES:
    BEGIN OF ty_added_pir,
      uom_code              TYPE zarn_uom_code,
      base_units_per_pallet TYPE zarn_base_units_per_pallet, " added
      qty_per_pallet_layer  TYPE zarn_qty_per_pallet_layer,
      qty_layers_per_pallet TYPE zarn_qty_layers_per_pallet,
      qty_units_per_pallet  TYPE zarn_qty_units_per_pallet,
    END OF ty_added_pir.

  DATA lt_added_pir               TYPE SORTED TABLE OF ty_added_pir
                                  WITH NON-UNIQUE KEY uom_code
                                  base_units_per_pallet qty_per_pallet_layer
                                  qty_layers_per_pallet qty_units_per_pallet.

  DATA ls_added_pir               TYPE ty_added_pir.
  DATA ls_arn_reg_uom             LIKE LINE OF fc_t_arn_reg_uom.
  DATA ls_arn_uom_variant         LIKE LINE OF gt_arn_uom_variant.
  DATA ls_arn_pir                 LIKE LINE OF gt_arn_pir.
  DATA ls_arn_uom_variant_child   LIKE LINE OF gt_arn_uom_variant.
  DATA ls_arn_gtin_var_child      LIKE LINE OF gt_arn_gtin_var.
  DATA ls_uom                     LIKE LINE OF gt_uom.
  DATA ls_maw1                    LIKE LINE OF gt_maw1.
  DATA ls_arn_reg_uom_temp        TYPE zarn_reg_uom.
  DATA ls_arn_reg_uom_all         LIKE LINE OF gt_arn_reg_uom_all.
  DATA ls_arn_reg_uom_tmp         LIKE LINE OF fc_t_arn_reg_uom.
  DATA: ls_pir_calc         TYPE zarn_pir,
        ls_uom_variant_calc TYPE zarn_uom_variant.

  DATA ls_marm_idno LIKE LINE OF gt_marm_idno.
  DATA:
    lt_marm_idno_tmp  TYPE ty_t_marm_idno,
    ls_marm_idno_tmp  TYPE ty_s_marm_idno,
*    lt_reg_uom_tmp_ui TYPE ztarn_reg_uom_ui,
    lt_reg_uom_ui_tmp TYPE ztarn_reg_uom_ui,
    ls_reg_uom_ui_tmp TYPE zsarn_reg_uom_ui,
    ls_reg_uom_tmp_ui TYPE zsarn_reg_uom_ui,
    ls_uom_cat        LIKE LINE OF gt_uom_cat,
    lv_seqno          TYPE zarn_seqno,
    lv_count          TYPE i,
    lv_uom_conv_err   TYPE flag.

  DATA ls_arn_reg_uom_in_db  LIKE LINE OF gt_arn_reg_uom_in_db.

*          lv_count              TYPE i,
*          lv_tabix              TYPE sy-tabix,
*          lv_max_relation       TYPE i,


  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
  WITH KEY tabname = 'ZARN_REG_UOM'.
  IF sy-subrc NE 0.
    READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH KEY tabname = 'ZARN_REG_BANNER'
             fldname = 'VRKME'.
    IF sy-subrc NE 0.

      LOOP AT gt_arn_reg_uom_in_db[] INTO ls_arn_reg_uom_in_db
        WHERE idno = fi_v_idno
          AND (    uom_category = 'LAYER'
                OR uom_category = 'PALLET' ).

        MOVE-CORRESPONDING ls_arn_reg_uom_in_db TO ls_arn_reg_uom.

        INSERT ls_arn_reg_uom INTO TABLE gt_arn_reg_uom_all.


        CLEAR ls_reg_uom_tmp_ui.
        MOVE-CORRESPONDING ls_arn_reg_uom TO ls_reg_uom_tmp_ui.

        CLEAR ls_uom_cat.
        READ TABLE gt_uom_cat INTO ls_uom_cat WITH KEY uom = ls_arn_reg_uom-meinh.
        IF sy-subrc EQ 0.
          ls_reg_uom_tmp_ui-cat_seqno = ls_uom_cat-cat_seqno.
          ls_reg_uom_tmp_ui-seqno = ls_uom_cat-seqno.
        ENDIF.

        APPEND ls_reg_uom_tmp_ui TO gt_arn_reg_uom_ui.

      ENDLOOP.

      EXIT.
    ENDIF.
  ENDIF.

  CHECK gt_arn_pir[] IS NOT INITIAL.

  FREE: fc_t_arn_reg_uom, lt_added_pir.

* Ignore National PIR where units per pallets are not maintained
* this means only records which have layer/pallet UOMs will be considered
*  DELETE gt_arn_pir[] WHERE base_units_per_pallet LE 0 AND                 "  - Added
*                            qty_per_pallet_layer  LE 0 AND
*                            qty_layers_per_pallet LE 0 AND
*                            qty_units_per_pallet  LE 0.

  LOOP AT gt_arn_pir INTO ls_arn_pir WHERE idno = fi_v_idno.


    IF ls_arn_pir-base_units_per_pallet   IS NOT INITIAL
      OR ls_arn_pir-qty_layers_per_pallet IS NOT INITIAL
      OR ls_arn_pir-qty_units_per_pallet  IS NOT INITIAL
      OR ls_arn_pir-qty_per_pallet_layer  IS NOT INITIAL. " added

      CLEAR ls_arn_reg_uom.
      " fill data for all relevant UOMs
      ls_arn_reg_uom-mandt = sy-mandt.
      ls_arn_reg_uom-idno = fi_v_idno.

* Logic for selecting PIR for creating Layer and Pallet in Regional UOM
* 1. Consider only those PIR records which has the 'Base/Pallet' or 'Qty./Layer' or 'Ly/Pallet' or 'Qty./Pallet'  <> 0
* 2. Loop for every HYBRIS UOM which is having  unique values in the above mentioned fields
*        and where 'Base/Pallet' / 'Qty./Pallet'  = 'No: of Base Unit' / 'Factor of Base Unit'  OR
*                  'Base/Pallet' / 'Qty./Layer'  * 'Ly/Pallet' = 'No: of Base Unit'  / 'Factor of Base Unit'

      READ TABLE lt_added_pir TRANSPORTING NO FIELDS
        WITH TABLE KEY uom_code              = ls_arn_pir-uom_code
                       base_units_per_pallet = ls_arn_pir-base_units_per_pallet
                       qty_per_pallet_layer  = ls_arn_pir-qty_per_pallet_layer
                       qty_layers_per_pallet = ls_arn_pir-qty_layers_per_pallet
                       qty_units_per_pallet  = ls_arn_pir-qty_units_per_pallet.
      IF sy-subrc NE 0.


        CLEAR ls_pir_calc.
        ls_pir_calc = ls_arn_pir.

        IF ls_pir_calc-qty_units_per_pallet IS INITIAL.
          ls_pir_calc-qty_units_per_pallet = 1.
        ENDIF.

        IF ls_pir_calc-qty_per_pallet_layer IS INITIAL.
          ls_pir_calc-qty_per_pallet_layer = 1.
        ENDIF.

        IF ls_pir_calc-base_units_per_pallet IS INITIAL.
          ls_pir_calc-base_units_per_pallet = 1.
        ENDIF.


        CLEAR ls_uom_variant_calc.
        CLEAR ls_arn_uom_variant.
        READ TABLE gt_arn_uom_variant INTO ls_arn_uom_variant
          WITH KEY idno = fi_v_idno version = fi_v_version uom_code = ls_arn_pir-uom_code.
        IF sy-subrc = 0.
          ls_uom_variant_calc = ls_arn_uom_variant.
        ENDIF.

        IF ls_uom_variant_calc-num_base_units IS INITIAL.
          ls_uom_variant_calc-num_base_units = 1.
        ENDIF.

        IF ls_uom_variant_calc-factor_of_base_units IS INITIAL.
          ls_uom_variant_calc-factor_of_base_units = 1.
        ENDIF.


        IF ( ( ls_pir_calc-base_units_per_pallet / ls_pir_calc-qty_units_per_pallet ) =
                               ( ls_uom_variant_calc-num_base_units / ls_uom_variant_calc-factor_of_base_units ) )
                                            OR
           ( ( ( ls_pir_calc-base_units_per_pallet / ls_pir_calc-qty_per_pallet_layer ) * ls_pir_calc-qty_layers_per_pallet ) =
                               ( ls_uom_variant_calc-num_base_units / ls_uom_variant_calc-factor_of_base_units ) ).

          CLEAR: ls_pir_calc, ls_uom_variant_calc.


          " record not found, then add to local table
          CLEAR ls_added_pir.
          ls_added_pir-uom_code              = ls_arn_pir-uom_code.
          ls_added_pir-base_units_per_pallet = ls_arn_pir-base_units_per_pallet.
          ls_added_pir-qty_per_pallet_layer  = ls_arn_pir-qty_per_pallet_layer.
          ls_added_pir-qty_layers_per_pallet = ls_arn_pir-qty_layers_per_pallet.
          ls_added_pir-qty_units_per_pallet  = ls_arn_pir-qty_units_per_pallet.
          INSERT ls_added_pir INTO TABLE lt_added_pir.

        ELSE.
          CONTINUE.
        ENDIF.
      ELSE.
        CONTINUE.
      ENDIF.


      ls_arn_reg_uom-pim_uom_code = ls_arn_pir-uom_code.

**    For Layer: ZARN_PIR-HYBRIS_INTERNAL_CODE selected above
**    For Pallet: ZARN_PIR-HYBRIS_INTERNAL_CODE selected above"
      ls_arn_reg_uom-hybris_internal_code = ls_arn_pir-hybris_internal_code.

      " check if the entry is in upload UOM file
*      READ TABLE gt_uom TRANSPORTING NO FIELDS
*        WITH TABLE KEY zzfan = fi_s_mara-zzfan hyb_uom_code = ls_arn_reg_uom-pim_uom_code
*                       hyb_pir = ls_arn_reg_uom-hybris_internal_code.
*      IF sy-subrc NE 0.
**        CONTINUE.
*      ENDIF.

      DO 2 TIMES.

        IF sy-index EQ 1.
          ls_arn_reg_uom-uom_category = gc_cat_layer.
        ELSE.
          ls_arn_reg_uom-uom_category = gc_cat_pallet.
        ENDIF.

        CLEAR: ls_arn_gtin_var_child, ls_arn_uom_variant, ls_arn_reg_uom-lower_uom.

        " ZARN_GTIN_VAR-UOM_CODE where ZARN_GTIN_VAR-GTIN_CODE = ZARN_UOM_VARIANT-CHILD_GTIN
        READ TABLE gt_arn_uom_variant INTO ls_arn_uom_variant
          WITH KEY idno = fi_v_idno version = fi_v_version uom_code = ls_arn_pir-uom_code.
        IF sy-subrc EQ 0.
          ls_arn_reg_uom-lower_uom = ls_arn_pir-uom_code.
        ENDIF.

        " ZARN_GTIN_VAR-UOM_CODE where ZARN_GTIN_VAR-GTIN_CODE = ZARN_UOM_VARIANT-CHILD_GTIN
        " & ZARN_UOM_VARIANT-UOM_CODE= ZARN_REG_UOM-LOWER_UOM
        CLEAR: ls_arn_uom_variant_child, ls_arn_reg_uom-lower_child_uom.
        READ TABLE gt_arn_uom_variant INTO ls_arn_uom_variant_child
          WITH KEY idno = fi_v_idno version = fi_v_version uom_code = ls_arn_reg_uom-lower_uom.
        IF sy-subrc EQ 0.
          CLEAR ls_arn_gtin_var_child.
          READ TABLE gt_arn_gtin_var INTO ls_arn_gtin_var_child
             WITH KEY idno = fi_v_idno version = fi_v_version gtin_code = ls_arn_uom_variant_child-child_gtin.
          IF sy-subrc EQ 0.
            ls_arn_reg_uom-lower_child_uom = ls_arn_gtin_var_child-uom_code.
          ENDIF.
        ENDIF.


** Factor of Bsse Units
*        ls_arn_reg_uom-factor_of_base_units = 1.


* If record exist in AReNa then build data using migrate version,
* else using BAU version
        CLEAR ls_arn_reg_uom_in_db.
        READ TABLE gt_arn_reg_uom_in_db INTO ls_arn_reg_uom_in_db
        WITH TABLE KEY idno                 = ls_arn_reg_uom-idno
                       uom_category         = ls_arn_reg_uom-uom_category
                       pim_uom_code         = ls_arn_reg_uom-pim_uom_code
                       hybris_internal_code = ls_arn_reg_uom-hybris_internal_code
                       lower_uom            = ls_arn_reg_uom-lower_uom
                       lower_child_uom      = ls_arn_reg_uom-lower_child_uom.
        IF sy-subrc EQ 0.

*          MOVE-CORRESPONDING ls_arn_reg_uom_in_db TO ls_arn_reg_uom.

*          ls_arn_reg_uom-net_weight_uom     = ls_arn_reg_uom_in_db-net_weight_uom.
*          ls_arn_reg_uom-net_weight_value   = ls_arn_reg_uom_in_db-net_weight_value.
          ls_arn_reg_uom-gross_weight_uom   = ls_arn_reg_uom_in_db-gross_weight_uom.
          ls_arn_reg_uom-gross_weight_value = ls_arn_reg_uom_in_db-gross_weight_value.
          ls_arn_reg_uom-height_uom         = ls_arn_reg_uom_in_db-height_uom.
          ls_arn_reg_uom-height_value       = ls_arn_reg_uom_in_db-height_value.
*          ls_arn_reg_uom-width_uom          = ls_arn_reg_uom_in_db-width_uom.
*          ls_arn_reg_uom-width_value        = ls_arn_reg_uom_in_db-width_value.
*          ls_arn_reg_uom-depth_uom          = ls_arn_reg_uom_in_db-depth_uom.
*          ls_arn_reg_uom-depth_value        = ls_arn_reg_uom_in_db-depth_value.
          ls_arn_reg_uom-unit_base          = ls_arn_reg_uom_in_db-unit_base.
          ls_arn_reg_uom-unit_purord        = ls_arn_reg_uom_in_db-unit_purord.
          ls_arn_reg_uom-sales_unit         = ls_arn_reg_uom_in_db-sales_unit.
          ls_arn_reg_uom-issue_unit         = ls_arn_reg_uom_in_db-issue_unit.


          "LAYER
          IF ls_arn_reg_uom-uom_category = gc_cat_layer.

            IF ls_arn_pir-qty_layers_per_pallet IS NOT INITIAL AND
               ls_arn_pir-base_units_per_pallet IS NOT INITIAL.

              ls_arn_reg_uom-num_base_units = ls_arn_pir-base_units_per_pallet / ls_arn_pir-qty_layers_per_pallet.
              ls_arn_reg_uom-factor_of_base_units = ls_arn_pir-factor_of_buom_per_pallet.

            ELSE.
              IF ls_arn_pir-qty_per_pallet_layer IS NOT INITIAL.

                ls_arn_reg_uom-num_base_units =  ls_arn_pir-qty_per_pallet_layer * ls_arn_uom_variant-num_base_units.
                ls_arn_reg_uom-factor_of_base_units = ls_arn_uom_variant-factor_of_base_units.

              ELSE.

                ls_arn_reg_uom-num_base_units = 1.

              ENDIF.
            ENDIF.


            LOOP AT gt_arn_sync TRANSPORTING NO FIELDS
            WHERE tabname = 'ZARN_REG_UOM'
              AND ( fldname = 'GROSS_WEIGHT_UOM' OR
                    fldname = 'GROSS_WEIGHT_VALUE' OR
                    fldname = 'HEIGHT_UOM' OR
                    fldname = 'HEIGHT_VALUE' ).

              ls_arn_reg_uom-gross_weight_value = 0.     " Gross Weight Value
              ls_arn_reg_uom-height_value       = 0.     " Gross Height Value

              CLEAR ls_arn_reg_uom_tmp.
              READ TABLE gt_arn_reg_uom_all INTO ls_arn_reg_uom_tmp
              WITH KEY idno                 = ls_arn_reg_uom-idno
                       pim_uom_code         = ls_arn_reg_uom-pim_uom_code
                       hybris_internal_code = space
                       lower_uom            = ls_arn_reg_uom-lower_child_uom.
              IF sy-subrc = 0.
                ls_arn_reg_uom-gross_weight_uom = ls_arn_reg_uom_tmp-gross_weight_uom.       " Gross Weight UOM
                ls_arn_reg_uom-height_uom       = ls_arn_reg_uom_tmp-height_uom.             " Height UOM
              ENDIF.

* INS Begin of Change INC5409316 JKH 17.11.2016
* Convert Height into 'MM'
              zcl_arn_gui_load=>unit_conversion_simple(
                    EXPORTING input    = ls_arn_reg_uom-height_value
                              unit_in  = ls_arn_reg_uom-height_uom
                              unit_out = 'MM'
                    IMPORTING output   = ls_arn_reg_uom-height_value
                              ev_error = lv_uom_conv_err ).
              IF lv_uom_conv_err IS INITIAL.
                ls_arn_reg_uom-height_uom = 'MMT'.
              ENDIF.
* INS End of Change INC5409316 JKH 17.11.2016

              EXIT.
            ENDLOOP.
*            IF sy-subrc NE 0.
*              ls_arn_reg_uom-gross_weight_uom   = ls_arn_reg_uom_in_db-gross_weight_uom.
*              ls_arn_reg_uom-gross_weight_value = ls_arn_reg_uom_in_db-gross_weight_value.
*              ls_arn_reg_uom-height_uom         = ls_arn_reg_uom_in_db-height_uom.
*              ls_arn_reg_uom-height_value       = ls_arn_reg_uom_in_db-height_value.
*            ENDIF.




            " PALLET
          ELSEIF ls_arn_reg_uom-uom_category = gc_cat_pallet.

            IF ls_arn_pir-base_units_per_pallet IS NOT INITIAL.

              ls_arn_reg_uom-num_base_units = ls_arn_pir-base_units_per_pallet.
              ls_arn_reg_uom-factor_of_base_units = ls_arn_pir-factor_of_buom_per_pallet.

            ELSE.
              IF ls_arn_pir-qty_units_per_pallet IS NOT INITIAL.

                ls_arn_reg_uom-num_base_units = ls_arn_pir-qty_units_per_pallet * ls_arn_uom_variant-num_base_units.
                ls_arn_reg_uom-factor_of_base_units = ls_arn_uom_variant-factor_of_base_units.

              ELSE.
                IF ls_arn_pir-qty_layers_per_pallet IS NOT INITIAL.

                  CLEAR ls_arn_reg_uom_tmp.
                  READ TABLE gt_arn_reg_uom_all INTO ls_arn_reg_uom_tmp
                  WITH KEY idno  = ls_arn_reg_uom-idno
                           meinh = ls_arn_reg_uom-lower_meinh.
                  IF sy-subrc = 0.

                    ls_arn_reg_uom-num_base_units = ls_arn_pir-qty_layers_per_pallet * ls_arn_reg_uom_tmp-num_base_units.
                    ls_arn_reg_uom-factor_of_base_units = ls_arn_reg_uom_tmp-factor_of_base_units.

                  ELSE.

                    ls_arn_reg_uom-num_base_units = 1.

                  ENDIF.
                ELSE.

                  ls_arn_reg_uom-num_base_units = 1.

                ENDIF.
              ENDIF.
            ENDIF.


            LOOP AT gt_arn_sync TRANSPORTING NO FIELDS
            WHERE tabname = 'ZARN_REG_UOM'
              AND ( fldname = 'GROSS_WEIGHT_UOM' OR
                    fldname = 'GROSS_WEIGHT_VALUE' OR
                    fldname = 'HEIGHT_UOM' OR
                    fldname = 'HEIGHT_VALUE' ).

              ls_arn_reg_uom-gross_weight_value = ls_arn_pir-gross_weight.     " Gross Weight Value
              ls_arn_reg_uom-height_value       = ls_arn_pir-gross_height.     " Gross Height Value

              CLEAR ls_arn_reg_uom_tmp.
              READ TABLE gt_arn_reg_uom_all INTO ls_arn_reg_uom_tmp
              WITH KEY idno                 = ls_arn_reg_uom-idno
                       pim_uom_code         = ls_arn_reg_uom-pim_uom_code
                       hybris_internal_code = space
                       lower_uom            = ls_arn_reg_uom-lower_child_uom.
              IF sy-subrc = 0.
                ls_arn_reg_uom-gross_weight_uom = ls_arn_reg_uom_tmp-gross_weight_uom.       " Gross Weight UOM
                ls_arn_reg_uom-height_uom       = ls_arn_reg_uom_tmp-height_uom.             " Height UOM
              ENDIF.

* INS Begin of Change INC5409316 JKH 17.11.2016
* Convert Height into 'MM'
              zcl_arn_gui_load=>unit_conversion_simple(
                    EXPORTING input    = ls_arn_reg_uom-height_value
                              unit_in  = ls_arn_reg_uom-height_uom
                              unit_out = 'MM'
                    IMPORTING output   = ls_arn_reg_uom-height_value
                              ev_error = lv_uom_conv_err ).
              IF lv_uom_conv_err IS INITIAL.
                ls_arn_reg_uom-height_uom = 'MMT'.
              ENDIF.
* INS End of Change INC5409316 JKH 17.11.2016

              EXIT.
            ENDLOOP.
*            IF sy-subrc NE 0.
*              ls_arn_reg_uom-gross_weight_uom   = ls_arn_reg_uom_in_db-gross_weight_uom.
*              ls_arn_reg_uom-gross_weight_value = ls_arn_reg_uom_in_db-gross_weight_value.
*              ls_arn_reg_uom-height_uom         = ls_arn_reg_uom_in_db-height_uom.
*              ls_arn_reg_uom-height_value       = ls_arn_reg_uom_in_db-height_value.
*            ENDIF.




          ENDIF.      " ls_arn_reg_uom-uom_category = LAYER/PALLET


          IF ls_arn_reg_uom-num_base_units IS INITIAL.
            ls_arn_reg_uom-num_base_units = 1.
          ENDIF.

          IF ls_arn_reg_uom-factor_of_base_units IS INITIAL.
            ls_arn_reg_uom-factor_of_base_units = 1.
          ENDIF.


**  For LAYER:FILE-SAP_UOMCODE where FILE-HYBRIS_UOMCODE= ZARN_REG_UOM-PIM_UOM_CODE &
**  FILE-PIM_PIRCODE = ZARN_REG_UOM-HYBRIS_INTERNAL_CODE &
**  belong to TVARVC-NAME= 'GC_LAYER_UNOMS'
          CLEAR: ls_uom, ls_arn_reg_uom-meinh.
          LOOP AT gt_uom INTO ls_uom
            WHERE zzfan = fi_s_mara-zzfan AND hyb_uom_code = ls_arn_reg_uom-pim_uom_code
            AND hyb_pir = ls_arn_reg_uom-hybris_internal_code.

            " Layer record
            IF ls_arn_reg_uom-uom_category EQ gc_cat_layer.
              IF ls_uom-sap_uom_code IN gtr_layer_uoms[].
                ls_arn_reg_uom-meinh = ls_uom-sap_uom_code.
                EXIT.
              ENDIF.
            ELSE.
              " Pallet record
              IF ls_uom-sap_uom_code IN gtr_pallets_uoms[].
                ls_arn_reg_uom-meinh = ls_uom-sap_uom_code.
                EXIT.
              ENDIF.
            ENDIF.
          ENDLOOP.


          "For Layer: FILE-SAP_UOMCODE where FILE-HYBRIS_UOMCODE= ZARN_REG_UOM-LOWER_UOM
          CLEAR ls_arn_reg_uom-lower_meinh.
          " Layer record
          IF ls_arn_reg_uom-uom_category EQ gc_cat_layer.
            CLEAR ls_arn_reg_uom_all.
            READ TABLE gt_arn_reg_uom_all INTO ls_arn_reg_uom_all TRANSPORTING meinh
              WITH KEY idno = fi_v_idno pim_uom_code = ls_arn_reg_uom-lower_uom.
            IF sy-subrc EQ 0.
              ls_arn_reg_uom-lower_meinh = ls_arn_reg_uom_all-meinh.
            ENDIF.
            "For Pallet: MARM-MEINH for Layer from above step"
          ELSE.
            CLEAR ls_arn_reg_uom_temp.
            READ TABLE gt_arn_reg_uom_all INTO ls_arn_reg_uom_temp
              WITH KEY pim_uom_code = ls_arn_reg_uom-pim_uom_code uom_category = gc_cat_layer.
            IF sy-subrc EQ 0.
              ls_arn_reg_uom-lower_meinh = ls_arn_reg_uom_temp-meinh.
            ENDIF.
          ENDIF.


          IF ls_arn_reg_uom-meinh IS NOT INITIAL AND ls_arn_reg_uom-lower_meinh IS NOT INITIAL.
            CLEAR ls_marm_idno .
            READ TABLE gt_marm_idno INTO ls_marm_idno
              WITH KEY meinh = ls_arn_reg_uom-meinh mesub = ls_arn_reg_uom-lower_meinh.
            IF sy-subrc NE 0.
              CLEAR ls_arn_reg_uom-meinh.
            ENDIF.
          ENDIF.


**********************************************************************
          " DEFAULTING LOGIC in case UOM missing in FILE
          IF ls_arn_reg_uom-meinh IS INITIAL.

            lt_marm_idno_tmp[] = gt_marm_idno[].

            DELETE lt_marm_idno_tmp[] WHERE uom_category NE ls_arn_reg_uom-uom_category.

* Get the highest UOM at last so that If multiple UOM exist for the same conversion,
* then the highest is defaulted
            SORT lt_marm_idno_tmp[] BY seqno.

* Check in MARM if UOM exist for the same conversion, get count
            CLEAR lv_count.
            CLEAR ls_marm_idno_tmp.
            LOOP AT lt_marm_idno_tmp INTO ls_marm_idno_tmp
             WHERE idno  = ls_arn_reg_uom-idno
               AND umrez = ls_arn_reg_uom-num_base_units
               AND umren = ls_arn_reg_uom-factor_of_base_units
               AND mesub = ls_arn_reg_uom-lower_meinh.
              lv_count = lv_count + 1.
            ENDLOOP.

            IF lv_count GE 1.

              ls_arn_reg_uom-meinh = ls_marm_idno_tmp-meinh.

            ELSE.

              ls_arn_reg_uom-relationship = 1.

* For MEINH, check if UOM already exist (either in DB or in new UOM),
* then get the next higher UOM
*            CLEAR: lt_reg_uom_tmp_ui[].
*          DATA ls_arn_reg_uom_temp LIKE LINE OF fc_t_arn_reg_uom.

              lt_reg_uom_ui_tmp[] = gt_arn_reg_uom_ui[].

              DELETE lt_reg_uom_ui_tmp[] WHERE uom_category NE ls_arn_reg_uom-uom_category.
              DELETE lt_reg_uom_ui_tmp[] WHERE meinh IS INITIAL.

* Get highest existing UOM
              SORT lt_reg_uom_ui_tmp[] BY seqno DESCENDING.
              CLEAR ls_reg_uom_ui_tmp.
              READ TABLE lt_reg_uom_ui_tmp[] INTO ls_reg_uom_ui_tmp INDEX 1.

* get highest UOM on the top
              SORT lt_marm_idno_tmp[] BY seqno DESCENDING.

* Get highest existing UOM from MARM
              CLEAR ls_marm_idno_tmp.
              READ TABLE lt_marm_idno_tmp[] INTO ls_marm_idno_tmp INDEX 1.

* If highest UOM is same in defaulted UOM and MARM then get the next higher UOM
* else, check the highest UOM in defaulted UOM and MARM and get the next higher UOM

              CLEAR lv_seqno.
              IF ls_reg_uom_ui_tmp-seqno = ls_marm_idno_tmp-seqno.
                lv_seqno = ls_reg_uom_ui_tmp-seqno + 1.
              ELSEIF ls_reg_uom_ui_tmp-seqno GT ls_marm_idno_tmp-seqno.
                lv_seqno = ls_reg_uom_ui_tmp-seqno + 1.
              ELSEIF ls_reg_uom_ui_tmp-seqno LT ls_marm_idno_tmp-seqno.
                lv_seqno = ls_marm_idno_tmp-seqno + 1.
              ENDIF.

              IF lv_seqno GT 0.
* Get next higher UOM to be defaulted
                CLEAR ls_uom_cat.
                READ TABLE gt_uom_cat[] INTO ls_uom_cat
                WITH KEY uom_category = ls_arn_reg_uom-uom_category
                         seqno = lv_seqno.
                IF sy-subrc = 0.
                  ls_arn_reg_uom-meinh = ls_uom_cat-uom.
                ENDIF.
              ENDIF.


            ENDIF.  " READ TABLE lt_marm_idno INTO ls_marm_idno

          ENDIF.
**********************************************************************

          READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_UOM' fldname = 'UNIT_BASE'.
          IF sy-subrc EQ 0.
*  UNIT_BASE = "X" if above ZARN_REG_UOM-MEINH=MARA-MEINS
            CLEAR ls_arn_reg_uom-unit_base.
            IF ls_arn_reg_uom-meinh EQ fi_s_mara-meins.
              ls_arn_reg_uom-unit_base = abap_true.
            ENDIF.
*          ELSE.
*            ls_arn_reg_uom-unit_base          = ls_arn_reg_uom_in_db-unit_base.
          ENDIF.  " 'UNIT_BASE'


          READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_UOM' fldname = 'UNIT_PURORD'.
          IF sy-subrc EQ 0.
**  UNIT_PURORD = "X" if above ZARN_REG_UOM-MEINH=MARA-BSTME
**  OR If above ZARN_REG_UOM-MEINH=MARA-MEINS & if MARA-BSTME="" "" then ""X"""
            CLEAR ls_arn_reg_uom-unit_purord.
            IF ( ls_arn_reg_uom-meinh EQ fi_s_mara-bstme AND fi_s_mara-bstme IS NOT INITIAL ) OR
              ( ls_arn_reg_uom-meinh EQ fi_s_mara-meins AND fi_s_mara-bstme IS INITIAL ).

              ls_arn_reg_uom-unit_purord = abap_true.
            ENDIF.
*          ELSE.
*            ls_arn_reg_uom-unit_purord        = ls_arn_reg_uom_in_db-unit_purord.
          ENDIF.  " 'UNIT_PURORD'


          LOOP AT gt_arn_sync TRANSPORTING NO FIELDS
          WHERE ( tabname = 'ZARN_REG_UOM' AND fldname = 'SALES_UNIT' )
             OR ( tabname = 'ZARN_REG_BANNER' AND fldname = 'VRKME' ).

**  SALES_UNIT = ""X"" if above ZARN_REG_UOM-MEINH=MAW1-WVRKM
            CLEAR: ls_maw1, ls_arn_reg_uom-sales_unit.
            READ TABLE gt_maw1 INTO ls_maw1 WITH TABLE KEY matnr = fi_s_mara-matnr.
            IF sy-subrc EQ 0 AND ls_maw1-wvrkm EQ ls_arn_reg_uom-meinh AND ls_maw1-wvrkm IS NOT INITIAL.
              ls_arn_reg_uom-sales_unit = abap_true.
**  If above ZARN_REG_UOM-MEINH=MARA-MEINS & if MAW1-WVRKM="" "" then ""X"""
            ELSEIF sy-subrc EQ 0 AND ls_maw1-wvrkm IS INITIAL AND ls_arn_reg_uom-meinh EQ fi_s_mara-meins.
              ls_arn_reg_uom-sales_unit = abap_true.
            ENDIF.

            EXIT.
          ENDLOOP.   " SALES_UNIT/VRKME
*          IF sy-subrc NE 0.
*            ls_arn_reg_uom-sales_unit         = ls_arn_reg_uom_in_db-sales_unit.
*          ENDIF.

          READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_UOM' fldname = 'ISSUE_UNIT'.
          IF sy-subrc EQ 0.
**  ISSUE_UNIT = "X" if above ZARN_REG_UOM-MEINH=MAW1-WAUSM
            CLEAR: ls_maw1, ls_arn_reg_uom-issue_unit.
            READ TABLE gt_maw1 INTO ls_maw1 WITH TABLE KEY matnr = fi_s_mara-matnr.
            IF sy-subrc EQ 0 AND ls_maw1-wausm EQ ls_arn_reg_uom-meinh AND ls_maw1-wausm IS NOT INITIAL.
              ls_arn_reg_uom-issue_unit = abap_true.
**  If above ZARN_REG_UOM-MEINH=MARA-MEINS & if MAW1-WAUSM="" "" then ""X"""
            ELSEIF sy-subrc EQ 0 AND ls_maw1-wausm IS INITIAL AND ls_arn_reg_uom-meinh EQ fi_s_mara-meins.
              ls_arn_reg_uom-issue_unit = abap_true.
            ENDIF.
*          ELSE.
*            ls_arn_reg_uom-issue_unit         = ls_arn_reg_uom_in_db-issue_unit.
          ENDIF.  " 'ISSUE_UNIT'



          " LAYER
          IF ls_arn_reg_uom-uom_category = gc_cat_layer.
            CLEAR ls_arn_reg_uom_all.
            READ TABLE gt_arn_reg_uom_all INTO ls_arn_reg_uom_all
              WITH KEY idno = fi_v_idno pim_uom_code = ls_arn_reg_uom-lower_uom
                       lower_uom = ls_arn_reg_uom-lower_child_uom.

            IF ls_arn_reg_uom_all-num_base_units IS INITIAL.
              ls_arn_reg_uom_all-num_base_units = 1.
            ENDIF.
            IF ls_arn_reg_uom_all-factor_of_base_units IS INITIAL.
              ls_arn_reg_uom_all-factor_of_base_units = 1.
            ENDIF.
            IF ls_arn_reg_uom-num_base_units IS INITIAL.
              ls_arn_reg_uom-num_base_units = 1.
            ENDIF.
            IF ls_arn_reg_uom-factor_of_base_units IS INITIAL.
              ls_arn_reg_uom-factor_of_base_units = 1.
            ENDIF.

            ls_arn_reg_uom-higher_level_units = ( ls_arn_reg_uom-num_base_units / ls_arn_reg_uom-factor_of_base_units ) /
                                                ( ls_arn_reg_uom_all-num_base_units / ls_arn_reg_uom_all-factor_of_base_units ) .




            " PALLET
          ELSEIF ls_arn_reg_uom-uom_category = gc_cat_pallet.
            CLEAR ls_arn_reg_uom_all.
            READ TABLE gt_arn_reg_uom_all INTO ls_arn_reg_uom_all
              WITH KEY idno = fi_v_idno uom_category = gc_cat_layer
                       pim_uom_code = ls_arn_reg_uom-pim_uom_code
                       hybris_internal_code = ls_arn_reg_uom-hybris_internal_code
                       lower_uom = ls_arn_reg_uom-lower_uom
                       lower_child_uom = ls_arn_reg_uom-lower_child_uom.


            IF ls_arn_reg_uom_all-num_base_units IS INITIAL.
              ls_arn_reg_uom_all-num_base_units = 1.
            ENDIF.
            IF ls_arn_reg_uom_all-factor_of_base_units IS INITIAL.
              ls_arn_reg_uom_all-factor_of_base_units = 1.
            ENDIF.
            IF ls_arn_reg_uom-num_base_units IS INITIAL.
              ls_arn_reg_uom-num_base_units = 1.
            ENDIF.
            IF ls_arn_reg_uom-factor_of_base_units IS INITIAL.
              ls_arn_reg_uom-factor_of_base_units = 1.
            ENDIF.

            ls_arn_reg_uom-higher_level_units = ( ls_arn_reg_uom-num_base_units / ls_arn_reg_uom-factor_of_base_units ) /
                                                ( ls_arn_reg_uom_all-num_base_units / ls_arn_reg_uom_all-factor_of_base_units ) .


          ENDIF.



***  System Date of Upload (No update)
*            ls_arn_reg_uom-ersda = sy-datlo.
*
***  System Date of Upload (No update)
*            ls_arn_reg_uom-erzet = sy-timlo.



*        INSERT ls_arn_reg_uom INTO TABLE fc_t_arn_reg_uom.
          INSERT ls_arn_reg_uom INTO TABLE gt_arn_reg_uom_all.

          CLEAR ls_reg_uom_tmp_ui.
          MOVE-CORRESPONDING ls_arn_reg_uom TO ls_reg_uom_tmp_ui.


          READ TABLE gt_uom_cat INTO ls_uom_cat WITH KEY uom = ls_arn_reg_uom-meinh.
          IF sy-subrc EQ 0.
            ls_reg_uom_tmp_ui-cat_seqno = ls_uom_cat-cat_seqno.
            ls_reg_uom_tmp_ui-seqno = ls_uom_cat-seqno.
          ENDIF.

          APPEND ls_reg_uom_tmp_ui TO gt_arn_reg_uom_ui.

        ELSE.   " READ TABLE gt_arn_reg_uom_in_db INTO ls_arn_reg_uom_in_db


          "LAYER
          IF ls_arn_reg_uom-uom_category = gc_cat_layer.

            IF ls_arn_pir-qty_layers_per_pallet IS NOT INITIAL AND
               ls_arn_pir-base_units_per_pallet IS NOT INITIAL.

              ls_arn_reg_uom-num_base_units = ls_arn_pir-base_units_per_pallet / ls_arn_pir-qty_layers_per_pallet.
              ls_arn_reg_uom-factor_of_base_units = ls_arn_pir-factor_of_buom_per_pallet.

            ELSE.
              IF ls_arn_pir-qty_per_pallet_layer IS NOT INITIAL.
                ls_arn_reg_uom-num_base_units =  ls_arn_pir-qty_per_pallet_layer * ls_arn_uom_variant-num_base_units.
                ls_arn_reg_uom-factor_of_base_units = ls_arn_uom_variant-factor_of_base_units.
              ELSE.
                ls_arn_reg_uom-num_base_units = 1.
              ENDIF.
            ENDIF.

            ls_arn_reg_uom-gross_weight_value = 0.     " Gross Weight Value
            ls_arn_reg_uom-height_value       = 0.     " Gross Height Value

            CLEAR ls_arn_reg_uom_tmp.
            READ TABLE gt_arn_reg_uom_all INTO ls_arn_reg_uom_tmp
            WITH KEY idno                 = ls_arn_reg_uom-idno
                     pim_uom_code         = ls_arn_reg_uom-pim_uom_code
                     hybris_internal_code = space
                     lower_uom            = ls_arn_reg_uom-lower_child_uom.
            IF sy-subrc = 0.
              ls_arn_reg_uom-gross_weight_uom = ls_arn_reg_uom_tmp-gross_weight_uom.       " Gross Weight UOM
              ls_arn_reg_uom-height_uom       = ls_arn_reg_uom_tmp-height_uom.             " Height UOM
            ENDIF.


            " PALLET
          ELSEIF ls_arn_reg_uom-uom_category = gc_cat_pallet.

            IF ls_arn_pir-base_units_per_pallet IS NOT INITIAL.

              ls_arn_reg_uom-num_base_units = ls_arn_pir-base_units_per_pallet.
              ls_arn_reg_uom-factor_of_base_units = ls_arn_pir-factor_of_buom_per_pallet.

            ELSE.
              IF ls_arn_pir-qty_units_per_pallet IS NOT INITIAL.
                ls_arn_reg_uom-num_base_units = ls_arn_pir-qty_units_per_pallet * ls_arn_uom_variant-num_base_units.
                ls_arn_reg_uom-factor_of_base_units = ls_arn_uom_variant-factor_of_base_units.

              ELSE.
                IF ls_arn_pir-qty_layers_per_pallet IS NOT INITIAL.

                  CLEAR ls_arn_reg_uom_tmp.
                  READ TABLE gt_arn_reg_uom_all INTO ls_arn_reg_uom_tmp
                  WITH KEY idno  = ls_arn_reg_uom-idno
                           meinh = ls_arn_reg_uom-lower_meinh.
                  IF sy-subrc = 0.
                    ls_arn_reg_uom-num_base_units = ls_arn_pir-qty_layers_per_pallet * ls_arn_reg_uom_tmp-num_base_units.
                    ls_arn_reg_uom-factor_of_base_units = ls_arn_reg_uom_tmp-factor_of_base_units.
                  ELSE.
                    ls_arn_reg_uom-num_base_units = 1.
                  ENDIF.
                ELSE.
                  ls_arn_reg_uom-num_base_units = 1.
                ENDIF.
              ENDIF.
            ENDIF.

            ls_arn_reg_uom-gross_weight_value = ls_arn_pir-gross_weight.     " Gross Weight Value
            ls_arn_reg_uom-height_value       = ls_arn_pir-gross_height.     " Gross Height Value

            CLEAR ls_arn_reg_uom_tmp.
            READ TABLE gt_arn_reg_uom_all INTO ls_arn_reg_uom_tmp
            WITH KEY idno                 = ls_arn_reg_uom-idno
                     pim_uom_code         = ls_arn_reg_uom-pim_uom_code
                     hybris_internal_code = space
                     lower_uom            = ls_arn_reg_uom-lower_child_uom.
            IF sy-subrc = 0.
              ls_arn_reg_uom-gross_weight_uom = ls_arn_reg_uom_tmp-gross_weight_uom.       " Gross Weight UOM
              ls_arn_reg_uom-height_uom       = ls_arn_reg_uom_tmp-height_uom.             " Height UOM
            ENDIF.


          ENDIF.      " ls_arn_reg_uom-uom_category = LAYER/PALLET


* INS Begin of Change INC5409316 JKH 17.11.2016
* Convert Height into 'MM'
          zcl_arn_gui_load=>unit_conversion_simple(
                EXPORTING input    = ls_arn_reg_uom-height_value
                          unit_in  = ls_arn_reg_uom-height_uom
                          unit_out = 'MM'
                IMPORTING output   = ls_arn_reg_uom-height_value
                          ev_error = lv_uom_conv_err ).
          IF lv_uom_conv_err IS INITIAL.
            ls_arn_reg_uom-height_uom = 'MMT'.
          ENDIF.
* INS End of Change INC5409316 JKH 17.11.2016

          IF ls_arn_reg_uom-num_base_units IS INITIAL.
            ls_arn_reg_uom-num_base_units = 1.
          ENDIF.

          IF ls_arn_reg_uom-factor_of_base_units IS INITIAL.
            ls_arn_reg_uom-factor_of_base_units = 1.
          ENDIF.





**  For LAYER:FILE-SAP_UOMCODE where FILE-HYBRIS_UOMCODE= ZARN_REG_UOM-PIM_UOM_CODE &
**  FILE-PIM_PIRCODE = ZARN_REG_UOM-HYBRIS_INTERNAL_CODE &
**  belong to TVARVC-NAME= 'GC_LAYER_UNOMS'
          CLEAR: ls_uom, ls_arn_reg_uom-meinh.
          LOOP AT gt_uom INTO ls_uom
            WHERE zzfan = fi_s_mara-zzfan AND hyb_uom_code = ls_arn_reg_uom-pim_uom_code
            AND hyb_pir = ls_arn_reg_uom-hybris_internal_code.

            " Layer record
            IF ls_arn_reg_uom-uom_category EQ gc_cat_layer.
              IF ls_uom-sap_uom_code IN gtr_layer_uoms[].
                ls_arn_reg_uom-meinh = ls_uom-sap_uom_code.
                EXIT.
              ENDIF.
            ELSE.
              " Pallet record
              IF ls_uom-sap_uom_code IN gtr_pallets_uoms[].
                ls_arn_reg_uom-meinh = ls_uom-sap_uom_code.
                EXIT.
              ENDIF.
            ENDIF.
          ENDLOOP.

          "For Layer: FILE-SAP_UOMCODE where FILE-HYBRIS_UOMCODE= ZARN_REG_UOM-LOWER_UOM
          CLEAR ls_arn_reg_uom-lower_meinh.
          " Layer record
          IF ls_arn_reg_uom-uom_category EQ gc_cat_layer.
            CLEAR ls_arn_reg_uom_all.
            READ TABLE gt_arn_reg_uom_all INTO ls_arn_reg_uom_all TRANSPORTING meinh
              WITH KEY idno = fi_v_idno pim_uom_code = ls_arn_reg_uom-lower_uom.
            IF sy-subrc EQ 0.
              ls_arn_reg_uom-lower_meinh = ls_arn_reg_uom_all-meinh.
            ENDIF.
            "For Pallet: MARM-MEINH for Layer from above step"
          ELSE.
            CLEAR ls_arn_reg_uom_temp.
            READ TABLE gt_arn_reg_uom_all INTO ls_arn_reg_uom_temp
              WITH KEY pim_uom_code = ls_arn_reg_uom-pim_uom_code uom_category = gc_cat_layer.
            IF sy-subrc EQ 0.
              ls_arn_reg_uom-lower_meinh = ls_arn_reg_uom_temp-meinh.
            ENDIF.
          ENDIF.


          IF ls_arn_reg_uom-meinh IS NOT INITIAL AND ls_arn_reg_uom-lower_meinh IS NOT INITIAL.
            CLEAR ls_marm_idno .
            READ TABLE gt_marm_idno INTO ls_marm_idno
              WITH KEY meinh = ls_arn_reg_uom-meinh mesub = ls_arn_reg_uom-lower_meinh.
            IF sy-subrc NE 0.
              CLEAR ls_arn_reg_uom-meinh.
            ENDIF.
          ENDIF.

**********************************************************************
          " DEFAULTING LOGIC in case UOM missing in FILE
          IF ls_arn_reg_uom-meinh IS INITIAL.

            lt_marm_idno_tmp[] = gt_marm_idno[].

            DELETE lt_marm_idno_tmp[] WHERE uom_category NE ls_arn_reg_uom-uom_category.

* Get the highest UOM at last so that If multiple UOM exist for the same conversion,
* then the highest is defaulted
            SORT lt_marm_idno_tmp[] BY seqno.

* Check in MARM if UOM exist for the same conversion, get count
            CLEAR lv_count.
            CLEAR ls_marm_idno_tmp.
            LOOP AT lt_marm_idno_tmp INTO ls_marm_idno_tmp
             WHERE idno  = ls_arn_reg_uom-idno
               AND umrez = ls_arn_reg_uom-num_base_units
               AND umren = ls_arn_reg_uom-factor_of_base_units
               AND mesub = ls_arn_reg_uom-lower_meinh.
              lv_count = lv_count + 1.
            ENDLOOP.

            IF lv_count GE 1.
              ls_arn_reg_uom-meinh = ls_marm_idno_tmp-meinh.
            ELSE.


              ls_arn_reg_uom-relationship = 1.

* For MEINH, check if UOM already exist (either in DB or in new UOM),
* then get the next higher UOM
*            CLEAR: lt_reg_uom_tmp_ui[].
*          DATA ls_arn_reg_uom_temp LIKE LINE OF fc_t_arn_reg_uom.

              lt_reg_uom_ui_tmp[] = gt_arn_reg_uom_ui[].

              DELETE lt_reg_uom_ui_tmp[] WHERE uom_category NE ls_arn_reg_uom-uom_category.
              DELETE lt_reg_uom_ui_tmp[] WHERE meinh IS INITIAL.

* Get highest existing UOM
              SORT lt_reg_uom_ui_tmp[] BY seqno DESCENDING.
              CLEAR ls_reg_uom_ui_tmp.
              READ TABLE lt_reg_uom_ui_tmp[] INTO ls_reg_uom_ui_tmp INDEX 1.

* get highest UOM on the top
              SORT lt_marm_idno_tmp[] BY seqno DESCENDING.

* Get highest existing UOM from MARM
              CLEAR ls_marm_idno_tmp.
              READ TABLE lt_marm_idno_tmp[] INTO ls_marm_idno_tmp INDEX 1.

* If highest UOM is same in defaulted UOM and MARM then get the next higher UOM
* else, check the highest UOM in defaulted UOM and MARM and get the next higher UOM

              CLEAR lv_seqno.
              IF ls_reg_uom_ui_tmp-seqno = ls_marm_idno_tmp-seqno.
                lv_seqno = ls_reg_uom_ui_tmp-seqno + 1.
              ELSEIF ls_reg_uom_ui_tmp-seqno GT ls_marm_idno_tmp-seqno.
                lv_seqno = ls_reg_uom_ui_tmp-seqno + 1.
              ELSEIF ls_reg_uom_ui_tmp-seqno LT ls_marm_idno_tmp-seqno.
                lv_seqno = ls_marm_idno_tmp-seqno + 1.
              ENDIF.

              IF lv_seqno GT 0.
* Get next higher UOM to be defaulted
                CLEAR ls_uom_cat.
                READ TABLE gt_uom_cat[] INTO ls_uom_cat
                WITH KEY uom_category = ls_arn_reg_uom-uom_category
                         seqno = lv_seqno.
                IF sy-subrc = 0.
                  ls_arn_reg_uom-meinh = ls_uom_cat-uom.
                ENDIF.
              ENDIF.

            ENDIF.  " READ TABLE lt_marm_idno INTO ls_marm_idno

          ENDIF.
**********************************************************************


*  UNIT_BASE = "X" if above ZARN_REG_UOM-MEINH=MARA-MEINS
          CLEAR ls_arn_reg_uom-unit_base.
          IF ls_arn_reg_uom-meinh EQ fi_s_mara-meins.
            ls_arn_reg_uom-unit_base = abap_true.
          ENDIF.

**  UNIT_PURORD = "X" if above ZARN_REG_UOM-MEINH=MARA-BSTME
**  OR If above ZARN_REG_UOM-MEINH=MARA-MEINS & if MARA-BSTME="" "" then ""X"""
          CLEAR ls_arn_reg_uom-unit_purord.
          IF ( ls_arn_reg_uom-meinh EQ fi_s_mara-bstme AND fi_s_mara-bstme IS NOT INITIAL ) OR
            ( ls_arn_reg_uom-meinh EQ fi_s_mara-meins AND fi_s_mara-bstme IS INITIAL ).

            ls_arn_reg_uom-unit_purord = abap_true.
          ENDIF.

**  SALES_UNIT = ""X"" if above ZARN_REG_UOM-MEINH=MAW1-WVRKM
          CLEAR: ls_maw1, ls_arn_reg_uom-sales_unit.
          READ TABLE gt_maw1 INTO ls_maw1 WITH TABLE KEY matnr = fi_s_mara-matnr.
          IF sy-subrc EQ 0 AND ls_maw1-wvrkm EQ ls_arn_reg_uom-meinh AND ls_maw1-wvrkm IS NOT INITIAL.
            ls_arn_reg_uom-sales_unit = abap_true.
**  If above ZARN_REG_UOM-MEINH=MARA-MEINS & if MAW1-WVRKM="" "" then ""X"""
          ELSEIF sy-subrc EQ 0 AND ls_maw1-wvrkm IS INITIAL AND ls_arn_reg_uom-meinh EQ fi_s_mara-meins.
            ls_arn_reg_uom-sales_unit = abap_true.
          ENDIF.

**  ISSUE_UNIT = "X" if above ZARN_REG_UOM-MEINH=MAW1-WAUSM
          CLEAR: ls_maw1, ls_arn_reg_uom-issue_unit.
          READ TABLE gt_maw1 INTO ls_maw1 WITH TABLE KEY matnr = fi_s_mara-matnr.
          IF sy-subrc EQ 0 AND ls_maw1-wausm EQ ls_arn_reg_uom-meinh AND ls_maw1-wausm IS NOT INITIAL.
            ls_arn_reg_uom-issue_unit = abap_true.
**  If above ZARN_REG_UOM-MEINH=MARA-MEINS & if MAW1-WAUSM="" "" then ""X"""
          ELSEIF sy-subrc EQ 0 AND ls_maw1-wausm IS INITIAL AND ls_arn_reg_uom-meinh EQ fi_s_mara-meins.
            ls_arn_reg_uom-issue_unit = abap_true.
          ENDIF.

          " LAYER
          IF ls_arn_reg_uom-uom_category = gc_cat_layer.
            CLEAR ls_arn_reg_uom_all.
            READ TABLE gt_arn_reg_uom_all INTO ls_arn_reg_uom_all
              WITH KEY idno = fi_v_idno pim_uom_code = ls_arn_reg_uom-lower_uom
                       lower_uom = ls_arn_reg_uom-lower_child_uom.

            IF ls_arn_reg_uom_all-num_base_units IS INITIAL.
              ls_arn_reg_uom_all-num_base_units = 1.
            ENDIF.
            IF ls_arn_reg_uom_all-factor_of_base_units IS INITIAL.
              ls_arn_reg_uom_all-factor_of_base_units = 1.
            ENDIF.
            IF ls_arn_reg_uom-num_base_units IS INITIAL.
              ls_arn_reg_uom-num_base_units = 1.
            ENDIF.
            IF ls_arn_reg_uom-factor_of_base_units IS INITIAL.
              ls_arn_reg_uom-factor_of_base_units = 1.
            ENDIF.

            ls_arn_reg_uom-higher_level_units = ( ls_arn_reg_uom-num_base_units / ls_arn_reg_uom-factor_of_base_units ) /
                                                ( ls_arn_reg_uom_all-num_base_units / ls_arn_reg_uom_all-factor_of_base_units ) .


            " PALLET
          ELSEIF ls_arn_reg_uom-uom_category = gc_cat_pallet.
            CLEAR ls_arn_reg_uom_all.
            READ TABLE gt_arn_reg_uom_all INTO ls_arn_reg_uom_all
              WITH KEY idno = fi_v_idno uom_category = gc_cat_layer
                       pim_uom_code = ls_arn_reg_uom-pim_uom_code
                       hybris_internal_code = ls_arn_reg_uom-hybris_internal_code
                       lower_uom = ls_arn_reg_uom-lower_uom
                       lower_child_uom = ls_arn_reg_uom-lower_child_uom.


            IF ls_arn_reg_uom_all-num_base_units IS INITIAL.
              ls_arn_reg_uom_all-num_base_units = 1.
            ENDIF.
            IF ls_arn_reg_uom_all-factor_of_base_units IS INITIAL.
              ls_arn_reg_uom_all-factor_of_base_units = 1.
            ENDIF.
            IF ls_arn_reg_uom-num_base_units IS INITIAL.
              ls_arn_reg_uom-num_base_units = 1.
            ENDIF.
            IF ls_arn_reg_uom-factor_of_base_units IS INITIAL.
              ls_arn_reg_uom-factor_of_base_units = 1.
            ENDIF.

            ls_arn_reg_uom-higher_level_units = ( ls_arn_reg_uom-num_base_units / ls_arn_reg_uom-factor_of_base_units ) /
                                                ( ls_arn_reg_uom_all-num_base_units / ls_arn_reg_uom_all-factor_of_base_units ) .


          ENDIF.

**  System Date of Upload (No update)
          ls_arn_reg_uom-ersda = sy-datlo.

**  System Date of Upload (No update)
          ls_arn_reg_uom-erzet = sy-timlo.

*        INSERT ls_arn_reg_uom INTO TABLE fc_t_arn_reg_uom.
          INSERT ls_arn_reg_uom INTO TABLE gt_arn_reg_uom_all.

          CLEAR ls_reg_uom_tmp_ui.
          MOVE-CORRESPONDING ls_arn_reg_uom TO ls_reg_uom_tmp_ui.


          READ TABLE gt_uom_cat INTO ls_uom_cat WITH KEY uom = ls_arn_reg_uom-meinh.
          IF sy-subrc EQ 0.
            ls_reg_uom_tmp_ui-cat_seqno = ls_uom_cat-cat_seqno.
            ls_reg_uom_tmp_ui-seqno = ls_uom_cat-seqno.
          ENDIF.

          APPEND ls_reg_uom_tmp_ui TO gt_arn_reg_uom_ui.


        ENDIF.   " READ TABLE gt_arn_reg_uom_in_db INTO ls_arn_reg_uom_in_db

      ENDDO.

    ENDIF.

  ENDLOOP.  " LOOP AT gt_arn_pir INTO ls_arn_pir WHERE idno = fi_v_idno

*  APPEND LINES OF gt_arn_reg_uom_ui TO gt_arn_reg_uom_all_ui.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  SET_REG_UOM_FLAGS_BAU
*&---------------------------------------------------------------------*
* Set UOM flags
*----------------------------------------------------------------------*
FORM set_reg_uom_flags_bau USING fi_v_version TYPE zarn_version
                                 fi_v_idno    TYPE zarn_idno.

  DATA:
    lv_base_count         TYPE i,
    lv_inv_count          TYPE i,
    lv_sale_count         TYPE i,
    lv_issu_count         TYPE i,
    lv_no_base            TYPE flag,
    lv_no_inv             TYPE flag,
    lv_no_sale            TYPE flag,
    lv_no_issu            TYPE flag,
    lv_count              TYPE i,
*    ls_uom_variant_base   TYPE zarn_uom_variant,
    ls_uom_variant_inv    TYPE zarn_uom_variant,
*    ls_uom_variant_sale   TYPE zarn_uom_variant,
    ls_uom_variant_issu   TYPE zarn_uom_variant,
    lt_reg_uom_tmp        TYPE ztarn_reg_uom_ui,
    lt_uomcat_range       TYPE RANGE OF zarn_uom_category,
    ls_uom_variant        TYPE zarn_uom_variant,
    ls_uom_cat            TYPE zarn_uom_cat,
    ls_uomcat_range       LIKE LINE OF lt_uomcat_range,
    ls_reg_uom_tmp        TYPE zsarn_reg_uom_ui,
    ls_reg_uom_exist_inv  TYPE zsarn_reg_uom_ui,
    ls_reg_uom_exist_sale TYPE zsarn_reg_uom_ui,
    ls_reg_uom_exist_issu TYPE zsarn_reg_uom_ui,
    ls_reg_uom_exist_base TYPE zsarn_reg_uom_ui,
    lt_uom_var_lower      TYPE ty_t_uom_var_lower,
    ls_uom_var_lower      TYPE ty_s_uom_var_lower,
    lt_uom_var_lower_tmp  TYPE ty_t_uom_var_lower,
    ls_uom_var_lower_tmp  TYPE ty_s_uom_var_lower,
    ls_gtin_var_child     LIKE LINE OF gt_arn_gtin_var,
    ls_uom_variant_child  LIKE LINE OF gt_arn_uom_variant,
    ls_prod_uom_t         TYPE ty_s_prod_uom_t,
    ls_reg_uom_exist      TYPE zsarn_reg_uom_ui.

  FIELD-SYMBOLS :
*    <ls_arn_reg_uom_all> LIKE LINE OF gt_arn_reg_uom_all,
    <ls_reg_uom_exist>   TYPE zarn_reg_uom.



  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
  WITH KEY tabname = 'ZARN_REG_UOM'.
  IF sy-subrc NE 0.
    READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH KEY tabname = 'ZARN_REG_BANNER'
             fldname = 'VRKME'.
    IF sy-subrc NE 0.
      EXIT.
    ENDIF.
  ENDIF.


*******************************************************************************

* Get Lower and Lower Child UOM codes for UOM variant to identify default units
  CLEAR: lt_uom_var_lower[].

  LOOP AT gt_arn_uom_variant[] INTO ls_uom_variant
    WHERE idno = fi_v_idno AND version = fi_v_version.

    CLEAR ls_uom_var_lower.
    ls_uom_var_lower-idno                 = ls_uom_variant-idno.
    ls_uom_var_lower-version              = ls_uom_variant-version.
    ls_uom_var_lower-uom_code             = ls_uom_variant-uom_code.
    ls_uom_var_lower-hybris_internal_code = space.

    ls_uom_var_lower-base_unit            = ls_uom_variant-base_unit.
    ls_uom_var_lower-consumer_unit        = ls_uom_variant-consumer_unit.
    ls_uom_var_lower-despatch_unit        = ls_uom_variant-despatch_unit.
    ls_uom_var_lower-an_invoice_unit      = ls_uom_variant-an_invoice_unit.



* Get Lower UOM and Lower child UOM
    CLEAR ls_gtin_var_child.
    " ZGTIN_VAR-UOM_CODE where ZGTIN_VAR-GTIN_CODE = ZUOM_VARIANT-CHILD_GTIN
    READ TABLE gt_arn_gtin_var[] INTO ls_gtin_var_child
      WITH KEY idno      = ls_uom_variant-idno
               version   = ls_uom_variant-version
               gtin_code = ls_uom_variant-child_gtin.

    IF sy-subrc EQ 0 AND ls_gtin_var_child-uom_code IS NOT INITIAL.
      ls_uom_var_lower-lower_uom = ls_gtin_var_child-uom_code.

      " ZGTIN_VAR-UOM_CODE where ZGTIN_VAR-GTIN_CODE = ZUOM_VARIANT-CHILD_GTIN
      " & ZUOM_VARIANT-UOM_CODE= ZREG_UOM-LOWER_UOM
      CLEAR ls_uom_variant_child.
      READ TABLE gt_arn_uom_variant[] INTO ls_uom_variant_child
        WITH KEY idno     = ls_uom_variant-idno
                 version  = ls_uom_variant-version
                 uom_code = ls_gtin_var_child-uom_code.
      IF sy-subrc EQ 0 AND ls_uom_variant_child-child_gtin IS NOT INITIAL.
        CLEAR ls_gtin_var_child.
        READ TABLE gt_arn_gtin_var[] INTO ls_gtin_var_child
           WITH KEY idno      = ls_uom_variant_child-idno
                    version   = ls_uom_variant_child-version
                    gtin_code = ls_uom_variant_child-child_gtin.
        IF sy-subrc EQ 0.
          ls_uom_var_lower-lower_child_uom = ls_gtin_var_child-uom_code.
        ENDIF.
      ENDIF.
    ENDIF.

* Get SAP UOM for Product UOM
    CLEAR ls_prod_uom_t.
    READ TABLE gt_arn_prod_uom_t[] INTO ls_prod_uom_t
    WITH TABLE KEY product_uom = ls_uom_variant-product_uom.
    IF sy-subrc = 0.
      ls_uom_var_lower-product_uom  = ls_prod_uom_t-sap_uom.
      ls_uom_var_lower-uom_category = ls_prod_uom_t-sap_uom_cat.
    ELSE.
      ls_uom_var_lower-product_uom = ls_uom_variant-product_uom.
    ENDIF.

    CLEAR ls_reg_uom_exist.
    READ TABLE gt_arn_reg_uom_ui INTO ls_reg_uom_exist
               WITH KEY idno                 = ls_uom_var_lower-idno
                        uom_category         = ls_uom_var_lower-uom_category
                        pim_uom_code         = ls_uom_var_lower-uom_code
                        hybris_internal_code = ls_uom_var_lower-hybris_internal_code
                        lower_uom            = ls_uom_var_lower-lower_uom
                        lower_child_uom      = ls_uom_var_lower-lower_child_uom.
    IF sy-subrc = 0.
      ls_uom_var_lower-meinh                = ls_reg_uom_exist-meinh.
      ls_uom_var_lower-lower_meinh          = ls_reg_uom_exist-lower_meinh.
      ls_uom_var_lower-uom_category         = ls_reg_uom_exist-uom_category.
      ls_uom_var_lower-cat_seqno            = ls_reg_uom_exist-cat_seqno.
      ls_uom_var_lower-seqno                = ls_reg_uom_exist-seqno.
    ENDIF.

    APPEND ls_uom_var_lower TO lt_uom_var_lower[].
  ENDLOOP.  " LOOP AT ls_prod_data-zarn_uom_variant[] INTO ls_uom_variant

  SORT lt_uom_var_lower[] BY cat_seqno seqno ASCENDING.


  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_UOM' fldname = 'UNIT_BASE'.
  IF sy-subrc EQ 0.
    " UNIT_BASE
* Check if Base Unit has to be defaulted
* If yes then, get the count from national how many base units are there
* else, mark no_base flag as true -> base unit will not be defaulted as it is already there from DB
    CLEAR: ls_reg_uom_exist_base, lv_base_count, lv_count, lv_no_base,
    ls_uom_var_lower.

    READ TABLE gt_arn_reg_uom_ui INTO ls_reg_uom_exist_base
    WITH KEY unit_base = abap_true.
    IF sy-subrc NE 0.

      LOOP AT lt_uom_var_lower[] INTO ls_uom_var_lower
        WHERE idno = fi_v_idno
        AND version = fi_v_version
        AND base_unit = abap_true.

        lv_count = lv_count + 1.
      ENDLOOP.

      lv_base_count = lv_count.
    ELSE.
      lv_no_base = abap_true.
    ENDIF.

* Default Base Unit, if required
    IF lv_no_base NE abap_true.

      lt_uom_var_lower_tmp[] = lt_uom_var_lower[].

      IF lv_base_count GE 1.
        DELETE lt_uom_var_lower_tmp[] WHERE base_unit NE abap_true.
      ENDIF.  " IF lv_base_count GE 1

* Get the lowest UOM Category and UOM on the top
      SORT lt_uom_var_lower_tmp[] BY cat_seqno seqno ASCENDING.

* Default the topmost
      CLEAR ls_uom_var_lower_tmp.
      READ TABLE lt_uom_var_lower_tmp[] INTO ls_uom_var_lower_tmp INDEX 1.
      IF sy-subrc = 0.
        READ TABLE gt_arn_reg_uom_all[] ASSIGNING <ls_reg_uom_exist>
                   WITH KEY idno                 = ls_uom_var_lower_tmp-idno
                            uom_category         = ls_uom_var_lower_tmp-uom_category
                            pim_uom_code         = ls_uom_var_lower_tmp-uom_code
                            hybris_internal_code = ls_uom_var_lower_tmp-hybris_internal_code
                            lower_uom            = ls_uom_var_lower_tmp-lower_uom
                            lower_child_uom      = ls_uom_var_lower_tmp-lower_child_uom.
        IF sy-subrc = 0.
*        <ls_reg_uom_exist>           = ls_reg_uom_tmp.
          <ls_reg_uom_exist>-unit_base = abap_true.
        ENDIF.

      ENDIF.

    ENDIF.  " IF lv_no_base NE abap_true
  ENDIF.  " 'UNIT_BASE'




**********************************************************************

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_UOM' fldname = 'SALES_UNIT'.
  IF sy-subrc EQ 0.

    " SALES_UNIT

* Check if Sale Unit has to be defaulted
* If yes then, get the count from national how many Sale units are there
* else, mark no_Sale flag as true -> Sale unit will not be defaulted as it is already there from DB
    CLEAR: ls_reg_uom_exist_sale, ls_uom_var_lower, lv_sale_count, lv_count, lv_no_sale.

    READ TABLE gt_arn_reg_uom_ui[] INTO ls_reg_uom_exist_sale
    WITH KEY sales_unit = abap_true.

    IF sy-subrc NE 0.
      LOOP AT lt_uom_var_lower[] INTO ls_uom_var_lower
        WHERE idno = fi_v_idno
        AND version = fi_v_version
        AND consumer_unit = abap_true.
        lv_count = lv_count + 1.
      ENDLOOP.

      lv_sale_count = lv_count.
    ELSE.
      lv_no_sale = abap_true.
    ENDIF.

* Default Sale Unit, if required
    IF lv_no_sale NE abap_true.


      lt_uom_var_lower_tmp[] = lt_uom_var_lower[].

      IF lv_sale_count GE 1.
        DELETE lt_uom_var_lower_tmp[] WHERE consumer_unit NE abap_true.
      ENDIF.  " IF lv_sale_count GE 1. GE 1

* Get the lowest UOM Category and UOM on the top
      SORT lt_uom_var_lower_tmp[] BY cat_seqno seqno ASCENDING.



* Default the topmost
      CLEAR ls_uom_var_lower_tmp.
      READ TABLE lt_uom_var_lower_tmp[] INTO ls_uom_var_lower_tmp INDEX 1.
      IF sy-subrc = 0.
        READ TABLE gt_arn_reg_uom_all[] ASSIGNING <ls_reg_uom_exist>
                   WITH KEY idno                 = ls_uom_var_lower_tmp-idno
                            uom_category         = ls_uom_var_lower_tmp-uom_category
                            pim_uom_code         = ls_uom_var_lower_tmp-uom_code
                            hybris_internal_code = ls_uom_var_lower_tmp-hybris_internal_code
                            lower_uom            = ls_uom_var_lower_tmp-lower_uom
                            lower_child_uom      = ls_uom_var_lower_tmp-lower_child_uom.
        IF sy-subrc = 0.
*        <ls_reg_uom_exist>           = ls_reg_uom_tmp.
          <ls_reg_uom_exist>-sales_unit = abap_true.
        ENDIF.

      ENDIF.

    ENDIF.  " IF lv_no_sale NE abap_true
  ENDIF.  " 'SALES_UNIT'



**********************************************************************

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_UOM' fldname = 'UNIT_PURORD'.
  IF sy-subrc EQ 0.
    " UNIT_PURORD

* Check if Invoice Unit has to be defaulted
* If yes then, get the count from national how many Invoice units are there
* else, mark no_Inv flag as true -> Invoice unit will not be defaulted as it is already there from DB
    CLEAR: ls_reg_uom_exist_inv, ls_uom_var_lower, lv_inv_count, lv_count, lv_no_inv.

    READ TABLE gt_arn_reg_uom_ui[] INTO ls_reg_uom_exist_inv
    WITH KEY unit_purord = abap_true.
    IF sy-subrc NE 0.
      LOOP AT lt_uom_var_lower[] INTO ls_uom_var_lower
        WHERE idno = fi_v_idno
        AND version = fi_v_version
        AND an_invoice_unit = abap_true.
        lv_count = lv_count + 1.
      ENDLOOP.

      lv_inv_count = lv_count.
    ELSE.
      lv_no_inv = abap_true.
    ENDIF.


* Default Invoice Unit, if required
    IF lv_no_inv NE abap_true.

      lt_uom_var_lower_tmp[] = lt_uom_var_lower[].

      IF lv_inv_count GE 1.
        DELETE lt_uom_var_lower_tmp[] WHERE an_invoice_unit NE abap_true.
      ENDIF.  " IF lv_inv_count GE 1

* Get the highest UOM Category and UOM on the top
      SORT lt_uom_var_lower_tmp[] BY cat_seqno DESCENDING seqno DESCENDING.

* Default the topmost
      CLEAR ls_uom_var_lower_tmp.
      READ TABLE lt_uom_var_lower_tmp[] INTO ls_uom_var_lower_tmp INDEX 1.
      IF sy-subrc = 0.
        READ TABLE gt_arn_reg_uom_all[] ASSIGNING <ls_reg_uom_exist>
                   WITH KEY idno                 = ls_uom_var_lower_tmp-idno
                            uom_category         = ls_uom_var_lower_tmp-uom_category
                            pim_uom_code         = ls_uom_var_lower_tmp-uom_code
                            hybris_internal_code = ls_uom_var_lower_tmp-hybris_internal_code
                            lower_uom            = ls_uom_var_lower_tmp-lower_uom
                            lower_child_uom      = ls_uom_var_lower_tmp-lower_child_uom.
        IF sy-subrc = 0.
*        <ls_reg_uom_exist>           = ls_reg_uom_tmp.
          <ls_reg_uom_exist>-unit_purord = abap_true.
        ENDIF.

      ENDIF.

    ENDIF.  " IF lv_no_inv NE abap_true
  ENDIF.  " 'UNIT_PURORD'



**********************************************************************

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH TABLE KEY tabname = 'ZARN_REG_UOM' fldname = 'ISSUE_UNIT'.
  IF sy-subrc EQ 0.

    " ISSUE_UNIT
* Check if Issue Unit has to be defaulted
* If yes then, get the count from national how many Issue units are there
* else, mark no_Issu flag as true -> Issue unit will not be defaulted as it is already there from DB
    CLEAR: ls_reg_uom_exist_issu, ls_uom_var_lower, lv_issu_count, lv_count, lv_no_issu.

    READ TABLE gt_arn_reg_uom_ui[] INTO ls_reg_uom_exist_issu
    WITH KEY issue_unit = abap_true.
    IF sy-subrc NE 0.
      LOOP AT lt_uom_var_lower[] INTO ls_uom_var_lower
        WHERE idno = fi_v_idno
        AND version = fi_v_version
        AND despatch_unit = abap_true.

        lv_count = lv_count + 1.
      ENDLOOP.

      lv_issu_count = lv_count.
    ELSE.
      lv_no_issu = abap_true.
    ENDIF.

* Default Issue Unit, if required
    IF lv_no_issu NE abap_true.

      lt_uom_var_lower_tmp[] = lt_uom_var_lower[].

      IF lv_issu_count GE 1.

        DELETE lt_uom_var_lower_tmp[] WHERE despatch_unit NE abap_true.

* Get the highest UOM Category and UOM on the top
        SORT lt_uom_var_lower_tmp[] BY cat_seqno DESCENDING seqno DESCENDING.

* Default the topmost
        CLEAR ls_uom_var_lower_tmp.
        READ TABLE lt_uom_var_lower_tmp[] INTO ls_uom_var_lower_tmp INDEX 1.
        IF sy-subrc = 0.
          READ TABLE gt_arn_reg_uom_all[] ASSIGNING <ls_reg_uom_exist>
                     WITH KEY idno                 = ls_uom_var_lower_tmp-idno
                              uom_category         = ls_uom_var_lower_tmp-uom_category
                              pim_uom_code         = ls_uom_var_lower_tmp-uom_code
                              hybris_internal_code = ls_uom_var_lower_tmp-hybris_internal_code
                              lower_uom            = ls_uom_var_lower_tmp-lower_uom
                              lower_child_uom      = ls_uom_var_lower_tmp-lower_child_uom.
          IF sy-subrc = 0.
*          <ls_reg_uom_exist>           = ls_reg_uom_tmp.
            <ls_reg_uom_exist>-issue_unit = abap_true.
          ENDIF.

        ENDIF.

      ELSEIF lv_issu_count EQ 0.

        READ TABLE gt_arn_reg_uom_all[] ASSIGNING <ls_reg_uom_exist>
        WITH KEY idno = fi_v_idno unit_purord = abap_true.
        IF sy-subrc = 0.
          <ls_reg_uom_exist>-issue_unit = abap_true.
        ENDIF.

      ENDIF.  " IF lv_issu_count GE 1

    ENDIF.  " IF lv_no_issu NE abap_true
  ENDIF.  " 'ISSUE_UNIT'

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  FILL_REG_EAN_BAU
*&---------------------------------------------------------------------*
* Fill ZARN_REG_EAN EAN for Retail/Inner/Shipper
* in ZARN_REG_UOM-PIM_UOM_CODE
*----------------------------------------------------------------------*
FORM fill_reg_ean_bau USING fi_v_matnr       TYPE matnr
                            fi_v_idno        TYPE zarn_idno
                            fu_t_reg_uom     TYPE ztarn_reg_uom
                   CHANGING fc_t_arn_reg_ean TYPE ztarn_reg_ean.

  DATA ls_arn_reg_ean TYPE zarn_reg_ean.
  DATA ls_mean        LIKE LINE OF gt_mean.
*  DATA ls_marm        LIKE LINE OF gt_marm.
  DATA ls_reg_uom     TYPE zarn_reg_uom.
  DATA ls_arn_reg_ean_in_db  LIKE LINE OF gt_arn_reg_ean_in_db.

  FREE fc_t_arn_reg_ean.


  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
  WITH KEY tabname = 'ZARN_REG_EAN'.
  IF sy-subrc NE 0.

*    fc_t_arn_reg_ean[] = VALUE #( FOR ean IN gt_arn_reg_ean_in_db[]
*                                     WHERE ( idno = fi_v_idno ) ( ean ) ).

    EXIT.
  ENDIF.




  LOOP AT gt_mean INTO ls_mean WHERE matnr = fi_v_matnr.

    CLEAR ls_arn_reg_ean.

    ls_arn_reg_ean-mandt = sy-mandt.

    " fill data for all relevant EANs
    ls_arn_reg_ean-idno = fi_v_idno.

    CLEAR: ls_reg_uom.
    READ TABLE fu_t_reg_uom INTO ls_reg_uom WITH KEY idno  = fi_v_idno
      meinh = ls_mean-meinh.

    IF ls_mean-ean11 IS NOT INITIAL AND
*         ls_mean-eantp IN gtr_numtp[] AND
       ls_reg_uom    IS NOT INITIAL.

**  Populate the  above ZARN_REG_UOM-MEINH which is having the MEAN-EAN11 with
**  MEAN-NUMTP in "ZP","ZD","ZI",","ZS","ZB","ZR" & MEAN-MATNR = ZARN_REG_HDR-SAP_ARTICLE
      ls_arn_reg_ean-meinh = ls_mean-meinh.

**  EAN11 where  MEAN-NUMTP in ""ZP"",""ZD"",""ZI"","",""ZS"",""ZB"",""ZR"" &
**  MEAN-MEINH=ZARN_REG_EAN-MEINH & MEAN-MATNR = ZARN_REG_HDR-SAP_ARTICLE"
      ls_arn_reg_ean-ean11 = ls_mean-ean11.


* If record exist in AReNa then build data using migrate version,
* else using BAU version
      CLEAR ls_arn_reg_ean_in_db.
      READ TABLE gt_arn_reg_ean_in_db INTO ls_arn_reg_ean_in_db
      WITH TABLE KEY idno  = ls_arn_reg_ean-idno
                     meinh = ls_arn_reg_ean-meinh
                     ean11 = ls_arn_reg_ean-ean11.
      IF sy-subrc EQ 0.

        ls_arn_reg_ean-eantp = ls_arn_reg_ean_in_db-eantp.
        ls_arn_reg_ean-hpean = ls_arn_reg_ean_in_db-hpean.



        READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname = 'ZARN_REG_EAN' fldname = 'EANTP'.
        IF sy-subrc EQ 0.
**NUMTP of the EAN11 above
          ls_arn_reg_ean-eantp = ls_mean-eantp.
*        ELSE.
*          ls_arn_reg_ean-eantp = ls_arn_reg_ean_in_db-eantp.
        ENDIF.

        READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname = 'ZARN_REG_EAN' fldname = 'HPEAN'.
        IF sy-subrc EQ 0.
**      HPEAN of the EAN11 above
          ls_arn_reg_ean-hpean = ls_mean-hpean.
*        ELSE.
*          ls_arn_reg_ean-hpean = ls_arn_reg_ean_in_db-hpean.
        ENDIF.

        INSERT ls_arn_reg_ean INTO TABLE fc_t_arn_reg_ean.

      ELSE.  " READ TABLE gt_arn_reg_ean_in_db INTO ls_arn_reg_ean_in_db



**NUMTP of the EAN11 above
        ls_arn_reg_ean-eantp = ls_mean-eantp.

**      HPEAN of the EAN11 above
        ls_arn_reg_ean-hpean = ls_mean-hpean.
        INSERT ls_arn_reg_ean INTO TABLE fc_t_arn_reg_ean.


      ENDIF.  " READ TABLE gt_arn_reg_ean_in_db INTO ls_arn_reg_ean_in_db

    ENDIF.  " IF ls_mean-ean11 IS NOT INITIAL

  ENDLOOP.  " LOOP AT gt_mean INTO ls_mean WHERE matnr = fi_v_matnr



*  " Regional EAN update
*  IF gt_arn_reg_ean_in_db_old[] IS NOT INITIAL.
*    LOOP AT gt_arn_reg_ean_in_db_old INTO ls_arn_reg_ean.
*
*      READ TABLE fc_t_arn_reg_ean TRANSPORTING NO FIELDS
*        WITH KEY idno  = ls_arn_reg_ean-idno
*                 meinh = ls_arn_reg_ean-meinh
*                 ean11 = ls_arn_reg_ean-ean11.
*      IF sy-subrc NE 0.
*        INSERT ls_arn_reg_ean INTO TABLE fc_t_arn_reg_ean.
*      ENDIF.
*    ENDLOOP.
*  ENDIF.








ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  FILL_REG_PIR_BAU
*&---------------------------------------------------------------------*
* Fill ZARN_REG_PIR table
*----------------------------------------------------------------------*
FORM fill_reg_pir_bau USING fi_s_mara        TYPE mara
                            fi_v_version     TYPE zarn_version
                            fi_v_idno        TYPE zarn_idno
                   CHANGING fc_t_arn_reg_pir TYPE ztarn_reg_pir.


  DATA ls_arn_list_price              LIKE LINE OF gt_arn_list_price.
  DATA ls_arn_reg_pir                 LIKE LINE OF fc_t_arn_reg_pir.
  DATA ls_arn_pir                     LIKE LINE OF gt_arn_pir.
  DATA ls_arn_pir_eine                LIKE LINE OF gt_arn_pir.
  DATA ls_eina                        LIKE LINE OF gt_eina.
  DATA ls_pir                         LIKE LINE OF gt_pir.
  DATA ls_arn_reg_uom                 LIKE LINE OF gt_arn_reg_uom_all.
  DATA ls_eine                        LIKE LINE OF gt_eine.
*  DATA lv_no_of_rec                   TYPE i.
*  DATA lv_eina_key                    TYPE boole_d.
  DATA lv_eine_order_uom_pim          TYPE zarn_uom_code.
  DATA lv_eine_hybris_internal_code   TYPE zarn_hybris_internal_code.
  DATA ls_pir_not_found               LIKE LINE OF gt_pir_not_found.
  DATA ls_uom_variant                 LIKE LINE OF gt_arn_uom_variant.
  DATA ls_gtin_var_child              LIKE LINE OF gt_arn_gtin_var.
  DATA ls_uom_variant_child           LIKE LINE OF gt_arn_uom_variant.
  DATA ls_pir_vendor                  LIKE LINE OF gt_pir_vendor.
  DATA lv_eina_found                  TYPE boole_d.
  DATA ls_arn_reg_pir_in_db           LIKE LINE OF gt_arn_reg_pir_in_db.


  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
  WITH KEY tabname = 'ZARN_REG_PIR'.
  IF sy-subrc NE 0.

*    fc_t_arn_reg_pir[] = VALUE #( FOR pir IN gt_arn_reg_pir_in_db[]
*                                     WHERE ( idno = fi_v_idno ) ( pir ) ).

    EXIT.
  ENDIF.




  CHECK gt_arn_pir[] IS NOT INITIAL.

  " Loop at National PIR records to create/update the regional PIRs
  LOOP AT gt_arn_pir INTO ls_arn_pir WHERE idno = fi_v_idno.

    CLEAR: ls_arn_reg_pir, ls_pir, ls_eina, ls_arn_reg_uom, ls_eine, lv_eine_order_uom_pim,
           lv_eine_hybris_internal_code, ls_arn_pir_eine, lv_eina_found.

    ls_arn_reg_pir-mandt = sy-mandt.
    ls_arn_reg_pir-idno  = ls_arn_pir-idno.

**      Fill order_uom_pim and hybris_internal_code from ZARN_PIR table
    ls_arn_reg_pir-order_uom_pim        = ls_arn_pir-uom_code.
    ls_arn_reg_pir-hybris_internal_code = ls_arn_pir-hybris_internal_code.


* If record exist in AReNa then build data using migrate version,
* else using BAU version
    CLEAR ls_arn_reg_pir_in_db.
    READ TABLE gt_arn_reg_pir_in_db INTO ls_arn_reg_pir_in_db
    WITH TABLE KEY idno                 = ls_arn_reg_pir-idno
                   order_uom_pim        = ls_arn_reg_pir-order_uom_pim
                   hybris_internal_code = ls_arn_reg_pir-hybris_internal_code.
    IF sy-subrc EQ 0.

      ls_arn_reg_pir-lifab = ls_arn_reg_pir_in_db-lifab.
      ls_arn_reg_pir-lifbi = ls_arn_reg_pir_in_db-lifbi.
      ls_arn_reg_pir-vabme = ls_arn_reg_pir_in_db-vabme.
      ls_arn_reg_pir-ekkol = ls_arn_reg_pir_in_db-ekkol.
      ls_arn_reg_pir-ekgrp = ls_arn_reg_pir_in_db-ekgrp.
      ls_arn_reg_pir-minbm = ls_arn_reg_pir_in_db-minbm.
      ls_arn_reg_pir-norbm = ls_arn_reg_pir_in_db-norbm.
      ls_arn_reg_pir-aplfz = ls_arn_reg_pir_in_db-aplfz.
      ls_arn_reg_pir-mwskz = ls_arn_reg_pir_in_db-mwskz.
      ls_arn_reg_pir-uebto = ls_arn_reg_pir_in_db-uebto.
      ls_arn_reg_pir-untto = ls_arn_reg_pir_in_db-untto.



**    Read the SAP PIR from the PIR upload file for the corresponding FAN/Hybris PIR
      READ TABLE gt_pir INTO ls_pir WITH TABLE KEY zzfan = fi_s_mara-zzfan
        hyb_pir = ls_arn_reg_pir-hybris_internal_code.
      IF sy-subrc EQ 0.

**    Read the SAP PIR from the PIR upload file for the corresponding FAN/Hybris PIR
*    READ TABLE gt_pir INTO ls_pir
        LOOP AT gt_pir INTO ls_pir WHERE zzfan = fi_s_mara-zzfan
          AND hyb_pir = ls_arn_reg_pir-hybris_internal_code.

          CLEAR: lv_eina_found, ls_eina.
*    IF sy-subrc EQ 0.
          " Read the VENDOR from the EINA and update the regional table field for the vendor
          READ TABLE gt_eina INTO ls_eina WITH TABLE KEY infnr = ls_pir-sap_pir.
          IF sy-subrc EQ 0.
            lv_eina_found = abap_true.

            " get GLN for the upload file PIR and compare it with the GLN of the National PIR,
            " if same then set the vendor and other attributes from the matching EINA else use defaulting logic...
            READ TABLE gt_pir_vendor INTO ls_pir_vendor WITH KEY lifnr = ls_eina-lifnr.
*              bbbnr = ls_arn_pir-gln+0(7)  bbsnr = ls_arn_pir-gln+7(5) bubkz = ls_arn_pir-gln+12(1).

            IF sy-subrc EQ 0 AND ( ls_pir_vendor-bbbnr && ls_pir_vendor-bbsnr && ls_pir_vendor-bubkz EQ ls_arn_pir-gln ).

              lv_eina_found = abap_true.


              ls_arn_reg_pir-lifnr = ls_eina-lifnr.

              READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
                WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'LIFAB'.
              IF sy-subrc EQ 0.
                ls_arn_reg_pir-lifab = ls_eina-lifab.
*              ELSE.
*                ls_arn_reg_pir-lifab = ls_arn_reg_pir_in_db-lifab.
              ENDIF.

              READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
                WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'LIFBI'.
              IF sy-subrc EQ 0.
                ls_arn_reg_pir-lifbi = ls_eina-lifbi.
*              ELSE.
*                ls_arn_reg_pir-lifbi = ls_arn_reg_pir_in_db-lifbi.
              ENDIF.

              ls_arn_reg_pir-idnlf = ls_eina-idnlf.

**********************************************************************
              CLEAR ls_uom_variant.
              READ TABLE gt_arn_uom_variant INTO ls_uom_variant
                WITH KEY idno     = ls_arn_pir-idno
                         version  = ls_arn_pir-version
                         uom_code = ls_arn_pir-uom_code.
              IF sy-subrc = 0.

* Get Lower UOM and Lower child UOM
                CLEAR ls_gtin_var_child.
                READ TABLE gt_arn_gtin_var INTO ls_gtin_var_child
                  WITH KEY idno      = ls_uom_variant-idno
                           version   = ls_uom_variant-version
                           gtin_code = ls_uom_variant-child_gtin.
                IF sy-subrc EQ 0 AND ls_gtin_var_child-uom_code IS NOT INITIAL.

                  ls_arn_reg_pir-lower_uom = ls_gtin_var_child-uom_code.

                  CLEAR ls_uom_variant_child.
                  READ TABLE gt_arn_uom_variant INTO ls_uom_variant_child
                    WITH KEY idno     = ls_uom_variant-idno
                             version  = ls_uom_variant-version
                             uom_code = ls_gtin_var_child-uom_code.
                  IF sy-subrc EQ 0 AND ls_uom_variant_child-child_gtin IS NOT INITIAL.
                    CLEAR ls_gtin_var_child.
                    READ TABLE gt_arn_gtin_var INTO ls_gtin_var_child
                       WITH KEY idno      = ls_uom_variant_child-idno
                                version   = ls_uom_variant_child-version
                                gtin_code = ls_uom_variant_child-child_gtin.
                    IF sy-subrc EQ 0.

                      ls_arn_reg_pir-lower_child_uom = ls_gtin_var_child-uom_code.

                    ENDIF.
                  ENDIF.


                ENDIF.

              ENDIF.  " READ TABLE ls_prod_data-zarn_uom_variant[] INTO ls_uom_variant
**********************************************************************

              " Vendor mismatch
            ELSE.
              CLEAR lv_eina_found.
              READ TABLE  fc_t_arn_reg_pir TRANSPORTING NO FIELDS
                WITH KEY idno = fi_v_idno
                         order_uom_pim = ls_arn_pir-uom_code
                         hybris_internal_code = ls_arn_pir-hybris_internal_code.
              IF sy-subrc NE 0.

                CLEAR: ls_pir_not_found.
                ls_pir_not_found-idno     = fi_v_idno.
                ls_pir_not_found-matnr    = fi_s_mara-matnr.
                ls_pir_not_found-hyb_pir  = ls_arn_pir-hybris_internal_code.
                ls_pir_not_found-uom_code = ls_arn_pir-uom_code.
                ls_pir_not_found-version  = fi_v_version.
                ls_pir_not_found-gln      = ls_arn_pir-gln.
                INSERT ls_pir_not_found INTO TABLE gt_pir_not_found.

              ENDIF.

            ENDIF.

            " in case the EINA doesn't exist
          ELSE.
            CLEAR lv_eina_found.
            READ TABLE  fc_t_arn_reg_pir TRANSPORTING NO FIELDS
               WITH KEY idno = fi_v_idno
                        order_uom_pim = ls_arn_pir-uom_code
                        hybris_internal_code = ls_arn_pir-hybris_internal_code.
            IF sy-subrc NE 0.

              CLEAR: ls_pir_not_found.
              ls_pir_not_found-idno     = fi_v_idno.
              ls_pir_not_found-matnr    = fi_s_mara-matnr.
              ls_pir_not_found-hyb_pir  = ls_arn_pir-hybris_internal_code.
              ls_pir_not_found-uom_code = ls_arn_pir-uom_code.
              ls_pir_not_found-version  = fi_v_version.
              ls_pir_not_found-gln      = ls_arn_pir-gln.
              INSERT ls_pir_not_found INTO TABLE gt_pir_not_found.

            ENDIF.

          ENDIF.

*        DO lv_no_of_rec TIMES. " NOT REQUIRED
          IF lv_eina_found EQ abap_true.

            CLEAR ls_arn_reg_uom.
            READ TABLE gt_arn_reg_uom_all INTO ls_arn_reg_uom
              WITH KEY idno = fi_v_idno pim_uom_code = ls_arn_reg_pir-order_uom_pim hybris_internal_code = ''
              lower_uom = ls_arn_reg_pir-lower_uom lower_child_uom = ls_arn_reg_pir-lower_child_uom.
            IF sy-subrc EQ 0.
              ls_arn_reg_pir-bprme = ls_arn_reg_uom-meinh.
            ENDIF.


**   ZARN_LIST_PRICE-GLN
            ls_arn_reg_pir-gln_no = ls_arn_pir-gln.

**  ZARN_LIST_PRICE-GLN_DESCRIPTION
            ls_arn_reg_pir-gln_name = ls_arn_pir-gln_description.

            READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
              WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'VABME'.
            IF sy-subrc EQ 0.
**  EINA-VABME  for the EINA-INFNR found in "SAP-PIR"
              ls_arn_reg_pir-vabme = ls_eina-vabme.
*            ELSE.
*              ls_arn_reg_pir-vabme = ls_arn_reg_pir_in_db-vabme.
            ENDIF.


            IF ls_arn_reg_pir-bprme EQ ls_eina-meins. "ls_arn_reg_uom-meinh EQ ls_eina-meins.

              ls_arn_reg_pir-pir_rel_eina = abap_true.

**  EINA-RELIF   for the EINA-INFNR found in "SAP-PIR"
              ls_arn_reg_pir-relif = ls_eina-relif.
            ELSE.
              CLEAR: ls_arn_reg_pir-pir_rel_eina, ls_arn_reg_pir-relif.
            ENDIF.  " ls_arn_reg_pir-bprme EQ ls_eina-meins


            CLEAR ls_eine.
            READ TABLE gt_eine INTO ls_eine WITH TABLE KEY infnr = ls_eina-infnr.
            IF sy-subrc EQ 0.

**  ESOKZ where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
              ls_arn_reg_pir-esokz = ls_eine-esokz.


              IF ls_arn_reg_pir-bprme EQ ls_eine-bprme. "ls_arn_reg_uom-meinh EQ ls_eine-bprme.

                ls_arn_reg_pir-pir_rel_eine = abap_true.

                READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
                  WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'EKKOL'.
                IF sy-subrc EQ 0.
                  ls_arn_reg_pir-ekkol = ls_eine-ekkol.
*                ELSE.
*                  ls_arn_reg_pir-ekkol = ls_arn_reg_pir_in_db-ekkol.
                ENDIF.

                READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
                  WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'EKGRP'.
                IF sy-subrc EQ 0.
**  EKGRP where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
                  ls_arn_reg_pir-ekgrp = ls_eine-ekgrp.
*                ELSE.
*                  ls_arn_reg_pir-ekgrp = ls_arn_reg_pir_in_db-ekgrp.
                ENDIF.

                READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
                  WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'MINBM'.
                IF sy-subrc EQ 0.
**    MINBM where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
                  ls_arn_reg_pir-minbm = ls_eine-minbm.
*                ELSE.
*                  ls_arn_reg_pir-minbm = ls_arn_reg_pir_in_db-minbm.
                ENDIF.

                READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
                  WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'NORBM'.
                IF sy-subrc EQ 0.
**  NORBM where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
                  ls_arn_reg_pir-norbm = ls_eine-norbm.
*                ELSE.
*                  ls_arn_reg_pir-norbm = ls_arn_reg_pir_in_db-norbm.
                ENDIF.

                READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
                  WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'APLFZ'.
                IF sy-subrc EQ 0.
**  APLFZ where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
                  ls_arn_reg_pir-aplfz = ls_eine-aplfz.
*                ELSE.
*                  ls_arn_reg_pir-aplfz = ls_arn_reg_pir_in_db-aplfz.
                ENDIF.

                READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
                  WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'MWSKZ'.
                IF sy-subrc EQ 0.
**  MWSKZ where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
                  ls_arn_reg_pir-mwskz = ls_eine-mwskz.
*                ELSE.
*                  ls_arn_reg_pir-mwskz = ls_arn_reg_pir_in_db-mwskz.
                ENDIF.

**  LOEKZ where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
                ls_arn_reg_pir-loekz = ls_eine-loekz.


                READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
                  WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'UEBTO'.
                IF sy-subrc EQ 0.
**  UEBTO where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
                  ls_arn_reg_pir-uebto = ls_eine-uebto.
*                ELSE.
*                  ls_arn_reg_pir-uebto = ls_arn_reg_pir_in_db-uebto.
                ENDIF.

                READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
                  WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'UNTTO'.
                IF sy-subrc EQ 0.
**  UNTTO where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
                  ls_arn_reg_pir-untto = ls_eine-untto.
*                ELSE.
*                  ls_arn_reg_pir-untto = ls_arn_reg_pir_in_db-untto.
                ENDIF.




**  "X" if NETPR = 0 where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
              ELSE.


                CLEAR ls_arn_reg_pir-pir_rel_eine.

                READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
                  WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'MINBM'.
                IF sy-subrc EQ 0.
                  ls_arn_reg_pir-minbm = 1.
                ENDIF.

                READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
                  WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'NORBM'.
                IF sy-subrc EQ 0.
                  ls_arn_reg_pir-norbm = 1.
                ENDIF.

                READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
                  WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'MWSKZ'.
                IF sy-subrc EQ 0.
                  ls_arn_reg_pir-mwskz = 'P1'.
                ENDIF.

              ENDIF.

              IF ls_eine-netpr EQ 0.
                ls_arn_reg_pir-allow_zero_price = abap_true.
              ENDIF.

            ENDIF. " EINE READ

            CLEAR ls_arn_list_price.
            " SELECT LIST PRICE
            READ TABLE gt_arn_list_price INTO ls_arn_list_price
              WITH KEY idno = fi_v_idno version = ls_arn_pir-version
                             uom_code = ls_arn_reg_pir-order_uom_pim gln = ls_arn_pir-gln active_lp = abap_true.
            IF sy-subrc EQ 0.

**CURRENCY where ZARN_REG_PIR-ORDER_UOM_PIM & ZARN_LIST_PRICE-GLN
              ls_arn_reg_pir-waers = ls_arn_list_price-currency.

**PRICE_VALUE where ZARN_REG_PIR-ORDER_UOM_PIM & ZARN_LIST_PRICE-GLN
              ls_arn_reg_pir-netpr = ls_arn_list_price-price_value.

**QUANTITY where ZARN_REG_PIR-ORDER_UOM_PIM & ZARN_LIST_PRICE-GLN found
              ls_arn_reg_pir-peinh = ls_arn_list_price-quantity.

**EFF_START_DATE where ZARN_REG_PIR-ORDER_UOM_PIM & ZARN_LIST_PRICE-GLN found in "SAP-PIR"
              ls_arn_reg_pir-datlb = ls_arn_list_price-eff_start_date.

**EFF_END_DATE where ZARN_REG_PIR-ORDER_UOM_PIM & ZARN_LIST_PRICE-GLN found in "SAP-PIR"
              ls_arn_reg_pir-prdat = ls_arn_list_price-eff_end_date.

            ENDIF. " Select list price`

**    insert as the records to be created/updated...
            INSERT ls_arn_reg_pir INTO TABLE fc_t_arn_reg_pir.

*        ENDDO.

          ENDIF. " lv_eina_found EQ abap_true.

        ENDLOOP.

      ELSE.

        READ TABLE  fc_t_arn_reg_pir TRANSPORTING NO FIELDS
                WITH KEY idno = fi_v_idno
                         order_uom_pim = ls_arn_pir-uom_code
                         hybris_internal_code = ls_arn_pir-hybris_internal_code.
        IF sy-subrc NE 0.
          CLEAR ls_pir_not_found.

          ls_pir_not_found-idno = fi_v_idno.
          ls_pir_not_found-matnr = fi_s_mara-matnr.
          ls_pir_not_found-hyb_pir = ls_arn_pir-hybris_internal_code.
          ls_pir_not_found-uom_code = ls_arn_pir-uom_code.
          ls_pir_not_found-version = fi_v_version.
          ls_pir_not_found-gln = ls_arn_pir-gln.

          INSERT ls_pir_not_found INTO TABLE gt_pir_not_found.
        ENDIF.

      ENDIF.  " READ TABLE gt_pir INTO ls_pir


    ELSE.  " READ TABLE gt_arn_reg_pir_in_db INTO ls_arn_reg_pir_in_db

**    Read the SAP PIR from the PIR upload file for the corresponding FAN/Hybris PIR
      READ TABLE gt_pir INTO ls_pir WITH TABLE KEY zzfan = fi_s_mara-zzfan
        hyb_pir = ls_arn_reg_pir-hybris_internal_code.
      IF sy-subrc EQ 0.

**    Read the SAP PIR from the PIR upload file for the corresponding FAN/Hybris PIR
*    READ TABLE gt_pir INTO ls_pir
        LOOP AT gt_pir INTO ls_pir WHERE zzfan = fi_s_mara-zzfan
          AND hyb_pir = ls_arn_reg_pir-hybris_internal_code.

          CLEAR: lv_eina_found, ls_eina.
*    IF sy-subrc EQ 0.
          " Read the VENDOR from the EINA and update the regional table field for the vendor
          READ TABLE gt_eina INTO ls_eina WITH TABLE KEY infnr = ls_pir-sap_pir.
          IF sy-subrc EQ 0.
            lv_eina_found = abap_true.

            " get GLN for the upload file PIR and compare it with the GLN of the National PIR,
            " if same then set the vendor and other attributes from the matching EINA else use defaulting logic...
            READ TABLE gt_pir_vendor INTO ls_pir_vendor WITH KEY lifnr = ls_eina-lifnr.
*              bbbnr = ls_arn_pir-gln+0(7)  bbsnr = ls_arn_pir-gln+7(5) bubkz = ls_arn_pir-gln+12(1).

            IF sy-subrc EQ 0 AND ( ls_pir_vendor-bbbnr && ls_pir_vendor-bbsnr && ls_pir_vendor-bubkz EQ ls_arn_pir-gln ).

              lv_eina_found = abap_true.
              ls_arn_reg_pir-lifnr = ls_eina-lifnr.
              ls_arn_reg_pir-lifab = ls_eina-lifab.
              ls_arn_reg_pir-lifbi = ls_eina-lifbi.
              ls_arn_reg_pir-idnlf = ls_eina-idnlf.

**********************************************************************
              CLEAR ls_uom_variant.
              READ TABLE gt_arn_uom_variant INTO ls_uom_variant
                WITH KEY idno     = ls_arn_pir-idno
                         version  = ls_arn_pir-version
                         uom_code = ls_arn_pir-uom_code.
              IF sy-subrc = 0.

* Get Lower UOM and Lower child UOM
                CLEAR ls_gtin_var_child.
                READ TABLE gt_arn_gtin_var INTO ls_gtin_var_child
                  WITH KEY idno      = ls_uom_variant-idno
                           version   = ls_uom_variant-version
                           gtin_code = ls_uom_variant-child_gtin.
                IF sy-subrc EQ 0 AND ls_gtin_var_child-uom_code IS NOT INITIAL.

                  ls_arn_reg_pir-lower_uom = ls_gtin_var_child-uom_code.

                  CLEAR ls_uom_variant_child.
                  READ TABLE gt_arn_uom_variant INTO ls_uom_variant_child
                    WITH KEY idno     = ls_uom_variant-idno
                             version  = ls_uom_variant-version
                             uom_code = ls_gtin_var_child-uom_code.
                  IF sy-subrc EQ 0 AND ls_uom_variant_child-child_gtin IS NOT INITIAL.
                    CLEAR ls_gtin_var_child.
                    READ TABLE gt_arn_gtin_var INTO ls_gtin_var_child
                       WITH KEY idno      = ls_uom_variant_child-idno
                                version   = ls_uom_variant_child-version
                                gtin_code = ls_uom_variant_child-child_gtin.
                    IF sy-subrc EQ 0.

                      ls_arn_reg_pir-lower_child_uom = ls_gtin_var_child-uom_code.

                    ENDIF.
                  ENDIF.
                ENDIF.

              ENDIF.  " READ TABLE ls_prod_data-zarn_uom_variant[] INTO ls_uom_variant
**********************************************************************

              " Vendor mismatch
            ELSE.
              CLEAR lv_eina_found.
              READ TABLE  fc_t_arn_reg_pir TRANSPORTING NO FIELDS
                WITH KEY idno = fi_v_idno
                         order_uom_pim = ls_arn_pir-uom_code
                         hybris_internal_code = ls_arn_pir-hybris_internal_code.
              IF sy-subrc NE 0.

                CLEAR: ls_pir_not_found.
                ls_pir_not_found-idno = fi_v_idno.
                ls_pir_not_found-matnr = fi_s_mara-matnr.
                ls_pir_not_found-hyb_pir = ls_arn_pir-hybris_internal_code.
                ls_pir_not_found-uom_code = ls_arn_pir-uom_code.
                ls_pir_not_found-version = fi_v_version.
                ls_pir_not_found-gln = ls_arn_pir-gln.
                INSERT ls_pir_not_found INTO TABLE gt_pir_not_found.

              ENDIF.

            ENDIF.

            " in case the EINA doesn't exist
          ELSE.
            CLEAR lv_eina_found.
            READ TABLE  fc_t_arn_reg_pir TRANSPORTING NO FIELDS
               WITH KEY idno = fi_v_idno
                        order_uom_pim = ls_arn_pir-uom_code
                        hybris_internal_code = ls_arn_pir-hybris_internal_code.
            IF sy-subrc NE 0.

              CLEAR: ls_pir_not_found.
              ls_pir_not_found-idno = fi_v_idno.
              ls_pir_not_found-matnr = fi_s_mara-matnr.
              ls_pir_not_found-hyb_pir = ls_arn_pir-hybris_internal_code.
              ls_pir_not_found-uom_code = ls_arn_pir-uom_code.
              ls_pir_not_found-version = fi_v_version.
              ls_pir_not_found-gln = ls_arn_pir-gln.
              INSERT ls_pir_not_found INTO TABLE gt_pir_not_found.

            ENDIF.

          ENDIF.


*        DO lv_no_of_rec TIMES. " NOT REQUIRED
          IF lv_eina_found EQ abap_true.

            CLEAR ls_arn_reg_uom.
            READ TABLE gt_arn_reg_uom_all INTO ls_arn_reg_uom
              WITH KEY idno = fi_v_idno pim_uom_code = ls_arn_reg_pir-order_uom_pim hybris_internal_code = ''
              lower_uom = ls_arn_reg_pir-lower_uom lower_child_uom = ls_arn_reg_pir-lower_child_uom.
            IF sy-subrc EQ 0.
              ls_arn_reg_pir-bprme = ls_arn_reg_uom-meinh.
            ENDIF.


**   ZARN_LIST_PRICE-GLN
            ls_arn_reg_pir-gln_no = ls_arn_pir-gln.

**  ZARN_LIST_PRICE-GLN_DESCRIPTION
            ls_arn_reg_pir-gln_name = ls_arn_pir-gln_description.

**  EINA-VABME  for the EINA-INFNR found in "SAP-PIR"
            ls_arn_reg_pir-vabme = ls_eina-vabme.

            IF ls_arn_reg_pir-bprme EQ ls_eina-meins. "ls_arn_reg_uom-meinh EQ ls_eina-meins.
              ls_arn_reg_pir-pir_rel_eina = abap_true.

**  EINA-RELIF   for the EINA-INFNR found in "SAP-PIR"
              ls_arn_reg_pir-relif = ls_eina-relif.
            ELSE.
              CLEAR: ls_arn_reg_pir-pir_rel_eina, ls_arn_reg_pir-relif.
            ENDIF.

            CLEAR ls_eine.
            READ TABLE gt_eine INTO ls_eine WITH TABLE KEY infnr = ls_eina-infnr.
            IF sy-subrc EQ 0.

**  ESOKZ where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
              ls_arn_reg_pir-esokz = ls_eine-esokz.

              IF ls_arn_reg_pir-bprme EQ ls_eine-bprme. "ls_arn_reg_uom-meinh EQ ls_eine-bprme.

                ls_arn_reg_pir-pir_rel_eine = abap_true.

                ls_arn_reg_pir-ekkol = ls_eine-ekkol.

**  EKGRP where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
                ls_arn_reg_pir-ekgrp = ls_eine-ekgrp.

**    MINBM where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
                ls_arn_reg_pir-minbm = ls_eine-minbm.

**  NORBM where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
                ls_arn_reg_pir-norbm = ls_eine-norbm.

**  APLFZ where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
                ls_arn_reg_pir-aplfz = ls_eine-aplfz.

**  MWSKZ where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
                ls_arn_reg_pir-mwskz = ls_eine-mwskz.

**  LOEKZ where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
                ls_arn_reg_pir-loekz = ls_eine-loekz.

**  UEBTO where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
                ls_arn_reg_pir-uebto = ls_eine-uebto.

**  UNTTO where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
                ls_arn_reg_pir-untto = ls_eine-untto.

**  "X" if NETPR = 0 where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
              ELSE.

                CLEAR ls_arn_reg_pir-pir_rel_eine.

                ls_arn_reg_pir-minbm = 1.
                ls_arn_reg_pir-norbm = 1.
                ls_arn_reg_pir-mwskz = 'P1'.
              ENDIF.

              IF ls_eine-netpr EQ 0.
                ls_arn_reg_pir-allow_zero_price = abap_true.
              ENDIF.

            ENDIF. " EINE READ

            CLEAR ls_arn_list_price.
            " SELECT LIST PRICE
            READ TABLE gt_arn_list_price INTO ls_arn_list_price
              WITH KEY idno = fi_v_idno version = ls_arn_pir-version
                             uom_code = ls_arn_reg_pir-order_uom_pim gln = ls_arn_pir-gln active_lp = abap_true.
            IF sy-subrc EQ 0.
**CURRENCY where ZARN_REG_PIR-ORDER_UOM_PIM & ZARN_LIST_PRICE-GLN
              ls_arn_reg_pir-waers = ls_arn_list_price-currency.

**PRICE_VALUE where ZARN_REG_PIR-ORDER_UOM_PIM & ZARN_LIST_PRICE-GLN
              ls_arn_reg_pir-netpr = ls_arn_list_price-price_value.

**QUANTITY where ZARN_REG_PIR-ORDER_UOM_PIM & ZARN_LIST_PRICE-GLN found
              ls_arn_reg_pir-peinh = ls_arn_list_price-quantity.

**EFF_START_DATE where ZARN_REG_PIR-ORDER_UOM_PIM & ZARN_LIST_PRICE-GLN found in "SAP-PIR"
              ls_arn_reg_pir-datlb = ls_arn_list_price-eff_start_date.

**EFF_END_DATE where ZARN_REG_PIR-ORDER_UOM_PIM & ZARN_LIST_PRICE-GLN found in "SAP-PIR"
              ls_arn_reg_pir-prdat = ls_arn_list_price-eff_end_date.

            ENDIF. " Select list price`

**    insert as the records to be created/updated...
            INSERT ls_arn_reg_pir INTO TABLE fc_t_arn_reg_pir.

*        ENDDO.

          ENDIF. " lv_eina_found EQ abap_true.

        ENDLOOP.

      ELSE.

        READ TABLE  fc_t_arn_reg_pir TRANSPORTING NO FIELDS
                WITH KEY idno = fi_v_idno
                         order_uom_pim = ls_arn_pir-uom_code
                         hybris_internal_code = ls_arn_pir-hybris_internal_code.
        IF sy-subrc NE 0.
          CLEAR ls_pir_not_found.

          ls_pir_not_found-idno = fi_v_idno.
          ls_pir_not_found-matnr = fi_s_mara-matnr.
          ls_pir_not_found-hyb_pir = ls_arn_pir-hybris_internal_code.
          ls_pir_not_found-uom_code = ls_arn_pir-uom_code.
          ls_pir_not_found-version = fi_v_version.
          ls_pir_not_found-gln = ls_arn_pir-gln.

          INSERT ls_pir_not_found INTO TABLE gt_pir_not_found.
        ENDIF.

      ENDIF.  "  READ TABLE gt_pir INTO ls_pir


    ENDIF.  " READ TABLE gt_arn_reg_pir_in_db INTO ls_arn_reg_pir_in_db

  ENDLOOP.  " LOOP AT gt_arn_pir INTO ls_arn_pir WHERE idno = fi_v_idno


  LOOP AT gt_pir_not_found INTO ls_pir_not_found.
    READ TABLE  fc_t_arn_reg_pir TRANSPORTING NO FIELDS
      WITH KEY idno = fi_v_idno
*     order_uom_pim = ls_pir_not_found-uom_code
      hybris_internal_code = ls_pir_not_found-hyb_pir.
    IF sy-subrc EQ 0.
      DELETE gt_pir_not_found WHERE idno = fi_v_idno AND hyb_pir = ls_pir_not_found-hyb_pir.
    ENDIF.
  ENDLOOP.

  DELETE ADJACENT DUPLICATES FROM gt_pir_not_found COMPARING ALL FIELDS.

  IF gt_pir_not_found[] IS NOT INITIAL.
    INSERT LINES OF gt_pir_not_found INTO TABLE gt_pir_not_found_all.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  FILL_REG_HSNO_BAU
*&---------------------------------------------------------------------*
* Fill ZARN_REG_HSNO table
*----------------------------------------------------------------------*
FORM fill_reg_hsno_bau USING fi_s_mara         TYPE mara
                             fi_v_version      TYPE zarn_version
                             fi_v_idno         TYPE zarn_idno
                    CHANGING fc_t_arn_reg_hsno TYPE ztarn_reg_hsno.


  DATA: ls_arn_hsno     TYPE zarn_hsno,
        ls_arn_reg_hsno TYPE zarn_reg_hsno.





  CLEAR fc_t_arn_reg_hsno[].
  FREE  fc_t_arn_reg_hsno[].

  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
  WITH KEY tabname = 'ZARN_REG_HSNO'.
  IF sy-subrc NE 0.

*    fc_t_arn_reg_hsno[] = VALUE #( FOR hsno IN gt_arn_reg_hsno_in_db[]
*                                     WHERE ( idno = fi_v_idno ) ( hsno ) ).

    EXIT.
  ENDIF.





  LOOP AT gt_arn_hsno[] INTO ls_arn_hsno
    WHERE idno    = fi_v_idno
      AND version = fi_v_version.

    CLEAR ls_arn_reg_hsno.
    ls_arn_reg_hsno-mandt     = sy-mandt.
    ls_arn_reg_hsno-idno      = ls_arn_hsno-idno.
    ls_arn_reg_hsno-hsno_code = ls_arn_hsno-hsno_code.

    APPEND ls_arn_reg_hsno TO fc_t_arn_reg_hsno[].
  ENDLOOP.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  GET_NOT_FOUND_PIR_BAU
*&---------------------------------------------------------------------*
FORM get_not_found_pir_bau .

  DATA ls_pir_not_found LIKE LINE OF gt_pir_not_found.
  DATA: lt_pir_vendor        TYPE ty_t_pir_vendor,
        lt_pir_vendor1       TYPE ty_t_pir_vendor,
        ls_pir_vendor        TYPE ty_s_pir_vendor,
        ls_marm              TYPE ty_s_marm,
        ls_marm_idno         TYPE ty_s_marm_idno,
        ls_uom_cat           TYPE zarn_uom_cat,
        lt_lfm1              TYPE SORTED TABLE OF lfm1 WITH UNIQUE KEY lifnr ekorg,
        ls_pir               LIKE LINE OF gt_arn_pir,
        lt_reg_pir_ui        TYPE ztarn_reg_pir_ui,
        lt_reg_pir           TYPE ztarn_reg_pir,
        ls_reg_pir           TYPE zarn_reg_pir,
        ls_reg_pir_ui        TYPE zsarn_reg_pir_ui,
        ls_uom_variant       LIKE LINE OF gt_arn_uom_variant,
        ls_gtin_var_child    LIKE LINE OF gt_arn_gtin_var,
        ls_uom_variant_child TYPE zarn_uom_variant,
        lv_count             TYPE i,
        ls_lfm1              TYPE lfm1,
        ls_list_price        TYPE zarn_list_price,
        lt_reg_uom_ui        TYPE ztarn_reg_uom_ui,
        ls_reg_uom_ui        TYPE zsarn_reg_uom_ui,
*        ls_reg_pir           TYPE zarn_reg_pir,
*        lt_reg_pir_new       TYPE ztarn_reg_pir_ui,
        lt_reg_pir_gln       TYPE ztarn_reg_pir_ui,
        ls_reg_pir_gln       TYPE zsarn_reg_pir_ui,
        lt_reg_pir_tmp       TYPE ztarn_reg_pir_ui,
        ls_reg_pir_tmp       TYPE zsarn_reg_pir_ui,
        lt_reg_pir_sort      TYPE ztarn_reg_pir_ui,
        ls_reg_pir_sort      TYPE zsarn_reg_pir_ui,
        lt_pir               TYPE zarn_pir,
        lt_pir_tmp           TYPE ztarn_pir,
        ls_pir_tmp           TYPE zarn_pir,
*        lt_reg_pir           TYPE ztarn_reg_pir,
        lt_reg_pir_db        TYPE ztarn_reg_pir,
        ls_reg_pir_db        TYPE zarn_reg_pir,
        ls_products          TYPE zarn_products,
        lt_reg_uom_tmp       TYPE ztarn_reg_uom_ui,
        ls_reg_uom_tmp       TYPE zsarn_reg_uom_ui,
        lt_list_price_tmp    TYPE ztarn_list_price,
        ls_list_price_tmp    TYPE zarn_list_price,
        lt_list_price_gln    TYPE ztarn_list_price,
        ls_list_price_gln    TYPE zarn_list_price,
        lv_no_of_rec         TYPE i,
        ls_eina              LIKE LINE OF gt_eina,
        lt_reg_pir_temp      TYPE ty_t_reg_pir,
        ls_reg_pir_temp      TYPE ty_s_reg_pir,
        ls_eine              LIKE LINE OF gt_eine,
*        lv_eina_key          TYPE boole_d,
*        lv_eine_key          TYPE boole_d,
        ls_arn_reg_uom       LIKE LINE OF gt_arn_reg_uom_all.

  DATA ls_arn_reg_pir_in_db           LIKE LINE OF gt_arn_reg_pir_in_db.


  FIELD-SYMBOLS:
    <ls_reg_pir_ui>   TYPE zsarn_reg_pir_ui,
    <ls_reg_pir_temp> TYPE ty_s_reg_pir.

*  FREE: gt_eina[], gt_eine[].


  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
  WITH KEY tabname = 'ZARN_REG_PIR'.
  IF sy-subrc NE 0.
    EXIT.
  ENDIF.


  CHECK gt_pir_not_found_all[] IS NOT INITIAL.

  lt_reg_uom_ui[] = gt_arn_reg_uom_all_ui[].


  CLEAR lt_pir_vendor1[].
  LOOP AT gt_pir_not_found_all INTO ls_pir_not_found.
    CLEAR ls_pir_vendor.
    ls_pir_vendor-bbbnr = ls_pir_not_found-gln+0(7).
    ls_pir_vendor-bbsnr = ls_pir_not_found-gln+7(5).
    ls_pir_vendor-bubkz = ls_pir_not_found-gln+12(1).
    INSERT ls_pir_vendor INTO TABLE lt_pir_vendor1[].
  ENDLOOP.

  IF lt_pir_vendor1[] IS NOT INITIAL.

    SELECT lifnr bbbnr bbsnr bubkz                      "#EC CI_NOFIELD
      INTO CORRESPONDING FIELDS OF TABLE lt_pir_vendor[]
      FROM lfa1
      FOR ALL ENTRIES IN lt_pir_vendor1[]
      WHERE bbbnr EQ lt_pir_vendor1-bbbnr
      AND bbsnr EQ lt_pir_vendor1-bbsnr
      AND bubkz EQ lt_pir_vendor1-bubkz.

    IF lt_pir_vendor[] IS NOT INITIAL.

      SELECT lifnr ekorg ekgrp plifz
        INTO CORRESPONDING FIELDS OF TABLE lt_lfm1[]
        FROM lfm1
        FOR ALL ENTRIES IN lt_pir_vendor[]
        WHERE lifnr EQ lt_pir_vendor-lifnr
        AND ekorg EQ '9999'.
    ENDIF.

  ENDIF.

**  get old DB values for  existing records
  SELECT * FROM zarn_reg_pir INTO TABLE gt_arn_reg_pir_in_db
    FOR ALL ENTRIES IN gt_pir_not_found_all
    WHERE idno = gt_pir_not_found_all-idno.


  LOOP AT gt_pir_not_found_all INTO ls_pir_not_found.

    READ TABLE gt_arn_pir INTO ls_pir
      WITH TABLE KEY idno = ls_pir_not_found-idno version = ls_pir_not_found-version
      uom_code = ls_pir_not_found-uom_code hybris_internal_code = ls_pir_not_found-hyb_pir.
    IF sy-subrc EQ 0.
      CLEAR ls_reg_pir_ui.
      ls_reg_pir_ui-mandt                = sy-mandt.
      ls_reg_pir_ui-idno                 = ls_pir-idno.
      ls_reg_pir_ui-order_uom_pim        = ls_pir-uom_code.
      ls_reg_pir_ui-hybris_internal_code = ls_pir-hybris_internal_code.



* If record exist in AReNa then build data using migrate version,
* else using BAU version
      CLEAR ls_arn_reg_pir_in_db.
      READ TABLE gt_arn_reg_pir_in_db_old INTO ls_arn_reg_pir_in_db
      WITH TABLE KEY idno                 = ls_reg_pir_ui-idno
                     order_uom_pim        = ls_reg_pir_ui-order_uom_pim
                     hybris_internal_code = ls_reg_pir_ui-hybris_internal_code.
      IF sy-subrc = 0.

        ls_reg_pir_ui-minbm = ls_arn_reg_pir_in_db-minbm.
        ls_reg_pir_ui-norbm = ls_arn_reg_pir_in_db-norbm.
        ls_reg_pir_ui-mwskz = ls_arn_reg_pir_in_db-mwskz.
        ls_reg_pir_ui-lifab = ls_arn_reg_pir_in_db-lifab.
        ls_reg_pir_ui-lifbi = ls_arn_reg_pir_in_db-lifbi.
        ls_reg_pir_ui-ekgrp = ls_arn_reg_pir_in_db-ekgrp.
        ls_reg_pir_ui-aplfz = ls_arn_reg_pir_in_db-aplfz.
        ls_reg_pir_ui-vabme = ls_arn_reg_pir_in_db-vabme.



        ls_reg_pir_ui-gln_no               = ls_pir-gln.
        ls_reg_pir_ui-gln_name             = ls_pir-gln_description.
        ls_reg_pir_ui-esokz                = '0'.

        READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'MINBM'.
        IF sy-subrc EQ 0.
          ls_reg_pir_ui-minbm                = 1.
*        ELSE.
*          ls_reg_pir_ui-minbm = ls_arn_reg_pir_in_db-minbm.
        ENDIF.

        READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'NORBM'.
        IF sy-subrc EQ 0.
          ls_reg_pir_ui-norbm                = 1.
*        ELSE.
*          ls_reg_pir_ui-norbm = ls_arn_reg_pir_in_db-norbm.
        ENDIF.

        READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'MWSKZ'.
        IF sy-subrc EQ 0.
          ls_reg_pir_ui-mwskz                = 'P1'.
*        ELSE.
*          ls_reg_pir_ui-mwskz = ls_arn_reg_pir_in_db-mwskz.
        ENDIF.

        READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'LIFAB'.
        IF sy-subrc EQ 0.
          ls_reg_pir_ui-lifab                = ls_pir-avail_start_date.
*        ELSE.
*          ls_reg_pir_ui-lifab = ls_arn_reg_pir_in_db-lifab.
        ENDIF.

        READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'LIFBI'.
        IF sy-subrc EQ 0.
          ls_reg_pir_ui-lifbi                = ls_pir-avail_end_date.

          IF ls_reg_pir_ui-lifab IS NOT INITIAL AND
             ls_reg_pir_ui-lifbi IS INITIAL.
            ls_reg_pir_ui-lifbi = '99991231'.
          ENDIF.
*        ELSE.
*          ls_reg_pir_ui-lifbi = ls_arn_reg_pir_in_db-lifbi.
        ENDIF.



        CLEAR ls_uom_variant.
        READ TABLE gt_arn_uom_variant INTO ls_uom_variant
          WITH KEY idno     = ls_pir-idno
                   version  = ls_pir-version
                   uom_code = ls_pir-uom_code.
        IF sy-subrc = 0.

* Get Lower UOM and Lower child UOM
          CLEAR ls_gtin_var_child.
          READ TABLE gt_arn_gtin_var INTO ls_gtin_var_child
            WITH KEY idno      = ls_uom_variant-idno
                     version   = ls_uom_variant-version
                     gtin_code = ls_uom_variant-child_gtin.
          IF sy-subrc EQ 0 AND ls_gtin_var_child-uom_code IS NOT INITIAL.

            ls_reg_pir_ui-lower_uom = ls_gtin_var_child-uom_code.

            CLEAR ls_uom_variant_child.
            READ TABLE gt_arn_uom_variant INTO ls_uom_variant_child
              WITH KEY idno     = ls_uom_variant-idno
                       version  = ls_uom_variant-version
                       uom_code = ls_gtin_var_child-uom_code.
            IF sy-subrc EQ 0 AND ls_uom_variant_child-child_gtin IS NOT INITIAL.
              CLEAR ls_gtin_var_child.
              READ TABLE gt_arn_gtin_var INTO ls_gtin_var_child
                 WITH KEY idno      = ls_uom_variant_child-idno
                          version   = ls_uom_variant_child-version
                          gtin_code = ls_uom_variant_child-child_gtin.
              IF sy-subrc EQ 0.

                ls_reg_pir_ui-lower_child_uom = ls_gtin_var_child-uom_code.

              ENDIF.
            ENDIF.
          ENDIF.

        ENDIF.  " READ TABLE ls_prod_data-zarn_uom_variant[] INTO ls_uom_variant



* Get vendor from GLN
        CLEAR: ls_pir_vendor, lv_count.
        LOOP AT lt_pir_vendor[] INTO ls_pir_vendor
        WHERE bbbnr = ls_pir-gln(7)
           AND            bbsnr = ls_pir-gln+7(5)
           AND            bubkz = ls_pir-gln+12(1).
          lv_count = lv_count + 1.
        ENDLOOP.

        IF lv_count GT 1.

* multiple Vendors
          ls_reg_pir_ui-lifnr = 'MULTIPLE'.

        ELSEIF lv_count EQ 1.

* single Vendors
          ls_reg_pir_ui-lifnr = ls_pir_vendor-lifnr.

* Vendor master record purchasing organization data
          CLEAR ls_lfm1.
          READ TABLE lt_lfm1[] INTO ls_lfm1
          WITH TABLE KEY lifnr = ls_reg_pir_ui-lifnr
                         ekorg = '9999'.
          IF sy-subrc = 0.


            READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
              WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'EKGRP'.
            IF sy-subrc EQ 0.
              ls_reg_pir_ui-ekgrp = ls_lfm1-ekgrp.
*            ELSE.
*              ls_reg_pir_ui-ekgrp = ls_arn_reg_pir_in_db-ekgrp.
            ENDIF.

            READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
              WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'APLFZ'.
            IF sy-subrc EQ 0.
              ls_reg_pir_ui-aplfz = ls_lfm1-plifz.
*            ELSE.
*              ls_reg_pir_ui-aplfz = ls_arn_reg_pir_in_db-aplfz.
            ENDIF.


          ENDIF.
        ENDIF. " IF lv_count GT 1


        READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'VABME'.
        IF sy-subrc EQ 0.
          IF ls_pir-variable_unit = abap_true.
            ls_reg_pir_ui-vabme = 1.
          ENDIF.
*        ELSE.
*          ls_reg_pir_ui-vabme = ls_arn_reg_pir_in_db-vabme.
        ENDIF.

* Get Pricing Data from national
        CLEAR ls_list_price.
        READ TABLE gt_arn_list_price INTO ls_list_price
             WITH KEY idno     = ls_pir-idno
                      version  = ls_pir-version
                      uom_code = ls_pir-uom_code
                      gln      = ls_pir-gln
                      active_lp = abap_true.
        IF sy-subrc = 0.
          ls_reg_pir_ui-waers = ls_list_price-currency.
          ls_reg_pir_ui-netpr = ls_list_price-price_value.
          ls_reg_pir_ui-peinh = ls_list_price-quantity.
          ls_reg_pir_ui-datlb = ls_list_price-eff_start_date.
          ls_reg_pir_ui-prdat = ls_list_price-eff_end_date.
        ENDIF.  " READ TABLE ls_prod_data-zarn_list_price[] INTO ls_list_price


* Get Order Unit from already defaulted REG_UOM
        CLEAR ls_reg_uom_ui.
        READ TABLE lt_reg_uom_ui INTO ls_reg_uom_ui
        WITH KEY idno                 = ls_pir-idno
                 pim_uom_code         = ls_reg_pir_ui-order_uom_pim
                 hybris_internal_code = space
                 lower_uom            = ls_reg_pir_ui-lower_uom
                 lower_child_uom      = ls_reg_pir_ui-lower_child_uom.     "++INC5599313 JKH 03.02.2017
        IF sy-subrc = 0.

          ls_reg_pir_ui-bprme                = ls_reg_uom_ui-meinh.
          ls_reg_pir_ui-meinh                = ls_reg_uom_ui-meinh.
          ls_reg_pir_ui-lower_meinh          = ls_reg_uom_ui-lower_meinh.
          ls_reg_pir_ui-num_base_units       = ls_reg_uom_ui-num_base_units.
          ls_reg_pir_ui-factor_of_base_units = ls_reg_uom_ui-factor_of_base_units.
          ls_reg_pir_ui-unit_purord          = ls_reg_uom_ui-unit_purord.
          ls_reg_pir_ui-cat_seqno            = ls_reg_uom_ui-cat_seqno.
        ENDIF.

        ls_reg_pir_ui-bbbnr = ls_pir-gln(7).
        ls_reg_pir_ui-bbsnr = ls_pir-gln+7(5).
        ls_reg_pir_ui-bubkz = ls_pir-gln+12(1).


* Check if REG_PIR already exist in Regional DB then dont default that row, use from DB
        CLEAR ls_reg_pir.
        READ TABLE gt_arn_reg_pir_in_db INTO ls_reg_pir
        WITH KEY idno                 = ls_reg_pir_ui-idno
                 order_uom_pim        = ls_reg_pir_ui-order_uom_pim
                 hybris_internal_code = ls_reg_pir_ui-hybris_internal_code.
        IF sy-subrc = 0.

          ls_reg_pir-gln_no   = ls_reg_pir_ui-gln_no.
          ls_reg_pir-gln_name = ls_reg_pir_ui-gln_name.
          ls_reg_pir-waers    = ls_reg_pir_ui-waers.
          ls_reg_pir-netpr    = ls_reg_pir_ui-netpr.
          ls_reg_pir-peinh    = ls_reg_pir_ui-peinh.
          ls_reg_pir-datlb    = ls_reg_pir_ui-datlb.
          ls_reg_pir-prdat    = ls_reg_pir_ui-prdat.
          ls_reg_pir-bprme    = ls_reg_pir_ui-bprme.
          ls_reg_pir-lifab    = ls_reg_pir_ui-lifab.
          ls_reg_pir-lifbi    = ls_reg_pir_ui-lifbi.
          ls_reg_pir-lifnr    = ls_reg_pir_ui-lifnr.
          ls_reg_pir-idnlf    = ls_reg_pir_ui-idnlf.

          IF ls_reg_pir-lifab IS NOT INITIAL AND
             ls_reg_pir-lifbi IS INITIAL.
            ls_reg_pir-lifbi = '99991231'.
          ENDIF.

          ls_reg_pir-lower_uom       = ls_reg_pir_ui-lower_uom.          "++INC5599313 JKH 03.02.2017
          ls_reg_pir-lower_child_uom = ls_reg_pir_ui-lower_child_uom.    "++INC5599313 JKH 03.02.2017

          MOVE-CORRESPONDING ls_reg_pir TO ls_reg_pir_ui.

          CLEAR ls_reg_pir_temp.
          ls_reg_pir_temp-include = ls_reg_pir_ui.
          ls_reg_pir_temp-matnr = ls_pir_not_found-matnr.

*        MOVE-CORRESPONDING ls_reg_pir_ui TO ls_reg_pir_temp.
          INSERT ls_reg_pir_temp INTO TABLE lt_reg_pir_temp.

          INSERT ls_reg_pir_ui INTO TABLE lt_reg_pir_ui.
        ELSE.

          CLEAR ls_reg_pir_temp.
          ls_reg_pir_temp-include = ls_reg_pir_ui.
          ls_reg_pir_temp-matnr = ls_pir_not_found-matnr.

*        MOVE-CORRESPONDING ls_reg_pir_ui TO ls_reg_pir_temp.
          INSERT ls_reg_pir_temp INTO TABLE lt_reg_pir_temp.

          INSERT ls_reg_pir_ui INTO TABLE lt_reg_pir_ui.

*        APPEND ls_reg_pir_ui TO lt_reg_pir_new[].
        ENDIF.




      ELSE.  " READ TABLE gt_arn_reg_pir_in_db_old INTO ls_arn_reg_pir_in_db


        ls_reg_pir_ui-gln_no               = ls_pir-gln.
        ls_reg_pir_ui-gln_name             = ls_pir-gln_description.
        ls_reg_pir_ui-esokz                = '0'.
        ls_reg_pir_ui-minbm                = 1.
        ls_reg_pir_ui-norbm                = 1.
        ls_reg_pir_ui-mwskz                = 'P1'.
        ls_reg_pir_ui-lifab                = ls_pir-avail_start_date.
        ls_reg_pir_ui-lifbi                = ls_pir-avail_end_date.

        IF ls_reg_pir_ui-lifab IS NOT INITIAL AND
           ls_reg_pir_ui-lifbi IS INITIAL.
          ls_reg_pir_ui-lifbi = '99991231'.
        ENDIF.


        CLEAR ls_uom_variant.
        READ TABLE gt_arn_uom_variant INTO ls_uom_variant
          WITH KEY idno     = ls_pir-idno
                   version  = ls_pir-version
                   uom_code = ls_pir-uom_code.
        IF sy-subrc = 0.

* Get Lower UOM and Lower child UOM
          CLEAR ls_gtin_var_child.
          READ TABLE gt_arn_gtin_var INTO ls_gtin_var_child
            WITH KEY idno      = ls_uom_variant-idno
                     version   = ls_uom_variant-version
                     gtin_code = ls_uom_variant-child_gtin.
          IF sy-subrc EQ 0 AND ls_gtin_var_child-uom_code IS NOT INITIAL.

            ls_reg_pir_ui-lower_uom = ls_gtin_var_child-uom_code.

            CLEAR ls_uom_variant_child.
            READ TABLE gt_arn_uom_variant INTO ls_uom_variant_child
              WITH KEY idno     = ls_uom_variant-idno
                       version  = ls_uom_variant-version
                       uom_code = ls_gtin_var_child-uom_code.
            IF sy-subrc EQ 0 AND ls_uom_variant_child-child_gtin IS NOT INITIAL.
              CLEAR ls_gtin_var_child.
              READ TABLE gt_arn_gtin_var INTO ls_gtin_var_child
                 WITH KEY idno      = ls_uom_variant_child-idno
                          version   = ls_uom_variant_child-version
                          gtin_code = ls_uom_variant_child-child_gtin.
              IF sy-subrc EQ 0.

                ls_reg_pir_ui-lower_child_uom = ls_gtin_var_child-uom_code.

              ENDIF.
            ENDIF.
          ENDIF.

        ENDIF.  " READ TABLE ls_prod_data-zarn_uom_variant[] INTO ls_uom_variant


* Get vendor from GLN
        CLEAR: ls_pir_vendor, lv_count.
        LOOP AT lt_pir_vendor[] INTO ls_pir_vendor
        WHERE bbbnr = ls_pir-gln(7)
           AND            bbsnr = ls_pir-gln+7(5)
           AND            bubkz = ls_pir-gln+12(1).
          lv_count = lv_count + 1.
        ENDLOOP.

        IF lv_count GT 1.
* multiple Vendors
          ls_reg_pir_ui-lifnr = 'MULTIPLE'.

        ELSEIF lv_count EQ 1.
* single Vendors
          ls_reg_pir_ui-lifnr = ls_pir_vendor-lifnr.

* Vendor master record purchasing organization data
          CLEAR ls_lfm1.
          READ TABLE lt_lfm1[] INTO ls_lfm1
          WITH TABLE KEY lifnr = ls_reg_pir_ui-lifnr
                         ekorg = '9999'.
          IF sy-subrc = 0.
            ls_reg_pir_ui-ekgrp = ls_lfm1-ekgrp.
            ls_reg_pir_ui-aplfz = ls_lfm1-plifz.
          ENDIF.
        ENDIF. " IF lv_count GT 1




        IF ls_pir-variable_unit = abap_true.
          ls_reg_pir_ui-vabme = 1.
        ENDIF.


* Get Pricing Data from national
        CLEAR ls_list_price.
        READ TABLE gt_arn_list_price INTO ls_list_price
             WITH KEY idno     = ls_pir-idno
                      version  = ls_pir-version
                      uom_code = ls_pir-uom_code
                      gln      = ls_pir-gln
                      active_lp = abap_true.
        IF sy-subrc = 0.
          ls_reg_pir_ui-waers = ls_list_price-currency.
          ls_reg_pir_ui-netpr = ls_list_price-price_value.
          ls_reg_pir_ui-peinh = ls_list_price-quantity.
          ls_reg_pir_ui-datlb = ls_list_price-eff_start_date.
          ls_reg_pir_ui-prdat = ls_list_price-eff_end_date.
        ENDIF.  " READ TABLE ls_prod_data-zarn_list_price[] INTO ls_list_price


* Get Order Unit from already defaulted REG_UOM
        CLEAR ls_reg_uom_ui.
        READ TABLE lt_reg_uom_ui INTO ls_reg_uom_ui
        WITH KEY idno                 = ls_pir-idno
                 pim_uom_code         = ls_reg_pir_ui-order_uom_pim
                 hybris_internal_code = space
                 lower_uom            = ls_reg_pir_ui-lower_uom
                 lower_child_uom      = ls_reg_pir_ui-lower_child_uom.     "++INC5599313 JKH 03.02.2017
        IF sy-subrc = 0.
          ls_reg_pir_ui-bprme                = ls_reg_uom_ui-meinh.
          ls_reg_pir_ui-meinh                = ls_reg_uom_ui-meinh.
          ls_reg_pir_ui-lower_meinh          = ls_reg_uom_ui-lower_meinh.
          ls_reg_pir_ui-num_base_units       = ls_reg_uom_ui-num_base_units.
          ls_reg_pir_ui-factor_of_base_units = ls_reg_uom_ui-factor_of_base_units.
          ls_reg_pir_ui-unit_purord          = ls_reg_uom_ui-unit_purord.
          ls_reg_pir_ui-cat_seqno            = ls_reg_uom_ui-cat_seqno.
        ENDIF.

        ls_reg_pir_ui-bbbnr = ls_pir-gln(7).
        ls_reg_pir_ui-bbsnr = ls_pir-gln+7(5).
        ls_reg_pir_ui-bubkz = ls_pir-gln+12(1).


* Check if REG_PIR already exist in Regional DB then dont default that row, use from DB
        CLEAR ls_reg_pir.
        READ TABLE gt_arn_reg_pir_in_db INTO ls_reg_pir
        WITH KEY idno                 = ls_reg_pir_ui-idno
                 order_uom_pim        = ls_reg_pir_ui-order_uom_pim
                 hybris_internal_code = ls_reg_pir_ui-hybris_internal_code.
        IF sy-subrc = 0.

          ls_reg_pir-gln_no   = ls_reg_pir_ui-gln_no.
          ls_reg_pir-gln_name = ls_reg_pir_ui-gln_name.
          ls_reg_pir-waers    = ls_reg_pir_ui-waers.
          ls_reg_pir-netpr    = ls_reg_pir_ui-netpr.
          ls_reg_pir-peinh    = ls_reg_pir_ui-peinh.
          ls_reg_pir-datlb    = ls_reg_pir_ui-datlb.
          ls_reg_pir-prdat    = ls_reg_pir_ui-prdat.
          ls_reg_pir-bprme    = ls_reg_pir_ui-bprme.
          ls_reg_pir-lifab    = ls_reg_pir_ui-lifab.
          ls_reg_pir-lifbi    = ls_reg_pir_ui-lifbi.
          ls_reg_pir-lifnr    = ls_reg_pir_ui-lifnr.
          ls_reg_pir-idnlf    = ls_reg_pir_ui-idnlf.

          IF ls_reg_pir-lifab IS NOT INITIAL AND
             ls_reg_pir-lifbi IS INITIAL.
            ls_reg_pir-lifbi = '99991231'.
          ENDIF.

          ls_reg_pir-lower_uom       = ls_reg_pir_ui-lower_uom.          "++INC5599313 JKH 03.02.2017
          ls_reg_pir-lower_child_uom = ls_reg_pir_ui-lower_child_uom.    "++INC5599313 JKH 03.02.2017

          MOVE-CORRESPONDING ls_reg_pir TO ls_reg_pir_ui.

          CLEAR ls_reg_pir_temp.
          ls_reg_pir_temp-include = ls_reg_pir_ui.
          ls_reg_pir_temp-matnr = ls_pir_not_found-matnr.

*        MOVE-CORRESPONDING ls_reg_pir_ui TO ls_reg_pir_temp.
          INSERT ls_reg_pir_temp INTO TABLE lt_reg_pir_temp.

          INSERT ls_reg_pir_ui INTO TABLE lt_reg_pir_ui.
        ELSE.

          CLEAR ls_reg_pir_temp.
          ls_reg_pir_temp-include = ls_reg_pir_ui.
          ls_reg_pir_temp-matnr = ls_pir_not_found-matnr.

*        MOVE-CORRESPONDING ls_reg_pir_ui TO ls_reg_pir_temp.
          INSERT ls_reg_pir_temp INTO TABLE lt_reg_pir_temp.

          INSERT ls_reg_pir_ui INTO TABLE lt_reg_pir_ui.

*        APPEND ls_reg_pir_ui TO lt_reg_pir_new[].
        ENDIF.



      ENDIF.  " READ TABLE gt_arn_reg_pir_in_db_old INTO ls_arn_reg_pir_in_db



    ENDIF.  " READ TABLE gt_arn_pir INTO ls_pir

  ENDLOOP.  "   LOOP AT gt_pir_not_found_all INTO ls_pir_not_found


  LOOP AT lt_reg_pir_temp ASSIGNING <ls_reg_pir_temp>.

* If record exist in AReNa then build data using migrate version,
* else using BAU version
    CLEAR ls_arn_reg_pir_in_db.
    READ TABLE gt_arn_reg_pir_in_db_old INTO ls_arn_reg_pir_in_db
    WITH TABLE KEY idno                 = <ls_reg_pir_temp>-include-idno
                   order_uom_pim        = <ls_reg_pir_temp>-include-order_uom_pim
                   hybris_internal_code = <ls_reg_pir_temp>-include-hybris_internal_code.
    IF sy-subrc = 0.

      <ls_reg_pir_temp>-include-lifab = ls_arn_reg_pir_in_db-lifab.
      <ls_reg_pir_temp>-include-lifbi = ls_arn_reg_pir_in_db-lifbi.
      <ls_reg_pir_temp>-include-vabme = ls_arn_reg_pir_in_db-vabme.
      <ls_reg_pir_temp>-include-ekgrp = ls_arn_reg_pir_in_db-ekgrp.
      <ls_reg_pir_temp>-include-minbm = ls_arn_reg_pir_in_db-minbm.
      <ls_reg_pir_temp>-include-norbm = ls_arn_reg_pir_in_db-norbm.
      <ls_reg_pir_temp>-include-aplfz = ls_arn_reg_pir_in_db-aplfz.
      <ls_reg_pir_temp>-include-ekkol = ls_arn_reg_pir_in_db-ekkol.
      <ls_reg_pir_temp>-include-mwskz = ls_arn_reg_pir_in_db-mwskz.
      <ls_reg_pir_temp>-include-uebto = ls_arn_reg_pir_in_db-uebto.
      <ls_reg_pir_temp>-include-untto = ls_arn_reg_pir_in_db-untto.


      CLEAR: ls_eina, ls_eine.
*    IF sy-subrc EQ 0.
      " Read the VENDOR from the EINA and update the regional table field for the vendor
      READ TABLE gt_eina INTO ls_eina
        WITH KEY matnr = <ls_reg_pir_temp>-matnr lifnr = <ls_reg_pir_temp>-include-lifnr.
      "meins = <ls_reg_pir_temp>-include-bprme.

      IF sy-subrc EQ 0.

        <ls_reg_pir_temp>-include-lifnr = ls_eina-lifnr.

        READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
        WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'LIFAB'.
        IF sy-subrc EQ 0.
          <ls_reg_pir_temp>-include-lifab = ls_eina-lifab.
*        ELSE.
*          <ls_reg_pir_temp>-include-lifab = ls_arn_reg_pir_in_db-lifab.
        ENDIF.

        READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
        WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'LIFBI'.
        IF sy-subrc EQ 0.
          <ls_reg_pir_temp>-include-lifbi = ls_eina-lifbi.
*        ELSE.
*          <ls_reg_pir_temp>-include-lifbi = ls_arn_reg_pir_in_db-lifbi.
        ENDIF.

        <ls_reg_pir_temp>-include-idnlf = ls_eina-idnlf.


*      CLEAR lv_no_of_rec.
*      " Read eine
*      READ TABLE gt_eine INTO ls_eine WITH TABLE KEY infnr = ls_eina-infnr.
*      IF sy-subrc EQ 0.
*        IF ls_eine-bprme EQ ls_eina-meins.
*          lv_no_of_rec = 1.
*        ELSE.
*          lv_no_of_rec = 2.
*        ENDIF.
*      ENDIF.


*    DO lv_no_of_rec TIMES.

        CLEAR ls_arn_reg_uom.
        READ TABLE gt_arn_reg_uom_all INTO ls_arn_reg_uom
          WITH KEY idno = <ls_reg_pir_temp>-include-idno pim_uom_code = <ls_reg_pir_temp>-include-order_uom_pim hybris_internal_code = ''
          lower_uom = <ls_reg_pir_temp>-include-lower_uom lower_child_uom = <ls_reg_pir_temp>-include-lower_child_uom.
        IF sy-subrc EQ 0.
          <ls_reg_pir_temp>-include-bprme = ls_arn_reg_uom-meinh.
        ENDIF.

        " if sy-index = 1 -> create primary keys from EINA-MEINS
        " if sy-index = 2 -> create primary keys from EINE-BPRME
*      IF sy-index EQ 1.
*        lv_eina_key = abap_true.
*      ELSE.
*        lv_eina_key = abap_false.
*      ENDIF.

**  EINA-VABME  for the EINA-INFNR found in "SAP-PIR"


**  PIR_REL_EINA => If  EINA-MEINS = ZARN_REG_UOM-MEINH
**  where ZARN_LIST_PRICE-UOM_CODE = ZARN_REG_UOM-PIM_UOM_CODE then ""X"" else "" """
*      IF lv_eina_key EQ abap_true.
        IF <ls_reg_pir_temp>-include-bprme EQ ls_eina-meins. "ls_arn_reg_uom-meinh EQ ls_eina-meins.
          IF <ls_reg_pir_temp>-include-lifnr NE 'MULTIPLE' AND <ls_reg_pir_temp>-include-lifnr NE ''.


            <ls_reg_pir_temp>-include-pir_rel_eina = abap_true.

**  EINA-RELIF   for the EINA-INFNR found in "SAP-PIR"
            <ls_reg_pir_temp>-include-relif = ls_eina-relif.
          ELSE.
            CLEAR: <ls_reg_pir_temp>-include-pir_rel_eina, <ls_reg_pir_temp>-include-relif.
          ENDIF.

          READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'VABME'.
          IF sy-subrc EQ 0.
            <ls_reg_pir_temp>-include-vabme = ls_eina-vabme.
*          ELSE.
*            <ls_reg_pir_temp>-include-vabme = ls_arn_reg_pir_in_db-vabme.
          ENDIF.

        ELSE.
          CLEAR: <ls_reg_pir_temp>-include-pir_rel_eina, <ls_reg_pir_temp>-include-relif.
        ENDIF.
*      ELSE.
*        CLEAR: <ls_reg_pir_temp>-include-pir_rel_eina, <ls_reg_pir_temp>-include-relif.
*      ENDIF.

        CLEAR ls_eine.
        READ TABLE gt_eine INTO ls_eine WITH TABLE KEY infnr = ls_eina-infnr.
        IF sy-subrc EQ 0.

**  ESOKZ where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
          <ls_reg_pir_temp>-include-esokz = ls_eine-esokz.


**  PIR_REL_EINE => If EINE-BPRME = ZARN_REG_UOM-MEINH where ZARN_LIST_PRICE-UOM_CODE = ZARN_REG_UOM-PIM_UOM_CODE and
**                  EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS="" then "X" else " "
*        IF lv_eina_key EQ abap_false.
*          CLEAR ls_arn_reg_uom.
*          READ TABLE gt_arn_reg_uom_all INTO ls_arn_reg_uom
*            WITH KEY idno = <ls_reg_pir_temp>-include-idno pim_uom_code = <ls_reg_pir_temp>-include-order_uom_pim hybris_internal_code = ''.
*          IF sy-subrc EQ 0 AND
          IF <ls_reg_pir_temp>-include-bprme EQ ls_eine-bprme. ""ls_arn_reg_uom-meinh EQ ls_eine-bprme.

            IF <ls_reg_pir_temp>-include-lifnr NE 'MULTIPLE' AND <ls_reg_pir_temp>-include-lifnr NE ''.
              <ls_reg_pir_temp>-include-pir_rel_eine = abap_true.
            ELSE.
              CLEAR <ls_reg_pir_temp>-include-pir_rel_eine.
            ENDIF.
*        ELSE.
*          CLEAR <ls_reg_pir_temp>-include-pir_rel_eine.
*        ENDIF.

            READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'EKGRP'.
            IF sy-subrc EQ 0.
**  EKGRP where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
              <ls_reg_pir_temp>-include-ekgrp = ls_eine-ekgrp.
*            ELSE.
*              <ls_reg_pir_temp>-include-ekgrp = ls_arn_reg_pir_in_db-ekgrp.
            ENDIF.

            READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'MINBM'.
            IF sy-subrc EQ 0.
**    MINBM where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
              <ls_reg_pir_temp>-include-minbm = ls_eine-minbm.
*            ELSE.
*              <ls_reg_pir_temp>-include-minbm = ls_arn_reg_pir_in_db-minbm.
            ENDIF.

            READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'NORBM'.
            IF sy-subrc EQ 0.
**  NORBM where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
              <ls_reg_pir_temp>-include-norbm = ls_eine-norbm.
*            ELSE.
*              <ls_reg_pir_temp>-include-norbm = ls_arn_reg_pir_in_db-norbm.
            ENDIF.

            READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'APLFZ'.
            IF sy-subrc EQ 0.
**  APLFZ where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
              <ls_reg_pir_temp>-include-aplfz = ls_eine-aplfz.
*            ELSE.
*              <ls_reg_pir_temp>-include-aplfz = ls_arn_reg_pir_in_db-aplfz.
            ENDIF.

            READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'EKKOL'.
            IF sy-subrc EQ 0.
**  EKKOL where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
              <ls_reg_pir_temp>-include-ekkol = ls_eine-ekkol.
*            ELSE.
*              <ls_reg_pir_temp>-include-ekkol = ls_arn_reg_pir_in_db-ekkol.
            ENDIF.

            READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'MWSKZ'.
            IF sy-subrc EQ 0.
**  MWSKZ where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
              <ls_reg_pir_temp>-include-mwskz = ls_eine-mwskz.
*            ELSE.
*              <ls_reg_pir_temp>-include-mwskz = ls_arn_reg_pir_in_db-mwskz.
            ENDIF.

**  LOEKZ where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
            <ls_reg_pir_temp>-include-loekz = ls_eine-loekz.


            READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
              WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'UEBTO'.
            IF sy-subrc EQ 0.
**  UEBTO where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
              <ls_reg_pir_temp>-include-uebto = ls_eine-uebto.
*            ELSE.
*              <ls_reg_pir_temp>-include-uebto = ls_arn_reg_pir_in_db-uebto.
            ENDIF.

            READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
              WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'UNTTO'.
            IF sy-subrc EQ 0.
**  UNTTO where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
              <ls_reg_pir_temp>-include-untto = ls_eine-untto.
*            ELSE.
*              <ls_reg_pir_temp>-include-untto = ls_arn_reg_pir_in_db-untto.
            ENDIF.

          ELSE.


            CLEAR <ls_reg_pir_temp>-include-pir_rel_eine.



            READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'MINBM'.
            IF sy-subrc EQ 0.
              <ls_reg_pir_temp>-include-minbm = 1.
*            ELSE.
*              <ls_reg_pir_temp>-include-minbm = ls_arn_reg_pir_in_db-minbm.
            ENDIF.

            READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'NORBM'.
            IF sy-subrc EQ 0.
              <ls_reg_pir_temp>-include-norbm = 1.
*            ELSE.
*              <ls_reg_pir_temp>-include-norbm = ls_arn_reg_pir_in_db-norbm.
            ENDIF.

            READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
            WITH TABLE KEY tabname = 'ZARN_REG_PIR' fldname = 'MWSKZ'.
            IF sy-subrc EQ 0.
              <ls_reg_pir_temp>-include-mwskz = 'P1'.
*            ELSE.
*              <ls_reg_pir_temp>-include-mwskz = ls_arn_reg_pir_in_db-mwskz.
            ENDIF.

          ENDIF.

**  "X" if NETPR = 0 where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
          IF ls_eine-netpr EQ 0.
            <ls_reg_pir_temp>-include-allow_zero_price = abap_true.
          ENDIF.

        ENDIF. " EINE READ

*    ENDDO.

      ENDIF.


    ELSE.  " READ TABLE gt_arn_reg_pir_in_db_old INTO ls_arn_reg_pir_in_db



      CLEAR: ls_eina, ls_eine.
*    IF sy-subrc EQ 0.
      " Read the VENDOR from the EINA and update the regional table field for the vendor
      READ TABLE gt_eina INTO ls_eina
        WITH KEY matnr = <ls_reg_pir_temp>-matnr lifnr = <ls_reg_pir_temp>-include-lifnr.
      "meins = <ls_reg_pir_temp>-include-bprme.

      IF sy-subrc EQ 0.
        <ls_reg_pir_temp>-include-lifnr = ls_eina-lifnr.
        <ls_reg_pir_temp>-include-lifab = ls_eina-lifab.
        <ls_reg_pir_temp>-include-lifbi = ls_eina-lifbi.
        <ls_reg_pir_temp>-include-idnlf = ls_eina-idnlf.

*      CLEAR lv_no_of_rec.
*      " Read eine
*      READ TABLE gt_eine INTO ls_eine WITH TABLE KEY infnr = ls_eina-infnr.
*      IF sy-subrc EQ 0.
*        IF ls_eine-bprme EQ ls_eina-meins.
*          lv_no_of_rec = 1.
*        ELSE.
*          lv_no_of_rec = 2.
*        ENDIF.
*      ENDIF.


*    DO lv_no_of_rec TIMES.

        CLEAR ls_arn_reg_uom.
        READ TABLE gt_arn_reg_uom_all INTO ls_arn_reg_uom
          WITH KEY idno = <ls_reg_pir_temp>-include-idno pim_uom_code = <ls_reg_pir_temp>-include-order_uom_pim hybris_internal_code = ''
          lower_uom = <ls_reg_pir_temp>-include-lower_uom lower_child_uom = <ls_reg_pir_temp>-include-lower_child_uom.
        IF sy-subrc EQ 0.
          <ls_reg_pir_temp>-include-bprme = ls_arn_reg_uom-meinh.
        ENDIF.

        " if sy-index = 1 -> create primary keys from EINA-MEINS
        " if sy-index = 2 -> create primary keys from EINE-BPRME
*      IF sy-index EQ 1.
*        lv_eina_key = abap_true.
*      ELSE.
*        lv_eina_key = abap_false.
*      ENDIF.

**  EINA-VABME  for the EINA-INFNR found in "SAP-PIR"


**  PIR_REL_EINA => If  EINA-MEINS = ZARN_REG_UOM-MEINH
**  where ZARN_LIST_PRICE-UOM_CODE = ZARN_REG_UOM-PIM_UOM_CODE then ""X"" else "" """
*      IF lv_eina_key EQ abap_true.
        IF <ls_reg_pir_temp>-include-bprme EQ ls_eina-meins. "ls_arn_reg_uom-meinh EQ ls_eina-meins.
          IF <ls_reg_pir_temp>-include-lifnr NE 'MULTIPLE' AND <ls_reg_pir_temp>-include-lifnr NE ''.
            <ls_reg_pir_temp>-include-pir_rel_eina = abap_true.
            <ls_reg_pir_temp>-include-relif = ls_eina-relif.
          ELSE.
            CLEAR: <ls_reg_pir_temp>-include-pir_rel_eina, <ls_reg_pir_temp>-include-relif.
          ENDIF.

**  EINA-RELIF   for the EINA-INFNR found in "SAP-PIR"

          <ls_reg_pir_temp>-include-vabme = ls_eina-vabme.
        ELSE.
          CLEAR: <ls_reg_pir_temp>-include-pir_rel_eina, <ls_reg_pir_temp>-include-relif.
        ENDIF.
*      ELSE.
*        CLEAR: <ls_reg_pir_temp>-include-pir_rel_eina, <ls_reg_pir_temp>-include-relif.
*      ENDIF.

        CLEAR ls_eine.
        READ TABLE gt_eine INTO ls_eine WITH TABLE KEY infnr = ls_eina-infnr.
        IF sy-subrc EQ 0.

**  ESOKZ where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
          <ls_reg_pir_temp>-include-esokz = ls_eine-esokz.

**  PIR_REL_EINE => If EINE-BPRME = ZARN_REG_UOM-MEINH where ZARN_LIST_PRICE-UOM_CODE = ZARN_REG_UOM-PIM_UOM_CODE and
**                  EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS="" then "X" else " "
*        IF lv_eina_key EQ abap_false.
*          CLEAR ls_arn_reg_uom.
*          READ TABLE gt_arn_reg_uom_all INTO ls_arn_reg_uom
*            WITH KEY idno = <ls_reg_pir_temp>-include-idno pim_uom_code = <ls_reg_pir_temp>-include-order_uom_pim hybris_internal_code = ''.
*          IF sy-subrc EQ 0 AND
          IF <ls_reg_pir_temp>-include-bprme EQ ls_eine-bprme. ""ls_arn_reg_uom-meinh EQ ls_eine-bprme.
            IF <ls_reg_pir_temp>-include-lifnr NE 'MULTIPLE' AND <ls_reg_pir_temp>-include-lifnr NE ''.
              <ls_reg_pir_temp>-include-pir_rel_eine = abap_true.
            ELSE.
              CLEAR <ls_reg_pir_temp>-include-pir_rel_eine.
            ENDIF.
*        ELSE.
*          CLEAR <ls_reg_pir_temp>-include-pir_rel_eine.
*        ENDIF.

**  EKGRP where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
            <ls_reg_pir_temp>-include-ekgrp = ls_eine-ekgrp.

**    MINBM where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
            <ls_reg_pir_temp>-include-minbm = ls_eine-minbm.

**  NORBM where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
            <ls_reg_pir_temp>-include-norbm = ls_eine-norbm.

**  APLFZ where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
            <ls_reg_pir_temp>-include-aplfz = ls_eine-aplfz.

**  EKKOL where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
            <ls_reg_pir_temp>-include-ekkol = ls_eine-ekkol.

**  MWSKZ where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
            <ls_reg_pir_temp>-include-mwskz = ls_eine-mwskz.

**  LOEKZ where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
            <ls_reg_pir_temp>-include-loekz = ls_eine-loekz.

**  UEBTO where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
            <ls_reg_pir_temp>-include-uebto = ls_eine-uebto.

**  UNTTO where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
            <ls_reg_pir_temp>-include-untto = ls_eine-untto.

          ELSE.

            CLEAR <ls_reg_pir_temp>-include-pir_rel_eine.

            <ls_reg_pir_temp>-include-minbm = 1.
            <ls_reg_pir_temp>-include-norbm = 1.
            <ls_reg_pir_temp>-include-mwskz = 'P1'.
          ENDIF.

**  "X" if NETPR = 0 where EINE-INFNR= EINA-INFNR found in "SAP-PIR" & EINE-EKORG=9999 & EINE-WERKS=""
          IF ls_eine-netpr EQ 0.
            <ls_reg_pir_temp>-include-allow_zero_price = abap_true.
          ENDIF.

        ENDIF. " EINE READ

*    ENDDO.

      ENDIF.  " READ TABLE gt_eina INTO ls_eina


    ENDIF.  " READ TABLE gt_arn_reg_pir_in_db_old INTO ls_arn_reg_pir_in_db


  ENDLOOP.  " LOOP AT lt_reg_pir_temp ASSIGNING <ls_reg_pir_temp>



  IF lt_reg_pir_temp[] IS NOT INITIAL.
    LOOP AT lt_reg_pir_temp INTO ls_reg_pir_temp.
      CLEAR ls_reg_pir.
      ls_reg_pir = ls_reg_pir_temp-include.
*      MOVE-CORRESPONDING ls_reg_pir_temp TO ls_reg_pir.
      INSERT ls_reg_pir INTO TABLE gt_arn_reg_pir_all[].
    ENDLOOP.
  ENDIF.

  CLEAR gt_arn_reg_pir_in_db[].


ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  GET_DATA_TO_UPDATE_BAU
*&---------------------------------------------------------------------*
* Get existing data from Arena regional tables which needs to be updated
*----------------------------------------------------------------------*
FORM get_data_to_update_bau .


  DATA:
    ls_arn_reg_hdr    TYPE zarn_reg_hdr,
    ls_arn_reg_banner TYPE zarn_reg_banner,
    ls_arn_reg_uom    TYPE zarn_reg_uom,
    ls_arn_reg_ean    TYPE zarn_reg_ean,
    ls_arn_reg_pir    TYPE zarn_reg_pir,
    ls_arn_ver_status TYPE zarn_ver_status,
    ls_arn_reg_hsno   TYPE zarn_reg_hsno,                                     " ++3169 JKH 20.10.2016
    ls_arn_sync       TYPE zarn_sync.

  FIELD-SYMBOLS:
    <fs_arn_reg_hdr_in_db>    LIKE LINE OF gt_arn_reg_hdr_in_db,
    <fs_arn_reg_banner_in_db> LIKE LINE OF gt_arn_reg_banner_in_db,
    <fs_arn_reg_uom_in_db>    LIKE LINE OF gt_arn_reg_uom_in_db,
    <fs_arn_reg_ean_in_db>    LIKE LINE OF gt_arn_reg_ean_in_db,
    <fs_arn_reg_pir_in_db>    LIKE LINE OF gt_arn_reg_pir_in_db,
    <fs_arn_reg_hsno_in_db>   LIKE LINE OF gt_arn_reg_hsno_in_db,             " ++3169 JKH 20.10.2016
    <fs_arn_ver_status_in_db> LIKE LINE OF gt_arn_ver_status_in_db,
    <fs_value_new>            TYPE any,
    <fs_value_old>            TYPE any.



  " Regional BANNER update
  IF gt_arn_reg_banner_in_db_old[] IS NOT INITIAL.
    LOOP AT gt_arn_reg_banner_in_db_old INTO ls_arn_reg_banner.

      READ TABLE gt_arn_reg_banner_in_db ASSIGNING <fs_arn_reg_banner_in_db>
        WITH TABLE KEY idno   = ls_arn_reg_banner-idno
                       banner = ls_arn_reg_banner-banner.
      IF sy-subrc NE 0.
        INSERT ls_arn_reg_banner INTO TABLE gt_arn_reg_banner_in_db.
      ENDIF.
    ENDLOOP.
  ENDIF.

*  " Regional UOM update
*  IF gt_arn_reg_uom_in_db_old[] IS NOT INITIAL.
*    LOOP AT gt_arn_reg_uom_in_db_old INTO ls_arn_reg_uom.
*
*      READ TABLE gt_arn_reg_uom_in_db ASSIGNING <fs_arn_reg_uom_in_db>
*        WITH TABLE KEY idno                 = ls_arn_reg_uom-idno
*                       uom_category         = ls_arn_reg_uom-uom_category
*                       pim_uom_code         = ls_arn_reg_uom-pim_uom_code
*                       hybris_internal_code = ls_arn_reg_uom-hybris_internal_code
*                       lower_uom            = ls_arn_reg_uom-lower_uom
*                       lower_child_uom      = ls_arn_reg_uom-lower_child_uom.
*      IF sy-subrc NE 0.
*        INSERT ls_arn_reg_uom INTO TABLE gt_arn_reg_uom_in_db.
*      ENDIF.
*    ENDLOOP.
*  ENDIF.

  " Regional EAN update
  IF gt_arn_reg_ean_in_db_old[] IS NOT INITIAL.
    LOOP AT gt_arn_reg_ean_in_db_old INTO ls_arn_reg_ean.

      READ TABLE gt_arn_reg_ean_in_db ASSIGNING <fs_arn_reg_ean_in_db>
        WITH TABLE KEY idno  = ls_arn_reg_ean-idno
                       meinh = ls_arn_reg_ean-meinh
                       ean11 = ls_arn_reg_ean-ean11.
      IF sy-subrc NE 0.
        INSERT ls_arn_reg_ean INTO TABLE gt_arn_reg_ean_in_db.
      ENDIF.
    ENDLOOP.
  ENDIF.

  " Regional PIR update
  IF gt_arn_reg_pir_in_db_old[] IS NOT INITIAL.
    LOOP AT gt_arn_reg_pir_in_db_old INTO ls_arn_reg_pir.

      READ TABLE gt_arn_reg_pir_in_db ASSIGNING <fs_arn_reg_pir_in_db>
        WITH TABLE KEY idno                 = ls_arn_reg_pir-idno
                       order_uom_pim        = ls_arn_reg_pir-order_uom_pim
                       hybris_internal_code = ls_arn_reg_pir-hybris_internal_code.
      IF sy-subrc NE 0.
        INSERT ls_arn_reg_pir INTO TABLE gt_arn_reg_pir_in_db.
      ENDIF.
    ENDLOOP.
  ENDIF.

  " Regional HSNO update
  IF gt_arn_reg_hsno_in_db_old[] IS NOT INITIAL.
    LOOP AT gt_arn_reg_hsno_in_db_old INTO ls_arn_reg_hsno.

      READ TABLE gt_arn_reg_hsno_in_db ASSIGNING <fs_arn_reg_hsno_in_db>
        WITH TABLE KEY idno      = ls_arn_reg_hsno-idno
                       hsno_code = ls_arn_reg_hsno-hsno_code.
      IF sy-subrc NE 0.
        INSERT ls_arn_reg_hsno INTO TABLE gt_arn_reg_hsno_in_db.
      ENDIF.
    ENDLOOP.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  FILL_REG_UOM_OBSOLETE_BAU
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
" fill obsolete UOMS, already existing UOMs which are not in National anymore
" or Local Uoms which are added by user and may be posted
*----------------------------------------------------------------------*
FORM fill_reg_uom_obsolete_bau USING fi_s_mara        TYPE mara
                                     fi_v_version     TYPE zarn_version
                                     fi_v_idno        TYPE zarn_idno.

  DATA: ls_arn_reg_uom    TYPE zarn_reg_uom,
        ls_reg_uom_tmp_ui TYPE zsarn_reg_uom_ui,
        ls_uom_cat        LIKE LINE OF gt_uom_cat,
        ls_marm           LIKE LINE OF gt_marm,
        ls_maw1           LIKE LINE OF gt_maw1.


  READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
  WITH KEY tabname = 'ZARN_REG_UOM'.
  IF sy-subrc NE 0.
    READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
    WITH KEY tabname = 'ZARN_REG_BANNER'
             fldname = 'VRKME'.
    IF sy-subrc NE 0.
      EXIT.
    ENDIF.
  ENDIF.

  CLEAR ls_maw1.
  READ TABLE gt_maw1 INTO ls_maw1 WITH TABLE KEY matnr = fi_s_mara-matnr.


  " Regional UOM update
  IF gt_arn_reg_uom_in_db[] IS NOT INITIAL.
    LOOP AT gt_arn_reg_uom_in_db INTO ls_arn_reg_uom
      WHERE idno = fi_v_idno.

      READ TABLE gt_arn_reg_uom_all[] TRANSPORTING NO FIELDS
        WITH KEY idno                 = ls_arn_reg_uom-idno
                 uom_category         = ls_arn_reg_uom-uom_category
                 pim_uom_code         = ls_arn_reg_uom-pim_uom_code
                 hybris_internal_code = ls_arn_reg_uom-hybris_internal_code
                 lower_uom            = ls_arn_reg_uom-lower_uom
                 lower_child_uom      = ls_arn_reg_uom-lower_child_uom.
      IF sy-subrc NE 0.


        CLEAR ls_marm.
        READ TABLE gt_marm INTO ls_marm
        WITH TABLE KEY matnr = fi_s_mara-matnr
                       meinh = ls_arn_reg_uom-meinh.


        CASE ls_arn_reg_uom-uom_category.
          WHEN 'RETAIL' OR 'INNER' OR 'SHIPER'.

            " Fill only if current unit is base unit
            IF ls_arn_reg_uom-meinh = fi_s_mara-meins.
              LOOP AT gt_arn_sync TRANSPORTING NO FIELDS
              WHERE tabname = 'ZARN_REG_UOM'
                AND ( fldname = 'NET_WEIGHT_UOM' OR fldname = 'NET_WEIGHT_VALUE' ).

                ls_arn_reg_uom-net_weight_value     = fi_s_mara-ntgew.           " Net Weight Value
                ls_arn_reg_uom-net_weight_uom       = fi_s_mara-gewei.            " Net Weight UOM

                EXIT.
              ENDLOOP.  " NET_WEIGHT_UOM/NET_WEIGHT_VALUE
            ENDIF.


            IF ls_marm IS NOT INITIAL.
              LOOP AT gt_arn_sync TRANSPORTING NO FIELDS
              WHERE tabname = 'ZARN_REG_UOM'
                AND ( fldname = 'GROSS_WEIGHT_UOM' OR
                      fldname = 'GROSS_WEIGHT_VALUE' OR
                      fldname = 'HEIGHT_UOM' OR
                      fldname = 'HEIGHT_VALUE' OR
                      fldname = 'WIDTH_UOM' OR
                      fldname = 'WIDTH_VALUE' OR
                      fldname = 'DEPTH_UOM' OR
                      fldname = 'DEPTH_VALUE' ).

                ls_arn_reg_uom-gross_weight_value   = ls_marm-brgew.                    " Gross Weight Value
                ls_arn_reg_uom-gross_weight_uom     = ls_marm-gewei.                    " Gross Weight UOM

                ls_arn_reg_uom-height_value         = ls_marm-hoehe.                    " Height Value
                ls_arn_reg_uom-height_uom           = ls_marm-meabm.                    " Height UOM

                ls_arn_reg_uom-width_value          = ls_marm-breit.                    " Width Value
                ls_arn_reg_uom-width_uom            = ls_marm-meabm.                    " Width UOM

                ls_arn_reg_uom-depth_value          = ls_marm-laeng.                    " Depth Value
                ls_arn_reg_uom-depth_uom            = ls_marm-meabm.                    " Depth UOM

                EXIT.
              ENDLOOP.
            ENDIF.

          WHEN 'LAYER' OR 'PALLET'.

            IF ls_marm IS NOT INITIAL.
              LOOP AT gt_arn_sync TRANSPORTING NO FIELDS
              WHERE tabname = 'ZARN_REG_UOM'
                AND ( fldname = 'GROSS_WEIGHT_UOM' OR
                      fldname = 'GROSS_WEIGHT_VALUE' OR
                      fldname = 'HEIGHT_UOM' OR
                      fldname = 'HEIGHT_VALUE' ).

                ls_arn_reg_uom-gross_weight_value   = ls_marm-brgew.                    " Gross Weight Value
                ls_arn_reg_uom-gross_weight_uom     = ls_marm-gewei.                    " Gross Weight UOM

                ls_arn_reg_uom-height_value         = ls_marm-hoehe.                    " Height Value
                ls_arn_reg_uom-height_uom           = ls_marm-meabm.                    " Height UOM

                EXIT.
              ENDLOOP.
            ENDIF.

        ENDCASE.



        READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname = 'ZARN_REG_UOM' fldname = 'UNIT_BASE'.
        IF sy-subrc EQ 0.
**  UNIT_BASE = "X" if above ZARN_REG_UOM-MEINH=MARA-MEINS
          CLEAR ls_arn_reg_uom-unit_base.
          IF ls_arn_reg_uom-meinh EQ fi_s_mara-meins.
            ls_arn_reg_uom-unit_base = abap_true.
          ENDIF.
        ENDIF.  " 'UNIT_BASE'


        READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname = 'ZARN_REG_UOM' fldname = 'UNIT_PURORD'.
        IF sy-subrc EQ 0.
          CLEAR ls_arn_reg_uom-unit_purord.
**  UNIT_PURORD = "X" if above ZARN_REG_UOM-MEINH=MARA-BSTME
**  OR If above ZARN_REG_UOM-MEINH=MARA-MEINS & if MARA-BSTME="" "" then ""X"""
          IF ( ls_arn_reg_uom-meinh EQ fi_s_mara-bstme AND fi_s_mara-bstme IS NOT INITIAL ) OR
            ( ls_arn_reg_uom-meinh EQ fi_s_mara-meins AND fi_s_mara-bstme IS INITIAL ).

            ls_arn_reg_uom-unit_purord = abap_true.
          ENDIF.
        ENDIF.  " 'UNIT_PURORD'



        LOOP AT gt_arn_sync TRANSPORTING NO FIELDS
        WHERE ( tabname = 'ZARN_REG_UOM' AND fldname = 'SALES_UNIT' )
           OR ( tabname = 'ZARN_REG_BANNER' AND fldname = 'VRKME' ).

**  SALES_UNIT = ""X"" if above ZARN_REG_UOM-MEINH=MAW1-WVRKM
          CLEAR: ls_arn_reg_uom-sales_unit.
          IF sy-subrc EQ 0 AND ls_maw1-wvrkm EQ ls_arn_reg_uom-meinh AND ls_maw1-wvrkm IS NOT INITIAL.
            ls_arn_reg_uom-sales_unit = abap_true.
**  If above ZARN_REG_UOM-MEINH=MARA-MEINS & if MAW1-WVRKM="" "" then ""X"""
          ELSEIF sy-subrc EQ 0 AND ls_maw1-wvrkm IS INITIAL AND ls_arn_reg_uom-meinh EQ fi_s_mara-meins.
            ls_arn_reg_uom-sales_unit = abap_true.
          ENDIF.

          EXIT.
        ENDLOOP.   " SALES_UNIT/VRKME

        READ TABLE gt_arn_sync TRANSPORTING NO FIELDS
          WITH TABLE KEY tabname = 'ZARN_REG_UOM' fldname = 'ISSUE_UNIT'.
        IF sy-subrc EQ 0.
**  ISSUE_UNIT = "X" if above ZARN_REG_UOM-MEINH=MAW1-WAUSM
          CLEAR: ls_arn_reg_uom-issue_unit.
          IF sy-subrc EQ 0 AND ls_maw1-wausm EQ ls_arn_reg_uom-meinh AND ls_maw1-wausm IS NOT INITIAL.
            ls_arn_reg_uom-issue_unit = abap_true.
**  If above ZARN_REG_UOM-MEINH=MARA-MEINS & if MAW1-WAUSM="" "" then ""X"""
          ELSEIF sy-subrc EQ 0 AND ls_maw1-wausm IS INITIAL AND ls_arn_reg_uom-meinh EQ fi_s_mara-meins.
            ls_arn_reg_uom-issue_unit = abap_true.
          ENDIF.
        ENDIF.  " 'ISSUE_UNIT'




        INSERT ls_arn_reg_uom INTO TABLE gt_arn_reg_uom_all[].

        CLEAR ls_reg_uom_tmp_ui.
        MOVE-CORRESPONDING ls_arn_reg_uom TO ls_reg_uom_tmp_ui.

        READ TABLE gt_uom_cat INTO ls_uom_cat WITH KEY uom = ls_arn_reg_uom-meinh.
        IF sy-subrc EQ 0.
          ls_reg_uom_tmp_ui-cat_seqno = ls_uom_cat-cat_seqno.
          ls_reg_uom_tmp_ui-seqno = ls_uom_cat-seqno.
        ENDIF.

        INSERT ls_reg_uom_tmp_ui INTO TABLE gt_arn_reg_uom_ui.


      ENDIF.
    ENDLOOP.  " LOOP AT gt_arn_reg_uom_in_db INTO ls_arn_reg_uom
  ENDIF.  "   IF gt_arn_reg_uom_in_db[] IS NOT INITIAL


ENDFORM.
