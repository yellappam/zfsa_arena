@AbapCatalog.sqlViewName: 'ZCARNDCSTCKRELDT'
@AbapCatalog.compiler.compareFilter: true
@AbapCatalog.preserveKey: true
@AccessControl.authorizationCheck: #CHECK
@EndUserText.label: 'DC stock release date'
define view ZCARN_DC_STOCK_REL_DATE as select from zarn_reg_sc as sc
 inner join zarn_reg_hdr as hdr on hdr.idno = sc.idno {
 
    key hdr.idno as idno,
    hdr.matnr as matnr,
    sc.dc_stk_rel_date as dc_stock_rel_date
    
}
