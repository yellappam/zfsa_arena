* regenerated at 24.05.2016 11:25:43 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_UOMCATEGORYTOP.              " Global Data
  INCLUDE LZARN_UOMCATEGORYUXX.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_UOMCATEGORYF...              " Subroutines
* INCLUDE LZARN_UOMCATEGORYO...              " PBO-Modules
* INCLUDE LZARN_UOMCATEGORYI...              " PAI-Modules
* INCLUDE LZARN_UOMCATEGORYE...              " Events
* INCLUDE LZARN_UOMCATEGORYP...              " Local class implement.
* INCLUDE LZARN_UOMCATEGORYT99.              " ABAP Unit tests
  INCLUDE LZARN_UOMCATEGORYF00                    . " subprograms
  INCLUDE LZARN_UOMCATEGORYI00                    . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
