*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 25.05.2016 at 12:13:10 by user C90001363
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_PRICETYPE_T................................*
DATA:  BEGIN OF STATUS_ZARN_PRICETYPE_T              .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_PRICETYPE_T              .
CONTROLS: TCTRL_ZARN_PRICETYPE_T
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_PRICETYPE_T              .
TABLES: ZARN_PRICETYPE_T               .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
