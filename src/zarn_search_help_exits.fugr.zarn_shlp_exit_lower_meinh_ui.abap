FUNCTION zarn_shlp_exit_lower_meinh_ui.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  TABLES
*"      SHLP_TAB TYPE  SHLP_DESCT
*"      RECORD_TAB STRUCTURE  SEAHLPRES
*"  CHANGING
*"     VALUE(SHLP) TYPE  SHLP_DESCR
*"     VALUE(CALLCONTROL) LIKE  DDSHF4CTRL STRUCTURE  DDSHF4CTRL
*"----------------------------------------------------------------------

  TYPES: BEGIN OF ty_s_records,
           uom_category TYPE zarn_uom_category,
           seqno        TYPE zarn_seqno,
           uom          TYPE meinh,
         END OF ty_s_records,
         ty_t_records TYPE STANDARD TABLE OF ty_s_records.


  DATA: lt_uomcategory TYPE STANDARD TABLE OF zarn_uomcategory,
        ls_uomcategory TYPE zarn_uomcategory,
        lt_uom_cat     TYPE STANDARD TABLE OF zarn_uom_cat,
        ls_uom_cat     TYPE zarn_uom_cat,
        lt_records     TYPE ty_t_records,
        ls_records     TYPE ty_s_records.



*"----------------------------------------------------------------------
* STEP SELONE  (Select one of the elementary searchhelps)
*"----------------------------------------------------------------------
* This step is only called for collective searchhelps. It may be used
* to reduce the amount of elementary searchhelps given in SHLP_TAB.
* The compound searchhelp is given in SHLP.
* If you do not change CALLCONTROL-STEP, the next step is the
* dialog, to select one of the elementary searchhelps.
* If you want to skip this dialog, you have to return the selected
* elementary searchhelp in SHLP and to change CALLCONTROL-STEP to
* either to 'PRESEL' or to 'SELECT'.
  IF callcontrol-step = 'SELONE'.
    EXIT.
  ENDIF.

*"----------------------------------------------------------------------
* STEP PRESEL  (Enter selection conditions)
*"----------------------------------------------------------------------
* This step allows you, to influence the selection conditions either
* before they are displayed or in order to skip the dialog completely.
* If you want to skip the dialog, you should change CALLCONTROL-STEP
* to 'SELECT'.
* Normaly only SHLP-SELOPT should be changed in this step.
  IF callcontrol-step = 'PRESEL'.
    EXIT.
  ENDIF.


*"----------------------------------------------------------------------
* STEP SELECT    (Select values)
*"----------------------------------------------------------------------
* This step may be used to overtake the data selection completely.
* To skip the standard seletion, you should return 'DISP' as following
* step in CALLCONTROL-STEP.
* Normally RECORD_TAB should be filled after this step.
* Standard function module F4UT_RESULTS_MAP may be very helpfull in this
* step.
  IF callcontrol-step = 'SELECT'.

*    CLEAR: lt_uomcategory[].
*    SELECT *
*    FROM zarn_uomcategory
*    INTO CORRESPONDING FIELDS OF TABLE lt_uomcategory[]
*    WHERE uom_category eq SHLP-INTERFACE[]
*

*    EXIT.

  ENDIF.


*"----------------------------------------------------------------------
* STEP DISP     (Display values)
*"----------------------------------------------------------------------
* This step is called, before the selected data is displayed.
* You can e.g. modify or reduce the data in RECORD_TAB
* according to the users authority.
* If you want to get the standard display dialog afterwards, you
* should not change CALLCONTROL-STEP.
* If you want to overtake the dialog on you own, you must return
* the following values in CALLCONTROL-STEP:
* - "RETURN" if one line was selected. The selected line must be
*   the only record left in RECORD_TAB. The corresponding fields of
*   this line are entered into the screen.
* - "EXIT" if the values request should be aborted
* - "PRESEL" if you want to return to the selection dialog
* Standard function modules F4UT_PARAMETER_VALUE_GET and
* F4UT_PARAMETER_RESULTS_PUT may be very helpfull in this step.
  IF callcontrol-step = 'DISP'.
*    DATA lt_interface TYPE DDSHIFACES.
    DATA ls_interface TYPE ddshiface.
    STATICS : lv_uom_cate TYPE zarn_uom_category,
              lv_seq_no   TYPE zarn_cat_seqno,
              lv_uom      TYPE meins.

    LOOP AT shlp-interface[] INTO ls_interface .
      IF ls_interface-shlpfield EQ 'UOM_CATEGORY' .
        lv_uom_cate = ls_interface-value.
      ENDIF.

      IF ls_interface-shlpfield EQ 'SEQNO' .
        lv_seq_no = ls_interface-value.
      ENDIF.

      IF ls_interface-shlpfield EQ 'MEINH' .
        lv_uom = ls_interface-value.
      ENDIF.
    ENDLOOP.

    CLEAR: lt_uomcategory[].
    SELECT *
    FROM zarn_uom_cat
    INTO TABLE lt_uom_cat
    WHERE uom_category EQ lv_uom_cate
      AND seqno LE lv_seq_no
      AND uom EQ lv_uom.

    IF sy-subrc EQ 0.
      LOOP AT lt_uom_cat INTO ls_uom_cat.
        record_tab+0(3)  = sy-mandt.
        record_tab+3(16) = ls_uom_cat.
        APPEND record_tab.
      ENDLOOP.

    ENDIF.

    SORT record_tab.
    DELETE ADJACENT DUPLICATES FROM record_tab.

    EXIT.
  ENDIF.



ENDFUNCTION.
