*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Functional Specification 3174
* DATE WRITTEN     # 07.03.2017
* SAP VERSION      # 740
* TYPE             # Function
* AUTHOR           # C90001363, Jitin Kharbanda
*-----------------------------------------------------------------------
* TITLE            # AReNa: Lean Search Hybris for article
* PURPOSE          # AReNa: Lean Search Hybris for article
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------
FUNCTION z_hybris_get_lean.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(HYBRIS_SEARCH_INPUT) TYPE
*"        ZPRODUCT_LEAN_SEARCH_REQUEST_F
*"  EXPORTING
*"     REFERENCE(HYBRIS_RESULTS) TYPE  ZPRODUCT_LEAN_SEARCH_RESPONS24
*"     REFERENCE(MESSAGES) TYPE  BAL_T_MSG
*"  EXCEPTIONS
*"      HYBRIS_NOT_AVAILABLE
*"----------------------------------------------------------------------



  DATA: lo_hybris     TYPE REF TO zco_product_search_basic_out,
        lo_root       TYPE REF TO cx_root,
        lo_payld_prot TYPE REF TO if_wsprotocol_payload,
        lv_msg        TYPE string,
        ls_search     TYPE zproduct_lean_search_request1,
        ls_results    TYPE zproduct_lean_search_respons24,
        ls_message    LIKE LINE OF messages.

  FIELD-SYMBOLS: <ls_product>     TYPE zproduct_lean_search_respons22.


  TRY.

      CLEAR ls_search.
      ls_search-product_lean_search_request-fsvendor_data_req = hybris_search_input.

      CREATE OBJECT lo_hybris.

      IF lo_hybris IS BOUND.

        IF ls_search-product_lean_search_request-fsvendor_data_req-controller[] IS NOT INITIAL.
* get payload protocol
          lo_payld_prot ?=  lo_hybris->get_protocol( if_wsprotocol=>payload ).

* set extended XML handling - enabled to read controller contents
          lo_payld_prot->set_extended_xml_handling( abap_true ).
        ENDIF.

        CALL METHOD lo_hybris->product_search_basic_out
          EXPORTING
            output = ls_search
          IMPORTING
            input  = ls_results.

        hybris_results = ls_results.

      ENDIF.


    CATCH cx_root INTO lo_root.
      lv_msg = lo_root->if_message~get_text( ).
      CLEAR ls_message.
      ls_message-msgid = 'ZARENA_MSG'.
      ls_message-msgty = 'E'.
      ls_message-msgno = '000'.
      PERFORM split_msg USING lv_msg CHANGING ls_message-msgv1 ls_message-msgv2 ls_message-msgv3 ls_message-msgv4.
      APPEND ls_message TO messages.
      PERFORM log_message USING lv_msg.

      WHILE NOT lo_root->previous IS INITIAL.
        lo_root = lo_root->previous.
        lv_msg = lo_root->if_message~get_text( ).
        CLEAR ls_message.
        ls_message-msgid = 'ZARENA_MSG'.
        ls_message-msgty = 'E'.
        ls_message-msgno = '000'.
        PERFORM split_msg USING lv_msg CHANGING ls_message-msgv1 ls_message-msgv2 ls_message-msgv3 ls_message-msgv4.
        APPEND ls_message TO messages.
        PERFORM log_message USING lv_msg.
      ENDWHILE.

      CLEAR ls_message.
      ls_message-msgid = 'ZARENA_MSG'.
      ls_message-msgty = 'S'.
      ls_message-msgno = '000'.
      ls_message-msgv1 = 'Continuing with local search'.
      APPEND ls_message TO messages.


      RAISE hybris_not_available.
  ENDTRY.


ENDFUNCTION.
