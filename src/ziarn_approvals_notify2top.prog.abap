*&---------------------------------------------------------------------*
*&  Include           ZIARN_APPROVALS_NOTIFY2TOP
*&---------------------------------------------------------------------*
REPORT zrarn_approvals_notify2.

TABLES: zarn_products.

INCLUDE ziarn_approvals_notify2cd1. "Class definition

DATA: go_approvals_notifier TYPE REF TO lcl_approvals_notify,
      gv_updated_on         TYPE zarn_approval-updated_timestamp.
