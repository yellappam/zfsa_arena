*&---------------------------------------------------------------------*
*&  Include           ZRARN_CHANGE_DOCUMENT_DISPS01
*&---------------------------------------------------------------------*


TABLES: cdhdr, zsarn_change_document_display.

SELECTION-SCREEN BEGIN OF BLOCK b1 WITH FRAME TITLE text-001.

*PARAMETERS: p_reg AS CHECKBOX DEFAULT abap_true MODIF ID chk USER-COMMAND chk.

PARAMETERS: p_reg RADIOBUTTON GROUP rad1 default 'X' USER-COMMAND chk.
SELECT-OPTIONS: s_idno  FOR zsarn_change_document_display-idno  NO INTERVALS.  " cdhdr-objectid NO INTERVALS,  ",

SELECTION-SCREEN SKIP.

*PARAMETERS: p_chg AS CHECKBOX MODIF ID chk USER-COMMAND chk.
PARAMETERS: p_chg RADIOBUTTON GROUP rad1.
SELECT-OPTIONS: s_chgid	 FOR zsarn_change_document_display-chgid NO INTERVALS.  " cdhdr-objectid NO INTERVALS,  ",
SELECT-OPTIONS: s_idnocc FOR zsarn_change_document_display-idno  NO INTERVALS.  " cdhdr-objectid NO INTERVALS,  ",

SELECTION-SCREEN SKIP.
SELECTION-SCREEN SKIP.

SELECT-OPTIONS: s_chg   FOR zsarn_change_document_display-chg_category,
                s_chgnr	FOR	cdhdr-changenr NO-EXTENSION,
                s_udate	FOR	cdhdr-udate    NO-EXTENSION,
                s_uname	FOR	cdhdr-username NO-EXTENSION NO INTERVALS DEFAULT sy-uname.
SELECTION-SCREEN END OF BLOCK b1.



INITIALIZATION.

  s_udate-sign   = 'I'.
  s_udate-option = 'BT'.
  s_udate-low    = sy-datum.
  s_udate-high   = sy-datum.
  APPEND s_udate.


AT SELECTION-SCREEN OUTPUT.
  LOOP AT SCREEN.
    IF screen-name CS 'S_IDNO'.
      IF p_reg = abap_true.
        screen-active = 1.
      ELSE.
        screen-active = 0.
      ENDIF.
    ENDIF.

    IF screen-name CS 'S_CHGID' OR screen-name CS 'S_IDNOCC'.
      IF p_chg = abap_true.
        screen-active = 1.
      ELSE.
        screen-active = 0.
      ENDIF.
    ENDIF.

    MODIFY SCREEN.
  ENDLOOP.

AT SELECTION-SCREEN.
  LOOP AT SCREEN.
    IF screen-name CS 'S_IDNO'.
      IF p_reg = abap_true.
        screen-active = 1.
      ELSE.
        screen-active = 0.
      ENDIF.
    ENDIF.

    IF screen-name CS 'S_CHGID'.
      IF p_chg = abap_true.
        screen-active = 1.
      ELSE.
        screen-active = 0.
      ENDIF.
    ENDIF.

    MODIFY SCREEN.
  ENDLOOP.
