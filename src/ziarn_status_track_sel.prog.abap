*&---------------------------------------------------------------------*
*&  Include           ZIARN_STATUS_TRACK_SEL
*&---------------------------------------------------------------------*

SELECTION-SCREEN BEGIN OF BLOCK b1 WITH FRAME TITLE text-001.
SELECT-OPTIONS:
     s_gtin1 FOR zarn_gtin_var-gtin_code,
     s_plu   FOR zarn_products-plu,
     s_matnr FOR zarn_products-matnr_ni,
     s_fan   FOR zarn_products-fan_id.
SELECTION-SCREEN END OF BLOCK b1.

SELECTION-SCREEN BEGIN OF BLOCK b2 WITH FRAME TITLE text-002.
SELECT-OPTIONS:
     s_vstat   FOR zarn_prd_version-version_status  NO INTERVALS,
     s_rstat   FOR zarn_prd_version-release_status  NO INTERVALS,
     s_adate   FOR zarn_products-avail_start_date,
     s_datein  FOR zarn_control-date_in.
SELECTION-SCREEN END OF BLOCK b2.

SELECTION-SCREEN BEGIN OF BLOCK b3 WITH FRAME TITLE text-003.
SELECT-OPTIONS:
     s_rcman  FOR zarn_reg_hdr-ret_zzcatman NO INTERVALS NO-DISPLAY,
     s_ccman  FOR zarn_reg_hdr-gil_zzcatman NO INTERVALS.
SELECTION-SCREEN END OF BLOCK b3.

SELECTION-SCREEN BEGIN OF BLOCK b4 WITH FRAME  TITLE text-006.
*Status Tracking - Exception report
*National Version (check box) + [parameter – greater than n days] Date of Entry
PARAMETERS:
  p_rbt_ex TYPE char1 RADIOBUTTON GROUP rpty DEFAULT 'X' USER-COMMAND rtyp.

SELECTION-SCREEN BEGIN OF BLOCK b7 WITH FRAME  TITLE text-007.
PARAMETERS:
   p_nver  TYPE xfeld,
   p_nvday TYPE zarn_e_duration.
*SELECTION-SCREEN SKIP.
*Regional change (check box) + [parameter – greater than n days] Date of Entry
PARAMETERS:
   p_rver  TYPE xfeld,
   p_rnvday TYPE zarn_e_duration.
SELECTION-SCREEN END OF BLOCK b7.

*SELECTION-SCREEN SKIP.
PARAMETERS p_rbt_ov TYPE char1 RADIOBUTTON GROUP rpty.

SELECTION-SCREEN BEGIN OF BLOCK b5 WITH FRAME TITLE text-005.
*Status Tracking - Overview report (radio buttons)
*In- flight
PARAMETERS:
   p_infl  TYPE char01 RADIOBUTTON GROUP rad1 DEFAULT 'X',
   p_hist  TYPE char01 RADIOBUTTON GROUP rad1.
*Historical
SELECTION-SCREEN END OF BLOCK b5.

SELECTION-SCREEN END OF BLOCK b4.

SELECTION-SCREEN BEGIN OF BLOCK b6 WITH FRAME TITLE text-008.
PARAMETERS:
   p_vari  TYPE disvariant-variant. " NO-DISPLAY .
SELECTION-SCREEN END OF BLOCK b6.

*---------------------------------------------------------------------*
*
*---------------------------------------------------------------------*
INITIALIZATION.

* is saved in 'def_layout'.
  IF gs_variant-report IS INITIAL.

    CLEAR gs_variant.
    gs_variant-report   = sy-repid.
    gs_variant-username = sy-uname.

    CALL FUNCTION 'LVC_VARIANT_DEFAULT_GET'
      EXPORTING
        i_save     = gc_save
      CHANGING
        cs_variant = gs_variant
      EXCEPTIONS
        not_found  = 2.
    IF sy-subrc = 2.
      RETURN.
    ELSE.
*     set name of layout on selection screen
      p_vari = gs_variant-variant.
    ENDIF.
  ENDIF.                             "default IS INITIAL
*---------------------------------------------------------------------*
*
*---------------------------------------------------------------------*
AT SELECTION-SCREEN.
* selection criteria check
  IF s_gtin1[]   IS INITIAL  AND
     s_plu[]     IS INITIAL  AND
     s_matnr[]   IS INITIAL  AND
     s_fan[]     IS INITIAL  AND
     s_rcman[]   IS INITIAL  AND
     s_ccman[]   IS INITIAL  AND
     s_vstat[]   IS INITIAL  AND
     s_rstat[]   IS INITIAL  AND
     s_adate[]   IS INITIAL  AND
     s_datein[]  IS INITIAL.

    MESSAGE e000 WITH text-200.  "reinstate later

  ENDIF.

AT SELECTION-SCREEN ON VALUE-REQUEST FOR p_vari.
  PERFORM alv_worklist_variant.

*---------------------------------------------------------------------*
*
*---------------------------------------------------------------------*
AT SELECTION-SCREEN OUTPUT.
  LOOP AT SCREEN.
    " exception report
    IF p_rbt_ex IS NOT INITIAL.
      IF screen-name EQ 'P_INFL' OR
        screen-name EQ 'P_HIST'.
        screen-input = 0.

        MODIFY SCREEN.
      ENDIF.

      " overview report
    ELSE.
      IF screen-name EQ 'P_NVER' OR
        screen-name EQ 'P_NVDAY' OR
        screen-name EQ 'P_RVER' OR
        screen-name EQ 'P_RNVDAY'.

        screen-input = 0.
        MODIFY SCREEN.
      ENDIF.
      CLEAR: p_nver, p_nvday, p_rver, p_rnvday.
    ENDIF.
  ENDLOOP.
*---------------------------------------------------------------------*
*
*---------------------------------------------------------------------*
*AT SELECTION-SCREEN ON VALUE-REQUEST FOR p_vari.
** popup F4 help to select a layout
*  PERFORM alv_worklist_variant.

*&---------------------------------------------------------------------*
*&      Form  ALV_WORKLIST_VARIANT
*&---------------------------------------------------------------------*
FORM alv_worklist_variant .

  DATA: lv_save        TYPE char01,  "I_SAVE: modus for saving a layout
        lv_layout      TYPE disvariant,
        lv_spec_layout TYPE disvariant,     "specific layout
        lv_exit        TYPE c.  "is set if the user has aborted a layout popup

  CLEAR lv_layout.
  MOVE sy-repid TO lv_layout-report.

  CALL FUNCTION 'LVC_VARIANT_F4'
    EXPORTING
      is_variant = lv_layout
      i_save     = gc_save
    IMPORTING
      e_exit     = lv_exit
      es_variant = lv_spec_layout
    EXCEPTIONS
      not_found  = 1
      OTHERS     = 2.
  IF sy-subrc NE 0.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
            WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ELSE.
    IF lv_exit IS INITIAL.
*     set name of layout on selection screen
      p_vari    = lv_spec_layout-variant.
    ENDIF.
  ENDIF.
ENDFORM.                    " ALV_WORKLIST_VARIANT
