*&---------------------------------------------------------------------*
*&  Include           ZIARN_APPROVALS_SLACI1
*&---------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*&       Class (Implementation)  lcl_approvals_sla
*&---------------------------------------------------------------------*
*        Text
*----------------------------------------------------------------------*
CLASS lcl_approvals_sla IMPLEMENTATION.

  METHOD get_default_wf_created_date.
    rv_date = sy-datum - 30.
  ENDMETHOD.

  METHOD constructor.

    me->ms_control = is_control.
    me->ms_selections = is_selections.

    me->mv_factory_calendar = zcl_arn_sla_services=>get_fact_cal( ).

  ENDMETHOD.

  METHOD run.

* Some preprocessing
    me->preprocess( ).

* Control processing based on report selection
    CASE me->ms_control-report.
* Pending
      WHEN c_report-pending.
        ro_result = me->run_pending( ).
* Approved
      WHEN c_report-approved.
        ro_result = me->run_approved( ).
* Product
      WHEN c_report-product.
        ro_result = me->run_product( ).
* Vendor
      WHEN c_report-vendor.
        ro_result = me->run_vendor( ).
      WHEN OTHERS.
    ENDCASE.


  ENDMETHOD.

  METHOD display.

* Prepare display
    me->prepare_display( io_result ).

* Display
    me->mo_result_alv->display( ).

  ENDMETHOD.

  METHOD query.

    DATA: lt_appr_guid TYPE TABLE OF zarn_e_appr_guid.

    DATA: lt_status TYPE RANGE OF zarn_e_appr_status.

* Approved workflow analysis
    IF me->ms_control-awa = abap_true.

* Get approved workflow analysis data
      me->get_awa_data( ).

* Get the article created history. This will provide the timeframe to determine New and change scenarios
*      me->get_article_create_history(  ).

* Get the AR history
*      me->get_ar_history_data( ).

* Pending workflow analysis
    ELSE.

      me->get_pwa_data( ).
    ENDIF.

  ENDMETHOD.

  METHOD preprocess.
* Convert our selections...for selection

    me->prepare_selections( ).

  ENDMETHOD.

  METHOD postprocess_query.






  ENDMETHOD.

  METHOD filter_results ##NEEDED.


  ENDMETHOD.


  METHOD run_pending.

* Get the pending items
    DATA(lt_sla_item) = me->get_pwa_data( ).

* Adjust the result
    me->adjust_results( CHANGING ct_sla_items = lt_sla_item ).

* Calculate the SLA
    me->calc_sla( CHANGING ct_sla_items = lt_sla_item ).

* Prepare the item output
    me->mt_item_output = me->prepare_item_output( lt_sla_item ).

    GET REFERENCE OF me->mt_item_output INTO ro_result.

  ENDMETHOD.

  METHOD run_approved.

* Get the approved items
    DATA(lt_sla_item) = me->get_awa_data( ).

* Get the article history
    me->get_article_create_history( lt_sla_item ).

* Get the AR history data
    me->get_ar_history_data( lt_sla_item ).

* Adjust the result
    me->adjust_results( EXPORTING iv_scrub_ccat = abap_true CHANGING ct_sla_items = lt_sla_item ).

* Calculate the duration
    lt_sla_item = me->calc_duration( lt_sla_item ).

* Calculate the SLA
    me->calc_sla( CHANGING ct_sla_items = lt_sla_item ).

* Prepare the item output
    me->mt_item_output = me->prepare_item_output( lt_sla_item ).

    GET REFERENCE OF me->mt_item_output INTO ro_result.

  ENDMETHOD.

  METHOD run_product.

* Get the approved items
    DATA(lt_sla_ap_item) = me->get_awa_data( ).

* Get the pending items
    DATA(lt_sla_ar_item) = me->get_pwa_data( ).

* Get the article history
    me->get_article_create_history( lt_sla_ap_item ).

* Get the AR history data
    me->get_ar_history_data( lt_sla_ap_item ).

* Adjust the result
    me->adjust_results( EXPORTING iv_scrub_ccat = abap_true CHANGING ct_sla_items = lt_sla_ap_item ).
    me->adjust_results( CHANGING ct_sla_items = lt_sla_ar_item ).

* Calculate the duration
    lt_sla_ap_item = me->calc_duration( lt_sla_ap_item  ).

* Calculate the SLA
    me->calc_sla( CHANGING ct_sla_items = lt_sla_ap_item ).

* Calculate the product items
    DATA(lt_product_items) =
      me->calc_product_items(
          it_pending_items  = lt_sla_ar_item
          it_approved_items = lt_sla_ap_item
      ).

*Prepare the product output
    me->mt_product_output = me->prepare_product_output( lt_product_items ).

    GET REFERENCE OF me->mt_product_output INTO ro_result.

  ENDMETHOD.



  METHOD run_vendor.


*Build the vendor output
    me->mt_vendor_output = me->build_vendor_output( ).







**** Get the Posted items
***    DATA(lt_sla_po_item) = me->get_post_data( ).
***
**** Get the PO history data
***    me->get_po_history_data( lt_sla_po_item ).
***
**** Get the AR history data
***    me->get_ar_history_data( lt_sla_po_item ).
***
**** Get the approval ready data for Posted products sorted by time history data
***    me->get_ar_po_history_data( lt_sla_po_item ).
***
***** Get the approval ready data for changed products posted sorted by time history data
****    me->get_chg_ar_history_data( lt_sla_po_item ).
***
***
**** Calculate the duration
***    lt_sla_po_item = me->calc_duration_vendor( lt_sla_po_item  ).
***
**** Calculate the SLA
***    me->calc_sla( CHANGING ct_sla_items = lt_sla_po_item ).
***
**** Calculate the Vendor items
***    DATA(lt_vendor_items) =
***      me->calc_vendor_items(
***          it_posted_items  = lt_sla_po_item
***      ).
***
****Prepare the vendor output
***    me->mt_vendor_output = me->prepare_vendor_output( lt_vendor_items ).

    GET REFERENCE OF me->mt_vendor_output INTO ro_result.



  ENDMETHOD.

  METHOD prepare_selections.
* Prepare the selections

    DATA: ls_matnr    LIKE LINE OF me->ms_selections-matnr,
          ls_upd_ts   LIKE LINE OF me->ms_selections-wfca_ts,
          ls_app_area LIKE LINE OF me->ms_selections-approval_area.


* If material=blank then scenario = NEW else scenario=CHG
    CASE abap_true.
* Only interested in new. Material number will be blank
      WHEN me->ms_selections-new.
        ls_matnr-sign = 'I'.
        ls_matnr-option = 'EQ'.
        APPEND ls_matnr TO me->ms_selections-matnr.
* Material number will not be blank
      WHEN me->ms_selections-chg.
        ls_matnr-sign = 'I'.
        ls_matnr-option = 'NE'.
        APPEND ls_matnr TO me->ms_selections-matnr.
      WHEN OTHERS.
    ENDCASE.

* Convert dates into timestamp
    DATA(ls_wfca) = VALUE #( me->ms_selections-wfca[ 1 ] OPTIONAL ).
    IF ls_wfca-low IS NOT INITIAL.
      MOVE-CORRESPONDING ls_wfca TO ls_upd_ts.
      ls_upd_ts-low = ls_wfca-low.
      ls_upd_ts-low+8 = '000000'.
    ENDIF.
    IF ls_wfca-high IS NOT INITIAL.
      ls_upd_ts-high = ls_wfca-high.
      ls_upd_ts-high+8 = '235959'.
    ENDIF.
    IF ls_upd_ts IS NOT INITIAL.
      APPEND ls_upd_ts TO me->ms_selections-wfca_ts.
    ENDIF.

* Add the approval status depending on selection
    IF me->ms_control-pwa = abap_true.
      me->ms_selections-status =
        VALUE #(
         ( sign = 'I'
           option = 'EQ'
           low = zcl_arn_approval_backend=>gc_stat-appr_ready )
        ).
    ENDIF.

* Add the national or regional selection
* Radio buttons National/Regional
    CASE abap_true.
      WHEN me->ms_selections-nat.
        ls_app_area-sign = 'I'.
        ls_app_area-option = 'EQ'.
        ls_app_area-low = zcl_arn_approval_backend=>gc_appr_area_national.
        APPEND ls_app_area TO me->ms_selections-approval_area.
      WHEN me->ms_selections-reg.
        ls_app_area-sign = 'I'.
        ls_app_area-option = 'EQ'.
        ls_app_area-low = zcl_arn_approval_backend=>gc_appr_area_regional.
        APPEND ls_app_area TO me->ms_selections-approval_area.
      WHEN OTHERS. "Empty range OK
    ENDCASE.

  ENDMETHOD.

  METHOD load_result_texts.

** Load category manager
*    me->load_catman( ).
*
** Load merch cat texts
*    me->load_mcat_texts( ).


  ENDMETHOD.

  METHOD load_catman.

* load the category manager buffer
    SELECT *
      INTO TABLE me->mt_category
      FROM zmd_category.

  ENDMETHOD.

  METHOD load_mcat_texts.

    CHECK it_item_data IS NOT INITIAL.

    DATA(lt_result) = it_item_data.

    SORT lt_result BY matkl.
    DELETE ADJACENT DUPLICATES FROM lt_result COMPARING matkl.

    SELECT matkl wgbez60
      FROM t023t
      INTO CORRESPONDING FIELDS OF TABLE me->mt_mcat_text
      FOR ALL ENTRIES IN lt_result
      WHERE matkl = lt_result-matkl AND
            spras = sy-langu.

  ENDMETHOD.

  METHOD load_lifnr_texts.

    CHECK it_vend_data IS NOT INITIAL.

    DATA(lt_result) = it_vend_data.

    SORT lt_result BY lifnr.
    DELETE ADJACENT DUPLICATES FROM lt_result COMPARING lifnr.

    SELECT lifnr name1
      FROM lfa1
      INTO CORRESPONDING FIELDS OF TABLE me->mt_lifnr_text
      FOR ALL ENTRIES IN lt_result
      WHERE lifnr = lt_result-lifnr.

  ENDMETHOD.

  METHOD prepare_item_output.

    DATA: ls_output TYPE zsarn_sla_item_ui.

* Load text stuff
    me->load_mcat_texts( it_sla_items ).
    me->load_catman( ).

* Anything without a material is new and anything with a material is change. Per businesss rule
    LOOP AT it_sla_items ASSIGNING FIELD-SYMBOL(<ls_result>).
* Merchandise category
      ls_output-wgbez60 = VALUE #( me->mt_mcat_text[ matkl = <ls_result>-matkl ]-wgbez60 OPTIONAL ).
* Whoelsale catman
      ls_output-gil_zzcatman_name = VALUE #( me->mt_category[ role_id = <ls_result>-gil_zzcatman ]-role_name OPTIONAL ).
* Retail catman
      ls_output-ret_zzcatman_name = VALUE #( me->mt_category[ role_id = <ls_result>-ret_zzcatman ]-role_name OPTIONAL ).
      MOVE-CORRESPONDING <ls_result> TO ls_output.
      APPEND ls_output TO rt_output.
    ENDLOOP.

  ENDMETHOD.

  METHOD prepare_product_output.

    DATA: ls_output TYPE zsarn_sla_po_ui.

* Load text stuff
    me->load_catman( ).

* Anything without a material is new and anything with a material is change. Per businesss rule
    LOOP AT it_product_items ASSIGNING FIELD-SYMBOL(<ls_result>).
      MOVE-CORRESPONDING <ls_result> TO ls_output.
* Whoelsale catman
      ls_output-catman_name = VALUE #( me->mt_category[ role_id = <ls_result>-catman ]-role_name OPTIONAL ).
      APPEND ls_output TO rt_output.
    ENDLOOP.

  ENDMETHOD.

  METHOD prepare_vendor_output.

    DATA: ls_output TYPE zsarn_sla_vo_ui.

* Load text stuff
    me->load_lifnr_texts( it_vendor_items ).
    me->load_catman( ).

    LOOP AT it_vendor_items ASSIGNING FIELD-SYMBOL(<ls_result>).
      MOVE-CORRESPONDING <ls_result> TO ls_output.
* Catman Name
      ls_output-catman_name = VALUE #( me->mt_category[ role_id = <ls_result>-catman ]-role_name OPTIONAL ).

* Vendor Name
      ls_output-lifnr_name = VALUE #( me->mt_lifnr_text[ lifnr = <ls_result>-lifnr ]-name1 OPTIONAL ).

      APPEND ls_output TO rt_output.
    ENDLOOP.

    SORT rt_output BY lifnr catman ret_gilm.

  ENDMETHOD.

  METHOD build_vendor_output.

    TYPES: BEGIN OF ty_s_idno,
             idno         TYPE zarn_reg_hdr-idno,
             version      TYPE zarn_reg_hdr-version,
             matnr        TYPE zarn_reg_hdr-matnr,
             initiation   TYPE zarn_reg_hdr-initiation,
             ret_zzcatman TYPE zarn_reg_hdr-ret_zzcatman,
             gil_zzcatman TYPE zarn_reg_hdr-gil_zzcatman,
             lifnr        TYPE zarn_reg_pir-lifnr,
           END OF ty_s_idno,
           ty_t_idno TYPE STANDARD TABLE OF ty_s_idno,

           BEGIN OF ty_s_chg_dates,
             idno       TYPE zarn_idno,
             created_on TYPE ersda,
             posted_on  TYPE zarn_posting_date,
           END OF ty_s_chg_dates,
           ty_t_chg_dates TYPE STANDARD TABLE OF ty_s_chg_dates.


    TYPES: BEGIN OF ty_s_prod_ctrl.
            INCLUDE TYPE zvarn_prod_ctrl.
    TYPES: proc_type TYPE char3,
           mark      TYPE flag.
    TYPES: END OF ty_s_prod_ctrl,
    ty_t_prod_ctrl TYPE STANDARD TABLE OF ty_s_prod_ctrl.




    DATA: lt_idno               TYPE STANDARD TABLE OF ty_s_idno,
          ls_idno               TYPE ty_s_idno,
          lt_vendor             TYPE STANDARD TABLE OF ty_s_idno,
          ls_vendor             TYPE ty_s_idno,
          ls_article_created    TYPE zst_arn_article_created,
          lt_prod_ctrl          TYPE ty_t_prod_ctrl,
          lt_prod_ctrl_new      TYPE ty_t_prod_ctrl,
          lt_prod_ctrl_chg      TYPE ty_t_prod_ctrl,
          lt_prod_ctrl_tmp      TYPE ty_t_prod_ctrl,
          lt_mara               TYPE STANDARD TABLE OF mara,
          lt_object_id          TYPE STANDARD TABLE OF zsarn_objectid,
          lt_chg_dates          TYPE ty_t_chg_dates,
          ls_chg_dates          TYPE ty_s_chg_dates,
          ls_output             TYPE zsarn_sla_vo_ui,
          lt_output             TYPE ztarn_sla_vo_ui,

          lv_article_created_ts TYPE zarn_e_appr_upd_tmstmp,
          lv_created_ts         TYPE zarn_e_appr_upd_tmstmp,
          lv_new_articles       TYPE sy-tabix,
          lv_chg_articles       TYPE sy-tabix,
          lv_tabix              TYPE sy-tabix,
          lv_days               TYPE int4,
          lv_from               TYPE datum,
          lv_to                 TYPE datum.



    FIELD-SYMBOLS: "<ls_approval>  TYPE ty_s_approval,
                   <ls_prod_ctrl> TYPE ty_s_prod_ctrl.


* Get IDNO based on selectoin criteria
    SELECT a~idno a~version a~matnr a~initiation a~ret_zzcatman a~gil_zzcatman
           b~lifnr
      INTO CORRESPONDING FIELDS OF TABLE lt_idno[]
      FROM zarn_reg_hdr AS a
      JOIN zarn_reg_pir AS b
      ON a~idno = b~idno
      WHERE ( a~ret_zzcatman IN me->ms_selections-catman OR a~gil_zzcatman IN me->ms_selections-catman )
        AND b~lifnr IN me->ms_selections-lifnr
        AND a~initiation NE space.
    IF sy-subrc NE 0.
      RETURN.
    ELSE.
      SORT lt_idno[].
      DELETE ADJACENT DUPLICATES FROM lt_idno[] COMPARING ALL FIELDS.
    ENDIF.

** Get Creation date from MARA
*    SELECT matnr ersda ernam laeda aenam zzfan
*      FROM mara
*      INTO CORRESPONDING FIELDS OF TABLE lt_mara[]
*          FOR ALL ENTRIES IN lt_idno[]
*          WHERE matnr EQ lt_idno-matnr.



    lt_object_id = CORRESPONDING #( lt_idno MAPPING objectid = matnr ) .
    SORT lt_object_id.
    DELETE lt_object_id WHERE objectid IS INITIAL.
    DELETE ADJACENT DUPLICATES FROM lt_object_id.

    IF lt_object_id[] IS NOT INITIAL.
* Pull the article created history from change docs
      SELECT objectid AS matnr , udate, utime
        FROM cdhdr
        INTO TABLE @me->mt_article_create_history
        FOR ALL ENTRIES IN @lt_object_id
        WHERE objectid = @lt_object_id-objectid
          AND objectclas = 'MAT_FULL'
          AND change_ind = 'I'.
*          AND username   = 'RFC_ARENA'.
*          AND udate IN @me->ms_selections-wfca[].

** Pull the article changed history from change docs
*      SELECT objectid AS matnr , udate, utime
*        FROM cdhdr
*        INTO TABLE @me->mt_article_update_history
*        FOR ALL ENTRIES IN @lt_object_id
*        WHERE objectid = @lt_object_id-objectid
*          AND objectclas = 'MAT_FULL'
*          AND change_ind = 'U'
*          AND username   = 'RFC_ARENA'
*          AND udate IN @me->ms_selections-wfca[].
    ENDIF.

* Get Version Created on date
    SELECT idno version id_type guid version_status release_status
           changed_on changed_at changed_by
           posted_on posted_at posted_by
           created_on created_at created_by
      FROM zvarn_prod_ctrl
      INTO CORRESPONDING FIELDS OF TABLE lt_prod_ctrl[]
      FOR ALL ENTRIES IN lt_idno[]
      WHERE idno           EQ lt_idno-idno
        AND version_status NE 'NCH'
        AND created_on     IN me->ms_selections-wfca[].
    IF sy-subrc NE 0.
      RETURN.
    ELSE.
      SORT lt_prod_ctrl[] BY idno version.
    ENDIF.


    LOOP AT lt_prod_ctrl[] ASSIGNING <ls_prod_ctrl>.

      CLEAR ls_idno.
      TRY.
          ls_idno = lt_idno[ idno = <ls_prod_ctrl>-idno ].
        CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
          CONTINUE.
      ENDTRY.

* If article is not created then NEW
      CLEAR ls_article_created.
      READ TABLE me->mt_article_create_history INTO ls_article_created
      WITH KEY matnr = ls_idno-matnr.
      IF sy-subrc NE 0.
        <ls_prod_ctrl>-proc_type = 'NEW'.
      ELSE.

        lv_article_created_ts = ls_article_created-udate && ls_article_created-utime.
        lv_created_ts = <ls_prod_ctrl>-created_on && <ls_prod_ctrl>-created_at.

* If article is changed on or before creation date then it is NEW, else CHG
        IF lv_created_ts LE lv_article_created_ts.
          <ls_prod_ctrl>-proc_type = 'NEW'.
        ELSE.
          <ls_prod_ctrl>-proc_type = 'CHG'.
        ENDIF.

      ENDIF.

    ENDLOOP.  " LOOP AT lt_prod_ctrl[] ASSIGNING <ls_prod_ctrl>



    lt_vendor[] = lt_idno[].
    SORT lt_vendor[] BY lifnr.
    DELETE ADJACENT DUPLICATES FROM lt_vendor[] COMPARING lifnr.

* Vendor Text
    SELECT lifnr name1
      FROM lfa1
      INTO CORRESPONDING FIELDS OF TABLE me->mt_lifnr_text
      FOR ALL ENTRIES IN lt_vendor
      WHERE lifnr = lt_vendor-lifnr.

* CATMAN Text
    me->load_catman( ).



    LOOP AT lt_vendor[] INTO ls_vendor.

      IF ls_vendor-ret_zzcatman IN me->ms_selections-catman[].

        CLEAR: lt_prod_ctrl_new[], lt_prod_ctrl_chg[].

* RETAIL
        LOOP AT lt_idno INTO ls_idno
          WHERE lifnr = ls_vendor-lifnr
            AND ret_zzcatman = ls_vendor-ret_zzcatman.

* Get NEW IDNOs
          TRY.
              CLEAR lt_prod_ctrl_tmp[].
              lt_prod_ctrl_tmp[] = VALUE #( FOR ls_new IN lt_prod_ctrl[]
                                            WHERE ( idno = ls_idno-idno AND
                                                    proc_type = 'NEW' )
                                                    ( ls_new ) ).
              APPEND LINES OF lt_prod_ctrl_tmp[] TO lt_prod_ctrl_new[].
            CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
              CONTINUE.
          ENDTRY.

* Get CHG IDNOs
          TRY.
              CLEAR lt_prod_ctrl_tmp[].
              lt_prod_ctrl_tmp[] = VALUE #( FOR ls_chg IN lt_prod_ctrl[]
                                            WHERE ( idno = ls_idno-idno AND
                                                    proc_type = 'CHG' )
                                                    ( ls_chg ) ).
              APPEND LINES OF lt_prod_ctrl_tmp[] TO lt_prod_ctrl_chg[].
            CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
              CONTINUE.
          ENDTRY.

        ENDLOOP.  " LOOP AT lt_idno INTO ls_idno - RETAIL



        SORT lt_prod_ctrl_chg[] BY idno version.

        CLEAR ls_output.
        ls_output-lifnr    = ls_idno-lifnr.
        ls_output-catman   = ls_idno-ret_zzcatman.
        ls_output-ret_gilm = 'RETAIL'.

* Vendor Name
        ls_output-lifnr_name = VALUE #( me->mt_lifnr_text[ lifnr = ls_output-lifnr ]-name1 OPTIONAL ).
* Catman Name
        ls_output-catman_name = VALUE #( me->mt_category[ role_id = ls_output-catman ]-role_name OPTIONAL ).


** NEW
        IF lt_prod_ctrl_new[] IS NOT INITIAL.

          SORT lt_prod_ctrl_new[] BY idno version.

          CLEAR: lv_new_articles, lv_days, lv_from, lv_to.

          LOOP AT lt_prod_ctrl_new[] INTO DATA(ls_prod_ctrl_new).

            lv_tabix = sy-tabix.

            DATA(ls_new_created) = lt_prod_ctrl_new[ lv_tabix ].

            AT NEW idno.
              lv_new_articles = lv_new_articles + 1.
              lv_from = ls_new_created-created_on.
            ENDAT.

            AT END OF idno.
              IF ls_new_created-posted_on IS NOT INITIAL.
                lv_to = ls_new_created-posted_on.
              ELSE.
                IF me->ms_selections-wfca[ 1 ]-high IS NOT INITIAL.
                  lv_to = me->ms_selections-wfca[ 1 ]-high.
                ELSE.
                  lv_to = me->ms_selections-wfca[ 1 ]-low.
                ENDIF.
              ENDIF.

              TRY.
* Get the working days between the First NEW and First posted
                  lv_days = lv_days + zcl_arn_sla_services=>calc_date_diff(
                                                                  iv_from             = lv_from
                                                                  iv_to               = lv_to
                                                                  iv_factory_calendar = me->mv_factory_calendar
                                                                           ) .
                CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
              ENDTRY.

              CLEAR:  lv_from, lv_to.
            ENDAT.

          ENDLOOP.  " LOOP AT lt_prod_ctrl_new[] INTO DATA(ls_prod_ctrl_new).

          IF lv_new_articles GE 0.
            ls_output-new_articles          = lv_new_articles.
            ls_output-average_proc_time_new = lv_days / lv_new_articles.
          ENDIF.

          CLEAR: lv_days, lv_from, lv_to, lv_new_articles.
        ENDIF.  " IF lt_prod_ctrl_new[] IS NOT INITIAL



** CHG
        IF lt_prod_ctrl_chg[] IS NOT INITIAL.

          SORT lt_prod_ctrl_chg[] BY idno version.

          CLEAR: lv_chg_articles, lv_days, lv_from, lv_to,
                 lt_chg_dates[].

          LOOP AT lt_prod_ctrl_chg[] INTO DATA(ls_prod_ctrl_chg).

            lv_tabix = sy-tabix.

            DATA(ls_new_changed) = lt_prod_ctrl_chg[ lv_tabix ].

            ls_chg_dates-idno       = ls_new_changed-idno.

            AT NEW idno.
              lv_chg_articles = lv_chg_articles + 1.
              lv_from = ls_new_changed-created_on.
            ENDAT.

            IF ls_new_changed-posted_on IS NOT INITIAL.
              ls_chg_dates-created_on = lv_from.
              ls_chg_dates-posted_on  = ls_new_changed-posted_on.
              APPEND ls_chg_dates TO lt_chg_dates[].

* Check if this is lat posted for current idno
              READ TABLE lt_prod_ctrl_chg[] INTO DATA(ls_prod_ctrl_nxt) INDEX ( lv_tabix + 1 ).
              IF sy-subrc = 0.
                IF ls_prod_ctrl_nxt-idno = ls_new_changed-idno.
                  lv_from = ls_prod_ctrl_nxt-created_on.
                ENDIF.
              ENDIF.
            ENDIF.


            AT END OF idno.
              IF ls_new_changed-posted_on IS NOT INITIAL.
                lv_to = ls_new_changed-posted_on.
              ELSE.
                IF me->ms_selections-wfca[ 1 ]-high IS NOT INITIAL.
                  lv_to = me->ms_selections-wfca[ 1 ]-high.
                ELSE.
                  lv_to = me->ms_selections-wfca[ 1 ]-low.
                ENDIF.
              ENDIF.

              ls_chg_dates-created_on = lv_from.
              ls_chg_dates-posted_on  = lv_to.
              APPEND ls_chg_dates TO lt_chg_dates[].

              CLEAR:  lv_from, lv_to.
            ENDAT.

          ENDLOOP.  " LOOP AT lt_prod_ctrl_chg[] INTO DATA(ls_prod_ctrl_chg)

          SORT lt_chg_dates[].
          DELETE ADJACENT DUPLICATES FROM lt_chg_dates[] COMPARING ALL FIELDS.

          LOOP AT lt_chg_dates[] INTO ls_chg_dates.
            lv_from = ls_chg_dates-created_on.
            lv_to = ls_chg_dates-posted_on.

            TRY.
* Get the working days between the First NEW and First posted
                lv_days = lv_days + zcl_arn_sla_services=>calc_date_diff(
                                                                iv_from             = lv_from
                                                                iv_to               = lv_to
                                                                iv_factory_calendar = me->mv_factory_calendar
                                                                         ) .
              CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
            ENDTRY.
          ENDLOOP.

          IF lv_chg_articles GE 0.
            ls_output-chg_articles          = lv_chg_articles.
            ls_output-average_proc_time_chg = lv_days / lv_chg_articles.
          ENDIF.

          CLEAR: lv_days, lv_from, lv_to, lv_chg_articles.

        ENDIF.  " IF lt_prod_ctrl_chg[] IS NOT INITIAL

        APPEND ls_output TO lt_output[].
      ENDIF.  " IF ls_vendor-ret_zzcatman IN me->ms_selections-catman[]











      IF ls_vendor-gil_zzcatman IN me->ms_selections-catman[].

        CLEAR: lt_prod_ctrl_new[], lt_prod_ctrl_chg[].

* RETAIL
        LOOP AT lt_idno INTO ls_idno
          WHERE lifnr = ls_vendor-lifnr
            AND gil_zzcatman = ls_vendor-gil_zzcatman.

* Get NEW IDNOs
          TRY.
              CLEAR lt_prod_ctrl_tmp[].
              lt_prod_ctrl_tmp[] = VALUE #( FOR ls_new IN lt_prod_ctrl[]
                                            WHERE ( idno = ls_idno-idno AND
                                                    proc_type = 'NEW' )
                                                    ( ls_new ) ).
              APPEND LINES OF lt_prod_ctrl_tmp[] TO lt_prod_ctrl_new[].
            CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
              CONTINUE.
          ENDTRY.

* Get CHG IDNOs
          TRY.
              CLEAR lt_prod_ctrl_tmp[].
              lt_prod_ctrl_tmp[] = VALUE #( FOR ls_chg IN lt_prod_ctrl[]
                                            WHERE ( idno = ls_idno-idno AND
                                                    proc_type = 'CHG' )
                                                    ( ls_chg ) ).
              APPEND LINES OF lt_prod_ctrl_tmp[] TO lt_prod_ctrl_chg[].
            CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
              CONTINUE.
          ENDTRY.

        ENDLOOP.  " LOOP AT lt_idno INTO ls_idno - RETAIL



        SORT lt_prod_ctrl_chg[] BY idno version.

        CLEAR ls_output.
        ls_output-lifnr    = ls_idno-lifnr.
        ls_output-catman   = ls_idno-gil_zzcatman.
        ls_output-ret_gilm = 'GILMOURS'.

* Vendor Name
        ls_output-lifnr_name = VALUE #( me->mt_lifnr_text[ lifnr = ls_output-lifnr ]-name1 OPTIONAL ).
* Catman Name
        ls_output-catman_name = VALUE #( me->mt_category[ role_id = ls_output-catman ]-role_name OPTIONAL ).


** NEW
        IF lt_prod_ctrl_new[] IS NOT INITIAL.

          SORT lt_prod_ctrl_new[] BY idno version.

          CLEAR: lv_new_articles, lv_days, lv_from, lv_to.

          LOOP AT lt_prod_ctrl_new[] INTO ls_prod_ctrl_new.

            lv_tabix = sy-tabix.

            ls_new_created = lt_prod_ctrl_new[ lv_tabix ].

            AT NEW idno.
              lv_new_articles = lv_new_articles + 1.
              lv_from = ls_new_created-created_on.
            ENDAT.

            AT END OF idno.
              IF ls_new_created-posted_on IS NOT INITIAL.
                lv_to = ls_new_created-posted_on.
              ELSE.
                IF me->ms_selections-wfca[ 1 ]-high IS NOT INITIAL.
                  lv_to = me->ms_selections-wfca[ 1 ]-high.
                ELSE.
                  lv_to = me->ms_selections-wfca[ 1 ]-low.
                ENDIF.
              ENDIF.

              TRY.
* Get the working days between the First NEW and First posted
                  lv_days = lv_days + zcl_arn_sla_services=>calc_date_diff(
                                                                  iv_from             = lv_from
                                                                  iv_to               = lv_to
                                                                  iv_factory_calendar = me->mv_factory_calendar
                                                                           ) .
                CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
              ENDTRY.

              CLEAR:  lv_from, lv_to.
            ENDAT.

          ENDLOOP.  " LOOP AT lt_prod_ctrl_new[] INTO DATA(ls_prod_ctrl_new).

          IF lv_new_articles GE 0.
            ls_output-new_articles          = lv_new_articles.
            ls_output-average_proc_time_new = lv_days / lv_new_articles.
          ENDIF.

          CLEAR: lv_days, lv_from, lv_to, lv_new_articles.
        ENDIF.  " IF lt_prod_ctrl_new[] IS NOT INITIAL




** CHG
        IF lt_prod_ctrl_chg[] IS NOT INITIAL.

          SORT lt_prod_ctrl_chg[] BY idno version.

          CLEAR: lv_chg_articles, lv_days, lv_from, lv_to,
                 lt_chg_dates[].

          LOOP AT lt_prod_ctrl_chg[] INTO ls_prod_ctrl_chg.

            lv_tabix = sy-tabix.

            ls_new_changed = lt_prod_ctrl_chg[ lv_tabix ].

            ls_chg_dates-idno       = ls_new_changed-idno.

            AT NEW idno.
              lv_chg_articles = lv_chg_articles + 1.
              lv_from = ls_new_changed-created_on.
            ENDAT.

            IF ls_new_changed-posted_on IS NOT INITIAL.
              ls_chg_dates-created_on = lv_from.
              ls_chg_dates-posted_on  = ls_new_changed-posted_on.
              APPEND ls_chg_dates TO lt_chg_dates[].

* Check if this is lat posted for current idno
              READ TABLE lt_prod_ctrl_chg[] INTO ls_prod_ctrl_nxt INDEX ( lv_tabix + 1 ).
              IF sy-subrc = 0.
                IF ls_prod_ctrl_nxt-idno = ls_new_changed-idno.
                  lv_from = ls_prod_ctrl_nxt-created_on.
                ENDIF.
              ENDIF.
            ENDIF.


            AT END OF idno.
              IF ls_new_changed-posted_on IS NOT INITIAL.
                lv_to = ls_new_changed-posted_on.
              ELSE.
                IF me->ms_selections-wfca[ 1 ]-high IS NOT INITIAL.
                  lv_to = me->ms_selections-wfca[ 1 ]-high.
                ELSE.
                  lv_to = me->ms_selections-wfca[ 1 ]-low.
                ENDIF.
              ENDIF.

              ls_chg_dates-created_on = lv_from.
              ls_chg_dates-posted_on  = lv_to.
              APPEND ls_chg_dates TO lt_chg_dates[].

              CLEAR:  lv_from, lv_to.
            ENDAT.

          ENDLOOP.  " LOOP AT lt_prod_ctrl_chg[] INTO DATA(ls_prod_ctrl_chg)

          SORT lt_chg_dates[].
          DELETE ADJACENT DUPLICATES FROM lt_chg_dates[] COMPARING ALL FIELDS.

          LOOP AT lt_chg_dates[] INTO ls_chg_dates.
            lv_from = ls_chg_dates-created_on.
            lv_to = ls_chg_dates-posted_on.

            TRY.
* Get the working days between the First NEW and First posted
                lv_days = lv_days + zcl_arn_sla_services=>calc_date_diff(
                                                                iv_from             = lv_from
                                                                iv_to               = lv_to
                                                                iv_factory_calendar = me->mv_factory_calendar
                                                                         ) .
              CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
            ENDTRY.
          ENDLOOP.

          IF lv_chg_articles GE 0.
            ls_output-chg_articles          = lv_chg_articles.
            ls_output-average_proc_time_chg = lv_days / lv_chg_articles.
          ENDIF.

          CLEAR: lv_days, lv_from, lv_to, lv_chg_articles.

        ENDIF.  " IF lt_prod_ctrl_chg[] IS NOT INITIAL

        APPEND ls_output TO lt_output[].

      ENDIF.  " IF ls_vendor-gil_zzcatman IN me->ms_selections-catman[]



    ENDLOOP.  " LOOP AT lt_vendor[] INTO ls_vendor

    rt_output[] = lt_output[].

  ENDMETHOD.  " build_vendor_output


  METHOD write_log ##NEEDED.
  ENDMETHOD.

  METHOD prepare_display.

    me->setup_alv( io_result ).

  ENDMETHOD.

  METHOD setup_alv.


    FIELD-SYMBOLS: <lt_output> TYPE ANY TABLE.

    ASSIGN io_result->* TO <lt_output>.

* Get the ALV
    TRY.
        cl_salv_table=>factory(
          IMPORTING
            r_salv_table   = me->mo_result_alv
          CHANGING
            t_table        = <lt_output>
        ).

* Get the ALV Function
        DATA(lo_functions) = me->mo_result_alv->get_functions( ).

        lo_functions->set_all( abap_true ).

        DATA(lo_display) = me->mo_result_alv->get_display_settings( ).

* Set the ALV Header (including some stats when testing)
        lo_display->set_list_header( CONV #( me->get_list_header( ) ) ) ##NO_TEXT.

* Add save layout
        DATA(lo_layout) = me->mo_result_alv->get_layout( ).
        lo_layout->set_key( VALUE salv_s_layout_key( report = sy-repid ) ).
        lo_layout->set_save_restriction( cl_salv_layout=>restrict_none ).

** Build the header
*        DATA(lo_header) = me->build_log_header( ).
*
** Set top-of-list
*        m e->mo_log_alv->set_top_of_list( lo_header ).
*        me->mo_log_alv->set_top_of_list_print( lo_header ).

* Columns stuff
        DATA(lo_columns) = me->mo_result_alv->get_columns( ).

* Make sure approval days has sign
        TRY.
            lo_columns->get_column( 'DAYS_TO_APPROVE' )->set_sign( abap_true ).
            lo_columns->get_column( 'DAYS_TO_APPROVE_CHG' )->set_sign( abap_true ).
            lo_columns->get_column( 'SLA_REMAINING' )->set_sign( abap_true ).
          CATCH cx_root ##CATCH_ALL ##NO_HANDLER.
        ENDTRY.

        me->hide_tech_columns( lo_columns ).

* Optimise the columns
        lo_columns->set_optimize( cl_salv_display_settings=>true ).


      CATCH cx_root INTO DATA(lo_ex) ##NO_HANDLER.
    ENDTRY.

  ENDMETHOD.

  METHOD hide_tech_columns.

* Hide columns
    LOOP AT me->get_tech_columns( ) ASSIGNING FIELD-SYMBOL(<ls_tech_col>).
      TRY.
          io_columns->get_column( <ls_tech_col> )->set_technical( ).
        CATCH cx_salv_not_found ##NO_HANDLER.
      ENDTRY.
    ENDLOOP.

  ENDMETHOD.

  METHOD get_tech_columns.

    IF me->ms_control-pwa = abap_true.
      rt_tech_columns =
        VALUE #(
          ( 'MATNR' )
          ( 'GUID' )
          ( 'POSTED_ON' )
          ( 'POSTED_AT' )
          ( 'AP_TIMESTAMP' )
          ( 'DAYS_TO_APPROVE' )
          ( 'RET_ZZCATMAN' )
          ( 'GIL_ZZCATMAN' )
          ( 'MATKL' )
        ).
    ELSE.
      rt_tech_columns =
        VALUE #(
          ( 'MATNR' )
          ( 'GUID' )
          ( 'APPROVAL_AREA' )
*          ( 'CHG_CATEGORY' )
          ( 'RET_ZZCATMAN' )
          ( 'GIL_ZZCATMAN' )
          ( 'MATKL' )
        ).
    ENDIF.

  ENDMETHOD.

  METHOD get_list_header.

* Return list header based on report runnning
    CASE me->ms_control-report.
      WHEN c_report-pending OR c_report-approved.
        rv_list_header = 'Itemised Report'(014).
      WHEN c_report-product.
        rv_list_header = 'Product Overview'(015).
      WHEN c_report-vendor.
        rv_list_header = 'Vendor Overview'(016).
    ENDCASE.

  ENDMETHOD.

  METHOD adjust_results.
* Adjust the result so that we can process our SLA rules consistently

    LOOP AT ct_sla_items ASSIGNING FIELD-SYMBOL(<ls_result>).

      DATA(lv_idx) = sy-tabix.

* Determine the process type
      <ls_result>-process_type = me->determine_process_type( <ls_result> ).
*        <ls_result>-process_type = 'NEW'.

* Further filtering based on NEW+CHG selections
      CASE abap_true.
        WHEN me->ms_selections-new.
          IF <ls_result>-process_type <> c_proc_type-new.
            DELETE ct_sla_items INDEX lv_idx.
            CONTINUE.
          ENDIF.
        WHEN me->ms_selections-chg.
          IF <ls_result>-process_type <> c_proc_type-change.
            DELETE ct_sla_items INDEX lv_idx.
            CONTINUE.
          ENDIF.
      ENDCASE.

* If the process type is new then the change category in the result is irrelevant so blank it
* This will allow us to group AR+AP correctly
* But is this only for approved scenario's?
      IF ( iv_scrub_ccat = abap_true AND <ls_result>-process_type = c_proc_type-new ).
        CLEAR <ls_result>-chg_category.
      ENDIF.

    ENDLOOP.


  ENDMETHOD.

  METHOD calc_duration.
* Calculates the number of days it took to do the approval

    DATA: lv_max_ap_ts TYPE timestamp,
          lv_max_ap    TYPE d,
          lv_days      TYPE i,
          lv_posted_ts TYPE timestamp.

    DATA(lt_sla_items) = it_sla_items.

* We need to consider the total approval time for a change by team ignoring the approval area(national or regional)
    LOOP AT lt_sla_items ASSIGNING FIELD-SYMBOL(<ls_result>)
      GROUP BY ( idno = <ls_result>-idno version = <ls_result>-version team_code = <ls_result>-team_code chg_category = <ls_result>-chg_category )
      ASSIGNING FIELD-SYMBOL(<lt_group>).

      CLEAR: lv_days, lv_max_ap, lv_posted_ts, lv_max_ap_ts.

* Per business rule, we only want to look at appprovals within our selection timeframe AND
* until the version has been posted. Everything else after for this version is irrelevant
* We now keep the version posting date in ZARN_PRD_VERSION-POSTED_ON
      LOOP AT GROUP <lt_group> ASSIGNING FIELD-SYMBOL(<ls_group>).
* Convert the posting date to timestamp
* BEGIN TEST ONLY
        IF <ls_group>-posted_on IS INITIAL.
          <ls_group>-posted_on = zcl_arn_sla_services=>get_default_posting_date( ).
        ENDIF.
* END TEST ONLY
        CONVERT DATE <ls_group>-posted_on TIME <ls_group>-posted_at INTO TIME STAMP lv_posted_ts TIME ZONE 'UTC'.
        IF lv_posted_ts IS INITIAL.
          lv_posted_ts = <ls_group>-ap_timestamp.
        ENDIF.
* Make sure we're within the posting timeframe
        IF <ls_group>-ap_timestamp <= lv_posted_ts.
* We take the AP time that is closet to the posting time so we can work out the distance between latest AP and earliest AR
          IF lv_posted_ts > lv_max_ap.
            lv_max_ap = <ls_group>-posted_on.
            lv_max_ap_ts = lv_posted_ts.
          ENDIF.
        ENDIF.
      ENDLOOP.

* If we don't have any AP then we're not within the posting timeframe and this should be ignored in the output
      IF lv_max_ap IS NOT INITIAL.
        TRY.
* Now read the earliest AR . For new this excludes chg_category
            DATA(lv_earliest_ar) = me->determine_earliest_ar( <ls_group> ).
* Get the working days between the earliest AR and latest AP
            lv_days = zcl_arn_sla_services=>calc_date_diff(
                iv_from             = CONV #( lv_earliest_ar(8) )
                iv_to               = lv_max_ap
                iv_factory_calendar = me->mv_factory_calendar
            ) .
          CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
        ENDTRY.
        <ls_group>-days_to_approve = lv_days.
        <ls_group>-ar_timestamp = lv_earliest_ar.
        <ls_group>-ap_timestamp = lv_max_ap_ts.
        APPEND <ls_group> TO rt_sla_items.
      ENDIF.
    ENDLOOP.

  ENDMETHOD.

  METHOD calc_duration_vendor.
* Calculates the number of days it took to do the approval

    DATA: lv_max_ap_ts TYPE timestamp,
          lv_max_ap    TYPE d,
          lv_days      TYPE i,
          lv_days_chg  TYPE i,
          lv_posted_ts TYPE timestamp.

    DATA(lt_sla_items) = it_sla_items.

* We need to consider the total approval time for a change by team ignoring the approval area(national or regional)
    LOOP AT lt_sla_items ASSIGNING FIELD-SYMBOL(<ls_result>)
      GROUP BY ( idno = <ls_result>-idno version = <ls_result>-version lifnr = <ls_result>-lifnr )
      ASSIGNING FIELD-SYMBOL(<lt_group>).

      CLEAR: lv_days, lv_max_ap, lv_posted_ts, lv_max_ap_ts.

* Per business rule, we only want to look at appprovals within our selection timeframe AND
* until the version has been posted. Everything else after for this version is irrelevant
* We now keep the version posting date in ZARN_PRD_VERSION-POSTED_ON
      LOOP AT GROUP <lt_group> ASSIGNING FIELD-SYMBOL(<ls_group>).
* Convert the posting date to timestamp
* BEGIN TEST ONLY
        IF <ls_group>-posted_on IS INITIAL.
          <ls_group>-posted_on = zcl_arn_sla_services=>get_default_posting_date( ).
        ENDIF.
* END TEST ONLY
        CONVERT DATE <ls_group>-posted_on TIME <ls_group>-posted_at INTO TIME STAMP lv_posted_ts TIME ZONE 'UTC'.
        IF lv_posted_ts IS INITIAL.
          lv_posted_ts = <ls_group>-ap_timestamp.
        ENDIF.
* Make sure we're within the posting timeframe
        IF <ls_group>-ap_timestamp <= lv_posted_ts.
* We take the AP time that is closet to the posting time so we can work out the distance between latest AP and earliest AR
          IF lv_posted_ts > lv_max_ap.
            lv_max_ap = <ls_group>-posted_on.
            lv_max_ap_ts = lv_posted_ts.
          ENDIF.
        ENDIF.
      ENDLOOP.

* If we don't have any AP then we're not within the posting timeframe and this should be ignored in the output
      IF lv_max_ap IS NOT INITIAL.
        TRY.
* Now read the earliest AR . For new this excludes chg_category
            DATA(lv_earliest_ar) = me->determine_earliest_ar_po( <ls_group> ).
* Get the working days between the earliest AR and latest AP
            lv_days = zcl_arn_sla_services=>calc_date_diff(
                iv_from             = CONV #( lv_earliest_ar(8) )
                iv_to               = lv_max_ap
                iv_factory_calendar = me->mv_factory_calendar
            ) .
          CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
        ENDTRY.

        TRY.
* Now read the earliest changed date . For new this excludes chg_category
            DATA(lv_earliest_chg) = me->determine_earliest_chg( <ls_group> ).
* Get the working days between the earliest change and latest AP
            lv_days = zcl_arn_sla_services=>calc_date_diff(
                iv_from             = CONV #( lv_earliest_chg(8) )
                iv_to               = lv_max_ap
                iv_factory_calendar = me->mv_factory_calendar
            ) .
          CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
        ENDTRY.



        <ls_group>-days_to_approve = lv_days.
        <ls_group>-days_to_approve_chg = lv_days_chg.
        <ls_group>-ar_timestamp = lv_earliest_ar.
        <ls_group>-ap_timestamp = lv_max_ap_ts.
        APPEND <ls_group> TO rt_sla_items.
      ENDIF.
    ENDLOOP.

  ENDMETHOD.

  METHOD calc_product_items.
* Calculate our stats for the Product overview items

* Calc the SLA stats
    me->calc_sla_stat(
      EXPORTING
        it_approved_items = it_approved_items
      CHANGING
        ct_po_items       = rt_po_items
    ).

* Now we determine the backlog items for the same group
    me->calc_backlog_stat(
      EXPORTING
        it_pending_items = it_pending_items
      CHANGING
        ct_po_items      = rt_po_items
    ).

  ENDMETHOD.


  METHOD calc_vendor_items.

    DATA: lt_posted_items TYPE ztarn_sla_item,
          ls_posted_items TYPE zsarn_sla_item,
          ls_vo_item      TYPE zsarn_sla_vo,
          lt_object_id    TYPE TABLE OF zsarn_objectid.




    CHECK it_posted_items IS NOT INITIAL.


    lt_posted_items = it_posted_items.

    SORT lt_posted_items[] BY fan_id lifnr version.
    DELETE ADJACENT DUPLICATES FROM lt_posted_items[] COMPARING fan_id lifnr.




    LOOP AT lt_posted_items ASSIGNING FIELD-SYMBOL(<ls_item>)
      WHERE ret_zzcatman IS NOT INITIAL AND ret_zzcatman IN me->ms_selections-catman
      GROUP BY ( lifnr = <ls_item>-lifnr
                 ret_zzcatman = <ls_item>-ret_zzcatman )
      ASSIGNING FIELD-SYMBOL(<lt_ret_group>).


      CLEAR ls_vo_item.
      LOOP AT GROUP <lt_ret_group> ASSIGNING FIELD-SYMBOL(<ls_ret_group>).

        ls_vo_item-lifnr              = <ls_ret_group>-lifnr.
        ls_vo_item-catman             = <ls_ret_group>-ret_zzcatman.
        ls_vo_item-ret_gilm           = 'RETAIL'.


* Increment total days
        ADD <ls_ret_group>-days_to_approve     TO ls_vo_item-total_days.
        ADD <ls_ret_group>-days_to_approve_chg TO ls_vo_item-total_days_chg.


        LOOP AT lt_posted_items INTO ls_posted_items
          WHERE lifnr        = <ls_ret_group>-lifnr
            AND ret_zzcatman = <ls_ret_group>-ret_zzcatman.

          READ TABLE me->mt_article_create_history[] TRANSPORTING NO FIELDS
          WITH KEY matnr = ls_posted_items-matnr.
          IF sy-subrc = 0.
            ls_vo_item-new_articles = ls_vo_item-new_articles + 1.
          ENDIF.

          READ TABLE me->mt_article_update_history[] TRANSPORTING NO FIELDS
          WITH KEY matnr = ls_posted_items-matnr.
          IF sy-subrc = 0.
            ls_vo_item-chg_articles = ls_vo_item-chg_articles + 1.
          ENDIF.

        ENDLOOP.  " LOOP AT lt_posted_items INTO ls_posted_items
      ENDLOOP.  " LOOP AT GROUP <lt_ret_group>

* Average processing time - NEW
      IF ls_vo_item-new_articles > 0.
        ls_vo_item-average_proc_time_new = ls_vo_item-total_days / ls_vo_item-new_articles.
      ENDIF.

* Average processing time - CHG
      IF ls_vo_item-chg_articles > 0.
        ls_vo_item-average_proc_time_chg = ls_vo_item-total_days_chg / ls_vo_item-chg_articles.
      ENDIF.

      APPEND ls_vo_item TO rt_vo_items[].

    ENDLOOP.    " RET CATMAN loop



    LOOP AT lt_posted_items ASSIGNING <ls_item>
      WHERE gil_zzcatman IS NOT INITIAL AND gil_zzcatman IN me->ms_selections-catman
      GROUP BY ( lifnr = <ls_item>-lifnr
                 gil_zzcatman = <ls_item>-gil_zzcatman )
      ASSIGNING FIELD-SYMBOL(<lt_gil_group>).

      CLEAR ls_vo_item.
      LOOP AT GROUP <lt_gil_group> ASSIGNING FIELD-SYMBOL(<ls_gil_group>).

        ls_vo_item-lifnr              = <ls_gil_group>-lifnr.
        ls_vo_item-catman             = <ls_gil_group>-ret_zzcatman.
        ls_vo_item-ret_gilm           = 'GILMOURS'.


* Increment total days
        ADD <ls_gil_group>-days_to_approve TO ls_vo_item-total_days.


        LOOP AT lt_posted_items INTO ls_posted_items
          WHERE lifnr        = <ls_gil_group>-lifnr
            AND gil_zzcatman = <ls_gil_group>-ret_zzcatman.

          READ TABLE me->mt_article_create_history[] TRANSPORTING NO FIELDS
          WITH KEY matnr = ls_posted_items-matnr.
          IF sy-subrc = 0.
            ls_vo_item-new_articles = ls_vo_item-new_articles + 1.
          ENDIF.

          READ TABLE me->mt_article_update_history[] TRANSPORTING NO FIELDS
          WITH KEY matnr = ls_posted_items-matnr.
          IF sy-subrc = 0.
            ls_vo_item-chg_articles = ls_vo_item-chg_articles + 1.
          ENDIF.

        ENDLOOP.  " LOOP AT lt_posted_items INTO ls_posted_items
      ENDLOOP. " LOOP AT GROUP <lt_gil_group>

* Average processing time - NEW
      IF ls_vo_item-new_articles > 0.
        ls_vo_item-average_proc_time_new = ls_vo_item-total_days / ls_vo_item-new_articles.
      ENDIF.

* Average processing time - CHG
      IF ls_vo_item-chg_articles > 0.
        ls_vo_item-average_proc_time_chg = ls_vo_item-total_days / ls_vo_item-chg_articles.
      ENDIF.

      APPEND ls_vo_item TO rt_vo_items[].

    ENDLOOP.    " GIL CATMAN loop



  ENDMETHOD.

  METHOD calc_backlog_stat.

    DATA: ls_po_item     TYPE zsarn_sla_po.

* Annoyingly we need to do this twice. One for each catman column as it will form a separate row
    LOOP AT it_pending_items ASSIGNING FIELD-SYMBOL(<ls_item>)
      WHERE ret_zzcatman IS NOT INITIAL AND ret_zzcatman IN me->ms_selections-catman
      GROUP BY ( initiation = <ls_item>-initiation
                 ret_zzcatman = <ls_item>-ret_zzcatman
                 team_code  = <ls_item>-team_code
                 process_type = <ls_item>-process_type
                 size = GROUP SIZE )
      ASSIGNING FIELD-SYMBOL(<ls_ret_group>).

      CLEAR ls_po_item.
      ASSIGN ct_po_items[ initiation = <ls_ret_group>-initiation
                          catman     = <ls_ret_group>-ret_zzcatman
                          team_code  = <ls_ret_group>-team_code
                          process_type = <ls_ret_group>-process_type ]
             TO FIELD-SYMBOL(<ls_po_item>).
      IF sy-subrc = 0.
        <ls_po_item>-items_in_backlog = <ls_ret_group>-size.
      ELSE.
        MOVE-CORRESPONDING <ls_ret_group> TO ls_po_item.
        ls_po_item-catman = <ls_ret_group>-ret_zzcatman.
        ls_po_item-items_in_backlog = <ls_ret_group>-size.
        APPEND ls_po_item TO ct_po_items.
      ENDIF.

    ENDLOOP.

* Annoyingly we need to do this twice. One for each catman column as it will form a separate row
    LOOP AT it_pending_items ASSIGNING <ls_item>
      WHERE gil_zzcatman IS NOT INITIAL AND gil_zzcatman IN me->ms_selections-catman
      GROUP BY ( initiation = <ls_item>-initiation
                 gil_zzcatman = <ls_item>-gil_zzcatman
                 team_code  = <ls_item>-team_code
                 process_type = <ls_item>-process_type
                 size = GROUP SIZE )
      ASSIGNING FIELD-SYMBOL(<ls_gil_group>).

      CLEAR ls_po_item.
      ASSIGN ct_po_items[ initiation = <ls_gil_group>-initiation
                          catman     = <ls_gil_group>-gil_zzcatman
                          team_code  = <ls_gil_group>-team_code
                          process_type = <ls_gil_group>-process_type ]
             TO <ls_po_item>.
      IF sy-subrc = 0.
        <ls_po_item>-items_in_backlog = <ls_gil_group>-size.
      ELSE.
        MOVE-CORRESPONDING <ls_gil_group> TO ls_po_item.
        ls_po_item-catman = <ls_gil_group>-gil_zzcatman.
        ls_po_item-items_in_backlog = <ls_gil_group>-size.
        APPEND ls_po_item TO ct_po_items.
      ENDIF.

    ENDLOOP.

  ENDMETHOD.

  METHOD calc_sla.

* Get our calculator
    DATA(lo_sla_calculator) = NEW zcl_arn_sla_processor( ).

    LOOP AT ct_sla_items ASSIGNING FIELD-SYMBOL(<ls_result>).
      TRY.
          DATA(ls_sla_info) =
              lo_sla_calculator->calculate_sla(
                  iv_key_date = CONV #( <ls_result>-ar_timestamp(8) )
                  iv_ref_date = COND #( WHEN <ls_result>-ap_timestamp IS INITIAL THEN sy-datum ELSE <ls_result>-ap_timestamp(8) )
                  is_calc_key = CORRESPONDING #( <ls_result> )
          ) ##ENH_OK.
          <ls_result>-sla_remaining = ls_sla_info-sla_rem_days.
          <ls_result>-sla_date = ls_sla_info-sla_date.
          <ls_result>-sla_target_days = ls_sla_info-sla_target_days.
        CATCH cx_sy_conversion_overflow ##NO_HANDLER.
      ENDTRY.
    ENDLOOP.


  ENDMETHOD.

  METHOD calc_sla_stat.
* Calculate the SLA stats based on the approved items

    DATA: ls_po_item     TYPE zsarn_sla_po.

* Annoyingly we need to do this twice. One for each catman column as it will form a separate row
    LOOP AT it_approved_items ASSIGNING FIELD-SYMBOL(<ls_item>)
      WHERE ret_zzcatman IS NOT INITIAL AND ret_zzcatman IN me->ms_selections-catman
      GROUP BY ( initiation = <ls_item>-initiation
                 ret_zzcatman = <ls_item>-ret_zzcatman
                 team_code  = <ls_item>-team_code
                 process_type = <ls_item>-process_type )
      ASSIGNING FIELD-SYMBOL(<lt_ret_group>).

      CLEAR ls_po_item.
      LOOP AT GROUP <lt_ret_group> ASSIGNING FIELD-SYMBOL(<ls_ret_group>).
* Negative indicates SLA was breached
        IF <ls_ret_group>-sla_remaining < 0.
          ADD 1 TO ls_po_item-items_over_sla.
* SLA was met
        ELSE.
          ADD 1 TO ls_po_item-items_met_sla.
        ENDIF.
* Increment total items
        ADD 1 TO ls_po_item-items_total.
* Increment total days
        ADD <ls_ret_group>-days_to_approve TO ls_po_item-total_days.
      ENDLOOP.

* Average processing time
      IF ls_po_item-total_days > 0.
        ls_po_item-average_proc_time = ls_po_item-total_days / ls_po_item-items_total.
      ENDIF.
      MOVE-CORRESPONDING <ls_ret_group> TO ls_po_item.
      ls_po_item-catman = <ls_ret_group>-ret_zzcatman.
      APPEND ls_po_item TO ct_po_items.
    ENDLOOP.

* Annoyingly we need to do this twice. One for each catman column as it will form a separate row
    LOOP AT it_approved_items ASSIGNING <ls_item>
      WHERE gil_zzcatman IS NOT INITIAL AND gil_zzcatman IN me->ms_selections-catman
      GROUP BY ( initiation = <ls_item>-initiation
                 gil_zzcatman = <ls_item>-gil_zzcatman
                 team_code  = <ls_item>-team_code
                 process_type = <ls_item>-process_type )
      ASSIGNING FIELD-SYMBOL(<lt_gil_group>).

      CLEAR ls_po_item.
      LOOP AT GROUP <lt_gil_group> ASSIGNING FIELD-SYMBOL(<ls_gil_group>).
* Negative indicates SLA was breached
        IF <ls_gil_group>-sla_remaining < 0.
          ADD 1 TO ls_po_item-items_over_sla.
* SLA was met
        ELSE.
          ADD 1 TO ls_po_item-items_met_sla.
        ENDIF.
* Increment total items
        ADD 1 TO ls_po_item-items_total.
* Increment total days
        ADD <ls_gil_group>-days_to_approve TO ls_po_item-total_days.
      ENDLOOP.

* Average processing time
      IF ls_po_item-total_days > 0.
        ls_po_item-average_proc_time = ls_po_item-total_days / ls_po_item-items_total.
      ENDIF.
      MOVE-CORRESPONDING <ls_gil_group> TO ls_po_item.
      ls_po_item-catman = <ls_gil_group>-gil_zzcatman.
      APPEND ls_po_item TO ct_po_items.
    ENDLOOP.

  ENDMETHOD.

  METHOD get_article_create_history.
* Pull the article created history from change docs

    DATA: lt_object_id TYPE TABLE OF zsarn_objectid.

    CHECK it_item_data IS NOT INITIAL.

    lt_object_id = CORRESPONDING #( it_item_data MAPPING objectid = matnr ) .

    SORT lt_object_id.
    DELETE ADJACENT DUPLICATES FROM lt_object_id.

    SELECT objectid AS matnr , udate, utime
      FROM cdhdr
      INTO TABLE @me->mt_article_create_history
      FOR ALL ENTRIES IN @lt_object_id
      WHERE objectid = @lt_object_id-objectid AND
            objectclas = 'MAT_FULL' AND
            change_ind = 'I'.


  ENDMETHOD.

  METHOD get_ar_history_data.
* Get the AR history data

    DATA(lt_result) = it_item_data.
    SORT lt_result BY guid version.
    DELETE ADJACENT DUPLICATES FROM lt_result COMPARING guid version.
* Now get the  approved ready so that we can work out the time difference
    SELECT b~idno, a~nat_version, b~team_code, b~chg_category, a~updated_timestamp
      FROM zarn_appr_hist AS a INNER JOIN
           zarn_approval AS b ON b~guid = a~guid
      INTO TABLE @me->mt_history_ar
      FOR ALL ENTRIES IN @lt_result
      WHERE a~guid = @lt_result-guid AND
            a~nat_version = @lt_result-version AND
            a~status IN ( @zcl_arn_approval_backend=>gc_stat-appr_ready ) .

* We only care about the earlist AR for a version for the below combination.
    DELETE ADJACENT DUPLICATES FROM me->mt_history_ar COMPARING idno nat_version team_code chg_category.

  ENDMETHOD.

  METHOD get_po_history_data.
* Get the AR history data

    DATA: lt_history_po TYPE STANDARD TABLE OF ty_history.


    DATA(lt_result) = it_item_data.
    SORT lt_result BY guid version.
    DELETE ADJACENT DUPLICATES FROM lt_result COMPARING guid version.
* Now get the  POSTED so that we can work out the time difference
    SELECT b~idno, a~nat_version, b~team_code, b~chg_category, a~updated_timestamp
      FROM zarn_appr_hist AS a INNER JOIN
           zarn_approval AS b ON b~guid = a~guid
      INTO TABLE @lt_history_po[]
      FOR ALL ENTRIES IN @lt_result
      WHERE a~guid = @lt_result-guid AND
            a~nat_version = @lt_result-version AND
            a~status IN ( @zcl_arn_approval_backend=>gc_stat-posted ) .



    " records which are only in approvals but not in appr_hist yet
* Now get the  POSTED so that we can work out the time difference
    SELECT idno nat_version team_code chg_category updated_timestamp
      FROM zarn_approval
      APPENDING CORRESPONDING FIELDS OF TABLE lt_history_po[]
      FOR ALL ENTRIES IN lt_result
      WHERE guid = lt_result-guid AND
            nat_version = lt_result-version AND
            status IN ( zcl_arn_approval_backend=>gc_stat-posted ) .

    me->mt_history_po[] = lt_history_po[].

* We only care about the earlist PO for a version for the below combination.
    DELETE ADJACENT DUPLICATES FROM me->mt_history_po COMPARING idno nat_version.

  ENDMETHOD.

  METHOD get_ar_po_history_data.
* Get the approval ready data for Posted products sorted by time history data


    DATA(lt_result) = it_item_data.
    SORT lt_result BY guid version.
    DELETE ADJACENT DUPLICATES FROM lt_result COMPARING guid version.
* Now get the  approved ready so that we can work out the time difference
    SELECT b~idno, a~nat_version, b~team_code, b~chg_category, a~updated_timestamp
      FROM zarn_appr_hist AS a INNER JOIN
           zarn_approval AS b ON b~guid = a~guid
      INTO TABLE @me->mt_history_ar_po
      FOR ALL ENTRIES IN @lt_result
      WHERE a~guid = @lt_result-guid AND
            a~nat_version = @lt_result-version AND
            a~status IN ( @zcl_arn_approval_backend=>gc_stat-appr_ready ) .

* We only care about the earlist AR for a version for the below combination.
    DELETE ADJACENT DUPLICATES FROM me->mt_history_ar_po COMPARING idno nat_version.

  ENDMETHOD.


  METHOD get_chg_ar_history_data.
* Get the approval ready data for changed products posted sorted by time history data

*
*
*    DATA: lt_history_chg_ar TYPE STANDARD TABLE OF ty_history.
*
*
*    DATA(lt_result) = it_item_data.
*    SORT lt_result BY guid version.
*    DELETE ADJACENT DUPLICATES FROM lt_result COMPARING guid version.
** Now get the  POSTED so that we can work out the time difference
*    SELECT b~idno, a~nat_version, b~team_code, b~chg_category, a~updated_timestamp
*      FROM zarn_appr_hist AS a INNER JOIN
*           zarn_approval AS b ON b~guid = a~guid
*      INTO TABLE @lt_history_chg_ar[]
*      FOR ALL ENTRIES IN @lt_result
*      WHERE a~guid = @lt_result-guid AND
*            a~nat_version = @lt_result-version AND
*            a~status IN ( @zcl_arn_approval_backend=>gc_stat-posted ) .
*
*
*
*    " records which are only in approvals but not in appr_hist yet
** Now get the  POSTED so that we can work out the time difference
*    SELECT idno nat_version team_code chg_category updated_timestamp
*      FROM zarn_approval
*      APPENDING CORRESPONDING FIELDS OF TABLE lt_history_chg_ar[]
*      FOR ALL ENTRIES IN lt_result
*      WHERE guid = lt_result-guid AND
*            nat_version = lt_result-version AND
*            status IN ( zcl_arn_approval_backend=>gc_stat-posted ) .
*
*
*
*
** Now get the  approved ready so that we can work out the time difference
*    SELECT b~idno, a~nat_version, b~team_code, b~chg_category, a~updated_timestamp
*      FROM zarn_appr_hist AS a INNER JOIN
*           zarn_approval AS b ON b~guid = a~guid
*      APPENDING CORRESPONDING FIELDS OF TABLE @lt_history_chg_ar[]
*      FOR ALL ENTRIES IN @lt_result
*      WHERE a~guid = @lt_result-guid AND
*            a~nat_version = @lt_result-version AND
*            a~status IN ( @zcl_arn_approval_backend=>gc_stat-appr_ready ) .
*
*
*    me->mt_history_chg_ar[] = lt_history_chg_ar[].
*
** We only care about the earlist PO for a version for the below combination.
*    DELETE ADJACENT DUPLICATES FROM me->mt_history_chg_ar COMPARING idno nat_version.

  ENDMETHOD.


  METHOD get_awa_data.

* Perform the main selection to get the approved workflow analysis data
    SELECT a~idno, a~fan_id, a~description, a~version,
           b~ret_zzcatman, b~gil_zzcatman, b~initiation, b~matnr,
           c~team_code, c~chg_category, c~approval_area,
           d~updated_timestamp AS ap_timestamp, d~guid,
           e~nat_mc_code AS matkl,
           f~posted_on, f~posted_at
      FROM zarn_products AS a INNER JOIN
           zarn_reg_hdr AS b ON b~idno = a~idno INNER JOIN
           zarn_approval AS c ON c~idno = a~idno INNER JOIN
           zarn_appr_hist AS d ON d~guid = c~guid AND d~nat_version = a~version INNER JOIN
           zarn_nat_mc_hier AS e ON e~idno = a~idno AND e~version = a~version INNER JOIN
           zarn_prd_version AS f ON f~idno = a~idno AND f~version = a~version
      WHERE a~fan_id IN @me->ms_selections-fanid AND
            b~idno IN @me->ms_selections-idno AND
            b~initiation IN @me->ms_selections-initiation AND
*              b~matnr IN @me->ms_selections-matnr AND "We do
            ( b~ret_zzcatman IN @me->ms_selections-catman OR b~gil_zzcatman IN @me->ms_selections-catman ) AND
            c~team_code IN @me->ms_selections-team_code AND
            c~approval_area IN @me->ms_selections-approval_area AND
            c~chg_category IN @me->ms_selections-ccat AND
            d~updated_timestamp IN @me->ms_selections-wfca_ts AND
            d~status IN ( @zcl_arn_approval_backend=>gc_stat-approved, @zcl_arn_approval_backend=>gc_stat-rejected ) AND
            d~status_reason <> @zcl_arn_approval_backend=>gc_stat_reas-autoapproved

      INTO CORRESPONDING FIELDS OF TABLE @rt_approved_data.

  ENDMETHOD.

  METHOD get_pwa_data.

* Perform the main selection for pending workflow analysis
    SELECT a~idno, a~fan_id, a~description, a~version,
           b~ret_zzcatman, b~gil_zzcatman, b~initiation, b~matnr,
           c~team_code, c~chg_category, c~approval_area, c~updated_timestamp AS ar_timestamp,
           d~nat_mc_code AS matkl
      FROM zarn_products AS a INNER JOIN
           zarn_reg_hdr AS b ON b~idno = a~idno INNER JOIN
           zarn_approval AS c ON c~idno = b~idno AND c~nat_version = a~version INNER JOIN
           zarn_nat_mc_hier AS d ON d~idno = c~idno AND d~version = c~nat_version
      WHERE a~fan_id IN @me->ms_selections-fanid AND
            b~idno IN @me->ms_selections-idno AND
            b~initiation IN @me->ms_selections-initiation AND
            b~matnr IN @me->ms_selections-matnr AND
            ( b~ret_zzcatman IN @me->ms_selections-catman OR b~gil_zzcatman IN @me->ms_selections-catman ) AND
            c~team_code IN @me->ms_selections-team_code AND
            c~approval_area IN @me->ms_selections-approval_area AND
            c~chg_category IN @me->ms_selections-ccat AND
            c~updated_timestamp IN @me->ms_selections-wfca_ts AND
            c~status = @zcl_arn_approval_backend=>gc_stat-appr_ready
      INTO CORRESPONDING FIELDS OF TABLE @rt_pending_data.

  ENDMETHOD.


  METHOD get_post_data.

    DATA: lt_posted_items TYPE ztarn_sla_item,
          lt_object_id    TYPE TABLE OF zsarn_objectid.

* Perform the main selection to get the approved workflow analysis data
    SELECT a~idno, a~fan_id, a~description, a~version,
           b~ret_zzcatman, b~gil_zzcatman, b~initiation, b~matnr,
           c~team_code, c~chg_category, c~approval_area,
           d~updated_timestamp AS ap_timestamp, d~guid,
           e~nat_mc_code AS matkl,
           f~posted_on, f~posted_at,
           p~lifnr
      FROM zarn_products AS a INNER JOIN
           zarn_reg_hdr AS b ON b~idno = a~idno INNER JOIN
           zarn_approval AS c ON c~idno = a~idno INNER JOIN
           zarn_appr_hist AS d ON d~guid = c~guid AND d~nat_version = a~version INNER JOIN
           zarn_nat_mc_hier AS e ON e~idno = a~idno AND e~version = a~version INNER JOIN
           zarn_prd_version AS f ON f~idno = a~idno AND f~version = a~version INNER JOIN
           zarn_reg_pir AS p ON p~idno = a~idno
      WHERE a~fan_id IN @me->ms_selections-fanid AND
            b~idno IN @me->ms_selections-idno AND
            b~initiation IN @me->ms_selections-initiation AND
*              b~matnr IN @me->ms_selections-matnr AND "We do
            ( b~ret_zzcatman IN @me->ms_selections-catman OR b~gil_zzcatman IN @me->ms_selections-catman ) AND
            c~team_code IN @me->ms_selections-team_code AND
            c~approval_area IN @me->ms_selections-approval_area AND
            c~chg_category IN @me->ms_selections-ccat AND
            d~updated_timestamp IN @me->ms_selections-wfca_ts AND
            d~status IN ( @zcl_arn_approval_backend=>gc_stat-posted ) AND
            d~status_reason <> @zcl_arn_approval_backend=>gc_stat_reas-autoapproved AND
            p~lifnr IN @me->ms_selections-lifnr

*      WHERE f~posted_on IN @me->ms_selections-wfca AND
*            p~lifnr IN @me->ms_selections-lifnr AND
*          ( b~ret_zzcatman IN @me->ms_selections-catman OR b~gil_zzcatman IN @me->ms_selections-catman )

      INTO CORRESPONDING FIELDS OF TABLE @lt_posted_items.


    " records which are only in approvals but not in appr_hist yet
* Perform the main selection to get the approved workflow analysis data
    SELECT a~idno, a~fan_id, a~description, a~version,
           b~ret_zzcatman, b~gil_zzcatman, b~initiation, b~matnr,
           c~team_code, c~chg_category, c~approval_area,
           c~updated_timestamp AS ap_timestamp, c~guid,
*           d~updated_timestamp AS ap_timestamp, d~guid,
           e~nat_mc_code AS matkl,
           f~posted_on, f~posted_at,
           p~lifnr
      FROM zarn_products AS a INNER JOIN
           zarn_reg_hdr AS b ON b~idno = a~idno INNER JOIN
           zarn_approval AS c ON c~idno = a~idno INNER JOIN
*           zarn_appr_hist AS d ON d~guid = c~guid AND d~nat_version = a~version INNER JOIN
           zarn_nat_mc_hier AS e ON e~idno = a~idno AND e~version = a~version INNER JOIN
           zarn_prd_version AS f ON f~idno = a~idno AND f~version = a~version INNER JOIN
           zarn_reg_pir AS p ON p~idno = a~idno
      WHERE a~fan_id IN @me->ms_selections-fanid AND
            b~idno IN @me->ms_selections-idno AND
            b~initiation IN @me->ms_selections-initiation AND
*              b~matnr IN @me->ms_selections-matnr AND "We do
            ( b~ret_zzcatman IN @me->ms_selections-catman OR b~gil_zzcatman IN @me->ms_selections-catman ) AND
            c~team_code IN @me->ms_selections-team_code AND
            c~approval_area IN @me->ms_selections-approval_area AND
            c~chg_category IN @me->ms_selections-ccat AND
            c~updated_timestamp IN @me->ms_selections-wfca_ts AND
            c~status IN ( @zcl_arn_approval_backend=>gc_stat-posted ) AND
            c~status_reason <> @zcl_arn_approval_backend=>gc_stat_reas-autoapproved AND
*            d~updated_timestamp IN @me->ms_selections-wfca_ts AND
*            d~status IN ( @zcl_arn_approval_backend=>gc_stat-posted ) AND
*            d~status_reason <> @zcl_arn_approval_backend=>gc_stat_reas-autoapproved AND
            p~lifnr IN @me->ms_selections-lifnr

*      WHERE f~posted_on IN @me->ms_selections-wfca AND
*            p~lifnr IN @me->ms_selections-lifnr AND
*          ( b~ret_zzcatman IN @me->ms_selections-catman OR b~gil_zzcatman IN @me->ms_selections-catman )

      APPENDING CORRESPONDING FIELDS OF TABLE @lt_posted_items.


    rt_posted_data[] = lt_posted_items[].
    CLEAR lt_posted_items[].


    SORT rt_posted_data[].
    DELETE ADJACENT DUPLICATES FROM rt_posted_data[] COMPARING ALL FIELDS.






    CHECK rt_posted_data IS NOT INITIAL.

    lt_posted_items = rt_posted_data.

    lt_object_id = CORRESPONDING #( lt_posted_items MAPPING objectid = matnr ) .
    SORT lt_object_id.
    DELETE ADJACENT DUPLICATES FROM lt_object_id.

* Pull the article created history from change docs
    SELECT objectid AS matnr , udate, utime
      FROM cdhdr
      INTO TABLE @me->mt_article_create_history
      FOR ALL ENTRIES IN @lt_object_id
      WHERE objectid = @lt_object_id-objectid
        AND objectclas = 'MAT_FULL'
        AND change_ind = 'I'
        AND username   = 'RFC_ARENA'
        AND udate IN @me->ms_selections-wfca[].


* Pull the article changed history from change docs
    SELECT objectid AS matnr , udate, utime
      FROM cdhdr
      INTO TABLE @me->mt_article_update_history
      FOR ALL ENTRIES IN @lt_object_id
      WHERE objectid = @lt_object_id-objectid
        AND objectclas = 'MAT_FULL'
        AND change_ind = 'U'
        AND username   = 'RFC_ARENA'
        AND udate IN @me->ms_selections-wfca[].





  ENDMETHOD.

  METHOD determine_process_type.
* Determine process type(NEW or CHG)

* Process type(old or new)
    IF me->ms_control-pwa = abap_true.
      rv_process_type =
        COND #( WHEN is_result-matnr IS INITIAL THEN c_proc_type-new ELSE c_proc_type-change ).
    ELSE.
      DATA(ls_created_at) = VALUE #( me->mt_article_create_history[ matnr = is_result-matnr ] OPTIONAL ).
      CONVERT DATE ls_created_at-udate TIME ls_created_at-utime INTO TIME STAMP DATA(lv_created_ts) TIME ZONE 'UTC'.
      IF is_result-ap_timestamp <= lv_created_ts.
        rv_process_type = c_proc_type-new.
      ELSE.
        rv_process_type = c_proc_type-change.
      ENDIF.
    ENDIF.

  ENDMETHOD.

  METHOD determine_earliest_ar.
* Determine the earliest AR based on process type(NEW or CHG)

    IF is_result-process_type = c_proc_type-new.
* For new scenarios we need to ignore the chg_category and get the earliest AR
      LOOP AT me->mt_history_ar ASSIGNING FIELD-SYMBOL(<ls_history_ar>)
        WHERE idno = is_result-idno AND
              nat_version = is_result-version AND
              team_code = is_result-team_code.
        IF rv_ar_ts IS INITIAL.
          rv_ar_ts = <ls_history_ar>-updated_timestamp.
        ELSE.
          IF <ls_history_ar>-updated_timestamp < rv_ar_ts.
            rv_ar_ts = <ls_history_ar>-updated_timestamp.
          ENDIF.
        ENDIF.
      ENDLOOP.
    ELSE.
      rv_ar_ts =
        VALUE #( me->mt_history_ar[ idno = is_result-idno nat_version = is_result-version team_code = is_result-team_code chg_category = is_result-chg_category ]-updated_timestamp OPTIONAL ).
    ENDIF.

  ENDMETHOD.

  METHOD determine_earliest_ar_po.
* Determine the earliest AR based on process type(NEW or CHG)

    IF is_result-process_type = c_proc_type-new.
* For new scenarios we need to ignore the chg_category and get the earliest AR
      LOOP AT me->mt_history_ar_po ASSIGNING FIELD-SYMBOL(<ls_history_ar_po>)
        WHERE idno = is_result-idno AND
              nat_version = is_result-version AND
              team_code = is_result-team_code.
        IF rv_ar_ts IS INITIAL.
          rv_ar_ts = <ls_history_ar_po>-updated_timestamp.
        ELSE.
          IF <ls_history_ar_po>-updated_timestamp < rv_ar_ts.
            rv_ar_ts = <ls_history_ar_po>-updated_timestamp.
          ENDIF.
        ENDIF.
      ENDLOOP.
    ELSE.
      rv_ar_ts =
        VALUE #( me->mt_history_ar_po[ idno = is_result-idno nat_version = is_result-version ]-updated_timestamp OPTIONAL ).
    ENDIF.

  ENDMETHOD.


  METHOD determine_earliest_chg.
* Determine the earliest change based on process type(NEW or CHG)


    DATA: lv_first_posted TYPE zarn_e_appr_upd_tmstmp.

    CLEAR rv_ar_ts.

    IF is_result-process_type = c_proc_type-new.

    ELSE.
      lv_first_posted =
        VALUE #( me->mt_history_po[ idno        = is_result-idno
                                    nat_version = is_result-version ]-updated_timestamp OPTIONAL ).

      DATA(lt_history_po) = me->mt_history_po.

      DELETE lt_history_po WHERE updated_timestamp = lv_first_posted.

      rv_ar_ts =
        VALUE #( lt_history_po[ idno        = is_result-idno
                                    nat_version = is_result-version ]-updated_timestamp OPTIONAL ).

    ENDIF.

  ENDMETHOD.

ENDCLASS.               "lcl_approvals_sla
