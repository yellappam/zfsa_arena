*&---------------------------------------------------------------------*
*& Report  ZRARN_ARTICLE_UPDATE
*&
*&---------------------------------------------------------------------*
*&---------------------------------------------------------------------*
* PROJECT          # FSA
* SPECIFICATION    # 3133
* DATE WRITTEN     # 02.03.2016
* SYSTEM           # DE1
* SAP VERSION      # ECC6.0
* TYPE             # Report
* AUTHOR           # C90001363
*-----------------------------------------------------------------------
* TITLE            # Article Update Program
* PURPOSE          # Create/Update Articles from AReNa in ECC
*                  # and maintains version and status
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      #
*-----------------------------------------------------------------------
* CALLS TO         #
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------


REPORT zrarn_article_update MESSAGE-ID zarena_msg.


* Constant declarations
INCLUDE ziarn_article_update_constants.

* Top Include for Declarations
INCLUDE ziarn_article_update_top.

* Selection Screen
INCLUDE ziarn_article_update_sel.

* Main Processing Logic
INCLUDE ziarn_article_update_main.

* Subroutines
INCLUDE ziarn_article_update_sub.
