*&---------------------------------------------------------------------*
*& Include ZIARN_GUI_TOP             Report ZRARN_GUI
**
*&---------------------------------------------------------------------*
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 13.04.2018
* CHANGE No.       # Charm 9000003482; Jira CI18-554
* DESCRIPTION      # CI18-554 Recipe Management - Ingredient Changes
*                  # New regional table for Allergen Type overrides and
*                  # fields for Ingredients statement overrides in Arena.
*                  # New Ingredients Type field in the Scales block in
*                  # Arena and NONSAP tab in material master.
* WHO              # C90005557, Tessa Newman
*-----------------------------------------------------------------------
* DATE             # 06.06.2018
* CHANGE No.       # ChaRM 9000003675; JIRA CI18-506 NEW Range Status flag
* DESCRIPTION      # New Range Flag, Range Availability and Range Detail
*                  # fields added in Arena and on material master in Listing
*                  # tab and Basic Data Text tab (materiallongtext).
* WHO              # C90005557, Tessa Newman
*-----------------------------------------------------------------------
* DATE             # 06/09/2018
* CHANGE No.       # Charm #9000004244 - CR092/93 PIM schema changes
* DESCRIPTION      # Changes to cater for the new national tables due to
*                  # the schema changes. The new tables are ZARN_NET_QTY,
*                  # ZARN_SERVE_SIZE and ZARN_BIOORGANISM. The last table
*                  # replaces ZARN_ORGANISM. Going forward no data will
*                  # maintained in ZARN_ORGANISM but until a complete
*                  # cutover to the new table, in the GUI data will be
*                  # picked from the new table and only when not there will
*                  # use data from the old table.
* WHO              # C90001929, Aruna Mackenjee
*-----------------------------------------------------------------------
REPORT zrarn_gui MESSAGE-ID  zarena_msg.

TYPE-POOLS: icon.

TABLES:
* Control Tables
  zarn_ver_status,
  zarn_control,
  zarn_prd_version,
  zarn_cc_hdr,

* National Tables
  zarn_additives,
  zarn_addit_info,
  zarn_allergen,
  zarn_cdt_level,
  zarn_chem_char,
  zarn_diet_info,
  zarn_fb_ingre,
  zarn_growing,
  zarn_gtin_var,
  zarn_hsno,
  zarn_ins_code,
  zarn_list_price,
  zarn_nat_mc_hier,
  zarn_ntr_claims,
*  ZARN_NTR_CLAIMTX,

  zarn_onlcat,
  zarn_benefits,

  zarn_ntr_health,
  zarn_nutrients,
  zarn_organism,
  zarn_pir,
  zarn_pir_comm_ch,
  zarn_prep_type,
  zarn_products,
  zarn_products_ex,
  zarn_products_2,
*  ZARN_PROD_STR,
  zarn_uom_variant,

  zarn_bioorganism,
  zarn_net_qty,
  zarn_serve_size,

* Regional Tables
  zarn_reg_hdr,
  zarn_reg_banner,
  zarn_reg_onlcat,      "++ONLD-835 JKH 12.05.2017
  zarn_reg_txt,
  zarn_reg_uom,
  zarn_reg_ean,
  zarn_reg_pir,
  zarn_reg_sc,                                              "9000004661
*  zarn_reg_lst_prc,
  zarn_reg_std_ter,
  zarn_reg_dc_sell,
  zarn_reg_rrp,
  zarn_reg_hsno,
  zarn_reg_prfam,
  zarn_s_approval_sel,  "+3162
  zarn_teams, "+3162
  zarn_reg_artlink,             " ++ONED-217 JKH 11.11.2016
  zarn_reg_allerg,
  lfa1,

  zsarn_icare_alv.              "++3174 JKH 23.03.2017

*-----------------------------------------------------------------------*
*-----------------------------------------------------------------------*
TYPES:
  ty_s_table_mastr TYPE zarn_table_mastr,
  ty_t_table_mastr TYPE STANDARD TABLE OF zarn_table_mastr,

  BEGIN OF ty_s_tabname,
    tabname TYPE tabname,
  END OF ty_s_tabname,

  BEGIN OF ty_s_text,
    name1       TYPE name1_gp,
    wgbez       TYPE wgbez,
    vtext       TYPE bezei40,
    wwghb       TYPE wwghb,
    nat_mc_desc TYPE wgbez,
    maktx       TYPE maktx,
    issue_unit  TYPE meinh,
    order_unit  TYPE meinh,
  END OF ty_s_text,

  ty_key       TYPE SORTED TABLE OF zsarn_key WITH UNIQUE KEY idno version,
  ty_t_tabname TYPE STANDARD TABLE OF ty_s_tabname,

  BEGIN OF ty_line,
    line TYPE sylisel,
  END OF ty_line,

  BEGIN OF ty_gtin,
    num_base_units TYPE zarn_num_base_units,
    uom_code       TYPE zarn_uom_code,
    gtin_code      TYPE zarn_gtin_code,
  END OF ty_gtin,
  ty_multi_gtin TYPE STANDARD TABLE OF ty_gtin.

TYPES: BEGIN OF ty_outtab.
TYPES: checkbox TYPE xfeld.               "field for checkbox
* Extend output table by a field to dis- or enable cells for input.
TYPES: celltab TYPE lvc_t_styl.           "field to switch editability
       INCLUDE STRUCTURE zsarn_icare_alv.
       TYPES: END OF ty_outtab,
       ty_t_outtab TYPE STANDARD TABLE OF ty_outtab.             "++3174 JKH 22.03.2017

TYPES:
  BEGIN OF ty_art_uom,
    uom TYPE meinh,
  END OF ty_art_uom,

  BEGIN OF ty_mara,
    matnr TYPE matnr,
    meins TYPE meins,
    mtart TYPE mtart,
    matkl type matkl,
  END OF ty_mara,

  BEGIN OF ty_mvke,
    vkorg TYPE vkorg,
  END OF ty_mvke.

*BEGIN OF ty_cdt_code,
*  cdt_code TYPE  zarn_cdt_code,
*END OF ty_cdt_code.
TYPES: BEGIN OF gty_product,
         product TYPE zmd_e_host_product,
       END OF gty_product.
*DATA:       gt_product        TYPE TABLE OF gty_product,
*            gs_product        TYPE          gty_product,
*            gv_ucomm          TYPE          sy-ucomm.

DATA:       gv_cursor_line    TYPE i.  " added by ashish on 29.05.2014 - Defect 5320


TYPES: BEGIN OF ty_open_po,
         ebeln TYPE ebeln,
         ebelp TYPE ebelp,
         matnr TYPE matnr,
         meins TYPE bstme,
         werks TYPE werks_d,
         reswk TYPE reswk,
       END OF ty_open_po,

       BEGIN OF ty_open_so,
         vbeln       TYPE vbeln_va,
         posnr       TYPE posnr_va,
         matnr       TYPE matnr,
         vrkme       TYPE vrkme,
         kdmat       TYPE matnr_ku,
         wms_product TYPE zmd_e_host_product,
         kunnr       TYPE kunnr,
         werks       TYPE werks_d,
       END OF ty_open_so.

TYPES: BEGIN OF ty_open_po_so,
         product     TYPE zmd_e_host_product,
         inner_uom   TYPE zmd_host_inner_uom,
         shipper_uom TYPE zmd_host_shipper_uom,
         retail_uom  TYPE zmd_host_retail_uom, " used for store PO/SO
         ebeln       TYPE ebeln,
         ebelp       TYPE ebelp,
         matnr       TYPE matnr,
         meins       TYPE bstme,
         kunnr       TYPE kunnr,
         werks       TYPE werks_d,
       END OF ty_open_po_so,

       ty_t_open_po TYPE TABLE OF ty_open_po,
       ty_t_open_so TYPE TABLE OF ty_open_so,


       BEGIN OF ty_uom_scenerio,
         hyb_int_code TYPE zarn_hybris_internal_code,
       END OF ty_uom_scenerio,

* DEL Begin of Change CTS-138 JKH 21.11.2016
       BEGIN OF ty_s_artlink_det,
         matnr TYPE matnr,
         matkl TYPE matkl,
         attyp TYPE attyp,
         mtart TYPE mtart,
         maktx TYPE maktx,
       END OF ty_s_artlink_det,
* DEL End of Change CTS-138 JKH 21.11.2016

* INS Begin of Change 3174 JKH 06.03.2017
       BEGIN OF ty_s_search_sort,
         sequence          TYPE n,
         sort_by_attribute TYPE char30,
         sort_direction    TYPE char30,
         sap_attribute     TYPE char30,
       END OF ty_s_search_sort,
       ty_t_search_sort TYPE SORTED TABLE OF ty_s_search_sort
                        WITH NON-UNIQUE KEY sequence,

       BEGIN OF ty_s_vlog,
         message_icon  TYPE zarn_e_msg_icon,
         validation_id TYPE zarn_e_rl_valid_id,
         code          TYPE zarn_code,
         message_txt1  TYPE zarn_message_text,
         message_txt2  TYPE zarn_message_text,
       END OF ty_s_vlog,
       ty_t_vlog TYPE STANDARD TABLE OF ty_s_vlog.

TYPES: BEGIN OF ts_cluster_range,
         cluster_range TYPE zmd_e_cluster_range,
         description   TYPE bezei40,
       END   OF ts_cluster_range,
       tt_cluster_range TYPE SORTED TABLE OF ts_cluster_range WITH UNIQUE KEY cluster_range.
* INS End of Change 3174 JKH 06.03.2017

TYPES: tt_nat_alv_uom TYPE STANDARD TABLE OF zsarn_nat_alv_uom WITH DEFAULT KEY.

*-----------------------------------------------------------------------*
*-----------------------------------------------------------------------*
CONSTANTS:
  gc_gilmours_banner       TYPE vkorg            VALUE '3000',
  gc_dc_sos                TYPE bwscl            VALUE '2',
  gc_non_dc_sos            TYPE bwscl            VALUE '1',
  gc_status_new            TYPE zarn_reg_status  VALUE 'NEW',
  gc_status_apr            TYPE zarn_reg_status  VALUE 'APR',
  gc_status_wip            TYPE zarn_reg_status  VALUE 'WIP',
  gc_screen_0100           TYPE sydynnr          VALUE '0100',
  gc_icare_tab             TYPE syucomm          VALUE 'PUSH2',
  gc_arena_tab             TYPE syucomm          VALUE 'PUSH1',
  gc_appro_tab             TYPE syucomm          VALUE 'PUSH3',
  gc_nat_pb                TYPE char15           VALUE 'GV_NAT_PB_00',
  gc_reg_pb                TYPE char15           VALUE 'GV_REG_PB_00',
  gc_txt_app               TYPE char15           VALUE 'TXT_APPROVAL',
  gc_field_chkbox          TYPE char08           VALUE 'CHECKBOX',
  gc_reg_data              TYPE char12           VALUE 'LS_REG_DATA-',
  gc_prod_data             TYPE char13           VALUE 'LS_PROD_DATA-',
  gc_gt_prefix             TYPE char03           VALUE 'GT_',
  gc_gs_prefix             TYPE char03           VALUE 'GS_',
  gc_ts_prefix             TYPE char03           VALUE 'TS_',
  gc_nat                   TYPE char01           VALUE 'N', "  National Table
  gc_reg                   TYPE char01           VALUE 'R', "  Regional Table
  gc_lock_reg              TYPE eqegraname       VALUE 'ZSARN_REGIONAL_LOCK',
* National containers
  gc_nncc01                TYPE scrfname         VALUE 'N_NUTRI_CC01',
  gc_nucc02                TYPE scrfname         VALUE 'N_UOM_CC02',
  gc_nlcc03                TYPE scrfname         VALUE 'N_VPRICE_CC03',
  gc_nicc04                TYPE scrfname         VALUE 'N_INGR_CC04',
  gc_nacc04                TYPE scrfname         VALUE 'N_ADDI_CC04',
  gc_npcc05                TYPE scrfname         VALUE 'N_PIR_CC05',
  gc_necc06                TYPE scrfname         VALUE 'N_ALLE_CC06',
  gc_ndcc07                TYPE scrfname         VALUE 'N_DIET_CC07',
  gc_nccc08                TYPE scrfname         VALUE 'N_CHEM_CC08',
  gc_nocc09                TYPE scrfname         VALUE 'N_ORGA_CC09',
  gc_nncc10                TYPE scrfname         VALUE 'N_ADDIT_INFO',
  gc_nncc11                TYPE scrfname         VALUE 'N_HSNO',
  gc_nncc12                TYPE scrfname         VALUE 'N_COMM_CH',
  gc_nncc13                TYPE scrfname         VALUE 'N_CC_INS_CODE',
  gc_nncc14                TYPE scrfname         VALUE 'N_CC_CDT_CODE',
  gc_nncc15                TYPE scrfname         VALUE 'N_CC_NUTRI_CLAIM',
  gc_nncc16                TYPE scrfname         VALUE 'N_CC_NUTRI_STAR',
  gc_nncc17                TYPE scrfname         VALUE 'N_CC_PREP_TYPE',
  gc_nncc18                TYPE scrfname         VALUE 'N_CC_GROWING',
  gc_nncc19                TYPE scrfname         VALUE 'N_CC_NET_QTY',
  gc_nscc20                TYPE scrfname         VALUE 'N_CC_SERVE_SIZE',
  gc_nscc21                TYPE scrfname         VALUE 'N_CC_ONLCAT',
  gc_nscc22                TYPE scrfname         VALUE 'N_CC_BENEFITS',
* Regional Containers
*  gc_rhcc01                   TYPE scrfname         VALUE 'RHCC01',
  gc_rhtxt01               TYPE scrfname         VALUE 'R_CCTXT',     " TEXT_EDITOR Main
  gc_rhtxt02               TYPE scrfname         VALUE 'R_CCTXT02',   " TEXT_EDITOR Price Family
  gc_rhtxt03               TYPE scrfname         VALUE 'R_CCTXT03',   " TEXT_EDITOR Cust Short Descr - Nat   "++ONLD-835 JKH 26.05.2017
  gc_rhtxt04               TYPE scrfname         VALUE 'R_CCTXT04',   " TEXT_EDITOR Cust Med Descr   - Nat   "++ONLD-835 JKH 26.05.2017
  gc_rhtxt05               TYPE scrfname         VALUE 'R_CCTXT05',   " TEXT_EDITOR Cust Long Descr  - Nat   "++ONLD-835 JKH 26.05.2017
  gc_rhtxt06               TYPE scrfname         VALUE 'R_CCTXT06',   " TEXT_EDITOR Cust Short Descr - Reg   "++ONLD-835 JKH 26.05.2017
  gc_rhtxt07               TYPE scrfname         VALUE 'R_CCTXT07',   " TEXT_EDITOR Cust Med Descr   - Reg   "++ONLD-835 JKH 26.05.2017
  gc_rhtxt08               TYPE scrfname         VALUE 'R_CCTXT08',   " TEXT_EDITOR Cust Long Descr  - Reg   "++ONLD-835 JKH 26.05.2017
  gc_rhtxt09               TYPE scrfname         VALUE 'R_CCTXT09',   " National Ingredients Statement
  gc_reg_ingr_stmt         TYPE scrfname         VALUE 'R_REG_INGR_STMT',   " Regional Ingredients Statement
  gc_rhtxt10               TYPE scrfname         VALUE 'R_CCTXT10',   " TEXT_EDITOR Ingredients Statement Override - Reg
  gc_rhtxt11               TYPE scrfname         VALUE 'R_CCTXT11',   " TEXT_EDITOR Range Detail - Reg

  gc_ruomcc01              TYPE scrfname         VALUE 'R_UOM_CC01',  " Regional UOM
  gc_rpfcc02               TYPE scrfname         VALUE 'R_PF_CC02',   " Regional Price Family
  gc_rconcc01              TYPE scrfname         VALUE 'R_CON_CC01',  " Regional PIR
  gc_rconcc02              TYPE scrfname         VALUE 'R_CON_CC02',  " Standard Terms (Discount)
  gc_rconcc03              TYPE scrfname         VALUE 'R_CON_CC03',  " Regional RRP
  gc_rconcc04              TYPE scrfname         VALUE 'R_CON_CC04',  " Regional DC Sell
  gc_rconcc05              TYPE scrfname         VALUE 'R_CON_CC05',  " Regional HSNO
  gc_rconcc06              TYPE scrfname         VALUE 'R_CON_CC06',  " Art Link
  gc_rconcc07              TYPE scrfname         VALUE 'R_CON_CC07',  " Regional Allergen & Ingr Overide

  gc_rpircc05              TYPE scrfname         VALUE 'R_PIR_CC05',  " Regional PIR
  gc_rrbcc06               TYPE scrfname         VALUE 'R_RB_CC06',   " Regional Banner
  gc_reg_cluster_container TYPE scrfname         VALUE 'R_REG_CLUSTER_CC', " Regional Cluster
  gc_ronlcc01              TYPE scrfname         VALUE 'R_ONL_CC01',   "Regional Banner Online Tab
  gc_rgtcc07               TYPE scrfname         VALUE 'R_GTN_CC07',  " Regional EAN
  gc_roccc08               TYPE scrfname         VALUE 'R_OC_CC08',   " Regional Online Category      "++ONLD-835 JKH 12.05.2017
  gc_rsccc09               TYPE scrfname         VALUE 'R_SC_CC09',   " Supply Chain    9000004661
* National ALV structu        res
  gc_alv_worklist          TYPE tabname          VALUE 'ZSARN_ICARE_ALV',
  gc_alv_nat_nutri         TYPE tabname          VALUE 'ZARN_NUTRIENTS',
  gc_alv_nat_uom           TYPE tabname          VALUE 'ZSARN_NAT_ALV_UOM',
  gc_alv_nat_lprice        TYPE tabname          VALUE 'ZARN_LIST_PRICE',
  gc_alv_nat_ingre         TYPE tabname          VALUE 'ZARN_FB_INGRE',
  gc_alv_nat_addi          TYPE tabname          VALUE 'ZARN_ADDITIVES',
  gc_alv_nat_pir           TYPE tabname          VALUE 'ZARN_PIR',
  gc_alv_nat_alle          TYPE tabname          VALUE 'ZARN_ALLERGEN',
  gc_alv_nat_diet          TYPE tabname          VALUE 'ZARN_DIET_INFO',
  gc_alv_nat_chem          TYPE tabname          VALUE 'ZARN_CHEM_CHAR',
  gc_alv_nat_orga          TYPE tabname          VALUE 'ZARN_ORGANISM',
  gc_alv_nat_hier          TYPE tabname          VALUE 'ZARN_NAT_MC_HIER',
  gc_alv_nat_gtin          TYPE tabname          VALUE 'ZARN_GTIN_VAR',
  gc_alv_nat_prod          TYPE tabname          VALUE 'ZARN_PRODUCTS',
  gc_alv_nat_prod_ex       TYPE tabname          VALUE 'ZARN_PRODUCTS_EX',
  gc_alv_nat_addit_info    TYPE tabname          VALUE 'ZARN_ADDIT_INFO',
  gc_alv_nat_hsno          TYPE tabname          VALUE 'ZARN_HSNO',
  gc_alv_nat_comm_ch       TYPE tabname          VALUE 'ZARN_PIR_COMM_CH',
  gc_alv_nat_ins_code      TYPE tabname          VALUE 'ZARN_INS_CODE',
  gc_alv_nat_cdt_code      TYPE tabname          VALUE 'ZARN_CDT_LEVEL',
  gc_alv_nat_nutri_clm     TYPE tabname          VALUE 'ZARN_NTR_CLAIMS',
  gc_alv_nat_nutri_str     TYPE tabname          VALUE 'ZARN_NTR_HEALTH',
  gc_alv_nat_prep_type     TYPE tabname          VALUE 'ZARN_PREP_TYPE',
  gc_alv_nat_growing       TYPE tabname          VALUE 'ZARN_GROWING',
  gc_alv_nat_bioorg        TYPE tabname          VALUE 'ZARN_BIOORGANISM',
  gc_alv_nat_net_qty       TYPE tabname          VALUE 'ZARN_NET_QTY',
  gc_alv_nat_serve_size    TYPE tabname          VALUE 'ZARN_SERVE_SIZE',
  gc_alv_nat_onlcat        TYPE tabname          VALUE 'ZARN_ONLCAT',
  gc_alv_nat_benefits      TYPE tabname          VALUE 'ZARN_BENEFITS',

* Regional ALV structu        res
  gc_alv_reg_uom           TYPE tabname          VALUE 'ZSARN_REG_UOM_UI',
  gc_alv_reg_rb            TYPE tabname          VALUE 'ZSARN_REG_BANNER_UI',
  gc_alv_reg_cluster       TYPE tabname          VALUE 'ZSARN_REG_CLUSTER_UI',
  gc_alv_reg_online        TYPE tabname          VALUE 'ZSARN_REG_ONLINE_UI',   "Online Tab
  gc_alv_reg_onlcat        TYPE tabname          VALUE 'ZSARN_REG_ONLCAT',                "++ONLD-835 JKH 12.05.2017
  gc_alv_reg_onlcat_ui     TYPE tabname          VALUE 'ZSARN_REG_ONLCAT_UI',             "++ONLD-835 JKH 12.05.2017
  gc_alv_reg_pf            TYPE tabname          VALUE 'ZARN_REG_PRFAM',
  gc_alv_reg_pir           TYPE tabname          VALUE 'ZSARN_REG_PIR_UI',
  gc_alv_reg_gtn           TYPE tabname          VALUE 'ZSARN_REG_EAN_UI',
*  gc_alv_reg_lpr         TYPE tabname          VALUE 'ZARN_REG_LST_PRC',
  gc_alv_reg_dis           TYPE tabname          VALUE 'ZARN_REG_STD_TER',
  gc_alv_reg_dcs           TYPE tabname          VALUE 'ZARN_REG_DC_SELL',
  gc_alv_reg_rrp           TYPE tabname          VALUE 'ZARN_REG_RRP',
  gc_alv_reg_hsno          TYPE tabname          VALUE 'ZARN_REG_HSNO',
  gc_alv_reg_artlink       TYPE tabname          VALUE 'ZARN_REG_ARTLINK',      " ++ONED-217 JKH 11.11.2016
  gc_alv_reg_artlink_ui    TYPE tabname          VALUE 'ZSARN_REG_ARTLINK_UI',  " ++ONED-217 JKH 11.11.2016
  gc_alv_reg_hdr           TYPE tabname          VALUE 'ZARN_REG_HDR',
*  gc_alv_reg_lst_prc     TYPE tabname          VALUE 'ZARN_REG_LST_PRC',
  gc_alv_reg_txt           TYPE tabname          VALUE 'ZARN_REG_TXT',
  gc_zarn_reg_str          TYPE tabname          VALUE 'ZARN_REG_STR',       "++ONLD-835 JKH 26.05.2017
  gc_zarn_reg_allerg       TYPE tabname          VALUE 'ZARN_REG_ALLERG',
  gc_alv_reg_allerg_ui     TYPE tabname          VALUE 'ZSARN_REG_ALLERG_UI',
  gc_alv_reg_sc            TYPE tabname          VALUE 'ZSARN_REG_SC_UI',            "Supply Chain 9000004661
  gc_zarn_reg_sc           TYPE tabname          VALUE 'ZARN_REG_SC',                "Supply Chain 9000004661


  gc_zarn_ntr_claimtx      TYPE tabname          VALUE 'ZARN_NTR_CLAIMTX',
  gc_zarn_prod_str         TYPE tabname          VALUE 'ZARN_PROD_STR',
  gc_gcatman               TYPE fieldname        VALUE 'GIL_ZZCATMAN',
  gc_initiation            TYPE fieldname        VALUE 'INITIATION',
  gc_rcatman               TYPE fieldname        VALUE 'RET_ZZCATMAN',
  gc_catman                TYPE fieldname        VALUE 'ZZCATMAN',
  gc_gtin_current          TYPE fieldname        VALUE 'GTIN_CURRENT',
  gc_gtin_multi            TYPE fieldname        VALUE 'GTIN_MULTI',
  gc_source_uni            TYPE fieldname        VALUE 'SOURCE_UNI',
  gc_prodh                 TYPE fieldname        VALUE 'PRODUCT_HIERARCHY',
  gc_source_lni            TYPE fieldname        VALUE 'SOURCE_LNI',
  gc_uom_code              TYPE fieldname        VALUE 'UOM_CODE',
  gc_pim_uom_code          TYPE fieldname        VALUE 'PIM_UOM_CODE',
  gc_hyb_code              TYPE fieldname        VALUE 'HYBRIS_INTERNAL_CODE',
  gc_l_uom                 TYPE fieldname        VALUE 'LOWER_UOM',
  gc_lc_uom                TYPE fieldname        VALUE 'LOWER_CHILD_UOM',
  gc_uom_meins             TYPE fieldname        VALUE 'MEINH',
  gc_uom_eantp             TYPE fieldname        VALUE 'EANTP',
  gc_uom_l_meins           TYPE fieldname        VALUE 'LOWER_MEINH',
  gc_main_gtin             TYPE fieldname        VALUE 'HPEAN',
  gc_sstuf                 TYPE fieldname        VALUE 'SSTUF',
  gc_ktgrm                 TYPE fieldname        VALUE 'KTGRM',
  gc_vtext_sstuf           TYPE fieldname        VALUE 'VTEXT_SSTUF',
  gc_vrkme                 TYPE fieldname        VALUE 'VRKME',
  gc_vtext_ktgrm           TYPE fieldname        VALUE 'VTEXT_KTGRM',
  gc_vtext_banner          TYPE fieldname        VALUE 'VTEXT_BANNER',
  gc_text_source_uni       TYPE fieldname        VALUE 'TEXT_SOURCE_UNI',
  gc_text_source_lni       TYPE fieldname        VALUE 'TEXT_SOURCE_LNI',
  gc_name_zzcatman         TYPE fieldname        VALUE 'NAME_ZZCATMAN',
  gc_vtext_prodh           TYPE fieldname        VALUE 'VTEXT_PRODH',
  gc_online_status         TYPE fieldname        VALUE 'ONLINE_STATUS', "+ONLD440
  gc_text_online_status    TYPE fieldname        VALUE 'TEXT_ONLINE_STATUS', "+ONLD440
  gc_banner                TYPE fieldname        VALUE 'BANNER',
  gc_prerf                 TYPE fieldname        VALUE 'PRERF',
  gc_bunit                 TYPE fieldname        VALUE 'BASE_UNIT',
  gc_cunit                 TYPE fieldname        VALUE 'CONSUMER_UNIT',
  gc_dunit                 TYPE fieldname        VALUE 'DESPATCH_UNIT',
  gc_iunit                 TYPE fieldname        VALUE 'AN_INVOICE_UNIT',
  gc_unit_base             TYPE fieldname        VALUE 'UNIT_BASE',
  gc_unit_po               TYPE fieldname        VALUE 'UNIT_PURORD',
  gc_po_unit               TYPE fieldname        VALUE 'PO_UNIT',
  gc_po_eina               TYPE fieldname        VALUE 'PIR_REL_EINA',
  gc_po_eine               TYPE fieldname        VALUE 'PIR_REL_EINE',
  gc_po_ekkol              TYPE fieldname        VALUE 'EKKOL',
  gc_unit_sales            TYPE fieldname        VALUE 'SALES_UNIT',
  gc_unit_issue            TYPE fieldname        VALUE 'ISSUE_UNIT',
  gc_preq                  TYPE fieldname        VALUE 'PRICE_REQUIRED',
  gc_pnsflag               TYPE fieldname        VALUE 'PNS_TPR_FLAG',
  gc_zero_pr               TYPE fieldname        VALUE 'ALLOW_ZERO_PRICE',
  gc_dele_ind              TYPE fieldname        VALUE 'LOEKZ',
  gc_norm_vend             TYPE fieldname        VALUE 'RELIF',
  gc_idno                  TYPE fieldname        VALUE 'IDNO',
  gc_version               TYPE fieldname        VALUE 'VERSION',
  gc_client                TYPE fieldname        VALUE 'MANDT',
  gc_guid                  TYPE fieldname        VALUE 'GUID',
  gc_uuid                  TYPE dom_ref          VALUE 'SYSUUID',
  gc_alv_icon              TYPE fieldname        VALUE 'ICON',
  gc_fsni_icare_value      TYPE fieldname        VALUE 'FSNI_ICARE_VALUE',
  gc_alv_lock_user         TYPE fieldname        VALUE 'LOCK_USER',
  gc_alv_po                TYPE fieldname        VALUE 'EKORG',
  gc_alv_po_grp            TYPE fieldname        VALUE 'EKGRP',
  gc_alv_lifnr             TYPE fieldname        VALUE 'LIFNR',
  gc_alv_uebto             TYPE fieldname        VALUE 'UEBTO',
  gc_alv_lifab             TYPE fieldname        VALUE 'LIFAB',
  gc_alv_lifbi             TYPE fieldname        VALUE 'LIFBI',
  gc_alv_untto             TYPE fieldname        VALUE 'UNTTO',
*  gc_alv_con_grp         TYPE fieldname        VALUE 'CONDITION_GROUP',
*  gc_alv_art_lvl         TYPE fieldname        VALUE 'ARTICLE_LEVEL',
*  gc_alv_cond_term       TYPE fieldname        VALUE 'CONDITION_TERM',
  gc_alv_cond_type         TYPE fieldname        VALUE 'CONDITION_TYPE',
  gc_alv_sales_org         TYPE fieldname        VALUE 'SALES_ORG',
  gc_alv_dist_ch           TYPE fieldname        VALUE 'DISTRIBUTION_CHANNEL',
  gc_esokz                 TYPE fieldname        VALUE 'ESOKZ',
  gc_minbm                 TYPE fieldname        VALUE 'MINBM',
  gc_norbm                 TYPE fieldname        VALUE 'NORBM',
  gc_vabme                 TYPE fieldname        VALUE 'VABME',
  gc_vkorg                 TYPE fieldname        VALUE 'VKORG',
  gc_mwskz                 TYPE fieldname        VALUE 'MWSKZ',
  gc_aplfz                 TYPE fieldname        VALUE 'APLFZ',
  gc_allow_zero_price      TYPE fieldname        VALUE 'ALLOW_ZERO_PRICE',
  gc_loekz                 TYPE fieldname        VALUE 'LOEKZ',
  gc_alv_locked            TYPE char04           VALUE 'C600',
  gc_alv_selected          TYPE char04           VALUE 'C500',
  gc_alv_emphasize         TYPE char04           VALUE 'C211',
  gc_alv_rowcolor          TYPE char08           VALUE 'ROWCOLOR',
  gc_extension             TYPE i                VALUE 40,  "least number to shrink do not go lower !
  gc_celltab               TYPE lvc_fname        VALUE 'CELLTAB',
  gc_cat_seqno             TYPE fieldname        VALUE 'CAT_SEQNO',
  gc_seqno                 TYPE fieldname        VALUE 'SEQNO',
  gc_nat_ean_flag          TYPE lvc_fname        VALUE 'NAT_EAN_FLAG',
  gc_save                  TYPE char01           VALUE 'A', "The user may save all types of a layout
  gc_def_ean_cate          TYPE rvari_vnam       VALUE 'ZARN_GUI_DEFAULT_EAN_CATEGORY',
  gc_def_prefix_ean_cate   TYPE rvari_vnam       VALUE 'ZARN_GUI_DEFLT_EAN_PREFIX_CATE',
  gc_stvarv_post_dest      TYPE rvari_vnam       VALUE 'ZARN_ARTICLE_POST_LOGSYS',
  gc_vmsta                 TYPE fieldname        VALUE 'VMSTA',
  gc_vmstd                 TYPE fieldname        VALUE 'VMSTD',
  gc_obs_ind               TYPE fieldname        VALUE 'OBSOLETE_IND',
  gc_hsno_code             TYPE fieldname        VALUE 'HSNO_CODE',
  gc_banner_def_ret        TYPE c                VALUE 'R',
  gc_banner_def_gil        TYPE c                VALUE 'G',
  gc_bwscl_1               TYPE c                VALUE '1',
  gc_bwscl_2               TYPE c                VALUE '2',
  gc_yes                   TYPE char01           VALUE 'J',
  gc_no                    TYPE char01           VALUE 'N',
  gc_cancel                TYPE char01           VALUE 'A',
  "value of ZARN_FREF authority object, field FIELD_REF that allows change
  gc_auth_fref_change      TYPE char4            VALUE '0001',
  gc_pbs_code              TYPE fieldname        VALUE 'PBS_CODE',
  gc_scagr                 TYPE fieldname        VALUE 'SCAGR',
  gc_pbs_name              TYPE fieldname        VALUE 'PBS_NAME_TEXT',

* INS Begin of Change ONED-217 JKH 11.11.2016
  gc_hdr_scn               TYPE fieldname        VALUE 'HDR_SCN',
  gc_hdr_scn_drdn          TYPE fieldname        VALUE 'HDR_SCN_DRDN',
  gc_hdr_matnr             TYPE fieldname        VALUE 'HDR_MATNR',
  gc_hdr_maktx             TYPE fieldname        VALUE 'HDR_MAKTX',
  gc_hdr_attyp             TYPE fieldname        VALUE 'HDR_ATTYP',
  gc_hdr_mtart             TYPE fieldname        VALUE 'HDR_MTART',

  gc_link_scn              TYPE fieldname        VALUE 'LINK_SCN',
  gc_link_scn_drdn         TYPE fieldname        VALUE 'LINK_SCN_DRDN',
  gc_link_matnr            TYPE fieldname        VALUE 'LINK_MATNR',
  gc_link_maktx            TYPE fieldname        VALUE 'LINK_MAKTX',
  gc_link_attyp            TYPE fieldname        VALUE 'LINK_ATTYP',
  gc_link_mtart            TYPE fieldname        VALUE 'LINK_MTART',

  gc_drdn_hdr_scn          TYPE fieldname        VALUE 'DRDN_HDR_SCN',
  gc_drdn_link_scn         TYPE fieldname        VALUE 'DRDN_LINK_SCN',
* INS End of Change ONED-217 JKH 11.11.2016

  gc_allergen_type         TYPE fieldname VALUE 'ALLERGEN_TYPE',
  gc_alg_typ_ovr           TYPE fieldname VALUE 'ALLERGEN_TYPE_OVR',
  gc_alg_typ_ovr_src       TYPE fieldname VALUE 'ALLERGEN_TYPE_OVR_SRC',
  gc_alg_typ_ovr_date      TYPE fieldname VALUE 'ALLERGEN_TYPE_OVR_DATE',
  gc_lvl_containment       TYPE fieldname VALUE 'LVL_CONTAINMENT',
  gc_lvl_cont_ovr          TYPE fieldname VALUE 'LVL_CONTAINMENT_OVR',
  gc_lvl_cont_ovr_src      TYPE fieldname VALUE 'LVL_CONTAINMENT_OVR_SRC',
  gc_lvl_cont_ovr_date     TYPE fieldname VALUE 'LVL_CONTAINMENT_OVR_DATE',
  gc_allergen_type_reg     TYPE fieldname VALUE 'ALLERGEN_TYPE_REG',
  gc_lvl_containment_reg   TYPE fieldname VALUE 'LVL_CONTAINMENT_REG',

* INS Begin of Change 3174 JKH 06.03.2017
  gc_sort_asc              TYPE char20 VALUE 'ASC',
  gc_sort_desc             TYPE char20 VALUE 'DESC',
  gc_online                TYPE char10 VALUE 'Online',
  gc_staged                TYPE char10 VALUE 'Staged',
  gc_cr_r1_switch          TYPE rvari_vnam VALUE 'ZARN_CR_R1_SWITCH',
  gc_cr_r3_switch          TYPE rvari_vnam VALUE 'ZARN_CR_R3_SWITCH',
  gc_enrich_sla            TYPE rvari_vnam VALUE 'ZARN_ENRICH_SLA',
* INS End of Change 3174 JKH 06.03.2017



  BEGIN OF gc_fcode,
    show_uom TYPE sy-ucomm VALUE 'SHOW_UOM',
    hide_uom TYPE sy-ucomm VALUE 'HIDE_UOM',
  END OF gc_fcode.

CONSTANTS: gc_feature_a4(2) VALUE 'A4'.  "Online Categories and UoM Descriptions


* Approvals
INCLUDE ziarn_gui_top_approve.

CONSTANTS:
  co_appr_mode_approve      TYPE c VALUE 'A',
  co_appr_mode_reject       TYPE c VALUE 'R',
  co_appr_mode_add_text     TYPE c VALUE 'T',
  co_appr_mode_display_text TYPE c VALUE 'D',
  co_appr_result_approved   TYPE c VALUE 'A',
  co_appr_result_rejected   TYPE c VALUE 'R',
  co_appr_result_reset      TYPE c VALUE 'S',
  co_appr_result_cancelled  TYPE c VALUE 'C',
  co_appr_result_text_added TYPE c VALUE 'T',
  co_appr_column_status     TYPE char4 VALUE 'STAT',
  co_appr_column_approve    TYPE char4 VALUE 'APPR',
  co_appr_column_reject     TYPE char4 VALUE 'REJE',
  co_appr_column_text       TYPE char4 VALUE 'TEXT'.

DATA:
  gv_url_doculink            TYPE string,
  gv_ref_purch_org           TYPE ekorg,
  gv_ref_sales_org           TYPE vkorg,
  gv_text_size               TYPE smp_dyntxt,
  gv_display                 TYPE xfeld,
  gv_zztktprnt               TYPE xfeld,
  gv_display_default         TYPE xfeld,
  gv_extension               TYPE i,
  gv_execute                 TYPE char01,
  gv_edit_toggle             TYPE int4,
  gv_prfam_done_text         TYPE ui_func, " 'Done',
  gv_reg_okcode              TYPE ui_func,
  gv_nat_okcode              TYPE ui_func,
  gv_appr_reg_okcode         TYPE ui_func,
  gv_appr_nat_okcode         TYPE ui_func,
  gv_okcode                  TYPE ui_func,
  gv_save_okcode             TYPE ui_func,
  gv_appr_syucomm            TYPE ui_func,
  gv_nat_approved            TYPE ui_func,
  gv_nat_okcode_uom          TYPE ui_func,
  gv_nat_okcode_pir          TYPE ui_func,
  gv_nat_okcode_lp           TYPE ui_func,
  gv_nat_okcode_fsi          TYPE ui_func, " food safety info
  gv_nat_okcode_ci           TYPE ui_func, " consumer info
  gv_nat_okcode_nai          TYPE ui_func, " Nutri/Ingre. info
  gv_nat_okcode_ai           TYPE ui_func, " Add. info
  gv_nat_okcode_dgi          TYPE ui_func, " Dang. goods info
  gv_nat_okcode_ingr         TYPE ui_func, " Ingredient information
  gv_nat_okcode_onlcat       TYPE ui_func, " Online Categories
  gv_nat_okcode_benefit      TYPE ui_func, " Benefits
  gv_sel_tab                 TYPE ui_func,  "required to check in method !
  gv_data_change             TYPE xfeld,
  gv_screen_no               TYPE sy-dynnr VALUE '0201',
  gv_rscreen_no              TYPE sy-dynnr VALUE '0203',
  gv_appr_screen_no          TYPE sy-dynnr VALUE '0702',
  gv_appr_rscreen_no         TYPE sy-dynnr VALUE '0702',
  gv_uom_rscreen_no          TYPE sy-dynnr VALUE '0704',
  gv_lp_rscreen_no           TYPE sy-dynnr VALUE '0706',
  gv_pir_rscreen_no          TYPE sy-dynnr VALUE '0708',
  gv_fsi_rscreen_no          TYPE sy-dynnr VALUE '0711',
  gv_ci_rscreen_no           TYPE sy-dynnr VALUE '0713',
  gv_nai_rscreen_no          TYPE sy-dynnr VALUE '0715',
  gv_ai_rscreen_no           TYPE sy-dynnr VALUE '0717',
  gv_dgi_rscreen_no          TYPE sy-dynnr VALUE '0719',
  gv_ingr_rscreen_no         TYPE sy-dynnr VALUE '0721',
  gv_onlcat_rscreen_no       TYPE sy-dynnr VALUE '0723',
  gv_benefit_rscreen_no      TYPE sy-dynnr VALUE '0725',
  gv_empty_screen_no         TYPE sy-dynnr VALUE '0999',
  gv_reg_cluster_scr_no      TYPE sy-dynnr VALUE '0116',
  gv_reg_cluster_scr         TYPE sy-dynnr,
  gv_nat_pb_01               TYPE char30,
  gv_reg_pb_01               TYPE char30,
  gv_reg_pb_cluster_expanded TYPE flag,
  gv_reg_pb_cluster          TYPE iconname,
  gv_nat_pb_00               TYPE char30,
  gv_reg_pb_00               TYPE char30,
  gv_nat_pb_02               TYPE char30,
  gv_nat_pb_03               TYPE char30,
  gv_nat_pb_04               TYPE char30,
  gv_nat_pb_05               TYPE char30,
  gv_nat_pb_06               TYPE char30,
  gv_nat_pb_07               TYPE char30,
  gv_nat_pb_08               TYPE char30,
  gv_nat_pb_09               TYPE char30,
  gv_nat_pb_10               TYPE char30,
  gv_nat_pb_11               TYPE char30,
  gv_nat_pb_12               TYPE char30,
  gv_nat_fsi                 TYPE boole_d,
  gv_nat_ai                  TYPE boole_d,
  gv_nat_ci                  TYPE boole_d,
  gv_nat_dgi                 TYPE boole_d,
  gv_nat_ni                  TYPE boole_d,     " National - Nutritional Information
  gv_nat_ii                  TYPE boole_d,     " National - Ingredient Information
  gv_matnr                   TYPE matnr,
  gv_row_index               TYPE sy-tabix,
  gv_row_total               TYPE sy-tabix,
  gv_set_default_cursor      TYPE abap_bool,
  gv_string                  TYPE string,

  gv_zattr6_def              TYPE char1, "flag,

  gs_variant                 TYPE disvariant,


  gv_field_ref               TYPE zarn_e_gui_field_ref,
  gv_sel_scr_tab             TYPE sy-ucomm,
  gv_tabname                 TYPE tabname,
  gv_fldname                 TYPE fieldname,
  gv_new                     TYPE xfeld,
  gv_existing                TYPE xfeld,
  gv_change                  TYPE xfeld,
  gs_screen                  TYPE screen,
  gs_gui_set                 TYPE zarn_gui_set,
  gs_gui_sel                 TYPE zarn_gui_sel,

  gv_banner_def_ret          TYPE flag,
  gv_banner_def_gil          TYPE flag,
  gv_toolbar_ucomm           TYPE sy-ucomm,

  BEGIN OF gs_control,
    nat_create_name TYPE ad_namtext,
    nat_change_name TYPE ad_namtext,
    reg_create_name TYPE ad_namtext,
    reg_change_name TYPE ad_namtext,
  END OF gs_control,

  BEGIN OF gs_zarn_prd_version,
    cversion_status TYPE zarn_prd_version-version_status,
    aversion_status TYPE zarn_prd_version-version_status,
    lversion_status TYPE zarn_prd_version-version_status,
  END OF gs_zarn_prd_version,

  BEGIN OF gs_price,
    list_unit TYPE zarn_dis_term1,
    settle    TYPE zfsvend-supdsc,
*    dc_sell   TYPE zarn_dis_term1,
  END OF gs_price,

  gt_tvarv_ean_cate    TYPE tvarvc_t,
  gt_values            TYPE STANDARD TABLE OF usvalues,
  gs_values            LIKE LINE OF gt_values,
  gv_auth_display_only TYPE kennzx, "IS - cater for display only auths
  gt_bdcdata           TYPE STANDARD TABLE OF bdcdata,
  gt_tabname           TYPE ty_t_table_mastr,
  gt_rows              TYPE lvc_t_row,
  gt_data              TYPE STANDARD TABLE OF zsarn_icare_alv,
  gt_data_org          TYPE STANDARD TABLE OF zsarn_icare_alv,
  gs_worklist          TYPE ty_outtab,
  gt_worklist          TYPE STANDARD TABLE OF ty_outtab,
  gt_worklist_orig     TYPE STANDARD TABLE OF ty_outtab.


DATA:
  gt_line              TYPE STANDARD TABLE OF ty_line,
  gt_line_prfam        TYPE STANDARD TABLE OF ty_line,
* ALV screens Related
  gs_nat_alv_uom       TYPE zsarn_nat_alv_uom,
  gt_nat_alv_uom       TYPE STANDARD TABLE OF zsarn_nat_alv_uom,

  gt_zarn_prod_uom_t   TYPE STANDARD TABLE OF zarn_prod_uom_t,
  gt_zarn_uom_cat      TYPE STANDARD TABLE OF  zarn_uom_cat,
  gt_prd_version       TYPE STANDARD TABLE OF  zarn_prd_version,
  gt_ver_status        TYPE STANDARD TABLE OF  zarn_ver_status,
  gt_control           TYPE STANDARD TABLE OF  zarn_control,
  gt_cc_hdr            TYPE STANDARD TABLE OF  zarn_cc_hdr,
  gt_cc_det            TYPE SORTED TABLE OF    zarn_cc_det WITH NON-UNIQUE KEY chgid idno,
  gt_prod_str          TYPE STANDARD TABLE OF  zarn_prod_str,
  gs_prod_str          TYPE                    zarn_prod_str,
  gt_t006              TYPE TABLE OF t006,
  gt_md_category       TYPE zmd_t_zmd_category,                                 "++3174 JKH 21.03.2017
  gt_bu_level          TYPE ztonl_bu_level,                                     "++ONLD-835 JKH 16.05.2017
  gt_onl_category      TYPE ztonl_category,                                     "++ONLD-835 JKH 16.05.2017
  gt_zarn_allergen_t   TYPE SORTED TABLE OF zarn_allergen_t WITH UNIQUE KEY allergen_type,
  gt_zarn_launchp_t    TYPE TABLE OF zarn_launchp_t,        "9000004661
  gt_zarn_order_app_t  TYPE TABLE OF zarn_order_app_t,      "9000004661
  gt_zarn_npd_del_t    TYPE TABLE OF zarn_npd_del_t,        "9000004661

**Deep structures for national and regional data
  gs_prod_data_o       TYPE zsarn_prod_data,
  gs_prod_data_n       TYPE zsarn_prod_data,
  gs_prod_data         TYPE zsarn_prod_data,
  gt_prod_data         TYPE ztarn_prod_data,

  gs_reg_data_o        TYPE zsarn_reg_data,
  gs_reg_data_n        TYPE zsarn_reg_data,
  gs_reg_data          TYPE zsarn_reg_data,
  gs_reg_data_copy     TYPE zsarn_reg_data,
  gs_reg_data_db       TYPE zsarn_reg_data,
  gs_reg_data_save     TYPE zsarn_reg_data,
  gt_reg_data          TYPE ztarn_reg_data,

** National Tables
  gt_products          TYPE  ztarn_products,
  gt_products_ex       TYPE  ztarn_products_ex,
  gt_cdt_level         TYPE  ztarn_cdt_level,
  gt_zarn_nutrients    TYPE  ztarn_nutrients,
  gt_zarn_ntr_health   TYPE  ztarn_ntr_health,
  gt_ntr_claimtx       TYPE  ztarn_ntr_claimtx,
  gt_zarn_ntr_claims   TYPE  ztarn_ntr_claims,
  gt_zarn_allergen     TYPE  ztarn_allergen,
  gt_hsno              TYPE  ztarn_hsno,
  gt_dgi_margin        TYPE  ztarn_dgi_margin,
  gt_zarn_prep_type    TYPE  ztarn_prep_type,
  gt_ins_code          TYPE  ztarn_ins_code,
  gt_zarn_diet_info    TYPE  ztarn_diet_info,
  gt_zarn_additives    TYPE  ztarn_additives,
  gt_zarn_growing      TYPE  ztarn_growing,

  gt_zarn_fb_ingre     TYPE  ztarn_fb_ingre,
  gt_zarn_chem_char    TYPE  ztarn_chem_char,
  gt_zarn_organism     TYPE  ztarn_organism,
  gt_nat_mc_hier       TYPE  ztarn_nat_mc_hier,
  gt_zarn_addit_info   TYPE  ztarn_addit_info,
  gt_zarn_uom_variant  TYPE  ztarn_uom_variant,
  gt_zarn_gtin_var     TYPE  ztarn_gtin_var,
  gt_zarn_pir          TYPE  ztarn_pir,
  gt_pir_comm_ch       TYPE  ztarn_pir_comm_ch,
  gt_zarn_list_price   TYPE  ztarn_list_price,
  gt_zarn_hsno         TYPE  ztarn_hsno,
  gt_zarn_pir_comm_ui  TYPE  ztarn_pir_comm_ch,
  gt_zarn_pir_comm_ch  TYPE  ztarn_pir_comm_ch,
  gt_zarn_cdt_level    TYPE  ztarn_cdt_level,
*  gt_zarn_cdt_lvl_ui  TYPE  TABLE OF ty_cdt_code,
  gt_zarn_ins_code     TYPE  ztarn_ins_code,
  gt_marm_uom          TYPE  TABLE OF ty_art_uom,
  gt_mvke              TYPE  TABLE OF ty_mvke,

  gt_zarn_bioorganism  TYPE ztarn_bioorganism,
  gt_zarn_net_qty      TYPE ztarn_net_qty,
  gt_zarn_serve_size   TYPE ztarn_serve_size,

  gt_zarn_onlcat       TYPE ztarn_onlcat,
  gt_zarn_benefits     TYPE ztarn_benefits,

** National structures
  gs_cdt_level         LIKE LINE OF  gt_cdt_level,
  gs_nutrients         LIKE LINE OF  gt_zarn_nutrients,
*  gs_ntr_health       LIKE LINE OF  gt_ntr_health,
  gs_ntr_claimtx       LIKE LINE OF  gt_ntr_claimtx,
*  gs_ntr_claims       LIKE LINE OF  gt_ntr_claims,
  gs_allergen          LIKE LINE OF  gt_zarn_allergen,
  gs_hsno              LIKE LINE OF  gt_hsno,
  gs_dgi_margin        LIKE LINE OF  gt_dgi_margin,
*  gs_prep_type        LIKE LINE OF  gt_prep_type,
  gs_ins_code          LIKE LINE OF  gt_ins_code,
  gs_diet_info         LIKE LINE OF  gt_zarn_diet_info,
  gs_additives         LIKE LINE OF  gt_zarn_additives,
  gs_fb_ingre          LIKE LINE OF  gt_zarn_fb_ingre,
  gs_chem_char         LIKE LINE OF  gt_zarn_chem_char,
  gs_organism          LIKE LINE OF  gt_zarn_organism,
  gs_nat_mc_hier       LIKE LINE OF  gt_nat_mc_hier,
  gs_addit_info        LIKE LINE OF  gt_zarn_addit_info,
  gs_uom_variant       LIKE LINE OF  gt_zarn_uom_variant,
  gs_gtin_var          LIKE LINE OF  gt_zarn_gtin_var,
  gs_pir               LIKE LINE OF  gt_zarn_pir,
  gs_pir_comm_ch       LIKE LINE OF  gt_pir_comm_ch,
  gs_list_price        LIKE LINE OF  gt_zarn_list_price,
  gs_mara              TYPE ty_mara,
  gs_mara_mc_ref       TYPE ty_mara,

  gs_zarn_onlcat       LIKE LINE OF gt_zarn_onlcat,
  gs_zarn_benefits     LIKE LINE OF gt_zarn_benefits,

  gt_nsap_leg_uom      TYPE ztmd_nsap_leg_uom,
  gt_hyb_int_code      TYPE TABLE OF ty_uom_scenerio,


** Regional tables
  gt_zarn_reg_hdr      TYPE ztarn_reg_hdr,
  gt_zarn_reg_banner   TYPE ztarn_reg_banner_ui,
  gt_zarn_reg_cluster  TYPE ztarn_reg_cluster_ui,
  gt_zarn_online_desc  TYPE ztarn_reg_online_ui,
  gt_zarn_reg_onlcat   TYPE ztarn_reg_onlcat_ui,                 "++ONLD-835 JKH 12.05.2017
  gt_zarn_reg_ean      TYPE ztarn_reg_ean_ui,
  gt_zarn_reg_pir      TYPE ztarn_reg_pir_ui,
  gt_zarn_reg_prfam    TYPE ztarn_reg_prfam,
  gt_zarn_reg_txt      TYPE ztarn_reg_txt,
  gt_zarn_reg_uom      TYPE ztarn_reg_uom_ui,
  gt_zarn_reg_hsno     TYPE ztarn_reg_hsno,
  gt_zarn_reg_artlink  TYPE ztarn_reg_artlink_ui,                "++ONED-217 JKH 11.11.2016
  gt_drdn_reg_artlink  TYPE lvc_t_drop,                          "++ONED-217 JKH 11.11.2016
  gt_zarn_reg_str      TYPE ztarn_reg_str,                       "++ONLD-835 JKH 26.05.2017
  gt_zarn_reg_allerg   TYPE ztarn_reg_allerg_ui,
  gt_zarn_reg_sc       TYPE ztarn_reg_sc,
  gt_zarn_reg_nutrien  TYPE ztarn_reg_nutrien,

  gt_filter_uom        TYPE lvc_t_filt,

  gt_t141              TYPE SORTED TABLE OF t141 WITH UNIQUE KEY mmsta, "++BOLT-318 Jitin 08.11.2016
  gt_tvms              TYPE SORTED TABLE OF tvms WITH UNIQUE KEY vmsta, "++BOLT-318 Jitin 08.11.2016

* Regional condition tables - 4
*  gt_zarn_reg_dc_sell TYPE  ztarn_reg_dc_sell,
  gt_zarn_reg_rrp      TYPE ztarn_reg_rrp,
*  gt_zarn_reg_lst_prc TYPE ztarn_reg_lst_prc,
  gt_zarn_reg_std_ter  TYPE ztarn_reg_std_ter,

* Ranges for upper case selection
  gr_short_descr       TYPE RANGE OF zarn_fs_short_descr_upper,
  gr_gln_descr         TYPE RANGE OF zarn_gln_descr_upper,

** Regional structures
  gs_zarn_reg_banner   LIKE LINE OF gt_zarn_reg_banner,
  gs_reg_hdr           LIKE LINE OF gt_zarn_reg_hdr,
  gs_reg_banner        LIKE LINE OF gt_zarn_reg_banner,
  gs_reg_ean           LIKE LINE OF gt_zarn_reg_ean,
  gs_reg_pir           LIKE LINE OF gt_zarn_reg_pir,
  gs_reg_prfam         LIKE LINE OF gt_zarn_reg_prfam,
  gs_reg_txt           LIKE LINE OF gt_zarn_reg_txt,
  gs_reg_uom           LIKE LINE OF gt_zarn_reg_uom,
  gs_reg_rrp           LIKE LINE OF gt_zarn_reg_rrp,
  gs_reg_hsno          LIKE LINE OF gt_zarn_reg_hsno,
  gs_reg_artlink       LIKE LINE OF gt_zarn_reg_artlink,    "++ONED-217 JKH 11.11.2016
  gs_reg_onlcat        LIKE LINE OF gt_zarn_reg_onlcat,     "++ONLD-835 JKH 12.05.2017
  gs_reg_str           LIKE LINE OF gt_zarn_reg_str,        "++ONLD-835 JKH 26.05.2017
  gs_reg_allerg        LIKE LINE OF gt_zarn_reg_allerg,
  gs_reg_sc            LIKE LINE OF gt_zarn_reg_sc,

*  gs_reg_lst_prc      LIKE LINE OF  gt_zarn_reg_lst_prc,

  gt_multi_gtin        TYPE ty_multi_gtin,


  gt_basic_product     TYPE zproduct_lean_search_resp_tab3,    "++3174 JKH 06.03.2017
  gt_search_sort       TYPE ty_t_search_sort,                  "++3174 JKH 06.03.2017
  gt_vlog              TYPE ty_t_vlog,                         "++3174 JKH 06.03.2017
  gv_cr_r1_switch      TYPE flag,                              "++3174 JKH 06.03.2017
  gv_start_index       TYPE int4,                              "++3174 JKH 06.03.2017
  gv_tot_search_cnt    TYPE int4,                              "++3174 JKH 06.03.2017
  gv_smore_disable     TYPE flag,                              "++3174 JKH 06.03.2017
  gv_worklist_cnt      TYPE int4,                              "++3174 JKH 06.03.2017
  gv_search_cnt        TYPE int4,                              "++3174 JKH 06.03.2017
  gv_error_cnt         TYPE int4,                              "++3174 JKH 06.03.2017
  gv_valerr_cnt        TYPE int4,                              "++3174 JKH 06.03.2017
  gv_vlog_enable       TYPE flag,                              "++3174 JKH 06.03.2017


  gv_cr_r3_switch      TYPE flag,                              "++3174 JKH 06.03.2017
  gv_rereq_auth        TYPE flag,                              "++3174 JKH 06.03.2017
  gv_brand_id_desc     TYPE wrf_brand_descr,

  gv_bunit_drdn        TYPE zonl_bunit,                       "++ONLD-835 JKH 15.05.2017
  gv_catalog_type_drdn TYPE zonl_catalog_type,                "++ONLD-835 JKH 15.05.2017


* Text tables for screen
  gt_brands            TYPE  STANDARD TABLE OF wrf_brands,
  gt_t179t             TYPE  STANDARD TABLE OF t179t,
  gs_t179t             TYPE                    t179t,
  gs_text              TYPE                    ty_s_text.


DATA: gv_feature_a4_enabled TYPE boole_d.

*-----------------------------------------------------------------------*
* Controls
*-----------------------------------------------------------------------*
DATA:
  go_docking_alv              TYPE REF TO cl_gui_docking_container,
  go_dialogbox_container      TYPE REF TO cl_gui_dialogbox_container,
  go_dialogbox_cont_gtin      TYPE REF TO cl_gui_dialogbox_container,
  go_worklist_alv             TYPE REF TO cl_gui_alv_grid,
  go_ver_stat_alv             TYPE REF TO cl_gui_alv_grid,
  go_nutri_alv                TYPE REF TO cl_gui_alv_grid,
  go_nat_uom_alv              TYPE REF TO cl_gui_alv_grid,
  go_nat_price_alv            TYPE REF TO cl_gui_alv_grid,
  go_nat_ingr_alv             TYPE REF TO cl_gui_alv_grid,
  go_nat_addi_alv             TYPE REF TO cl_gui_alv_grid,
  go_nat_addit_info_alv       TYPE REF TO cl_gui_alv_grid,
  go_nat_hsno_alv             TYPE REF TO cl_gui_alv_grid,
  go_nat_comm_ch_alv          TYPE REF TO cl_gui_alv_grid,
  go_nat_alle_alv             TYPE REF TO cl_gui_alv_grid,
  go_nat_diet_alv             TYPE REF TO cl_gui_alv_grid,
  go_nat_organ_alv            TYPE REF TO cl_gui_alv_grid,
  go_nat_chem_alv             TYPE REF TO cl_gui_alv_grid,
  go_nat_ins_code_alv         TYPE REF TO cl_gui_alv_grid,
  go_nat_cdt_code_alv         TYPE REF TO cl_gui_alv_grid,
  go_nat_nutri_clm_alv        TYPE REF TO cl_gui_alv_grid,
  go_nat_nutri_str_alv        TYPE REF TO cl_gui_alv_grid,
  go_nat_prep_type_alv        TYPE REF TO cl_gui_alv_grid,
  go_nat_growing_alv          TYPE REF TO cl_gui_alv_grid,
  go_nat_pir_alv              TYPE REF TO cl_gui_alv_grid,
  go_nat_net_qty_alv          TYPE REF TO cl_gui_alv_grid,
  go_nat_serve_size_alv       TYPE REF TO cl_gui_alv_grid,
  go_nat_onlcat_alv           TYPE REF TO cl_gui_alv_grid,
  go_nat_benefits_alv         TYPE REF TO cl_gui_alv_grid,
  go_reg_uom_alv              TYPE REF TO cl_gui_alv_grid,
  go_reg_gtn_alv              TYPE REF TO cl_gui_alv_grid,
  go_reg_pir_alv              TYPE REF TO cl_gui_alv_grid,
  go_reg_rb_alv               TYPE REF TO cl_gui_alv_grid,
  go_reg_cluster_alv          TYPE REF TO cl_gui_alv_grid,
  go_reg_onl_alv              TYPE REF TO cl_gui_alv_grid,        "Online Tab
  go_reg_sc_alv               TYPE REF TO cl_gui_alv_grid,        "Supply Chain Tab 9000004661
  go_reg_pf_alv               TYPE REF TO cl_gui_alv_grid,
*  go_reg_con_dcs_alv     TYPE REF TO cl_gui_alv_grid,
  go_reg_con_rrp_alv          TYPE REF TO cl_gui_alv_grid,
  go_reg_con_hsno_alv         TYPE REF TO cl_gui_alv_grid,
  go_reg_con_artlink_alv      TYPE REF TO cl_gui_alv_grid,        "++ONED-217 JKH 11.11.2016
  go_reg_con_onlcat_alv       TYPE REF TO cl_gui_alv_grid,        "++ONLD-835 JKH 12.05.2017
  go_reg_con_lpr_alv          TYPE REF TO cl_gui_alv_grid,
  go_reg_con_dis_alv          TYPE REF TO cl_gui_alv_grid,
  go_reg_allerg_alv           TYPE REF TO cl_gui_alv_grid,
  go_reg_editor               TYPE REF TO cl_gui_textedit,
  go_reg_editor_prfam         TYPE REF TO cl_gui_textedit,
  go_reg_editor_desc_n_sh     TYPE REF TO cl_gui_textedit,        "++ONLD-835 JKH 26.05.2017
  go_reg_editor_desc_n_md     TYPE REF TO cl_gui_textedit,        "++ONLD-835 JKH 26.05.2017
  go_reg_editor_desc_n_lg     TYPE REF TO cl_gui_textedit,        "++ONLD-835 JKH 26.05.2017
  go_reg_editor_desc_r_sh     TYPE REF TO cl_gui_textedit,        "++ONLD-835 JKH 26.05.2017
  go_reg_editor_desc_r_md     TYPE REF TO cl_gui_textedit,        "++ONLD-835 JKH 26.05.2017
  go_reg_editor_desc_r_lg     TYPE REF TO cl_gui_textedit,        "++ONLD-835 JKH 26.05.2017
  go_reg_editor_ingr_stmt_r   TYPE REF TO cl_gui_textedit,
  go_reg_editor_ingr_stmt_n   TYPE REF TO cl_gui_textedit,
  go_reg_editor_ingr_stmt_reg TYPE REF TO cl_gui_textedit,
  go_reg_editor_range_det_r   TYPE REF TO cl_gui_textedit,


  go_rhcc01                   TYPE REF TO cl_gui_custom_container,
  go_rhtxt01                  TYPE REF TO cl_gui_custom_container,
  go_rhtxt02                  TYPE REF TO cl_gui_custom_container,
  go_rhtxt03                  TYPE REF TO cl_gui_custom_container, "++ONLD-835 JKH 26.05.2017
  go_rhtxt04                  TYPE REF TO cl_gui_custom_container, "++ONLD-835 JKH 26.05.2017
  go_rhtxt05                  TYPE REF TO cl_gui_custom_container, "++ONLD-835 JKH 26.05.2017
  go_rhtxt06                  TYPE REF TO cl_gui_custom_container, "++ONLD-835 JKH 26.05.2017
  go_rhtxt07                  TYPE REF TO cl_gui_custom_container, "++ONLD-835 JKH 26.05.2017
  go_rhtxt08                  TYPE REF TO cl_gui_custom_container, "++ONLD-835 JKH 26.05.2017
  go_rhtxt09                  TYPE REF TO cl_gui_custom_container,
  go_reg_ingr_stmt_container  TYPE REF TO cl_gui_custom_container,
  go_rhtxt10                  TYPE REF TO cl_gui_custom_container,
  go_rhtxt11                  TYPE REF TO cl_gui_custom_container,

  go_ruomcc01                 TYPE REF TO cl_gui_custom_container,
  go_rpfcc02                  TYPE REF TO cl_gui_custom_container,
  go_rconcc01                 TYPE REF TO cl_gui_custom_container,
  go_rconcc02                 TYPE REF TO cl_gui_custom_container,
  go_rconcc03                 TYPE REF TO cl_gui_custom_container,
  go_rconcc04                 TYPE REF TO cl_gui_custom_container,
  go_rconcc05                 TYPE REF TO cl_gui_custom_container,
  go_rconcc06                 TYPE REF TO cl_gui_custom_container,             " ++ONED-217 JKH 11.11.2016
  go_rconcc07                 TYPE REF TO cl_gui_custom_container,
  go_rpcc05                   TYPE REF TO cl_gui_custom_container,
  go_rrbcc06                  TYPE REF TO cl_gui_custom_container,
  go_reg_cluster_container    TYPE REF TO cl_gui_custom_container,
  go_roccc08                  TYPE REF TO cl_gui_custom_container,             "++ONLD-835 JKH 12.05.2017
  go_rgtcc07                  TYPE REF TO cl_gui_custom_container,
  go_ronl01                   TYPE REF TO cl_gui_custom_container,              "Online Tab
  go_rsccc09                  TYPE REF TO cl_gui_custom_container,              "9000004661 Supply Chain Tab

  go_nncc01                   TYPE REF TO cl_gui_custom_container,
  go_nucc02                   TYPE REF TO cl_gui_custom_container,
  go_nlcc03                   TYPE REF TO cl_gui_custom_container,
  go_nicc04                   TYPE REF TO cl_gui_custom_container,
  go_nacc04                   TYPE REF TO cl_gui_custom_container,
  go_npcc05                   TYPE REF TO cl_gui_custom_container,
  go_necc06                   TYPE REF TO cl_gui_custom_container,
  go_ndcc07                   TYPE REF TO cl_gui_custom_container,
  go_nccc08                   TYPE REF TO cl_gui_custom_container,
  go_nocc09                   TYPE REF TO cl_gui_custom_container,
  go_nncc10                   TYPE REF TO cl_gui_custom_container,
  go_nncc11                   TYPE REF TO cl_gui_custom_container,
  go_nncc12                   TYPE REF TO cl_gui_custom_container,
  go_nncc13                   TYPE REF TO cl_gui_custom_container,
  go_nncc14                   TYPE REF TO cl_gui_custom_container,
  go_nncc15                   TYPE REF TO cl_gui_custom_container,
  go_nncc16                   TYPE REF TO cl_gui_custom_container,
  go_nncc17                   TYPE REF TO cl_gui_custom_container,
  go_nncc18                   TYPE REF TO cl_gui_custom_container,
  go_nncc19                   TYPE REF TO cl_gui_custom_container,  " Net quantity ALV
  go_nscc20                   TYPE REF TO cl_gui_custom_container,  " Serve size ALV
  go_nscc21                   TYPE REF TO cl_gui_custom_container,  " Online Categories ALV
  go_nscc22                   TYPE REF TO cl_gui_custom_container,  " Benefits ALV

* Picture Related
  go_container                TYPE REF TO cl_gui_custom_container,
  go_splitter                 TYPE REF TO cl_gui_easy_splitter_container,
  go_container_1              TYPE REF TO cl_gui_container,
  go_picture_1                TYPE REF TO cl_gui_picture,

* Validation and defaults
  gr_validation_engine        TYPE REF TO zcl_arn_validation_engine,
  gr_validation_alv           TYPE REF TO cl_salv_table,
  gt_val_output               TYPE zarn_t_rl_output,
  gr_gui_load                 TYPE REF TO zcl_arn_gui_load.


** POPUP
DATA gt_open_po              TYPE TABLE OF ty_open_po.
DATA gt_open_po_disp         TYPE TABLE OF ty_open_po.
DATA gt_open_so              TYPE TABLE OF ty_open_so.
DATA gt_open_so_disp         TYPE TABLE OF ty_open_so.
DATA gt_open_po_so_disp      TYPE TABLE OF ty_open_po_so.
DATA gr_ccont TYPE REF TO cl_gui_custom_container.   "Custom container object
DATA gr_alvgd TYPE REF TO cl_gui_alv_grid.            "ALV grid object
DATA gt_fcat                 TYPE lvc_t_fcat.
DATA gv_no_refresh           TYPE boole_d.
DATA gt_open_stock           TYPE ztmd_stock_check_alv.
DATA gr_ccont_stock TYPE REF TO cl_gui_custom_container.   "Custom container object
DATA gr_alvgd_stock TYPE REF TO cl_gui_alv_grid.            "ALV grid object
DATA gt_fcat_stock           TYPE lvc_t_fcat.
DATA gt_zmd_host_sap_itab    TYPE ztmd_host_sap.
DATA gv_revert_issue_uom     TYPE boole_d.
DATA gv_issue_uom_msg        TYPE string.
DATA gv_online_tab_switch    TYPE char01.

*-----------------------------------------------------------------------*
* Tab Strip
*-----------------------------------------------------------------------*
CONSTANTS: BEGIN OF c_ts_main,
             tab1 LIKE sy-ucomm VALUE 'TS_MAIN_FC1',
             tab2 LIKE sy-ucomm VALUE 'TS_MAIN_FC2',
           END OF c_ts_main.

CONTROLS:  ts_main TYPE TABSTRIP.
DATA: BEGIN OF g_ts_main,
        subscreen   LIKE sy-dynnr,
        prog        LIKE sy-repid VALUE 'ZRARN_GUI',
        pressed_tab LIKE sy-ucomm, " VALUE c_ts_main-tab1,
      END OF g_ts_main.

CONSTANTS: BEGIN OF c_ts_reg,
             tab1 LIKE sy-ucomm VALUE 'TS_REG_FC1',
             tab2 LIKE sy-ucomm VALUE 'TS_REG_FC2',
             tab3 LIKE sy-ucomm VALUE 'TS_REG_FC3',
             tab4 LIKE sy-ucomm VALUE 'TS_REG_FC4',
             tab5 LIKE sy-ucomm VALUE 'TS_REG_FC5',         "9000004661
           END OF c_ts_reg.

CONTROLS:  ts_reg TYPE TABSTRIP.
DATA: BEGIN OF g_ts_reg,
        subscreen   LIKE sy-dynnr,
        prog        LIKE sy-repid VALUE 'ZRARN_GUI',
        pressed_tab LIKE sy-ucomm VALUE c_ts_reg-tab1,
      END OF g_ts_reg.

* Approvals
*&---------------------------------------------------------------------*
*&  Class Definition - Event handler class definition for ALV
*&---------------------------------------------------------------------*
CLASS lcl_appr_nat_event_handler DEFINITION.
  PUBLIC SECTION.
    METHODS :
*       Hotspots on ALV rows
      appr_nat_handle_hotspot_click FOR EVENT hotspot_click OF cl_gui_alv_grid
        IMPORTING e_row_id e_column_id es_row_no,

      appr_handle_close FOR EVENT close OF cl_gui_dialogbox_container
        IMPORTING sender.

ENDCLASS.                    "lcl_appr_nat_event_handler DEFINITION
*----------------------------------------------------------------------*
*       CLASS lcl_appr_reg_event_handler DEFINITION
*----------------------------------------------------------------------*
*
*----------------------------------------------------------------------*
CLASS lcl_appr_reg_event_handler DEFINITION.
  PUBLIC SECTION.
    METHODS :
*       Hotspots on ALV rows
      appr_reg_handle_hotspot_click FOR EVENT hotspot_click OF cl_gui_alv_grid
        IMPORTING e_row_id e_column_id es_row_no.

ENDCLASS.                    "lcl_appr_reg_event_handler DEFINITION

TYPES: BEGIN OF ty_cc_comment,
         idno         TYPE zarn_cc_log-idno,
         chg_category TYPE zarn_cc_log-chg_category,
         team_code    TYPE zarn_cc_log-team_code,
         comment      TYPE zarn_cc_log-approval_comment,
       END OF ty_cc_comment.

DATA: gv_approval_data              TYPE abap_bool,
      gv_appr_mode                  TYPE c,
      gv_appr_result                TYPE c,
      gv_appr_comment               TYPE string,
      gv_appr_rejection_reason      TYPE zarn_rejection_reason,
      go_appr_editor_container      TYPE REF TO cl_gui_custom_container,
      go_appr_text_editor           TYPE REF TO cl_gui_textedit,
      go_appr_nat_grid_container    TYPE REF TO cl_gui_custom_container,
      go_appr_nat_grid              TYPE REF TO cl_gui_alv_grid,
      gt_appr_nat_fieldcat          TYPE lvc_t_fcat,
      gt_appr_nat_approvals         TYPE ty_t_appr_output,
      go_appr_nat_event_handler     TYPE REF TO lcl_appr_nat_event_handler,
      gv_appr_nat_approvals         TYPE flag,
      go_appr_reg_grid_container    TYPE REF TO cl_gui_custom_container,
      go_appr_reg_grid              TYPE REF TO cl_gui_alv_grid,
      gt_appr_reg_fieldcat          TYPE lvc_t_fcat,
      gt_appr_reg_approvals         TYPE ty_t_appr_output,
      go_appr_reg_event_handler     TYPE REF TO lcl_appr_reg_event_handler,
      gv_appr_reg_approvals         TYPE flag,
      gs_appr_layout                TYPE lvc_s_layo,
      gt_approval_teams             TYPE STANDARD TABLE OF zarn_teams,
      gt_approval_comments          TYPE SORTED TABLE OF zarn_cc_log WITH NON-UNIQUE KEY chg_category team_code,
      gv_appr_nat_status            TYPE zarn_ver_status,
      gv_appr_reg_status            TYPE zarn_reg_status,
      gv_appr_nat_release_status    TYPE zarn_rel_status,
      gv_appr_nat_release_statust   TYPE zarn_release_status_desc,
      gv_appr_reg_release_status    TYPE zarn_rel_status,
      gv_appr_reg_release_statust   TYPE zarn_release_status_desc,
      gv_appr_chg_cat               TYPE zarn_chg_category,
      go_dialogbox_chg_category     TYPE REF TO cl_gui_dialogbox_container,
      gt_chg_category_changes       TYPE STANDARD TABLE OF zarn_cc_det_popup_fields,
      gt_cc_comment                 TYPE SORTED TABLE OF ty_cc_comment WITH UNIQUE KEY idno chg_category team_code,
      gv_appr_nat_is_approved       TYPE flag,
      gv_appr_national_not_approved TYPE text120,
      gv_validation_errors          TYPE flag,
      gv_heading_9001               TYPE char35.




*----------------------------------------------------------------------*
*       CLASS lcl_appr_nat_event_handler IMPLEMENTATION
*----------------------------------------------------------------------*
CLASS lcl_appr_nat_event_handler IMPLEMENTATION.

*       Hotspot Click on ALV rows
  METHOD appr_nat_handle_hotspot_click.
    PERFORM appr_handle_hotspot_click USING e_row_id
                                            e_column_id
                                            es_row_no
                                            co_appr_called_from_nat.
  ENDMETHOD.                    "appr_nat_handle_hotspot_click

  METHOD appr_handle_close.
    IF NOT go_dialogbox_chg_category IS INITIAL.
      CALL METHOD go_dialogbox_chg_category->free.
      FREE go_dialogbox_chg_category.
    ENDIF.
  ENDMETHOD.                    "handle_close

ENDCLASS.                    "lcl_appr_nat_event_handler IMPLEMENTATION
*----------------------------------------------------------------------*
*       CLASS lcl_appr_reg_event_handler IMPLEMENTATION
*----------------------------------------------------------------------*
*
*----------------------------------------------------------------------*
CLASS lcl_appr_reg_event_handler IMPLEMENTATION.

*       Hotspot Click on ALV rows
  METHOD appr_reg_handle_hotspot_click.
    PERFORM appr_handle_hotspot_click USING e_row_id
                                            e_column_id
                                            es_row_no
                                            co_appr_called_from_reg.
  ENDMETHOD.                    "appr_handle_hotspot_click

ENDCLASS.                    "lcl_appr_reg_event_handler IMPLEMENTATION
