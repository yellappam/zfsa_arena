*&---------------------------------------------------------------------*
*&  Include           ZIARN_GUI_MAIN
*&---------------------------------------------------------------------*

START-OF-SELECTION.
* global parameter for display only
* gv_display         = p_disp.
  gv_prfam_done_text = TEXT-039.
  gv_approval_data   = abap_false.


* Initialize Data
  PERFORM initialize_data. "++3174 JKH 06.03.2017



* Use singe selection option on the screen
* for Retail Cat manager
  s_rcman    = s_ccman.
  s_rcman[]  = s_ccman[].

  s_aprrcm    = s_aprcmn.
  s_aprrcm[]  = s_aprcmn[].

* Regional screen
  CREATE OBJECT go_event_receiver_edit.
  SET HANDLER go_event_receiver_edit->handle_data_changed FOR ALL INSTANCES.

* Create Screen atrributes and defaults object
  CREATE OBJECT gr_gui_load.


* Create validation object
  PERFORM initiate_validation_check.

*  WRITE icon_locked TO gv_prfam_done_text.
*  gv_prfam_done_text = gv_prfam_done_text && text-039. "Done

* national header screen collapse button
  WRITE icon_collapse       TO gv_nat_pb_01 AS ICON.
  WRITE icon_collapse       TO gv_reg_pb_01 AS ICON.
  WRITE icon_collapse       TO gv_nat_pb_02 AS ICON.
  WRITE icon_collapse       TO gv_nat_pb_03 AS ICON.
  WRITE icon_collapse       TO gv_nat_pb_04 AS ICON.

  PERFORM change_reg_cluster_icon_0112.

  WRITE icon_expand         TO gv_nat_pb_05 AS ICON.
  WRITE icon_expand         TO gv_nat_pb_06 AS ICON.
  WRITE icon_expand         TO gv_nat_pb_07 AS ICON.
  WRITE icon_expand         TO gv_nat_pb_08 AS ICON.
  WRITE icon_expand         TO gv_nat_pb_09 AS ICON.
  WRITE icon_expand         TO gv_nat_pb_10 AS ICON.
  WRITE icon_expand         TO gv_nat_pb_11 AS ICON.
  WRITE icon_expand         TO gv_nat_pb_12 AS ICON.


* calculate expand size
* 20 is a number which works, otherwise we run the risk of loosing the screen separator !
  gv_extension = ( p_ratio * 20 ).
  gv_text_size-icon_id = icon_view_expand_horizontal.
  gv_text_size-text      = TEXT-i01.
  gv_text_size-icon_text = TEXT-i01.

* start approvals closed
  gv_appr_nat_okcode = 'X'.
  gv_appr_reg_okcode = 'X'.
  WRITE icon_expand  TO gv_nat_pb_00 AS ICON.
  WRITE icon_expand  TO gv_reg_pb_00 AS ICON.

*>>> IS encapsulate  the selection logic and call the same subroutine
  "upo initial selection and also on "BACK TO WORKLIST" button



* get data based on selection screen active tab

  gv_sel_tab = mytab-activetab.

  PERFORM get_data.

*<<< IS
* keep the selection screen tab in memory for next sessionCoding01

  SET PARAMETER ID 'ZARN_GUI_TAB' FIELD gv_sel_tab.

  IF gt_prod_data IS INITIAL.
    MESSAGE i000 WITH TEXT-201.

    IF ( gv_valerr_cnt GT 0 OR gv_error_cnt GT 0 ) AND mytab-activetab = gc_icare_tab.    "++3174 JKH 20.03.2017
    ELSE.
      RETURN.
    ENDIF.
  ENDIF.

  PERFORM fill_tvarv_values.

* Create ALV worklist
  PERFORM create_worklist.

* I Care only lock
  PERFORM lock_icare_worklist.

* Docking containers and ALV worklist
  PERFORM load_controls using p_ratio.

* (ok_code -> GV_OKCODE)
  CALL SCREEN '0100'.

END-OF-SELECTION.
  PERFORM free_objects.
