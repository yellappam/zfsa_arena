class ZMASTER_CO_PRODUCT_SEARCH_REQU definition
  public
  inheriting from CL_PROXY_CLIENT
  create public .

public section.

  methods CONSTRUCTOR
    importing
      !LOGICAL_PORT_NAME type PRX_LOGICAL_PORT_NAME optional
    raising
      CX_AI_SYSTEM_FAULT .
  methods PRODUCT_SEARCH_REQUEST_OUT
    importing
      !OUTPUT type ZMASTER_PRODUCT_SEARCH1
    exporting
      !INPUT type ZMASTER_PRODUCT_WITH_PRICE_CRE
    raising
      CX_AI_SYSTEM_FAULT
      ZCX_MASTER_PRODUCT_WITH_PRICE .
protected section.
private section.
ENDCLASS.



CLASS ZMASTER_CO_PRODUCT_SEARCH_REQU IMPLEMENTATION.


  method CONSTRUCTOR.

  super->constructor(
    class_name          = 'ZMASTER_CO_PRODUCT_SEARCH_REQU'
    logical_port_name   = logical_port_name
  ).

  endmethod.


  method PRODUCT_SEARCH_REQUEST_OUT.

  data:
    ls_parmbind type abap_parmbind,
    lt_parmbind type abap_parmbind_tab.

  ls_parmbind-name = 'OUTPUT'.
  ls_parmbind-kind = cl_abap_objectdescr=>importing.
  get reference of OUTPUT into ls_parmbind-value.
  insert ls_parmbind into table lt_parmbind.

  ls_parmbind-name = 'INPUT'.
  ls_parmbind-kind = cl_abap_objectdescr=>exporting.
  get reference of INPUT into ls_parmbind-value.
  insert ls_parmbind into table lt_parmbind.

  if_proxy_client~execute(
    exporting
      method_name = 'PRODUCT_SEARCH_REQUEST_OUT'
    changing
      parmbind_tab = lt_parmbind
  ).

  endmethod.
ENDCLASS.
