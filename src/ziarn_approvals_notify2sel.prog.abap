*&---------------------------------------------------------------------*
*&  Include           ZIARN_APPROVALS_NOTIFY2SEL
*&---------------------------------------------------------------------*

SELECTION-SCREEN BEGIN OF BLOCK blk_tst WITH FRAME TITLE TEXT-001.
PARAMETERS: p_consum AS CHECKBOX DEFAULT 'X'.
SELECT-OPTIONS: s_idno  FOR zarn_products-idno,
                s_fanid FOR zarn_products-fan_id,
                s_updon FOR gv_updated_on.
SELECTION-SCREEN END OF BLOCK blk_tst.
SELECTION-SCREEN BEGIN OF BLOCK blk_ctl WITH FRAME TITLE TEXT-005.
PARAMETERS: p_test AS CHECKBOX DEFAULT 'X'.
SELECTION-SCREEN END OF BLOCK blk_ctl.
