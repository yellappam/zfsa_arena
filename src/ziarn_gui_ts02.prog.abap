*&---------------------------------------------------------------------*
*&  Include           ZIARN_GUI_TS02
*&---------------------------------------------------------------------*

*&SPWIZARD: OUTPUT MODULE FOR TS 'TS_REG'. DO NOT CHANGE THIS LINE!
*&SPWIZARD: SETS ACTIVE TAB
MODULE ts_reg_active_tab_set OUTPUT.
  ts_reg-activetab = g_ts_reg-pressed_tab.
  CASE g_ts_reg-pressed_tab.
    WHEN c_ts_reg-tab1.
      g_ts_reg-subscreen = '0111'.
    WHEN c_ts_reg-tab2.
      g_ts_reg-subscreen = '0112'.
    WHEN c_ts_reg-tab3.
      g_ts_reg-subscreen = '0113'.
    WHEN c_ts_reg-tab4.
      g_ts_reg-subscreen = '0114'.
    WHEN c_ts_reg-tab5.
      g_ts_reg-subscreen = '0115'.
    WHEN OTHERS.
*&SPWIZARD:      DO NOTHING
  ENDCASE.
ENDMODULE.                    "TS_REG_ACTIVE_TAB_SET OUTPUT

*&SPWIZARD: INPUT MODULE FOR TS 'TS_REG'. DO NOT CHANGE THIS LINE!
*&SPWIZARD: GETS ACTIVE TAB
MODULE ts_reg_active_tab_get INPUT.

  CASE gv_okcode.
    WHEN c_ts_reg-tab1.
      g_ts_reg-pressed_tab = c_ts_reg-tab1.
    WHEN c_ts_reg-tab2.
      g_ts_reg-pressed_tab = c_ts_reg-tab2.
    WHEN c_ts_reg-tab3.
      g_ts_reg-pressed_tab = c_ts_reg-tab3.
    WHEN c_ts_reg-tab4.
      g_ts_reg-pressed_tab = c_ts_reg-tab4.
    WHEN c_ts_reg-tab5.
      g_ts_reg-pressed_tab = c_ts_reg-tab5. "9000004661
    WHEN OTHERS.
*&SPWIZARD:      DO NOTHING
  ENDCASE.
ENDMODULE.                    "TS_REG_ACTIVE_TAB_GET INPUT
