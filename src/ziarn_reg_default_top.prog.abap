*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_REG_DEFAULT_TOP
*&---------------------------------------------------------------------*

TABLES: zarn_products.

CLASS lcl_main DEFINITION DEFERRED.


TYPES: BEGIN OF ty_s_log,
         idno    TYPE zarn_idno,
         msgty   TYPE sy-msgty,
         message TYPE char50,
       END OF ty_s_log,
       ty_t_log TYPE STANDARD TABLE OF ty_s_log.


DATA: go_main TYPE REF TO lcl_main,
      gt_log  TYPE ty_t_log.
