*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE lzarn_article_posttop.             " Global Data
  INCLUDE lzarn_article_postuxx.             " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_ARTICLE_POSTF...             " Subroutines
* INCLUDE LZARN_ARTICLE_POSTO...             " PBO-Modules
* INCLUDE LZARN_ARTICLE_POSTI...             " PAI-Modules
* INCLUDE LZARN_ARTICLE_POSTE...             " Events
  INCLUDE lzarn_article_postd01.             " Local class definition
  INCLUDE lzarn_article_postp01.             " Local class implementation
* INCLUDE LZARN_ARTICLE_POSTT99.             " ABAP Unit tests

  INCLUDE lzarn_article_postf01.
