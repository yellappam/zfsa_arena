*----------------------------------------------------------------------*
***INCLUDE LZARN_APPROVALSF01 .
*----------------------------------------------------------------------*

*&---------------------------------------------------------------------*
*&      Form  get_process_type
*&---------------------------------------------------------------------*
FORM get_process_type USING pv_idno TYPE zarn_idno CHANGING pv_process_type TYPE zarn_process_type.

  DATA: ls_ver_status TYPE zarn_ver_status.

* Is new if we do not have a SAP article
  pv_process_type = 'NEW'.
  CLEAR ls_ver_status.
  SELECT SINGLE article_ver
    INTO ls_ver_status-article_ver
    FROM zarn_ver_status
    WHERE idno EQ pv_idno.
  IF NOT ls_ver_status-article_ver IS INITIAL.
    pv_process_type = 'CHG'.
  ENDIF.

ENDFORM.                    "get_process_type

*&---------------------------------------------------------------------*
*&      Form  email_substitute
*&---------------------------------------------------------------------*
FORM email_substitute USING pv_datetime ps_arn_team TYPE zarn_teams CHANGING pv_tdline TYPE tdline.

  DATA: lv_len TYPE i.

  REPLACE '<(>&<)>' WITH '&' INTO pv_tdline.
  IF pv_tdline CS '&DATETIME&'.
    lv_len = strlen( pv_datetime ).
    REPLACE '&DATETIME&' WITH pv_datetime(lv_len) INTO pv_tdline.
  ENDIF.

  IF pv_tdline CS '&TEAM_CODE&'.
    REPLACE '&TEAM_CODE&' WITH ps_arn_team-team_code INTO pv_tdline.
  ENDIF.

  IF pv_tdline CS '&TEAM_NAME&'.
    lv_len = strlen( ps_arn_team-team_name ).
    REPLACE '&TEAM_NAME&' WITH ps_arn_team-team_name(lv_len) INTO pv_tdline.
  ENDIF.

ENDFORM.                    "email_substitute

*&---------------------------------------------------------------------*
*&      Form  email_add_line
*&---------------------------------------------------------------------*
FORM email_add_line USING pv_text CHANGING pt_body TYPE soli_tab.

  DATA ls_body LIKE LINE OF pt_body.

  CLEAR ls_body.
  ls_body-line = pv_text.
  APPEND ls_body TO pt_body.

ENDFORM.                    "email_add_line
