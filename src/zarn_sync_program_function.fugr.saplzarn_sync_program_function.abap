*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_SYNC_PROGRAM_FUNCTIONTOP.    " Global Data
  INCLUDE LZARN_SYNC_PROGRAM_FUNCTIONUXX.    " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_SYNC_PROGRAM_FUNCTIONF...    " Subroutines
* INCLUDE LZARN_SYNC_PROGRAM_FUNCTIONO...    " PBO-Modules
* INCLUDE LZARN_SYNC_PROGRAM_FUNCTIONI...    " PAI-Modules
* INCLUDE LZARN_SYNC_PROGRAM_FUNCTIONE...    " Events
* INCLUDE LZARN_SYNC_PROGRAM_FUNCTIONP...    " Local class implement.
* INCLUDE LZARN_SYNC_PROGRAM_FUNCTIONT99.    " ABAP Unit tests
