FUNCTION zarn_regional_db_update.
*"----------------------------------------------------------------------
*"*"Update Function Module:
*"
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(IT_REG_DATA) TYPE  ZTARN_REG_DATA
*"     VALUE(IT_REG_DATA_OLD) TYPE  ZTARN_REG_DATA OPTIONAL
*"     VALUE(IV_NO_DELETE) TYPE  XFELD OPTIONAL
*"     VALUE(IV_NO_CHG_CAT) TYPE  XFELD DEFAULT SPACE
*"     VALUE(IV_NO_CHG_DOC) TYPE  XFELD DEFAULT SPACE
*"     VALUE(IT_IDNO_VAL_ERROR) TYPE  ZTARN_IDNO_KEY OPTIONAL
*"     VALUE(IV_REDETERMINE_STATUS) TYPE  BOOLE_D DEFAULT SPACE
*"----------------------------------------------------------------------
*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Functional Specification 3125
* DATE WRITTEN     # 07 03 2016
* SAP VERSION      # 731
* TYPE             # Function Module (UPDATE module)
* AUTHOR           # C90002769, Rajesh Anumolu
*-----------------------------------------------------------------------
* TITLE            # Update data into DB with change documents
* PURPOSE          # Regional Data Update into DB
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 26.06.2020
* CHANGE No.       # 9000007255: SSM-1 NW Range Policy ZARN_GUI
* DESCRIPTION      # Added table ZARN_REG_CLUSTER
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* DATE             # 20.07.2020
* CHANGE No.       # CIP-1359: ZARN_GUI changes (GS1  Updates)
* DESCRIPTION      # Added table ZARN_REG_NUTRIEN
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* Create Change Document
*    CALL FUNCTION 'ZARN_CTRL_REGNL_WRITE_DOCUMENT'

  CONSTANTS:
      lc_table_type_r     TYPE zarn_arena_table_type VALUE 'R'.

  DATA:
    lv_chg_flag           TYPE cdchngindh      VALUE 'U',
    lv_update_flag        TYPE cdflag          VALUE 'U',
    lv_objectid           TYPE cdobjectv,
    lv_change_number      TYPE cdchangenr,
    ls_reg_data           TYPE zsarn_reg_data,
    ls_reg_data_old       TYPE zsarn_reg_data,

    lt_cdtxt              TYPE                    isu_cdtxt,
    lt_xzarn_control      TYPE STANDARD TABLE OF yzarn_control,
    lt_yzarn_control      TYPE STANDARD TABLE OF yzarn_control,
    lt_xzarn_prd_version  TYPE STANDARD TABLE OF yzarn_prd_version,
    lt_yzarn_prd_version  TYPE STANDARD TABLE OF yzarn_prd_version,
    lt_xzarn_reg_banner   TYPE STANDARD TABLE OF yzarn_reg_banner,
    lt_yzarn_reg_banner   TYPE STANDARD TABLE OF yzarn_reg_banner,
    lt_xzarn_reg_cluster  TYPE STANDARD TABLE OF yzarn_reg_cluster,
    lt_yzarn_reg_cluster  TYPE STANDARD TABLE OF yzarn_reg_cluster,

    lt_xzarn_reg_dc_sell  TYPE STANDARD TABLE OF yzarn_reg_dc_sell,
    lt_yzarn_reg_dc_sell  TYPE STANDARD TABLE OF yzarn_reg_dc_sell,
    lt_xzarn_reg_lst_prc  TYPE STANDARD TABLE OF yzarn_reg_lst_prc,
    lt_yzarn_reg_lst_prc  TYPE STANDARD TABLE OF yzarn_reg_lst_prc,
    lt_xzarn_reg_rrp      TYPE STANDARD TABLE OF yzarn_reg_rrp,
    lt_yzarn_reg_rrp      TYPE STANDARD TABLE OF yzarn_reg_rrp,
    lt_xzarn_reg_std_ter  TYPE STANDARD TABLE OF yzarn_reg_std_ter,
    lt_yzarn_reg_std_ter  TYPE STANDARD TABLE OF yzarn_reg_std_ter,
    lt_xzarn_reg_allerg   TYPE STANDARD TABLE OF yzarn_reg_allerg,
    lt_yzarn_reg_allerg   TYPE STANDARD TABLE OF yzarn_reg_allerg,
    lt_xzarn_reg_artlink  TYPE STANDARD TABLE OF yzarn_reg_artlink,
    lt_yzarn_reg_artlink  TYPE STANDARD TABLE OF yzarn_reg_artlink,
    lt_xzarn_reg_onlcat   TYPE STANDARD TABLE OF yzarn_reg_onlcat,
    lt_yzarn_reg_onlcat   TYPE STANDARD TABLE OF yzarn_reg_onlcat,
    lt_xzarn_reg_str      TYPE yzarn_reg_strs,
    lt_yzarn_reg_str      TYPE yzarn_reg_strs,

    lt_xzarn_reg_ean      TYPE STANDARD TABLE OF yzarn_reg_ean,
    lt_yzarn_reg_ean      TYPE STANDARD TABLE OF yzarn_reg_ean,
    lt_xzarn_reg_hdr      TYPE STANDARD TABLE OF yzarn_reg_hdr,
    lt_yzarn_reg_hdr      TYPE STANDARD TABLE OF yzarn_reg_hdr,
    lt_xzarn_reg_hsno     TYPE STANDARD TABLE OF yzarn_reg_hsno,
    lt_yzarn_reg_hsno     TYPE STANDARD TABLE OF yzarn_reg_hsno,
    lt_xzarn_reg_pir      TYPE STANDARD TABLE OF yzarn_reg_pir,
    lt_yzarn_reg_pir      TYPE STANDARD TABLE OF yzarn_reg_pir,
    lt_xzarn_reg_prfam    TYPE STANDARD TABLE OF yzarn_reg_prfam,
    lt_yzarn_reg_prfam    TYPE STANDARD TABLE OF yzarn_reg_prfam,
    lt_xzarn_reg_txt      TYPE STANDARD TABLE OF yzarn_reg_txt,
    lt_yzarn_reg_txt      TYPE STANDARD TABLE OF yzarn_reg_txt,
    lt_xzarn_reg_uom      TYPE STANDARD TABLE OF yzarn_reg_uom,
    lt_yzarn_reg_uom      TYPE STANDARD TABLE OF yzarn_reg_uom,
    lt_xzarn_reg_sc       TYPE STANDARD TABLE OF yzarn_reg_sc,
    lt_yzarn_reg_sc       TYPE STANDARD TABLE OF yzarn_reg_sc,
    lt_xzarn_ver_status   TYPE STANDARD TABLE OF yzarn_ver_status,
    lt_yzarn_ver_status   TYPE STANDARD TABLE OF yzarn_ver_status,
    lt_xzarn_reg_nutrien  TYPE STANDARD TABLE OF yzarn_reg_nutrien,
    lt_yzarn_reg_nutrien  TYPE STANDARD TABLE OF yzarn_reg_nutrien,

    lt_table_mastr        TYPE STANDARD TABLE OF zarn_table_mastr,
    lt_cc_hdr             TYPE ztarn_cc_hdr,
    lt_cc_det             TYPE ztarn_cc_det,
    lt_cc_hdr_db          TYPE ztarn_cc_hdr,
    lt_cc_det_db          TYPE ztarn_cc_det,
    ls_cc_hdr             TYPE zarn_cc_hdr,
    ls_cc_det             TYPE zarn_cc_det,
    lv_chgid              TYPE zarn_chg_id,
*>>>IS1609ins Feature 3162 - AReNa Approval Stabilisation
    lo_approvals          TYPE REF TO zcl_arn_approval_backend,
    lt_approval           TYPE zarn_t_approval,
    lt_approval_ch        TYPE zarn_t_approval_ch,
    lt_approval_hist      TYPE zarn_t_appr_hist,
    lt_approval_db        TYPE zarn_t_approval,
    lt_approval_ch_db     TYPE zarn_t_approval_ch,
    lt_approval_hist_db   TYPE zarn_t_appr_hist,
    lt_after_appr_action  TYPE zarn_t_after_appr_action,
    lt_after_appr_act_all TYPE zarn_t_after_appr_action,
    lv_val_error          TYPE xfeld.
*<<<IS1609ins Feature 3162 - AReNa Approval Stabilisation


  IF iv_no_chg_cat = space.
* Get Table Master Data
    SELECT * FROM zarn_table_mastr
      INTO CORRESPONDING FIELDS OF TABLE lt_table_mastr[]
      WHERE arena_table_type EQ lc_table_type_r.  " 'R' Regional Table

* get change category id number
    CALL FUNCTION 'NUMBER_GET_NEXT'
      EXPORTING
        nr_range_nr             = '01'
        object                  = 'ZARN_CHGID'
      IMPORTING
        number                  = lv_chgid
      EXCEPTIONS
        interval_not_found      = 1
        number_range_not_intern = 2
        object_not_found        = 3
        quantity_is_0           = 4
        quantity_is_not_1       = 5
        interval_overflow       = 6
        buffer_overflow         = 7
        OTHERS                  = 8.
  ENDIF.  " IF iv_no_chg_cat = space

** first update DB
  IF iv_no_delete IS NOT INITIAL.
    CALL FUNCTION 'ZARN_REGIONAL_DATA_UPDATE'
      EXPORTING
        it_reg_data = it_reg_data.
  ELSE.
    CALL FUNCTION 'ZARN_REGIONAL_DATA_UPDATE'
      EXPORTING
        it_reg_data     = it_reg_data
        it_reg_data_del = it_reg_data_old.
  ENDIF.

* Then call Change documents with changes
  LOOP AT it_reg_data INTO ls_reg_data.

*  read the deletion table for old values
    READ TABLE it_reg_data_old INTO ls_reg_data_old
         WITH KEY idno =  ls_reg_data-idno.

    IF iv_no_chg_doc = space.

      CLEAR lv_objectid.
      lv_objectid = ls_reg_data-idno.

      lt_yzarn_reg_banner   = ls_reg_data_old-zarn_reg_banner.
      lt_yzarn_reg_cluster  = ls_reg_data_old-zarn_reg_cluster.
      lt_yzarn_reg_ean      = ls_reg_data_old-zarn_reg_ean.
      lt_yzarn_reg_hdr      = ls_reg_data_old-zarn_reg_hdr.
      lt_yzarn_reg_hsno     = ls_reg_data_old-zarn_reg_hsno.
      lt_yzarn_reg_pir      = ls_reg_data_old-zarn_reg_pir.
      lt_yzarn_reg_prfam    = ls_reg_data_old-zarn_reg_prfam.
      lt_yzarn_reg_txt      = ls_reg_data_old-zarn_reg_txt.
      lt_yzarn_reg_uom      = ls_reg_data_old-zarn_reg_uom.
      lt_yzarn_reg_dc_sell  = ls_reg_data_old-zarn_reg_dc_sell.
      lt_yzarn_reg_std_ter  = ls_reg_data_old-zarn_reg_std_ter.
      lt_yzarn_reg_lst_prc  = ls_reg_data_old-zarn_reg_lst_prc.
      lt_yzarn_reg_rrp      = ls_reg_data_old-zarn_reg_rrp.
      lt_yzarn_reg_artlink  = ls_reg_data_old-zarn_reg_artlink.
      lt_yzarn_reg_onlcat   = ls_reg_data_old-zarn_reg_onlcat.
      lt_yzarn_reg_allerg   = ls_reg_data_old-zarn_reg_allerg.
      lt_yzarn_reg_nutrien   = ls_reg_data_old-zarn_reg_nutrien.
      lt_yzarn_reg_sc       = ls_reg_data_old-zarn_reg_sc.
      MOVE-CORRESPONDING ls_reg_data_old-zarn_reg_str TO lt_yzarn_reg_str.

*   New values
      lt_xzarn_reg_banner    = ls_reg_data-zarn_reg_banner.
      lt_xzarn_reg_cluster   = ls_reg_data-zarn_reg_cluster.
      lt_xzarn_reg_ean       = ls_reg_data-zarn_reg_ean.
      lt_xzarn_reg_hdr       = ls_reg_data-zarn_reg_hdr.
      lt_xzarn_reg_hsno      = ls_reg_data-zarn_reg_hsno.
      lt_xzarn_reg_pir       = ls_reg_data-zarn_reg_pir.
      lt_xzarn_reg_prfam     = ls_reg_data-zarn_reg_prfam.
      lt_xzarn_reg_txt       = ls_reg_data-zarn_reg_txt.
      lt_xzarn_reg_uom       = ls_reg_data-zarn_reg_uom.
      lt_xzarn_reg_dc_sell   = ls_reg_data-zarn_reg_dc_sell.
      lt_xzarn_reg_std_ter   = ls_reg_data-zarn_reg_std_ter.
      lt_xzarn_reg_lst_prc   = ls_reg_data-zarn_reg_lst_prc.
      lt_xzarn_reg_rrp       = ls_reg_data-zarn_reg_rrp.
      lt_xzarn_reg_artlink   = ls_reg_data-zarn_reg_artlink.
      lt_xzarn_reg_onlcat    = ls_reg_data-zarn_reg_onlcat.
      lt_xzarn_reg_allerg    = ls_reg_data-zarn_reg_allerg.
      lt_xzarn_reg_nutrien   = ls_reg_data-zarn_reg_nutrien.
      lt_xzarn_reg_sc        = ls_reg_data-zarn_reg_sc.
      MOVE-CORRESPONDING ls_reg_data-zarn_reg_str TO lt_xzarn_reg_str.


      SORT: lt_yzarn_reg_banner[],
            lt_yzarn_reg_cluster,
            lt_yzarn_reg_ean[],
            lt_yzarn_reg_hdr[],
            lt_yzarn_reg_hsno[],
            lt_yzarn_reg_pir[],
            lt_yzarn_reg_prfam[],
            lt_yzarn_reg_txt[],
            lt_yzarn_reg_uom[],
            lt_yzarn_reg_dc_sell[],
            lt_yzarn_reg_std_ter[],
            lt_yzarn_reg_lst_prc[],
            lt_yzarn_reg_rrp[],
            lt_yzarn_reg_artlink[],
            lt_yzarn_reg_onlcat[],
            lt_yzarn_reg_str[],
            lt_yzarn_reg_allerg[],
            lt_yzarn_reg_nutrien,
            lt_yzarn_reg_sc[],

            lt_xzarn_reg_banner[],
            lt_xzarn_reg_cluster,
            lt_xzarn_reg_ean[],
            lt_xzarn_reg_hdr[],
            lt_xzarn_reg_hsno[],
            lt_xzarn_reg_pir[],
            lt_xzarn_reg_prfam[],
            lt_xzarn_reg_txt[],
            lt_xzarn_reg_uom[],
            lt_xzarn_reg_dc_sell[],
            lt_xzarn_reg_std_ter[],
            lt_xzarn_reg_lst_prc[],
            lt_xzarn_reg_rrp[],
            lt_xzarn_reg_artlink[],
            lt_xzarn_reg_onlcat[],
            lt_xzarn_reg_str[],
            lt_xzarn_reg_allerg[],
            lt_xzarn_reg_nutrien,
            lt_xzarn_reg_sc[].


*   Change document
*
      CALL FUNCTION 'ZARN_CTRL_REGNL_WRITE_DOC_CUST'
        EXPORTING
          objectid                = lv_objectid
          tcode                   = sy-tcode
          utime                   = sy-uzeit
          udate                   = sy-datum
          username                = sy-uname
*         PLANNED_CHANGE_NUMBER   = ' '
          object_change_indicator = lv_chg_flag
*         PLANNED_OR_REAL_CHANGES = ' '
*         NO_CHANGE_POINTERS      = ' '
*         UPD_ICDTXT_ZARN_CTRL_REGNL = ' '
*         UPD_ZARN_CONTROL        = ' '
*         UPD_ZARN_PRD_VERSION    = ' '
          upd_zarn_reg_allerg     = lv_update_flag
          upd_zarn_reg_artlink    = lv_update_flag
          upd_zarn_reg_banner     = lv_update_flag
          upd_zarn_reg_dc_sell    = lv_update_flag
          upd_zarn_reg_ean        = lv_update_flag
          upd_zarn_reg_hdr        = lv_update_flag
          upd_zarn_reg_hsno       = lv_update_flag
          upd_zarn_reg_lst_prc    = lv_update_flag
          upd_zarn_reg_onlcat     = lv_update_flag
          upd_zarn_reg_pir        = lv_update_flag
          upd_zarn_reg_prfam      = lv_update_flag
          upd_zarn_reg_rrp        = lv_update_flag
          upd_zarn_reg_std_ter    = lv_update_flag
          upd_zarn_reg_str        = lv_update_flag
          xzarn_reg_str           = lt_xzarn_reg_str
          yzarn_reg_str           = lt_yzarn_reg_str
          upd_zarn_reg_txt        = lv_update_flag
          upd_zarn_reg_uom        = lv_update_flag
*         UPD_ZARN_VER_STATUS     = ' '
          upd_zarn_reg_sc         = lv_update_flag
          upd_zarn_reg_nutrien    = lv_update_flag
          upd_zarn_reg_cluster    = lv_update_flag
        IMPORTING
          ev_changenumber         = lv_change_number
        TABLES
          icdtxt_zarn_ctrl_regnl  = lt_cdtxt
          xzarn_control           = lt_xzarn_control
          yzarn_control           = lt_yzarn_control
          xzarn_prd_version       = lt_xzarn_prd_version
          yzarn_prd_version       = lt_yzarn_prd_version
          xzarn_reg_allerg        = lt_xzarn_reg_allerg
          yzarn_reg_allerg        = lt_yzarn_reg_allerg
          xzarn_reg_artlink       = lt_xzarn_reg_artlink
          yzarn_reg_artlink       = lt_yzarn_reg_artlink
          xzarn_reg_banner        = lt_xzarn_reg_banner
          yzarn_reg_banner        = lt_yzarn_reg_banner
          xzarn_reg_cluster       = lt_xzarn_reg_cluster
          yzarn_reg_cluster       = lt_yzarn_reg_cluster
          xzarn_reg_dc_sell       = lt_xzarn_reg_dc_sell
          yzarn_reg_dc_sell       = lt_yzarn_reg_dc_sell
          xzarn_reg_ean           = lt_xzarn_reg_ean
          yzarn_reg_ean           = lt_yzarn_reg_ean
          xzarn_reg_hdr           = lt_xzarn_reg_hdr
          yzarn_reg_hdr           = lt_yzarn_reg_hdr
          xzarn_reg_hsno          = lt_xzarn_reg_hsno
          yzarn_reg_hsno          = lt_yzarn_reg_hsno
          xzarn_reg_lst_prc       = lt_xzarn_reg_lst_prc
          yzarn_reg_lst_prc       = lt_yzarn_reg_lst_prc
          xzarn_reg_onlcat        = lt_xzarn_reg_onlcat
          yzarn_reg_onlcat        = lt_yzarn_reg_onlcat
          xzarn_reg_pir           = lt_xzarn_reg_pir
          yzarn_reg_pir           = lt_yzarn_reg_pir
          xzarn_reg_prfam         = lt_xzarn_reg_prfam
          yzarn_reg_prfam         = lt_yzarn_reg_prfam
          xzarn_reg_rrp           = lt_xzarn_reg_rrp
          yzarn_reg_rrp           = lt_yzarn_reg_rrp
          xzarn_reg_std_ter       = lt_xzarn_reg_std_ter
          yzarn_reg_std_ter       = lt_yzarn_reg_std_ter
          xzarn_reg_txt           = lt_xzarn_reg_txt
          yzarn_reg_txt           = lt_yzarn_reg_txt
          xzarn_reg_uom           = lt_xzarn_reg_uom
          yzarn_reg_uom           = lt_yzarn_reg_uom
          xzarn_ver_status        = lt_xzarn_ver_status
          yzarn_ver_status        = lt_yzarn_ver_status
          xzarn_reg_sc            = lt_xzarn_reg_sc
          yzarn_reg_sc            = lt_yzarn_reg_sc
          xzarn_reg_nutrien       = lt_xzarn_reg_nutrien
          yzarn_reg_nutrien       = lt_yzarn_reg_nutrien.


* ONLD-1061 Field used by Online?
      zcl_onl_arena=>zarn_regional_zsaw(
        EXPORTING
          it_yreg_str      = lt_yzarn_reg_str    " before
          it_xreg_str      = lt_xzarn_reg_str    " after
          it_yreg_hdr      = lt_yzarn_reg_hdr    " before
          it_xreg_hdr      = lt_xzarn_reg_hdr    " after
          it_yreg_banner   = lt_yzarn_reg_banner    " before
          it_xreg_banner   = lt_xzarn_reg_banner    " after
          it_yreg_onlcat   = lt_yzarn_reg_onlcat    " before
          it_xreg_onlcat   = lt_xzarn_reg_onlcat    " after
      ).


    ENDIF.  " IF iv_no_chg_doc = space


    IF iv_no_chg_cat = space.
      CLEAR: lt_cc_hdr[], lt_cc_det[].
*   Build Change Category Data
      CALL FUNCTION 'ZARN_CC_BUILD_REGIONAL_DATA'
        EXPORTING
          it_table_mastr   = lt_table_mastr
          is_reg_data_db   = ls_reg_data_old
          is_reg_data_n    = ls_reg_data
          iv_chgid         = lv_chgid
          iv_change_number = lv_change_number
        IMPORTING
          et_cc_hdr        = lt_cc_hdr
          et_cc_det        = lt_cc_det.



* Do we need to redetermine/set the regional header status to WIP?
      IF iv_redetermine_status = abap_true.
* Must have a change and the regional header must not be in WIP
        IF lt_cc_hdr IS NOT INITIAL.
          IF NOT line_exists( ls_reg_data-zarn_reg_hdr[ status = zcl_arn_approval_backend=>gc_hdr_status_wip ] ).
            CALL FUNCTION 'ZARN_REG_STATUS_UPDATE'
              EXPORTING
                iv_idno   = ls_reg_data-idno
                iv_status = zcl_arn_approval_backend=>gc_hdr_status_wip.
          ENDIF.
        ENDIF.
      ENDIF.

      TRY.

          IF line_exists( it_idno_val_error[ idno = ls_reg_data-idno ] ).
            lv_val_error = abap_true.
          ELSE.
            lv_val_error = abap_false.
          ENDIF.

          lo_approvals = NEW #( iv_approval_event = zcl_arn_approval_backend=>gc_event_ui ).
          lo_approvals->build_approvals_from_chg_singl( EXPORTING iv_idno                = ls_reg_data-idno
                                                                  iv_nat_version         = ls_reg_data-zarn_reg_hdr[ 1 ]-version
                                                                  it_zarn_cc_hdr         = lt_cc_hdr
                                                                  iv_contains_val_errors = lv_val_error
                                                        IMPORTING et_zarn_approval       = lt_approval
                                                                  et_zarn_approval_ch    = lt_approval_ch
                                                                  et_zarn_appr_hist      = lt_approval_hist
                                                                  et_after_appr_actions  = lt_after_appr_action  ).



          APPEND LINES OF lt_approval TO lt_approval_db.
          APPEND LINES OF lt_approval_ch TO lt_approval_ch_db.
          APPEND LINES OF lt_approval_hist TO lt_approval_hist_db.
          APPEND LINES OF lt_after_appr_action TO lt_after_appr_act_all.

        CATCH zcx_excep_cls.

      ENDTRY.

      APPEND LINES OF lt_cc_hdr[]  TO lt_cc_hdr_db[].
      APPEND LINES OF lt_cc_det[]  TO lt_cc_det_db[].

*** ONLD-1061 Field used by Online Approved?
***     check if the approval changes have any approvals
***      - yes, then check if these changes cover any of the online fields
***      - yes, then set the ZSAW change log
**      zcl_onl_arena=>zarn_approval_process( EXPORTING it_approval_new = lt_approval
**                                                      it_approval_ch  = lt_approval_ch
**                                                      it_cc_det       = lt_cc_det ).

    ENDIF.  " IF iv_no_chg_cat = space

  ENDLOOP.



  IF iv_no_chg_cat = space.
* Change category
    SORT lt_cc_hdr_db[] BY chgid idno chg_area national_ver_id chg_category.
    DELETE ADJACENT DUPLICATES FROM lt_cc_hdr_db[]
          COMPARING chgid idno chg_area national_ver_id chg_category.



    SORT lt_cc_det_db[] BY chgid chg_category.

* Update Change Category tables
    CALL FUNCTION 'ZARN_CHANGE_CATEGORY_UPDATE'
      EXPORTING
        it_cc_hdr = lt_cc_hdr_db[]
        it_cc_det = lt_cc_det_db[].
*>>>IS1609ins Feature 3162 - AReNa Approval Stabilisation

    lo_approvals->post_approvals( EXPORTING it_approval    = lt_approval_db
                                            it_approval_ch = lt_approval_ch_db
                                            it_appr_hist   = lt_approval_hist_db ).

    lo_approvals->call_after_appr_actions( lt_after_appr_act_all ).

*<<<IS1609ins Feature 3162 - AReNa Approval Stabilisation

    IF iv_no_chg_doc = space.
* Write Change Document for Change Category Data
      IF lv_change_number IS NOT INITIAL.
        CALL FUNCTION 'ZARN_CHG_CAT_CHANGE_DOC'
          EXPORTING
            it_cc_hdr = lt_cc_hdr_db[]
*           it_cc_hdr_old =
            it_cc_det = lt_cc_det_db[]
*           it_cc_det_old =
            iv_chgid  = lv_chgid.

      ENDIF.
    ENDIF.  " IF iv_no_chg_doc = space

  ENDIF.  " IF iv_no_chg_cat = space


ENDFUNCTION.
