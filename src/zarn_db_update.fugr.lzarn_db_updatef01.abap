*----------------------------------------------------------------------*
***INCLUDE LZARN_DB_UPDATEF01 .
*----------------------------------------------------------------------*

*&---------------------------------------------------------------------*
*&      Form  BUILD_CHANGE_CATEGORY_DATA
*&---------------------------------------------------------------------*
* Build Change Category Data
*----------------------------------------------------------------------*
FORM build_change_category_data USING fu_s_table_mastr   TYPE ztarn_table_mastr
                                      fu_s_reg_data_db   TYPE zsarn_reg_data
                                      fu_s_reg_data_n    TYPE zsarn_reg_data
                                      fu_v_chgid         TYPE zarn_chg_id
                                      fu_v_change_number TYPE cdchangenr
                             CHANGING fc_t_cc_hdr      TYPE ztarn_cc_hdr
                                      fc_t_cc_det      TYPE ztarn_cc_det.

  DATA: ls_table_mastr TYPE zarn_table_mastr,
        ls_reg_data_n  TYPE zsarn_reg_data,
        ls_reg_data_db TYPE zsarn_reg_data,
        lv_itabname    TYPE char50,
        lt_cc_hdr      TYPE ztarn_cc_hdr,
        lt_cc_det      TYPE ztarn_cc_det.

  FIELD-SYMBOLS: <table_db> TYPE table,
                 <table_n>  TYPE table.


  ls_reg_data_n  = fu_s_reg_data_n.
  ls_reg_data_db = fu_s_reg_data_db.


  LOOP AT fu_s_table_mastr INTO ls_table_mastr.

    IF <table_db>   IS ASSIGNED. UNASSIGN <table_db>.   ENDIF.
    IF <table_n>    IS ASSIGNED. UNASSIGN <table_n>.    ENDIF.

    CLEAR: lv_itabname.
    CONCATENATE 'LS_REG_DATA_DB-' ls_table_mastr-arena_table INTO lv_itabname.
    ASSIGN (lv_itabname) TO <table_db>.

    CLEAR: lv_itabname.
    CONCATENATE 'LS_REG_DATA_N-' ls_table_mastr-arena_table INTO lv_itabname.
    ASSIGN (lv_itabname) TO <table_n>.

    IF <table_n> IS NOT INITIAL AND ls_table_mastr-arena_table IS NOT INITIAL.
      CLEAR: lt_cc_hdr[], lt_cc_det[].
      CALL FUNCTION 'ZARN_CHANGE_CATEGORY_DATA'
        EXPORTING
          it_table_o    = <table_db>
          it_table_n    = <table_n>
          iv_tabname    = ls_table_mastr-arena_table
          iv_chgid      = fu_v_chgid
          iv_chgdoc     = fu_v_change_number
          is_reg_data_n = ls_reg_data_n
          is_reg_data_o = ls_reg_data_db        " ++ONED-217 JKH 23.11.2016
        IMPORTING
          et_cc_hdr     = lt_cc_hdr[]
          et_cc_det     = lt_cc_det[]
        EXCEPTIONS
          input_missing = 1
          input_invalid = 1
          error         = 3
          OTHERS        = 4.

      APPEND LINES OF lt_cc_hdr[] TO fc_t_cc_hdr[].
      APPEND LINES OF lt_cc_det[] TO fc_t_cc_det[].
    ENDIF.




  ENDLOOP.

ENDFORM.                    " BUILD_CHANGE_CATEGORY_DATA
*&---------------------------------------------------------------------*
*&      Form  GET_VERSION_MULTI
*&---------------------------------------------------------------------*
*       Return the Arena version
*----------------------------------------------------------------------*
*      -->P_IT_IDNO  text
*      <--P_LT_VERSION  text
*----------------------------------------------------------------------*
FORM get_version_multi  USING    fu_t_idno    TYPE ztarn_reg_key
                        CHANGING fc_t_version TYPE ztarn_key.

  CHECK fu_t_idno IS NOT INITIAL.

  CLEAR fc_t_version.

  SELECT idno current_ver
    FROM zarn_ver_status
    INTO  TABLE fc_t_version
    FOR ALL ENTRIES IN fu_t_idno
    WHERE idno = fu_t_idno-idno.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  ENRICH_REG_DATA
*&---------------------------------------------------------------------*
*       Enrich regional data
*----------------------------------------------------------------------*
*      <--P_LT_REG_DATA  text
*----------------------------------------------------------------------*
FORM enrich_reg_data USING    fu_t_prod_data    TYPE ztarn_prod_data
                              fc_t_reg_data_old TYPE ztarn_reg_data
                     CHANGING fc_t_reg_data_new TYPE ztarn_reg_data.

  FIELD-SYMBOLS: <ls_reg_hdr> TYPE zarn_reg_hdr.

  DATA: ls_prod_data         TYPE zsarn_prod_data.

  DATA: lv_laeda TYPE laeda,
        lv_eruet TYPE aezet,
        lv_aenam TYPE aenam.

  TRY.
* Get instance of GUI load service
      DATA(lo_arena_gui_loader) = NEW zcl_arn_gui_load( ).
* Set changed details only once for each update
      lv_laeda = sy-datum.
      lv_eruet = sy-uzeit.
      lv_aenam = sy-uname.
* Enrich each record
      LOOP AT fc_t_reg_data_old ASSIGNING FIELD-SYMBOL(<ls_reg_data>).
        TRY.
            lo_arena_gui_loader->set_regional_default(
              EXPORTING
                is_reg_data         = <ls_reg_data>
                is_prod_data        = VALUE #( fu_t_prod_data[ idno = <ls_reg_data>-idno ] )
*        iv_reg_only         = SPACE    " 'X' - Get Regional Records only, no defaults
              IMPORTING
                es_reg_data_def     = DATA(ls_reg_data_enriched)
            ).
* Add the changed by details(consider moving to ZARN_REGIONAL_DB_UPDATE)
            ASSIGN ls_reg_data_enriched-zarn_reg_hdr[ 1 ] TO <ls_reg_hdr>.
            <ls_reg_hdr>-laeda = lv_laeda.
            <ls_reg_hdr>-eruet = lv_eruet.
            <ls_reg_hdr>-aenam = lv_aenam.
            APPEND ls_reg_data_enriched TO fc_t_reg_data_new.
            CLEAR ls_reg_data_enriched.
          CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
        ENDTRY.
      ENDLOOP.
    CATCH zcx_arn_gui_load_err ##NO_HANDLER.
  ENDTRY.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  VALIDATE_DATA
*&---------------------------------------------------------------------*
*       Validate data
*----------------------------------------------------------------------*
*      -->P_LT_PROD_DATA  text
*      -->P_LT_REG_DATA_NEW  text
*      <--P_LT_IDNO_VAL_ERROR  text
*----------------------------------------------------------------------*
FORM validate_data  USING    fu_t_prod_data      TYPE ztarn_prod_data
                             fu_t_reg_data_new   TYPE ztarn_reg_data
                    CHANGING fc_t_idno_val_error TYPE ztarn_idno_key.

  CLEAR fc_t_idno_val_error.

  TRY.
* Grab the engine
      DATA(lo_engine) = NEW zcl_arn_validation_engine( iv_event = zcl_arn_validation_engine=>gc_event_arena_ui iv_bapiret_output = abap_true ).

* Validate one-by-one
      LOOP AT fu_t_reg_data_new ASSIGNING FIELD-SYMBOL(<ls_reg_data>).
        TRY.
            lo_engine->validate_arn_single(
              EXPORTING
                is_zsarn_prod_data          = VALUE #( fu_t_prod_data[ idno = <ls_reg_data>-idno ] )
                is_zsarn_reg_data           = <ls_reg_data>
              IMPORTING
                et_output                   = DATA(lt_output)
            ).

* INS Begin of INC5718222 JKH 28.02.2017
** if any warning message is returned by validation, then also
** IDNO is passed to IDNO_VAL_ERROR table and auto-approvals doesn't works
** as it is considered as ERROR in validations
** Thus, warning must be ignored
            DELETE lt_output WHERE msgty NE 'E'.
* INS Begin of INC5718222 JKH 28.02.2017

* Record any errors for the idno
            IF lt_output IS NOT INITIAL.
              CLEAR lt_output.
              APPEND <ls_reg_data>-idno TO fc_t_idno_val_error.
            ENDIF.
          CATCH zcx_arn_validation_err cx_sy_itab_line_not_found ##NO_HANDLER.
        ENDTRY.
      ENDLOOP.
    CATCH zcx_arn_validation_err ##NO_HANDLER.
  ENDTRY.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  POST_ARTICLE
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->IT_ART_POST_KEY  text
*----------------------------------------------------------------------*
FORM post_article  USING    it_art_post_key TYPE ztarn_key.

  " NOTE: Logic below taken from ZRARN_GUI program,
  "       routine APPR_POST_ARTICLE. Code not relevant has been
  "       excluded.

  DATA:
    lr_status     TYPE ztarn_status_range,
    lrs_status    LIKE LINE OF lr_status,
    lt_message    TYPE bal_t_msg,
    lv_post_dest  TYPE rfcdes-rfcdest,
    lv_own_logsys TYPE tbdls-logsys,
    ls_tvarvc     TYPE tvarvc,
    lv_rfcchk     TYPE boolean.

  SELECT sign opti AS option low high
    INTO TABLE lr_status
    FROM tvarvc
    WHERE name EQ 'ZARN_ARTICLE_UPDATE_STATUS'.
  IF lr_status[] IS INITIAL.
    CLEAR lrs_status.
    lrs_status-sign   = 'I'.
    lrs_status-option = 'EQ'.
    lrs_status-low    = 'APR'.
    APPEND lrs_status TO lr_status.
  ENDIF.

  SELECT SINGLE *
    INTO ls_tvarvc
    FROM tvarvc
    WHERE name EQ 'ZARN_ARTICLE_POST_LOGSYS'
    AND   type EQ 'P'.
  IF sy-subrc EQ 0 AND ls_tvarvc-low IS NOT INITIAL.
    lv_post_dest = ls_tvarvc-low.

    CALL FUNCTION 'CHECK_RFC_DESTINATION'
      EXPORTING
        i_destination              = lv_post_dest
      IMPORTING
        e_user_password_incomplete = lv_rfcchk.
    IF lv_rfcchk NE space.
      CLEAR lv_post_dest.
    ENDIF.
  ENDIF.

  IF lv_post_dest IS INITIAL.
    CALL FUNCTION 'OWN_LOGICAL_SYSTEM_GET'
      IMPORTING
        own_logical_system             = lv_own_logsys
      EXCEPTIONS
        own_logical_system_not_defined = 1
        OTHERS                         = 2.
    lv_post_dest = lv_own_logsys.
  ENDIF.

  CALL FUNCTION 'ZARN_ARTICLE_POSTING'
    DESTINATION lv_post_dest
    EXPORTING
      it_key           = it_art_post_key
      ir_status        = lr_status
      iv_batch         = ' '
      iv_display_log   = ' '
      iv_no_change_doc = ' '
      iv_commit        = abap_false
    IMPORTING
      et_message       = lt_message.

ENDFORM.
