* regenerated at 25.05.2016 11:23:40 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_NUTRCLAIM_TTOP.              " Global Data
  INCLUDE LZARN_NUTRCLAIM_TUXX.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_NUTRCLAIM_TF...              " Subroutines
* INCLUDE LZARN_NUTRCLAIM_TO...              " PBO-Modules
* INCLUDE LZARN_NUTRCLAIM_TI...              " PAI-Modules
* INCLUDE LZARN_NUTRCLAIM_TE...              " Events
* INCLUDE LZARN_NUTRCLAIM_TP...              " Local class implement.
* INCLUDE LZARN_NUTRCLAIM_TT99.              " ABAP Unit tests
  INCLUDE LZARN_NUTRCLAIM_TF00                    . " subprograms
  INCLUDE LZARN_NUTRCLAIM_TI00                    . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
