*&---------------------------------------------------------------------*
*&  Include           ZIARN_GUI_CL01
*&---------------------------------------------------------------------*
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 13.04.2018
* CHANGE No.       # Charm 9000003482; Jira CI18-554
* DESCRIPTION      # CI18-554 Recipe Management - Ingredient Changes
*                  # New regional table for Allergen Type overrides and
*                  # fields for Ingredients statement overrides in Arena.
*                  # New Ingredients Type field in the Scales block in
*                  # Arena and NONSAP tab in material master.
* WHO              # C90005557, Tessa Newman
*-----------------------------------------------------------------------
* DATE             # November 2018
* CHANGE No.       # Version B-03199
* DESCRIPTION      # Add Online tab to Regional
* WHO              # C90007074, Jos van Gysen
*-----------------------------------------------------------------------
*---------------------------------------------------------------------*
*       CLASS lcl_eventhandler DEFINITION
*---------------------------------------------------------------------*
*
*---------------------------------------------------------------------*

CLASS lcl_event_handler_edit DEFINITION.

  PUBLIC SECTION.
    METHODS:
      handle_data_changed
                  FOR EVENT data_changed OF cl_gui_alv_grid
        IMPORTING er_data_changed
                  e_ucomm
                  sender,

      handle_hotspot_click
                  FOR EVENT hotspot_click OF cl_gui_alv_grid
        IMPORTING e_row_id e_column_id es_row_no sender,

      handle_close
                  FOR EVENT close OF cl_gui_dialogbox_container
        IMPORTING sender,


*-- Method to handle F4 action from the controls which are registered
      handle_on_f4  FOR EVENT onf4 OF cl_gui_alv_grid
        IMPORTING e_fieldname
                  es_row_no
                  er_event_data
                  e_display
                  sender,

      handle_toolbar
                  FOR EVENT toolbar OF cl_gui_alv_grid
        IMPORTING e_object e_interactive sender,

      handle_user_command
                  FOR EVENT user_command OF cl_gui_alv_grid
        IMPORTING e_ucomm sender.

ENDCLASS.                    "lcl_eventhandler DEFINITION

*---------------------------------------------------------------------*
*       CLASS lcl_eventhandler IMPLEMENTATION
*---------------------------------------------------------------------*
*
*---------------------------------------------------------------------*
CLASS lcl_event_handler_edit IMPLEMENTATION.

  METHOD handle_data_changed.

    DATA: lo_grid TYPE REF TO cl_gui_alv_grid.
    lo_grid ?= sender.


    CASE sender.
      WHEN go_worklist_alv.
        RETURN.  " Seperate Handler for this ALV !

      WHEN go_reg_uom_alv.
        PERFORM manage_key_field_values
          USING gc_alv_reg_uom go_reg_uom_alv CHANGING er_data_changed
                                        gv_data_change.
      WHEN go_reg_sc_alv.
        PERFORM manage_key_field_values
          USING gc_alv_reg_sc go_reg_sc_alv CHANGING er_data_changed
                                        gv_data_change.
      WHEN go_reg_pir_alv.
        PERFORM manage_key_field_values
          USING gc_alv_reg_pir go_reg_pir_alv CHANGING er_data_changed
                                                       gv_data_change.

      WHEN go_reg_rb_alv.
        PERFORM manage_key_field_values
          USING gc_alv_reg_rb  go_reg_rb_alv CHANGING er_data_changed
                                                      gv_data_change.

      WHEN go_reg_gtn_alv.
        PERFORM manage_key_field_values
          USING gc_alv_reg_gtn go_reg_gtn_alv CHANGING er_data_changed
                                                       gv_data_change.


      WHEN go_reg_con_artlink_alv.      " ++ONED-217 JKH 21.11.2016
        PERFORM manage_key_field_values
          USING gc_alv_reg_artlink_ui go_reg_con_artlink_alv CHANGING er_data_changed
                                                                      gv_data_change.


      WHEN go_reg_con_onlcat_alv.      " ++ONLD-835 JKH 16.05.2017
        PERFORM manage_key_field_values
          USING gc_alv_reg_onlcat_ui go_reg_con_onlcat_alv CHANGING er_data_changed
                                                                    gv_data_change.

      WHEN OTHERS.
    ENDCASE.


  ENDMETHOD.                    "handle_data_changed
*--------------------------------------------------------------------
  METHOD handle_close.
* ^Handle the CLOSE-button of the dialogbox
* (the dialogbox is destroyed outomatically when the user
* switches to another dynpro).
    CALL METHOD go_dialogbox_container->free.
    FREE go_dialogbox_container.
* closing the dialogbox leads to make it invisible.
* It is also conceivable to destroy it
* and recreate it if the user doubleclicks a line again.
* Displaying a great amount of data has a greater impact on performance.
  ENDMETHOD.                    "handle_close


  METHOD handle_hotspot_click.

    DATA: ls_zarn_reg_uom  LIKE LINE OF gt_zarn_reg_uom,


          lv_tabix         TYPE sy-tabix,
          ls_zarn_reg_iuom LIKE LINE OF gt_zarn_reg_uom,


          lo_grid          TYPE REF TO cl_gui_alv_grid.
*      ls_reg_pir LIKE LINE OF gt_zarn_reg_pir.

    FIELD-SYMBOLS:
      <ls_reg_uom> LIKE LINE OF gt_zarn_reg_uom.
*      <ls_reg_pir> LIKE LINE OF gt_zarn_reg_pir.

    CHECK gv_display IS INITIAL.
    CLEAR gv_revert_issue_uom.

*   to get the sender for identification
    lo_grid ?= sender.

*   Handle Checkbox like a radiobutton
    CASE sender.

      WHEN go_reg_uom_alv.

        CLEAR ls_zarn_reg_uom.
        READ TABLE gt_zarn_reg_uom INTO ls_zarn_reg_uom INDEX e_row_id.
        IF sy-subrc = 0.
          IF ls_zarn_reg_uom-meinh IS NOT INITIAL.

            IF e_column_id EQ gc_unit_issue.  "'ISSUE_UNIT'.
              READ TABLE gt_zarn_reg_uom INTO ls_zarn_reg_iuom TRANSPORTING meinh
                WITH KEY issue_unit = abap_true.
              IF sy-subrc EQ 0.
                PERFORM popup_approve_issue_unit USING ls_zarn_reg_iuom-meinh " old
                                                       ls_zarn_reg_uom-meinh. " new
              ENDIF.

              IF gv_revert_issue_uom IS NOT INITIAL.
                RETURN.
              ENDIF.
            ENDIF.

            LOOP AT gt_zarn_reg_uom ASSIGNING <ls_reg_uom>.

              lv_tabix = sy-tabix.

*       based on the field clicked !
              CASE  e_column_id.
                WHEN gc_unit_base. "'UNIT_BASE'.
                  IF lv_tabix EQ e_row_id.
                    <ls_reg_uom>-unit_base = abap_true.
                  ELSE.
                    CLEAR <ls_reg_uom>-unit_base.
                  ENDIF.
                WHEN gc_unit_po. "  'UNIT_PURORD'.
                  IF lv_tabix EQ e_row_id.
                    <ls_reg_uom>-unit_purord = abap_true.
                  ELSE.
                    CLEAR <ls_reg_uom>-unit_purord.
                  ENDIF.
                WHEN   gc_unit_sales. " 'SALES_UNIT'.
                  IF lv_tabix EQ e_row_id.
                    <ls_reg_uom>-sales_unit = abap_true.
                  ELSE.
                    CLEAR <ls_reg_uom>-sales_unit.
                  ENDIF.
                WHEN   gc_unit_issue.  "'ISSUE_UNIT'.
                  IF lv_tabix EQ e_row_id.
                    <ls_reg_uom>-issue_unit = abap_true.
                  ELSE.
                    CLEAR <ls_reg_uom>-issue_unit.
                  ENDIF.
              ENDCASE.

            ENDLOOP.

          ELSE.

* SAP UOM is required for Unit attribute
            MESSAGE s082 DISPLAY LIKE 'E'.

          ENDIF.  " IF ls_zarn_reg_uom-meinh IS NOT INITIAL
        ENDIF.  " READ TABLE gt_zarn_reg_uom INTO ls_zarn_reg_uom INDEX e_row_id


***           refresh Screen values
        cl_gui_cfw=>set_new_ok_code( new_code = 'DUMMY').
**        CALL METHOD go_reg_uom_alv->refresh_table_display.

*      WHEN go_reg_pir_alv.
*        " get changed row
*        READ TABLE gt_zarn_reg_pir INTO ls_reg_pir INDEX e_row_id.
*
*        LOOP AT gt_zarn_reg_pir ASSIGNING <ls_reg_pir>.
**         based on the field clicked !
*          CASE  e_column_id.
*            WHEN  gc_po_eine.
*              IF sy-tabix EQ e_row_id.
*                " set allowed ZERO price to 'X' in case of price is initial
*                IF <ls_reg_pir>-netpr IS INITIAL.
*                  <ls_reg_pir>-allow_zero_price = abap_true.
*                ENDIF.
*                <ls_reg_pir>-pir_rel_eine = abap_true.
*              ELSEIF ls_reg_pir-gln_no EQ <ls_reg_pir>-gln_no.
*                CLEAR <ls_reg_pir>-pir_rel_eine.
*                CLEAR <ls_reg_pir>-allow_zero_price.
*              ENDIF.
*            WHEN  gc_po_eina.
*
*              IF sy-tabix EQ e_row_id.
*                <ls_reg_pir>-pir_rel_eina = abap_true.
*                " clear per GLN
*              ELSEIF ls_reg_pir-gln_no EQ <ls_reg_pir>-gln_no.
*                CLEAR <ls_reg_pir>-pir_rel_eina.
*              ENDIF.
*
*            WHEN gc_norm_vend. " 'RELIF'
*              IF sy-tabix EQ e_row_id.
*                <ls_reg_pir>-relif = abap_true.
*                IF <ls_reg_pir>-lifnr NE 'MULTIPLE'.
*                  zarn_reg_pir-lifnr = <ls_reg_pir>-lifnr.
*                  PERFORM get_vendor_text.
*                ELSE.
*                  CLEAR zarn_reg_pir-lifnr.
*                  PERFORM get_vendor_text.
*                ENDIF.
*              ELSE.
*                CLEAR <ls_reg_pir>-relif.
*              ENDIF.
*
*          ENDCASE.
*
*        ENDLOOP.
***           refresh Screen values
*        cl_gui_cfw=>set_new_ok_code( new_code = 'DUMMY').
*        CALL METHOD go_reg_pir_alv->refresh_table_display.

      WHEN OTHERS.
    ENDCASE.

  ENDMETHOD.                    "handle_hotspot_click

*&--  Method of Handle f4
  METHOD handle_on_f4.

    DATA lt_uom             TYPE TABLE OF ty_art_uom.
    DATA ls_uom             TYPE ty_art_uom.
    DATA ls_zarn_uom_cat    LIKE LINE OF gt_zarn_uom_cat.
    DATA lt_ret             TYPE TABLE OF ddshretval.
    DATA ls_modi            TYPE lvc_s_modi.
    DATA ls_zarn_reg_uom    LIKE LINE OF gt_zarn_reg_uom.
    DATA ls_zarn_reg_banner LIKE LINE OF gt_zarn_reg_banner.
    DATA ls_sel             TYPE ddshretval.
    DATA ls_marm            LIKE LINE OF gt_marm_uom.
    DATA ls_zarn_reg_ean    LIKE LINE OF gt_zarn_reg_ean.
    DATA ls_style           TYPE lvc_s_styl.
    DATA lv_display         TYPE ddbool_d.

    DATA: ls_zarn_reg_onlcat TYPE zarn_reg_onlcat.


    FIELD-SYMBOLS: <itab> TYPE lvc_t_modi.

    CASE sender.

*-----Event triggered from Reg. UOM ALV
      WHEN go_reg_uom_alv.

*-- F4 for UOM field MEINS
        IF e_fieldname EQ gc_uom_l_meins.

          FREE lt_uom[].
*-- Get the item to show the relevant F4
          READ TABLE gt_zarn_reg_uom INDEX es_row_no-row_id
                                     INTO ls_zarn_reg_uom .
          IF sy-subrc NE 0.
            RETURN.
          ENDIF.

*-- Get the possible UOM's
          LOOP AT gt_zarn_uom_cat INTO ls_zarn_uom_cat
            WHERE cat_seqno LE ls_zarn_reg_uom-cat_seqno.

            READ TABLE gt_zarn_reg_uom TRANSPORTING NO FIELDS
              WITH KEY meinh = ls_zarn_uom_cat-uom.
            IF sy-subrc EQ 0.
              ls_uom-uom = ls_zarn_uom_cat-uom.
              APPEND ls_uom TO lt_uom. CLEAR ls_uom.
            ENDIF.
          ENDLOOP.

*-- Call the function module to display the custom F4 values
          CALL FUNCTION 'F4IF_INT_TABLE_VALUE_REQUEST'
            EXPORTING
              retfield        = gc_uom_l_meins
              window_title    = 'SAP Lower UOM'
              value_org       = 'S'
            TABLES
              value_tab       = lt_uom
              return_tab      = lt_ret
            EXCEPTIONS
              parameter_error = 1
              no_values_found = 2
              OTHERS          = 3.
          IF sy-subrc = 0.
            READ TABLE lt_ret INTO ls_sel INDEX 1.
            IF ls_sel-fieldval IS NOT INITIAL.
              ASSIGN er_event_data->m_data->* TO <itab>.
              ls_modi-row_id   = es_row_no-row_id.
              ls_modi-fieldname = gc_uom_l_meins.
              ls_modi-value     = ls_sel-fieldval.
              APPEND ls_modi TO <itab>.
            ENDIF.
          ENDIF.

          er_event_data->m_event_handled = 'X'.

        ENDIF.

*-- F4 for UOM field MEINS
        IF e_fieldname EQ gc_hyb_code.

*-- Get the item to show the relevant F4
          READ TABLE gt_zarn_reg_uom INDEX es_row_no-row_id
                                     INTO ls_zarn_reg_uom .
          IF sy-subrc NE 0.
            RETURN.
          ENDIF.

          IF ls_zarn_reg_uom-celltab IS NOT INITIAL.
            READ TABLE ls_zarn_reg_uom-celltab INTO ls_style
              WITH KEY fieldname = gc_hyb_code.
            IF sy-subrc EQ 0.
              RETURN.
            ENDIF.
          ENDIF.

*-- Call the function module to display the custom F4 values
          CALL FUNCTION 'F4IF_INT_TABLE_VALUE_REQUEST'
            EXPORTING
              retfield        = gc_hyb_code
              window_title    = 'UOM Addition Scenario'
              value_org       = 'S'
            TABLES
              value_tab       = gt_hyb_int_code
              return_tab      = lt_ret
            EXCEPTIONS
              parameter_error = 1
              no_values_found = 2
              OTHERS          = 3.
          IF sy-subrc = 0.
            READ TABLE lt_ret INTO ls_sel INDEX 1.
            IF ls_sel-fieldval IS NOT INITIAL.
              ASSIGN er_event_data->m_data->* TO <itab>.
              ls_modi-row_id   = es_row_no-row_id.
              ls_modi-fieldname = gc_hyb_code.
              ls_modi-value     = ls_sel-fieldval.
              APPEND ls_modi TO <itab>.
            ENDIF.
          ENDIF.

          er_event_data->m_event_handled = 'X'.

        ENDIF.

*-----Event triggered from Reg. Banner ALV
      WHEN go_reg_rb_alv.

*-- F4 for UOM field
        IF e_fieldname EQ gc_vrkme.

          FREE lt_uom[].
*-- Get the item to show the relevant F4
          READ TABLE gt_zarn_reg_banner INDEX es_row_no-row_id
                                     INTO ls_zarn_reg_banner.
          IF sy-subrc NE 0.
            RETURN.
          ENDIF.

*-- Get the possible UOM's
          LOOP AT gt_zarn_reg_uom INTO ls_zarn_reg_uom.
            ls_uom-uom = ls_zarn_reg_uom-meinh.
            APPEND ls_uom TO lt_uom. CLEAR ls_uom.
          ENDLOOP.

          " set marm uom in F4 if there!
          IF gt_marm_uom[] IS NOT INITIAL.
            LOOP AT gt_marm_uom INTO ls_marm.
              READ TABLE lt_uom TRANSPORTING NO FIELDS WITH KEY uom = ls_marm-uom.
              IF sy-subrc NE 0.
                ls_uom-uom = ls_marm-uom.
                APPEND ls_uom TO lt_uom. CLEAR ls_uom.
              ENDIF.
            ENDLOOP.
          ENDIF.

*-- Call the function module to display the custom F4 values
          CALL FUNCTION 'F4IF_INT_TABLE_VALUE_REQUEST'
            EXPORTING
              retfield        = gc_vrkme
              window_title    = 'Sales UOM'
              value_org       = 'S'
            TABLES
              value_tab       = lt_uom
              return_tab      = lt_ret
            EXCEPTIONS
              parameter_error = 1
              no_values_found = 2
              OTHERS          = 3.
          IF sy-subrc = 0.
            READ TABLE lt_ret INTO ls_sel INDEX 1.
            IF ls_sel-fieldval IS NOT INITIAL.
              ASSIGN er_event_data->m_data->* TO <itab>.
              ls_modi-row_id   = es_row_no-row_id.
              ls_modi-fieldname = gc_vrkme.
              ls_modi-value     = ls_sel-fieldval.
              APPEND ls_modi TO <itab>.
            ENDIF.
          ENDIF.

          er_event_data->m_event_handled = 'X'.

        ENDIF.


*-----Event triggered from Reg. Banner ALV
      WHEN go_reg_gtn_alv.

*-- F4 for UOM field
        IF e_fieldname EQ gc_uom_meins.


          FREE lt_uom[].
*-- Get the item to show the relevant F4
          READ TABLE gt_zarn_reg_ean INDEX es_row_no-row_id
                                     INTO ls_zarn_reg_ean.
          IF sy-subrc NE 0.
            RETURN.
          ELSE.
            READ TABLE ls_zarn_reg_ean-celltab[] TRANSPORTING NO FIELDS
            WITH KEY fieldname = gc_uom_meins " 'MEINH'
                     style     = cl_gui_alv_grid=>mc_style_disabled.
            IF sy-subrc = 0.
              lv_display = abap_true.
            ELSE.
              CLEAR lv_display.
            ENDIF.
          ENDIF.

*-- Get the possible UOM's
          LOOP AT gt_zarn_reg_uom INTO ls_zarn_reg_uom.
            ls_uom-uom = ls_zarn_reg_uom-meinh.
            APPEND ls_uom TO lt_uom. CLEAR ls_uom.
          ENDLOOP.

          " set marm uom in F4 if there!
          IF gt_marm_uom[] IS NOT INITIAL.
            LOOP AT gt_marm_uom INTO ls_marm.
              READ TABLE lt_uom TRANSPORTING NO FIELDS WITH KEY uom = ls_marm-uom.
              IF sy-subrc NE 0.
                ls_uom-uom = ls_marm-uom.
                APPEND ls_uom TO lt_uom. CLEAR ls_uom.
              ENDIF.
            ENDLOOP.
          ENDIF.

*-- Call the function module to display the custom F4 values
          CALL FUNCTION 'F4IF_INT_TABLE_VALUE_REQUEST'
            EXPORTING
              retfield        = gc_uom_meins
              window_title    = 'UOM'
              value_org       = 'S'
              display         = lv_display
            TABLES
              value_tab       = lt_uom
              return_tab      = lt_ret
            EXCEPTIONS
              parameter_error = 1
              no_values_found = 2
              OTHERS          = 3.
          IF sy-subrc = 0.
            READ TABLE lt_ret INTO ls_sel INDEX 1.
            IF ls_sel-fieldval IS NOT INITIAL.
              ASSIGN er_event_data->m_data->* TO <itab>.
              ls_modi-row_id   = es_row_no-row_id.
              ls_modi-fieldname = gc_uom_meins.
              ls_modi-value     = ls_sel-fieldval.
              APPEND ls_modi TO <itab>.
            ENDIF.
          ENDIF.

          er_event_data->m_event_handled = 'X'.

        ENDIF.

* INS Begin of Change ONLD-835 JKH 16.05.2017
      WHEN go_reg_con_onlcat_alv.

        CLEAR ls_zarn_reg_onlcat.
        READ TABLE gt_zarn_reg_onlcat INDEX es_row_no-row_id
                                      INTO ls_zarn_reg_onlcat.
        IF sy-subrc NE 0.
          RETURN.
        ENDIF.

        PERFORM f4_online_categories USING ls_zarn_reg_onlcat
                                           e_fieldname
                                           es_row_no
                                  CHANGING er_event_data.

* INS End of Change ONLD-835 JKH 16.05.2017

      WHEN OTHERS.

    ENDCASE.

  ENDMETHOD.  "HANDLE_ON_F4
  METHOD handle_user_command.
* At event USER_COMMAND query the function code of each function

    DATA:
      lt_rows TYPE lvc_t_row,
      lo_grid TYPE REF TO cl_gui_alv_grid.

*   to get the sender for identification
    lo_grid ?= sender.

*   ALV Worklist Processing
    CASE e_ucomm.
      WHEN 'ADD_UOM'.
        PERFORM alv_add_reg_uom USING go_reg_uom_alv.


      WHEN 'DEL_UOM'.
*   get selected row in banner table
        CALL METHOD go_reg_uom_alv->get_selected_rows
          IMPORTING
            et_index_rows = lt_rows.

        IF lt_rows[] IS INITIAL.
          MESSAGE s012 DISPLAY LIKE 'E'.
          RETURN.
        ENDIF.

        PERFORM alv_del_reg_uom USING lt_rows.

      WHEN 'ADD_CAT'.             "++ONLD-835 JKH 12.05.2017
        PERFORM alv_add_reg_onlcat USING go_reg_con_onlcat_alv.


      WHEN 'DEL_CAT'.             "++ONLD-835 JKH 12.05.2017
*   get selected row in category table
        CALL METHOD go_reg_con_onlcat_alv->get_selected_rows
          IMPORTING
            et_index_rows = lt_rows.

        IF lt_rows[] IS INITIAL.
          MESSAGE s012 DISPLAY LIKE 'E'.
          RETURN.
        ENDIF.

        PERFORM alv_del_reg_onlcat USING lt_rows.


      WHEN OTHERS.


    ENDCASE.

  ENDMETHOD.
  METHOD handle_toolbar.

*    in event handler method for event toolbar: append own functions
*    by using event parameter e_object.
    DATA: ls_toolbar TYPE stb_button,
          lo_grid    TYPE REF TO cl_gui_alv_grid.

    lo_grid ?= sender.

    CASE lo_grid.

      WHEN go_reg_uom_alv.
        CLEAR ls_toolbar.
        MOVE 3 TO ls_toolbar-butn_type.
        APPEND ls_toolbar TO e_object->mt_toolbar.
*
        CLEAR ls_toolbar.
        MOVE 'ADD_UOM' TO ls_toolbar-function.
        MOVE icon_add_row TO ls_toolbar-icon.
        MOVE 'Add' TO ls_toolbar-quickinfo.
        MOVE 'Add' TO ls_toolbar-text.

*     display/change
        IF gv_display IS INITIAL.
          MOVE ' ' TO ls_toolbar-disabled.
        ELSE.
          MOVE 'X' TO ls_toolbar-disabled.
        ENDIF.

        INSERT ls_toolbar INTO TABLE e_object->mt_toolbar.
*
*        CLEAR ls_toolbar.
*        MOVE 3 TO ls_toolbar-butn_type.
*        APPEND ls_toolbar TO e_object->mt_toolbar.
*
        CLEAR ls_toolbar.
        MOVE 'DEL_UOM' TO ls_toolbar-function.
        MOVE icon_delete_row TO ls_toolbar-icon.
        MOVE 'Delete' TO ls_toolbar-quickinfo.
        MOVE 'Delete' TO ls_toolbar-text.

*     display/change
        IF gv_display IS INITIAL.
          MOVE ' ' TO ls_toolbar-disabled.
        ELSE.
          MOVE 'X' TO ls_toolbar-disabled.
        ENDIF.

        INSERT ls_toolbar INTO TABLE e_object->mt_toolbar.

      WHEN go_reg_con_onlcat_alv.             "++ONLD-835 JKH 12.05.2017
        CLEAR ls_toolbar.
        MOVE 3 TO ls_toolbar-butn_type.
        APPEND ls_toolbar TO e_object->mt_toolbar.
*


        CLEAR ls_toolbar.
        MOVE 'ADD_CAT' TO ls_toolbar-function.
        MOVE icon_add_row TO ls_toolbar-icon.
        MOVE 'Add' TO ls_toolbar-quickinfo.
        MOVE 'Add' TO ls_toolbar-text.

*     display/change
        IF gv_display IS INITIAL.
          MOVE ' ' TO ls_toolbar-disabled.
        ELSE.
          MOVE 'X' TO ls_toolbar-disabled.
        ENDIF.

        INSERT ls_toolbar INTO TABLE e_object->mt_toolbar.



        CLEAR ls_toolbar.
        MOVE 'DEL_CAT' TO ls_toolbar-function.
        MOVE icon_delete_row TO ls_toolbar-icon.
        MOVE 'Delete' TO ls_toolbar-quickinfo.
        MOVE 'Delete' TO ls_toolbar-text.

*     display/change
        IF gv_display IS INITIAL.
          MOVE ' ' TO ls_toolbar-disabled.
        ELSE.
          MOVE 'X' TO ls_toolbar-disabled.
        ENDIF.

        INSERT ls_toolbar INTO TABLE e_object->mt_toolbar.

      WHEN OTHERS.
    ENDCASE.

  ENDMETHOD.


ENDCLASS.                    "lcl_eventhandler IMPLEMENTATION
*---------------------------------------------------------------------*
*       CLASS lcl_reg_banner_ui DEFINITION
*---------------------------------------------------------------------*
*
*---------------------------------------------------------------------*
CLASS lcl_reg_banner_ui DEFINITION.

  PUBLIC SECTION.
    CLASS-METHODS: get_instance RETURNING VALUE(ro_instance) TYPE REF TO lcl_reg_banner_ui.
    METHODS:
      set_alv_instance IMPORTING io_alv TYPE REF TO cl_gui_alv_grid,

      handle_data_changed
                  FOR EVENT data_changed OF cl_gui_alv_grid
        IMPORTING er_data_changed
                  e_ucomm
                  sender,

      handle_data_changed_finished
                  FOR EVENT data_changed_finished OF cl_gui_alv_grid
        IMPORTING e_modified
                  et_good_cells,

      handle_display_mode_changed
                  FOR EVENT display_mode_changed OF lcl_controller
        IMPORTING ev_display
                  ev_alv_edit_mode,

      validate_input
        IMPORTING
          it_mod_cells      TYPE lvc_t_modi OPTIONAL
          it_data           TYPE ztarn_reg_banner_ui
        RETURNING
          VALUE(rv_changed) TYPE abap_bool,

      adjust_ui
        IMPORTING
          io_mod_data TYPE REF TO cl_alv_changed_data_protocol.

  PRIVATE SECTION.
    CLASS-DATA: so_singleton TYPE REF TO lcl_reg_banner_ui.
    DATA: mo_alv TYPE REF TO cl_gui_alv_grid.

ENDCLASS.                    "lcl_reg_banner_ui DEFINITION

*---------------------------------------------------------------------*
*       CLASS lcl_reg_banner_ui IMPLEMENTATION
*---------------------------------------------------------------------*
*
*---------------------------------------------------------------------*
CLASS lcl_reg_banner_ui IMPLEMENTATION.

  METHOD get_instance.
    IF so_singleton IS NOT BOUND.
      so_singleton = NEW #( ).
    ENDIF.
    ro_instance = so_singleton.
  ENDMETHOD.

  METHOD set_alv_instance.
    mo_alv = io_alv.
  ENDMETHOD.

  METHOD handle_data_changed.


    FIELD-SYMBOLS: <lt_mod_rows>         TYPE ztarn_reg_banner_ui.

    ASSIGN er_data_changed->mp_mod_rows->* TO <lt_mod_rows>.

* Validate input
    IF me->validate_input(
        EXPORTING
          it_mod_cells  = er_data_changed->mt_mod_cells
          it_data       = <lt_mod_rows>
      ).
      gv_data_change = abap_true.
    ENDIF.

* Adjust the UI after a change
    me->adjust_ui(
      EXPORTING
        io_mod_data  = er_data_changed
    ).

  ENDMETHOD.                    "handle_data_changed

  METHOD handle_data_changed_finished.
    IF line_exists( et_good_cells[ fieldname = 'SSTUF' ] ).
      lcl_reg_cluster_ui=>get_instance( )->update_regional_cluster( EXPORTING iv_force_cluster_update = abap_true
                                                                              it_zarn_reg_banner      = gt_zarn_reg_banner
                                                                    CHANGING  ct_reg_cluster_ui       = gt_zarn_reg_cluster ).
    ENDIF.
  ENDMETHOD.

  METHOD handle_display_mode_changed.
    IF mo_alv IS BOUND.
      mo_alv->set_ready_for_input( i_ready_for_input = ev_alv_edit_mode ).
    ENDIF.
  ENDMETHOD.

*---------------------------------------------------------------------*
*       VALIDATE_INPUT
*---------------------------------------------------------------------*
* Input validations
*---------------------------------------------------------------------*
  METHOD validate_input.

    LOOP AT it_mod_cells INTO DATA(ls_mod_cells).

      READ TABLE it_data ASSIGNING FIELD-SYMBOL(<ls_mod_rows_rb>) INDEX ls_mod_cells-tabix.
      IF sy-subrc EQ 0.
*         based on the field clicked !
        IF ls_mod_cells-fieldname EQ gc_vrkme AND <ls_mod_rows_rb>-vrkme IS NOT INITIAL.
*-- Check the possible UOM's
          READ TABLE gt_zarn_reg_uom TRANSPORTING NO FIELDS
            WITH KEY meinh = <ls_mod_rows_rb>-vrkme.
          IF sy-subrc NE 0 AND gt_marm_uom[] IS INITIAL.
            MESSAGE s017 DISPLAY LIKE 'E'.
          ELSEIF sy-subrc NE 0 AND gt_marm_uom[] IS NOT INITIAL.
            READ TABLE gt_marm_uom TRANSPORTING NO FIELDS
              WITH KEY uom = <ls_mod_rows_rb>-vrkme.
            IF sy-subrc NE 0.
              MESSAGE s017 DISPLAY LIKE 'E'.
            ENDIF.
          ENDIF.

        ELSEIF ls_mod_cells-fieldname EQ 'ZZCATMAN' OR ls_mod_cells-fieldname EQ 'SSTUF'.
          IF ls_mod_cells-value IS INITIAL AND
            ( <ls_mod_rows_rb>-banner EQ zcl_constants=>gc_banner_4000 OR
              <ls_mod_rows_rb>-banner  EQ zcl_constants=>gc_banner_5000 OR
              <ls_mod_rows_rb>-banner EQ zcl_constants=>gc_banner_6000 ) AND
            gv_banner_def_ret IS NOT INITIAL.

            MESSAGE s038 WITH ls_mod_cells-fieldname 'Retail Ranging' DISPLAY LIKE 'E'.

          ELSEIF ls_mod_cells-value IS INITIAL AND
            <ls_mod_rows_rb>-banner EQ zcl_constants=>gc_banner_3000 AND
            gv_banner_def_gil IS NOT INITIAL.

            MESSAGE s038 WITH ls_mod_cells-fieldname 'Wholesale Ranging' DISPLAY LIKE 'E'.

          ELSEIF ls_mod_cells-value IS INITIAL AND
            <ls_mod_rows_rb>-banner EQ zcl_constants=>gc_banner_1000 AND
            zarn_reg_hdr-bwscl EQ gc_bwscl_2
            AND ls_mod_cells-fieldname NE 'SSTUF'.

            MESSAGE s038 WITH ls_mod_cells-fieldname 'DC Source of Supply' DISPLAY LIKE 'E'.

          ENDIF.

** INS Begin of Change ONLD-822 JKH 17.01.2017
*        ELSEIF ls_mod_cells-fieldname EQ gc_online_status.
*
*          IF ls_mod_cells-value IS NOT INITIAL.
*
*            CLEAR: lv_excluded.
*            TRY.
*
** Is article excluded from Online catalogue
*                CALL METHOD zcl_onl_art_range_exclusion=>is_article_excluded
*                  EXPORTING
**                   iv_article      =
*                    iv_arena_id_num = <ls_mod_rows_rb>-idno
*                    iv_banner       = <ls_mod_rows_rb>-banner
**                   iv_store        =
*                  RECEIVING
*                    rv_value        = lv_excluded.
*
*              CATCH zcx_invalid_input.
*
*            ENDTRY.
*
*            IF lv_excluded = abap_true AND
*               ls_mod_cells-value NE zcl_onl_arena=>mc_onl_stat_3.
*              " Banner '&' is excluded, Online Status must be '03'
*              MESSAGE s125 WITH <ls_mod_rows_rb>-banner DISPLAY LIKE 'E'.
*            ENDIF.
*
*          ENDIF.  " IF ls_mod_cells-value IS NOT INITIAL
** INS End of Change ONLD-822 JKH 17.01.2017

        ENDIF.

* flag a change
        rv_changed = abap_true.
      ENDIF.
    ENDLOOP.

  ENDMETHOD.
*---------------------------------------------------------------------*
*       ADJUST_UI
*---------------------------------------------------------------------*
* Adjust the UI with additional data(texts,etc) after a change
*---------------------------------------------------------------------*
  METHOD adjust_ui.

    DATA: lv_value      TYPE lvc_value,
          lv_fieldname  TYPE lvc_fname,
          ls_reg_banner TYPE zarn_reg_banner,
          lv_tabix      TYPE sy-tabix.

    FIELD-SYMBOLS: <lt_mod_rows> TYPE ztarn_reg_banner_ui,
                   <ls_mod_rows> TYPE zsarn_reg_banner_ui.



    ASSIGN io_mod_data->mp_mod_rows->* TO <lt_mod_rows>.


* Enrich UI
    LOOP AT io_mod_data->mt_mod_cells ASSIGNING FIELD-SYMBOL(<ls_mod_cell>).
      CLEAR lv_value.
      CASE <ls_mod_cell>-fieldname.

* Online status
        WHEN gc_online_status.

* INS Begin of Change ONLD-822 JKH 16.01.2017
          IF <ls_mod_cell>-value IS INITIAL.

            IF <ls_mod_rows> IS ASSIGNED. UNASSIGN <ls_mod_rows>. ENDIF.
            READ TABLE <lt_mod_rows> ASSIGNING <ls_mod_rows> INDEX <ls_mod_cell>-tabix.
            IF <ls_mod_rows> IS ASSIGNED.

              CLEAR ls_reg_banner.
              MOVE-CORRESPONDING <ls_mod_rows> TO ls_reg_banner.

              IF ls_reg_banner-banner NE zcl_constants=>gc_banner_1000.

* Check if Banner if Live for Online
                IF abap_true = zcl_onl_art_range_exclusion=>is_banner_prep_for_online( ls_reg_banner-banner ).
                  TRY.
                      <ls_mod_cell>-value = lv_value = zcl_onl_arena=>get_default_onl_stat(
                          is_reg_hdr    = zarn_reg_hdr
                          is_reg_banner = ls_reg_banner ).
                    CATCH zcx_invalid_input INTO DATA(lo_exc).
                  ENDTRY.

                  io_mod_data->modify_cell(
                    EXPORTING
                      i_row_id    = <ls_mod_cell>-row_id
                      i_fieldname = gc_online_status
                      i_value     = lv_value ).

                ENDIF.

              ENDIF.

            ENDIF.  " IF <ls_mod_rows> IS ASSIGNED
          ENDIF.  " IF <ls_mod_cell>-value IS INITIAL
* INS End of Change ONLD-822 JKH 16.01.2017



          lv_value =  zcl_md_olr_cust=>get_ols_single( CONV #( <ls_mod_cell>-value ) )-description.
          lv_fieldname = |TEXT_{ gc_online_status }|.

*          IF lv_value IS NOT INITIAL.    "--ONLD-822 JKH 16.01.2017
          io_mod_data->modify_cell(
            EXPORTING
              i_row_id    = <ls_mod_cell>-row_id
              i_fieldname = lv_fieldname
              i_value     = lv_value
          ).
*          ENDIF.

        WHEN gc_pbs_code.
          lv_value =  zcl_arn_pbs_data_check=>get_pbs_single(
                                     CONV #( <ls_mod_cell>-value ) )-pbs_name_text.
          lv_fieldname = gc_pbs_name.

          io_mod_data->modify_cell(
            EXPORTING
              i_row_id    = <ls_mod_cell>-row_id
              i_fieldname = lv_fieldname
              i_value     = lv_value
          ).

        WHEN gc_scagr.      "++CI18-124 JKH 06.07.2017

          IF <ls_mod_rows> IS ASSIGNED. UNASSIGN <ls_mod_rows>. ENDIF.
          READ TABLE <lt_mod_rows> ASSIGNING <ls_mod_rows> INDEX <ls_mod_cell>-tabix.
          IF <ls_mod_rows> IS ASSIGNED.
            IF <ls_mod_rows>-banner EQ '4000' OR
               <ls_mod_rows>-banner EQ '5000' OR
               <ls_mod_rows>-banner EQ '6000'.

              LOOP AT gt_zarn_reg_banner[] ASSIGNING FIELD-SYMBOL(<ls_reg_rb>)
                WHERE banner EQ '4000'
                   OR banner EQ '5000'
                   OR banner EQ '6000'.
                lv_tabix = sy-tabix.
                lv_fieldname = gc_scagr.
                <ls_reg_rb>-scagr = <ls_mod_rows>-scagr.
              ENDLOOP.
              zarn_reg_hdr-scagr = <ls_mod_rows>-scagr.

              PERFORM create_zs_zg_ean_111.

**       refresh Screen values
              cl_gui_cfw=>set_new_ok_code( new_code = 'SYSTEM').
            ENDIF. " <ls_mod_rows>-banner EQ 4000/5000/6000
          ENDIF.  " IF <ls_mod_rows> IS ASSIGNED

      ENDCASE.

    ENDLOOP.

  ENDMETHOD.

ENDCLASS.                    "lcl_reg_banner_ui IMPLEMENTATION

*---------------------------------------------------------------------*
*       Class relevant global declarations
*---------------------------------------------------------------------*
*
*---------------------------------------------------------------------*

DATA:
  go_event_receiver_edit TYPE REF TO lcl_event_handler_edit,
  go_reg_banner_ui       TYPE REF TO lcl_reg_banner_ui.


CLASS lcl_report DEFINITION.
*
  PUBLIC SECTION.
    DATA: lo_html TYPE REF TO cl_gui_html_viewer.
**          lo_html TYPE REF TO cl_ssf_html_viewer. "tried this for set_zoom percent
    METHODS:
      generate_html_button,
      build_html
        RETURNING VALUE(rt_html) TYPE  w3_htmltab.

ENDCLASS.                    "lcl_report DEFINITION

DATA: lo_report TYPE REF TO lcl_report.

DATA: my_container        TYPE REF TO cl_gui_custom_container,
      prog_repid          LIKE sy-repid,
      lv_sys              TYPE string,
      lv_article_target   TYPE string,
      lv_article_64_url   TYPE string,
      lv_wholesale_url    TYPE string,
      lv_wholesale_target TYPE string,
      lv_admin_url        TYPE string.

*----------------------------------------------------------------------*
* Local Class Implementation
*----------------------------------------------------------------------*
CLASS lcl_report IMPLEMENTATION.
  METHOD generate_html_button.
*   local data
    DATA: t_event_tab TYPE cntl_simple_events.
    DATA: ls_event LIKE LINE OF t_event_tab.
    DATA: doc_url(80),
          lt_html TYPE TABLE OF w3_html.

    IF NOT lo_html IS INITIAL.
      lo_html->free( ).
    ENDIF.

    IF NOT my_container IS INITIAL.
      my_container->free( ).
    ENDIF.
    prog_repid = sy-repid.

    CREATE OBJECT my_container
      EXPORTING
        container_name = '114_HTML_CONTROL'.


    CREATE OBJECT lo_html
      EXPORTING
        parent = my_container.
    IF sy-subrc NE 0.
      MESSAGE 'Error in the HTML control' TYPE 'S'.
      EXIT.
    ENDIF.

    lo_html->set_width( 150 ).

*   Build HTML
    lt_html = me->build_html( ).
*
*   register event
    ls_event-eventid = lo_html->m_id_sapevent.
    ls_event-appl_event = 'x'.
    APPEND ls_event TO t_event_tab.
    lo_html->set_registered_events(
        EXPORTING
           events = t_event_tab ).

    lo_html->load_data( IMPORTING assigned_url = doc_url
                          CHANGING  data_table = lt_html ).

    lo_html->show_url( url = doc_url ).

    lo_html->set_ui_flag( lo_html->uiflag_no3dborder ).

  ENDMETHOD.                    "generate_html_button
*
  METHOD build_html.
    DATA: lv_med_desc        TYPE text100,
          lv_reg_med_desc    TYPE text100,
          lv_reg_cust_short  TYPE text100,
          lv_multipack       TYPE text100,
          lv_nat_pack        TYPE text100,
          lv_marketing       TYPE text1000,
          lv_string255       TYPE text255,
          lv_nat_ingredients TYPE string,
          lv_reg_marketing   TYPE string,
          lv_reg_ingredients TYPE string,
          ls_reg_data        TYPE zsarn_reg_data,
          ls_prod_desc       TYPE zvw_prod_desc,
          lt_tvarvc          TYPE tvarvc_t.

    CONSTANTS: lc_arena_url_admin TYPE rvari_vnam VALUE 'ZONL_ARENA_ADMIN_UI_URL'.

    DEFINE add_html.
      APPEND &1 TO rt_html.
    END-OF-DEFINITION.


*   default to current values
    SELECT SINGLE * FROM zvw_prod_desc INTO ls_prod_desc
      WHERE matnr = zarn_reg_hdr-matnr.

*--------------------------------------------------------------------------------------------------
* National Data - start with National Values
*--------------------------------------------------------------------------------------------------
*   Pack: National short description
    lv_nat_pack        = zarn_products_ex-fs_cust_short_descr.
*   Marketing: National long description
    lv_marketing       = gs_prod_str-fs_cust_long_descr.
*   Ingredients
    lv_nat_ingredients = gs_prod_str-ingredient_statement.
*   Medium description
    lv_med_desc        = gs_prod_str-fs_cust_medium_descr.

*--------------------------------------------------------------------------------------------------
* Regional Data - get Regional Values
*--------------------------------------------------------------------------------------------------
    lv_reg_med_desc    = gs_reg_str-fs_cust_medium_descr.
    lv_reg_marketing   = gs_reg_str-fs_cust_long_descr.
    lv_reg_ingredients = gs_reg_str-ingredient_statement.
    lv_multipack       = zarn_reg_hdr-multipack_text.
    lv_reg_cust_short  = zarn_reg_hdr-fs_cust_short_descr.

*   get Regional header and String texts - may have changed
    APPEND zarn_reg_hdr TO ls_reg_data-zarn_reg_hdr.
    IF gs_reg_str-mandt IS INITIAL.
      gs_reg_str-mandt = sy-mandt.
      gs_reg_str-idno  = zarn_reg_hdr-idno.
    ENDIF.
    APPEND gs_reg_str   TO ls_reg_data-zarn_reg_str.
    PERFORM get_text_editor_descr CHANGING ls_reg_data-zarn_reg_hdr[]
                                           ls_reg_data-zarn_reg_str[].

*   Regional medium and long description
    READ TABLE ls_reg_data-zarn_reg_str WITH KEY idno = zarn_reg_hdr-idno ASSIGNING FIELD-SYMBOL(<ls_reg_str>).
    IF sy-subrc = 0.
*      IF  <ls_reg_str>-fs_cust_medium_descr IS NOT INITIAL.
      lv_reg_med_desc  = <ls_reg_str>-fs_cust_medium_descr.
*      ENDIF.
*      IF <ls_reg_str>-fs_cust_long_descr IS NOT INITIAL.
      lv_reg_marketing = <ls_reg_str>-fs_cust_long_descr.
*      ENDIF.
*      IF <ls_reg_str>-ingredient_statement IS NOT INITIAL.
      lv_reg_ingredients = <ls_reg_str>-ingredient_statement.
*      ENDIF.
    ENDIF.

    IF lv_reg_ingredients IS INITIAL.
      lv_reg_ingredients = lv_nat_ingredients.
    ENDIF.

*   packaging - if multipack is empty use regional customer short desc
    READ TABLE ls_reg_data-zarn_reg_hdr WITH KEY idno = zarn_reg_hdr-idno ASSIGNING FIELD-SYMBOL(<ls_reg_hdr>).
    IF sy-subrc = 0.
      lv_reg_cust_short = <ls_reg_hdr>-fs_cust_short_descr.
    ENDIF.

    IF NOT lv_reg_marketing IS INITIAL.
      lv_marketing = lv_reg_marketing.
    ENDIF.


*   Final Description: lv_brand + lv_med_desc + lv_multipack
**    lv_article_url    = 'http://picking-images.newworld.co.nz/FAN/' && zarn_products-fan_id && '.png'.
**    lv_article_64_url = 'http://picking-images.newworld.co.nz/FAN/' && zarn_products-fan_id && '_64.png'.

    CASE sy-sysid.
      WHEN 'DE0'.
        lv_sys = '/dev'.
      WHEN 'QE1'.
        lv_sys = '/qa'.
    ENDCASE.

*   set up base url
    lv_article_64_url = lv_article_target   = 'https://a.fsimg.co.nz' && lv_sys && '/product/retail/fan/image'.
    lv_wholesale_url  = lv_wholesale_target = 'https://a.fsimg.co.nz' && lv_sys && '/product/wholesale/fan/image'.

*   add rest
    lv_article_64_url   = lv_article_64_url   && '/140x140/' && zarn_products-fan_id && '.png'.
    lv_article_target   = lv_article_target   && '/400x400/' && zarn_products-fan_id && '.png'.
    lv_wholesale_url    = lv_wholesale_url    && '/100x100/' && zarn_products-fan_id && '.png'.
    lv_wholesale_target = lv_wholesale_target && '/500x500/' && zarn_products-fan_id && '.png'.

    DATA(lv_temp) = lv_article_64_url.

*   See STVARV  'ZONL_ARENA_ADMIN_UI_URL'.
    APPEND INITIAL LINE TO lt_tvarvc ASSIGNING FIELD-SYMBOL(<ls_tvarvc>).
    <ls_tvarvc>-name = lc_arena_url_admin.

    zcl_stvarv_constant=>get_stvarv_constant(
      CHANGING
        xt_stvarv_constant = lt_tvarvc ).
    LOOP AT lt_tvarvc ASSIGNING <ls_tvarvc>.
      lv_admin_url = <ls_tvarvc>-low.
      EXIT.
    ENDLOOP.
    IF sy-subrc NE 0.
      CASE sy-sysid.
        WHEN 'DE0'.  lv_admin_url = 'https://fsnz-admin-ingress-dev.dev.fsniwaikato.kiwi/ui/home/image-management/view-approve'.
        WHEN 'QE1'.  lv_admin_url = 'https://fsnz-admin-ingress-qa.test.fsniwaikato.kiwi/ui/home/image-management/view-approve'.
        WHEN 'PE1'.  lv_admin_url = 'https://fsnz-admin-ingress-qa.test.fsniwaikato.kiwi/ui/home/image-management/view-approve'.
        WHEN OTHERS. lv_admin_url = 'https://fsnz-admin-ingress-dev.dev.fsniwaikato.kiwi/ui/home/image-management/view-approve'.
      ENDCASE.
    ENDIF.

    lv_article_64_url = '<a href="' && lv_article_target   && '" target="_blank"><img src="' && lv_article_64_url && '" width="80%" height="80%"/></a>'.
    lv_wholesale_url  = '<a href="' && lv_wholesale_target && '" target="_blank"><img src="' && lv_wholesale_url  && '" width="80%" height="80%"/></a>'.
    lv_admin_url      = '<a href="' && lv_admin_url        && '" target="_blank"> Admin_UI</a>'.

*   get current product description (includes article characteristics)
**    DATA(lv_product_description) = zcl_online_services=>get_product_description( iv_matnr = zarn_reg_hdr-matnr )-medium_desc.

    ls_prod_desc-reg_str_fs_cust_medium        = lv_reg_med_desc.
    ls_prod_desc-prod_str_fs_cust_medium_descr = lv_med_desc.
    ls_prod_desc-multipack_text                = zarn_reg_hdr-multipack_text.
    ls_prod_desc-reg_hdr_brand_id              = zarn_reg_hdr-brand_id.

*   get latest brand description
    SELECT SINGLE brand_descr FROM wrf_brands_t INTO gv_brand_id_desc
      WHERE brand_id = zarn_reg_hdr-brand_id
      AND   language = sy-langu.
    ls_prod_desc-wrf_brand_descr = gv_brand_id_desc.

    ls_prod_desc-reg_hdr_brand_override        = zarn_reg_hdr-brand_override.
    ls_prod_desc-onl_net_content               = zarn_reg_hdr-onl_net_content.
    ls_prod_desc-onl_net_content_uom           = zarn_reg_hdr-onl_net_content_uom.
    "IS 2019/07 for multipack description, enable also regional Net Content as fallback
    ls_prod_desc-inhal                         = zarn_reg_hdr-inhal.
    ls_prod_desc-inhme                         = zarn_reg_hdr-inhme.
    ls_prod_desc-reg_hdr_fs_cust_short_desc    = lv_reg_cust_short.

*   Build latest product description
    DATA(ls_new_desc)  = zcl_online_services=>get_product_description_v2( EXPORTING iv_matnr = zarn_reg_hdr-matnr iv_read_char = '' is_prod_desc = ls_prod_desc
                                                                                    iv_multipack_reg_netcontent = abap_true ).
    DATA(lv_new_long)  = ls_new_desc-medium_desc.
    DATA(lv_new_short) = ls_new_desc-short_desc.

    add_html:
      '<html>',

      '<style type="text/css">',
      'HTML { overflow: auto; height: 100%;}',
      'body{margin: 0;  padding: 0; background: white; font-family:Arial; }',
      'document.body.style.webkitTransform =         // Chrome, Opera, Safari',
      'document.body.style.msTransform =           // IE 9',
      `document.body.style.transform = 'scale(4)';`,

      'input.fbutton{',
      ' font-size:15px;',
      ' font-weight:bold;',
      ' width:300px;',
      '	height:30px;',
      ' background-color: orange',
      '	border-style:double;',
      '	cursor: pointer;}',

      '.text01{ line-height:1.200; font-size:15px; font-weight:bold; text-align:left !important; width:300px;}',
*      '.text02{ line-height:1.075; white-space:pre-wrap;  padding:1em; text-align:left;}',
*      '.text02{ line-height:1.075; white-space:pre-wrap; margin: 10px 20px 20px 20px; text-align:left; font-size: 75.0%;}',
      '.text02{ line-height:1.075; white-space:pre-line; margin: 1px 20px 1px 20px; font-size: 75.0%;}',
      '.outer { width:90%; margin:0 auto; outline:1px solid red; text-align:left;}',



      '</style>',
      '<body>', "{ font-size: 50.0%;}', "transform: scale(0.5); transform-origin: 0 0;}>',  "font-size: 62.5%

      '<table>',

*       Show small image plus short description and large image plus long description
        '<tr>',
          '<td style="vertical-align: top;">',
          '<table>',
            '<tr>', " <th>Search View</th> <th  style="vertical-align: top;">Full Display View</th>',

          '<FORM name="zzhtmlbutton"',
          '       method=post',
          '       action=SAPEVENT:MY_BUTTON_1?PARAM1=Hello&PARAM2=Jos>',
          '<input type=submit name="TEST_BUTTON"',
          '       class="fbutton" value="Refresh Descriptions"',
          '       title="">',
          '</form>',

          '<th style="width:100px;font-size:12px;">Retail</th> <th  style="vertical-align: top; font-size:12px;;">Wholesale</th>',



            '</tr>',
            '<tr><td style="width:100px;">', lv_article_64_url, '</td>',
                '<td style="width:100px;">', lv_wholesale_url,    '</td></tr>',

            '<tr><td>', lv_admin_url,      '</td>',
                '<td>',      '</td></tr>',
          '</table>',
          '</td>',

*         Show long description with fields in colours
          '<td  style="vertical-align: top;">',
           '<table>',
             '<tr> <td>',

                '<span style=" background-color:#FFF843; font-size:15px;">', ls_new_desc-desc_brand,   '</span>',    "List Total, intensified
                '<span style=" background-color:#A6E5F4; font-size:15px;">', ls_new_desc-desc_medium,  '</span>'.    "List Heading, intensified

*         Show packing in colour depending on where it came from
    lv_multipack = ls_new_desc-desc_multipack.
    CASE ls_new_desc-multipack_source.
      WHEN '1'.    "MultiPack Text
        add_html: '<span style=" background-color:#FF6758; font-size:15px;">', lv_multipack, '</span>'.    "List Negative, intensified - red
      WHEN '2'.    "Size Unit
        add_html: '<span style=" background-color:#D4DFEF; font-size:15px;">', lv_multipack, '</span>'.    "List Normal,   intensified - grey
      WHEN '3'.    "Onl_Content
        add_html: '<span style=" background-color:#FF6758; font-size:15px;">', lv_multipack, '</span>'.    "List Normal,   intensified - red
    ENDCASE.


    add_html:
         '</td></tr>',
         '<td>'.

    IF NOT lv_marketing IS INITIAL.
      lv_string255 = lv_marketing.
      add_html:
      '<span class="text01">', 'Marketing Text:', '</span>',
       '<div class="outer">',
       '<div class="text02">',
       cl_abap_char_utilities=>newline.

      add_html: lv_string255.
      DO.
        IF strlen( lv_marketing ) > 255.
          SHIFT lv_marketing LEFT BY 255 PLACES.
          lv_string255 = lv_marketing.
          add_html: lv_string255.
        ELSE.
          add_html: cl_abap_char_utilities=>newline.
          EXIT.
        ENDIF.
      ENDDO.

      add_html: '</div></div>'.
    ENDIF.

    IF NOT lv_reg_ingredients IS INITIAL.

      lv_string255 = lv_reg_ingredients.
      add_html:
      '<span class="text01">', 'Ingredients:', '</span>',
       '<div class="outer">',
       '<div class="text02">',
       cl_abap_char_utilities=>newline.

      add_html: lv_string255.
      DO.
        IF strlen( lv_reg_ingredients ) > 255.
          SHIFT lv_reg_ingredients LEFT BY 255 PLACES.
          lv_string255 = lv_reg_ingredients.
          add_html: lv_string255.
        ELSE.
          add_html: cl_abap_char_utilities=>newline.
          EXIT.
        ENDIF.
      ENDDO.

      add_html: '</div></div>'.

    ENDIF.

*   Allergens
    LOOP AT gt_zarn_reg_allerg ASSIGNING FIELD-SYMBOL(<ls_allerg>).
      IF sy-tabix > 1.
        add_html: ','.
      ENDIF.
      AT FIRST.
        add_html: '<br>', '<span class="text01">', 'Allergens:', '</span>'.
      ENDAT.
      add_html: '<span style=" font-size:15px;">'.

      IF NOT <ls_allerg>-lvl_containment_ovr IS INITIAL.
        DATA(lv_cont) = <ls_allerg>-lvl_containment_ovr.
      ELSE.
        lv_cont = <ls_allerg>-lvl_containment.
      ENDIF.
      zcl_online_services=>change_text_case( EXPORTING iv_text = lv_cont  IMPORTING ev_result = lv_cont ).
      add_html: lv_cont, ':'.

      IF NOT <ls_allerg>-allergen_type_ovr_desc_fsni IS INITIAL.
        add_html: <ls_allerg>-allergen_type_ovr_desc_fsni.
      ELSE.
        add_html: <ls_allerg>-allergen_type_desc_fsni.
      ENDIF.
      add_html: '</span>'.
    ENDLOOP.

    "if alcohol
    IF zarn_reg_hdr-matkl(3) = '145'.   "Liquor
      add_html:
          '<br><span class="text01">', 'Alcohol Percentage:', '</span>', zarn_products_ex-alcohol_pct, '%'.
    ENDIF.

    add_html:
      '</span>',
      '</table></td></tr>',
      '</body>',
      '</html>'.

  ENDMETHOD.                    "build_html

ENDCLASS.                    "lcl_report IMPLEMENTATION
