*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Functional Specification 3174
* DATE WRITTEN     # 20.03.2016
* SAP VERSION      # 740
* TYPE             # Function
* AUTHOR           # C90001363, Jitin Kharbanda
*-----------------------------------------------------------------------
* TITLE            # AReNa: Send I-Care flag to Hybris (R3)
* PURPOSE          # AReNa: Send I-Care flag to Hybris (R3)
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------
FUNCTION z_hybris_set_r3_async.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(REQUEST) TYPE  ZPRODUCT_STAGING_ICARE_REQUES4
*"  EXPORTING
*"     VALUE(RESPONSE) TYPE  ZPRODUCT_STAGING_ICARE_RESPON4
*"     VALUE(ES_CONTROL) TYPE  ZARN_CONTROL
*"     VALUE(ES_PRD_VERSION) TYPE  ZARN_PRD_VERSION
*"     VALUE(ES_VER_STATUS) TYPE  ZARN_VER_STATUS
*"     VALUE(MESSAGES) TYPE  BAL_T_MSG
*"----------------------------------------------------------------------

* Call Proxy and build AReNa control data
  PERFORM hybris_set_r3 USING request
                     CHANGING response
                              es_control
                              es_prd_version
                              es_ver_status
                              messages[].

ENDFUNCTION.
