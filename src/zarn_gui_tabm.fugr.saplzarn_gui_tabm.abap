* regenerated at 25.02.2016 15:38:07 by  C90002769
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_GUI_TABMTOP.                 " Global Data
  INCLUDE LZARN_GUI_TABMUXX.                 " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_GUI_TABMF...                 " Subroutines
* INCLUDE LZARN_GUI_TABMO...                 " PBO-Modules
* INCLUDE LZARN_GUI_TABMI...                 " PAI-Modules
* INCLUDE LZARN_GUI_TABME...                 " Events
* INCLUDE LZARN_GUI_TABMP...                 " Local class implement.
* INCLUDE LZARN_GUI_TABMT99.                 " ABAP Unit tests
  INCLUDE LZARN_GUI_TABMF00                       . " subprograms
  INCLUDE LZARN_GUI_TABMI00                       . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
