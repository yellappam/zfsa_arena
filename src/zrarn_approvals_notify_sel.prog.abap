*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Functional Specification 3132
* DATE WRITTEN     # 07 03 2016
* SAP VERSION      # 731
* TYPE             # Report Program
* AUTHOR           # C90001448, Greg van der Loeff
*-----------------------------------------------------------------------
* TITLE            # AReNa: Send notifications for approvals
* PURPOSE          # AReNa: Send notifications for approvals
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------
*&---------------------------------------------------------------------*
*&  Include           ZRARN_APPROVALS_NOTIFY_SEL
*&---------------------------------------------------------------------*
TABLES: zarn_products.
SELECTION-SCREEN BEGIN OF BLOCK tst WITH FRAME TITLE text-tst.
PARAMETERS: p_consum AS CHECKBOX DEFAULT 'X'.
SELECT-OPTIONS: s_idno  FOR zarn_products-idno,
                s_fanid FOR zarn_products-fan_id.
SELECTION-SCREEN END OF BLOCK tst.
