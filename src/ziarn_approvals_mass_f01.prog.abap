*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 11.05.2020
* CHANGE No.       # Charm 9000007115
* DESCRIPTION      # IDNO field does not exist on table ZARN_REG_ARTLINK
*                  # Checking if IDNO exist in the field list
* WHO              # I90009976, Ashwaan Morris
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_APPROVALS_MASS_F01
*&---------------------------------------------------------------------*


*&---------------------------------------------------------------------*
*&      Form  BUILD_HIERARCHY_HEADER
*&---------------------------------------------------------------------*
* Set the hierarchy header
*----------------------------------------------------------------------*
FORM build_hierarchy_header_100 CHANGING fc_s_hierarchy_header TYPE treev_hhdr.

  CLEAR fc_s_hierarchy_header.
  fc_s_hierarchy_header-heading   = 'BP Code/Chg Area/Chg Cat/Table/Field'.
  fc_s_hierarchy_header-tooltip   = 'BP Code/Chg Area/Chg Cat/Table/Field'.
  fc_s_hierarchy_header-width     = 60.
  fc_s_hierarchy_header-width_pix = ''.

ENDFORM.  " BUILD_HIERARCHY_HEADER
*&---------------------------------------------------------------------*
*&      Form  BUILD_VARIANT
*&---------------------------------------------------------------------*
* Set the variant info
*----------------------------------------------------------------------*
FORM build_variant_100 CHANGING fc_s_variant TYPE disvariant.

  CLEAR fc_s_variant.
  fc_s_variant-report   = sy-repid.
  fc_s_variant-username = sy-uname.
  fc_s_variant-handle   = 'TRE'.

ENDFORM.  " BUILD_VARIANT
*&---------------------------------------------------------------------*
*&      Form  BUILD_FIELDCATALOG_100
*&---------------------------------------------------------------------*
* Set the fieldcatalog
*----------------------------------------------------------------------*
FORM build_fieldcatalog_100 CHANGING fc_t_fieldcat TYPE lvc_t_fcat.


  FIELD-SYMBOLS: <ls_fieldcat> TYPE lvc_s_fcat.

  CLEAR: fc_t_fieldcat[].

* The following function module generates a fieldcatalog according
* to a given structure.
  CALL FUNCTION 'LVC_FIELDCATALOG_MERGE'
    EXPORTING
      i_structure_name   = 'ZSARN_APPROVALS_MASS_TREE'
      i_bypassing_buffer = 'X'
    CHANGING
      ct_fieldcat        = fc_t_fieldcat[].

  LOOP AT fc_t_fieldcat[] ASSIGNING <ls_fieldcat>.

    <ls_fieldcat>-tooltip = <ls_fieldcat>-scrtext_l.
    <ls_fieldcat>-col_opt = abap_true.



    CASE <ls_fieldcat>-fieldname.
      WHEN 'IDNO'.
        <ls_fieldcat>-tech = abap_true.

      WHEN 'CHG_AREA'.
        <ls_fieldcat>-tech = abap_true.

      WHEN 'CHG_CATEGORY'.
        <ls_fieldcat>-tech = abap_true.

      WHEN 'CHG_TABLE'.
        <ls_fieldcat>-tech = abap_true.

      WHEN 'CHG_FIELD'.
        <ls_fieldcat>-tech = abap_true.

      WHEN 'NODE_KEY'.
        <ls_fieldcat>-tech = abap_true.

      WHEN 'ITEM_KEY'.
        <ls_fieldcat>-tech = abap_true.

      WHEN 'LINE_TYPE'.
        <ls_fieldcat>-tech = abap_true.

      WHEN 'DESCRIPTION'.
        <ls_fieldcat>-outputlen = 30.

      WHEN 'ADD_DETAILS'.
        <ls_fieldcat>-outputlen = 30.

      WHEN 'STATUS'.
        <ls_fieldcat>-outputlen = 10.

      WHEN 'LOCK_USER'.
        <ls_fieldcat>-outputlen = 15.
        <ls_fieldcat>-tech = abap_true.

      WHEN 'LOCKED_BY'.
        <ls_fieldcat>-outputlen = 15.

      WHEN 'LOCK_IND'.
        <ls_fieldcat>-outputlen = 7.
        <ls_fieldcat>-icon      = abap_true.

      WHEN 'APPR01'.
        <ls_fieldcat>-scrtext_l = <ls_fieldcat>-scrtext_m = <ls_fieldcat>-scrtext_s = <ls_fieldcat>-coltext = <ls_fieldcat>-reptext = 'CT'.
        <ls_fieldcat>-tooltip = 'Category Team - Approve'.
        <ls_fieldcat>-outputlen = 6.
        <ls_fieldcat>-icon      = abap_true.
        <ls_fieldcat>-hotspot   = abap_true.
        <ls_fieldcat>-edit      = abap_true.

      WHEN 'STAT01'.
        <ls_fieldcat>-tech = abap_true.
        <ls_fieldcat>-scrtext_l = <ls_fieldcat>-scrtext_m = <ls_fieldcat>-scrtext_s = <ls_fieldcat>-coltext = <ls_fieldcat>-reptext = 'CT'.
        <ls_fieldcat>-tooltip = 'Category Team - Status'.
        <ls_fieldcat>-outputlen = 6.
        <ls_fieldcat>-icon      = abap_true.
        <ls_fieldcat>-hotspot   = abap_true.
        <ls_fieldcat>-edit      = abap_true.

      WHEN 'REJE01'.
        <ls_fieldcat>-scrtext_l = <ls_fieldcat>-scrtext_m = <ls_fieldcat>-scrtext_s = <ls_fieldcat>-coltext = <ls_fieldcat>-reptext = 'CT'.
        <ls_fieldcat>-tooltip = 'Category Team - Reject'.
        <ls_fieldcat>-outputlen = 6.
        <ls_fieldcat>-icon      = abap_true.
        <ls_fieldcat>-hotspot   = abap_true.
        <ls_fieldcat>-edit      = abap_true.

      WHEN 'TEXT01'.
        <ls_fieldcat>-scrtext_l = <ls_fieldcat>-scrtext_m = <ls_fieldcat>-scrtext_s = <ls_fieldcat>-coltext = <ls_fieldcat>-reptext = 'CT'.
        <ls_fieldcat>-tooltip = 'Category Team - Comment'.
        <ls_fieldcat>-outputlen = 6.
        <ls_fieldcat>-icon      = abap_true.
        <ls_fieldcat>-hotspot   = abap_true.
        <ls_fieldcat>-edit      = abap_true.

      WHEN 'MASS01'.
        <ls_fieldcat>-outputlen = 6.
        <ls_fieldcat>-tech = abap_true.



      WHEN 'APPR02'.
        <ls_fieldcat>-scrtext_l = <ls_fieldcat>-scrtext_m = <ls_fieldcat>-scrtext_s = <ls_fieldcat>-coltext = <ls_fieldcat>-reptext = 'BY'.
        <ls_fieldcat>-tooltip = 'Buyer - Approve'.
        <ls_fieldcat>-outputlen = 6.
        <ls_fieldcat>-icon      = abap_true.
        <ls_fieldcat>-hotspot   = abap_true.
        <ls_fieldcat>-edit      = abap_true.

      WHEN 'STAT02'.
        <ls_fieldcat>-tech = abap_true.
        <ls_fieldcat>-scrtext_l = <ls_fieldcat>-scrtext_m = <ls_fieldcat>-scrtext_s = <ls_fieldcat>-coltext = <ls_fieldcat>-reptext = 'BY'.
        <ls_fieldcat>-tooltip = 'Buyer - Status'.
        <ls_fieldcat>-outputlen = 6.
        <ls_fieldcat>-icon      = abap_true.
        <ls_fieldcat>-hotspot   = abap_true.
        <ls_fieldcat>-edit      = abap_true.

      WHEN 'REJE02'.
        <ls_fieldcat>-scrtext_l = <ls_fieldcat>-scrtext_m = <ls_fieldcat>-scrtext_s = <ls_fieldcat>-coltext = <ls_fieldcat>-reptext = 'BY'.
        <ls_fieldcat>-tooltip = 'Buyer - Reject'.
        <ls_fieldcat>-outputlen = 6.
        <ls_fieldcat>-icon      = abap_true.
        <ls_fieldcat>-hotspot   = abap_true.
        <ls_fieldcat>-edit      = abap_true.

      WHEN 'TEXT02'.
        <ls_fieldcat>-scrtext_l = <ls_fieldcat>-scrtext_m = <ls_fieldcat>-scrtext_s = <ls_fieldcat>-coltext = <ls_fieldcat>-reptext = 'BY'.
        <ls_fieldcat>-tooltip = 'Buyer - Comment'.
        <ls_fieldcat>-outputlen = 6.
        <ls_fieldcat>-icon      = abap_true.
        <ls_fieldcat>-hotspot   = abap_true.
        <ls_fieldcat>-edit      = abap_true.

      WHEN 'MASS02'.
        <ls_fieldcat>-outputlen = 6.
        <ls_fieldcat>-tech = abap_true.





      WHEN 'APPR03'.
        <ls_fieldcat>-scrtext_l = <ls_fieldcat>-scrtext_m = <ls_fieldcat>-scrtext_s = <ls_fieldcat>-coltext = <ls_fieldcat>-reptext = 'AM'.
        <ls_fieldcat>-tooltip = 'AMD Specialist - Approve'.
        <ls_fieldcat>-outputlen = 6.
        <ls_fieldcat>-icon      = abap_true.
        <ls_fieldcat>-hotspot   = abap_true.
        <ls_fieldcat>-edit      = abap_true.

      WHEN 'STAT03'.
        <ls_fieldcat>-tech = abap_true.
        <ls_fieldcat>-scrtext_l = <ls_fieldcat>-scrtext_m = <ls_fieldcat>-scrtext_s = <ls_fieldcat>-coltext = <ls_fieldcat>-reptext = 'AM'.
        <ls_fieldcat>-tooltip = 'AMD Specialist - Status'.
        <ls_fieldcat>-outputlen = 6.
        <ls_fieldcat>-icon      = abap_true.
        <ls_fieldcat>-hotspot   = abap_true.
        <ls_fieldcat>-edit      = abap_true.

      WHEN 'REJE03'.
        <ls_fieldcat>-scrtext_l = <ls_fieldcat>-scrtext_m = <ls_fieldcat>-scrtext_s = <ls_fieldcat>-coltext = <ls_fieldcat>-reptext = 'AM'.
        <ls_fieldcat>-tooltip = 'AMD Specialist - Reject'.
        <ls_fieldcat>-outputlen = 6.
        <ls_fieldcat>-icon      = abap_true.
        <ls_fieldcat>-hotspot   = abap_true.
        <ls_fieldcat>-edit      = abap_true.

      WHEN 'TEXT03'.
        <ls_fieldcat>-scrtext_l = <ls_fieldcat>-scrtext_m = <ls_fieldcat>-scrtext_s = <ls_fieldcat>-coltext = <ls_fieldcat>-reptext = 'AM'.
        <ls_fieldcat>-tooltip = 'AMD Specialist - Comment'.
        <ls_fieldcat>-outputlen = 6.
        <ls_fieldcat>-icon      = abap_true.
        <ls_fieldcat>-hotspot   = abap_true.
        <ls_fieldcat>-edit      = abap_true.

      WHEN 'MASS03'.
        <ls_fieldcat>-outputlen = 6.
        <ls_fieldcat>-tech = abap_true.






      WHEN 'APPR04'.
        <ls_fieldcat>-scrtext_l = <ls_fieldcat>-scrtext_m = <ls_fieldcat>-scrtext_s = <ls_fieldcat>-coltext = <ls_fieldcat>-reptext = 'PT'.
        <ls_fieldcat>-tooltip = 'AMD-Pricing - Approve'.
        <ls_fieldcat>-outputlen = 6.
        <ls_fieldcat>-icon      = abap_true.
        <ls_fieldcat>-hotspot   = abap_true.
        <ls_fieldcat>-edit      = abap_true.

      WHEN 'STAT04'.
        <ls_fieldcat>-tech = abap_true.
        <ls_fieldcat>-scrtext_l = <ls_fieldcat>-scrtext_m = <ls_fieldcat>-scrtext_s = <ls_fieldcat>-coltext = <ls_fieldcat>-reptext = 'PT'.
        <ls_fieldcat>-tooltip = 'AMD-Pricing - Status'.
        <ls_fieldcat>-outputlen = 6.
        <ls_fieldcat>-icon      = abap_true.
        <ls_fieldcat>-hotspot   = abap_true.
        <ls_fieldcat>-edit      = abap_true.

      WHEN 'REJE04'.
        <ls_fieldcat>-scrtext_l = <ls_fieldcat>-scrtext_m = <ls_fieldcat>-scrtext_s = <ls_fieldcat>-coltext = <ls_fieldcat>-reptext = 'PT'.
        <ls_fieldcat>-tooltip = 'AMD-Pricing - Reject'.
        <ls_fieldcat>-outputlen = 6.
        <ls_fieldcat>-icon      = abap_true.
        <ls_fieldcat>-hotspot   = abap_true.
        <ls_fieldcat>-edit      = abap_true.

      WHEN 'TEXT04'.
        <ls_fieldcat>-scrtext_l = <ls_fieldcat>-scrtext_m = <ls_fieldcat>-scrtext_s = <ls_fieldcat>-coltext = <ls_fieldcat>-reptext = 'PT'.
        <ls_fieldcat>-tooltip = 'AMD-Pricing - Comment'.
        <ls_fieldcat>-outputlen = 6.
        <ls_fieldcat>-icon      = abap_true.
        <ls_fieldcat>-hotspot   = abap_true.
        <ls_fieldcat>-edit      = abap_true.

      WHEN 'MASS04'.
        <ls_fieldcat>-outputlen = 6.
        <ls_fieldcat>-tech = abap_true.






      WHEN 'APPR05'.
        <ls_fieldcat>-scrtext_l = <ls_fieldcat>-scrtext_m = <ls_fieldcat>-scrtext_s = <ls_fieldcat>-coltext = <ls_fieldcat>-reptext = 'MG'.
        <ls_fieldcat>-tooltip = 'AMD-Mgr/Analyst - Approve'.
        <ls_fieldcat>-outputlen = 6.
        <ls_fieldcat>-icon      = abap_true.
        <ls_fieldcat>-hotspot   = abap_true.
        <ls_fieldcat>-edit      = abap_true.

      WHEN 'STAT05'.
        <ls_fieldcat>-tech = abap_true.
        <ls_fieldcat>-scrtext_l = <ls_fieldcat>-scrtext_m = <ls_fieldcat>-scrtext_s = <ls_fieldcat>-coltext = <ls_fieldcat>-reptext = 'MG'.
        <ls_fieldcat>-tooltip = 'AMD-Mgr/Analyst - Status'.
        <ls_fieldcat>-outputlen = 6.
        <ls_fieldcat>-icon      = abap_true.
        <ls_fieldcat>-hotspot   = abap_true.
        <ls_fieldcat>-edit      = abap_true.

      WHEN 'REJE05'.
        <ls_fieldcat>-scrtext_l = <ls_fieldcat>-scrtext_m = <ls_fieldcat>-scrtext_s = <ls_fieldcat>-coltext = <ls_fieldcat>-reptext = 'MG'.
        <ls_fieldcat>-tooltip = 'AMD-Mgr/Analyst - Reject'.
        <ls_fieldcat>-outputlen = 6.
        <ls_fieldcat>-icon      = abap_true.
        <ls_fieldcat>-hotspot   = abap_true.
        <ls_fieldcat>-edit      = abap_true.

      WHEN 'TEXT05'.
        <ls_fieldcat>-scrtext_l = <ls_fieldcat>-scrtext_m = <ls_fieldcat>-scrtext_s = <ls_fieldcat>-coltext = <ls_fieldcat>-reptext = 'MG'.
        <ls_fieldcat>-tooltip = 'AMD-Mgr/Analyst - Comment'.
        <ls_fieldcat>-outputlen = 6.
        <ls_fieldcat>-icon      = abap_true.
        <ls_fieldcat>-hotspot   = abap_true.
        <ls_fieldcat>-edit      = abap_true.

      WHEN 'MASS05'.
        <ls_fieldcat>-outputlen = 6.
        <ls_fieldcat>-tech = abap_true.




      WHEN 'REFERENCE_DATA'.
        <ls_fieldcat>-outputlen = 30.

      WHEN 'VALUE_OLD'.
        <ls_fieldcat>-outputlen = 15.

      WHEN 'VALUE_NEW'.
        <ls_fieldcat>-outputlen = 15.

      WHEN 'CHG_IND'.
        <ls_fieldcat>-outputlen = 12.

      WHEN 'CHG_CAT_CRE_ON'.
        <ls_fieldcat>-convexit = 'TSTPS'.
        <ls_fieldcat>-outputlen = 20.

      WHEN 'CRE_BY_USERNAME'.
        <ls_fieldcat>-outputlen = 20.

      WHEN 'CHG_CAT_CHG_ON'.
        <ls_fieldcat>-convexit = 'TSTPS'.
        <ls_fieldcat>-outputlen = 20.

      WHEN 'UPD_BY_USERNAME'.
        <ls_fieldcat>-outputlen = 20.

      WHEN 'CURRENT_VER'.
        <ls_fieldcat>-outputlen = 14.

      WHEN 'FAN_ID'.
        <ls_fieldcat>-outputlen = 12.

      WHEN 'MATNR'.
        <ls_fieldcat>-outputlen = 12.

      WHEN 'RETAIL_UNIT_DESC'.
        <ls_fieldcat>-outputlen = 30.

      WHEN 'CHGID'.
        <ls_fieldcat>-tech = abap_true.


      WHEN OTHERS.
        DELETE fc_t_fieldcat[] WHERE fieldname = <ls_fieldcat>-fieldname.
    ENDCASE.








  ENDLOOP.

ENDFORM.  " BUILD_FIELDCATALOG_100
*&---------------------------------------------------------------------*
*&      Form  CREATE_HIERARCHY_100
*&---------------------------------------------------------------------*
* Build hierarchial data for tree
*----------------------------------------------------------------------*
FORM create_hierarchy_100 USING fu_t_cc_hdr      TYPE ty_t_cc_hdr
                                fu_t_ver_data    TYPE ty_t_ver_data
                                fu_t_cc_desc     TYPE ty_t_cc_desc
                                fu_t_approval    TYPE ty_t_approval
                                fu_t_appr_ch     TYPE ty_t_appr_ch
                                fu_t_rel_matrix  TYPE ty_t_rel_matrix
                                fu_t_table_mastr TYPE ty_t_table_mastr
                                fu_t_field_confg TYPE ty_t_field_confg
                                fu_r_idno        TYPE ty_r_idno
                                fu_r_chgcat      TYPE ty_r_chgcat
                                fu_r_nstat       TYPE ty_r_nstat
                                fu_r_rstat       TYPE ty_r_rstat
                                fu_o_alv_tree    TYPE REF TO cl_gui_alv_tree
                       CHANGING fc_t_reg_hdr     TYPE ty_t_reg_hdr
                                fc_t_prd_version TYPE ty_t_prd_version
                                fc_t_fieldcat    TYPE lvc_t_fcat
                                fc_t_output      TYPE ztarn_approvals_mass_tree.




  DATA: lt_cc_hdr       TYPE ty_t_cc_hdr,
        lt_cc_hdr_tmp   TYPE ztarn_cc_hdr,
        ls_cc_hdr       TYPE zarn_cc_hdr,
        lt_cc_det       TYPE ty_t_cc_det,
        ls_cc_det       TYPE zarn_cc_det,
        ls_outline_data TYPE zsarn_approvals_mass_tree,
        lt_approval     TYPE zarn_t_approval,
        ls_approval     TYPE zarn_approval,
        lt_approval_tmp TYPE zarn_t_approval,
        ls_approval_tmp TYPE zarn_approval,
        ls_appr_ch      TYPE zarn_approval_ch,
        ls_idno_appr    TYPE ty_s_idno_appr,

        lv_node_text    TYPE lvc_value,
        lv_chg_area     TYPE zarn_chg_area,
        lv_top_key      TYPE lvc_nkey,
        lv_item_key     TYPE lvc_nkey,
        lv_lines        TYPE sy-tabix,
        ls_node_layout  TYPE lvc_s_layn,
        ls_item_layout  TYPE lvc_s_layi,
        lt_item_layout  TYPE lvc_t_layi,
        lv_lock_user    TYPE sy-uname,
        lv_locked_by    TYPE zarn_locked_by,
        lv_lock_ind     TYPE zarn_lock_ind.


  FIELD-SYMBOLS: <ls_idno_appr> TYPE ty_s_idno_appr,
                 <ls_output>    TYPE zsarn_approvals_mass_tree.


* Get distinct IDNO/CHG AREA/CHG CATEGORY from approvals
  lt_approval[] = fu_t_approval[].
  SORT lt_approval[] BY idno approval_area chg_category ASCENDING updated_timestamp DESCENDING.
  DELETE ADJACENT DUPLICATES FROM lt_approval[] COMPARING idno approval_area chg_category.


*  LOOP AT lt_cc_hdr_tmp[] INTO ls_cc_hdr.

  LOOP AT lt_approval[] INTO ls_approval.


* Get Approval details of current chg category and area
    lt_approval_tmp[] = fu_t_approval[].

    DELETE lt_approval_tmp[] WHERE idno          NE ls_approval-idno.
    DELETE lt_approval_tmp[] WHERE approval_area NE ls_approval-approval_area.
    DELETE lt_approval_tmp[] WHERE chg_category  NE ls_approval-chg_category.

    IF lt_approval_tmp[] IS INITIAL.
      CONTINUE.
    ENDIF.

* get change area from approval area
    CLEAR lv_chg_area.
    IF ls_approval-approval_area = 'NAT'.
      lv_chg_area = 'N'.
    ELSEIF ls_approval-approval_area = 'REG'.
      lv_chg_area = 'R'.
    ENDIF.

    CLEAR: lt_cc_hdr_tmp[].

* Get chg_hdr details from aprovals
    CLEAR ls_cc_hdr.
    LOOP AT fu_t_cc_hdr INTO ls_cc_hdr
      WHERE idno         = ls_approval-idno
        AND chg_category = ls_approval-chg_category
        AND chg_area     = lv_chg_area.
      APPEND ls_cc_hdr TO lt_cc_hdr_tmp[].
    ENDLOOP.


* if more than one chgid exist for IDNo, then consider the recent chgid
    IF lt_cc_hdr_tmp[] IS NOT INITIAL.
      SORT lt_cc_hdr_tmp[] BY idno chg_area chg_category ASCENDING chgid DESCENDING.
      DELETE ADJACENT DUPLICATES FROM lt_cc_hdr_tmp[] COMPARING idno chg_area chg_category.
    ELSE.
      CONTINUE.
    ENDIF.

    CLEAR ls_cc_hdr.
    READ TABLE lt_cc_hdr_tmp[] INTO ls_cc_hdr INDEX 1.
    IF sy-subrc NE 0.
      CONTINUE.
    ENDIF.


    CLEAR lv_node_text.
    lv_node_text = ls_approval-idno         && '/' &&
                   lv_chg_area              && '/' &&
                   ls_approval-chg_category.


    AT NEW idno.
* Lock IDNO - same lock object as of AReNa Gui
      CLEAR: lv_lock_user, lv_locked_by, lv_lock_ind.
      PERFORM lock_idno USING ls_approval-idno
                              '2'
                              space
                     CHANGING lv_lock_user
                              lv_locked_by
                              lv_lock_ind.
    ENDAT.



    CLEAR ls_idno_appr.
    ls_idno_appr-idno         = ls_approval-idno.
    ls_idno_appr-chg_area     = lv_chg_area.
    ls_idno_appr-chg_category = ls_approval-chg_category.

* Build Header Outline Data
    CLEAR ls_outline_data.
    PERFORM build_header_outline_data USING ls_cc_hdr
                                            fu_t_ver_data[]
                                            fu_t_cc_desc[]
                                            fu_t_rel_matrix[]
                                            ls_approval
                                   CHANGING ls_outline_data
                                            fc_t_reg_hdr[]
                                            fc_t_prd_version[]
                                            fc_t_fieldcat[]
                                            ls_idno_appr.


    ls_outline_data-lock_user = lv_lock_user.
    ls_outline_data-locked_by = lv_locked_by.
    ls_outline_data-lock_ind  = lv_lock_ind.


* Maintain Approvals variables in a table
    IF <ls_idno_appr> IS ASSIGNED. UNASSIGN <ls_idno_appr>. ENDIF.
    READ TABLE gt_idno_appr[] ASSIGNING <ls_idno_appr>
    WITH TABLE KEY idno         = ls_approval-idno
                   chg_area     = lv_chg_area
                   chg_category = ls_approval-chg_category.
    IF sy-subrc = 0.
      <ls_idno_appr> = ls_idno_appr.
    ELSE.
      INSERT ls_idno_appr INTO TABLE gt_idno_appr[].
    ENDIF.


    CLEAR: ls_node_layout, lt_item_layout[].
    CASE lv_chg_area.
      WHEN 'N'.
* Create Item Layouts - National
        PERFORM create_item_layouts USING co_appr_called_from_nat
                                          'N'
                                          ls_outline_data
                                          ls_idno_appr
                                 CHANGING lt_item_layout[].

      WHEN 'R'.
* Create Item Layouts - Regional
        PERFORM create_item_layouts USING co_appr_called_from_reg
                                          'N'
                                          ls_outline_data
                                          ls_idno_appr
                                 CHANGING lt_item_layout[].

    ENDCASE.

    ls_node_layout-isfolder = abap_true.

    ls_outline_data-line_type = 'N'.

* Define one top node - Chg Cat Header
    CLEAR lv_top_key.
    CALL METHOD fu_o_alv_tree->add_node
      EXPORTING
        i_relat_node_key = ''
        i_relationship   = cl_gui_column_tree=>relat_last_child
        i_node_text      = lv_node_text
        is_outtab_line   = ls_outline_data
        is_node_layout   = ls_node_layout
        it_item_layout   = lt_item_layout[]
      IMPORTING
        e_new_node_key   = lv_top_key.

    ls_outline_data-node_key  = lv_top_key.



    CLEAR lv_lines.
    DESCRIBE TABLE fc_t_output[] LINES lv_lines.

    IF <ls_output> IS ASSIGNED. UNASSIGN <ls_output>. ENDIF.
    READ TABLE fc_t_output[] ASSIGNING <ls_output> INDEX lv_lines.
    IF <ls_output> IS ASSIGNED.
      <ls_output>-node_key = lv_top_key.
    ENDIF.








    CLEAR: lt_cc_hdr_tmp[].
* Get chg_hdr details from aprovals based on GUID
* more than one chgid can be associated with with IDNO, thus show details of all
    LOOP AT lt_approval_tmp[] INTO ls_approval_tmp.

      LOOP AT fu_t_appr_ch[] INTO ls_appr_ch
      WHERE guid = ls_approval_tmp-guid.

        CLEAR ls_cc_hdr.
        READ TABLE fu_t_cc_hdr INTO ls_cc_hdr
        WITH KEY chgid        = ls_appr_ch-chgid
                 idno         = ls_approval_tmp-idno
                 chg_category = ls_approval_tmp-chg_category
                 chg_area     = lv_chg_area.
        IF sy-subrc = 0.
          APPEND ls_cc_hdr TO lt_cc_hdr_tmp[].
        ENDIF.
      ENDLOOP.  " LOOP AT fu_t_appr_ch[] INTO ls_appr_ch
    ENDLOOP.  " LOOP AT lt_approval_tmp[] INTO ls_approval_tmp

    " ZARN_CC_DET was not retreived in mo_data-get_data because new_value and old_value
    " retreival was taking too much of memory and programn was giving short dump
    " as "Internal table ran out of space, can't add more rows"
    " thus retreiving separately per header
* Read Change Category Details
    CLEAR: lt_cc_det[].

    IF lt_cc_hdr_tmp[] IS NOT INITIAL.
      SELECT chgid idno chg_category chg_table chg_field reference_data
             chg_area chg_ind value_new value_old
        FROM zarn_cc_det
        INTO CORRESPONDING FIELDS OF TABLE lt_cc_det[]
        FOR ALL ENTRIES IN lt_cc_hdr_tmp[]
        WHERE chgid        EQ lt_cc_hdr_tmp-chgid
          AND idno         EQ lt_cc_hdr_tmp-idno
          AND chg_category EQ lt_cc_hdr_tmp-chg_category
          AND chg_area     EQ lt_cc_hdr_tmp-chg_area.
    ENDIF.


    LOOP AT lt_cc_det[] INTO ls_cc_det.

      CLEAR lv_node_text.
      lv_node_text = ls_cc_det-idno         && '/' &&
                     ls_cc_det-chg_area     && '/' &&
                     ls_cc_det-chg_category && '/' &&
                     ls_cc_det-chg_table    && '/' &&
                     ls_cc_det-chg_field.





* Build Detail Outline Data
      CLEAR ls_outline_data.
      PERFORM build_detail_outline_data USING ls_cc_det
                                              lt_cc_hdr_tmp[]
                                              fu_t_ver_data[]
                                              fu_t_table_mastr[]
                                              fu_t_field_confg[]
                                              fu_t_approval[]
                                              fu_t_appr_ch[]
                                              ls_approval
                                     CHANGING ls_outline_data
                                              fc_t_reg_hdr[]
                                              fc_t_prd_version[].


      ls_outline_data-node_key  = lv_top_key.
      ls_outline_data-line_type = 'I'.

      CLEAR: ls_node_layout, lt_item_layout[].
      CASE lv_chg_area.
        WHEN 'N'.
* Create Item Layouts - National
          PERFORM create_item_layouts USING co_appr_called_from_nat
                                            'I'
                                            ls_outline_data
                                            ls_idno_appr
                                   CHANGING lt_item_layout[].

        WHEN 'R'.
* Create Item Layouts - Regional
          PERFORM create_item_layouts USING co_appr_called_from_reg
                                            'I'
                                            ls_outline_data
                                            ls_idno_appr
                                   CHANGING lt_item_layout[].

      ENDCASE.


      CLEAR lv_item_key.
* Define one top node - Chg Cat Header
      CALL METHOD fu_o_alv_tree->add_node
        EXPORTING
          i_relat_node_key = lv_top_key
          i_relationship   = cl_gui_column_tree=>relat_last_child
          i_node_text      = lv_node_text
          is_outtab_line   = ls_outline_data
          is_node_layout   = ls_node_layout
          it_item_layout   = lt_item_layout[]
        IMPORTING
          e_new_node_key   = lv_item_key.

      CLEAR lv_lines.
      DESCRIBE TABLE fc_t_output[] LINES lv_lines.

      IF <ls_output> IS ASSIGNED. UNASSIGN <ls_output>. ENDIF.
      READ TABLE fc_t_output[] ASSIGNING <ls_output> INDEX lv_lines.
      IF <ls_output> IS ASSIGNED.
        <ls_output>-item_key = lv_item_key.
      ENDIF.

    ENDLOOP.  " LOOP AT lt_cc_det[] INTO ls_cc_det
  ENDLOOP.  " LOOP AT lt_approval[] INTO ls_approval

ENDFORM.  " CREATE_HIERARCHY_100
*&---------------------------------------------------------------------*
*&      Form  BUILD_HEADER_OUTLINE_DATA
*&---------------------------------------------------------------------*
* Build Header Outline Data
*----------------------------------------------------------------------*
FORM build_header_outline_data USING fu_s_cc_hdr       TYPE zarn_cc_hdr
                                     fu_t_ver_data     TYPE ty_t_ver_data
                                     fu_t_cc_desc      TYPE ty_t_cc_desc
                                     fu_t_rel_matrix   TYPE ty_t_rel_matrix
                                     fu_s_approval     TYPE zarn_approval
                            CHANGING fc_s_outline_data TYPE zsarn_approvals_mass_tree
                                     fc_t_reg_hdr      TYPE ty_t_reg_hdr
                                     fc_t_prd_version  TYPE ty_t_prd_version
                                     fc_t_fieldcat     TYPE lvc_t_fcat
                                     fc_s_idno_appr    TYPE ty_s_idno_appr.


  DATA: ls_reg_hdr      TYPE zarn_reg_hdr,
        ls_ver_data     TYPE ty_s_ver_data,
        ls_cc_desc      TYPE zarn_cc_desc,
        lv_user	        TYPE sy-uname,
        ls_user_address TYPE addr3_val,
        ls_approval     TYPE zarn_approval.

  CLEAR: fc_s_outline_data.
  CLEAR: ls_reg_hdr, ls_ver_data, ls_cc_desc.

  TRY.
      ls_reg_hdr = fc_t_reg_hdr[ idno = fu_s_cc_hdr-idno ].
    CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
  ENDTRY.

  TRY.
      ls_ver_data = fu_t_ver_data[ idno = fu_s_cc_hdr-idno ].
    CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
  ENDTRY.

  TRY.
      ls_cc_desc = fu_t_cc_desc[ chg_category = fu_s_cc_hdr-chg_category ].
    CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
  ENDTRY.


  fc_s_outline_data-idno              = fu_s_cc_hdr-idno.
  fc_s_outline_data-chg_area          = fu_s_cc_hdr-chg_area.
  fc_s_outline_data-chg_category      = fu_s_cc_hdr-chg_category.
  fc_s_outline_data-current_ver       = ls_ver_data-current_ver.
  fc_s_outline_data-fan_id            = ls_ver_data-fan_id.
  fc_s_outline_data-matnr             = ls_reg_hdr-matnr.
  fc_s_outline_data-chgid             = fu_s_cc_hdr-chgid.
  fc_s_outline_data-retail_unit_desc  = ls_reg_hdr-retail_unit_desc.

  fc_s_outline_data-description = ls_cc_desc-chg_cat_desc.
*  fc_s_outline_data-add_details       =

  IF fu_s_cc_hdr-chg_area = 'R'.
    fc_s_outline_data-status = ls_reg_hdr-status.
  ELSEIF fu_s_cc_hdr-chg_area = 'N'.
    fc_s_outline_data-status = ls_ver_data-version_status.
  ENDIF.


  fc_s_outline_data-chg_cat_cre_on    = fu_s_cc_hdr-chg_cat_cre_on.


  CLEAR: lv_user, ls_user_address.
  lv_user   = fu_s_cc_hdr-chg_cat_cre_by.

  CALL FUNCTION 'SUSR_USER_ADDRESS_READ'
    EXPORTING
      user_name              = lv_user
    IMPORTING
      user_address           = ls_user_address
    EXCEPTIONS
      user_address_not_found = 1
      OTHERS                 = 2.
  IF sy-subrc IS INITIAL.
    fc_s_outline_data-cre_by_username = ls_user_address-name_text.
  ELSE.
    fc_s_outline_data-cre_by_username   = fu_s_cc_hdr-chg_cat_cre_by.
  ENDIF.




  fc_s_outline_data-chg_cat_chg_on = fu_s_approval-updated_timestamp.

  CLEAR: lv_user, ls_user_address.
  lv_user   = fu_s_approval-updated_by.

  CALL FUNCTION 'SUSR_USER_ADDRESS_READ'
    EXPORTING
      user_name              = lv_user
    IMPORTING
      user_address           = ls_user_address
    EXCEPTIONS
      user_address_not_found = 1
      OTHERS                 = 2.
  IF sy-subrc IS INITIAL.
    fc_s_outline_data-upd_by_username = ls_user_address-name_text.
  ELSE.
    fc_s_outline_data-upd_by_username   = fu_s_approval-updated_by.
  ENDIF.


  CASE fu_s_cc_hdr-chg_area.

    WHEN 'N'.

* Set Mass Relevancy
      PERFORM set_mass_relevancy USING co_appr_called_from_nat
                                       ls_ver_data
                                       fu_t_rel_matrix
                              CHANGING fc_s_outline_data.

* Build Approvals Grid data - National
      PERFORM approvals_build_grid2 USING co_appr_called_from_nat
                                          fu_s_cc_hdr
                                          ls_reg_hdr
                                          ls_ver_data
                                 CHANGING fc_t_reg_hdr[]
                                          fc_t_prd_version[]
                                          fc_t_fieldcat[]
                                          fc_s_outline_data
                                          fc_s_idno_appr.

    WHEN 'R'.

* Set Mass Relevancy
      PERFORM set_mass_relevancy USING co_appr_called_from_reg
                                       ls_ver_data
                                       fu_t_rel_matrix
                              CHANGING fc_s_outline_data.

* Build Approvals Grid data - Regional
      PERFORM approvals_build_grid2 USING co_appr_called_from_reg
                                          fu_s_cc_hdr
                                          ls_reg_hdr
                                          ls_ver_data
                                 CHANGING fc_t_reg_hdr[]
                                          fc_t_prd_version[]
                                          fc_t_fieldcat[]
                                          fc_s_outline_data
                                          fc_s_idno_appr.

  ENDCASE.

ENDFORM.  " BUILD_HEADER_OUTLINE_DATA
*&---------------------------------------------------------------------*
*&      Form  BUILD_DETAIL_OUTLINE_DATA
*&---------------------------------------------------------------------*
* Build Detail Outline Data
*----------------------------------------------------------------------*
FORM build_detail_outline_data USING fu_s_cc_det       TYPE zarn_cc_det
                                     fu_t_cc_hdr       TYPE ztarn_cc_hdr
                                     fu_t_ver_data     TYPE ty_t_ver_data
                                     fu_t_table_mastr  TYPE ty_t_table_mastr
                                     fu_t_field_confg  TYPE ty_t_field_confg
                                     fu_t_approval     TYPE ty_t_approval
                                     fu_t_appr_ch      TYPE ty_t_appr_ch
                                     fu_s_approval     TYPE zarn_approval
                            CHANGING fc_s_outline_data TYPE zsarn_approvals_mass_tree
                                     fc_t_reg_hdr      TYPE ty_t_reg_hdr
                                     fc_t_prd_version  TYPE ty_t_prd_version.

  DATA: ls_reg_hdr      TYPE zarn_reg_hdr,
        ls_ver_data     TYPE ty_s_ver_data,
        ls_cc_hdr       TYPE zarn_cc_hdr,
        ls_appr_ch      TYPE zarn_approval_ch,
        ls_approval     TYPE zarn_approval,
        ls_table_mastr  TYPE zarn_table_mastr,
        ls_field_confg  TYPE zarn_field_confg,

        lv_user	        TYPE sy-uname,
        ls_user_address TYPE addr3_val,
        lt_dfies        TYPE STANDARD TABLE OF dfies,
        ls_dfies        TYPE dfies.


  CLEAR: fc_s_outline_data.
  CLEAR: ls_reg_hdr, ls_ver_data.

  TRY.
      ls_reg_hdr = fc_t_reg_hdr[ idno = fu_s_cc_det-idno ].
    CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
  ENDTRY.

  TRY.
      ls_ver_data = fu_t_ver_data[ idno = fu_s_cc_det-idno ].
    CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
  ENDTRY.



  CLEAR: ls_cc_hdr, ls_appr_ch, ls_approval.
  TRY.
      ls_cc_hdr = fu_t_cc_hdr[ chgid        = fu_s_cc_det-chgid
                               idno         = fu_s_cc_det-idno
                               chg_area     = fu_s_cc_det-chg_area
                               chg_category = fu_s_cc_det-chg_category
                             ].

*      ls_appr_ch = fu_t_appr_ch[ chgid = fu_s_cc_det-chgid
*                                 idno  = fu_s_cc_det-idno
*                               ].


*      ls_approval = fu_t_approval[ guid = ls_appr_ch-guid ].


      ls_approval = fu_s_approval.

    CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
  ENDTRY.



  fc_s_outline_data-idno              = fu_s_cc_det-idno.
  fc_s_outline_data-chg_area          = fu_s_cc_det-chg_area.
  fc_s_outline_data-chg_category      = fu_s_cc_det-chg_category.
  fc_s_outline_data-chg_table         = fu_s_cc_det-chg_table.
  fc_s_outline_data-chg_field         = fu_s_cc_det-chg_field.
  fc_s_outline_data-current_ver       = ls_ver_data-current_ver.
  fc_s_outline_data-fan_id            = ls_ver_data-fan_id.
  fc_s_outline_data-matnr             = ls_reg_hdr-matnr.
  fc_s_outline_data-chgid             = fu_s_cc_det-chgid.
  fc_s_outline_data-retail_unit_desc  = ls_reg_hdr-retail_unit_desc.



*  fc_s_outline_data-add_details       =
  fc_s_outline_data-reference_data    = fu_s_cc_det-reference_data.
  fc_s_outline_data-value_old         = fu_s_cc_det-value_old.
  fc_s_outline_data-value_new         = fu_s_cc_det-value_new.
  fc_s_outline_data-chg_ind           = fu_s_cc_det-chg_ind.



  IF fc_s_outline_data-chg_table IS NOT INITIAL AND
     fc_s_outline_data-chg_field IS NOT INITIAL.
* Get field Description
    REFRESH lt_dfies[].
    CALL FUNCTION 'DDIF_FIELDINFO_GET'
      EXPORTING
        tabname        = fc_s_outline_data-chg_table
        fieldname      = fc_s_outline_data-chg_field
        langu          = sy-langu
      TABLES
        dfies_tab      = lt_dfies[]
      EXCEPTIONS
        not_found      = 1
        internal_error = 2
        OTHERS         = 3.
    IF sy-subrc = 0.
      CLEAR ls_dfies.
      READ TABLE lt_dfies[] INTO ls_dfies INDEX 1.
      IF sy-subrc = 0.
        fc_s_outline_data-description = ls_dfies-fieldtext.
      ENDIF.
    ENDIF.
  ENDIF.

  IF fc_s_outline_data-chg_field = 'KEY'.
    fc_s_outline_data-description = 'Key'.
  ENDIF.




  fc_s_outline_data-chg_cat_cre_on    = ls_cc_hdr-chg_cat_cre_on.


  CLEAR: lv_user, ls_user_address.
  lv_user   = ls_cc_hdr-chg_cat_cre_by.

  CALL FUNCTION 'SUSR_USER_ADDRESS_READ'
    EXPORTING
      user_name              = lv_user
    IMPORTING
      user_address           = ls_user_address
    EXCEPTIONS
      user_address_not_found = 1
      OTHERS                 = 2.
  IF sy-subrc IS INITIAL.
    fc_s_outline_data-cre_by_username = ls_user_address-name_text.
  ELSE.
    fc_s_outline_data-cre_by_username   = ls_cc_hdr-chg_cat_cre_by.
  ENDIF.




  fc_s_outline_data-chg_cat_chg_on = ls_approval-updated_timestamp.

  CLEAR: lv_user, ls_user_address.
  lv_user   = ls_approval-updated_by.

  CALL FUNCTION 'SUSR_USER_ADDRESS_READ'
    EXPORTING
      user_name              = lv_user
    IMPORTING
      user_address           = ls_user_address
    EXCEPTIONS
      user_address_not_found = 1
      OTHERS                 = 2.
  IF sy-subrc IS INITIAL.
    fc_s_outline_data-upd_by_username = ls_user_address-name_text.
  ELSE.
    fc_s_outline_data-upd_by_username   = ls_approval-updated_by.
  ENDIF.





  CLEAR: ls_field_confg, ls_table_mastr.
  TRY.
      ls_field_confg = fu_t_field_confg[ tabname = fu_s_cc_det-chg_table
                                         fldname = fu_s_cc_det-chg_field
                                       ].

      ls_table_mastr = fu_t_table_mastr[ arena_table = fu_s_cc_det-chg_table ].

    CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
  ENDTRY.

* Build Additional Detail
  PERFORM build_additional_detail USING ls_field_confg
                                        ls_table_mastr
                                        ls_ver_data
                                        fu_s_cc_det
                               CHANGING fc_s_outline_data-add_details.




ENDFORM.  " BUILD_DETAIL_OUTLINE_DATA
*&---------------------------------------------------------------------*
*&      Form  BUILD_ADDITIONAL_DETAIL
*&---------------------------------------------------------------------*
* Build Additional Detail
*----------------------------------------------------------------------*
FORM build_additional_detail USING fu_s_field_confg TYPE zarn_field_confg
                                   fu_s_table_mastr TYPE zarn_table_mastr
                                   fu_s_ver_data    TYPE ty_s_ver_data
                                   fu_s_cc_det      TYPE zarn_cc_det
                          CHANGING fc_s_add_details TYPE zarn_add_details.

  DATA: ls_prod_data TYPE zsarn_prod_data,
        ls_reg_data  TYPE zsarn_reg_data,

        lt_dfies     TYPE dfies_tab,
        ls_dfies     TYPE dfies,

        lt_line      TYPE STANDARD TABLE OF char100,
        ls_line      TYPE char100,

        lv_where     TYPE string,
        lv_detail    TYPE string,
        lv_tabname   TYPE string,
        lv_fldname   TYPE string,
        lv_tabix     TYPE sy-tabix.



  FIELD-SYMBOLS: <lt_table> TYPE table,
                 <ls_wa>    TYPE any,
                 <lv_field> TYPE any.


  CLEAR fc_s_add_details.


* Check if Addnl table and fields are filled,
* table name is valid
* Addnl table name must be same as CC_DET table name as CC_DET ref_data has key for itself only
* thus it is difficult to get addnl details from another table
  IF fu_s_field_confg-addtnl_det_fld IS INITIAL OR
     fu_s_table_mastr                IS INITIAL.
    RETURN.
  ENDIF.



* Get field Description
  REFRESH lt_dfies[].
  CALL FUNCTION 'DDIF_FIELDINFO_GET'
    EXPORTING
      tabname        = fu_s_cc_det-chg_table
      langu          = sy-langu
    TABLES
      dfies_tab      = lt_dfies[]
    EXCEPTIONS
      not_found      = 1
      internal_error = 2
      OTHERS         = 3.

  IF line_exists( lt_dfies[ fieldname = 'IDNO' ] ).                           "9000007115
    DATA(lv_idno_found) = abap_true.                                          "9000007115
  ENDIF.                                                                      "9000007115
* Keep only keys
  DELETE lt_dfies[] WHERE keyflag EQ abap_false.
* Ignore mandt, idno and version
  DELETE lt_dfies[] WHERE fieldname EQ 'MANDT'
                       OR fieldname EQ 'IDNO'
                       OR fieldname EQ 'VERSION'.


* Build dynamic where clause for remaining keys getting values to be checked from the ref_data
  CLEAR: lt_line[], lv_where.
  IF fu_s_cc_det-reference_data IS NOT INITIAL.
    SPLIT fu_s_cc_det-reference_data AT '/'
           INTO TABLE lt_line[].

    DELETE lt_line[] WHERE table_line EQ '/'.

    IF lt_line[] IS NOT INITIAL.

      LOOP AT lt_dfies[] INTO ls_dfies.
        lv_tabix = sy-tabix.

        CLEAR ls_line.
        READ TABLE lt_line[] INTO ls_line INDEX lv_tabix.
        IF sy-subrc = 0.
          IF lv_tabix EQ 1 AND lv_idno_found EQ abap_false.                            "9000007115
            lv_where = |{ ls_dfies-fieldname } = '{ ls_line }'|.                       "9000007115
          ELSE.                                                                        "9000007115
            lv_where = |{ lv_where } AND { ls_dfies-fieldname } = '{ ls_line }'|.
          ENDIF.                                                                       "9000007115
        ENDIF.  " READ TABLE lt_line[] INTO ls_line INDEX lv_tabix.
      ENDLOOP.  " LOOP AT lt_dfies[] INTO ls_dfies

    ENDIF.
  ENDIF.





  CASE fu_s_table_mastr-arena_table_type.

    WHEN 'N'.
      CLEAR: ls_prod_data.
      TRY.
          ls_prod_data = gt_prod_data[ idno    = fu_s_ver_data-idno
                                       version = fu_s_ver_data-current_ver
                                     ].
        CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
      ENDTRY.

* Read data from Addnl Detail Table
      CLEAR: lv_tabname, lv_fldname.
      lv_tabname = |LS_PROD_DATA-{ fu_s_cc_det-chg_table }|.
      ASSIGN (lv_tabname) TO <lt_table>.

* Add IDNO and VERSION to where clause
      IF lv_idno_found EQ abap_true.                                                  "9000007115
        IF lv_where IS INITIAL.
          lv_where = |IDNO = '{ fu_s_ver_data-idno }' AND VERSION = '{ fu_s_ver_data-current_ver }'|.
        ELSE.
          lv_where = |IDNO = '{ fu_s_ver_data-idno }' AND VERSION = '{ fu_s_ver_data-current_ver }' { lv_where }|.
        ENDIF.
      ENDIF.                                                                          "9000007115

    WHEN 'R'.
      CLEAR: ls_reg_data.
      TRY.
          ls_reg_data = gt_reg_data[ idno    = fu_s_ver_data-idno ].
        CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
      ENDTRY.

* Read data from Addnl Detail Table
      CLEAR: lv_tabname, lv_fldname.
      lv_tabname = |LS_REG_DATA-{ fu_s_cc_det-chg_table }|.
      ASSIGN (lv_tabname) TO <lt_table>.

* Add IDNO to where clause
      IF lv_idno_found EQ abap_true.                                                   "9000007115
        IF lv_where IS INITIAL.
          lv_where = |IDNO = '{ fu_s_ver_data-idno }'|.
        ELSE.
          lv_where = |IDNO = '{ fu_s_ver_data-idno }' { lv_where }|.
        ENDIF.                                                                         "9000007115
      ENDIF.
  ENDCASE.


  CLEAR: lt_line[], lv_detail.

  IF <lt_table> IS ASSIGNED.
* Read Addnl Detail table based on where clause (with complete keys)
    LOOP AT <lt_table> ASSIGNING <ls_wa> WHERE (lv_where).
      EXIT.
    ENDLOOP.

    IF <ls_wa> IS ASSIGNED.



      SPLIT fu_s_field_confg-addtnl_det_fld AT '/' INTO TABLE lt_line[].

      LOOP AT lt_line[] INTO ls_line.
        lv_tabix = sy-tabix.

        IF ls_line IS NOT INITIAL.
* Get value of Addnl Detail Field from the record read
          ASSIGN COMPONENT ls_line OF STRUCTURE <ls_wa> TO <lv_field>.

          IF <lv_field> IS ASSIGNED.

            IF lv_tabix = 1.
              lv_detail = <lv_field>.
            ELSE.
              lv_detail = |{ lv_detail }/{ <lv_field> }|.
            ENDIF.

          ENDIF.  "  IF <lv_field> IS ASSIGNED.
        ENDIF.   " IF ls_line IS NOT INITIAL.
      ENDLOOP.  " LOOP AT lt_line[] INTO ls_line.


      IF lv_detail IS NOT INITIAL.
        fc_s_add_details = lv_detail.
      ENDIF.


    ENDIF.  " IF <ls_wa> IS ASSIGNED.
  ENDIF.  "IF <lt_table> IS ASSIGNED.


ENDFORM.  " BUILD_ADDITIONAL_DETAIL
*&---------------------------------------------------------------------*
*&      Form  REGISTER_EVENTS_100
*&---------------------------------------------------------------------*
* Register Tree Events
*----------------------------------------------------------------------*
FORM register_events_100 USING fu_o_alv_tree     TYPE REF TO cl_gui_alv_tree
                               fc_o_toolbar_tree TYPE REF TO cl_gui_toolbar
                               fc_t_output       TYPE ztarn_approvals_mass_tree.

  DATA: lt_events TYPE cntl_simple_events,
        l_event   TYPE cntl_simple_event.


* Nevertheless you have to provide their IDs again if you register
* additional events with SET_REGISTERED_EVENTS
* To do so, call first method  GET_REGISTERED_EVENTS (this way,
* all already registered events remain registered, even your own):
  CALL METHOD fu_o_alv_tree->get_registered_events
    IMPORTING
      events = lt_events[].
* (If you do not these events will be deregistered!!!).
* You do not have to register events of the toolbar again.

* Frontend registration: add additional event ids
  l_event-eventid = cl_gui_column_tree=>eventid_link_click.
  APPEND l_event TO lt_events[].

*Frontend registration: provide new event table to alv tree
  CALL METHOD fu_o_alv_tree->set_registered_events
    EXPORTING
      events                    = lt_events[]
    EXCEPTIONS
      cntl_error                = 1
      cntl_system_error         = 2
      illegal_event_combination = 3.
  IF sy-subrc <> 0.
    MESSAGE x208(00) WITH 'ERROR'.                          "#EC NOTEXT
  ENDIF.


* Register events on backend (ABAP Objects event handling)
  CREATE OBJECT go_appr_mass_event_handler.

  " Hotspots
  SET HANDLER go_appr_mass_event_handler->appr_mass_handle_link_click FOR fu_o_alv_tree.

*  " Toolbar buttons
  SET HANDLER go_appr_mass_event_handler->on_function_selected FOR fc_o_toolbar_tree.

*  gt_output[] = fc_t_output[].

ENDFORM.  " REGISTER_EVENTS_100
*&---------------------------------------------------------------------*
*&      Form  CREATE_ITEM_LAYOUTS
*&---------------------------------------------------------------------*
* Create Item Layouts
*----------------------------------------------------------------------*
FORM create_item_layouts USING fu_v_called_from  TYPE char3
                               fu_v_line_type    TYPE char1
                               fu_s_outline_data TYPE zsarn_approvals_mass_tree
                               fu_s_idno_appr    TYPE ty_s_idno_appr
                      CHANGING fc_t_item_layout  TYPE lvc_t_layi.

  DATA: ls_item_layout TYPE lvc_s_layi,
        lt_item_layout TYPE lvc_t_layi,

        lv_hotspot     TYPE i.


  CLEAR: fc_t_item_layout[].


  CASE fu_v_line_type.
    WHEN 'N'.

*        IF piv_input_disabled = abap_true.
*          <ls_fieldcat>-edit    = abap_false.
*          IF <ls_fieldcat>-fieldname(4) NE co_appr_column_text.
*            "enable to display text even in Display mode
*            <ls_fieldcat>-hotspot = abap_false.
*          ENDIF.
*        ENDIF.

      CLEAR lv_hotspot.
      lv_hotspot = cl_gui_column_tree=>item_class_link.

* If National is not approved, then no horspot for regional approvals
      IF fu_v_called_from EQ co_appr_called_from_reg.
        IF fu_s_idno_appr-gv_appr_nat_is_approved EQ abap_false.
          CLEAR lv_hotspot.
        ENDIF.
      ENDIF.

* If IDNO is locked, then no hotspot
      IF fu_s_outline_data-lock_ind EQ icon_locked.
        CLEAR lv_hotspot.
      ENDIF.


      CLEAR: ls_item_layout.
      ls_item_layout-fieldname = 'APPR01'.
      IF fu_s_outline_data-appr01 = icon_okay OR fu_s_outline_data-appr01 = icon_led_green OR fu_s_outline_data-appr01 = icon_led_red.
        ls_item_layout-class = lv_hotspot.
      ENDIF.

      IF fu_s_outline_data-mass01 IS NOT INITIAL.
        ls_item_layout-style = 5.
      ENDIF.
      APPEND ls_item_layout TO lt_item_layout[].


      CLEAR: ls_item_layout.
      ls_item_layout-fieldname = 'APPR02'.
      IF fu_s_outline_data-appr02 = icon_okay OR fu_s_outline_data-appr02 = icon_led_green OR fu_s_outline_data-appr02 = icon_led_red.
        ls_item_layout-class = lv_hotspot.
      ENDIF.


      IF fu_s_outline_data-mass02 IS NOT INITIAL.
        ls_item_layout-style = 6.
      ENDIF.
      APPEND ls_item_layout TO lt_item_layout[].


      CLEAR: ls_item_layout.
      ls_item_layout-fieldname = 'APPR03'.
      IF fu_s_outline_data-appr03 = icon_okay OR fu_s_outline_data-appr03 = icon_led_green OR fu_s_outline_data-appr03 = icon_led_red.
        ls_item_layout-class = lv_hotspot.
      ENDIF.

      IF fu_s_outline_data-mass03 IS NOT INITIAL.
        ls_item_layout-style = 7.
      ENDIF.
      APPEND ls_item_layout TO lt_item_layout[].


      CLEAR: ls_item_layout.
      ls_item_layout-fieldname = 'APPR04'.
      IF fu_s_outline_data-appr04 = icon_okay OR fu_s_outline_data-appr04 = icon_led_green OR fu_s_outline_data-appr04 = icon_led_red.
        ls_item_layout-class = lv_hotspot.
      ENDIF.

      IF fu_s_outline_data-mass04 IS NOT INITIAL.
        ls_item_layout-style = 6.
      ENDIF.
      APPEND ls_item_layout TO lt_item_layout[].


      CLEAR: ls_item_layout.
      ls_item_layout-fieldname = 'APPR05'.
      IF fu_s_outline_data-appr05 = icon_okay OR fu_s_outline_data-appr05 = icon_led_green OR fu_s_outline_data-appr05 = icon_led_red.
        ls_item_layout-class = lv_hotspot.
      ENDIF.

      IF fu_s_outline_data-mass05 IS NOT INITIAL.
        ls_item_layout-style = 7.
      ENDIF.
      APPEND ls_item_layout TO lt_item_layout[].


      CLEAR: ls_item_layout.
      ls_item_layout-fieldname = 'REJE01'.
      IF fu_s_outline_data-reje01 = icon_cancel.
        ls_item_layout-class = lv_hotspot.
      ENDIF.

      IF fu_s_outline_data-mass01 IS NOT INITIAL.
        ls_item_layout-style = 5.
      ENDIF.
      APPEND ls_item_layout TO lt_item_layout[].


      CLEAR: ls_item_layout.
      ls_item_layout-fieldname = 'REJE02'.
      IF fu_s_outline_data-reje02 = icon_cancel.
        ls_item_layout-class = lv_hotspot.
      ENDIF.

      IF fu_s_outline_data-mass02 IS NOT INITIAL.
        ls_item_layout-style = 6.
      ENDIF.
      APPEND ls_item_layout TO lt_item_layout[].


      CLEAR: ls_item_layout.
      ls_item_layout-fieldname = 'REJE03'.
      IF fu_s_outline_data-reje03 = icon_cancel.
        ls_item_layout-class = lv_hotspot.
      ENDIF.

      IF fu_s_outline_data-mass03 IS NOT INITIAL.
        ls_item_layout-style = 7.
      ENDIF.
      APPEND ls_item_layout TO lt_item_layout[].


      CLEAR: ls_item_layout.
      ls_item_layout-fieldname = 'REJE04'.
      IF fu_s_outline_data-reje04 = icon_cancel.
        ls_item_layout-class = lv_hotspot.
      ENDIF.

      IF fu_s_outline_data-mass04 IS NOT INITIAL.
        ls_item_layout-style = 6.
      ENDIF.
      APPEND ls_item_layout TO lt_item_layout[].


      CLEAR: ls_item_layout.
      ls_item_layout-fieldname = 'REJE05'.
      IF fu_s_outline_data-reje05 = icon_cancel.
        ls_item_layout-class = lv_hotspot.
      ENDIF.

      IF fu_s_outline_data-mass05 IS NOT INITIAL.
        ls_item_layout-style = 7.
      ENDIF.
      APPEND ls_item_layout TO lt_item_layout[].





      CLEAR lv_hotspot.
      lv_hotspot = cl_gui_column_tree=>item_class_link.




      CLEAR: ls_item_layout.
      ls_item_layout-fieldname = 'TEXT01'.
      IF fu_s_outline_data-text01 IS NOT INITIAL AND
         fu_s_outline_data-text01 = icon_display_text OR
         ( ( NOT fu_s_outline_data-lock_ind EQ icon_locked ) AND ( fu_s_outline_data-text01 = icon_create_text OR
                                                                   fu_s_outline_data-text01 = icon_change_text ) ).
        ls_item_layout-class = lv_hotspot.
      ENDIF.

      IF fu_s_outline_data-mass01 IS NOT INITIAL.
        ls_item_layout-style = 5.
      ENDIF.
      APPEND ls_item_layout TO lt_item_layout[].


      CLEAR: ls_item_layout.
      ls_item_layout-fieldname = 'TEXT02'.
      IF fu_s_outline_data-text02 IS NOT INITIAL AND
         fu_s_outline_data-text02 = icon_display_text OR
         ( ( NOT fu_s_outline_data-lock_ind EQ icon_locked ) AND ( fu_s_outline_data-text02 = icon_create_text OR
                                                                   fu_s_outline_data-text02 = icon_change_text ) ).
        ls_item_layout-class = lv_hotspot.
      ENDIF.

      IF fu_s_outline_data-mass02 IS NOT INITIAL.
        ls_item_layout-style = 6.
      ENDIF.
      APPEND ls_item_layout TO lt_item_layout[].


      CLEAR: ls_item_layout.
      ls_item_layout-fieldname = 'TEXT03'.
      IF fu_s_outline_data-text03 IS NOT INITIAL AND
         fu_s_outline_data-text03 = icon_display_text OR
         ( ( NOT fu_s_outline_data-lock_ind EQ icon_locked ) AND ( fu_s_outline_data-text03 = icon_create_text OR
                                                                   fu_s_outline_data-text03 = icon_change_text ) ).
        ls_item_layout-class = lv_hotspot.
      ENDIF.

      IF fu_s_outline_data-mass03 IS NOT INITIAL.
        ls_item_layout-style = 7.
      ENDIF.
      APPEND ls_item_layout TO lt_item_layout[].


      CLEAR: ls_item_layout.
      ls_item_layout-fieldname = 'TEXT04'.
      IF fu_s_outline_data-text04 IS NOT INITIAL AND
         fu_s_outline_data-text04 = icon_display_text OR
         ( ( NOT fu_s_outline_data-lock_ind EQ icon_locked ) AND ( fu_s_outline_data-text04 = icon_create_text OR
                                                                   fu_s_outline_data-text04 = icon_change_text ) ).
        ls_item_layout-class = lv_hotspot.
      ENDIF.

      IF fu_s_outline_data-mass04 IS NOT INITIAL.
        ls_item_layout-style = 6.
      ENDIF.
      APPEND ls_item_layout TO lt_item_layout[].


      CLEAR: ls_item_layout.
      ls_item_layout-fieldname = 'TEXT05'.
      IF fu_s_outline_data-text05 IS NOT INITIAL AND
         fu_s_outline_data-text05 = icon_display_text OR
         ( ( NOT fu_s_outline_data-lock_ind EQ icon_locked ) AND ( fu_s_outline_data-text05 = icon_create_text OR
                                                                   fu_s_outline_data-text05 = icon_change_text ) ).
        ls_item_layout-class = lv_hotspot.
      ENDIF.

      IF fu_s_outline_data-mass05 IS NOT INITIAL.
        ls_item_layout-style = 7.
      ENDIF.
      APPEND ls_item_layout TO lt_item_layout[].


    WHEN 'I'.

      CLEAR: ls_item_layout.
      ls_item_layout-fieldname = 'APPR01'.
      ls_item_layout-style = 5.
      APPEND ls_item_layout TO lt_item_layout[].


      CLEAR: ls_item_layout.
      ls_item_layout-fieldname = 'APPR02'.
      ls_item_layout-style = 6.
      APPEND ls_item_layout TO lt_item_layout[].


      CLEAR: ls_item_layout.
      ls_item_layout-fieldname = 'APPR03'.
      ls_item_layout-style = 7.
      APPEND ls_item_layout TO lt_item_layout[].


      CLEAR: ls_item_layout.
      ls_item_layout-fieldname = 'APPR04'.
      ls_item_layout-style = 6.
      APPEND ls_item_layout TO lt_item_layout[].


      CLEAR: ls_item_layout.
      ls_item_layout-fieldname = 'APPR05'.
      ls_item_layout-style = 7.
      APPEND ls_item_layout TO lt_item_layout[].


      CLEAR: ls_item_layout.
      ls_item_layout-fieldname = 'REJE01'.
      ls_item_layout-style = 5.
      APPEND ls_item_layout TO lt_item_layout[].


      CLEAR: ls_item_layout.
      ls_item_layout-fieldname = 'REJE02'.
      ls_item_layout-style = 6.
      APPEND ls_item_layout TO lt_item_layout[].


      CLEAR: ls_item_layout.
      ls_item_layout-fieldname = 'REJE03'.
      ls_item_layout-style = 7.
      APPEND ls_item_layout TO lt_item_layout[].


      CLEAR: ls_item_layout.
      ls_item_layout-fieldname = 'REJE04'.
      ls_item_layout-style = 6.
      APPEND ls_item_layout TO lt_item_layout[].


      CLEAR: ls_item_layout.
      ls_item_layout-fieldname = 'REJE05'.
      ls_item_layout-style = 7.
      APPEND ls_item_layout TO lt_item_layout[].


      CLEAR: ls_item_layout.
      ls_item_layout-fieldname = 'TEXT01'.
      ls_item_layout-style = 5.
      APPEND ls_item_layout TO lt_item_layout[].


      CLEAR: ls_item_layout.
      ls_item_layout-fieldname = 'TEXT02'.
      ls_item_layout-style = 6.
      APPEND ls_item_layout TO lt_item_layout[].


      CLEAR: ls_item_layout.
      ls_item_layout-fieldname = 'TEXT03'.
      ls_item_layout-style = 7.
      APPEND ls_item_layout TO lt_item_layout[].


      CLEAR: ls_item_layout.
      ls_item_layout-fieldname = 'TEXT04'.
      ls_item_layout-style = 6.
      APPEND ls_item_layout TO lt_item_layout[].


      CLEAR: ls_item_layout.
      ls_item_layout-fieldname = 'TEXT05'.
      ls_item_layout-style = 7.
      APPEND ls_item_layout TO lt_item_layout[].

  ENDCASE.




  fc_t_item_layout[] = lt_item_layout[].






ENDFORM.  " CREATE_ITEM_LAYOUTS
*&---------------------------------------------------------------------*
*&      Form  BUILD_MESSAGE
*&---------------------------------------------------------------------*
* Build Message for Log
*----------------------------------------------------------------------*
FORM build_message USING ps_idno_appr     TYPE ty_s_idno_appr
                         ps_ver_data      TYPE ty_s_ver_data
                         ps_reg_hdr       TYPE zarn_reg_hdr
                         pv_validation_id TYPE zarn_e_rl_valid_id
                         pv_msgid         TYPE sy-msgid
                         pv_msgty         TYPE sy-msgty
                         pv_msgno         TYPE sy-msgno
                         pv_msgv1         TYPE sy-msgv1
                         pv_msgv2         TYPE sy-msgv2
                         pv_msgv3         TYPE sy-msgv3
                         pv_msgv4         TYPE sy-msgv4
                CHANGING pcs_log          TYPE zsarn_approvals_mass_log.

  CLEAR pcs_log.

  pcs_log-node  = ps_idno_appr-idno     && '/' &&
                  ps_idno_appr-chg_area && '/' &&
                  ps_idno_appr-chg_category.

  pcs_log-validation_id = pv_validation_id.
  pcs_log-msgno  = pv_msgno.
  pcs_log-msgty  = pv_msgty.
  pcs_log-msgid  = pv_msgid.
  pcs_log-msgv1  = pv_msgv1.
  pcs_log-msgv2  = pv_msgv2.
  pcs_log-msgv3  = pv_msgv3.
  pcs_log-msgv4  = pv_msgv4.
  pcs_log-idno   = ps_ver_data-idno.

  IF ps_idno_appr-chg_area = 'N'.
    pcs_log-version = ps_ver_data-current_ver.
  ELSEIF ps_idno_appr-chg_area = 'R'.
    pcs_log-version = ps_reg_hdr-version.
  ENDIF.


  CASE pv_msgty.
    WHEN 'S'.
      pcs_log-message_icon = icon_system_okay.
    WHEN 'E'.
      pcs_log-message_icon = icon_led_red.
    WHEN 'W'.
      pcs_log-message_icon = icon_led_yellow.
    WHEN 'I'.
      pcs_log-message_icon = icon_led_green.
  ENDCASE.

  MESSAGE ID pv_msgid
        TYPE pv_msgty
      NUMBER pv_msgno
        WITH pv_msgv1 pv_msgv2 pv_msgv3 pv_msgv4
        INTO pcs_log-message_txt.



ENDFORM.  " BUILD_MESSAGE
*&---------------------------------------------------------------------*
*&      Form  GET_NAT_REG_DATA
*&---------------------------------------------------------------------*
* Get National and Regional Data
*----------------------------------------------------------------------*
FORM get_nat_reg_data USING fu_t_ver_data      TYPE ty_t_ver_data
                   CHANGING fc_t_prod_data     TYPE ty_t_prod_data
                            fc_t_reg_data      TYPE ty_t_reg_data
                            fc_t_prod_data_o   TYPE ty_t_prod_data
                            fc_t_reg_data_o    TYPE ty_t_reg_data.

  DATA: ls_ver_data  TYPE ty_s_ver_data,
        lt_key       TYPE ztarn_key,
        ls_key       TYPE zsarn_key,
        lt_reg_key   TYPE ztarn_reg_key,
        ls_reg_key   TYPE zsarn_reg_key,
        lt_prod_data TYPE ty_t_prod_data,
        ls_prod_data TYPE ty_s_prod_data,
        lt_reg_data  TYPE ty_t_reg_data,
        ls_reg_data  TYPE ty_s_reg_data.

  CLEAR : fc_t_prod_data[],
          fc_t_reg_data[],
          fc_t_prod_data_o[],
          fc_t_reg_data_o[].

  IF fu_t_ver_data[] IS INITIAL.
    RETURN.
  ENDIF.

  LOOP AT fu_t_ver_data[] INTO ls_ver_data.

    CLEAR ls_key.
    ls_key-idno    = ls_ver_data-idno.
    ls_key-version = ls_ver_data-current_ver.
    APPEND ls_key TO lt_key[].

    CLEAR ls_reg_key.
    ls_reg_key-idno = ls_ver_data-idno.
    APPEND ls_reg_key TO lt_reg_key[].

  ENDLOOP.


* Get National Data
  CLEAR lt_prod_data[].
  CALL FUNCTION 'ZARN_READ_NATIONAL_DATA'
    EXPORTING
      it_key  = lt_key[]
    IMPORTING
      et_data = lt_prod_data[].


* Get Regional Data
  CLEAR lt_reg_data[].
  CALL FUNCTION 'ZARN_READ_REGIONAL_DATA'
    EXPORTING
      it_key  = lt_reg_key[]
    IMPORTING
      et_data = lt_reg_data[].


  fc_t_prod_data[]     = lt_prod_data[].
  fc_t_reg_data[]      = lt_reg_data[].

  fc_t_prod_data_o[]   = lt_prod_data[].
  fc_t_reg_data_o[]    = lt_reg_data[].



ENDFORM.  " GET_NAT_REG_DATA
*&---------------------------------------------------------------------*
*&      Form  DISPLAY_LOG
*&---------------------------------------------------------------------*
* Display Log
*----------------------------------------------------------------------*
FORM display_log .

  DATA: lo_dialogbox_container TYPE REF TO cl_gui_dialogbox_container,
        lo_log_alv             TYPE REF TO cl_salv_table,
        "lt_val_output TYPE zarn_t_rl_output,
        "lr_table      TYPE REF TO cl_salv_table,
        lr_layout              TYPE REF TO cl_salv_layout,
        ls_lay_key             TYPE salv_s_layout_key,
        lv_header              TYPE lvc_title,
        lr_excep               TYPE REF TO zcx_arn_validation_err.

*  IF lo_dialogbox_container IS INITIAL.
  CREATE OBJECT lo_dialogbox_container
    EXPORTING
      top     = 150
      left    = 150
*     lifetime = CNTL_LIFETIME_session
      caption = 'Message Log'
      width   = 1000
      height  = 200.

  SET HANDLER go_appr_mass_event_handler->dialog_close FOR lo_dialogbox_container.
*  ELSE.
*    CALL METHOD lo_dialogbox_container->set_visible
*      EXPORTING
*        visible = abap_true.
*
*  ENDIF.




  CLEAR lo_log_alv.
  TRY.

      cl_salv_table=>factory( EXPORTING r_container = lo_dialogbox_container
                              IMPORTING r_salv_table = lo_log_alv
                              CHANGING t_table = gt_log[] ).

* CATCH cx_salv_msg .
*ENDTRY.

      lo_log_alv->get_columns( )->set_optimize( 'X' ).

*      lr_layout = lr_table->get_layout( ).
*      lr_layout->set_default( 'X' ).
*      ls_lay_key-report = sy-repid.
*      lr_layout->set_key( ls_lay_key ).
*      lr_layout->set_save_restriction( if_salv_c_layout=>restrict_none ).

      lv_header = |AReNa: Mass Approvals Message Log - Number of messages { lines( gt_log ) } |.
      lo_log_alv->get_display_settings( )->set_list_header( lv_header ).
      lo_log_alv->get_functions( )->set_all( ).

      lo_log_alv->display( ).
*    lr_table->refresh_table_display( ).
    CATCH cx_salv_msg.
      "TBD
  ENDTRY.


ENDFORM.  " DISPLAY_LOG
*&---------------------------------------------------------------------*
*&      Form  REFRESH_TREE
*&---------------------------------------------------------------------*
* Refresh Tree
*----------------------------------------------------------------------*
FORM refresh_tree .

  PERFORM initialize.


  TRY.
      lcl_controller=>get( )->main( ).
    CATCH lcx_exception.
      WRITE 'error'.
      RETURN.
  ENDTRY.

* Get National and Regional Data
  PERFORM get_nat_reg_data USING gt_ver_data[]
                        CHANGING gt_prod_data[]
                                 gt_reg_data[]
                                 gt_prod_data_o[]
                                 gt_reg_data_o[].

  lcl_controller=>get( )->call_screen_0100( abap_false ).

  lcl_controller=>get( )->mo_view->refresh_tree_display( ).

  lcl_controller=>get( )->mo_view->get_data( IMPORTING et_output = gt_output[] ).


ENDFORM.  " REFRESH_TREE
*&---------------------------------------------------------------------*
*&      Form  INITIALIZE
*&---------------------------------------------------------------------*
* Initialize Data
*----------------------------------------------------------------------*
FORM initialize .

*  lcl_controller=>initialize( ).

  FREE : "go_appr_mass_event_handler,
         "go_approval,
         "go_dialogbox_container,
         "go_log_alv,

         gv_display,

         gt_idno_appr[],
         gt_appr_nat_fieldcat[],
         gt_appr_reg_fieldcat[],
         gt_output[],
         gt_reg_hdr[],
         gt_prd_version[],
         gt_ver_data[],
         gt_log[],
         gt_val_output[],
         gt_prod_data[],
         gt_reg_data[],
         gt_prod_data_o[],
         gt_reg_data_o[].

  CLEAR: "go_appr_mass_event_handler,
         "go_approval,
         "go_dialogbox_container,
         "go_log_alv,

         gv_display,

         gt_idno_appr[],
         gt_appr_nat_fieldcat[],
         gt_appr_reg_fieldcat[],
         gt_output[],
         gt_reg_hdr[],
         gt_prd_version[],
         gt_ver_data[],
         gt_log[],
         gt_val_output[],
         gt_prod_data[],
         gt_reg_data[],
         gt_prod_data_o[],
         gt_reg_data_o[].



ENDFORM.  " INITIALIZE
*&---------------------------------------------------------------------*
*&      Form  CHANGE_TOOLBAR
*&---------------------------------------------------------------------*
* Extend funtions of standard toolbar
*----------------------------------------------------------------------*
FORM change_toolbar USING fu_o_alv_tree     TYPE REF TO cl_gui_alv_tree
                 CHANGING fc_o_toolbar_tree TYPE REF TO cl_gui_toolbar.

  DATA: lv_disabled TYPE c.



* When you instantiate an instance of CL_GUI_ALV_TREE the constructor
* of the base class (CL_ALV_TREE_BASE) creates a toolbar.
* Fetch its reference with the following method if you want to
* modify it:
  CALL METHOD fu_o_alv_tree->get_toolbar_object
    IMPORTING
      er_toolbar = fc_o_toolbar_tree.


* could happen if you do not use thes tandard toolbar
  CHECK NOT fc_o_toolbar_tree IS INITIAL.

* Add SEPERATOR to toolbar
  CALL METHOD fc_o_toolbar_tree->add_button
    EXPORTING
      fcode     = ''
      icon      = ''
      butn_type = cntb_btype_sep.


* REFRESH button
  CALL METHOD fc_o_toolbar_tree->add_button
    EXPORTING
      fcode     = gc_fcode-refresh
      icon      = icon_refresh
      butn_type = cntb_btype_button
      text      = ''
      quickinfo = 'Refresh Tree'.

* EXCEL button
  CALL METHOD fc_o_toolbar_tree->add_button
    EXPORTING
      fcode     = gc_fcode-excel
      icon      = icon_xls
      butn_type = cntb_btype_button
      text      = ''
      quickinfo = 'Excel Download'.

* Add SEPERATOR to toolbar
  CALL METHOD fc_o_toolbar_tree->add_button
    EXPORTING
      fcode     = ''
      icon      = ''
      butn_type = cntb_btype_sep.


  CLEAR lv_disabled.
* APPROVE ALL button
  AUTHORITY-CHECK OBJECT 'ZMD_CPQCHG'
                      ID 'ACTVT' FIELD '37'.
  IF sy-subrc NE 0.
    lv_disabled = abap_true.
  ENDIF.

  CALL METHOD fc_o_toolbar_tree->add_button
    EXPORTING
      fcode       = gc_fcode-mass_appr
      icon        = icon_okay
      butn_type   = cntb_btype_button
      is_disabled = lv_disabled
      text        = 'Mass Approve'
      quickinfo   = 'Approve All/Selected'.


ENDFORM.  " CHANGE_TOOLBAR
*&---------------------------------------------------------------------*
*&      Form  FUNCTION_SELECTED
*&---------------------------------------------------------------------*
* Take action on function selected
*----------------------------------------------------------------------*
FORM function_selected USING fu_v_fcode TYPE ui_func.

  DATA: lo_alv_tree     TYPE REF TO cl_gui_alv_tree,
        lo_toolbar_tree TYPE REF TO cl_gui_toolbar,
        lt_output       TYPE ztarn_approvals_mass_tree,
        lt_output_nodes TYPE ztarn_approvals_mass_tree,
        ls_output_nodes TYPE zsarn_approvals_mass_tree,
        ls_output       TYPE zsarn_approvals_mass_tree,
        lt_nodes        TYPE lvc_t_nkey,
        ls_nodes        TYPE lvc_s_nkey.

* Get Tree and Toolbar instances
  IF lcl_controller=>get( )->mo_view IS NOT INITIAL.
    lcl_controller=>get( )->mo_view->get_data(
                                     IMPORTING eo_alv_tree     = lo_alv_tree
                                               eo_toolbar_tree = lo_toolbar_tree
                                               et_output       = lt_output[]
                                               ).
  ENDIF.

  CHECK lo_alv_tree IS NOT INITIAL.

  CASE fu_v_fcode.

* Refresh Tree
    WHEN gc_fcode-refresh.
      PERFORM refresh_tree.



* Excel Download
    WHEN gc_fcode-excel.
      PERFORM excel_download USING lt_output[].



* Mass approval
    WHEN gc_fcode-mass_appr.
      PERFORM act_mass_approval USING lo_alv_tree
                                      lt_output[]
                                      gt_ver_data[]
                                      gt_reg_hdr[]
                             CHANGING gt_idno_appr[]
                                      gt_log[]
                                      gt_prd_version[]
                                      gt_reg_data[]
                                      gt_prod_data[]
                                      gt_reg_data_o[]
                                      gt_prod_data_o[]
                                      lt_output_nodes[].

      IF gt_log[] IS NOT INITIAL.
* Display Log
        PERFORM display_log.
      ENDIF.

* Refresh Tree
      PERFORM refresh_tree.

      CLEAR lt_nodes[].
      LOOP AT lt_output_nodes[] INTO ls_output_nodes.

        CLEAR ls_output.
* Get Node details which is Clicked
        TRY.
            ls_output = gt_output[ idno         = ls_output_nodes-idno
                                   chg_area     = ls_output_nodes-chg_area
                                   chg_category = ls_output_nodes-chg_category
                                   line_type    = 'N'
                                 ].

            CLEAR ls_nodes.
            ls_nodes-node_key = ls_output-node_key.
            APPEND ls_nodes TO lt_nodes[].

          CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
        ENDTRY.
      ENDLOOP.

      IF lt_nodes[] IS NOT INITIAL.
* Set Nodes as Selected
        CALL METHOD lo_alv_tree->set_selected_nodes
          EXPORTING
            it_selected_nodes       = lt_nodes[]
          EXCEPTIONS
            cntl_system_error       = 1
            dp_error                = 2
            failed                  = 3
            error_in_node_key_table = 4
            OTHERS                  = 5.
      ENDIF.



  ENDCASE.


ENDFORM.  " FUNCTION_SELECTED
*&---------------------------------------------------------------------*
*&      Form  LOCK_IDNO
*&---------------------------------------------------------------------*
* Lock IDNO - same lock object as of AReNa Gui
*----------------------------------------------------------------------*
FORM lock_idno USING pv_idno       TYPE zarn_idno
                     pv_scope
                     pv_wait
            CHANGING pcv_lock_user TYPE sy-uname
                     pcv_locked_by TYPE zarn_locked_by
                     pcv_lock_ind  TYPE zarn_lock_ind.

  DATA: lt_enq  TYPE STANDARD TABLE OF seqg3,
        ls_enq  LIKE LINE OF  lt_enq,
        lv_garg TYPE eqegraarg.

  CLEAR: pcv_lock_user, pcv_locked_by.

*       Check lock object
  CALL FUNCTION 'ENQUEUE_EZARN_REGIONAL'
    EXPORTING
      idno           = pv_idno
      _scope         = pv_scope
      _wait          = pv_wait
    EXCEPTIONS
      foreign_lock   = 1
      system_failure = 2
      OTHERS         = 3.
  IF sy-subrc IS NOT INITIAL.
    CLEAR lv_garg.
    lv_garg = sy-mandt && pv_idno.
*         check lock and get user name
    CALL FUNCTION 'ENQUEUE_READ'
      EXPORTING
        gclient               = sy-mandt
        gname                 = 'ZSARN_REGIONAL_LOCK'
        garg                  = lv_garg
        guname                = ' '
      TABLES
        enq                   = lt_enq
      EXCEPTIONS
        communication_failure = 1
        system_failure        = 2
        OTHERS                = 3.
    IF lt_enq IS NOT INITIAL.
      READ TABLE lt_enq INTO ls_enq INDEX 1.

      pcv_lock_user = ls_enq-guname.

      PERFORM get_control_data USING pcv_lock_user
                            CHANGING pcv_locked_by.
    ENDIF.

    pcv_lock_ind = icon_locked.

  ENDIF.

ENDFORM.  " LOCK_IDNO
*&---------------------------------------------------------------------*
*&      Form  GET_CONTROL_DATA
*&---------------------------------------------------------------------*
FORM get_control_data USING pv_uname      TYPE sy-uname
                   CHANGING pv_name_text TYPE zarn_locked_by.
  DATA:
       ls_user_address TYPE addr3_val.


  CALL FUNCTION 'SUSR_USER_ADDRESS_READ'
    EXPORTING
      user_name              = pv_uname
    IMPORTING
      user_address           = ls_user_address
    EXCEPTIONS
      user_address_not_found = 1
      OTHERS                 = 2.

  IF sy-subrc IS INITIAL.
    pv_name_text = ls_user_address-name_text.
  ELSE.
    CLEAR pv_name_text.
  ENDIF.


ENDFORM.  " GET_CONTROL_DATA
*&---------------------------------------------------------------------*
*&      Form  EXCEL_DOWNLOAD
*&---------------------------------------------------------------------*
* Excel Download
*----------------------------------------------------------------------*
FORM excel_download USING fu_t_output TYPE ztarn_approvals_mass_tree.

  DATA: lt_output   TYPE ztarn_approvals_mass_tree,
        lt_header   TYPE ty_t_line,
        lt_detail   TYPE ty_t_line,

        lv_filename TYPE string,
        lv_dir      TYPE string.


  lt_output[] = fu_t_output[].

  CLEAR lv_dir.
  CALL METHOD cl_gui_frontend_services=>directory_browse
    CHANGING
      selected_folder      = lv_dir
    EXCEPTIONS
      cntl_error           = 1
      error_no_gui         = 2
      not_supported_by_gui = 3
      OTHERS               = 4.



*  EXIT.




  CREATE OBJECT w_excel 'EXCEL.APPLICATION'. "Create object for Excel

  SET PROPERTY OF w_excel  'VISIBLE' = 1. "In background Mode

  CALL METHOD OF w_excel 'WORKBOOKS' = w_workbook.

  CALL METHOD OF w_workbook 'ADD'. "Create a new Workbook

  SET PROPERTY OF w_excel 'SheetsInNewWorkbook' = 2. "No of sheets



  CLEAR: lt_header[], lt_detail[].
* Build Excel Data
  PERFORM build_excel_data USING lt_output[]
                        CHANGING lt_header[]
                                 lt_detail[].


* Downloading Header Sheet to Excel
  PERFORM download_header_sheet USING 1
                               'Header'
                               lt_header[].

* Downloading Detail Sheet to Excel
  PERFORM download_detail_sheet USING 2
                               'Detail'
                               lt_detail[].

* Save the Excel file
  GET PROPERTY OF w_excel 'ActiveWorkbook' = w_workbook.

  CLEAR lv_filename.
  lv_filename = '\Mass_Approvals_' && sy-datum && sy-uzeit && '.XLSX'.
  lv_filename = lv_dir && lv_filename.

  CALL METHOD OF w_workbook 'SAVEAS'
    EXPORTING
      #1 = lv_filename.


*  \\vprdfile01\users$\Jitin.Kharbanda\desktop\Reg Syncup




  FREE OBJECT: w_worksheet, w_excel.

ENDFORM.  " EXCEL_DOWNLOAD
*&---------------------------------------------------------------------*
*&      Form  DOWNLOAD_HEADER_SHEET
*&---------------------------------------------------------------------*
* Downloading Header Sheet to Excel
*----------------------------------------------------------------------*
FORM download_header_sheet USING p_sheet TYPE i
                          p_name  TYPE string
                          pt_tab  TYPE ty_t_line.


  DATA: w_rc TYPE i.

  CALL METHOD OF w_excel 'WORKSHEETS' = w_worksheet
     EXPORTING
     #1 = p_sheet.

  CALL METHOD OF w_worksheet 'ACTIVATE'.

  SET PROPERTY OF w_worksheet 'NAME' = p_name.

  CALL METHOD OF w_excel 'Range' = w_range
    EXPORTING
    #1 = 'A1'
    #2 = 'S1'.

  CALL METHOD OF w_range 'INTERIOR' = w_int.
  SET PROPERTY OF w_int 'ColorIndex' = 6.
  SET PROPERTY OF w_int 'Pattern' = 1.

* Initially unlock all the columns( by default all the columns are locked )
  CALL METHOD OF w_excel 'Columns' = w_columns.

  SET PROPERTY OF w_columns 'Locked' = 0.

* Locking and formatting first column
  CALL METHOD OF w_excel 'Columns' = w_columns
    EXPORTING
    #1 = 1.

  SET PROPERTY OF w_columns  'Locked' = 1.

  SET PROPERTY OF w_columns  'NumberFormat' = '@'.

* Export the contents in the internal table to the clipboard
  CALL METHOD cl_gui_frontend_services=>clipboard_export
    IMPORTING
      data                 = pt_tab[]
    CHANGING
      rc                   = w_rc
    EXCEPTIONS
      cntl_error           = 1
      error_no_gui         = 2
      not_supported_by_gui = 3
      OTHERS               = 4.

* Paste the contents in the clipboard to the worksheet
  CALL METHOD OF w_worksheet 'Paste'.

* Autofit the columns according to the contents
  CALL METHOD OF w_excel 'Columns' = w_columns.

  CALL METHOD OF w_columns 'AutoFit'.

  FREE OBJECT: w_columns, w_range.



ENDFORM.  " DOWNLOAD_HEADER_SHEET
*&---------------------------------------------------------------------*
*&      Form  DOWNLOAD_DETAIL_SHEET
*&---------------------------------------------------------------------*
* Downloading Detail Sheet to Excel
*----------------------------------------------------------------------*
FORM download_detail_sheet USING p_sheet TYPE i
                          p_name  TYPE string
                          pt_tab  TYPE ty_t_line.


  DATA: w_rc TYPE i.

  CALL METHOD OF w_excel 'WORKSHEETS' = w_worksheet
     EXPORTING
     #1 = p_sheet.

  CALL METHOD OF w_worksheet 'ACTIVATE'.

  SET PROPERTY OF w_worksheet 'NAME' = p_name.

  CALL METHOD OF w_excel 'Range' = w_range
    EXPORTING
    #1 = 'A1'
    #2 = 'R1'.

  CALL METHOD OF w_range 'INTERIOR' = w_int.
  SET PROPERTY OF w_int 'ColorIndex' = 6.
  SET PROPERTY OF w_int 'Pattern' = 1.

* Initially unlock all the columns( by default all the columns are locked )
  CALL METHOD OF w_excel 'Columns' = w_columns.

  SET PROPERTY OF w_columns 'Locked' = 0.

* Locking and formatting first column
  CALL METHOD OF w_excel 'Columns' = w_columns
    EXPORTING
    #1 = 1.

  SET PROPERTY OF w_columns  'Locked' = 1.

  SET PROPERTY OF w_columns  'NumberFormat' = '@'.

* Export the contents in the internal table to the clipboard
  CALL METHOD cl_gui_frontend_services=>clipboard_export
    IMPORTING
      data                 = pt_tab[]
    CHANGING
      rc                   = w_rc
    EXCEPTIONS
      cntl_error           = 1
      error_no_gui         = 2
      not_supported_by_gui = 3
      OTHERS               = 4.

* Paste the contents in the clipboard to the worksheet
  CALL METHOD OF w_worksheet 'Paste'.

* Autofit the columns according to the contents
  CALL METHOD OF w_excel 'Columns' = w_columns.

  CALL METHOD OF w_columns 'AutoFit'.

  FREE OBJECT: w_columns, w_range.



ENDFORM.  " DOWNLOAD_DETAIL_SHEET
*&---------------------------------------------------------------------*
*&      Form  BUILD_EXCEL_DATA
*&---------------------------------------------------------------------*
* Build Excel Data
*----------------------------------------------------------------------*
FORM build_excel_data USING pt_output TYPE ztarn_approvals_mass_tree
                   CHANGING pt_header TYPE ty_t_line
                            pt_detail TYPE ty_t_line.

  DATA: lt_output TYPE ztarn_approvals_mass_tree,
        ls_output TYPE zsarn_approvals_mass_tree,
        lt_header TYPE ty_t_line,
        ls_header TYPE ty_s_line,
        lt_detail TYPE ty_t_line,
        ls_detail TYPE ty_s_line,

        w_deli(1) TYPE c, "Delimiter
        w_hex     TYPE x,

        lv_appr01 TYPE char50,
        lv_appr02 TYPE char50,
        lv_appr03 TYPE char50,
        lv_appr04 TYPE char50,
        lv_appr05 TYPE char50,
        lv_credat TYPE char30,
        lv_upddat TYPE char30.

  FIELD-SYMBOLS: <fs> .

  CONSTANTS wl_c09(2) TYPE n VALUE 09.


  lt_output[] = pt_output[].


  ASSIGN w_deli TO <fs> TYPE 'X'.
  w_hex = wl_c09.
  <fs> = w_hex.

********************** H E A D E R **********************

  CLEAR lt_header[].
  CLEAR ls_header.
  CONCATENATE 'Id Number'
              'Change Area'
              'Change Category'
              'Description'
              'Status'
              'Locked By'
              'CT'
              'BY'
              'AM'
              'PT'
              'MG'
              'Created On'
              'Created By'
              'Updated On'
              'Updated By'
              'Current Ver'
              'FAN Id'
              'Article Number'
              'Retail Unit Desc'
  INTO ls_header-line SEPARATED BY w_deli.

  APPEND ls_header TO lt_header[].

  LOOP AT lt_output[] INTO ls_output WHERE line_type = 'N'.

    CLEAR: lv_appr01, lv_appr02, lv_appr03, lv_appr04, lv_appr05.

    IF ls_output-appr01 = icon_okay.
      lv_appr01 = 'AR-Approve Now'.
    ELSEIF ls_output-appr01 = icon_led_green.
      lv_appr01 = 'AP-Approved'.
    ELSEIF ls_output-appr01 = icon_led_red.
      lv_appr01 = 'RJ-Rejected'.
    ELSEIF ls_output-appr01 = icon_led_yellow.
      lv_appr01 = 'AQ-Approval in Queue'.
    ELSEIF ls_output-appr01 = icon_led_inactive.
      lv_appr01 = 'NA-Not Athorized'.
    ENDIF.

    IF ls_output-appr02 = icon_okay.
      lv_appr02 = 'AR-Approve Now'.
    ELSEIF ls_output-appr02 = icon_led_green.
      lv_appr02 = 'AP-Approved'.
    ELSEIF ls_output-appr02 = icon_led_red.
      lv_appr02 = 'RJ-Rejected'.
    ELSEIF ls_output-appr02 = icon_led_yellow.
      lv_appr02 = 'AQ-Approval in Queue'.
    ELSEIF ls_output-appr02 = icon_led_inactive.
      lv_appr02 = 'NA-Not Athorized'.
    ENDIF.

    IF ls_output-appr03 = icon_okay.
      lv_appr03 = 'AR-Approve Now'.
    ELSEIF ls_output-appr03 = icon_led_green.
      lv_appr03 = 'AP-Approved'.
    ELSEIF ls_output-appr03 = icon_led_red.
      lv_appr03 = 'RJ-Rejected'.
    ELSEIF ls_output-appr03 = icon_led_yellow.
      lv_appr03 = 'AQ-Approval in Queue'.
    ELSEIF ls_output-appr03 = icon_led_inactive.
      lv_appr03 = 'NA-Not Athorized'.
    ENDIF.

    IF ls_output-appr04 = icon_okay.
      lv_appr04 = 'AR-Approve Now'.
    ELSEIF ls_output-appr04 = icon_led_green.
      lv_appr04 = 'AP-Approved'.
    ELSEIF ls_output-appr04 = icon_led_red.
      lv_appr04 = 'RJ-Rejected'.
    ELSEIF ls_output-appr04 = icon_led_yellow.
      lv_appr04 = 'AQ-Approval in Queue'.
    ELSEIF ls_output-appr04 = icon_led_inactive.
      lv_appr04 = 'NA-Not Athorized'.
    ENDIF.

    IF ls_output-appr05 = icon_okay.
      lv_appr05 = 'AR-Approve Now'.
    ELSEIF ls_output-appr05 = icon_led_green.
      lv_appr05 = 'AP-Approved'.
    ELSEIF ls_output-appr05 = icon_led_red.
      lv_appr05 = 'RJ-Rejected'.
    ELSEIF ls_output-appr05 = icon_led_yellow.
      lv_appr05 = 'AQ-Approval in Queue'.
    ELSEIF ls_output-appr05 = icon_led_inactive.
      lv_appr05 = 'NA-Not Athorized'.
    ENDIF.


    CLEAR lv_credat.
    CALL FUNCTION 'CONVERSION_EXIT_TSTPS_OUTPUT'
      EXPORTING
        input  = ls_output-chg_cat_cre_on
      IMPORTING
        output = lv_credat.

    CLEAR lv_upddat.
    CALL FUNCTION 'CONVERSION_EXIT_TSTPS_OUTPUT'
      EXPORTING
        input  = ls_output-chg_cat_chg_on
      IMPORTING
        output = lv_upddat.

    CLEAR ls_header.
    CONCATENATE ls_output-idno
                ls_output-chg_area
                ls_output-chg_category
                ls_output-description
                ls_output-status
                ls_output-locked_by
                lv_appr01
                lv_appr02
                lv_appr03
                lv_appr04
                lv_appr05
                lv_credat
                ls_output-cre_by_username
                lv_upddat
                ls_output-upd_by_username
                ls_output-current_ver
                ls_output-fan_id
                ls_output-matnr
                ls_output-retail_unit_desc
      INTO ls_header-line SEPARATED BY w_deli.

    APPEND ls_header TO lt_header[].
  ENDLOOP. " LOOP AT lt_output[] INTO ls_output WHERE line_type = 'N'.



********************** D E T A I L **********************


  CLEAR lt_detail[].
  CLEAR ls_detail.
  CONCATENATE 'Id Number'
              'Change Area'
              'Change Category'
              'Change Table'
              'Change Field'
              'Description'
              'Additional Details'
              'Reference Data'
              'Old Value'
              'New Value'
              'Created On'
              'Created By'
              'Updated On'
              'Updated By'
              'Current Ver'
              'FAN Id'
              'Article Number'
              'Retail Unit Desc'
  INTO ls_detail-line SEPARATED BY w_deli.

  APPEND ls_detail TO lt_detail[].


  LOOP AT lt_output[] INTO ls_output WHERE line_type = 'I'.


    CLEAR lv_credat.
    CALL FUNCTION 'CONVERSION_EXIT_TSTPS_OUTPUT'
      EXPORTING
        input  = ls_output-chg_cat_cre_on
      IMPORTING
        output = lv_credat.

    CLEAR lv_upddat.
    CALL FUNCTION 'CONVERSION_EXIT_TSTPS_OUTPUT'
      EXPORTING
        input  = ls_output-chg_cat_chg_on
      IMPORTING
        output = lv_upddat.

    CLEAR ls_detail.
    CONCATENATE ls_output-idno
                ls_output-chg_area
                ls_output-chg_category
                ls_output-chg_table
                ls_output-chg_field
                ls_output-description
                ls_output-add_details
                ls_output-reference_data
                ls_output-value_old
                ls_output-value_new
                lv_credat
                ls_output-cre_by_username
                lv_upddat
                ls_output-upd_by_username
                ls_output-current_ver
                ls_output-fan_id
                ls_output-matnr
                ls_output-retail_unit_desc
      INTO ls_detail-line SEPARATED BY w_deli.

    APPEND ls_detail TO lt_detail[].
  ENDLOOP. " LOOP AT lt_output[] INTO ls_output WHERE line_type = 'I'.


**********************


  pt_header[] = lt_header[].
  pt_detail[] = lt_detail[].

ENDFORM.  "BUILD_EXCEL_DATA
