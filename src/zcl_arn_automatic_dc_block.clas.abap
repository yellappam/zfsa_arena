class ZCL_ARN_AUTOMATIC_DC_BLOCK definition
  public
  final
  create private .

public section.

  constants GC_CLASS_GROUP_DC type KLASSENGR value 'Z_ZSBMDC' ##NO_TEXT.

  methods DEFAULT_ZN_UI_REG_BANNER_DC
    importing
      !IV_IDNO type ZARN_IDNO
      !IS_REG_HDR type ZARN_REG_HDR
      !IV_DCSTOCK_REL_DT type DATS
    changing
      !CT_REG_BANNER type ZTARN_REG_BANNER_UI .
  class-methods GET_INSTANCE
    returning
      value(RO_INST) type ref to ZCL_ARN_AUTOMATIC_DC_BLOCK .
  methods AUTOMATIC_SUPPLY_BLOCK_CREATE
    importing
      !IT_REG_DATA type ZTARN_REG_DATA
      !IT_PROC_IDNO type ZTARN_IDNO_DATA
    returning
      value(RT_MESSAGE) type BAL_T_MSG .
  methods INIT_LOG .
  methods SELECT_FOR_REMOVE_ZN
    importing
      !IT_RNG_MATNR type RANGE_T_MATNR
      !IT_RNG_SITEGR type ZMM_T_RNG_SITE_GROUP .
  methods PROCESS_ZN_REMOVAL
    importing
      !IV_TEST type XFELD .
  methods DISPLAY_LOG .
  methods SAVE_APP_LOG .
  methods IS_REL_ARN_DC_GROUP
    importing
      !IV_SITE_GROUP type ZMM_SITE_GROUP
    returning
      value(RV_IS_REL) type ABAP_BOOL .
  methods GET_SUPPLIED_SITES
    importing
      !IV_SITE_GROUP type ZMM_SITE_GROUP
      !IV_ARTICLE type MATNR
    returning
      value(RT_WERKS) type ZTSAW_WERKS .
  PROTECTED SECTION.
private section.

  class-data SO_INST type ref to ZCL_ARN_AUTOMATIC_DC_BLOCK .
  class-data:
    st_sos_sitegroups TYPE SORTED TABLE OF zarn_site_group WITH  NON-UNIQUE KEY sos mc_type .
  class-data:
    st_mctype TYPE SORTED TABLE OF zmd_dep_mc_type WITH NON-UNIQUE KEY department .
  data:
    mt_remove_supply_block TYPE STANDARD TABLE OF zmm_supply_block .
  data MO_APP_LOG type ref to ZCL_APP_LOG .
  data MV_MSG type STRING .
  constants GC_APPLOG_OBJ type BALOBJ_D value 'ZMD' ##NO_TEXT.
  constants GC_APPLOG_SUBOBJ type BALSUBOBJ value 'ZMD_REMOVE_ZN_BL' ##NO_TEXT.

  methods CREATE_SUPPLY_BLOCK_ENTRY
    importing
      !IV_ARTICLE type MATNR
      !IV_SITEGROUP type KLASSE_D
    returning
      value(RV_CREATED) type ABAP_BOOL .
  methods EVAL_SITE_GROUPS
    importing
      !IS_REG_HDR type ZARN_REG_HDR
    exporting
      !EV_SITEGRP_LNI type KLASSE_D
      !EV_SITEGRP_UNI type KLASSE_D .
  methods IS_BLOCK_REQUIRED
    importing
      !IS_REG_HDR type ZARN_REG_HDR
      !IV_LNI_ACTIVE type ABAP_BOOL
      !IV_DCSTOCK_REL_DT type DATS
    returning
      value(RV_REQUIRED) type ABAP_BOOL .
ENDCLASS.



CLASS ZCL_ARN_AUTOMATIC_DC_BLOCK IMPLEMENTATION.


  METHOD automatic_supply_block_create.

    DATA: lv_lni_active TYPE abap_bool,
          ls_message    LIKE LINE OF rt_message.

    CLEAR rt_message.

    TRY.
        lv_lni_active = zcl_ftf_store_factory=>get_instance( )->get_feature(
                              iv_store     = 'RFST'    "Dummy Value
                              iv_appl_area = zif_ftf_appl_area_c=>gc_arena
                              iv_feature   = zif_ftf_feature_set_c=>gc_arena_lni_aut_zn
                                                              )->is_active( ).
      CATCH zcx_object_not_found.
        CLEAR lv_lni_active.
    ENDTRY.



    LOOP AT it_proc_idno ASSIGNING FIELD-SYMBOL(<ls_idno>).

      ASSIGN it_reg_data[ idno = <ls_idno>-idno ] TO FIELD-SYMBOL(<ls_reg_data>).
      CHECK sy-subrc EQ 0.

      TRY.

          "first check if banner 1000 has ZN => this is trigger to create ZMM_SUPPLY_BLOCK
          CHECK <ls_reg_data>-zarn_reg_banner[ banner = zif_sales_org_c=>gc_distr_centre_banner ]-vmsta
             = zif_article_status_dchain_c=>gc_not_avail_in_dc.

          eval_site_groups( EXPORTING is_reg_hdr = <ls_reg_data>-zarn_reg_hdr[ 1 ]
                            IMPORTING ev_sitegrp_uni = DATA(lv_sitegrp_uni)
                                      ev_sitegrp_lni = DATA(lv_sitegrp_lni) )  .

          IF lv_sitegrp_uni IS NOT INITIAL
          AND <ls_reg_data>-zarn_reg_hdr[ 1 ]-zz_uni_dc = abap_true.
            "create for UNI DC
            IF  create_supply_block_entry( iv_article = <ls_idno>-sap_article
                                         iv_sitegroup = lv_sitegrp_uni ) = abap_true.
              "if new record created, return message
              ls_message = VALUE #( msgid = 'ZARENA_MSG'
                                    msgno = '137'
                                    msgty = 'I'
                                    msgv1 = lv_sitegrp_uni ).
              IF 1 = 2. MESSAGE i137(zarena_msg). ENDIF.
              APPEND ls_message TO rt_message.
            ENDIF.
          ENDIF.

          IF lv_sitegrp_lni IS NOT INITIAL
           AND <ls_reg_data>-zarn_reg_hdr[ 1 ]-zz_lni_dc = abap_true
           AND lv_lni_active = abap_true.
            "create for LNI DC
            IF create_supply_block_entry( iv_article = <ls_idno>-sap_article
                                       iv_sitegroup = lv_sitegrp_lni ) = abap_true.
              "if new record created, return message
              ls_message = VALUE #( msgid = 'ZARENA_MSG'
                                    msgno = '137'
                                    msgty = 'I'
                                    msgv1 = lv_sitegrp_lni ).
              IF 1 = 2. MESSAGE i137(zarena_msg). ENDIF.
              APPEND ls_message TO rt_message.
            ENDIF.
          ENDIF.


        CATCH zcx_object_not_found.
        CATCH cx_sy_itab_line_not_found.
      ENDTRY.

    ENDLOOP.


  ENDMETHOD.


  METHOD create_supply_block_entry.

    DATA: ls_zmm_supply_block_mod TYPE zmm_supply_block.

    clear rv_created.

    "check if there is no valid entry in ZMM_SUPPLY_BLOCK for article, sitegroup and ZN status
    SELECT COUNT( * ) FROM zmm_supply_block
      WHERE lifnr = space
        AND matnr = iv_article
        AND site_group = iv_sitegroup
        AND werks = space
        AND bwscl = space
        AND mmsta = zif_article_status_dchain_c=>gc_not_avail_in_dc
        AND datab <= sy-datum
        AND datbi = '99991231'.

    CHECK sy-subrc NE 0.

    "if no valid entry -> create entry, call update task function
    CLEAR ls_zmm_supply_block_mod.
    ls_zmm_supply_block_mod = VALUE #( matnr      = iv_article
                                       site_group = iv_sitegroup
                                       mmsta      = zif_article_status_dchain_c=>gc_not_avail_in_dc
                                       datab      = sy-datum
                                       datbi      = '99991231'
                                       created_on = sy-datum
                                       created_by = sy-uname ).

    CALL FUNCTION 'Z_MM_SUPPLY_BLOCK_UPDATE' IN UPDATE TASK
      EXPORTING
        is_supply_block = ls_zmm_supply_block_mod
        iv_mod_type     = 'M'.  "modify = insert or overwrite

    rv_created = abap_true.

  ENDMETHOD.


  METHOD default_zn_ui_reg_banner_dc.

    DATA: lv_lni_active     TYPE abap_bool,
          lv_block_required TYPE abap_bool.

    READ TABLE  ct_reg_banner ASSIGNING FIELD-SYMBOL(<ls_reg_banner>)
     WITH  KEY idno   = iv_idno
               banner = zif_sales_org_c=>gc_distr_centre_banner.
    IF sy-subrc EQ 0.
      "should be always

      CLEAR lv_lni_active.

      TRY.
          lv_lni_active =  zcl_ftf_store_factory=>get_instance( )->get_feature(
                           iv_store     = 'RFST'    "Dummy Value
                           iv_appl_area = zif_ftf_appl_area_c=>gc_arena
                           iv_feature   = zif_ftf_feature_set_c=>gc_arena_lni_aut_zn
                                             )->is_active( ).
        CATCH zcx_object_not_found .
          CLEAR lv_lni_active.
      ENDTRY.

      "do this for UNI always
      "for LNI only if toggle is active
      IF is_reg_hdr-zz_uni_dc      = abap_true
        OR (  lv_lni_active        = abap_true
          AND is_reg_hdr-zz_lni_dc = abap_true ).

        IF <ls_reg_banner>-vmsta IS INITIAL
          "check if setting block is really required (DCs dont have stock etc..)
          AND is_block_required( is_reg_hdr = is_reg_hdr
                                 iv_lni_active = lv_lni_active
                                 iv_dcstock_rel_dt = iv_dcstock_rel_dt ).

          <ls_reg_banner>-vmsta = zif_article_status_dchain_c=>gc_not_avail_in_dc.
          IF <ls_reg_banner>-vmstd IS INITIAL.
            <ls_reg_banner>-vmstd = sy-datum.
          ENDIF.
        ENDIF.
      ELSE.

        IF <ls_reg_banner>-vmsta = zif_article_status_dchain_c=>gc_not_avail_in_dc.
          CLEAR: <ls_reg_banner>-vmsta, <ls_reg_banner>-vmstd.
        ENDIF.
      ENDIF.
    ENDIF.


  ENDMETHOD.


  METHOD display_log.
    mo_app_log->display( ).
  ENDMETHOD.


  METHOD eval_site_groups.

    CLEAR: ev_sitegrp_lni, ev_sitegrp_uni.

    "now determine site groups
    IF st_mctype[] IS INITIAL.
      SELECT * FROM zmd_dep_mc_type INTO TABLE st_mctype[].
    ENDIF.

    TRY.
        "department is first 3 characters of Merch. Category
        "get MC Type for department
        ASSIGN st_mctype[ department = is_reg_hdr-matkl(3) ] TO FIELD-SYMBOL(<ls_mctype>).
        CHECK sy-subrc EQ 0.

      CATCH cx_sy_itab_line_not_found.
        RETURN.
    ENDTRY.

    IF st_sos_sitegroups[] IS INITIAL.
      SELECT * FROM zarn_site_group INTO TABLE st_sos_sitegroups[].
    ENDIF.

    TRY.
        ev_sitegrp_uni = st_sos_sitegroups[ sos = 'UNI' mc_type = <ls_mctype>-mc_type ]-site_group.
      CATCH cx_sy_itab_line_not_found.
    ENDTRY.

    TRY.
        ev_sitegrp_lni = st_sos_sitegroups[ sos = 'LNI' mc_type = <ls_mctype>-mc_type ]-site_group.
      CATCH cx_sy_itab_line_not_found.
    ENDTRY.

  ENDMETHOD.


  METHOD get_instance.
    IF so_inst IS INITIAL.
      so_inst = NEW #( ).
    ENDIF.
    ro_inst = so_inst.
  ENDMETHOD.


  METHOD get_supplied_sites.

    DATA: lt_listed_dcs        TYPE STANDARD TABLE OF werks_d,
          lt_rng_listed_dcs    TYPE RANGE OF werks_d,
          lt_rng_mctype_reldcs TYPE RANGE OF werks_d.

    CLEAR rt_werks.

    "filter the sites from group only to DCs for which Article is listed today
    SELECT site~werks
        FROM t001w AS site
        INNER JOIN zcmd_sites_in_groups AS grp
        ON site~werks = grp~site
        WHERE grp~site_group = @iv_site_group
          AND site~vlfkz = @zif_site_category_c=>gc_dc
     INTO TABLE @lt_listed_dcs.

    "usually will be just 1 but rather do this in loop
    LOOP AT lt_listed_dcs ASSIGNING FIELD-SYMBOL(<lv_listed_dc>).

      "if article is not listed to this DC, dont do anything
      IF NOT  zcl_article_services=>get_instance( )->is_listed_to_site( iv_matnr = iv_article
                                                                        iv_site = <lv_listed_dc> ).
        DELETE lt_listed_dcs.
      ENDIF.

    ENDLOOP.

    CHECK lt_listed_dcs[] IS NOT INITIAL.

    lt_rng_listed_dcs = VALUE #(  FOR lv_dc IN lt_listed_dcs ( sign = 'I' option = 'EQ' low = lv_dc ) ).

    "get all DCS relevant for the MC Type of Article (such as Chilled or Ambient)
    "we want to search "highest priority supplying" among these DCs, not all DCs

    SELECT SINGLE matkl FROM mara INTO @DATA(lv_matkl)
      WHERE matnr = @iv_article.

    SELECT 'I' AS sign, 'EQ' AS option, grp~site AS low
      FROM zcmd_sites_in_groups AS grp
      INNER JOIN zarn_site_group AS arn_grp
        ON grp~site_group = arn_grp~site_group
      INNER JOIN zmd_dep_mc_type AS mc
        ON arn_grp~mc_type = mc~mc_type
      WHERE mc~department = @lv_matkl(3)
      INTO CORRESPONDING FIELDS OF TABLE @lt_rng_mctype_reldcs.

    IF lt_rng_mctype_reldcs[] IS INITIAL.
      "this is safety check to cater for situation when ZARN_SITE_GROUP
      "or ZMD_DEP_MC_TYPE configs are not found
      "should not happen - if it does, dont explode the sitegroup to "supplied sites"
      RETURN.
    ENDIF.

    SELECT site~werks FROM t001w AS site
                INNER JOIN wrf3 AS sup
        ON site~kunnr = sup~locnr
        WHERE sup~loclb IN @lt_rng_listed_dcs
         AND site~werks NOT LIKE 'R%' "dont want the ref. stores to be returned
         AND sup~datab <= @sy-datum
         AND sup~datbi >= @sy-datum
         "select only if the DC is the highest priority for the store
         AND NOT EXISTS ( SELECT locnr FROM wrf3 WHERE locnr = sup~locnr
                                                   AND datab <= @sy-datum
                                                   AND datbi >= @sy-datum
                                                   AND loclb IN @lt_rng_mctype_reldcs
                                                   AND prioritaet < sup~prioritaet )
    INTO TABLE @rt_werks.

  ENDMETHOD.


  METHOD init_log.

    TRY.
        mo_app_log = zcl_app_log=>get_instance( iv_object = gc_applog_obj
                                                iv_subobject = gc_applog_subobj ).
      CATCH zcx_appl_log.
        MESSAGE e184 WITH gc_applog_obj gc_applog_subobj.
    ENDTRY.

  ENDMETHOD.


  METHOD is_block_required.

    DATA: lv_required_uni TYPE abap_bool,
          lv_required_lni TYPE abap_bool,
          lv_tomorrow     TYPE dats.

    rv_required = abap_true.
    lv_required_uni = abap_true.
    lv_required_lni = abap_true.

    lv_tomorrow = sy-datum + 1.

    IF is_reg_hdr-matnr IS INITIAL.
      "block required - article not yet created
      RETURN.
    ENDIF.

    IF is_reg_hdr-matkl IS INITIAL.
      "without MC cant determine the DC group => not sure about stock
      "we treat it as "required.
      RETURN.
    ENDIF.

    "first check DC stock release date
    IF iv_dcstock_rel_dt > lv_tomorrow.
      "block is required
      RETURN.
    ENDIF.

    eval_site_groups( EXPORTING is_reg_hdr = is_reg_hdr
                  IMPORTING ev_sitegrp_uni = DATA(lv_sitegrp_uni)
                            ev_sitegrp_lni = DATA(lv_sitegrp_lni) ).

    IF is_reg_hdr-zz_uni_dc NE abap_true
    OR lv_sitegrp_uni IS INITIAL.
      "not required from UNI perspective
      CLEAR lv_required_uni.
    ELSE.
      SELECT COUNT( * ) FROM zcmd_art_sitegrp_stock "this CDS view have info about stock for sites in sitegroup
                     WHERE material  = @is_reg_hdr-matnr
                      AND site_group = @lv_sitegrp_uni
                           AND stock > 0.
      IF sy-subrc EQ 0.
        "stock found on respective DC sitegroup -> not required to block
        CLEAR lv_required_uni.
      ENDIF.
    ENDIF.

    IF is_reg_hdr-zz_lni_dc NE abap_true
        OR iv_lni_active NE abap_true
        OR lv_sitegrp_lni IS INITIAL.
      "not required from LNI perspective
      CLEAR lv_required_lni.
    ELSE.
      "if at least 1 sitegroup has 0 stock
      SELECT  COUNT( * )  FROM zcmd_art_sitegrp_stock "this CDS view have info about stock for sites in sitegroup
                         WHERE material   = @is_reg_hdr-matnr
                           AND site_group = @lv_sitegrp_lni
                           AND stock > 0.
      IF sy-subrc EQ 0.
        "stock found on respective DC sitegroup -> not required to block
        CLEAR lv_required_lni.
      ENDIF.
    ENDIF.

    "block not required both from UNI and LNI perspective -> dont set it
    IF lv_required_uni IS INITIAL
    AND lv_required_lni IS INITIAL.
      CLEAR rv_required.
    ENDIF.

  ENDMETHOD.


  METHOD is_rel_arn_dc_group.

    CLEAR rv_is_rel.

    SELECT COUNT( * ) FROM klah
    WHERE class = iv_site_group
      AND klart = zif_classification_classtype_c=>gc_site_group
      AND klagr = gc_class_group_dc
      AND vondt <= sy-datum
      AND bisdt >= sy-datum.
    IF sy-subrc EQ 0.
      rv_is_rel = abap_true.
    ENDIF.

  ENDMETHOD.


  METHOD process_zn_removal.

    DATA(lt_seldata) = VALUE mass_tabdata(  ( tabname-name = 'ZARN_REG_BANNER'
                                             fieldnames =
                                             VALUE mass_fieldtab( ( 'VMSTA' )
                                                                  ( 'VMSTD') ) ) ).

    DATA(lv_rfcdest_arena) = zcl_rfc_destination_services=>get_instance(
                                )->get_arena_article_post_rfc( ).

    DATA : lt_message             TYPE bal_t_msg,
           lt_zarn_reg_banner_upd TYPE ztarn_reg_banner,
           lt_return              TYPE bapirettab,
           ls_return              LIKE LINE OF lt_return,
           lv_contains_err        TYPE abap_bool.

    IF iv_test = abap_true.
      MESSAGE s197 INTO mv_msg.
      mo_app_log->add_bapi_message_no_excep( ).
    ENDIF.

    IF lv_rfcdest_arena IS INITIAL.
      "posting RFC not found -> dont update, finish
      MESSAGE e183 INTO mv_msg.
      mo_app_log->add_bapi_message_no_excep( ).
      RETURN.
    ENDIF.

    IF mt_remove_supply_block[] IS INITIAL.
      MESSAGE s195 INTO mv_msg.
      mo_app_log->add_bapi_message_no_excep( ).
      RETURN.
    ENDIF.

    SELECT b~*, h~matnr FROM zarn_reg_banner AS b
    INNER JOIN zarn_reg_hdr AS h
        ON b~idno = h~idno
    FOR ALL ENTRIES IN @mt_remove_supply_block
        WHERE h~matnr  = @mt_remove_supply_block-matnr
          AND b~banner = @zif_sales_org_c=>gc_distr_centre_banner
          INTO TABLE @DATA(lt_zarn_reg_banner).

    SORT lt_zarn_reg_banner BY matnr.

    LOOP AT mt_remove_supply_block ASSIGNING FIELD-SYMBOL(<ls_remove_supply_block>).

      CLEAR: lt_message, lt_return, lv_contains_err,
      lt_zarn_reg_banner_upd[].

      MESSAGE s198 INTO mv_msg WITH <ls_remove_supply_block>-matnr
                                    <ls_remove_supply_block>-site_group.
      mo_app_log->add_bapi_message_no_excep( ).

      IF iv_test EQ abap_true.
        CONTINUE.
      ENDIF.

      "update the ZMM_SUPPLY block record
      <ls_remove_supply_block>-datbi = sy-datum.
      <ls_remove_supply_block>-changed_on = sy-datum.
      <ls_remove_supply_block>-changed_by = sy-uname.

      CALL FUNCTION 'Z_MM_SUPPLY_BLOCK_UPDATE' IN UPDATE TASK
        EXPORTING
          is_supply_block = <ls_remove_supply_block>
          iv_mod_type     = 'U'.  "update

      READ TABLE lt_zarn_reg_banner ASSIGNING FIELD-SYMBOL(<ls_zarn_reg_banner>)
       WITH KEY matnr = <ls_remove_supply_block>-matnr
       BINARY SEARCH.
      IF sy-subrc EQ 0.

        CLEAR: <ls_zarn_reg_banner>-b-vmsta,
               <ls_zarn_reg_banner>-b-vmstd.

        lt_zarn_reg_banner_upd = VALUE #( ( CORRESPONDING #( <ls_zarn_reg_banner>-b ) ) ).

        "update AReNa
        CALL FUNCTION 'ZARN_MASS_UPDATE_REGIONAL_RFC'
          DESTINATION lv_rfcdest_arena
          EXPORTING
            seldata                = lt_seldata
            iv_important_msgs_only = abap_true
          IMPORTING
            et_message             = lt_message
          TABLES
            szarn_reg_banner       = lt_zarn_reg_banner_upd.

        LOOP AT lt_message ASSIGNING FIELD-SYMBOL(<ls_message>).
          IF <ls_message>-msgty CA 'AEX'.
            lv_contains_err = abap_true.
          ENDIF.
          ls_return-id         = <ls_message>-msgid.
          ls_return-number     = <ls_message>-msgno.
          ls_return-type       = <ls_message>-msgty.
          ls_return-message_v1 = <ls_message>-msgv1.
          ls_return-message_v2 = <ls_message>-msgv2.
          ls_return-message_v3 = <ls_message>-msgv3.
          ls_return-message_v4 = <ls_message>-msgv4.
          APPEND ls_return TO lt_return.
        ENDLOOP.

      ENDIF.

      IF lv_contains_err IS INITIAL.
        MESSAGE s399 WITH <ls_remove_supply_block>-matnr
                          <ls_remove_supply_block>-site_group INTO mv_msg.
        mo_app_log->add_bapi_message_no_excep( ).
        COMMIT WORK.
      ELSE.

        ROLLBACK WORK.
        MESSAGE e414 WITH <ls_remove_supply_block>-matnr
                         <ls_remove_supply_block>-site_group INTO mv_msg.
        mo_app_log->add_bapi_message_no_excep( ).


        TRY.
            mo_app_log->add_bapi_messages( lt_return ).
          CATCH zcx_appl_log.
        ENDTRY.
      ENDIF.

    ENDLOOP.

  ENDMETHOD.


  METHOD save_app_log.
    TRY.
        mo_app_log->save_log_to_db( iv_commit = abap_true
                                    iv_refresh = abap_false ).
      CATCH zcx_appl_log.
        "should never happen
    ENDTRY.
  ENDMETHOD.


  METHOD select_for_remove_zn.
    "select all valid records with ZN block from ZMM_SUPPLY_BLOCK
    "for all sitegroups specified in ZARN_SITE_GROUPS

    SELECT * FROM zmm_supply_block AS b
     WHERE b~lifnr      = @space
       AND b~matnr      IN @it_rng_matnr
       AND b~site_group IN @it_rng_sitegr
       AND b~site_group IN ( SELECT site_group FROM zarn_site_group )
       AND b~werks      = @space
       AND b~bwscl      = @space
       AND b~mmsta      = @zif_article_status_dchain_c=>gc_not_avail_in_dc
       AND b~datbi      > @sy-datum
       AND EXISTS ( SELECT material FROM zcmd_art_sitegrp_stock "this CDS view have info about stock for sites in sitegroup
                                   WHERE material  = b~matnr
                                    AND site_group = b~site_group
                                    AND stock > 0 )
       AND NOT EXISTS ( SELECT idno FROM zcarn_dc_stock_rel_date    "this CDS view have info about DC stock release dates
                               WHERE matnr = b~matnr
                                 AND dc_stock_rel_date > @( sy-datum + 1 )  )
            INTO TABLE @mt_remove_supply_block.

  ENDMETHOD.
ENDCLASS.
