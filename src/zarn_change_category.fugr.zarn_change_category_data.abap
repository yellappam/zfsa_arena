FUNCTION zarn_change_category_data.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(IT_TABLE_O) TYPE  TABLE OPTIONAL
*"     REFERENCE(IT_TABLE_N) TYPE  TABLE
*"     REFERENCE(IV_TABNAME) TYPE  TABNAME
*"     REFERENCE(IV_NEW_VERSION) TYPE  ZARN_VERSION OPTIONAL
*"     REFERENCE(IV_OLD_VERSION) TYPE  ZARN_VERSION OPTIONAL
*"     REFERENCE(IV_CHGID) TYPE  ZARN_CHG_ID
*"     REFERENCE(IV_CHGDOC) TYPE  CDCHANGENR OPTIONAL
*"     REFERENCE(IS_PROD_DATA_N) TYPE  ZSARN_PROD_DATA OPTIONAL
*"     REFERENCE(IS_REG_DATA_N) TYPE  ZSARN_REG_DATA OPTIONAL
*"     REFERENCE(IS_PROD_DATA_O) TYPE  ZSARN_PROD_DATA OPTIONAL
*"     REFERENCE(IS_REG_DATA_O) TYPE  ZSARN_REG_DATA OPTIONAL
*"  EXPORTING
*"     REFERENCE(ET_CC_HDR) TYPE  ZTARN_CC_HDR
*"     REFERENCE(ET_CC_DET) TYPE  ZTARN_CC_DET
*"  EXCEPTIONS
*"      INPUT_MISSING
*"      INPUT_INVALID
*"      ERROR
*"----------------------------------------------------------------------
*&---------------------------------------------------------------------*
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 13/09/2018
* CHANGE No.       # Charm #9000004244 - CR092/93 PIM schema changes
* DESCRIPTION      # New national tables contain UUID as key field and
*                  # creation of WHERE clause fails because not CHAR
*                  # field. Also, does not make sense to use GUID as
*                  # key to identify whether data has changed.
*                  # Therefore adjusted logic to cater for GUID as key
*                  # and to use secondary key if defined. Note, secondary
*                  # key must be defined with GROUP = SK for it to be
*                  # recognised as such If VERSION is part of secondary
*                  # key it will be excluded from search.
* WHO              # C90001929, Aruna Mackenjee
*-----------------------------------------------------------------------


  DATA: lt_dfies          TYPE dfies_tab,
        ls_dfies          TYPE dfies,
        lt_dfies_keys     TYPE dfies_tab,
        ls_dfies_keys     TYPE dfies,
        ls_dfies_keys_ref TYPE dfies,
        ls_table_mastr    TYPE zarn_table_mastr,
        lt_fconf_distinct TYPE STANDARD TABLE OF zarn_field_confg,
        lt_field_confg    TYPE STANDARD TABLE OF zarn_field_confg,
        ls_field_confg    TYPE zarn_field_confg,
        lt_cc_hdr         TYPE ztarn_cc_hdr,
        lt_cc_det         TYPE ztarn_cc_det,
        ls_cc_hdr         TYPE zarn_cc_hdr,
        ls_cc_det         TYPE zarn_cc_det,
        ls_prod_data_n    TYPE zsarn_prod_data,
        ls_reg_data_n     TYPE zsarn_reg_data,
        ls_prod_data_o    TYPE zsarn_prod_data,    " ++ONED-217 JKH 23.11.2016
        ls_reg_data_o     TYPE zsarn_reg_data,    " ++ONED-217 JKH 23.11.2016

        lt_reg_uom_o      TYPE ztarn_reg_uom,
        ls_reg_uom_o      TYPE zarn_reg_uom,
        lt_reg_uom_n      TYPE ztarn_reg_uom,
        ls_reg_uom_n      TYPE zarn_reg_uom,
        lv_inner_cpq      TYPE flag,
        lv_shipper_cpq    TYPE flag,
        lv_gln_nr_flag    TYPE flag,
        lv_lines          TYPE i,
        lv_idno           TYPE zarn_idno,  " ++ONED-217 JKH 23.11.2016

        lt_list_price_o   TYPE ztarn_list_price,
        lt_list_price_n   TYPE ztarn_list_price,
        lt_gln_nr         TYPE ty_t_gln_nr,
        ls_gln_nr         TYPE ty_s_gln_nr,
        lt_gln_nr_del     TYPE ty_t_gln_nr,
        ls_gln_nr_del     TYPE ty_s_gln_nr,
        lt_reg_pir_o      TYPE ztarn_reg_pir,
        lt_reg_pir_n      TYPE ztarn_reg_pir,
        lt_vendor_nr      TYPE ty_t_vendor_nr,
        ls_vendor_nr      TYPE ty_s_vendor_nr,
        lt_vendor_nr_del  TYPE ty_t_vendor_nr,
        ls_vendor_nr_del  TYPE ty_s_vendor_nr,



        lv_tabname        TYPE ddobjname,
        lv_chg_ind        TYPE c,
        lv_where_str      TYPE string,
        lv_fldval         TYPE string,
        lv_where_clause   TYPE string,
        lv_fld_o          TYPE string,
        lv_fld_n          TYPE string,
        lv_key_ref        TYPE string,
        lv_uom_cat        TYPE string,
        lv_gln_nr         TYPE string,
        lv_lifnr_nr       TYPE string,
        lv_uom_code_nr    TYPE string,
        lv_hyb_code_nr    TYPE string,


        lt_table_o        TYPE REF TO data,
        ls_table_o        TYPE REF TO data,
        lt_table_n        TYPE REF TO data,
        ls_table_n        TYPE REF TO data,

        lt_excl_field     TYPE ztmd_fname_range.


  FIELD-SYMBOLS: <table_o>     TYPE table,
                 <ls_table_o>  TYPE any,
                 <table_n>     TYPE table,
                 <ls_table_n>  TYPE any,
                 <ls_fldval>   TYPE any,
                 <ls_fld_o>    TYPE any,
                 <ls_fld_n>    TYPE any,
                 <ls_uom_cat>  TYPE any,
                 <ls_gln_nr>   TYPE any,
                 <ls_uom_code> TYPE any,
                 <ls_hyb_code> TYPE any,
                 <ls_lifnr_nr> TYPE any,
                 <lv_key_ref>  TYPE any,
                 <ls_where>    TYPE any,
                 <lv_idno>     TYPE zarn_idno,
                 <lv_version>  TYPE zarn_nat_version,
                 <lv_ver_comp> TYPE zarn_ver_comp_with.

  CLEAR: et_cc_hdr[], et_cc_det[].

  IF ( it_table_n IS INITIAL AND
       it_table_o IS INITIAL ) OR
     iv_tabname IS INITIAL OR
     iv_chgid   IS INITIAL.
    RAISE input_missing.
  ENDIF.

* Validate table name
  CLEAR ls_table_mastr.
  SELECT SINGLE * FROM zarn_table_mastr
    INTO ls_table_mastr
    WHERE arena_table = iv_tabname.
  IF sy-subrc NE 0.
    RAISE input_invalid.
  ENDIF.

* Change Doc number must be provided for Regional table
  IF ls_table_mastr-arena_table_type = 'R' AND
     iv_chgdoc IS INITIAL.
    RAISE input_missing.
  ENDIF.



* Get Change Category Data
  CLEAR: lt_field_confg[].
  SELECT * FROM zarn_field_confg
    INTO CORRESPONDING FIELDS OF TABLE lt_field_confg[]
    WHERE tabname = iv_tabname.
  IF sy-subrc NE 0.
    RAISE error.
  ENDIF.

  ls_prod_data_n = is_prod_data_n.
  ls_reg_data_n  = is_reg_data_n.
  ls_prod_data_o = is_prod_data_o.        " ++ONED-217 JKH 23.11.2016
  ls_reg_data_o  = is_reg_data_o.         " ++ONED-217 JKH 23.11.2016

  lv_tabname = iv_tabname.
* Get Fields of the table
  REFRESH: lt_dfies[].
  CALL FUNCTION 'DDIF_FIELDINFO_GET'
    EXPORTING
      tabname        = lv_tabname
      langu          = sy-langu
    TABLES
      dfies_tab      = lt_dfies[]
    EXCEPTIONS
      not_found      = 1
      internal_error = 2
      OTHERS         = 3.
  IF sy-subrc <> 0.
    RAISE error.
  ENDIF.

  DELETE lt_dfies[] WHERE fieldname EQ 'MANDT'.
* Get list of table keys only
  lt_dfies_keys[] = lt_dfies[].
  DELETE lt_dfies_keys[] WHERE keyflag   NE abap_true.




  CREATE DATA lt_table_o TYPE TABLE OF (iv_tabname).
  ASSIGN lt_table_o->* TO <table_o>.

  CREATE DATA lt_table_n TYPE TABLE OF (iv_tabname).
  ASSIGN lt_table_n->* TO <table_n>.

  CREATE DATA ls_table_o TYPE (iv_tabname).
  CREATE DATA ls_table_n TYPE (iv_tabname).



  <table_o> = it_table_o.
  <table_n> = it_table_n.


* CPQ Change category should be determined by coding on top of change category UOM
  IF lv_tabname = 'ZARN_REG_UOM'.
    lt_reg_uom_o[] = <table_o>.
    lt_reg_uom_n[] = <table_n>.
  ENDIF.  " CPQ - IF lv_tabname = 'ZARN_REG_UOM'.


  IF lv_tabname = 'ZARN_LIST_PRICE'.
    CLEAR: lt_gln_nr[].
    CLEAR: lt_gln_nr_del[].

    lt_list_price_o[] = <table_o>.
    lt_list_price_n[] = <table_n>.

* Get GLN to be set to NR for LIST_PRICE
    PERFORM get_list_price_gln_nr USING lt_list_price_n[]
                                        lt_list_price_o[]
                                        ls_prod_data_n-zarn_uom_variant[]
                                        ls_prod_data_n-zarn_gtin_var[]
                                        space
                               CHANGING lt_gln_nr[].

* Get GLN to be set to NR for LIST_PRICE - for deletion
    PERFORM get_list_price_gln_nr USING lt_list_price_n[]
                                        lt_list_price_o[]
                                        ls_prod_data_n-zarn_uom_variant[]
                                        ls_prod_data_n-zarn_gtin_var[]
                                        'DEL'
                               CHANGING lt_gln_nr_del[].

  ENDIF.  " IF lv_tabname = 'ZARN_LIST_PRICE'


  IF lv_tabname = 'ZARN_REG_PIR'.
    CLEAR: lt_gln_nr[], lt_vendor_nr[].
    CLEAR: lt_gln_nr_del[], lt_vendor_nr_del[].

    lt_reg_pir_o[] = <table_o>.
    lt_reg_pir_n[] = <table_n>.

* Get GLN to be set to NR for REG_PIR
    PERFORM get_reg_pir_gln_nr USING lt_reg_pir_n[]
                                     lt_reg_pir_o[]
                                     space
                            CHANGING lt_gln_nr[]
                                     lt_vendor_nr[].

* Get GLN to be set to NR for REG_PIR - for deletion
    PERFORM get_reg_pir_gln_nr USING lt_reg_pir_n[]
                                     lt_reg_pir_o[]
                                     'DEL'
                            CHANGING lt_gln_nr_del[]
                                     lt_vendor_nr_del[].

  ENDIF.  " IF lv_tabname = 'ZARN_REG_PIR'









* For records which have been Changed or are New
  LOOP AT <table_n> ASSIGNING <ls_table_n>.

    IF <lv_idno>     IS ASSIGNED. UNASSIGN <lv_idno>.     ENDIF.
    IF <lv_version>  IS ASSIGNED. UNASSIGN <lv_version>.  ENDIF.


    ASSIGN ('<LS_TABLE_N>-IDNO')    TO <lv_idno>.
    ASSIGN ('<LS_TABLE_N>-VERSION') TO <lv_version>.

* INS Begin of Change ONED-217 JKH 23.11.2016
    IF <lv_idno> IS NOT ASSIGNED.
      IF ls_table_mastr-arena_table_type = 'N'.

        CLEAR lv_idno.
        lv_idno = ls_prod_data_n-idno.
        IF lv_idno IS INITIAL.
          lv_idno = ls_prod_data_o-idno.
        ENDIF.
        IF lv_idno IS INITIAL.
          CONTINUE.
        ENDIF.

        ASSIGN lv_idno TO <lv_idno>.


      ELSEIF ls_table_mastr-arena_table_type = 'R'.

        CLEAR lv_idno.
        lv_idno = ls_reg_data_n-idno.
        IF lv_idno IS INITIAL.
          lv_idno = ls_reg_data_o-idno.
        ENDIF.
        IF lv_idno IS INITIAL.
          CONTINUE.
        ENDIF.

        ASSIGN lv_idno TO <lv_idno>.

      ENDIF.
    ENDIF.

    IF <lv_idno> IS NOT ASSIGNED.
      CONTINUE.
    ENDIF.
* INS End of Change ONED-217 JKH 23.11.2016


* Prepare WHERE clause to Read old data
    CLEAR lv_where_clause.
    DATA(lv_use_sk) = abap_false.
    LOOP AT lt_dfies_keys[] INTO ls_dfies_keys.

      IF ls_table_mastr-arena_table_type = 'N'.
* dont read old data with version as old data will always have old version number
* hence record will not be found for new version number
        IF ls_dfies_keys-fieldname = 'VERSION'.
          CONTINUE.
        ENDIF.

        " Do not read data by GUID, will use secondary key to identify record
        " for comparison
        IF ls_dfies_keys-domname = 'SYSUUID'.
          lv_use_sk = abap_true.
          CONTINUE.
        ENDIF.

      ELSEIF ls_table_mastr-arena_table_type = 'R'.

      ENDIF.

      IF <ls_fldval> IS ASSIGNED. UNASSIGN <ls_fldval>. ENDIF.

      CLEAR lv_fldval.
      CONCATENATE '<LS_TABLE_N>-' ls_dfies_keys-fieldname INTO lv_fldval.
      CONDENSE lv_fldval.
      ASSIGN (lv_fldval) TO <ls_fldval>.

      CLEAR lv_where_str.
*      CONCATENATE ls_dfies_keys-fieldname '=' <ls_fldval> INTO lv_where_str SEPARATED BY space.

      CONCATENATE ls_dfies_keys-fieldname 'EQ' '''' INTO lv_where_str SEPARATED BY space.
      CONCATENATE lv_where_str <ls_fldval> '''' INTO lv_where_str.

      IF lv_where_clause IS NOT INITIAL.
        CONCATENATE lv_where_clause 'AND' lv_where_str INTO lv_where_clause SEPARATED BY space.
      ELSE.
        lv_where_clause = lv_where_str.
      ENDIF.
    ENDLOOP.  " LOOP AT lt_dfies_keys[] INTO ls_dfies_keys

    " When GUID is primary key, not expecting any other key fields other than client.
    IF lv_use_sk = abap_true.
      " Use secondary key to identify record for comparison
      CLEAR lv_where_clause.

      " Want to exclude VERSION from where clause so build clause from secondary key
      lt_excl_field[] = VALUE #( ( sign = 'I' option = 'EQ' low = 'VERSION' ) ).

      " Ensure secondary key is defined
      ASSIGN COMPONENT 'SK' OF STRUCTURE <ls_table_n> TO FIELD-SYMBOL(<ls_sk>).
      IF sy-subrc IS INITIAL.
        PERFORM construct_where_clause USING <ls_sk>
                                             lt_excl_field[]
                                       CHANGING lv_where_clause.
      ELSE.
        CLEAR lv_where_clause.
      ENDIF.
    ENDIF.

    IF lv_where_clause IS NOT INITIAL.

* Read Old data to determine changes or new insertions
      IF <ls_table_o> IS ASSIGNED. UNASSIGN <ls_table_o>. ENDIF.
      IF <ls_where> IS ASSIGNED. UNASSIGN <ls_where>. ENDIF.
      ASSIGN lv_where_clause TO <ls_where>.

      LOOP AT <table_o> ASSIGNING <ls_table_o> WHERE (<ls_where>).
        EXIT.
      ENDLOOP.

      CLEAR lv_chg_ind.
      IF <ls_table_o> IS ASSIGNED.
        lv_chg_ind = 'U'.

** Build Change Category Data
*        PERFORM build_change_category_data using lt_dfies
*                                                 space

        IF <lv_ver_comp> IS ASSIGNED. UNASSIGN <lv_ver_comp>. ENDIF.
        ASSIGN ('<LS_TABLE_O>-VERSION') TO <lv_ver_comp>.


        IF lv_tabname = 'ZARN_LIST_PRICE'.
* Get GLN Field Name and Value
          IF <ls_gln_nr> IS ASSIGNED. UNASSIGN <ls_gln_nr>. ENDIF.
          CLEAR lv_gln_nr.
          CONCATENATE '<LS_TABLE_N>-' 'GLN' INTO lv_gln_nr.
          CONDENSE lv_gln_nr.
          ASSIGN (lv_gln_nr) TO <ls_gln_nr>.

* Get UOM_CODE Field Name and Value
          IF <ls_uom_code> IS ASSIGNED. UNASSIGN <ls_uom_code>. ENDIF.
          CLEAR lv_uom_code_nr.
          CONCATENATE '<LS_TABLE_N>-' 'UOM_CODE' INTO lv_uom_code_nr.
          CONDENSE lv_uom_code_nr.
          ASSIGN (lv_uom_code_nr) TO <ls_uom_code>.

* Check if GLN exist in GLN_NR List then set flag as TRUE
          CLEAR lv_gln_nr_flag.
          CLEAR ls_gln_nr.
          READ TABLE lt_gln_nr[] TRANSPORTING NO FIELDS
          WITH KEY idno     = <lv_idno>
                   version  = <lv_version>
                   gln      = <ls_gln_nr>
                   uom_code = <ls_uom_code>.
          IF sy-subrc = 0.
            lv_gln_nr_flag = abap_true.
          ENDIF.

        ENDIF.  " IF lv_tabname = 'ZARN_LIST_PRICE'

        IF lv_tabname = 'ZARN_REG_PIR'.
* Get GLN_NO Field Name and Value
          IF <ls_gln_nr> IS ASSIGNED. UNASSIGN <ls_gln_nr>. ENDIF.
          CLEAR lv_gln_nr.
          CONCATENATE '<LS_TABLE_N>-' 'GLN_NO' INTO lv_gln_nr.
          CONDENSE lv_gln_nr.
          ASSIGN (lv_gln_nr) TO <ls_gln_nr>.

* Get LIFNR Field Name and Value
          IF <ls_lifnr_nr> IS ASSIGNED. UNASSIGN <ls_lifnr_nr>. ENDIF.
          CLEAR lv_lifnr_nr.
          CONCATENATE '<LS_TABLE_N>-' 'LIFNR' INTO lv_lifnr_nr.
          CONDENSE lv_lifnr_nr.
          ASSIGN (lv_lifnr_nr) TO <ls_lifnr_nr>.

* Get ORDER_UOM_PIM Field Name and Value - uom_code
          IF <ls_uom_code> IS ASSIGNED. UNASSIGN <ls_uom_code>. ENDIF.
          CLEAR lv_uom_code_nr.
          CONCATENATE '<LS_TABLE_N>-' 'ORDER_UOM_PIM' INTO lv_uom_code_nr.
          CONDENSE lv_uom_code_nr.
          ASSIGN (lv_uom_code_nr) TO <ls_uom_code>.

* Get HYBRIS_INTERNAL_CODE Field Name and Value - hyb_code
          IF <ls_hyb_code> IS ASSIGNED. UNASSIGN <ls_hyb_code>. ENDIF.
          CLEAR lv_hyb_code_nr.
          CONCATENATE '<LS_TABLE_N>-' 'HYBRIS_INTERNAL_CODE' INTO lv_hyb_code_nr.
          CONDENSE lv_hyb_code_nr.
          ASSIGN (lv_hyb_code_nr) TO <ls_hyb_code>.

* Check if GLN exist in GLN_NR List then set flag as TRUE
          CLEAR lv_gln_nr_flag.


          IF <ls_lifnr_nr> EQ 'MULTIPLE' OR <ls_lifnr_nr> EQ space.
            CLEAR ls_gln_nr.
            READ TABLE lt_gln_nr[] TRANSPORTING NO FIELDS
            WITH KEY idno                 = <lv_idno>
                     gln                  = <ls_gln_nr>
                     uom_code             = <ls_uom_code>
                     hybris_internal_code = <ls_hyb_code>.
            IF sy-subrc = 0.
              lv_gln_nr_flag = abap_true.
            ENDIF.
          ELSEIF <ls_lifnr_nr> NE 'MULTIPLE' AND
                 <ls_lifnr_nr> NE space      AND
                 <ls_lifnr_nr> IS NOT INITIAL.
            CLEAR ls_vendor_nr.
            READ TABLE lt_vendor_nr[] TRANSPORTING NO FIELDS
            WITH KEY idno                 = <lv_idno>
                     lifnr                = <ls_lifnr_nr>
                     uom_code             = <ls_uom_code>
                     hybris_internal_code = <ls_hyb_code>.
            IF sy-subrc = 0.
              lv_gln_nr_flag = abap_true.
            ENDIF.
          ENDIF.
        ENDIF.  " IF lv_tabname = 'ZARN_REG_PIR'





        LOOP AT lt_dfies INTO ls_dfies WHERE keyflag NE abap_true.

* Get Old Field Name and Value
          IF <ls_fld_o> IS ASSIGNED. UNASSIGN <ls_fld_o>. ENDIF.
          CLEAR lv_fld_o.
          CONCATENATE '<LS_TABLE_O>-' ls_dfies-fieldname INTO lv_fld_o.
          CONDENSE lv_fld_o.
          ASSIGN (lv_fld_o) TO <ls_fld_o>.

* Get New Field Name and Value
          IF <ls_fld_n> IS ASSIGNED. UNASSIGN <ls_fld_n>. ENDIF.
          CLEAR lv_fld_n.
          CONCATENATE '<LS_TABLE_N>-' ls_dfies-fieldname INTO lv_fld_n.
          CONDENSE lv_fld_n.
          ASSIGN (lv_fld_n) TO <ls_fld_n>.



          IF <ls_fld_o> NE <ls_fld_n>.
* Create Detail and Header table
            CLEAR: ls_cc_hdr.
            ls_cc_hdr-mandt = sy-mandt.
            ls_cc_hdr-chgid = iv_chgid.
            ls_cc_hdr-idno  = <lv_idno>.
            ls_cc_hdr-chg_area = ls_table_mastr-arena_table_type.

            IF ls_table_mastr-arena_table_type = 'N'.
              ls_cc_hdr-national_ver_id = iv_new_version.
              ls_cc_hdr-ver_comp_with   = iv_old_version.
            ENDIF.

            ls_cc_hdr-chg_cat_cre_by = sy-uname.

            CONCATENATE sy-datum sy-uzeit INTO ls_cc_hdr-chg_cat_cre_on.

* Get Change Category
            CLEAR ls_field_confg.
            READ TABLE lt_field_confg[] INTO ls_field_confg
            WITH KEY tabname = iv_tabname
                     fldname = ls_dfies-fieldname.
            IF sy-subrc = 0 AND ls_field_confg-chg_category IS NOT INITIAL.

*>>> SUP-222 IS 2019/02 - possibility to enable case insensitive compare for specific fields
              TRY.
                  IF ls_field_confg-ignore_case_comp = abap_true
                    AND ( to_upper( <ls_fld_o> ) = to_upper( <ls_fld_n> ) ).
                    CONTINUE.
                  ENDIF.
                CATCH cx_sy_strg_par_val.
                  "if someone configured for not suitable data type
                  "then just ignore the case insensitive compare
              ENDTRY.
*<<< SUP-222 IS 2019/02 - possibility to enable case insensitive compare for specific fields

              IF lv_tabname = 'ZARN_LIST_PRICE'.
                IF <ls_gln_nr>                 IS ASSIGNED  AND
                   lv_gln_nr_flag              EQ abap_true AND
                   ls_field_confg-chg_category EQ 'LP'.
                  ls_field_confg-chg_category  = 'NR'.
                ENDIF.
              ENDIF. " IF lv_tabname = 'ZARN_LIST_PRICE'

              IF lv_tabname = 'ZARN_REG_PIR'.
                IF <ls_gln_nr>                 IS ASSIGNED  AND
                   lv_gln_nr_flag              EQ abap_true AND
                   ls_field_confg-chg_category EQ 'LP'.
                  ls_field_confg-chg_category  = 'NR'.
                ENDIF.
              ENDIF. " IF lv_tabname = 'ZARN_REG_PIR'






              ls_cc_hdr-chg_category = ls_field_confg-chg_category.
              APPEND ls_cc_hdr TO lt_cc_hdr[].

              CLEAR ls_cc_det.
              ls_cc_det-mandt = sy-mandt.
              ls_cc_det-chgid = iv_chgid.
              ls_cc_det-idno  = <lv_idno>.
              ls_cc_det-chg_area = ls_table_mastr-arena_table_type.
              IF ls_table_mastr-arena_table_type = 'N'.
                ls_cc_det-national_ver_id = iv_new_version.
                ls_cc_det-ver_comp_with   = iv_old_version.
              ENDIF.

              ls_cc_det-chg_category = ls_field_confg-chg_category.
              ls_cc_det-chg_table    = iv_tabname.
              ls_cc_det-chg_field    = ls_dfies-fieldname.
              ls_cc_det-chg_ind      = lv_chg_ind.
              ls_cc_det-value_new    = <ls_fld_n>.
              ls_cc_det-value_old    = <ls_fld_o>.

              CONDENSE: ls_cc_det-value_new, ls_cc_det-value_old.

* Reference data - concatenation of keys
              LOOP AT lt_dfies_keys INTO ls_dfies_keys_ref.

                IF ls_table_mastr-arena_table_type = 'N'.

                  IF ls_dfies_keys_ref-fieldname NE 'IDNO' AND
                     ls_dfies_keys_ref-fieldname NE 'VERSION'.

                    IF <lv_key_ref> IS ASSIGNED. UNASSIGN <lv_key_ref>. ENDIF.
                    CLEAR lv_key_ref.
                    CONCATENATE '<LS_TABLE_N>-' ls_dfies_keys_ref-fieldname INTO lv_key_ref.
                    CONDENSE lv_key_ref.
                    ASSIGN (lv_key_ref) TO <lv_key_ref>.

                    IF ls_cc_det-reference_data IS INITIAL.
                      ls_cc_det-reference_data = <lv_key_ref>.
                    ELSE.
                      CONCATENATE ls_cc_det-reference_data <lv_key_ref> INTO ls_cc_det-reference_data
                      SEPARATED BY '/'.
                    ENDIF.
                  ENDIF.

                ELSEIF ls_table_mastr-arena_table_type = 'R'.

                  IF ls_dfies_keys_ref-fieldname NE 'IDNO'.

                    IF <lv_key_ref> IS ASSIGNED. UNASSIGN <lv_key_ref>. ENDIF.
                    CLEAR lv_key_ref.
                    CONCATENATE '<LS_TABLE_N>-' ls_dfies_keys_ref-fieldname INTO lv_key_ref.
                    CONDENSE lv_key_ref.
                    ASSIGN (lv_key_ref) TO <lv_key_ref>.

                    IF ls_cc_det-reference_data IS INITIAL.
                      ls_cc_det-reference_data = <lv_key_ref>.
                    ELSE.
                      CONCATENATE ls_cc_det-reference_data <lv_key_ref> INTO ls_cc_det-reference_data
                      SEPARATED BY '/'.
                    ENDIF.
                  ENDIF.

                ENDIF.

              ENDLOOP.  " LOOP AT lt_dfies_keys INTO ls_dfies_keys_ref


              IF ls_cc_det-reference_data CS '/'.
                CONCATENATE ls_cc_det-reference_data '/' INTO ls_cc_det-reference_data.
              ENDIF.

              IF ls_table_mastr-arena_table_type = 'R'.
                ls_cc_det-reg_cdhdr = iv_chgdoc.
              ENDIF.
              APPEND ls_cc_det TO lt_cc_det[].

            ENDIF.

          ENDIF.  " IF <ls_fld_o> NE <ls_fld_n>.

        ENDLOOP.  " LOOP AT lt_dfies INTO ls_dfies


        DELETE TABLE <table_o> FROM <ls_table_o>.

      ELSE.
        lv_chg_ind = 'I'.


*        LOOP AT lt_dfies_keys INTO ls_dfies_keys.

* Get New Field Name and Value
        IF <ls_fld_n> IS ASSIGNED. UNASSIGN <ls_fld_n>. ENDIF.
        CLEAR lv_fld_n.
        CONCATENATE '<LS_TABLE_N>-' 'IDNO' INTO lv_fld_n.
        CONDENSE lv_fld_n.
        ASSIGN (lv_fld_n) TO <ls_fld_n>.


* Set CPQ Flags for 2nd+ inner/shipper UOMs for REG_UOM - specific check
        IF lv_tabname = 'ZARN_REG_UOM'.

* Get UOM Category Field Name and Value
          IF <ls_uom_cat> IS ASSIGNED. UNASSIGN <ls_uom_cat>. ENDIF.
          CLEAR lv_uom_cat.
          CONCATENATE '<LS_TABLE_N>-' 'UOM_CATEGORY' INTO lv_uom_cat.
          CONDENSE lv_uom_cat.
          ASSIGN (lv_uom_cat) TO <ls_uom_cat>.


          CLEAR : lv_inner_cpq, lv_shipper_cpq.

          IF <ls_uom_cat> IS ASSIGNED AND <ls_uom_cat> = 'INNER'.
            READ TABLE lt_reg_uom_o[] TRANSPORTING NO FIELDS
            WITH KEY uom_category = 'INNER'.
            IF sy-subrc = 0.
              lv_inner_cpq = abap_true.
            ENDIF.
          ENDIF.

          IF <ls_uom_cat> IS ASSIGNED AND <ls_uom_cat> = 'SHIPPER'.
            READ TABLE lt_reg_uom_o[] TRANSPORTING NO FIELDS
            WITH KEY uom_category = 'SHIPPER'.
            IF sy-subrc = 0.
              lv_shipper_cpq = abap_true.
            ENDIF.
          ENDIF.

        ENDIF.  " CPQ - IF lv_tabname = 'ZARN_REG_UOM'



        IF lv_tabname = 'ZARN_LIST_PRICE'.
* Get GLN Field Name and Value
          IF <ls_gln_nr> IS ASSIGNED. UNASSIGN <ls_gln_nr>. ENDIF.
          CLEAR lv_gln_nr.
          CONCATENATE '<LS_TABLE_N>-' 'GLN' INTO lv_gln_nr.
          CONDENSE lv_gln_nr.
          ASSIGN (lv_gln_nr) TO <ls_gln_nr>.

* Get UOM_CODE Field Name and Value
          IF <ls_uom_code> IS ASSIGNED. UNASSIGN <ls_uom_code>. ENDIF.
          CLEAR lv_uom_code_nr.
          CONCATENATE '<LS_TABLE_N>-' 'UOM_CODE' INTO lv_uom_code_nr.
          CONDENSE lv_uom_code_nr.
          ASSIGN (lv_uom_code_nr) TO <ls_uom_code>.


* Check if GLN exist in GLN_NR List then set flag as TRUE
          CLEAR lv_gln_nr_flag.
          CLEAR ls_gln_nr.
          READ TABLE lt_gln_nr[] TRANSPORTING NO FIELDS
          WITH KEY idno     = <lv_idno>
                   version  = <lv_version>
                   gln      = <ls_gln_nr>
                   uom_code = <ls_uom_code>.
          IF sy-subrc = 0.
            lv_gln_nr_flag = abap_true.
          ENDIF.

        ENDIF.  " IF lv_tabname = 'ZARN_LIST_PRICE'

        IF lv_tabname = 'ZARN_REG_PIR'.
* Get GLN_NO Field Name and Value
          IF <ls_gln_nr> IS ASSIGNED. UNASSIGN <ls_gln_nr>. ENDIF.
          CLEAR lv_gln_nr.
          CONCATENATE '<LS_TABLE_N>-' 'GLN_NO' INTO lv_gln_nr.
          CONDENSE lv_gln_nr.
          ASSIGN (lv_gln_nr) TO <ls_gln_nr>.

* Get LIFNR Field Name and Value
          IF <ls_lifnr_nr> IS ASSIGNED. UNASSIGN <ls_lifnr_nr>. ENDIF.
          CLEAR lv_lifnr_nr.
          CONCATENATE '<LS_TABLE_N>-' 'LIFNR' INTO lv_lifnr_nr.
          CONDENSE lv_lifnr_nr.
          ASSIGN (lv_lifnr_nr) TO <ls_lifnr_nr>.

* Get ORDER_UOM_PIM Field Name and Value - uom_code
          IF <ls_uom_code> IS ASSIGNED. UNASSIGN <ls_uom_code>. ENDIF.
          CLEAR lv_uom_code_nr.
          CONCATENATE '<LS_TABLE_N>-' 'ORDER_UOM_PIM' INTO lv_uom_code_nr.
          CONDENSE lv_uom_code_nr.
          ASSIGN (lv_uom_code_nr) TO <ls_uom_code>.

* Get HYBRIS_INTERNAL_CODE Field Name and Value - hyb_code
          IF <ls_hyb_code> IS ASSIGNED. UNASSIGN <ls_hyb_code>. ENDIF.
          CLEAR lv_hyb_code_nr.
          CONCATENATE '<LS_TABLE_N>-' 'HYBRIS_INTERNAL_CODE' INTO lv_hyb_code_nr.
          CONDENSE lv_hyb_code_nr.
          ASSIGN (lv_hyb_code_nr) TO <ls_hyb_code>.


* Check if GLN exist in GLN_NR List then set flag as TRUE
          CLEAR lv_gln_nr_flag.


          IF <ls_lifnr_nr> EQ 'MULTIPLE' OR <ls_lifnr_nr> EQ space.
            CLEAR ls_gln_nr.
            READ TABLE lt_gln_nr[] TRANSPORTING NO FIELDS
            WITH KEY idno                 = <lv_idno>
                     gln                  = <ls_gln_nr>
                     uom_code             = <ls_uom_code>
                     hybris_internal_code = <ls_hyb_code>.
            IF sy-subrc = 0.
              lv_gln_nr_flag = abap_true.
            ENDIF.
          ELSEIF <ls_lifnr_nr> NE 'MULTIPLE' AND
                 <ls_lifnr_nr> NE space      AND
                 <ls_lifnr_nr> IS NOT INITIAL.
            CLEAR ls_vendor_nr.
            READ TABLE lt_vendor_nr[] TRANSPORTING NO FIELDS
            WITH KEY idno                 = <lv_idno>
                     lifnr                = <ls_lifnr_nr>
                     uom_code             = <ls_uom_code>
                     hybris_internal_code = <ls_hyb_code>.
            IF sy-subrc = 0.
              lv_gln_nr_flag = abap_true.
            ENDIF.
          ENDIF.
        ENDIF.  " IF lv_tabname = 'ZARN_REG_PIR'



* Create Detail and Header table
        CLEAR: ls_cc_hdr.
        ls_cc_hdr-mandt = sy-mandt.
        ls_cc_hdr-chgid = iv_chgid.
        ls_cc_hdr-idno  = <lv_idno>.
        ls_cc_hdr-chg_area = ls_table_mastr-arena_table_type.

        IF ls_table_mastr-arena_table_type = 'N'.
          ls_cc_hdr-national_ver_id = iv_new_version.
          ls_cc_hdr-ver_comp_with   = iv_old_version.
        ENDIF.

        ls_cc_hdr-chg_cat_cre_by = sy-uname.

        CONCATENATE sy-datum sy-uzeit INTO ls_cc_hdr-chg_cat_cre_on.

* For Insertion, maintain all distinct change categories
        lt_fconf_distinct[] = lt_field_confg[].
        DELETE lt_fconf_distinct[] WHERE tabname NE iv_tabname.
        DELETE lt_fconf_distinct[] WHERE chg_category IS INITIAL.
        SORT lt_fconf_distinct[] BY chg_category.
        DELETE ADJACENT DUPLICATES FROM lt_fconf_distinct[] COMPARING chg_category.

* Get Distinct Change Categories
        CLEAR ls_field_confg.
        LOOP AT lt_fconf_distinct[] INTO ls_field_confg.

** Get Change Category
*          CLEAR ls_field_confg.
*          READ TABLE lt_field_confg[] INTO ls_field_confg
*          WITH KEY tabname = iv_tabname
*                   fldname = 'KEY'.  " ls_dfies_keys-fieldname.
*          IF sy-subrc = 0.


          IF lv_tabname = 'ZARN_REG_UOM'.

            IF <ls_uom_cat>                IS ASSIGNED  AND
               <ls_uom_cat>                EQ 'INNER'   AND
               lv_inner_cpq                EQ abap_true AND
               ls_field_confg-chg_category EQ 'UOM'.
              ls_field_confg-chg_category  = 'CPQ'.
            ENDIF.

            IF <ls_uom_cat>                IS ASSIGNED  AND
               <ls_uom_cat>                EQ 'SHIPPER'   AND
               lv_shipper_cpq              EQ abap_true AND
               ls_field_confg-chg_category EQ 'UOM'.
              ls_field_confg-chg_category  = 'CPQ'.
            ENDIF.

          ENDIF.  " IF lv_tabname = 'ZARN_REG_UOM'


          IF lv_tabname = 'ZARN_LIST_PRICE'.
            IF <ls_gln_nr>                 IS ASSIGNED  AND
               lv_gln_nr_flag              EQ abap_true AND
               ls_field_confg-chg_category EQ 'LP'.
              ls_field_confg-chg_category  = 'NR'.
            ENDIF.
          ENDIF. " IF lv_tabname = 'ZARN_LIST_PRICE'

          IF lv_tabname = 'ZARN_REG_PIR'.
            IF <ls_gln_nr>                 IS ASSIGNED  AND
               lv_gln_nr_flag              EQ abap_true AND
               ls_field_confg-chg_category EQ 'LP'.
              ls_field_confg-chg_category  = 'NR'.
            ENDIF.
          ENDIF. " IF lv_tabname = 'ZARN_REG_PIR'

          ls_cc_hdr-chg_category = ls_field_confg-chg_category.

          APPEND ls_cc_hdr TO lt_cc_hdr[].

          CLEAR ls_cc_det.
          ls_cc_det-mandt = sy-mandt.
          ls_cc_det-chgid = iv_chgid.
          ls_cc_det-idno  = <lv_idno>.
          ls_cc_det-chg_area = ls_table_mastr-arena_table_type.
          IF ls_table_mastr-arena_table_type = 'N'.
            ls_cc_det-national_ver_id = iv_new_version.
            ls_cc_det-ver_comp_with   = iv_old_version.
          ENDIF.

          ls_cc_det-chg_category = ls_field_confg-chg_category.
          ls_cc_det-chg_table    = iv_tabname.
          ls_cc_det-chg_field    = 'KEY'.  " ls_dfies_keys-fieldname.
          ls_cc_det-chg_ind      = lv_chg_ind.
          ls_cc_det-value_new    = space.  " <ls_fld_n>.
          ls_cc_det-value_old    = space.

          CONDENSE: ls_cc_det-value_new, ls_cc_det-value_old.

* Reference data - concatenation of keys
          LOOP AT lt_dfies_keys INTO ls_dfies_keys_ref.

            IF ls_table_mastr-arena_table_type = 'N'.

              IF ls_dfies_keys_ref-fieldname NE 'IDNO' AND
                 ls_dfies_keys_ref-fieldname NE 'VERSION'.

                IF <lv_key_ref> IS ASSIGNED. UNASSIGN <lv_key_ref>. ENDIF.
                CLEAR lv_key_ref.
                CONCATENATE '<LS_TABLE_N>-' ls_dfies_keys_ref-fieldname INTO lv_key_ref.
                CONDENSE lv_key_ref.
                ASSIGN (lv_key_ref) TO <lv_key_ref>.

                IF ls_cc_det-reference_data IS INITIAL.
                  ls_cc_det-reference_data = <lv_key_ref>.
                ELSE.
                  CONCATENATE ls_cc_det-reference_data <lv_key_ref> INTO ls_cc_det-reference_data
                  SEPARATED BY '/'.
                ENDIF.
              ENDIF.

            ELSEIF ls_table_mastr-arena_table_type = 'R'.

              IF ls_dfies_keys_ref-fieldname NE 'IDNO'.

                IF <lv_key_ref> IS ASSIGNED. UNASSIGN <lv_key_ref>. ENDIF.
                CLEAR lv_key_ref.
                CONCATENATE '<LS_TABLE_N>-' ls_dfies_keys_ref-fieldname INTO lv_key_ref.
                CONDENSE lv_key_ref.
                ASSIGN (lv_key_ref) TO <lv_key_ref>.

                IF ls_cc_det-reference_data IS INITIAL.
                  ls_cc_det-reference_data = <lv_key_ref>.
                ELSE.
                  CONCATENATE ls_cc_det-reference_data <lv_key_ref> INTO ls_cc_det-reference_data
                  SEPARATED BY '/'.
                ENDIF.
              ENDIF.

            ENDIF.

          ENDLOOP.  " LOOP AT lt_dfies_keys INTO ls_dfies_keys_ref


          IF ls_cc_det-reference_data CS '/'.
            CONCATENATE ls_cc_det-reference_data '/' INTO ls_cc_det-reference_data.
          ENDIF.

          IF ls_table_mastr-arena_table_type = 'R'.
            ls_cc_det-reg_cdhdr = iv_chgdoc.
          ENDIF.
          APPEND ls_cc_det TO lt_cc_det[].

*          ENDIF.  " READ TABLE lt_field_confg[] INTO ls_field_confg
        ENDLOOP. " LOOP AT lt_fconf_distinct[] INTO ls_field_confg.


*        ENDLOOP.  " LOOP AT lt_dfies_keys INTO ls_dfies_keys

      ENDIF.  " if <ls_table_o> is ASSIGNED



    ENDIF.  " IF lv_where_clause IS NOT INITIAL



  ENDLOOP.  " LOOP AT <table_n> ASSIGNING <ls_table_n>



* For records which have been Deleted
  LOOP AT <table_o> ASSIGNING <ls_table_o>.
    lv_chg_ind = 'D'.

    IF <lv_idno> IS ASSIGNED.    UNASSIGN <lv_idno>.    ENDIF.
    IF <lv_version> IS ASSIGNED. UNASSIGN <lv_version>. ENDIF.

    ASSIGN ('<LS_TABLE_O>-IDNO')    TO <lv_idno>.
    ASSIGN ('<LS_TABLE_O>-VERSION') TO <lv_version>.


* INS Begin of Change ONED-217 JKH 23.11.2016
    IF <lv_idno> IS NOT ASSIGNED.
      IF ls_table_mastr-arena_table_type = 'N'.

        CLEAR lv_idno.
        lv_idno = ls_prod_data_o-idno.
        IF lv_idno IS INITIAL.
          lv_idno = ls_prod_data_n-idno.
        ENDIF.
        IF lv_idno IS INITIAL.
          CONTINUE.
        ENDIF.

        ASSIGN lv_idno TO <lv_idno>.


      ELSEIF ls_table_mastr-arena_table_type = 'R'.

        CLEAR lv_idno.
        lv_idno = ls_reg_data_o-idno.
        IF lv_idno IS INITIAL.
          lv_idno = ls_reg_data_n-idno.
        ENDIF.
        IF lv_idno IS INITIAL.
          CONTINUE.
        ENDIF.

        ASSIGN lv_idno TO <lv_idno>.

      ENDIF.
    ENDIF.

    IF <lv_idno> IS NOT ASSIGNED.
      CONTINUE.
    ENDIF.
* INS End of Change ONED-217 JKH 23.11.2016


*    LOOP AT lt_dfies_keys INTO ls_dfies_keys.

* Get New Field Name and Value
    IF <ls_fld_o> IS ASSIGNED. UNASSIGN <ls_fld_o>. ENDIF.
    CLEAR lv_fld_o.
    CONCATENATE '<LS_TABLE_O>-' 'IDNO' INTO lv_fld_o.
    CONDENSE lv_fld_o.
    ASSIGN (lv_fld_o) TO <ls_fld_o>.



    IF lv_tabname = 'ZARN_LIST_PRICE'.
* Get GLN Field Name and Value
      IF <ls_gln_nr> IS ASSIGNED. UNASSIGN <ls_gln_nr>. ENDIF.
      CLEAR lv_gln_nr.
      CONCATENATE '<LS_TABLE_O>-' 'GLN' INTO lv_gln_nr.
      CONDENSE lv_gln_nr.
      ASSIGN (lv_gln_nr) TO <ls_gln_nr>.

* Get UOM_CODE Field Name and Value
      IF <ls_uom_code> IS ASSIGNED. UNASSIGN <ls_uom_code>. ENDIF.
      CLEAR lv_uom_code_nr.
      CONCATENATE '<LS_TABLE_O>-' 'UOM_CODE' INTO lv_uom_code_nr.
      CONDENSE lv_uom_code_nr.
      ASSIGN (lv_uom_code_nr) TO <ls_uom_code>.


* Check if GLN exist in GLN_NR List then set flag as TRUE
      CLEAR lv_gln_nr_flag.
      READ TABLE lt_gln_nr_del[] TRANSPORTING NO FIELDS
      WITH KEY idno     = <lv_idno>
               version  = <lv_version>
               gln      = <ls_gln_nr>
               uom_code = <ls_uom_code>.
      IF sy-subrc = 0.
        lv_gln_nr_flag = abap_true.
      ENDIF.

    ENDIF.  " IF lv_tabname = 'ZARN_LIST_PRICE'

    IF lv_tabname = 'ZARN_REG_PIR'.
* Get GLN Field Name and Value
      IF <ls_gln_nr> IS ASSIGNED. UNASSIGN <ls_gln_nr>. ENDIF.
      CLEAR lv_gln_nr.
      CONCATENATE '<LS_TABLE_O>-' 'GLN_NO' INTO lv_gln_nr.
      CONDENSE lv_gln_nr.
      ASSIGN (lv_gln_nr) TO <ls_gln_nr>.

* Get LIFNR Field Name and Value
      IF <ls_lifnr_nr> IS ASSIGNED. UNASSIGN <ls_lifnr_nr>. ENDIF.
      CLEAR lv_lifnr_nr.
      CONCATENATE '<LS_TABLE_O>-' 'LIFNR' INTO lv_lifnr_nr.
      CONDENSE lv_lifnr_nr.
      ASSIGN (lv_lifnr_nr) TO <ls_lifnr_nr>.

* Get ORDER_UOM_PIM Field Name and Value - uom_code
      IF <ls_uom_code> IS ASSIGNED. UNASSIGN <ls_uom_code>. ENDIF.
      CLEAR lv_uom_code_nr.
      CONCATENATE '<LS_TABLE_O>-' 'ORDER_UOM_PIM' INTO lv_uom_code_nr.
      CONDENSE lv_uom_code_nr.
      ASSIGN (lv_uom_code_nr) TO <ls_uom_code>.

* Get HYBRIS_INTERNAL_CODE Field Name and Value - hyb_code
      IF <ls_hyb_code> IS ASSIGNED. UNASSIGN <ls_hyb_code>. ENDIF.
      CLEAR lv_hyb_code_nr.
      CONCATENATE '<LS_TABLE_O>-' 'HYBRIS_INTERNAL_CODE' INTO lv_hyb_code_nr.
      CONDENSE lv_hyb_code_nr.
      ASSIGN (lv_hyb_code_nr) TO <ls_hyb_code>.


* Check if GLN exist in GLN_NR List then set flag as TRUE
      CLEAR lv_gln_nr_flag.

      IF <ls_lifnr_nr> EQ 'MULTIPLE' OR <ls_lifnr_nr> EQ space.
        READ TABLE lt_gln_nr_del[] TRANSPORTING NO FIELDS
        WITH KEY idno                 = <lv_idno>
                 gln                  = <ls_gln_nr>
                 uom_code             = <ls_uom_code>
                 hybris_internal_code = <ls_hyb_code>.
        IF sy-subrc = 0.
          lv_gln_nr_flag = abap_true.
        ENDIF.
      ELSEIF <ls_lifnr_nr> NE 'MULTIPLE' AND
             <ls_lifnr_nr> NE space      AND
             <ls_lifnr_nr> IS NOT INITIAL.
        READ TABLE lt_vendor_nr_del[] TRANSPORTING NO FIELDS
        WITH KEY idno                 = <lv_idno>
                 lifnr                = <ls_lifnr_nr>
                 uom_code             = <ls_uom_code>
                 hybris_internal_code = <ls_hyb_code>.
        IF sy-subrc = 0.
          lv_gln_nr_flag = abap_true.
        ENDIF.
      ENDIF.
    ENDIF.  " IF lv_tabname = 'ZARN_REG_PIR'







* Create Detail and Header table
    CLEAR: ls_cc_hdr.
    ls_cc_hdr-mandt = sy-mandt.
    ls_cc_hdr-chgid = iv_chgid.
    ls_cc_hdr-idno  = <lv_idno>.
    ls_cc_hdr-chg_area = ls_table_mastr-arena_table_type.

    IF ls_table_mastr-arena_table_type = 'N'.
      ls_cc_hdr-national_ver_id = iv_new_version.
      ls_cc_hdr-ver_comp_with   = iv_old_version.
    ENDIF.

    ls_cc_hdr-chg_cat_cre_by = sy-uname.

    CONCATENATE sy-datum sy-uzeit INTO ls_cc_hdr-chg_cat_cre_on.

* For Deletion, maintain all distinct change categories
    lt_fconf_distinct[] = lt_field_confg[].
    DELETE lt_fconf_distinct[] WHERE tabname NE iv_tabname.
    DELETE lt_fconf_distinct[] WHERE chg_category IS INITIAL.
    SORT lt_fconf_distinct[] BY chg_category.
    DELETE ADJACENT DUPLICATES FROM lt_fconf_distinct[] COMPARING chg_category.

* Get Distinct Change Categories
    CLEAR ls_field_confg.
    LOOP AT lt_fconf_distinct[] INTO ls_field_confg.

** Get Change Category
*    CLEAR ls_field_confg.
*    READ TABLE lt_field_confg[] INTO ls_field_confg
*    WITH KEY tabname = iv_tabname
*             fldname = 'KEY'.  " ls_dfies_keys-fieldname.
*    IF sy-subrc = 0.




      IF lv_tabname = 'ZARN_LIST_PRICE'.
        IF <ls_gln_nr>                 IS ASSIGNED  AND
           lv_gln_nr_flag              EQ abap_true AND
           ls_field_confg-chg_category EQ 'LP'.
          ls_field_confg-chg_category  = 'NR'.
        ENDIF.
      ENDIF. " IF lv_tabname = 'ZARN_LIST_PRICE'

      IF lv_tabname = 'ZARN_REG_PIR'.
        IF <ls_gln_nr>                 IS ASSIGNED  AND
           lv_gln_nr_flag              EQ abap_true AND
           ls_field_confg-chg_category EQ 'LP'.
          ls_field_confg-chg_category  = 'NR'.
        ENDIF.
      ENDIF. " IF lv_tabname = 'ZARN_REG_PIR'









      ls_cc_hdr-chg_category = ls_field_confg-chg_category.
      APPEND ls_cc_hdr TO lt_cc_hdr[].

      CLEAR ls_cc_det.
      ls_cc_det-mandt = sy-mandt.
      ls_cc_det-chgid = iv_chgid.
      ls_cc_det-idno  = <lv_idno>.
      ls_cc_det-chg_area = ls_table_mastr-arena_table_type.
      IF ls_table_mastr-arena_table_type = 'N'.
        ls_cc_det-national_ver_id = iv_new_version.
        ls_cc_det-ver_comp_with   = iv_old_version.
      ENDIF.

      ls_cc_det-chg_category = ls_field_confg-chg_category.
      ls_cc_det-chg_table    = iv_tabname.
      ls_cc_det-chg_field    = 'KEY'.  " ls_dfies_keys-fieldname.
      ls_cc_det-chg_ind      = lv_chg_ind.
      ls_cc_det-value_new    = space.
      ls_cc_det-value_old    = space.  " <ls_fld_o>.

      CONDENSE: ls_cc_det-value_new, ls_cc_det-value_old.

* Reference data - concatenation of keys
      LOOP AT lt_dfies_keys INTO ls_dfies_keys_ref.

        IF ls_table_mastr-arena_table_type = 'N'.

          IF ls_dfies_keys_ref-fieldname NE 'IDNO' AND
             ls_dfies_keys_ref-fieldname NE 'VERSION'.

            IF <lv_key_ref> IS ASSIGNED. UNASSIGN <lv_key_ref>. ENDIF.
            CLEAR lv_key_ref.
            CONCATENATE '<LS_TABLE_O>-' ls_dfies_keys_ref-fieldname INTO lv_key_ref.
            CONDENSE lv_key_ref.
            ASSIGN (lv_key_ref) TO <lv_key_ref>.

            IF ls_cc_det-reference_data IS INITIAL.
              ls_cc_det-reference_data = <lv_key_ref>.
            ELSE.
              CONCATENATE ls_cc_det-reference_data <lv_key_ref> INTO ls_cc_det-reference_data
              SEPARATED BY '/'.
            ENDIF.
          ENDIF.

        ELSEIF ls_table_mastr-arena_table_type = 'R'.

          IF ls_dfies_keys_ref-fieldname NE 'IDNO'.

            IF <lv_key_ref> IS ASSIGNED. UNASSIGN <lv_key_ref>. ENDIF.
            CLEAR lv_key_ref.
            CONCATENATE '<LS_TABLE_O>-' ls_dfies_keys_ref-fieldname INTO lv_key_ref.
            CONDENSE lv_key_ref.
            ASSIGN (lv_key_ref) TO <lv_key_ref>.

            IF ls_cc_det-reference_data IS INITIAL.
              ls_cc_det-reference_data = <lv_key_ref>.
            ELSE.
              CONCATENATE ls_cc_det-reference_data <lv_key_ref> INTO ls_cc_det-reference_data
              SEPARATED BY '/'.
            ENDIF.
          ENDIF.

        ENDIF.

      ENDLOOP.  " LOOP AT lt_dfies_keys INTO ls_dfies_keys_ref

      IF ls_cc_det-reference_data CS '/'.
        CONCATENATE ls_cc_det-reference_data '/' INTO ls_cc_det-reference_data.
      ENDIF.

      IF ls_table_mastr-arena_table_type = 'R'.
        ls_cc_det-reg_cdhdr = iv_chgdoc.
      ENDIF.
      APPEND ls_cc_det TO lt_cc_det[].



*   ENDIF.  " READ TABLE lt_field_confg[] INTO ls_field_confg
    ENDLOOP. " LOOP AT lt_fconf_distinct[] INTO ls_field_confg.


*    ENDLOOP.  " LOOP AT lt_dfies_keys INTO ls_dfies_keys


  ENDLOOP.  " LOOP AT <table_o> ASSIGNING <ls_table_o>


*  SORT lt_cc_hdr[] BY chg_category.
*  DELETE ADJACENT DUPLICATES FROM lt_cc_hdr[] COMPARING chg_category.
*
*  SORT lt_cc_det[] BY chg_category.


  et_cc_hdr[] = lt_cc_hdr[].
  et_cc_det[] = lt_cc_det[].

ENDFUNCTION.
