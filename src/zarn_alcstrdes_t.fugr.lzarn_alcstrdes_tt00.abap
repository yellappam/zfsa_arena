*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 02.05.2016 at 16:25:10 by user C90001363
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_ALCSTRDES_T................................*
DATA:  BEGIN OF STATUS_ZARN_ALCSTRDES_T              .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_ALCSTRDES_T              .
CONTROLS: TCTRL_ZARN_ALCSTRDES_T
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_ALCSTRDES_T              .
TABLES: ZARN_ALCSTRDES_T               .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
