*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 05.06.2019 at 16:08:08
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_PIR_PUR_CNF................................*
DATA:  BEGIN OF STATUS_ZARN_PIR_PUR_CNF              .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_PIR_PUR_CNF              .
CONTROLS: TCTRL_ZARN_PIR_PUR_CNF
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_PIR_PUR_CNF              .
TABLES: ZARN_PIR_PUR_CNF               .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
