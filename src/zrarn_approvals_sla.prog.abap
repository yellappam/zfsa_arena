*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Feature 3142 - Arena Core Reporting
* DATE WRITTEN     # 28-11-16
* SAP VERSION      # 740
* TYPE             # Report Program
* AUTHOR           # C90003972, Paul Collins
*-----------------------------------------------------------------------
* TITLE            # Arena Core Reporting - SLA Report
* PURPOSE          # Report on Arena SLA approval times
*                  #
*                  #
*                  #
*                  #
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------


INCLUDE ziarn_approvals_slatop.

INCLUDE ziarn_approvals_slasel.

INCLUDE ziarn_approvals_slaci1.

INCLUDE ziarn_approvals_slamain.
