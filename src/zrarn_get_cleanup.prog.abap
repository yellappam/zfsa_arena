*&---------------------------------------------------------------------*
*& Report  ZRARN_GET_CLEANUP
*&
*&---------------------------------------------------------------------*
*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Functional Specification 3122
* DATE WRITTEN     # 11.04.2016
* SAP VERSION      # 731
* TYPE             # Report Program
* AUTHOR           # C90001448, Greg van der Loeff
*-----------------------------------------------------------------------
* TITLE            # AReNa: Cleanup for GET
* PURPOSE          # AReNa: Delete AReNa records created by GET that
*                  # are not I-Care not SET
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

REPORT zrarn_get_cleanup.

INCLUDE ziarn_get_cleanup_top.                           " global Data
INCLUDE ziarn_get_cleanup_sel.                           " selection screen
INCLUDE ziarn_get_cleanup_f01.                           " FORM-Routines

END-OF-SELECTION.
  PERFORM get_cleanup.
