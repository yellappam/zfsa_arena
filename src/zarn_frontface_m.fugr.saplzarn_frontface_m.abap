* regenerated at 24.08.2018 13:21:17
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_FRONTFACE_MTOP.              " Global Declarations
  INCLUDE LZARN_FRONTFACE_MUXX.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_FRONTFACE_MF...              " Subroutines
* INCLUDE LZARN_FRONTFACE_MO...              " PBO-Modules
* INCLUDE LZARN_FRONTFACE_MI...              " PAI-Modules
* INCLUDE LZARN_FRONTFACE_ME...              " Events
* INCLUDE LZARN_FRONTFACE_MP...              " Local class implement.
* INCLUDE LZARN_FRONTFACE_MT99.              " ABAP Unit tests
  INCLUDE LZARN_FRONTFACE_MF00                    . " subprograms
  INCLUDE LZARN_FRONTFACE_MI00                    . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
