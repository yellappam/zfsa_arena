FUNCTION zarn_reg_data_enrich_update.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(IT_IDNO) TYPE  ZTARN_REG_KEY
*"----------------------------------------------------------------------
*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Functional Specification 3113
* DATE WRITTEN     # 17112015
* SAP VERSION      # 740
* TYPE             # Function Module
* AUTHOR           # C90003972, Paul Collins
*-----------------------------------------------------------------------
* TITLE            # Support enrichment of regional data outside of UI
* PURPOSE          # Support enrichment of regional data outside of UI
*                  #
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 27/11/2019
* CHANGE No.       # Charm 9000006356; Jira CIP-148
* DESCRIPTION      # Added logic to post data when all approvals are
*                  # completed.
* WHO              # C90001929, Aruna Mackenjee
*-----------------------------------------------------------------------

  DATA: lt_version        TYPE ztarn_key,
        lt_prod_data      TYPE ztarn_prod_data,
        lt_reg_data_old   TYPE ztarn_reg_data,
        lt_reg_data_new   TYPE ztarn_reg_data,
        lt_idno_val_error TYPE ztarn_idno_key.

* Rquired
  CHECK it_idno IS NOT INITIAL.

* Get the version info
  PERFORM get_version_multi USING    it_idno
                            CHANGING lt_version.
* Required(Assumed always there)
  CHECK lt_version IS NOT INITIAL.

* Read the national data
  CALL FUNCTION 'ZARN_READ_NATIONAL_DATA'
    EXPORTING
      it_key  = lt_version
    IMPORTING
      et_data = lt_prod_data.
* Required(Assumed always there)
  CHECK lt_prod_data IS NOT INITIAL.

* Read the regional data
  CALL FUNCTION 'ZARN_READ_REGIONAL_DATA'
    EXPORTING
      it_key  = it_idno
    IMPORTING
      et_data = lt_reg_data_old.
* Required(Assumed always there)
  CHECK lt_reg_data_old IS NOT INITIAL.

* Enrich the regional data. We also timestamp the update here, which
* is consistent with how it's being update elsewhere(it's not fitting
* nicely in ZARN_REGIONAL_DB_UPDATE where it belongs)
  PERFORM enrich_reg_data USING    lt_prod_data
                                   lt_reg_data_old
                          CHANGING lt_reg_data_new.

* Run validations on the new data as we might need to stop auto-approval
  PERFORM validate_data USING    lt_prod_data
                                 lt_reg_data_new
                        CHANGING lt_idno_val_error.

* Process the update
  IF lt_reg_data_new IS NOT INITIAL.
    CALL FUNCTION 'ZARN_REGIONAL_DB_UPDATE'
      EXPORTING
        it_reg_data           = lt_reg_data_new
        it_reg_data_old       = lt_reg_data_old
        it_idno_val_error     = lt_idno_val_error
        iv_redetermine_status = abap_true.

    " Post changes if fully approved
    DATA(lt_art_post_key) = VALUE ztarn_key( ).

    LOOP AT lt_version INTO DATA(ls_version).
      " Ignore record if there's a validation error
      IF line_exists( lt_idno_val_error[ idno = ls_version-idno ] ).
        CONTINUE.
      ENDIF.

      DATA(lo_approval) = NEW zcl_arn_approval_backend( iv_idno = ls_version-idno iv_load_db_approvals = abap_true ).

      DATA(lv_nat_approved) = lo_approval->is_fully_approved( iv_approval_area          = zcl_arn_approval_backend=>gc_appr_area_national
                                                              iv_no_records_as_approved = abap_true ).
      DATA(lv_reg_approved) = lo_approval->is_fully_approved( zcl_arn_approval_backend=>gc_appr_area_regional ).

      IF lv_nat_approved = abap_true
     AND lv_reg_approved = abap_true.
        INSERT ls_version INTO TABLE lt_art_post_key.

        " Need to release exclusive lock so that during article posting, the approvals
        " are updated. Unlock and then set optimistic lock
        lo_approval->dequeue_appr( ).
        TRY.
            lo_approval->enqueue_appr_idno_or_wait( iv_mode = zif_enq_mode_c=>optimistic ).
          CATCH zcx_excep_cls INTO DATA(lo_excp)  ##NEEDED ##NO_HANDLER.
        ENDTRY.

      ENDIF.
    ENDLOOP.

    IF lt_art_post_key[] IS NOT INITIAL.
      PERFORM post_article USING lt_art_post_key.
    ENDIF.

  ENDIF.

ENDFUNCTION.
