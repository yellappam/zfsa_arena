*-----------------------------------------------------------------------
* PROJECT          # AReNa
* SPECIFICATION    #
* DATE WRITTEN     # 13.06.2017
* SAP VERSION      # 740
* TYPE             # Program
* AUTHOR           # C90001363, Jitin Kharbanda
*-----------------------------------------------------------------------
* TITLE            # Trigger Regional Defaulting
* PURPOSE          # This program triggers Regional Defaulting if IDNO
*                  # exist and National is approved
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      #
*                  #
*-----------------------------------------------------------------------
* CALLS TO         #
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

REPORT zrarn_reg_default MESSAGE-ID zarena_msg.

* Top Include for Declarations
INCLUDE ziarn_reg_default_top.

* Selection Screen
INCLUDE ziarn_reg_default_sel.

* Class Definition
INCLUDE ziarn_reg_default_cl_def.

* Class Implementation
INCLUDE ziarn_reg_default_cl_imp.

* Main Processing Logic
INCLUDE ziarn_reg_default_main.

* Subroutines Main
INCLUDE ziarn_reg_default_sub.
