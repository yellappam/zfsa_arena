*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Functional Specification 3151
* DATE WRITTEN     # 18/05/2016
* SAP VERSION      # 731
* TYPE             # Report Program
* AUTHOR           # C90001448, Greg van der Loeff
*-----------------------------------------------------------------------
* TITLE            # AReNa: Test program for Hybris
* PURPOSE          # AReNa: Test program for Hybris
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 17.08.2016
* CHANGE No.       # Online Initial Load
* DESCRIPTION      # Change to allow records returned from Hybris to be
*                  # sent to Online
* WHO              # C90001448, Greg van der Loeff
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------
REPORT zarn_hybris_get
       MESSAGE-ID zarena_msg.


INCLUDE zarn_hybris_get_def.
INCLUDE zarn_hybris_get_sel.
INCLUDE zarn_hybris_get_frm.

INITIALIZATION.
  s_cr_dat-sign   = 'I'.
  s_cr_dat-option = 'EQ'.
  s_cr_dat-low    = sy-datum.
  APPEND s_cr_dat.

END-OF-SELECTION.
  IF p_hyb EQ abap_true.
    PERFORM get_hybris.
  ELSEIF p_arn EQ abap_true.
    PERFORM update_arena.
  ELSEIF p_ard EQ abap_true.
    PERFORM delete_arena USING p_run_id abap_false.
    SELECT *
      INTO TABLE gt_hybris
      FROM zarn_hybris
      WHERE run_id EQ p_run_id.
  ENDIF.
  IF p_rep EQ abap_true.
    SELECT *
      INTO TABLE gt_hybris
      FROM zarn_hybris
      WHERE run_id      IN s_run_id
      AND   create_date IN s_cr_dat.
  ENDIF.
  IF p_del EQ abap_true.
    PERFORM delete_results.
  ELSE.
    PERFORM display_results.
  ENDIF.
