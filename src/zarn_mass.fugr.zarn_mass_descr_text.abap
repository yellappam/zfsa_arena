FUNCTION zarn_mass_descr_text.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(TABNAME) LIKE  DCOBJDEF-NAME
*"     VALUE(SELECTION) TYPE  STANDARD TABLE
*"  EXPORTING
*"     VALUE(D_STRUCT) LIKE  DCOBJDEF-NAME
*"     VALUE(D_FIELDS) TYPE  MASS_LISTFIELDS
*"     VALUE(DESCRIPTION) TYPE  STANDARD TABLE
*"----------------------------------------------------------------------

  DATA: lt_idno     TYPE ty_t_idno,
        lt_idno_tmp TYPE ty_t_idno,
        ls_idno     TYPE ty_s_idno,
        lt_reg_hdr  TYPE ztarn_reg_hdr,
        ls_reg_hdr  TYPE zarn_reg_hdr,

        fields      LIKE LINE OF d_fields.

  FIELD-SYMBOLS: <fs>   TYPE any,
                 <idno> TYPE zarn_idno.

*  EXIT.

  d_struct = 'ZARN_REG_HDR'.

  CLEAR fields.
  fields-name = 'RETAIL_UNIT_DESC'. APPEND fields TO d_fields.
*  fields-name = 'MATNR'. APPEND fields TO d_fields.

  CLEAR lt_idno[].
* Get Distinct IDNO
  LOOP AT selection ASSIGNING <fs>.

    IF <idno> IS ASSIGNED. UNASSIGN <idno>. ENDIF.
    ASSIGN COMPONENT 'IDNO' OF STRUCTURE <fs> TO <idno>.
    IF <idno> IS ASSIGNED.
      CLEAR ls_idno.
      ls_idno-idno = <idno>.
      APPEND ls_idno TO lt_idno[].
    ENDIF.
  ENDLOOP.

  IF lt_idno[] IS INITIAL.
    EXIT.
  ENDIF.

  lt_idno_tmp[] = lt_idno[].

  SORT lt_idno_tmp[] BY idno.
  DELETE ADJACENT DUPLICATES FROM lt_idno_tmp[] COMPARING idno.


  SELECT idno matnr retail_unit_desc
    INTO CORRESPONDING FIELDS OF TABLE lt_reg_hdr[]
    FROM zarn_reg_hdr
    FOR ALL ENTRIES IN lt_idno_tmp[]
    WHERE idno = lt_idno_tmp-idno.


  LOOP AT lt_idno[] INTO ls_idno.
    CLEAR ls_reg_hdr.
    READ TABLE lt_reg_hdr[] INTO ls_reg_hdr
    WITH KEY idno = ls_idno-idno.

    APPEND ls_reg_hdr TO description[].
  ENDLOOP.  " LOOP AT lt_idno[] INTO ls_idno

ENDFUNCTION.
