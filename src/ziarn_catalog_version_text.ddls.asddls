@AbapCatalog.sqlViewName: 'ziarncatlgvert'
@AbapCatalog.compiler.compareFilter: true
@AccessControl.authorizationCheck: #NOT_REQUIRED
@EndUserText.label: 'Catalog Version Text'
@ObjectModel.dataCategory: #TEXT
@ObjectModel.representativeKey: 'Catalog_Version'
define view ziarn_catalog_version_text as select from dd07t {    
  key cast( substring( domvalue_l, 1, 1 ) as ZARN_E_CATALOG_VERSION ) as Catalog_Version,
  @Semantics.language: true
  key dd07t.ddlanguage as Language,
  @Semantics.text: true
  dd07t.ddtext as Catalog_Version_Text
}
  where dd07t.domname = 'ZARN_D_CATALOG_VERSION'
    and dd07t.as4local = 'A'
