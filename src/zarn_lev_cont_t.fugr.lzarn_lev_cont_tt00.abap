*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 25.05.2016 at 12:09:45 by user C90001363
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_LEV_CONT_T.................................*
DATA:  BEGIN OF STATUS_ZARN_LEV_CONT_T               .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_LEV_CONT_T               .
CONTROLS: TCTRL_ZARN_LEV_CONT_T
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_LEV_CONT_T               .
TABLES: ZARN_LEV_CONT_T                .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
