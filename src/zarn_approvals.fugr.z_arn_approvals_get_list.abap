FUNCTION z_arn_approvals_get_list.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(IDNO) TYPE  ZARN_IDNO
*"     REFERENCE(VERSION) TYPE  ZARN_VERSION
*"     REFERENCE(DATA_LEVEL) TYPE  ZARN_DATA_LEVEL
*"     REFERENCE(PROCESS_TYPE) TYPE  ZARN_PROCESS_TYPE
*"     REFERENCE(ZARN_CC_HDR_TAB) TYPE  ZTARN_CC_HDR OPTIONAL
*"     REFERENCE(CHECK_AUTHS) TYPE  FLAG DEFAULT 'X'
*"     REFERENCE(IV_NO_CHGCAT_UPD) TYPE  FLAG DEFAULT SPACE
*"  EXPORTING
*"     REFERENCE(APPROVALS) TYPE  ZARN_APPROVALS_T
*"     REFERENCE(RELEASE_STATUS_CODE) TYPE  ZARN_RELEASE_STATUS_CODE
*"     REFERENCE(RELEASE_STATUS) TYPE  ZARN_REL_STATUS
*"     REFERENCE(RELEASE_STATUS_DESC) TYPE  ZARN_RELEASE_STATUS_DESC
*"----------------------------------------------------------------------

*>>>DELETED - Ivo Skolek, 3162 approval stabilisation clean up - code not USED anymore *<<<


**
**  STATICS: lt_teams_buffer  TYPE SORTED TABLE OF zarn_teams WITH UNIQUE KEY team_code.
**
**  TYPES: BEGIN OF ty_status,
**           chg_category TYPE zarn_cc_hdr-chg_category,
**           team_code    TYPE zarn_teams-team_code,
**           status       TYPE zarn_cc_team-status,
**         END OF ty_status,
**         BEGIN OF ty_rel_code,
**           display_order TYPE zarn_teams-display_order,
**           release_code  TYPE zarn_release_status_code,
**           count         TYPE i,
**         END OF ty_rel_code.
**
**  DATA: lt_matrix_all      TYPE zarn_rel_matrix_tab,
**        lt_matrix          TYPE zarn_rel_matrix_tab,
**        lt_approvals       LIKE approvals[],
**        ls_approval        LIKE LINE OF lt_approvals,
**        lv_sequence        LIKE zarn_rel_matrix-sequence,
**        lv_row             TYPE i,
**        lv_row_prior       TYPE i,
**        lt_cc_hdr          TYPE ztarn_cc_hdr,
**        ls_cc_hdr          TYPE zarn_cc_hdr,
**        lt_status          TYPE SORTED TABLE OF ty_status WITH UNIQUE KEY chg_category team_code,
**        ls_status          LIKE LINE OF lt_status,
**        lv_chgid           TYPE zarn_chg_id,
**        lt_change_cats     TYPE zarn_chg_category_t,
**        lv_authorized      TYPE flag,
**        ls_reg_hdr         TYPE zarn_reg_hdr,
**        ls_reg_hdr_new     TYPE zarn_reg_hdr,
**        ls_prd_version     TYPE zarn_prd_version,
**        ls_prd_version_new TYPE zarn_prd_version,
**        lt_cc_det          TYPE ztarn_cc_det,
**        lt_cc_team         TYPE ztarn_cc_team,
**        lt_cc_team_old     TYPE ztarn_cc_team,
**        ls_cc_team         LIKE LINE OF lt_cc_team,
**        ls_arn_cc_log      TYPE zarn_cc_log,
**        lv_changes         TYPE flag,
**        lt_rel_code        TYPE STANDARD TABLE OF ty_rel_code,
**        ls_rel_code        LIKE LINE OF lt_rel_code,
**        lt_arn_prd_version TYPE STANDARD TABLE OF zarn_prd_version.
**
**  RANGES: lr_data_level   FOR zarn_rel_matrix-data_level,
**          lr_process_type FOR zarn_rel_matrix-process_type.
**
**  FIELD-SYMBOLS: <ls_matrix_all>      LIKE LINE OF lt_matrix_all,
**                 <ls_matrix>          LIKE LINE OF lt_matrix,
**                 <ls_approval>        LIKE LINE OF lt_approvals,
**                 <ls_approval_prior>  LIKE LINE OF lt_approvals,
**                 <ls_change_cat>      LIKE LINE OF lt_change_cats,
**                 <ls_cc_hdr>          LIKE LINE OF lt_cc_hdr,
**                 <ls_status>          LIKE LINE OF lt_status,
**                 <ls_cc_team>         LIKE LINE OF lt_cc_team,
**                 <ls_team_buffer>     LIKE LINE OF lt_teams_buffer,
**                 <ls_arn_prd_version> LIKE LINE OF lt_arn_prd_version.
**
**  lv_changes = abap_false.
**
**  REFRESH approvals.
**  CLEAR: release_status,
**         release_status_desc.
**
**  IF NOT data_level IS INITIAL.
**    lr_data_level-sign   = 'I'.
**    lr_data_level-option = 'EQ'.
**    lr_data_level-low    = data_level.
**    APPEND lr_data_level.
**  ENDIF.
**
**  IF NOT process_type IS INITIAL.
**    lr_process_type-sign   = 'I'.
**    lr_process_type-option = 'EQ'.
**    lr_process_type-low    = process_type.
**    APPEND lr_process_type.
**  ENDIF.
**
**  CALL FUNCTION 'Z_ARN_APPROVALS_GET_MATRIX'
**    EXPORTING
**      idno           = idno
**      version        = version
**    TABLES
**      release_matrix = lt_matrix_all.
**
**  IF lt_teams_buffer[] IS INITIAL.
**    SELECT *
**      INTO TABLE lt_teams_buffer
**      FROM zarn_teams
**      WHERE display_order GE 1.
**    LOOP AT lt_matrix_all ASSIGNING <ls_matrix_all>.
**      READ TABLE lt_teams_buffer TRANSPORTING NO FIELDS WITH TABLE KEY team_code = <ls_matrix_all>-release_team.
**      IF sy-subrc NE 0.
**        <ls_matrix_all>-release_team = space.
**      ENDIF.
**    ENDLOOP.
**    DELETE lt_matrix WHERE release_team EQ space.
**  ENDIF.
**
**
*** Translate the change category table
**  REFRESH lt_cc_hdr.
**  IF zarn_cc_hdr_tab[] IS INITIAL.
**    CASE data_level.
**      WHEN co_appr_called_from_nat.
**        SELECT *
**          INTO TABLE lt_cc_hdr
**          FROM zarn_cc_hdr
**          WHERE idno            EQ idno
**          AND   national_ver_id LE version
**          AND   chg_area        EQ 'N'.
**      WHEN co_appr_called_from_reg.
**        SELECT *
**          INTO TABLE lt_cc_hdr
**          FROM zarn_cc_hdr
**          WHERE idno            EQ idno
**          AND   national_ver_id LE version
**          AND   chg_area        EQ 'R'.
**      WHEN OTHERS.
**        SELECT *
**          INTO TABLE lt_cc_hdr
**          FROM zarn_cc_hdr
**          WHERE idno            EQ idno
**          AND   national_ver_id LE version.
**    ENDCASE.
**  ELSE.
**    LOOP AT zarn_cc_hdr_tab ASSIGNING <ls_cc_hdr> WHERE idno EQ idno AND national_ver_id LE version.
**      APPEND <ls_cc_hdr> TO lt_cc_hdr.
**    ENDLOOP.
**  ENDIF.
**
**
**  CLEAR lt_cc_hdr.
** Translate the change category table
**  CASE data_level.
**    WHEN co_appr_called_from_nat.
**      READ TABLE zarn_cc_hdr_tab[] TRANSPORTING NO FIELDS
**      WITH KEY idno     = idno
**               chg_area = 'N'.
**      IF sy-subrc = 0.
**        LOOP AT zarn_cc_hdr_tab ASSIGNING <ls_cc_hdr>
**          WHERE idno            EQ idno
**            AND national_ver_id LE version
**            AND chg_area        EQ 'N'.
**          APPEND <ls_cc_hdr> TO lt_cc_hdr.
**        ENDLOOP.
**      ELSE.
**        SELECT *
**          INTO TABLE lt_cc_hdr
**          FROM zarn_cc_hdr
**          WHERE idno            EQ idno
**          AND   national_ver_id LE version
**          AND   chg_area        EQ 'N'.
**      ENDIF.
**
**    WHEN co_appr_called_from_reg.
**      READ TABLE zarn_cc_hdr_tab[] TRANSPORTING NO FIELDS
**      WITH KEY idno     = idno
**               chg_area = 'R'.
**      IF sy-subrc = 0.
**        LOOP AT zarn_cc_hdr_tab ASSIGNING <ls_cc_hdr>
**          WHERE idno            EQ idno
**          >>>IS fix - national version is currently not stored at regional change categories
**            AND national_ver_id LE version
**            AND chg_area        EQ 'R'.
**          <<<IS fix - national version is currently not stored at regional change categories
**          APPEND <ls_cc_hdr> TO lt_cc_hdr.
**        ENDLOOP.
**      ELSE.
**        SELECT *
**          INTO TABLE lt_cc_hdr
**          FROM zarn_cc_hdr
**          WHERE idno            EQ idno
**          >>>IS fix - national version is currently not stored at regional change categories
**          AND   national_ver_id LE version
**          <<<IS fix - national version is currently not stored at regional change categories
**          AND   chg_area        EQ 'R'.
**      ENDIF.
**
**    WHEN OTHERS.
**      IF zarn_cc_hdr_tab[] IS INITIAL.
**        SELECT *
**          INTO TABLE lt_cc_hdr
**          FROM zarn_cc_hdr
**          WHERE idno            EQ idno
**          AND   national_ver_id LE version.
**      ELSE.
**        LOOP AT zarn_cc_hdr_tab ASSIGNING <ls_cc_hdr>
**          WHERE idno            EQ idno
**            AND national_ver_id LE version.
**          APPEND <ls_cc_hdr> TO lt_cc_hdr.
**        ENDLOOP.
**      ENDIF.
**
**  ENDCASE.
**
**
**
**
**
**
**
**  REFRESH lt_status.
**
** Need to ignore any obsolete version for national
**  IF data_level EQ co_appr_called_from_nat.
**
**>>>IS160915mod - in Case of national
**    we want to
**    A) keep all keep all changes in case of NEW scenario (not yet posted)
**    B) keep only changes related to current version in case of
**
**    this is based on new logic of creating change categories
**    in NEW scenario if national data are already approved but article not yet posted
**    change categories during inbound of new version will be only deltas
**    => matrix need to look at all versions (not only the current one)
**
**
**    SELECT SINGLE article_ver
**      INTO @DATA(lv_article_version)
**      FROM zarn_ver_status
**      WHERE idno EQ @idno.
**    IF lv_article_version IS NOT INITIAL.
**
**      however in CHANGE scenario (article is already posted)
**      changes are always generated against "posted" version
**      that means we need look only at posted version
**
**
**      DELETE lt_cc_hdr
**        WHERE idno            EQ idno
**        AND   national_ver_id NE version.
**
**    ENDIF.
**
**    SELECT *
**      INTO TABLE lt_arn_prd_version
**      FROM zarn_prd_version
**      WHERE idno           EQ idno
**      AND   version_status EQ 'OBS'.
**    LOOP AT lt_arn_prd_version ASSIGNING <ls_arn_prd_version>.
**      DELETE lt_cc_hdr
**        WHERE idno            EQ <ls_arn_prd_version>-idno
**        AND   national_ver_id EQ <ls_arn_prd_version>-version.
**    ENDLOOP.
**
**
**<<<IS160915mod - in Case of national changes we need to keep only changes of current version
**
**  ENDIF.
**
**  IF NOT lt_cc_hdr[] IS INITIAL.
**
**    SELECT *
**      INTO TABLE lt_cc_team
**      FROM zarn_cc_team
**      FOR ALL ENTRIES IN lt_cc_hdr
**      WHERE chgid        EQ lt_cc_hdr-chgid
**      AND   idno         EQ lt_cc_hdr-idno
**      AND   chg_category EQ lt_cc_hdr-chg_category
**      AND   chg_area     EQ lt_cc_hdr-chg_area.
**    SORT lt_cc_team BY chg_category chg_area upd_on.
**    lt_cc_team_old[] = lt_cc_team[].
**  ENDIF.
**
**  LOOP AT lt_cc_hdr ASSIGNING <ls_cc_hdr>.
**    APPEND <ls_cc_hdr>-chg_category TO lt_change_cats.
**  ENDLOOP.
**  SORT lt_change_cats BY change_category.
**  DELETE ADJACENT DUPLICATES FROM lt_change_cats COMPARING change_category.
**
**  LOOP AT lt_change_cats ASSIGNING <ls_change_cat>.
**    LOOP AT lt_teams_buffer ASSIGNING <ls_team_buffer>.
**      CLEAR ls_status.
**      ls_status-chg_category = <ls_change_cat>-change_category.
**      ls_status-team_code    = <ls_team_buffer>-team_code.
**      LOOP AT lt_matrix_all TRANSPORTING NO FIELDS
**        WHERE data_level      IN lr_data_level
**        AND   process_type    IN lr_process_type
**        AND   change_category EQ <ls_change_cat>-change_category
**        AND   release_team    EQ <ls_team_buffer>-team_code.
**        EXIT.
**      ENDLOOP.
**      IF sy-subrc EQ 0.
**       We have something in the matrix for this change category and team
**        ls_status-status = zif_arn_approval_status=>gc_arn_appr_cc_status_pending.
**      ELSE.
**       Nothing in the matrix so not relevant
**        ls_status-status = zif_arn_approval_status=>gc_arn_appr_cc_status_notrele.
**      ENDIF.
**
**     Get the latest status for the change category and team
**      LOOP AT lt_cc_team ASSIGNING <ls_cc_team> WHERE chg_category EQ <ls_change_cat>-change_category AND team_code EQ <ls_team_buffer>-team_code.
**        IF <ls_cc_team>-status NE zif_arn_approval_status=>gc_arn_appr_cc_status_notrele.
**        ls_status-status = <ls_cc_team>-status.
**        ENDIF.
**      ENDLOOP.
**
**      INSERT ls_status INTO TABLE lt_status.
**    ENDLOOP.
**  ENDLOOP.
**
**  LOOP AT lt_change_cats ASSIGNING <ls_change_cat>.
**    REFRESH: lt_matrix,
**             lt_approvals.
**    LOOP AT lt_matrix_all ASSIGNING <ls_matrix_all>
**      WHERE data_level      IN lr_data_level
**      AND   process_type    IN lr_process_type
**      AND   change_category EQ <ls_change_cat>-change_category.
**      APPEND <ls_matrix_all> TO lt_matrix.
**    ENDLOOP.
**    IF NOT lt_matrix[] IS INITIAL.
**      SORT lt_matrix BY sequence.
**
**     Process each level of approvals individually so we can refer to previous sequence status
**      lv_row = 0.
**      LOOP AT lt_matrix ASSIGNING <ls_matrix>.
**        lv_row = lv_row + 1.
**        lv_row_prior = lv_row - 1.
**        CLEAR ls_approval.
**        ls_approval-change_category   = <ls_matrix>-change_category.
**        ls_approval-approval_sequence = <ls_matrix>-sequence.
**        ls_approval-approval_team     = <ls_matrix>-release_team.
**        ls_approval-enriching_team    = <ls_matrix>-enriching_team.
**        CALL FUNCTION 'Z_ARN_APPROVALS_GET_TEAM_NAME'
**          EXPORTING
**            approval_team      = ls_approval-approval_team
**          IMPORTING
**            approval_team_name = ls_approval-approval_team_name.
**
**       Default to waiting approval
**        ls_approval-approval_status = zif_arn_approval_status=>gc_arn_appr_status_awaitappr.
**
**        IF lv_row_prior GT 0.
**         Was the prior sequence approved ?
**          READ TABLE lt_approvals ASSIGNING <ls_approval> INDEX lv_row_prior.
**          IF sy-subrc EQ 0.
**            IF <ls_approval>-approval_status NE zif_arn_approval_status=>gc_arn_appr_status_approved.
**             Prior sequence is not approved
**              ls_approval-approval_status = zif_arn_approval_status=>gc_arn_appr_status_awaitprior.
**            ENDIF.
**          ENDIF.
**        ENDIF.
**
**        IF ls_approval-approval_status EQ zif_arn_approval_status=>gc_arn_appr_status_awaitappr.
**         Waiting approval so is it approved or rejected ?
**          IF <ls_matrix>-auto_approve EQ abap_true.
**            ls_approval-approval_status = zif_arn_approval_status=>gc_arn_appr_status_autoappr.
**          ELSE.
**           Check to see if we have any history for this
**            READ TABLE lt_status ASSIGNING <ls_status> WITH TABLE KEY chg_category = ls_approval-change_category team_code = ls_approval-approval_team.
**            IF sy-subrc EQ 0.
**              CASE <ls_status>-status.
**                WHEN zif_arn_approval_status=>gc_arn_appr_cc_status_approved.
**                  ls_approval-approval_status = zif_arn_approval_status=>gc_arn_appr_status_approved.
**                WHEN zif_arn_approval_status=>gc_arn_appr_cc_status_rejected.
**                  ls_approval-approval_status = zif_arn_approval_status=>gc_arn_appr_status_rejected.
**                WHEN zif_arn_approval_status=>gc_arn_appr_cc_status_pending.
**                  ls_approval-approval_status = zif_arn_approval_status=>gc_arn_appr_status_awaitappr.
**                WHEN zif_arn_approval_status=>gc_arn_appr_cc_status_notrele.
**                 Not relevant so do not display at all
**                  ls_approval-approval_status = space.
**              ENDCASE.
**            ENDIF.
**          ENDIF.
**        ENDIF.
**
**        IF ls_approval-approval_status EQ zif_arn_approval_status=>gc_arn_appr_status_awaitprior.
**         If we are awaiting a prior was this rejected ?
**          LOOP AT lt_status ASSIGNING <ls_status> WHERE chg_category EQ ls_approval-change_category AND team_code EQ ls_approval-approval_team.
**            CASE <ls_status>-status.
**              WHEN zif_arn_approval_status=>gc_arn_appr_cc_status_rejected.
**                ls_approval-approval_status = zif_arn_approval_status=>gc_arn_appr_status_rejected.
**              WHEN OTHERS.
**              Ignore
**            ENDCASE.
**          ENDLOOP.
**        ENDIF.
**
**       Need to change the previous sequence to awaiting approval if this is rejected
**        IF ls_approval-approval_status EQ zif_arn_approval_status=>gc_arn_appr_status_rejected.
**          IF lv_row_prior GT 0.
**            READ TABLE lt_approvals ASSIGNING <ls_approval_prior> INDEX lv_row_prior.
**            IF sy-subrc EQ 0.
**              IF <ls_approval_prior>-approval_status EQ zif_arn_approval_status=>gc_arn_appr_status_approved.
**               Current was rejected and prior was approved so set prior to awaiting approval
**                <ls_approval_prior>-approval_status = zif_arn_approval_status=>gc_arn_appr_status_awaitappr.
**              ELSEIF <ls_approval_prior>-approval_status EQ zif_arn_approval_status=>gc_arn_appr_status_autoappr.
**               Current was rejected but the prior is auto approved so set this to awaiting approval
**                ls_approval-approval_status = zif_arn_approval_status=>gc_arn_appr_status_awaitappr.
**              ENDIF.
**            ENDIF.
**          ENDIF.
**        ENDIF.
**
**        IF ls_approval-approval_status EQ zif_arn_approval_status=>gc_arn_appr_status_awaitappr.
**          CLEAR ls_rel_code.
**          READ TABLE lt_teams_buffer ASSIGNING <ls_team_buffer> WITH TABLE KEY team_code = ls_approval-approval_team.
**          IF sy-subrc EQ 0.
**            ls_rel_code-display_order = <ls_team_buffer>-display_order.
**            ls_rel_code-release_code  = <ls_matrix>-release_status_code.
**            ls_rel_code-count         = 1.
**            COLLECT ls_rel_code INTO lt_rel_code.
**          ENDIF.
**        ENDIF.
**
**        IF ls_approval-approval_status EQ zif_arn_approval_status=>gc_arn_appr_status_awaitappr.
**          IF check_auths EQ abap_true.
**           Does the user have authority to approve for this team?
**            CALL FUNCTION 'Z_ARN_APPROVALS_TEAM_AUTH'
**              EXPORTING
**                team_code  = ls_approval-approval_team
**              IMPORTING
**                authorized = lv_authorized.
**            IF lv_authorized EQ abap_false.
**              ls_approval-approval_status = zif_arn_approval_status=>gc_arn_not_authorized.
**            ENDIF.
**          ENDIF.
**        ENDIF.
**
**        IF ls_approval-approval_status NE space.
**          APPEND ls_approval TO lt_approvals.
**        ENDIF.
**      ENDLOOP.
**    ENDIF.
**
**    APPEND LINES OF lt_approvals TO approvals.
**  ENDLOOP.
**
**  SORT lt_rel_code BY display_order count DESCENDING.
**  READ TABLE lt_rel_code INTO ls_rel_code INDEX 1.
**  IF sy-subrc EQ 0.
**    release_status_code = ls_rel_code-release_code.
**  ENDIF.
**
**  IF NOT release_status_code IS INITIAL.
**    SELECT SINGLE release_status release_status_desc
**      INTO (release_status, release_status_desc)
**      FROM zarn_rel_statust
**      WHERE release_status_code EQ release_status_code.
**  ENDIF.
**
** Update any auto approvals
**  LOOP AT approvals INTO ls_approval WHERE approval_status EQ zif_arn_approval_status=>gc_arn_appr_status_autoappr.
**    LOOP AT lt_cc_hdr ASSIGNING <ls_cc_hdr> WHERE chg_category EQ ls_approval-change_category.
**      LOOP AT lt_status ASSIGNING <ls_status> WHERE chg_category EQ ls_approval-change_category AND team_code EQ ls_approval-approval_team AND status EQ zif_arn_approval_status=>gc_arn_appr_cc_status_approved.
**        EXIT.
**      ENDLOOP.
**      IF sy-subrc NE 0.
**        READ TABLE lt_teams_buffer ASSIGNING <ls_team_buffer> WITH TABLE KEY team_code = ls_approval-approval_team.
**        IF sy-subrc EQ 0.
**          UNASSIGN <ls_cc_team>.
**          READ TABLE lt_cc_team
**            ASSIGNING <ls_cc_team>
**            WITH KEY chgid        = <ls_cc_hdr>-chgid
**                     idno         = <ls_cc_hdr>-idno
**                     chg_category = <ls_cc_hdr>-chg_category
**                     chg_area     = <ls_cc_hdr>-chg_area
**                     team_code    = <ls_team_buffer>-team_code.
**          IF sy-subrc NE 0.
**            CLEAR ls_cc_team.
**            ls_cc_team-chgid        = <ls_cc_hdr>-chgid.
**            ls_cc_team-idno         = <ls_cc_hdr>-idno.
**            ls_cc_team-chg_category = <ls_cc_hdr>-chg_category.
**            ls_cc_team-chg_area     = <ls_cc_hdr>-chg_area.
**            ls_cc_team-team_code    = <ls_team_buffer>-team_code.
**            ls_cc_team-status       = zif_arn_approval_status=>gc_arn_appr_cc_status_pending.
**            APPEND ls_cc_team TO lt_cc_team.
**            READ TABLE lt_cc_team
**              ASSIGNING <ls_cc_team>
**              WITH KEY chgid        = <ls_cc_hdr>-chgid
**                       idno         = <ls_cc_hdr>-idno
**                       chg_category = <ls_cc_hdr>-chg_category
**                       chg_area     = <ls_cc_hdr>-chg_area
**                       team_code    = <ls_team_buffer>-team_code.
**          ENDIF.
**
**          IF <ls_cc_team> IS ASSIGNED.
**            IF <ls_cc_team>-status EQ zif_arn_approval_status=>gc_arn_appr_cc_status_pending.
**              <ls_cc_team>-status = zif_arn_approval_status=>gc_arn_appr_cc_status_approved.
**              <ls_cc_team>-upd_by = sy-uname.
**              CONCATENATE sy-datum sy-uzeit INTO <ls_cc_team>-upd_on.
**
**              CLEAR ls_arn_cc_log.
**              ls_arn_cc_log-chgid            = <ls_cc_team>-chgid.
**              ls_arn_cc_log-idno             = <ls_cc_team>-idno.
**              ls_arn_cc_log-chg_category     = <ls_cc_team>-chg_category.
**              ls_arn_cc_log-chg_area         = <ls_cc_team>-chg_area.
**              ls_arn_cc_log-team_code        = <ls_cc_team>-team_code.
**              ls_arn_cc_log-approval_result  = zif_arn_approval_status=>gc_arn_appr_cc_status_approved.
**              ls_arn_cc_log-reject_reason    = space.
**              ls_arn_cc_log-approval_datum   = sy-datum.
**              ls_arn_cc_log-approval_uzeit   = sy-uzeit.
**              ls_arn_cc_log-approval_uname   = sy-uname.
**              ls_arn_cc_log-approval_comment = text-aap.
**
**              IF iv_no_chgcat_upd IS INITIAL.  "++IR5120608 JKH 31.08.2016
**                INSERT zarn_cc_log FROM ls_arn_cc_log.
**              ENDIF.
**
**              lv_changes = abap_true.
**            ENDIF.
**          ENDIF.
**        ENDIF.
**      ENDIF.
**    ENDLOOP.
**  ENDLOOP.
**
*** Update any teams awaiting approval that are currently flagged as pending to not relevant
**  LOOP AT approvals INTO ls_approval
**    WHERE approval_status EQ zif_arn_approval_status=>gc_arn_appr_status_awaitappr
**    OR    approval_status EQ zif_arn_approval_status=>gc_arn_appr_status_awaitprior.
**    LOOP AT lt_cc_hdr ASSIGNING <ls_cc_hdr> WHERE chg_category EQ ls_approval-change_category.
**      READ TABLE lt_teams_buffer ASSIGNING <ls_team_buffer> WITH TABLE KEY team_code = ls_approval-approval_team.
**      IF sy-subrc EQ 0.
**        UNASSIGN <ls_cc_team>.
**        READ TABLE lt_cc_team
**          ASSIGNING <ls_cc_team>
**          WITH KEY chgid        = <ls_cc_hdr>-chgid
**                   idno         = <ls_cc_hdr>-idno
**                   chg_category = <ls_cc_hdr>-chg_category
**                   chg_area     = <ls_cc_hdr>-chg_area
**                   team_code    = <ls_team_buffer>-team_code.
**        IF sy-subrc EQ 0.
**          IF <ls_cc_team>-status EQ zif_arn_approval_status=>gc_arn_appr_cc_status_notrele.
**            <ls_cc_team>-status = zif_arn_approval_status=>gc_arn_appr_cc_status_pending.
**            <ls_cc_team>-upd_by = sy-uname.
**            CONCATENATE sy-datum sy-uzeit INTO <ls_cc_team>-upd_on.
**            lv_changes = abap_true.
**          ENDIF.
**        ENDIF.
**      ENDIF.
**    ENDLOOP.
**  ENDLOOP.
**
** if IV_NO_CHGCAT_UPD = 'X' then dont update change categories
**  IF iv_no_chgcat_upd IS INITIAL.  "++IR5120608 JKH 31.08.2016
**    IF lv_changes EQ abap_true.
**      CALL FUNCTION 'ZARN_CHANGE_CATEGORY_UPDATE'
**        EXPORTING
**          it_cc_hdr  = lt_cc_hdr
**          it_cc_det  = lt_cc_det
**          it_cc_team = lt_cc_team.
**      CALL FUNCTION 'ZARN_CHG_CAT_CHANGE_DOC'
**        EXPORTING
**          it_cc_hdr      = lt_cc_hdr
**          it_cc_team     = lt_cc_team
**          it_cc_team_old = lt_cc_team_old.
**    ENDIF.
**  ENDIF.


ENDFUNCTION.
