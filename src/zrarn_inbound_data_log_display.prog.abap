*&---------------------------------------------------------------------*
*& Report  ZRARN_INBOUND_DATA_LOG_DISPLAY
*&
*&---------------------------------------------------------------------*
*&---------------------------------------------------------------------*
* PROJECT          # FSA
* SPECIFICATION    # 3113
* DATE WRITTEN     # 20.11.2015
* SYSTEM           # DE1
* SAP VERSION      # ECC6.0
* TYPE             # Report
* AUTHOR           # C90001363
*-----------------------------------------------------------------------
* TITLE            # Log Display for Inbound Data
* PURPOSE          # SLG1 Log Display for Inbound Data
*                  #
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      #
*-----------------------------------------------------------------------
* CALLS TO         #
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

REPORT zrarn_inbound_data_log_display.

TABLES: balm, t160m, mean, lfa1.

CONSTANTS: lc_colon TYPE c     VALUE ':',
           lc_and   TYPE c     VALUE '&',
           lc_spras TYPE spras VALUE 'EN'.

TYPES: ty_r_fdesc  TYPE RANGE OF zsarn_inbound_data_log_display-fs_descr_s,
       ty_r_bcode  TYPE RANGE OF mean-ean11,
       ty_r_vend   TYPE RANGE OF lfa1-name1,
       ty_r_brand  TYPE RANGE OF zsarn_inbound_data_log_display-brandid,
       ty_r_sbrand TYPE RANGE OF zsarn_inbound_data_log_display-brandid.

DATA : ls_display  TYPE zsarn_inbound_data_log_display.


DATA: lv_sub_obj        TYPE balsubobj,
      lv_no_of_logs     TYPE i,
      lt_header_data    TYPE TABLE OF balhdr,
      ls_header_data    TYPE balhdr,
      lt_header_params  TYPE TABLE OF balhdrp,
      lt_messages       TYPE TABLE OF balm,
      ls_messages       TYPE balm,
      lt_message_params TYPE TABLE OF balmp,
      ls_message_params TYPE balmp,
      lt_contexts       TYPE TABLE OF balc,
      lt_t_exceptions   TYPE TABLE OF bal_s_exception,
      lt_alv            TYPE ztarn_inbound_data_log_display,
      ls_alv            TYPE zsarn_inbound_data_log_display,
      lv_text           TYPE char250_d,
      lv_fldname        TYPE char100,
      lv_exit           TYPE flag,
      lv_tabix          TYPE sy-tabix,

      lr_vend           TYPE ty_r_vend,
      ls_vend           TYPE LINE OF ty_r_vend,

      lv_desc           TYPE flag,
      lv_brand          TYPE flag,
      lv_sbrand         TYPE flag,
      lv_bcode          TYPE flag,
      lt_line           TYPE STANDARD TABLE OF string,
      ls_line           TYPE string.





FIELD-SYMBOLS : <field>          TYPE any,
                <ls_header_data> LIKE LINE OF lt_header_data.


INCLUDE zrarn_inbound_data_log_dispo01.
INCLUDE zrarn_inbound_data_log_dispi01.
INCLUDE zrarn_inbound_data_log_dispf01.


SELECTION-SCREEN BEGIN OF BLOCK b1 WITH FRAME TITLE text-001.

SELECTION-SCREEN BEGIN OF LINE.
PARAMETERS: r_inb  RADIOBUTTON GROUP rad DEFAULT 'X' MODIF ID rad USER-COMMAND inb.
SELECTION-SCREEN: COMMENT 4(12) text-003 FOR FIELD r_inb.  " Inbound Log

PARAMETERS: r_post RADIOBUTTON GROUP rad.
SELECTION-SCREEN: COMMENT 20(20) text-004 FOR FIELD r_post.  " Article Posting Log

PARAMETERS: r_proxy RADIOBUTTON GROUP rad.
SELECTION-SCREEN: COMMENT 44(10) text-006 FOR FIELD r_proxy.  " Proxy Log

SELECTION-SCREEN POSITION 55.
SELECTION-SCREEN: COMMENT 55(30) text-005 FOR FIELD p_object.  " Object Name (Application Code)
PARAMETERS: p_object   LIKE balobj-object OBLIGATORY DEFAULT 'ZARN'.
SELECTION-SCREEN END OF LINE.


SELECTION-SCREEN SKIP.

PARAMETERS: p_datefr TYPE baldate   DEFAULT sy-datum,
            p_timefr TYPE baltime   DEFAULT '000000',
            p_dateto TYPE baldate   DEFAULT sy-datum,
            p_timeto TYPE baltime   DEFAULT '240000', "sy-uzeit,
            p_log    TYPE balprobcl DEFAULT '4'.

SELECT-OPTIONS: s_msgty FOR t160m-msgtp,
                s_msgid FOR balm-msgid,
                s_msgno FOR balm-msgno.
SELECTION-SCREEN END OF BLOCK b1.


SELECTION-SCREEN BEGIN OF BLOCK b2 WITH FRAME TITLE text-002.
SELECT-OPTIONS: s_guid   FOR ls_display-guid                    MODIF ID inb,
                s_cre_on FOR ls_display-created_on NO-DISPLAY   MODIF ID inb,
                s_cre_at FOR ls_display-created_at NO-DISPLAY   MODIF ID inb,
                s_cre_by FOR ls_display-created_by              MODIF ID inb,
                s_fan_id FOR ls_display-fan_id                  MODIF ID inb,
                s_fdesc  FOR ls_display-fs_descr_s NO INTERVALS MODIF ID inb,
                s_mode   FOR ls_display-arn_mode                MODIF ID inb,
                s_code   FOR ls_display-code                    MODIF ID inb,
                s_matkl  FOR ls_display-matkl                   MODIF ID inb,
                s_vend   FOR lfa1-name1            NO INTERVALS MODIF ID inb,
                s_bcode  FOR mean-ean11            NO INTERVALS MODIF ID inb,
                s_brand  FOR ls_display-brandid    NO INTERVALS MODIF ID inb,
                s_sbrand FOR ls_display-brandid    NO INTERVALS MODIF ID inb.
SELECTION-SCREEN END OF BLOCK b2.


AT SELECTION-SCREEN OUTPUT.

  LOOP AT SCREEN.
    IF screen-name CS 'P_OBJECT'.
      screen-input = 0.
      MODIFY SCREEN.
    ENDIF.
  ENDLOOP.


  IF r_inb EQ abap_true.
    LOOP AT SCREEN.
      IF screen-group1 CS 'INB'.
        screen-active = 1.
        screen-invisible = 0.
        MODIFY SCREEN.
      ENDIF.
    ENDLOOP.

  ELSE.
    LOOP AT SCREEN.
      IF screen-group1 CS 'INB'.
        screen-active = 0.
        screen-invisible = 1.
        MODIFY SCREEN.
      ENDIF.
    ENDLOOP.

  ENDIF.


AT SELECTION-SCREEN.

  IF r_inb EQ abap_true.
    LOOP AT SCREEN.
      IF screen-group1 CS 'INB'.
        screen-active = 1.
        screen-invisible = 0.
        MODIFY SCREEN.
      ENDIF.
    ENDLOOP.

  ELSE.
    LOOP AT SCREEN.
      IF screen-group1 CS 'INB'.
        screen-active = 0.
        screen-invisible = 1.
        MODIFY SCREEN.
      ENDIF.
    ENDLOOP.

  ENDIF.


START-OF-SELECTION.

  lv_sub_obj = '*'.

  IF r_inb = abap_true.
    lv_sub_obj = 'ZARN_INBOUND'.
  ELSEIF r_post = abap_true.
    lv_sub_obj = 'ZARN_ART_POST'.
  ELSEIF r_proxy = abap_true.
    lv_sub_obj = 'ZARN_PROXY'.
  ENDIF.

* Get Log Details
  CALL FUNCTION 'APPL_LOG_READ_DB'
    EXPORTING
      object             = p_object
      subobject          = lv_sub_obj
      external_number    = ' '
      date_from          = p_datefr
      date_to            = p_dateto
      time_from          = p_timefr
      time_to            = p_timeto
      log_class          = p_log
      program_name       = '*'
      transaction_code   = '*'
      user_id            = ' '
      mode               = '+'
      put_into_memory    = ' '
    IMPORTING
      number_of_logs     = lv_no_of_logs
    TABLES
      header_data        = lt_header_data
      header_parameters  = lt_header_params
      messages           = lt_messages
      message_parameters = lt_message_params
      contexts           = lt_contexts
      t_exceptions       = lt_t_exceptions.



  lr_vend[] = s_vend[].

  LOOP AT lr_vend INTO ls_vend.
    lv_tabix = sy-tabix.

    TRANSLATE ls_vend-low TO UPPER CASE.

    IF ls_vend-option NE 'CP'.
      ls_vend-option = 'CP'.
      CONCATENATE '*' ls_vend-low '*' INTO ls_vend-low.
    ENDIF.

    IF ls_vend-high IS NOT INITIAL.
      CLEAR ls_vend-high.
    ENDIF.

    MODIFY lr_vend FROM ls_vend INDEX lv_tabix.
  ENDLOOP.



* Prepare ALV Data to be diaplayed
  REFRESH: lt_alv[].
  LOOP AT lt_messages INTO ls_messages
    WHERE msgty IN s_msgty
      AND msgid IN s_msgid
      AND msgno IN s_msgno.
    CLEAR: ls_alv.

    IF ls_messages-msgty = 'E'.
      ls_alv-msgty_icon = icon_led_red.
    ELSEIF ls_messages-msgty = 'I'.
      ls_alv-msgty_icon = icon_led_green.
    ELSEIF ls_messages-msgty = 'W'.
      ls_alv-msgty_icon = icon_led_yellow.
    ELSEIF ls_messages-msgty = 'S'.
      ls_alv-msgty_icon = icon_system_okay.
    ELSE.
      ls_alv-msgty_icon = icon_led_inactive.
    ENDIF.

    MESSAGE ID ls_messages-msgid
            TYPE ls_messages-msgty
            NUMBER ls_messages-msgno
            INTO ls_alv-message
            WITH ls_messages-msgv1 ls_messages-msgv2 ls_messages-msgv3 ls_messages-msgv4.

    ls_alv-lognumber = ls_messages-lognumber.


    READ TABLE lt_message_params[] INTO ls_message_params
       WITH KEY lognumber = ls_messages-lognumber
                msgnumber = ls_messages-msgnumber.
    IF sy-subrc = 0.
      CLEAR: ls_message_params, lv_exit, lv_desc, lv_brand, lv_sbrand, lv_bcode.
      LOOP AT lt_message_params[] INTO ls_message_params
        WHERE lognumber = ls_messages-lognumber
          AND msgnumber = ls_messages-msgnumber.

        CLEAR lv_fldname.
        CONCATENATE 'LS_ALV-' ls_message_params-parname INTO lv_fldname.
        ASSIGN (lv_fldname) TO <field>.



        CASE ls_message_params-parname.
          WHEN 'GUID'.
            IF ls_message_params-parvalue NOT IN s_guid[].
              lv_exit = abap_true.
              EXIT.
            ELSE.
              <field> = ls_message_params-parvalue.
            ENDIF.
          WHEN 'CREATED_ON'.
            IF ls_message_params-parvalue NOT IN s_cre_on[].
              lv_exit = abap_true.
              EXIT.
            ELSE.
              <field> = ls_message_params-parvalue.
            ENDIF.

          WHEN 'CREATED_AT'.
            IF ls_message_params-parvalue NOT IN s_cre_at[].
              lv_exit = abap_true.
              EXIT.
            ELSE.
              <field> = ls_message_params-parvalue.
            ENDIF.

          WHEN 'CREATED_BY'.
            IF ls_message_params-parvalue NOT IN s_cre_by[].
              lv_exit = abap_true.
              EXIT.
            ELSE.
              <field> = ls_message_params-parvalue.
            ENDIF.

          WHEN 'FAN_ID'.
            IF ls_message_params-parvalue NOT IN s_fan_id[].
              lv_exit = abap_true.
              EXIT.
            ELSE.
              <field> = ls_message_params-parvalue.
            ENDIF.

          WHEN 'ARN_MODE'.
            IF ls_message_params-parvalue NOT IN s_mode[].
              lv_exit = abap_true.
              EXIT.
            ELSE.
              <field> = ls_message_params-parvalue.
            ENDIF.

          WHEN 'CODE'.
            IF ls_message_params-parvalue NOT IN s_code[].
              lv_exit = abap_true.
              EXIT.
            ELSE.
              <field> = ls_message_params-parvalue.
            ENDIF.

          WHEN 'MATKL'.
            IF ls_message_params-parvalue NOT IN s_matkl[].
              lv_exit = abap_true.
              EXIT.
            ELSE.
              <field> = ls_message_params-parvalue.
            ENDIF.

          WHEN 'VEND_NAME'.
            IF ls_message_params-parvalue NOT IN lr_vend[].
              lv_exit = abap_true.
              EXIT.
            ELSE.
              <field> = ls_message_params-parvalue.
            ENDIF.

          WHEN 'IDNO'.
            <field> = ls_message_params-parvalue.

          WHEN 'VERSION'.
            <field> = ls_message_params-parvalue.

          WHEN 'FANID'.
            <field> = ls_message_params-parvalue.

          WHEN 'NAT_STATUS'.
            <field> = ls_message_params-parvalue.

          WHEN 'REG_STATUS'.
            <field> = ls_message_params-parvalue.

          WHEN 'SAPARTICLE'.
            <field> = ls_message_params-parvalue.

          WHEN 'BATCH_MODE'.
            <field> = ls_message_params-parvalue.

          WHEN OTHERS.
            <field> = ls_message_params-parvalue.

        ENDCASE.
      ENDLOOP.  " LOOP AT lt_message_params[] INTO ls_message_params

      IF lv_exit = abap_true.
        CONTINUE.
      ELSE.
        APPEND ls_alv TO lt_alv[].
      ENDIF.

    ELSE.

      IF r_proxy EQ abap_true OR r_post EQ abap_true.
        READ TABLE lt_header_data ASSIGNING <ls_header_data> WITH KEY lognumber = ls_messages-lognumber.
        IF sy-subrc EQ 0.
          ls_alv-created_at = <ls_header_data>-altime.
          ls_alv-created_on = <ls_header_data>-aldate.
          ls_alv-created_by = <ls_header_data>-aluser.
        ENDIF.

        APPEND ls_alv TO lt_alv[].
      ENDIF.

    ENDIF.

  ENDLOOP.  "   LOOP AT lt_messages INTO ls_messages


  IF r_inb EQ abap_true.
* Apply Filters on ALV
    PERFORM apply_filters_on_alv USING s_fdesc[]
                                       "s_vend[]
                                       s_bcode[]
                                       s_brand[]
                                       s_sbrand[]
                              CHANGING lt_alv[].

    SORT lt_alv[] BY created_on DESCENDING
                     created_at DESCENDING
                     fan_id     ASCENDING.
  ENDIF.

  IF r_proxy EQ abap_true OR r_post EQ abap_true.
    SORT lt_alv[] BY created_on DESCENDING
                     created_at DESCENDING.
  ENDIF.




END-OF-SELECTION.

* Call the screen 9000 for displaying ALV grid.
  CALL SCREEN 9000.
