FUNCTION zarn_mass_check_data.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(OBJECT) LIKE  MASSFUNC-OBJECT OPTIONAL
*"     REFERENCE(TABNAME) TYPE  TABNAME OPTIONAL
*"  TABLES
*"      FIELDS STRUCTURE  MASSDFIES OPTIONAL
*"      NDATA TYPE  STANDARD TABLE OPTIONAL
*"      ODATA TYPE  STANDARD TABLE OPTIONAL
*"----------------------------------------------------------------------

  DATA: lt_reg_hdr    TYPE ty_t_reg_hdr,
        ls_reg_hdr    TYPE zarn_reg_hdr,
        lt_act_fields TYPE ty_t_act_fields,
        ls_log_handle TYPE balloghndl,

        lv_sel_flag   TYPE xfeld,
        lv_flag_err   TYPE xfeld.


  FIELD-SYMBOLS: <user_command> TYPE sy-ucomm,
                 <tabledef>     LIKE tabledef,
                 <flag_save>    TYPE xfeld,
                 <flag_test>    TYPE xfeld.

* Validation to be managed by validation engine
  EXIT.

*get additional info from table def MAIN programme
*  ASSIGN (gc_tabledef) TO <tabledef>.
*  IF sy-subrc NE 0.
*    MESSAGE 'Internal application error' TYPE 'E'.
*    RETURN.
*  ENDIF.
*
*  CLEAR: lt_reg_hdr[], lt_act_fields[].
*  "refresh data in global memory
*  CASE tabname.
*    WHEN 'ZARN_REG_HDR'.
*      LOOP AT ndata[] INTO ls_reg_hdr.
*        CLEAR lv_sel_flag.
*        READ TABLE <tabledef>-sel_flags INDEX sy-tabix
*            INTO lv_sel_flag.
*        IF sy-subrc NE 0 OR lv_sel_flag NE 'X'.
*          "this line is not selected for update
*          "skip application checks
*          CONTINUE.
*        ENDIF.
*        INSERT ls_reg_hdr INTO TABLE lt_reg_hdr.
*      ENDLOOP.
*      lt_act_fields[] = <tabledef>-act_fields[].
*  ENDCASE.
*
*
*  "read user commnand from MAIN programme
*  ASSIGN (gc_user_command_ref) TO <user_command>.
*  IF sy-subrc NE 0.
*    MESSAGE 'Internal application error' TYPE 'E'.
*    RETURN.
*  ENDIF.
*
*
*  PERFORM init_log.
*
*  CLEAR lv_flag_err.
*
*
*  "checks performed only if value is changed
*  PERFORM reg_hdr_check USING fields[]
*                              <user_command>
*                              lt_reg_hdr[]
*                              lt_act_fields[]
*                     CHANGING lv_flag_err.
*
*
*
*
*  "if there are errors show the results in amodal log to user
*  IF lv_flag_err EQ 'X'.
*    ASSIGN (gc_flag_save) TO <flag_save>.
*    IF sy-subrc EQ 0.
*      CLEAR <flag_save>.
*      UNASSIGN <flag_save>.
*    ENDIF.
*    ASSIGN (gc_flag_test) TO <flag_test>.
*    IF sy-subrc EQ 0.
*      CLEAR <flag_test>.
*      UNASSIGN <flag_test>.
*    ENDIF.
*
*    CLEAR <user_command>.
*    PERFORM display_log.
*    MESSAGE 'Errors in data consistency checks. See application log for details'
*        TYPE 'W' DISPLAY LIKE 'E'.
*    LEAVE SCREEN.
*  ENDIF.
*
*  UNASSIGN <user_command>.
*  UNASSIGN <tabledef>.



ENDFUNCTION.
