*----------------------------------------------------------------------*
***INCLUDE ZIARN_GUI_I02.
*----------------------------------------------------------------------*


MODULE 203_validate_mstde INPUT.

  IF zarn_reg_hdr-mstae IS NOT INITIAL.
    IF gt_t141 IS INITIAL.
      SELECT mmsta FROM t141
      INTO CORRESPONDING FIELDS OF TABLE gt_t141
      WHERE zz_restricted_in_basic EQ space.
    ENDIF.

    READ TABLE gt_t141 TRANSPORTING NO FIELDS
    WITH TABLE KEY mmsta = zarn_reg_hdr-mstae.
    IF sy-subrc NE 0.
* IDNO &: X-site status '&' is not allowed in basic view.
      MESSAGE e114(zarena_msg) WITH zarn_reg_hdr-idno zarn_reg_hdr-mstae.
    ENDIF.
  ENDIF.

ENDMODULE.
MODULE 203_validate_mstdv INPUT.

  IF zarn_reg_hdr-mstav IS NOT INITIAL.
    IF gt_tvms IS INITIAL.
      SELECT vmsta FROM tvms
      INTO CORRESPONDING FIELDS OF TABLE gt_tvms
      WHERE zz_restricted_in_basic EQ space.
    ENDIF.

    READ TABLE gt_tvms TRANSPORTING NO FIELDS
    WITH TABLE KEY vmsta = zarn_reg_hdr-mstav.
    IF sy-subrc NE 0.
* IDNO &: X-DChain status '&' is not allowed in basic view.
      MESSAGE e115(zarena_msg) WITH zarn_reg_hdr-idno zarn_reg_hdr-mstav.
    ENDIF.
  ENDIF.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  203_DEFAULT_MSTDE  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE 203_default_mstde INPUT.

  IF zarn_reg_hdr-mstae IS NOT INITIAL AND zarn_reg_hdr-mstde IS INITIAL.
    zarn_reg_hdr-mstde = sy-datum.
  ENDIF.

ENDMODULE.
MODULE 203_default_mstdv INPUT.

  IF zarn_reg_hdr-mstav IS NOT INITIAL AND zarn_reg_hdr-mstdv IS INITIAL.
    zarn_reg_hdr-mstdv = sy-datum.
  ENDIF.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  DEFAULT_PLU_ARTICLE_111  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE default_plu_article_111 INPUT.

  DATA: ls_reg_ean_ui TYPE zsarn_reg_ean_ui.


  PERFORM default_plu_article_111 USING ls_reg_ean_ui.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  DEFAULT_NON_SAP_FIELDS_113  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE default_non_sap_fields_113 OUTPUT.

  IF gv_display IS INITIAL.
* Default Legacy UOMs
    PERFORM default_legacy_uoms_113.

* Default Host Product Indicator
    PERFORM default_host_prod_ind_113.
  ENDIF.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  CREATE_ZS_ZG_EAN_111  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE create_zs_zg_ean_111 INPUT.

  PERFORM create_zs_zg_ean_111.         "++CI18-124 JKH 06.07.2017


* INS Begin of Change CI18-124 JKH 29.06.2017
  LOOP AT gt_zarn_reg_banner[] ASSIGNING FIELD-SYMBOL(<ls_reg_bnr>)
    WHERE banner EQ '4000'
       OR banner EQ '5000'
       OR banner EQ '6000'.
    <ls_reg_bnr>-scagr = zarn_reg_hdr-scagr.
  ENDLOOP.
* INS End of Change CI18-124 JKH 29.06.2017

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Form  DEFAULT_PIR_FROM_UOM_SCR
*&---------------------------------------------------------------------*
* Default PIR data from UOM data dynamically from screen
*   " ++IR5061015 JKH 30.08.2016
*----------------------------------------------------------------------*
FORM default_pir_from_uom_scr USING fu_t_zarn_reg_uom TYPE ztarn_reg_uom_ui.

  DATA: ls_reg_data      TYPE zsarn_reg_data,
        ls_reg_data_def  TYPE zsarn_reg_data,
        ls_prod_data     TYPE zsarn_prod_data,
        ls_reg_pir_relif TYPE zsarn_reg_pir_ui,
        lt_pir_old       LIKE gt_zarn_reg_pir,

        lo_gui_load      TYPE REF TO zcl_arn_gui_load.


  lt_pir_old = gt_zarn_reg_pir[].



  CLEAR ls_reg_data.
  ls_reg_data-idno = gs_worklist-idno.

  MOVE-CORRESPONDING gt_zarn_reg_hdr[]     TO ls_reg_data-zarn_reg_hdr[].
  MOVE-CORRESPONDING gt_zarn_reg_banner[]  TO ls_reg_data-zarn_reg_banner[].
  MOVE-CORRESPONDING gt_zarn_reg_ean[]     TO ls_reg_data-zarn_reg_ean[].
  MOVE-CORRESPONDING gt_zarn_reg_pir[]     TO ls_reg_data-zarn_reg_pir[].
  MOVE-CORRESPONDING gt_zarn_reg_prfam[]   TO ls_reg_data-zarn_reg_prfam[].
  MOVE-CORRESPONDING gt_zarn_reg_txt[]     TO ls_reg_data-zarn_reg_txt[].
  MOVE-CORRESPONDING fu_t_zarn_reg_uom[]   TO ls_reg_data-zarn_reg_uom[].
  MOVE-CORRESPONDING gt_zarn_reg_std_ter[] TO ls_reg_data-zarn_reg_std_ter[].
  MOVE-CORRESPONDING gt_zarn_reg_rrp[]     TO ls_reg_data-zarn_reg_rrp[].

  CLEAR ls_prod_data.
  READ TABLE gt_prod_data INTO ls_prod_data
       WITH KEY idno    =  gs_worklist-idno
                version =  gs_worklist-version.

* Create Screen atrributes and defaults object
  TRY.
      CREATE OBJECT lo_gui_load.
    CATCH zcx_arn_gui_load_err INTO DATA(lo_exc).
  ENDTRY.
* Default PIR
  CLEAR gt_zarn_reg_pir[].
  CALL METHOD lo_gui_load->set_reg_pir_default
    EXPORTING
      is_reg_data     = ls_reg_data
      is_prod_data    = ls_prod_data
      it_reg_uom_ui   = fu_t_zarn_reg_uom[]
      iv_reg_only     = gv_display
    IMPORTING
      es_reg_data_def = ls_reg_data_def
      et_reg_pir_ui   = gt_zarn_reg_pir[].






* Default Regular Vendor for Basic screen
  CLEAR zarn_reg_pir-lifnr.

  CLEAR: ls_reg_pir_relif.
  READ TABLE gt_zarn_reg_pir[] INTO ls_reg_pir_relif
  WITH KEY idno = gs_worklist-idno
           relif = abap_true.
  IF sy-subrc EQ 0 AND
     ls_reg_pir_relif-lifnr IS NOT INITIAL AND
     ls_reg_pir_relif-lifnr NE 'MULTIPLE'.
    zarn_reg_pir-lifnr = ls_reg_pir_relif-lifnr.
  ENDIF.


ENDFORM.
*//START insert Charm 9000003206
MODULE set_bbdays_bypass_111 INPUT.

  PERFORM set_days_111.

ENDMODULE.
*\\END insert Charm 9000003206
*&---------------------------------------------------------------------*
*&      Module  114_CHECK_CHANGED  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE 114_check_changed INPUT.

ENDMODULE.
