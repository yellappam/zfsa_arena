class ZCL_ARN_SLA_PROCESSOR definition
  public
  create public .

public section.

  methods CONSTRUCTOR
    importing
      !IV_FACTORY_CALENDAR type WFCID optional .
  methods GET_SLA_CUSTOMISING
    returning
      value(RT_SLA_CUST) type ZTT_ARN_SLA_ST .
  methods CALCULATE_SLA
    importing
      !IV_KEY_DATE type DATUM
      !IV_REF_DATE type DATUM
      !IS_CALC_KEY type ZST_ARN_SLA_CALC_KEY
    returning
      value(RS_SLA_INFO) type ZST_ARN_SLA_DETAIL .
protected section.
private section.

  data MT_SLA_CUST type ZTT_ARN_SLA_ST .
  data MV_FACTORY_CALENDAR type WFCID .

  methods DETERMINE_SLA_RECORD
    importing
      !IS_CALC_KEY type ZST_ARN_SLA_CALC_KEY
    returning
      value(RV_DAYS) type ZARN_E_DURATION .
ENDCLASS.



CLASS ZCL_ARN_SLA_PROCESSOR IMPLEMENTATION.


  METHOD calculate_sla.
* Calculate the SLA days

    DATA: lt_dates TYPE TABLE OF rke_dat.

    DATA: lv_key_date          TYPE d,
          lv_current_fact_date TYPE d,
          lv_current_date      TYPE d,
          lv_from              TYPE d,
          lv_to                TYPE d,
          lv_sla_breach        TYPE abap_bool,
          lv_rc                TYPE i.

* Init dates
    lv_key_date = iv_key_date - 1." + 1.
*    lv_current_date = sy-datum.

* Get the current factory date for today(if run on holiday/w'end)
    CALL FUNCTION 'DATE_CONVERT_TO_FACTORYDATE'
      EXPORTING
        date                         = iv_ref_date
        factory_calendar_id          = me->mv_factory_calendar
      IMPORTING
        date                         = lv_current_fact_date
      EXCEPTIONS
        calendar_buffer_not_loadable = 1
        correct_option_invalid       = 2
        date_after_range             = 3
        date_before_range            = 4
        date_invalid                 = 5
        factory_calendar_not_found   = 6
        OTHERS                       = 7.
    IF sy-subrc <> 0.
      RETURN.
    ENDIF.

* Get the SLA days based on access sequence
    rs_sla_info-sla_target_days =  me->determine_sla_record( is_calc_key ).
    IF rs_sla_info-sla_target_days IS INITIAL.
      RETURN.
    ENDIF.

* Determine the SLA date. From key date
    CALL FUNCTION 'FKK_ADD_WORKINGDAY'
      EXPORTING
        i_date      = lv_key_date
        i_days      = CONV i( rs_sla_info-sla_target_days )
        i_calendar1 = me->mv_factory_calendar
      IMPORTING
        e_date      = rs_sla_info-sla_date
        e_return    = lv_rc.
    IF lv_rc <> 0.
      RETURN.
    ENDIF.

* SLA exceeded?
    IF lv_current_fact_date > rs_sla_info-sla_date.
      lv_sla_breach = abap_true.
      lv_from = rs_sla_info-sla_date.
      lv_to = lv_current_fact_date.
* SLA not exceeded
    ELSE.
      lv_from = lv_current_fact_date.
      lv_to = rs_sla_info-sla_date.
    ENDIF.

*    IF lv_from <> lv_to. "Leave inclusive
* Determine the workday difference between the two dates
    CALL FUNCTION 'RKE_SELECT_FACTDAYS_FOR_PERIOD'
      EXPORTING
        i_datab               = lv_from
        i_datbi               = lv_to
        i_factid              = me->mv_factory_calendar
      TABLES
        eth_dats              = lt_dates
      EXCEPTIONS
        date_conversion_error = 1
        OTHERS                = 2.
    IF sy-subrc = 0.
      rs_sla_info-sla_rem_days = lines( lt_dates ) - 1.
* Inverse if the SLA is a breach
      IF lv_sla_breach = abap_true.
        rs_sla_info-sla_rem_days = rs_sla_info-sla_rem_days * -1.
      ENDIF.
    ENDIF.

  ENDMETHOD.


  METHOD constructor.

* Load the SLA customising
    me->get_sla_customising( ).

* Get the factory calendar if not supplied
    IF iv_factory_calendar IS INITIAL.
      TRY.
          me->mv_factory_calendar = zcl_arn_sla_services=>get_fact_cal( ).
        CATCH zcx_arn_param_err ##NO_HANDLER.
      ENDTRY.
    ELSE.
      me->mv_factory_calendar = iv_factory_calendar.
    ENDIF.


  ENDMETHOD.


  METHOD determine_sla_record.
* Determine the SLA record
* 1: Match full key
* 2: Match full key with chg_category = blank

    TRY.
        rv_days =
            me->mt_sla_cust[ initiation = is_calc_key-initiation  process_type = is_calc_key-process_type chg_category = is_calc_key-chg_category team_code = is_calc_key-team_code ]-duration .
      CATCH cx_sy_itab_line_not_found.
        rv_days = VALUE #(
            me->mt_sla_cust[ initiation = is_calc_key-initiation process_type = is_calc_key-process_type chg_category = space team_code = is_calc_key-team_code ]-duration OPTIONAL ) .
    ENDTRY.

  ENDMETHOD.


  METHOD get_sla_customising.
* Return SLA customising

    IF me->mt_sla_cust IS INITIAL.
      SELECT *
        FROM zarn_sla
        INTO TABLE me->mt_sla_cust.
    ENDIF.

    IF rt_sla_cust IS REQUESTED.
      rt_sla_cust = me->mt_sla_cust.
    ENDIF.

  ENDMETHOD.
ENDCLASS.
