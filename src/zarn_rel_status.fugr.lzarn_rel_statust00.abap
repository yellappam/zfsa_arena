*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 23.02.2016 at 08:13:04 by user C90001448
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_REL_STATUST................................*
DATA:  BEGIN OF STATUS_ZARN_REL_STATUST              .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_REL_STATUST              .
CONTROLS: TCTRL_ZARN_REL_STATUST
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_REL_STATUST              .
TABLES: ZARN_REL_STATUST               .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
