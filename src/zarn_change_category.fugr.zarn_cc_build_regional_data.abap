FUNCTION zarn_cc_build_regional_data.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(IT_TABLE_MASTR) TYPE  ZTARN_TABLE_MASTR
*"     REFERENCE(IS_REG_DATA_DB) TYPE  ZSARN_REG_DATA
*"     REFERENCE(IS_REG_DATA_N) TYPE  ZSARN_REG_DATA
*"     REFERENCE(IV_CHGID) TYPE  ZARN_CHG_ID
*"     REFERENCE(IV_CHANGE_NUMBER) TYPE  CDCHANGENR
*"  EXPORTING
*"     REFERENCE(ET_CC_HDR) TYPE  ZTARN_CC_HDR
*"     REFERENCE(ET_CC_DET) TYPE  ZTARN_CC_DET
*"----------------------------------------------------------------------
* DATE             # 30.06.2020
* CHANGE No.       # SSM-1 NW Range Policy ZARN_GUI
* DESCRIPTION      # When all records are deleted, CC table was not populated.
*                  # This is fixed
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
  DATA: ls_table_mastr TYPE zarn_table_mastr,
        ls_reg_data_n  TYPE zsarn_reg_data,
        ls_reg_data_db TYPE zsarn_reg_data,
        lv_itabname    TYPE char50,
        lt_cc_hdr      TYPE ztarn_cc_hdr,
        lt_cc_det      TYPE ztarn_cc_det.

  FIELD-SYMBOLS: <table_db> TYPE table,
                 <table_n>  TYPE table.


  CLEAR: et_cc_hdr[], et_cc_det[].

  ls_reg_data_n  = is_reg_data_n.
  ls_reg_data_db = is_reg_data_db.


  LOOP AT it_table_mastr INTO ls_table_mastr.


    IF <table_db>   IS ASSIGNED. UNASSIGN <table_db>.   ENDIF.
    IF <table_n>    IS ASSIGNED. UNASSIGN <table_n>.    ENDIF.

    CLEAR: lv_itabname.
    CONCATENATE 'LS_REG_DATA_DB-' ls_table_mastr-arena_table INTO lv_itabname.
    ASSIGN (lv_itabname) TO <table_db>.

    CLEAR: lv_itabname.
    CONCATENATE 'LS_REG_DATA_N-' ls_table_mastr-arena_table INTO lv_itabname.
    ASSIGN (lv_itabname) TO <table_n>.

    CLEAR: lt_cc_hdr[], lt_cc_det[].
    CALL FUNCTION 'ZARN_CHANGE_CATEGORY_DATA'
      EXPORTING
        it_table_o    = <table_db>
        it_table_n    = <table_n>
        iv_tabname    = ls_table_mastr-arena_table
        iv_chgid      = iv_chgid
        iv_chgdoc     = iv_change_number
        is_reg_data_n = ls_reg_data_n
        is_reg_data_o = ls_reg_data_db        " ++ONED-217 JKH 23.11.2016
      IMPORTING
        et_cc_hdr     = lt_cc_hdr[]
        et_cc_det     = lt_cc_det[]
      EXCEPTIONS
        input_missing = 1
        input_invalid = 1
        error         = 3
        OTHERS        = 4.
    IF sy-subrc = 0.
      APPEND LINES OF lt_cc_hdr[]  TO et_cc_hdr[].
      APPEND LINES OF lt_cc_det[]  TO et_cc_det[].
    ENDIF.

  ENDLOOP.


  SORT et_cc_hdr[] BY chgid idno chg_category chg_area.
  DELETE ADJACENT DUPLICATES FROM et_cc_hdr[] COMPARING chgid idno chg_category chg_area.

ENDFUNCTION.
