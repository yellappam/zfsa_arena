*---------------------------------------------------------------------*
*    program for:   TABLEFRAME_ZARN_NUTR_BQTY_T
*   generation date: 12.05.2016 at 16:32:50 by user C90001363
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
FUNCTION TABLEFRAME_ZARN_NUTR_BQTY_T   .

  PERFORM TABLEFRAME TABLES X_HEADER X_NAMTAB DBA_SELLIST DPL_SELLIST
                            EXCL_CUA_FUNCT
                     USING  CORR_NUMBER VIEW_ACTION VIEW_NAME.

ENDFUNCTION.
