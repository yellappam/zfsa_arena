*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 24.05.2016 at 11:32:34 by user C90001363
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_UOM_CAT....................................*
DATA:  BEGIN OF STATUS_ZARN_UOM_CAT                  .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_UOM_CAT                  .
CONTROLS: TCTRL_ZARN_UOM_CAT
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_UOM_CAT                  .
TABLES: ZARN_UOM_CAT                   .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
