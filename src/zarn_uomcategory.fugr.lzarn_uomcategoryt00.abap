*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 24.05.2016 at 11:25:42 by user C90001363
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_UOMCATEGORY................................*
DATA:  BEGIN OF STATUS_ZARN_UOMCATEGORY              .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_UOMCATEGORY              .
CONTROLS: TCTRL_ZARN_UOMCATEGORY
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_UOMCATEGORY              .
TABLES: ZARN_UOMCATEGORY               .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
