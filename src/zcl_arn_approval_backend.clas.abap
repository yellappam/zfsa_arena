class ZCL_ARN_APPROVAL_BACKEND definition
  public
  final
  create public .

public section.

  types:
    ty_t_approval_db_hashed TYPE HASHED TABLE OF zarn_approval
         WITH UNIQUE KEY idno
                         approval_area
                         chg_category
                         team_code .

  constants:
    BEGIN OF gc_stat_reas,
        notif_sent   TYPE zarn_e_appr_status_reason VALUE '21',
        reset        TYPE zarn_e_appr_status_reason VALUE '99' ##NO_TEXT,
        autoapproved TYPE zarn_e_appr_status_reason VALUE '51' ##NO_TEXT,
        national_rej type zarn_e_appr_status_reason VALUE '31',
      END OF gc_stat_reas .
  constants:
    BEGIN OF gc_stat,
        appr_in_queue TYPE zarn_e_appr_status VALUE 'AQ' ##NO_TEXT,
        appr_ready    TYPE zarn_e_appr_status VALUE 'AR' ##NO_TEXT,
        approved      TYPE zarn_e_appr_status VALUE 'AP' ##NO_TEXT,
        rejected      TYPE zarn_e_appr_status VALUE 'RJ' ##NO_TEXT,
        posted        TYPE zarn_e_appr_status VALUE 'PO' ##NO_TEXT,
      END OF gc_stat .
  constants:
    begin of GC_NOTIF_STAT,
              sent type zarn_e_notif_status value 'S',
             end of gc_notif_stat .
  constants GC_EVENT_INBOUND type ZARN_E_APPROVAL_EVENT value 'IN' ##NO_TEXT.
  constants GC_EVENT_UI type ZARN_E_APPROVAL_EVENT value 'UI' ##NO_TEXT.
  constants GC_APPR_AREA_NATIONAL type ZARN_DATA_LEVEL value 'NAT' ##NO_TEXT.
  constants GC_APPR_AREA_REGIONAL type ZARN_DATA_LEVEL value 'REG' ##NO_TEXT.
  constants GC_HDR_STATUS_WIP type ZARN_REG_STATUS value 'WIP' ##NO_TEXT.
  constants GC_HDR_STATUS_CREATED type ZARN_REG_STATUS value 'CRE' ##NO_TEXT.
  constants GC_HDR_STATUS_APPROVED type ZARN_REG_STATUS value 'APR' ##NO_TEXT.

  methods ACTION_APPROVE
    importing
      !IV_TEAM_CODE type ZARN_TEAM_CODE
      !IV_CHG_CATEGORY type ZARN_CHG_CATEGORY optional
    exporting
      !ET_ZARN_APPROVAL type ZARN_T_APPROVAL
      !ET_ZARN_APPR_HIST type ZARN_T_APPR_HIST
    raising
      ZCX_EXCEP_CLS .
  methods ACTION_CHANGE_COMMENT
    importing
      !IV_CHG_CATEGORY type ZARN_CHG_CATEGORY
      !IV_TEAM_CODE type ZARN_TEAM_CODE
      !IV_COMMENT type ZARN_E_APPR_COMMENT
    exporting
      !ET_ZARN_APPROVAL type ZARN_T_APPROVAL
      !ET_ZARN_APPR_HIST type ZARN_T_APPR_HIST
    raising
      ZCX_EXCEP_CLS .
  methods ACTION_REJECT
    importing
      !IV_TEAM_CODE type ZARN_TEAM_CODE
      !IV_CHG_CATEGORY type ZARN_CHG_CATEGORY
      !IV_COMMENT type ZARN_E_APPR_COMMENT
      !IV_STATUS_REASON type ZARN_E_APPR_STATUS_REASON
      !IV_APPROVAL_AREA type ZARN_E_APPR_AREA optional
    exporting
      !ET_ZARN_APPROVAL type ZARN_T_APPROVAL
      !ET_ZARN_APPR_HIST type ZARN_T_APPR_HIST
    raising
      ZCX_EXCEP_CLS
      ZCX_ARN_NOTIF_ERR .
  methods ACTION_RESET
    importing
      !IV_TEAM_CODE type ZARN_TEAM_CODE
      !IV_CHG_CATEGORY type ZARN_CHG_CATEGORY
    exporting
      !ET_ZARN_APPROVAL type ZARN_T_APPROVAL
      !ET_ZARN_APPR_HIST type ZARN_T_APPR_HIST
    raising
      ZCX_EXCEP_CLS .
  methods BUILD_APPROVALS_FROM_CHG_BULK
    importing
      !IT_ZARN_CC_HDR type ZTARN_CC_HDR
      !IT_IDNO_VAL_ERR type ZTARN_IDNO_KEY optional
    exporting
      !ET_ZARN_APPROVAL type ZARN_T_APPROVAL
      !ET_ZARN_APPROVAL_CH type ZARN_T_APPROVAL_CH
      !ET_ZARN_APPR_HIST type ZARN_T_APPR_HIST
      !ET_ZARN_APPROVAL_CH_DEL type ZARN_T_APPROVAL_CH
      !ET_AFTER_APPR_ACTIONS type ZARN_T_AFTER_APPR_ACTION .
  methods BUILD_APPROVALS_FROM_CHG_SINGL
    importing
      !IV_IDNO type ZARN_IDNO
      !IV_NAT_VERSION type ZARN_E_APPR_NAT_VERS
      !IT_ZARN_CC_HDR type ZTARN_CC_HDR
      !IV_CONTAINS_VAL_ERRORS type XFELD default ''
    exporting
      !ET_ZARN_APPROVAL type ZARN_T_APPROVAL
      !ET_ZARN_APPROVAL_CH type ZARN_T_APPROVAL_CH
      !ET_ZARN_APPR_HIST type ZARN_T_APPR_HIST
      !ET_ZARN_APPROVAL_CH_DEL type ZARN_T_APPROVAL_CH
      !ET_AFTER_APPR_ACTIONS type ZARN_T_AFTER_APPR_ACTION
    raising
      ZCX_EXCEP_CLS .
  methods CALL_AFTER_APPR_ACTIONS
    importing
      !IT_AFTER_APPR_ACTION type ZARN_T_AFTER_APPR_ACTION .
  methods CHANGE_APPR_FROM_ARTPOST_BULK
    importing
      !IT_IDNO type ZTARN_IDNO_KEY
    exporting
      !ET_ZARN_APPROVAL_CH_DEL type ZARN_T_APPROVAL_CH
      !ET_ZARN_APPROVAL type ZARN_T_APPROVAL
      !ET_ZARN_APPR_HIST type ZARN_T_APPR_HIST .
  methods CONSTRUCTOR
    importing
      !IV_APPROVAL_EVENT type ZARN_E_APPROVAL_EVENT optional
      !IV_IDNO type ZARN_IDNO optional
      !IV_LOAD_DB_APPROVALS type XFELD default ' ' .
  methods DEQUEUE_APPR
    importing
      !IV_MODE type ENQMODE default 'E'
      !IV_IDNO type ZARN_IDNO optional
    preferred parameter IV_MODE .
  methods ENQUEUE_APPR_IDNO_OR_WAIT
    importing
      !IV_IDNO type ZARN_IDNO optional
      !IV_MODE type ENQMODE default 'E'
      !IV_DEQUEUE_ON_COMMIT type XFELD default 'X'
    preferred parameter IV_MODE
    raising
      ZCX_EXCEP_CLS .
  methods GET_APPROVALS
    returning
      value(RT_APPROVALS) type ZARN_T_APPROVAL .
  methods GET_APPROVALS_CHANGE_IDS
    importing
      !IV_CHG_CATEGORY type ZARN_CHG_CATEGORY optional
    exporting
      !ET_CC_DET type ZTARN_CC_DET
      !ET_CC_HDR type ZARN_T_CC_HDR_SORTED .
  methods GET_APPROVAL_RECORD
    importing
      !IV_CHG_CATEGORY type ZARN_CHG_CATEGORY
      !IV_TEAM_CODE type ZARN_TEAM_CODE
    returning
      value(RS_APPROVAL) type ZARN_APPROVAL .
  methods GET_CONF_TEAMS
    importing
      !IV_DISPLAY_ONLY type ABAP_BOOL default ' '
    returning
      value(RT_TEAMS) type ZARN_T_TEAMS .
  methods GET_HEADER_STATUS
    importing
      !IV_APPROVAL_AREA type ZARN_E_APPR_AREA
    returning
      value(RV_STATUS) type ZARN_REG_STATUS .
  class-methods GET_PENDING_APPROVALS
    importing
      !IT_IDNO type ZTARN_IDNO_RANGE optional
      !IT_APPR_UPDATED_ON type ZARN_T_APPR_UPD_TMSTMP optional
    returning
      value(RT_PENDING_APPROVALS) type ZARN_T_APPROVAL .
  methods GET_PREVIOUS_TEAM
    importing
      !IV_TEAM_CODE type ZARN_TEAM_CODE
    returning
      value(RV_PREV_TEAM_CODE) type ZARN_TEAM_CODE .
  methods GET_TEAM_READY_FOR_APPROVAL
    returning
      value(RV_TEAM_CODE) type ZARN_TEAM_CODE .
  methods HAS_TEAM_STARTED_APPROVING
    importing
      !IV_TEAM_CODE type ZARN_TEAM_CODE
    returning
      value(RV_STARTED_APPROVING) type XFELD .
  methods IS_AUTHORISED_FOR_TEAM
    importing
      !IV_TEAM_CODE type ZARN_TEAM_CODE
    returning
      value(RV_AUTHORISED) type ABAP_BOOL .
  methods IS_FULLY_APPROVED
    importing
      !IV_APPROVAL_AREA type ZARN_E_APPR_AREA optional
      !IT_APPROVAL type ZARN_T_APPROVAL optional
      !IV_NO_RECORDS_AS_APPROVED type ABAP_BOOL default ''
    preferred parameter IV_APPROVAL_AREA
    returning
      value(RV_FULLY_APPROVED) type ABAP_BOOL .
  methods IS_REJECT_NOTIF_REQUIRED
    importing
      !IV_APPROVAL_AREA type ZARN_E_APPR_AREA
      !IV_STATUS_REASON type ZARN_E_APPR_STATUS_REASON
    returning
      value(RV_REQUIRED) type ABAP_BOOL .
  methods LOAD_RECENT_APPROVAL_DATA .
  methods NOTIFY_REJECT
    importing
      !IV_IDNO type ZARN_IDNO
      !IV_REJECT_COMMENT type STRING
      !IT_APPROVALS type ZARN_T_APPROVAL
    raising
      ZCX_ARN_NOTIF_ERR .
  methods POST_APPROVALS
    importing
      !IT_APPROVAL type ZARN_T_APPROVAL
      !IT_APPROVAL_CH type ZARN_T_APPROVAL_CH optional
      !IT_APPR_HIST type ZARN_T_APPR_HIST
      !IT_APPROVAL_CH_DEL type ZARN_T_APPROVAL_CH optional .
  methods SET_APPROVAL_AREA
    importing
      !IV_APPROVAL_AREA type ZARN_E_APPR_AREA .
  methods UPDATE_NOTIFICATION_STATUS
    importing
      !IT_APPROVALS type ZARN_T_APPROVAL
      !IV_TARGET_STATUS type ZARN_E_NOTIF_STATUS default GC_NOTIF_STAT-SENT .
protected section.
private section.

  data MV_IDNO type ZARN_IDNO .
  data MV_NAT_VERSION type ZARN_E_APPR_NAT_VERS .
  class-data MV_TEMP_ACTIVE type CHAR1 .
  class-data MT_CONF_MATRIX type ZARN_REL_MATRIX_TAB .
  class-data MT_CONF_TEAMS type ZARN_T_TEAMS .
  data MV_APPROVAL_EVENT type ZARN_E_APPROVAL_EVENT .
  data MT_APPROVALS type ZARN_T_APPROVAL .
  data MV_APPR_AREA type ZARN_E_APPR_AREA .
  constants:
    BEGIN OF gc_priv,
      process_change            TYPE zarn_process_type        VALUE 'CHG' ##NO_TEXT,
      process_new               TYPE zarn_process_type        VALUE 'NEW' ##NO_TEXT,
      change_area_national      TYPE zarn_chg_area            VALUE 'N' ##NO_TEXT,
      change_area_regional      TYPE zarn_chg_area            VALUE 'R' ##NO_TEXT,
      after_action_stat_nat_apr TYPE zarn_e_after_appr_action VALUE '01' ##NO_TEXT,
      after_action_reg_enrich   TYPE zarn_e_after_appr_action VALUE '02' ##NO_TEXT,
      after_action_stat_reg_apr TYPE zarn_e_after_appr_action VALUE '03' ##NO_TEXT,
      sync                      TYPE zarn_e_action_sync       VALUE 'S' ##NO_TEXT,
      async                     TYPE zarn_e_action_sync       VALUE 'A' ##NO_TEXT,
    END OF gc_priv .
  data MS_IDNO_PROCESS_TYPE type ZARN_S_IDNO_PROCESS_TYPE .

  methods RUN_EVENT_BASED_AUTO_APPROVAL
    importing
      !IV_CONTAINS_VAL_ERRORS type XFELD
      !IV_PROCESS_TYPE type ZARN_PROCESS_TYPE
    changing
      !CT_APPROVAL type ZARN_T_APPROVAL .
  methods BUILD_ATTACHMENT
    exporting
      !EV_TYPE type SOODK-OBJTP
      !EV_SUBJECT type SOOD-OBJDES
      !EV_SIZE type SOOD-OBJLEN
      !ET_CONTENT type SOLIX_TAB .
  methods BUILD_CONTENT .
  methods BUILD_HISTORY_TABLE
    importing
      !IT_APPROVAL_OLD type ZARN_T_APPROVAL
    changing
      !CT_APPROVAL_NEW type ZARN_T_APPROVAL
    returning
      value(RT_APPROVAL_HIST) type ZARN_T_APPR_HIST .
  methods CONVERT_CHG_AREA_TO_APPR_AREA
    importing
      !IV_CHG_AREA type ZARN_CHG_AREA
    returning
      value(RV_APPR_AREA) type ZARN_E_APPR_AREA .
  methods DETERMINE_PROCESS_TYPE
    returning
      value(RV_PROCESS_TYPE) type ZARN_PROCESS_TYPE .
  methods DETERMINE_RECIPIENTS .
  methods DETERMINE_STAT_READY_FOR_APPR
    changing
      !CT_APPROVAL type ZARN_T_APPROVAL .
  methods DETERM_AFTER_APPR_ACTIONS
    importing
      !IT_APPROVAL_OLD type ZARN_T_APPROVAL
      !IT_APPROVAL_NEW type ZARN_T_APPROVAL
    returning
      value(RT_AFTER_APPR_ACTIONS) type ZARN_T_AFTER_APPR_ACTION .
  methods DETERM_AUTOAPPR_PARAMETERS
    importing
      !IV_CONTAINS_VAL_ERRORS type XFELD
    returning
      value(RS_AUTOAPPR_PARAMS) type ZARN_S_AUTOAPPR_PARAMS .
  methods GET_APPROVALS_DB
    returning
      value(RT_APPROVAL_DB) type ZARN_T_APPROVAL .
  methods INITIALIZE_APPROVALS_STATUS
    importing
      !IV_PROCESS_TYPE type ZARN_PROCESS_TYPE
    exporting
      !ET_APPROVAL_CH_DEL type ZARN_T_APPROVAL_CH
    changing
      !CT_APPROVAL type ZARN_T_APPROVAL .
  methods LOAD_APPROVAL_CONFIG .
  methods MODIFY_APPROVAL
    importing
      !IS_MATRIX type ZARN_REL_MATRIX
    changing
      !CT_APPROVAL type ZARN_T_APPROVAL
    returning
      value(RV_GUID) type ZARN_E_APPR_GUID .
  methods RUN_AUTOMATIC_STATUS_DETERM
    importing
      !IV_CONTAINS_VAL_ERRORS type XFELD optional
    changing
      !CT_APPROVAL type ZARN_T_APPROVAL .
  methods SET_APPROVALS_POSTED
    importing
      !IV_APPROVAL_AREA type ZARN_E_APPR_AREA default ''
    exporting
      !ET_APPROVAL_CH_DEL type ZARN_T_APPROVAL_CH
    changing
      !CT_APPROVAL type ZARN_T_APPROVAL .
  methods SET_DATA_SINGLE
    importing
      !IV_IDNO type ZARN_IDNO
      !IV_NAT_VERSION type ZARN_E_APPR_NAT_VERS optional .
ENDCLASS.



CLASS ZCL_ARN_APPROVAL_BACKEND IMPLEMENTATION.


  METHOD action_approve.


    DATA: ls_approval    TYPE zarn_approval,
          lt_approval_db LIKE mt_approvals.

    enqueue_appr_idno_or_wait( iv_mode = zif_enq_mode_c=>promote_optimistic ).

    "keep old snapshot of data
    lt_approval_db[] = mt_approvals[].

    ls_approval-status = gc_stat-approved.

    IF iv_chg_category IS NOT INITIAL.
      "approving specific change category
      MODIFY mt_approvals FROM ls_approval
        TRANSPORTING status
        WHERE approval_area = mv_appr_area
          AND team_code = iv_team_code
          AND chg_category = iv_chg_category
          AND status    = gc_stat-appr_ready.

    ELSE.

      "approving everything for 1 team
      MODIFY mt_approvals FROM ls_approval
      TRANSPORTING status
      WHERE approval_area = mv_appr_area
        AND team_code = iv_team_code
        AND status    = gc_stat-appr_ready.

    ENDIF.


    "this will determine all statuses including autoapproval logic
    run_automatic_status_determ( CHANGING ct_approval = mt_approvals ).

    et_zarn_approval[] = mt_approvals[].

    "now build the history table
    et_zarn_appr_hist = build_history_table( EXPORTING it_approval_old = lt_approval_db
                                              CHANGING ct_approval_new = et_zarn_approval ).

    "Approve action should possibly generate "after approval action"
    "however now these actions are built in GUI so don't do it with "after actions"

*    et_after_appr_actions = determ_after_appr_actions(  EXPORTING it_approval_old = lt_approval_db
*                                                                  it_approval_new = et_zarn_approval ).

  ENDMETHOD.


  METHOD action_change_comment.

    DATA: ls_approval    TYPE zarn_approval,
          lt_approval_db LIKE mt_approvals.

    enqueue_appr_idno_or_wait( iv_mode = zif_enq_mode_c=>promote_optimistic ).

    "keep old snapshot of data
    lt_approval_db[] = mt_approvals[].

    ls_approval-status_comment  = iv_comment.

    "change comment
    MODIFY mt_approvals FROM ls_approval
      TRANSPORTING status_comment
      WHERE approval_area = mv_appr_area
        AND team_code = iv_team_code
        AND chg_category = iv_chg_category.

    et_zarn_approval[] = mt_approvals[].

    "now build the history table
    et_zarn_appr_hist = build_history_table( EXPORTING it_approval_old = lt_approval_db
                                             CHANGING ct_approval_new = et_zarn_approval ).


  ENDMETHOD.


  METHOD action_reject.

    DATA: lt_approvals_upd TYPE zarn_t_approval,
          lt_chg_category  TYPE RANGE OF zarn_chg_category.

    DATA: ls_approval    TYPE zarn_approval,
          lt_approval_db LIKE mt_approvals.


    enqueue_appr_idno_or_wait( iv_mode = zif_enq_mode_c=>promote_optimistic ).

    "keep old snapshot of data
    lt_approval_db[] = mt_approvals[].

    ls_approval-status          = gc_stat-rejected.
    ls_approval-status_comment  = iv_comment.
    ls_approval-status_reason   = iv_status_reason.

    IF iv_chg_category IS NOT INITIAL.
      "approving specific change category
      MODIFY mt_approvals FROM ls_approval
        TRANSPORTING status status_reason status_comment
        WHERE approval_area = mv_appr_area
          AND team_code = iv_team_code
          AND chg_category = iv_chg_category
          AND status    = gc_stat-appr_ready.

    ELSE.
      "approving everything for 1 team
      MODIFY mt_approvals FROM ls_approval
      TRANSPORTING status status_reason status_comment
      WHERE approval_area = mv_appr_area
        AND team_code = iv_team_code
        AND status    = gc_stat-appr_ready.

    ENDIF.

    "this will determine all statuses including autoapproval logic
    run_automatic_status_determ( CHANGING ct_approval = mt_approvals ).

    et_zarn_approval[] = mt_approvals[].

    "now build the history table
    et_zarn_appr_hist = build_history_table( EXPORTING it_approval_old = lt_approval_db
                                             CHANGING ct_approval_new = et_zarn_approval ).

* Send notification?
    IF me->is_reject_notif_required( iv_approval_area = iv_approval_area iv_status_reason = iv_status_reason ).

* Get the list of updated approvals by change category
      IF iv_chg_category IS NOT INITIAL.
        lt_chg_category =
          VALUE #(
            ( sign = 'I'
              option = 'EQ'
              low = iv_chg_category )
          ).
      ENDIF.

      LOOP AT me->mt_approvals ASSIGNING FIELD-SYMBOL(<ls_approval>)
        WHERE approval_area = mv_appr_area AND
              team_code = iv_team_code AND
              chg_category IN lt_chg_category AND
              status = gc_stat-rejected.
        APPEND <ls_approval> TO lt_approvals_upd.
      ENDLOOP.

* Notify
      me->notify_reject(
        EXPORTING
          iv_idno           = me->mv_idno
          iv_reject_comment = iv_comment
          it_approvals      = lt_approvals_upd
      ).

    ENDIF.

  ENDMETHOD.


  METHOD action_reset.

    DATA: ls_approval    TYPE zarn_approval,
          lt_approval_db LIKE mt_approvals.


    enqueue_appr_idno_or_wait( iv_mode = zif_enq_mode_c=>promote_optimistic ).

    "keep old snapshot of data
    lt_approval_db[] = mt_approvals[].

    ls_approval-status = gc_stat-appr_ready.
    ls_approval-status_reason = gc_stat_reas-reset.
    CLEAR ls_approval-status_comment.

    "reseting 1 category for 1 team
    MODIFY mt_approvals FROM ls_approval
       TRANSPORTING status status_reason status_comment
       WHERE approval_area  = mv_appr_area
         AND team_code      = iv_team_code
         AND chg_category   = iv_chg_category.

    "this will determine all statuses including autoapproval logic
    run_automatic_status_determ( CHANGING ct_approval = mt_approvals ).

    "tbd possibly do auto approvals - might require WHILE loop...

    et_zarn_approval[] = mt_approvals[].

    "now build the history table
    et_zarn_appr_hist = build_history_table( EXPORTING it_approval_old = lt_approval_db
                                              CHANGING ct_approval_new = et_zarn_approval ).


  ENDMETHOD.


  METHOD build_approvals_from_chg_bulk.

    "logic to build approval entries from change categories (ZARN_CC_HDR)
    "bulk - multiple IDNOs

    TYPES: BEGIN OF ty_s_idno,
             idno            TYPE zarn_idno,
             national_ver_id TYPE zarn_nat_version,
           END OF ty_s_idno.

    TYPES: ty_t_cc_hdr_sorted TYPE SORTED TABLE OF zarn_cc_hdr WITH NON-UNIQUE KEY idno.


    DATA: lt_idno                  TYPE STANDARD TABLE OF ty_s_idno,
          lt_zarn_cc_hdr           TYPE ty_t_cc_hdr_sorted,
          lt_cc_hdr_singl          LIKE it_zarn_cc_hdr,
          lt_approval_singl        LIKE et_zarn_approval,
          lt_approval_ch_singl     LIKE et_zarn_approval_ch,
          lt_appr_hist_singl       LIKE et_zarn_appr_hist,
          lt_approval_ch_del_singl LIKE et_zarn_approval_ch_del,
          lt_after_appr_act_singl  LIKE et_after_appr_actions,
          lv_val_err               TYPE xfeld.

    "ASSUMPTIONS FOR IT_ZARN_CC_HDR
    "1) will have NAT_VERSION populated
    "2) will not have multiple NAT_VERSION per 1 IDNO
    lt_idno = CORRESPONDING #(  it_zarn_cc_hdr ).
    SORT lt_idno BY idno national_ver_id.
    DELETE ADJACENT DUPLICATES FROM lt_idno COMPARING idno.

    CLEAR: lt_zarn_cc_hdr[], et_zarn_approval[], et_zarn_approval_ch[].
    INSERT LINES OF it_zarn_cc_hdr INTO TABLE lt_zarn_cc_hdr.

    LOOP AT lt_idno INTO DATA(ls_idno).

      IF line_exists( it_idno_val_err[ idno = ls_idno-idno ] ).
        lv_val_err = abap_true.
      ELSE.
        lv_val_err = abap_false.
      ENDIF.

      CLEAR: lt_cc_hdr_singl, lt_approval_singl,
             lt_approval_ch_singl, lt_approval_ch_del_singl,
             lt_appr_hist_singl, lt_after_appr_act_singl.

      lt_cc_hdr_singl = FILTER #( lt_zarn_cc_hdr WHERE idno = ls_idno-idno  ).

      TRY.
          build_approvals_from_chg_singl( EXPORTING iv_idno                 = ls_idno-idno
                                                    iv_nat_version          = ls_idno-national_ver_id
                                                    it_zarn_cc_hdr          = lt_cc_hdr_singl
                                                    iv_contains_val_errors  = lv_val_err
                                          IMPORTING et_zarn_approval        = lt_approval_singl
                                                    et_zarn_approval_ch     = lt_approval_ch_singl
                                                    et_zarn_approval_ch_del = lt_approval_ch_del_singl
                                                    et_zarn_appr_hist       = lt_appr_hist_singl
                                                    et_after_appr_actions   = lt_after_appr_act_singl ).

          APPEND LINES OF lt_approval_singl        TO et_zarn_approval.
          APPEND LINES OF lt_approval_ch_singl     TO et_zarn_approval_ch.
          APPEND LINES OF lt_approval_ch_del_singl TO et_zarn_approval_ch_del.
          APPEND LINES OF lt_appr_hist_singl       TO et_zarn_appr_hist.
          APPEND LINES OF lt_after_appr_act_singl  TO et_after_appr_actions.

        CATCH zcx_excep_cls.

      ENDTRY.

    ENDLOOP.

  ENDMETHOD.


  METHOD build_approvals_from_chg_singl.

    "logic to build approval entries from change categories (ZARN_CC_HDR)
    "always single IDNO inside ZARN_CC_HDR

    DATA: lt_approval    TYPE zarn_t_approval,
          ls_approval    TYPE zarn_approval,
          ls_approval_ch TYPE zarn_approval_ch.

    CLEAR : et_zarn_approval_ch[], et_zarn_appr_hist[],
            et_zarn_approval_ch_del[], et_after_appr_actions[].

    IF it_zarn_cc_hdr[] IS INITIAL.
      "in case that no new change ids are determined, don't do anything
      RETURN.
    ENDIF.

    "set attributes that will be used by other methods later
    set_data_single( iv_idno = iv_idno
                     iv_nat_version = iv_nat_version ).

    "enqueue IDNO for approvals with possible wait
    enqueue_appr_idno_or_wait( ).


    "first select what we currently have in ZARN_APPROVAL
    DATA(lt_approval_db) = get_approvals_db( ).
    lt_approval[] = lt_approval_db[].

    "now determine whether it is Create/Change scenario
    DATA(lv_process_type) = determine_process_type( ).


    "this will initialize status of existing approval records
    "and possibly determines obsolete Approval - Changes to be deleted
    "depends on process type (create / change) and event (PIM inbound / GUI change etc)
    initialize_approvals_status( EXPORTING iv_process_type    = lv_process_type
                                 IMPORTING et_approval_ch_del = et_zarn_approval_ch_del
                                  CHANGING ct_approval        = lt_approval ).

    LOOP AT it_zarn_cc_hdr INTO DATA(ls_cc_hdr).

      DATA(lv_approval_area) = convert_chg_area_to_appr_area( ls_cc_hdr-chg_area ).

      "check the config whether this change is relevant to
      "any of the teams for approving
      LOOP AT mt_conf_matrix ASSIGNING FIELD-SYMBOL(<ls_matrix>)
          WHERE data_level = lv_approval_area
            AND process_type = lv_process_type
            AND change_category = ls_cc_hdr-chg_category.

        "this will create / update approval record and returns its GUID
        ls_approval_ch-guid = modify_approval( EXPORTING is_matrix = <ls_matrix>
                                               CHANGING ct_approval = lt_approval  ).

        "now insert a new record to for Approval-Change
        "this table stores relationship between Approval and Changes to be Approved
        ls_approval_ch-chgid = ls_cc_hdr-chgid.
        ls_approval_ch-idno = iv_idno.

        APPEND ls_approval_ch TO et_zarn_approval_ch.
      ENDLOOP.
    ENDLOOP.


    "this will determine all status including autoapproval logic
    run_automatic_status_determ( EXPORTING iv_contains_val_errors = iv_contains_val_errors
                                 CHANGING ct_approval = lt_approval ).

    et_zarn_approval[] = lt_approval[].

    "last step - build the history table
    "and change update information
    et_zarn_appr_hist = build_history_table( EXPORTING it_approval_old = lt_approval_db
                                              CHANGING ct_approval_new = et_zarn_approval ).

    et_after_appr_actions = determ_after_appr_actions( EXPORTING it_approval_old = lt_approval_db
                                                                 it_approval_new = et_zarn_approval ).

  ENDMETHOD.


  METHOD build_attachment.

    DATA: lv_attachment TYPE string.

    CLEAR: ev_type, ev_size, ev_subject, et_content.

* Build the header for the attachment
    CONCATENATE
      'Article'
      'Base Unit GTIN'
      'Description'
      'Vendor Name'
      'Vendor GLN / Number'
      'Notes'
      INTO lv_attachment SEPARATED BY cl_abap_char_utilities=>horizontal_tab.

    CONCATENATE
     lv_attachment
     cl_abap_char_utilities=>cr_lf
     '123'
     cl_abap_char_utilities=>horizontal_tab
     'PAK'
     cl_abap_char_utilities=>horizontal_tab
     'An article number 123'
     cl_abap_char_utilities=>horizontal_tab
     'Shoes Ltd'
     cl_abap_char_utilities=>horizontal_tab
     '200'
     cl_abap_char_utilities=>horizontal_tab
     'Bad data'
     INTO lv_attachment.

* Get into a nice attachment format
    TRY.
        cl_bcs_convert=>string_to_solix(
          EXPORTING
            iv_string   = lv_attachment
          IMPORTING
            et_solix    = et_content
            ev_size     = ev_size
        ).
        ev_subject = 'reject_articles.xls'.
        ev_type = 'XLS'.
      CATCH cx_bcs ##NO_HANDLER.
    ENDTRY.


  ENDMETHOD.


  method BUILD_CONTENT.
  endmethod.


  METHOD build_history_table.

* historical records will be created only for
* "old records that has changed"
* also those records
* will get change information (changed by, changed timestamp) updated

    DATA ls_appr_hist TYPE zarn_appr_hist.

    CLEAR rt_approval_hist[].

    LOOP AT it_approval_old ASSIGNING FIELD-SYMBOL(<ls_old>).

      TRY.
          ASSIGN ct_approval_new[ guid = <ls_old>-guid ] TO FIELD-SYMBOL(<ls_new>).

          IF <ls_new>-data NE <ls_old>-data.
            "change happened => write history
            ls_appr_hist-guid = <ls_old>-guid.
            ls_appr_hist-updated_timestamp = <ls_old>-updated_timestamp.
            ls_appr_hist-data = <ls_old>-data.
            ls_appr_hist-updated_by = <ls_old>-updated_by.
            APPEND ls_appr_hist TO rt_approval_hist.

            <ls_new>-updated_by  = sy-uname.

            GET TIME.
*            CONVERT DATE sy-datum TIME sy-uzeit
*              INTO TIME STAMP
            <ls_new>-updated_timestamp = sy-datum && sy-uzeit.

          ENDIF.

        CATCH cx_sy_itab_line_not_found.
          "TBD error should not happen
      ENDTRY.
    ENDLOOP.
  ENDMETHOD.


  METHOD call_after_appr_actions.

    DATA lt_idno TYPE ztarn_reg_key.

    "call additional actions after approval happens
    LOOP AT it_after_appr_action INTO DATA(ls_after_appr_action).

      CASE ls_after_appr_action-action.

        WHEN gc_priv-after_action_stat_nat_apr.

          CASE ls_after_appr_action-sync_async.
            WHEN gc_priv-sync.
              CALL FUNCTION 'ZARN_NAT_STATUS_UPDATE'
                EXPORTING
                  iv_idno    = ls_after_appr_action-idno
                  iv_version = ls_after_appr_action-version
                  iv_status  = gc_hdr_status_approved.

            WHEN gc_priv-async.
              CALL FUNCTION 'ZARN_NAT_STATUS_UPDATE'
                IN BACKGROUND TASK AS SEPARATE UNIT
                EXPORTING
                  iv_idno    = ls_after_appr_action-idno
                  iv_version = ls_after_appr_action-version
                  iv_status  = gc_hdr_status_approved.

          ENDCASE.

        WHEN gc_priv-after_action_reg_enrich.

          CLEAR  lt_idno[].
          lt_idno = VALUE #( ( idno = ls_after_appr_action-idno )  ).

          CASE ls_after_appr_action-sync_async.
            WHEN gc_priv-sync.
              CALL FUNCTION 'ZARN_REG_DATA_ENRICH_UPDATE'
                EXPORTING
                  it_idno = lt_idno.


            WHEN gc_priv-async.
              CALL FUNCTION 'ZARN_REG_DATA_ENRICH_UPDATE'
                IN BACKGROUND TASK AS SEPARATE UNIT
                EXPORTING
                  it_idno = lt_idno.

          ENDCASE.

        WHEN gc_priv-after_action_stat_reg_apr.

          CASE ls_after_appr_action-sync_async.
            WHEN gc_priv-sync.
              CALL FUNCTION 'ZARN_REG_STATUS_UPDATE'
                EXPORTING
                  iv_idno   = ls_after_appr_action-idno
                  iv_status = gc_hdr_status_approved.

            WHEN gc_priv-async.
              CALL FUNCTION 'ZARN_REG_STATUS_UPDATE'
                IN BACKGROUND TASK AS SEPARATE UNIT
                EXPORTING
                  iv_idno   = ls_after_appr_action-idno
                  iv_status = gc_hdr_status_approved.

          ENDCASE.
      ENDCASE.

    ENDLOOP.

  ENDMETHOD.


  METHOD change_appr_from_artpost_bulk.

    DATA : lt_approval        TYPE zarn_t_approval,
           lt_approval_posted TYPE zarn_t_approval.

    LOOP AT it_idno INTO DATA(ls_idno).

      TRY.
          set_data_single( EXPORTING iv_idno = ls_idno-idno ).

          enqueue_appr_idno_or_wait( ).

          "get existing approval data from database
          DATA(lt_approval_old) = get_approvals_db( ).
          lt_approval[] = lt_approval_old.

          "set status to posted and delete all relations to change categories
          set_approvals_posted( IMPORTING et_approval_ch_del = DATA(lt_approval_ch_del)
                                CHANGING  ct_approval        = lt_approval ).

          lt_approval_posted[] = lt_approval.
          CLEAR lt_approval[].

          "build history table
          DATA(lt_appr_hist) = build_history_table( EXPORTING it_approval_old = lt_approval_old
                                                    CHANGING ct_approval_new  = lt_approval_posted ).
          APPEND LINES OF lt_approval_posted  TO et_zarn_approval.
          APPEND LINES OF lt_approval_ch_del TO et_zarn_approval_ch_del.
          APPEND LINES OF lt_appr_hist TO et_zarn_appr_hist.

        CATCH zcx_excep_cls.

      ENDTRY.
    ENDLOOP.

  ENDMETHOD.


  METHOD constructor.

    "approval event specifies
    "from where the approval logic is called
    "for example PIM inbound, AReNa GUI etc...

    mv_approval_event = iv_approval_event.

    mv_idno = iv_idno.

    load_approval_config( ).

    IF iv_load_db_approvals = abap_true.
      mt_approvals = get_approvals_db( ).
    ENDIF.



  ENDMETHOD.


  METHOD convert_chg_area_to_appr_area.

    CASE iv_chg_area.
      WHEN gc_priv-change_area_national.
        rv_appr_area = gc_appr_area_national.
      WHEN gc_priv-change_area_regional.
        rv_appr_area = gc_appr_area_regional.
      WHEN OTHERS.
        "TBD error - not defined
    ENDCASE.

  ENDMETHOD.


  METHOD dequeue_appr.

    DATA lv_idno TYPE zarn_idno.

    IF iv_idno IS INITIAL.
      lv_idno = mv_idno.
    ELSE.
      lv_idno = iv_idno.
    ENDIF.

    CALL FUNCTION 'DEQUEUE_EZARN_APPROVAL'
      EXPORTING
        idno                = lv_idno
        mode_zsarn_idno_key = iv_mode.

  ENDMETHOD.


  METHOD determine_process_type.

    "determine process type - new / change

    DATA: ls_ver_status TYPE zarn_ver_status.

    IF ms_idno_process_type-idno = mv_idno
      AND ms_idno_process_type-process_type IS NOT INITIAL.

      rv_process_type =  ms_idno_process_type-process_type.

    ELSE.

      "Is new if we do not have a SAP article
      rv_process_type = gc_priv-process_new.

      CLEAR ls_ver_status.
      SELECT SINGLE article_ver
        INTO @DATA(lv_article_version)
        FROM zarn_ver_status
        WHERE idno EQ @mv_idno.
      IF NOT lv_article_version IS INITIAL.
        rv_process_type = gc_priv-process_change.
      ENDIF.


      ms_idno_process_type-idno         = mv_idno.
      ms_idno_process_type-process_type = rv_process_type.

    ENDIF.

  ENDMETHOD.


  method DETERMINE_RECIPIENTS.
  endmethod.


  METHOD determine_stat_ready_for_appr.

    "check all AQ status and change to AR (approval ready)
    "those that belong to Team with "lowest" queue position

    "Rejected is treated as "higher priority"


    DATA: lv_open_appr_area TYPE zarn_e_appr_area,
          lv_open_appr_team TYPE zarn_team_code,
          ls_approval       TYPE zarn_approval.

    "first set everything which is currently "Approval Ready"
    "back to "approval in queue"

     ls_approval-status = gc_stat-appr_in_queue.

      MODIFY ct_approval FROM ls_approval
      TRANSPORTING status
      WHERE idno = mv_idno
        AND status = gc_stat-appr_ready.


    "need to distinguish whether national / regional is active

    IF line_exists( ct_approval[ approval_area = gc_appr_area_national
                                 status        = gc_stat-appr_in_queue ] )

      OR line_exists( ct_approval[ approval_area = gc_appr_area_national
                                 status        = gc_stat-rejected ] ).

      "if any national exists in queue or rejected,
      "approvals will be definitely open only at National Level
      lv_open_appr_area = gc_appr_area_national.

    ELSE.

      "otherwise approvals will be open at regional level
      lv_open_appr_area = gc_appr_area_regional.

    ENDIF.

    SORT mt_conf_teams BY display_order ASCENDING.

    "search for "lowest" team with rejected or approvals in queue
    LOOP AT mt_conf_teams INTO DATA(ls_team).

      "rejected has higher priority
      IF line_exists( ct_approval[ team_code     = ls_team-team_code
                                   approval_area = lv_open_appr_area
                                   status        = gc_stat-rejected ] ).

        lv_open_appr_team = ls_team-team_code.
        EXIT.

      ENDIF.

      IF line_exists( ct_approval[ team_code     = ls_team-team_code
                                   approval_area = lv_open_appr_area
                                   status        = gc_stat-appr_in_queue ] ).

        lv_open_appr_team = ls_team-team_code.
        EXIT.

      ENDIF.
    ENDLOOP.
    IF sy-subrc NE 0.
      "TBD error - shouldn't happen
      "something is awaiting for approval but not found for any of the configured teams
    ENDIF.

    "Now set all records for this team, data level and status AQ
    " to AR (approval ready)
    ls_approval-status = gc_stat-appr_ready.

    MODIFY ct_approval FROM ls_approval TRANSPORTING status
      WHERE team_code     = lv_open_appr_team
        AND approval_area = lv_open_appr_area
        AND status        = gc_stat-appr_in_queue.



  ENDMETHOD.


  METHOD determ_after_appr_actions.

    DATA: ls_after_appr_action  TYPE zarn_s_after_appr_action,
          lv_new_fully_approved TYPE xfeld.

    "determine after approval actions
    CASE mv_approval_event.

      WHEN gc_event_inbound.

        "in case of inbound, call the after actions asynchronously
        ls_after_appr_action-sync_async = gc_priv-async.

        "is eveyrything approved in national?
        IF is_fully_approved(  it_approval = it_approval_new[]
                               iv_approval_area = gc_appr_area_national ).
         " AND iv_data_change = abap_true.

          "if national is fully approved, change national status to APR and do automatic enrichment

          ls_after_appr_action-idno     = mv_idno.
          ls_after_appr_action-version  = mv_nat_version.
          ls_after_appr_action-action   = gc_priv-after_action_stat_nat_apr.
          APPEND ls_after_appr_action TO rt_after_appr_actions.

          ls_after_appr_action-idno     = mv_idno.
          ls_after_appr_action-version  = mv_nat_version.
          ls_after_appr_action-action   = gc_priv-after_action_reg_enrich.
          APPEND ls_after_appr_action TO rt_after_appr_actions.

        ENDIF.

      WHEN OTHERS.
        "in case of other events (from GUI / automatic enrichment etc.), call synchronously
        ls_after_appr_action-sync_async = gc_priv-sync.

        "other than inbound event - possibly regional status can change to APR
        "is eveyrything approved in regional?
        IF  is_fully_approved( it_approval = it_approval_new[]
                               iv_approval_area = gc_appr_area_regional ).
          "AND iv_data_change = abap_true.


          ls_after_appr_action-idno   = mv_idno.
          ls_after_appr_action-action = gc_priv-after_action_stat_reg_apr.
          APPEND ls_after_appr_action TO rt_after_appr_actions.

        ENDIF.
    ENDCASE.



  ENDMETHOD.


  METHOD determ_autoappr_parameters.

    CLEAR rs_autoappr_params.


    CASE mv_approval_event.
      WHEN gc_event_inbound.
        "in case of inbound run autoapprovals only in national area
        rs_autoappr_params-area = gc_appr_area_national.

      WHEN OTHERS.
        "otherwise, run autoapprovals only if no validation errors found
        IF iv_contains_val_errors = abap_true.
          rs_autoappr_params-skip = abap_true.
        ENDIF.
    ENDCASE.

    "temp_test_autoapprovals( ).


  ENDMETHOD.


  METHOD enqueue_appr_idno_or_wait.

    DATA : lv_success TYPE xfeld,
           lv_idno    TYPE zarn_idno,
           lv_mode    TYPE enqmode,
           lv_subrc   TYPE sy-subrc.

    IF iv_idno IS INITIAL.
      lv_idno = mv_idno.
    ELSE.
      lv_idno = iv_idno.
    ENDIF.

    lv_mode = iv_mode.

    DO 20 TIMES.
      "scope 2 = will be released after COMMIT WORK + update function module finished
      "mode default E = exclusive, but depends on calling application (can be O - optimistic as well)

      IF lv_mode = zif_enq_mode_c=>exclusive.
        "if called with mode E, we need to do sequence O + R
        lv_mode = zif_enq_mode_c=>optimistic.

        CALL FUNCTION 'ENQUEUE_EZARN_APPROVAL'
          EXPORTING
            idno                = lv_idno
            mode_zsarn_idno_key = lv_mode
          EXCEPTIONS
            foreign_lock        = 1
            system_failure      = 2
            OTHERS              = 3.
        IF sy-subrc NE 0.
          RAISE EXCEPTION TYPE zcx_excep_cls.
        ENDIF.

        lv_mode = zif_enq_mode_c=>promote_optimistic.
      ENDIF.

      CALL FUNCTION 'ENQUEUE_EZARN_APPROVAL'
        EXPORTING
          idno                = lv_idno
          mode_zsarn_idno_key = lv_mode
        EXCEPTIONS
          foreign_lock        = 1
          system_failure      = 2
          OTHERS              = 3.
      CASE sy-subrc.

        WHEN 0.
          "all good locked
          lv_success = abap_true.
          IF iv_dequeue_on_commit EQ abap_true.
            CALL FUNCTION 'ZARN_RELEASE_LOCK_ON_COMMIT' IN UPDATE TASK.
          ENDIF.
          EXIT.
        WHEN 1.
          IF iv_mode EQ zif_enq_mode_c=>exclusive.
            "if E wait
            WAIT UP TO 2 SECONDS.
          ELSE.
            "otherwise directly error - in case of O or R
            RAISE EXCEPTION TYPE zcx_excep_cls.
          ENDIF.
        WHEN OTHERS.
          RAISE EXCEPTION TYPE zcx_excep_cls.
      ENDCASE.
    ENDDO.

    IF lv_success NE abap_true.
      RAISE EXCEPTION TYPE zcx_excep_cls.
    ENDIF.

  ENDMETHOD.


  METHOD get_approvals.

    rt_approvals[] = mt_approvals[].

    IF mv_appr_area IS NOT INITIAL.
      DELETE rt_approvals WHERE approval_area NE mv_appr_area.
    ENDIF.

  ENDMETHOD.


  METHOD get_approvals_change_ids.

    "this will find all changes related to approvals

    DATA: lt_approvals    LIKE mt_approvals,
          lt_approvals_ch TYPE zarn_t_approval_ch.

    lt_approvals[] = mt_approvals[].

    IF mv_appr_area IS NOT INITIAL.
      DELETE lt_approvals WHERE approval_area NE mv_appr_area.
    ENDIF.

    IF iv_chg_category IS NOT INITIAL.
      DELETE lt_approvals WHERE chg_category NE iv_chg_category.
    ENDIF.

    IF lt_approvals[] IS INITIAL.
      "just return - no approvals
      RETURN.
    ENDIF.

    CLEAR lt_approvals_ch[].
    SELECT * FROM zarn_approval_ch INTO TABLE lt_approvals_ch
      FOR ALL ENTRIES IN lt_approvals
      WHERE guid = lt_approvals-guid.

    IF lt_approvals_ch[] IS NOT INITIAL.
      SELECT * FROM zarn_cc_det INTO TABLE et_cc_det
        FOR ALL ENTRIES IN lt_approvals_ch
        WHERE chgid = lt_approvals_ch-chgid
          AND idno  = lt_approvals_ch-idno.

      SELECT * FROM zarn_cc_hdr INTO TABLE et_cc_hdr
        FOR ALL ENTRIES IN lt_approvals_ch
        WHERE chgid = lt_approvals_ch-chgid
          AND idno  = lt_approvals_ch-idno.
    ENDIF.

    IF iv_chg_category IS NOT INITIAL.
      delete et_cc_det where chg_category NE iv_chg_category.
      delete et_cc_hdr where chg_category NE iv_chg_category.
    ENDIF.

  ENDMETHOD.


  method GET_APPROVALS_DB.

    CLEAR rt_approval_db.
    SELECT * FROM zarn_approval INTO TABLE rt_approval_db
      WHERE idno = mv_idno.


  endmethod.


  METHOD GET_APPROVAL_RECORD.

    TRY.

    rs_approval = mt_approvals[ approval_area = mv_appr_area
                                team_code     = iv_team_code
                                chg_category  = iv_chg_category ].

      catch cx_sy_itab_line_not_found.
        clear rs_approval.

    ENDTRY.

  ENDMETHOD.


  METHOD get_conf_teams.

    rt_teams[] = mt_conf_teams[].
    IF iv_display_only = abap_true.
      DELETE rt_teams WHERE display_order IS INITIAL.
    ENDIF.

    SORT rt_teams BY display_order.
  ENDMETHOD.


  METHOD GET_HEADER_STATUS.


    "TBD not used at the moment


*    DATA ltr_stat TYPE RANGE OF zarn_e_appr_status.
*
*    load_approval_config( ).
*
*    clear rv_status.
*
*    "first try to check whether it is WIP
*    ltr_stat[] = VALUE #( sign = 'I' option = 'EQ'
*                        ( low = gc_stat_appr_in_queue )
*                        ( low = gc_stat_appr_ready )
*                        ( low = gc_stat_rejected ) ).
*
*    LOOP AT mt_approvals TRANSPORTING NO FIELDS
*                         WHERE approval_area = iv_approval_area
*                           AND status IN ltr_stat.
*      EXIT.
*    ENDLOOP.
*    if sy-subrc EQ 0.
*      "TBD WIP
*      return.
*    endif.
*
*    check rv_status is initial.
*
*    "then try whether everything is approved but not posted
*    clear ltr_stat[].
*    ltr_stat[] = VALUE #( sign = 'I' option = 'EQ'
*                        ( low = gc_stat_approved ) ).
*    LOOP AT mt_approvals TRANSPORTING NO FIELDS
*                         WHERE approval_area = iv_approval_area
*                           AND status IN ltr_stat.
*      EXIT.
*    ENDLOOP.
*    IF sy-subrc EQ 0.
*      "TBD APR
*    endif.
*
*    check rv_status is initial.
*
*    "then try whether everything is approved but not posted
*    clear ltr_stat[].
*    ltr_stat[] = VALUE #( sign = 'I' option = 'EQ'
*                        ( low = gc_stat_posted ) ).
*    LOOP AT mt_approvals TRANSPORTING NO FIELDS
*                         WHERE approval_area = iv_approval_area
*                           AND status IN ltr_stat.
*      EXIT.
*    ENDLOOP.
*    IF sy-subrc EQ 0.
*      "TBD CRE
*    endif.
*






  ENDMETHOD.


  METHOD get_pending_approvals.
* Returns pending approvals from DB

    DATA: lt_notif_status TYPE RANGE OF zarn_e_notif_status.

* Exclude sent notifications from the selection(default behaviour) if
* no id selection is provided
    IF it_idno IS INITIAL.
      lt_notif_status =
        VALUE #(
          ( sign = 'E'
            option = 'EQ'
            low = gc_notif_stat-sent
          )
        ).
    ENDIF.

* Pull the pending approvals
    SELECT *
      FROM zarn_approval
      INTO TABLE rt_pending_approvals
      WHERE idno IN it_idno AND "Blanks is fine here
            updated_timestamp IN it_appr_updated_on AND
            status = gc_stat-appr_ready AND
            notification_status IN lt_notif_status.

  ENDMETHOD.


  METHOD get_previous_team.

    DATA: lv_order          TYPE i,
          lv_order_numc     TYPE zarn_team_display_order,
          lv_prev_team_code TYPE zarn_team_code.

    "get previous team in approval matrix - if not found, then empty
    CLEAR rv_prev_team_code.

    TRY.

        lv_order = mt_conf_teams[ team_code = iv_team_code  ]-display_order.

        WHILE lv_order GT 1.

          SUBTRACT 1 FROM lv_order.
          lv_order_numc = lv_order.

          lv_prev_team_code = mt_conf_teams[ display_order = lv_order_numc ]-team_code.

          "we want the previous team that exists in approval matrix
          IF line_exists( mt_approvals[ approval_area = mv_appr_area
                                        team_code     =  lv_prev_team_code  ] ).

            rv_prev_team_code = lv_prev_team_code.
            EXIT.
          ENDIF.

        ENDWHILE.

      CATCH cx_sy_itab_line_not_found.

    ENDTRY.
  ENDMETHOD.


  METHOD get_team_ready_for_approval.

    TRY.

        IF line_exists( mt_approvals[ status =  gc_stat-appr_ready ] ) .
          "if at least one "ready for approve record exists

          rv_team_code = mt_approvals[ approval_area = mv_appr_area
                                       status        = gc_stat-appr_ready ]-team_code.

        ELSE.
          "also might happen that everything for "ready team" is rejected

          rv_team_code = mt_approvals[ approval_area = mv_appr_area
                                       status        = gc_stat-rejected ]-team_code.

        ENDIF.

      CATCH cx_sy_itab_line_not_found.
        CLEAR rv_team_code.
    ENDTRY.


  ENDMETHOD.


  METHOD has_team_started_approving.

    "check whether team already started approving

    CLEAR rv_started_approving.

    LOOP AT mt_approvals TRANSPORTING NO FIELDS
      WHERE approval_area = mv_appr_area
        AND team_code     = iv_team_code
        AND ( status      = gc_stat-approved
          OR status       = gc_stat-rejected ).


      rv_started_approving = abap_true.
      EXIT.

    ENDLOOP.

  ENDMETHOD.


  METHOD initialize_approvals_status.

    DATA ls_approval TYPE zarn_approval.

    CLEAR ls_approval.

    IF iv_process_type = gc_priv-process_change
      AND mv_approval_event = gc_event_inbound.

      "in case of change coming from PIM (new version)
      "always first reset status of existing national approvals to Posted
      "and then let the logic determine
      "which of the changes between "posted version" - "new version"
      "are relevant for approvals
      "this will clear all approved / rejected item in progress (and not posted)

      set_approvals_posted( EXPORTING iv_approval_area   = gc_appr_area_national
                            IMPORTING et_approval_ch_del = et_approval_ch_del
                            CHANGING  ct_approval        = ct_approval ).


    ENDIF.

  ENDMETHOD.


  METHOD IS_AUTHORISED_FOR_TEAM.

    rv_authorised = abap_false.

    AUTHORITY-CHECK OBJECT 'ZARN_TEAM'
             ID 'ZARN_TEAM' FIELD iv_team_code.
    IF sy-subrc EQ 0.
      rv_authorised = abap_true.
    ENDIF.

  ENDMETHOD.


  METHOD is_fully_approved.

    FIELD-SYMBOLS <lt_approval> TYPE zarn_t_approval.

    IF it_approval IS SUPPLIED.
      ASSIGN it_approval[] TO <lt_approval>.
    ELSE.
      ASSIGN mt_approvals[] TO <lt_approval>.
    ENDIF.

    CLEAR rv_fully_approved.


    IF    <lt_approval>[] IS INITIAL
      OR NOT line_exists( <lt_approval>[ approval_area = iv_approval_area ] ).

      "if there are no relevant approvals consider it as approved or not approved
      "based on the situation - default is as "not approved"
      IF iv_no_records_as_approved = abap_true.
        rv_fully_approved = abap_true.
      ENDIF.

      RETURN.
    ENDIF.


    IF iv_approval_area IS INITIAL.

      "if approval area is initial, do a global check - both regional and national

      LOOP AT <lt_approval> TRANSPORTING NO FIELDS
                          WHERE status = gc_stat-appr_in_queue
                             OR status = gc_stat-appr_ready
                             OR status = gc_stat-rejected.
        "check for any "not approved or posted" status
        EXIT.
      ENDLOOP.

      IF sy-subrc NE 0.
        "if no entries exist that means everything is either approved or posted
        rv_fully_approved = abap_true.
      ENDIF.

    ELSE.

      "else check only for specified area
      LOOP AT <lt_approval> TRANSPORTING NO FIELDS
                    WHERE approval_area = iv_approval_area
                      AND  ( status = gc_stat-appr_in_queue
                       OR status = gc_stat-appr_ready
                       OR status = gc_stat-rejected ).
        "check for any "not approved or posted" status
        EXIT.
      ENDLOOP.

      IF sy-subrc NE 0.
        "if no entries exist that means everything is either approved or posted
        rv_fully_approved = abap_true.
      ENDIF.

    ENDIF.

  ENDMETHOD.


  METHOD is_reject_notif_required.
* Returns true if notification of rejection is required

* Always required when rejecting from national
    IF iv_approval_area = gc_appr_area_national.
      rv_required = abap_true.
    ELSE.
* For a regional rejection, notification to national is only required for status reasons
      TRY.
          IF iv_status_reason IN zcl_arn_param_prov=>get_range( zif_arn_param_const=>c_notify_reject_reg_reas ).
            rv_required = abap_true.
          ENDIF.
        CATCH zcx_arn_param_err ##NO_HANDLER.
      ENDTRY.
    ENDIF.

  ENDMETHOD.


  METHOD load_approval_config.

    "load approval related configs into buffers

    IF mt_conf_matrix[] IS INITIAL.
      SELECT * FROM zarn_rel_matrix INTO TABLE mt_conf_matrix.
    ENDIF.

    IF mt_conf_teams[] IS INITIAL.
      SELECT * FROM zarn_teams INTO TABLE mt_conf_teams.
    ENDIF.

  ENDMETHOD.


  METHOD load_recent_approval_data.

    CLEAR mt_approvals[].

    mt_approvals[] = get_approvals_db( ).

  ENDMETHOD.


  METHOD modify_approval.
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 26/11/2019
* CHANGE No.       # Charm 9000006356; Jira CIP-148
* DESCRIPTION      # When modifying existing approval record the status
*                  # reason is not cleared so value was carried over
*                  # from previous data. Adjusted code accordingly to
*                  # fix issue.
* WHO              # C90001929, Aruna Mackenjee
*-----------------------------------------------------------------------

    DATA ls_approval TYPE zarn_approval.

    GET TIME.

    "check existing approval records
    READ TABLE ct_approval ASSIGNING FIELD-SYMBOL(<ls_approval>)
          WITH  KEY idno = mv_idno
                    approval_area = is_matrix-data_level
                    chg_category = is_matrix-change_category
                    team_code     = is_matrix-release_team.
    IF sy-subrc EQ 0.
      "record already exists - clear non key data
      CLEAR <ls_approval>-data.

      "just change the status to "Approval in Queue"
      <ls_approval>-status = gc_stat-appr_in_queue.
      <ls_approval>-nat_version = mv_nat_version.

      "updating "change by and changed timestamp" will happen later
      "in BUILD_HISTORY_TABLE

      rv_guid = <ls_approval>-guid.
    ELSE.
      "build new record and append to the table

      CLEAR ls_approval.
      ls_approval-idno = mv_idno.
      ls_approval-approval_area = is_matrix-data_level.
      ls_approval-chg_category = is_matrix-change_category.
      ls_approval-team_code    = is_matrix-release_team.
      ls_approval-status       = gc_stat-appr_in_queue.
      ls_approval-nat_version  = mv_nat_version.

      ls_approval-updated_by   = sy-uname.

      ls_approval-updated_timestamp = sy-datum && sy-uzeit.

*      CONVERT DATE sy-datum TIME sy-uzeit
*        INTO TIME STAMP ls_approval-updated_timestamp TIME ZONE sy-zonlo.

      "generate GUID
      TRY.

          ls_approval-guid = cl_system_uuid=>create_uuid_c32_static( ).

        CATCH cx_uuid_error.
          "TBD error handling
      ENDTRY.
      INSERT ls_approval INTO TABLE ct_approval.

      rv_guid = ls_approval-guid.

    ENDIF.


  ENDMETHOD.


  METHOD notify_reject.
* Notify of rejection

  data: ls_notif_param type zsarn_notif_param.

  ls_notif_param-sender_as_receiver = abap_true.

* Generate the notification
    DATA(lo_notif) = CAST zif_arn_notif(
      NEW zcl_arn_notif_reject(
        iv_idno = iv_idno
        iv_notif_text = iv_reject_comment
        it_approvals = it_approvals
        is_notif_param = ls_notif_param
      ) ).
    IF lo_notif->generate( ).
* Send it
      lo_notif->transmit( iv_commit = abap_false ).
    ENDIF.


  ENDMETHOD.


  METHOD post_approvals.

    "update / insert all tables
    "this will be called from UPDATE TASK function module

    IF it_approval[] IS NOT INITIAL.
      MODIFY zarn_approval FROM TABLE it_approval.
    ENDIF.

    "also possibility to delete link to obsolete changes
    IF it_approval_ch_del[] IS NOT INITIAL.
      DELETE zarn_approval_ch FROM TABLE it_approval_ch_del.
    ENDIF.

    IF it_approval_ch[] IS NOT INITIAL.
      INSERT zarn_approval_ch FROM TABLE it_approval_ch ACCEPTING DUPLICATE KEYS.
    ENDIF.

    IF it_appr_hist[] IS NOT INITIAL.
      MODIFY zarn_appr_hist FROM TABLE it_appr_hist.
    ENDIF.



  ENDMETHOD.


  METHOD run_automatic_status_determ.
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 20/11/2019
* CHANGE No.       # Charm 9000006356; Jira CIP-148
* DESCRIPTION      # Added method to intiate event based auto approval.
* WHO              # C90001929, Aruna Mackenjee
*-----------------------------------------------------------------------


    DATA: lv_continue TYPE abap_bool.

    DATA(ls_autoappr_params) = determ_autoappr_parameters( iv_contains_val_errors  ).

    "now determine whether it is Create/Change scenario
    DATA(lv_process_type) = determine_process_type( ).

    lv_continue = abap_true.

    "do while anything changes
    "once no autoapproval happens, stop
    WHILE lv_continue EQ abap_true.

      CLEAR lv_continue.

      "this will determine what is AQ (in queue) and AR (ready)
      determine_stat_ready_for_appr( CHANGING ct_approval = ct_approval ).

      IF ls_autoappr_params-skip EQ abap_true.
        CONTINUE.
      ENDIF.

      LOOP AT ct_approval ASSIGNING FIELD-SYMBOL(<ls_approval>)
        WHERE status = gc_stat-appr_ready.

        IF     ls_autoappr_params-area IS NOT INITIAL
          AND  <ls_approval>-approval_area NE ls_autoappr_params-area .
          CONTINUE.
        ENDIF.

        "go through all approvals that are "ready to be approved" - RA
        "if they are configured as auto-approved, switch them to AP
        IF line_exists( mt_conf_matrix[ change_category = <ls_approval>-chg_category
                                        release_team  = <ls_approval>-team_code
                                        data_level    = <ls_approval>-approval_area
                                        process_type  = lv_process_type
                                        auto_approve  = abap_true ] ).

          <ls_approval>-status = gc_stat-approved.
          <ls_approval>-status_reason = gc_stat_reas-autoapproved.
          <ls_approval>-status_comment = 'Auto-approved'(aa1).

          lv_continue = abap_true.
        ENDIF.
      ENDLOOP.

    ENDWHILE.


    " Execute auto approval based on corresponding event
    me->run_event_based_auto_approval(
      EXPORTING
        iv_contains_val_errors = iv_contains_val_errors
        iv_process_type        = lv_process_type
      CHANGING
        ct_approval = ct_approval
    ).


  ENDMETHOD.


  METHOD run_event_based_auto_approval.
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 20/11/2019
* CHANGE No.       # Charm 9000006356; Jira CIP-148
* DESCRIPTION      # New method to perform auto approval based on new
*                  # event that works over and above the change
*                  # category auto approval.
* WHO              # C90001929, Aruna Mackenjee
*-----------------------------------------------------------------------

    DATA:
      lt_appr_area_r TYPE RANGE OF zarn_e_appr_area.

    " Don't do anything if there are validation errors or when not
    " in change mode
    IF iv_contains_val_errors = abap_true
    OR NOT iv_process_type    = gc_priv-process_change.
      RETURN.
    ENDIF.

    " For inbound event, only processing National approvals otherwise all
    " areas will be processed.
    " Nothing to do if there's nothing ready for approval
    IF mv_approval_event = gc_event_inbound.
      IF NOT line_exists( ct_approval[ status = gc_stat-appr_ready approval_area = gc_appr_area_national ] ).
        RETURN.
      ENDIF.

      lt_appr_area_r = VALUE #( ( sign = 'I' option = 'EQ' low = gc_appr_area_national ) ).
    ELSE.
      IF NOT line_exists( ct_approval[ status = gc_stat-appr_ready ] ).
        RETURN.
      ENDIF.

      " Process all approval areas
      CLEAR lt_appr_area_r[].
    ENDIF.

    " Check whether auto approval event exists
    TRY .
        DATA(lo_validation) = NEW zcl_arn_validation_engine( iv_event = zcl_arn_validation_engine=>gc_event_auto_approval ).

        " Nothing to do if there are no rules defined
        IF lo_validation->gt_validation[] IS INITIAL.
          RETURN.
        ENDIF.

        " Set up key info for data extract
        DATA(lt_key)       = VALUE ztarn_key( ( idno = mv_idno version = mv_nat_version ) ).
        DATA(lt_reg_key)   = VALUE ztarn_reg_key( ( idno = mv_idno ) ).

        DATA(ls_prod_data) = VALUE zsarn_prod_data( ).
        DATA(ls_reg_data)  = VALUE zsarn_reg_data( ).

        DATA(lo_inb_util) = zcl_arn_inbound_utils=>get_instance( ).
        lo_inb_util->get_prod_reg_data(
          IMPORTING
            et_product_data  = DATA(lt_prod_data)
            et_regional_data = DATA(lt_reg_data)
          ).

        " Get buffered data when available
        IF lo_inb_util->mv_prod_reg_data_set = abap_true.
          ls_prod_data = VALUE #( lt_prod_data[ idno = mv_idno version = mv_nat_version ] OPTIONAL ).
          ls_reg_data  = VALUE #( lt_reg_data[ idno = mv_idno ] OPTIONAL ).
        ELSE.
          CALL FUNCTION 'ZARN_READ_NATIONAL_DATA'
            EXPORTING
              it_key      = lt_key
            IMPORTING
*             ET_DATA     =
              es_data_all = ls_prod_data.
        ENDIF.

        IF ls_reg_data IS INITIAL.
          " From inbound regional data may not be set so get the latest DB version
          CALL FUNCTION 'ZARN_READ_REGIONAL_DATA'
            EXPORTING
              it_key      = lt_reg_key
            IMPORTING
*             ET_DATA     =
              es_data_all = ls_reg_data.
        ENDIF.

        " Check if auto approval should be carried out based on special logic
        lo_validation->validate_arn_single(
          EXPORTING is_zsarn_prod_data = ls_prod_data
                    is_zsarn_reg_data  = ls_reg_data
          IMPORTING et_output = DATA(lt_validation_output) ).

        IF lines( lt_validation_output ) GT 0.
          " Implies conditions not met so don't auto approve
          RETURN.
        ENDIF.

        " The WHILE loop logic taken from RUN_AUTOMATIC_STATUS_DETERM method
        " with slight amendments.
        DATA(lv_continue) = abap_true.

        "do while anything changes
        "once no autoapproval happens, stop
        WHILE lv_continue EQ abap_true.

          lv_continue = abap_false.

          "this will determine what is AQ (in queue) and AR (ready)
          me->determine_stat_ready_for_appr( CHANGING ct_approval = ct_approval ).

          LOOP AT ct_approval ASSIGNING FIELD-SYMBOL(<ls_approval>)
            WHERE status = gc_stat-appr_ready
              AND approval_area IN lt_appr_area_r.

            <ls_approval>-status         = gc_stat-approved.
            <ls_approval>-status_reason  = gc_stat_reas-autoapproved.
            <ls_approval>-status_comment = 'Event based auto-approval'(aa2).

          ENDLOOP.
          IF sy-subrc IS INITIAL.
            " If there was at least 1 approved need to check if there are
            " any more in the queue
            lv_continue = abap_true.
          ENDIF.

        ENDWHILE.

      CATCH zcx_arn_validation_err INTO DATA(lo_excp)   ##NO_HANDLER ##NEEDED.
        " Ignoring error for now - rules not set up correctly

    ENDTRY.

  ENDMETHOD.


  METHOD set_approvals_posted.

    DATA : ls_approval TYPE zarn_approval,
           lt_approval TYPE zarn_t_approval.

    CLEAR ls_approval.

    CLEAR et_approval_ch_del[].

    "set all approvals posted and delete relations to change categories

    ls_approval-status = gc_stat-posted.

    IF iv_approval_area IS INITIAL.

      MODIFY ct_approval FROM ls_approval
       TRANSPORTING status
       WHERE idno = mv_idno.

      IF ct_approval[] IS NOT INITIAL.

        SELECT * FROM zarn_approval_ch INTO TABLE et_approval_ch_del
        FOR ALL ENTRIES IN ct_approval
          WHERE guid = ct_approval-guid.

      ENDIF.

    ELSE.

      MODIFY ct_approval FROM ls_approval
       TRANSPORTING status
       WHERE idno          = mv_idno
         AND approval_area = iv_approval_area.

      lt_approval[] = ct_approval[].
      DELETE lt_approval WHERE approval_area NE iv_approval_area.

      IF lt_approval[] IS NOT INITIAL.

        SELECT * FROM zarn_approval_ch INTO TABLE et_approval_ch_del
        FOR ALL ENTRIES IN lt_approval
          WHERE guid = lt_approval-guid.

      ENDIF.

    ENDIF.

  ENDMETHOD.


  method SET_APPROVAL_AREA.

    mv_appr_area = iv_approval_area.

  endmethod.


  method SET_DATA_SINGLE.

    mv_idno = iv_idno.
    mv_nat_version = iv_nat_version.

  endmethod.


  METHOD update_notification_status.
* Post the approvals from the notification

    DATA: lt_approvals_range TYPE RANGE OF zarn_e_appr_guid.
    DATA: ls_approval_range LIKE LINE OF lt_approvals_range.


* Build the range for set update for notifications not in requested target status
    ls_approval_range-sign = 'I'.
    ls_approval_range-option = 'EQ'.
    LOOP AT it_approvals ASSIGNING FIELD-SYMBOL(<ls_approval>)
      WHERE notification_status <> iv_target_status.
      ls_approval_range-low = <ls_approval>-guid.
      APPEND ls_approval_range TO lt_approvals_range.
    ENDLOOP.

* Perform the update
    IF lt_approvals_range IS NOT INITIAL.
      UPDATE zarn_approval
        SET notification_status = @iv_target_status
        WHERE guid IN @lt_approvals_range.
    ENDIF.

  ENDMETHOD.
ENDCLASS.
