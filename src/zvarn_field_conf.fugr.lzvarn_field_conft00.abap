*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 03.03.2017 at 09:52:42
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZVARN_FIELD_CONF................................*
TABLES: ZVARN_FIELD_CONF, *ZVARN_FIELD_CONF. "view work areas
CONTROLS: TCTRL_ZVARN_FIELD_CONF
TYPE TABLEVIEW USING SCREEN '0001'.
DATA: BEGIN OF STATUS_ZVARN_FIELD_CONF. "state vector
          INCLUDE STRUCTURE VIMSTATUS.
DATA: END OF STATUS_ZVARN_FIELD_CONF.
* Table for entries selected to show on screen
DATA: BEGIN OF ZVARN_FIELD_CONF_EXTRACT OCCURS 0010.
INCLUDE STRUCTURE ZVARN_FIELD_CONF.
          INCLUDE STRUCTURE VIMFLAGTAB.
DATA: END OF ZVARN_FIELD_CONF_EXTRACT.
* Table for all entries loaded from database
DATA: BEGIN OF ZVARN_FIELD_CONF_TOTAL OCCURS 0010.
INCLUDE STRUCTURE ZVARN_FIELD_CONF.
          INCLUDE STRUCTURE VIMFLAGTAB.
DATA: END OF ZVARN_FIELD_CONF_TOTAL.

*.........table declarations:.................................*
TABLES: ZARN_FIELD_CONFG               .
