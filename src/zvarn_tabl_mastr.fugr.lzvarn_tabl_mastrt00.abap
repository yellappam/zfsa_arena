*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 25.02.2016 at 15:08:40 by user C90001363
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZVARN_TABL_MASTR................................*
TABLES: ZVARN_TABL_MASTR, *ZVARN_TABL_MASTR. "view work areas
CONTROLS: TCTRL_ZVARN_TABL_MASTR
TYPE TABLEVIEW USING SCREEN '0001'.
DATA: BEGIN OF STATUS_ZVARN_TABL_MASTR. "state vector
          INCLUDE STRUCTURE VIMSTATUS.
DATA: END OF STATUS_ZVARN_TABL_MASTR.
* Table for entries selected to show on screen
DATA: BEGIN OF ZVARN_TABL_MASTR_EXTRACT OCCURS 0010.
INCLUDE STRUCTURE ZVARN_TABL_MASTR.
          INCLUDE STRUCTURE VIMFLAGTAB.
DATA: END OF ZVARN_TABL_MASTR_EXTRACT.
* Table for all entries loaded from database
DATA: BEGIN OF ZVARN_TABL_MASTR_TOTAL OCCURS 0010.
INCLUDE STRUCTURE ZVARN_TABL_MASTR.
          INCLUDE STRUCTURE VIMFLAGTAB.
DATA: END OF ZVARN_TABL_MASTR_TOTAL.

*.........table declarations:.................................*
TABLES: ZARN_TABLE_MASTR               .
