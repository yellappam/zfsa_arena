*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_SEARCH_HELP_EXITSTOP.        " Global Data
  INCLUDE LZARN_SEARCH_HELP_EXITSUXX.        " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_SEARCH_HELP_EXITSF...        " Subroutines
* INCLUDE LZARN_SEARCH_HELP_EXITSO...        " PBO-Modules
* INCLUDE LZARN_SEARCH_HELP_EXITSI...        " PAI-Modules
* INCLUDE LZARN_SEARCH_HELP_EXITSE...        " Events
* INCLUDE LZARN_SEARCH_HELP_EXITSP...        " Local class implement.
* INCLUDE LZARN_SEARCH_HELP_EXITST99.        " ABAP Unit tests
