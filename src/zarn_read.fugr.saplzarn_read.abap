*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_READTOP.                     " Global Data
  INCLUDE LZARN_READUXX.                     " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_READF...                     " Subroutines
* INCLUDE LZARN_READO...                     " PBO-Modules
* INCLUDE LZARN_READI...                     " PAI-Modules
* INCLUDE LZARN_READE...                     " Events
* INCLUDE LZARN_READP...                     " Local class implement.
* INCLUDE LZARN_READT99.                     " ABAP Unit tests

INCLUDE LZARN_READF01.
