FUNCTION zarn_ctrl_regnl_write_doc_cust .
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(OBJECTID) TYPE  CDHDR-OBJECTID
*"     VALUE(TCODE) TYPE  CDHDR-TCODE
*"     VALUE(UTIME) TYPE  CDHDR-UTIME
*"     VALUE(UDATE) TYPE  CDHDR-UDATE
*"     VALUE(USERNAME) TYPE  CDHDR-USERNAME
*"     VALUE(PLANNED_CHANGE_NUMBER) TYPE  CDHDR-PLANCHNGNR DEFAULT
*"       SPACE
*"     VALUE(OBJECT_CHANGE_INDICATOR) TYPE  CDHDR-CHANGE_IND DEFAULT
*"       'U'
*"     VALUE(PLANNED_OR_REAL_CHANGES) TYPE  CDHDR-CHANGE_IND DEFAULT
*"       SPACE
*"     VALUE(NO_CHANGE_POINTERS) TYPE  CDHDR-CHANGE_IND DEFAULT SPACE
*"     VALUE(UPD_ICDTXT_ZARN_CTRL_REGNL) TYPE  CDPOS-CHNGIND DEFAULT
*"       SPACE
*"     VALUE(UPD_ZARN_CONTROL) TYPE  CDPOS-CHNGIND DEFAULT SPACE
*"     VALUE(UPD_ZARN_PRD_VERSION) TYPE  CDPOS-CHNGIND DEFAULT SPACE
*"     VALUE(UPD_ZARN_REG_ALLERG) TYPE  CDPOS-CHNGIND DEFAULT SPACE
*"     VALUE(UPD_ZARN_REG_ARTLINK) TYPE  CDPOS-CHNGIND DEFAULT SPACE
*"     VALUE(UPD_ZARN_REG_BANNER) TYPE  CDPOS-CHNGIND DEFAULT SPACE
*"     VALUE(UPD_ZARN_REG_DC_SELL) TYPE  CDPOS-CHNGIND DEFAULT SPACE
*"     VALUE(UPD_ZARN_REG_EAN) TYPE  CDPOS-CHNGIND DEFAULT SPACE
*"     VALUE(UPD_ZARN_REG_HDR) TYPE  CDPOS-CHNGIND DEFAULT SPACE
*"     VALUE(UPD_ZARN_REG_HSNO) TYPE  CDPOS-CHNGIND DEFAULT SPACE
*"     VALUE(UPD_ZARN_REG_LST_PRC) TYPE  CDPOS-CHNGIND DEFAULT SPACE
*"     VALUE(UPD_ZARN_REG_ONLCAT) TYPE  CDPOS-CHNGIND DEFAULT SPACE
*"     VALUE(UPD_ZARN_REG_PIR) TYPE  CDPOS-CHNGIND DEFAULT SPACE
*"     VALUE(UPD_ZARN_REG_PRFAM) TYPE  CDPOS-CHNGIND DEFAULT SPACE
*"     VALUE(UPD_ZARN_REG_RRP) TYPE  CDPOS-CHNGIND DEFAULT SPACE
*"     VALUE(UPD_ZARN_REG_STD_TER) TYPE  CDPOS-CHNGIND DEFAULT SPACE
*"     VALUE(UPD_ZARN_REG_STR) TYPE  CDPOS-CHNGIND DEFAULT SPACE
*"     VALUE(XZARN_REG_STR) TYPE  YZARN_REG_STRS OPTIONAL
*"     VALUE(YZARN_REG_STR) TYPE  YZARN_REG_STRS OPTIONAL
*"     VALUE(UPD_ZARN_REG_TXT) TYPE  CDPOS-CHNGIND DEFAULT SPACE
*"     VALUE(UPD_ZARN_REG_UOM) TYPE  CDPOS-CHNGIND DEFAULT SPACE
*"     VALUE(UPD_ZARN_VER_STATUS) TYPE  CDPOS-CHNGIND DEFAULT SPACE
*"     VALUE(UPD_ZARN_REG_SC) TYPE  CDPOS-CHNGIND DEFAULT SPACE
*"     VALUE(UPD_ZARN_REG_CLUSTER) TYPE  CDPOS-CHNGIND DEFAULT SPACE
*"     VALUE(UPD_ZARN_REG_NUTRIEN) TYPE  CDPOS-CHNGIND DEFAULT SPACE
*"  EXPORTING
*"     REFERENCE(EV_CHANGENUMBER) TYPE  CDHDR-CHANGENR
*"  TABLES
*"      ICDTXT_ZARN_CTRL_REGNL STRUCTURE  CDTXT
*"      XZARN_CONTROL STRUCTURE  YZARN_CONTROL OPTIONAL
*"      YZARN_CONTROL STRUCTURE  YZARN_CONTROL OPTIONAL
*"      XZARN_PRD_VERSION STRUCTURE  YZARN_PRD_VERSION OPTIONAL
*"      YZARN_PRD_VERSION STRUCTURE  YZARN_PRD_VERSION OPTIONAL
*"      XZARN_REG_ALLERG STRUCTURE  YZARN_REG_ALLERG OPTIONAL
*"      YZARN_REG_ALLERG STRUCTURE  YZARN_REG_ALLERG OPTIONAL
*"      XZARN_REG_ARTLINK STRUCTURE  YZARN_REG_ARTLINK OPTIONAL
*"      YZARN_REG_ARTLINK STRUCTURE  YZARN_REG_ARTLINK OPTIONAL
*"      XZARN_REG_BANNER STRUCTURE  YZARN_REG_BANNER OPTIONAL
*"      YZARN_REG_BANNER STRUCTURE  YZARN_REG_BANNER OPTIONAL
*"      XZARN_REG_DC_SELL STRUCTURE  YZARN_REG_DC_SELL OPTIONAL
*"      YZARN_REG_DC_SELL STRUCTURE  YZARN_REG_DC_SELL OPTIONAL
*"      XZARN_REG_EAN STRUCTURE  YZARN_REG_EAN OPTIONAL
*"      YZARN_REG_EAN STRUCTURE  YZARN_REG_EAN OPTIONAL
*"      XZARN_REG_HDR STRUCTURE  YZARN_REG_HDR OPTIONAL
*"      YZARN_REG_HDR STRUCTURE  YZARN_REG_HDR OPTIONAL
*"      XZARN_REG_HSNO STRUCTURE  YZARN_REG_HSNO OPTIONAL
*"      YZARN_REG_HSNO STRUCTURE  YZARN_REG_HSNO OPTIONAL
*"      XZARN_REG_LST_PRC STRUCTURE  YZARN_REG_LST_PRC OPTIONAL
*"      YZARN_REG_LST_PRC STRUCTURE  YZARN_REG_LST_PRC OPTIONAL
*"      XZARN_REG_ONLCAT STRUCTURE  YZARN_REG_ONLCAT OPTIONAL
*"      YZARN_REG_ONLCAT STRUCTURE  YZARN_REG_ONLCAT OPTIONAL
*"      XZARN_REG_PIR STRUCTURE  YZARN_REG_PIR OPTIONAL
*"      YZARN_REG_PIR STRUCTURE  YZARN_REG_PIR OPTIONAL
*"      XZARN_REG_PRFAM STRUCTURE  YZARN_REG_PRFAM OPTIONAL
*"      YZARN_REG_PRFAM STRUCTURE  YZARN_REG_PRFAM OPTIONAL
*"      XZARN_REG_RRP STRUCTURE  YZARN_REG_RRP OPTIONAL
*"      YZARN_REG_RRP STRUCTURE  YZARN_REG_RRP OPTIONAL
*"      XZARN_REG_STD_TER STRUCTURE  YZARN_REG_STD_TER OPTIONAL
*"      YZARN_REG_STD_TER STRUCTURE  YZARN_REG_STD_TER OPTIONAL
*"      XZARN_REG_TXT STRUCTURE  YZARN_REG_TXT OPTIONAL
*"      YZARN_REG_TXT STRUCTURE  YZARN_REG_TXT OPTIONAL
*"      XZARN_REG_UOM STRUCTURE  YZARN_REG_UOM OPTIONAL
*"      YZARN_REG_UOM STRUCTURE  YZARN_REG_UOM OPTIONAL
*"      XZARN_VER_STATUS STRUCTURE  YZARN_VER_STATUS OPTIONAL
*"      YZARN_VER_STATUS STRUCTURE  YZARN_VER_STATUS OPTIONAL
*"      XZARN_REG_SC STRUCTURE  YZARN_REG_SC OPTIONAL
*"      YZARN_REG_SC STRUCTURE  YZARN_REG_SC OPTIONAL
*"      XZARN_REG_CLUSTER STRUCTURE  YZARN_REG_CLUSTER OPTIONAL
*"      YZARN_REG_CLUSTER STRUCTURE  YZARN_REG_CLUSTER OPTIONAL
*"      XZARN_REG_NUTRIEN STRUCTURE  YZARN_REG_NUTRIEN OPTIONAL
*"      YZARN_REG_NUTRIEN STRUCTURE  YZARN_REG_NUTRIEN OPTIONAL
*"----------------------------------------------------------------------
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 13.04.2018
* CHANGE No.       # Charm 9000003482; Jira CI18-554
* DESCRIPTION      # CI18-554 Recipe Management - Ingredient Changes
*                  # New regional table for Allergen Type overrides and
*                  # fields for Ingredients statement overrides in Arena.
*                  # New Ingredients Type field in the Scales block in
*                  # Arena and NONSAP tab in material master.
*                  # needed to regenerate change doc object for new table ZARN_REG_ALLERG
* WHO              # C90005557, Tessa Newman
*-----------------------------------------------------------------------
* DATE             # 26.06.2020
* CHANGE No.       # 9000007255: SSM-1 NW Range Policy ZARN_GUI
* DESCRIPTION      # Added table ZARN_REG_CLUSTER
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* DATE             # 20.07.2020
* CHANGE No.       # CIP-1359: ZARN_GUI changes (GS1  Updates)
* DESCRIPTION      # Added table ZARN_REG_NUTRIEN
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
** NOTES:  - C90005557, Tessa Newman 10.04.2018
* This customer FM and associated Include has been copied from the generated version and adjusted.
* If regeneration of the Change Object ZARN_CTRL_REGNL is required for example because additional
* tables need to be added relevant for change docs. The process is to perform the re-generation using
* transaction SCDO for INCLUDE LZARN_CTRL_REGNLU01 and Function Group ZARN_CTRL_REGNL,
* which will re-generate and change the Function Module ZARN_CTRL_REGNL_WRITE_DOCUMENT.
* Then do a compare of this FM with FM ZARN_CTRL_REGNL_WRITE_DOCUMENT and move the
* new source code for the new table/changes into this FM manually (also need to adjust
* the corresponding Import parameters or Tables.)

* The differences noted between the 2 FMs is this one is regular FM whereas ZARN_CTRL_REGNL_WRITE_DOCUMENT
* is an update module. There is an additional exporting parameter on this FM EV_CHANGENUMBER and minor code
* adjustments to support it.
*
*************************************************************************
*
* FURTHER NOTES: - C90012814, Brad Gorlicki 31.01.2019
* Just to confirm on Tessa's notes above..
* 1. Run SCDO for change object ZARN_CTRL_REGNL.
* 2. Click edit/change.
* 3. Add your new table to the list
* 4. Hit Generate - I left everything default except changing it to immediate
* 5. Run Utilities > Code Comparison (turn on compare mode) on
*    ZARN_CTRL_REGNL_WRITE_DOC_CUST against ZARN_CTRL_REGNL_WRITE_DOCUMENT and
*    copy your new structure code into this FM.
* 6. You now need to add your new structure to the import/table parameters
*
* It appears that this function has been modified so this appears to be the safest
* way to update the change docs structure without removing the modified code
*************************************************************************

* THIS FILE IS GENERATED. NEVER CHANGE IT MANUALLY, PLEASE!

  CALL FUNCTION 'CHANGEDOCUMENT_OPEN'
    EXPORTING
      objectclass             = 'ZARN_CTRL_REGNL'
      objectid                = objectid
      planned_change_number   = planned_change_number
      planned_or_real_changes = planned_or_real_changes
    EXCEPTIONS
      sequence_invalid        = 1
      OTHERS                  = 2.

  CASE sy-subrc.
    WHEN 0.                                   "OK.
    WHEN 1. MESSAGE a600 WITH 'SEQUENCE INVALID'.
    WHEN 2. MESSAGE a600 WITH 'OPEN ERROR'.
  ENDCASE.

  IF upd_zarn_control NE space.
    IF ( xzarn_control[] IS INITIAL ) AND
       ( yzarn_control[] IS INITIAL ).
      upd_zarn_control = space.
    ENDIF.
  ENDIF.

  IF upd_zarn_control NE space.
    CALL FUNCTION 'CHANGEDOCUMENT_MULTIPLE_CASE'
      EXPORTING
        tablename              = 'ZARN_CONTROL'
        change_indicator       = upd_zarn_control
        docu_delete            = ''
        docu_insert            = ''
        docu_delete_if         = ''
        docu_insert_if         = ''
      TABLES
        table_old              = yzarn_control
        table_new              = xzarn_control
      EXCEPTIONS
        nametab_error          = 1
        open_missing           = 2
        position_insert_failed = 3
        OTHERS                 = 4.

    CASE sy-subrc.
      WHEN 0.                                "OK.
      WHEN 1. MESSAGE a600 WITH 'NAMETAB-ERROR'.
      WHEN 2. MESSAGE a600 WITH 'OPEN MISSING'.
      WHEN 3. MESSAGE a600 WITH 'INSERT ERROR'.
      WHEN 4. MESSAGE a600 WITH 'MULTIPLE ERROR'.
    ENDCASE.
  ENDIF.

  IF upd_zarn_prd_version NE space.
    IF ( xzarn_prd_version[] IS INITIAL ) AND
       ( yzarn_prd_version[] IS INITIAL ).
      upd_zarn_prd_version = space.
    ENDIF.
  ENDIF.

  IF upd_zarn_prd_version NE space.
    CALL FUNCTION 'CHANGEDOCUMENT_MULTIPLE_CASE'
      EXPORTING
        tablename              = 'ZARN_PRD_VERSION'
        change_indicator       = upd_zarn_prd_version
        docu_delete            = ''
        docu_insert            = ''
        docu_delete_if         = ''
        docu_insert_if         = ''
      TABLES
        table_old              = yzarn_prd_version
        table_new              = xzarn_prd_version
      EXCEPTIONS
        nametab_error          = 1
        open_missing           = 2
        position_insert_failed = 3
        OTHERS                 = 4.

    CASE sy-subrc.
      WHEN 0.                                "OK.
      WHEN 1. MESSAGE a600 WITH 'NAMETAB-ERROR'.
      WHEN 2. MESSAGE a600 WITH 'OPEN MISSING'.
      WHEN 3. MESSAGE a600 WITH 'INSERT ERROR'.
      WHEN 4. MESSAGE a600 WITH 'MULTIPLE ERROR'.
    ENDCASE.
  ENDIF.

  IF upd_zarn_reg_allerg NE space.
    IF ( xzarn_reg_allerg[] IS INITIAL ) AND
       ( yzarn_reg_allerg[] IS INITIAL ).
      upd_zarn_reg_allerg = space.
    ENDIF.
  ENDIF.

  IF upd_zarn_reg_allerg NE space.
    CALL FUNCTION 'CHANGEDOCUMENT_MULTIPLE_CASE'
      EXPORTING
        tablename              = 'ZARN_REG_ALLERG'
        change_indicator       = upd_zarn_reg_allerg
        docu_delete            = ''
        docu_insert            = ''
        docu_delete_if         = ''
        docu_insert_if         = ''
      TABLES
        table_old              = yzarn_reg_allerg
        table_new              = xzarn_reg_allerg
      EXCEPTIONS
        nametab_error          = 1
        open_missing           = 2
        position_insert_failed = 3
        OTHERS                 = 4.

    CASE sy-subrc.
      WHEN 0.                                "OK.
      WHEN 1. MESSAGE a600 WITH 'NAMETAB-ERROR'.
      WHEN 2. MESSAGE a600 WITH 'OPEN MISSING'.
      WHEN 3. MESSAGE a600 WITH 'INSERT ERROR'.
      WHEN 4. MESSAGE a600 WITH 'MULTIPLE ERROR'.
    ENDCASE.
  ENDIF.

  IF upd_zarn_reg_artlink NE space.
    IF ( xzarn_reg_artlink[] IS INITIAL ) AND
       ( yzarn_reg_artlink[] IS INITIAL ).
      upd_zarn_reg_artlink = space.
    ENDIF.
  ENDIF.

  IF upd_zarn_reg_artlink NE space.
    CALL FUNCTION 'CHANGEDOCUMENT_MULTIPLE_CASE'
      EXPORTING
        tablename              = 'ZARN_REG_ARTLINK'
        change_indicator       = upd_zarn_reg_artlink
        docu_delete            = ''
        docu_insert            = ''
        docu_delete_if         = ''
        docu_insert_if         = ''
      TABLES
        table_old              = yzarn_reg_artlink
        table_new              = xzarn_reg_artlink
      EXCEPTIONS
        nametab_error          = 1
        open_missing           = 2
        position_insert_failed = 3
        OTHERS                 = 4.

    CASE sy-subrc.
      WHEN 0.                                "OK.
      WHEN 1. MESSAGE a600 WITH 'NAMETAB-ERROR'.
      WHEN 2. MESSAGE a600 WITH 'OPEN MISSING'.
      WHEN 3. MESSAGE a600 WITH 'INSERT ERROR'.
      WHEN 4. MESSAGE a600 WITH 'MULTIPLE ERROR'.
    ENDCASE.
  ENDIF.

  IF upd_zarn_reg_banner NE space.
    IF ( xzarn_reg_banner[] IS INITIAL ) AND
       ( yzarn_reg_banner[] IS INITIAL ).
      upd_zarn_reg_banner = space.
    ENDIF.
  ENDIF.

  IF upd_zarn_reg_banner NE space.
    CALL FUNCTION 'CHANGEDOCUMENT_MULTIPLE_CASE'
      EXPORTING
        tablename              = 'ZARN_REG_BANNER'
        change_indicator       = upd_zarn_reg_banner
        docu_delete            = ''
        docu_insert            = ''
        docu_delete_if         = ''
        docu_insert_if         = ''
      TABLES
        table_old              = yzarn_reg_banner
        table_new              = xzarn_reg_banner
      EXCEPTIONS
        nametab_error          = 1
        open_missing           = 2
        position_insert_failed = 3
        OTHERS                 = 4.

    CASE sy-subrc.
      WHEN 0.                                "OK.
      WHEN 1. MESSAGE a600 WITH 'NAMETAB-ERROR'.
      WHEN 2. MESSAGE a600 WITH 'OPEN MISSING'.
      WHEN 3. MESSAGE a600 WITH 'INSERT ERROR'.
      WHEN 4. MESSAGE a600 WITH 'MULTIPLE ERROR'.
    ENDCASE.
  ENDIF.

  IF upd_zarn_reg_dc_sell NE space.
    IF ( xzarn_reg_dc_sell[] IS INITIAL ) AND
       ( yzarn_reg_dc_sell[] IS INITIAL ).
      upd_zarn_reg_dc_sell = space.
    ENDIF.
  ENDIF.

  IF upd_zarn_reg_dc_sell NE space.
    CALL FUNCTION 'CHANGEDOCUMENT_MULTIPLE_CASE'
      EXPORTING
        tablename              = 'ZARN_REG_DC_SELL'
        change_indicator       = upd_zarn_reg_dc_sell
        docu_delete            = ''
        docu_insert            = ''
        docu_delete_if         = ''
        docu_insert_if         = ''
      TABLES
        table_old              = yzarn_reg_dc_sell
        table_new              = xzarn_reg_dc_sell
      EXCEPTIONS
        nametab_error          = 1
        open_missing           = 2
        position_insert_failed = 3
        OTHERS                 = 4.

    CASE sy-subrc.
      WHEN 0.                                "OK.
      WHEN 1. MESSAGE a600 WITH 'NAMETAB-ERROR'.
      WHEN 2. MESSAGE a600 WITH 'OPEN MISSING'.
      WHEN 3. MESSAGE a600 WITH 'INSERT ERROR'.
      WHEN 4. MESSAGE a600 WITH 'MULTIPLE ERROR'.
    ENDCASE.
  ENDIF.

  IF upd_zarn_reg_ean NE space.
    IF ( xzarn_reg_ean[] IS INITIAL ) AND
       ( yzarn_reg_ean[] IS INITIAL ).
      upd_zarn_reg_ean = space.
    ENDIF.
  ENDIF.

  IF upd_zarn_reg_ean NE space.
    CALL FUNCTION 'CHANGEDOCUMENT_MULTIPLE_CASE'
      EXPORTING
        tablename              = 'ZARN_REG_EAN'
        change_indicator       = upd_zarn_reg_ean
        docu_delete            = ''
        docu_insert            = ''
        docu_delete_if         = ''
        docu_insert_if         = ''
      TABLES
        table_old              = yzarn_reg_ean
        table_new              = xzarn_reg_ean
      EXCEPTIONS
        nametab_error          = 1
        open_missing           = 2
        position_insert_failed = 3
        OTHERS                 = 4.

    CASE sy-subrc.
      WHEN 0.                                "OK.
      WHEN 1. MESSAGE a600 WITH 'NAMETAB-ERROR'.
      WHEN 2. MESSAGE a600 WITH 'OPEN MISSING'.
      WHEN 3. MESSAGE a600 WITH 'INSERT ERROR'.
      WHEN 4. MESSAGE a600 WITH 'MULTIPLE ERROR'.
    ENDCASE.
  ENDIF.

  IF upd_zarn_reg_hdr NE space.
    IF ( xzarn_reg_hdr[] IS INITIAL ) AND
       ( yzarn_reg_hdr[] IS INITIAL ).
      upd_zarn_reg_hdr = space.
    ENDIF.
  ENDIF.

  IF upd_zarn_reg_hdr NE space.
    CALL FUNCTION 'CHANGEDOCUMENT_MULTIPLE_CASE'
      EXPORTING
        tablename              = 'ZARN_REG_HDR'
        change_indicator       = upd_zarn_reg_hdr
        docu_delete            = ''
        docu_insert            = ''
        docu_delete_if         = ''
        docu_insert_if         = ''
      TABLES
        table_old              = yzarn_reg_hdr
        table_new              = xzarn_reg_hdr
      EXCEPTIONS
        nametab_error          = 1
        open_missing           = 2
        position_insert_failed = 3
        OTHERS                 = 4.

    CASE sy-subrc.
      WHEN 0.                                "OK.
      WHEN 1. MESSAGE a600 WITH 'NAMETAB-ERROR'.
      WHEN 2. MESSAGE a600 WITH 'OPEN MISSING'.
      WHEN 3. MESSAGE a600 WITH 'INSERT ERROR'.
      WHEN 4. MESSAGE a600 WITH 'MULTIPLE ERROR'.
    ENDCASE.
  ENDIF.

  IF upd_zarn_reg_hsno NE space.
    IF ( xzarn_reg_hsno[] IS INITIAL ) AND
       ( yzarn_reg_hsno[] IS INITIAL ).
      upd_zarn_reg_hsno = space.
    ENDIF.
  ENDIF.

  IF upd_zarn_reg_hsno NE space.
    CALL FUNCTION 'CHANGEDOCUMENT_MULTIPLE_CASE'
      EXPORTING
        tablename              = 'ZARN_REG_HSNO'
        change_indicator       = upd_zarn_reg_hsno
        docu_delete            = ''
        docu_insert            = ''
        docu_delete_if         = ''
        docu_insert_if         = ''
      TABLES
        table_old              = yzarn_reg_hsno
        table_new              = xzarn_reg_hsno
      EXCEPTIONS
        nametab_error          = 1
        open_missing           = 2
        position_insert_failed = 3
        OTHERS                 = 4.

    CASE sy-subrc.
      WHEN 0.                                "OK.
      WHEN 1. MESSAGE a600 WITH 'NAMETAB-ERROR'.
      WHEN 2. MESSAGE a600 WITH 'OPEN MISSING'.
      WHEN 3. MESSAGE a600 WITH 'INSERT ERROR'.
      WHEN 4. MESSAGE a600 WITH 'MULTIPLE ERROR'.
    ENDCASE.
  ENDIF.

  IF upd_zarn_reg_lst_prc NE space.
    IF ( xzarn_reg_lst_prc[] IS INITIAL ) AND
       ( yzarn_reg_lst_prc[] IS INITIAL ).
      upd_zarn_reg_lst_prc = space.
    ENDIF.
  ENDIF.

  IF upd_zarn_reg_lst_prc NE space.
    CALL FUNCTION 'CHANGEDOCUMENT_MULTIPLE_CASE'
      EXPORTING
        tablename              = 'ZARN_REG_LST_PRC'
        change_indicator       = upd_zarn_reg_lst_prc
        docu_delete            = ''
        docu_insert            = ''
        docu_delete_if         = ''
        docu_insert_if         = ''
      TABLES
        table_old              = yzarn_reg_lst_prc
        table_new              = xzarn_reg_lst_prc
      EXCEPTIONS
        nametab_error          = 1
        open_missing           = 2
        position_insert_failed = 3
        OTHERS                 = 4.

    CASE sy-subrc.
      WHEN 0.                                "OK.
      WHEN 1. MESSAGE a600 WITH 'NAMETAB-ERROR'.
      WHEN 2. MESSAGE a600 WITH 'OPEN MISSING'.
      WHEN 3. MESSAGE a600 WITH 'INSERT ERROR'.
      WHEN 4. MESSAGE a600 WITH 'MULTIPLE ERROR'.
    ENDCASE.
  ENDIF.

  IF upd_zarn_reg_onlcat NE space.
    IF ( xzarn_reg_onlcat[] IS INITIAL ) AND
       ( yzarn_reg_onlcat[] IS INITIAL ).
      upd_zarn_reg_onlcat = space.
    ENDIF.
  ENDIF.

  IF upd_zarn_reg_onlcat NE space.
    CALL FUNCTION 'CHANGEDOCUMENT_MULTIPLE_CASE'
      EXPORTING
        tablename              = 'ZARN_REG_ONLCAT'
        change_indicator       = upd_zarn_reg_onlcat
        docu_delete            = ''
        docu_insert            = ''
        docu_delete_if         = ''
        docu_insert_if         = ''
      TABLES
        table_old              = yzarn_reg_onlcat
        table_new              = xzarn_reg_onlcat
      EXCEPTIONS
        nametab_error          = 1
        open_missing           = 2
        position_insert_failed = 3
        OTHERS                 = 4.

    CASE sy-subrc.
      WHEN 0.                                "OK.
      WHEN 1. MESSAGE a600 WITH 'NAMETAB-ERROR'.
      WHEN 2. MESSAGE a600 WITH 'OPEN MISSING'.
      WHEN 3. MESSAGE a600 WITH 'INSERT ERROR'.
      WHEN 4. MESSAGE a600 WITH 'MULTIPLE ERROR'.
    ENDCASE.
  ENDIF.

  IF upd_zarn_reg_pir NE space.
    IF ( xzarn_reg_pir[] IS INITIAL ) AND
       ( yzarn_reg_pir[] IS INITIAL ).
      upd_zarn_reg_pir = space.
    ENDIF.
  ENDIF.

  IF upd_zarn_reg_pir NE space.
    CALL FUNCTION 'CHANGEDOCUMENT_MULTIPLE_CASE'
      EXPORTING
        tablename              = 'ZARN_REG_PIR'
        change_indicator       = upd_zarn_reg_pir
        docu_delete            = ''
        docu_insert            = ''
        docu_delete_if         = ''
        docu_insert_if         = ''
      TABLES
        table_old              = yzarn_reg_pir
        table_new              = xzarn_reg_pir
      EXCEPTIONS
        nametab_error          = 1
        open_missing           = 2
        position_insert_failed = 3
        OTHERS                 = 4.

    CASE sy-subrc.
      WHEN 0.                                "OK.
      WHEN 1. MESSAGE a600 WITH 'NAMETAB-ERROR'.
      WHEN 2. MESSAGE a600 WITH 'OPEN MISSING'.
      WHEN 3. MESSAGE a600 WITH 'INSERT ERROR'.
      WHEN 4. MESSAGE a600 WITH 'MULTIPLE ERROR'.
    ENDCASE.
  ENDIF.

  IF upd_zarn_reg_prfam NE space.
    IF ( xzarn_reg_prfam[] IS INITIAL ) AND
       ( yzarn_reg_prfam[] IS INITIAL ).
      upd_zarn_reg_prfam = space.
    ENDIF.
  ENDIF.

  IF upd_zarn_reg_prfam NE space.
    CALL FUNCTION 'CHANGEDOCUMENT_MULTIPLE_CASE'
      EXPORTING
        tablename              = 'ZARN_REG_PRFAM'
        change_indicator       = upd_zarn_reg_prfam
        docu_delete            = ''
        docu_insert            = ''
        docu_delete_if         = ''
        docu_insert_if         = ''
      TABLES
        table_old              = yzarn_reg_prfam
        table_new              = xzarn_reg_prfam
      EXCEPTIONS
        nametab_error          = 1
        open_missing           = 2
        position_insert_failed = 3
        OTHERS                 = 4.

    CASE sy-subrc.
      WHEN 0.                                "OK.
      WHEN 1. MESSAGE a600 WITH 'NAMETAB-ERROR'.
      WHEN 2. MESSAGE a600 WITH 'OPEN MISSING'.
      WHEN 3. MESSAGE a600 WITH 'INSERT ERROR'.
      WHEN 4. MESSAGE a600 WITH 'MULTIPLE ERROR'.
    ENDCASE.
  ENDIF.

  IF upd_zarn_reg_rrp NE space.
    IF ( xzarn_reg_rrp[] IS INITIAL ) AND
       ( yzarn_reg_rrp[] IS INITIAL ).
      upd_zarn_reg_rrp = space.
    ENDIF.
  ENDIF.

  IF upd_zarn_reg_rrp NE space.
    CALL FUNCTION 'CHANGEDOCUMENT_MULTIPLE_CASE'
      EXPORTING
        tablename              = 'ZARN_REG_RRP'
        change_indicator       = upd_zarn_reg_rrp
        docu_delete            = ''
        docu_insert            = ''
        docu_delete_if         = ''
        docu_insert_if         = ''
      TABLES
        table_old              = yzarn_reg_rrp
        table_new              = xzarn_reg_rrp
      EXCEPTIONS
        nametab_error          = 1
        open_missing           = 2
        position_insert_failed = 3
        OTHERS                 = 4.

    CASE sy-subrc.
      WHEN 0.                                "OK.
      WHEN 1. MESSAGE a600 WITH 'NAMETAB-ERROR'.
      WHEN 2. MESSAGE a600 WITH 'OPEN MISSING'.
      WHEN 3. MESSAGE a600 WITH 'INSERT ERROR'.
      WHEN 4. MESSAGE a600 WITH 'MULTIPLE ERROR'.
    ENDCASE.
  ENDIF.

  IF upd_zarn_reg_std_ter NE space.
    IF ( xzarn_reg_std_ter[] IS INITIAL ) AND
       ( yzarn_reg_std_ter[] IS INITIAL ).
      upd_zarn_reg_std_ter = space.
    ENDIF.
  ENDIF.

  IF upd_zarn_reg_std_ter NE space.
    CALL FUNCTION 'CHANGEDOCUMENT_MULTIPLE_CASE'
      EXPORTING
        tablename              = 'ZARN_REG_STD_TER'
        change_indicator       = upd_zarn_reg_std_ter
        docu_delete            = ''
        docu_insert            = ''
        docu_delete_if         = ''
        docu_insert_if         = ''
      TABLES
        table_old              = yzarn_reg_std_ter
        table_new              = xzarn_reg_std_ter
      EXCEPTIONS
        nametab_error          = 1
        open_missing           = 2
        position_insert_failed = 3
        OTHERS                 = 4.

    CASE sy-subrc.
      WHEN 0.                                "OK.
      WHEN 1. MESSAGE a600 WITH 'NAMETAB-ERROR'.
      WHEN 2. MESSAGE a600 WITH 'OPEN MISSING'.
      WHEN 3. MESSAGE a600 WITH 'INSERT ERROR'.
      WHEN 4. MESSAGE a600 WITH 'MULTIPLE ERROR'.
    ENDCASE.
  ENDIF.

  IF upd_zarn_reg_str NE space.
    IF ( xzarn_reg_str IS INITIAL ) AND
       ( yzarn_reg_str IS INITIAL ).
      upd_zarn_reg_str = space.
    ENDIF.
  ENDIF.

  IF upd_zarn_reg_str NE space.
    CALL FUNCTION 'CHANGEDOCUMENT_MULTIPLE_CASE2'
      EXPORTING
        tablename              = 'ZARN_REG_STR'
        change_indicator       = upd_zarn_reg_str
        docu_delete            = ''
        docu_insert            = ''
        docu_delete_if         = ''
        docu_insert_if         = ''
        table_old              = yzarn_reg_str
        table_new              = xzarn_reg_str
      EXCEPTIONS
        nametab_error          = 1
        open_missing           = 2
        position_insert_failed = 3
        OTHERS                 = 4.

    CASE sy-subrc.
      WHEN 0.                                "OK.
      WHEN 1. MESSAGE a600 WITH 'NAMETAB-ERROR'.
      WHEN 2. MESSAGE a600 WITH 'OPEN MISSING'.
      WHEN 3. MESSAGE a600 WITH 'INSERT ERROR'.
      WHEN 4. MESSAGE a600 WITH 'MULTIPLE ERROR'.
    ENDCASE.
  ENDIF.

  IF upd_zarn_reg_txt NE space.
    IF ( xzarn_reg_txt[] IS INITIAL ) AND
       ( yzarn_reg_txt[] IS INITIAL ).
      upd_zarn_reg_txt = space.
    ENDIF.
  ENDIF.

  IF upd_zarn_reg_txt NE space.
    CALL FUNCTION 'CHANGEDOCUMENT_MULTIPLE_CASE'
      EXPORTING
        tablename              = 'ZARN_REG_TXT'
        change_indicator       = upd_zarn_reg_txt
        docu_delete            = ''
        docu_insert            = ''
        docu_delete_if         = ''
        docu_insert_if         = ''
      TABLES
        table_old              = yzarn_reg_txt
        table_new              = xzarn_reg_txt
      EXCEPTIONS
        nametab_error          = 1
        open_missing           = 2
        position_insert_failed = 3
        OTHERS                 = 4.

    CASE sy-subrc.
      WHEN 0.                                "OK.
      WHEN 1. MESSAGE a600 WITH 'NAMETAB-ERROR'.
      WHEN 2. MESSAGE a600 WITH 'OPEN MISSING'.
      WHEN 3. MESSAGE a600 WITH 'INSERT ERROR'.
      WHEN 4. MESSAGE a600 WITH 'MULTIPLE ERROR'.
    ENDCASE.
  ENDIF.

  IF upd_zarn_reg_uom NE space.
    IF ( xzarn_reg_uom[] IS INITIAL ) AND
       ( yzarn_reg_uom[] IS INITIAL ).
      upd_zarn_reg_uom = space.
    ENDIF.
  ENDIF.

  IF upd_zarn_reg_uom NE space.
    CALL FUNCTION 'CHANGEDOCUMENT_MULTIPLE_CASE'
      EXPORTING
        tablename              = 'ZARN_REG_UOM'
        change_indicator       = upd_zarn_reg_uom
        docu_delete            = ''
        docu_insert            = ''
        docu_delete_if         = ''
        docu_insert_if         = ''
      TABLES
        table_old              = yzarn_reg_uom
        table_new              = xzarn_reg_uom
      EXCEPTIONS
        nametab_error          = 1
        open_missing           = 2
        position_insert_failed = 3
        OTHERS                 = 4.

    CASE sy-subrc.
      WHEN 0.                                "OK.
      WHEN 1. MESSAGE a600 WITH 'NAMETAB-ERROR'.
      WHEN 2. MESSAGE a600 WITH 'OPEN MISSING'.
      WHEN 3. MESSAGE a600 WITH 'INSERT ERROR'.
      WHEN 4. MESSAGE a600 WITH 'MULTIPLE ERROR'.
    ENDCASE.
  ENDIF.

  IF upd_zarn_ver_status NE space.
    IF ( xzarn_ver_status[] IS INITIAL ) AND
       ( yzarn_ver_status[] IS INITIAL ).
      upd_zarn_ver_status = space.
    ENDIF.
  ENDIF.

  IF upd_zarn_ver_status NE space.
    CALL FUNCTION 'CHANGEDOCUMENT_MULTIPLE_CASE'
      EXPORTING
        tablename              = 'ZARN_VER_STATUS'
        change_indicator       = upd_zarn_ver_status
        docu_delete            = ''
        docu_insert            = ''
        docu_delete_if         = ''
        docu_insert_if         = ''
      TABLES
        table_old              = yzarn_ver_status
        table_new              = xzarn_ver_status
      EXCEPTIONS
        nametab_error          = 1
        open_missing           = 2
        position_insert_failed = 3
        OTHERS                 = 4.

    CASE sy-subrc.
      WHEN 0.                                "OK.
      WHEN 1. MESSAGE a600 WITH 'NAMETAB-ERROR'.
      WHEN 2. MESSAGE a600 WITH 'OPEN MISSING'.
      WHEN 3. MESSAGE a600 WITH 'INSERT ERROR'.
      WHEN 4. MESSAGE a600 WITH 'MULTIPLE ERROR'.
    ENDCASE.
  ENDIF.

  IF upd_zarn_reg_sc NE space.
    IF ( xzarn_reg_sc[] IS INITIAL ) AND
       ( yzarn_reg_sc[] IS INITIAL ).
      upd_zarn_reg_sc = space.
    ENDIF.
  ENDIF.

  IF upd_zarn_reg_sc NE space.
    CALL FUNCTION 'CHANGEDOCUMENT_MULTIPLE_CASE'
      EXPORTING
        tablename              = 'ZARN_REG_SC'
        change_indicator       = upd_zarn_reg_sc
        docu_delete            = ''
        docu_insert            = ''
        docu_delete_if         = ''
        docu_insert_if         = ''
      TABLES
        table_old              = yzarn_reg_sc
        table_new              = xzarn_reg_sc
      EXCEPTIONS
        nametab_error          = 1
        open_missing           = 2
        position_insert_failed = 3
        OTHERS                 = 4.

    CASE sy-subrc.
      WHEN 0.                                "OK.
      WHEN 1. MESSAGE a600 WITH 'NAMETAB-ERROR'.
      WHEN 2. MESSAGE a600 WITH 'OPEN MISSING'.
      WHEN 3. MESSAGE a600 WITH 'INSERT ERROR'.
      WHEN 4. MESSAGE a600 WITH 'MULTIPLE ERROR'.
    ENDCASE.
  ENDIF.

  IF upd_icdtxt_zarn_ctrl_regnl NE space.
    CALL FUNCTION 'CHANGEDOCUMENT_TEXT_CASE'
      TABLES
        texttable              = icdtxt_zarn_ctrl_regnl
      EXCEPTIONS
        open_missing           = 1
        position_insert_failed = 2
        OTHERS                 = 3.

    CASE sy-subrc.
      WHEN 0.                                "OK.
      WHEN 1. MESSAGE a600 WITH 'OPEN MISSING'.
      WHEN 2. MESSAGE a600 WITH 'INSERT ERROR'.
      WHEN 3. MESSAGE a600 WITH 'TEXT ERROR'.
    ENDCASE.
  ENDIF.

  IF upd_zarn_reg_cluster NE space.
    IF ( xzarn_reg_cluster[] IS INITIAL ) AND
       ( yzarn_reg_cluster[] IS INITIAL ).
      upd_zarn_reg_cluster = space.
    ENDIF.
  ENDIF.

  IF upd_zarn_reg_cluster NE space.
    CALL FUNCTION 'CHANGEDOCUMENT_MULTIPLE_CASE'
      EXPORTING
        tablename              = 'ZARN_REG_CLUSTER'
        change_indicator       = upd_zarn_reg_cluster
        docu_delete            = ''
        docu_insert            = ''
        docu_delete_if         = ''
        docu_insert_if         = ''
      TABLES
        table_old              = yzarn_reg_cluster
        table_new              = xzarn_reg_cluster
      EXCEPTIONS
        nametab_error          = 1
        open_missing           = 2
        position_insert_failed = 3
        OTHERS                 = 4.

    CASE sy-subrc.
      WHEN 0.                                "OK.
      WHEN 1. MESSAGE a600 WITH 'NAMETAB-ERROR'.
      WHEN 2. MESSAGE a600 WITH 'OPEN MISSING'.
      WHEN 3. MESSAGE a600 WITH 'INSERT ERROR'.
      WHEN 4. MESSAGE a600 WITH 'MULTIPLE ERROR'.
    ENDCASE.
  ENDIF.

  IF upd_zarn_reg_nutrien NE space.
    IF ( xzarn_reg_nutrien[] IS INITIAL ) AND
       ( yzarn_reg_nutrien[] IS INITIAL ).
      upd_zarn_reg_nutrien = space.
    ENDIF.
  ENDIF.

  IF upd_zarn_reg_nutrien NE space.
    CALL FUNCTION 'CHANGEDOCUMENT_MULTIPLE_CASE'
      EXPORTING
        tablename              = 'ZARN_REG_NUTRIEN'
        change_indicator       = upd_zarn_reg_nutrien
        docu_delete            = ''
        docu_insert            = ''
        docu_delete_if         = ''
        docu_insert_if         = ''
      TABLES
        table_old              = yzarn_reg_nutrien
        table_new              = xzarn_reg_nutrien
      EXCEPTIONS
        nametab_error          = 1
        open_missing           = 2
        position_insert_failed = 3
        OTHERS                 = 4.

    CASE sy-subrc.
      WHEN 0.                                "OK.
      WHEN 1. MESSAGE a600 WITH 'NAMETAB-ERROR'.
      WHEN 2. MESSAGE a600 WITH 'OPEN MISSING'.
      WHEN 3. MESSAGE a600 WITH 'INSERT ERROR'.
      WHEN 4. MESSAGE a600 WITH 'MULTIPLE ERROR'.
    ENDCASE.
  ENDIF.

  CALL FUNCTION 'CHANGEDOCUMENT_CLOSE'
    EXPORTING
      objectclass             = 'ZARN_CTRL_REGNL'
      objectid                = objectid
      date_of_change          = udate
      time_of_change          = utime
      tcode                   = tcode
      username                = username
      object_change_indicator = object_change_indicator
      no_change_pointers      = no_change_pointers
    IMPORTING
      changenumber            = ev_changenumber
    EXCEPTIONS
      header_insert_failed    = 1
      object_invalid          = 2
      open_missing            = 3
      no_position_inserted    = 4
      OTHERS                  = 5.

  CASE sy-subrc.
    WHEN 0.                                   "OK.
    WHEN 1. MESSAGE a600 WITH 'INSERT HEADER FAILED'.
    WHEN 2. MESSAGE a600 WITH 'OBJECT INVALID'.
    WHEN 3. MESSAGE a600 WITH 'OPEN MISSING'.
*    WHEN 4. MESSAGE A600 WITH 'NO_POSITION_INSERTED'.
* do not abort, if positions are not inserted!!!
    WHEN 5. MESSAGE a600 WITH 'CLOSE ERROR'.
  ENDCASE.

ENDFUNCTION.
