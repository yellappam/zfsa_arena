*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Functional Specification 3113
* DATE WRITTEN     # 17112015
* SAP VERSION      # 731
* TYPE             # Function Module
* AUTHOR           # C90001363, Jitin Kharbanda
*-----------------------------------------------------------------------
* TITLE            # Update data into DB
* PURPOSE          # Inbound Data Update from PI to Version and National
*                  # Tables
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------
FUNCTION zarn_inbound_data_update .
*"----------------------------------------------------------------------
*"*"Update Function Module:
*"
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(IT_CONTROL) TYPE  ZTARN_CONTROL
*"     VALUE(IT_PRD_VERSION) TYPE  ZTARN_PRD_VERSION
*"     VALUE(IT_VER_STATUS) TYPE  ZTARN_VER_STATUS
*"     VALUE(IT_PROD_DATA) TYPE  ZTARN_PROD_DATA
*"     VALUE(IT_REG_DATA) TYPE  ZTARN_REG_DATA
*"     VALUE(IT_CONTROL_OLD) TYPE  ZTARN_CONTROL OPTIONAL
*"     VALUE(IT_PRD_VERSION_OLD) TYPE  ZTARN_PRD_VERSION OPTIONAL
*"     VALUE(IT_VER_STATUS_OLD) TYPE  ZTARN_VER_STATUS OPTIONAL
*"     VALUE(IT_REG_DATA_OLD) TYPE  ZTARN_REG_DATA OPTIONAL
*"     VALUE(IT_CC_HDR) TYPE  ZTARN_CC_HDR OPTIONAL
*"     VALUE(IT_CC_DET) TYPE  ZTARN_CC_DET OPTIONAL
*"     VALUE(IT_APPROVAL) TYPE  ZARN_T_APPROVAL OPTIONAL
*"     VALUE(IT_APPROVAL_CH) TYPE  ZARN_T_APPROVAL_CH OPTIONAL
*"     VALUE(IT_APPROVAL_CH_DEL) TYPE  ZARN_T_APPROVAL_CH OPTIONAL
*"     VALUE(IT_APPR_HIST) TYPE  ZARN_T_APPR_HIST OPTIONAL
*"     VALUE(IV_CHGID) TYPE  ZARN_CHG_ID OPTIONAL
*"     VALUE(IT_IDNO_LOCKED) TYPE  ZTARN_NATIONAL_LOCK OPTIONAL
*"     VALUE(IV_NO_CHANGE_DOC) TYPE  FLAG DEFAULT SPACE
*"     VALUE(IV_NO_CHG_CAT) TYPE  FLAG DEFAULT SPACE
*"----------------------------------------------------------------------

  DATA: lt_idno_locked TYPE ztarn_national_lock,
        ls_idno_locked TYPE zsarn_national_lock.

*>>>IS1609ins Feature 3162 - AReNa Approval Stabilisation
  DATA: lo_approvals  TYPE REF TO zcl_arn_approval_backend.
*<<<IS1609ins Feature 3162 - AReNa Approval Stabilisation



* Update Version and Control tables
  CALL FUNCTION 'ZARN_VERSION_CONTROL_UPDATE'
    EXPORTING
      it_control     = it_control[]
      it_prd_version = it_prd_version[]
      it_ver_status  = it_ver_status[].



* National Data Update from PI into DB
  CALL FUNCTION 'ZARN_NATIONAL_DATA_UPDATE'
    EXPORTING
      it_prod_data = it_prod_data[].

* Regional Data Update
  CALL FUNCTION 'ZARN_REGIONAL_DATA_UPDATE'
    EXPORTING
      it_reg_data = it_reg_data[].


* Write Change Document for Control and Regional Data
  IF iv_no_change_doc EQ space.
    CALL FUNCTION 'ZARN_CTRL_REGNL_CHANGE_DOC'
      EXPORTING
*       IT_CONTROL         =
        it_prd_version     = it_prd_version[]
        it_ver_status      = it_ver_status[]
        it_reg_data        = it_reg_data[]
        it_prod_data       = it_prod_data[]
*       IT_CONTROL_OLD     =
        it_prd_version_old = it_prd_version_old[]
        it_ver_status_old  = it_ver_status_old[]
        it_reg_data_old    = it_reg_data_old[]
*       IV_CI_CONTROL      = ' '
        iv_ci_prd_version  = abap_true
        iv_ci_ver_status   = abap_true
        iv_ci_reg_hdr      = abap_true
        iv_ci_reg_data     = abap_true.
  ENDIF.


*>>>IS1609ins Feature 3162 - AReNa Approval Stabilisation

  "Approval stabilisation - remove CC TEAM from
  "function modules below will not be used

  "Logic of Change Categories and Approvals will be decoupled

  IF iv_no_chg_cat = space.
* Update Change Category tables
    CALL FUNCTION 'ZARN_CHANGE_CATEGORY_UPDATE'
      EXPORTING
        it_cc_hdr  = it_cc_hdr[]
        it_cc_det  = it_cc_det[].

* Write Change Document for Change Category Data
    IF iv_no_change_doc EQ space.
      CALL FUNCTION 'ZARN_CHG_CAT_CHANGE_DOC'
        EXPORTING
          it_cc_hdr  = it_cc_hdr[]
*         it_cc_hdr_old  =
          it_cc_det  = it_cc_det[]
*         it_cc_det_old  =
          iv_chgid   = iv_chgid.

    ENDIF.
  ENDIF.   " IF iv_no_chg_cat = space


  lo_approvals = NEW #( iv_approval_event = zcl_arn_approval_backend=>gc_event_inbound ).
  lo_approvals->post_approvals( EXPORTING it_approval    = it_approval
                                          it_approval_ch = it_approval_ch
                                          it_appr_hist   = it_appr_hist
                                          it_approval_ch_del = it_approval_ch_del ).

*<<<IS1609ins Feature 3162 - AReNa Approval Stabilisation

  lt_idno_locked[] = it_idno_locked[].

* DeQueue Locked IDNO
  LOOP AT lt_idno_locked[] INTO ls_idno_locked.
    CALL FUNCTION 'DEQUEUE_EZARN_NATIONAL'
      EXPORTING
        idno = ls_idno_locked-idno.
  ENDLOOP.

ENDFUNCTION.
