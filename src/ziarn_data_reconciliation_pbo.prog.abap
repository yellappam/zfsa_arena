*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_DATA_RECONCILIATION_PBO
*&---------------------------------------------------------------------*

*&---------------------------------------------------------------------*
*&      Module  STATUS_9000  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE status_9000 OUTPUT.

  CONSTANTS : lc_pf     TYPE char20 VALUE 'ZPF_ARN_RECON',
              lc_tb_dsp TYPE char20 VALUE 'ZTB_ARN_RECON_DSP',
              lc_tb_ecc TYPE char20 VALUE 'ZTB_ARN_RECON_ECC'.

* Set PF status and title bar
  SET PF-STATUS lc_pf.

  IF gv_mode = gc_mode-dsp.
    SET TITLEBAR  lc_tb_dsp.
  ELSEIF gv_mode = gc_mode-ecc.
    SET TITLEBAR  lc_tb_ecc.
  ENDIF.

ENDMODULE.                 " STATUS_9000  OUTPUT

*&SPWIZARD: OUTPUT MODULE FOR TS 'TS_RECON'. DO NOT CHANGE THIS LINE!
*&SPWIZARD: SETS ACTIVE TAB
MODULE ts_recon_active_tab_set OUTPUT.
  ts_recon-activetab = g_ts_recon-pressed_tab.
  CASE g_ts_recon-pressed_tab.
    WHEN c_ts_recon-tab1.
      g_ts_recon-subscreen = '9001'.
    WHEN c_ts_recon-tab2.
      g_ts_recon-subscreen = '9002'.
    WHEN c_ts_recon-tab3.
      g_ts_recon-subscreen = '9003'.
    WHEN c_ts_recon-tab4.
      g_ts_recon-subscreen = '9004'.
    WHEN c_ts_recon-tab5.
      g_ts_recon-subscreen = '9005'.
    WHEN c_ts_recon-tab6.
      g_ts_recon-subscreen = '9006'.
    WHEN c_ts_recon-tab7.
      g_ts_recon-subscreen = '9007'.
    WHEN c_ts_recon-tab8.
      g_ts_recon-subscreen = '9008'.

    WHEN OTHERS.
*&SPWIZARD:      DO NOTHING
  ENDCASE.
ENDMODULE.                    "TS_RECON_ACTIVE_TAB_SET OUTPUT
*&---------------------------------------------------------------------*
*&      Module  DISPLAY_ALV_9001  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE display_alv_9001 OUTPUT.

* Display ALV 9001
  PERFORM display_alv_9001.

ENDMODULE.                 " DISPLAY_ALV_9001  OUTPUT
*&---------------------------------------------------------------------*
*&      Module  DISPLAY_ALV_9002  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE display_alv_9002 OUTPUT.

* Display ALV 9002
  PERFORM display_alv_9002.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  DISPLAY_ALV_9003  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE display_alv_9003 OUTPUT.

* Display ALV 9003
  PERFORM display_alv_9003.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  DISPLAY_ALV_9004  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE display_alv_9004 OUTPUT.

* Display ALV 9004
  PERFORM display_alv_9004.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  DISPLAY_ALV_9005  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE display_alv_9005 OUTPUT.

* Display ALV 9005
  PERFORM display_alv_9005.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  DISPLAY_ALV_9006  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE display_alv_9006 OUTPUT.

* Display ALV 9006
  PERFORM display_alv_9006.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  DISPLAY_ALV_9007  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE display_alv_9007 OUTPUT.

* Display ALV 9007
  PERFORM display_alv_9007.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  DISPLAY_ALV_9008  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE display_alv_9008 OUTPUT.

* Display ALV 9008
  PERFORM display_alv_9008.

ENDMODULE.
