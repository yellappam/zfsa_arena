*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_DATA_RECONCILIATION_TOP
*&---------------------------------------------------------------------*



TYPES:
* IDNO Data
  ty_s_idno_data   TYPE zsarn_idno_data,
  ty_t_idno_data   TYPE SORTED TABLE OF ty_s_idno_data WITH UNIQUE KEY idno,

** Main FAN List
  ty_s_fan         TYPE zarn_fan_id_s,
  ty_t_fan         TYPE zarn_fan_id_t,

* Fan Range
  ty_t_fan_range   TYPE RANGE OF zarn_fan_id,

* FAN Exist
  ty_s_fan_exist   TYPE zsarn_data_reco_fan_exist,
  ty_t_fan_exist   TYPE ztarn_data_reco_fan_exist,

* Reconciliation BASIC 1 Data
  ty_s_reco_basic1 TYPE zsarn_data_reco_basic1,
  ty_t_reco_basic1 TYPE ztarn_data_reco_basic1,

* Reconciliation BASIC 2 Data
  ty_s_reco_basic2 TYPE zsarn_data_reco_basic2,
  ty_t_reco_basic2 TYPE ztarn_data_reco_basic2,

* Reconciliation Attributes Data
  ty_s_reco_attr   TYPE zsarn_data_reco_attributes,
  ty_t_reco_attr   TYPE ztarn_data_reco_attributes,

* Reconciliation UOM Data
  ty_s_reco_uom    TYPE zsarn_data_reco_uom,
  ty_t_reco_uom    TYPE ztarn_data_reco_uom,

* Reconciliation PIR Data
  ty_s_reco_pir    TYPE zsarn_data_reco_pir,
  ty_t_reco_pir    TYPE ztarn_data_reco_pir,

* Reconciliation EAN Data
  ty_s_reco_ean    TYPE zsarn_data_reco_ean,
  ty_t_reco_ean    TYPE ztarn_data_reco_ean,

* Reconciliation BANNER Data
  ty_s_reco_banner TYPE zsarn_data_reco_banner,
  ty_t_reco_banner TYPE ztarn_data_reco_banner,


* National Products
  ty_s_products    TYPE zarn_products,
  ty_t_products    TYPE SORTED TABLE OF ty_s_products WITH UNIQUE KEY idno version,

* National Data
  ty_s_prod_data   TYPE zsarn_prod_data,
  ty_t_prod_data   TYPE ztarn_prod_data,

* Regional Data
  ty_s_reg_data    TYPE zsarn_reg_data,
  ty_t_reg_data    TYPE ztarn_reg_data,

* Units of Measurement
  ty_s_t006        TYPE t006,
  ty_t_t006        TYPE SORTED TABLE OF ty_s_t006 WITH UNIQUE KEY msehi,

* Countries
  ty_s_t005        TYPE t005,
  ty_t_t005        TYPE SORTED TABLE OF ty_s_t005 WITH UNIQUE KEY land1,

* Currency
  ty_s_tcurc       TYPE tcurc,
  ty_t_tcurc       TYPE SORTED TABLE OF ty_s_tcurc WITH UNIQUE KEY waers,

* Article Types
  ty_s_t134        TYPE t134,
  ty_t_t134        TYPE SORTED TABLE OF ty_s_t134 WITH NON-UNIQUE KEY mtart,

* UOM Category
  ty_s_uom_cat     TYPE zarn_uom_cat,
  ty_t_uom_cat     TYPE SORTED TABLE OF ty_s_uom_cat WITH UNIQUE KEY uom_category seqno uom,

* Country of Origin Text
  ty_t_coo_t       TYPE SORTED TABLE OF zarn_coo_t WITH UNIQUE KEY country_of_origin spras,
  ty_t_gen_uom_t   TYPE SORTED TABLE OF zarn_gen_uom_t WITH UNIQUE KEY gen_uom spras,

* MARM-IDNO Data
  BEGIN OF ty_s_marm_idno,
    idno         TYPE zarn_idno,
    meinh        TYPE lrmei,
    umrez        TYPE umrez,
    umren        TYPE umren,
    mesub        TYPE mesub,
    matnr        TYPE matnr,
    uom_category TYPE zarn_uom_category,
    seqno        TYPE zarn_seqno,
    uom          TYPE meinh,
    cat_seqno    TYPE zarn_cat_seqno,
  END OF ty_s_marm_idno ,
  ty_t_marm_idno   TYPE STANDARD TABLE OF ty_s_marm_idno,

* MARA
  ty_t_mara        TYPE SORTED TABLE OF mara WITH UNIQUE KEY matnr,

* MARC
  ty_t_marc        TYPE SORTED TABLE OF marc WITH UNIQUE KEY matnr werks,

* MAKT
  ty_t_makt        TYPE SORTED TABLE OF makt WITH UNIQUE KEY matnr,

* MAW1
  ty_t_maw1        TYPE SORTED TABLE OF maw1 WITH UNIQUE KEY matnr,

* MARM
  ty_t_marm        TYPE SORTED TABLE OF marm WITH UNIQUE KEY matnr meinh,

* MEAN
  ty_t_mean        TYPE SORTED TABLE OF mean WITH UNIQUE KEY matnr meinh lfnum,

* EINA
  ty_t_eina        TYPE SORTED TABLE OF eina WITH NON-UNIQUE KEY matnr lifnr,

* EINE
  ty_t_eine        TYPE SORTED TABLE OF eine WITH NON-UNIQUE KEY infnr ekorg,

* MVKE
  ty_t_mvke        TYPE SORTED TABLE OF mvke WITH UNIQUE KEY matnr vkorg vtweg,

* WLK2
  ty_t_wlk2        TYPE SORTED TABLE OF wlk2 WITH UNIQUE KEY matnr vkorg vtweg,

* TVARVC EKORG
  ty_r_ekorg       TYPE RANGE OF ekorg,

* TVARVC VKORG
  ty_r_vkorg       TYPE RANGE OF vkorg,

* Reference Article
  ty_s_ref_article TYPE mara,
  ty_t_ref_article TYPE SORTED TABLE OF ty_s_ref_article WITH NON-UNIQUE KEY matkl,

* VEndor Purch Org
  ty_s_lfm1        TYPE lfm1,
  ty_t_lfm1        TYPE SORTED TABLE OF ty_s_lfm1 WITH UNIQUE KEY lifnr ekorg,

* FAN ID - MARA
  ty_s_fanid       TYPE mara,
  ty_t_fanid       TYPE SORTED TABLE OF ty_s_fanid WITH NON-UNIQUE KEY zzfan,

* Product UOM Text
  ty_s_prod_uom_t  TYPE zarn_prod_uom_t,
  ty_t_prod_uom_t  TYPE SORTED TABLE OF ty_s_prod_uom_t WITH UNIQUE KEY product_uom,

* UOM Category Sequence
  ty_s_uomcategory TYPE zarn_uomcategory,
  ty_t_uomcategory TYPE SORTED TABLE OF ty_s_uomcategory WITH UNIQUE KEY uom_category,

* Host Product Type
  ty_s_host_prdtyp TYPE zmd_host_prdtyp,
  ty_t_host_prdtyp TYPE SORTED TABLE OF ty_s_host_prdtyp WITH UNIQUE KEY wwgha,

* MC Sub Department
  ty_s_mc_subdep   TYPE zmd_mc_subdep,
  ty_t_mc_subdep   TYPE SORTED TABLE OF ty_s_mc_subdep WITH UNIQUE KEY matkl,

** PIR already added
*  BEGIN OF ty_s_added_pir,
*    uom_code              TYPE zarn_uom_code,
*    base_units_per_pallet  TYPE zarn_base_units_per_pallet,
*    qty_layers_per_pallet  TYPE zarn_qty_layers_per_pallet,
*    qty_units_per_pallet  TYPE zarn_qty_units_per_pallet,
*  END OF ty_s_added_pir ,
*  ty_t_added_pir TYPE SORTED TABLE OF ty_s_added_pir
*                                           WITH NON-UNIQUE KEY uom_code
*                                                               base_units_per_pallet
*                                                               qty_layers_per_pallet
*                                                               qty_units_per_pallet ,


* PIR Vendor
  BEGIN OF ty_s_pir_vendor,
    bbbnr TYPE bbbnr,
    bbsnr TYPE bbsnr,
    bubkz TYPE bubkz,
    lifnr TYPE lifnr,
  END OF ty_s_pir_vendor ,
  ty_t_pir_vendor TYPE SORTED TABLE OF ty_s_pir_vendor WITH NON-UNIQUE KEY bbbnr bbsnr bubkz.










DATA:                      gt_fan                     TYPE ty_t_fan,
                           gt_idno_data               TYPE ty_t_idno_data,
                           gt_prod_data               TYPE ty_t_prod_data,
                           gt_reg_data                TYPE ty_t_reg_data,
                           gt_reg_data_def            TYPE ty_t_reg_data,
                           gs_prod_data_all           TYPE ty_s_prod_data,
                           gs_reg_data_all            TYPE ty_s_reg_data,

                           gt_fan_exist               TYPE ty_t_fan_exist,
                           gt_reco_basic1             TYPE ty_t_reco_basic1,
                           gt_reco_basic2             TYPE ty_t_reco_basic2,
                           gt_reco_attr               TYPE ty_t_reco_attr,
                           gt_reco_uom                TYPE ty_t_reco_uom,
                           gt_reco_pir                TYPE ty_t_reco_pir,
                           gt_reco_ean                TYPE ty_t_reco_ean,
                           gt_reco_banner             TYPE ty_t_reco_banner,

                           gt_output_9006             TYPE ty_t_reco_pir,

                           gt_mara                    TYPE ty_t_mara,
                           gt_marc                    TYPE ty_t_marc,
                           gt_makt                    TYPE ty_t_makt,
                           gt_maw1                    TYPE ty_t_maw1,
                           gt_marm                    TYPE ty_t_marm,
                           gt_marm_idno               TYPE ty_t_marm_idno,
                           gt_mean                    TYPE ty_t_mean,
                           gt_eina                    TYPE ty_t_eina,
                           gt_eine                    TYPE ty_t_eine,
                           gt_mvke                    TYPE ty_t_mvke,
                           gt_wlk2                    TYPE ty_t_wlk2,

                           gt_t006                    TYPE ty_t_t006,
                           gt_t005                    TYPE ty_t_t005,
                           gt_tcurc                   TYPE ty_t_tcurc,
                           gt_coo_t                   TYPE ty_t_coo_t,
                           gt_gen_uom_t               TYPE ty_t_gen_uom_t,
                           gt_t134                    TYPE ty_t_t134,
                           gr_tvarvc_ekorg            TYPE ty_r_ekorg,
                           gr_tvarvc_vkorg            TYPE ty_r_vkorg,
                           gt_tvarvc_p_org            TYPE tvarvc_t,
                           gt_tvarvc_uni_lni          TYPE tvarvc_t,
                           gt_tvarvc_hyb_code         TYPE tvarvc_t,
                           gt_tvta                    TYPE tvta_tt,

                           gt_lfm1                    TYPE ty_t_lfm1,
                           gt_fanid                   TYPE ty_t_fanid,
                           gt_prod_uom_t              TYPE ty_t_prod_uom_t,
                           gt_uom_cat                 TYPE ty_t_uom_cat,
                           gt_uomcategory             TYPE ty_t_uomcategory,
                           gt_ref_article             TYPE ty_t_ref_article,
                           gt_pir_vendor              TYPE ty_t_pir_vendor,
                           gt_tvarv_ean_cate          TYPE tvarvc_t,
                           gt_host_prdtyp             TYPE ty_t_host_prdtyp,
                           gt_mc_subdep               TYPE ty_t_mc_subdep,

                           gt_headdata_arn            TYPE mat_bapie1mathead_tty,
                           gt_clientdata_arn          TYPE bapie1marart_tab,
                           gt_clientext_arn           TYPE bapie1maraextrt_tab,
                           gt_addnlclientdata_arn     TYPE bapie1maw1rt_tab,
                           gt_materialdescription_arn TYPE bapie1maktrt_tab,
                           gt_unitsofmeasure_arn      TYPE bapie1marmrt_tab,
                           gt_internationalartno_arn  TYPE bapie1meanrt_tab,
                           gt_inforecord_general_arn  TYPE wrf_bapieina_tty,
                           gt_inforecord_purchorg_arn TYPE wrf_bapieine_tty,
                           gt_salesdata_arn           TYPE bapie1mvkert_tab,
                           gt_salesext_arn            TYPE bapie1mvkeextrt_tab,
                           gt_posdata_arn             TYPE bapie1wlk2rt_tab,
                           gt_plantdata_arn           TYPE bapie1marcrt_tab,
                           gt_plantext_arn            TYPE bapie1marcextrt_tab,     "++ONLD-822 JKH 17.02.2017
                           gt_bapi_clientext_arn      TYPE zmd_t_bapi_clientext,
                           gt_bapi_salesext_arn       TYPE zmd_t_bapi_salesext,
                           gt_bapi_plantext_arn       TYPE zmd_t_bapi_plantext,     "++ONLD-822 JKH 17.02.2017


                           gt_headdata                TYPE mat_bapie1mathead_tty,
                           gt_clientdata              TYPE bapie1marart_tab,
                           gt_clientext               TYPE bapie1maraextrt_tab,
                           gt_addnlclientdata         TYPE bapie1maw1rt_tab,
                           gt_materialdescription     TYPE bapie1maktrt_tab,
                           gt_unitsofmeasure          TYPE bapie1marmrt_tab,
                           gt_internationalartno      TYPE bapie1meanrt_tab,
                           gt_inforecord_general      TYPE wrf_bapieina_tty,
                           gt_inforecord_purchorg     TYPE wrf_bapieine_tty,
                           gt_salesdata               TYPE bapie1mvkert_tab,
                           gt_salesext                TYPE bapie1mvkeextrt_tab,
                           gt_posdata                 TYPE bapie1wlk2rt_tab,
                           gt_plantdata               TYPE bapie1marcrt_tab,
                           gt_plantext                TYPE bapie1marcextrt_tab,     "++ONLD-822 JKH 17.02.2017
                           gt_bapi_clientext          TYPE zmd_t_bapi_clientext,
                           gt_bapi_salesext           TYPE zmd_t_bapi_salesext,
                           gt_bapi_plantext           TYPE zmd_t_bapi_plantext,     "++ONLD-822 JKH 17.02.2017



                           go_gui_load                TYPE REF TO zcl_arn_gui_load,

                           gv_mode                    TYPE char1,   " D-DSP, E-ECC
                           gv_pir_ekorg               TYPE ekorg,
                           gv_error                   TYPE flag,




                           go_container_9001          TYPE REF TO cl_gui_custom_container,
                           go_container_9002          TYPE REF TO cl_gui_custom_container,
                           go_container_9003          TYPE REF TO cl_gui_custom_container,
                           go_container_9004          TYPE REF TO cl_gui_custom_container,
                           go_container_9005          TYPE REF TO cl_gui_custom_container,
                           go_container_9006          TYPE REF TO cl_gui_custom_container,
                           go_container_9007          TYPE REF TO cl_gui_custom_container,
                           go_container_9008          TYPE REF TO cl_gui_custom_container,


                           go_alvgrid_9001            TYPE REF TO cl_gui_alv_grid,
                           go_alvgrid_9002            TYPE REF TO cl_gui_alv_grid,
                           go_alvgrid_9003            TYPE REF TO cl_gui_alv_grid,
                           go_alvgrid_9004            TYPE REF TO cl_gui_alv_grid,
                           go_alvgrid_9005            TYPE REF TO cl_gui_alv_grid,
                           go_alvgrid_9006            TYPE REF TO cl_gui_alv_grid,
                           go_alvgrid_9007            TYPE REF TO cl_gui_alv_grid,
                           go_alvgrid_9008            TYPE REF TO cl_gui_alv_grid,

                           gv_pir_rel_9006            TYPE xfeld,

                           gv_count_9002              TYPE i,
                           gv_count_9003              TYPE i,
                           gv_count_9004              TYPE i,
                           gv_count_9005              TYPE i,
                           gv_count_9006              TYPE i,
                           gv_count_9007              TYPE i,
                           gv_count_9008              TYPE i.











*&SPWIZARD: DATA FOR TABSTRIP 'TS_RECON'
CONTROLS:  ts_recon TYPE TABSTRIP.
DATA:      BEGIN OF g_ts_recon,
             subscreen   LIKE sy-dynnr,
             prog        LIKE sy-repid VALUE 'ZARN_DATA_RECONCILIATION',
             pressed_tab LIKE sy-ucomm VALUE c_ts_recon-tab1,
           END OF g_ts_recon.
DATA:      ok_code LIKE sy-ucomm.
