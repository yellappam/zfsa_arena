FUNCTION z_arn_approvals_get_chgcat_des.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(CHG_CATEGORY) TYPE  ZARN_CHG_CATEGORY
*"  EXPORTING
*"     REFERENCE(CHG_CAT_DESC) TYPE  ZARN_CHG_CAT_DESC
*"----------------------------------------------------------------------

  STATICS: lt_change_cats TYPE HASHED TABLE OF zarn_cc_desc WITH UNIQUE KEY chg_category.

  FIELD-SYMBOLS: <ls_chg_cats> LIKE LINE OF lt_change_cats.

  IF lt_change_cats[] IS INITIAL.
    SELECT *
      INTO TABLE lt_change_cats
      FROM zarn_cc_desc.          "#EC CI_NOWHERE
  ENDIF.

  CLEAR chg_cat_desc.

  READ TABLE lt_change_cats ASSIGNING <ls_chg_cats> WITH TABLE KEY chg_category = chg_category.
  IF sy-subrc EQ 0.
    chg_cat_desc = <ls_chg_cats>-chg_cat_desc.
  ENDIF.

ENDFUNCTION.
