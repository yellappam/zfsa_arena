FUNCTION-POOL zarn_change_category.         "MESSAGE-ID ..

* INCLUDE LZARN_CHANGE_CATEGORYD...          " Local class definition

INCLUDE ziarn_gui_top_approve.

INCLUDE lzarn_approvalsf01.







TYPES:
* Type for GLN to be set to NR from LP
  BEGIN OF ty_s_gln_nr,
    idno                 TYPE zarn_idno,
    version              TYPE zarn_version,
    gln                  TYPE zarn_gln,
    uom_code             TYPE zarn_uom_pim,
    hybris_internal_code TYPE zarn_hybris_internal_code,
    lower_uom            TYPE zarn_lower_uom,
    lower_child_uom      TYPE zarn_lower_child_uom,
    lifnr                TYPE elifn,
  END OF ty_s_gln_nr,
  ty_t_gln_nr TYPE STANDARD TABLE OF ty_s_gln_nr,

* Type for Vendor to be set to NR from LP
  BEGIN OF ty_s_vendor_nr,
    idno                 TYPE zarn_idno,
    version              TYPE zarn_version,
    gln                  TYPE zarn_gln,
    uom_code             TYPE zarn_uom_pim,
    hybris_internal_code TYPE zarn_hybris_internal_code,
    lower_uom            TYPE zarn_lower_uom,
    lower_child_uom      TYPE zarn_lower_child_uom,
    lifnr                TYPE elifn,
  END OF ty_s_vendor_nr,
  ty_t_vendor_nr TYPE STANDARD TABLE OF ty_s_vendor_nr,






* Vendors for GLN
  BEGIN OF ty_s_pir_vendor,
    bbbnr TYPE bbbnr,
    bbsnr TYPE bbsnr,
    bubkz TYPE bubkz,
    lifnr TYPE lifnr,
  END OF ty_s_pir_vendor .
TYPES:
  ty_t_pir_vendor TYPE SORTED TABLE OF ty_s_pir_vendor WITH NON-UNIQUE KEY bbbnr bbsnr bubkz.
