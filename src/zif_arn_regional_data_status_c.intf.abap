interface ZIF_ARN_REGIONAL_DATA_STATUS_C
  public .


  constants GC_NONE type ZARN_REG_STATUS value '' ##NO_TEXT.
  constants GC_NEW type ZARN_REG_STATUS value 'NEW' ##NO_TEXT.
  constants GC_CHANGE type ZARN_REG_STATUS value 'CHG' ##NO_TEXT.
  constants GC_NO_CHANGE type ZARN_REG_STATUS value 'NCH' ##NO_TEXT.
  constants GC_WORK_IN_PROGRESS type ZARN_REG_STATUS value 'WIP' ##NO_TEXT.
  constants GC_APPROVED type ZARN_REG_STATUS value 'APR' ##NO_TEXT.
  constants GC_CREATED type ZARN_REG_STATUS value 'CRE' ##NO_TEXT.
  constants GC_REJECTED type ZARN_REG_STATUS value 'REJ' ##NO_TEXT.
  constants GC_OBSOLETE type ZARN_REG_STATUS value 'OBS' ##NO_TEXT.
  constants GC_MIGRATION type ZARN_REG_STATUS value 'MIG' ##NO_TEXT.
  constants GC_SYNC_UP type ZARN_REG_STATUS value 'SYNC' ##NO_TEXT.
endinterface.
