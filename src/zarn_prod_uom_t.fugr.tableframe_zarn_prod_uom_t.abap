*---------------------------------------------------------------------*
*    program for:   TABLEFRAME_ZARN_PROD_UOM_T
*   generation date: 19.07.2016 at 11:09:54
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
FUNCTION TABLEFRAME_ZARN_PROD_UOM_T    .

  PERFORM TABLEFRAME TABLES X_HEADER X_NAMTAB DBA_SELLIST DPL_SELLIST
                            EXCL_CUA_FUNCT
                     USING  CORR_NUMBER VIEW_ACTION VIEW_NAME.

ENDFUNCTION.
