* regenerated at 23.02.2016 08:06:06 by  C90001448
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_TEAMSTOP.                    " Global Data
  INCLUDE LZARN_TEAMSUXX.                    " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_TEAMSF...                    " Subroutines
* INCLUDE LZARN_TEAMSO...                    " PBO-Modules
* INCLUDE LZARN_TEAMSI...                    " PAI-Modules
* INCLUDE LZARN_TEAMSE...                    " Events
* INCLUDE LZARN_TEAMSP...                    " Local class implement.
* INCLUDE LZARN_TEAMST99.                    " ABAP Unit tests
  INCLUDE LZARN_TEAMSF00                          . " subprograms
  INCLUDE LZARN_TEAMSI00                          . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
