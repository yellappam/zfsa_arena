* regenerated at 05.05.2016 16:03:49 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_LIQMKTSEG_TTOP.              " Global Data
  INCLUDE LZARN_LIQMKTSEG_TUXX.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_LIQMKTSEG_TF...              " Subroutines
* INCLUDE LZARN_LIQMKTSEG_TO...              " PBO-Modules
* INCLUDE LZARN_LIQMKTSEG_TI...              " PAI-Modules
* INCLUDE LZARN_LIQMKTSEG_TE...              " Events
* INCLUDE LZARN_LIQMKTSEG_TP...              " Local class implement.
* INCLUDE LZARN_LIQMKTSEG_TT99.              " ABAP Unit tests
  INCLUDE LZARN_LIQMKTSEG_TF00                    . " subprograms
  INCLUDE LZARN_LIQMKTSEG_TI00                    . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
