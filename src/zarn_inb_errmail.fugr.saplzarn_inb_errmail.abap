* regenerated at 16.05.2016 09:39:14 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_INB_ERRMAILTOP.              " Global Data
  INCLUDE LZARN_INB_ERRMAILUXX.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_INB_ERRMAILF...              " Subroutines
* INCLUDE LZARN_INB_ERRMAILO...              " PBO-Modules
* INCLUDE LZARN_INB_ERRMAILI...              " PAI-Modules
* INCLUDE LZARN_INB_ERRMAILE...              " Events
* INCLUDE LZARN_INB_ERRMAILP...              " Local class implement.
* INCLUDE LZARN_INB_ERRMAILT99.              " ABAP Unit tests
  INCLUDE LZARN_INB_ERRMAILF00                    . " subprograms
  INCLUDE LZARN_INB_ERRMAILI00                    . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
