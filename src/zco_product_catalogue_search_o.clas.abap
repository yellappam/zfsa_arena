class ZCO_PRODUCT_CATALOGUE_SEARCH_O definition
  public
  inheriting from CL_PROXY_CLIENT
  create public .

public section.

  methods CONSTRUCTOR
    importing
      !LOGICAL_PORT_NAME type PRX_LOGICAL_PORT_NAME optional
    raising
      CX_AI_SYSTEM_FAULT .
  methods PRODUCT_CATALOGUE_SEARCH_OUT
    importing
      !OUTPUT type ZCATALOGUE_SEARCH_REQ
    exporting
      !INPUT type ZCATALOGUE_SEARCH_RESP
    raising
      CX_AI_SYSTEM_FAULT .
protected section.
private section.
ENDCLASS.



CLASS ZCO_PRODUCT_CATALOGUE_SEARCH_O IMPLEMENTATION.


  method CONSTRUCTOR.

  super->constructor(
    class_name          = 'ZCO_PRODUCT_CATALOGUE_SEARCH_O'
    logical_port_name   = logical_port_name
  ).

  endmethod.


  method PRODUCT_CATALOGUE_SEARCH_OUT.

  data:
    ls_parmbind type abap_parmbind,
    lt_parmbind type abap_parmbind_tab.

  ls_parmbind-name = 'OUTPUT'.
  ls_parmbind-kind = cl_abap_objectdescr=>importing.
  get reference of OUTPUT into ls_parmbind-value.
  insert ls_parmbind into table lt_parmbind.

  ls_parmbind-name = 'INPUT'.
  ls_parmbind-kind = cl_abap_objectdescr=>exporting.
  get reference of INPUT into ls_parmbind-value.
  insert ls_parmbind into table lt_parmbind.

  if_proxy_client~execute(
    exporting
      method_name = 'PRODUCT_CATALOGUE_SEARCH_OUT'
    changing
      parmbind_tab = lt_parmbind
  ).

  endmethod.
ENDCLASS.
