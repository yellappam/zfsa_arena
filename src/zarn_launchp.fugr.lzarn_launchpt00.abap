*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 11.12.2018 at 09:16:30
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_LAUNCHP_T..................................*
DATA:  BEGIN OF STATUS_ZARN_LAUNCHP_T                .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_LAUNCHP_T                .
CONTROLS: TCTRL_ZARN_LAUNCHP_T
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_LAUNCHP_T                .
TABLES: ZARN_LAUNCHP_T                 .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
