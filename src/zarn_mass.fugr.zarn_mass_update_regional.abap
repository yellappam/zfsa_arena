FUNCTION zarn_mass_update_regional .
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(SELDATA) TYPE  MASS_TABDATA
*"     VALUE(TESTMODE) OPTIONAL
*"     VALUE(MASSSAVEINFOS) OPTIONAL
*"  EXPORTING
*"     VALUE(MSG) TYPE  MASS_MSGS
*"  TABLES
*"      SZARN_REG_BANNER STRUCTURE  ZARN_REG_BANNER OPTIONAL
*"      SZARN_REG_DC_SELL STRUCTURE  ZARN_REG_DC_SELL OPTIONAL
*"      SZARN_REG_EAN STRUCTURE  ZARN_REG_EAN OPTIONAL
*"      SZARN_REG_HDR STRUCTURE  ZARN_REG_HDR OPTIONAL
*"      SZARN_REG_LST_PRC STRUCTURE  ZARN_REG_LST_PRC OPTIONAL
*"      SZARN_REG_PIR STRUCTURE  ZARN_REG_PIR OPTIONAL
*"      SZARN_REG_PRFAM STRUCTURE  ZARN_REG_PRFAM OPTIONAL
*"      SZARN_REG_RRP STRUCTURE  ZARN_REG_RRP OPTIONAL
*"      SZARN_REG_STD_TER STRUCTURE  ZARN_REG_STD_TER OPTIONAL
*"      SZARN_REG_TXT STRUCTURE  ZARN_REG_TXT OPTIONAL
*"      SZARN_REG_UOM STRUCTURE  ZARN_REG_UOM OPTIONAL
*"      SZARN_REG_CLUSTER STRUCTURE  ZARN_REG_CLUSTER OPTIONAL
*"      MASSGENCHANGE OPTIONAL
*"----------------------------------------------------------------------
* DATE             # 23.10.2019
* CHANGE No.       # SUP-317
* DESCRIPTION      # EINE table not updating EKKO field. Introduced commit
*                  # work as EINE table getting updated in UPDATE TASK and
*                  # there seems to be no commit work issues by framework
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* DATE             # 27.07.2020
* CHANGE No.       # SSM-1: NW Range-ZARN_GUI CHANGE
* DESCRIPTION      # Added ZARN_REG_CLUSTER for mass maintenance
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
  "Refactoring IS 1902 - code to get RFCDEST encapsulated into method

  DATA: ls_msg     TYPE massmsg,
        lt_message TYPE bal_t_msg,
        lv_post_dest  TYPE rfcdes-rfcdest,
        lo_mass_update TYPE REF TO zcl_arn_mass_update,
        lv_log_no      TYPE balognr.

  lv_post_dest = zcl_rfc_destination_services=>get_instance(
                          )->get_arena_article_post_rfc( ).


  CLEAR lt_message[].
  IF lv_post_dest IS NOT INITIAL.

* Call MASS Regional Update through RFC
    CALL FUNCTION 'ZARN_MASS_UPDATE_REGIONAL_RFC' DESTINATION lv_post_dest
      EXPORTING
        seldata           = seldata[]
        testmode          = testmode
        iv_commit         = abap_true
      IMPORTING
        et_message        = lt_message[]
      TABLES
        szarn_reg_banner  = szarn_reg_banner[]
        szarn_reg_dc_sell = szarn_reg_dc_sell[]
        szarn_reg_ean     = szarn_reg_ean[]
        szarn_reg_hdr     = szarn_reg_hdr[]
        szarn_reg_lst_prc = szarn_reg_lst_prc[]
        szarn_reg_pir     = szarn_reg_pir[]
        szarn_reg_prfam   = szarn_reg_prfam[]
        szarn_reg_rrp     = szarn_reg_rrp[]
        szarn_reg_std_ter = szarn_reg_std_ter[]
        szarn_reg_txt     = szarn_reg_txt[]
        szarn_reg_uom     = szarn_reg_uom[]
        szarn_reg_cluster = szarn_reg_cluster[]
        msg               = msg[].

  ELSE.
* RFC not Open
    CLEAR ls_msg.
    ls_msg-objkey = 'ZARENA'.
    ls_msg-msgty  = 'E'.
    ls_msg-msgid  = 'ZARENA_MSG'.
    ls_msg-msgno  = '092'.

    APPEND ls_msg TO msg[].
  ENDIF.




* Maintain SLG Log
  IF lt_message[] IS NOT INITIAL.

    CREATE OBJECT lo_mass_update.

    CLEAR lv_log_no.
    CALL METHOD lo_mass_update->maintain_bal_log
      EXPORTING
        it_message     = lt_message[]
        iv_display_log = 'X'
      IMPORTING
        ev_log_no      = lv_log_no.
  ENDIF.


ENDFUNCTION.
