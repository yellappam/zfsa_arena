*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 11.05.2017 at 14:15:45
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_BU.........................................*
DATA:  BEGIN OF STATUS_ZARN_BU                       .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_BU                       .
CONTROLS: TCTRL_ZARN_BU
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_BU                       .
TABLES: ZARN_BU                        .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
