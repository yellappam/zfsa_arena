*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_ARTICLE_LINK_SUB
*&---------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*&      Form  GET_DATA
*&---------------------------------------------------------------------*
* Retreive Article Linking Data
*----------------------------------------------------------------------*
FORM get_data .

  DATA: lt_mara TYPE ty_t_mara.

  IF s_scn[] IS NOT INITIAL.

* If Scenario is entered then,
* 1) retreive data as per scenario
* 2) ignore articles from link data which do not match entered articles
* 3) for retreived link data, get article data as per selection criteria


    SELECT * FROM zarn_reg_artlink
      INTO CORRESPONDING FIELDS OF TABLE gt_reg_artlink
      WHERE hdr_scn  IN s_scn[]
         OR link_scn IN s_scn[].

    DELETE gt_reg_artlink WHERE hdr_matnr NOT IN s_matnr AND link_matnr NOT IN s_matnr.


    IF gt_reg_artlink IS NOT INITIAL.
      SELECT a~matnr a~matkl a~attyp a~mtart b~maktx
        INTO CORRESPONDING FIELDS OF TABLE gt_mara
        FROM mara AS a
        JOIN makt AS b
        ON a~matnr = b~matnr
        FOR ALL ENTRIES IN gt_reg_artlink
        WHERE ( a~matnr = gt_reg_artlink-hdr_matnr OR a~matnr = gt_reg_artlink-link_matnr )
          AND a~matkl IN s_matkl
          AND a~attyp IN s_attyp
          AND a~mtart IN s_mtart
          AND b~maktg IN s_maktx
          AND b~spras = sy-langu.
    ENDIF.

  ELSE.

* If Scenario is not entered then,
* 1) retreive article data as per selection criteria
* 2) get link data based on article data retreived
* 3) for retreived link data, get article data

    SELECT a~matnr a~matkl a~attyp a~mtart b~maktx
      INTO CORRESPONDING FIELDS OF TABLE lt_mara
      FROM mara AS a
      JOIN makt AS b
      ON a~matnr = b~matnr
      WHERE a~matnr IN s_matnr
        AND a~matkl IN s_matkl
        AND a~attyp IN s_attyp
        AND a~mtart IN s_mtart
        AND b~maktg IN s_maktx
        AND b~spras = sy-langu.
    IF lt_mara IS NOT INITIAL.
      SELECT * FROM zarn_reg_artlink
        INTO CORRESPONDING FIELDS OF TABLE gt_reg_artlink
        FOR ALL ENTRIES IN lt_mara
        WHERE ( hdr_matnr = lt_mara-matnr OR link_matnr = lt_mara-matnr ).
      IF gt_reg_artlink IS NOT INITIAL.
        SELECT a~matnr a~matkl a~attyp a~mtart b~maktx
          INTO CORRESPONDING FIELDS OF TABLE gt_mara
          FROM mara AS a
          JOIN makt AS b
          ON a~matnr = b~matnr
          FOR ALL ENTRIES IN gt_reg_artlink
          WHERE ( a~matnr = gt_reg_artlink-hdr_matnr OR a~matnr = gt_reg_artlink-link_matnr ).
      ENDIF.


    ENDIF.

  ENDIF.

* Get header scenario text
  gt_hdr_scn_t[]  = zcl_arn_reg_artlink=>get_hdr_scn_t( ).

* Get linked scenario text
  gt_link_scn_t[] = zcl_arn_reg_artlink=>get_link_scn_t( ).




ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  BUILD_OUTPUT_DATA
*&---------------------------------------------------------------------*
* Build Output Data
*----------------------------------------------------------------------*
FORM build_output_data .

  DATA: ls_output      TYPE zsarn_article_link_alv,
        ls_mara_hdr    TYPE ty_s_mara,
        ls_mara_link   TYPE ty_s_mara,
        ls_reg_artlink TYPE zarn_reg_artlink,
        ls_hdr_scn_t   TYPE zarn_hdr_scn_t,
        ls_link_scn_t  TYPE zarn_link_scn_t.


  LOOP AT gt_reg_artlink INTO ls_reg_artlink.

* Get hdr article data, if not found then ignore record
    CLEAR ls_mara_hdr.
    TRY.
        ls_mara_hdr = gt_mara[ matnr = ls_reg_artlink-hdr_matnr ].
      CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
        CONTINUE.
    ENDTRY.

* Get link article data, if not found then ignore record
    CLEAR ls_mara_link.
    TRY.
        ls_mara_link = gt_mara[ matnr = ls_reg_artlink-link_matnr ].
      CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
        CONTINUE.
    ENDTRY.

    CLEAR ls_output.
    ls_output-hdr_scn       = ls_reg_artlink-hdr_scn.
    ls_output-hdr_matnr     = ls_reg_artlink-hdr_matnr.
    ls_output-hdr_maktx     = ls_mara_hdr-maktx.
    ls_output-hdr_mtart     = ls_mara_hdr-mtart.
    ls_output-hdr_attyp     = ls_mara_hdr-attyp.
    ls_output-hdr_matkl     = ls_mara_hdr-matkl.

    ls_output-link_scn      = ls_reg_artlink-link_scn.
    ls_output-link_matnr    = ls_reg_artlink-link_matnr.
    ls_output-link_maktx    = ls_mara_link-maktx.
    ls_output-link_mtart    = ls_mara_link-mtart.
    ls_output-link_attyp    = ls_mara_link-attyp.
    ls_output-link_matkl    = ls_mara_link-matkl.

* Get hdr scenario text
    CLEAR ls_hdr_scn_t.
    TRY.
        ls_hdr_scn_t = gt_hdr_scn_t[ hdr_scn = ls_reg_artlink-hdr_scn ].
        ls_output-hdr_scn_drdn+0(2)  = ls_reg_artlink-hdr_scn.
        ls_output-hdr_scn_drdn+2(1)  = space.
        ls_output-hdr_scn_drdn+3(20) = ls_hdr_scn_t-hdr_scn_text.
      CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
        ls_output-hdr_scn_drdn  = ls_reg_artlink-hdr_scn.
    ENDTRY.

* Get link scenario text
    CLEAR ls_link_scn_t.
    TRY.
        ls_link_scn_t = gt_link_scn_t[ link_scn = ls_reg_artlink-link_scn ].
        ls_output-link_scn_drdn+0(2)  = ls_reg_artlink-link_scn.
        ls_output-link_scn_drdn+2(1)  = space.
        ls_output-link_scn_drdn+3(20) = ls_link_scn_t-link_scn_text.
      CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
        ls_output-link_scn_drdn  = ls_reg_artlink-link_scn.
    ENDTRY.


    APPEND ls_output TO gt_output.
  ENDLOOP.   " LOOP AT gt_reg_artlink INTO ls_reg_artlink


ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  DISPLAY_OUTPUT_DATA
*&---------------------------------------------------------------------*
* Display Output Data
*----------------------------------------------------------------------*
FORM display_output_data .

* Call the screen 9000 for displaying ALV grid.
  CALL SCREEN 9000.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  DISPLAY_ALV
*&---------------------------------------------------------------------*
* Display ALV
*----------------------------------------------------------------------*
FORM display_alv .


  CONSTANTS: lc_a        TYPE char01 VALUE 'A'.

* Declaration
  DATA: lt_fieldcat   TYPE        lvc_t_fcat,                "Field catalog
        ls_layout     TYPE        lvc_s_layo ,               "Layout structure
        ls_disvariant TYPE        disvariant.                "Variant


  IF go_alvgrid IS INITIAL .
*   Create docking container
    CREATE OBJECT go_docking
      EXPORTING
        parent = cl_gui_container=>screen0
*       ratio  = 90  " 90% yet not full-screen size
      EXCEPTIONS
        OTHERS = 6.
    IF sy-subrc NE 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

*   Full size screen for ALV
    CALL METHOD go_docking->set_extension
      EXPORTING
        extension  = 99999  " full-screen size !!!
      EXCEPTIONS
        cntl_error = 1
        OTHERS     = 2.
    IF sy-subrc NE 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

* Create ALV grid
    CREATE OBJECT go_alvgrid
      EXPORTING
        i_parent = go_docking
      EXCEPTIONS
        OTHERS   = 5.
    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.


*   Preparing field catalog
    PERFORM prepare_fcat CHANGING lt_fieldcat[].

*   Preparing layout structure
    PERFORM prepare_layout CHANGING ls_layout.

* Prepare Variant
    ls_disvariant-report    = sy-repid.
    ls_disvariant-username  = sy-uname.

*   ALV Display
    CALL METHOD go_alvgrid->set_table_for_first_display
      EXPORTING
        i_bypassing_buffer            = abap_true
        is_variant                    = ls_disvariant
        i_save                        = lc_a
        is_layout                     = ls_layout
*       it_toolbar_excluding          = lt_toolbar
      CHANGING
        it_outtab                     = gt_output[]
        it_fieldcatalog               = lt_fieldcat[]
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.


  ELSE.

*   ALV Refresh Display
    CALL METHOD go_alvgrid->refresh_table_display
      EXPORTING
*       is_stable      = ls_stable
        i_soft_refresh = abap_true
      EXCEPTIONS
        finished       = 1
        OTHERS         = 2.
  ENDIF.



ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  PREPARE_FCAT
*&---------------------------------------------------------------------*
*   Preparing field catalog
*----------------------------------------------------------------------*
FORM prepare_fcat  CHANGING fc_t_fieldcat TYPE lvc_t_fcat.

* Declaration
  DATA: lt_fieldcat TYPE lvc_t_fcat,
        ls_fieldcat TYPE lvc_s_fcat.

  FIELD-SYMBOLS : <ls_fieldcat>    TYPE lvc_s_fcat.

* Prepare field catalog for ALV display by merging structure
  REFRESH: lt_fieldcat[].
  CALL FUNCTION 'LVC_FIELDCATALOG_MERGE'
    EXPORTING
      i_structure_name       = 'ZSARN_ARTICLE_LINK_ALV'
    CHANGING
      ct_fieldcat            = lt_fieldcat[]
    EXCEPTIONS
      inconsistent_interface = 1
      program_error          = 2
      OTHERS                 = 3.
  IF sy-subrc NE 0.
    REFRESH: fc_t_fieldcat.
  ENDIF.


* Looping to make the output field
  LOOP AT lt_fieldcat ASSIGNING <ls_fieldcat>.

    <ls_fieldcat>-tooltip = <ls_fieldcat>-scrtext_l.
    <ls_fieldcat>-col_opt = abap_true.

    CASE <ls_fieldcat>-fieldname.
      WHEN 'HDR_SCN'.
        <ls_fieldcat>-tech = abap_true.

      WHEN 'HDR_SCN_DRDN'.
      WHEN 'HDR_MATNR'.
      WHEN 'HDR_MAKTX'.
      WHEN 'HDR_MTART'.
      WHEN 'HDR_ATTYP'.
      WHEN 'HDR_MATKL'.


      WHEN 'LINK_SCN'.
        <ls_fieldcat>-tech = abap_true.

      WHEN 'LINK_SCN_DRDN'.
      WHEN 'LINK_MATNR'.
      WHEN 'LINK_MAKTX'.
      WHEN 'LINK_MTART'.
      WHEN 'LINK_ATTYP'.
      WHEN 'LINK_MATKL'.

      WHEN OTHERS.
        DELETE lt_fieldcat WHERE fieldname = <ls_fieldcat>-fieldname.

    ENDCASE.

  ENDLOOP.

  fc_t_fieldcat[] = lt_fieldcat[].

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  PREPARE_LAYOUT
*&---------------------------------------------------------------------*
*       Field Catalog for STO Display
*----------------------------------------------------------------------*
FORM prepare_layout CHANGING fc_s_layout    TYPE lvc_s_layo.

  fc_s_layout-sel_mode = 'A'.
  fc_s_layout-zebra    = abap_true.
  fc_s_layout-col_opt  = abap_true.


ENDFORM.                    " PREPARE_LAYOUT
