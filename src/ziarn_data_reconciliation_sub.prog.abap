*&---------------------------------------------------------------------*
*&  Include           ZIARN_DATA_RECONCILIATION_SUB
*&---------------------------------------------------------------------*
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 13.04.2018
* CHANGE No.       # Charm 9000003482; Jira CI18-554
* DESCRIPTION      # CI18-554 Recipe Management - Ingredient Changes
*                  # New regional table for Allergen Type overrides and
*                  # fields for Ingredients statement overrides in Arena.
*                  # New Ingredients Type field in the Scales block in
*                  # Arena and NONSAP tab in material master.
* WHO              # C90005557, Tessa Newman
*-----------------------------------------------------------------------
* DATE             # 06.06.2018
* CHANGE No.       # ChaRM 9000003675; JIRA CI18-506 NEW Range Status flag
* DESCRIPTION      # New Range Flag, Range Availability and Range Detail
*                  # fields added in Arena and on material master in Listing
*                  # tab and Basic Data Text tab (materiallongtext).
* WHO              # C90005557, Tessa Newman
*-----------------------------------------------------------------------
*&---------------------------------------------------------------------*
*&      Form  INITIALIZE
*&---------------------------------------------------------------------*
* Initialize
*----------------------------------------------------------------------*
FORM initialize .
  CLEAR: gt_fan[], gt_idno_data[], gt_prod_data[], gt_reg_data[], gt_reg_data_def[],

         gt_fan_exist[], gt_reco_basic1[], gt_reco_basic2[], gt_reco_attr[], gt_reco_uom[], gt_reco_pir[],
         gt_reco_ean[], gt_reco_banner[],

         gt_output_9006[],

         gt_mara[], gt_marc[], gt_makt[], gt_maw1[], gt_marm[], gt_mean[], gt_eina[], gt_eine[],
          gt_mvke[], gt_wlk2[], gt_marm_idno[],

         gt_t006[], gt_t005[], gt_tcurc[], gt_coo_t[], gt_gen_uom_t[], gt_t134[], gr_tvarvc_ekorg[],
         gr_tvarvc_vkorg[], gt_tvarvc_p_org[], gt_tvarvc_uni_lni[], gt_tvta[], gt_tvarvc_hyb_code[],

         gt_lfm1[], gt_fanid[], gt_prod_uom_t[], gt_uom_cat[], gt_uomcategory[], gt_ref_article[],
         gt_pir_vendor[], gt_tvarv_ean_cate[], gt_host_prdtyp[], gt_mc_subdep[],

         gt_headdata[], gt_clientdata_arn[], gt_clientext_arn[], gt_addnlclientdata_arn[], gt_materialdescription_arn[],
         gt_unitsofmeasure_arn[], gt_internationalartno_arn[], gt_inforecord_general_arn[],
         gt_inforecord_purchorg_arn[], gt_salesdata_arn[], gt_salesext_arn[], gt_posdata_arn[], gt_plantdata_arn[],
         gt_plantext_arn[],
         gt_bapi_clientext_arn[], gt_bapi_salesext_arn[], gt_bapi_plantext_arn[],

         gt_headdata_arn[], gt_clientdata[], gt_clientext[], gt_addnlclientdata[], gt_materialdescription[],
         gt_unitsofmeasure[], gt_internationalartno[], gt_inforecord_general[],
         gt_inforecord_purchorg[], gt_salesdata[], gt_salesext[], gt_posdata[], gt_plantdata[],
         gt_plantext[],
         gt_bapi_clientext[], gt_bapi_salesext[], gt_bapi_plantext[],

         gs_prod_data_all, gs_reg_data_all,

         go_gui_load,

         gv_mode,  gv_pir_ekorg,  gv_error.



  FREE: go_alvgrid_9001,
        go_alvgrid_9002,
        go_alvgrid_9003,
        go_alvgrid_9004,
        go_alvgrid_9005,
        go_alvgrid_9006,
        go_alvgrid_9007,
        go_alvgrid_9008,

        go_container_9001,
        go_container_9002,
        go_container_9003,
        go_container_9004,
        go_container_9005,
        go_container_9006,
        go_container_9007,
        go_container_9008.



  CLEAR: go_alvgrid_9001,
         go_alvgrid_9002,
         go_alvgrid_9003,
         go_alvgrid_9004,
         go_alvgrid_9005,
         go_alvgrid_9006,
         go_alvgrid_9007,
         go_alvgrid_9008,

         go_container_9001,
         go_container_9002,
         go_container_9003,
         go_container_9004,
         go_container_9005,
         go_container_9006,
         go_container_9007,
         go_container_9008,


         gv_pir_rel_9006,

         gv_count_9002,
         gv_count_9003,
         gv_count_9004,
         gv_count_9005,
         gv_count_9006,
         gv_count_9007,
         gv_count_9008.


ENDFORM.                    " INITIALIZE
*&---------------------------------------------------------------------*
*&      Form  SET_COMPARISION_MODE
*&---------------------------------------------------------------------*
* Set Comparision Mode
*----------------------------------------------------------------------*
FORM set_comparision_mode USING fu_v_ecc  TYPE char1
                                fu_v_dsp  TYPE char1
                       CHANGING fc_v_mode TYPE char1.


  IF fu_v_dsp = abap_true.
    fc_v_mode = gc_mode-dsp.
  ELSEIF fu_v_ecc = abap_true.
    fc_v_mode = gc_mode-ecc.
  ENDIF.

ENDFORM.                    " SET_COMPARISION_MODE
*&---------------------------------------------------------------------*
*&      Form  GET_MAIN_FAN_LIST
*&---------------------------------------------------------------------*
* Get main FAN ID List
*----------------------------------------------------------------------*
FORM get_main_fan_list USING fu_t_fan   TYPE ty_t_fan_range
                    CHANGING fc_t_fan   TYPE ty_t_fan
                             fc_v_error TYPE flag.

  DATA: lt_fan_range TYPE ty_t_fan_range,
        ls_fan_range TYPE LINE OF ty_t_fan_range,
        ls_fan       TYPE ty_s_fan.


  CLEAR: fc_t_fan[].

  lt_fan_range[] = fu_t_fan[].
  DELETE lt_fan_range[] WHERE low IS INITIAL.


  LOOP AT lt_fan_range[] INTO ls_fan_range.
    CLEAR ls_fan.
    ls_fan-fan_id = ls_fan_range-low.
    INSERT ls_fan INTO TABLE fc_t_fan[].
  ENDLOOP.


  IF fc_t_fan[] IS INITIAL.
* No FAN ID to Display
    MESSAGE s055 DISPLAY LIKE 'E'.
    fc_v_error = abap_true.
    EXIT.
  ENDIF.

  SORT fc_t_fan[] BY fan_id.
  DELETE ADJACENT DUPLICATES FROM fc_t_fan[] COMPARING fan_id.

ENDFORM.                    " GET_MAIN_FAN_LIST
*&---------------------------------------------------------------------*
*&      Form  BUILD_IDNO_DATA
*&---------------------------------------------------------------------*
* Build IDNO Data
*----------------------------------------------------------------------*
FORM build_idno_data USING fu_t_fan         TYPE ty_t_fan
                  CHANGING fc_t_idno_data   TYPE ty_t_idno_data
                           fc_t_mara        TYPE ty_t_mara.

  DATA: ls_fan       TYPE ty_s_fan,
        ls_idno_data TYPE ty_s_idno_data,
        ls_mara      TYPE mara.

  FIELD-SYMBOLS: <ls_idno_data>   TYPE ty_s_idno_data.

  IF fu_t_fan[] IS NOT INITIAL.

* Get Product Details to get MATNR for entered IDNOs
    SELECT a~idno a~fan_id
           b~current_ver b~previous_ver b~latest_ver b~article_ver
*           c~version_status AS nat_status
*           c~id_type
      INTO CORRESPONDING FIELDS OF TABLE fc_t_idno_data[]
      FROM zarn_products AS a
      JOIN zarn_ver_status AS b
        ON a~idno    = b~idno
       AND a~version = b~current_ver
*      JOIN zarn_prd_version AS c
*        ON c~idno    = b~idno
*       AND c~version = b~current_ver
       FOR ALL ENTRIES IN fu_t_fan[]
     WHERE a~fan_id EQ fu_t_fan-fan_id.


* Get SAP Articles for the FANs entered
    SELECT *
      FROM mara
      INTO CORRESPONDING FIELDS OF TABLE fc_t_mara[]
      FOR ALL ENTRIES IN fu_t_fan[]
      WHERE zzfan = fu_t_fan-fan_id.

  ENDIF.



  LOOP AT fc_t_idno_data ASSIGNING <ls_idno_data>.

    CLEAR ls_mara.
    READ TABLE fc_t_mara[] INTO ls_mara
    WITH KEY zzfan = <ls_idno_data>-fan_id.
    IF sy-subrc NE 0.
      <ls_idno_data>-sap_article  = <ls_idno_data>-fan_id.
    ELSE.
      <ls_idno_data>-sap_article  = ls_mara-matnr.
    ENDIF.


    CALL FUNCTION 'CONVERSION_EXIT_MATN1_INPUT'
      EXPORTING
        input        = <ls_idno_data>-sap_article
      IMPORTING
        output       = <ls_idno_data>-sap_article
      EXCEPTIONS
        length_error = 1
        OTHERS       = 2.

  ENDLOOP.  " LOOP AT fc_t_idno_data ASSIGNING <ls_idno_data>





ENDFORM.                    " BUILD_IDNO_DATA
*&---------------------------------------------------------------------*
*&      Form  GET_NAT_REG_DATA
*&---------------------------------------------------------------------*
* Get National and Regional Data
*----------------------------------------------------------------------*
FORM get_nat_reg_data USING fu_t_idno_data     TYPE ty_t_idno_data
                   CHANGING fc_t_prod_data     TYPE ty_t_prod_data
                            fc_t_reg_data      TYPE ty_t_reg_data
                            fc_s_prod_data_all TYPE ty_s_prod_data
                            fc_s_reg_data_all  TYPE ty_s_reg_data.


  DATA: ls_idno_data     TYPE ty_s_idno_data,
        lt_key           TYPE ztarn_key,
        ls_key           TYPE zsarn_key,
        lt_reg_key       TYPE ztarn_reg_key,
        ls_reg_key       TYPE zsarn_reg_key,
        ls_prod_data_all TYPE ty_s_prod_data,
        ls_reg_data_all  TYPE ty_s_reg_data,
        lt_prod_data     TYPE ty_t_prod_data,
        ls_prod_data     TYPE ty_s_prod_data,
        lt_reg_data      TYPE ty_t_reg_data,
        ls_reg_data      TYPE ty_s_reg_data.



  LOOP AT fu_t_idno_data[] INTO ls_idno_data.

    CLEAR ls_key.
    ls_key-idno    = ls_idno_data-idno.
    ls_key-version = ls_idno_data-current_ver.
    APPEND ls_key TO lt_key[].

    CLEAR ls_reg_key.
    ls_reg_key-idno = ls_idno_data-idno.
    APPEND ls_reg_key TO lt_reg_key[].

  ENDLOOP.


* Get National Data
  CLEAR lt_prod_data[].
  CALL FUNCTION 'ZARN_READ_NATIONAL_DATA'
    EXPORTING
      it_key      = lt_key[]
    IMPORTING
      et_data     = lt_prod_data[]
      es_data_all = ls_prod_data_all.


* Get Regional Data
  CLEAR lt_reg_data[].
  CALL FUNCTION 'ZARN_READ_REGIONAL_DATA'
    EXPORTING
      it_key      = lt_reg_key[]
    IMPORTING
      et_data     = lt_reg_data[]
      es_data_all = ls_reg_data_all.


  fc_t_prod_data[]     = lt_prod_data[].
  fc_t_reg_data[]      = lt_reg_data[].

  fc_s_prod_data_all   = ls_prod_data_all.
  fc_s_reg_data_all    = ls_reg_data_all.



ENDFORM.                    " GET_NAT_REG_DATA
*&---------------------------------------------------------------------*
*&      Form  BUILD_FAN_EXIST_DATA
*&---------------------------------------------------------------------*
* Build FAN Exist Data
*----------------------------------------------------------------------*
FORM build_fan_exist_data USING fu_t_fan           TYPE ty_t_fan
                                fu_t_idno_data     TYPE ty_t_idno_data
                                fu_s_prod_data_all TYPE ty_s_prod_data
                                fu_s_reg_data_all  TYPE ty_s_reg_data
                                fu_t_mara          TYPE ty_t_mara
                       CHANGING fc_t_fan_exist     TYPE ty_t_fan_exist.

  DATA: ls_fan           TYPE ty_s_fan,
        ls_idno_data     TYPE ty_s_idno_data,
        ls_mara          TYPE mara,
        ls_prod_data_all TYPE ty_s_prod_data,
        ls_reg_data_all  TYPE ty_s_reg_data,
        ls_fan_exist     TYPE ty_s_fan_exist,

        ls_products      TYPE zarn_products,
        ls_reg_hdr       TYPE zarn_reg_hdr,

        lv_no_fan        TYPE flag.


* For each FAN entered on Screen
  LOOP AT fu_t_fan[] INTO ls_fan.

* Get data from MARA - Article Master
    CLEAR ls_mara.
    READ TABLE fu_t_mara[] INTO ls_mara
    WITH KEY zzfan = ls_fan-fan_id.

    lv_no_fan = abap_true.

* Ideally not, but if there are multiple IDNOs for a FAN then this must be displayed
    LOOP AT fu_t_idno_data[] INTO ls_idno_data
      WHERE fan_id = ls_fan-fan_id.

* Get data from National Products
      CLEAR ls_products.
      READ TABLE fu_s_prod_data_all-zarn_products[] INTO ls_products
      WITH KEY idno    = ls_idno_data-idno
               version = ls_idno_data-current_ver.

* Get data from Regional Header
      CLEAR ls_reg_hdr.
      READ TABLE fu_s_reg_data_all-zarn_reg_hdr[] INTO ls_reg_hdr
      WITH KEY idno    = ls_idno_data-idno.


      CLEAR ls_fan_exist.
      ls_fan_exist-fan        = ls_fan-fan_id.
      ls_fan_exist-nat_fan    = ls_products-fan_id.
      ls_fan_exist-sap_fan    = ls_mara-zzfan.
      ls_fan_exist-matnr_ni   = ls_products-matnr_ni.
      ls_fan_exist-matnr_reg  = ls_reg_hdr-matnr.
      ls_fan_exist-matnr_sap  = ls_mara-matnr.

      ls_fan_exist-idno        = ls_idno_data-idno.
      ls_fan_exist-current_ver = ls_idno_data-current_ver.

      CALL FUNCTION 'CONVERSION_EXIT_MATN1_INPUT'
        EXPORTING
          input  = ls_fan_exist-matnr_ni
        IMPORTING
          output = ls_fan_exist-matnr_ni.





* If Entered FAN, National FAN and SAP FAN are same
* AND
* If FSNI Article, Regional Article and SAP Article are same
* then only it is matched,
      IF ( ( ls_fan_exist-fan     = ls_fan_exist-nat_fan ) AND
           ( ls_fan_exist-nat_fan = ls_fan_exist-sap_fan ) ) AND

         ( ( ls_fan_exist-matnr_ni  = ls_fan_exist-matnr_reg ) AND
           ( ls_fan_exist-matnr_reg = ls_fan_exist-matnr_sap ) ) AND

        ( ls_fan_exist-matnr_ni  IS NOT INITIAL AND
          ls_fan_exist-matnr_reg IS NOT INITIAL AND
          ls_fan_exist-matnr_sap IS NOT INITIAL AND
          ls_fan_exist-sap_fan   IS NOT INITIAL ).

        CLEAR ls_fan_exist-status_fan.
      ELSE.
* Else, Un-matched - show it as 'X'
        ls_fan_exist-status_fan = abap_true.
      ENDIF.

      IF ls_fan_exist-status_fan = abap_true.
        ls_fan_exist-ovr_status = abap_true.
      ENDIF.



      APPEND ls_fan_exist TO fc_t_fan_exist[].

      CLEAR lv_no_fan.

    ENDLOOP.  " LOOP AT fu_t_idno_data[] INTO ls_idno_data


    IF lv_no_fan = abap_true.
      CLEAR ls_fan_exist.
      ls_fan_exist-fan        = ls_fan-fan_id.
      ls_fan_exist-sap_fan    = ls_mara-zzfan.
      ls_fan_exist-matnr_sap  = ls_mara-matnr.
      ls_fan_exist-status_fan = abap_true.
      APPEND ls_fan_exist TO fc_t_fan_exist[].
    ENDIF.  " IF lv_no_fan = abap_true

  ENDLOOP.  " LOOP AT fu_t_fan[] INTO ls_fan


ENDFORM.                    " BUILD_FAN_EXIST_DATA
*&---------------------------------------------------------------------*
*&      Form  GET_SAP_DATA
*&---------------------------------------------------------------------*
* Get SAP data from DB
*----------------------------------------------------------------------*
FORM get_sap_data USING fu_t_idno_data       TYPE ty_t_idno_data
                        fu_t_mara            TYPE ty_t_mara
                        fu_s_prod_data_all   TYPE ty_s_prod_data
                        fu_s_reg_data_all    TYPE ty_s_reg_data
               CHANGING fc_t_makt            TYPE ty_t_makt
                        fc_t_maw1            TYPE ty_t_maw1
                        fc_t_marm            TYPE ty_t_marm
                        fc_t_marm_idno       TYPE ty_t_marm_idno
                        fc_t_mean            TYPE ty_t_mean
                        fc_t_marc            TYPE ty_t_marc
                        fc_t_eina            TYPE ty_t_eina
                        fc_t_eine            TYPE ty_t_eine
                        fc_t_mvke            TYPE ty_t_mvke
                        fc_t_wlk2            TYPE ty_t_wlk2
                        fc_t_t006            TYPE ty_t_t006
                        fc_t_t005            TYPE ty_t_t005
                        fc_t_tcurc           TYPE ty_t_tcurc
                        fc_t_coo_t           TYPE ty_t_coo_t
                        fc_t_gen_uom_t       TYPE ty_t_gen_uom_t
                        fc_t_t134            TYPE ty_t_t134
                        fc_t_tvarvc_ekorg    TYPE ty_r_ekorg
                        fc_t_tvarvc_vkorg    TYPE ty_r_vkorg
                        fc_t_tvarvc_p_org    TYPE tvarvc_t
                        fc_t_tvarvc_uni_lni  TYPE tvarvc_t
                        fc_t_tvarvc_hyb_code TYPE tvarvc_t
                        fc_t_tvta            TYPE tvta_tt
                        fc_t_lfm1            TYPE ty_t_lfm1
                        fc_t_fanid           TYPE ty_t_fanid
                        fc_t_prod_uom_t      TYPE ty_t_prod_uom_t
                        fc_t_uom_cat         TYPE ty_t_uom_cat
                        fc_t_uomcategory     TYPE ty_t_uomcategory
                        fc_t_ref_article     TYPE ty_t_ref_article
                        fc_t_pir_vendor      TYPE ty_t_pir_vendor
                        fc_t_tvarv_ean_cate  TYPE tvarvc_t
                        fc_t_host_prdtyp     TYPE ty_t_host_prdtyp
                        fc_t_mc_subdep       TYPE ty_t_mc_subdep
                        fc_v_pir_ekorg       TYPE ekorg.


  DATA: ls_pir          TYPE zarn_pir,
        lt_pir_vendor   TYPE ty_t_pir_vendor,
        ls_pir_vendor   TYPE ty_s_pir_vendor,
        ls_idno_data    TYPE ty_s_idno_data,
        ls_uom_cat      TYPE zarn_uom_cat,
        ls_marm         TYPE marm,
        ls_marm_idno    TYPE ty_s_marm_idno,
        ls_fanid        TYPE ty_s_fanid,
        ls_mara         TYPE mara,
        ls_products     TYPE zarn_products,

        ls_tvarvc_vkorg LIKE LINE OF fc_t_tvarvc_vkorg,
        lr_marc_werks   TYPE RANGE OF werks_d,
        ls_marc_werks   LIKE LINE OF lr_marc_werks.


* EINE-EKORG from TVARVC for PIR posting for multiple Purch Orgs
  SELECT sign opti AS option low FROM tvarvc
    INTO CORRESPONDING FIELDS OF TABLE fc_t_tvarvc_ekorg[]
    WHERE name = gc_tvarvc-eine_ekorg
      AND type = gc_tvarvc-type_s
      AND high = abap_true.

* Sales Org For UNI/LNI from TVARVC
  SELECT sign opti AS option low high FROM tvarvc
    INTO CORRESPONDING FIELDS OF TABLE fc_t_tvarvc_vkorg[]
    WHERE name = gc_tvarvc-vkorg_uni
       OR name = gc_tvarvc-vkorg_lni
      AND type = gc_tvarvc-type_s.

* EINE-EKORG from TVARVC for PIR posting for multiple Purch Orgs
  SELECT * FROM tvarvc
    INTO CORRESPONDING FIELDS OF TABLE fc_t_tvarvc_p_org[]
    WHERE name = gc_tvarvc-eine_ekorg
      AND type = gc_tvarvc-type_s
      AND high = abap_true.


* Sales Org For UNI/LNI from TVARVC
  SELECT * FROM tvarvc
    INTO CORRESPONDING FIELDS OF TABLE fc_t_tvarvc_uni_lni[]
    WHERE name = gc_tvarvc-vkorg_uni
       OR name = gc_tvarvc-vkorg_lni
      AND type = gc_tvarvc-type_s.

* Default Purch. Org.
  SELECT SINGLE low INTO fc_v_pir_ekorg
         FROM tvarvc
         WHERE name EQ gc_tvarvc-eine_ekorg
           AND type EQ gc_tvarvc-type_s
           AND high EQ abap_true.

  SELECT *
  FROM tvarvc
  INTO TABLE gt_tvarv_ean_cate[]
  WHERE name = gc_tvarvc-def_eancat
    AND type = gc_tvarvc-type_p.

  SELECT *
  FROM tvarvc
  APPENDING TABLE gt_tvarv_ean_cate[]
  WHERE name = gc_tvarvc-def_eancat_prefix
    AND type = gc_tvarvc-type_s.


  SELECT *
  FROM tvarvc
  INTO CORRESPONDING FIELDS OF TABLE fc_t_tvarvc_hyb_code[]
  WHERE name = gc_tvarvc-uom_hyb_code
    AND type = gc_tvarvc-type_s.


* Sites for MARC Data
  LOOP AT fc_t_tvarvc_vkorg[] INTO ls_tvarvc_vkorg.
    CLEAR ls_marc_werks.
    ls_marc_werks-sign   = 'I'.
    ls_marc_werks-option = 'EQ'.
    ls_marc_werks-low    = ls_tvarvc_vkorg-high.
    APPEND ls_marc_werks TO lr_marc_werks[].
  ENDLOOP.

  CLEAR ls_marc_werks.
  ls_marc_werks-sign   = 'I'.
  ls_marc_werks-option = 'EQ'.
  ls_marc_werks-low    = 'RFST'.
  APPEND ls_marc_werks TO lr_marc_werks[].






  IF fu_t_mara[] IS NOT INITIAL.

* MAKT
    SELECT matnr spras maktx maktg
    FROM makt
    INTO CORRESPONDING FIELDS OF TABLE fc_t_makt[]
    FOR ALL ENTRIES IN fu_t_mara[]
    WHERE matnr = fu_t_mara-matnr
      AND spras = sy-langu.

* MAW1
    SELECT matnr wvrkm wausm wherl
    FROM maw1
    INTO CORRESPONDING FIELDS OF TABLE fc_t_maw1[]
    FOR ALL ENTRIES IN fu_t_mara[]
    WHERE matnr = fu_t_mara-matnr.

* MARM
    SELECT matnr meinh umrez umren eannr ean11 numtp laeng
           breit hoehe meabm volum voleh brgew gewei mesub
    FROM marm
    INTO CORRESPONDING FIELDS OF TABLE fc_t_marm[]
    FOR ALL ENTRIES IN fu_t_mara[]
    WHERE matnr = fu_t_mara-matnr.

* MEAN
    SELECT matnr meinh lfnum ean11 eantp hpean
    FROM mean
    INTO CORRESPONDING FIELDS OF TABLE fc_t_mean[]
    FOR ALL ENTRIES IN fu_t_mara[]
    WHERE matnr = fu_t_mara-matnr.

* MARC
    SELECT matnr werks bwscl zzonline_status zz_pbs
    FROM marc
    INTO CORRESPONDING FIELDS OF TABLE fc_t_marc[]
    FOR ALL ENTRIES IN fu_t_mara[]
    WHERE matnr EQ fu_t_mara-matnr
      AND werks IN lr_marc_werks[].

* EINA
    SELECT *
    FROM eina
    INTO CORRESPONDING FIELDS OF TABLE fc_t_eina[]
    FOR ALL ENTRIES IN fu_t_mara[]
    WHERE matnr = fu_t_mara-matnr.
    IF sy-subrc EQ 0.
* EINE
      SELECT *
      FROM eine
      INTO CORRESPONDING FIELDS OF TABLE fc_t_eine[]
      FOR ALL ENTRIES IN fc_t_eina[]
      WHERE infnr EQ fc_t_eina-infnr
        AND ekorg IN fc_t_tvarvc_ekorg[]
        AND werks EQ space.
    ENDIF.

* MVKE
    SELECT matnr vkorg vtweg zzcatman sstuf vrkme prodh versg ktgrm vmsta vmstd aumng
    FROM mvke
    INTO CORRESPONDING FIELDS OF TABLE fc_t_mvke[]
    FOR ALL ENTRIES IN fu_t_mara[]
    WHERE matnr = fu_t_mara-matnr
      AND (
            ( ( vkorg EQ '3000' OR vkorg EQ zcl_constants=>gc_vkorg_4000
              OR vkorg EQ zcl_constants=>gc_vkorg_5000 OR vkorg EQ zcl_constants=>gc_vkorg_6000 )
              AND vtweg EQ '20' )
*              AND ( zzcatman NE '' OR sstuf NE '' ) )
          OR
            ( vkorg EQ zcl_constants=>gc_banner_1000
              AND vtweg EQ zcl_constants=>gc_distr_chan_10 )
*              AND zzcatman NE '' )
          ).

* WLK2
    SELECT *
    FROM wlk2
    INTO CORRESPONDING FIELDS OF TABLE fc_t_wlk2[]
    FOR ALL ENTRIES IN fu_t_mara[]
    WHERE matnr = fu_t_mara-matnr
      AND werks EQ ''.

  ENDIF.   " IF fu_t_mara[] IS INITIAL










  IF fu_s_prod_data_all-zarn_nat_mc_hier[] IS NOT INITIAL.
* Get UOM  from Merch Cat.
    SELECT a~matnr a~matkl a~mtart a~meins a~attyp a~zzfan
      INTO CORRESPONDING FIELDS OF TABLE fc_t_ref_article[]
      FROM mara AS a
      JOIN t023 AS b
      ON a~matnr = b~wwgda
      FOR ALL ENTRIES IN fu_s_prod_data_all-zarn_nat_mc_hier[]
      WHERE b~matkl = fu_s_prod_data_all-zarn_nat_mc_hier-nat_mc_code.


* Get MC Sub Department
    SELECT * FROM zmd_mc_subdep
      INTO CORRESPONDING FIELDS OF TABLE fc_t_mc_subdep[]
      FOR ALL ENTRIES IN fu_s_prod_data_all-zarn_nat_mc_hier[]
      WHERE matkl = fu_s_prod_data_all-zarn_nat_mc_hier-nat_mc_code.
    IF fc_t_mc_subdep[] IS NOT INITIAL.

* Host Product Type
      SELECT * FROM zmd_host_prdtyp
        INTO CORRESPONDING FIELDS OF TABLE fc_t_host_prdtyp[]
        FOR ALL ENTRIES IN fc_t_mc_subdep[]
        WHERE wwgha = fc_t_mc_subdep-subdep.
    ENDIF.

  ENDIF.



* PIR Vendor
  CLEAR lt_pir_vendor[].
  LOOP AT fu_s_prod_data_all-zarn_pir[] INTO ls_pir.
    CLEAR ls_pir_vendor.
    ls_pir_vendor-bbbnr = ls_pir-gln+0(7).
    ls_pir_vendor-bbsnr = ls_pir-gln+7(5).
    ls_pir_vendor-bubkz = ls_pir-gln+12(1).
    INSERT ls_pir_vendor INTO TABLE lt_pir_vendor[].
  ENDLOOP.

  IF lt_pir_vendor[] IS NOT INITIAL.

    SELECT lifnr bbbnr bbsnr bubkz                      "#EC CI_NOFIELD
      INTO CORRESPONDING FIELDS OF TABLE fc_t_pir_vendor[]
       FROM lfa1
      FOR ALL ENTRIES IN lt_pir_vendor[]
              WHERE bbbnr EQ lt_pir_vendor-bbbnr
                AND bbsnr EQ lt_pir_vendor-bbsnr
                AND bubkz EQ lt_pir_vendor-bubkz.

    IF fc_v_pir_ekorg    IS NOT INITIAL AND
       fc_t_pir_vendor[] IS NOT INITIAL.

      SELECT lifnr ekorg ekgrp plifz
        INTO CORRESPONDING FIELDS OF TABLE fc_t_lfm1[]
             FROM lfm1
        FOR ALL ENTRIES IN fc_t_pir_vendor[]
             WHERE lifnr EQ fc_t_pir_vendor-lifnr
               AND ekorg EQ fc_v_pir_ekorg.
    ENDIF.
  ENDIF.


* Fan Id from National
  IF fu_s_prod_data_all-zarn_products[] IS NOT INITIAL.
    SELECT matnr matkl mtart meins attyp zzfan
      FROM mara
    INTO CORRESPONDING FIELDS OF TABLE fc_t_fanid[]
      FOR ALL ENTRIES IN fu_s_prod_data_all-zarn_products[]
      WHERE zzfan = fu_s_prod_data_all-zarn_products-fan_id.
  ENDIF.






  SELECT * FROM zarn_uom_cat
    INTO CORRESPONDING FIELDS OF TABLE fc_t_uom_cat[].

  SELECT * FROM zarn_uomcategory
    INTO CORRESPONDING FIELDS OF TABLE fc_t_uomcategory[].

  SELECT * FROM zarn_prod_uom_t
  INTO CORRESPONDING FIELDS OF TABLE fc_t_prod_uom_t[]
  WHERE spras = sy-langu.

  SELECT * FROM zarn_coo_t
    INTO CORRESPONDING FIELDS OF TABLE fc_t_coo_t[]
    WHERE spras = sy-langu.

  SELECT * FROM zarn_gen_uom_t
    INTO CORRESPONDING FIELDS OF TABLE fc_t_gen_uom_t[]
    WHERE spras = sy-langu.

  SELECT msehi isocode FROM t006
    INTO CORRESPONDING FIELDS OF TABLE fc_t_t006[].

  SELECT land1 intca FROM t005
    INTO CORRESPONDING FIELDS OF TABLE fc_t_t005[].

  SELECT waers isocd FROM tcurc
  INTO CORRESPONDING FIELDS OF TABLE fc_t_tcurc[].

  SELECT mtart vmtpo FROM t134
    INTO CORRESPONDING FIELDS OF TABLE fc_t_t134[].

  SELECT vkorg vtweg spart FROM tvta
    INTO CORRESPONDING FIELDS OF TABLE fc_t_tvta[].






  IF fc_t_marm[] IS NOT INITIAL.

    LOOP AT fc_t_marm[] INTO ls_marm.

      CLEAR ls_mara.
      READ TABLE fu_t_mara INTO ls_mara
      WITH TABLE KEY matnr = ls_marm-matnr.
      IF sy-subrc = 0.

        CLEAR ls_products.
        READ TABLE fu_s_prod_data_all-zarn_products[] INTO ls_products
        WITH KEY fan_id = ls_mara-zzfan.
        IF sy-subrc = 0.

          CLEAR ls_uom_cat.
          READ TABLE fc_t_uom_cat INTO ls_uom_cat
          WITH KEY uom = ls_marm-meinh.
          CLEAR ls_marm_idno.
          ls_marm_idno-idno         = ls_products-idno.
          ls_marm_idno-meinh        = ls_marm-meinh.
          ls_marm_idno-umrez        = ls_marm-umrez.
          ls_marm_idno-umren        = ls_marm-umren.
          ls_marm_idno-mesub        = ls_marm-mesub.
          ls_marm_idno-matnr        = ls_marm-matnr.
          ls_marm_idno-uom_category = ls_uom_cat-uom_category.
          ls_marm_idno-seqno        = ls_uom_cat-seqno.
          ls_marm_idno-uom          = ls_uom_cat-uom.
          ls_marm_idno-cat_seqno    = ls_uom_cat-cat_seqno.


* If UOM cat is not 'Retail' and LUOM is blank then consider BAse UOM as LUOM
          IF ls_uom_cat-uom_category NE 'RETAIL' AND ls_marm_idno-mesub IS INITIAL.
            CLEAR ls_fanid.
            READ TABLE fc_t_fanid INTO ls_fanid
            WITH KEY matnr = ls_marm-matnr.
            IF sy-subrc = 0.
              ls_marm_idno-mesub = ls_fanid-meins.
            ENDIF.
          ENDIF.

          APPEND ls_marm_idno TO fc_t_marm_idno[].

        ENDIF.  " read PRODUCTS
      ENDIF.  " read MARA
    ENDLOOP.

  ENDIF.



ENDFORM.                    " GET_SAP_DATA
*&---------------------------------------------------------------------*
*&      Form  BUILD_ARENA_DATA
*&---------------------------------------------------------------------*
* Build ARENA Data
*----------------------------------------------------------------------*
FORM build_arena_data USING fu_t_fan            TYPE ty_t_fan
                            fu_t_idno_data      TYPE ty_t_idno_data
                            fu_t_prod_data      TYPE ty_t_prod_data
                            fu_t_reg_data       TYPE ty_t_reg_data
                            fu_t_mara           TYPE ty_t_mara
                            fu_t_makt           TYPE ty_t_makt
                            fu_t_maw1           TYPE ty_t_maw1
                            fu_t_marm           TYPE ty_t_marm
                            fu_t_marm_idno      TYPE ty_t_marm_idno
                            fu_t_mean           TYPE ty_t_mean
                            fu_t_eina           TYPE ty_t_eina
                            fu_t_eine           TYPE ty_t_eine
                            fu_t_mvke           TYPE ty_t_mvke
                            fu_t_wlk2           TYPE ty_t_wlk2
                            fu_t_t006           TYPE ty_t_t006
                            fu_t_t005           TYPE ty_t_t005
                            fu_t_tcurc          TYPE ty_t_tcurc
                            fu_t_coo_t          TYPE ty_t_coo_t
                            fu_t_gen_uom_t      TYPE ty_t_gen_uom_t
                            fu_t_t134           TYPE ty_t_t134
                            fu_t_tvarvc_ekorg   TYPE ty_r_ekorg
                            fu_t_tvarvc_vkorg   TYPE ty_r_vkorg
                            fu_t_tvarvc_p_org   TYPE tvarvc_t
                            fu_t_tvarvc_uni_lni TYPE tvarvc_t
                            fu_t_tvarvc_hyb_code TYPE tvarvc_t
                            fu_t_tvta           TYPE tvta_tt
                            fu_t_lfm1           TYPE ty_t_lfm1
                            fu_t_fanid          TYPE ty_t_fanid
                            fu_t_prod_uom_t     TYPE ty_t_prod_uom_t
                            fu_t_uom_cat        TYPE ty_t_uom_cat
                            fu_t_uomcategory    TYPE ty_t_uomcategory
                            fu_t_ref_article    TYPE ty_t_ref_article
                            fu_t_pir_vendor     TYPE ty_t_pir_vendor
                            fu_t_tvarv_ean_cate TYPE tvarvc_t
                            fu_t_host_prdtyp    TYPE ty_t_host_prdtyp
                            fu_t_mc_subdep      TYPE ty_t_mc_subdep
                            fu_v_ekorg          TYPE ekorg
                            fu_o_gui_load       TYPE REF TO zcl_arn_gui_load
                   CHANGING fc_t_reg_data_def        TYPE ty_t_reg_data
                            fc_t_headdata            TYPE mat_bapie1mathead_tty
                            fc_t_clientdata          TYPE bapie1marart_tab
                            fc_t_clientext           TYPE bapie1maraextrt_tab
                            fc_t_addnlclientdata     TYPE bapie1maw1rt_tab
                            fc_t_materialdescription TYPE bapie1maktrt_tab
                            fc_t_unitsofmeasure      TYPE bapie1marmrt_tab
                            fc_t_internationalartno  TYPE bapie1meanrt_tab
                            fc_t_inforecord_general  TYPE wrf_bapieina_tty
                            fc_t_inforecord_purchorg TYPE wrf_bapieine_tty
                            fc_t_salesdata           TYPE bapie1mvkert_tab
                            fc_t_salesext            TYPE bapie1mvkeextrt_tab
                            fc_t_posdata             TYPE bapie1wlk2rt_tab
                            fc_t_plantdata           TYPE bapie1marcrt_tab
                            fc_t_plantext            TYPE bapie1marcextrt_tab
                            fc_t_bapi_clientext      TYPE zmd_t_bapi_clientext
                            fc_t_bapi_salesext       TYPE zmd_t_bapi_salesext
                            fc_t_bapi_plantext       TYPE zmd_t_bapi_plantext.


  DATA: ls_idno_data           TYPE ty_s_idno_data,
        ls_prod_data           TYPE zsarn_prod_data,
        ls_reg_data            TYPE zsarn_reg_data,
        lt_reg_data_def        TYPE ty_t_reg_data,
        ls_reg_data_def        TYPE ty_s_reg_data,

        lr_status              TYPE ztarn_status_range,
        lt_host_sap            TYPE ztmd_host_sap,
        ls_matgrp_hier         TYPE wrf_matgrp_hier,
        lt_matgrp_prod         TYPE wrf_t_matgrp_prod,
        lt_art_hier            TYPE ztarn_art_hier,
        ls_reg_hdr             TYPE zarn_reg_hdr,
        ls_t134                TYPE ty_s_t134,
        ls_reg_uom             TYPE zarn_reg_uom,


        lt_clientdata          TYPE bapie1marart_tab,
        lt_clientext           TYPE bapie1maraextrt_tab,
        lt_bapi_clientext      TYPE zmd_t_bapi_clientext,
        lt_addnlclientdata     TYPE bapie1maw1rt_tab,
        lt_materialdescription TYPE bapie1maktrt_tab,
        lt_unitsofmeasure      TYPE bapie1marmrt_tab,
        lt_internationalartnos TYPE bapie1meanrt_tab,
        lt_inforecord_general  TYPE wrf_bapieina_tty,
        lt_inforecord_purchorg TYPE wrf_bapieine_tty,
        lt_salesdata           TYPE bapie1mvkert_tab,
        lt_salesext            TYPE bapie1mvkeextrt_tab,
        lt_bapi_salesext       TYPE zmd_t_bapi_salesext,
        lt_posdata             TYPE bapie1wlk2rt_tab,
        lt_plantdata           TYPE bapie1marcrt_tab,
        lt_plantext            TYPE bapie1marcextrt_tab,                            "++ONLD-822 JKH 17.02.2017
        lt_bapi_plantext       TYPE zmd_t_bapi_plantext,                            "++ONLD-822 JKH 17.02.2017
        lt_warehousenumberdata TYPE bapie1mlgnrt_tab,                               "++ONLD-822 JKH 17.02.2017

        ls_clientdata          TYPE bapie1marart,
        ls_clientext           TYPE bapie1maraextrt,
        ls_bapi_clientext      TYPE zmd_s_bapi_clientext,
        ls_addnlclientdata     TYPE bapie1maw1rt,
        ls_materialdescription TYPE bapie1maktrt,
        ls_unitsofmeasure      TYPE bapie1marmrt,
        ls_internationalartnos TYPE bapie1meanrt,
        ls_inforecord_general  TYPE bapieina,
        ls_inforecord_purchorg TYPE wrfbapieine,
        ls_salesdata           TYPE bapie1mvkert,
        ls_salesext            TYPE bapie1mvkeextrt,
        ls_bapi_salesext       TYPE zmd_s_bapi_salesext,
        ls_posdata             TYPE bapie1wlk2rt,
        ls_plantdata           TYPE bapie1marcrt,
        ls_plantext            TYPE bapie1marcextrt,                                "++ONLD-822 JKH 17.02.2017
        ls_bapi_plantext       TYPE zmd_s_bapi_plantext,                            "++ONLD-822 JKH 17.02.2017
        ls_warehousenumberdata TYPE bapie1mlgnrt,                                   "++ONLD-822 JKH 17.02.2017

        ls_headdata            TYPE bapie1mathead.



  DATA: lt_mara            TYPE mara_tab,
        lt_marm            TYPE marm_tab,
        lt_mean            TYPE mean_tab,
        lt_eina            TYPE mmpr_eina,
        lt_eine            TYPE mmpr_eine,
        lt_maw1            TYPE wrf_maw1_tty,
        lt_t006            TYPE tt_t006,
        lt_t005            TYPE wgds_countries_tty,
        lt_tcurc           TYPE fiappt_t_tcurc_cbr,
        lt_coo_t           TYPE ztarn_coo_t,
        lt_gen_uom_t       TYPE ztarn_gen_uom_t,
        lt_t134            TYPE wrf_t134_tty,
        lt_tvarvc_ekorg    TYPE tvarvc_t,
        ls_tvarvc_ekorg    TYPE tvarvc,
        lt_tvarvc          TYPE tvarvc_t,
        lt_tvarvc_hyb_code TYPE tvarvc_t,
        lt_tvta            TYPE tvta_tt,
        lt_uom_cat         TYPE ztarn_uom_cat,

        lv_ekorg_main      TYPE ekorg.


  FIELD-SYMBOLS: <ls_clientdata>   TYPE bapie1marart.








  lt_mara[]            = fu_t_mara[].
  lt_marm[]            = fu_t_marm[].
  lt_mean[]            = fu_t_mean[].
  lt_eina[]            = fu_t_eina[].
  lt_eine[]            = fu_t_eine[].
  lt_maw1[]            = fu_t_maw1[].
  lt_t006[]            = fu_t_t006[].
  lt_t005[]            = fu_t_t005[].
  lt_tcurc[]           = fu_t_tcurc[].
  lt_coo_t[]           = fu_t_coo_t[].
  lt_gen_uom_t[]       = fu_t_gen_uom_t[].
  lt_t134[]            = fu_t_t134[].
  lt_tvarvc_ekorg[]    = fu_t_tvarvc_p_org[].
  lt_tvarvc[]          = fu_t_tvarvc_uni_lni[].
  lt_tvarvc_hyb_code[] = fu_t_tvarvc_hyb_code[].
  lt_tvta[]            = fu_t_tvta[].
  lt_uom_cat[]         = fu_t_uom_cat[].


* Get main EKORG
  CLEAR: ls_tvarvc_ekorg, lv_ekorg_main.
  READ TABLE lt_tvarvc_ekorg[] INTO ls_tvarvc_ekorg
  WITH KEY high = abap_true.
  IF sy-subrc = 0.
    lv_ekorg_main = ls_tvarvc_ekorg-low.
  ELSE.
    lv_ekorg_main = '9999'.
  ENDIF.

* Default Regional Data

  IF fu_o_gui_load IS INITIAL.
    TRY.
        CREATE OBJECT fu_o_gui_load.
      CATCH zcx_arn_gui_load_err INTO DATA(lo_exc).
    ENDTRY.
  ENDIF.


  IF fu_o_gui_load IS NOT INITIAL.

    CLEAR: lt_reg_data_def[].
    LOOP AT fu_t_idno_data INTO ls_idno_data.

      CLEAR ls_reg_data.
      READ TABLE fu_t_reg_data[] INTO ls_reg_data
      WITH KEY idno = ls_idno_data-idno.

      CLEAR ls_prod_data.
      READ TABLE fu_t_prod_data[] INTO ls_prod_data
      WITH KEY idno = ls_idno_data-idno
               version = ls_idno_data-current_ver.
      IF sy-subrc = 0.

* Get Regional Defaulted data
        CLEAR ls_reg_data_def.
        CALL METHOD fu_o_gui_load->set_regional_default
          EXPORTING
            is_reg_data       = ls_reg_data
            is_prod_data      = ls_prod_data
            it_lfm1           = fu_t_lfm1[]
            it_fanid          = fu_t_fanid[]
            it_marm           = fu_t_marm[]
            it_marm_idno      = fu_t_marm_idno[]
            it_mean           = fu_t_mean[]
            it_prod_uom_t     = fu_t_prod_uom_t[]
            it_uom_cat        = fu_t_uom_cat[]
            it_uomcategory    = fu_t_uomcategory[]
            it_ref_article    = fu_t_ref_article[]
            it_pir_vendor     = fu_t_pir_vendor[]
            it_tvarv_ean_cate = fu_t_tvarv_ean_cate[]
            it_host_prdtyp    = fu_t_host_prdtyp[]
            it_mc_subdep      = fu_t_mc_subdep[]
            iv_pir_ekorg      = fu_v_ekorg
          IMPORTING
            es_reg_data_def   = ls_reg_data_def.

        APPEND ls_reg_data_def TO fc_t_reg_data_def[].

        CLEAR: ls_headdata.
        CLEAR: lt_clientdata[], lt_clientext[], lt_bapi_clientext[], lt_addnlclientdata[],
               lt_materialdescription[], lt_unitsofmeasure[], lt_internationalartnos[],
               lt_inforecord_general[], lt_inforecord_purchorg[], lt_salesdata[],
               lt_salesext[], lt_bapi_salesext[], lt_posdata[], lt_plantdata[],
               lt_plantext[], lt_warehousenumberdata[], lt_bapi_plantext[].        "++ONLD-822 JKH 17.02.2017

* Build Mapping Data for Article Posting
        CALL FUNCTION 'ZARN_MAP_ARTICLE_POST_DATA'
          EXPORTING
            is_idno_data           = ls_idno_data
            is_prod_data           = ls_prod_data
            is_reg_data            = ls_reg_data_def
            ir_status              = lr_status[]
            it_mara                = lt_mara[]
            it_marm                = lt_marm[]
            it_mean                = lt_mean[]
            it_eina                = lt_eina[]
            it_eine                = lt_eine[]
            it_maw1                = lt_maw1[]
            it_host_sap            = lt_host_sap[]
            it_t006                = lt_t006[]
            it_t005                = lt_t005[]
            it_tcurc               = lt_tcurc[]
            it_coo_t               = lt_coo_t[]
            it_gen_uom_t           = lt_gen_uom_t[]
            it_t134                = lt_t134[]
*decomissioning usage of TVARVC for PIR
            "it_tvarvc_ekorg        = lt_tvarvc_ekorg[]   "IS 2019/06
            it_tvarvc_hyb_code     = lt_tvarvc_hyb_code[]
            it_tvarvc              = lt_tvarvc[]
            it_tvta                = lt_tvta[]
            it_uom_cat             = lt_uom_cat[]
            is_matgrp_hier         = ls_matgrp_hier
            it_matgrp_prod         = lt_matgrp_prod[]
            it_art_hier            = lt_art_hier[]
            iv_ekorg_main          = lv_ekorg_main
*           iv_batch               = fu_v_batch
          IMPORTING
            es_headdata            = ls_headdata
            et_clientdata          = lt_clientdata[]
            et_clientext           = lt_clientext[]
            et_addnlclientdata     = lt_addnlclientdata[]
            et_materialdescription = lt_materialdescription[]
            et_unitsofmeasure      = lt_unitsofmeasure[]
            et_internationalartnos = lt_internationalartnos[]
            et_inforecord_general  = lt_inforecord_general[]
            et_inforecord_purchorg = lt_inforecord_purchorg[]
            et_salesdata           = lt_salesdata[]
            et_salesext            = lt_salesext[]
            et_posdata             = lt_posdata[]
            et_plantdata           = lt_plantdata[]
            et_plantext            = lt_plantext[]                                  "++ONLD-822 JKH 17.02.2017
            et_warehousenumberdata = lt_warehousenumberdata[]                       "++ONLD-822 JKH 17.02.2017
            et_bapi_clientext      = lt_bapi_clientext[]
            et_bapi_salesext       = lt_bapi_salesext[]
            et_bapi_plantext       = lt_bapi_plantext[].                            "++ONLD-822 JKH 17.02.2017


* Default Regional BASIC1 fields if not filled
        CLEAR ls_reg_hdr.
        READ TABLE ls_reg_data_def-zarn_reg_hdr[] INTO ls_reg_hdr INDEX 1.
        IF sy-subrc = 0.

          IF ls_headdata-matl_group IS INITIAL.
            ls_headdata-matl_group = ls_reg_hdr-matkl.
          ENDIF.

          IF ls_headdata-matl_type IS INITIAL.
            ls_headdata-matl_type  = ls_reg_hdr-mtart.
          ENDIF.

          IF ls_headdata-matl_cat IS INITIAL.
            ls_headdata-matl_cat   = ls_reg_hdr-attyp.
          ENDIF.


          IF <ls_clientdata> IS ASSIGNED. UNASSIGN <ls_clientdata>. ENDIF.
          READ TABLE lt_clientdata[] ASSIGNING <ls_clientdata>
          WITH KEY material = ls_headdata-material.
          IF sy-subrc = 0.

            IF <ls_clientdata>-base_uom IS INITIAL.
              CLEAR ls_reg_uom.
              READ TABLE ls_reg_data_def-zarn_reg_uom[] INTO ls_reg_uom
              WITH KEY idno      = ls_reg_data_def-idno
                       unit_base = abap_true.
              IF sy-subrc = 0.
                <ls_clientdata>-base_uom = ls_reg_uom-meinh.
              ENDIF.
            ENDIF.  " IF <ls_clientdata>-base_uom IS INITIAL

            IF <ls_clientdata>-item_cat IS INITIAL.
              CLEAR ls_t134.
              READ TABLE fu_t_t134 INTO ls_t134
                WITH TABLE KEY mtart = ls_headdata-matl_type.
              IF sy-subrc = 0.
                <ls_clientdata>-item_cat = ls_t134-vmtpo.
              ENDIF.
            ENDIF.  " IF <ls_clientdata>-item_cat IS INITIAL

          ENDIF.  " IF sy-subrc = 0


        ENDIF.  " READ TABLE ls_reg_data_def-zarn_reg_hdr[] INTO ls_reg_hdr







        APPEND          ls_headdata              TO fc_t_headdata[].
        APPEND LINES OF lt_clientdata[]          TO fc_t_clientdata[].
        APPEND LINES OF lt_clientext[]           TO fc_t_clientext[].
        APPEND LINES OF lt_addnlclientdata[]     TO fc_t_addnlclientdata[].
        APPEND LINES OF lt_materialdescription[] TO fc_t_materialdescription[].
        APPEND LINES OF lt_unitsofmeasure[]      TO fc_t_unitsofmeasure[].
        APPEND LINES OF lt_internationalartnos[] TO fc_t_internationalartno[].
        APPEND LINES OF lt_inforecord_general[]  TO fc_t_inforecord_general[].
        APPEND LINES OF lt_inforecord_purchorg[] TO fc_t_inforecord_purchorg[].
        APPEND LINES OF lt_salesdata[]           TO fc_t_salesdata[].
        APPEND LINES OF lt_salesext[]            TO fc_t_salesext[].
        APPEND LINES OF lt_posdata[]             TO fc_t_posdata[].
        APPEND LINES OF lt_plantdata[]           TO fc_t_plantdata[].
        APPEND LINES OF lt_plantext[]            TO fc_t_plantext[].                "++ONLD-822 JKH 17.02.2017
        APPEND LINES OF lt_bapi_clientext[]      TO fc_t_bapi_clientext[].
        APPEND LINES OF lt_bapi_salesext[]       TO fc_t_bapi_salesext[].
        APPEND LINES OF lt_bapi_plantext[]       TO fc_t_bapi_plantext[].           "++ONLD-822 JKH 17.02.2017

      ENDIF.

    ENDLOOP.  " LOOP AT fu_t_idno_data INTO ls_idno_data

  ENDIF.  " IF fu_o_gui_load IS NOT INITIAL

* Build ARENA Article Post Data




ENDFORM.                    " BUILD_ARENA_DATA
*&---------------------------------------------------------------------*
*&      Form  BUILD_COMPARE_ECC_DATA
*&---------------------------------------------------------------------*
* Build Comparision from ECC Data
*----------------------------------------------------------------------*
FORM build_compare_ecc_data USING fu_t_fan           TYPE ty_t_fan
                                  fu_t_idno_data     TYPE ty_t_idno_data
                                  fu_t_mara          TYPE ty_t_mara
                                  fu_t_makt          TYPE ty_t_makt
                                  fu_t_maw1          TYPE ty_t_maw1
                                  fu_t_marm          TYPE ty_t_marm
                                  fu_t_mean          TYPE ty_t_mean
                                  fu_t_marc          TYPE ty_t_marc
                                  fu_t_eina          TYPE ty_t_eina
                                  fu_t_eine          TYPE ty_t_eine
                                  fu_t_mvke          TYPE ty_t_mvke
                                  fu_t_wlk2          TYPE ty_t_wlk2
                         CHANGING fc_t_headdata            TYPE mat_bapie1mathead_tty
                                  fc_t_clientdata          TYPE bapie1marart_tab
                                  fc_t_clientext           TYPE bapie1maraextrt_tab
                                  fc_t_addnlclientdata     TYPE bapie1maw1rt_tab
                                  fc_t_materialdescription TYPE bapie1maktrt_tab
                                  fc_t_unitsofmeasure      TYPE bapie1marmrt_tab
                                  fc_t_internationalartno  TYPE bapie1meanrt_tab
                                  fc_t_inforecord_general  TYPE wrf_bapieina_tty
                                  fc_t_inforecord_purchorg TYPE wrf_bapieine_tty
                                  fc_t_salesdata           TYPE bapie1mvkert_tab
                                  fc_t_salesext            TYPE bapie1mvkeextrt_tab
                                  fc_t_posdata             TYPE bapie1wlk2rt_tab
                                  fc_t_plantdata           TYPE bapie1marcrt_tab
                                  fc_t_plantext            TYPE bapie1marcextrt_tab
                                  fc_t_bapi_clientext      TYPE zmd_t_bapi_clientext
                                  fc_t_bapi_salesext       TYPE zmd_t_bapi_salesext
                                  fc_t_bapi_plantext       TYPE zmd_t_bapi_plantext.

  DATA: lt_headdata            TYPE mat_bapie1mathead_tty,
        lt_clientdata          TYPE bapie1marart_tab,
        lt_clientext           TYPE bapie1maraextrt_tab,
        lt_bapi_clientext      TYPE zmd_t_bapi_clientext,
        lt_addnlclientdata     TYPE bapie1maw1rt_tab,
        lt_materialdescription TYPE bapie1maktrt_tab,
        lt_unitsofmeasure      TYPE bapie1marmrt_tab,
        lt_internationalartnos TYPE bapie1meanrt_tab,
        lt_inforecord_general  TYPE wrf_bapieina_tty,
        lt_inforecord_purchorg TYPE wrf_bapieine_tty,
        lt_salesdata           TYPE bapie1mvkert_tab,
        lt_salesext            TYPE bapie1mvkeextrt_tab,
        lt_bapi_salesext       TYPE zmd_t_bapi_salesext,
        lt_posdata             TYPE bapie1wlk2rt_tab,
        lt_plantdata           TYPE bapie1marcrt_tab,
        lt_plantext            TYPE bapie1marcextrt_tab,                            "++ONLD-822 JKH 17.02.2017
        lt_bapi_plantext       TYPE zmd_t_bapi_plantext,                            "++ONLD-822 JKH 17.02.2017

        ls_headdata            TYPE bapie1mathead,
        ls_clientdata          TYPE bapie1marart,
        ls_clientext           TYPE bapie1maraextrt,
        ls_bapi_clientext      TYPE zmd_s_bapi_clientext,
        ls_addnlclientdata     TYPE bapie1maw1rt,
        ls_materialdescription TYPE bapie1maktrt,
        ls_unitsofmeasure      TYPE bapie1marmrt,
        ls_internationalartnos TYPE bapie1meanrt,
        ls_inforecord_general  TYPE bapieina,
        ls_inforecord_purchorg TYPE wrfbapieine,
        ls_salesdata           TYPE bapie1mvkert,
        ls_salesext            TYPE bapie1mvkeextrt,
        ls_bapi_salesext       TYPE zmd_s_bapi_salesext,
        ls_posdata             TYPE bapie1wlk2rt,
        ls_plantdata           TYPE bapie1marcrt,
        ls_plantext            TYPE bapie1marcextrt,                                "++ONLD-822 JKH 17.02.2017
        ls_bapi_plantext       TYPE zmd_s_bapi_plantext,                            "++ONLD-822 JKH 17.02.2017

        ls_mara                TYPE mara,
        ls_makt                TYPE makt,
        ls_maw1                TYPE maw1,
        ls_marm                TYPE marm,
        ls_mean                TYPE mean,
        ls_marc                TYPE marc,
        ls_eina                TYPE eina,
        ls_eine                TYPE eine,
        ls_mvke                TYPE mvke,
        ls_wlk2                TYPE wlk2.


  LOOP AT fu_t_mara INTO ls_mara.

*** CLIENTDATA
    CLEAR ls_clientdata.
    ls_clientdata-material = ls_mara-matnr.

    ls_clientdata-created_on = ls_mara-ersda.
    ls_clientdata-created_by = ls_mara-ernam.
    ls_clientdata-valid_from = ls_mara-datab.
    ls_clientdata-last_chnge = ls_mara-laeda.
    ls_clientdata-changed_by = ls_mara-aenam.
    ls_clientdata-countryori = ls_mara-herkl.
*    ls_clientdata-countryori_iso = ls_mara-herkl.

    ls_clientdata-cont_unit      = ls_mara-inhme.
*    ls_clientdata-cont_unit_iso  = ls_mara-inhme.
    ls_clientdata-net_cont       = ls_mara-inhal.

    ls_clientdata-base_uom        = ls_mara-meins.
*    ls_clientdata-base_uom_iso    = ls_mara-meins.
    ls_clientdata-item_cat        = ls_mara-mtpos_mara.
    ls_clientdata-po_unit         = ls_mara-bstme.
*    ls_clientdata-po_unit_iso     = ls_mara-bstme.
    ls_clientdata-net_weight      = ls_mara-ntgew.
    ls_clientdata-temp_conds      = ls_mara-tempb.
    ls_clientdata-season          = ls_mara-saiso.
    ls_clientdata-prod_hier       = ls_mara-prdha.
    ls_clientdata-minremlife      = ls_mara-mhdrz.
    ls_clientdata-saeson_yr       = ls_mara-saisj.
    ls_clientdata-pur_status      = ls_mara-mstae.
    ls_clientdata-sal_status      = ls_mara-mstav.
    ls_clientdata-pvalidfrom      = ls_mara-mstde.
    ls_clientdata-svalidfrom      = ls_mara-mstdv.
    ls_clientdata-creation_status = ls_mara-bstat.
    ls_clientdata-brand_id        = ls_mara-brand_id.


    APPEND ls_clientdata TO lt_clientdata[].


*** CLIENTEXT
    CLEAR ls_bapi_clientext.
    ls_bapi_clientext-material = ls_mara-matnr.

    ls_bapi_clientext-zzfan                 = ls_mara-zzfan.
    ls_bapi_clientext-zzlni_repack          = ls_mara-zzlni_repack.
    ls_bapi_clientext-zzlni_bulk            = ls_mara-zzlni_bulk.
    ls_bapi_clientext-zzlni_prboly          = ls_mara-zzlni_prboly.
    ls_bapi_clientext-zzunnum               = ls_mara-zzunnum.
    ls_bapi_clientext-zzpackgrp             = ls_mara-zzpackgrp.
    ls_bapi_clientext-zzhcls                = ls_mara-zzhcls.
    ls_bapi_clientext-zzhsno                = ls_mara-zzhsno.

    ls_bapi_clientext-zzattr1               = ls_mara-zzattr1.
    ls_bapi_clientext-zzattr2               = ls_mara-zzattr2.
    ls_bapi_clientext-zzattr3               = ls_mara-zzattr3.
    ls_bapi_clientext-zzattr4               = ls_mara-zzattr4.
    ls_bapi_clientext-zzattr5               = ls_mara-zzattr5.
    ls_bapi_clientext-zzattr6               = ls_mara-zzattr6.
    ls_bapi_clientext-zzattr7               = ls_mara-zzattr7.
    ls_bapi_clientext-zzattr8               = ls_mara-zzattr8.
    ls_bapi_clientext-zzattr9               = ls_mara-zzattr9.
    ls_bapi_clientext-zzattr10              = ls_mara-zzattr10.
    ls_bapi_clientext-zzattr11              = ls_mara-zzattr11.
    ls_bapi_clientext-zzattr12              = ls_mara-zzattr12.
    ls_bapi_clientext-zzattr13              = ls_mara-zzattr13.
    ls_bapi_clientext-zzattr14              = ls_mara-zzattr14.
    ls_bapi_clientext-zzattr15              = ls_mara-zzattr15.
    ls_bapi_clientext-zzattr16              = ls_mara-zzattr16.
    ls_bapi_clientext-zzattr17              = ls_mara-zzattr17.
    ls_bapi_clientext-zzattr18              = ls_mara-zzattr18.
    ls_bapi_clientext-zzattr19              = ls_mara-zzattr19.
    ls_bapi_clientext-zzattr20              = ls_mara-zzattr20.
    ls_bapi_clientext-zzattr21              = ls_mara-zzattr21.
    ls_bapi_clientext-zzattr22              = ls_mara-zzattr22.
    ls_bapi_clientext-zzattr23              = ls_mara-zzattr23.

    ls_bapi_clientext-zztktprnt             = ls_mara-zztktprnt.
    ls_bapi_clientext-zzhard                = ls_mara-zzhard.
    ls_bapi_clientext-zzprep                = ls_mara-zzprep.
    ls_bapi_clientext-zzstrd                = ls_mara-zzstrd.
    ls_bapi_clientext-zzcomp_prod           = ls_mara-zzcomp_prod.
    ls_bapi_clientext-zzprice_book_fam      = ls_mara-zzprice_book_fam.
    ls_bapi_clientext-zzprice_book_desc     = ls_mara-zzprice_book_desc.
    ls_bapi_clientext-zzas4desc             = ls_mara-zzas4desc.
    ls_bapi_clientext-zzas4pksize           = ls_mara-zzas4pksize.
    ls_bapi_clientext-zzas4subdept          = ls_mara-zzas4subdept.
    ls_bapi_clientext-zzpwdesc              = ls_mara-zzpwdesc.
    ls_bapi_clientext-zzbbdf                = ls_mara-zzbbdf.
    ls_bapi_clientext-zzbestbefore          = ls_mara-zzbestbefore.
    ls_bapi_clientext-zzdatatype            = ls_mara-zzdatatype.
    ls_bapi_clientext-zzimage_ref_art       = ls_mara-zzimage_ref_art.
    ls_bapi_clientext-zzimage_name          = ls_mara-zzimage_name.
    ls_bapi_clientext-zzsco_weighed_flag    = ls_mara-zzsco_weighed_flag.
    ls_bapi_clientext-zzvar_wt_flag         = ls_mara-zzvar_wt_flag.
    ls_bapi_clientext-zzprdtype             = ls_mara-zzprdtype.
*    ls_bapi_clientext-zzselling_only        = ls_mara-zzselling_only. " --ONED-217 JKH 24.11.2016
    ls_bapi_clientext-zzbuy_sell            = ls_mara-zzbuy_sell.      " ++ONED-217 JKH 24.11.2016
    ls_bapi_clientext-zz_uni_dc             = ls_mara-zz_uni_dc.
    ls_bapi_clientext-zz_lni_dc             = ls_mara-zz_lni_dc.
    ls_bapi_clientext-zz_nonsap_update_date = ls_mara-zz_nonsap_update_date.
    ls_bapi_clientext-zz_nonsap_update_time = ls_mara-zz_nonsap_update_time.
    ls_bapi_clientext-zzsell                = ls_mara-zzsell.
    ls_bapi_clientext-zzuse                 = ls_mara-zzuse.
    ls_bapi_clientext-zzstrg                = ls_mara-zzstrg.
    ls_bapi_clientext-zzlabnum              = ls_mara-zzlabnum.
    ls_bapi_clientext-zzpkddt               = ls_mara-zzpkddt.
    ls_bapi_clientext-zzmandst              = ls_mara-zzmandst.
    ls_bapi_clientext-zzwnmsg               = ls_mara-zzwnmsg.
    ls_bapi_clientext-zzpedesc_flag         = ls_mara-zzpedesc_flag.
    ls_bapi_clientext-zzingretype           = ls_mara-zzingretype.
    ls_bapi_clientext-yy_publish_date       = ls_mara-yy_publish_date.
    ls_bapi_clientext-yy_format             = ls_mara-yy_format.
    ls_bapi_clientext-yy_author             = ls_mara-yy_author.
    ls_bapi_clientext-yy_publisher          = ls_mara-yy_publisher .
    ls_bapi_clientext-yy_brand              = ls_mara-yy_brand.
    ls_bapi_clientext-zzrange_flag          = ls_mara-zzrange_flag.
    ls_bapi_clientext-zzrange_avail         = ls_mara-zzrange_avail.

    APPEND ls_bapi_clientext TO lt_bapi_clientext[].


*** HEADDATA
    CLEAR ls_headdata.
    ls_headdata-material   = ls_mara-matnr.
    ls_headdata-matl_group = ls_mara-matkl.
    ls_headdata-matl_type  = ls_mara-mtart.
    ls_headdata-matl_cat   = ls_mara-attyp.
    APPEND ls_headdata TO lt_headdata[].



  ENDLOOP.  " LOOP AT fu_t_mara INTO ls_mara



  LOOP AT fu_t_makt INTO ls_makt.

*** MATERIALDESCRIPTION
    CLEAR ls_materialdescription.
    ls_materialdescription-material = ls_makt-matnr.

    ls_materialdescription-langu     = ls_makt-spras.
*    ls_materialdescription-langu_iso = ls_makt-spras.
    ls_materialdescription-matl_desc = ls_makt-maktx.

    APPEND ls_materialdescription TO lt_materialdescription[].

  ENDLOOP.  " LOOP AT fu_t_makt INTO ls_makt



  LOOP AT fu_t_maw1 INTO ls_maw1.

*** ADDNLCLIENTDATA
    CLEAR ls_addnlclientdata.
    ls_addnlclientdata-material = ls_maw1-matnr.

    ls_addnlclientdata-li_proc_st     = ls_maw1-lstfl.
    ls_addnlclientdata-li_proc_dc     = ls_maw1-lstvz.
    ls_addnlclientdata-list_st_fr     = ls_maw1-ldvfl.
    ls_addnlclientdata-list_st_to     = ls_maw1-ldbfl.
    ls_addnlclientdata-list_dc_fr     = ls_maw1-ldvzl.
    ls_addnlclientdata-list_dc_to     = ls_maw1-ldbzl.
    ls_addnlclientdata-sell_st_fr     = ls_maw1-vdvfl.
    ls_addnlclientdata-sell_st_to     = ls_maw1-vdbfl.
    ls_addnlclientdata-sell_dc_fr     = ls_maw1-vdvzl.
    ls_addnlclientdata-sell_dc_to     = ls_maw1-vdbzl.
    ls_addnlclientdata-countryori     = ls_maw1-wherl.
*    ls_addnlclientdata-countryori_iso = ls_maw1-wherl.
    ls_addnlclientdata-sales_unit     = ls_maw1-wvrkm.
*    ls_addnlclientdata-sales_unit_iso = ls_maw1-wvrkm.
    ls_addnlclientdata-issue_unit     = ls_maw1-wausm.
*    ls_addnlclientdata-issue_unit_iso = ls_maw1-wausm.

    APPEND ls_addnlclientdata TO lt_addnlclientdata[].

  ENDLOOP.  " LOOP AT fu_t_maw1 INTO ls_maw1



  LOOP AT fu_t_marm INTO ls_marm.

*** UNITSOFMEASURE
    CLEAR ls_unitsofmeasure.
    ls_unitsofmeasure-material = ls_marm-matnr.
    ls_unitsofmeasure-alt_unit = ls_marm-meinh.

*    ls_unitsofmeasure-alt_unit_iso = ls_marm-meinh.
    ls_unitsofmeasure-numerator    = ls_marm-umrez.
    ls_unitsofmeasure-denominatr   = ls_marm-umren.
    ls_unitsofmeasure-length       = ls_marm-laeng.
    ls_unitsofmeasure-width        = ls_marm-breit.
    ls_unitsofmeasure-height       = ls_marm-hoehe.
    ls_unitsofmeasure-gross_wt     = ls_marm-brgew.
    ls_unitsofmeasure-unit_dim     = ls_marm-meabm.
*    ls_unitsofmeasure-unit_dim_iso = ls_marm-meabm.
    ls_unitsofmeasure-unit_of_wt   = ls_marm-gewei.
*    ls_unitsofmeasure-unit_of_wt_iso = ls_marm-gewei.
    ls_unitsofmeasure-ean_upc      = ls_marm-ean11.
    ls_unitsofmeasure-ean_cat      = ls_marm-numtp.


    ls_unitsofmeasure-sub_uom      = ls_marm-mesub.
*        ls_unitsofmeasure-sub_uom_iso  = ls_mara-meins.


    IF ls_marm-mesub IS INITIAL.

* Get Basic UOM if Mesub is blank
      CLEAR ls_mara.
      READ TABLE fu_t_mara INTO ls_mara
      WITH KEY matnr = ls_marm-matnr.
      IF sy-subrc = 0 AND ls_marm-meinh NE ls_mara-meins.
        ls_unitsofmeasure-sub_uom      = ls_mara-meins.
*        ls_unitsofmeasure-sub_uom_iso  = ls_mara-meins.
      ENDIF.

    ENDIF.




    APPEND ls_unitsofmeasure TO lt_unitsofmeasure[].

  ENDLOOP.  " LOOP AT fu_t_marm INTO ls_marm


  LOOP AT fu_t_mean INTO ls_mean.

*** INTERNATIONALARTNOS
    CLEAR ls_internationalartnos.
    ls_internationalartnos-material = ls_mean-matnr.
    ls_internationalartnos-unit     = ls_mean-meinh.

*    ls_internationalartnos-unit_iso = ls_mean-meinh.
    ls_internationalartnos-ean_upc  = ls_mean-ean11.
    ls_internationalartnos-ean_cat  = ls_mean-eantp.

    APPEND ls_internationalartnos TO lt_internationalartnos[].

  ENDLOOP.  " LOOP AT fu_t_mean INTO ls_mean


  LOOP AT fu_t_eina INTO ls_eina.

*** INFORECORD_GENERAL
    CLEAR ls_inforecord_general.
    ls_inforecord_general-info_rec     = ls_eina-infnr.
    ls_inforecord_general-material     = ls_eina-matnr.
    ls_inforecord_general-vendor       = ls_eina-lifnr.

    ls_inforecord_general-created_at   = ls_eina-erdat.
    ls_inforecord_general-created_by   = ls_eina-ernam.
    ls_inforecord_general-var_ord_un   = ls_eina-vabme.
    ls_inforecord_general-suppl_from   = ls_eina-lifab.
    ls_inforecord_general-suppl_to     = ls_eina-lifbi.
    ls_inforecord_general-po_unit      = ls_eina-meins.
*    ls_inforecord_general-po_unit_iso  = ls_eina-meins.
    ls_inforecord_general-norm_vend    = ls_eina-relif.
    ls_inforecord_general-conv_num1    = ls_eina-umrez.
    ls_inforecord_general-conv_den1    = ls_eina-umren.
    ls_inforecord_general-vend_mat     = ls_eina-idnlf.
    ls_inforecord_general-base_uom     = ls_eina-lmein.
*    ls_inforecord_general-base_uom_iso = ls_eina-lmein.


    APPEND ls_inforecord_general TO lt_inforecord_general[].




    LOOP AT fu_t_eine INTO ls_eine
      WHERE infnr = ls_eina-infnr.

*** INFORECORD_PURCHORG
      CLEAR ls_inforecord_purchorg.

      ls_inforecord_purchorg-material   = ls_eina-matnr.
      ls_inforecord_purchorg-vendor     = ls_eina-lifnr.

      ls_inforecord_purchorg-info_rec   = ls_eine-infnr.
      ls_inforecord_purchorg-purch_org  = ls_eine-ekorg.
      ls_inforecord_purchorg-info_type  = ls_eine-esokz.
      ls_inforecord_purchorg-plant      = ls_eine-werks.

      ls_inforecord_purchorg-created_at = ls_eine-erdat.
      ls_inforecord_purchorg-created_by = ls_eine-ernam.
      ls_inforecord_purchorg-info_type  = ls_eine-esokz.
      ls_inforecord_purchorg-delete_ind = ls_eine-loekz.
      ls_inforecord_purchorg-pur_group  = ls_eine-ekgrp.
      ls_inforecord_purchorg-currency   = ls_eine-waers.
      ls_inforecord_purchorg-min_po_qty = ls_eine-minbm.
      ls_inforecord_purchorg-nrm_po_qty = ls_eine-norbm.
      ls_inforecord_purchorg-plnd_delry = ls_eine-aplfz.
      ls_inforecord_purchorg-net_price  = ls_eine-netpr.
      ls_inforecord_purchorg-price_unit = ls_eine-peinh.
      ls_inforecord_purchorg-eff_price  = ls_eine-netpr.
      ls_inforecord_purchorg-orderpr_un = ls_eine-bprme.
      ls_inforecord_purchorg-cond_group = ls_eine-ekkol.
      ls_inforecord_purchorg-tax_code   = ls_eine-mwskz.
*    ls_inforecord_purchorg-last_po    = ls_eine-datlb.
      ls_inforecord_purchorg-price_date = ls_eine-prdat.
      ls_inforecord_purchorg-overdeltol = ls_eine-uebto.
      ls_inforecord_purchorg-under_tol  = ls_eine-untto.
*    ls_inforecord_purchorg-orderpr_un_iso = ls_eine-bprme.
*    ls_inforecord_purchorg-currency_iso   = ls_eine-waers.
      ls_inforecord_purchorg-conv_num1  = ls_eine-bpumz.
      ls_inforecord_purchorg-conv_den1  = ls_eine-bpumn.

      APPEND ls_inforecord_purchorg TO lt_inforecord_purchorg[].

    ENDLOOP.  " LOOP AT fu_t_eine INTO ls_eine

  ENDLOOP.  " LOOP AT fu_t_eina INTO ls_eina



  LOOP AT fu_t_mvke INTO ls_mvke.

*** SALESDATA
    CLEAR: ls_salesdata.
    ls_salesdata-material   = ls_mvke-matnr.
    ls_salesdata-sales_org  = ls_mvke-vkorg.
    ls_salesdata-distr_chan = ls_mvke-vtweg.

    ls_salesdata-matl_stats = ls_mvke-versg.
    ls_salesdata-cash_disc  = ls_mvke-sktof.
    ls_salesdata-sal_status = ls_mvke-vmsta.
    ls_salesdata-valid_from = ls_mvke-vmstd.
    ls_salesdata-min_order  = ls_mvke-aumng.
    ls_salesdata-sales_unit = ls_mvke-vrkme.
*    ls_salesdata-sales_unit_iso = ls_mvke-vrkme.
    ls_salesdata-item_cat   = ls_mvke-mtpos.
    ls_salesdata-prod_hier  = ls_mvke-prodh.
    ls_salesdata-acct_assgt = ls_mvke-ktgrm.
    ls_salesdata-assort_lev = ls_mvke-sstuf.
    ls_salesdata-mat_pr_grp = ls_mvke-kondm.

    ls_salesdata-li_proc_st = ls_mvke-lstfl.
    ls_salesdata-li_proc_dc = ls_mvke-lstvz.
    ls_salesdata-list_st_fr = ls_mvke-ldvfl.
    ls_salesdata-list_st_to = ls_mvke-ldbfl.
    ls_salesdata-list_dc_fr = ls_mvke-ldvzl.
    ls_salesdata-list_dc_to = ls_mvke-ldbzl.
    ls_salesdata-sell_st_fr = ls_mvke-vdvfl.
    ls_salesdata-sell_st_to = ls_mvke-vdbfl.
    ls_salesdata-sell_dc_fr = ls_mvke-vdvzl.
    ls_salesdata-sell_dc_to = ls_mvke-vdbzl.

    APPEND ls_salesdata TO lt_salesdata[].



*** SALESEXT
    CLEAR: ls_bapi_salesext.
    ls_bapi_salesext-material   = ls_mvke-matnr.
    ls_bapi_salesext-sales_org  = ls_mvke-vkorg.
    ls_bapi_salesext-distr_chan = ls_mvke-vtweg.

    ls_bapi_salesext-zzcatman   = ls_mvke-zzcatman.

    APPEND ls_bapi_salesext TO lt_bapi_salesext[].

  ENDLOOP.  " LOOP AT fu_t_mvke INTO ls_mvke



  LOOP AT fu_t_wlk2 INTO ls_wlk2.

*** POSDATA
    CLEAR: ls_posdata.
    ls_posdata-material     = ls_wlk2-matnr.
    ls_posdata-sales_org    = ls_wlk2-vkorg.
    ls_posdata-distr_chan   = ls_wlk2-vtweg.
    ls_posdata-plant        = ls_wlk2-werks.

    ls_posdata-no_rep_key   = ls_wlk2-kwdht.
    ls_posdata-price_reqd   = ls_wlk2-prerf.
    ls_posdata-sal_status   = ls_wlk2-mstav.
    ls_posdata-valid_from   = ls_wlk2-mstdv.
    ls_posdata-disc_allwd   = ls_wlk2-rbzul.
    ls_posdata-scales_grp   = ls_wlk2-scagr.
    ls_posdata-sell_st_fr   = ls_wlk2-vkdab.
    ls_posdata-sell_st_to   = ls_wlk2-vkbis.

    APPEND ls_posdata TO lt_posdata[].

  ENDLOOP.  " LOOP AT fu_t_wlk2 INTO ls_wlk2



  LOOP AT fu_t_marc INTO ls_marc.

*** PLANTDATA
    CLEAR ls_plantdata.
    ls_plantdata-material = ls_marc-matnr.
    ls_plantdata-plant    = ls_marc-werks.

    ls_plantdata-sup_source = ls_marc-bwscl.

    APPEND ls_plantdata TO lt_plantdata[].


*** PLANTEXT
    CLEAR: ls_bapi_plantext.
    ls_bapi_plantext-material = ls_marc-matnr.
    ls_bapi_plantext-plant    = ls_marc-werks.

    ls_bapi_plantext-zzonline_status = ls_marc-zzonline_status.
    ls_bapi_plantext-zz_pbs          = ls_marc-zz_pbs.

    APPEND ls_bapi_plantext TO lt_bapi_plantext[].

  ENDLOOP.  " LOOP AT fu_t_marc INTO ls_marc


  APPEND LINES OF lt_headdata[]            TO fc_t_headdata[].
  APPEND LINES OF lt_clientdata[]          TO fc_t_clientdata[].
  APPEND LINES OF lt_clientext[]           TO fc_t_clientext[].
  APPEND LINES OF lt_addnlclientdata[]     TO fc_t_addnlclientdata[].
  APPEND LINES OF lt_materialdescription[] TO fc_t_materialdescription[].
  APPEND LINES OF lt_unitsofmeasure[]      TO fc_t_unitsofmeasure[].
  APPEND LINES OF lt_internationalartnos[] TO fc_t_internationalartno[].
  APPEND LINES OF lt_inforecord_general[]  TO fc_t_inforecord_general[].
  APPEND LINES OF lt_inforecord_purchorg[] TO fc_t_inforecord_purchorg[].
  APPEND LINES OF lt_salesdata[]           TO fc_t_salesdata[].
  APPEND LINES OF lt_salesext[]            TO fc_t_salesext[].
  APPEND LINES OF lt_posdata[]             TO fc_t_posdata[].
  APPEND LINES OF lt_plantdata[]           TO fc_t_plantdata[].
  APPEND LINES OF lt_plantext[]            TO fc_t_plantext[].                      "++ONLD-822 JKH 17.02.2017
  APPEND LINES OF lt_bapi_clientext[]      TO fc_t_bapi_clientext[].
  APPEND LINES OF lt_bapi_salesext[]       TO fc_t_bapi_salesext[].
  APPEND LINES OF lt_bapi_plantext[]       TO fc_t_bapi_plantext[].                 "++ONLD-822 JKH 17.02.2017


ENDFORM.                    " BUILD_COMPARE_ECC_DATA
*&---------------------------------------------------------------------*
*&      Form  BUILD_DISPLAY_DATA
*&---------------------------------------------------------------------*
* Build Comparison data to be displayed
*----------------------------------------------------------------------*
FORM build_display_data USING fu_t_fan                     TYPE ty_t_fan
                              fu_t_idno_data               TYPE ty_t_idno_data
                              fu_t_prod_data               TYPE ty_t_prod_data
                              fu_t_reg_data_def            TYPE ty_t_reg_data
                              fu_t_fanid                   TYPE ty_t_fanid
                              fu_t_tvarvc_uni_lni          TYPE tvarvc_t
                              fu_t_mean                    TYPE ty_t_mean
                              fu_t_headdata_arn            TYPE mat_bapie1mathead_tty
                              fu_t_clientdata_arn          TYPE bapie1marart_tab
                              fu_t_clientext_arn           TYPE bapie1maraextrt_tab
                              fu_t_addnlclientdata_arn     TYPE bapie1maw1rt_tab
                              fu_t_materialdescription_arn TYPE bapie1maktrt_tab
                              fu_t_unitsofmeasure_arn      TYPE bapie1marmrt_tab
                              fu_t_internationalartno_arn  TYPE bapie1meanrt_tab
                              fu_t_inforecord_general_arn  TYPE wrf_bapieina_tty
                              fu_t_inforecord_purchorg_arn TYPE wrf_bapieine_tty
                              fu_t_salesdata_arn           TYPE bapie1mvkert_tab
                              fu_t_salesext_arn            TYPE bapie1mvkeextrt_tab
                              fu_t_posdata_arn             TYPE bapie1wlk2rt_tab
                              fu_t_plantdata_arn           TYPE bapie1marcrt_tab
                              fu_t_plantext_arn            TYPE bapie1marcextrt_tab           "++ONLD-822 JKH 17.02.2017
                              fu_t_bapi_clientext_arn      TYPE zmd_t_bapi_clientext
                              fu_t_bapi_salesext_arn       TYPE zmd_t_bapi_salesext
                              fu_t_bapi_plantext_arn       TYPE zmd_t_bapi_plantext           "++ONLD-822 JKH 17.02.2017
                              fu_t_headdata                TYPE mat_bapie1mathead_tty
                              fu_t_clientdata              TYPE bapie1marart_tab
                              fu_t_clientext               TYPE bapie1maraextrt_tab
                              fu_t_addnlclientdata         TYPE bapie1maw1rt_tab
                              fu_t_materialdescription     TYPE bapie1maktrt_tab
                              fu_t_unitsofmeasure          TYPE bapie1marmrt_tab
                              fu_t_internationalartno      TYPE bapie1meanrt_tab
                              fu_t_inforecord_general      TYPE wrf_bapieina_tty
                              fu_t_inforecord_purchorg     TYPE wrf_bapieine_tty
                              fu_t_salesdata               TYPE bapie1mvkert_tab
                              fu_t_salesext                TYPE bapie1mvkeextrt_tab
                              fu_t_posdata                 TYPE bapie1wlk2rt_tab
                              fu_t_plantdata               TYPE bapie1marcrt_tab
                              fu_t_plantext                TYPE bapie1marcextrt_tab           "++ONLD-822 JKH 17.02.2017
                              fu_t_bapi_clientext          TYPE zmd_t_bapi_clientext
                              fu_t_bapi_salesext           TYPE zmd_t_bapi_salesext
                              fu_t_bapi_plantext           TYPE zmd_t_bapi_plantext           "++ONLD-822 JKH 17.02.2017
                     CHANGING fc_t_fan_exist               TYPE ty_t_fan_exist
                              fc_t_reco_basic1             TYPE ty_t_reco_basic1
                              fc_t_reco_basic2             TYPE ty_t_reco_basic2
                              fc_t_reco_attr               TYPE ty_t_reco_attr
                              fc_t_reco_uom                TYPE ty_t_reco_uom
                              fc_t_reco_ean                TYPE ty_t_reco_ean
                              fc_t_reco_pir                TYPE ty_t_reco_pir
                              fc_t_reco_banner             TYPE ty_t_reco_banner
                              fc_v_count_9002              TYPE i
                              fc_v_count_9003              TYPE i
                              fc_v_count_9004              TYPE i
                              fc_v_count_9005              TYPE i
                              fc_v_count_9006              TYPE i
                              fc_v_count_9007              TYPE i
                              fc_v_count_9008              TYPE i.





* Build Reconciliation BASIC 1 Data
  PERFORM build_reco_basic1_data USING fu_t_fan
                                       fu_t_idno_data
                                       fu_t_prod_data
                                       fu_t_reg_data_def
                                       fu_t_fanid
                                       fu_t_headdata_arn
                                       fu_t_clientdata_arn
                                       fu_t_addnlclientdata_arn
                                       fu_t_materialdescription_arn
                                       fu_t_headdata
                                       fu_t_clientdata
                                       fu_t_addnlclientdata
                                       fu_t_materialdescription
                              CHANGING fc_t_fan_exist
                                       fc_t_reco_basic1
                                       fc_v_count_9002.

* Build Reconciliation BASIC 2 Data
  PERFORM build_reco_basic2_data USING fu_t_fan
                                       fu_t_idno_data
                                       fu_t_prod_data
                                       fu_t_reg_data_def
                                       fu_t_fanid
                                       fu_t_bapi_clientext_arn
                                       fu_t_bapi_clientext
                              CHANGING fc_t_fan_exist
                                       fc_t_reco_basic2
                                       fc_v_count_9003.

* Build Reconciliation Attributes Data
  PERFORM build_reco_attr_data USING fu_t_fan
                                     fu_t_idno_data
                                     fu_t_prod_data
                                     fu_t_reg_data_def
                                     fu_t_fanid
                                     fu_t_bapi_clientext_arn
                                     fu_t_bapi_clientext
                            CHANGING fc_t_fan_exist
                                     fc_t_reco_attr
                                     fc_v_count_9008.

* Build Reconciliation UOM Data
  PERFORM build_reco_uom_data USING fu_t_fan
                                    fu_t_idno_data
                                    fu_t_prod_data
                                    fu_t_reg_data_def
                                    fu_t_fanid
                                    fu_t_unitsofmeasure_arn
                                    fu_t_unitsofmeasure
                           CHANGING fc_t_fan_exist
                                    fc_t_reco_uom
                                    fc_v_count_9004.

* Build Reconciliation EAN Data
  PERFORM build_reco_ean_data USING fu_t_fan
                                    fu_t_idno_data
                                    fu_t_prod_data
                                    fu_t_reg_data_def
                                    fu_t_fanid
                                    fu_t_mean
                                    fu_t_internationalartno_arn
                                    fu_t_internationalartno
                           CHANGING fc_t_fan_exist
                                    fc_t_reco_ean
                                    fc_v_count_9005.

* Build Reconciliation PIR Data
  PERFORM build_reco_pir_data USING fu_t_fan
                                    fu_t_idno_data
                                    fu_t_prod_data
                                    fu_t_reg_data_def
                                    fu_t_fanid
                                    fu_t_inforecord_general_arn
                                    fu_t_inforecord_purchorg_arn
                                    fu_t_inforecord_general
                                    fu_t_inforecord_purchorg
                           CHANGING fc_t_fan_exist
                                    fc_t_reco_pir
                                    fc_v_count_9006.

* Build Reconciliation BANNER Data
  PERFORM build_reco_banner_data USING fu_t_fan
                                       fu_t_idno_data
                                       fu_t_prod_data
                                       fu_t_reg_data_def
                                       fu_t_fanid
                                       fu_t_tvarvc_uni_lni
                                       fu_t_salesdata_arn
                                       fu_t_salesdata
                                       fu_t_posdata_arn
                                       fu_t_posdata
                                       fu_t_plantdata_arn
                                       fu_t_plantdata
                                       fu_t_bapi_salesext_arn
                                       fu_t_bapi_salesext
                                       fu_t_bapi_plantext_arn
                                       fu_t_bapi_plantext
                              CHANGING fc_t_fan_exist
                                       fc_t_reco_banner
                                       fc_v_count_9007.


ENDFORM.                    " BUILD_DISPLAY_DATA
*&---------------------------------------------------------------------*
*&      Form  BUILD_RECO_BASIC1_DATA
*&---------------------------------------------------------------------*
* Build Reconciliation BASIC 1 Data
*----------------------------------------------------------------------*
FORM build_reco_basic1_data USING fu_t_fan                TYPE ty_t_fan
                                  fu_t_idno_data          TYPE ty_t_idno_data
                                  fu_t_prod_data          TYPE ty_t_prod_data
                                  fu_t_reg_data_def       TYPE ty_t_reg_data
                                  fu_t_fanid              TYPE ty_t_fanid
                                  fu_t_headdata_arn       TYPE mat_bapie1mathead_tty
                                  fu_t_client_arn         TYPE bapie1marart_tab
                                  fu_t_addnl_arn          TYPE bapie1maw1rt_tab
                                  fu_t_matdes_arn         TYPE bapie1maktrt_tab
                                  fu_t_headdata           TYPE mat_bapie1mathead_tty
                                  fu_t_client             TYPE bapie1marart_tab
                                  fu_t_addnl              TYPE bapie1maw1rt_tab
                                  fu_t_matdes             TYPE bapie1maktrt_tab
                         CHANGING fc_t_fan_exist          TYPE ty_t_fan_exist
                                  fc_t_reco_basic1        TYPE ty_t_reco_basic1
                                  fc_v_count              TYPE i.

  DATA: lt_fan           TYPE ty_t_fan,
        ls_fan           TYPE ty_s_fan,
        lt_idno_data     TYPE ty_t_idno_data,
        ls_idno_data     TYPE ty_s_idno_data,
        lt_prod_data     TYPE ty_t_prod_data,
        ls_prod_data     TYPE ty_s_prod_data,
        lt_reg_data_def  TYPE ty_t_reg_data,
        ls_reg_data_def  TYPE ty_s_reg_data,
        lt_fanid         TYPE ty_t_fanid,
        ls_fanid         TYPE ty_s_fanid,

        lt_headdata_arn  TYPE mat_bapie1mathead_tty,
        lt_client_arn    TYPE bapie1marart_tab,
        lt_addnl_arn     TYPE bapie1maw1rt_tab,
        lt_matdes_arn    TYPE bapie1maktrt_tab,
        lt_headdata      TYPE mat_bapie1mathead_tty,
        lt_client        TYPE bapie1marart_tab,
        lt_addnl         TYPE bapie1maw1rt_tab,
        lt_matdes        TYPE bapie1maktrt_tab,

        ls_headdata_arn  TYPE bapie1mathead,
        ls_client_arn    TYPE bapie1marart,
        ls_addnl_arn     TYPE bapie1maw1rt,
        ls_matdes_arn    TYPE bapie1maktrt,
        ls_headdata      TYPE bapie1mathead,
        ls_client        TYPE bapie1marart,
        ls_addnl         TYPE bapie1maw1rt,
        ls_matdes        TYPE bapie1maktrt,

        lt_fan_exist     TYPE ty_t_fan_exist,
        lt_reco_basic1   TYPE ty_t_reco_basic1,
        ls_reco_basic1   TYPE ty_s_reco_basic1,
        lt_reg_hdr       TYPE ztarn_reg_hdr,
        ls_reg_hdr       TYPE zarn_reg_hdr,

        lv_matnr         TYPE matnr,
        lv_no_basic1     TYPE flag,
        lv_count         TYPE i,
        lv_basic1_status TYPE zarn_reco_status,
        lv_ovr_status    TYPE zarn_ovr_status.

  FIELD-SYMBOLS: <ls_fan_exist>  TYPE ty_s_fan_exist.



  lt_fan[]          = fu_t_fan[].
  lt_idno_data[]    = fu_t_idno_data[].
  lt_prod_data[]    = fu_t_prod_data[].
  lt_reg_data_def[] = fu_t_reg_data_def[].
  lt_fanid[]        = fu_t_fanid[].

  lt_headdata_arn[] = fu_t_headdata_arn[].
  lt_client_arn[]   = fu_t_client_arn[].
  lt_addnl_arn[]    = fu_t_addnl_arn[].
  lt_matdes_arn[]   = fu_t_matdes_arn[].

  lt_headdata[]     = fu_t_headdata[].
  lt_client[]       = fu_t_client[].
  lt_addnl[]        = fu_t_addnl[].
  lt_matdes[]       = fu_t_matdes[].

  lt_fan_exist[]    = fc_t_fan_exist[].



  LOOP AT lt_fan[] INTO ls_fan.

    CLEAR lv_count.

    CLEAR: ls_reco_basic1, lv_basic1_status.
    ls_reco_basic1-fan         = ls_fan-fan_id.

    lv_no_basic1 = abap_true.


* Get Article from FAN ID list, if not found then assign fan id as article
    CLEAR ls_fanid.
    READ TABLE lt_fanid[] INTO ls_fanid
    WITH TABLE KEY zzfan = ls_fan-fan_id.
    IF sy-subrc = 0.
      lv_matnr = ls_fanid-matnr.
    ELSE.
      lv_matnr = ls_fan-fan_id.

      CALL FUNCTION 'CONVERSION_EXIT_MATN1_INPUT'
        EXPORTING
          input        = lv_matnr
        IMPORTING
          output       = lv_matnr
        EXCEPTIONS
          length_error = 1
          OTHERS       = 2.

    ENDIF.  " READ TABLE lt_fanid[] INTO ls_fanid


* Check if IDNO exist
    CLEAR ls_idno_data.
    READ TABLE lt_idno_data[] INTO ls_idno_data
    WITH KEY fan_id = ls_fan-fan_id.
    IF sy-subrc = 0.

      ls_reco_basic1-idno        = ls_idno_data-idno.
      ls_reco_basic1-current_ver = ls_idno_data-current_ver.

* Get Regional ean data
      CLEAR ls_reg_data_def.
      READ TABLE lt_reg_data_def[] INTO ls_reg_data_def
      WITH KEY idno = ls_idno_data-idno.
      IF sy-subrc = 0.


* Data to be posted - CLIENTDATA
        CLEAR ls_client_arn.
        READ TABLE lt_client_arn[] INTO ls_client_arn
        WITH KEY material = lv_matnr.
        IF sy-subrc = 0.
          ls_reco_basic1-material        = ls_client_arn-material.
          ls_reco_basic1-cont_unit       = ls_client_arn-cont_unit.
          ls_reco_basic1-net_cont        = ls_client_arn-net_cont.
          ls_reco_basic1-base_uom        = ls_client_arn-base_uom.
          ls_reco_basic1-po_unit         = ls_client_arn-po_unit.
          ls_reco_basic1-item_cat        = ls_client_arn-item_cat.
          ls_reco_basic1-net_weight      = ls_client_arn-net_weight.
          ls_reco_basic1-temp_conds      = ls_client_arn-temp_conds.
          ls_reco_basic1-season          = ls_client_arn-season.
          ls_reco_basic1-prod_hier       = ls_client_arn-prod_hier.
          ls_reco_basic1-minremlife      = ls_client_arn-minremlife.
          ls_reco_basic1-saeson_yr       = ls_client_arn-saeson_yr.
          ls_reco_basic1-pur_status      = ls_client_arn-pur_status.
          ls_reco_basic1-sal_status      = ls_client_arn-sal_status.
          ls_reco_basic1-pvalidfrom      = ls_client_arn-pvalidfrom.
          ls_reco_basic1-svalidfrom      = ls_client_arn-svalidfrom.
          ls_reco_basic1-creation_status = ls_client_arn-creation_status.
          ls_reco_basic1-brand_id        = ls_client_arn-brand_id.
        ENDIF.

* Existing Data - CLIENTDATA
        CLEAR ls_client.
        READ TABLE lt_client[] INTO ls_client
        WITH KEY material = lv_matnr.
        IF sy-subrc = 0.
          ls_reco_basic1-sap_material        = ls_client-material.
          ls_reco_basic1-sap_cont_unit       = ls_client-cont_unit.
          ls_reco_basic1-sap_net_cont        = ls_client-net_cont.
          ls_reco_basic1-sap_base_uom        = ls_client-base_uom.
          ls_reco_basic1-sap_po_unit         = ls_client-po_unit.
          ls_reco_basic1-sap_item_cat        = ls_client-item_cat.
          ls_reco_basic1-sap_net_weight      = ls_client-net_weight.
          ls_reco_basic1-sap_temp_conds      = ls_client-temp_conds.
          ls_reco_basic1-sap_season          = ls_client-season.
          ls_reco_basic1-sap_prod_hier       = ls_client-prod_hier.
          ls_reco_basic1-sap_minremlife      = ls_client-minremlife.
          ls_reco_basic1-sap_saeson_yr       = ls_client-saeson_yr.
          ls_reco_basic1-sap_pur_status      = ls_client-pur_status.
          ls_reco_basic1-sap_sal_status      = ls_client-sal_status.
          ls_reco_basic1-sap_pvalidfrom      = ls_client-pvalidfrom.
          ls_reco_basic1-sap_svalidfrom      = ls_client-svalidfrom.
          ls_reco_basic1-sap_creation_status = ls_client-creation_status.
          ls_reco_basic1-sap_brand_id        = ls_client-brand_id.
        ENDIF.


* Data to be posted - HEADDATA
        CLEAR ls_headdata_arn.
        READ TABLE lt_headdata_arn[] INTO ls_headdata_arn
        WITH KEY material = lv_matnr.
        IF sy-subrc = 0.
          ls_reco_basic1-matl_group = ls_headdata_arn-matl_group.
          ls_reco_basic1-matl_type  = ls_headdata_arn-matl_type.
          ls_reco_basic1-matl_cat   = ls_headdata_arn-matl_cat.
        ENDIF.

* Existing Data - HEADDATA
        CLEAR ls_headdata.
        READ TABLE lt_headdata[] INTO ls_headdata
        WITH KEY material = lv_matnr.
        IF sy-subrc = 0.
          ls_reco_basic1-sap_matl_group = ls_headdata-matl_group.
          ls_reco_basic1-sap_matl_type  = ls_headdata-matl_type.
          ls_reco_basic1-sap_matl_cat   = ls_headdata-matl_cat.
        ENDIF.


* Data to be posted - ADDITIONALCLIENTDATA
        CLEAR ls_addnl_arn.
        READ TABLE lt_addnl_arn[] INTO ls_addnl_arn
        WITH KEY material = lv_matnr.
        IF sy-subrc = 0.
          ls_reco_basic1-countryori       = ls_addnl_arn-countryori.
          ls_reco_basic1-sales_unit       = ls_addnl_arn-sales_unit.
          ls_reco_basic1-issue_unit       = ls_addnl_arn-issue_unit.
        ENDIF.

* Existing Data - ADDITIONALCLIENTDATA
        CLEAR ls_addnl.
        READ TABLE lt_addnl[] INTO ls_addnl
        WITH KEY material = lv_matnr.
        IF sy-subrc = 0.
          ls_reco_basic1-sap_countryori       = ls_addnl-countryori.
          ls_reco_basic1-sap_sales_unit       = ls_addnl-sales_unit.
          ls_reco_basic1-sap_issue_unit       = ls_addnl-issue_unit.
        ENDIF.


* Data to be posted - MATERIALDESCRIPTION
        CLEAR ls_matdes_arn.
        READ TABLE lt_matdes_arn[] INTO ls_matdes_arn
        WITH KEY material = lv_matnr.
        IF sy-subrc = 0.
          ls_reco_basic1-matl_desc       = ls_matdes_arn-matl_desc.
        ENDIF.

* Existing Data - MATERIALDESCRIPTION
        CLEAR ls_matdes.
        READ TABLE lt_matdes[] INTO ls_matdes
        WITH KEY material = lv_matnr.
        IF sy-subrc = 0.
          ls_reco_basic1-sap_matl_desc       = ls_matdes-matl_desc.
        ENDIF.


        IF ls_reco_basic1-sap_countryori IS NOT INITIAL AND ls_reco_basic1-countryori IS INITIAL.
          ls_reco_basic1-countryori = ls_reco_basic1-sap_countryori.
        ENDIF.

        CLEAR lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic1-material
                                       ls_reco_basic1-sap_material
                                       ls_client_arn
                                       ls_client
                              CHANGING ls_reco_basic1-status1
                                       lv_basic1_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic1-matl_desc
                                       ls_reco_basic1-sap_matl_desc
                                       ls_matdes_arn
                                       ls_matdes
                              CHANGING ls_reco_basic1-status2
                                       lv_basic1_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic1-cont_unit
                                       ls_reco_basic1-sap_cont_unit
                                       ls_client_arn
                                       ls_client
                              CHANGING ls_reco_basic1-status3
                                       lv_basic1_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic1-net_cont
                                       ls_reco_basic1-sap_net_cont
                                       ls_client_arn
                                       ls_client
                              CHANGING ls_reco_basic1-status4
                                       lv_basic1_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic1-base_uom
                                       ls_reco_basic1-sap_base_uom
                                       ls_client_arn
                                       ls_client
                              CHANGING ls_reco_basic1-status5
                                       lv_basic1_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic1-po_unit
                                       ls_reco_basic1-sap_po_unit
                                       ls_client_arn
                                       ls_client
                              CHANGING ls_reco_basic1-status6
                                       lv_basic1_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic1-item_cat
                                       ls_reco_basic1-sap_item_cat
                                       ls_client_arn
                                       ls_client
                              CHANGING ls_reco_basic1-status7
                                       lv_basic1_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic1-net_weight
                                       ls_reco_basic1-sap_net_weight
                                       ls_client_arn
                                       ls_client
                              CHANGING ls_reco_basic1-status8
                                       lv_basic1_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic1-temp_conds
                                       ls_reco_basic1-sap_temp_conds
                                       ls_client_arn
                                       ls_client
                              CHANGING ls_reco_basic1-status9
                                       lv_basic1_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic1-season
                                       ls_reco_basic1-sap_season
                                       ls_client_arn
                                       ls_client
                              CHANGING ls_reco_basic1-status10
                                       lv_basic1_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic1-prod_hier
                                       ls_reco_basic1-sap_prod_hier
                                       ls_client_arn
                                       ls_client
                              CHANGING ls_reco_basic1-status11
                                       lv_basic1_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic1-minremlife
                                       ls_reco_basic1-sap_minremlife
                                       ls_client_arn
                                       ls_client
                              CHANGING ls_reco_basic1-status12
                                       lv_basic1_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic1-saeson_yr
                                       ls_reco_basic1-sap_saeson_yr
                                       ls_client_arn
                                       ls_client
                              CHANGING ls_reco_basic1-status13
                                       lv_basic1_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic1-pur_status
                                       ls_reco_basic1-sap_pur_status
                                       ls_client_arn
                                       ls_client
                              CHANGING ls_reco_basic1-status14
                                       lv_basic1_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic1-sal_status
                                       ls_reco_basic1-sap_sal_status
                                       ls_client_arn
                                       ls_client
                              CHANGING ls_reco_basic1-status15
                                       lv_basic1_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic1-pvalidfrom
                                       ls_reco_basic1-sap_pvalidfrom
                                       ls_client_arn
                                       ls_client
                              CHANGING ls_reco_basic1-status16
                                       lv_basic1_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic1-svalidfrom
                                       ls_reco_basic1-sap_svalidfrom
                                       ls_client_arn
                                       ls_client
                              CHANGING ls_reco_basic1-status17
                                       lv_basic1_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic1-creation_status
                                       ls_reco_basic1-sap_creation_status
                                       ls_client_arn
                                       ls_client
                              CHANGING ls_reco_basic1-status18
                                       lv_basic1_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic1-brand_id
                                       ls_reco_basic1-sap_brand_id
                                       ls_client_arn
                                       ls_client
                              CHANGING ls_reco_basic1-status19
                                       lv_basic1_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic1-countryori
                                       ls_reco_basic1-sap_countryori
                                       ls_addnl_arn
                                       ls_addnl
                              CHANGING ls_reco_basic1-status20
                                       lv_basic1_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic1-sales_unit
                                       ls_reco_basic1-sap_sales_unit
                                       ls_addnl_arn
                                       ls_addnl
                              CHANGING ls_reco_basic1-status21
                                       lv_basic1_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic1-issue_unit
                                       ls_reco_basic1-sap_issue_unit
                                       ls_addnl_arn
                                       ls_addnl
                              CHANGING ls_reco_basic1-status22
                                       lv_basic1_status
                                       lv_ovr_status.


* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic1-matl_group
                                       ls_reco_basic1-sap_matl_group
                                       ls_headdata_arn
                                       ls_headdata
                              CHANGING ls_reco_basic1-status23
                                       lv_basic1_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic1-matl_type
                                       ls_reco_basic1-sap_matl_type
                                       ls_headdata_arn
                                       ls_headdata
                              CHANGING ls_reco_basic1-status24
                                       lv_basic1_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic1-matl_cat
                                       ls_reco_basic1-sap_matl_cat
                                       ls_headdata_arn
                                       ls_headdata
                              CHANGING ls_reco_basic1-status25
                                       lv_basic1_status
                                       lv_ovr_status.


        ls_reco_basic1-ovr_status = lv_ovr_status.

        IF lv_ovr_status IS NOT INITIAL.
          lv_count = lv_count + 1.
        ENDIF.

        APPEND ls_reco_basic1 TO lt_reco_basic1[].
        lv_no_basic1 = abap_false.



      ENDIF.  " READ TABLE lt_reg_data_def[] INTO ls_reg_data_def
    ENDIF.  " READ TABLE lt_idno_data[] INTO ls_idno_data



    IF lv_no_basic1 = abap_true.
      ls_reco_basic1-ovr_status = abap_true.
      lv_count = lv_count + 1.
      lv_basic1_status = abap_true.
      APPEND ls_reco_basic1 TO lt_reco_basic1[].
    ENDIF.


* Update ean status in FAN Exist data
    IF <ls_fan_exist> IS ASSIGNED. UNASSIGN <ls_fan_exist>. ENDIF.
    READ TABLE lt_fan_exist ASSIGNING <ls_fan_exist>
    WITH KEY fan = ls_fan-fan_id.
    IF sy-subrc = 0.

      IF lv_basic1_status = abap_true.
        <ls_fan_exist>-ovr_status = abap_true.
        fc_v_count = fc_v_count + 1.
      ENDIF.


      <ls_fan_exist>-status_basic1 =  lv_count.  " lv_basic1_status.
    ENDIF.

  ENDLOOP.  " LOOP AT lt_fan[] INTO ls_fan


  fc_t_reco_basic1[]  = lt_reco_basic1[].
  fc_t_fan_exist[] = lt_fan_exist[].







ENDFORM.                    " BUILD_RECO_BASIC1_DATA
*&---------------------------------------------------------------------*
*&      Form  BUILD_RECO_BASIC2_DATA
*&---------------------------------------------------------------------*
* Build Reconciliation BASIC 2 Data
*----------------------------------------------------------------------*
FORM build_reco_basic2_data USING fu_t_fan                TYPE ty_t_fan
                                  fu_t_idno_data          TYPE ty_t_idno_data
                                  fu_t_prod_data          TYPE ty_t_prod_data
                                  fu_t_reg_data_def       TYPE ty_t_reg_data
                                  fu_t_fanid              TYPE ty_t_fanid
                                  fu_t_clientext_arn      TYPE zmd_t_bapi_clientext
                                  fu_t_clientext          TYPE zmd_t_bapi_clientext
                         CHANGING fc_t_fan_exist          TYPE ty_t_fan_exist
                                  fc_t_reco_basic2        TYPE ty_t_reco_basic2
                                  fc_v_count              TYPE i.


  DATA: lt_fan           TYPE ty_t_fan,
        ls_fan           TYPE ty_s_fan,
        lt_idno_data     TYPE ty_t_idno_data,
        ls_idno_data     TYPE ty_s_idno_data,
        lt_prod_data     TYPE ty_t_prod_data,
        ls_prod_data     TYPE ty_s_prod_data,
        lt_reg_data_def  TYPE ty_t_reg_data,
        ls_reg_data_def  TYPE ty_s_reg_data,
        lt_fanid         TYPE ty_t_fanid,
        ls_fanid         TYPE ty_s_fanid,

        lt_clientext_arn TYPE zmd_t_bapi_clientext,
        lt_clientext     TYPE zmd_t_bapi_clientext,

        ls_clientext_arn TYPE zmd_s_bapi_clientext,
        ls_clientext     TYPE zmd_s_bapi_clientext,

        lt_fan_exist     TYPE ty_t_fan_exist,
        lt_reco_basic2   TYPE ty_t_reco_basic2,
        ls_reco_basic2   TYPE ty_s_reco_basic2,
        lt_reg_hdr       TYPE ztarn_reg_hdr,
        ls_reg_hdr       TYPE zarn_reg_hdr,

        lv_matnr         TYPE matnr,
        lv_no_basic2     TYPE flag,
        lv_count         TYPE i,
        lv_basic2_status TYPE zarn_reco_status,
        lv_ovr_status    TYPE zarn_ovr_status.

  FIELD-SYMBOLS: <ls_fan_exist>  TYPE ty_s_fan_exist.



  lt_fan[]           = fu_t_fan[].
  lt_idno_data[]     = fu_t_idno_data[].
  lt_prod_data[]     = fu_t_prod_data[].
  lt_reg_data_def[]  = fu_t_reg_data_def[].
  lt_fanid[]         = fu_t_fanid[].
  lt_clientext_arn[] = fu_t_clientext_arn[].
  lt_clientext[]     = fu_t_clientext[].
  lt_fan_exist[]     = fc_t_fan_exist[].




  LOOP AT lt_fan[] INTO ls_fan.

    CLEAR lv_count.

    CLEAR: ls_reco_basic2, lv_basic2_status.
    ls_reco_basic2-fan         = ls_fan-fan_id.

    lv_no_basic2 = abap_true.


* Get Article from FAN ID list, if not found then assign fan id as article
    CLEAR ls_fanid.
    READ TABLE lt_fanid[] INTO ls_fanid
    WITH TABLE KEY zzfan = ls_fan-fan_id.
    IF sy-subrc = 0.
      lv_matnr = ls_fanid-matnr.
    ELSE.
      lv_matnr = ls_fan-fan_id.

      CALL FUNCTION 'CONVERSION_EXIT_MATN1_INPUT'
        EXPORTING
          input        = lv_matnr
        IMPORTING
          output       = lv_matnr
        EXCEPTIONS
          length_error = 1
          OTHERS       = 2.

    ENDIF.  " READ TABLE lt_fanid[] INTO ls_fanid


* Check if IDNO exist
    CLEAR ls_idno_data.
    READ TABLE lt_idno_data[] INTO ls_idno_data
    WITH KEY fan_id = ls_fan-fan_id.
    IF sy-subrc = 0.

      ls_reco_basic2-idno        = ls_idno_data-idno.
      ls_reco_basic2-current_ver = ls_idno_data-current_ver.

* Get Regional ean data
      CLEAR ls_reg_data_def.
      READ TABLE lt_reg_data_def[] INTO ls_reg_data_def
      WITH KEY idno = ls_idno_data-idno.
      IF sy-subrc = 0.

* Data to be posted - CLIENTDATA
        CLEAR ls_clientext_arn.
        READ TABLE lt_clientext_arn[] INTO ls_clientext_arn
        WITH KEY material = lv_matnr.
        IF sy-subrc = 0.
          ls_reco_basic2-material            = ls_clientext_arn-material.
          ls_reco_basic2-zzfan               = ls_clientext_arn-zzfan.
*          ls_reco_basic2-zzselling_only      = ls_clientext_arn-zzselling_only.        " --ONED-217 JKH 24.11.2016
          ls_reco_basic2-zzbuy_sell          = ls_clientext_arn-zzbuy_sell.            " ++ONED-217 JKH 24.11.2016
          ls_reco_basic2-zzsell              = ls_clientext_arn-zzsell.
          ls_reco_basic2-zzuse               = ls_clientext_arn-zzuse.
          ls_reco_basic2-zztktprnt           = ls_clientext_arn-zztktprnt.
          ls_reco_basic2-zzpedesc_flag       = ls_clientext_arn-zzpedesc_flag.

* DEL Begin of Change INC5348789 JKH 31.10.2016
*          ls_reco_basic2-zzunnum             = ls_clientext_arn-zzunnum.
*          ls_reco_basic2-zzpackgrp           = ls_clientext_arn-zzpackgrp.
*          ls_reco_basic2-zzhcls              = ls_clientext_arn-zzhcls.
*          ls_reco_basic2-zzhsno              = ls_clientext_arn-zzhsno.
*          ls_reco_basic2-zzhard              = ls_clientext_arn-zzhard.
* DEL End of Change INC5348789 JKH 31.10.2016

*          ls_reco_basic2-zzprep              = ls_clientext_arn-zzprep.
          ls_reco_basic2-zzstrd              = ls_clientext_arn-zzstrd.
          ls_reco_basic2-zzsco_weighed_flag  = ls_clientext_arn-zzsco_weighed_flag.
          ls_reco_basic2-zzstrg              = ls_clientext_arn-zzstrg.
          ls_reco_basic2-zzlabnum            = ls_clientext_arn-zzlabnum.
          ls_reco_basic2-zzpkddt             = ls_clientext_arn-zzpkddt.
          ls_reco_basic2-zzmandst            = ls_clientext_arn-zzmandst.
          ls_reco_basic2-zzwnmsg             = ls_clientext_arn-zzwnmsg.
          ls_reco_basic2-zzprdtype           = ls_clientext_arn-zzprdtype.
          ls_reco_basic2-zzas4subdept        = ls_clientext_arn-zzas4subdept .
          ls_reco_basic2-zzvar_wt_flag       = ls_clientext_arn-zzvar_wt_flag.
          ls_reco_basic2-zz_uni_dc           = ls_clientext_arn-zz_uni_dc.
          ls_reco_basic2-zz_lni_dc           = ls_clientext_arn-zz_lni_dc.
          ls_reco_basic2-zzlni_repack        = ls_clientext_arn-zzlni_repack.
          ls_reco_basic2-zzlni_bulk          = ls_clientext_arn-zzlni_bulk.
          ls_reco_basic2-zzlni_prboly        = ls_clientext_arn-zzlni_prboly.
        ENDIF.

* Existing Data - CLIENTDATA
        CLEAR ls_clientext.
        READ TABLE lt_clientext[] INTO ls_clientext
        WITH KEY material = lv_matnr.
        IF sy-subrc = 0.
          ls_reco_basic2-sap_material            = ls_clientext-material.
          ls_reco_basic2-sap_zzfan               = ls_clientext-zzfan.
*          ls_reco_basic2-sap_zzselling_only      = ls_clientext-zzselling_only.        " --ONED-217 JKH 24.11.2016
          ls_reco_basic2-sap_zzbuy_sell          = ls_clientext-zzbuy_sell.            " ++ONED-217 JKH 24.11.2016
          ls_reco_basic2-sap_zzsell              = ls_clientext-zzsell.
          ls_reco_basic2-sap_zzuse               = ls_clientext-zzuse.
          ls_reco_basic2-sap_zztktprnt           = ls_clientext-zztktprnt.
          ls_reco_basic2-sap_zzpedesc_flag       = ls_clientext-zzpedesc_flag.

* DEL Begin of Change INC5348789 JKH 31.10.2016
*          ls_reco_basic2-sap_zzunnum             = ls_clientext-zzunnum.
*          ls_reco_basic2-sap_zzpackgrp           = ls_clientext-zzpackgrp.
*          ls_reco_basic2-sap_zzhcls              = ls_clientext-zzhcls.
*          ls_reco_basic2-sap_zzhsno              = ls_clientext-zzhsno.
*          ls_reco_basic2-sap_zzhard              = ls_clientext-zzhard.
* DEL End of Change INC5348789 JKH 31.10.2016

*          ls_reco_basic2-sap_zzprep              = ls_clientext-zzprep.
          ls_reco_basic2-sap_zzstrd              = ls_clientext-zzstrd.
          ls_reco_basic2-sap_zzsco_weighed_flag  = ls_clientext-zzsco_weighed_flag.
          ls_reco_basic2-sap_zzstrg              = ls_clientext-zzstrg.
          ls_reco_basic2-sap_zzlabnum            = ls_clientext-zzlabnum.
          ls_reco_basic2-sap_zzpkddt             = ls_clientext-zzpkddt.
          ls_reco_basic2-sap_zzmandst            = ls_clientext-zzmandst.
          ls_reco_basic2-sap_zzwnmsg             = ls_clientext-zzwnmsg.
          ls_reco_basic2-sap_zzprdtype           = ls_clientext-zzprdtype.
          ls_reco_basic2-sap_zzas4subdept        = ls_clientext-zzas4subdept .
          ls_reco_basic2-sap_zzvar_wt_flag       = ls_clientext-zzvar_wt_flag.
          ls_reco_basic2-sap_zz_uni_dc           = ls_clientext-zz_uni_dc.
          ls_reco_basic2-sap_zz_lni_dc           = ls_clientext-zz_lni_dc.
          ls_reco_basic2-sap_zzlni_repack        = ls_clientext-zzlni_repack.
          ls_reco_basic2-sap_zzlni_bulk          = ls_clientext-zzlni_bulk.
          ls_reco_basic2-sap_zzlni_prboly        = ls_clientext-zzlni_prboly.
        ENDIF.



        IF ls_reco_basic2-sap_zzlni_repack IS NOT INITIAL AND ls_reco_basic2-zzlni_repack IS INITIAL.
          ls_reco_basic2-zzlni_repack = ls_reco_basic2-sap_zzlni_repack.
        ENDIF.

        IF ls_reco_basic2-sap_zzlni_bulk IS NOT INITIAL AND ls_reco_basic2-zzlni_bulk IS INITIAL.
          ls_reco_basic2-zzlni_bulk = ls_reco_basic2-sap_zzlni_bulk.
        ENDIF.

        IF ls_reco_basic2-sap_zzlni_prboly IS NOT INITIAL AND ls_reco_basic2-zzlni_prboly IS INITIAL.
          ls_reco_basic2-zzlni_prboly = ls_reco_basic2-sap_zzlni_prboly.
        ENDIF.


* DEL Begin of Change INC5348789 JKH 31.10.2016
*        IF ls_reco_basic2-sap_zzunnum IS NOT INITIAL AND ls_reco_basic2-zzunnum IS INITIAL.
*          ls_reco_basic2-zzunnum = ls_reco_basic2-sap_zzunnum.
*        ENDIF.
*
*        IF ls_reco_basic2-sap_zzpackgrp IS NOT INITIAL AND ls_reco_basic2-zzpackgrp IS INITIAL.
*          ls_reco_basic2-zzpackgrp = ls_reco_basic2-sap_zzpackgrp.
*        ENDIF.
*
*        IF ls_reco_basic2-sap_zzhcls IS NOT INITIAL AND ls_reco_basic2-zzhcls IS INITIAL.
*          ls_reco_basic2-zzhcls = ls_reco_basic2-sap_zzhcls.
*        ENDIF.
*
*        IF ls_reco_basic2-sap_zzhsno IS NOT INITIAL AND ls_reco_basic2-zzhsno IS INITIAL.
*          ls_reco_basic2-zzhsno = ls_reco_basic2-sap_zzhsno.
*        ENDIF.
*
*        IF ls_reco_basic2-sap_zzhard IS NOT INITIAL AND ls_reco_basic2-zzhard IS INITIAL.
*          ls_reco_basic2-zzhard = ls_reco_basic2-sap_zzhard.
*        ENDIF.
* DEL End of Change INC5348789 JKH 31.10.2016





        CLEAR lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic2-material
                                       ls_reco_basic2-sap_material
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_basic2-status1
                                       lv_basic2_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic2-zzfan
                                       ls_reco_basic2-sap_zzfan
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_basic2-status2
                                       lv_basic2_status
                                       lv_ovr_status.

** Get Reconciliation Status
*        PERFORM get_recon_status USING ls_reco_basic2-zzselling_only        " --ONED-217 JKH 24.11.2016
*                                       ls_reco_basic2-sap_zzselling_only
*                                       ls_clientext_arn
*                                       ls_clientext
*                              CHANGING ls_reco_basic2-status3
*                                       lv_basic2_status
*                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic2-zzbuy_sell           " ++ONED-217 JKH 24.11.2016
                                       ls_reco_basic2-sap_zzbuy_sell
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_basic2-status3
                                       lv_basic2_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic2-zzsell
                                       ls_reco_basic2-sap_zzsell
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_basic2-status4
                                       lv_basic2_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic2-zzuse
                                       ls_reco_basic2-sap_zzuse
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_basic2-status5
                                       lv_basic2_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic2-zztktprnt
                                       ls_reco_basic2-sap_zztktprnt
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_basic2-status6
                                       lv_basic2_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic2-zzpedesc_flag
                                       ls_reco_basic2-sap_zzpedesc_flag
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_basic2-status7
                                       lv_basic2_status
                                       lv_ovr_status.


* DEL Begin of Change INC5348789 JKH 31.10.2016
** Get Reconciliation Status
*        PERFORM get_recon_status USING ls_reco_basic2-zzunnum
*                                       ls_reco_basic2-sap_zzunnum
*                                       ls_clientext_arn
*                                       ls_clientext
*                              CHANGING ls_reco_basic2-status8
*                                       lv_basic2_status
*                                       lv_ovr_status.
*
** Get Reconciliation Status
*        PERFORM get_recon_status USING ls_reco_basic2-zzpackgrp
*                                       ls_reco_basic2-sap_zzpackgrp
*                                       ls_clientext_arn
*                                       ls_clientext
*                              CHANGING ls_reco_basic2-status9
*                                       lv_basic2_status
*                                       lv_ovr_status.
*
** Get Reconciliation Status
*        PERFORM get_recon_status USING ls_reco_basic2-zzhcls
*                                       ls_reco_basic2-sap_zzhcls
*                                       ls_clientext_arn
*                                       ls_clientext
*                              CHANGING ls_reco_basic2-status10
*                                       lv_basic2_status
*                                       lv_ovr_status.
*
** Get Reconciliation Status
*        PERFORM get_recon_status USING ls_reco_basic2-zzhsno
*                                       ls_reco_basic2-sap_zzhsno
*                                       ls_clientext_arn
*                                       ls_clientext
*                              CHANGING ls_reco_basic2-status11
*                                       lv_basic2_status
*                                       lv_ovr_status.
*
** Get Reconciliation Status
*        PERFORM get_recon_status USING ls_reco_basic2-zzhard
*                                       ls_reco_basic2-sap_zzhard
*                                       ls_clientext_arn
*                                       ls_clientext
*                              CHANGING ls_reco_basic2-status12
*                                       lv_basic2_status
*                                       lv_ovr_status.
* DEL End of Change INC5348789 JKH 31.10.2016




** Get Reconciliation Status
*        PERFORM get_recon_status USING ls_reco_basic2-zzprep
*                                       ls_reco_basic2-sap_zzprep
*                                       ls_clientext_arn
*                                       ls_clientext
*                              CHANGING ls_reco_basic2-status13
*                                       lv_basic2_status
*                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic2-zzstrd
                                       ls_reco_basic2-sap_zzstrd
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_basic2-status14
                                       lv_basic2_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic2-zzsco_weighed_flag
                                       ls_reco_basic2-sap_zzsco_weighed_flag
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_basic2-status15
                                       lv_basic2_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic2-zzstrg
                                       ls_reco_basic2-sap_zzstrg
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_basic2-status16
                                       lv_basic2_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic2-zzlabnum
                                       ls_reco_basic2-zzlabnum
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_basic2-status17
                                       lv_basic2_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic2-zzpkddt
                                       ls_reco_basic2-sap_zzpkddt
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_basic2-status18
                                       lv_basic2_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic2-zzmandst
                                       ls_reco_basic2-sap_zzmandst
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_basic2-status19
                                       lv_basic2_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic2-zzwnmsg
                                       ls_reco_basic2-sap_zzwnmsg
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_basic2-status20
                                       lv_basic2_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic2-zzprdtype
                                       ls_reco_basic2-sap_zzprdtype
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_basic2-status21
                                       lv_basic2_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic2-zzas4subdept
                                       ls_reco_basic2-sap_zzas4subdept
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_basic2-status22
                                       lv_basic2_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic2-zzvar_wt_flag
                                       ls_reco_basic2-sap_zzvar_wt_flag
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_basic2-status23
                                       lv_basic2_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic2-zz_uni_dc
                                       ls_reco_basic2-sap_zz_uni_dc
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_basic2-status24
                                       lv_basic2_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic2-zz_lni_dc
                                       ls_reco_basic2-sap_zz_lni_dc
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_basic2-status25
                                       lv_basic2_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic2-zzlni_repack
                                       ls_reco_basic2-sap_zzlni_repack
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_basic2-status26
                                       lv_basic2_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic2-zzlni_bulk
                                       ls_reco_basic2-sap_zzlni_bulk
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_basic2-status27
                                       lv_basic2_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_basic2-zzlni_prboly
                                       ls_reco_basic2-sap_zzlni_prboly
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_basic2-status28
                                       lv_basic2_status
                                       lv_ovr_status.


        ls_reco_basic2-ovr_status = lv_ovr_status.

        IF lv_ovr_status IS NOT INITIAL.
          lv_count = lv_count + 1.
        ENDIF.

        APPEND ls_reco_basic2 TO lt_reco_basic2[].
        lv_no_basic2 = abap_false.



      ENDIF.  " READ TABLE lt_reg_data_def[] INTO ls_reg_data_def
    ENDIF.  " READ TABLE lt_idno_data[] INTO ls_idno_data



    IF lv_no_basic2 = abap_true.
      ls_reco_basic2-ovr_status = abap_true.
      lv_count = lv_count + 1.
      lv_basic2_status = abap_true.
      APPEND ls_reco_basic2 TO lt_reco_basic2[].
    ENDIF.


* Update ean status in FAN Exist data
    IF <ls_fan_exist> IS ASSIGNED. UNASSIGN <ls_fan_exist>. ENDIF.
    READ TABLE lt_fan_exist ASSIGNING <ls_fan_exist>
    WITH KEY fan = ls_fan-fan_id.
    IF sy-subrc = 0.

      IF lv_basic2_status = abap_true.
        <ls_fan_exist>-ovr_status = abap_true.
        fc_v_count = fc_v_count + 1.
      ENDIF.

      <ls_fan_exist>-status_basic2 = lv_count.  " lv_basic2_status.
    ENDIF.

  ENDLOOP.  " LOOP AT lt_fan[] INTO ls_fan


  fc_t_reco_basic2[]  = lt_reco_basic2[].
  fc_t_fan_exist[] = lt_fan_exist[].





ENDFORM.                    " BUILD_RECO_BASIC2_DATA
*&---------------------------------------------------------------------*
*&      Form  BUILD_RECO_ATTR_DATA
*&---------------------------------------------------------------------*
* Build Reconciliation Attributes Data
*----------------------------------------------------------------------*
FORM build_reco_attr_data USING fu_t_fan                TYPE ty_t_fan
                                fu_t_idno_data          TYPE ty_t_idno_data
                                fu_t_prod_data          TYPE ty_t_prod_data
                                fu_t_reg_data_def       TYPE ty_t_reg_data
                                fu_t_fanid              TYPE ty_t_fanid
                                fu_t_clientext_arn      TYPE zmd_t_bapi_clientext
                                fu_t_clientext          TYPE zmd_t_bapi_clientext
                       CHANGING fc_t_fan_exist          TYPE ty_t_fan_exist
                                fc_t_reco_attr          TYPE ty_t_reco_attr
                                fc_v_count              TYPE i.



  DATA: lt_fan           TYPE ty_t_fan,
        ls_fan           TYPE ty_s_fan,
        lt_idno_data     TYPE ty_t_idno_data,
        ls_idno_data     TYPE ty_s_idno_data,
        lt_prod_data     TYPE ty_t_prod_data,
        ls_prod_data     TYPE ty_s_prod_data,
        lt_reg_data_def  TYPE ty_t_reg_data,
        ls_reg_data_def  TYPE ty_s_reg_data,
        lt_fanid         TYPE ty_t_fanid,
        ls_fanid         TYPE ty_s_fanid,

        lt_clientext_arn TYPE zmd_t_bapi_clientext,
        lt_clientext     TYPE zmd_t_bapi_clientext,

        ls_clientext_arn TYPE zmd_s_bapi_clientext,
        ls_clientext     TYPE zmd_s_bapi_clientext,

        lt_fan_exist     TYPE ty_t_fan_exist,
        lt_reco_attr     TYPE ty_t_reco_attr,
        ls_reco_attr     TYPE ty_s_reco_attr,
        lt_reg_hdr       TYPE ztarn_reg_hdr,
        ls_reg_hdr       TYPE zarn_reg_hdr,

        lv_matnr         TYPE matnr,
        lv_no_attr       TYPE flag,
        lv_count         TYPE i,
        lv_attr_status   TYPE zarn_reco_status,
        lv_ovr_status    TYPE zarn_ovr_status.

  FIELD-SYMBOLS: <ls_fan_exist>  TYPE ty_s_fan_exist.



  lt_fan[]           = fu_t_fan[].
  lt_idno_data[]     = fu_t_idno_data[].
  lt_prod_data[]     = fu_t_prod_data[].
  lt_reg_data_def[]  = fu_t_reg_data_def[].
  lt_fanid[]         = fu_t_fanid[].
  lt_clientext_arn[] = fu_t_clientext_arn[].
  lt_clientext[]     = fu_t_clientext[].
  lt_fan_exist[]     = fc_t_fan_exist[].




  LOOP AT lt_fan[] INTO ls_fan.

    CLEAR lv_count.

    CLEAR: ls_reco_attr, lv_attr_status.
    ls_reco_attr-fan         = ls_fan-fan_id.

    lv_no_attr = abap_true.


* Get Article from FAN ID list, if not found then assign fan id as article
    CLEAR ls_fanid.
    READ TABLE lt_fanid[] INTO ls_fanid
    WITH TABLE KEY zzfan = ls_fan-fan_id.
    IF sy-subrc = 0.
      lv_matnr = ls_fanid-matnr.
    ELSE.
      lv_matnr = ls_fan-fan_id.

      CALL FUNCTION 'CONVERSION_EXIT_MATN1_INPUT'
        EXPORTING
          input        = lv_matnr
        IMPORTING
          output       = lv_matnr
        EXCEPTIONS
          length_error = 1
          OTHERS       = 2.

    ENDIF.  " READ TABLE lt_fanid[] INTO ls_fanid


* Check if IDNO exist
    CLEAR ls_idno_data.
    READ TABLE lt_idno_data[] INTO ls_idno_data
    WITH KEY fan_id = ls_fan-fan_id.
    IF sy-subrc = 0.

      ls_reco_attr-idno        = ls_idno_data-idno.
      ls_reco_attr-current_ver = ls_idno_data-current_ver.

* Get Regional ean data
      CLEAR ls_reg_data_def.
      READ TABLE lt_reg_data_def[] INTO ls_reg_data_def
      WITH KEY idno = ls_idno_data-idno.
      IF sy-subrc = 0.

* Data to be posted - CLIENTDATA
        CLEAR ls_clientext_arn.
        READ TABLE lt_clientext_arn[] INTO ls_clientext_arn
        WITH KEY material = lv_matnr.
        IF sy-subrc = 0.
          ls_reco_attr-material = ls_clientext_arn-material.
          ls_reco_attr-zzattr1  = ls_clientext_arn-zzattr1.
          ls_reco_attr-zzattr2  = ls_clientext_arn-zzattr2.
          ls_reco_attr-zzattr3  = ls_clientext_arn-zzattr3.
          ls_reco_attr-zzattr4  = ls_clientext_arn-zzattr4.
          ls_reco_attr-zzattr5  = ls_clientext_arn-zzattr5.
          ls_reco_attr-zzattr6  = ls_clientext_arn-zzattr6.
          ls_reco_attr-zzattr7  = ls_clientext_arn-zzattr7.
          ls_reco_attr-zzattr8  = ls_clientext_arn-zzattr8.
          ls_reco_attr-zzattr9  = ls_clientext_arn-zzattr9.
          ls_reco_attr-zzattr10 = ls_clientext_arn-zzattr10.
          ls_reco_attr-zzattr11 = ls_clientext_arn-zzattr11.
          ls_reco_attr-zzattr12 = ls_clientext_arn-zzattr12.
          ls_reco_attr-zzattr13 = ls_clientext_arn-zzattr13.
          ls_reco_attr-zzattr14 = ls_clientext_arn-zzattr14.
          ls_reco_attr-zzattr15 = ls_clientext_arn-zzattr15.
          ls_reco_attr-zzattr16 = ls_clientext_arn-zzattr16.
          ls_reco_attr-zzattr17 = ls_clientext_arn-zzattr17.
          ls_reco_attr-zzattr18 = ls_clientext_arn-zzattr18.
          ls_reco_attr-zzattr19 = ls_clientext_arn-zzattr19.
          ls_reco_attr-zzattr20 = ls_clientext_arn-zzattr20.
          ls_reco_attr-zzattr21 = ls_clientext_arn-zzattr21.
          ls_reco_attr-zzattr22 = ls_clientext_arn-zzattr22.
          ls_reco_attr-zzattr23 = ls_clientext_arn-zzattr23.

        ENDIF.

* Existing Data - CLIENTDATA
        CLEAR ls_clientext.
        READ TABLE lt_clientext[] INTO ls_clientext
        WITH KEY material = lv_matnr.
        IF sy-subrc = 0.
          ls_reco_attr-sap_material = ls_clientext-material.
          ls_reco_attr-sap_zzattr1  = ls_clientext-zzattr1.
          ls_reco_attr-sap_zzattr2  = ls_clientext-zzattr2.
          ls_reco_attr-sap_zzattr3  = ls_clientext-zzattr3.
          ls_reco_attr-sap_zzattr4  = ls_clientext-zzattr4.
          ls_reco_attr-sap_zzattr5  = ls_clientext-zzattr5.
          ls_reco_attr-sap_zzattr6  = ls_clientext-zzattr6.
          ls_reco_attr-sap_zzattr7  = ls_clientext-zzattr7.
          ls_reco_attr-sap_zzattr8  = ls_clientext-zzattr8.
          ls_reco_attr-sap_zzattr9  = ls_clientext-zzattr9.
          ls_reco_attr-sap_zzattr10 = ls_clientext-zzattr10.
          ls_reco_attr-sap_zzattr11 = ls_clientext-zzattr11.
          ls_reco_attr-sap_zzattr12 = ls_clientext-zzattr12.
          ls_reco_attr-sap_zzattr13 = ls_clientext-zzattr13.
          ls_reco_attr-sap_zzattr14 = ls_clientext-zzattr14.
          ls_reco_attr-sap_zzattr15 = ls_clientext-zzattr15.
          ls_reco_attr-sap_zzattr16 = ls_clientext-zzattr16.
          ls_reco_attr-sap_zzattr17 = ls_clientext-zzattr17.
          ls_reco_attr-sap_zzattr18 = ls_clientext-zzattr18.
          ls_reco_attr-sap_zzattr19 = ls_clientext-zzattr19.
          ls_reco_attr-sap_zzattr20 = ls_clientext-zzattr20.
          ls_reco_attr-sap_zzattr21 = ls_clientext-zzattr21.
          ls_reco_attr-sap_zzattr22 = ls_clientext-zzattr22.
          ls_reco_attr-sap_zzattr23 = ls_clientext-zzattr23.
        ENDIF.




        CLEAR lv_ovr_status.


* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_attr-material
                                       ls_reco_attr-sap_material
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_attr-status1
                                       lv_attr_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_attr-zzattr1
                                       ls_reco_attr-sap_zzattr1
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_attr-status2
                                       lv_attr_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_attr-zzattr2
                                       ls_reco_attr-sap_zzattr2
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_attr-status3
                                       lv_attr_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_attr-zzattr3
                                       ls_reco_attr-sap_zzattr3
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_attr-status4
                                       lv_attr_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_attr-zzattr4
                                       ls_reco_attr-sap_zzattr4
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_attr-status5
                                       lv_attr_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_attr-zzattr5
                                       ls_reco_attr-sap_zzattr5
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_attr-status6
                                       lv_attr_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_attr-zzattr6
                                       ls_reco_attr-sap_zzattr6
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_attr-status7
                                       lv_attr_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_attr-zzattr7
                                       ls_reco_attr-sap_zzattr7
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_attr-status8
                                       lv_attr_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_attr-zzattr8
                                       ls_reco_attr-sap_zzattr8
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_attr-status9
                                       lv_attr_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_attr-zzattr9
                                       ls_reco_attr-sap_zzattr9
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_attr-status10
                                       lv_attr_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_attr-zzattr10
                                       ls_reco_attr-sap_zzattr10
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_attr-status11
                                       lv_attr_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_attr-zzattr11
                                       ls_reco_attr-sap_zzattr11
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_attr-status12
                                       lv_attr_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_attr-zzattr12
                                       ls_reco_attr-sap_zzattr12
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_attr-status13
                                       lv_attr_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_attr-zzattr13
                                       ls_reco_attr-sap_zzattr13
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_attr-status14
                                       lv_attr_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_attr-zzattr14
                                       ls_reco_attr-sap_zzattr14
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_attr-status15
                                       lv_attr_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_attr-zzattr15
                                       ls_reco_attr-sap_zzattr15
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_attr-status16
                                       lv_attr_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_attr-zzattr16
                                       ls_reco_attr-sap_zzattr16
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_attr-status17
                                       lv_attr_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_attr-zzattr17
                                       ls_reco_attr-sap_zzattr17
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_attr-status18
                                       lv_attr_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_attr-zzattr18
                                       ls_reco_attr-sap_zzattr18
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_attr-status19
                                       lv_attr_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_attr-zzattr19
                                       ls_reco_attr-sap_zzattr19
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_attr-status20
                                       lv_attr_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_attr-zzattr20
                                       ls_reco_attr-sap_zzattr20
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_attr-status21
                                       lv_attr_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_attr-zzattr21
                                       ls_reco_attr-sap_zzattr21
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_attr-status22
                                       lv_attr_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_attr-zzattr22
                                       ls_reco_attr-sap_zzattr22
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_attr-status23
                                       lv_attr_status
                                       lv_ovr_status.

* Get Reconciliation Status
        PERFORM get_recon_status USING ls_reco_attr-zzattr23
                                       ls_reco_attr-sap_zzattr23
                                       ls_clientext_arn
                                       ls_clientext
                              CHANGING ls_reco_attr-status24
                                       lv_attr_status
                                       lv_ovr_status.




        ls_reco_attr-ovr_status = lv_ovr_status.

        IF lv_ovr_status IS NOT INITIAL.
          lv_count = lv_count + 1.
        ENDIF.

        APPEND ls_reco_attr TO lt_reco_attr[].
        lv_no_attr = abap_false.



      ENDIF.  " READ TABLE lt_reg_data_def[] INTO ls_reg_data_def
    ENDIF.  " READ TABLE lt_idno_data[] INTO ls_idno_data



    IF lv_no_attr = abap_true.
      ls_reco_attr-ovr_status = abap_true.
      lv_count = lv_count + 1.
      lv_attr_status = abap_true.
      APPEND ls_reco_attr TO lt_reco_attr[].
    ENDIF.


* Update ean status in FAN Exist data
    IF <ls_fan_exist> IS ASSIGNED. UNASSIGN <ls_fan_exist>. ENDIF.
    READ TABLE lt_fan_exist ASSIGNING <ls_fan_exist>
    WITH KEY fan = ls_fan-fan_id.
    IF sy-subrc = 0.

      IF lv_attr_status = abap_true.
        <ls_fan_exist>-ovr_status = abap_true.
        fc_v_count = fc_v_count + 1.
      ENDIF.

      <ls_fan_exist>-status_attr = lv_count.  " lv_attr_status.
    ENDIF.

  ENDLOOP.  " LOOP AT lt_fan[] INTO ls_fan


  fc_t_reco_attr[]  = lt_reco_attr[].
  fc_t_fan_exist[] = lt_fan_exist[].



ENDFORM.                    " BUILD_RECO_ATTR_DATA
*&---------------------------------------------------------------------*
*&      Form  BUILD_RECO_UOM_DATA
*&---------------------------------------------------------------------*
* Build Reconciliation UOM Data
*----------------------------------------------------------------------*
FORM build_reco_uom_data USING fu_t_fan                TYPE ty_t_fan
                               fu_t_idno_data          TYPE ty_t_idno_data
                               fu_t_prod_data          TYPE ty_t_prod_data
                               fu_t_reg_data_def       TYPE ty_t_reg_data
                               fu_t_fanid              TYPE ty_t_fanid
                               fu_t_uom_arn            TYPE bapie1marmrt_tab
                               fu_t_uom                TYPE bapie1marmrt_tab
                      CHANGING fc_t_fan_exist          TYPE ty_t_fan_exist
                               fc_t_reco_uom           TYPE ty_t_reco_uom
                               fc_v_count              TYPE i.


  DATA: lt_fan          TYPE ty_t_fan,
        ls_fan          TYPE ty_s_fan,
        lt_idno_data    TYPE ty_t_idno_data,
        ls_idno_data    TYPE ty_s_idno_data,
        lt_prod_data    TYPE ty_t_prod_data,
        ls_prod_data    TYPE ty_s_prod_data,
        lt_reg_data_def TYPE ty_t_reg_data,
        ls_reg_data_def TYPE ty_s_reg_data,
        lt_fanid        TYPE ty_t_fanid,
        ls_fanid        TYPE ty_s_fanid,
        lt_uom_arn      TYPE bapie1marmrt_tab,
        ls_uom_arn      TYPE bapie1marmrt,
        lt_uom          TYPE bapie1marmrt_tab,
        ls_uom          TYPE bapie1marmrt,
        lt_fan_exist    TYPE ty_t_fan_exist,
        lt_reco_uom     TYPE ty_t_reco_uom,
        ls_reco_uom     TYPE ty_s_reco_uom,
        lt_reg_uom      TYPE ztarn_reg_uom,
        ls_reg_uom      TYPE zarn_reg_uom,

        lv_matnr        TYPE matnr,
        lv_no_uom       TYPE flag,
        lv_count        TYPE i,
        lv_uom_status   TYPE zarn_reco_status,
        lv_ovr_status   TYPE zarn_ovr_status.


  FIELD-SYMBOLS: <ls_fan_exist>  TYPE ty_s_fan_exist.



  lt_fan[]          = fu_t_fan[].
  lt_idno_data[]    = fu_t_idno_data[].
  lt_prod_data[]    = fu_t_prod_data[].
  lt_reg_data_def[] = fu_t_reg_data_def[].
  lt_fanid[]        = fu_t_fanid[].
  lt_uom_arn[]      = fu_t_uom_arn[].
  lt_uom[]          = fu_t_uom[].
  lt_fan_exist[]    = fc_t_fan_exist[].

  LOOP AT lt_fan[] INTO ls_fan.

    CLEAR lv_count.

    CLEAR: ls_reco_uom, lv_uom_status.
    ls_reco_uom-fan         = ls_fan-fan_id.

    lv_no_uom = abap_true.


* Get Article from FAN ID list, if not found then assign fan id as article
    CLEAR ls_fanid.
    READ TABLE lt_fanid[] INTO ls_fanid
    WITH TABLE KEY zzfan = ls_fan-fan_id.
    IF sy-subrc = 0.
      lv_matnr = ls_fanid-matnr.
    ELSE.
      lv_matnr = ls_fan-fan_id.

      CALL FUNCTION 'CONVERSION_EXIT_MATN1_INPUT'
        EXPORTING
          input        = lv_matnr
        IMPORTING
          output       = lv_matnr
        EXCEPTIONS
          length_error = 1
          OTHERS       = 2.

    ENDIF.  " READ TABLE lt_fanid[] INTO ls_fanid


* Check if IDNO exist
    CLEAR ls_idno_data.
    READ TABLE lt_idno_data[] INTO ls_idno_data
    WITH KEY fan_id = ls_fan-fan_id.
    IF sy-subrc = 0.

      ls_reco_uom-idno        = ls_idno_data-idno.
      ls_reco_uom-current_ver = ls_idno_data-current_ver.

* Get Regional UOM data
      CLEAR ls_reg_data_def.
      READ TABLE lt_reg_data_def[] INTO ls_reg_data_def
      WITH KEY idno = ls_idno_data-idno.
      IF sy-subrc = 0.

        lt_reg_uom[] = ls_reg_data_def-zarn_reg_uom[].

* For each UOM, get the data expected to be posted and data which is already existing
        LOOP AT lt_reg_uom[] INTO ls_reg_uom.

          CLEAR: ls_reco_uom.
          ls_reco_uom-fan         = ls_fan-fan_id.
          ls_reco_uom-idno        = ls_idno_data-idno.
          ls_reco_uom-current_ver = ls_idno_data-current_ver.

          ls_reco_uom-uom_category         = ls_reg_uom-uom_category.
          ls_reco_uom-pim_uom_code         = ls_reg_uom-pim_uom_code.
          ls_reco_uom-hybris_internal_code = ls_reg_uom-hybris_internal_code.
          ls_reco_uom-lower_uom            = ls_reg_uom-lower_uom.
          ls_reco_uom-lower_child_uom      = ls_reg_uom-lower_child_uom.

* Data to be posted - UNITOFMEASURE
          CLEAR ls_uom_arn.
          READ TABLE lt_uom_arn[] INTO ls_uom_arn
          WITH KEY material = lv_matnr
                   alt_unit = ls_reg_uom-meinh.
          IF sy-subrc = 0.
            ls_reco_uom-material   = ls_uom_arn-material.
            ls_reco_uom-alt_unit   = ls_uom_arn-alt_unit.
            ls_reco_uom-numerator  = ls_uom_arn-numerator.
            ls_reco_uom-denominatr = ls_uom_arn-denominatr.
            ls_reco_uom-ean_upc    = ls_uom_arn-ean_upc.
            ls_reco_uom-ean_cat    = ls_uom_arn-ean_cat..
            ls_reco_uom-length     = ls_uom_arn-length.
            ls_reco_uom-width      = ls_uom_arn-width.
            ls_reco_uom-height     = ls_uom_arn-height.
            ls_reco_uom-unit_dim   = ls_uom_arn-unit_dim.
            ls_reco_uom-gross_wt   = ls_uom_arn-gross_wt.
            ls_reco_uom-unit_of_wt = ls_uom_arn-unit_of_wt.
            ls_reco_uom-sub_uom    = ls_uom_arn-sub_uom.
          ENDIF.

* Existing data - UNITOFMEASURE
          CLEAR ls_uom.
          READ TABLE lt_uom[] INTO ls_uom
          WITH KEY material = lv_matnr
                   alt_unit = ls_reg_uom-meinh.
          IF sy-subrc = 0.
            ls_reco_uom-sap_material   = ls_uom-material.
            ls_reco_uom-sap_alt_unit   = ls_uom-alt_unit.
            ls_reco_uom-sap_numerator  = ls_uom-numerator.
            ls_reco_uom-sap_denominatr = ls_uom-denominatr.
            ls_reco_uom-sap_ean_upc    = ls_uom-ean_upc.
            ls_reco_uom-sap_ean_cat    = ls_uom-ean_cat..
            ls_reco_uom-sap_length     = ls_uom-length.
            ls_reco_uom-sap_width      = ls_uom-width.
            ls_reco_uom-sap_height     = ls_uom-height.
            ls_reco_uom-sap_unit_dim   = ls_uom-unit_dim.
            ls_reco_uom-sap_gross_wt   = ls_uom-gross_wt.
            ls_reco_uom-sap_unit_of_wt = ls_uom-unit_of_wt.
            ls_reco_uom-sap_sub_uom    = ls_uom-sub_uom.
          ENDIF.

          CLEAR lv_ovr_status.

* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_uom-material
                                         ls_reco_uom-sap_material
                                         ls_uom_arn
                                         ls_uom
                                CHANGING ls_reco_uom-status1
                                         lv_uom_status
                                         lv_ovr_status.

* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_uom-alt_unit
                                         ls_reco_uom-sap_alt_unit
                                         ls_uom_arn
                                         ls_uom
                                CHANGING ls_reco_uom-status2
                                         lv_uom_status
                                         lv_ovr_status.

* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_uom-numerator
                                         ls_reco_uom-sap_numerator
                                         ls_uom_arn
                                         ls_uom
                                CHANGING ls_reco_uom-status3
                                         lv_uom_status
                                         lv_ovr_status.

* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_uom-denominatr
                                         ls_reco_uom-sap_denominatr
                                         ls_uom_arn
                                         ls_uom
                                CHANGING ls_reco_uom-status4
                                         lv_uom_status
                                         lv_ovr_status.

* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_uom-ean_upc
                                         ls_reco_uom-sap_ean_upc
                                         ls_uom_arn
                                         ls_uom
                                CHANGING ls_reco_uom-status5
                                         lv_uom_status
                                         lv_ovr_status.

* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_uom-ean_cat
                                         ls_reco_uom-sap_ean_cat
                                         ls_uom_arn
                                         ls_uom
                                CHANGING ls_reco_uom-status6
                                         lv_uom_status
                                         lv_ovr_status.

* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_uom-length
                                         ls_reco_uom-sap_length
                                         ls_uom_arn
                                         ls_uom
                                CHANGING ls_reco_uom-status7
                                         lv_uom_status
                                         lv_ovr_status.

* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_uom-width
                                         ls_reco_uom-sap_width
                                         ls_uom_arn
                                         ls_uom
                                CHANGING ls_reco_uom-status8
                                         lv_uom_status
                                         lv_ovr_status.

* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_uom-height
                                         ls_reco_uom-sap_height
                                         ls_uom_arn
                                         ls_uom
                                CHANGING ls_reco_uom-status9
                                         lv_uom_status
                                         lv_ovr_status.

* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_uom-unit_dim
                                         ls_reco_uom-sap_unit_dim
                                         ls_uom_arn
                                         ls_uom
                                CHANGING ls_reco_uom-status10
                                         lv_uom_status
                                         lv_ovr_status.

* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_uom-gross_wt
                                         ls_reco_uom-sap_gross_wt
                                         ls_uom_arn
                                         ls_uom
                                CHANGING ls_reco_uom-status11
                                         lv_uom_status
                                         lv_ovr_status.

* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_uom-unit_of_wt
                                         ls_reco_uom-sap_unit_of_wt
                                         ls_uom_arn
                                         ls_uom
                                CHANGING ls_reco_uom-status12
                                         lv_uom_status
                                         lv_ovr_status.

* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_uom-sub_uom
                                         ls_reco_uom-sap_sub_uom
                                         ls_uom_arn
                                         ls_uom
                                CHANGING ls_reco_uom-status13
                                         lv_uom_status
                                         lv_ovr_status.



          ls_reco_uom-ovr_status = lv_ovr_status.

          IF lv_ovr_status IS NOT INITIAL.
            lv_count = lv_count + 1.
          ENDIF.

          APPEND ls_reco_uom TO lt_reco_uom[].
          lv_no_uom = abap_false.

        ENDLOOP.  " LOOP AT lt_reg_uom[] INTO ls_reg_uom


      ENDIF.  " READ TABLE lt_reg_data_def[] INTO ls_reg_data_def
    ENDIF.  " READ TABLE lt_idno_data[] INTO ls_idno_data



    IF lv_no_uom = abap_true.
      ls_reco_uom-ovr_status = abap_true.
      lv_count = lv_count + 1.
      lv_uom_status = abap_true.
      APPEND ls_reco_uom TO lt_reco_uom[].
    ENDIF.


* Update UOM status in FAN Exist data
    IF <ls_fan_exist> IS ASSIGNED. UNASSIGN <ls_fan_exist>. ENDIF.
    READ TABLE lt_fan_exist ASSIGNING <ls_fan_exist>
    WITH KEY fan = ls_fan-fan_id.
    IF sy-subrc = 0.

      IF lv_uom_status = abap_true.
        <ls_fan_exist>-ovr_status = abap_true.
      ENDIF.

      <ls_fan_exist>-status_uom = lv_count.  " lv_uom_status.
      fc_v_count = fc_v_count + 1.
    ENDIF.

  ENDLOOP.  " LOOP AT lt_fan[] INTO ls_fan


  fc_t_reco_uom[]  = lt_reco_uom[].
  fc_t_fan_exist[] = lt_fan_exist[].

ENDFORM.                    " BUILD_RECO_UOM_DATA
*&---------------------------------------------------------------------*
*&      Form  BUILD_RECO_EAN_DATA
*&---------------------------------------------------------------------*
* Build Reconciliation EAN Data
*----------------------------------------------------------------------*
FORM build_reco_ean_data USING fu_t_fan                TYPE ty_t_fan
                               fu_t_idno_data          TYPE ty_t_idno_data
                               fu_t_prod_data          TYPE ty_t_prod_data
                               fu_t_reg_data_def       TYPE ty_t_reg_data
                               fu_t_fanid              TYPE ty_t_fanid
                               fu_t_mean                    TYPE ty_t_mean
                               fu_t_ean_arn            TYPE bapie1meanrt_tab
                               fu_t_ean                TYPE bapie1meanrt_tab
                      CHANGING fc_t_fan_exist          TYPE ty_t_fan_exist
                               fc_t_reco_ean           TYPE ty_t_reco_ean
                               fc_v_count              TYPE i.

  DATA: lt_fan          TYPE ty_t_fan,
        ls_fan          TYPE ty_s_fan,
        lt_idno_data    TYPE ty_t_idno_data,
        ls_idno_data    TYPE ty_s_idno_data,
        lt_prod_data    TYPE ty_t_prod_data,
        ls_prod_data    TYPE ty_s_prod_data,
        lt_reg_data_def TYPE ty_t_reg_data,
        ls_reg_data_def TYPE ty_s_reg_data,
        lt_fanid        TYPE ty_t_fanid,
        ls_fanid        TYPE ty_s_fanid,
        lt_mean         TYPE ty_t_mean,
        ls_mean         TYPE mean,

        lt_ean_arn      TYPE bapie1meanrt_tab,
        ls_ean_arn      TYPE bapie1meanrt,
        lt_ean          TYPE bapie1meanrt_tab,
        ls_ean          TYPE bapie1meanrt,

        lt_fan_exist    TYPE ty_t_fan_exist,
        lt_reco_ean     TYPE ty_t_reco_ean,
        ls_reco_ean     TYPE ty_s_reco_ean,
        lt_reg_ean      TYPE ztarn_reg_ean,
        ls_reg_ean      TYPE zarn_reg_ean,

        lv_matnr        TYPE matnr,
        lv_no_ean       TYPE flag,
        lv_count        TYPE i,
        lv_ean_status   TYPE zarn_reco_status,
        lv_ovr_status   TYPE zarn_ovr_status.

  FIELD-SYMBOLS: <ls_fan_exist>  TYPE ty_s_fan_exist.


  lt_fan[]          = fu_t_fan[].
  lt_idno_data[]    = fu_t_idno_data[].
  lt_prod_data[]    = fu_t_prod_data[].
  lt_reg_data_def[] = fu_t_reg_data_def[].
  lt_fanid[]        = fu_t_fanid[].
  lt_mean[]         = fu_t_mean[].
  lt_ean_arn[]      = fu_t_ean_arn[].
  lt_ean[]          = fu_t_ean[].
  lt_fan_exist[]    = fc_t_fan_exist[].



  LOOP AT lt_fan[] INTO ls_fan.

    CLEAR lv_count.

    CLEAR: ls_reco_ean, lv_ean_status.
    ls_reco_ean-fan         = ls_fan-fan_id.

    lv_no_ean = abap_true.


* Get Article from FAN ID list, if not found then assign fan id as article
    CLEAR ls_fanid.
    READ TABLE lt_fanid[] INTO ls_fanid
    WITH TABLE KEY zzfan = ls_fan-fan_id.
    IF sy-subrc = 0.
      lv_matnr = ls_fanid-matnr.
    ELSE.
      lv_matnr = ls_fan-fan_id.

      CALL FUNCTION 'CONVERSION_EXIT_MATN1_INPUT'
        EXPORTING
          input        = lv_matnr
        IMPORTING
          output       = lv_matnr
        EXCEPTIONS
          length_error = 1
          OTHERS       = 2.

    ENDIF.  " READ TABLE lt_fanid[] INTO ls_fanid


* Check if IDNO exist
    CLEAR ls_idno_data.
    READ TABLE lt_idno_data[] INTO ls_idno_data
    WITH KEY fan_id = ls_fan-fan_id.
    IF sy-subrc = 0.

      ls_reco_ean-idno        = ls_idno_data-idno.
      ls_reco_ean-current_ver = ls_idno_data-current_ver.

* Get Regional ean data
      CLEAR ls_reg_data_def.
      READ TABLE lt_reg_data_def[] INTO ls_reg_data_def
      WITH KEY idno = ls_idno_data-idno.
      IF sy-subrc = 0.

        lt_reg_ean[] = ls_reg_data_def-zarn_reg_ean[].

* For each ean, get the data expected to be posted and data which is already existing
        LOOP AT lt_reg_ean[] INTO ls_reg_ean.

          CLEAR: ls_reco_ean.
          ls_reco_ean-fan         = ls_fan-fan_id.
          ls_reco_ean-idno        = ls_idno_data-idno.
          ls_reco_ean-current_ver = ls_idno_data-current_ver.

*          ls_reco_ean-meinh = ls_reg_ean-meinh.
*          ls_reco_ean-ean11 = ls_reg_ean-ean11.
*          ls_reco_ean-eantp = ls_reg_ean-eantp.

          ls_reco_ean-hpean = ls_reg_ean-hpean.

* Get Existing Main GTIN from
          CLEAR ls_mean.
          READ TABLE lt_mean INTO ls_mean
          WITH KEY matnr = lv_matnr
                   meinh = ls_reg_ean-meinh
                   ean11 = ls_reg_ean-ean11.
          IF sy-subrc = 0.
            ls_reco_ean-sap_hpean = ls_mean-hpean.
          ENDIF.

* Data to be posted - INTERNATIONALARTNOS
          CLEAR ls_ean_arn.
          READ TABLE lt_ean_arn[] INTO ls_ean_arn
          WITH KEY material = lv_matnr
                   unit     = ls_reg_ean-meinh
                   ean_upc  = ls_reg_ean-ean11.
          IF sy-subrc = 0.
            ls_reco_ean-material = ls_ean_arn-material.
            ls_reco_ean-unit     = ls_ean_arn-unit.
            ls_reco_ean-ean_upc  = ls_ean_arn-ean_upc.
            ls_reco_ean-ean_cat  = ls_ean_arn-ean_cat.
          ENDIF.

* Existing data - INTERNATIONALARTNOS
          CLEAR ls_ean.
          READ TABLE lt_ean[] INTO ls_ean
          WITH KEY material = lv_matnr
                   unit     = ls_reg_ean-meinh
                   ean_upc  = ls_reg_ean-ean11.
          IF sy-subrc = 0.
            ls_reco_ean-sap_material = ls_ean-material.
            ls_reco_ean-sap_unit     = ls_ean-unit.
            ls_reco_ean-sap_ean_upc  = ls_ean-ean_upc.
            ls_reco_ean-sap_ean_cat  = ls_ean-ean_cat.
          ENDIF.

          CLEAR lv_ovr_status.

* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_ean-material
                                         ls_reco_ean-sap_material
                                         ls_ean_arn
                                         ls_ean
                                CHANGING ls_reco_ean-status1
                                         lv_ean_status
                                         lv_ovr_status.

* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_ean-unit
                                         ls_reco_ean-sap_unit
                                         ls_ean_arn
                                         ls_ean
                                CHANGING ls_reco_ean-status2
                                         lv_ean_status
                                         lv_ovr_status.

* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_ean-ean_upc
                                         ls_reco_ean-sap_ean_upc
                                         ls_ean_arn
                                         ls_ean
                                CHANGING ls_reco_ean-status3
                                         lv_ean_status
                                         lv_ovr_status.

* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_ean-ean_cat
                                         ls_reco_ean-sap_ean_cat
                                         ls_ean_arn
                                         ls_ean
                                CHANGING ls_reco_ean-status4
                                         lv_ean_status
                                         lv_ovr_status.

* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_ean-hpean
                                         ls_reco_ean-sap_hpean
                                         ls_reg_ean
                                         ls_mean
                                CHANGING ls_reco_ean-status5
                                         lv_ean_status
                                         lv_ovr_status.


          ls_reco_ean-ovr_status = lv_ovr_status.

          IF lv_ovr_status IS NOT INITIAL.
            lv_count = lv_count + 1.
          ENDIF.

          APPEND ls_reco_ean TO lt_reco_ean[].
          lv_no_ean = abap_false.

        ENDLOOP.  " LOOP AT lt_reg_ean[] INTO ls_reg_ean


      ENDIF.  " READ TABLE lt_reg_data_def[] INTO ls_reg_data_def
    ENDIF.  " READ TABLE lt_idno_data[] INTO ls_idno_data



    IF lv_no_ean = abap_true.
      ls_reco_ean-ovr_status = abap_true.
      lv_count = lv_count + 1.
      lv_ean_status = abap_true.
      APPEND ls_reco_ean TO lt_reco_ean[].
    ENDIF.


* Update ean status in FAN Exist data
    IF <ls_fan_exist> IS ASSIGNED. UNASSIGN <ls_fan_exist>. ENDIF.
    READ TABLE lt_fan_exist ASSIGNING <ls_fan_exist>
    WITH KEY fan = ls_fan-fan_id.
    IF sy-subrc = 0.

      IF lv_ean_status = abap_true.
        <ls_fan_exist>-ovr_status = abap_true.
        fc_v_count = fc_v_count + 1.
      ENDIF.

      <ls_fan_exist>-status_ean = lv_count.
    ENDIF.

  ENDLOOP.  " LOOP AT lt_fan[] INTO ls_fan


  fc_t_reco_ean[]  = lt_reco_ean[].
  fc_t_fan_exist[] = lt_fan_exist[].




ENDFORM.                    " BUILD_RECO_EAN_DATA
*&---------------------------------------------------------------------*
*&      Form  BUILD_RECO_PIR_DATA
*&---------------------------------------------------------------------*
* Build Reconciliation PIR Data
*----------------------------------------------------------------------*
FORM build_reco_pir_data USING fu_t_fan                     TYPE ty_t_fan
                               fu_t_idno_data               TYPE ty_t_idno_data
                               fu_t_prod_data               TYPE ty_t_prod_data
                               fu_t_reg_data_def            TYPE ty_t_reg_data
                               fu_t_fanid                   TYPE ty_t_fanid
                               fu_t_inforecord_general_arn  TYPE wrf_bapieina_tty
                               fu_t_inforecord_purchorg_arn TYPE wrf_bapieine_tty
                               fu_t_inforecord_general      TYPE wrf_bapieina_tty
                               fu_t_inforecord_purchorg     TYPE wrf_bapieine_tty
                      CHANGING fc_t_fan_exist               TYPE ty_t_fan_exist
                               fc_t_reco_pir                TYPE ty_t_reco_pir
                               fc_v_count                   TYPE i.


  DATA: lt_fan             TYPE ty_t_fan,
        ls_fan             TYPE ty_s_fan,
        lt_idno_data       TYPE ty_t_idno_data,
        ls_idno_data       TYPE ty_s_idno_data,
        lt_prod_data       TYPE ty_t_prod_data,
        ls_prod_data       TYPE ty_s_prod_data,
        lt_reg_data_def    TYPE ty_t_reg_data,
        ls_reg_data_def    TYPE ty_s_reg_data,
        lt_fanid           TYPE ty_t_fanid,
        ls_fanid           TYPE ty_s_fanid,
        lt_fan_exist       TYPE ty_t_fan_exist,

        lt_reco_pir        TYPE ty_t_reco_pir,
        ls_reco_pir        TYPE ty_s_reco_pir,

        lt_reg_pir         TYPE ztarn_reg_pir,
        ls_reg_pir         TYPE zarn_reg_pir,
        ls_reg_pir_po      TYPE zarn_reg_pir,

        lt_eina_arn        TYPE wrf_bapieina_tty,
        ls_eina_arn        TYPE bapieina,
        lt_eine_arn        TYPE wrf_bapieine_tty,
        ls_eine_arn        TYPE wrfbapieine,
        lt_eina            TYPE wrf_bapieina_tty,
        ls_eina            TYPE bapieina,
        lt_eine            TYPE wrf_bapieine_tty,
        ls_eine            TYPE wrfbapieine,

        lv_matnr           TYPE matnr,
        lv_no_pir          TYPE flag,
        lv_pir_status      TYPE zarn_reco_status,
        lv_count           TYPE i,
        lv_ovr_status_eina TYPE zarn_ovr_status,
        lv_ovr_status_eine TYPE zarn_ovr_status.


  FIELD-SYMBOLS: <ls_fan_exist> TYPE ty_s_fan_exist,
                 <ls_reco_pir>  TYPE ty_s_reco_pir.



  lt_fan[]                = fu_t_fan[].
  lt_idno_data[]          = fu_t_idno_data[].
  lt_prod_data[]          = fu_t_prod_data[].
  lt_reg_data_def[]       = fu_t_reg_data_def[].
  lt_fanid[]              = fu_t_fanid[].
  lt_eina_arn[]           = fu_t_inforecord_general_arn[].
  lt_eina[]               = fu_t_inforecord_general[].
  lt_eine_arn[]           = fu_t_inforecord_purchorg_arn[].
  lt_eine[]               = fu_t_inforecord_purchorg[].
  lt_fan_exist[]          = fc_t_fan_exist[].




  LOOP AT lt_fan[] INTO ls_fan.

    CLEAR lv_count.

    CLEAR: ls_reco_pir, lv_pir_status.
    ls_reco_pir-fan         = ls_fan-fan_id.

    lv_no_pir = abap_true.


* Get Article from FAN ID list, if not found then assign fan id as article
    CLEAR ls_fanid.
    READ TABLE lt_fanid[] INTO ls_fanid
    WITH TABLE KEY zzfan = ls_fan-fan_id.
    IF sy-subrc = 0.
      lv_matnr = ls_fanid-matnr.
    ELSE.
      lv_matnr = ls_fan-fan_id.

      CALL FUNCTION 'CONVERSION_EXIT_MATN1_INPUT'
        EXPORTING
          input        = lv_matnr
        IMPORTING
          output       = lv_matnr
        EXCEPTIONS
          length_error = 1
          OTHERS       = 2.

    ENDIF.  " READ TABLE lt_fanid[] INTO ls_fanid


* Check if IDNO exist
    CLEAR ls_idno_data.
    READ TABLE lt_idno_data[] INTO ls_idno_data
    WITH KEY fan_id = ls_fan-fan_id.
    IF sy-subrc = 0.

      ls_reco_pir-idno        = ls_idno_data-idno.
      ls_reco_pir-current_ver = ls_idno_data-current_ver.


* Get Regional PIR data
      CLEAR ls_reg_data_def.
      READ TABLE lt_reg_data_def[] INTO ls_reg_data_def
      WITH KEY idno = ls_idno_data-idno.
      IF sy-subrc = 0.

        lt_reg_pir[] = ls_reg_data_def-zarn_reg_pir[].
        SORT lt_reg_pir[] BY idno lifnr   ASCENDING
                             pir_rel_eina DESCENDING.


* For each PIR, get the data expected to be posted and data which is already existing
        LOOP AT lt_reg_pir[] INTO ls_reg_pir.

          CLEAR: ls_reco_pir.
          ls_reco_pir-fan         = ls_fan-fan_id.
          ls_reco_pir-idno        = ls_idno_data-idno.
          ls_reco_pir-current_ver = ls_idno_data-current_ver.

          ls_reco_pir-order_uom_pim        = ls_reg_pir-order_uom_pim.
          ls_reco_pir-hybris_internal_code = ls_reg_pir-hybris_internal_code.
          ls_reco_pir-gln_no               = ls_reg_pir-gln_no.

          ls_reco_pir-lower_uom            = ls_reg_pir-lower_uom.
          ls_reco_pir-lower_child_uom      = ls_reg_pir-lower_child_uom.
          ls_reco_pir-pir_rel_eina         = ls_reg_pir-pir_rel_eina.
          ls_reco_pir-pir_rel_eine         = ls_reg_pir-pir_rel_eine.

* EINA from Defaulted PIR
          ls_reco_pir-material   = lv_matnr.
          ls_reco_pir-vendor     = ls_reg_pir-lifnr.
          ls_reco_pir-po_unit    = ls_reg_pir-bprme.
*          ls_reco_pir-umrez      = ls_reg_pir-
*          ls_reco_pir-umren      = ls_reg_pir-
          ls_reco_pir-vend_mat   = ls_reg_pir-idnlf.
*          ls_reco_pir-base_uom   = ls_reg_pir-
          ls_reco_pir-var_ord_un = ls_reg_pir-vabme.
          ls_reco_pir-suppl_from = ls_reg_pir-lifab.
          ls_reco_pir-suppl_to   = ls_reg_pir-lifbi.
          ls_reco_pir-norm_vend  = ls_reg_pir-relif.


* EINE from Defaulted PIR
*          ls_reco_pir-purch_org  = ls_reg_pir-
          ls_reco_pir-info_type  = ls_reg_pir-esokz.
*          ls_reco_pir-plant      = ls_reg_pir-
          ls_reco_pir-delete_ind = ls_reg_pir-loekz.
          ls_reco_pir-pur_group  = ls_reg_pir-ekgrp.
          ls_reco_pir-currency   = ls_reg_pir-waers.
          ls_reco_pir-min_po_qty = ls_reg_pir-minbm.
          ls_reco_pir-nrm_po_qty = ls_reg_pir-norbm.
          ls_reco_pir-plnd_delry = ls_reg_pir-aplfz.
          ls_reco_pir-overdeltol = ls_reg_pir-uebto.
          ls_reco_pir-under_tol  = ls_reg_pir-untto.
          ls_reco_pir-net_price  = ls_reg_pir-netpr.
          ls_reco_pir-price_unit = ls_reg_pir-peinh.
          ls_reco_pir-orderpr_un = ls_reg_pir-bprme.
          ls_reco_pir-price_date = ls_reg_pir-prdat.
*          ls_reco_pir-bpumz      = ls_reg_pir-
*          ls_reco_pir-bpumn      = ls_reg_pir-
          ls_reco_pir-eff_price  = ls_reg_pir-netpr.
          ls_reco_pir-cond_group = ls_reg_pir-ekkol.
          ls_reco_pir-tax_code   = ls_reg_pir-mwskz.


* Regional Original fields
          ls_reco_pir-ro_currency    = ls_reg_pir-waers.
          ls_reco_pir-ro_net_price   = ls_reg_pir-netpr.
          ls_reco_pir-ro_price_unit  = ls_reg_pir-peinh.
          ls_reco_pir-ro_orderpr_un  = ls_reg_pir-bprme.
          ls_reco_pir-ro_price_date  = ls_reg_pir-prdat.
          ls_reco_pir-ro_eff_price   = ls_reg_pir-netpr.


          CLEAR lv_ovr_status_eina.


* EINA FLAG
          IF ls_reg_pir-pir_rel_eina = abap_true.

* Data to be posted - INFORECORDGENERAL
            CLEAR ls_eina_arn.
            READ TABLE lt_eina_arn[] INTO ls_eina_arn
            WITH KEY material = lv_matnr
                     vendor   = ls_reg_pir-lifnr.
            IF sy-subrc = 0.
              ls_reco_pir-material   = ls_eina_arn-material.
              ls_reco_pir-vendor     = ls_eina_arn-vendor.
              ls_reco_pir-po_unit    = ls_eina_arn-po_unit.
              ls_reco_pir-calc_umrez = ls_eina_arn-conv_num1.
              ls_reco_pir-calc_umren = ls_eina_arn-conv_den1.
              ls_reco_pir-vend_mat   = ls_eina_arn-vend_mat.
              ls_reco_pir-base_uom   = ls_eina_arn-base_uom.
              ls_reco_pir-var_ord_un = ls_eina_arn-var_ord_un.
              ls_reco_pir-suppl_from = ls_eina_arn-suppl_from.
              ls_reco_pir-suppl_to   = ls_eina_arn-suppl_to.
              ls_reco_pir-norm_vend  = ls_eina_arn-norm_vend.
            ENDIF.

* Existing Data - INFORECORDGENERAL
            CLEAR ls_eina.
            READ TABLE lt_eina[] INTO ls_eina
            WITH KEY material = lv_matnr
                     vendor   = ls_reg_pir-lifnr.
            IF sy-subrc = 0.
              ls_reco_pir-sap_material   = ls_eina-material.
              ls_reco_pir-sap_vendor     = ls_eina-vendor.
              ls_reco_pir-sap_po_unit    = ls_eina-po_unit.
              ls_reco_pir-sap_umrez      = ls_eina-conv_num1.
              ls_reco_pir-sap_umren      = ls_eina-conv_den1.
              ls_reco_pir-sap_vend_mat   = ls_eina-vend_mat.
              ls_reco_pir-sap_base_uom   = ls_eina-base_uom.
              ls_reco_pir-sap_var_ord_un = ls_eina-var_ord_un.
              ls_reco_pir-sap_suppl_from = ls_eina-suppl_from.
              ls_reco_pir-sap_suppl_to   = ls_eina-suppl_to.
              ls_reco_pir-sap_norm_vend  = ls_eina-norm_vend.
            ENDIF.

* Get Reconciliation Status
            PERFORM get_recon_status USING ls_reco_pir-material
                                           ls_reco_pir-sap_material
                                           ls_eina_arn
                                           ls_eina
                                  CHANGING ls_reco_pir-status1
                                           lv_pir_status
                                           lv_ovr_status_eina.

* Get Reconciliation Status
            PERFORM get_recon_status USING ls_reco_pir-vendor
                                           ls_reco_pir-sap_vendor
                                           ls_eina_arn
                                           ls_eina
                                  CHANGING ls_reco_pir-status2
                                           lv_pir_status
                                           lv_ovr_status_eina.

* Get Reconciliation Status
            PERFORM get_recon_status USING ls_reco_pir-po_unit
                                           ls_reco_pir-sap_po_unit
                                           ls_eina_arn
                                           ls_eina
                                  CHANGING ls_reco_pir-status3
                                           lv_pir_status
                                           lv_ovr_status_eina.

* Get Reconciliation Status
            PERFORM get_recon_status USING ls_reco_pir-calc_umrez
                                           ls_reco_pir-sap_umrez
                                           ls_eina_arn
                                           ls_eina
                                  CHANGING ls_reco_pir-status4
                                           lv_pir_status
                                           lv_ovr_status_eina.

* Get Reconciliation Status
            PERFORM get_recon_status USING ls_reco_pir-calc_umren
                                           ls_reco_pir-sap_umren
                                           ls_eina_arn
                                           ls_eina
                                  CHANGING ls_reco_pir-status5
                                           lv_pir_status
                                           lv_ovr_status_eina.

* Get Reconciliation Status
            PERFORM get_recon_status USING ls_reco_pir-vend_mat
                                           ls_reco_pir-sap_vend_mat
                                           ls_eina_arn
                                           ls_eina
                                  CHANGING ls_reco_pir-status6
                                           lv_pir_status
                                           lv_ovr_status_eina.

* Get Reconciliation Status
            PERFORM get_recon_status USING ls_reco_pir-base_uom
                                           ls_reco_pir-sap_base_uom
                                           ls_eina_arn
                                           ls_eina
                                  CHANGING ls_reco_pir-status7
                                           lv_pir_status
                                           lv_ovr_status_eina.

* Get Reconciliation Status
            PERFORM get_recon_status USING ls_reco_pir-var_ord_un
                                           ls_reco_pir-sap_var_ord_un
                                           ls_eina_arn
                                           ls_eina
                                  CHANGING ls_reco_pir-status8
                                           lv_pir_status
                                           lv_ovr_status_eina.

* Get Reconciliation Status
            PERFORM get_recon_status USING ls_reco_pir-suppl_from
                                           ls_reco_pir-sap_suppl_from
                                           ls_eina_arn
                                           ls_eina
                                  CHANGING ls_reco_pir-status9
                                           lv_pir_status
                                           lv_ovr_status_eina.

* Get Reconciliation Status
            PERFORM get_recon_status USING ls_reco_pir-suppl_to
                                           ls_reco_pir-sap_suppl_to
                                           ls_eina_arn
                                           ls_eina
                                  CHANGING ls_reco_pir-status10
                                           lv_pir_status
                                           lv_ovr_status_eina.

* Get Reconciliation Status
            PERFORM get_recon_status USING ls_reco_pir-norm_vend
                                           ls_reco_pir-sap_norm_vend
                                           ls_eina_arn
                                           ls_eina
                                  CHANGING ls_reco_pir-status11
                                           lv_pir_status
                                           lv_ovr_status_eina.




          ENDIF.  " IF ls_reg_pir-pir_rel_eina = abap_true



          IF ls_reg_pir-pir_rel_eine = abap_true.

* Data to be posted  - INFORECORDPURCHORG
            CLEAR ls_eine_arn.
            LOOP AT lt_eine_arn[] INTO ls_eine_arn
              WHERE material = lv_matnr
                AND vendor   = ls_reg_pir-lifnr.

              ls_reco_pir-material   = ls_eine_arn-material.
              ls_reco_pir-vendor     = ls_eine_arn-vendor.
              ls_reco_pir-purch_org  = ls_eine_arn-purch_org.
              ls_reco_pir-info_type  = ls_eine_arn-info_type.
              ls_reco_pir-plant      = ls_eine_arn-plant.
              ls_reco_pir-delete_ind = ls_eine_arn-delete_ind.
              ls_reco_pir-pur_group  = ls_eine_arn-pur_group.
              ls_reco_pir-currency   = ls_eine_arn-currency.
              ls_reco_pir-min_po_qty = ls_eine_arn-min_po_qty.
              ls_reco_pir-nrm_po_qty = ls_eine_arn-nrm_po_qty.
              ls_reco_pir-plnd_delry = ls_eine_arn-plnd_delry.
              ls_reco_pir-overdeltol = ls_eine_arn-overdeltol.
              ls_reco_pir-under_tol  = ls_eine_arn-under_tol.
              ls_reco_pir-net_price  = ls_eine_arn-net_price.
              ls_reco_pir-price_unit = ls_eine_arn-price_unit.
              ls_reco_pir-orderpr_un = ls_eine_arn-orderpr_un.
              ls_reco_pir-price_date = ls_eine_arn-price_date.
              ls_reco_pir-calc_bpumz = ls_eine_arn-conv_num1.
              ls_reco_pir-calc_bpumn = ls_eine_arn-conv_den1.
              ls_reco_pir-eff_price  = ls_eine_arn-eff_price.
              ls_reco_pir-cond_group = ls_eine_arn-cond_group.
              ls_reco_pir-tax_code   = ls_eine_arn-tax_code.

* Existing Data  - INFORECORDPURCHORG
              CLEAR ls_eine.
              READ TABLE lt_eine[] INTO ls_eine
              WITH KEY material  = ls_eine_arn-material
                       vendor    = ls_eine_arn-vendor
                       purch_org = ls_eine_arn-purch_org
                       info_type = ls_eine_arn-info_type
                       plant     = ls_eine_arn-plant.
              IF sy-subrc = 0.
                ls_reco_pir-sap_material   = ls_eine-material.
                ls_reco_pir-sap_vendor     = ls_eine-vendor.
                ls_reco_pir-sap_purch_org  = ls_eine-purch_org.
                ls_reco_pir-sap_info_type  = ls_eine-info_type.
                ls_reco_pir-sap_plant      = ls_eine-plant.
                ls_reco_pir-sap_delete_ind = ls_eine-delete_ind.
                ls_reco_pir-sap_pur_group  = ls_eine-pur_group.
                ls_reco_pir-sap_currency   = ls_eine-currency.
                ls_reco_pir-sap_min_po_qty = ls_eine-min_po_qty.
                ls_reco_pir-sap_nrm_po_qty = ls_eine-nrm_po_qty.
                ls_reco_pir-sap_plnd_delry = ls_eine-plnd_delry.
                ls_reco_pir-sap_overdeltol = ls_eine-overdeltol.
                ls_reco_pir-sap_under_tol  = ls_eine-under_tol.
                ls_reco_pir-sap_net_price  = ls_eine-net_price.
                ls_reco_pir-sap_price_unit = ls_eine-price_unit.
                ls_reco_pir-sap_orderpr_un = ls_eine-orderpr_un.
                ls_reco_pir-sap_price_date = ls_eine-price_date.
                ls_reco_pir-sap_bpumz      = ls_eine-conv_num1.
                ls_reco_pir-sap_bpumn      = ls_eine-conv_den1.
                ls_reco_pir-sap_eff_price  = ls_eine-eff_price.
                ls_reco_pir-sap_cond_group = ls_eine-cond_group.
                ls_reco_pir-sap_tax_code   = ls_eine-tax_code.
              ENDIF.



              CLEAR lv_ovr_status_eine.


* Get Reconciliation Status
              PERFORM get_recon_status USING ls_reco_pir-purch_org
                                             ls_reco_pir-sap_purch_org
                                             ls_eine_arn
                                             ls_eine
                                    CHANGING ls_reco_pir-status12
                                             lv_pir_status
                                             lv_ovr_status_eine.

* Get Reconciliation Status
              PERFORM get_recon_status USING ls_reco_pir-info_type
                                             ls_reco_pir-sap_info_type
                                             ls_eine_arn
                                             ls_eine
                                    CHANGING ls_reco_pir-status13
                                             lv_pir_status
                                             lv_ovr_status_eine.

** Get Reconciliation Status
*              PERFORM get_recon_status USING ls_reco_pir-plant
*                                             ls_reco_pir-sap_plant
*                                             ls_eine_arn
*                                             ls_eine
*                                    CHANGING ls_reco_pir-status14
*                                             lv_pir_status
*                                             lv_ovr_status_eine.
*
** Get Reconciliation Status
*              PERFORM get_recon_status USING ls_reco_pir-delete_ind
*                                             ls_reco_pir-sap_delete_ind
*                                             ls_eine_arn
*                                             ls_eine
*                                    CHANGING ls_reco_pir-status15
*                                             lv_pir_status
*                                             lv_ovr_status_eine.

* Get Reconciliation Status
              PERFORM get_recon_status USING ls_reco_pir-pur_group
                                             ls_reco_pir-sap_pur_group
                                             ls_eine_arn
                                             ls_eine
                                    CHANGING ls_reco_pir-status16
                                             lv_pir_status
                                             lv_ovr_status_eine.

* Get Reconciliation Status
              PERFORM get_recon_status USING ls_reco_pir-currency
                                             ls_reco_pir-sap_currency
                                             ls_eine_arn
                                             ls_eine
                                    CHANGING ls_reco_pir-status17
                                             lv_pir_status
                                             lv_ovr_status_eine.

* Get Reconciliation Status
              PERFORM get_recon_status USING ls_reco_pir-min_po_qty
                                             ls_reco_pir-sap_min_po_qty
                                             ls_eine_arn
                                             ls_eine
                                    CHANGING ls_reco_pir-status18
                                             lv_pir_status
                                             lv_ovr_status_eine.

* Get Reconciliation Status
              PERFORM get_recon_status USING ls_reco_pir-nrm_po_qty
                                             ls_reco_pir-sap_nrm_po_qty
                                             ls_eine_arn
                                             ls_eine
                                    CHANGING ls_reco_pir-status19
                                             lv_pir_status
                                             lv_ovr_status_eine.

* Get Reconciliation Status
              PERFORM get_recon_status USING ls_reco_pir-plnd_delry
                                             ls_reco_pir-sap_plnd_delry
                                             ls_eine_arn
                                             ls_eine
                                    CHANGING ls_reco_pir-status20
                                             lv_pir_status
                                             lv_ovr_status_eine.

** Get Reconciliation Status
*              PERFORM get_recon_status USING ls_reco_pir-overdeltol
*                                             ls_reco_pir-sap_overdeltol
*                                             ls_eine_arn
*                                             ls_eine
*                                    CHANGING ls_reco_pir-status21
*                                             lv_pir_status
*                                             lv_ovr_status_eine.
*
** Get Reconciliation Status
*              PERFORM get_recon_status USING ls_reco_pir-under_tol
*                                             ls_reco_pir-sap_under_tol
*                                             ls_eine_arn
*                                             ls_eine
*                                    CHANGING ls_reco_pir-status22
*                                             lv_pir_status
*                                             lv_ovr_status_eine.

* Get Reconciliation Status
              PERFORM get_recon_status USING ls_reco_pir-net_price
                                             ls_reco_pir-sap_net_price
                                             ls_eine_arn
                                             ls_eine
                                    CHANGING ls_reco_pir-status23
                                             lv_pir_status
                                             lv_ovr_status_eine.

* Get Reconciliation Status
              PERFORM get_recon_status USING ls_reco_pir-price_unit
                                             ls_reco_pir-sap_price_unit
                                             ls_eine_arn
                                             ls_eine
                                    CHANGING ls_reco_pir-status24
                                             lv_pir_status
                                             lv_ovr_status_eine.

* Get Reconciliation Status
              PERFORM get_recon_status USING ls_reco_pir-orderpr_un
                                             ls_reco_pir-sap_orderpr_un
                                             ls_eine_arn
                                             ls_eine
                                    CHANGING ls_reco_pir-status25
                                             lv_pir_status
                                             lv_ovr_status_eine.

* Get Reconciliation Status
              PERFORM get_recon_status USING ls_reco_pir-price_date
                                             ls_reco_pir-sap_price_date
                                             ls_eine_arn
                                             ls_eine
                                    CHANGING ls_reco_pir-status26
                                             lv_pir_status
                                             lv_ovr_status_eine.

* Get Reconciliation Status
              PERFORM get_recon_status USING ls_reco_pir-calc_bpumz
                                             ls_reco_pir-sap_bpumz
                                             ls_eine_arn
                                             ls_eine
                                    CHANGING ls_reco_pir-status27
                                             lv_pir_status
                                             lv_ovr_status_eine.

* Get Reconciliation Status
              PERFORM get_recon_status USING ls_reco_pir-calc_bpumn
                                             ls_reco_pir-sap_bpumn
                                             ls_eine_arn
                                             ls_eine
                                    CHANGING ls_reco_pir-status28
                                             lv_pir_status
                                             lv_ovr_status_eine.

** Get Reconciliation Status
*              PERFORM get_recon_status USING ls_reco_pir-eff_price
*                                             ls_reco_pir-sap_eff_price
*                                             ls_eine_arn
*                                             ls_eine
*                                    CHANGING ls_reco_pir-status29
*                                             lv_pir_status
*                                             lv_ovr_status_eine.

* Get Reconciliation Status
              PERFORM get_recon_status USING ls_reco_pir-cond_group
                                             ls_reco_pir-sap_cond_group
                                             ls_eine_arn
                                             ls_eine
                                    CHANGING ls_reco_pir-status30
                                             lv_pir_status
                                             lv_ovr_status_eine.

* Get Reconciliation Status
              PERFORM get_recon_status USING ls_reco_pir-tax_code
                                             ls_reco_pir-sap_tax_code
                                             ls_eine_arn
                                             ls_eine
                                    CHANGING ls_reco_pir-status31
                                             lv_pir_status
                                             lv_ovr_status_eine.


              IF lv_ovr_status_eina = lv_ovr_status_eine.
                ls_reco_pir-ovr_status = lv_ovr_status_eine.
              ELSE.
                ls_reco_pir-ovr_status = abap_true.
              ENDIF.

              IF ls_reco_pir-ovr_status IS NOT INITIAL.
                lv_count = lv_count + 1.
              ENDIF.

              APPEND ls_reco_pir TO lt_reco_pir[].
              lv_no_pir = abap_false.

            ENDLOOP.  " LOOP AT lt_eine_arn[] INTO ls_eine_arn


          ENDIF.  " IF ls_reg_pir-pir_rel_eine = abap_true


          IF ls_reg_pir-pir_rel_eine IS INITIAL.

            ls_reco_pir-ovr_status = lv_ovr_status_eina.

            IF ls_reco_pir-ovr_status IS NOT INITIAL.
              lv_count = lv_count + 1.
            ENDIF.

            APPEND ls_reco_pir TO lt_reco_pir[].
            lv_no_pir = abap_false.
          ENDIF.



        ENDLOOP.  " LOOP AT lt_reg_pir[] INTO ls_reg_pir


      ENDIF.  " READ TABLE lt_reg_data_def[] INTO ls_reg_data_def
    ENDIF.  " READ TABLE lt_idno_data[] INTO ls_idno_data



    IF lv_no_pir = abap_true.
      ls_reco_pir-ovr_status = abap_true.
      lv_count = lv_count + 1.
      lv_pir_status = abap_true.
      APPEND ls_reco_pir TO lt_reco_pir[].
    ENDIF.


* Update UOM status in FAN Exist data
    IF <ls_fan_exist> IS ASSIGNED. UNASSIGN <ls_fan_exist>. ENDIF.
    READ TABLE lt_fan_exist ASSIGNING <ls_fan_exist>
    WITH KEY fan = ls_fan-fan_id.
    IF sy-subrc = 0.

      IF lv_pir_status = abap_true.
        <ls_fan_exist>-ovr_status = abap_true.
        fc_v_count = fc_v_count + 1.
      ENDIF.

      <ls_fan_exist>-status_pir = lv_count.  " lv_pir_status.
    ENDIF.

  ENDLOOP.  " LOOP AT lt_fan[] INTO ls_fan




  fc_t_reco_pir[]  = lt_reco_pir[].
  fc_t_fan_exist[] = lt_fan_exist[].


ENDFORM.                    " BUILD_RECO_PIR_DATA
*&---------------------------------------------------------------------*
*&      Form  BUILD_RECO_BANNER_DATA
*&---------------------------------------------------------------------*
* Build Reconciliation BANNER Data
*----------------------------------------------------------------------*
FORM build_reco_banner_data USING fu_t_fan                TYPE ty_t_fan
                                  fu_t_idno_data          TYPE ty_t_idno_data
                                  fu_t_prod_data          TYPE ty_t_prod_data
                                  fu_t_reg_data_def       TYPE ty_t_reg_data
                                  fu_t_fanid              TYPE ty_t_fanid
                                  fu_t_tvarvc_uni_lni     TYPE tvarvc_t
                                  fu_t_salesdata_arn      TYPE bapie1mvkert_tab
                                  fu_t_salesdata          TYPE bapie1mvkert_tab
                                  fu_t_posdata_arn        TYPE bapie1wlk2rt_tab
                                  fu_t_posdata            TYPE bapie1wlk2rt_tab
                                  fu_t_plantdata_arn      TYPE bapie1marcrt_tab
                                  fu_t_plantdata          TYPE bapie1marcrt_tab
                                  fu_t_bapi_salesext_arn  TYPE zmd_t_bapi_salesext
                                  fu_t_bapi_salesext      TYPE zmd_t_bapi_salesext
                                  fu_t_bapi_plantext_arn  TYPE zmd_t_bapi_plantext
                                  fu_t_bapi_plantext      TYPE zmd_t_bapi_plantext
                         CHANGING fc_t_fan_exist          TYPE ty_t_fan_exist
                                  fc_t_reco_banner        TYPE ty_t_reco_banner
                                  fc_v_count              TYPE i.


  DATA: lt_fan            TYPE ty_t_fan,
        ls_fan            TYPE ty_s_fan,
        lt_idno_data      TYPE ty_t_idno_data,
        ls_idno_data      TYPE ty_s_idno_data,
        lt_prod_data      TYPE ty_t_prod_data,
        ls_prod_data      TYPE ty_s_prod_data,
        lt_reg_data_def   TYPE ty_t_reg_data,
        ls_reg_data_def   TYPE ty_s_reg_data,
        lt_fanid          TYPE ty_t_fanid,
        ls_fanid          TYPE ty_s_fanid,

        lt_sales_arn      TYPE bapie1mvkert_tab,
        lt_sales          TYPE bapie1mvkert_tab,
        lt_pos_arn        TYPE bapie1wlk2rt_tab,
        lt_pos            TYPE bapie1wlk2rt_tab,
        lt_plant_arn      TYPE bapie1marcrt_tab,
        lt_plant          TYPE bapie1marcrt_tab,
        lt_salesext_arn   TYPE zmd_t_bapi_salesext,
        lt_salesext       TYPE zmd_t_bapi_salesext,
        lt_plantext_arn   TYPE zmd_t_bapi_plantext,
        lt_plantext       TYPE zmd_t_bapi_plantext,


        ls_sales_arn      TYPE bapie1mvkert,
        ls_sales          TYPE bapie1mvkert,
        ls_pos_arn        TYPE bapie1wlk2rt,
        ls_pos            TYPE bapie1wlk2rt,
        ls_plant_arn      TYPE bapie1marcrt,
        ls_plant          TYPE bapie1marcrt,
        ls_salesext_arn   TYPE zmd_s_bapi_salesext,
        ls_salesext       TYPE zmd_s_bapi_salesext,
        ls_plantext_arn   TYPE zmd_s_bapi_plantext,
        ls_plantext       TYPE zmd_s_bapi_plantext,


        lt_fan_exist      TYPE ty_t_fan_exist,
        ls_tvarvc_uni_lni TYPE tvarvc,
        lt_reco_banner    TYPE ty_t_reco_banner,
        ls_reco_banner    TYPE ty_s_reco_banner,
        lt_reg_banner     TYPE ztarn_reg_banner,
        ls_reg_banner     TYPE zarn_reg_banner,

        lv_matnr          TYPE matnr,
        lv_vkorg          TYPE vkorg,
        lv_vtweg          TYPE vtweg,
        lv_site_uni       TYPE werks_d,
        lv_site_lni       TYPE werks_d,
        lv_no_banner      TYPE flag,
        lv_count          TYPE i,
        lv_banner_status  TYPE zarn_reco_status,
        lv_ovr_status     TYPE zarn_ovr_status.

  FIELD-SYMBOLS: <ls_fan_exist>  TYPE ty_s_fan_exist.


  lt_fan[]          = fu_t_fan[].
  lt_idno_data[]    = fu_t_idno_data[].
  lt_prod_data[]    = fu_t_prod_data[].
  lt_reg_data_def[] = fu_t_reg_data_def[].
  lt_fanid[]        = fu_t_fanid[].
  lt_sales_arn[]    = fu_t_salesdata_arn[].
  lt_sales[]        = fu_t_salesdata[].
  lt_pos_arn[]      = fu_t_posdata_arn[].
  lt_pos[]          = fu_t_posdata[].
  lt_plant_arn[]    = fu_t_plantdata_arn[].
  lt_plant[]        = fu_t_plantdata[].
  lt_salesext_arn[] = fu_t_bapi_salesext_arn[].
  lt_salesext[]     = fu_t_bapi_salesext[].
  lt_fan_exist[]    = fc_t_fan_exist[].
  lt_plantext_arn[] = fu_t_bapi_plantext_arn[].                                     "++ONLD-822 JKH 17.02.2017
  lt_plantext[]     = fu_t_bapi_plantext[].                                         "++ONLD-822 JKH 17.02.2017

  LOOP AT lt_fan[] INTO ls_fan.

    CLEAR lv_count.

    CLEAR: ls_reco_banner, lv_banner_status.
    ls_reco_banner-fan         = ls_fan-fan_id.

    lv_no_banner = abap_true.


* Get Article from FAN ID list, if not found then assign fan id as article
    CLEAR ls_fanid.
    READ TABLE lt_fanid[] INTO ls_fanid
    WITH TABLE KEY zzfan = ls_fan-fan_id.
    IF sy-subrc = 0.
      lv_matnr = ls_fanid-matnr.
    ELSE.
      lv_matnr = ls_fan-fan_id.

      CALL FUNCTION 'CONVERSION_EXIT_MATN1_INPUT'
        EXPORTING
          input        = lv_matnr
        IMPORTING
          output       = lv_matnr
        EXCEPTIONS
          length_error = 1
          OTHERS       = 2.

    ENDIF.  " READ TABLE lt_fanid[] INTO ls_fanid


* Check if IDNO exist
    CLEAR ls_idno_data.
    READ TABLE lt_idno_data[] INTO ls_idno_data
    WITH KEY fan_id = ls_fan-fan_id.
    IF sy-subrc = 0.

      ls_reco_banner-idno        = ls_idno_data-idno.
      ls_reco_banner-current_ver = ls_idno_data-current_ver.

* Get Regional ean data
      CLEAR ls_reg_data_def.
      READ TABLE lt_reg_data_def[] INTO ls_reg_data_def
      WITH KEY idno = ls_idno_data-idno.
      IF sy-subrc = 0.

        lt_reg_banner[] = ls_reg_data_def-zarn_reg_banner[].

* For each ean, get the data expected to be posted and data which is already existing
        LOOP AT lt_reg_banner[] INTO ls_reg_banner.

          CLEAR: lv_vkorg, lv_vtweg, lv_site_uni, lv_site_lni.

          CASE ls_reg_banner-banner.
            WHEN zcl_constants=>gc_banner_1000.              lv_vtweg = zcl_constants=>gc_dist_ch_10.
            WHEN zcl_constants=>gc_banner_3000.              lv_vtweg = zcl_constants=>gc_dist_ch_20.
            WHEN zcl_constants=>gc_banner_4000.              lv_vtweg = zcl_constants=>gc_dist_ch_20.
            WHEN zcl_constants=>gc_banner_5000.              lv_vtweg = zcl_constants=>gc_dist_ch_20.
            WHEN zcl_constants=>gc_banner_6000.              lv_vtweg = zcl_constants=>gc_dist_ch_20.
          ENDCASE.


* Get Site from Banner for UNI
          CLEAR ls_tvarvc_uni_lni.
          READ TABLE fu_t_tvarvc_uni_lni[] INTO ls_tvarvc_uni_lni
          WITH KEY name = gc_tvarvc-vkorg_uni
                   low  = ls_reg_banner-banner.
          IF sy-subrc = 0 AND ls_tvarvc_uni_lni-high IS NOT INITIAL.
            lv_site_uni = lv_site_lni = ls_tvarvc_uni_lni-high.
          ENDIF.


          IF ls_reg_banner-banner NE zcl_constants=>gc_banner_1000.
* Get Site from Banner for LNI - overwrite LNI
            CLEAR ls_tvarvc_uni_lni.
            READ TABLE fu_t_tvarvc_uni_lni[] INTO ls_tvarvc_uni_lni
            WITH KEY name = gc_tvarvc-vkorg_lni
                     low  = ls_reg_banner-banner.
            IF sy-subrc = 0 AND ls_tvarvc_uni_lni-high IS NOT INITIAL.
              lv_site_lni = ls_tvarvc_uni_lni-high.
            ELSE.
              CLEAR lv_site_lni.
            ENDIF.
          ENDIF.

          CLEAR: ls_reco_banner.
          ls_reco_banner-fan         = ls_fan-fan_id.
          ls_reco_banner-idno        = ls_idno_data-idno.
          ls_reco_banner-current_ver = ls_idno_data-current_ver.

          ls_reco_banner-banner      = ls_reg_banner-banner.

* Data to be posted - SALESDATA
          CLEAR ls_sales_arn.
          READ TABLE lt_sales_arn[] INTO ls_sales_arn
          WITH KEY material   = lv_matnr
                   sales_org  = ls_reg_banner-banner
                   distr_chan = lv_vtweg.
          IF sy-subrc = 0.
            ls_reco_banner-material   = ls_sales_arn-material.
            ls_reco_banner-sales_org  = ls_sales_arn-sales_org.
            ls_reco_banner-distr_chan = ls_sales_arn-distr_chan.

            ls_reco_banner-matl_stats = ls_sales_arn-matl_stats.
            ls_reco_banner-cash_disc  = ls_sales_arn-cash_disc.
            ls_reco_banner-sal_status = ls_sales_arn-sal_status.
            ls_reco_banner-valid_from = ls_sales_arn-valid_from.
            ls_reco_banner-min_order  = ls_sales_arn-min_order.
            ls_reco_banner-sales_unit = ls_sales_arn-sales_unit.
            ls_reco_banner-item_cat   = ls_sales_arn-item_cat.
            ls_reco_banner-prod_hier  = ls_sales_arn-prod_hier.
            ls_reco_banner-acct_assgt = ls_sales_arn-acct_assgt.
            ls_reco_banner-assort_lev = ls_sales_arn-assort_lev.
            ls_reco_banner-mat_pr_grp = ls_sales_arn-mat_pr_grp.
            ls_reco_banner-li_proc_st = ls_sales_arn-li_proc_st.
            ls_reco_banner-li_proc_dc = ls_sales_arn-li_proc_dc.
            ls_reco_banner-list_st_fr = ls_sales_arn-list_st_fr.
            ls_reco_banner-list_st_to = ls_sales_arn-list_st_to.
            ls_reco_banner-list_dc_fr = ls_sales_arn-list_dc_fr.
            ls_reco_banner-list_dc_to = ls_sales_arn-list_dc_to.
            ls_reco_banner-sell_st_fr = ls_sales_arn-sell_st_fr.
            ls_reco_banner-sell_st_to = ls_sales_arn-sell_st_to.
            ls_reco_banner-sell_dc_fr = ls_sales_arn-sell_dc_fr.
            ls_reco_banner-sell_dc_to = ls_sales_arn-sell_dc_to.
          ENDIF.


* Existing data - SALESDATA
          CLEAR ls_sales.
          READ TABLE lt_sales[] INTO ls_sales
          WITH KEY material   = lv_matnr
                   sales_org  = ls_reg_banner-banner
                   distr_chan = lv_vtweg.
          IF sy-subrc = 0.
            ls_reco_banner-sap_material   = ls_sales-material.
            ls_reco_banner-sap_sales_org  = ls_sales-sales_org.
            ls_reco_banner-sap_distr_chan = ls_sales-distr_chan.

            ls_reco_banner-sap_matl_stats = ls_sales-matl_stats.
            ls_reco_banner-sap_cash_disc  = ls_sales-cash_disc.
            ls_reco_banner-sap_sal_status = ls_sales-sal_status.
            ls_reco_banner-sap_valid_from = ls_sales-valid_from.
            ls_reco_banner-sap_min_order  = ls_sales-min_order.
            ls_reco_banner-sap_sales_unit = ls_sales-sales_unit.
            ls_reco_banner-sap_item_cat   = ls_sales-item_cat.
            ls_reco_banner-sap_prod_hier  = ls_sales-prod_hier.
            ls_reco_banner-sap_acct_assgt = ls_sales-acct_assgt.
            ls_reco_banner-sap_assort_lev = ls_sales-assort_lev.
            ls_reco_banner-sap_mat_pr_grp = ls_sales-mat_pr_grp.
            ls_reco_banner-sap_li_proc_st = ls_sales-li_proc_st.
            ls_reco_banner-sap_li_proc_dc = ls_sales-li_proc_dc.
            ls_reco_banner-sap_list_st_fr = ls_sales-list_st_fr.
            ls_reco_banner-sap_list_st_to = ls_sales-list_st_to.
            ls_reco_banner-sap_list_dc_fr = ls_sales-list_dc_fr.
            ls_reco_banner-sap_list_dc_to = ls_sales-list_dc_to.
            ls_reco_banner-sap_sell_st_fr = ls_sales-sell_st_fr.
            ls_reco_banner-sap_sell_st_to = ls_sales-sell_st_to.
            ls_reco_banner-sap_sell_dc_fr = ls_sales-sell_dc_fr.
            ls_reco_banner-sap_sell_dc_to = ls_sales-sell_dc_to.
          ENDIF.



* Data to be posted - SALESEXTDATA
          CLEAR ls_salesext_arn.
          READ TABLE lt_salesext_arn[] INTO ls_salesext_arn
          WITH KEY material   = lv_matnr
                   sales_org  = ls_reg_banner-banner
                   distr_chan = lv_vtweg.
          IF sy-subrc = 0.
            ls_reco_banner-zzcatman = ls_salesext_arn-zzcatman.
          ENDIF.

* Existing data - SALESEXTDATA
          CLEAR ls_salesext.
          READ TABLE lt_salesext[] INTO ls_salesext
          WITH KEY material   = lv_matnr
                   sales_org  = ls_reg_banner-banner
                   distr_chan = lv_vtweg.
          IF sy-subrc = 0.
            ls_reco_banner-sap_zzcatman = ls_salesext-zzcatman.
          ENDIF.



* Data to be posted - POSDATA
          CLEAR ls_pos_arn.
          READ TABLE lt_pos_arn[] INTO ls_pos_arn
          WITH KEY material   = lv_matnr
                   sales_org  = ls_reg_banner-banner
                   distr_chan = lv_vtweg
                   plant      = space.
          IF sy-subrc = 0.
            ls_reco_banner-wlk2_no_rep_key = ls_pos_arn-no_rep_key.
            ls_reco_banner-wlk2_price_reqd = ls_pos_arn-price_reqd.
            ls_reco_banner-wlk2_sell_st_fr = ls_pos_arn-sell_st_fr.
            ls_reco_banner-wlk2_sell_st_to = ls_pos_arn-sell_st_to.
            ls_reco_banner-wlk2_sal_status = ls_pos_arn-sal_status.
            ls_reco_banner-wlk2_valid_from = ls_pos_arn-valid_from.
            ls_reco_banner-wlk2_disc_allwd = ls_pos_arn-disc_allwd.
            ls_reco_banner-wlk2_scales_grp = ls_pos_arn-scales_grp.

          ENDIF.

* Existing data - POSDATA
          CLEAR ls_pos.
          READ TABLE lt_pos[] INTO ls_pos
          WITH KEY material   = lv_matnr
                   sales_org  = ls_reg_banner-banner
                   distr_chan = lv_vtweg
                   plant      = space.
          IF sy-subrc = 0.
            ls_reco_banner-sap_wlk2_no_rep_key = ls_pos-no_rep_key.
            ls_reco_banner-sap_wlk2_price_reqd = ls_pos-price_reqd.
            ls_reco_banner-sap_wlk2_sell_st_fr = ls_pos-sell_st_fr.
            ls_reco_banner-sap_wlk2_sell_st_to = ls_pos-sell_st_to.
            ls_reco_banner-sap_wlk2_sal_status = ls_pos-sal_status.
            ls_reco_banner-sap_wlk2_valid_from = ls_pos-valid_from.
            ls_reco_banner-sap_wlk2_disc_allwd = ls_pos-disc_allwd.
            ls_reco_banner-sap_wlk2_scales_grp = ls_pos-scales_grp.
          ENDIF.



* Data to be posted UNI/LNI - PLANTDATA
          CLEAR ls_plant_arn.
          READ TABLE lt_plant_arn[] INTO ls_plant_arn
          WITH KEY material = lv_matnr
                   plant    = lv_site_uni.
          IF sy-subrc = 0.
* Get SOS for Site from Banner for UNI
            ls_reco_banner-uni_sup_source = ls_reco_banner-lni_sup_source = ls_plant_arn-sup_source.
          ENDIF.


          IF ls_reg_banner-banner NE zcl_constants=>gc_banner_1000.
            CLEAR ls_plant_arn.
            READ TABLE lt_plant_arn[] INTO ls_plant_arn
            WITH KEY material = lv_matnr
                     plant    = lv_site_lni.
            IF sy-subrc = 0.
* Get SOS for Site from Banner for LNI - Overwrite LNI
              ls_reco_banner-lni_sup_source = ls_plant_arn-sup_source.
            ELSE.
              CLEAR ls_reco_banner-lni_sup_source.
            ENDIF.
          ENDIF.


* Existing data - PLANTDATA
          CLEAR ls_plant.
          READ TABLE lt_plant[] INTO ls_plant
          WITH KEY material = lv_matnr
                   plant    = lv_site_uni.
          IF sy-subrc = 0.
* Get SOS for Site from Banner for UNI
            ls_reco_banner-sap_uni_sup_source = ls_reco_banner-sap_lni_sup_source = ls_plant-sup_source.
          ENDIF.


          IF ls_reg_banner-banner NE zcl_constants=>gc_banner_1000.
            CLEAR ls_plant.
            READ TABLE lt_plant[] INTO ls_plant
            WITH KEY material = lv_matnr
                     plant    = lv_site_lni.
            IF sy-subrc = 0.
* Get SOS for Site from Banner for LNI - Overwrite LNI
              ls_reco_banner-sap_lni_sup_source = ls_plant-sup_source.
            ELSE.
              CLEAR ls_reco_banner-sap_lni_sup_source.
            ENDIF.
          ENDIF.


* Data to be posted - PLANTEXTDATA
          CLEAR ls_plantext_arn.                                                    "++ONLD-822 JKH 17.02.2017
          READ TABLE lt_plantext_arn[] INTO ls_plantext_arn
          WITH KEY material   = lv_matnr
                   plant      = lv_site_uni.
          IF sy-subrc = 0.
            ls_reco_banner-zzonline_status = ls_plantext_arn-zzonline_status.
            ls_reco_banner-zz_pbs          = ls_plantext_arn-zz_pbs.
          ENDIF.

* Existing data - PLANTEXTDATA
          CLEAR ls_plantext.                                                        "++ONLD-822 JKH 17.02.2017
          READ TABLE lt_plantext[] INTO ls_plantext
          WITH KEY material   = lv_matnr
                   plant      = lv_site_uni.
          IF sy-subrc = 0.
            ls_reco_banner-sap_zzonline_status = ls_plantext-zzonline_status.
            ls_reco_banner-sap_zz_pbs          = ls_plantext-zz_pbs.
          ENDIF.



          CLEAR lv_ovr_status.

* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_banner-material
                                         ls_reco_banner-sap_material
                                         ls_sales_arn
                                         ls_sales
                                CHANGING ls_reco_banner-status1
                                         lv_banner_status
                                         lv_ovr_status.

** Get Reconciliation Status
*          PERFORM get_recon_status USING ls_reco_banner-sales_org
*                                         ls_reco_banner-sap_sales_org
*                                         ls_sales_arn
*                                         ls_sales
*                                CHANGING ls_reco_banner-status2
*                                         lv_banner_status
*                                         lv_ovr_status.
*
** Get Reconciliation Status
*          PERFORM get_recon_status USING ls_reco_banner-distr_chan
*                                         ls_reco_banner-sap_distr_chan
*                                         ls_sales_arn
*                                         ls_sales
*                                CHANGING ls_reco_banner-status3
*                                         lv_banner_status
*                                         lv_ovr_status.

* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_banner-uni_sup_source
                                         ls_reco_banner-sap_uni_sup_source
                                         ls_plant_arn
                                         ls_plant
                                CHANGING ls_reco_banner-status4
                                         lv_banner_status
                                         lv_ovr_status.

* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_banner-lni_sup_source
                                         ls_reco_banner-sap_lni_sup_source
                                         ls_plant_arn
                                         ls_plant
                                CHANGING ls_reco_banner-status5
                                         lv_banner_status
                                         lv_ovr_status.

* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_banner-matl_stats
                                         ls_reco_banner-sap_matl_stats
                                         ls_sales_arn
                                         ls_sales
                                CHANGING ls_reco_banner-status6
                                         lv_banner_status
                                         lv_ovr_status.
** Get Reconciliation Status
*          PERFORM get_recon_status USING ls_reco_banner-cash_disc
*                                         ls_reco_banner-sap_cash_disc
*                                         ls_sales_arn
*                                         ls_sales
*                                CHANGING ls_reco_banner-status7
*                                         lv_banner_status
*                                         lv_ovr_status.

** Get Reconciliation Status
*          PERFORM get_recon_status USING ls_reco_banner-sal_status
*                                         ls_reco_banner-sap_sal_status      "--IR5047357 JKH 16.08.2016
*                                         ls_sales_arn
*                                         ls_sales
*                                CHANGING ls_reco_banner-status8
*                                         lv_banner_status
*                                         lv_ovr_status.
*
** Get Reconciliation Status
*          PERFORM get_recon_status USING ls_reco_banner-valid_from
*                                         ls_reco_banner-sap_valid_from      "--IR5047357 JKH 16.08.2016
*                                         ls_sales_arn
*                                         ls_sales
*                                CHANGING ls_reco_banner-status9
*                                         lv_banner_status
*                                         lv_ovr_status.

* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_banner-min_order
                                         ls_reco_banner-sap_min_order
                                         ls_sales_arn
                                         ls_sales
                                CHANGING ls_reco_banner-status10
                                         lv_banner_status
                                         lv_ovr_status.

* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_banner-sales_unit
                                         ls_reco_banner-sap_sales_unit
                                         ls_sales_arn
                                         ls_sales
                                CHANGING ls_reco_banner-status11
                                         lv_banner_status
                                         lv_ovr_status.

** Get Reconciliation Status
*          PERFORM get_recon_status USING ls_reco_banner-item_cat
*                                         ls_reco_banner-sap_item_cat
*                                         ls_sales_arn
*                                         ls_sales
*                                CHANGING ls_reco_banner-status12
*                                         lv_banner_status
*                                         lv_ovr_status.

* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_banner-prod_hier
                                         ls_reco_banner-sap_prod_hier
                                         ls_sales_arn
                                         ls_sales
                                CHANGING ls_reco_banner-status13
                                         lv_banner_status
                                         lv_ovr_status.

* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_banner-acct_assgt
                                         ls_reco_banner-sap_acct_assgt
                                         ls_sales_arn
                                         ls_sales
                                CHANGING ls_reco_banner-status14
                                         lv_banner_status
                                         lv_ovr_status.

* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_banner-assort_lev
                                         ls_reco_banner-sap_assort_lev
                                         ls_sales_arn
                                         ls_sales
                                CHANGING ls_reco_banner-status15
                                         lv_banner_status
                                         lv_ovr_status.

** Get Reconciliation Status
*          PERFORM get_recon_status USING ls_reco_banner-mat_pr_grp
*                                         ls_reco_banner-sap_mat_pr_grp
*                                         ls_sales_arn
*                                         ls_sales
*                                CHANGING ls_reco_banner-status16
*                                         lv_banner_status
*                                         lv_ovr_status.
*
** Get Reconciliation Status
*          PERFORM get_recon_status USING ls_reco_banner-li_proc_st
*                                         ls_reco_banner-sap_li_proc_st
*                                         ls_sales_arn
*                                         ls_sales
*                                CHANGING ls_reco_banner-status17
*                                         lv_banner_status
*                                         lv_ovr_status.
*
** Get Reconciliation Status
*          PERFORM get_recon_status USING ls_reco_banner-li_proc_dc
*                                         ls_reco_banner-sap_li_proc_dc
*                                         ls_sales_arn
*                                         ls_sales
*                                CHANGING ls_reco_banner-status18
*                                         lv_banner_status
*                                         lv_ovr_status.
*
** Get Reconciliation Status
*          PERFORM get_recon_status USING ls_reco_banner-list_st_fr
*                                         ls_reco_banner-sap_list_st_fr
*                                         ls_sales_arn
*                                         ls_sales
*                                CHANGING ls_reco_banner-status19
*                                         lv_banner_status
*                                         lv_ovr_status.
*
** Get Reconciliation Status
*          PERFORM get_recon_status USING ls_reco_banner-list_st_to
*                                         ls_reco_banner-sap_list_st_to
*                                         ls_sales_arn
*                                         ls_sales
*                                CHANGING ls_reco_banner-status20
*                                         lv_banner_status
*                                         lv_ovr_status.
*
** Get Reconciliation Status
*          PERFORM get_recon_status USING ls_reco_banner-list_dc_fr
*                                         ls_reco_banner-sap_list_dc_fr
*                                         ls_sales_arn
*                                         ls_sales
*                                CHANGING ls_reco_banner-status21
*                                         lv_banner_status
*                                         lv_ovr_status.
*
** Get Reconciliation Status
*          PERFORM get_recon_status USING ls_reco_banner-list_dc_to
*                                         ls_reco_banner-sap_list_dc_to
*                                         ls_sales_arn
*                                         ls_sales
*                                CHANGING ls_reco_banner-status22
*                                         lv_banner_status
*                                         lv_ovr_status.
*
** Get Reconciliation Status
*          PERFORM get_recon_status USING ls_reco_banner-sell_st_fr
*                                         ls_reco_banner-sap_sell_st_fr
*                                         ls_sales_arn
*                                         ls_sales
*                                CHANGING ls_reco_banner-status23
*                                         lv_banner_status
*                                         lv_ovr_status.
*
** Get Reconciliation Status
*          PERFORM get_recon_status USING ls_reco_banner-sell_st_to
*                                         ls_reco_banner-sap_sell_st_to
*                                         ls_sales_arn
*                                         ls_sales
*                                CHANGING ls_reco_banner-status24
*                                         lv_banner_status
*                                         lv_ovr_status.
*
** Get Reconciliation Status
*          PERFORM get_recon_status USING ls_reco_banner-sell_dc_fr
*                                         ls_reco_banner-sap_sell_dc_fr
*                                         ls_sales_arn
*                                         ls_sales
*                                CHANGING ls_reco_banner-status25
*                                         lv_banner_status
*                                         lv_ovr_status.
*
** Get Reconciliation Status
*          PERFORM get_recon_status USING ls_reco_banner-sell_dc_to
*                                         ls_reco_banner-sap_sell_dc_to
*                                         ls_sales_arn
*                                         ls_sales
*                                CHANGING ls_reco_banner-status26
*                                         lv_banner_status
*                                         lv_ovr_status.

* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_banner-zzcatman
                                         ls_reco_banner-sap_zzcatman
                                         ls_salesext_arn
                                         ls_salesext
                                CHANGING ls_reco_banner-status27
                                         lv_banner_status
                                         lv_ovr_status.

* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_banner-wlk2_no_rep_key
                                         ls_reco_banner-sap_wlk2_no_rep_key
                                         ls_pos_arn
                                         ls_pos
                                CHANGING ls_reco_banner-status28
                                         lv_banner_status
                                         lv_ovr_status.

* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_banner-wlk2_price_reqd
                                         ls_reco_banner-sap_wlk2_price_reqd
                                         ls_pos_arn
                                         ls_pos
                                CHANGING ls_reco_banner-status29
                                         lv_banner_status
                                         lv_ovr_status.

** Get Reconciliation Status
*          PERFORM get_recon_status USING ls_reco_banner-wlk2_sell_st_fr
*                                         ls_reco_banner-sap_wlk2_sell_st_fr
*                                         ls_pos_arn
*                                         ls_pos
*                                CHANGING ls_reco_banner-status30
*                                         lv_banner_status
*                                         lv_ovr_status.
*
** Get Reconciliation Status
*          PERFORM get_recon_status USING ls_reco_banner-wlk2_sell_st_to
*                                         ls_reco_banner-sap_wlk2_sell_st_to
*                                         ls_pos_arn
*                                         ls_pos
*                                CHANGING ls_reco_banner-status31
*                                         lv_banner_status
*                                         lv_ovr_status.
*
** Get Reconciliation Status
*          PERFORM get_recon_status USING ls_reco_banner-wlk2_sal_status
*                                         ls_reco_banner-sap_wlk2_sal_status
*                                         ls_pos_arn
*                                         ls_pos
*                                CHANGING ls_reco_banner-status32
*                                         lv_banner_status
*                                         lv_ovr_status.
*
** Get Reconciliation Status
*          PERFORM get_recon_status USING ls_reco_banner-wlk2_valid_from
*                                         ls_reco_banner-sap_wlk2_valid_from
*                                         ls_pos_arn
*                                         ls_pos
*                                CHANGING ls_reco_banner-status33
*                                         lv_banner_status
*                                         lv_ovr_status.

* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_banner-wlk2_disc_allwd
                                         ls_reco_banner-sap_wlk2_disc_allwd
                                         ls_pos_arn
                                         ls_pos
                                CHANGING ls_reco_banner-status34
                                         lv_banner_status
                                         lv_ovr_status.

* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_banner-wlk2_scales_grp
                                         ls_reco_banner-sap_wlk2_scales_grp
                                         ls_pos_arn
                                         ls_pos
                                CHANGING ls_reco_banner-status35
                                         lv_banner_status
                                         lv_ovr_status.

* INS Begin of Change CI17-343 JKH 09.11.2016
* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_banner-zz_pbs
                                         ls_reco_banner-sap_zz_pbs
                                         ls_plantext_arn
                                         ls_plantext
                                CHANGING ls_reco_banner-status36
                                         lv_banner_status
                                         lv_ovr_status.
* INS End of Change CI17-343 JKH 09.11.2016

* Get Reconciliation Status
          PERFORM get_recon_status USING ls_reco_banner-zzonline_status             "++ONLD-822 JKH 17.02.2017
                                         ls_reco_banner-sap_zzonline_status
                                         ls_plantext_arn
                                         ls_plantext
                                CHANGING ls_reco_banner-status37
                                         lv_banner_status
                                         lv_ovr_status.



          ls_reco_banner-ovr_status = lv_ovr_status.

          IF lv_ovr_status IS NOT INITIAL.
            lv_count = lv_count + 1.
          ENDIF.

          APPEND ls_reco_banner TO lt_reco_banner[].
          lv_no_banner = abap_false.

        ENDLOOP.  " LOOP AT lt_reg_banner[] INTO ls_reg_banner


      ENDIF.  " READ TABLE lt_reg_data_def[] INTO ls_reg_data_def
    ENDIF.  " READ TABLE lt_idno_data[] INTO ls_idno_data



    IF lv_no_banner = abap_true.
      ls_reco_banner-ovr_status = abap_true.
      lv_count = lv_count + 1.
      lv_banner_status = abap_true.
      APPEND ls_reco_banner TO lt_reco_banner[].
    ENDIF.


* Update ean status in FAN Exist data
    IF <ls_fan_exist> IS ASSIGNED. UNASSIGN <ls_fan_exist>. ENDIF.
    READ TABLE lt_fan_exist ASSIGNING <ls_fan_exist>
    WITH KEY fan = ls_fan-fan_id.
    IF sy-subrc = 0.

      IF lv_banner_status = abap_true.
        <ls_fan_exist>-ovr_status = abap_true.
        fc_v_count = fc_v_count + 1.
      ENDIF.

      <ls_fan_exist>-status_banner = lv_count.  " lv_banner_status.
    ENDIF.

  ENDLOOP.  " LOOP AT lt_fan[] INTO ls_fan


  fc_t_reco_banner[]  = lt_reco_banner[].
  fc_t_fan_exist[]    = lt_fan_exist[].


ENDFORM.                    " BUILD_RECO_BANNER_DATA
*&---------------------------------------------------------------------*
*&      Form  GET_RECON_STATUS
*&---------------------------------------------------------------------*
* Get Reconciliation Status
*----------------------------------------------------------------------*
FORM get_recon_status USING fu_v_field
                            fu_v_sap_field
                            fu_s_post
                            fu_s_data
                   CHANGING fc_v_status     TYPE zarn_reco_status
                            fc_v_fld_status TYPE zarn_reco_status
                            fc_v_ovr_status TYPE zarn_ovr_status.

  CLEAR fc_v_status.

* if data in post field and data field doesn't exist then status is matched by default


  IF fu_v_field EQ fu_v_sap_field AND
     fu_s_post IS NOT INITIAL     AND
     fu_s_data IS NOT INITIAL.
* when data in post field and data field is same and both records exist
    fc_v_status = gc_status-matched.

  ELSEIF fu_v_field NE fu_v_sap_field AND
         fu_s_post IS NOT INITIAL     AND
         fu_s_data IS NOT INITIAL.
* when data in post field and data field is not same and both records exist
    fc_v_status     = gc_status-update.
    fc_v_fld_status = gc_status-unmatched.
    fc_v_ovr_status = gc_status-unmatched.

  ELSEIF fu_s_post IS NOT INITIAL AND
         fu_s_data IS INITIAL.
* when data in post field exist and in data field doesn't exist
    fc_v_status     = gc_status-insert.
    fc_v_fld_status = gc_status-unmatched.
    fc_v_ovr_status = gc_status-insert.

  ENDIF.

ENDFORM.                    " GET_RECON_STATUS





*&---------------------------------------------------------------------*
*&      Form  FREE_MEMORY
*&---------------------------------------------------------------------*
* FREE_MEMORY
*----------------------------------------------------------------------*
FORM free_memory .
  FREE: gt_fan[], gt_idno_data[], gt_prod_data[], gt_reg_data[], gt_reg_data_def[],

*         gt_fan_exist[], gt_reco_basic1[], gt_reco_basic2[], gt_reco_attr[], gt_reco_uom[], gt_reco_pir[],
*         gt_reco_ean[], gt_reco_banner[],

*         gt_output_9006[],

         gt_mara[], gt_marc[], gt_makt[], gt_maw1[], gt_marm[], gt_mean[], gt_eina[], gt_eine[],
          gt_mvke[], gt_wlk2[], gt_marm_idno[],

         gt_t006[], gt_t005[], gt_tcurc[], gt_coo_t[], gt_gen_uom_t[], gt_t134[], gr_tvarvc_ekorg[],
         gr_tvarvc_vkorg[], gt_tvarvc_p_org[], gt_tvarvc_uni_lni[], gt_tvta[], gt_tvarvc_hyb_code[],

         gt_lfm1[], gt_fanid[], gt_prod_uom_t[], gt_uom_cat[], gt_uomcategory[], gt_ref_article[],
         gt_pir_vendor[], gt_tvarv_ean_cate[], gt_host_prdtyp[], gt_mc_subdep[],

         gt_headdata[], gt_clientdata_arn[], gt_clientext_arn[], gt_addnlclientdata_arn[], gt_materialdescription_arn[],
         gt_unitsofmeasure_arn[], gt_internationalartno_arn[], gt_inforecord_general_arn[],
         gt_inforecord_purchorg_arn[], gt_salesdata_arn[], gt_salesext_arn[], gt_posdata_arn[], gt_plantdata_arn[],
         gt_bapi_clientext_arn[], gt_bapi_salesext_arn[],

         gt_headdata_arn[], gt_clientdata[], gt_clientext[], gt_addnlclientdata[], gt_materialdescription[],
         gt_unitsofmeasure[], gt_internationalartno[], gt_inforecord_general[],
         gt_inforecord_purchorg[], gt_salesdata[], gt_salesext[], gt_posdata[], gt_plantdata[],
         gt_bapi_clientext[], gt_bapi_salesext[],

         gs_prod_data_all, gs_reg_data_all,

         go_gui_load,

         gv_mode,  gv_pir_ekorg,  gv_error.



*  FREE: go_alvgrid_9001,
*        go_alvgrid_9002,
*        go_alvgrid_9003,
*        go_alvgrid_9004,
*        go_alvgrid_9005,
*        go_alvgrid_9006,
*        go_alvgrid_9007,
*        go_alvgrid_9008,
*
*        go_container_9001,
*        go_container_9002,
*        go_container_9003,
*        go_container_9004,
*        go_container_9005,
*        go_container_9006,
*        go_container_9007,
*        go_container_9008.



*  CLEAR: go_alvgrid_9001,
*         go_alvgrid_9002,
*         go_alvgrid_9003,
*         go_alvgrid_9004,
*         go_alvgrid_9005,
*         go_alvgrid_9006,
*         go_alvgrid_9007,
*         go_alvgrid_9008,
*
*         go_container_9001,
*         go_container_9002,
*         go_container_9003,
*         go_container_9004,
*         go_container_9005,
*         go_container_9006,
*         go_container_9007,
*         go_container_9008,
*
*         gv_pir_rel_9006,
*
*         gv_count_9002,
*         gv_count_9003,
*         gv_count_9004,
*         gv_count_9005,
*         gv_count_9006,
*         gv_count_9007,
*         gv_count_9008.


ENDFORM.                    " FREE_MEMORY
