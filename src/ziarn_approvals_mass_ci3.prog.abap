*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_APPROVALS_MASS_CI3
*&---------------------------------------------------------------------*

*&---------------------------------------------------------------------*
*&       Class (Implementation)  lcl_model
*&---------------------------------------------------------------------*
*        Text
*----------------------------------------------------------------------*
CLASS lcl_model IMPLEMENTATION.

  METHOD constructor.

    me->initialize( ).

    mr_idno[]   = ir_idno[].
    mr_chgcat[] = ir_chgcat[].
    mr_chgare[] = ir_chgare[].
    mr_nstat[]  = ir_nstat[].
    mr_rstat[]  = ir_rstat[].

  ENDMETHOD.


  METHOD initialize.
    CLEAR: mt_cc_hdr[],
           mt_cc_desc[],
           mt_reg_hdr[],
           mt_prd_version[],
           mt_ver_data[],
           mt_approval[],
           mt_appr_ch[],
           mt_rel_matrix[],
           mt_table_mastr[],
           mt_field_confg[].
  ENDMETHOD.


  METHOD get_data.

    DATA: lt_ver_data  TYPE ty_t_ver_data,
          lr_appr_area TYPE RANGE OF zarn_e_appr_area,
          ls_appr_area LIKE LINE OF lr_appr_area,
          ls_approval  TYPE zarn_approval,
          lt_appr_hdr  TYPE ty_t_appr_hdr,
          ls_appr_hdr  TYPE ty_s_appr_hdr,
          ls_chgare    LIKE LINE OF mr_chgare.





* Convert chg_area input to appr_area
    CLEAR: lr_appr_area[].
    LOOP AT mr_chgare[] INTO ls_chgare.
      ls_appr_area-sign   = ls_chgare-sign.
      ls_appr_area-option = ls_chgare-option.

      IF ls_chgare-low = 'N'.
        ls_appr_area-low = 'NAT'.
      ELSEIF ls_chgare-low = 'R'.
        ls_appr_area-low = 'REG'.
      ENDIF.

      IF ls_chgare-high = 'N'.
        ls_appr_area-high = 'NAT'.
      ELSEIF ls_chgare-high = 'R'.
        ls_appr_area-high = 'REG'.
      ENDIF.

      APPEND ls_appr_area TO lr_appr_area[].
    ENDLOOP.  " LOOP AT mr_chgare[] INTO ls_chgare


* Read Approvals
* only not posted are retreived, in GUI all the records are retreived but PO records are later deleted
* in mass, only <> PO are retreived
    SELECT * FROM zarn_approval
      INTO CORRESPONDING FIELDS OF TABLE mt_approval[]
      WHERE idno          IN mr_idno[]
        AND approval_area IN lr_appr_area[]
        AND chg_category  IN mr_chgcat[]
        AND status        NE 'PO'.
    IF mt_approval[] IS INITIAL.
      RETURN.
    ENDIF.


    CLEAR: lt_appr_hdr[].
    LOOP AT mt_approval[] INTO ls_approval.
      CLEAR ls_appr_hdr.
      ls_appr_hdr-idno          = ls_approval-idno.
      ls_appr_hdr-chg_category  = ls_approval-chg_category.
      ls_appr_hdr-approval_area = ls_approval-approval_area.

      IF ls_approval-approval_area = 'NAT'.
        ls_appr_hdr-chg_area = 'N'.
      ELSEIF ls_approval-approval_area = 'REG'.
        ls_appr_hdr-chg_area = 'R'.
      ENDIF.

      INSERT ls_appr_hdr INTO TABLE lt_appr_hdr[].
    ENDLOOP.


    DELETE ADJACENT DUPLICATES FROM lt_appr_hdr[] COMPARING idno.



** 1) Get IDNOs based on National status first
** 2) Based on the IDNOs which matches the national status,
**    get Regional IDNOs matching the regional status
** 3) Get the IDNOs which matched the National and Regional Status


** 1) Get IDNOs based on National status first
* Read National Version Data with national Status
    SELECT a~idno a~version_status
           b~previous_ver b~current_ver b~latest_ver b~article_ver
           c~fan_id c~fsni_icare_value
      INTO CORRESPONDING FIELDS OF TABLE lt_ver_data[]
      FROM zarn_prd_version AS a
      JOIN zarn_ver_status AS b
        ON a~idno = b~idno
       AND a~version = b~current_ver
      JOIN zarn_products AS c
        ON c~idno    = b~idno
       AND c~version = b~current_ver
      FOR ALL ENTRIES IN lt_appr_hdr[]
     WHERE a~idno           EQ lt_appr_hdr-idno "IN mr_idno[]
       AND a~version_status IN mr_nstat[].


** 2) Based on the IDNOs which matches the national status,
**    get Regional IDNOs matching the regional status
    IF lt_ver_data[] IS NOT INITIAL.
* Read Regional Header Data
      SELECT idno version status matnr retail_unit_desc
        FROM zarn_reg_hdr
        INTO CORRESPONDING FIELDS OF TABLE mt_reg_hdr[]
        FOR ALL ENTRIES IN lt_ver_data[]
       WHERE idno   EQ lt_ver_data-idno
         AND status IN mr_rstat[].
    ENDIF.


** 3) Get the IDNOs which matched the National and Regional Status
* Get National Version Data with national Status for Regional data retreived
    IF mt_reg_hdr[] IS NOT INITIAL.
      SELECT a~idno a~version_status
             b~previous_ver b~current_ver b~latest_ver b~article_ver
             c~fan_id c~fsni_icare_value
        INTO CORRESPONDING FIELDS OF TABLE mt_ver_data[]
        FROM zarn_prd_version AS a
        JOIN zarn_ver_status AS b
          ON a~idno = b~idno
         AND a~version = b~current_ver
       JOIN zarn_products AS c
         ON c~idno    = b~idno
        AND c~version = b~current_ver
        FOR ALL ENTRIES IN mt_reg_hdr[]
       WHERE a~idno EQ mt_reg_hdr-idno.
    ENDIF. " IF mt_reg_hdr IS NOT INITIAL





    IF mt_ver_data[] IS NOT INITIAL.
      DELETE ADJACENT DUPLICATES FROM mt_ver_data[] COMPARING idno.

* Read Approvals
* only not posted are retreived, in GUI all the records are retreived but PO records are later deleted
* in mass, only <> PO are retreived
      SELECT * FROM zarn_approval
        INTO CORRESPONDING FIELDS OF TABLE mt_approval[]
        FOR ALL ENTRIES IN mt_ver_data[]
        WHERE idno          EQ mt_ver_data-idno
          AND approval_area IN lr_appr_area[]
          AND chg_category  IN mr_chgcat[]
          AND status        NE 'PO'.
      IF mt_approval[] IS NOT INITIAL.


        CLEAR: lt_appr_hdr[].
        LOOP AT mt_approval[] INTO ls_approval.
          CLEAR ls_appr_hdr.
          ls_appr_hdr-idno          = ls_approval-idno.
          ls_appr_hdr-chg_category  = ls_approval-chg_category.
          ls_appr_hdr-approval_area = ls_approval-approval_area.

          IF ls_approval-approval_area = 'NAT'.
            ls_appr_hdr-chg_area = 'N'.
          ELSEIF ls_approval-approval_area = 'REG'.
            ls_appr_hdr-chg_area = 'R'.
          ENDIF.

          INSERT ls_appr_hdr INTO TABLE lt_appr_hdr[].
        ENDLOOP.

        DELETE ADJACENT DUPLICATES FROM lt_appr_hdr[] COMPARING idno chg_area chg_category.

* Get Approval-Change Id link
        SELECT * FROM zarn_approval_ch
          INTO CORRESPONDING FIELDS OF TABLE mt_appr_ch[]
          FOR ALL ENTRIES IN mt_approval[]
          WHERE guid EQ mt_approval-guid.


* Read Change Category Header
        SELECT * FROM zarn_cc_hdr
          INTO CORRESPONDING FIELDS OF TABLE mt_cc_hdr[]
          FOR ALL ENTRIES IN lt_appr_hdr[]
          WHERE idno          EQ lt_appr_hdr-idno
            AND chg_area      EQ lt_appr_hdr-chg_area
            AND chg_category  EQ lt_appr_hdr-chg_category
            AND chg_area      IN mr_chgare[].


      ENDIF.  " IF mt_approval[] IS NOT INITIAL





* Get PRD Version data for all IDNOs as entered
      SELECT A~*
        INTO CORRESPONDING FIELDS OF TABLE @mt_prd_version[]
        FROM zarn_prd_version AS a
        JOIN zarn_ver_status AS b
          ON a~idno = b~idno
         AND a~version = b~current_ver
       WHERE a~idno IN @mr_idno[].

* Get Change Category Descriptions
      SELECT * FROM zarn_cc_desc
        INTO CORRESPONDING FIELDS OF TABLE mt_cc_desc[].

* Get Release Matrix
      SELECT * FROM zarn_rel_matrix
        INTO CORRESPONDING FIELDS OF TABLE mt_rel_matrix[].

* Get Table Master
      SELECT * FROM zarn_table_mastr
        INTO CORRESPONDING FIELDS OF TABLE mt_table_mastr[].

* Get Field Config
      SELECT * FROM zarn_field_confg
        INTO CORRESPONDING FIELDS OF TABLE mt_field_confg[].


    ENDIF.  " IF mt_ver_data[] IS NOT INITIAL

  ENDMETHOD.

ENDCLASS.               "lcl_model
