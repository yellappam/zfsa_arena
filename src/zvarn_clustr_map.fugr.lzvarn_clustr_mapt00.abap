*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 24.06.2020 at 13:53:36
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZVARN_CLUSTR_MAP................................*
TABLES: ZVARN_CLUSTR_MAP, *ZVARN_CLUSTR_MAP. "view work areas
CONTROLS: TCTRL_ZVARN_CLUSTR_MAP
TYPE TABLEVIEW USING SCREEN '9000'.
DATA: BEGIN OF STATUS_ZVARN_CLUSTR_MAP. "state vector
          INCLUDE STRUCTURE VIMSTATUS.
DATA: END OF STATUS_ZVARN_CLUSTR_MAP.
* Table for entries selected to show on screen
DATA: BEGIN OF ZVARN_CLUSTR_MAP_EXTRACT OCCURS 0010.
INCLUDE STRUCTURE ZVARN_CLUSTR_MAP.
          INCLUDE STRUCTURE VIMFLAGTAB.
DATA: END OF ZVARN_CLUSTR_MAP_EXTRACT.
* Table for all entries loaded from database
DATA: BEGIN OF ZVARN_CLUSTR_MAP_TOTAL OCCURS 0010.
INCLUDE STRUCTURE ZVARN_CLUSTR_MAP.
          INCLUDE STRUCTURE VIMFLAGTAB.
DATA: END OF ZVARN_CLUSTR_MAP_TOTAL.

*.........table declarations:.................................*
TABLES: ZARN_CLUSTER_MAP               .
