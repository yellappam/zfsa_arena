* regenerated at 25.05.2017 13:49:18
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_RL_COMPLEXTOP.               " Global Data
  INCLUDE LZARN_RL_COMPLEXUXX.               " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_RL_COMPLEXF...               " Subroutines
* INCLUDE LZARN_RL_COMPLEXO...               " PBO-Modules
* INCLUDE LZARN_RL_COMPLEXI...               " PAI-Modules
* INCLUDE LZARN_RL_COMPLEXE...               " Events
* INCLUDE LZARN_RL_COMPLEXP...               " Local class implement.
* INCLUDE LZARN_RL_COMPLEXT99.               " ABAP Unit tests
  INCLUDE LZARN_RL_COMPLEXF00                     . " subprograms
  INCLUDE LZARN_RL_COMPLEXI00                     . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
