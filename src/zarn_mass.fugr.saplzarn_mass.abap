*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_MASSTOP.                     " Global Data
  INCLUDE LZARN_MASSUXX.                     " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_MASSF...                     " Subroutines
* INCLUDE LZARN_MASSO...                     " PBO-Modules
* INCLUDE LZARN_MASSI...                     " PAI-Modules
* INCLUDE LZARN_MASSE...                     " Events
* INCLUDE LZARN_MASSP...                     " Local class implement.
* INCLUDE LZARN_MASST99.                     " ABAP Unit tests

INCLUDE lzarn_massf01.
