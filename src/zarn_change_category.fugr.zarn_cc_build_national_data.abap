FUNCTION zarn_cc_build_national_data.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(IT_TABLE_MASTR) TYPE  ZTARN_TABLE_MASTR
*"     REFERENCE(IS_PROD_DATA_DB) TYPE  ZSARN_PROD_DATA
*"     REFERENCE(IS_PROD_DATA_N) TYPE  ZSARN_PROD_DATA
*"     REFERENCE(IV_CHGID) TYPE  ZARN_CHG_ID
*"  EXPORTING
*"     REFERENCE(ET_CC_HDR) TYPE  ZTARN_CC_HDR
*"     REFERENCE(ET_CC_DET) TYPE  ZTARN_CC_DET
*"     REFERENCE(ET_APPROVAL) TYPE  ZARN_T_APPROVAL
*"     REFERENCE(ET_APPROVAL_CH) TYPE  ZARN_T_APPROVAL_CH
*"----------------------------------------------------------------------


  DATA: ls_table_mastr  TYPE zarn_table_mastr,
        ls_prod_data_n  TYPE zsarn_prod_data,
        ls_prod_data_db TYPE zsarn_prod_data,
        lv_itabname     TYPE char50,
        lt_cc_hdr       TYPE ztarn_cc_hdr,
        lt_cc_det       TYPE ztarn_cc_det.

*>>>IS1609ins Feature 3162 - AReNa Approval Stabilisation
  DATA  lo_approvals    TYPE REF TO zcl_arn_approval_backend.
*<<<IS1609ins Feature 3162 - AReNa Approval Stabilisation


  FIELD-SYMBOLS: <table_db> TYPE table,
                 <table_n>  TYPE table.

  CLEAR: et_cc_hdr[], et_cc_det[].

  ls_prod_data_n  = is_prod_data_n.
  ls_prod_data_db = is_prod_data_db.


  LOOP AT it_table_mastr INTO ls_table_mastr.

    IF <table_db>   IS ASSIGNED. UNASSIGN <table_db>.   ENDIF.
    IF <table_n>    IS ASSIGNED. UNASSIGN <table_n>.    ENDIF.

    CLEAR: lv_itabname.
    CONCATENATE 'LS_PROD_DATA_DB-' ls_table_mastr-arena_table INTO lv_itabname.
    ASSIGN (lv_itabname) TO <table_db>.

    CLEAR: lv_itabname.
    CONCATENATE 'LS_PROD_DATA_N-' ls_table_mastr-arena_table INTO lv_itabname.
    ASSIGN (lv_itabname) TO <table_n>.

    IF <table_n> IS NOT INITIAL AND ls_table_mastr-arena_table IS NOT INITIAL.
      CLEAR: lt_cc_hdr[], lt_cc_det[].
      CALL FUNCTION 'ZARN_CHANGE_CATEGORY_DATA'
        EXPORTING
          it_table_o     = <table_db>
          it_table_n     = <table_n>
          iv_tabname     = ls_table_mastr-arena_table
          iv_new_version = ls_prod_data_n-version
          iv_old_version = ls_prod_data_db-version
          iv_chgid       = iv_chgid
*         iv_chgdoc      =
          is_prod_data_n = ls_prod_data_n
          is_prod_data_o = ls_prod_data_db        " ++ONED-217 JKH 23.11.2016
*         is_reg_data_n  =
*         is_reg_data_o  =
        IMPORTING
          et_cc_hdr      = lt_cc_hdr[]
          et_cc_det      = lt_cc_det[]
        EXCEPTIONS
          input_missing  = 1
          input_invalid  = 1
          error          = 3
          OTHERS         = 4.

      APPEND LINES OF lt_cc_hdr[]  TO et_cc_hdr[].
      APPEND LINES OF lt_cc_det[]  TO et_cc_det[].
    ENDIF.

  ENDLOOP.  " LOOP AT it_table_mastr INTO ls_table_mastr



  SORT et_cc_hdr[] BY chgid idno chg_category chg_area.
  DELETE ADJACENT DUPLICATES FROM et_cc_hdr[] COMPARING chgid idno chg_category chg_area.


ENDFUNCTION.
