* regenerated at 28.07.2016 11:36:14
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_COO_TTOP.                    " Global Data
  INCLUDE LZARN_COO_TUXX.                    " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_COO_TF...                    " Subroutines
* INCLUDE LZARN_COO_TO...                    " PBO-Modules
* INCLUDE LZARN_COO_TI...                    " PAI-Modules
* INCLUDE LZARN_COO_TE...                    " Events
* INCLUDE LZARN_COO_TP...                    " Local class implement.
* INCLUDE LZARN_COO_TT99.                    " ABAP Unit tests
  INCLUDE LZARN_COO_TF00                          . " subprograms
  INCLUDE LZARN_COO_TI00                          . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
