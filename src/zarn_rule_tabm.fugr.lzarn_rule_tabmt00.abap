*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 27.09.2016 at 12:10:25
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_RL_CLOG_FLD................................*
DATA:  BEGIN OF STATUS_ZARN_RL_CLOG_FLD              .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_RL_CLOG_FLD              .
CONTROLS: TCTRL_ZARN_RL_CLOG_FLD
            TYPE TABLEVIEW USING SCREEN '0011'.
*...processing: ZARN_RL_EVENT...................................*
DATA:  BEGIN OF STATUS_ZARN_RL_EVENT                 .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_RL_EVENT                 .
CONTROLS: TCTRL_ZARN_RL_EVENT
            TYPE TABLEVIEW USING SCREEN '0009'.
*...processing: ZARN_RL_EV_VALV.................................*
TABLES: ZARN_RL_EV_VALV, *ZARN_RL_EV_VALV. "view work areas
CONTROLS: TCTRL_ZARN_RL_EV_VALV
TYPE TABLEVIEW USING SCREEN '0008'.
DATA: BEGIN OF STATUS_ZARN_RL_EV_VALV. "state vector
          INCLUDE STRUCTURE VIMSTATUS.
DATA: END OF STATUS_ZARN_RL_EV_VALV.
* Table for entries selected to show on screen
DATA: BEGIN OF ZARN_RL_EV_VALV_EXTRACT OCCURS 0010.
INCLUDE STRUCTURE ZARN_RL_EV_VALV.
          INCLUDE STRUCTURE VIMFLAGTAB.
DATA: END OF ZARN_RL_EV_VALV_EXTRACT.
* Table for all entries loaded from database
DATA: BEGIN OF ZARN_RL_EV_VALV_TOTAL OCCURS 0010.
INCLUDE STRUCTURE ZARN_RL_EV_VALV.
          INCLUDE STRUCTURE VIMFLAGTAB.
DATA: END OF ZARN_RL_EV_VALV_TOTAL.

*...processing: ZARN_RL_RULE....................................*
DATA:  BEGIN OF STATUS_ZARN_RL_RULE                  .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_RL_RULE                  .
CONTROLS: TCTRL_ZARN_RL_RULE
            TYPE TABLEVIEW USING SCREEN '0001'.
*...processing: ZARN_RL_RULE_RNG................................*
DATA:  BEGIN OF STATUS_ZARN_RL_RULE_RNG              .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_RL_RULE_RNG              .
CONTROLS: TCTRL_ZARN_RL_RULE_RNG
            TYPE TABLEVIEW USING SCREEN '0003'.
*...processing: ZARN_RL_RULE_RNV................................*
TABLES: ZARN_RL_RULE_RNV, *ZARN_RL_RULE_RNV. "view work areas
CONTROLS: TCTRL_ZARN_RL_RULE_RNV
TYPE TABLEVIEW USING SCREEN '0004'.
DATA: BEGIN OF STATUS_ZARN_RL_RULE_RNV. "state vector
          INCLUDE STRUCTURE VIMSTATUS.
DATA: END OF STATUS_ZARN_RL_RULE_RNV.
* Table for entries selected to show on screen
DATA: BEGIN OF ZARN_RL_RULE_RNV_EXTRACT OCCURS 0010.
INCLUDE STRUCTURE ZARN_RL_RULE_RNV.
          INCLUDE STRUCTURE VIMFLAGTAB.
DATA: END OF ZARN_RL_RULE_RNV_EXTRACT.
* Table for all entries loaded from database
DATA: BEGIN OF ZARN_RL_RULE_RNV_TOTAL OCCURS 0010.
INCLUDE STRUCTURE ZARN_RL_RULE_RNV.
          INCLUDE STRUCTURE VIMFLAGTAB.
DATA: END OF ZARN_RL_RULE_RNV_TOTAL.

*...processing: ZARN_RL_TABLE...................................*
DATA:  BEGIN OF STATUS_ZARN_RL_TABLE                 .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_RL_TABLE                 .
CONTROLS: TCTRL_ZARN_RL_TABLE
            TYPE TABLEVIEW USING SCREEN '0007'.
*...processing: ZARN_RL_VALID...................................*
DATA:  BEGIN OF STATUS_ZARN_RL_VALID                 .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_RL_VALID                 .
CONTROLS: TCTRL_ZARN_RL_VALID
            TYPE TABLEVIEW USING SCREEN '0005'.
*...processing: ZARN_RL_VALID_GR................................*
DATA:  BEGIN OF STATUS_ZARN_RL_VALID_GR              .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_RL_VALID_GR              .
CONTROLS: TCTRL_ZARN_RL_VALID_GR
            TYPE TABLEVIEW USING SCREEN '0010'.
*.........table declarations:.................................*
TABLES: *ZARN_RL_CLOG_FLD              .
TABLES: *ZARN_RL_EVENT                 .
TABLES: *ZARN_RL_EVENT_T               .
TABLES: *ZARN_RL_RULE                  .
TABLES: *ZARN_RL_RULE_RNG              .
TABLES: *ZARN_RL_RULE_T                .
TABLES: *ZARN_RL_TABLE                 .
TABLES: *ZARN_RL_TABLE_T               .
TABLES: *ZARN_RL_VALID                 .
TABLES: *ZARN_RL_VALIDGRT              .
TABLES: *ZARN_RL_VALID_GR              .
TABLES: *ZARN_RL_VALID_T               .
TABLES: ZARN_RL_CLOG_FLD               .
TABLES: ZARN_RL_EVENT                  .
TABLES: ZARN_RL_EVENT_T                .
TABLES: ZARN_RL_EV_VAL                 .
TABLES: ZARN_RL_RULE                   .
TABLES: ZARN_RL_RULE_RNG               .
TABLES: ZARN_RL_RULE_T                 .
TABLES: ZARN_RL_TABLE                  .
TABLES: ZARN_RL_TABLE_T                .
TABLES: ZARN_RL_VALID                  .
TABLES: ZARN_RL_VALIDGRT               .
TABLES: ZARN_RL_VALID_GR               .
TABLES: ZARN_RL_VALID_T                .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
