*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 19.07.2016 at 11:09:55
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_PROD_UOM_T.................................*
DATA:  BEGIN OF STATUS_ZARN_PROD_UOM_T               .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_PROD_UOM_T               .
CONTROLS: TCTRL_ZARN_PROD_UOM_T
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_PROD_UOM_T               .
TABLES: ZARN_PROD_UOM_T                .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
