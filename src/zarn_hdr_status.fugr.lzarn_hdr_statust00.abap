*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 23.02.2016 at 08:21:25 by user C90001448
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_HDR_STATUS.................................*
DATA:  BEGIN OF STATUS_ZARN_HDR_STATUS               .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_HDR_STATUS               .
CONTROLS: TCTRL_ZARN_HDR_STATUS
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_HDR_STATUS               .
TABLES: ZARN_HDR_STATUS                .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
