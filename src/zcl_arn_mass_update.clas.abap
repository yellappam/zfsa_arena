class ZCL_ARN_MASS_UPDATE definition
  public
  final
  create public .

public section.

  types:
    BEGIN OF ty_s_mass_fields,
        table TYPE tabname,
        field TYPE fieldname,
      END OF ty_s_mass_fields .
  types:
    ty_t_mass_fields TYPE SORTED TABLE OF ty_s_mass_fields WITH NON-UNIQUE KEY table field .
  types:
    ty_t_reg_key TYPE SORTED TABLE OF zsarn_reg_key WITH UNIQUE KEY idno .
  types:
    ty_t_key       TYPE SORTED TABLE OF zsarn_key     WITH UNIQUE KEY idno version .
* IDNO Data
  types TY_S_IDNO_DATA type ZSARN_IDNO_DATA .
  types:
    ty_t_idno_data TYPE SORTED TABLE OF ty_s_idno_data WITH UNIQUE KEY idno .
  types:
    ty_t_mara TYPE SORTED TABLE OF mara WITH UNIQUE KEY matnr .
  types:
    ty_t_marm TYPE SORTED TABLE OF marm WITH UNIQUE KEY matnr meinh .
  types:
    ty_t_marc TYPE SORTED TABLE OF marc WITH UNIQUE KEY matnr werks .
  types:
    ty_t_mean TYPE SORTED TABLE OF mean WITH UNIQUE KEY matnr meinh lfnum .
  types:
    ty_t_maw1 TYPE SORTED TABLE OF maw1 WITH UNIQUE KEY matnr .
  types:
    ty_t_eina TYPE SORTED TABLE OF eina WITH NON-UNIQUE KEY matnr lifnr .
  types:
    ty_t_eine TYPE SORTED TABLE OF eine WITH NON-UNIQUE KEY infnr ekorg .
  types:
    ty_t_mvke TYPE SORTED TABLE OF mvke WITH UNIQUE KEY matnr vkorg vtweg .
  types:
    ty_t_wlk2 TYPE SORTED TABLE OF wlk2 WITH UNIQUE KEY matnr vkorg vtweg .
  types:
    ty_t_ver_status  TYPE SORTED TABLE OF zarn_ver_status WITH UNIQUE KEY idno .
  types:
    ty_t_prd_version TYPE SORTED TABLE OF zarn_prd_version WITH UNIQUE KEY idno version .
  types:
* TVARVC EKORG
    ty_r_ekorg     TYPE RANGE OF ekorg .
  types:
* TVARVC VKORG
    ty_r_vkorg     TYPE RANGE OF vkorg .
* Units of Measurement
  types TY_S_T006 type T006 .
  types:
    ty_t_t006      TYPE SORTED TABLE OF ty_s_t006 WITH UNIQUE KEY msehi .
* Countries
  types TY_S_T005 type T005 .
  types:
    ty_t_t005      TYPE SORTED TABLE OF ty_s_t005 WITH UNIQUE KEY land1 .
* Currency
  types TY_S_TCURC type TCURC .
  types:
    ty_t_tcurc     TYPE SORTED TABLE OF ty_s_tcurc WITH UNIQUE KEY waers .
* Article Types
  types TY_S_T134 type T134 .
  types:
    ty_t_t134      TYPE SORTED TABLE OF ty_s_t134 WITH NON-UNIQUE KEY mtart .
* UOM Category
  types TY_S_UOM_CAT type ZARN_UOM_CAT .
  types:
    ty_t_uom_cat   TYPE SORTED TABLE OF ty_s_uom_cat WITH UNIQUE KEY uom_category seqno uom .
  types:
* Country of Origin Text
    ty_t_coo_t     TYPE SORTED TABLE OF zarn_coo_t WITH UNIQUE KEY country_of_origin spras .
  types:
    ty_t_gen_uom_t TYPE SORTED TABLE OF zarn_gen_uom_t WITH UNIQUE KEY gen_uom spras .
* Host Product
  types TY_S_HOST_SAP type ZMD_HOST_SAP .
  types:
    ty_t_host_sap  TYPE SORTED TABLE OF ty_s_host_sap WITH UNIQUE KEY matnr product .

  data MS_REG_DATA_MASS type ZSARN_REG_DATA .
  data MS_REG_DATA_DB_MASS type ZSARN_REG_DATA .
  data MT_PROD_DATA_DB type ZTARN_PROD_DATA .
  data MT_REG_DATA_DB type ZTARN_REG_DATA .
  data MT_REG_DATA_MASS type ZTARN_REG_DATA .
  data MT_REG_DATA_DB_MASS type ZTARN_REG_DATA .
  data MT_REG_DATA_SAVE type ZTARN_REG_DATA .
  data MT_MASS_FIELDS type TY_T_MASS_FIELDS .
  data MT_IDNO_DATA type TY_T_IDNO_DATA .
  data MV_INITIALIZED type FLAG .
  data MV_EKORG_MAIN type EKORG .
  data MC_VKORG_UNI type RVARI_VNAM value 'ZC_REF_VKORG_VTREFS' ##NO_TEXT.
  data MC_VKORG_LNI type RVARI_VNAM value 'ZMD_VKORG_REF_SITES_LNI' ##NO_TEXT.
  data MC_EINE_EKORG type RVARI_VNAM value 'ZARN_ARTICLE_POST_EINE_EKORG' ##NO_TEXT.
  data MC_UOM_HYB_CODE type RVARI_VNAM value 'ZARN_GUI_UOM_ADDITION_SCENARIO' ##NO_TEXT.
  data MC_TYPE_S type RSSCR_KIND value 'S' ##NO_TEXT.
  data MC_TYPE_P type RSSCR_KIND value 'P' ##NO_TEXT.
  data MC_BADI_CALLMODE type WRFCALLMODE value '14' ##NO_TEXT.
  data MC_LOCK_REG type EQEGRANAME value 'ZSARN_REGIONAL_LOCK' ##NO_TEXT.
  data MT_MARA type TY_T_MARA .
  data MT_MARM type TY_T_MARM .
  data MT_MARC type TY_T_MARC .
  data MT_MEAN type TY_T_MEAN .
  data MT_EINA type TY_T_EINA .
  data MT_EINE type TY_T_EINE .
  data MT_MAW1 type TY_T_MAW1 .
  data MT_MVKE type TY_T_MVKE .
  data MT_WLK2 type TY_T_WLK2 .
  data MT_HOST_SAP type TY_T_HOST_SAP .
  data MT_T006 type TY_T_T006 .
  data MT_T005 type TY_T_T005 .
  data MT_TCURC type TY_T_TCURC .
  data MT_COO_T type TY_T_COO_T .
  data MT_GEN_UOM_T type TY_T_GEN_UOM_T .
  data MT_T134 type TY_T_T134 .
  data MT_TVARVC_EKORG type TVARVC_T .
  data MT_TVARVC type TVARVC_T .
  data MT_TVARVC_HYB_CODE type TVARVC_T .
  data MT_TVTA type TVTA_TT .
  data MT_UOM_CAT type TY_T_UOM_CAT .
  data MT_MESSAGE type BAL_T_MSG .
  data MT_MSG type MASS_MSGS .

  methods REGIONAL_MASS_UPDATE_PROCESS
    importing
      !IT_SELDATA type MASS_TABDATA
      value(IV_TESTMODE) type FLAG default SPACE
      !IV_NO_CHG_DOC type FLAG default SPACE
    exporting
      !ET_MSG type MASS_MSGS
      !ET_MESSAGE type BAL_T_MSG
    changing
      !CT_REG_BANNER type ZTARN_REG_BANNER optional
      !CT_REG_DC_SELL type ZTARN_REG_DC_SELL optional
      !CT_REG_EAN type ZTARN_REG_EAN optional
      !CT_REG_HDR type ZTARN_REG_HDR optional
      !CT_REG_LST_PRC type ZTARN_REG_LST_PRC optional
      !CT_REG_PIR type ZTARN_REG_PIR optional
      !CT_REG_PRFAM type ZTARN_REG_PRFAM optional
      !CT_REG_RRP type ZTARN_REG_RRP optional
      !CT_REG_STD_TER type ZTARN_REG_STD_TER optional
      !CT_REG_TXT type ZTARN_REG_TXT optional
      !CT_REG_UOM type ZTARN_REG_UOM optional
      !CT_REG_CLUSTER type ZTARN_REG_CLUSTER optional
    exceptions
      ERROR .
  methods MAINTAIN_REGIONAL_DATA
    importing
      !IS_REG_DATA_DB type ZSARN_REG_DATA
      !IS_REG_DATA_MASS type ZSARN_REG_DATA
      !IT_REG_KEY type TY_T_REG_KEY
      !IV_NO_CHG_DOC type FLAG default SPACE .
  methods CHECK_IDNO_LOCK
    importing
      !IV_IDNO type ZARN_IDNO
    exporting
      !EV_LOCKED type FLAG
      !EV_USER type SY-UNAME
      !EV_USERNAME type AD_NAMTEXT
    changing
      !CT_MSG type MASS_MSGS .
  methods LOCK_IDNO_REGIONAL
    importing
      !IV_IDNO type ZARN_IDNO
    changing
      !CT_MSG type MASS_MSGS
      !CV_ERROR type FLAG .
  methods UNLOCK_IDNO_REGIONAL
    importing
      !IV_IDNO type ZARN_IDNO .
  methods MAINTAIN_BAL_LOG
    importing
      !IT_MESSAGE type BAL_T_MSG
      !IV_DISPLAY_LOG type BOOLE_D default 'X'
    exporting
      !EV_LOG_NO type BALOGNR .
  methods CONSTRUCTOR
    importing
      !IV_IMPORTANT_MSGS_ONLY type ABAP_BOOL default ABAP_FALSE .
protected section.
private section.

  types:
    BEGIN OF ts_id_banner,
           idno   TYPE zarn_idno,
           banner TYPE vkorg,
         END   OF ts_id_banner .
  types:
    tt_id_banner TYPE STANDARD TABLE OF ts_id_banner WITH DEFAULT KEY .

  data MS_REG_DATA_MASS_DELETE type ZSARN_REG_DATA .
  data MV_ONLY_IMPORTANT_MESSAGES type ABAP_BOOL value ABAP_FALSE ##NO_TEXT.
  constants GC_DEFAULT_WEIGHT_UOM type ZARN_NET_WEIGHT_UOM value 'KGM' ##NO_TEXT.
  constants GC_DEFAULT_DIM_UOM type ZARN_DEPTH_UOM value 'MMT' ##NO_TEXT.
  data MO_MESSAGE_SERVICE type ref to ZCL_MESSAGE_SERVICES .

  methods SAVE_AND_DISPLAY_BAL_LOG
    importing
      !IS_LOG_HANDLE type BALLOGHNDL
      !IV_SAVE type BOOLE_D default 'X'
      !IV_DISPLAY type BOOLE_D default 'X'
    changing
      !CV_LOG_NO type BALOGNR .
  methods GET_MASS_TABLE_FIELDS
    importing
      !IT_SELDATA type MASS_TABDATA
    exporting
      !ET_MASS_FIELDS type TY_T_MASS_FIELDS .
  methods BUILD_REGIONAL_MASS_DATA
    importing
      !IT_MASS_FIELDS type TY_T_MASS_FIELDS
      !IT_REG_BANNER type ZTARN_REG_BANNER optional
      !IT_REG_DC_SELL type ZTARN_REG_DC_SELL optional
      !IT_REG_EAN type ZTARN_REG_EAN optional
      !IT_REG_HDR type ZTARN_REG_HDR optional
      !IT_REG_LST_PRC type ZTARN_REG_LST_PRC optional
      !IT_REG_PIR type ZTARN_REG_PIR optional
      !IT_REG_PRFAM type ZTARN_REG_PRFAM optional
      !IT_REG_RRP type ZTARN_REG_RRP optional
      !IT_REG_STD_TER type ZTARN_REG_STD_TER optional
      !IT_REG_TXT type ZTARN_REG_TXT optional
      !IT_REG_UOM type ZTARN_REG_UOM optional
      !IT_REG_CLUSTER type ZTARN_REG_CLUSTER optional
    exporting
      !ET_REG_KEY type TY_T_REG_KEY
      !ES_REG_DATA_MASS type ZSARN_REG_DATA
      !ES_REG_DATA_MASS_DELETE type ZSARN_REG_DATA .
  methods REGIONAL_CHANGE_DOC_UPDATE
    importing
      !IS_REG_DATA_OLD type ZSARN_REG_DATA
      !IS_REG_DATA_NEW type ZSARN_REG_DATA
      !IT_REG_KEY type TY_T_REG_KEY
      !IT_REG_DATA_OLD type ZTARN_REG_DATA optional
      !IT_REG_DATA_NEW type ZTARN_REG_DATA optional
      !IV_BULK_MODE type FLAG default 'X' .
  methods INITIALIZE .
  methods GET_NAT_REG_DATA
    importing
      !IS_REG_DATA_MASS type ZSARN_REG_DATA
      !IT_REG_DATA_MASS type ZTARN_REG_DATA
      !IT_REG_KEY type TY_T_REG_KEY
    exporting
      !ET_KEY type TY_T_KEY
      !ES_PROD_DATA_ALL type ZSARN_PROD_DATA
      !ES_REG_DATA_ALL type ZSARN_REG_DATA
      !ET_PROD_DATA_DB type ZTARN_PROD_DATA
      !ET_REG_DATA_DB type ZTARN_REG_DATA
      !ET_REG_DATA_SAVE type ZTARN_REG_DATA
      !ET_VER_STATUS type TY_T_VER_STATUS
      !ET_PRD_VERSION type TY_T_PRD_VERSION .
  methods GET_REGIONAL_DB_DATA
    importing
      !IT_MASS_FIELDS type TY_T_MASS_FIELDS optional
      !IT_REG_BANNER type ZTARN_REG_BANNER optional
      !IT_REG_DC_SELL type ZTARN_REG_DC_SELL optional
      !IT_REG_EAN type ZTARN_REG_EAN optional
      !IT_REG_HDR type ZTARN_REG_HDR optional
      !IT_REG_LST_PRC type ZTARN_REG_LST_PRC optional
      !IT_REG_PIR type ZTARN_REG_PIR optional
      !IT_REG_PRFAM type ZTARN_REG_PRFAM optional
      !IT_REG_RRP type ZTARN_REG_RRP optional
      !IT_REG_STD_TER type ZTARN_REG_STD_TER optional
      !IT_REG_TXT type ZTARN_REG_TXT optional
      !IT_REG_UOM type ZTARN_REG_UOM optional
      !IT_REG_KEY type TY_T_REG_KEY
      !IT_REG_CLUSTER type ZTARN_REG_CLUSTER optional
      !IS_REG_DATA_MASS_DELETE type ZSARN_REG_DATA
    exporting
      !ES_REG_DATA_DB_MASS type ZSARN_REG_DATA
      !ET_REG_DATA_MASS type ZTARN_REG_DATA
      !ET_REG_DATA_DB_MASS type ZTARN_REG_DATA
    changing
      !CS_REG_DATA_MASS type ZSARN_REG_DATA .
  methods BUILD_MASS_MAPPING_DATA
    importing
      !IS_IDNO_DATA type TY_S_IDNO_DATA
      !IT_MASS_FIELDS type TY_T_MASS_FIELDS
      !IS_PROD_DATA type ZSARN_PROD_DATA
      !IS_REG_DATA type ZSARN_REG_DATA
      !IS_REG_DATA_MASS type ZSARN_REG_DATA
    exporting
      !EV_POST type FLAG
    changing
      !CS_HEADDATA type BAPIE1MATHEAD
      !CS_PIR_KEY type ZSARN_KEY
      !CT_CLIENTDATA type BAPIE1MARART_TAB
      !CT_CLIENTDATAX type BAPIE1MARARTX_TAB
      !CT_CLIENTEXT type BAPIE1MARAEXTRT_TAB
      !CT_CLIENTEXTX type BAPIE1MARAEXTRTX_TAB
      !CT_ADDNLCLIENTDATA type BAPIE1MAW1RT_TAB
      !CT_ADDNLCLIENTDATAX type BAPIE1MAW1RTX_TAB
      !CT_MATERIALDESCRIPTION type BAPIE1MAKTRT_TAB
      !CT_UNITSOFMEASURE type BAPIE1MARMRT_TAB
      !CT_UNITSOFMEASUREX type BAPIE1MARMRTX_TAB
      !CT_INTERNATIONALARTNOS type BAPIE1MEANRT_TAB
      !CT_PLANTDATA type BAPIE1MARCRT_TAB
      !CT_PLANTDATAX type BAPIE1MARCRTX_TAB
      !CT_PLANTEXT type BAPIE1MARCEXTRT_TAB
      !CT_PLANTEXTX type BAPIE1MARCEXTRTX_TAB
      !CT_VALUATIONDATA type BAPIE1MBEWRT_TAB
      !CT_VALUATIONDATAX type BAPIE1MBEWRTX_TAB
      !CT_WAREHOUSENUMBERDATA type BAPIE1MLGNRT_TAB
      !CT_WAREHOUSENUMBERDATAX type BAPIE1MLGNRTX_TAB
      !CT_SALESDATA type BAPIE1MVKERT_TAB
      !CT_SALESDATAX type BAPIE1MVKERTX_TAB
      !CT_SALESEXT type BAPIE1MVKEEXTRT_TAB
      !CT_SALESEXTX type BAPIE1MVKEEXTRTX_TAB
      !CT_POSDATA type BAPIE1WLK2RT_TAB
      !CT_POSDATAX type BAPIE1WLK2RTX_TAB
      !CT_PLANTKEYS type BAPIE1WRKKEY_TAB
      !CT_STORAGELOCATIONKEYS type BAPIE1LGOKEY_TAB
      !CT_DISTRCHAINKEYS type BAPIE1VTLKEY_TAB
      !CT_VALUATIONTYPEKEYS type BAPIE1BWAKEY_TAB
      !CT_IMPORTEXTENSION type WRF_BAPIPARMATEX_TTY
      !CT_INFORECORD_GENERAL type WRF_BAPIEINA_TTY optional
      !CT_INFORECORD_PURCHORG type WRF_BAPIEINE_TTY optional
      !CT_CALP_VB type CALP_VB_TAB
      !CV_ERROR type FLAG
      !CT_MSG type MASS_MSGS .
  methods POST_CONDITIONS
    importing
      !IT_CALP_VB type CALP_VB_TAB
      !IT_INFORECORD_GENERAL type WRF_BAPIEINA_TTY
      !IS_IDNO_DATA type TY_S_IDNO_DATA
    changing
      !CT_MSG type MASS_MSGS
      !CV_ERROR type FLAG .
  methods POST_MASS_DATA
    importing
      !IS_IDNO_DATA type TY_S_IDNO_DATA
      !IT_MASS_FIELDS type TY_T_MASS_FIELDS
      !IS_PROD_DATA type ZSARN_PROD_DATA
      !IS_REG_DATA type ZSARN_REG_DATA
      !IS_REG_DATA_MASS type ZSARN_REG_DATA
    changing
      !CS_HEADDATA type BAPIE1MATHEAD
      !CS_PIR_KEY type ZSARN_KEY
      !CT_CLIENTDATA type BAPIE1MARART_TAB
      !CT_CLIENTDATAX type BAPIE1MARARTX_TAB
      !CT_CLIENTEXT type BAPIE1MARAEXTRT_TAB
      !CT_CLIENTEXTX type BAPIE1MARAEXTRTX_TAB
      !CT_ADDNLCLIENTDATA type BAPIE1MAW1RT_TAB
      !CT_ADDNLCLIENTDATAX type BAPIE1MAW1RTX_TAB
      !CT_MATERIALDESCRIPTION type BAPIE1MAKTRT_TAB
      !CT_UNITSOFMEASURE type BAPIE1MARMRT_TAB
      !CT_UNITSOFMEASUREX type BAPIE1MARMRTX_TAB
      !CT_INTERNATIONALARTNOS type BAPIE1MEANRT_TAB
      !CT_PLANTDATA type BAPIE1MARCRT_TAB
      !CT_PLANTDATAX type BAPIE1MARCRTX_TAB
      !CT_PLANTEXT type BAPIE1MARCEXTRT_TAB
      !CT_PLANTEXTX type BAPIE1MARCEXTRTX_TAB
      !CT_VALUATIONDATA type BAPIE1MBEWRT_TAB
      !CT_VALUATIONDATAX type BAPIE1MBEWRTX_TAB
      !CT_WAREHOUSENUMBERDATA type BAPIE1MLGNRT_TAB
      !CT_WAREHOUSENUMBERDATAX type BAPIE1MLGNRTX_TAB
      !CT_SALESDATA type BAPIE1MVKERT_TAB
      !CT_SALESDATAX type BAPIE1MVKERTX_TAB
      !CT_SALESEXT type BAPIE1MVKEEXTRT_TAB
      !CT_SALESEXTX type BAPIE1MVKEEXTRTX_TAB
      !CT_POSDATA type BAPIE1WLK2RT_TAB
      !CT_POSDATAX type BAPIE1WLK2RTX_TAB
      !CT_PLANTKEYS type BAPIE1WRKKEY_TAB
      !CT_STORAGELOCATIONKEYS type BAPIE1LGOKEY_TAB
      !CT_DISTRCHAINKEYS type BAPIE1VTLKEY_TAB
      !CT_VALUATIONTYPEKEYS type BAPIE1BWAKEY_TAB
      !CT_IMPORTEXTENSION type WRF_BAPIPARMATEX_TTY
      !CT_INFORECORD_GENERAL type WRF_BAPIEINA_TTY optional
      !CT_INFORECORD_PURCHORG type WRF_BAPIEINE_TTY optional
      !CT_CALP_VB type CALP_VB_TAB
      !CT_MSG type MASS_MSGS
      !CV_ERROR type FLAG
      !CT_RETURN type WTY_BAPIRETURN1_TAB
      !CV_MAIN_DATA_ERROR type BOOLEAN
      !CV_ADDITIONAL_DATA_ERROR type BOOLEAN .
  methods GET_SAP_DATA
    importing
      !IT_KEY type TY_T_KEY
      !IS_PROD_DATA_ALL type ZSARN_PROD_DATA
      !IS_REG_DATA_ALL type ZSARN_REG_DATA
      !IT_VER_STATUS type TY_T_VER_STATUS
      !IT_PRD_VERSION type TY_T_PRD_VERSION
    exporting
      !ET_IDNO_DATA type TY_T_IDNO_DATA .
  methods VALIDATE_MASS_DATA
    importing
      !IS_IDNO_DATA type TY_S_IDNO_DATA optional
      !IS_PROD_DATA type ZSARN_PROD_DATA
      !IS_REG_DATA type ZSARN_REG_DATA
      !IT_MASS_FIELDS type TY_T_MASS_FIELDS
    changing
      !CT_MSG type MASS_MSGS
      !CV_ERROR type FLAG .
  methods REGIONAL_DATA_UPDATE
    importing
      !IS_REG_DATA_DB type ZSARN_REG_DATA
      !IS_REG_DATA_MASS type ZSARN_REG_DATA
      !IS_REG_DATA_MASS_DELETE type ZSARN_REG_DATA optional .
  methods MODIFY_REG_DATA_MASS_UOM
    importing
      !IT_REG_UOM_DB type ZTARN_REG_UOM
    changing
      !CT_REG_UOM_MASS type ZTARN_REG_UOM .
  methods MASSAGE_BEFORE_POST
    changing
      !CT_REG_DATA_MASS type ZTARN_REG_DATA
      !CT_REG_DATA type ZTARN_REG_DATA .
  methods UPDATE_INFO_RECORDS
    importing
      !IT_REG_PIR type ZTARN_REG_PIR
      !IS_IDNO_DATA type TY_S_IDNO_DATA
      !IT_MASS_FIELDS type TY_T_MASS_FIELDS
    changing
      !CT_MSG type MASS_MSGS optional .
  methods ADD_MESSAGES
    importing
      !IT_MESSAGE type BAPIRET2_T optional
    changing
      !CT_MSG type MASS_MSGS .
  methods FILTER_REG_HEADER_DATA
    importing
      !IS_IDNO_DATA type TY_S_IDNO_DATA
      !IT_MASS_FIELDS type TY_T_MASS_FIELDS
      !IS_PROD_DATA type ZSARN_PROD_DATA
      !IS_REG_DATA type ZSARN_REG_DATA
      !IS_REG_DATA_MASS type ZSARN_REG_DATA
      !IT_MARA type TY_T_MARA
    exporting
      !EV_POST type FLAG
    changing
      !CT_CLIENTDATA type BAPIE1MARART_TAB
      !CT_CLIENTDATAX type BAPIE1MARARTX_TAB
      !CT_CLIENTEXT type BAPIE1MARAEXTRT_TAB
      !CT_CLIENTEXTX type BAPIE1MARAEXTRTX_TAB
      !CT_ADDNLCLIENTDATA type BAPIE1MAW1RT_TAB
      !CT_ADDNLCLIENTDATAX type BAPIE1MAW1RTX_TAB
      !CT_MATERIALDESCRIPTION type BAPIE1MAKTRT_TAB
      !CT_PLANTDATA type BAPIE1MARCRT_TAB
      !CT_PLANTDATAX type BAPIE1MARCRTX_TAB
      !CT_MSG type MASS_MSGS optional .
  methods FILTER_REG_UOM_DATA
    importing
      !IS_IDNO_DATA type TY_S_IDNO_DATA
      !IT_MASS_FIELDS type TY_T_MASS_FIELDS
      !IS_REG_DATA_MASS type ZSARN_REG_DATA
      !IT_MARM type TY_T_MARM
      !IT_MEAN type TY_T_MEAN
    exporting
      !EV_POST type FLAG
    changing
      !CT_UNITSOFMEASURE type BAPIE1MARMRT_TAB
      !CT_UNITSOFMEASUREX type BAPIE1MARMRTX_TAB
      !CT_CLIENTDATA type BAPIE1MARART_TAB
      !CT_CLIENTDATAX type BAPIE1MARARTX_TAB
      !CT_ADDNLCLIENTDATA type BAPIE1MAW1RT_TAB
      !CT_ADDNLCLIENTDATAX type BAPIE1MAW1RTX_TAB
      !CT_MSG type MASS_MSGS optional .
  methods FILTER_REG_BANNER_DATA
    importing
      !IS_IDNO_DATA type TY_S_IDNO_DATA
      !IT_MASS_FIELDS type TY_T_MASS_FIELDS
      !IS_PROD_DATA type ZSARN_PROD_DATA
      !IS_REG_DATA type ZSARN_REG_DATA
      !IS_REG_DATA_MASS type ZSARN_REG_DATA
      !IT_MARC type TY_T_MARC
      !IT_MVKE type TY_T_MVKE
      !IT_WLK2 type TY_T_WLK2
      !IT_HOST_SAP type TY_T_HOST_SAP
      !IT_TVARVC type TVARVC_T
      !IT_TVTA type TVTA_TT
    exporting
      !EV_POST type FLAG
    changing
      !CT_PLANTDATA type BAPIE1MARCRT_TAB
      !CT_PLANTDATAX type BAPIE1MARCRTX_TAB
      !CT_PLANTEXT type BAPIE1MARCEXTRT_TAB
      !CT_PLANTEXTX type BAPIE1MARCEXTRTX_TAB
      !CT_VALUATIONDATA type BAPIE1MBEWRT_TAB
      !CT_VALUATIONDATAX type BAPIE1MBEWRTX_TAB
      !CT_WAREHOUSENUMBERDATA type BAPIE1MLGNRT_TAB
      !CT_WAREHOUSENUMBERDATAX type BAPIE1MLGNRTX_TAB
      !CT_SALESDATA type BAPIE1MVKERT_TAB
      !CT_SALESDATAX type BAPIE1MVKERTX_TAB
      !CT_SALESEXT type BAPIE1MVKEEXTRT_TAB
      !CT_SALESEXTX type BAPIE1MVKEEXTRTX_TAB
      !CT_POSDATA type BAPIE1WLK2RT_TAB
      !CT_POSDATAX type BAPIE1WLK2RTX_TAB
      !CT_PLANTKEYS type BAPIE1WRKKEY_TAB
      !CT_STORAGELOCATIONKEYS type BAPIE1LGOKEY_TAB
      !CT_DISTRCHAINKEYS type BAPIE1VTLKEY_TAB
      !CT_VALUATIONTYPEKEYS type BAPIE1BWAKEY_TAB
      !CT_IMPORTEXTENSION type WRF_BAPIPARMATEX_TTY
      !CT_MSG type MASS_MSGS optional .
  methods FILTER_REG_PIR_DATA
    importing
      !IS_IDNO_DATA type TY_S_IDNO_DATA
      !IT_MASS_FIELDS type TY_T_MASS_FIELDS
      !IS_PROD_DATA type ZSARN_PROD_DATA
      !IS_REG_DATA type ZSARN_REG_DATA
      !IS_REG_DATA_MASS type ZSARN_REG_DATA
      !IT_EINA type TY_T_EINA
      !IT_EINE type TY_T_EINE
      !IT_MARM type TY_T_MARM
      !IT_T006 type TY_T_T006
      !IT_TCURC type TY_T_TCURC
    exporting
      !EV_POST type FLAG
    changing
      !CT_INFORECORD_GENERAL type WRF_BAPIEINA_TTY
      !CT_INFORECORD_PURCHORG type WRF_BAPIEINE_TTY
      !CT_CALP_VB type CALP_VB_TAB
      !CT_CLIENTDATA type BAPIE1MARART_TAB
      !CT_CLIENTDATAX type BAPIE1MARARTX_TAB
      !CT_ADDNLCLIENTDATA type BAPIE1MAW1RT_TAB
      !CT_ADDNLCLIENTDATAX type BAPIE1MAW1RTX_TAB
      !CT_MSG type MASS_MSGS optional .
  methods BUILD_BAL_MESSAGE
    importing
      !IS_IDNO_DATA type TY_S_IDNO_DATA
      !IT_MSG type MASS_MSGS .
  methods IS_INFO_RECORD_CHG_RELEVANT
    importing
      !IV_ARTICLE type MATNR
      !IT_MASS_FIELDS type TY_T_MASS_FIELDS
    returning
      value(RV_RELEVANT) type BOOLEAN_FLG .
  methods ADD_BAL_MESSAGE
    importing
      !IS_IDNO_DATA type TY_S_IDNO_DATA .
  methods SAVE_POST_MASS_DATA
    importing
      !IT_IDNO_DATA type TY_T_IDNO_DATA
      !IT_MASS_FIELDS type TY_T_MASS_FIELDS
      !IT_PROD_DATA type ZTARN_PROD_DATA
      !IT_REG_DATA type ZTARN_REG_DATA
      !IT_REG_DATA_MASS type ZTARN_REG_DATA
      !IT_REG_DATA_DB type ZTARN_REG_DATA
      !IT_REG_KEY type TY_T_REG_KEY
      !IV_TESTMODE type FLAG default SPACE
      !IV_NO_CHG_DOC type FLAG default SPACE
      !IS_REG_DATA_MASS_DELETE type ZSARN_REG_DATA optional
    changing
      !CT_MSG type MASS_MSGS .
  methods POST_LAYOUT_MODULE
    importing
      !IT_REG_CLUSTER_MASS type ZTARN_REG_CLUSTER
      !IV_ARTICLE type MATNR
    changing
      !CT_MESSAGE type MASS_MSGS .
  methods SAVE_MASS_DATA
    importing
      !IS_IDNO_DATA type TY_S_IDNO_DATA
      !IS_REG_DATA type ZSARN_REG_DATA
      !IS_REG_DATA_MASS type ZSARN_REG_DATA
      !IT_REG_KEY type TY_T_REG_KEY
      !IV_NO_CHG_DOC type FLAG default SPACE
      !IT_MASS_FIELDS type TY_T_MASS_FIELDS
      !IS_REG_DATA_MASS_DELETE type ZSARN_REG_DATA optional
    changing
      !CT_MSG type MASS_MSGS .
  methods BUILD_REGIONAL_MASS_DELETE
    importing
      !IT_MASS_FIELDS type TY_T_MASS_FIELDS
      !IT_REG_BANNER type ZTARN_REG_BANNER
    exporting
      !ES_REG_DATA_MASS_DELETE type ZSARN_REG_DATA .
  methods BUILD_MAP_DATA_AND_POST_MASS
    importing
      !IS_IDNO_DATA type TY_S_IDNO_DATA
      !IT_MASS_FIELDS type TY_T_MASS_FIELDS
      !IS_PROD_DATA type ZSARN_PROD_DATA
      !IS_REG_DATA type ZSARN_REG_DATA
      !IS_REG_DATA_DB type ZSARN_REG_DATA
      !IS_REG_DATA_MASS type ZSARN_REG_DATA
      !IS_REG_DATA_MASS_DELETE type ZSARN_REG_DATA optional
    changing
      !CT_MSG type MASS_MSGS
      !CV_ERROR type FLAG .
ENDCLASS.



CLASS ZCL_ARN_MASS_UPDATE IMPLEMENTATION.


METHOD add_bal_message.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 29.05.2020
* SYSTEM           # DE0
* SPECIFICATION    # SSM-8: SSM-8 Symphony -ZARN_MASS
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Add BAL message
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  DATA: lt_mass_message TYPE mass_msgs.

  APPEND INITIAL LINE TO lt_mass_message ASSIGNING FIELD-SYMBOL(<ls_mass_message>).
  MOVE-CORRESPONDING syst TO <ls_mass_message>.
  <ls_mass_message>-objkey = zif_mass_message_object_key=>gc_arena.

  me->build_bal_message( is_idno_data = is_idno_data
                         it_msg       = lt_mass_message ).

ENDMETHOD.


METHOD add_messages.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 06.12.2018
* SYSTEM           # DE0
* SPECIFICATION    # 9000004644: SAPTR-147 Defect649 ZARN_MASS PIR Update
* SAP VERSION      # SAP_BASIS  750 0011
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Add message(s)
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  DATA: lt_message TYPE bapiret2_t.

  IF it_message IS NOT INITIAL.
    lt_message = it_message.
  ELSE.
    zcl_message_services=>add_message_to_bapiret_tab( CHANGING ct_return = lt_message ).
  ENDIF.

  LOOP AT lt_message INTO DATA(ls_message).
    APPEND INITIAL LINE TO ct_msg ASSIGNING FIELD-SYMBOL(<ls_msg>).
    <ls_msg>-objkey = zif_mass_message_object_key=>gc_arena.
    <ls_msg>-msgty  = ls_message-type.
    <ls_msg>-msgid  = ls_message-id.
    <ls_msg>-msgno  = ls_message-number.
    <ls_msg>-msgv1  = ls_message-message_v1.
    <ls_msg>-msgv2  = ls_message-message_v2.
    <ls_msg>-msgv3  = ls_message-message_v3.
    <ls_msg>-msgv4  = ls_message-message_v4.
  ENDLOOP.

ENDMETHOD.


  METHOD build_bal_message.

    DATA: ls_idno_data     TYPE ty_s_idno_data,
          lt_msg           TYPE mass_msgs,
          ls_msg           TYPE massmsg,
          ls_message       TYPE bal_s_msg,
          ls_log_params    TYPE zsarn_slg1_log_params_art_post,
          lv_context_value TYPE string.

    ls_idno_data = is_idno_data.
    lt_msg[]     = it_msg[].

    IF lt_msg[] IS INITIAL.
      RETURN.
    ENDIF.

    APPEND LINES OF lt_msg[] TO mt_msg[].


* Build Return Message
    CLEAR: ls_log_params.
    ls_log_params-idno        = ls_idno_data-idno.
    ls_log_params-fanid       = ls_idno_data-fan_id.
    ls_log_params-version     = ls_idno_data-current_ver.
    ls_log_params-nat_status  = ls_idno_data-nat_status.
    ls_log_params-reg_status  = ls_idno_data-reg_status.
    ls_log_params-saparticle  = ls_idno_data-sap_article.


    LOOP AT lt_msg[] INTO ls_msg.

      CLEAR ls_message.
      ls_message-msgty  = ls_msg-msgty.
      ls_message-msgid  = ls_msg-msgid.
      ls_message-msgno  = ls_msg-msgno.
      ls_message-msgv1  = ls_msg-msgv1.
      ls_message-msgv2  = ls_msg-msgv2.
      ls_message-msgv3  = ls_msg-msgv3.
      ls_message-msgv4  = ls_msg-msgv4.

      lv_context_value           = ls_log_params.
      ls_message-context-tabname = 'ZSARN_SLG1_LOG_PARAMS_ART_POST'.
      ls_message-context-value   = lv_context_value.

      APPEND ls_message TO mt_message[].
    ENDLOOP.

  ENDMETHOD.


  METHOD build_map_data_and_post_mass.
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             # 06.12.2018
* CHANGE No.       # 9000004644: SAPTR-147 Defect649 ZARN_MASS PIR Update
* DESCRIPTION      # Moved Info Record update to another method
*                  # UPDATE_INFO_RECORDS as original logic was updating only
*                  # purchase org 9999 record
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* DATE             # 03.08.2020
* CHANGE No.       # SSM-1: NW Range-ZARN_GUI CHANGE
* DESCRIPTION      # Mass update ZARN_REG_CLUSTER and corresponding post
*                  # layout module
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
    DATA: ls_idno_data     TYPE ty_s_idno_data,
          lt_mass_fields   TYPE ty_t_mass_fields,
          ls_mass_fields   TYPE ty_s_mass_fields,
          ls_prod_data     TYPE zsarn_prod_data,
          ls_reg_data      TYPE zsarn_reg_data,
          ls_reg_data_mass TYPE zsarn_reg_data,
          ls_mara          TYPE mara,
          ls_maw1          TYPE maw1,
          ls_marm          TYPE marm,
          ls_mean          TYPE mean,
          ls_mvke          TYPE mvke,
          ls_wlk2          TYPE wlk2,
          ls_eina          TYPE eina,
          ls_eine          TYPE eine.

    DATA: lt_mara                  TYPE mara_tab,
          lt_marm                  TYPE marm_tab,
          lt_mean                  TYPE mean_tab,
          lt_eina                  TYPE mmpr_eina,
          lt_eine                  TYPE mmpr_eine,
          lt_maw1                  TYPE wrf_maw1_tty,
          lt_t006                  TYPE tt_t006,
          lt_t005                  TYPE wgds_countries_tty,
          lt_tcurc                 TYPE fiappt_t_tcurc_cbr,
          lt_coo_t                 TYPE ztarn_coo_t,
          lt_gen_uom_t             TYPE ztarn_gen_uom_t,
          lt_t134                  TYPE wrf_t134_tty,
          lt_tvarvc_ekorg          TYPE tvarvc_t,
          lt_tvarvc                TYPE tvarvc_t,
          lt_tvarvc_hyb_code       TYPE tvarvc_t,
          lt_tvta                  TYPE tvta_tt,
          lt_uom_cat               TYPE ztarn_uom_cat,
          lr_status                TYPE ztarn_status_range,
          lt_host_sap              TYPE ztmd_host_sap,
          ls_matgrp_hier           TYPE wrf_matgrp_hier,
          lt_matgrp_prod           TYPE wrf_t_matgrp_prod,
          lt_art_hier              TYPE ztarn_art_hier,


          ls_pir_key               TYPE zsarn_key,
          ls_headdata              TYPE bapie1mathead,
          lt_return                TYPE wty_bapireturn1_tab,
          ls_return                TYPE bapireturn1,
          lt_errorkeys             TYPE STANDARD TABLE OF wrfvalkey,
          lt_clientdata            TYPE STANDARD TABLE OF bapie1marart,
          lt_clientdatax           TYPE STANDARD TABLE OF bapie1marartx,
          lt_clientext             TYPE STANDARD TABLE OF bapie1maraextrt,
          lt_clientextx            TYPE STANDARD TABLE OF bapie1maraextrtx,
          lt_addnlclientdata       TYPE STANDARD TABLE OF bapie1maw1rt,
          lt_addnlclientdatax      TYPE STANDARD TABLE OF bapie1maw1rtx,
          lt_materialdescription   TYPE STANDARD TABLE OF bapie1maktrt,
          lt_plantdata             TYPE STANDARD TABLE OF bapie1marcrt,
          lt_plantdatax            TYPE STANDARD TABLE OF bapie1marcrtx,
          lt_plantext              TYPE STANDARD TABLE OF bapie1marcextrt,          "++ONLD-822 JKH 17.02.2017
          lt_plantextx             TYPE STANDARD TABLE OF bapie1marcextrtx,         "++ONLD-822 JKH 17.02.2017
          lt_unitsofmeasure        TYPE STANDARD TABLE OF bapie1marmrt,
          lt_unitsofmeasurex       TYPE STANDARD TABLE OF bapie1marmrtx,
          lt_internationalartnos   TYPE STANDARD TABLE OF bapie1meanrt,
          lt_valuationdata         TYPE STANDARD TABLE OF bapie1mbewrt,
          lt_valuationdatax        TYPE STANDARD TABLE OF bapie1mbewrtx,
          lt_salesdata             TYPE STANDARD TABLE OF bapie1mvkert,
          lt_salesdatax            TYPE STANDARD TABLE OF bapie1mvkertx,
          lt_salesext              TYPE STANDARD TABLE OF bapie1mvkeextrt,
          lt_salesextx             TYPE STANDARD TABLE OF bapie1mvkeextrtx,
          lt_posdata               TYPE STANDARD TABLE OF bapie1wlk2rt,
          lt_posdatax              TYPE STANDARD TABLE OF bapie1wlk2rtx,
          lt_plantkeys             TYPE STANDARD TABLE OF bapie1wrkkey,
          lt_storagelocationkeys   TYPE STANDARD TABLE OF bapie1lgokey,
          lt_distrchainkeys        TYPE STANDARD TABLE OF bapie1vtlkey,
          lt_storagetypekeys       TYPE STANDARD TABLE OF bapie1lgtkey,
          lt_valuationtypekeys     TYPE STANDARD TABLE OF bapie1bwakey,
          lt_warehousenumberdata   TYPE STANDARD TABLE OF bapie1mlgnrt,             "++ONLD-822 JKH 17.02.2017
          lt_warehousenumberdatax  TYPE STANDARD TABLE OF bapie1mlgnrtx,            "++ONLD-822 JKH 17.02.2017
          lt_importextension       TYPE STANDARD TABLE OF bapiparmatex,
          lt_calp_vb               TYPE calp_vb_tab,

          ls_bapi_clientext        TYPE zmd_s_bapi_clientext,
          ls_bapi_clientextx       TYPE zmd_s_bapi_clientextx,
          ls_bapi_salesext         TYPE zmd_s_bapi_salesext,
          ls_bapi_salesextx        TYPE zmd_s_bapi_salesextx,
          lt_bapi_salesext         TYPE zmd_t_bapi_salesext,
          lt_bapi_plantext         TYPE zmd_t_bapi_plantext,                        "++ONLD-822 JKH 17.02.2017
          lt_legacy_struct         TYPE ztarn_legacy_struct,
          lt_eine_unit_error       TYPE wrf_bapieine_tty,

          ls_msg                   TYPE massmsg,
          lv_post                  TYPE flag,
          lv_error                 TYPE flag,
          lv_main_data_error       TYPE boolean,
          lv_additional_data_error TYPE boolean.

    DATA: lt_reg_cluster TYPE ztarn_reg_cluster.


    ls_idno_data       = is_idno_data.
    lt_mass_fields[]   = it_mass_fields[].
    ls_prod_data       = is_prod_data.
    ls_reg_data        = is_reg_data.
    ls_reg_data_mass   = is_reg_data_mass.




    CLEAR lv_post.
* Build Mapping Data for Mass
    CALL METHOD me->build_mass_mapping_data
      EXPORTING
        is_idno_data            = ls_idno_data
        it_mass_fields          = lt_mass_fields[]
        is_prod_data            = ls_prod_data
        is_reg_data             = ls_reg_data
        is_reg_data_mass        = ls_reg_data_mass
      IMPORTING
        ev_post                 = lv_post
      CHANGING
        cs_pir_key              = ls_pir_key
        cs_headdata             = ls_headdata
        ct_clientdata           = lt_clientdata[]
        ct_clientdatax          = lt_clientdatax[]
        ct_clientext            = lt_clientext[]
        ct_clientextx           = lt_clientextx[]
        ct_addnlclientdata      = lt_addnlclientdata[]
        ct_addnlclientdatax     = lt_addnlclientdatax[]
        ct_materialdescription  = lt_materialdescription[]
        ct_unitsofmeasure       = lt_unitsofmeasure[]
        ct_unitsofmeasurex      = lt_unitsofmeasurex[]
        ct_internationalartnos  = lt_internationalartnos[]
        ct_plantdata            = lt_plantdata[]
        ct_plantdatax           = lt_plantdatax[]
        ct_plantext             = lt_plantext[]                                     "++ONLD-822 JKH 17.02.2017
        ct_plantextx            = lt_plantextx[]                                    "++ONLD-822 JKH 17.02.2017
        ct_valuationdata        = lt_valuationdata[]
        ct_valuationdatax       = lt_valuationdatax[]
        ct_warehousenumberdata  = lt_warehousenumberdata[]                          "++ONLD-822 JKH 17.02.2017
        ct_warehousenumberdatax = lt_warehousenumberdatax[]                         "++ONLD-822 JKH 17.02.2017
        ct_salesdata            = lt_salesdata[]
        ct_salesdatax           = lt_salesdatax[]
        ct_salesext             = lt_salesext[]
        ct_salesextx            = lt_salesextx[]
        ct_posdata              = lt_posdata[]
        ct_posdatax             = lt_posdatax[]
        ct_plantkeys            = lt_plantkeys[]
        ct_storagelocationkeys  = lt_storagelocationkeys[]
        ct_distrchainkeys       = lt_distrchainkeys[]
        ct_valuationtypekeys    = lt_valuationtypekeys[]
        ct_importextension      = lt_importextension[]
        ct_calp_vb              = lt_calp_vb[]
        ct_msg                  = ct_msg[]
        cv_error                = cv_error.


    IF lv_post IS NOT INITIAL.

* Post data to Article Master
      CALL METHOD me->post_mass_data
        EXPORTING
          is_idno_data             = ls_idno_data
          it_mass_fields           = lt_mass_fields[]
          is_prod_data             = ls_prod_data
          is_reg_data              = ls_reg_data
          is_reg_data_mass         = ls_reg_data_mass
        CHANGING
          cs_pir_key               = ls_pir_key
          cs_headdata              = ls_headdata
          ct_clientdata            = lt_clientdata[]
          ct_clientdatax           = lt_clientdatax[]
          ct_clientext             = lt_clientext[]
          ct_clientextx            = lt_clientextx[]
          ct_addnlclientdata       = lt_addnlclientdata[]
          ct_addnlclientdatax      = lt_addnlclientdatax[]
          ct_materialdescription   = lt_materialdescription[]
          ct_unitsofmeasure        = lt_unitsofmeasure[]
          ct_unitsofmeasurex       = lt_unitsofmeasurex[]
          ct_internationalartnos   = lt_internationalartnos[]
          ct_plantdata             = lt_plantdata[]
          ct_plantdatax            = lt_plantdatax[]
          ct_plantext              = lt_plantext[]                                     "++ONLD-822 JKH 17.02.2017
          ct_plantextx             = lt_plantextx[]                                    "++ONLD-822 JKH 17.02.2017
          ct_valuationdata         = lt_valuationdata[]
          ct_valuationdatax        = lt_valuationdatax[]
          ct_warehousenumberdata   = lt_warehousenumberdata[]                          "++ONLD-822 JKH 17.02.2017
          ct_warehousenumberdatax  = lt_warehousenumberdatax[]                         "++ONLD-822 JKH 17.02.2017
          ct_salesdata             = lt_salesdata[]
          ct_salesdatax            = lt_salesdatax[]
          ct_salesext              = lt_salesext[]
          ct_salesextx             = lt_salesextx[]
          ct_posdata               = lt_posdata[]
          ct_posdatax              = lt_posdatax[]
          ct_plantkeys             = lt_plantkeys[]
          ct_storagelocationkeys   = lt_storagelocationkeys[]
          ct_distrchainkeys        = lt_distrchainkeys[]
          ct_valuationtypekeys     = lt_valuationtypekeys[]
          ct_importextension       = lt_importextension[]
          ct_calp_vb               = lt_calp_vb[]
          ct_msg                   = ct_msg[]
          ct_return                = lt_return[]
          cv_main_data_error       = lv_main_data_error
          cv_additional_data_error = lv_additional_data_error
          cv_error                 = cv_error.

    ENDIF.  " IF lv_post IS NOT INITIAL

    " When cluster is deleted, add blank entry so that existing layout module assignment will be deleted
    lt_reg_cluster = is_reg_data_mass-zarn_reg_cluster.
    LOOP AT is_reg_data_mass_delete-zarn_reg_cluster INTO DATA(ls_reg_cluster_delete) WHERE idno = is_reg_data_mass-idno.
      APPEND INITIAL LINE TO lt_reg_cluster ASSIGNING FIELD-SYMBOL(<ls_reg_cluster>).
      <ls_reg_cluster>-idno   = ls_reg_cluster_delete-idno.
      <ls_reg_cluster>-banner = ls_reg_cluster_delete-banner.
    ENDLOOP.
    me->post_layout_module( EXPORTING iv_article          = is_idno_data-sap_article
                                      it_reg_cluster_mass = lt_reg_cluster
                            CHANGING  ct_message          = ct_msg ).

  ENDMETHOD.


  METHOD build_mass_mapping_data.

    DATA: ls_idno_data     TYPE ty_s_idno_data,
          lt_mass_fields   TYPE ty_t_mass_fields,
          ls_mass_fields   TYPE ty_s_mass_fields,
          ls_prod_data     TYPE zsarn_prod_data,
          ls_reg_data      TYPE zsarn_reg_data,
          ls_reg_data_mass TYPE zsarn_reg_data,
          lt_msg           TYPE mass_msgs,
          ls_msg           TYPE massmsg,

          ls_mara          TYPE mara,
          ls_maw1          TYPE maw1,
          ls_marm          TYPE marm,
          ls_mean          TYPE mean,
          ls_mvke          TYPE mvke,
          ls_wlk2          TYPE wlk2,
          ls_eina          TYPE eina,
          ls_eine          TYPE eine.

    DATA: lt_mara                  TYPE mara_tab,
          lt_marm                  TYPE marm_tab,
          lt_mean                  TYPE mean_tab,
          lt_eina                  TYPE mmpr_eina,
          lt_eine                  TYPE mmpr_eine,
          lt_maw1                  TYPE wrf_maw1_tty,
          lt_t006                  TYPE tt_t006,
          lt_t005                  TYPE wgds_countries_tty,
          lt_tcurc                 TYPE fiappt_t_tcurc_cbr,
          lt_coo_t                 TYPE ztarn_coo_t,
          lt_gen_uom_t             TYPE ztarn_gen_uom_t,
          lt_t134                  TYPE wrf_t134_tty,
          lt_tvarvc_ekorg          TYPE tvarvc_t,
          lt_tvarvc                TYPE tvarvc_t,
          lt_tvarvc_hyb_code       TYPE tvarvc_t,
          lt_tvta                  TYPE tvta_tt,
          lt_uom_cat               TYPE ztarn_uom_cat,
          lr_status                TYPE ztarn_status_range,
          lt_host_sap              TYPE ztmd_host_sap,
          ls_matgrp_hier           TYPE wrf_matgrp_hier,
          lt_matgrp_prod           TYPE wrf_t_matgrp_prod,
          lt_art_hier              TYPE ztarn_art_hier,


          ls_pir_key               TYPE zsarn_key,
          ls_headdata              TYPE bapie1mathead,
          lt_return                TYPE wty_bapireturn1_tab,
          ls_return                TYPE bapireturn1,
          lt_errorkeys             TYPE STANDARD TABLE OF wrfvalkey,
          lt_clientdata            TYPE STANDARD TABLE OF bapie1marart,
          lt_clientdatax           TYPE STANDARD TABLE OF bapie1marartx,
          lt_clientext             TYPE STANDARD TABLE OF bapie1maraextrt,
          lt_clientextx            TYPE STANDARD TABLE OF bapie1maraextrtx,
          lt_addnlclientdata       TYPE STANDARD TABLE OF bapie1maw1rt,
          lt_addnlclientdatax      TYPE STANDARD TABLE OF bapie1maw1rtx,
          lt_materialdescription   TYPE STANDARD TABLE OF bapie1maktrt,
          lt_plantdata             TYPE STANDARD TABLE OF bapie1marcrt,
          lt_plantdatax            TYPE STANDARD TABLE OF bapie1marcrtx,
          lt_plantext              TYPE STANDARD TABLE OF bapie1marcextrt,          "++ONLD-822 JKH 17.02.2017
          lt_plantextx             TYPE STANDARD TABLE OF bapie1marcextrtx,         "++ONLD-822 JKH 17.02.2017
          lt_unitsofmeasure        TYPE STANDARD TABLE OF bapie1marmrt,
          lt_unitsofmeasurex       TYPE STANDARD TABLE OF bapie1marmrtx,
          lt_internationalartnos   TYPE STANDARD TABLE OF bapie1meanrt,
          lt_valuationdata         TYPE STANDARD TABLE OF bapie1mbewrt,
          lt_valuationdatax        TYPE STANDARD TABLE OF bapie1mbewrtx,
          lt_warehousenumberdata   TYPE STANDARD TABLE OF bapie1mlgnrt,             "++ONLD-822 JKH 17.02.2017
          lt_warehousenumberdatax  TYPE STANDARD TABLE OF bapie1mlgnrtx,            "++ONLD-822 JKH 17.02.2017
          lt_salesdata             TYPE STANDARD TABLE OF bapie1mvkert,
          lt_salesdatax            TYPE STANDARD TABLE OF bapie1mvkertx,
          lt_salesext              TYPE STANDARD TABLE OF bapie1mvkeextrt,
          lt_salesextx             TYPE STANDARD TABLE OF bapie1mvkeextrtx,
          lt_posdata               TYPE STANDARD TABLE OF bapie1wlk2rt,
          lt_posdatax              TYPE STANDARD TABLE OF bapie1wlk2rtx,
          lt_plantkeys             TYPE STANDARD TABLE OF bapie1wrkkey,
          lt_storagelocationkeys   TYPE STANDARD TABLE OF bapie1lgokey,
          lt_distrchainkeys        TYPE STANDARD TABLE OF bapie1vtlkey,
          lt_storagetypekeys       TYPE STANDARD TABLE OF bapie1lgtkey,
          lt_valuationtypekeys     TYPE STANDARD TABLE OF bapie1bwakey,
          lt_importextension       TYPE STANDARD TABLE OF bapiparmatex,
          lt_inforecord_general    TYPE STANDARD TABLE OF bapieina,
          lt_inforecord_purchorg   TYPE STANDARD TABLE OF wrfbapieine,
          lt_calp_vb               TYPE calp_vb_tab,

          ls_bapi_clientext        TYPE zmd_s_bapi_clientext,
          ls_bapi_clientextx       TYPE zmd_s_bapi_clientextx,
          ls_bapi_salesext         TYPE zmd_s_bapi_salesext,
          ls_bapi_salesextx        TYPE zmd_s_bapi_salesextx,
          lt_bapi_salesext         TYPE zmd_t_bapi_salesext,
          lt_bapi_plantext         TYPE zmd_t_bapi_plantext,                        "++ONLD-822 JKH 17.02.2017
          lt_legacy_struct         TYPE ztarn_legacy_struct,
          lt_eine_unit_error       TYPE wrf_bapieine_tty,

          lv_main_data_error       TYPE boolean,
          lv_additional_data_error TYPE boolean,
          lv_post                  TYPE flag.




    ls_idno_data     = is_idno_data.
    ls_prod_data     = is_prod_data.
    ls_reg_data      = is_reg_data.
    ls_reg_data_mass = is_reg_data_mass.
    lt_mass_fields   = it_mass_fields[].


    lt_mara[]            = mt_mara[].
    lt_marm[]            = mt_marm[].
    lt_mean[]            = mt_mean[].
    lt_eina[]            = mt_eina[].
    lt_eine[]            = mt_eine[].
    lt_maw1[]            = mt_maw1[].
    lt_host_sap[]        = mt_host_sap[].
    lt_t006[]            = mt_t006[].
    lt_t005[]            = mt_t005[].
    lt_tcurc[]           = mt_tcurc[].
    lt_coo_t[]           = mt_coo_t[].
    lt_gen_uom_t[]       = mt_gen_uom_t[].
    lt_t134[]            = mt_t134[].
    lt_tvarvc_ekorg[]    = mt_tvarvc_ekorg[].
    lt_tvarvc[]          = mt_tvarvc[].
    lt_tvarvc_hyb_code[] = mt_tvarvc_hyb_code[].
    lt_tvta[]            = mt_tvta[].
    lt_uom_cat[]         = mt_uom_cat[].

* Build Mapping Data for Article Posting
    CALL FUNCTION 'ZARN_MAP_ARTICLE_POST_DATA'
      EXPORTING
        is_idno_data           = ls_idno_data
        is_prod_data           = ls_prod_data
        is_reg_data            = ls_reg_data
        ir_status              = lr_status[]
        it_mara                = lt_mara[]
        it_marm                = lt_marm[]
        it_mean                = lt_mean[]
        it_eina                = lt_eina[]
        it_eine                = lt_eine[]
        it_maw1                = lt_maw1[]
        it_host_sap            = lt_host_sap[]
        it_t006                = lt_t006[]
        it_t005                = lt_t005[]
        it_tcurc               = lt_tcurc[]
        it_coo_t               = lt_coo_t[]
        it_gen_uom_t           = lt_gen_uom_t[]
        it_t134                = lt_t134[]
*decomissioning usage for TVARVC for PIR ekorg - should be replaced by ZARN_PIR_PUR_CNF
        "it_tvarvc_ekorg        = lt_tvarvc_ekorg[]   "IS 2019/06
        it_tvarvc_hyb_code     = lt_tvarvc_hyb_code[]
        it_tvarvc              = lt_tvarvc[]
        it_tvta                = lt_tvta[]
        it_uom_cat             = lt_uom_cat[]
        is_matgrp_hier         = ls_matgrp_hier
        it_matgrp_prod         = lt_matgrp_prod[]
        it_art_hier            = lt_art_hier[]
        iv_batch               = space
      IMPORTING
        es_pir_key             = ls_pir_key
        es_headdata            = ls_headdata
        et_errorkeys           = lt_errorkeys[]
        et_clientdata          = lt_clientdata[]
*       et_clientdatax         = lt_clientdatax[]
        et_clientext           = lt_clientext[]
*       et_clientextx          = lt_clientextx[]
        et_addnlclientdata     = lt_addnlclientdata[]
*       et_addnlclientdatax    = lt_addnlclientdatax[]
        et_materialdescription = lt_materialdescription[]
        et_plantdata           = lt_plantdata[]
*       et_plantdatax          = lt_plantdatax[]
        et_plantext            = lt_plantext[]                                      "++ONLD-822 JKH 17.02.2017
*       et_plantdextx          = lt_plantextx[]                                     "++ONLD-822 JKH 17.02.2017
        et_unitsofmeasure      = lt_unitsofmeasure[]
*       et_unitsofmeasurex     = lt_unitsofmeasurex[]
        et_internationalartnos = lt_internationalartnos[]
        et_valuationdata       = lt_valuationdata[]
*       et_valuationdatax      = lt_valuationdatax[]
        et_warehousenumberdata = lt_warehousenumberdata[]                          "++ONLD-822 JKH 17.02.2017
*       et_warehousenumberdatax = lt_warehousenumberdatax[]                         "++ONLD-822 JKH 17.02.2017
        et_salesdata           = lt_salesdata[]
*       et_salesdatax          = lt_salesdatax[]
        et_salesext            = lt_salesext[]
*       et_salesextx           = lt_salesextx[]
        et_posdata             = lt_posdata[]
*       et_posdatax            = lt_posdatax[]
        et_plantkeys           = lt_plantkeys[]
        et_storagelocationkeys = lt_storagelocationkeys[]
        et_distrchainkeys      = lt_distrchainkeys[]
        et_valuationtypekeys   = lt_valuationtypekeys[]
        et_importextension     = lt_importextension[]
        et_inforecord_general  = lt_inforecord_general[]
        et_inforecord_purchorg = lt_inforecord_purchorg[]
        et_eine_unit_error     = lt_eine_unit_error[]
*       et_hierarchy_items     = lt_hierarchy_items[]
*       et_followupmatdata     = lt_followupmatdata[]
*       et_materialdatax       = lt_materialdatax[]
        et_calp_vb             = lt_calp_vb[].

    CLEAR: lt_clientdatax[], lt_clientextx[], lt_addnlclientdatax[], lt_unitsofmeasurex[],
           lt_plantdatax[], lt_valuationdatax[], lt_salesdatax[], lt_salesextx[], lt_posdatax[],
           lt_plantextx[], lt_warehousenumberdatax[].                                            "++ONLD-822 JKH 17.02.2017


    READ TABLE lt_mass_fields[] TRANSPORTING NO FIELDS
    WITH KEY table = 'ZARN_REG_HDR'.
    IF sy-subrc = 0.

      CLEAR lv_post.
      CALL METHOD me->filter_reg_header_data
        EXPORTING
          is_idno_data           = ls_idno_data
          it_mass_fields         = lt_mass_fields
          is_prod_data           = ls_prod_data
          is_reg_data            = ls_reg_data
          is_reg_data_mass       = ls_reg_data_mass
          it_mara                = mt_mara[]
        IMPORTING
          ev_post                = lv_post
        CHANGING
          ct_clientdata          = lt_clientdata[]
          ct_clientdatax         = lt_clientdatax[]
          ct_clientext           = lt_clientext[]
          ct_clientextx          = lt_clientextx[]
          ct_addnlclientdata     = lt_addnlclientdata[]
          ct_addnlclientdatax    = lt_addnlclientdatax[]
          ct_materialdescription = lt_materialdescription[]
          ct_plantdata           = lt_plantdata[]
          ct_plantdatax          = lt_plantdatax[]
          ct_msg                 = lt_msg[].


      IF lv_post IS NOT INITIAL.
        ev_post = lv_post.
      ENDIF.

    ELSE.
      CLEAR: "lt_clientdata[],
             "lt_clientdatax[],
             lt_clientext[],
             lt_clientextx[],
             "lt_addnlclientdata[],
             "lt_addnlclientdatax[],
             lt_materialdescription[].
    ENDIF.


    READ TABLE lt_mass_fields[] TRANSPORTING NO FIELDS
    WITH KEY table = 'ZARN_REG_UOM'.
    IF sy-subrc = 0.

      CLEAR lv_post.
      CALL METHOD me->filter_reg_uom_data
        EXPORTING
          is_idno_data        = ls_idno_data
          it_mass_fields      = lt_mass_fields
          is_reg_data_mass    = ls_reg_data_mass
          it_marm             = mt_marm[]
          it_mean             = mt_mean[]
        IMPORTING
          ev_post             = lv_post
        CHANGING
          ct_unitsofmeasure   = lt_unitsofmeasure[]
          ct_unitsofmeasurex  = lt_unitsofmeasurex[]
          ct_clientdata       = lt_clientdata[]
          ct_clientdatax      = lt_clientdatax[]
          ct_addnlclientdata  = lt_addnlclientdata[]
          ct_addnlclientdatax = lt_addnlclientdatax[]
          ct_msg              = lt_msg[].

      IF lv_post IS NOT INITIAL.
        ev_post = lv_post.
      ENDIF.

    ELSE.

      READ TABLE lt_mass_fields[] TRANSPORTING NO FIELDS
      WITH KEY table = 'ZARN_REG_EAN'.
      IF sy-subrc = 0.

        CLEAR lv_post.
        CALL METHOD me->filter_reg_uom_data
          EXPORTING
            is_idno_data        = ls_idno_data
            it_mass_fields      = lt_mass_fields
            is_reg_data_mass    = ls_reg_data_mass
            it_marm             = mt_marm[]
            it_mean             = mt_mean[]
          IMPORTING
            ev_post             = lv_post
          CHANGING
            ct_unitsofmeasure   = lt_unitsofmeasure[]
            ct_unitsofmeasurex  = lt_unitsofmeasurex[]
            ct_clientdata       = lt_clientdata[]
            ct_clientdatax      = lt_clientdatax[]
            ct_addnlclientdata  = lt_addnlclientdata[]
            ct_addnlclientdatax = lt_addnlclientdatax[]
            ct_msg              = lt_msg[].

        IF lv_post IS NOT INITIAL.
          ev_post = lv_post.
        ENDIF.

      ELSE.
        CLEAR : lt_unitsofmeasure[],
                lt_unitsofmeasurex[].
      ENDIF.
    ENDIF.

    READ TABLE lt_mass_fields[] TRANSPORTING NO FIELDS
    WITH KEY table = 'ZARN_REG_BANNER'.
    IF sy-subrc = 0.

      CLEAR lv_post.
      CALL METHOD me->filter_reg_banner_data
        EXPORTING
          is_idno_data            = ls_idno_data
          it_mass_fields          = lt_mass_fields
          is_prod_data            = ls_prod_data
          is_reg_data             = ls_reg_data
          is_reg_data_mass        = ls_reg_data_mass
          it_marc                 = mt_marc[]
          it_mvke                 = mt_mvke[]
          it_wlk2                 = mt_wlk2[]
          it_host_sap             = mt_host_sap[]
          it_tvarvc               = mt_tvarvc[]
          it_tvta                 = mt_tvta[]
        IMPORTING
          ev_post                 = lv_post
        CHANGING
          ct_plantdata            = lt_plantdata[]
          ct_plantdatax           = lt_plantdatax[]
          ct_plantext             = lt_plantext[]                                     "++ONLD-822 JKH 17.02.2017
          ct_plantextx            = lt_plantextx[]                                    "++ONLD-822 JKH 17.02.2017
          ct_valuationdata        = lt_valuationdata[]
          ct_valuationdatax       = lt_valuationdatax[]
          ct_warehousenumberdata  = lt_warehousenumberdata[]                          "++ONLD-822 JKH 17.02.2017
          ct_warehousenumberdatax = lt_warehousenumberdatax[]                         "++ONLD-822 JKH 17.02.2017
          ct_salesdata            = lt_salesdata[]
          ct_salesdatax           = lt_salesdatax[]
          ct_salesext             = lt_salesext[]
          ct_salesextx            = lt_salesextx[]
          ct_posdata              = lt_posdata[]
          ct_posdatax             = lt_posdatax[]
          ct_plantkeys            = lt_plantkeys[]
          ct_storagelocationkeys  = lt_storagelocationkeys[]
          ct_distrchainkeys       = lt_distrchainkeys[]
          ct_valuationtypekeys    = lt_valuationtypekeys[]
          ct_importextension      = lt_importextension[]
          ct_msg                  = lt_msg[].

      IF lv_post IS NOT INITIAL.
        ev_post = lv_post.
      ENDIF.

    ELSE.
      CLEAR : "lt_plantdata[],
              "lt_plantdatax[],
              lt_plantext[],                                                        "++ONLD-822 JKH 17.02.2017
              lt_plantextx[],                                                       "++ONLD-822 JKH 17.02.2017
              lt_valuationdata[],
              lt_valuationdatax[],
              lt_warehousenumberdata[],                                             "++ONLD-822 JKH 17.02.2017
              lt_warehousenumberdatax[],                                            "++ONLD-822 JKH 17.02.2017
              lt_salesdata[],
              lt_salesdatax[],
              lt_salesext[],
              lt_salesextx[],
              lt_posdata[],
              lt_posdatax[],
              lt_plantkeys[],
              lt_storagelocationkeys[],
              lt_distrchainkeys[],
              lt_valuationtypekeys[],
              lt_importextension[].
    ENDIF.


    READ TABLE lt_mass_fields[] TRANSPORTING NO FIELDS
    WITH KEY table = 'ZARN_REG_PIR'.
    IF sy-subrc = 0.

      CLEAR lv_post.
      CALL METHOD me->filter_reg_pir_data
        EXPORTING
          is_idno_data           = ls_idno_data
          it_mass_fields         = lt_mass_fields
          is_prod_data           = ls_prod_data
          is_reg_data            = ls_reg_data
          is_reg_data_mass       = ls_reg_data_mass
          it_eina                = mt_eina[]
          it_eine                = mt_eine[]
          it_marm                = mt_marm[]
          it_t006                = mt_t006[]
          it_tcurc               = mt_tcurc[]
        IMPORTING
          ev_post                = lv_post
        CHANGING
          ct_inforecord_general  = lt_inforecord_general[]
          ct_inforecord_purchorg = lt_inforecord_purchorg[]
          ct_calp_vb             = lt_calp_vb[]
          ct_clientdata          = lt_clientdata[]
          ct_clientdatax         = lt_clientdatax[]
          ct_addnlclientdata     = lt_addnlclientdata[]
          ct_addnlclientdatax    = lt_addnlclientdatax[]
          ct_msg                 = lt_msg[].

      IF lv_post IS NOT INITIAL.
        ev_post = lv_post.
      ENDIF.

    ELSE.
      CLEAR: lt_inforecord_general[],
             lt_inforecord_purchorg[],
             lt_eine_unit_error[],
             lt_calp_vb[].
    ENDIF.

    cs_pir_key                = ls_pir_key.
    cs_headdata               = ls_headdata.
    ct_clientdata[]           = lt_clientdata[].
    ct_clientdatax[]          = lt_clientdatax[].
    ct_clientext[]            = lt_clientext[].
    ct_clientextx[]           = lt_clientextx[].
    ct_addnlclientdata[]      = lt_addnlclientdata[].
    ct_addnlclientdatax[]     = lt_addnlclientdatax[].
    ct_materialdescription[]  = lt_materialdescription[].
    ct_unitsofmeasure[]       = lt_unitsofmeasure[].
    ct_unitsofmeasurex[]      = lt_unitsofmeasurex[].
    ct_plantdata[]            = lt_plantdata[].
    ct_plantdatax[]           = lt_plantdatax[].
    ct_plantext[]             = lt_plantext[].                                       "++ONLD-822 JKH 17.02.2017
    ct_plantextx[]            = lt_plantextx[].                                      "++ONLD-822 JKH 17.02.2017
    ct_valuationdata[]        = lt_valuationdata[].
    ct_valuationdatax[]       = lt_valuationdatax[].
    ct_warehousenumberdata[]  = lt_warehousenumberdata[].                           "++ONLD-822 JKH 17.02.2017
    ct_warehousenumberdatax[] = lt_warehousenumberdatax[].                          "++ONLD-822 JKH 17.02.2017
    ct_salesdata[]            = lt_salesdata[].
    ct_salesdatax[]           = lt_salesdatax[].
    ct_salesext[]             = lt_salesext[].
    ct_salesextx[]            = lt_salesextx[].
    ct_posdata[]              = lt_posdata[].
    ct_posdatax[]             = lt_posdatax[].
    ct_plantkeys[]            = lt_plantkeys[].
    ct_storagelocationkeys[]  = lt_storagelocationkeys[].
    ct_distrchainkeys[]       = lt_distrchainkeys[].
    ct_valuationtypekeys[]    = lt_valuationtypekeys[].
    ct_importextension[]      = lt_importextension[].
    ct_inforecord_general[]   = lt_inforecord_general[].
    ct_inforecord_purchorg[]  = lt_inforecord_purchorg[].
    ct_calp_vb[]              = lt_calp_vb[].

    ">>>IS 1902 - the filter methods can return type E messages but the posting will still happen
    "when called from external application, we want to ignore such messages - its not a real error
    "which would mean the update didnt happen

    IF mv_only_important_messages = abap_true
      AND cv_error NE abap_true.
      "dont return these messages because they are not impacting the process
    ELSE.
      APPEND LINES OF lt_msg[] TO ct_msg[].
    ENDIF.

    "<<<IS 1902

  ENDMETHOD.


  METHOD build_regional_mass_data.
*-----------------------------------------------------------------------
* DATE             # 27.07.2020
* CHANGE No.       # SSM-1: NW Range-ZARN_GUI CHANGE
* DESCRIPTION      # Added ZARN_REG_CLUSTER for mass maintenance
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* DATE             # 18.08.2020
* CHANGE No.       # SSM-1: NW Range-ZARN_GUI CHANGE
* DESCRIPTION      # Delete ZARN_REG_CLUSTER based on banner values
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
    DATA: ls_reg_data_mass TYPE zsarn_reg_data,
          ls_reg_key       TYPE zsarn_reg_key,
          lt_reg_data      TYPE ztarn_reg_data,
          ls_reg_data      TYPE zsarn_reg_data,

          lt_dfies         TYPE dfies_tab,
          ls_dfies         TYPE dfies,

          lv_itabname      TYPE char50.

    FIELD-SYMBOLS: <table> TYPE STANDARD TABLE,
                   <wa>    TYPE any,
                   <idno>  TYPE zarn_idno.

    CLEAR: ls_reg_data_mass, es_reg_data_mass, et_reg_key[], es_reg_data_mass_delete.


* Build Mass Data
    ls_reg_data_mass-zarn_reg_hdr[]     = it_reg_hdr[].
    ls_reg_data_mass-zarn_reg_banner[]  = it_reg_banner[].
    ls_reg_data_mass-zarn_reg_ean[]     = it_reg_ean[].
    ls_reg_data_mass-zarn_reg_pir[]     = it_reg_pir[].
    ls_reg_data_mass-zarn_reg_prfam[]   = it_reg_prfam[].
    ls_reg_data_mass-zarn_reg_txt[]     = it_reg_txt[].
    ls_reg_data_mass-zarn_reg_uom[]     = it_reg_uom[].
    ls_reg_data_mass-zarn_reg_lst_prc[] = it_reg_lst_prc[].
    ls_reg_data_mass-zarn_reg_dc_sell[] = it_reg_dc_sell[].
    ls_reg_data_mass-zarn_reg_std_ter[] = it_reg_std_ter[].
    ls_reg_data_mass-zarn_reg_rrp[]     = it_reg_rrp[].

    " Fetch records which needs to be deleted. If the user has modified deleted records, ignore them
    me->build_regional_mass_delete( EXPORTING it_mass_fields          = it_mass_fields
                                              it_reg_banner           = it_reg_banner
                                    IMPORTING es_reg_data_mass_delete = es_reg_data_mass_delete ).
    LOOP AT it_reg_cluster INTO DATA(ls_reg_cluster).
      READ TABLE es_reg_data_mass_delete-zarn_reg_cluster WITH KEY idno   = ls_reg_cluster-idno
                                                                   banner = ls_reg_cluster-banner
                                                                   BINARY SEARCH TRANSPORTING NO FIELDS.
      IF sy-subrc <> 0.
        APPEND ls_reg_cluster TO ls_reg_data_mass-zarn_reg_cluster.
      ENDIF.
    ENDLOOP.

    es_reg_data_mass = ls_reg_data_mass.

* Get Fields of the table
    REFRESH: lt_dfies[].
    CALL FUNCTION 'DDIF_FIELDINFO_GET'
      EXPORTING
        tabname        = 'ZSARN_REG_DATA'
        langu          = sy-langu
        all_types      = abap_true
      TABLES
        dfies_tab      = lt_dfies[]
      EXCEPTIONS
        not_found      = 1
        internal_error = 2
        OTHERS         = 3.
    IF sy-subrc <> 0.
      RETURN.
    ENDIF.


* For each regional table, collect distinct IDNOs into ET_REG_KEY
    LOOP AT lt_dfies[] INTO ls_dfies.

      IF ls_dfies-fieldname = 'IDNO' OR ls_dfies-fieldname = 'VERSION'.
        CONTINUE.
      ENDIF.


      IF <table> IS ASSIGNED. UNASSIGN <table>. ENDIF.
      CLEAR: lv_itabname.
      CONCATENATE 'LS_REG_DATA_MASS-' ls_dfies-fieldname INTO lv_itabname.
      ASSIGN (lv_itabname) TO <table>.

      IF <table> IS INITIAL OR <table> IS NOT ASSIGNED.
        CONTINUE.
      ENDIF.

* For each Regional Table
      LOOP AT <table> ASSIGNING <wa>.

        IF <idno> IS ASSIGNED. UNASSIGN <idno>. ENDIF.
        ASSIGN COMPONENT 'IDNO' OF STRUCTURE <wa> TO <idno>.

        IF <idno> IS ASSIGNED AND <idno> IS NOT INITIAL.
          READ TABLE et_reg_key[] TRANSPORTING NO FIELDS
          WITH TABLE KEY idno = <idno>.
          IF sy-subrc NE 0.
            CLEAR ls_reg_key.
            ls_reg_key-idno = <idno>.
            INSERT ls_reg_key INTO TABLE et_reg_key[].
          ENDIF.
        ENDIF.  " IF <idno> IS ASSIGNED AND <idno> IS NOT INITIAL

      ENDLOOP.  " LOOP AT <table> ASSIGNING <wa>
    ENDLOOP.  " LOOP AT lt_dfies[] INTO ls_dfies.

  ENDMETHOD.


METHOD build_regional_mass_delete.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 18.08.2020
* SYSTEM           # DE0
* SPECIFICATION    # SSM-1: NW Range-ZARN_GUI CHANGE
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Determine mass deletion records
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  DATA: lt_id_banner           TYPE tt_id_banner,
        lt_valid_cluster_range TYPE RANGE OF zmd_e_cluster_range.

  CLEAR: es_reg_data_mass_delete.

  " When banner is changed, check if regional cluster need to be adjusted
  READ TABLE it_mass_fields WITH KEY table = 'ZARN_REG_BANNER'
                                     field = 'SSTUF'
                                     TRANSPORTING NO FIELDS.
  IF sy-subrc <> 0.
    RETURN.
  ENDIF.

  " Get valid cluster ranges
  SELECT 'I'         AS sign,
         'EQ'        AS option,
         assordimval AS low,
         ' '         AS high
    FROM zvmd_cluster_rng
    INTO TABLE @lt_valid_cluster_range.

  " If the banner range is changed to invalid cluster range then delete existing cluster range data
  LOOP AT it_reg_banner INTO DATA(ls_reg_banner) WHERE sstuf NOT IN lt_valid_cluster_range.
    APPEND INITIAL LINE TO lt_id_banner ASSIGNING FIELD-SYMBOL(<ls_id_banner>).
    MOVE-CORRESPONDING ls_reg_banner TO <ls_id_banner>.
  ENDLOOP.
  IF lt_id_banner IS INITIAL.
    RETURN.
  ENDIF.

  " Now, read all existing clusters
  SELECT *
    FROM zarn_reg_cluster
    INTO TABLE @es_reg_data_mass_delete-zarn_reg_cluster
    FOR ALL ENTRIES IN @lt_id_banner
    WHERE idno   = @lt_id_banner-idno
      AND banner = @lt_id_banner-banner.

  SORT es_reg_data_mass_delete-zarn_reg_cluster BY idno banner.

ENDMETHOD.


  METHOD check_idno_lock.


    DATA: lv_idno	        TYPE zarn_idno,
          lv_locked	      TYPE flag,
          lv_user	        TYPE sy-uname,
          lv_username	    TYPE ad_namtext,

          ls_msg          TYPE massmsg,
          lt_enq          TYPE STANDARD TABLE OF seqg3,
          ls_enq          TYPE seqg3,
          lv_garg         TYPE eqegraarg,
          ls_user_address TYPE addr3_val.

    lv_idno  = iv_idno.
    lv_garg  = sy-mandt && lv_idno.


* Check lock
    CLEAR: lt_enq[].
    CALL FUNCTION 'ENQUEUE_READ'
      EXPORTING
        gclient               = sy-mandt
        gname                 = mc_lock_reg
        garg                  = lv_garg
        guname                = ' '
      TABLES
        enq                   = lt_enq[]
      EXCEPTIONS
        communication_failure = 1
        system_failure        = 2
        OTHERS                = 3.


    IF lt_enq IS NOT INITIAL.

      CLEAR ls_enq.
      READ TABLE lt_enq INTO ls_enq INDEX 1.
      IF sy-subrc = 0.

        lv_locked = abap_true.
        lv_user   = ls_enq-guname.

        CLEAR ls_user_address.
        CALL FUNCTION 'SUSR_USER_ADDRESS_READ'
          EXPORTING
            user_name              = lv_user
          IMPORTING
            user_address           = ls_user_address
          EXCEPTIONS
            user_address_not_found = 1
            OTHERS                 = 2.
        IF sy-subrc IS INITIAL.
          lv_username = ls_user_address-name_text.
        ELSE.
          CLEAR lv_username.
        ENDIF.


* IDNO & is Locked by & - &
        CLEAR ls_msg.
        ls_msg-objkey = 'ZARENA'.
        ls_msg-msgty  = 'E'.
        ls_msg-msgid  = 'ZARENA_MSG'.
        ls_msg-msgno  = '091'.
        ls_msg-msgv1  = lv_idno.
        ls_msg-msgv2  = lv_user.
        ls_msg-msgv3  = lv_username.
* ls_msg-MSGV4  =
        APPEND ls_msg TO ct_msg[].

      ENDIF.  " READ TABLE lt_enq INTO ls_enq
    ENDIF.  " IF lt_enq IS NOT INITIAL


    ev_locked   = lv_locked.
    ev_user     = lv_user.
    ev_username = lv_username.




  ENDMETHOD.


  METHOD constructor.

    mv_only_important_messages = iv_important_msgs_only.
    mo_message_service         = zcl_message_services=>get_instance( ).

  ENDMETHOD.


  METHOD filter_reg_banner_data.

    DATA: ls_idno_data            TYPE ty_s_idno_data,
          lt_mass_fields          TYPE ty_t_mass_fields,
          ls_prod_data            TYPE zsarn_prod_data,
          ls_reg_data	            TYPE zsarn_reg_data,
          ls_reg_data_mass        TYPE zsarn_reg_data,
          ls_msg                  TYPE massmsg,

          lt_marc                 TYPE ty_t_marc,
          lt_mvke                 TYPE ty_t_mvke,
          lt_wlk2                 TYPE ty_t_wlk2,
          lt_host_sap             TYPE ty_t_host_sap,
          lt_tvarvc               TYPE tvarvc_t,
          lt_tvta                 TYPE tvta_tt,
          lt_plantdata            TYPE bapie1marcrt_tab,
          lt_plantdatax           TYPE bapie1marcrtx_tab,
          lt_plantext             TYPE bapie1marcextrt_tab,                          "++ONLD-822 JKH 17.02.2017
          lt_plantextx            TYPE bapie1marcextrtx_tab,                         "++ONLD-822 JKH 17.02.2017
          lt_valuationdata        TYPE bapie1mbewrt_tab,
          lt_valuationdatax	      TYPE bapie1mbewrtx_tab,
          lt_warehousenumberdata  TYPE bapie1mlgnrt_tab,                             "++ONLD-822 JKH 17.02.2017
          lt_warehousenumberdatax TYPE bapie1mlgnrtx_tab,                            "++ONLD-822 JKH 17.02.2017
          lt_salesdata            TYPE bapie1mvkert_tab,
          lt_salesdatax	          TYPE bapie1mvkertx_tab,
          lt_salesext	            TYPE bapie1mvkeextrt_tab,
          lt_salesextx            TYPE bapie1mvkeextrtx_tab,
          lt_posdata              TYPE bapie1wlk2rt_tab,
          lt_posdatax	            TYPE bapie1wlk2rtx_tab,
          lt_plantkeys            TYPE bapie1wrkkey_tab,
          lt_storagelocationkeys  TYPE bapie1lgokey_tab,
          lt_distrchainkeys	      TYPE bapie1vtlkey_tab,
          lt_valuationtypekeys    TYPE bapie1bwakey_tab,
          lt_importextension      TYPE wrf_bapiparmatex_tty,

          ls_mass_fields          TYPE ty_s_mass_fields,
          ls_reg_banner_mass      TYPE zarn_reg_banner,
          ls_marc                 TYPE marc,
          ls_mvke                 TYPE mvke,
          ls_wlk2                 TYPE wlk2,
          ls_host_sap             TYPE ty_s_host_sap,
          ls_tvarvc               TYPE tvarvc,
*          ls_tvarvc_ekorg        TYPE tvarvc,
          ls_tvta                 TYPE tvta,
          lv_werks                TYPE werks_d,

          ls_bapi_salesextx       TYPE zmd_s_bapi_salesextx,
          ls_bapi_plantextx       TYPE zmd_s_bapi_plantextx,                         "++ONLD-822 JKH 17.02.2017
          lv_string               TYPE string,
          lv_append               TYPE flag,

          BEGIN OF ls_data_sales,
            field1 TYPE bapie1mvkeextrtx-field1,
            field2 TYPE bapie1mvkeextrtx-field2,
            field3 TYPE bapie1mvkeextrtx-field3,
            field4 TYPE bapie1mvkeextrtx-field4,
          END OF ls_data_sales,

          BEGIN OF ls_data_plant,                                                   "++ONLD-822 JKH 17.02.2017
            field1 TYPE bapie1marcextrtx-field1,
            field2 TYPE bapie1marcextrtx-field2,
            field3 TYPE bapie1marcextrtx-field3,
            field4 TYPE bapie1marcextrtx-field4,
          END OF ls_data_plant,

          ls_plantdata            TYPE bapie1marcrt,
          ls_plantdatax	          TYPE bapie1marcrtx,
          ls_plantext             TYPE bapie1marcextrt,                             "++ONLD-822 JKH 17.02.2017
          ls_plantextx            TYPE bapie1marcextrtx,                            "++ONLD-822 JKH 17.02.2017
          ls_valuationdata        TYPE bapie1mbewrt,
          ls_valuationdatax	      TYPE bapie1mbewrtx,
          ls_warehousenumberdata  TYPE bapie1mlgnrt,                                "++ONLD-822 JKH 17.02.2017
          ls_warehousenumberdatax TYPE bapie1mlgnrtx,                               "++ONLD-822 JKH 17.02.2017
          ls_salesdata            TYPE bapie1mvkert,
          ls_salesdatax	          TYPE bapie1mvkertx,
          ls_salesext	            TYPE bapie1mvkeextrt,
          ls_salesextx            TYPE bapie1mvkeextrtx,
          ls_posdata              TYPE bapie1wlk2rt,
          ls_posdatax	            TYPE bapie1wlk2rtx,
          ls_plantkeys            TYPE bapie1wrkkey,
          ls_storagelocationkeys  TYPE bapie1lgokey,
          ls_distrchainkeys	      TYPE bapie1vtlkey,
          ls_valuationtypekeys    TYPE bapie1bwakey,
          ls_importextension      TYPE bapiparmatex,

          lv_tabix                TYPE sy-tabix.



    ls_idno_data              = is_idno_data.
    lt_mass_fields[]          = it_mass_fields[].
    ls_prod_data              = is_prod_data.
    ls_reg_data               = is_reg_data.
    ls_reg_data_mass          = is_reg_data_mass.
    lt_marc[]                 = it_marc[].
    lt_mvke[]                 = it_mvke[].
    lt_wlk2[]                 = it_wlk2[].
    lt_host_sap[]             = it_host_sap[].
    lt_tvarvc[]               = it_tvarvc[].
    lt_tvta[]                 = it_tvta[].
    lt_plantdata[]            = ct_plantdata[].
    lt_plantdatax[]           = ct_plantdatax[].
    lt_plantext               = ct_plantext[].                                     "++ONLD-822 JKH 17.02.2017
    lt_plantextx              = ct_plantextx[].                                    "++ONLD-822 JKH 17.02.2017
    lt_valuationdata[]        = ct_valuationdata[].
    lt_valuationdatax[]       = ct_valuationdatax[].
    lt_warehousenumberdata[]  = ct_warehousenumberdata[].                          "++ONLD-822 JKH 17.02.2017
    lt_warehousenumberdatax[] = ct_warehousenumberdatax[].                         "++ONLD-822 JKH 17.02.2017
    lt_salesdata[]            = ct_salesdata[].
    lt_salesdatax[]           = ct_salesdatax[].
    lt_salesext[]             = ct_salesext[].
    lt_salesextx[]            = ct_salesextx[].
    lt_posdata[]              = ct_posdata[].
    lt_posdatax[]             = ct_posdatax[].
    lt_plantkeys[]            = ct_plantkeys[].
    lt_storagelocationkeys[]  = ct_storagelocationkeys[].
    lt_distrchainkeys[]       = ct_distrchainkeys[].
    lt_valuationtypekeys[]    = ct_valuationtypekeys[].
    lt_importextension[]      = ct_importextension[].



**********************************************************************************

    LOOP AT lt_plantdata[] INTO ls_plantdata.

      CLEAR lv_tabix.
      lv_tabix = sy-tabix.


      CLEAR ls_tvarvc.
      READ TABLE lt_tvarvc INTO ls_tvarvc
      WITH KEY high = ls_plantdata-plant.
      IF sy-subrc = 0.

        CLEAR lv_werks.
        lv_werks = ls_tvarvc-low.


* Remove Entries which are not in MASS Data
        CLEAR ls_reg_banner_mass.
        READ TABLE ls_reg_data_mass-zarn_reg_banner INTO ls_reg_banner_mass
        WITH KEY idno   = ls_idno_data-idno
                 banner = lv_werks.
        IF sy-subrc NE 0.
          DELETE lt_plantdata[] INDEX lv_tabix.

          DELETE lt_plantext[]            WHERE material = ls_plantdata-material AND
                                                plant    = ls_plantdata-plant.        "++ONLD-822 JKH 17.02.2017
          DELETE lt_valuationdata[]       WHERE material = ls_plantdata-material AND
                                                val_area = ls_plantdata-plant.
          DELETE lt_plantkeys[]           WHERE material = ls_plantdata-material AND
                                                plant    = ls_plantdata-plant.
          DELETE lt_valuationtypekeys[]   WHERE material = ls_plantdata-material AND
                                              val_area = ls_plantdata-plant.
          CONTINUE.
        ENDIF.

* Remove Entries which are not in MARC
        CLEAR ls_marc.
        READ TABLE lt_marc[] INTO ls_marc
        WITH KEY matnr = ls_idno_data-sap_article
                 werks = ls_plantdata-plant.
        IF sy-subrc NE 0.
* Record doesn't exist in Article Site Data (MARC): Article &, Site &
          CLEAR ls_msg.
          ls_msg-objkey = 'ZARENA'.
          ls_msg-msgty  = 'E'.
          ls_msg-msgid  = 'ZARENA_MSG'.
          ls_msg-msgno  = '095'.
          ls_msg-msgv1  = ls_idno_data-sap_article.
          ls_msg-msgv2  = ls_plantdata-plant.
* ls_msg-msgv3  =
* ls_msg-MSGV4  =
          APPEND ls_msg TO ct_msg[].


          DELETE lt_plantdata[] INDEX lv_tabix.

          DELETE lt_plantext[]            WHERE material = ls_plantdata-material AND
                                                plant    = ls_plantdata-plant.        "++ONLD-822 JKH 17.02.2017
          DELETE lt_valuationdata[]       WHERE material = ls_plantdata-material AND
                                                val_area = ls_plantdata-plant.
          DELETE lt_plantkeys[]           WHERE material = ls_plantdata-material AND
                                                plant    = ls_plantdata-plant.
          DELETE lt_valuationtypekeys[]   WHERE material = ls_plantdata-material AND
                                              val_area = ls_plantdata-plant.
        ENDIF.

      ENDIF.  " READ TABLE lt_tvarvc INTO ls_tvarvc
    ENDLOOP.  " LOOP AT lt_plantdata[] INTO ls_plantdata



**********************************************************************************

    LOOP AT lt_salesdata[] INTO ls_salesdata.

      CLEAR lv_tabix.
      lv_tabix = sy-tabix.

      CLEAR ls_tvta.
      READ TABLE lt_tvta[] INTO ls_tvta
      WITH KEY vkorg = ls_salesdata-sales_org
               vtweg = ls_salesdata-distr_chan.
      IF sy-subrc = 0.

* Remove Entries which are not in MASS Data
        CLEAR ls_reg_banner_mass.
        READ TABLE ls_reg_data_mass-zarn_reg_banner INTO ls_reg_banner_mass
        WITH KEY idno   = ls_idno_data-idno
                 banner+0(1) = ls_tvta-vkorg+0(1).
        IF sy-subrc NE 0.
          DELETE lt_salesdata[] INDEX lv_tabix.
          DELETE lt_salesext[]       WHERE material     = ls_salesdata-material AND
                                           sales_org    = ls_salesdata-sales_org AND
                                           distr_chan   = ls_salesdata-distr_chan.
          DELETE lt_posdata[]        WHERE material     = ls_salesdata-material AND
                                           sales_org    = ls_salesdata-sales_org AND
                                           distr_chan   = ls_salesdata-distr_chan.
          DELETE lt_distrchainkeys[] WHERE material     = ls_salesdata-material  AND
                                           sales_org    = ls_salesdata-sales_org AND
                                           distr_chan   = ls_salesdata-distr_chan.
          CONTINUE.
        ENDIF.

* Remove Entries which are not in MVKE
        CLEAR ls_mvke.
        READ TABLE lt_mvke[] INTO ls_mvke
        WITH TABLE KEY matnr = ls_salesdata-material
                       vkorg = ls_salesdata-sales_org
                       vtweg = ls_salesdata-distr_chan.
        IF sy-subrc NE 0.
* Record doesn't exist in Article Sales Data (MVKE): Article &, &/&
          CLEAR ls_msg.
          ls_msg-objkey = 'ZARENA'.
          ls_msg-msgty  = 'E'.
          ls_msg-msgid  = 'ZARENA_MSG'.
          ls_msg-msgno  = '096'.
          ls_msg-msgv1  = ls_idno_data-sap_article.
          ls_msg-msgv2  = ls_salesdata-sales_org.
          ls_msg-msgv3  = ls_salesdata-distr_chan.
* ls_msg-MSGV4  =
          APPEND ls_msg TO ct_msg[].


          DELETE lt_salesdata[] INDEX lv_tabix.
          DELETE lt_salesext[]       WHERE material     = ls_salesdata-material AND
                                           sales_org    = ls_salesdata-sales_org AND
                                           distr_chan   = ls_salesdata-distr_chan.
          DELETE lt_distrchainkeys[] WHERE material     = ls_salesdata-material  AND
                                           sales_org    = ls_salesdata-sales_org AND
                                           distr_chan   = ls_salesdata-distr_chan.
        ENDIF.

* Remove Entries which are not in WLK2
        CLEAR ls_wlk2.
        READ TABLE lt_wlk2[] INTO ls_wlk2
        WITH TABLE KEY matnr = ls_salesdata-material
                       vkorg = ls_salesdata-sales_org
                       vtweg = ls_salesdata-distr_chan.
        IF sy-subrc NE 0.
* Record doesn't exist in Article POS Data (WLK2): Article &, &/&
          CLEAR ls_msg.
          ls_msg-objkey = 'ZARENA'.
          ls_msg-msgty  = 'E'.
          ls_msg-msgid  = 'ZARENA_MSG'.
          ls_msg-msgno  = '097'.
          ls_msg-msgv1  = ls_idno_data-sap_article.
          ls_msg-msgv2  = ls_salesdata-sales_org.
          ls_msg-msgv3  = ls_salesdata-distr_chan.
* ls_msg-MSGV4  =
          APPEND ls_msg TO ct_msg[].


          DELETE lt_posdata[] WHERE material     = ls_salesdata-material AND
                                    sales_org    = ls_salesdata-sales_org AND
                                    distr_chan   = ls_salesdata-distr_chan.
        ENDIF.


      ENDIF.  " READ TABLE lt_tvta[] INTO ls_tvta
    ENDLOOP.  " LOOP AT lt_salesdata[] INTO ls_salesdata


**********************************************************************************

    CLEAR lv_append.

*** PLANTDATAX
    IF lt_plantdata[] IS NOT INITIAL.
      LOOP AT lt_plantdata[] INTO ls_plantdata.

        CLEAR ls_plantdatax.
        ls_plantdatax-function = ls_plantdata-function.
        ls_plantdatax-material = ls_plantdata-material.
        ls_plantdatax-plant    = ls_plantdata-plant.

        LOOP AT lt_mass_fields[] INTO ls_mass_fields WHERE table = 'ZARN_REG_BANNER'.
          CASE ls_mass_fields-field.
            WHEN 'SOURCE_UNI'.
              READ TABLE lt_tvarvc TRANSPORTING NO FIELDS
              WITH KEY high = ls_plantdata-plant
                       name = mc_vkorg_uni.
              IF sy-subrc = 0.
                ls_plantdatax-sup_source = abap_true.
              ENDIF.

              lv_append = abap_true.

            WHEN 'SOURCE_LNI'.
              READ TABLE lt_tvarvc TRANSPORTING NO FIELDS
              WITH KEY high = ls_plantdata-plant
                       name = mc_vkorg_lni.
              IF sy-subrc = 0.
                ls_plantdatax-sup_source = abap_true.
              ENDIF.

              lv_append = abap_true.


          ENDCASE.
        ENDLOOP.  " LOOP AT lt_mass_fields[] INTO ls_mass_fields

        IF lv_append = abap_true.
          APPEND ls_plantdatax TO lt_plantdatax[].
          ev_post = abap_true.


*** VALUATIONDATAX
          CLEAR ls_valuationdatax.
          ls_valuationdatax-function   = ls_plantdata-function.
          ls_valuationdatax-material   = ls_plantdata-material.
          ls_valuationdatax-val_area   = ls_plantdata-plant.
          ls_valuationdatax-val_type   = space.
*  ls_valuationdatax-price_ctrl = abap_true.
*  ls_valuationdatax-price_unit = abap_true.
*  ls_valuationdatax-val_class  = abap_true.
          APPEND ls_valuationdatax TO lt_valuationdatax[].
        ENDIF.  " IF lv_append = abap_true.


      ENDLOOP.  " LOOP AT lt_plantdata[] INTO ls_plantdata
    ENDIF.  " IF lt_plantdata[] IS NOT INITIAL



**********************************************************************************



    CLEAR lv_append.

*** PLANTEXTX
    IF lt_plantext[] IS NOT INITIAL.
      LOOP AT lt_plantext[] INTO ls_plantext.

*** PLANTEXTX
        CLEAR: ls_bapi_plantextx.
        ls_bapi_plantextx-function   = ls_plantext-function.
        ls_bapi_plantextx-material   = ls_plantext-material.
        ls_bapi_plantextx-plant      = ls_plantext-plant.

        LOOP AT lt_mass_fields[] INTO ls_mass_fields WHERE table = 'ZARN_REG_BANNER'.
          CASE ls_mass_fields-field.
            WHEN 'ONLINE_STATUS'.
              ls_bapi_plantextx-zzonline_status = abap_true.
              lv_append = abap_true.

            WHEN 'PBS_CODE'.
              ls_bapi_plantextx-zz_pbs = abap_true.
              lv_append = abap_true.
          ENDCASE.
        ENDLOOP.  " LOOP AT lt_mass_fields[] INTO ls_mass_fields

        CLEAR lv_string.
        cl_abap_container_utilities=>fill_container_c(
          EXPORTING im_value = ls_bapi_plantextx-datax
          IMPORTING ex_container = lv_string ).

        CLEAR ls_data_plant.
        ls_data_plant-field1(30) = 'ZMD_S_BAPI_PLANTEXTX'.
        ls_data_plant+30 = lv_string.

        CLEAR ls_plantextx.
        ls_plantextx-function   = ls_plantext-function.
        ls_plantextx-material   = ls_plantext-material.
        ls_plantextx-plant      = ls_plantext-plant.
        ls_plantextx-field1     = ls_data_plant-field1.
        ls_plantextx-field2     = ls_data_plant-field2.
        ls_plantextx-field3     = ls_data_plant-field3.
        ls_plantextx-field4     = ls_data_plant-field4.

        IF lv_append = abap_true.
          APPEND ls_plantextx TO lt_plantextx[].
          ev_post = abap_true.
        ENDIF.

      ENDLOOP.  " LOOP AT lt_plantext[] INTO ls_plantext

    ENDIF.  " IF lt_plantext[] IS NOT INITIAL


**********************************************************************************

    CLEAR lv_append.

    IF lt_plantextx[] IS NOT INITIAL.
*** WAREHOUSENUMBERDATAX
      LOOP AT lt_warehousenumberdata INTO ls_warehousenumberdata.

        CLEAR ls_mass_fields .
        READ TABLE lt_mass_fields[] INTO ls_mass_fields
        WITH KEY table = 'ZARN_REG_BANNER'
                 field = 'ONLINE_STATUS'.
        IF sy-subrc = 0.
          CLEAR ls_warehousenumberdatax.
          ls_warehousenumberdatax-function = ls_warehousenumberdata-function.
          ls_warehousenumberdatax-material = ls_warehousenumberdata-material.
          ls_warehousenumberdatax-whse_no  = ls_warehousenumberdata-whse_no.

          lv_append = abap_true.
        ENDIF.

        IF lv_append = abap_true.
          APPEND ls_warehousenumberdatax TO lt_warehousenumberdatax[].
          ev_post = abap_true.
        ENDIF.

      ENDLOOP.  " LOOP AT lt_warehousenumberdata INTO ls_warehousenumberdata
    ENDIF.  " IF lt_plantextx[] IS NOT INITIAL
**********************************************************************************


    CLEAR lv_append.

*** SALESDATAX
    IF lt_salesdata[] IS NOT INITIAL.
      LOOP AT lt_salesdata[] INTO ls_salesdata.

        CLEAR ls_salesdatax.
        ls_salesdatax-function   = ls_salesdata-function.
        ls_salesdatax-material   = ls_salesdata-material.
        ls_salesdatax-sales_org  = ls_salesdata-sales_org.
        ls_salesdatax-distr_chan = ls_salesdata-distr_chan.

        LOOP AT lt_mass_fields[] INTO ls_mass_fields WHERE table = 'ZARN_REG_BANNER'.
          CASE ls_mass_fields-field.
            WHEN 'VERSG'.              ls_salesdatax-matl_stats     =  abap_true.       lv_append = abap_true.
            WHEN 'AUMNG'.              ls_salesdatax-min_order      =  abap_true.       lv_append = abap_true.
            WHEN 'VRKME'.
              ls_salesdatax-sales_unit     =  abap_true.
              ls_salesdatax-sales_unit_iso =  abap_true.
              lv_append = abap_true.

            WHEN 'PRODH'.              ls_salesdatax-prod_hier      =  abap_true.       lv_append = abap_true.
            WHEN 'KTGRM'.              ls_salesdatax-acct_assgt     =  abap_true.       lv_append = abap_true.
            WHEN 'SSTUF'.              ls_salesdatax-assort_lev     =  abap_true.       lv_append = abap_true.
            WHEN 'MVGR3'.              ls_salesdatax-matl_grp_3     =  abap_true.       lv_append = abap_true.     " ++CI17-343 JKH 10.11.2016
          ENDCASE.
        ENDLOOP.  " LOOP AT lt_mass_fields[] INTO ls_mass_fields

        IF lv_append = abap_true.
          APPEND ls_salesdatax TO lt_salesdatax[].
          ev_post = abap_true.
        ENDIF.

      ENDLOOP.  " LOOP AT lt_salesdata[] INTO ls_salesdata
    ENDIF.  " IF lt_salesdata[] IS NOT INITIAL

**********************************************************************************

    CLEAR lv_append.

*** SALESEXTX
    IF lt_salesext[] IS NOT INITIAL.
      LOOP AT lt_salesext[] INTO ls_salesext.

*** SALESEXTX
        CLEAR: ls_bapi_salesextx.
        ls_bapi_salesextx-function   = ls_salesext-function.
        ls_bapi_salesextx-material   = ls_salesext-material.
        ls_bapi_salesextx-sales_org  = ls_salesext-sales_org.
        ls_bapi_salesextx-distr_chan = ls_salesext-distr_chan.

        LOOP AT lt_mass_fields[] INTO ls_mass_fields WHERE table = 'ZARN_REG_BANNER'.
          CASE ls_mass_fields-field.
            WHEN 'ZZCATMAN'.
              ls_bapi_salesextx-zzcatman = abap_true.
              lv_append = abap_true.

          ENDCASE.
        ENDLOOP.  " LOOP AT lt_mass_fields[] INTO ls_mass_fields

        CLEAR lv_string.
        cl_abap_container_utilities=>fill_container_c(
          EXPORTING im_value = ls_bapi_salesextx-datax
          IMPORTING ex_container = lv_string ).

        CLEAR ls_data_sales.
        ls_data_sales-field1(30) = 'ZMD_S_BAPI_SALESEXTX'.
        ls_data_sales+30 = lv_string.

        CLEAR ls_salesextx.
        ls_salesextx-function   = ls_salesext-function.
        ls_salesextx-material   = ls_salesext-material.
        ls_salesextx-sales_org  = ls_salesext-sales_org.
        ls_salesextx-distr_chan = ls_salesext-distr_chan.
        ls_salesextx-field1     = ls_data_sales-field1.
        ls_salesextx-field2     = ls_data_sales-field2.
        ls_salesextx-field3     = ls_data_sales-field3.
        ls_salesextx-field4     = ls_data_sales-field4.

        IF lv_append = abap_true.
          APPEND ls_salesextx TO lt_salesextx[].
          ev_post = abap_true.
        ENDIF.

      ENDLOOP.  " LOOP AT lt_salesext[] INTO ls_salesext
    ENDIF.  " IF lt_salesext[] IS NOT INITIAL

**********************************************************************************

    CLEAR lv_append.

*** POSDATAX
    IF lt_posdata[] IS NOT INITIAL.
      LOOP AT lt_posdata[] INTO ls_posdata.

        CLEAR ls_posdatax.
        ls_posdatax-function   = ls_posdata-function.
        ls_posdatax-material   = ls_posdata-material.
        ls_posdatax-sales_org  = ls_posdata-sales_org.
        ls_posdatax-distr_chan = ls_posdata-distr_chan.

        "**INC6715727 JKH 05.12.2017 change ls_posdata to ls_posdataX
        LOOP AT lt_mass_fields[] INTO ls_mass_fields WHERE table = 'ZARN_REG_BANNER'.
          CASE ls_mass_fields-field.
            WHEN 'QTY_REQUIRED'.       ls_posdatax-no_rep_key   = abap_true.       lv_append = abap_true. "**INC6715727 JKH 05.12.2017
            WHEN 'PRERF'.              ls_posdatax-price_reqd   = abap_true.       lv_append = abap_true. "**INC6715727 JKH 05.12.2017
            WHEN 'PNS_TPR_FLAG'.       ls_posdatax-disc_allwd   = abap_true.       lv_append = abap_true. "**INC6715727 JKH 05.12.2017
            WHEN 'SCAGR'.              ls_posdatax-scales_grp   = abap_true.       lv_append = abap_true. "**INC6715727 JKH 05.12.2017
          ENDCASE.
        ENDLOOP.  " LOOP AT lt_mass_fields[] INTO ls_mass_fields

        IF lv_append = abap_true.
          APPEND ls_posdatax TO lt_posdatax[].
          ev_post = abap_true.
        ENDIF.

      ENDLOOP.  " LOOP AT lt_POSDATA[] INTO ls_POSDATA
    ENDIF.  " IF lt_POSDATA[] IS NOT INITIAL

**********************************************************************************

    ct_plantdata            = lt_plantdata.
    ct_plantdatax           = lt_plantdatax.
    ct_plantext             = lt_plantext.                                           "++ONLD-822 JKH 17.02.2017
    ct_plantextx            = lt_plantextx.                                          "++ONLD-822 JKH 17.02.2017
    ct_valuationdata        = lt_valuationdata.
    ct_valuationdatax       = lt_valuationdatax.
    ct_warehousenumberdata  = lt_warehousenumberdata.                               "++ONLD-822 JKH 17.02.2017
    ct_warehousenumberdatax = lt_warehousenumberdatax.                              "++ONLD-822 JKH 17.02.2017
    ct_salesdata            = lt_salesdata.
    ct_salesdatax           = lt_salesdatax.
    ct_salesext             = lt_salesext.
    ct_salesextx            = lt_salesextx.
    ct_posdata              = lt_posdata.
    ct_posdatax             = lt_posdatax.
    ct_plantkeys            = lt_plantkeys.
    ct_storagelocationkeys  = lt_storagelocationkeys.
    ct_distrchainkeys       = lt_distrchainkeys.
    ct_valuationtypekeys    = lt_valuationtypekeys.
    ct_importextension      = lt_importextension.


  ENDMETHOD.


  METHOD filter_reg_header_data.
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             # 07.11.2019
* CHANGE No.       # CIP-148
* DESCRIPTION      # When both UNI and LNI DC flags are blank, reset
*                  # MARA-ZZVAR_WT_FLAG flag
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* DATE             # 11.12.2019
* CHANGE No.       # CIP-148
* DESCRIPTION      # Sync ZARN_REG_HDR and MARA DC flags
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------

    DATA: ls_idno_data            TYPE ty_s_idno_data,
          lt_mass_fields          TYPE ty_t_mass_fields,
          ls_prod_data            TYPE zsarn_prod_data,
          ls_reg_data	            TYPE zsarn_reg_data,
          ls_reg_data_mass        TYPE zsarn_reg_data,
          lt_mara	                TYPE ty_t_mara,
          lt_clientdata	          TYPE bapie1marart_tab,
          lt_clientdatax          TYPE bapie1marartx_tab,
          lt_clientext            TYPE bapie1maraextrt_tab,
          lt_clientextx	          TYPE bapie1maraextrtx_tab,
          lt_addnlclientdata      TYPE bapie1maw1rt_tab,
          lt_addnlclientdatax	    TYPE bapie1maw1rtx_tab,
          lt_materialdescription  TYPE bapie1maktrt_tab,
          lt_plantdata            TYPE bapie1marcrt_tab,
          lt_plantdatax	          TYPE bapie1marcrtx_tab,

          ls_mass_fields          TYPE ty_s_mass_fields,
          ls_mara	                TYPE mara,
          ls_clientdata           TYPE bapie1marart,
          ls_clientdatax          TYPE bapie1marartx,
          ls_clientextx           TYPE bapie1maraextrtx,
          ls_addnlclientdata      TYPE bapie1maw1rt,
          ls_addnlclientdatax     TYPE bapie1maw1rtx,
          ls_materialdescription  TYPE bapie1maktrt,
          ls_plantdata            TYPE bapie1marcrt,
          ls_plantdatax	          TYPE bapie1marcrtx,

          ls_reg_hdr              TYPE zarn_reg_hdr,
          ls_bapie1maraext_fields TYPE zsmd_bapie1maraext_fields,
          ls_bapie1maraext_data   TYPE zsmd_bapie1maraext_data,
          ls_bapi_clientextx      TYPE zmd_s_bapi_clientextx,
          lv_string               TYPE string,
          lv_append               TYPE flag,

          BEGIN OF ls_data,
            field1 TYPE bapie1maraextrtx-field1,
            field2 TYPE bapie1maraextrtx-field2,
            field3 TYPE bapie1maraextrtx-field3,
            field4 TYPE bapie1maraextrtx-field4,
          END OF ls_data.


    FIELD-SYMBOLS: <ls_plantdatax>           TYPE bapie1marcrtx.


    ls_idno_data             = is_idno_data.
    lt_mass_fields[]         = it_mass_fields[].
    ls_prod_data             = is_prod_data.
    ls_reg_data              = is_reg_data.
    ls_reg_data_mass         = is_reg_data_mass.
    lt_mara[]                = it_mara[].
    lt_clientdata[]          = ct_clientdata[].
    lt_clientdatax[]         = ct_clientdatax[].
    lt_clientext[]           = ct_clientext[].
    lt_clientextx[]          = ct_clientextx[].
    lt_addnlclientdata[]     = ct_addnlclientdata[].
    lt_addnlclientdatax[]    = ct_addnlclientdatax[].
    lt_materialdescription[] = ct_materialdescription[].
    lt_plantdata[]           = ct_plantdata[].
    lt_plantdatax[]          = ct_plantdatax[].

**********************************************************************************

* Remove Entries which are not in MARA
    CLEAR ls_mara.
    READ TABLE lt_mara[] INTO ls_mara
    WITH TABLE KEY matnr = ls_idno_data-sap_article.
    IF sy-subrc NE 0.
      DELETE lt_clientdata[]          WHERE material EQ ls_idno_data-sap_article.
      DELETE lt_clientext[]           WHERE material EQ ls_idno_data-sap_article.
      DELETE lt_addnlclientdata[]     WHERE material EQ ls_idno_data-sap_article.
      DELETE lt_materialdescription[] WHERE material EQ ls_idno_data-sap_article.
    ENDIF.



    CLEAR ls_reg_hdr.
    READ TABLE ls_reg_data_mass-zarn_reg_hdr[] INTO ls_reg_hdr
    WITH KEY idno = ls_idno_data-idno.

**********************************************************************************

    CLEAR lv_append.

*** CLIENTDATAX
    IF lt_clientdata[] IS NOT INITIAL.

      LOOP AT lt_clientdata[] INTO ls_clientdata.
        CLEAR ls_clientdatax.
        ls_clientdatax-function = ls_clientdata-function.
        ls_clientdatax-material = ls_clientdata-material.

        LOOP AT lt_mass_fields[] INTO ls_mass_fields WHERE table = 'ZARN_REG_HDR'.
          CASE ls_mass_fields-field.
            WHEN 'INHME'.
              ls_clientdatax-cont_unit        = abap_true.
              ls_clientdatax-cont_unit_iso   = abap_true.
              lv_append = abap_true.

            WHEN 'INHAL'.               ls_clientdatax-net_cont        = abap_true.  lv_append = abap_true.
            WHEN 'RETAIL_NET_WEIGHT'.   ls_clientdatax-net_weight      = abap_true.  lv_append = abap_true.
            WHEN 'STORAGE_TEMP'.        ls_clientdatax-temp_conds      = abap_true.  lv_append = abap_true.
            WHEN 'SAISO'.               ls_clientdatax-season          = abap_true.  lv_append = abap_true.
            WHEN 'PRDHA'.               ls_clientdatax-prod_hier       = abap_true.  lv_append = abap_true.
            WHEN 'MHDRZ'.               ls_clientdatax-minremlife      = abap_true.  lv_append = abap_true.
            WHEN 'SAISJ'.               ls_clientdatax-saeson_yr       = abap_true.  lv_append = abap_true.
            WHEN 'MSTAE'.               ls_clientdatax-pur_status      = abap_true.  lv_append = abap_true.
            WHEN 'MSTAV'.               ls_clientdatax-sal_status      = abap_true.  lv_append = abap_true.
            WHEN 'MSTDE'.               ls_clientdatax-pvalidfrom      = abap_true.  lv_append = abap_true.
            WHEN 'MSTDV'.               ls_clientdatax-svalidfrom      = abap_true.  lv_append = abap_true.
            WHEN 'BSTAT'.               ls_clientdatax-creation_status = abap_true.  lv_append = abap_true.
            WHEN 'BRAND_ID'.            ls_clientdatax-brand_id        = abap_true.  lv_append = abap_true.

            WHEN 'BWSCL'.
              IF <ls_plantdatax> IS ASSIGNED. UNASSIGN <ls_plantdatax>. ENDIF.
              READ TABLE lt_plantdatax[] ASSIGNING <ls_plantdatax>
              WITH KEY material = ls_clientdata-material
                       plant    = 'RFST'.
              IF sy-subrc = 0.
                <ls_plantdatax>-sup_source = abap_true.
              ELSE.
                CLEAR ls_plantdatax.
                ls_plantdatax-function = ls_clientdata-function.
                ls_plantdatax-material = ls_clientdata-material.
                ls_plantdatax-plant    = 'RFST'.

                ls_plantdatax-sup_source = abap_true.
                APPEND ls_plantdatax TO lt_plantdatax[].
              ENDIF.

          ENDCASE.
        ENDLOOP.  " LOOP AT lt_mass_fields[] INTO ls_mass_fields

        IF lv_append = abap_true.
          APPEND ls_clientdatax TO lt_clientdatax[].
          ev_post = abap_true.
        ENDIF.


      ENDLOOP.  " LOOP AT lt_clientdata[] INTO ls_clientdata
    ENDIF.  " IF lt_clientdata[] IS NOT INITIAL

**********************************************************************************

    CLEAR lv_append.

*** CLIENTEXTX
    IF lt_clientext[] IS NOT INITIAL.

      LOOP AT lt_clientext[] ASSIGNING FIELD-SYMBOL(<ls_clientext>).

        CLEAR ls_bapi_clientextx.
        ls_bapi_clientextx-function = <ls_clientext>-function.
        ls_bapi_clientextx-material = <ls_clientext>-material.

        MOVE-CORRESPONDING <ls_clientext> TO ls_bapie1maraext_fields.

        cl_abap_container_utilities=>fill_container_c( EXPORTING im_value     = ls_bapie1maraext_fields
                                                       IMPORTING ex_container = lv_string ).

        cl_abap_container_utilities=>read_container_c( EXPORTING im_container = lv_string
                                                       IMPORTING ex_value     = ls_bapie1maraext_data ).

        LOOP AT lt_mass_fields[] INTO ls_mass_fields WHERE table = 'ZARN_REG_HDR'.
          CASE ls_mass_fields-field.
            WHEN 'ZZPRDTYPE'.           ls_bapi_clientextx-zzprdtype          = abap_true.   lv_append = abap_true.
            WHEN 'ZZBUY_SELL'.          ls_bapi_clientextx-zzbuy_sell         = abap_true.   lv_append = abap_true. "++ONED-217 JKH 24.11.2016
            WHEN 'ZZAS4SUBDEPT'.        ls_bapi_clientextx-zzas4subdept       = abap_true.   lv_append = abap_true.
            WHEN 'ZZVAR_WT_FLAG'.       ls_bapi_clientextx-zzvar_wt_flag      = abap_true.   lv_append = abap_true.
            WHEN 'ZZ_UNI_DC'.           ls_bapi_clientextx-zz_uni_dc          = abap_true.   lv_append = abap_true.
            WHEN 'ZZ_LNI_DC'.           ls_bapi_clientextx-zz_lni_dc          = abap_true.   lv_append = abap_true.
            WHEN 'ZZSELL'.              ls_bapi_clientextx-zzsell             = abap_true.   lv_append = abap_true.
            WHEN 'ZZUSE'.               ls_bapi_clientextx-zzuse              = abap_true.   lv_append = abap_true.
            WHEN 'ZZTKTPRNT'.           ls_bapi_clientextx-zztktprnt          = abap_true.   lv_append = abap_true.
            WHEN 'ZZPEDESC_FLAG'.       ls_bapi_clientextx-zzpedesc_flag      = abap_true.   lv_append = abap_true.
            WHEN 'ZZATTR1'.             ls_bapi_clientextx-zzattr1            = abap_true.   lv_append = abap_true.
            WHEN 'ZZATTR2'.             ls_bapi_clientextx-zzattr2            = abap_true.   lv_append = abap_true.
            WHEN 'ZZATTR3'.             ls_bapi_clientextx-zzattr3            = abap_true.   lv_append = abap_true.
            WHEN 'ZZATTR4'.             ls_bapi_clientextx-zzattr4            = abap_true.   lv_append = abap_true.
            WHEN 'ZZATTR5'.             ls_bapi_clientextx-zzattr5            = abap_true.   lv_append = abap_true.
            WHEN 'ZZATTR6'.             ls_bapi_clientextx-zzattr6            = abap_true.   lv_append = abap_true.
            WHEN 'ZZATTR7'.             ls_bapi_clientextx-zzattr7            = abap_true.   lv_append = abap_true.
            WHEN 'ZZATTR8'.             ls_bapi_clientextx-zzattr8            = abap_true.   lv_append = abap_true.
            WHEN 'ZZATTR9'.             ls_bapi_clientextx-zzattr9            = abap_true.   lv_append = abap_true.
            WHEN 'ZZATTR10'.            ls_bapi_clientextx-zzattr10           = abap_true.   lv_append = abap_true.
            WHEN 'ZZATTR11'.            ls_bapi_clientextx-zzattr11           = abap_true.   lv_append = abap_true.
            WHEN 'ZZATTR12'.            ls_bapi_clientextx-zzattr12           = abap_true.   lv_append = abap_true.
            WHEN 'ZZATTR13'.            ls_bapi_clientextx-zzattr13           = abap_true.   lv_append = abap_true.
            WHEN 'ZZATTR14'.            ls_bapi_clientextx-zzattr14           = abap_true.   lv_append = abap_true.
            WHEN 'ZZATTR15'.            ls_bapi_clientextx-zzattr15           = abap_true.   lv_append = abap_true.
            WHEN 'ZZATTR16'.            ls_bapi_clientextx-zzattr16           = abap_true.   lv_append = abap_true.
            WHEN 'ZZATTR17'.            ls_bapi_clientextx-zzattr17           = abap_true.   lv_append = abap_true.
            WHEN 'ZZATTR18'.            ls_bapi_clientextx-zzattr18           = abap_true.   lv_append = abap_true.
            WHEN 'ZZATTR19'.            ls_bapi_clientextx-zzattr19           = abap_true.   lv_append = abap_true.
            WHEN 'ZZATTR20'.            ls_bapi_clientextx-zzattr20           = abap_true.   lv_append = abap_true.
            WHEN 'ZZATTR21'.            ls_bapi_clientextx-zzattr21           = abap_true.   lv_append = abap_true.
            WHEN 'ZZATTR22'.            ls_bapi_clientextx-zzattr22           = abap_true.   lv_append = abap_true.
            WHEN 'ZZATTR23'.            ls_bapi_clientextx-zzattr23           = abap_true.   lv_append = abap_true.
            WHEN 'ZZSTRD'.              ls_bapi_clientextx-zzstrd             = abap_true.   lv_append = abap_true.
            WHEN 'ZZSCO_WEIGHED_FLAG'.  ls_bapi_clientextx-zzsco_weighed_flag = abap_true.   lv_append = abap_true.
            WHEN 'ZZSTRG'.              ls_bapi_clientextx-zzstrg             = abap_true.   lv_append = abap_true.
            WHEN 'ZZLABNUM'.            ls_bapi_clientextx-zzlabnum           = abap_true.   lv_append = abap_true.
            WHEN 'ZZPKDDT'.             ls_bapi_clientextx-zzpkddt            = abap_true.   lv_append = abap_true.
            WHEN 'ZZMANDST'.            ls_bapi_clientextx-zzmandst           = abap_true.   lv_append = abap_true.
            WHEN 'ZZWNMSG'.             ls_bapi_clientextx-zzwnmsg            = abap_true.   lv_append = abap_true.

            WHEN 'ZZLNI_REPACK'.        ls_bapi_clientextx-zzlni_repack       = abap_true.   lv_append = abap_true.
            WHEN 'ZZLNI_BULK'.          ls_bapi_clientextx-zzlni_bulk         = abap_true.   lv_append = abap_true.
            WHEN 'ZZLNI_PRBOLY'.        ls_bapi_clientextx-zzlni_prboly       = abap_true.   lv_append = abap_true.
            WHEN 'ZZINGRETYPE'.          ls_bapi_clientextx-zzingretype       = abap_true.   lv_append = abap_true.
            WHEN 'ZZRANGE_FLAG'.        ls_bapi_clientextx-zzrange_flag       = abap_true.   lv_append = abap_true.
            WHEN 'ZZRANGE_AVAIL'.       ls_bapi_clientextx-zzrange_avail      = abap_true.   lv_append = abap_true.

          ENDCASE.
        ENDLOOP.  " LOOP AT lt_mass_fields[] INTO ls_mass_fields

        " Sometimes, custom table and standard table might go out of sync especially for DC flags.
        " When at least one of the DC flags has been modified, sync other field as well even though it is not requested
        " for update
        IF ls_bapi_clientextx-zz_uni_dc IS NOT INITIAL OR
           ls_bapi_clientextx-zz_lni_dc IS NOT INITIAL.

          ls_bapi_clientextx-zz_uni_dc  = abap_true.
          ls_bapi_clientextx-zz_lni_dc  = abap_true.

          ls_bapie1maraext_data-zz_uni_dc = ls_reg_hdr-zz_uni_dc. " Latest state after change
          ls_bapie1maraext_data-zz_lni_dc = ls_reg_hdr-zz_lni_dc. " Latest state after change
        ENDIF.

        " When ZZ_UNI_DC and ZZ_LNI_DC are blank it means it is changing source of supply from DC to Vendor. In this case variable flag
        " indicator must to set to blank though it is not requested by user
        IF ls_bapie1maraext_data-zz_uni_dc IS INITIAL AND
           ls_bapie1maraext_data-zz_lni_dc IS INITIAL.
          ls_bapi_clientextx-zzvar_wt_flag = abap_true.
          lv_append = abap_true.
        ENDIF.

        IF ls_reg_hdr-zz_lni_dc = abap_true AND ls_mara-zzlni_repack IS INITIAL.
        ELSE.
          CLEAR: ls_bapi_clientextx-zzlni_repack, ls_bapi_clientextx-zzlni_bulk, ls_bapi_clientextx-zzlni_prboly.
        ENDIF.


        CLEAR lv_string.
        cl_abap_container_utilities=>fill_container_c(
          EXPORTING im_value = ls_bapi_clientextx-datax
          IMPORTING ex_container = lv_string ).

        CLEAR ls_data.
        ls_data-field1(30) = 'ZMD_S_BAPI_CLIENTEXTX'.
        ls_data+30 = lv_string.

        ls_clientextx-function = ls_bapi_clientextx-function.
        ls_clientextx-material = ls_bapi_clientextx-material.
        ls_clientextx-field1   = ls_data-field1.
        ls_clientextx-field2   = ls_data-field2.
        ls_clientextx-field3   = ls_data-field3.
        ls_clientextx-field4   = ls_data-field4.

        IF lv_append = abap_true.
          APPEND ls_clientextx TO lt_clientextx[].
          ev_post = abap_true.
        ENDIF.

        " Modify the updated data
        cl_abap_container_utilities=>fill_container_c( EXPORTING im_value     = ls_bapie1maraext_data
                                                       IMPORTING ex_container = lv_string ).

        cl_abap_container_utilities=>read_container_c( EXPORTING im_container = lv_string
                                                       IMPORTING ex_value     = ls_bapie1maraext_fields ).
        MOVE-CORRESPONDING ls_bapie1maraext_fields TO <ls_clientext>.

      ENDLOOP.  " LOOP AT lt_clientdata[] INTO ls_clientdata
    ENDIF.  " IF lt_clientdata[] IS NOT INITIAL

    CLEAR lv_append.

*** MATERIALDESCRIPTION
    IF lt_materialdescription[] IS NOT INITIAL.

      CLEAR ls_mass_fields.
      READ TABLE lt_mass_fields[] INTO ls_mass_fields
      WITH KEY table = 'ZARN_REG_HDR'
               field = 'RETAIL_UNIT_DESC'.
      IF sy-subrc NE 0.
        CLEAR: lt_materialdescription[].
      ELSE.
        ev_post = abap_true.
      ENDIF.

    ENDIF.  " IF lt_materialdescription[] IS NOT INITIAL

**********************************************************************************


    ct_clientdata[]          = lt_clientdata[].
    ct_clientdatax[]         = lt_clientdatax[].
    ct_clientext[]           = lt_clientext[].
    ct_clientextx[]          = lt_clientextx[].
    ct_addnlclientdata[]     = lt_addnlclientdata[].
    ct_addnlclientdatax[]    = lt_addnlclientdatax[].
    ct_materialdescription[] = lt_materialdescription[].
    ct_plantdata[]           = lt_plantdata[].
    ct_plantdatax[]          = lt_plantdatax[].


  ENDMETHOD.


  METHOD filter_reg_pir_data.

    DATA: ls_idno_data           TYPE ty_s_idno_data,
          lt_mass_fields         TYPE ty_t_mass_fields,
          ls_prod_data           TYPE zsarn_prod_data,
          ls_reg_data	           TYPE zsarn_reg_data,
          ls_reg_data_mass       TYPE zsarn_reg_data,
          ls_msg                 TYPE massmsg,

          lt_eina                TYPE ty_t_eina,
          lt_eine                TYPE ty_t_eine,
          lt_marm                TYPE ty_t_marm,
          lt_t006	               TYPE ty_t_t006,
          lt_tcurc               TYPE ty_t_tcurc,

          lt_inforecord_general	 TYPE wrf_bapieina_tty,
          lt_inforecord_purchorg TYPE wrf_bapieine_tty,
          lt_calp_vb             TYPE calp_vb_tab,
          lt_clientdata	         TYPE bapie1marart_tab,
          lt_clientdatax         TYPE bapie1marartx_tab,
          lt_addnlclientdata     TYPE bapie1maw1rt_tab,
          lt_addnlclientdatax	   TYPE bapie1maw1rtx_tab,


          ls_mass_fields         TYPE ty_s_mass_fields,
          ls_reg_pir_mass        TYPE zarn_reg_pir,
          ls_reg_pir_mass_eina   TYPE zarn_reg_pir,
          ls_reg_pir_mass_eine   TYPE zarn_reg_pir,
          ls_uom_variant         TYPE zarn_uom_variant,
          ls_reg_uom             TYPE zarn_reg_uom,
          ls_eina                TYPE eina,
          ls_eine                TYPE eine,
          ls_marm                TYPE marm,
          ls_t006                TYPE ty_s_t006,
          ls_tcurc               TYPE ty_s_tcurc,
          ls_inforecord_general	 TYPE bapieina,
          ls_inforecord_purchorg TYPE wrfbapieine,
          ls_calp_vb             TYPE calp_vb,
          ls_clientdata	         TYPE bapie1marart,
          ls_clientdatax         TYPE bapie1marartx,
          ls_addnlclientdata     TYPE bapie1maw1rt,
          ls_addnlclientdatax	   TYPE bapie1maw1rtx,

          lv_append              TYPE flag,
          lv_tabix               TYPE sy-tabix.

    FIELD-SYMBOLS: <ls_inforecord_general>  TYPE bapieina,
                   <ls_inforecord_purchorg> TYPE wrfbapieine,
                   <ls_clientdatax>         TYPE bapie1marartx,
                   <ls_addnlclientdatax>    TYPE bapie1maw1rtx.

**********************************************************************************

    ls_idno_data             = is_idno_data.
    lt_mass_fields[]         = it_mass_fields[].
    ls_prod_data             = is_prod_data.
    ls_reg_data              = is_reg_data.
    ls_reg_data_mass         = is_reg_data_mass.
    lt_eina[]                = it_eina[].
    lt_eine[]                = it_eine[].
    lt_marm[]                = it_marm[].
    lt_t006[]                = it_t006[].
    lt_tcurc[]               = it_tcurc[].
    lt_inforecord_general[]  = ct_inforecord_general[].
    lt_inforecord_purchorg[] = ct_inforecord_purchorg[].
    lt_calp_vb[]             = ct_calp_vb[].
    lt_clientdata[]          = ct_clientdata[].
    lt_clientdatax[]         = ct_clientdatax[].
    lt_addnlclientdata[]     = ct_addnlclientdata[].
    lt_addnlclientdatax[]    = ct_addnlclientdatax[].


**********************************************************************************



    IF lt_inforecord_general[] IS NOT INITIAL.
      LOOP AT lt_inforecord_general[] ASSIGNING <ls_inforecord_general>.

        CLEAR lv_tabix.
        lv_tabix = sy-tabix.

        IF <ls_inforecord_purchorg> IS ASSIGNED. UNASSIGN <ls_inforecord_purchorg>. ENDIF.
        READ TABLE lt_inforecord_purchorg[] ASSIGNING <ls_inforecord_purchorg>
        WITH KEY material = <ls_inforecord_general>-material
                 vendor   = <ls_inforecord_general>-vendor.
        IF sy-subrc NE 0.
          DELETE lt_inforecord_purchorg[] WHERE material = <ls_inforecord_general>-material
                                            AND vendor   = <ls_inforecord_general>-vendor.
          DELETE lt_calp_vb[] WHERE matnr = <ls_inforecord_general>-material
                                AND lifnr = <ls_inforecord_general>-vendor.
          DELETE lt_inforecord_general[] INDEX lv_tabix.
          CONTINUE.
        ENDIF.

* Remove Entries which are not in MASS Data
        CLEAR ls_reg_pir_mass.
        READ TABLE ls_reg_data_mass-zarn_reg_pir INTO ls_reg_pir_mass
        WITH KEY idno         = ls_idno_data-idno
                 lifnr        = <ls_inforecord_general>-vendor.
        IF sy-subrc NE 0.
          DELETE lt_inforecord_purchorg[] WHERE material = <ls_inforecord_general>-material
                                            AND vendor   = <ls_inforecord_general>-vendor.
          DELETE lt_calp_vb[] WHERE matnr = <ls_inforecord_general>-material
                                AND lifnr = <ls_inforecord_general>-vendor.
          DELETE lt_inforecord_general[] INDEX lv_tabix.
          CONTINUE.
        ENDIF.

* Remove Entries which are not in EINA/EINE
        CLEAR ls_eina.
        READ TABLE lt_eina[] INTO ls_eina
        WITH KEY matnr = <ls_inforecord_general>-material
                 lifnr = <ls_inforecord_general>-vendor.
        IF sy-subrc = 0.
          CLEAR ls_eine.
          READ TABLE lt_eine[] INTO ls_eine
          WITH KEY infnr = ls_eina-infnr.
          IF sy-subrc NE 0.
* Record doesn't exist in PIR Gen/Purch (EINA/EINE): Article &, Vendor &
            CLEAR ls_msg.
            ls_msg-objkey = 'ZARENA'.
            ls_msg-msgty  = 'E'.
            ls_msg-msgid  = 'ZARENA_MSG'.
            ls_msg-msgno  = '098'.
            ls_msg-msgv1  = ls_idno_data-sap_article.
            ls_msg-msgv2  = <ls_inforecord_general>-vendor.
* ls_msg-msgv3  =
* ls_msg-MSGV4  =
            APPEND ls_msg TO ct_msg[].


            DELETE lt_inforecord_purchorg[] WHERE material = <ls_inforecord_general>-material
                                              AND vendor   = <ls_inforecord_general>-vendor.
            DELETE lt_calp_vb[] WHERE matnr = <ls_inforecord_general>-material
                                  AND lifnr = <ls_inforecord_general>-vendor.
            DELETE lt_inforecord_general[] INDEX lv_tabix.
            CONTINUE.

          ENDIF.
        ELSE.
* Record doesn't exist in PIR Gen/Purch (EINA/EINE): Article &, Vendor &
          CLEAR ls_msg.
          ls_msg-objkey = 'ZARENA'.
          ls_msg-msgty  = 'E'.
          ls_msg-msgid  = 'ZARENA_MSG'.
          ls_msg-msgno  = '098'.
          ls_msg-msgv1  = ls_idno_data-sap_article.
          ls_msg-msgv2  = <ls_inforecord_general>-vendor.
* ls_msg-msgv3  =
* ls_msg-MSGV4  =
          APPEND ls_msg TO ct_msg[].


          DELETE lt_inforecord_purchorg[] WHERE material = <ls_inforecord_general>-material
                                            AND vendor   = <ls_inforecord_general>-vendor.
          DELETE lt_calp_vb[] WHERE matnr = <ls_inforecord_general>-material
                                AND lifnr = <ls_inforecord_general>-vendor.
          DELETE lt_inforecord_general[] INDEX lv_tabix.
          CONTINUE.

        ENDIF.


* Check that EINA unit exist in MARM
        CLEAR ls_marm.
        READ TABLE lt_marm TRANSPORTING NO FIELDS
        WITH TABLE KEY matnr = <ls_inforecord_general>-material
                       meinh = <ls_inforecord_general>-po_unit.
        IF sy-subrc NE 0.
* Order Unit & doesn't exist in Article UoM (MARM): Article &
          CLEAR ls_msg.
          ls_msg-objkey = 'ZARENA'.
          ls_msg-msgty  = 'E'.
          ls_msg-msgid  = 'ZARENA_MSG'.
          ls_msg-msgno  = '104'.
          ls_msg-msgv1  = <ls_inforecord_general>-po_unit.
          ls_msg-msgv2  = ls_idno_data-sap_article.
* ls_msg-msgv3  =
* ls_msg-MSGV4  =
          APPEND ls_msg TO ct_msg[].


          DELETE lt_inforecord_purchorg[] WHERE material = <ls_inforecord_general>-material
                                            AND vendor   = <ls_inforecord_general>-vendor.
          DELETE lt_calp_vb[] WHERE matnr = <ls_inforecord_general>-material
                                AND lifnr = <ls_inforecord_general>-vendor.
          DELETE lt_inforecord_general[] INDEX lv_tabix.
          CONTINUE.
        ENDIF.

* Check that EINE unit exist in MARM
        CLEAR ls_marm.
        READ TABLE lt_marm TRANSPORTING NO FIELDS
        WITH TABLE KEY matnr = <ls_inforecord_purchorg>-material
                       meinh = <ls_inforecord_purchorg>-orderpr_un.
        IF sy-subrc NE 0.
* Order Price Unit & doesn't exist in Article UoM (MARM): Article &
          CLEAR ls_msg.
          ls_msg-objkey = 'ZARENA'.
          ls_msg-msgty  = 'E'.
          ls_msg-msgid  = 'ZARENA_MSG'.
          ls_msg-msgno  = '105'.
          ls_msg-msgv1  = <ls_inforecord_purchorg>-orderpr_un.
          ls_msg-msgv2  = ls_idno_data-sap_article.
* ls_msg-msgv3  =
* ls_msg-MSGV4  =
          APPEND ls_msg TO ct_msg[].


          DELETE lt_inforecord_purchorg[] WHERE material = <ls_inforecord_general>-material
                                            AND vendor   = <ls_inforecord_general>-vendor.
          DELETE lt_calp_vb[] WHERE matnr = <ls_inforecord_general>-material
                                AND lifnr = <ls_inforecord_general>-vendor.
          DELETE lt_inforecord_general[] INDEX lv_tabix.
          CONTINUE.
        ENDIF.


* Check that EINA base unit exist in MARM
        CLEAR ls_marm.
        READ TABLE lt_marm TRANSPORTING NO FIELDS
        WITH TABLE KEY matnr = <ls_inforecord_general>-material
                       meinh = <ls_inforecord_general>-base_uom.
        IF sy-subrc NE 0.
* Base Unit & doesn't exist in Article UoM (MARM): Article &
          CLEAR ls_msg.
          ls_msg-objkey = 'ZARENA'.
          ls_msg-msgty  = 'E'.
          ls_msg-msgid  = 'ZARENA_MSG'.
          ls_msg-msgno  = '106'.
          ls_msg-msgv1  = <ls_inforecord_general>-base_uom.
          ls_msg-msgv2  = ls_idno_data-sap_article.
* ls_msg-msgv3  =
* ls_msg-MSGV4  =
          APPEND ls_msg TO ct_msg[].


          DELETE lt_inforecord_purchorg[] WHERE material = <ls_inforecord_general>-material
                                            AND vendor   = <ls_inforecord_general>-vendor.
          DELETE lt_calp_vb[] WHERE matnr = <ls_inforecord_general>-material
                                AND lifnr = <ls_inforecord_general>-vendor.
          DELETE lt_inforecord_general[] INDEX lv_tabix.
          CONTINUE.
        ENDIF.


        CLEAR lv_append.

* Get existing values from EINA
        <ls_inforecord_general>-var_ord_un   = ls_eina-vabme.
        <ls_inforecord_general>-suppl_from   = ls_eina-lifab.
        <ls_inforecord_general>-suppl_to     = ls_eina-lifbi.
        <ls_inforecord_general>-norm_vend    = ls_eina-relif.

        <ls_inforecord_general>-vend_mat     = ls_eina-idnlf.

        <ls_inforecord_general>-po_unit      = ls_eina-meins.
        <ls_inforecord_general>-conv_num1    = ls_eina-umren.
        <ls_inforecord_general>-conv_den1    = ls_eina-umrez.
        <ls_inforecord_general>-base_uom     = ls_eina-lmein.


* Get new EINA record from Mass data
        CLEAR ls_reg_pir_mass_eina.
        READ TABLE ls_reg_data_mass-zarn_reg_pir INTO ls_reg_pir_mass_eina
        WITH KEY idno         = ls_idno_data-idno
                 lifnr        = <ls_inforecord_general>-vendor
                 pir_rel_eina = abap_true.
        IF sy-subrc = 0.  "++JKH INC5555469 26.01.2007

          LOOP AT lt_mass_fields[] INTO ls_mass_fields WHERE table = 'ZARN_REG_PIR'.
            CASE ls_mass_fields-field.
              WHEN 'VABME'.          <ls_inforecord_general>-var_ord_un   = ls_reg_pir_mass_eina-vabme. lv_append = abap_true.
              WHEN 'LIFAB'.          <ls_inforecord_general>-suppl_from   = ls_reg_pir_mass_eina-lifab. lv_append = abap_true.
              WHEN 'LIFBI'.          <ls_inforecord_general>-suppl_to     = ls_reg_pir_mass_eina-lifbi. lv_append = abap_true.
              WHEN 'RELIF'.
                <ls_inforecord_general>-norm_vend    = ls_reg_pir_mass_eina-relif. lv_append = abap_true.

                IF <ls_clientdatax> IS ASSIGNED. UNASSIGN <ls_clientdatax>. ENDIF.
                READ TABLE lt_clientdatax[] ASSIGNING <ls_clientdatax>
                WITH KEY material = ls_idno_data-sap_article.
                IF sy-subrc = 0.
                  <ls_clientdatax>-countryori     = abap_true.
                  <ls_clientdatax>-countryori_iso = abap_true.
                  ev_post = abap_true.
                ELSE.
                  CLEAR ls_clientdatax.
                  ls_clientdatax-function     = '005'.
                  ls_clientdatax-material     = <ls_inforecord_general>-material.

                  ls_clientdatax-countryori     = abap_true.
                  ls_clientdatax-countryori_iso = abap_true.
                  APPEND ls_clientdatax TO lt_clientdatax[].
                  ev_post = abap_true.
                ENDIF.


                IF <ls_addnlclientdatax> IS ASSIGNED. UNASSIGN <ls_addnlclientdatax>. ENDIF.
                READ TABLE lt_addnlclientdatax[] ASSIGNING <ls_addnlclientdatax>
                WITH KEY material = ls_idno_data-sap_article.
                IF sy-subrc = 0.
                  <ls_addnlclientdatax>-countryori     = abap_true.
                  <ls_addnlclientdatax>-countryori_iso = abap_true.
                  ev_post = abap_true.
                ELSE.
                  CLEAR ls_addnlclientdatax.
                  ls_addnlclientdatax-function     = '005'.
                  ls_addnlclientdatax-material     = <ls_inforecord_general>-material.

                  ls_addnlclientdatax-countryori     = abap_true.
                  ls_addnlclientdatax-countryori_iso = abap_true.
                  APPEND ls_addnlclientdatax TO lt_addnlclientdatax[].
                  ev_post = abap_true.
                ENDIF.



            ENDCASE.
          ENDLOOP.  " LOOP AT lt_mass_fields[] INTO ls_mass_fields



* If EINA Unit changes
          IF ls_reg_pir_mass_eina-bprme NE ls_eina-meins AND ls_reg_pir_mass_eina IS NOT INITIAL.
            <ls_inforecord_general>-po_unit      = ls_reg_pir_mass_eina-bprme.                    lv_append = abap_true.

*          CLEAR ls_uom_variant.
*          READ TABLE ls_prod_data-zarn_uom_variant[] INTO ls_uom_variant
*          WITH KEY idno     = ls_prod_data-idno
*                   version  = ls_prod_data-version
*                   uom_code = ls_reg_pir_mass_eina-order_uom_pim.
*          IF sy-subrc = 0.
*            <ls_inforecord_general>-conv_num1    = ls_uom_variant-num_base_units.               lv_append = abap_true.
*            <ls_inforecord_general>-conv_den1    = ls_uom_variant-factor_of_base_units.         lv_append = abap_true.
*          ENDIF.

            CLEAR ls_reg_uom.
            READ TABLE ls_reg_data_mass-zarn_reg_uom[] INTO ls_reg_uom
            WITH KEY  idno                 = ls_reg_data_mass-idno
                      pim_uom_code         = ls_reg_pir_mass_eina-order_uom_pim
                      hybris_internal_code = space
                      lower_uom            = ls_reg_pir_mass_eina-lower_uom
                      lower_child_uom      = ls_reg_pir_mass_eina-lower_child_uom.
            IF sy-subrc = 0.
              <ls_inforecord_general>-conv_num1 = ls_reg_uom-num_base_units.                      lv_append = abap_true.
              <ls_inforecord_general>-conv_den1 = ls_reg_uom-factor_of_base_units.                lv_append = abap_true.
            ENDIF.



            CLEAR ls_reg_uom.
            READ TABLE ls_reg_data_mass-zarn_reg_uom[] INTO ls_reg_uom
            WITH KEY idno      = ls_reg_data_mass-idno
                     unit_base = abap_true.
            IF sy-subrc = 0.
              <ls_inforecord_general>-base_uom     = ls_reg_uom-meinh.                            lv_append = abap_true.
            ENDIF.
          ENDIF.  "  IF ls_reg_pir_mass_eina-bprme NE ls_eina-meins

        ENDIF. " IF sy-subrc = 0, READ TABLE ls_reg_data_mass-zarn_reg_pir INTO ls_reg_pir_mass_eina

* PO_UNIT_ISO
        CLEAR ls_t006.
        READ TABLE lt_t006[] INTO ls_t006
        WITH TABLE KEY msehi = <ls_inforecord_general>-po_unit.
        IF sy-subrc = 0.
          <ls_inforecord_general>-po_unit_iso  = ls_t006-isocode.
        ENDIF.

* BASE_UOM_ISO
        CLEAR ls_t006.
        READ TABLE lt_t006[] INTO ls_t006
        WITH TABLE KEY msehi = <ls_inforecord_general>-base_uom.
        IF sy-subrc = 0.
          <ls_inforecord_general>-base_uom_iso  = ls_t006-isocode.
        ENDIF.

        IF lv_append = abap_true.
          ev_post = abap_true.
        ENDIF.








        CLEAR lv_append.

        <ls_inforecord_purchorg>-info_type  = ls_eine-esokz.
        <ls_inforecord_purchorg>-delete_ind = ls_eine-loekz.
        <ls_inforecord_purchorg>-pur_group  = ls_eine-ekgrp.
        <ls_inforecord_purchorg>-min_po_qty = ls_eine-minbm.
        <ls_inforecord_purchorg>-nrm_po_qty = ls_eine-norbm.
        <ls_inforecord_purchorg>-plnd_delry = ls_eine-aplfz.
        <ls_inforecord_purchorg>-cond_group = ls_eine-ekkol.
        <ls_inforecord_purchorg>-tax_code   = ls_eine-mwskz.
        <ls_inforecord_purchorg>-overdeltol = ls_eine-uebto.
        <ls_inforecord_purchorg>-under_tol  = ls_eine-untto.


* Get new EINE record from Mass data
        CLEAR ls_reg_pir_mass_eine.
        READ TABLE ls_reg_data_mass-zarn_reg_pir INTO ls_reg_pir_mass_eine
        WITH KEY idno         = ls_idno_data-idno
                 lifnr        = <ls_inforecord_general>-vendor
                 pir_rel_eine = abap_true.
        IF sy-subrc = 0.  "++JKH INC5555469 26.01.2007

          LOOP AT lt_mass_fields[] INTO ls_mass_fields WHERE table = 'ZARN_REG_PIR'.
            CASE ls_mass_fields-field.
              WHEN 'ESOKZ'.           <ls_inforecord_purchorg>-info_type  = ls_reg_pir_mass_eine-esokz.  lv_append = abap_true.
              WHEN 'EKGRP'.           <ls_inforecord_purchorg>-pur_group  = ls_reg_pir_mass_eine-ekgrp.  lv_append = abap_true.
              WHEN 'MINBM'.           <ls_inforecord_purchorg>-min_po_qty = ls_reg_pir_mass_eine-minbm.  lv_append = abap_true.
              WHEN 'NORBM'.           <ls_inforecord_purchorg>-nrm_po_qty = ls_reg_pir_mass_eine-norbm.  lv_append = abap_true.
              WHEN 'APLFZ'.           <ls_inforecord_purchorg>-plnd_delry = ls_reg_pir_mass_eine-aplfz.  lv_append = abap_true.
              WHEN 'EKKOL'.           <ls_inforecord_purchorg>-cond_group = ls_reg_pir_mass_eine-ekkol.  lv_append = abap_true.
              WHEN 'MWSKZ'.           <ls_inforecord_purchorg>-tax_code   = ls_reg_pir_mass_eine-mwskz.  lv_append = abap_true.
              WHEN 'LOEKZ'.           <ls_inforecord_purchorg>-delete_ind = ls_reg_pir_mass_eine-loekz.  lv_append = abap_true.
              WHEN 'UEBTO'.           <ls_inforecord_purchorg>-overdeltol = ls_reg_pir_mass_eine-uebto.  lv_append = abap_true.
              WHEN 'UNTTO'.           <ls_inforecord_purchorg>-under_tol  = ls_reg_pir_mass_eine-untto.  lv_append = abap_true.
            ENDCASE.
          ENDLOOP.  " LOOP AT lt_mass_fields[] INTO ls_mass_fields

        ENDIF.  " if sy-subrc = 0, READ TABLE ls_reg_data_mass-zarn_reg_pir INTO ls_reg_pir_mass_eine

* If EINE Unit changes
        IF ls_reg_pir_mass_eine-bprme NE ls_eine-bprme AND ls_reg_pir_mass_eine IS NOT INITIAL.

        ELSE.
          <ls_inforecord_purchorg>-currency   = ls_eine-waers.
          <ls_inforecord_purchorg>-net_price  = ls_eine-netpr.
          <ls_inforecord_purchorg>-price_unit = ls_eine-peinh.
          <ls_inforecord_purchorg>-eff_price  = ls_eine-netpr.
          <ls_inforecord_purchorg>-orderpr_un = ls_eine-bprme.
          <ls_inforecord_purchorg>-price_date = ls_eine-prdat.

        ENDIF.


* ORDERPR_UN_ISO
        CLEAR ls_t006.
        READ TABLE lt_t006[] INTO ls_t006
        WITH TABLE KEY msehi = <ls_inforecord_purchorg>-orderpr_un.
        IF sy-subrc = 0.
          <ls_inforecord_purchorg>-orderpr_un_iso  = ls_t006-isocode.
        ENDIF.

* CURRENCY_ISO
        CLEAR ls_tcurc.
        READ TABLE lt_tcurc[] INTO ls_tcurc
        WITH TABLE KEY waers = <ls_inforecord_purchorg>-currency.
        IF sy-subrc = 0.
          <ls_inforecord_purchorg>-currency_iso  = ls_tcurc-isocd.
        ENDIF.

        IF lv_append = abap_true.
          ev_post = abap_true.
        ENDIF.


      ENDLOOP.  " LOOP AT lt_inforecord_general[] ASSIGNING <ls_inforecord_general>
    ENDIF.  " IF lt_inforecord_general[]is NOT INITIAL


**********************************************************************************


    IF lt_inforecord_general[] IS INITIAL.
      CLEAR lt_calp_vb[].
    ENDIF.




**********************************************************************************

    ct_inforecord_general[]  = lt_inforecord_general[].
    ct_inforecord_purchorg[] = lt_inforecord_purchorg[].
    ct_calp_vb[]             = lt_calp_vb[].
    ct_clientdata[]          = lt_clientdata[].
    ct_clientdatax[]         = lt_clientdatax[].
    ct_addnlclientdata[]     = lt_addnlclientdata[].
    ct_addnlclientdatax[]    = lt_addnlclientdatax[].


  ENDMETHOD.


  METHOD filter_reg_uom_data.
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             # 29.05.2020
* CHANGE No.       # 9000007141: SSM-8 Symphony -ZARN_MASS
* DESCRIPTION      # Calculate Volume when one of the dimention parameter changes.
*                  # Also, remove the usage of additional internal table for UOM
*                  # Code clean up.
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------

    DATA: ls_msg              TYPE massmsg,
          ls_mass_fields      TYPE ty_s_mass_fields,
          ls_reg_uom_mass     TYPE zarn_reg_uom,
          ls_reg_ean_mass     TYPE zarn_reg_ean,
          ls_marm             TYPE marm,
          ls_mean             TYPE mean,
          ls_unitsofmeasurex  TYPE bapie1marmrtx,
          ls_clientdata       TYPE bapie1marart,
          ls_clientdatax      TYPE bapie1marartx,
          ls_addnlclientdata  TYPE bapie1maw1rt,
          ls_addnlclientdatax TYPE bapie1maw1rtx,

          lv_append           TYPE flag,
          lv_calculate_vol    TYPE flag.

    FIELD-SYMBOLS: <ls_clientdatax>      TYPE bapie1marartx,
                   <ls_addnlclientdatax> TYPE bapie1maw1rtx.

    LOOP AT ct_unitsofmeasure ASSIGNING FIELD-SYMBOL(<ls_unitsofmeasure>).

* Remove Entries which are not in MASS Data
      CLEAR ls_reg_uom_mass.
      READ TABLE is_reg_data_mass-zarn_reg_uom INTO ls_reg_uom_mass
      WITH KEY idno  = is_idno_data-idno
               meinh = <ls_unitsofmeasure>-alt_unit.
      IF sy-subrc NE 0.

        READ TABLE it_mass_fields[] TRANSPORTING NO FIELDS
        WITH KEY table = 'ZARN_REG_EAN'.
        IF sy-subrc NE 0.
          CLEAR: <ls_unitsofmeasure>-material.
          CONTINUE.
        ELSE.

* Remove Entries which are not in MASS Data
          CLEAR ls_reg_ean_mass.
          READ TABLE is_reg_data_mass-zarn_reg_ean INTO ls_reg_ean_mass
          WITH KEY idno  = is_idno_data-idno
                   meinh = <ls_unitsofmeasure>-alt_unit.
          IF sy-subrc NE 0.
            CLEAR: <ls_unitsofmeasure>-material.
            CONTINUE.
          ENDIF.

        ENDIF.
      ENDIF.  " READ TABLE ls_reg_data_mass-zarn_reg_uom INTO ls_reg_uom_mass


* Remove Entries which are not in MARM
      CLEAR ls_marm.
      READ TABLE it_marm INTO ls_marm
      WITH KEY matnr = is_idno_data-sap_article
               meinh = <ls_unitsofmeasure>-alt_unit.
      IF sy-subrc NE 0.
* Record doesn't exist in Article UoM (MARM): Article &, Unit &
        CLEAR ls_msg.
        ls_msg-objkey = 'ZARENA'.
        ls_msg-msgty  = 'E'.
        ls_msg-msgid  = 'ZARENA_MSG'.
        ls_msg-msgno  = '094'.
        ls_msg-msgv1  = is_idno_data-sap_article.
        ls_msg-msgv2  = <ls_unitsofmeasure>-alt_unit.
        APPEND ls_msg TO ct_msg[].

        CLEAR: <ls_unitsofmeasure>-material.
      ENDIF.

    ENDLOOP.  "  LOOP AT lt_unitsofmeasure[] INTO ls_unitsofmeasure


**********************************************************************************

    CLEAR lv_append.

*** UNITSOFMEASUREX
    DELETE ct_unitsofmeasure WHERE material IS INITIAL.
    LOOP AT ct_unitsofmeasure ASSIGNING <ls_unitsofmeasure>.

      CLEAR: lv_calculate_vol,
             ls_unitsofmeasurex.

      ls_unitsofmeasurex-function     = <ls_unitsofmeasure>-function.
      ls_unitsofmeasurex-material     = <ls_unitsofmeasure>-material.
      ls_unitsofmeasurex-alt_unit     = <ls_unitsofmeasure>-alt_unit.
      ls_unitsofmeasurex-alt_unit_iso = <ls_unitsofmeasure>-alt_unit_iso.


* Set Main GTIN if Main indicator is change in REG_EAN
      CLEAR ls_mass_fields.
      READ TABLE it_mass_fields INTO ls_mass_fields
        WITH KEY table = 'ZARN_REG_EAN'
                 field = 'HPEAN'.
      IF sy-subrc = 0.
* Mark 'X' only if Main GTIN of current UOM is changed
        READ TABLE is_reg_data_mass-zarn_reg_ean TRANSPORTING NO FIELDS
          WITH KEY idno  = is_idno_data-idno
                   meinh = <ls_unitsofmeasure>-alt_unit.
        IF sy-subrc = 0.

* If Main GTIN in UOM doesn't exist in MEAN then ignore that UOM
          READ TABLE it_mean TRANSPORTING NO FIELDS
          WITH KEY matnr = <ls_unitsofmeasure>-material
                   meinh = <ls_unitsofmeasure>-alt_unit
                   ean11 = <ls_unitsofmeasure>-ean_upc
                   eantp = <ls_unitsofmeasure>-ean_cat.
          IF sy-subrc NE 0.
* Record doesn't exist in Article GTINs (MEAN): Article &, Unit &, GTIN &
            CLEAR ls_msg.
            ls_msg-objkey = 'ZARENA'.
            ls_msg-msgty  = 'E'.
            ls_msg-msgid  = 'ZARENA_MSG'.
            ls_msg-msgno  = '107'.
            ls_msg-msgv1  = is_idno_data-sap_article.
            ls_msg-msgv2  = <ls_unitsofmeasure>-alt_unit.
            ls_msg-msgv3  = <ls_unitsofmeasure>-ean_upc.
* ls_msg-MSGV4  =
            APPEND ls_msg TO ct_msg[].


            CLEAR: <ls_unitsofmeasure>-material.
            CONTINUE.
          ELSE.
            ls_unitsofmeasurex-ean_upc = abap_true.
            ls_unitsofmeasurex-ean_cat = abap_true.
            lv_append = abap_true.
          ENDIF.

        ENDIF.

      ENDIF.

      LOOP AT it_mass_fields INTO ls_mass_fields WHERE table = 'ZARN_REG_UOM'.
        CASE ls_mass_fields-field.
          WHEN 'NUM_BASE_UNITS' OR 'FACTOR_OF_BASE_UNITS'.
            ls_unitsofmeasurex-numerator  = abap_true.
            ls_unitsofmeasurex-denominatr = abap_true.
            lv_append = abap_true.

          WHEN 'DEPTH_VALUE'.
            ls_unitsofmeasurex-length = abap_true.
            lv_append                 = abap_true.
            lv_calculate_vol          = abap_true.
          WHEN 'WIDTH_VALUE'.
            ls_unitsofmeasurex-width  = abap_true.
            lv_append                 = abap_true.
            lv_calculate_vol          = abap_true.
          WHEN 'HEIGHT_VALUE'.
            ls_unitsofmeasurex-height = abap_true.
            lv_append                 = abap_true.
            lv_calculate_vol          = abap_true.
          WHEN 'GROSS_WEIGHT_VALUE'.
            ls_unitsofmeasurex-gross_wt = abap_true.
            lv_append                   = abap_true.

          WHEN 'HEIGHT_UOM'.
            ls_unitsofmeasurex-unit_dim       = abap_true.
            ls_unitsofmeasurex-unit_dim_iso   = abap_true.
            lv_append = abap_true.

          WHEN 'GROSS_WEIGHT_UOM'.
            ls_unitsofmeasurex-unit_of_wt     = abap_true.
            ls_unitsofmeasurex-unit_of_wt_iso = abap_true.
            lv_append = abap_true.

          WHEN 'UNIT_BASE'.
            IF <ls_clientdatax> IS ASSIGNED. UNASSIGN <ls_clientdatax>. ENDIF.
            READ TABLE ct_clientdatax ASSIGNING <ls_clientdatax>
            WITH KEY material = is_idno_data-sap_article.
            IF sy-subrc = 0.
              <ls_clientdatax>-base_uom     = abap_true.
              <ls_clientdatax>-base_uom_iso = abap_true.
            ELSE.
              CLEAR ls_clientdatax.
              ls_clientdatax-function     = <ls_unitsofmeasure>-function.
              ls_clientdatax-material     = <ls_unitsofmeasure>-material.

              ls_clientdatax-base_uom     = abap_true.
              ls_clientdatax-base_uom_iso = abap_true.
              APPEND ls_clientdatax TO ct_clientdatax.
              ev_post = abap_true.
            ENDIF.

          WHEN 'UNIT_PURORD'.
            IF <ls_clientdatax> IS ASSIGNED. UNASSIGN <ls_clientdatax>. ENDIF.
            READ TABLE ct_clientdatax ASSIGNING <ls_clientdatax>
            WITH KEY material = is_idno_data-sap_article.
            IF sy-subrc = 0.
              <ls_clientdatax>-po_unit     = abap_true.
              <ls_clientdatax>-po_unit_iso = abap_true.
            ELSE.
              CLEAR ls_clientdatax.
              ls_clientdatax-function     = <ls_unitsofmeasure>-function.
              ls_clientdatax-material     = <ls_unitsofmeasure>-material.

              ls_clientdatax-po_unit      = abap_true.
              ls_clientdatax-po_unit_iso  = abap_true.
              APPEND ls_clientdatax TO ct_clientdatax.
              ev_post = abap_true.
            ENDIF.

          WHEN 'SALES_UNIT'.
            IF <ls_addnlclientdatax> IS ASSIGNED. UNASSIGN <ls_addnlclientdatax>. ENDIF.
            READ TABLE ct_addnlclientdatax ASSIGNING <ls_addnlclientdatax>
            WITH KEY material = is_idno_data-sap_article.
            IF sy-subrc = 0.
              <ls_addnlclientdatax>-sales_unit     = abap_true.
              <ls_addnlclientdatax>-sales_unit_iso = abap_true.
            ELSE.
              CLEAR ls_addnlclientdatax.
              ls_addnlclientdatax-function     = <ls_unitsofmeasure>-function.
              ls_addnlclientdatax-material     = <ls_unitsofmeasure>-material.

              ls_addnlclientdatax-sales_unit     = abap_true.
              ls_addnlclientdatax-sales_unit_iso = abap_true.
              APPEND ls_addnlclientdatax TO ct_addnlclientdatax.
              ev_post = abap_true.
            ENDIF.

          WHEN 'ISSUE_UNIT'.
            IF <ls_addnlclientdatax> IS ASSIGNED. UNASSIGN <ls_addnlclientdatax>. ENDIF.
            READ TABLE ct_addnlclientdatax ASSIGNING <ls_addnlclientdatax>
            WITH KEY material = is_idno_data-sap_article.
            IF sy-subrc = 0.
              <ls_addnlclientdatax>-issue_unit     = abap_true.
              <ls_addnlclientdatax>-issue_unit_iso = abap_true.
            ELSE.
              CLEAR ls_addnlclientdatax.
              ls_addnlclientdatax-function     = <ls_unitsofmeasure>-function.
              ls_addnlclientdatax-material     = <ls_unitsofmeasure>-material.

              ls_addnlclientdatax-issue_unit     = abap_true.
              ls_addnlclientdatax-issue_unit_iso = abap_true.
              APPEND ls_addnlclientdatax TO ct_addnlclientdatax.
              ev_post = abap_true.
            ENDIF.

        ENDCASE.
      ENDLOOP.

      IF lv_append = abap_false.
        CONTINUE.
      ENDIF.

      " Calculate Volume when necessary
      IF lv_calculate_vol = abap_true.
        <ls_unitsofmeasure>-volume     = <ls_unitsofmeasure>-length * <ls_unitsofmeasure>-width * <ls_unitsofmeasure>-height.
        <ls_unitsofmeasure>-volumeunit = 'MMQ'.

        ls_unitsofmeasurex-volume     = abap_true.
        ls_unitsofmeasurex-volumeunit = abap_true.
      ENDIF.

      APPEND ls_unitsofmeasurex TO ct_unitsofmeasurex.
      ev_post = abap_true.

    ENDLOOP.

**********************************************************************************

  ENDMETHOD.


  METHOD get_mass_table_fields.

    DATA: lt_seldata     TYPE mass_tabdata,
          ls_seldata     TYPE mass_wa_tabdata,
          lt_fieldname   TYPE mass_fieldtab,
          ls_fieldname   LIKE LINE OF lt_fieldname,
          ls_mass_fields TYPE ty_s_mass_fields.

    lt_seldata[] = it_seldata[].

    LOOP AT lt_seldata[] INTO ls_seldata.
      CLEAR ls_mass_fields.
      ls_mass_fields-table = ls_seldata-tabname.

      lt_fieldname[] = ls_seldata-fieldnames.

      LOOP AT lt_fieldname[] INTO ls_fieldname.
        ls_mass_fields-field = ls_fieldname.
        INSERT ls_mass_fields INTO TABLE et_mass_fields[].
      ENDLOOP.


    ENDLOOP.  " LOOP AT lt_seldata[] INTO ls_seldata


  ENDMETHOD.


  METHOD get_nat_reg_data.
*-----------------------------------------------------------------------
* DATE             # 27.07.2020
* CHANGE No.       # SSM-1: NW Range-ZARN_GUI CHANGE
* DESCRIPTION      # Added ZARN_REG_CLUSTER for mass maintenance
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
    DATA: ls_reg_data_mass TYPE zsarn_reg_data,
          lt_reg_data_save TYPE ztarn_reg_data,
          ls_reg_data_save TYPE zsarn_reg_data,

          ls_prod_data_all TYPE zsarn_prod_data,
          ls_reg_data_all  TYPE zsarn_reg_data,

          lt_ver_status    TYPE ty_t_ver_status,
          ls_ver_status    TYPE zarn_ver_status,
          lt_prd_version   TYPE ty_t_prd_version,
          ls_prd_version   TYPE zarn_prd_version,
          lt_reg_key       TYPE ztarn_reg_key,
          ls_reg_key       TYPE zsarn_reg_key,
          lt_key           TYPE ztarn_key,
          ls_key           TYPE zsarn_key,


          ls_mass_banner   TYPE zarn_reg_banner,
          ls_mass_dc_sell  TYPE zarn_reg_dc_sell,
          ls_mass_ean      TYPE zarn_reg_ean,
          ls_mass_hdr      TYPE zarn_reg_hdr,
          ls_mass_lst_prc  TYPE zarn_reg_lst_prc,
          ls_mass_pir      TYPE zarn_reg_pir,
          ls_mass_prfam    TYPE zarn_reg_prfam,
          ls_mass_rrp      TYPE zarn_reg_rrp,
          ls_mass_std_ter  TYPE zarn_reg_std_ter,
          ls_mass_txt      TYPE zarn_reg_txt,
          ls_mass_uom      TYPE zarn_reg_uom.


    FIELD-SYMBOLS: <reg_banner>	 TYPE zarn_reg_banner,
                   <reg_dc_sell> TYPE zarn_reg_dc_sell,
                   <reg_ean>     TYPE zarn_reg_ean,
                   <reg_hdr>     TYPE zarn_reg_hdr,
                   <reg_lst_prc> TYPE zarn_reg_lst_prc,
                   <reg_pir>     TYPE zarn_reg_pir,
                   <reg_prfam>   TYPE zarn_reg_prfam,
                   <reg_rrp>     TYPE zarn_reg_rrp,
                   <reg_std_ter> TYPE zarn_reg_std_ter,
                   <reg_txt>     TYPE zarn_reg_txt,
                   <reg_uom>     TYPE zarn_reg_uom.


    ls_reg_data_mass = is_reg_data_mass.
    lt_reg_key[]     = it_reg_key[].

    IF lt_reg_key[] IS INITIAL.
      RETURN.
    ENDIF.

    CLEAR: lt_ver_status[], lt_prd_version[].

* Get Version status Data
    SELECT * FROM zarn_ver_status
      INTO CORRESPONDING FIELDS OF TABLE lt_ver_status[]
      FOR ALL ENTRIES IN lt_reg_key[]
      WHERE idno = lt_reg_key-idno.

* Get Product Version Data
    SELECT * FROM zarn_prd_version
      INTO CORRESPONDING FIELDS OF TABLE lt_prd_version[]
      FOR ALL ENTRIES IN lt_reg_key[]
      WHERE idno = lt_reg_key-idno.

* Build Key to retreive National Data
    LOOP AT lt_ver_status[] INTO ls_ver_status.
      CLEAR ls_key.
      ls_key-idno = ls_ver_status-idno.
      ls_key-version = ls_ver_status-current_ver.
      APPEND ls_key TO lt_key[].
    ENDLOOP.


* Get National Data
    CLEAR et_prod_data_db[].
    CALL FUNCTION 'ZARN_READ_NATIONAL_DATA'
      EXPORTING
        it_key      = lt_key[]
      IMPORTING
        et_data     = et_prod_data_db[]
        es_data_all = ls_prod_data_all.


* Get Regional Data
    CLEAR et_reg_data_db[].
    CALL FUNCTION 'ZARN_READ_REGIONAL_DATA'
      EXPORTING
        it_key      = lt_reg_key[]
      IMPORTING
        et_data     = et_reg_data_db[]
        es_data_all = ls_reg_data_all.

    et_ver_status[]   = lt_ver_status[].
    et_prd_version[]  = lt_prd_version[].
    es_prod_data_all  = ls_prod_data_all.
    es_reg_data_all   = ls_reg_data_all.
    et_key[]          = lt_key[].




*****************************************************************************


* Update regional DB tables with Mass data to get new updated regional data tobe saved and posted

* Banner
    LOOP AT ls_reg_data_all-zarn_reg_banner ASSIGNING <reg_banner>.
      CLEAR ls_mass_banner.
      READ TABLE ls_reg_data_mass-zarn_reg_banner INTO ls_mass_banner
      WITH  KEY idno   = <reg_banner>-idno
                banner = <reg_banner>-banner.
      IF sy-subrc = 0.
        <reg_banner> = ls_mass_banner.
      ENDIF.
    ENDLOOP.

* DC Sell
    LOOP AT ls_reg_data_all-zarn_reg_dc_sell ASSIGNING <reg_dc_sell>.
      CLEAR ls_mass_dc_sell.
      READ TABLE ls_reg_data_mass-zarn_reg_dc_sell INTO ls_mass_dc_sell
      WITH  KEY idno           = <reg_dc_sell>-idno
                condition_type = <reg_dc_sell>-condition_type
                vkorg          = <reg_dc_sell>-vkorg
                lifnr          = <reg_dc_sell>-lifnr
                cond_unit      = <reg_dc_sell>-cond_unit.
      IF sy-subrc = 0.
        <reg_dc_sell> = ls_mass_dc_sell.
      ENDIF.
    ENDLOOP.

* EAN
    LOOP AT ls_reg_data_all-zarn_reg_ean ASSIGNING <reg_ean>.
      CLEAR ls_mass_ean.
      READ TABLE ls_reg_data_mass-zarn_reg_ean INTO ls_mass_ean
      WITH  KEY idno   = <reg_ean>-idno
                meinh  = <reg_ean>-meinh
                ean11  = <reg_ean>-ean11.
      IF sy-subrc = 0.
        <reg_ean> = ls_mass_ean.
      ENDIF.
    ENDLOOP.

* Header
    LOOP AT ls_reg_data_all-zarn_reg_hdr ASSIGNING <reg_hdr>.
      CLEAR ls_mass_hdr.
      READ TABLE ls_reg_data_mass-zarn_reg_hdr INTO ls_mass_hdr
      WITH  KEY idno   = <reg_hdr>-idno.
      IF sy-subrc = 0.
        <reg_hdr>       = ls_mass_hdr.
        <reg_hdr>-laeda = sy-datum.
        <reg_hdr>-eruet = sy-uzeit.
        <reg_hdr>-aenam = sy-uname.
      ENDIF.
    ENDLOOP.

* List Price
    LOOP AT ls_reg_data_all-zarn_reg_lst_prc ASSIGNING <reg_lst_prc>.
      CLEAR ls_mass_lst_prc.
      READ TABLE ls_reg_data_mass-zarn_reg_lst_prc INTO ls_mass_lst_prc
      WITH  KEY idno           = <reg_lst_prc>-idno
                condition_type = <reg_lst_prc>-condition_type
                ekorg          = <reg_lst_prc>-ekorg
                lifnr          = <reg_lst_prc>-lifnr
                cond_unit      = <reg_lst_prc>-cond_unit.
      IF sy-subrc = 0.
        <reg_lst_prc> = ls_mass_lst_prc.
      ENDIF.
    ENDLOOP.

* PIR
    LOOP AT ls_reg_data_all-zarn_reg_pir ASSIGNING <reg_pir>.
      CLEAR ls_mass_pir.
      READ TABLE ls_reg_data_mass-zarn_reg_pir INTO ls_mass_pir
      WITH  KEY idno                 = <reg_pir>-idno
                order_uom_pim        = <reg_pir>-order_uom_pim
                hybris_internal_code = <reg_pir>-hybris_internal_code.
      IF sy-subrc = 0.
        <reg_pir> = ls_mass_pir.
      ENDIF.
    ENDLOOP.

* Price Family
    LOOP AT ls_reg_data_all-zarn_reg_prfam ASSIGNING <reg_prfam>.
      CLEAR ls_mass_prfam.
      READ TABLE ls_reg_data_mass-zarn_reg_prfam INTO ls_mass_prfam
      WITH  KEY idno      = <reg_prfam>-idno
                prfam_uom = <reg_prfam>-prfam_uom.
      IF sy-subrc = 0.
        <reg_prfam> = ls_mass_prfam.
      ENDIF.
    ENDLOOP.

* RRP
    LOOP AT ls_reg_data_all-zarn_reg_rrp ASSIGNING <reg_rrp>.
      CLEAR ls_mass_rrp.
      READ TABLE ls_reg_data_mass-zarn_reg_rrp INTO ls_mass_rrp
      WITH  KEY idno           = <reg_rrp>-idno
                vkorg          = <reg_rrp>-vkorg
                pltyp          = <reg_rrp>-pltyp
                cond_unit      = <reg_rrp>-cond_unit
                condition_type = <reg_rrp>-condition_type.
      IF sy-subrc = 0.
        <reg_rrp> = ls_mass_rrp.
      ENDIF.
    ENDLOOP.

* STD TER
    LOOP AT ls_reg_data_all-zarn_reg_std_ter ASSIGNING <reg_std_ter>.
      CLEAR ls_mass_std_ter.
      READ TABLE ls_reg_data_mass-zarn_reg_std_ter INTO ls_mass_std_ter
      WITH  KEY idno  = <reg_std_ter>-idno
                vkorg = <reg_std_ter>-vkorg.
      IF sy-subrc = 0.
        <reg_std_ter> = ls_mass_std_ter.
      ENDIF.
    ENDLOOP.

* Text
    LOOP AT ls_reg_data_all-zarn_reg_txt ASSIGNING <reg_txt>.
      CLEAR ls_mass_txt.
      READ TABLE ls_reg_data_mass-zarn_reg_txt INTO ls_mass_txt
      WITH  KEY idno    = <reg_txt>-idno
                line_no = <reg_txt>-line_no.
      IF sy-subrc = 0.
        <reg_txt> = ls_mass_txt.
      ENDIF.
    ENDLOOP.

* UOM
    LOOP AT ls_reg_data_all-zarn_reg_uom ASSIGNING <reg_uom>.
      CLEAR ls_mass_uom.
      READ TABLE ls_reg_data_mass-zarn_reg_uom INTO ls_mass_uom
      WITH  KEY idno                 = <reg_uom>-idno
                uom_category         = <reg_uom>-uom_category
                pim_uom_code         = <reg_uom>-pim_uom_code
                hybris_internal_code = <reg_uom>-hybris_internal_code
                lower_uom            = <reg_uom>-lower_uom
                lower_child_uom      = <reg_uom>-lower_child_uom.
      IF sy-subrc = 0.
        <reg_uom> = ls_mass_uom.
      ENDIF.
    ENDLOOP.

    LOOP AT ls_reg_data_all-zarn_reg_cluster ASSIGNING FIELD-SYMBOL(<ls_reg_cluster>).
      READ TABLE ls_reg_data_mass-zarn_reg_cluster INTO DATA(ls_mass_cluster)
      WITH  KEY idno   = <ls_reg_cluster>-idno
                banner = <ls_reg_cluster>-banner.
      IF sy-subrc = 0.
        <ls_reg_cluster> = ls_mass_cluster.
      ENDIF.
    ENDLOOP.

* Build Mass regional data as per IDNO
    LOOP AT lt_reg_key[] INTO ls_reg_key.

* Build Old and New tables as per IDNO
      CLEAR ls_reg_data_save.
      ls_reg_data_save-idno = ls_reg_key-idno.

      ls_reg_data_save-zarn_reg_hdr[]     = VALUE #( FOR hdr IN ls_reg_data_all-zarn_reg_hdr[]
                                                WHERE ( idno = ls_reg_key-idno ) ( hdr ) ).

      ls_reg_data_save-zarn_reg_banner[]  = VALUE #( FOR banner IN ls_reg_data_all-zarn_reg_banner[]
                                                WHERE ( idno = ls_reg_key-idno ) ( banner ) ).

      ls_reg_data_save-zarn_reg_ean[]     = VALUE #( FOR ean IN ls_reg_data_all-zarn_reg_ean[]
                                                WHERE ( idno = ls_reg_key-idno ) ( ean ) ).

      ls_reg_data_save-zarn_reg_pir[]     = VALUE #( FOR pir IN ls_reg_data_all-zarn_reg_pir[]
                                                WHERE ( idno = ls_reg_key-idno ) ( pir ) ).

      ls_reg_data_save-zarn_reg_prfam[]   = VALUE #( FOR prfam IN ls_reg_data_all-zarn_reg_prfam[]
                                                WHERE ( idno = ls_reg_key-idno ) ( prfam ) ).

      ls_reg_data_save-zarn_reg_txt[]     = VALUE #( FOR txt IN ls_reg_data_all-zarn_reg_txt[]
                                                WHERE ( idno = ls_reg_key-idno ) ( txt ) ).

      ls_reg_data_save-zarn_reg_uom[]     = VALUE #( FOR uom IN ls_reg_data_all-zarn_reg_uom[]
                                                WHERE ( idno = ls_reg_key-idno ) ( uom ) ).

      ls_reg_data_save-zarn_reg_lst_prc[] = VALUE #( FOR lst_prc IN ls_reg_data_all-zarn_reg_lst_prc[]
                                                WHERE ( idno = ls_reg_key-idno ) ( lst_prc ) ).

      ls_reg_data_save-zarn_reg_dc_sell[] = VALUE #( FOR dc_sell IN ls_reg_data_all-zarn_reg_dc_sell[]
                                                WHERE ( idno = ls_reg_key-idno ) ( dc_sell ) ).

      ls_reg_data_save-zarn_reg_std_ter[] = VALUE #( FOR std_ter IN ls_reg_data_all-zarn_reg_std_ter[]
                                                WHERE ( idno = ls_reg_key-idno ) ( std_ter ) ).

      ls_reg_data_save-zarn_reg_rrp[]     = VALUE #( FOR reg_rrp IN ls_reg_data_all-zarn_reg_rrp[]
                                                WHERE ( idno = ls_reg_key-idno ) ( reg_rrp ) ).

      ls_reg_data_save-zarn_reg_cluster  = VALUE #( FOR ls_reg_cluster IN ls_reg_data_all-zarn_reg_cluster
                                                WHERE ( idno = ls_reg_key-idno ) ( ls_reg_cluster ) ).

      APPEND ls_reg_data_save TO lt_reg_data_save[].
    ENDLOOP.  " LOOP AT lt_reg_key[] INTO ls_reg_key

    et_reg_data_save[] = lt_reg_data_save[].
  ENDMETHOD.


  METHOD get_regional_db_data.
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             # 18.05.2020
* CHANGE No.       # 9000007141: SSM-8 Symphony -ZARN_MASS
* DESCRIPTION      # Modularise some part of the code into method
*                  # MODIFY_REG_DATA_MASS_UOM and allow some modifications
*                  # UOM records
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* DATE             # 27.07.2020
* CHANGE No.       # SSM-1: NW Range-ZARN_GUI CHANGE
* DESCRIPTION      # Added ZARN_REG_CLUSTER for mass maintenance
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
    DATA: ls_reg_data_mass    TYPE zsarn_reg_data,
          ls_reg_data_db_mass TYPE zsarn_reg_data,

          lt_reg_data         TYPE ztarn_reg_data,
          ls_reg_data         TYPE zsarn_reg_data,
          lt_reg_key          TYPE ty_t_reg_key,
          ls_reg_key          TYPE zsarn_reg_key,
          lt_reg_cluster      TYPE ztarn_reg_cluster,

          ls_reg_uom          TYPE zarn_reg_uom,
          ls_mass_uom         TYPE zarn_reg_uom,

          lt_mass_fields      TYPE ty_t_mass_fields,
          ls_mass_fields      TYPE ty_s_mass_fields.


    FIELD-SYMBOLS: <reg_mass_uom> TYPE zarn_reg_uom,
                   <reg_mass_hdr> TYPE zarn_reg_hdr.


    lt_mass_fields[] = it_mass_fields[].

    CLEAR es_reg_data_db_mass.

* Regional Header data
    IF it_reg_hdr[] IS NOT INITIAL.
      SELECT * FROM zarn_reg_hdr
        INTO CORRESPONDING FIELDS OF TABLE es_reg_data_db_mass-zarn_reg_hdr[]
        FOR ALL ENTRIES IN it_reg_hdr[]
        WHERE idno = it_reg_hdr-idno.
    ENDIF.

* Regional UOM data
    IF it_reg_uom[] IS NOT INITIAL.
      SELECT * FROM zarn_reg_uom
        INTO CORRESPONDING FIELDS OF TABLE es_reg_data_db_mass-zarn_reg_uom[]
        FOR ALL ENTRIES IN it_reg_uom[]
        WHERE idno                 = it_reg_uom-idno
          AND uom_category         = it_reg_uom-uom_category
          AND pim_uom_code         = it_reg_uom-pim_uom_code
          AND hybris_internal_code = it_reg_uom-hybris_internal_code
          AND lower_uom            = it_reg_uom-lower_uom
          AND lower_child_uom      = it_reg_uom-lower_child_uom.
    ENDIF.

* Regional PIR data
    IF it_reg_pir[] IS NOT INITIAL.
      SELECT * FROM zarn_reg_pir
        INTO CORRESPONDING FIELDS OF TABLE es_reg_data_db_mass-zarn_reg_pir[]
        FOR ALL ENTRIES IN it_reg_pir[]
        WHERE idno                 = it_reg_pir-idno
          AND order_uom_pim        = it_reg_pir-order_uom_pim
          AND hybris_internal_code = it_reg_pir-hybris_internal_code.
    ENDIF.

* Regional EAN data
    IF it_reg_ean[] IS NOT INITIAL.
      SELECT * FROM zarn_reg_ean
        INTO CORRESPONDING FIELDS OF TABLE es_reg_data_db_mass-zarn_reg_ean[]
        FOR ALL ENTRIES IN it_reg_ean[]
        WHERE idno  = it_reg_ean-idno
          AND meinh = it_reg_ean-meinh
          AND ean11 = it_reg_ean-ean11.
    ENDIF.

* Regional Banner data
    IF it_reg_banner[] IS NOT INITIAL.
      SELECT * FROM zarn_reg_banner
        INTO CORRESPONDING FIELDS OF TABLE es_reg_data_db_mass-zarn_reg_banner[]
        FOR ALL ENTRIES IN it_reg_banner[]
        WHERE idno   = it_reg_banner-idno
          AND banner = it_reg_banner-banner.
    ENDIF.

* Regional Price Family data
    IF it_reg_prfam[] IS NOT INITIAL.
      SELECT * FROM zarn_reg_prfam
        INTO CORRESPONDING FIELDS OF TABLE es_reg_data_db_mass-zarn_reg_prfam[]
        FOR ALL ENTRIES IN it_reg_prfam[]
        WHERE idno      = it_reg_prfam-idno
          AND prfam_uom = it_reg_prfam-prfam_uom.
    ENDIF.

* Regional DC Sell data
    IF it_reg_dc_sell[] IS NOT INITIAL.
      SELECT * FROM zarn_reg_dc_sell
        INTO CORRESPONDING FIELDS OF TABLE es_reg_data_db_mass-zarn_reg_dc_sell[]
        FOR ALL ENTRIES IN it_reg_dc_sell[]
        WHERE idno           = it_reg_dc_sell-idno
          AND condition_type = it_reg_dc_sell-condition_type
          AND vkorg          = it_reg_dc_sell-vkorg
          AND lifnr          = it_reg_dc_sell-lifnr
          AND cond_unit      = it_reg_dc_sell-cond_unit.
    ENDIF.

* Regional List Price data
    IF it_reg_lst_prc[] IS NOT INITIAL.
      SELECT * FROM zarn_reg_lst_prc
        INTO CORRESPONDING FIELDS OF TABLE es_reg_data_db_mass-zarn_reg_lst_prc[]
        FOR ALL ENTRIES IN it_reg_lst_prc[]
        WHERE idno           = it_reg_lst_prc-idno
          AND condition_type = it_reg_lst_prc-condition_type
          AND ekorg          = it_reg_lst_prc-ekorg
          AND lifnr          = it_reg_lst_prc-lifnr
          AND cond_unit      = it_reg_lst_prc-cond_unit.
    ENDIF.

* Regional RRP data
    IF it_reg_rrp[] IS NOT INITIAL.
      SELECT * FROM zarn_reg_rrp
        INTO CORRESPONDING FIELDS OF TABLE es_reg_data_db_mass-zarn_reg_rrp[]
        FOR ALL ENTRIES IN it_reg_rrp[]
        WHERE idno           = it_reg_rrp-idno
          AND vkorg          = it_reg_rrp-vkorg
          AND pltyp          = it_reg_rrp-pltyp
          AND cond_unit      = it_reg_rrp-cond_unit
          AND condition_type = it_reg_rrp-condition_type.
    ENDIF.

* Regional STD TER data
    IF it_reg_std_ter[] IS NOT INITIAL.
      SELECT * FROM zarn_reg_std_ter
        INTO CORRESPONDING FIELDS OF TABLE es_reg_data_db_mass-zarn_reg_std_ter[]
        FOR ALL ENTRIES IN it_reg_std_ter[]
        WHERE idno  = it_reg_std_ter-idno
          AND vkorg = it_reg_std_ter-vkorg.
    ENDIF.

* Regional Text data
    IF it_reg_txt[] IS NOT INITIAL.
      SELECT * FROM zarn_reg_txt
        INTO CORRESPONDING FIELDS OF TABLE es_reg_data_db_mass-zarn_reg_txt[]
        FOR ALL ENTRIES IN it_reg_txt[]
        WHERE idno    = it_reg_txt-idno
          AND line_no = it_reg_txt-line_no.
    ENDIF.

    " Regional Cluster Range
    lt_reg_cluster = it_reg_cluster.
    APPEND LINES OF is_reg_data_mass_delete-zarn_reg_cluster TO lt_reg_cluster.
    IF lt_reg_cluster IS NOT INITIAL.
      SELECT * FROM zarn_reg_cluster
        INTO TABLE es_reg_data_db_mass-zarn_reg_cluster
        FOR ALL ENTRIES IN lt_reg_cluster
        WHERE idno   = lt_reg_cluster-idno
          AND banner = lt_reg_cluster-banner.
    ENDIF.

    LOOP AT cs_reg_data_mass-zarn_reg_hdr ASSIGNING <reg_mass_hdr>.
      <reg_mass_hdr>-laeda = sy-datum.
      <reg_mass_hdr>-eruet = sy-uzeit.
      <reg_mass_hdr>-aenam = sy-uname.
    ENDLOOP.


    modify_reg_data_mass_uom( EXPORTING it_reg_uom_db   = es_reg_data_db_mass-zarn_reg_uom
                              CHANGING  ct_reg_uom_mass = cs_reg_data_mass-zarn_reg_uom ).

    lt_reg_key[]        = it_reg_key[].
    ls_reg_data_mass    = cs_reg_data_mass.
    ls_reg_data_db_mass = es_reg_data_db_mass.


* Build Mass regional data as per IDNO
    CLEAR lt_reg_data[].
    LOOP AT lt_reg_key[] INTO ls_reg_key.

      " Build new mass data
      CLEAR ls_reg_data.
      ls_reg_data-idno = ls_reg_key-idno.

      ls_reg_data-zarn_reg_hdr[]     = VALUE #( FOR hdr IN ls_reg_data_mass-zarn_reg_hdr[]
                                                WHERE ( idno = ls_reg_key-idno ) ( hdr ) ).

      ls_reg_data-zarn_reg_banner[]  = VALUE #( FOR banner IN ls_reg_data_mass-zarn_reg_banner[]
                                                WHERE ( idno = ls_reg_key-idno ) ( banner ) ).

      ls_reg_data-zarn_reg_ean[]     = VALUE #( FOR ean IN ls_reg_data_mass-zarn_reg_ean[]
                                                WHERE ( idno = ls_reg_key-idno ) ( ean ) ).

      ls_reg_data-zarn_reg_pir[]     = VALUE #( FOR pir IN ls_reg_data_mass-zarn_reg_pir[]
                                                WHERE ( idno = ls_reg_key-idno ) ( pir ) ).

      ls_reg_data-zarn_reg_prfam[]   = VALUE #( FOR prfam IN ls_reg_data_mass-zarn_reg_prfam[]
                                                WHERE ( idno = ls_reg_key-idno ) ( prfam ) ).

      ls_reg_data-zarn_reg_txt[]     = VALUE #( FOR txt IN ls_reg_data_mass-zarn_reg_txt[]
                                                WHERE ( idno = ls_reg_key-idno ) ( txt ) ).

      ls_reg_data-zarn_reg_uom[]     = VALUE #( FOR uom IN ls_reg_data_mass-zarn_reg_uom[]
                                                WHERE ( idno = ls_reg_key-idno ) ( uom ) ).

      ls_reg_data-zarn_reg_lst_prc[] = VALUE #( FOR lst_prc IN ls_reg_data_mass-zarn_reg_lst_prc[]
                                                WHERE ( idno = ls_reg_key-idno ) ( lst_prc ) ).

      ls_reg_data-zarn_reg_dc_sell[] = VALUE #( FOR dc_sell IN ls_reg_data_mass-zarn_reg_dc_sell[]
                                                WHERE ( idno = ls_reg_key-idno ) ( dc_sell ) ).

      ls_reg_data-zarn_reg_std_ter[] = VALUE #( FOR std_ter IN ls_reg_data_mass-zarn_reg_std_ter[]
                                                WHERE ( idno = ls_reg_key-idno ) ( std_ter ) ).

      ls_reg_data-zarn_reg_rrp[]     = VALUE #( FOR reg_rrp IN ls_reg_data_mass-zarn_reg_rrp[]
                                                WHERE ( idno = ls_reg_key-idno ) ( reg_rrp ) ).

      ls_reg_data-zarn_reg_cluster   = VALUE #( FOR ls_reg_cluster IN ls_reg_data_mass-zarn_reg_cluster
                                                WHERE ( idno = ls_reg_key-idno ) ( ls_reg_cluster ) ).

      APPEND ls_reg_data TO et_reg_data_mass[].

    ENDLOOP.  " LOOP AT et_reg_key[] INTO ls_reg_key

**************************************************************************************************


* Build DB data for MASS as per IDNO
    CLEAR lt_reg_data[].
    LOOP AT lt_reg_key[] INTO ls_reg_key.

      CLEAR ls_reg_data.
      ls_reg_data-idno = ls_reg_key-idno.

      ls_reg_data-zarn_reg_hdr[]     = VALUE #( FOR hdr IN ls_reg_data_db_mass-zarn_reg_hdr[]
                                                WHERE ( idno = ls_reg_key-idno ) ( hdr ) ).

      ls_reg_data-zarn_reg_banner[]  = VALUE #( FOR banner IN ls_reg_data_db_mass-zarn_reg_banner[]
                                                WHERE ( idno = ls_reg_key-idno ) ( banner ) ).

      ls_reg_data-zarn_reg_ean[]     = VALUE #( FOR ean IN ls_reg_data_db_mass-zarn_reg_ean[]
                                                WHERE ( idno = ls_reg_key-idno ) ( ean ) ).

      ls_reg_data-zarn_reg_pir[]     = VALUE #( FOR pir IN ls_reg_data_db_mass-zarn_reg_pir[]
                                                WHERE ( idno = ls_reg_key-idno ) ( pir ) ).

      ls_reg_data-zarn_reg_prfam[]   = VALUE #( FOR prfam IN ls_reg_data_db_mass-zarn_reg_prfam[]
                                                WHERE ( idno = ls_reg_key-idno ) ( prfam ) ).

      ls_reg_data-zarn_reg_txt[]     = VALUE #( FOR txt IN ls_reg_data_db_mass-zarn_reg_txt[]
                                                WHERE ( idno = ls_reg_key-idno ) ( txt ) ).

      ls_reg_data-zarn_reg_uom[]     = VALUE #( FOR uom IN ls_reg_data_db_mass-zarn_reg_uom[]
                                                WHERE ( idno = ls_reg_key-idno ) ( uom ) ).

      ls_reg_data-zarn_reg_lst_prc[] = VALUE #( FOR lst_prc IN ls_reg_data_db_mass-zarn_reg_lst_prc[]
                                                WHERE ( idno = ls_reg_key-idno ) ( lst_prc ) ).

      ls_reg_data-zarn_reg_dc_sell[] = VALUE #( FOR dc_sell IN ls_reg_data_db_mass-zarn_reg_dc_sell[]
                                                WHERE ( idno = ls_reg_key-idno ) ( dc_sell ) ).

      ls_reg_data-zarn_reg_std_ter[] = VALUE #( FOR std_ter IN ls_reg_data_db_mass-zarn_reg_std_ter[]
                                                WHERE ( idno = ls_reg_key-idno ) ( std_ter ) ).

      ls_reg_data-zarn_reg_rrp[]     = VALUE #( FOR reg_rrp IN ls_reg_data_db_mass-zarn_reg_rrp[]
                                                WHERE ( idno = ls_reg_key-idno ) ( reg_rrp ) ).

      ls_reg_data-zarn_reg_cluster   = VALUE #( FOR ls_reg_cluster IN ls_reg_data_db_mass-zarn_reg_cluster
                                                WHERE ( idno = ls_reg_key-idno ) ( ls_reg_cluster ) ).

      APPEND ls_reg_data TO et_reg_data_db_mass[].

    ENDLOOP.  " LOOP AT et_reg_key[] INTO ls_reg_key

  ENDMETHOD.


  METHOD get_sap_data.

    DATA: ls_prod_data_all TYPE zsarn_prod_data,
          ls_reg_data_all  TYPE zsarn_reg_data,
          lt_key           TYPE ty_t_key,
          ls_key           TYPE zsarn_key,

          lt_ver_status    TYPE ty_t_ver_status,
          ls_ver_status    TYPE zarn_ver_status,
          lt_prd_version   TYPE ty_t_prd_version,
          ls_prd_version   TYPE zarn_prd_version,
          lt_idno_data     TYPE ty_t_idno_data,
          ls_idno_data     TYPE ty_s_idno_data,
          ls_reg_hdr       TYPE zarn_reg_hdr,
          ls_mara          TYPE mara,
          ls_products      TYPE zarn_products.


    lt_key[]         = it_key[].
    ls_prod_data_all = is_prod_data_all.
    ls_reg_data_all  = is_reg_data_all.
    lt_ver_status[]  = it_ver_status[].
    lt_prd_version[] = it_prd_version[].




* Get Article Data
    IF ls_prod_data_all-zarn_products[] IS NOT INITIAL.
* MARA
      SELECT matnr meins zzfan zzlni_repack zzlni_bulk zzlni_prboly
        FROM mara
        INTO CORRESPONDING FIELDS OF TABLE mt_mara[]
        FOR ALL ENTRIES IN ls_prod_data_all-zarn_products[]
        WHERE zzfan = ls_prod_data_all-zarn_products-fan_id.
    ENDIF.



    LOOP AT lt_key[] INTO ls_key.

* Get Regional Header
      CLEAR ls_reg_hdr.
      READ TABLE ls_reg_data_all-zarn_reg_hdr[] INTO ls_reg_hdr
      WITH KEY idno = ls_key-idno.


      CLEAR ls_products.
      READ TABLE ls_prod_data_all-zarn_products[] INTO ls_products
      WITH KEY idno    = ls_key-idno
               version = ls_key-version.

* Get Version status
      CLEAR ls_ver_status.
      READ TABLE lt_ver_status[] INTO ls_ver_status
      WITH TABLE KEY idno = ls_key-idno.

* Get Product Version
      CLEAR ls_prd_version.
      READ TABLE lt_prd_version[] INTO ls_prd_version
      WITH TABLE KEY idno    = ls_key-idno
                     version = ls_key-version.


      CLEAR ls_idno_data.
      READ TABLE lt_idno_data[] INTO ls_idno_data
      WITH TABLE KEY idno = ls_key-idno.
      IF sy-subrc NE 0.
        ls_idno_data-idno         = ls_key-idno.
        ls_idno_data-current_ver  = ls_ver_status-current_ver.
        ls_idno_data-previous_ver = ls_ver_status-previous_ver.
        ls_idno_data-latest_ver   = ls_ver_status-latest_ver.
        ls_idno_data-article_ver  = ls_ver_status-article_ver.
        ls_idno_data-fan_id       = ls_products-fan_id.
        ls_idno_data-nat_status   = ls_prd_version-version_status.
        ls_idno_data-reg_status   = ls_reg_hdr-status.


        CLEAR ls_mara.
        READ TABLE mt_mara[] INTO ls_mara
        WITH KEY zzfan = ls_products-fan_id.
        IF sy-subrc NE 0.
*        ls_idno_data-sap_article  = ls_fan_id-fan_id.
*          CONTINUE.
        ELSE.
          ls_idno_data-sap_article  = ls_mara-matnr.
        ENDIF.


        CALL FUNCTION 'CONVERSION_EXIT_MATN1_INPUT'
          EXPORTING
            input        = ls_idno_data-sap_article
          IMPORTING
            output       = ls_idno_data-sap_article
          EXCEPTIONS
            length_error = 1
            OTHERS       = 2.

        INSERT ls_idno_data INTO TABLE lt_idno_data[].
      ENDIF.

    ENDLOOP.  "  LOOP AT lt_key[] INTO ls_key


    et_idno_data[] = mt_idno_data[] = lt_idno_data[].





    IF lt_idno_data[] IS NOT INITIAL.
* MARM
      SELECT matnr meinh umrez umren eannr ean11 numtp laeng
             breit hoehe meabm volum voleh brgew gewei mesub
      FROM marm
      INTO CORRESPONDING FIELDS OF TABLE mt_marm[]
      FOR ALL ENTRIES IN lt_idno_data[]
      WHERE matnr = lt_idno_data-sap_article
        AND matnr NE space.

* MARC
      SELECT matnr werks bwscl
      FROM marc
      INTO CORRESPONDING FIELDS OF TABLE mt_marc[]
      FOR ALL ENTRIES IN lt_idno_data[]
      WHERE matnr = lt_idno_data-sap_article
        AND matnr NE space.


* MEAN
      SELECT matnr meinh lfnum ean11 eantp hpean
      FROM mean
      INTO CORRESPONDING FIELDS OF TABLE mt_mean[]
      FOR ALL ENTRIES IN lt_idno_data[]
      WHERE matnr = lt_idno_data-sap_article
        AND matnr NE space.

* MAW1
      SELECT matnr wausm
      FROM maw1
      INTO CORRESPONDING FIELDS OF TABLE mt_maw1[]
      FOR ALL ENTRIES IN lt_idno_data[]
      WHERE matnr = lt_idno_data-sap_article
        AND matnr NE space.

* ZMD_HOST_SAP
      SELECT * FROM zmd_host_sap
      INTO CORRESPONDING FIELDS OF TABLE mt_host_sap[]
      FOR ALL ENTRIES IN lt_idno_data[]
      WHERE matnr = lt_idno_data-sap_article
        AND matnr   NE space
        AND deleted EQ space.


* MVKE
      SELECT matnr vkorg vtweg zzcatman sstuf vrkme prodh versg ktgrm vmsta vmstd aumng
      FROM mvke
      INTO CORRESPONDING FIELDS OF TABLE mt_mvke[]
      FOR ALL ENTRIES IN lt_idno_data[]
      WHERE matnr = lt_idno_data-sap_article
        AND matnr NE space.
*        AND (
*              ( ( vkorg EQ zcl_constants=>gc_banner_3000 OR
*                  vkorg EQ zcl_constants=>gc_vkorg_4000  OR
*                  vkorg EQ zcl_constants=>gc_vkorg_5000  OR
*                  vkorg EQ zcl_constants=>gc_vkorg_6000 )
*                AND vtweg EQ '20'
*                AND ( zzcatman NE '' OR sstuf NE '' ) )
*            OR
*              ( vkorg EQ zcl_constants=>gc_banner_1000
*                AND vtweg EQ zcl_constants=>gc_distr_chan_10
*                AND zzcatman NE '' )
*            ).


* WLK2
      SELECT *
      FROM wlk2
      INTO CORRESPONDING FIELDS OF TABLE mt_wlk2[]
      FOR ALL ENTRIES IN lt_idno_data[]
      WHERE matnr = lt_idno_data-sap_article
        AND matnr NE space
        AND werks EQ ''.


      SELECT * FROM eina
      INTO CORRESPONDING FIELDS OF TABLE mt_eina[]
      FOR ALL ENTRIES IN lt_idno_data[]
      WHERE matnr = lt_idno_data-sap_article
        AND matnr NE space.
      IF sy-subrc = 0.
        SELECT * FROM eine
        INTO CORRESPONDING FIELDS OF TABLE mt_eine[]
        FOR ALL ENTRIES IN mt_eina[]
        WHERE infnr = mt_eina-infnr
          AND ekorg = mv_ekorg_main                         " '9999'
          AND werks = space.
      ENDIF.

    ENDIF.   " IF lt_idno_data[] IS NOT INITIAL


    SELECT * FROM zarn_uom_cat
      INTO CORRESPONDING FIELDS OF TABLE mt_uom_cat.

    SELECT * FROM zarn_coo_t
      INTO CORRESPONDING FIELDS OF TABLE mt_coo_t
      WHERE spras = sy-langu.

    SELECT * FROM zarn_gen_uom_t
      INTO CORRESPONDING FIELDS OF TABLE mt_gen_uom_t
      WHERE spras = sy-langu.

    SELECT msehi isocode FROM t006
      INTO CORRESPONDING FIELDS OF TABLE mt_t006[].

    SELECT land1 intca FROM t005
      INTO CORRESPONDING FIELDS OF TABLE mt_t005[].

    SELECT waers isocd FROM tcurc
    INTO CORRESPONDING FIELDS OF TABLE mt_tcurc[].

    SELECT mtart vmtpo FROM t134
      INTO CORRESPONDING FIELDS OF TABLE mt_t134[].

    SELECT vkorg vtweg spart FROM tvta
      INTO CORRESPONDING FIELDS OF TABLE mt_tvta[].










  ENDMETHOD.


  METHOD initialize.


    CLEAR: ms_reg_data_mass, ms_reg_data_db_mass,

           mt_prod_data_db[], mt_reg_data_db[], mt_reg_data_mass[], mt_reg_data_db_mass[],
           mt_reg_data_save[], mt_mass_fields[], mt_idno_data[],

           mt_mara[], mt_marm[], mt_marc[], mt_mean[], mt_eina[], mt_eine[], mt_maw1[],
           mt_mvke[], mt_wlk2[], mt_host_sap[], mt_t006[], mt_t005[], mt_tcurc[],
           mt_coo_t[], mt_gen_uom_t[], mt_t134[], mt_tvarvc_ekorg[],
           mt_tvarvc[], mt_tvarvc_hyb_code[], mt_tvta[], mt_uom_cat[],

           mt_message[], mt_msg[],

           mv_ekorg_main, mv_ekorg_main.



    mv_initialized = abap_true.


*****************************************************************************
* EINE-EKORG from TVARVC for PIR posting for multiple Purch Orgs
    SELECT name type numb sign opti low high FROM tvarvc
      INTO CORRESPONDING FIELDS OF TABLE mt_tvarvc_ekorg[]
      WHERE name = mc_eine_ekorg
        AND type = mc_type_s.

* Main EINE-EKORG from TVARVC for PIR posting for multiple Purch Orgs
    SELECT SINGLE low FROM tvarvc
      INTO mv_ekorg_main
      WHERE name = mc_eine_ekorg
        AND type = mc_type_s
        AND high = abap_true.

* Sales Org For UNI/LNI from TVARVC
    SELECT name type numb sign opti low high FROM tvarvc
      INTO CORRESPONDING FIELDS OF TABLE mt_tvarvc[]
      WHERE name = mc_vkorg_uni
         OR name = mc_vkorg_lni
        AND type = mc_type_s.

* Hybris Code for REG_UOM additional Scenario
    SELECT name type numb sign opti low high FROM tvarvc
      INTO CORRESPONDING FIELDS OF TABLE mt_tvarvc_hyb_code[]
      WHERE name = mc_uom_hyb_code
        AND type = mc_type_s.

  ENDMETHOD.


METHOD is_info_record_chg_relevant.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 23.10.2019
* SYSTEM           # DE0
* SPECIFICATION    # SUP-317
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Returns true if mass change is relevant for info
*                  # record update
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  DATA: lt_field TYPE RANGE OF fieldname.

  IF iv_article IS INITIAL.
    RETURN.
  ENDIF.

  lt_field = VALUE #( sign = 'I' option = 'EQ' ( low = 'EKKOL' )
                                               ( low = 'UEBTO' )
                                               ( low = 'UNTTO' ) ).

  LOOP AT it_mass_fields TRANSPORTING NO FIELDS WHERE table = 'ZARN_REG_PIR' AND field IN lt_field.
    rv_relevant = abap_true.
    RETURN.
  ENDLOOP.

ENDMETHOD.


  METHOD lock_idno_regional.


    DATA: lv_idno	    TYPE zarn_idno,
          lv_locked   TYPE flag,
          lv_error    TYPE flag,
          lv_user     TYPE sy-uname,
          lv_username TYPE ad_namtext.

    lv_idno = iv_idno.

* Lock IDNO
    CALL FUNCTION 'ENQUEUE_EZARN_REGIONAL'
      EXPORTING
        idno           = lv_idno
*       _wait          = abap_true
      EXCEPTIONS
        foreign_lock   = 1
        system_failure = 2
        OTHERS         = 3.
    IF sy-subrc NE 0.

      cv_error = abap_true.

* Check IDNO Lock
      CLEAR lv_locked.
      CALL METHOD me->check_idno_lock
        EXPORTING
          iv_idno   = lv_idno
        IMPORTING
          ev_locked = lv_locked
        CHANGING
          ct_msg    = ct_msg[].

    ENDIF.



  ENDMETHOD.


  METHOD maintain_bal_log.



    DATA:   ls_s_log      TYPE bal_s_log,
            ls_log_handle TYPE balloghndl,
            lt_message    TYPE bal_t_msg,
            ls_message    TYPE bal_s_msg.


    lt_message[] = it_message[].

    ls_s_log-object    = 'ZARN'.
    ls_s_log-subobject = 'ZARN_ART_POST'.
    ls_s_log-aldate    = sy-datum.
    ls_s_log-altime    = sy-uzeit.
    ls_s_log-aluser    = sy-uname.
    ls_s_log-altcode   = sy-tcode.




* Create Log
    CLEAR ls_log_handle.
    CALL FUNCTION 'BAL_LOG_CREATE'
      EXPORTING
        i_s_log                 = ls_s_log
      IMPORTING
        e_log_handle            = ls_log_handle
      EXCEPTIONS
        log_header_inconsistent = 1
        OTHERS                  = 2.
    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
              WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

* Write Message to Log
    LOOP AT lt_message INTO ls_message.

      CALL FUNCTION 'BAL_LOG_MSG_ADD'
        EXPORTING
          i_log_handle     = ls_log_handle
          i_s_msg          = ls_message
        EXCEPTIONS
          log_not_found    = 1
          msg_inconsistent = 2
          log_is_full      = 3
          OTHERS           = 4.

      IF sy-subrc <> 0.
        MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
      ENDIF.

    ENDLOOP.




    CALL METHOD me->save_and_display_bal_log
      EXPORTING
        is_log_handle = ls_log_handle
        iv_save       = 'X'
        iv_display    = 'X'
      CHANGING
        cv_log_no     = ev_log_no.














*  DATA: ls_log             TYPE bal_s_log,
*        ls_display_profile TYPE bal_s_prof,
*        lt_log_handle      TYPE bal_t_logh,
*        lv_log_handle      TYPE balloghndl,
*        lv_text            TYPE text255.
*
*  FIELD-SYMBOLS: <ls_message> LIKE LINE OF it_message.
*
*  ls_log-object    = 'ZARN'.
*  ls_log-subobject = 'ZARN_ART_POST'.
*
*  CALL FUNCTION 'BAL_LOG_CREATE'
*    EXPORTING
*      i_s_log                 = ls_log
*    IMPORTING
*      e_log_handle            = lv_log_handle
*    EXCEPTIONS
*      log_header_inconsistent = 1.
*
*  IF sy-subrc EQ 0.
*    LOOP AT it_message ASSIGNING <ls_message>.
*      IF  <ls_message>-msgid EQ 'ZARENA_MSG'
*      AND <ls_message>-msgno EQ '000'.
*        CONCATENATE
*          <ls_message>-msgv1
*          <ls_message>-msgv2
*          <ls_message>-msgv3
*          <ls_message>-msgv4
*          INTO lv_text.
*        CALL FUNCTION 'BAL_LOG_MSG_ADD_FREE_TEXT'
*          EXPORTING
*            i_log_handle     = lv_log_handle
*            i_msgty          = <ls_message>-msgty
*            i_text           = lv_text
*          EXCEPTIONS
*            log_not_found    = 1
*            msg_inconsistent = 2
*            log_is_full      = 3
*            OTHERS           = 4.
*      ELSE.
*        CALL FUNCTION 'BAL_LOG_MSG_ADD'
*          EXPORTING
*            i_log_handle     = lv_log_handle
*            i_s_msg          = <ls_message>
*          EXCEPTIONS
*            log_not_found    = 1
*            msg_inconsistent = 2
*            log_is_full      = 3.
*      ENDIF.
*    ENDLOOP.
*
*    CALL FUNCTION 'BAL_DSP_PROFILE_POPUP_GET'
**     EXPORTING
**       START_COL                 = 5
**       START_ROW                 = 5
**       END_COL                   = 87
**       END_ROW                   = 25
*      IMPORTING
*        e_s_display_profile = ls_display_profile.
*
*    APPEND lv_log_handle TO lt_log_handle.
*    CALL FUNCTION 'BAL_DSP_LOG_DISPLAY'
*      EXPORTING
*        i_s_display_profile  = ls_display_profile
*        i_t_log_handle       = lt_log_handle
**       I_AMODAL             = ' '
*      EXCEPTIONS
*        profile_inconsistent = 1
*        internal_error       = 2
*        no_data_available    = 3
*        no_authority         = 4.
*  ENDIF.



  ENDMETHOD.


  METHOD maintain_regional_data.

* Update Regional Data into DB
    CALL METHOD me->regional_data_update
      EXPORTING
        is_reg_data_db   = is_reg_data_db
        is_reg_data_mass = is_reg_data_mass.


    IF iv_no_chg_doc IS INITIAL.
* Update Change Documents for Regional Data
      CALL METHOD me->regional_change_doc_update
        EXPORTING
          is_reg_data_old = is_reg_data_db
          is_reg_data_new = is_reg_data_mass
          it_reg_key      = it_reg_key[]
*         it_reg_data_old = lt_reg_data_db
*         it_reg_data_new = lt_reg_data_mass
          iv_bulk_mode    = abap_true.
    ENDIF.



  ENDMETHOD.


METHOD massage_before_post.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 06.11.2019
* SYSTEM           # DE0
* SPECIFICATION    # CIP-682 - Maintain Prices
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Massage data before posting
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  LOOP AT ct_reg_data_mass ASSIGNING FIELD-SYMBOL(<ls_reg_data_mass>).
    LOOP AT <ls_reg_data_mass>-zarn_reg_hdr ASSIGNING FIELD-SYMBOL(<ls_reg_hdr>).

      IF <ls_reg_hdr>-zz_uni_dc IS INITIAL AND
         <ls_reg_hdr>-zz_lni_dc IS INITIAL.
        CLEAR: <ls_reg_hdr>-zzvar_wt_flag.
      ENDIF.

    ENDLOOP.
  ENDLOOP.

  LOOP AT ct_reg_data ASSIGNING FIELD-SYMBOL(<ls_reg_data>).
    LOOP AT <ls_reg_data>-zarn_reg_hdr ASSIGNING <ls_reg_hdr>.

      IF <ls_reg_hdr>-zz_uni_dc IS INITIAL AND
         <ls_reg_hdr>-zz_lni_dc IS INITIAL.
        CLEAR: <ls_reg_hdr>-zzvar_wt_flag.
      ENDIF.

    ENDLOOP.
  ENDLOOP.

ENDMETHOD.


METHOD modify_reg_data_mass_uom.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 18.05.2020
* SYSTEM           # DE0
* SPECIFICATION    # 9000007141: SSM-8 Symphony -ZARN_MASS
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Modify Regional UOM mass data entered by user
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  DATA: ls_reg_uom TYPE zarn_reg_uom,
        lv_ignored TYPE flag.

  " Note: This ignores some of the changes entered by the user in ZARN_MASS

  SELECT *
    FROM zarn_reg_uom_mc
    INTO TABLE @DATA(lt_reg_uom_mc)
    WHERE mass_change_allowed = @abap_true.

  LOOP AT ct_reg_uom_mass ASSIGNING FIELD-SYMBOL(<ls_reg_uom_mass>).

    CLEAR ls_reg_uom.

    " Only certain UOM categories are allowed for mass change. Otherwise, ignore all user changes
    READ TABLE lt_reg_uom_mc WITH KEY uom_category = <ls_reg_uom_mass>-uom_category TRANSPORTING NO FIELDS.
    IF sy-subrc <> 0.
      lv_ignored = abap_true.
      MESSAGE w153 WITH <ls_reg_uom_mass>-idno <ls_reg_uom_mass>-uom_category INTO zcl_message_services=>sv_msg_dummy.
      add_bal_message( is_idno_data = VALUE #( idno = <ls_reg_uom_mass>-idno ) ).
      CLEAR: <ls_reg_uom_mass>-uom_category.
      CONTINUE.
    ENDIF.

    DATA(ls_reg_uom_orig) = <ls_reg_uom_mass>.

    READ TABLE it_reg_uom_db INTO ls_reg_uom
    WITH  KEY idno                 = <ls_reg_uom_mass>-idno
              uom_category         = <ls_reg_uom_mass>-uom_category
              pim_uom_code         = <ls_reg_uom_mass>-pim_uom_code
              hybris_internal_code = <ls_reg_uom_mass>-hybris_internal_code
              lower_uom            = <ls_reg_uom_mass>-lower_uom
              lower_child_uom      = <ls_reg_uom_mass>-lower_child_uom.
    IF sy-subrc = 0.
      " Default Weight Unit
      IF <ls_reg_uom_mass>-net_weight_uom IS INITIAL.
        <ls_reg_uom_mass>-net_weight_uom = gc_default_weight_uom.
      ENDIF.
      IF <ls_reg_uom_mass>-gross_weight_uom IS INITIAL.
        <ls_reg_uom_mass>-gross_weight_uom = gc_default_weight_uom.
      ENDIF.

      " Default Dimention UOM
      IF <ls_reg_uom_mass>-height_uom IS INITIAL.
        <ls_reg_uom_mass>-height_uom = gc_default_dim_uom.
      ENDIF.
      IF <ls_reg_uom_mass>-width_uom IS INITIAL.
        <ls_reg_uom_mass>-width_uom = gc_default_dim_uom.
      ENDIF.
      IF <ls_reg_uom_mass>-depth_uom IS INITIAL.
        <ls_reg_uom_mass>-depth_uom = gc_default_dim_uom.
      ENDIF.
    ENDIF.

    "Read Lower UOMs num/den from MASS Data  (updated data if found)
    CLEAR ls_reg_uom.
    IF <ls_reg_uom_mass>-uom_category = 'PALLET'.
      READ TABLE ct_reg_uom_mass INTO ls_reg_uom
          WITH KEY idno                 = <ls_reg_uom_mass>-idno
                   uom_category         = 'LAYER'
                   pim_uom_code         = <ls_reg_uom_mass>-pim_uom_code
                   hybris_internal_code = <ls_reg_uom_mass>-hybris_internal_code
                   lower_uom            = <ls_reg_uom_mass>-lower_uom
                   lower_child_uom      = <ls_reg_uom_mass>-lower_child_uom.
    ELSE.
      READ TABLE ct_reg_uom_mass INTO ls_reg_uom
          WITH KEY idno                 = <ls_reg_uom_mass>-idno
                   pim_uom_code         = <ls_reg_uom_mass>-lower_uom
                   hybris_internal_code = space
                   lower_uom            = <ls_reg_uom_mass>-lower_child_uom.
    ENDIF.

    "Read Lower UOMs num/den from DB if not found in MASS Data
    IF ls_reg_uom IS INITIAL.
      IF <ls_reg_uom_mass>-uom_category = 'PALLET'.
        READ TABLE it_reg_uom_db INTO ls_reg_uom
            WITH KEY idno                 = <ls_reg_uom_mass>-idno
                     uom_category         = 'LAYER'
                     pim_uom_code         = <ls_reg_uom_mass>-pim_uom_code
                     hybris_internal_code = <ls_reg_uom_mass>-hybris_internal_code
                     lower_uom            = <ls_reg_uom_mass>-lower_uom
                     lower_child_uom      = <ls_reg_uom_mass>-lower_child_uom.
      ELSE.
        READ TABLE it_reg_uom_db INTO ls_reg_uom
            WITH KEY idno                 = <ls_reg_uom_mass>-idno
                     pim_uom_code         = <ls_reg_uom_mass>-lower_uom
                     hybris_internal_code = space
                     lower_uom            = <ls_reg_uom_mass>-lower_child_uom.
      ENDIF.
    ENDIF.


    IF <ls_reg_uom_mass>-num_base_units IS INITIAL.
      <ls_reg_uom_mass>-num_base_units = 1.
    ENDIF.

    IF <ls_reg_uom_mass>-factor_of_base_units IS INITIAL.
      <ls_reg_uom_mass>-factor_of_base_units = 1.
    ENDIF.

    IF ls_reg_uom-num_base_units IS INITIAL.
      ls_reg_uom-num_base_units = 1.
    ENDIF.

    IF ls_reg_uom-factor_of_base_units IS INITIAL.
      ls_reg_uom-factor_of_base_units = 1.
    ENDIF.

    <ls_reg_uom_mass>-higher_level_units =
      ( <ls_reg_uom_mass>-num_base_units / <ls_reg_uom_mass>-factor_of_base_units ) /
      ( ls_reg_uom-num_base_units / ls_reg_uom-factor_of_base_units ).

  ENDLOOP.

  IF lv_ignored = abap_true.
    DELETE ct_reg_uom_mass WHERE uom_category IS INITIAL.
  ENDIF.

ENDMETHOD.


  METHOD post_conditions.



    DATA: lv_error              TYPE flag,
          ls_calp_vb            TYPE calp_vb,
*        ls_inforecord_general TYPE bapieina,
          lt_calp_err           TYPE STANDARD TABLE OF erro,
          ls_calp_err           TYPE erro,
          lv_tabix              TYPE sy-tabix,
          ls_msg                TYPE massmsg,


          lt_calp_vb            TYPE calp_vb_tab,
          lt_inforecord_general	TYPE wrf_bapieina_tty,
          ls_idno_data          TYPE ty_s_idno_data.



    lt_calp_vb[]            = it_calp_vb[].
    lt_inforecord_general[] = it_inforecord_general[].
    ls_idno_data            = is_idno_data.






* Don't Post conditions for records for which PIR is not posted
    LOOP AT lt_calp_vb INTO ls_calp_vb.
      lv_tabix = sy-tabix.

      READ TABLE lt_inforecord_general[] TRANSPORTING NO FIELDS
      WITH KEY material = ls_calp_vb-matnr
               vendor   = ls_calp_vb-lifnr.
      IF sy-subrc NE 0.
        DELETE lt_calp_vb[] INDEX lv_tabix.
      ENDIF.
    ENDLOOP.  " LOOP AT lt_calp_vb INTO ls_calp_vb

    IF lt_calp_vb[] IS INITIAL.
      RETURN.
    ENDIF.


* Post all conditions in one step to avoid short dump
* because of duplicate key's concerning unit of measure
* i.e. when posting into A018
    CLEAR: lt_calp_err[], lv_error.
    CALL FUNCTION 'SALES_CONDITIONS_POSTE'
      EXPORTING
        pi_commit_work         = ' '
      TABLES
        px_t_calp_vb           = lt_calp_vb[]
        pe_t_erro              = lt_calp_err[]
      EXCEPTIONS
        no_records_for_posting = 1
        OTHERS                 = 2.
    IF sy-subrc = 0.

      LOOP AT lt_calp_err[] INTO ls_calp_err.
* Build Return Message
        CLEAR ls_msg.
        ls_msg-objkey = 'ZARENA'.
        ls_msg-msgty  = ls_calp_err-msgty.
        ls_msg-msgid  = ls_calp_err-msgid.
        ls_msg-msgno  = ls_calp_err-msgno.
        ls_msg-msgv1  = ls_calp_err-msgv1.
        ls_msg-msgv2  = ls_calp_err-msgv2.
        ls_msg-msgv3  = ls_calp_err-msgv3.
        ls_msg-msgv4  = ls_calp_err-msgv4.
        APPEND ls_msg TO ct_msg[].

        IF ls_calp_err-msgty = 'E'.
          lv_error = abap_true.
        ENDIF.
      ENDLOOP.

    ELSE.
      lv_error = abap_true.
    ENDIF.

    IF lv_error = abap_true.
      ROLLBACK WORK.
    ELSE.
      COMMIT WORK AND WAIT.
    ENDIF.

    cv_error = lv_error.


  ENDMETHOD.


METHOD post_layout_module.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 03.08.2020
* SYSTEM           # DE0
* SPECIFICATION    # SSM-1: Base Build
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Posts layout module
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  DATA: lt_mass_message TYPE mass_msgs.
  DATA(lo_helper) = zcl_arena_helper=>get_instance( ).
  lo_helper->post_layout_module( EXPORTING iv_article     = iv_article
                                           it_reg_cluster = it_reg_cluster_mass
                                 IMPORTING et_message     = DATA(lt_message) ).

  " Note: AReNa cluster tables will be updated irrespective of assignment failure/success. Hence, we need to
  " save assignments which were successful even though we have few failures. In the next run, it will process
  " failed assignments
  CALL FUNCTION 'BAPI_TRANSACTION_COMMIT'
    EXPORTING
      wait = abap_true.

  lt_mass_message = mo_message_service->convert_to_mass_messages( EXPORTING iv_object_key = zif_mass_message_object_key=>gc_arena
                                                                            it_message    = lt_message ).
  APPEND LINES OF lt_mass_message TO ct_message.

ENDMETHOD.


  METHOD post_mass_data.
*-----------------------------------------------------------------------
* DATE             # 03.08.2020
* CHANGE No.       # SSM-1: NW Range-ZARN_GUI CHANGE
* DESCRIPTION      # Code clean-up
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
    DATA: ls_idno_data     TYPE ty_s_idno_data,
          lt_mass_fields   TYPE ty_t_mass_fields,
          ls_mass_fields   TYPE ty_s_mass_fields,
          ls_prod_data     TYPE zsarn_prod_data,
          ls_reg_data      TYPE zsarn_reg_data,
          ls_reg_data_mass TYPE zsarn_reg_data.

    DATA : lt_return                TYPE STANDARD TABLE OF bapireturn1,
           ls_return                TYPE bapireturn1,
           ls_message               TYPE bal_s_msg,
           ls_msg                   TYPE massmsg,

           lv_error                 TYPE flag,
           lv_main_data_error       TYPE boolean,
           lv_additional_data_error TYPE boolean.


    ls_idno_data     = is_idno_data.
    lt_mass_fields   = it_mass_fields[].
    ls_prod_data     = is_prod_data.
    ls_reg_data      = is_reg_data.
    ls_reg_data_mass = is_reg_data_mass.



    CLEAR: lt_return[], lv_main_data_error, lv_additional_data_error.
    CALL FUNCTION 'WRF_MATERIAL_MAINTAINDATA_RT'
      EXPORTING
        headdata                 = cs_headdata
        badi_callmode            = '14'
      IMPORTING
        ev_main_data_error       = cv_main_data_error
        ev_additional_data_error = cv_additional_data_error
      TABLES
        return                   = lt_return[]
        clientdata               = ct_clientdata[]
        clientdatax              = ct_clientdatax[]
        clientext                = ct_clientext[]
        clientextx               = ct_clientextx[]
        addnlclientdata          = ct_addnlclientdata[]
        addnlclientdatax         = ct_addnlclientdatax[]
        materialdescription      = ct_materialdescription[]
        plantdata                = ct_plantdata[]
        plantdatax               = ct_plantdatax[]
        plantext                 = ct_plantext[]                                    "++ONLD-822 JKH 17.02.2017
        plantextx                = ct_plantextx[]                                   "++ONLD-822 JKH 17.02.2017
        unitsofmeasure           = ct_unitsofmeasure[]
        unitsofmeasurex          = ct_unitsofmeasurex[]
        internationalartnos      = ct_internationalartnos[]
        valuationdata            = ct_valuationdata[]
        valuationdatax           = ct_valuationdatax[]
        warehousenumberdata      = ct_warehousenumberdata[]                         "++ONLD-822 JKH 17.02.2017
        warehousenumberdatax     = ct_warehousenumberdatax[]                        "++ONLD-822 JKH 17.02.2017
        salesdata                = ct_salesdata[]
        salesdatax               = ct_salesdatax[]
        salesext                 = ct_salesext[]
        salesextx                = ct_salesextx[]
        posdata                  = ct_posdata[]
        posdatax                 = ct_posdatax[]
        plantkeys                = ct_plantkeys[]
        storagelocationkeys      = ct_storagelocationkeys[]
        distrchainkeys           = ct_distrchainkeys[]
        valuationtypekeys        = ct_valuationtypekeys[]
        importextension          = ct_importextension[]
        inforecord_general       = ct_inforecord_general[]
        inforecord_purchorg      = ct_inforecord_purchorg[].

    CLEAR lv_error.
    LOOP AT lt_return[] INTO ls_return.
      CLEAR ls_msg.
      ls_msg-objkey = 'ZARENA'.
      ls_msg-msgty  = ls_return-type.
      ls_msg-msgid  = ls_return-id.
      ls_msg-msgno  = ls_return-number.
      ls_msg-msgv1  = ls_return-message_v1.
      ls_msg-msgv2  = ls_return-message_v2.
      ls_msg-msgv3  = ls_return-message_v3.
      ls_msg-msgv4  = ls_return-message_v4.
      APPEND ls_msg TO ct_msg[].

      IF ls_return-type = 'E'.
        lv_error = abap_true.
      ENDIF.

    ENDLOOP.  " LOOP AT lt_return[] INTO ls_return


    cv_error    = lv_error.
    ct_return[] = lt_return[].


    IF cv_error = abap_true.
      " Rollback
      CALL FUNCTION 'BAPI_TRANSACTION_ROLLBACK'.
    ELSE.
      " Commit
      CALL FUNCTION 'BAPI_TRANSACTION_COMMIT'
        EXPORTING
          wait = abap_true.

* Post Conditions
      CALL METHOD me->post_conditions
        EXPORTING
          it_calp_vb            = ct_calp_vb[]
          it_inforecord_general = ct_inforecord_general[]
          is_idno_data          = ls_idno_data
        CHANGING
          ct_msg                = ct_msg
          cv_error              = cv_error.
    ENDIF.


  ENDMETHOD.


  METHOD regional_change_doc_update.
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             # 20.07.2020
* CHANGE No.       # CIP-1359: ZARN_GUI changes (GS1  Updates)
* DESCRIPTION      # Added regional table ZARN_REG_NUTRIEN
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------


    DATA: ls_reg_data_old      TYPE zsarn_reg_data,
          ls_reg_data_new      TYPE zsarn_reg_data,
          lt_reg_key           TYPE ty_t_reg_key,
          ls_reg_key           TYPE zsarn_reg_key,
          lt_reg_old           TYPE ztarn_reg_data,
          ls_reg_old           TYPE zsarn_reg_data,
          lt_reg_new           TYPE ztarn_reg_data,
          ls_reg_new           TYPE zsarn_reg_data,

          lt_cdtxt             TYPE                    isu_cdtxt,

          lt_xzarn_control     TYPE STANDARD TABLE OF yzarn_control,
          lt_yzarn_control     TYPE STANDARD TABLE OF yzarn_control,
          lt_xzarn_prd_version TYPE STANDARD TABLE OF yzarn_prd_version,
          lt_yzarn_prd_version TYPE STANDARD TABLE OF yzarn_prd_version,
          lt_xzarn_ver_status  TYPE STANDARD TABLE OF yzarn_ver_status,
          lt_yzarn_ver_status  TYPE STANDARD TABLE OF yzarn_ver_status,

          lt_xzarn_reg_dc_sell TYPE STANDARD TABLE OF yzarn_reg_dc_sell,
          lt_yzarn_reg_dc_sell TYPE STANDARD TABLE OF yzarn_reg_dc_sell,
          lt_xzarn_reg_lst_prc TYPE STANDARD TABLE OF yzarn_reg_lst_prc,
          lt_yzarn_reg_lst_prc TYPE STANDARD TABLE OF yzarn_reg_lst_prc,
          lt_xzarn_reg_rrp     TYPE STANDARD TABLE OF yzarn_reg_rrp,
          lt_yzarn_reg_rrp     TYPE STANDARD TABLE OF yzarn_reg_rrp,
          lt_xzarn_reg_std_ter TYPE STANDARD TABLE OF yzarn_reg_std_ter,
          lt_yzarn_reg_std_ter TYPE STANDARD TABLE OF yzarn_reg_std_ter,
          lt_xzarn_reg_ean     TYPE STANDARD TABLE OF yzarn_reg_ean,
          lt_yzarn_reg_ean     TYPE STANDARD TABLE OF yzarn_reg_ean,
          lt_xzarn_reg_hdr     TYPE STANDARD TABLE OF yzarn_reg_hdr,
          lt_yzarn_reg_hdr     TYPE STANDARD TABLE OF yzarn_reg_hdr,
          lt_xzarn_reg_hsno    TYPE STANDARD TABLE OF yzarn_reg_hsno,
          lt_yzarn_reg_hsno    TYPE STANDARD TABLE OF yzarn_reg_hsno,
          lt_xzarn_reg_pir     TYPE STANDARD TABLE OF yzarn_reg_pir,
          lt_yzarn_reg_pir     TYPE STANDARD TABLE OF yzarn_reg_pir,
          lt_xzarn_reg_prfam   TYPE STANDARD TABLE OF yzarn_reg_prfam,
          lt_yzarn_reg_prfam   TYPE STANDARD TABLE OF yzarn_reg_prfam,
          lt_xzarn_reg_txt     TYPE STANDARD TABLE OF yzarn_reg_txt,
          lt_yzarn_reg_txt     TYPE STANDARD TABLE OF yzarn_reg_txt,
          lt_xzarn_reg_uom     TYPE STANDARD TABLE OF yzarn_reg_uom,
          lt_yzarn_reg_uom     TYPE STANDARD TABLE OF yzarn_reg_uom,
          lt_xzarn_reg_banner  TYPE STANDARD TABLE OF yzarn_reg_banner,
          lt_yzarn_reg_banner  TYPE STANDARD TABLE OF yzarn_reg_banner,
          lt_xzarn_reg_cluster TYPE STANDARD TABLE OF yzarn_reg_cluster,
          lt_yzarn_reg_cluster TYPE STANDARD TABLE OF yzarn_reg_cluster,
          lt_xzarn_reg_artlink TYPE STANDARD TABLE OF yzarn_reg_artlink,
          lt_yzarn_reg_artlink TYPE STANDARD TABLE OF yzarn_reg_artlink,
          lt_xzarn_reg_onlcat  TYPE STANDARD TABLE OF yzarn_reg_onlcat,
          lt_yzarn_reg_onlcat  TYPE STANDARD TABLE OF yzarn_reg_onlcat,
          lt_xzarn_reg_allerg  TYPE STANDARD TABLE OF yzarn_reg_allerg,
          lt_yzarn_reg_allerg  TYPE STANDARD TABLE OF yzarn_reg_allerg,
          lt_xzarn_reg_nutrien TYPE STANDARD TABLE OF yzarn_reg_nutrien,
          lt_yzarn_reg_nutrien TYPE STANDARD TABLE OF yzarn_reg_nutrien,
          lt_xzarn_reg_str     TYPE yzarn_reg_strs,
          lt_yzarn_reg_str     TYPE yzarn_reg_strs,

          lv_objectid          TYPE cdobjectv,
          lv_change_number     TYPE cdchangenr,
          lv_chg_flag          TYPE cdchngindh      VALUE 'U',
          lv_update_flag       TYPE cdflag          VALUE 'U'.

    ls_reg_data_old = is_reg_data_old.
    ls_reg_data_new = is_reg_data_new.
    lt_reg_key[]    = it_reg_key[].
    lt_reg_old[]    = it_reg_data_old[].
    lt_reg_new[]    = it_reg_data_new[].


    IF iv_bulk_mode = abap_true.
* If all the IDNO data is collected in REG_DATA structure then collect the data as per IDNO

      CLEAR: lt_reg_old[], lt_reg_new[].
      LOOP AT lt_reg_key[] INTO ls_reg_key.

* Build Old and New tables as per IDNO
        CLEAR ls_reg_old.
        ls_reg_old-idno = ls_reg_key-idno.

        ls_reg_old-zarn_reg_hdr[]     = VALUE #( FOR hdr IN ls_reg_data_old-zarn_reg_hdr[]
                                                  WHERE ( idno = ls_reg_key-idno ) ( hdr ) ).

        ls_reg_old-zarn_reg_hsno[]    = VALUE #( FOR hsno IN ls_reg_data_old-zarn_reg_hsno[]
                                                  WHERE ( idno = ls_reg_key-idno ) ( hsno ) ).

        ls_reg_old-zarn_reg_banner[]  = VALUE #( FOR banner IN ls_reg_data_old-zarn_reg_banner[]
                                                  WHERE ( idno = ls_reg_key-idno ) ( banner ) ).

        ls_reg_old-zarn_reg_cluster  = VALUE #( FOR ls_cluster IN ls_reg_data_old-zarn_reg_cluster
                                                         WHERE ( idno = ls_reg_key-idno ) ( ls_cluster ) ).

        ls_reg_old-zarn_reg_ean[]     = VALUE #( FOR ean IN ls_reg_data_old-zarn_reg_ean[]
                                                  WHERE ( idno = ls_reg_key-idno ) ( ean ) ).

        ls_reg_old-zarn_reg_pir[]     = VALUE #( FOR pir IN ls_reg_data_old-zarn_reg_pir[]
                                                  WHERE ( idno = ls_reg_key-idno ) ( pir ) ).

        ls_reg_old-zarn_reg_prfam[]   = VALUE #( FOR prfam IN ls_reg_data_old-zarn_reg_prfam[]
                                                  WHERE ( idno = ls_reg_key-idno ) ( prfam ) ).

        ls_reg_old-zarn_reg_txt[]     = VALUE #( FOR txt IN ls_reg_data_old-zarn_reg_txt[]
                                                  WHERE ( idno = ls_reg_key-idno ) ( txt ) ).

        ls_reg_old-zarn_reg_uom[]     = VALUE #( FOR uom IN ls_reg_data_old-zarn_reg_uom[]
                                                  WHERE ( idno = ls_reg_key-idno ) ( uom ) ).

        ls_reg_old-zarn_reg_lst_prc[] = VALUE #( FOR lst_prc IN ls_reg_data_old-zarn_reg_lst_prc[]
                                                  WHERE ( idno = ls_reg_key-idno ) ( lst_prc ) ).

        ls_reg_old-zarn_reg_dc_sell[] = VALUE #( FOR dc_sell IN ls_reg_data_old-zarn_reg_dc_sell[]
                                                  WHERE ( idno = ls_reg_key-idno ) ( dc_sell ) ).

        ls_reg_old-zarn_reg_std_ter[] = VALUE #( FOR std_ter IN ls_reg_data_old-zarn_reg_std_ter[]
                                                  WHERE ( idno = ls_reg_key-idno ) ( std_ter ) ).

        ls_reg_old-zarn_reg_rrp[]     = VALUE #( FOR reg_rrp IN ls_reg_data_old-zarn_reg_rrp[]
                                                  WHERE ( idno = ls_reg_key-idno ) ( reg_rrp ) ).

        ls_reg_old-zarn_reg_onlcat[]  = VALUE #( FOR reg_onlcat IN ls_reg_data_old-zarn_reg_onlcat[]
                                                  WHERE ( idno = ls_reg_key-idno ) ( reg_onlcat ) ).

        ls_reg_old-zarn_reg_str[]     = VALUE #( FOR reg_str IN ls_reg_data_old-zarn_reg_str[]
                                                  WHERE ( idno = ls_reg_key-idno ) ( reg_str ) ).

        ls_reg_old-zarn_reg_allerg[]     = VALUE #( FOR reg_allerg IN ls_reg_data_old-zarn_reg_allerg[]
                                                  WHERE ( idno = ls_reg_key-idno ) ( reg_allerg ) ).

        APPEND ls_reg_old TO lt_reg_old[].




* Build New tables as per IDNO
        CLEAR ls_reg_new.
        ls_reg_new-idno = ls_reg_key-idno.

        ls_reg_new-zarn_reg_hdr[]     = VALUE #( FOR hdr IN ls_reg_data_new-zarn_reg_hdr[]
                                                  WHERE ( idno = ls_reg_key-idno ) ( hdr ) ).

        ls_reg_new-zarn_reg_hsno[]    = VALUE #( FOR hsno IN ls_reg_data_new-zarn_reg_hsno[]
                                                  WHERE ( idno = ls_reg_key-idno ) ( hsno ) ).

        ls_reg_new-zarn_reg_banner[]  = VALUE #( FOR banner IN ls_reg_data_new-zarn_reg_banner[]
                                                  WHERE ( idno = ls_reg_key-idno ) ( banner ) ).

*        ls_reg_new-zarn_reg_cluster  = VALUE #( FOR ls_cluster IN ls_reg_data_new-zarn_reg_cluster
*                                                       WHERE ( idno = ls_reg_key-idno ) ( ls_cluster ) ).

        ls_reg_new-zarn_reg_ean[]     = VALUE #( FOR ean IN ls_reg_data_new-zarn_reg_ean[]
                                                  WHERE ( idno = ls_reg_key-idno ) ( ean ) ).

        ls_reg_new-zarn_reg_pir[]     = VALUE #( FOR pir IN ls_reg_data_new-zarn_reg_pir[]
                                                  WHERE ( idno = ls_reg_key-idno ) ( pir ) ).

        ls_reg_new-zarn_reg_prfam[]   = VALUE #( FOR prfam IN ls_reg_data_new-zarn_reg_prfam[]
                                                  WHERE ( idno = ls_reg_key-idno ) ( prfam ) ).

        ls_reg_new-zarn_reg_txt[]     = VALUE #( FOR txt IN ls_reg_data_new-zarn_reg_txt[]
                                                  WHERE ( idno = ls_reg_key-idno ) ( txt ) ).

        ls_reg_new-zarn_reg_uom[]     = VALUE #( FOR uom IN ls_reg_data_new-zarn_reg_uom[]
                                                  WHERE ( idno = ls_reg_key-idno ) ( uom ) ).

        ls_reg_new-zarn_reg_lst_prc[] = VALUE #( FOR lst_prc IN ls_reg_data_new-zarn_reg_lst_prc[]
                                                  WHERE ( idno = ls_reg_key-idno ) ( lst_prc ) ).

        ls_reg_new-zarn_reg_dc_sell[] = VALUE #( FOR dc_sell IN ls_reg_data_new-zarn_reg_dc_sell[]
                                                  WHERE ( idno = ls_reg_key-idno ) ( dc_sell ) ).

        ls_reg_new-zarn_reg_std_ter[] = VALUE #( FOR std_ter IN ls_reg_data_new-zarn_reg_std_ter[]
                                                  WHERE ( idno = ls_reg_key-idno ) ( std_ter ) ).

        ls_reg_new-zarn_reg_rrp[]     = VALUE #( FOR reg_rrp IN ls_reg_data_new-zarn_reg_rrp[]
                                                  WHERE ( idno = ls_reg_key-idno ) ( reg_rrp ) ).

        ls_reg_new-zarn_reg_onlcat[]  = VALUE #( FOR reg_onlcat IN ls_reg_data_new-zarn_reg_onlcat[]
                                                  WHERE ( idno = ls_reg_key-idno ) ( reg_onlcat ) ).

        ls_reg_new-zarn_reg_str[]     = VALUE #( FOR reg_str IN ls_reg_data_new-zarn_reg_str[]
                                                  WHERE ( idno = ls_reg_key-idno ) ( reg_str ) ).

        ls_reg_new-zarn_reg_allerg[]  = VALUE #( FOR reg_allerg IN ls_reg_data_new-zarn_reg_allerg[]
                                                  WHERE ( idno = ls_reg_key-idno ) ( reg_allerg ) ).

        APPEND ls_reg_new TO lt_reg_new[].
      ENDLOOP.  " LOOP AT lt_reg_key[] INTO ls_reg_key
    ENDIF.


    IF lt_reg_old[] IS INITIAL AND lt_reg_new[] IS INITIAL.
      RETURN.
    ENDIF.

* Maintain change document per IDNO
    LOOP AT lt_reg_key[] INTO ls_reg_key.

      CLEAR lv_objectid.
      lv_objectid = ls_reg_key-idno.

*  Old values
      CLEAR ls_reg_old.
      READ TABLE lt_reg_old[] INTO ls_reg_old
           WITH KEY idno = ls_reg_key-idno.

      lt_yzarn_reg_banner   = ls_reg_old-zarn_reg_banner.
      lt_yzarn_reg_cluster  = ls_reg_old-zarn_reg_cluster.
      lt_yzarn_reg_ean      = ls_reg_old-zarn_reg_ean.
      lt_yzarn_reg_hdr      = ls_reg_old-zarn_reg_hdr.
      lt_yzarn_reg_hsno     = ls_reg_old-zarn_reg_hsno.
      lt_yzarn_reg_pir      = ls_reg_old-zarn_reg_pir.
      lt_yzarn_reg_prfam    = ls_reg_old-zarn_reg_prfam.
      lt_yzarn_reg_txt      = ls_reg_old-zarn_reg_txt.
      lt_yzarn_reg_uom      = ls_reg_old-zarn_reg_uom.
      lt_yzarn_reg_dc_sell  = ls_reg_old-zarn_reg_dc_sell.
      lt_yzarn_reg_std_ter  = ls_reg_old-zarn_reg_std_ter.
      lt_yzarn_reg_lst_prc  = ls_reg_old-zarn_reg_lst_prc.
      lt_yzarn_reg_rrp      = ls_reg_old-zarn_reg_rrp.
      lt_yzarn_reg_artlink  = ls_reg_old-zarn_reg_artlink.
      lt_yzarn_reg_onlcat   = ls_reg_old-zarn_reg_onlcat.
      lt_yzarn_reg_allerg   = ls_reg_old-zarn_reg_allerg.
      lt_yzarn_reg_nutrien   = ls_reg_old-zarn_reg_nutrien.
      MOVE-CORRESPONDING ls_reg_old-zarn_reg_str TO lt_yzarn_reg_str.


*  New values
      CLEAR ls_reg_new.
      READ TABLE lt_reg_new[] INTO ls_reg_new
           WITH KEY idno = ls_reg_key-idno.

      lt_xzarn_reg_banner   = ls_reg_new-zarn_reg_banner.
      lt_xzarn_reg_cluster  = ls_reg_new-zarn_reg_cluster.
      lt_xzarn_reg_ean      = ls_reg_new-zarn_reg_ean.
      lt_xzarn_reg_hdr      = ls_reg_new-zarn_reg_hdr.
      lt_xzarn_reg_hsno     = ls_reg_new-zarn_reg_hsno.
      lt_xzarn_reg_pir      = ls_reg_new-zarn_reg_pir.
      lt_xzarn_reg_prfam    = ls_reg_new-zarn_reg_prfam.
      lt_xzarn_reg_txt      = ls_reg_new-zarn_reg_txt.
      lt_xzarn_reg_uom      = ls_reg_new-zarn_reg_uom.
      lt_xzarn_reg_dc_sell  = ls_reg_new-zarn_reg_dc_sell.
      lt_xzarn_reg_std_ter  = ls_reg_new-zarn_reg_std_ter.
      lt_xzarn_reg_lst_prc  = ls_reg_new-zarn_reg_lst_prc.
      lt_xzarn_reg_rrp      = ls_reg_new-zarn_reg_rrp.
      lt_xzarn_reg_artlink  = ls_reg_new-zarn_reg_artlink.
      lt_xzarn_reg_onlcat   = ls_reg_new-zarn_reg_onlcat.
      lt_xzarn_reg_allerg   = ls_reg_new-zarn_reg_allerg.
      lt_xzarn_reg_nutrien  = ls_reg_new-zarn_reg_nutrien.
      MOVE-CORRESPONDING ls_reg_new-zarn_reg_str TO lt_xzarn_reg_str.

      SORT: lt_yzarn_reg_banner[],
            lt_yzarn_reg_cluster,
            lt_yzarn_reg_ean[],
            lt_yzarn_reg_hdr[],
            lt_yzarn_reg_hsno[],
            lt_yzarn_reg_pir[],
            lt_yzarn_reg_prfam[],
            lt_yzarn_reg_txt[],
            lt_yzarn_reg_uom[],
            lt_yzarn_reg_dc_sell[],
            lt_yzarn_reg_std_ter[],
            lt_yzarn_reg_lst_prc[],
            lt_yzarn_reg_rrp[],
            lt_yzarn_reg_artlink[],
            lt_yzarn_reg_onlcat[],
            lt_yzarn_reg_str[],
            lt_yzarn_reg_allerg[],
            lt_yzarn_reg_nutrien,

            lt_xzarn_reg_banner[],
            lt_xzarn_reg_cluster,
            lt_xzarn_reg_ean[],
            lt_xzarn_reg_hdr[],
            lt_xzarn_reg_hsno[],
            lt_xzarn_reg_pir[],
            lt_xzarn_reg_prfam[],
            lt_xzarn_reg_txt[],
            lt_xzarn_reg_uom[],
            lt_xzarn_reg_dc_sell[],
            lt_xzarn_reg_std_ter[],
            lt_xzarn_reg_lst_prc[],
            lt_xzarn_reg_rrp[],
            lt_xzarn_reg_artlink[],
            lt_xzarn_reg_onlcat[],
            lt_xzarn_reg_str[],
            lt_xzarn_reg_allerg[],
            lt_xzarn_reg_nutrien.


*   Change document
*
      CALL FUNCTION 'ZARN_CTRL_REGNL_WRITE_DOC_CUST'
        EXPORTING
          objectid                = lv_objectid
          tcode                   = sy-tcode
          utime                   = sy-uzeit
          udate                   = sy-datum
          username                = sy-uname
          object_change_indicator = lv_chg_flag
          upd_zarn_reg_allerg     = lv_update_flag
          upd_zarn_reg_artlink    = lv_update_flag
          upd_zarn_reg_banner     = lv_update_flag
          upd_zarn_reg_cluster    = lv_update_flag
          upd_zarn_reg_dc_sell    = lv_update_flag
          upd_zarn_reg_ean        = lv_update_flag
          upd_zarn_reg_hdr        = lv_update_flag
          upd_zarn_reg_hsno       = lv_update_flag
          upd_zarn_reg_lst_prc    = lv_update_flag
          upd_zarn_reg_onlcat     = lv_update_flag
          upd_zarn_reg_pir        = lv_update_flag
          upd_zarn_reg_prfam      = lv_update_flag
          upd_zarn_reg_rrp        = lv_update_flag
          upd_zarn_reg_std_ter    = lv_update_flag
          upd_zarn_reg_str        = lv_update_flag
          xzarn_reg_str           = lt_xzarn_reg_str
          yzarn_reg_str           = lt_yzarn_reg_str
          upd_zarn_reg_txt        = lv_update_flag
          upd_zarn_reg_uom        = lv_update_flag
          upd_zarn_reg_nutrien    = lv_update_flag
        IMPORTING
          ev_changenumber         = lv_change_number
        TABLES
          icdtxt_zarn_ctrl_regnl  = lt_cdtxt
          xzarn_control           = lt_xzarn_control
          yzarn_control           = lt_yzarn_control
          xzarn_prd_version       = lt_xzarn_prd_version
          yzarn_prd_version       = lt_yzarn_prd_version
          xzarn_reg_allerg        = lt_xzarn_reg_allerg
          yzarn_reg_allerg        = lt_yzarn_reg_allerg
          xzarn_reg_artlink       = lt_xzarn_reg_artlink
          yzarn_reg_artlink       = lt_yzarn_reg_artlink
          xzarn_reg_banner        = lt_xzarn_reg_banner
          yzarn_reg_banner        = lt_yzarn_reg_banner
          xzarn_reg_cluster       = lt_xzarn_reg_cluster
          yzarn_reg_cluster       = lt_yzarn_reg_cluster
          xzarn_reg_dc_sell       = lt_xzarn_reg_dc_sell
          yzarn_reg_dc_sell       = lt_yzarn_reg_dc_sell
          xzarn_reg_ean           = lt_xzarn_reg_ean
          yzarn_reg_ean           = lt_yzarn_reg_ean
          xzarn_reg_hdr           = lt_xzarn_reg_hdr
          yzarn_reg_hdr           = lt_yzarn_reg_hdr
          xzarn_reg_hsno          = lt_xzarn_reg_hsno
          yzarn_reg_hsno          = lt_yzarn_reg_hsno
          xzarn_reg_lst_prc       = lt_xzarn_reg_lst_prc
          yzarn_reg_lst_prc       = lt_yzarn_reg_lst_prc
          xzarn_reg_onlcat        = lt_xzarn_reg_onlcat
          yzarn_reg_onlcat        = lt_yzarn_reg_onlcat
          xzarn_reg_pir           = lt_xzarn_reg_pir
          yzarn_reg_pir           = lt_yzarn_reg_pir
          xzarn_reg_prfam         = lt_xzarn_reg_prfam
          yzarn_reg_prfam         = lt_yzarn_reg_prfam
          xzarn_reg_rrp           = lt_xzarn_reg_rrp
          yzarn_reg_rrp           = lt_yzarn_reg_rrp
          xzarn_reg_std_ter       = lt_xzarn_reg_std_ter
          yzarn_reg_std_ter       = lt_yzarn_reg_std_ter
          xzarn_reg_txt           = lt_xzarn_reg_txt
          yzarn_reg_txt           = lt_yzarn_reg_txt
          xzarn_reg_uom           = lt_xzarn_reg_uom
          yzarn_reg_uom           = lt_yzarn_reg_uom
          xzarn_ver_status        = lt_xzarn_ver_status
          yzarn_ver_status        = lt_yzarn_ver_status
          xzarn_reg_nutrien       = lt_xzarn_reg_nutrien
          yzarn_reg_nutrien       = lt_yzarn_reg_nutrien.

* ONLD-1061 / ONLM-1470 Online related fields changed? If so trigger ZSAW
      zcl_onl_arena=>zarn_regional_zsaw(
        EXPORTING
          it_yreg_str      = lt_yzarn_reg_str       " before
          it_xreg_str      = lt_xzarn_reg_str       " after
          it_yreg_hdr      = lt_yzarn_reg_hdr       " before
          it_xreg_hdr      = lt_xzarn_reg_hdr       " after
          it_yreg_banner   = lt_yzarn_reg_banner    " before
          it_xreg_banner   = lt_xzarn_reg_banner    " after
          it_yreg_onlcat   = lt_yzarn_reg_onlcat    " before
          it_xreg_onlcat   = lt_xzarn_reg_onlcat    " after
      ).


    ENDLOOP.  " LOOP AT lt_reg_key[] INTO ls_reg_key


  ENDMETHOD.


  METHOD regional_data_update.
*-----------------------------------------------------------------------
* DATE             # 18.08.2020
* CHANGE No.       # SSM-1: NW Range-ZARN_GUI CHANGE
* DESCRIPTION      # Delete regional tables
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
    DATA: ls_reg_data_db          TYPE zsarn_reg_data,
          ls_reg_data_mass        TYPE zsarn_reg_data,
          ls_reg_data_mass_delete TYPE zsarn_reg_data,

          lt_dfies                TYPE dfies_tab,
          ls_dfies                TYPE dfies,

          lv_itabname             TYPE char50.


    FIELD-SYMBOLS: <table_upd> TYPE STANDARD TABLE,
                   <table_del> TYPE STANDARD TABLE.

    ls_reg_data_db          = is_reg_data_db.
    ls_reg_data_mass        = is_reg_data_mass.
    ls_reg_data_mass_delete = is_reg_data_mass_delete.

* Get Fields of the table
    REFRESH: lt_dfies[].
    CALL FUNCTION 'DDIF_FIELDINFO_GET'
      EXPORTING
        tabname        = 'ZSARN_REG_DATA'
        langu          = sy-langu
        all_types      = abap_true
      TABLES
        dfies_tab      = lt_dfies[]
      EXCEPTIONS
        not_found      = 1
        internal_error = 2
        OTHERS         = 3.
    IF sy-subrc <> 0.
      RETURN.
    ENDIF.

* Update/Delete Regional Data
    LOOP AT lt_dfies[] INTO ls_dfies.

      IF ls_dfies-fieldname = 'IDNO' OR ls_dfies-fieldname = 'VERSION'.
        CONTINUE.
      ENDIF.

      " Update
      CONCATENATE 'LS_REG_DATA_MASS-' ls_dfies-fieldname INTO lv_itabname.
      ASSIGN (lv_itabname) TO <table_upd>.
      IF sy-subrc = 0 AND
         <table_upd> IS NOT INITIAL.
        MODIFY (ls_dfies-fieldname) FROM TABLE <table_upd>.
      ENDIF.

      " Delete
      CONCATENATE 'LS_REG_DATA_MASS_DELETE-' ls_dfies-fieldname INTO lv_itabname.
      ASSIGN (lv_itabname) TO <table_del>.
      IF sy-subrc = 0 AND
        <table_del> IS NOT INITIAL.
        DELETE (ls_dfies-fieldname) FROM TABLE <table_del>.
      ENDIF.

    ENDLOOP.

  ENDMETHOD.


  METHOD regional_mass_update_process.
*-----------------------------------------------------------------------
* DATE             # 27.07.2020
* CHANGE No.       # SSM-1: NW Range-ZARN_GUI CHANGE
* DESCRIPTION      # Added ZARN_REG_CLUSTER for mass maintenance
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
    DATA: ls_reg_data_mass TYPE	zsarn_reg_data,
          ls_prod_data_all TYPE zsarn_prod_data,
          ls_reg_data_all  TYPE zsarn_reg_data,
          lt_idno_data     TYPE ty_t_idno_data,
          ls_idno_data     TYPE ty_s_idno_data,
          lt_reg_key       TYPE ty_t_reg_key,
          ls_reg_key       TYPE zsarn_reg_key,
          lt_key           TYPE ty_t_key,
          ls_key           TYPE zsarn_key,
          lt_ver_status    TYPE ty_t_ver_status,
          lt_prd_version   TYPE ty_t_prd_version,
          ls_msg           TYPE massmsg,
          lv_log_no        TYPE balognr.


* Initialize
    IF mv_initialized IS INITIAL.
      CALL METHOD me->initialize.
    ENDIF.

* Get table and fields for mass update
    CALL METHOD me->get_mass_table_fields
      EXPORTING
        it_seldata     = it_seldata[]
      IMPORTING
        et_mass_fields = mt_mass_fields[].


* Build Regional DB data
    CALL METHOD me->build_regional_mass_data
      EXPORTING
        it_mass_fields          = mt_mass_fields
        it_reg_banner           = ct_reg_banner[]
        it_reg_dc_sell          = ct_reg_dc_sell[]
        it_reg_ean              = ct_reg_ean[]
        it_reg_hdr              = ct_reg_hdr[]
        it_reg_lst_prc          = ct_reg_lst_prc[]
        it_reg_pir              = ct_reg_pir[]
        it_reg_prfam            = ct_reg_prfam[]
        it_reg_rrp              = ct_reg_rrp[]
        it_reg_std_ter          = ct_reg_std_ter[]
        it_reg_txt              = ct_reg_txt[]
        it_reg_uom              = ct_reg_uom[]
        it_reg_cluster          = ct_reg_cluster
      IMPORTING
        et_reg_key              = lt_reg_key[]
        es_reg_data_mass        = ms_reg_data_mass
        es_reg_data_mass_delete = ms_reg_data_mass_delete.

* Get Regional DB data
    CALL METHOD me->get_regional_db_data
      EXPORTING
        is_reg_data_mass_delete = ms_reg_data_mass_delete
        it_mass_fields          = mt_mass_fields[]
        it_reg_banner           = ct_reg_banner[]
        it_reg_dc_sell          = ct_reg_dc_sell[]
        it_reg_ean              = ct_reg_ean[]
        it_reg_hdr              = ct_reg_hdr[]
        it_reg_lst_prc          = ct_reg_lst_prc[]
        it_reg_pir              = ct_reg_pir[]
        it_reg_prfam            = ct_reg_prfam[]
        it_reg_rrp              = ct_reg_rrp[]
        it_reg_std_ter          = ct_reg_std_ter[]
        it_reg_txt              = ct_reg_txt[]
        it_reg_uom              = ct_reg_uom[]
        it_reg_cluster          = ct_reg_cluster
        it_reg_key              = lt_reg_key[]
      IMPORTING
        es_reg_data_db_mass     = ms_reg_data_db_mass
        et_reg_data_mass        = mt_reg_data_mass[]
        et_reg_data_db_mass     = mt_reg_data_db_mass[]
      CHANGING
        cs_reg_data_mass        = ms_reg_data_mass.

* Get National and Regional Data from DB
    CALL METHOD me->get_nat_reg_data
      EXPORTING
        is_reg_data_mass = ms_reg_data_mass
        it_reg_data_mass = mt_reg_data_mass[]
        it_reg_key       = lt_reg_key[]
      IMPORTING
        et_key           = lt_key[]
        et_prod_data_db  = mt_prod_data_db[]
        et_reg_data_db   = mt_reg_data_db[]
        es_prod_data_all = ls_prod_data_all
        es_reg_data_all  = ls_reg_data_all
        et_reg_data_save = mt_reg_data_save[]
        et_ver_status    = lt_ver_status[]
        et_prd_version   = lt_prd_version[].


* Get SAP Data
    CALL METHOD me->get_sap_data
      EXPORTING
        it_key           = lt_key[]
        is_prod_data_all = ls_prod_data_all
        is_reg_data_all  = ls_reg_data_all
        it_ver_status    = lt_ver_status[]
        it_prd_version   = lt_prd_version[]
      IMPORTING
        et_idno_data     = lt_idno_data[].


* Validate, Save and Post MASS Data
    CALL METHOD me->save_post_mass_data
      EXPORTING
        is_reg_data_mass_delete = ms_reg_data_mass_delete
        it_idno_data            = mt_idno_data[]
        it_mass_fields          = mt_mass_fields[]
        it_prod_data            = mt_prod_data_db[]
        it_reg_data             = mt_reg_data_save[]
        it_reg_data_mass        = mt_reg_data_mass[]
        it_reg_data_db          = mt_reg_data_db_mass[]
        it_reg_key              = lt_reg_key[]
        iv_testmode             = iv_testmode
        iv_no_chg_doc           = iv_no_chg_doc
      CHANGING
        ct_msg                  = et_msg[].

    et_message[] = mt_message[].


  ENDMETHOD.


  METHOD save_and_display_bal_log.


    DATA: lt_log_handle TYPE bal_t_logh,
          lt_lognumbers TYPE bal_t_lgnm,
          ls_lognumbers TYPE bal_s_lgnm.

    DATA: ls_display_profile TYPE bal_s_prof,
          ls_fcat            TYPE bal_s_fcat,
          lv_msg             TYPE string.


    APPEND is_log_handle TO lt_log_handle.

    IF iv_save = abap_true.

      REFRESH: lt_lognumbers[].
      CALL FUNCTION 'BAL_DB_SAVE'
        EXPORTING
          i_t_log_handle   = lt_log_handle
        IMPORTING
          e_new_lognumbers = lt_lognumbers[]
        EXCEPTIONS
          log_not_found    = 1
          save_not_allowed = 2
          numbering_error  = 3
          OTHERS           = 4.
      IF sy-subrc <> 0.

        MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
      ELSE.

        CLEAR ls_lognumbers.
        READ TABLE lt_lognumbers[] INTO ls_lognumbers
        WITH KEY log_handle = is_log_handle.
        IF sy-subrc = 0.
          cv_log_no = ls_lognumbers-lognumber.
        ENDIF.

      ENDIF.

    ENDIF.




    IF iv_display = abap_true.

      " Get the display profile because want to adjust the
      " field catalog so that site, article can be output as
      " columns of the log
      CALL FUNCTION 'BAL_DSP_PROFILE_SINGLE_LOG_GET'
        IMPORTING
          e_s_display_profile = ls_display_profile.

      " Add to field catalog - will display as additional
      " columns in the log
      ls_fcat-ref_table = 'ZSARN_SLG1_LOG_PARAMS_ART_POST'.
      ls_fcat-ref_field = 'IDNO'.
      APPEND ls_fcat TO ls_display_profile-mess_fcat.

      ls_fcat-ref_table = 'ZSARN_SLG1_LOG_PARAMS_ART_POST'.
      ls_fcat-ref_field = 'VERSION'.
      APPEND ls_fcat TO ls_display_profile-mess_fcat.

      ls_fcat-ref_table = 'ZSARN_SLG1_LOG_PARAMS_ART_POST'.
      ls_fcat-ref_field = 'FANID'.
      APPEND ls_fcat TO ls_display_profile-mess_fcat.

      ls_fcat-ref_table = 'ZSARN_SLG1_LOG_PARAMS_ART_POST'.
      ls_fcat-ref_field = 'NAT_STATUS'.
      APPEND ls_fcat TO ls_display_profile-mess_fcat.

      ls_fcat-ref_table = 'ZSARN_SLG1_LOG_PARAMS_ART_POST'.
      ls_fcat-ref_field = 'REG_STATUS'.
      APPEND ls_fcat TO ls_display_profile-mess_fcat.

      ls_fcat-ref_table = 'ZSARN_SLG1_LOG_PARAMS_ART_POST'.
      ls_fcat-ref_field = 'SAPARTICLE'.
      APPEND ls_fcat TO ls_display_profile-mess_fcat.

*      ls_fcat-ref_table = 'ZSARN_SLG1_LOG_PARAMS_ART_POST'.
*      ls_fcat-ref_field = 'BATCH_MODE'.
*      APPEND ls_fcat TO ls_display_profile-mess_fcat.





      ls_display_profile-disvariant-report = sy-repid.
      ls_display_profile-disvariant-handle = 'LOG' .
      ls_display_profile-cwidth_opt        = abap_true.


*      CALL FUNCTION 'BAL_DSP_PROFILE_POPUP_GET'
**     EXPORTING
**       START_COL                 = 5
**       START_ROW                 = 5
**       END_COL                   = 87
**       END_ROW                   = 25
*        IMPORTING
*          e_s_display_profile = ls_display_profile.



      CALL FUNCTION 'BAL_DSP_LOG_DISPLAY'
        EXPORTING
          i_s_display_profile  = ls_display_profile
          i_t_log_handle       = lt_log_handle
        EXCEPTIONS
          profile_inconsistent = 1
          internal_error       = 2
          no_data_available    = 3
          no_authority         = 4
          OTHERS               = 5.
      IF sy-subrc <> 0.
        MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
      ENDIF.

    ENDIF.



  ENDMETHOD.


  METHOD save_mass_data.
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             # 06.12.2018
* CHANGE No.       # 9000004644: SAPTR-147 Defect649 ZARN_MASS PIR Update
* DESCRIPTION      # Update all info records of an article
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
    DATA: ls_msg TYPE massmsg.


* Update Regional Data into DB
    CALL METHOD me->regional_data_update
      EXPORTING
        is_reg_data_db          = is_reg_data
        is_reg_data_mass        = is_reg_data_mass
        is_reg_data_mass_delete = is_reg_data_mass_delete.

    me->update_info_records( EXPORTING is_idno_data   = is_idno_data
                                       it_mass_fields = it_mass_fields
                                       it_reg_pir     = is_reg_data_mass-zarn_reg_pir
                             CHANGING  ct_msg         = ct_msg ).

    IF iv_no_chg_doc IS INITIAL.
* Update Change Documents for Regional Data
      CALL METHOD me->regional_change_doc_update
        EXPORTING
          is_reg_data_old = is_reg_data
          is_reg_data_new = is_reg_data_mass
          it_reg_key      = it_reg_key[]
          iv_bulk_mode    = abap_true.
    ENDIF.



* IDNO &, Article & saved successfully
    CLEAR ls_msg.
    ls_msg-objkey = 'ZARENA'.
    ls_msg-msgty  = 'S'.
    ls_msg-msgid  = 'ZARENA_MSG'.
    ls_msg-msgno  = '090'.
    ls_msg-msgv1  = is_idno_data-idno.
    ls_msg-msgv2  = is_idno_data-sap_article.
*        ls_msg-msgv3  =
*        ls_msg-MSGV4  =
    APPEND ls_msg TO ct_msg[].



  ENDMETHOD.


  METHOD save_post_mass_data.
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             # 06.11.2019
* CHANGE No.       # CIP-148
* DESCRIPTION      # When both UNI and LNI DC flags are blank, reset
*                  # MARA-ZZVAR_WT_FLAG flag
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------

    DATA: lt_idno_data     TYPE ty_t_idno_data,
          ls_idno_data     TYPE ty_s_idno_data,
          lt_mass_fields   TYPE ty_t_mass_fields,
          ls_mass_fields   TYPE ty_s_mass_fields,
          lt_prod_data     TYPE ztarn_prod_data,
          ls_prod_data     TYPE zsarn_prod_data,
          lt_reg_data      TYPE ztarn_reg_data,
          ls_reg_data      TYPE zsarn_reg_data,
          lt_reg_data_db   TYPE ztarn_reg_data,
          ls_reg_data_db   TYPE zsarn_reg_data,
          lt_reg_data_mass TYPE ztarn_reg_data,
          ls_reg_data_mass TYPE zsarn_reg_data,
          lt_reg_key       TYPE ty_t_reg_key,
          lt_msg           TYPE mass_msgs,
          ls_msg           TYPE massmsg,

          lv_locked        TYPE flag,
          lv_error         TYPE flag,
          lv_user          TYPE sy-uname,
          lv_username      TYPE ad_namtext.



    lt_idno_data[]     = it_idno_data[].
    lt_mass_fields[]   = it_mass_fields[].
    lt_prod_data[]     = it_prod_data[].
    lt_reg_data[]      = it_reg_data[].
    lt_reg_data_mass[] = it_reg_data_mass[].
    lt_reg_data_db[]   = it_reg_data_db[].
    lt_reg_key[]       = it_reg_key[].

    me->massage_before_post( CHANGING ct_reg_data      = lt_reg_data
                                      ct_reg_data_mass = lt_reg_data_mass ).

    LOOP AT lt_idno_data INTO ls_idno_data.

      CLEAR: lv_error, lt_msg[].
      CLEAR: lv_locked, lv_user, lv_username.

* Check IDNO Lock
      CALL METHOD me->check_idno_lock
        EXPORTING
          iv_idno   = ls_idno_data-idno
        IMPORTING
          ev_locked = lv_locked
*         ev_user   = lv_user
*         ev_username = lv_username
        CHANGING
          ct_msg    = lt_msg[].
      IF lv_locked IS NOT INITIAL.
        CALL METHOD me->build_bal_message
          EXPORTING
            is_idno_data = ls_idno_data
            it_msg       = lt_msg[].

        CONTINUE.
      ENDIF.

* National Data
      CLEAR ls_prod_data.
      READ TABLE lt_prod_data[] INTO ls_prod_data
      WITH KEY idno    = ls_idno_data-idno
               version = ls_idno_data-current_ver.

* DB data updated with MASS Data
      CLEAR ls_reg_data.
      READ TABLE lt_reg_data[] INTO ls_reg_data
      WITH KEY idno = ls_idno_data-idno.

* MASS Data
      CLEAR ls_reg_data_mass.
      READ TABLE lt_reg_data_mass[] INTO ls_reg_data_mass
      WITH KEY idno = ls_idno_data-idno.
      IF ls_reg_data_mass-reg_tables IS INITIAL.
        CONTINUE.
      ENDIF.

* DB Data for MASS Records only
      CLEAR ls_reg_data_db.
      READ TABLE lt_reg_data_db[] INTO ls_reg_data_db
      WITH KEY idno = ls_idno_data-idno.



* Validate Mass Data
      CALL METHOD me->validate_mass_data
        EXPORTING
          is_idno_data   = ls_idno_data
          is_prod_data   = ls_prod_data
          is_reg_data    = ls_reg_data
          it_mass_fields = lt_mass_fields
        CHANGING
          ct_msg         = lt_msg[]
          cv_error       = lv_error.
      IF lv_error IS NOT INITIAL.
        CALL METHOD me->build_bal_message
          EXPORTING
            is_idno_data = ls_idno_data
            it_msg       = lt_msg[].

        CONTINUE.
      ENDIF.


* No DB Updates if in Testmode
      IF iv_testmode IS INITIAL AND lv_error IS INITIAL.

* Lock IDNO to Save and Post Mass Regional Changes
        CALL METHOD me->lock_idno_regional
          EXPORTING
            iv_idno  = ls_idno_data-idno
          CHANGING
            ct_msg   = lt_msg[]
            cv_error = lv_error.
        IF lv_error IS NOT INITIAL.
          CALL METHOD me->build_bal_message
            EXPORTING
              is_idno_data = ls_idno_data
              it_msg       = lt_msg[].

          CONTINUE.
        ENDIF.


* Save MASS Data and maintain Change Documents
        CALL METHOD me->save_mass_data
          EXPORTING
            is_idno_data            = ls_idno_data
            is_reg_data             = ls_reg_data_db
            is_reg_data_mass        = ls_reg_data_mass
            is_reg_data_mass_delete = is_reg_data_mass_delete
            it_reg_key              = lt_reg_key[]
            iv_no_chg_doc           = iv_no_chg_doc
            it_mass_fields          = it_mass_fields
          CHANGING
            ct_msg                  = lt_msg[].

* Post Article only if it exist in Article Master(MARA),
* as AReNa Regional Mass Update is for Data updation, not creation
* neither any new Article will be created nor
* any new key will be created in entire article master
        IF ls_idno_data-sap_article IS NOT INITIAL.

* Build Mapping Data and Post Article for Mass
          CALL METHOD me->build_map_data_and_post_mass
            EXPORTING
              is_reg_data_mass_delete = is_reg_data_mass_delete
              is_idno_data            = ls_idno_data
              it_mass_fields          = lt_mass_fields[]
              is_prod_data            = ls_prod_data
              is_reg_data             = ls_reg_data
              is_reg_data_db          = ls_reg_data_db
              is_reg_data_mass        = ls_reg_data_mass
            CHANGING
              ct_msg                  = lt_msg[]
              cv_error                = lv_error.
        ELSE.

* Article doesn't exist in Article Master (MARA): FAN Id &
          CLEAR ls_msg.
          ls_msg-objkey = 'ZARENA'.
          ls_msg-msgty  = 'E'.
          ls_msg-msgid  = 'ZARENA_MSG'.
          ls_msg-msgno  = '093'.
          ls_msg-msgv1  = ls_idno_data-fan_id.

          APPEND ls_msg TO lt_msg[].

        ENDIF.  " IF ls_idno_data-sap_article IS NOT INITIAL

* Unlock IDNO
        CALL METHOD me->unlock_idno_regional
          EXPORTING
            iv_idno = ls_idno_data-idno.


      ENDIF.  " IF iv_testmode IS INITIAL


      CALL METHOD me->build_bal_message
        EXPORTING
          is_idno_data = ls_idno_data
          it_msg       = lt_msg[].


    ENDLOOP.  " LOOP AT lt_idno_data INTO ls_idno_data.

    ct_msg[] = mt_msg[].

  ENDMETHOD.


  METHOD unlock_idno_regional.

    DATA: lv_idno	    TYPE zarn_idno.

    lv_idno = iv_idno.


*   Unlock locked object
    CALL FUNCTION 'DEQUEUE_EZARN_REGIONAL'
      EXPORTING
        mandt = sy-mandt
        idno  = lv_idno.


  ENDMETHOD.


METHOD update_info_records.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 05.12.2018
* SYSTEM           # DE0
* SPECIFICATION    # 9000004644: SAPTR-147 Defect649 ZARN_MASS PIR Update
* SAP VERSION      # SAP_BASIS  750 0011
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Update Purchase Info Records
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             # 23.10.2019
* CHANGE No.       # SUP-317
* DESCRIPTION      # Added additional fields UEBTO and UNTTO to update in
*                  # EINE table
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------

  DATA: lt_reg_pir         TYPE ztarn_reg_pir,
        ls_selection_param TYPE zcl_pir_mass_update_model=>ts_selection_param,
        lo_pir_model       TYPE REF TO zcl_pir_mass_update_model.

  IF is_info_record_chg_relevant( iv_article     = is_idno_data-sap_article
                                  it_mass_fields = it_mass_fields ) = abap_false.
    RETURN.
  ENDIF.

  " There could be multiple records with same values.
  lt_reg_pir = it_reg_pir.
  SORT lt_reg_pir BY lifnr pir_rel_eine DESCENDING.
  DELETE ADJACENT DUPLICATES FROM lt_reg_pir COMPARING lifnr.

  LOOP AT lt_reg_pir INTO DATA(ls_reg_pir).
    ls_selection_param-vendor  = ls_reg_pir-lifnr.
    ls_selection_param-article = VALUE #( sign = 'I' option = 'EQ' ( low = is_idno_data-sap_article ) ).

    TRY.
        lo_pir_model = zcl_pir_mass_update_model=>get_instance( is_selection_param = ls_selection_param ).
        lo_pir_model->set_condition_group( iv_condition_group = ls_reg_pir-ekkol ).
        lo_pir_model->set_tolerance_limit( iv_overdelivery  = ls_reg_pir-uebto
                                           iv_underdelivery = ls_reg_pir-untto ).
        lo_pir_model->save( IMPORTING et_eine = DATA(lt_eine) ).

        " Add messages
        LOOP AT lt_eine INTO DATA(ls_eine).
          MESSAGE s134 WITH ls_eine-infnr ls_eine-ekorg ls_eine-esokz ls_eine-werks INTO zcl_message_services=>sv_msg_dummy.
          me->add_messages( CHANGING  ct_msg = ct_msg ).
        ENDLOOP.

      CATCH zcx_pir_mass_update_model INTO DATA(lo_x_error).
        me->add_messages( EXPORTING it_message   = lo_x_error->mt_bapi_return
                          CHANGING  ct_msg       = ct_msg ).
    ENDTRY.
  ENDLOOP.

ENDMETHOD.


  METHOD validate_mass_data.


    DATA: lo_validation_engine TYPE REF TO zcl_arn_validation_engine,
          lv_event             TYPE zarn_e_rl_event_id,
          lo_excep             TYPE REF TO zcx_arn_validation_err,
          lt_mass_fields       TYPE ty_t_mass_fields,
          ls_mass_fields       TYPE ty_s_mass_fields,
          lt_field_filter      TYPE ztarn_table_field,
          ls_field_filter      TYPE zsarn_table_field,
          lt_output            TYPE zarn_t_rl_output,
          ls_output            TYPE zarn_s_rl_output,
          ls_msg               TYPE massmsg.



    LOOP AT it_mass_fields[] INTO ls_mass_fields.
      CLEAR ls_field_filter.
      ls_field_filter-table_name = ls_mass_fields-table.
      ls_field_filter-field_name = ls_mass_fields-field.
      APPEND ls_field_filter TO lt_field_filter[].
    ENDLOOP.



    TRY.
*        lv_event = zcl_arn_validation_engine=>gc_event_mass_upload.   " 'MASS_UPLD'
        lv_event = zcl_arn_validation_engine=>gc_event_arena_ui.      " 'REG_UI'


        CREATE OBJECT lo_validation_engine
          EXPORTING
            iv_event          = lv_event
            iv_bapiret_output = abap_true
            it_field_filter   = lt_field_filter[].

      CATCH zcx_arn_validation_err INTO lo_excep.
        MESSAGE i000(zarena_msg) WITH lo_excep->msgv1 lo_excep->msgv2
                                      lo_excep->msgv3 lo_excep->msgv4.
        cv_error = abap_true.
        RETURN.
    ENDTRY.




* Validation Rule Engine
    CLEAR: lt_output[].
    TRY.
        CALL METHOD lo_validation_engine->validate_arn_single
          EXPORTING
            is_zsarn_prod_data = is_prod_data
            is_zsarn_reg_data  = is_reg_data
          IMPORTING
            et_output          = lt_output.
      CATCH zcx_arn_validation_err ##no_handler.
    ENDTRY.


    LOOP AT lt_output[] INTO ls_output.
      CLEAR ls_msg.
      ls_msg-objkey = 'ZARENA'.
      ls_msg-msgty  = ls_output-msgty.
      ls_msg-msgid  = ls_output-msgid.
      ls_msg-msgno  = ls_output-msgno.
      ls_msg-msgv1  = ls_output-msgv1.
      ls_msg-msgv2  = ls_output-msgv2.
      ls_msg-msgv3  = ls_output-msgv3.
      ls_msg-msgv4  = ls_output-msgv4.
      APPEND ls_msg TO ct_msg[].

      IF ls_output-msgty = 'E'.
        cv_error = abap_true.
      ENDIF.
    ENDLOOP.


  ENDMETHOD.
ENDCLASS.
