* regenerated at 25.05.2016 12:09:45 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_LEV_CONT_TTOP.               " Global Data
  INCLUDE LZARN_LEV_CONT_TUXX.               " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_LEV_CONT_TF...               " Subroutines
* INCLUDE LZARN_LEV_CONT_TO...               " PBO-Modules
* INCLUDE LZARN_LEV_CONT_TI...               " PAI-Modules
* INCLUDE LZARN_LEV_CONT_TE...               " Events
* INCLUDE LZARN_LEV_CONT_TP...               " Local class implement.
* INCLUDE LZARN_LEV_CONT_TT99.               " ABAP Unit tests
  INCLUDE LZARN_LEV_CONT_TF00                     . " subprograms
  INCLUDE LZARN_LEV_CONT_TI00                     . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
