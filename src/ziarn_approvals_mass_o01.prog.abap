*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*----------------------------------------------------------------------*
***INCLUDE ZIARN_APPROVALS_MASSO01.
*----------------------------------------------------------------------*

*&---------------------------------------------------------------------*
*&      Module  display_0100  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE display_0100 OUTPUT.

*  lcl_controller=>get( )->mo_view->modify_screen_0100( ).

  lcl_controller=>get( )->mo_view->display_0100( ).

  lcl_controller=>get( )->mo_view->get_data( IMPORTING et_output = gt_output[] ).

*  gt_output[] = lcl_controller=>get( )->mo_view->mt_output.



ENDMODULE.

*&---------------------------------------------------------------------*
*&      Module  status_0100  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE status_0100 OUTPUT.

  SET PF-STATUS '0100'.
  SET TITLEBAR '0100'.

ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  STATUS_0102  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE status_0102 OUTPUT.

  PERFORM appr_set_status_0102 CHANGING <gs_idno_appr>.

  PERFORM appr_populate_drop_down_0102 CHANGING <gs_idno_appr>.

  PERFORM appr_create_text_edit_0102 CHANGING <gs_idno_appr>.


ENDMODULE.
