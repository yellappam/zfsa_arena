*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Functional Specification 3113
* DATE WRITTEN     # 17112015
* SAP VERSION      # 731
* TYPE             # Function Module
* AUTHOR           # C90001363, Jitin Kharbanda
*-----------------------------------------------------------------------
* TITLE            # Update data into DB
* PURPOSE          # Update Change Category tables into DB
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------
FUNCTION zarn_change_category_update.
*"----------------------------------------------------------------------
*"*"Update Function Module:
*"
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(IT_CC_HDR) TYPE  ZTARN_CC_HDR
*"     VALUE(IT_CC_DET) TYPE  ZTARN_CC_DET
*"----------------------------------------------------------------------



* AReNa: Change Category Header
  IF it_cc_hdr[] IS NOT INITIAL.
    MODIFY zarn_cc_hdr FROM TABLE it_cc_hdr[].
  ENDIF.


* AReNa: Change Category Detail
  IF it_cc_det[] IS NOT INITIAL.
    MODIFY zarn_cc_det FROM TABLE it_cc_det[].
  ENDIF.


ENDFUNCTION.
