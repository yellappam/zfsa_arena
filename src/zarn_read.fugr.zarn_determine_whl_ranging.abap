FUNCTION zarn_determine_whl_ranging.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(IT_ZARN_REG_BANNER) TYPE  ZTARN_REG_BANNER
*"  CHANGING
*"     REFERENCE(CV_WHL_RANGING) TYPE  FLAG
*"----------------------------------------------------------------------

  "determines Wholesale Ranging Flag

  "this function is used both by GUI and validation engine

  LOOP AT IT_ZARN_REG_BANNER INTO DATA(ls_zarn_reg_banner)
    WHERE banner   EQ zcl_constants=>gc_banner_3000
      AND zzcatman IS NOT INITIAL.

    cv_whl_ranging = abap_true.
    EXIT.
  ENDLOOP.

ENDFUNCTION.
