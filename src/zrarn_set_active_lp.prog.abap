*&---------------------------------------------------------------------*
*& Report  ZRARN_SET_ACTIVE_LP
*&
*&---------------------------------------------------------------------*
*&---------------------------------------------------------------------*
* PROJECT          # FSA
* SPECIFICATION    #
* DATE WRITTEN     # 01.08.2016
* SYSTEM           # DE0
* SAP VERSION      # ECC6.0
* TYPE             # Report
* AUTHOR           # C90001363
*-----------------------------------------------------------------------
* TITLE            # Set Active List Price
* PURPOSE          # To Set Active List Price in National Data so as to
*                  # get correct price which is effective on current
*                  # date and time.
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      #
*-----------------------------------------------------------------------
* CALLS TO         #
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

REPORT zrarn_set_active_lp MESSAGE-ID zarena_msg.


*
** Top Include for Declarations
*INCLUDE ziarn_set_active_lp_top.
*
** Selection Screen
*INCLUDE ziarn_set_active_lp_sel.
*
** Main Processing Logic
*INCLUDE ziarn_set_active_lp_main.
*
** Subroutines Main
*INCLUDE ziarn_set_active_lp_sub.
