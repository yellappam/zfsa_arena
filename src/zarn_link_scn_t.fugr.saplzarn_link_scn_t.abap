* regenerated at 18.11.2016 14:20:42
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_LINK_SCN_TTOP.               " Global Data
  INCLUDE LZARN_LINK_SCN_TUXX.               " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_LINK_SCN_TF...               " Subroutines
* INCLUDE LZARN_LINK_SCN_TO...               " PBO-Modules
* INCLUDE LZARN_LINK_SCN_TI...               " PAI-Modules
* INCLUDE LZARN_LINK_SCN_TE...               " Events
* INCLUDE LZARN_LINK_SCN_TP...               " Local class implement.
* INCLUDE LZARN_LINK_SCN_TT99.               " ABAP Unit tests
  INCLUDE LZARN_LINK_SCN_TF00                     . " subprograms
  INCLUDE LZARN_LINK_SCN_TI00                     . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
