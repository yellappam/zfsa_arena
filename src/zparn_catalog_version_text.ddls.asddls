@AbapCatalog.sqlViewName: 'zparncatlgvert'
@AbapCatalog.compiler.compareFilter: true
@AccessControl.authorizationCheck: #NOT_REQUIRED
@EndUserText.label: 'Catalog Version Text'
define view zparn_catalog_version_text as select from ziarn_catalog_version_text {    
  key Catalog_Version,
  Catalog_Version_Text
}
  where Language = $session.system_language
