*&---------------------------------------------------------------------*
*&  Include           ZIARN_CLEANUP_FRM
*&---------------------------------------------------------------------*
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 13.04.2018
* CHANGE No.       # Charm 9000003482; Jira CI18-554
* DESCRIPTION      # CI18-554 Recipe Management - Ingredient Changes
*                  # New regional table for Allergen Type overrides and
*                  # fields for Ingredients statement overrides in Arena.
*                  # New Ingredients Type field in the Scales block in
*                  # Arena and NONSAP tab in material master.
* WHO              # C90005557, Tessa Newman
*-----------------------------------------------------------------------
* DATE             # 19/08/2018
* CHANGE No.       # Charm #9000004219 - Tech refresh
* DESCRIPTION      # Assignment of variables that are not flat structures
*                  # and that are not identical results in runtime error.
*                  # Prior to upgrade this was not an issue. Table with
*                  # string fields impacted. Changed coding in routine
*                  # MAINTAIN_CHANGE_DOCUMENT.
* WHO              # C90001929, Aruna Mackenjee
*-----------------------------------------------------------------------
FORM process_tables.

  DATA: lt_table_mastr TYPE STANDARD TABLE OF zarn_table_mastr,
        lt_idno        TYPE ztarn_idno_range,
        ls_idno        LIKE LINE OF lt_idno,
        lv_dir         TYPE string,
        lv_rc          TYPE i.

  FIELD-SYMBOLS: <ls_table_mastr> LIKE LINE OF lt_table_mastr.

  REFRESH: lt_table_mastr,
           lt_idno.

  SELECT *
    INTO TABLE lt_table_mastr
    FROM zarn_table_mastr
    WHERE arena_table_type EQ 'N'
    OR    arena_table_type EQ 'R'
    OR    arena_table_type EQ 'CC'
    OR    arena_table      EQ 'ZARN_PRD_VERSION'
    OR    arena_table      EQ 'ZARN_VER_STATUS'.

  CLEAR ls_idno.
  ls_idno-sign   = 'I'.
  ls_idno-option = 'EQ'.
  SELECT idno
    INTO ls_idno-low
    FROM zarn_products
    WHERE idno   IN s_idno
    AND   fan_id IN s_fan_id.
    INSERT ls_idno INTO TABLE lt_idno.
  ENDSELECT.
  SORT lt_idno.
  DELETE ADJACENT DUPLICATES FROM lt_idno COMPARING ALL FIELDS.
  IF lt_idno[] IS INITIAL.
    WRITE:/ 'No products found matching criteria'.
    EXIT.
  ENDIF.

* bulid root directory
  CONCATENATE
    p_dir
    '\'
    sy-datum
    '_'
    sy-uzeit(4)
    '\'
    INTO lv_dir.
  CALL METHOD cl_gui_frontend_services=>directory_create
    EXPORTING
      directory                = lv_dir
    CHANGING
      rc                       = lv_rc
    EXCEPTIONS
      directory_create_failed  = 1
      cntl_error               = 2
      error_no_gui             = 3
      directory_access_denied  = 4
      directory_already_exists = 5
      path_not_found           = 6
      unknown_error            = 7
      not_supported_by_gui     = 8
      wrong_parameter          = 9
      OTHERS                   = 10.
  IF sy-subrc NE 0.
    WRITE:/ 'Could not create directory', lv_dir.
    EXIT.
  ELSE.
    WRITE:/ 'Created directory', lv_dir.
  ENDIF.
  LOOP AT lt_table_mastr ASSIGNING <ls_table_mastr>.
    PERFORM process_table USING <ls_table_mastr> lt_idno lv_dir.
  ENDLOOP.

ENDFORM.

FORM process_table USING ps_table TYPE zarn_table_mastr
                         pt_idno  TYPE ztarn_idno_range
                         pv_dir   TYPE string.

  DATA: lv_name      TYPE ddobjname,
        lv_filename  TYPE string,
        lv_append    TYPE c,
        lv_filenamel TYPE i,
        lt_dd03p     TYPE STANDARD TABLE OF dd03p,
        lt_table     TYPE REF TO data,
        lt_header    TYPE STANDARD TABLE OF char4000,
        lv_header    LIKE LINE OF lt_header.

  FIELD-SYMBOLS: <lt_table> TYPE table,
                 <ls_dd03p> LIKE LINE OF lt_dd03p.

  WRITE:/ 'Processing table', ps_table-arena_table.

  lv_name = ps_table-arena_table.
  CALL FUNCTION 'DDIF_TABL_GET'
    EXPORTING
      name          = lv_name
*     STATE         = 'A'
*     LANGU         = ' '
    TABLES
      dd03p_tab     = lt_dd03p
    EXCEPTIONS
      illegal_input = 1
      OTHERS        = 2.
  DELETE lt_dd03p WHERE fieldname EQ '.INCLUDE'.
  READ TABLE lt_dd03p TRANSPORTING NO FIELDS WITH KEY fieldname = 'IDNO'.
  IF sy-subrc NE 0.
    WRITE: ' - Table does not contain IDNO field'.
    EXIT.
  ENDIF.

  CREATE DATA lt_table TYPE TABLE OF (lv_name).
  ASSIGN lt_table->* TO <lt_table>.

  SELECT *   "#EC CI_SEL_NESTED
    INTO TABLE <lt_table>
    FROM (lv_name)
    WHERE idno IN pt_idno.
  IF sy-subrc NE 0.
    WRITE: ' - No rows found'.
  ELSE.
    CONCATENATE pv_dir lv_name '.txt' INTO lv_filename.
    lv_filenamel = strlen( lv_filename ).

    lv_append = abap_false.

    IF p_hdr EQ abap_true.
*   Build header
      CLEAR lv_header.
      LOOP AT lt_dd03p ASSIGNING <ls_dd03p>.
        IF lv_header IS INITIAL.
          lv_header = <ls_dd03p>-fieldname.
        ELSE.
          CONCATENATE
            lv_header
            cl_abap_char_utilities=>horizontal_tab
            <ls_dd03p>-fieldname
            INTO lv_header.
        ENDIF.
      ENDLOOP.
      APPEND lv_header TO lt_header.
      CALL METHOD cl_gui_frontend_services=>gui_download
        EXPORTING
          filename                = lv_filename
          filetype                = 'ASC'
          append                  = lv_append
          write_field_separator   = ' '
*         header                  = '00'
*         trunc_trailing_blanks   = SPACE
          write_lf                = 'X'
*         col_select              = SPACE
*         col_select_mask         = SPACE
*         dat_mode                = SPACE
*         confirm_overwrite       = SPACE
*         no_auth_check           = SPACE
*         codepage                = SPACE
*         ignore_cerr             = ABAP_TRUE
*         replacement             = '#'
*         write_bom               = SPACE
*         trunc_trailing_blanks_eol = 'X'
*         wk1_n_format            = SPACE
*         wk1_n_size              = SPACE
*         wk1_t_format            = SPACE
*         wk1_t_size              = SPACE
          show_transfer_status    = ' '
*         fieldnames              =
*         write_lf_after_last_line  = 'X'
*         virus_scan_profile      = '/SCET/GUI_DOWNLOAD'
*      IMPORTING
*         filelength              =
        CHANGING
          data_tab                = lt_header
        EXCEPTIONS
          file_write_error        = 1
          no_batch                = 2
          gui_refuse_filetransfer = 3
          invalid_type            = 4
          no_authority            = 5
          unknown_error           = 6
          header_not_allowed      = 7
          separator_not_allowed   = 8
          filesize_not_allowed    = 9
          header_too_long         = 10
          dp_error_create         = 11
          dp_error_send           = 12
          dp_error_write          = 13
          unknown_dp_error        = 14
          access_denied           = 15
          dp_out_of_memory        = 16
          disk_full               = 17
          dp_timeout              = 18
          file_not_found          = 19
          dataprovider_exception  = 20
          control_flush_error     = 21
          not_supported_by_gui    = 22
          error_no_gui            = 23
          OTHERS                  = 24.
      lv_append = abap_true.
    ENDIF.

    CALL METHOD cl_gui_frontend_services=>gui_download
      EXPORTING
        filename                = lv_filename
        filetype                = 'ASC'
        write_field_separator   = 'X'
        append                  = 'X'
*       header                  = '00'
*       trunc_trailing_blanks   = SPACE
        write_lf                = 'X'
*       col_select              = SPACE
*       col_select_mask         = SPACE
*       dat_mode                = SPACE
*       confirm_overwrite       = SPACE
*       no_auth_check           = SPACE
*       codepage                = SPACE
*       ignore_cerr             = ABAP_TRUE
*       replacement             = '#'
*       write_bom               = SPACE
*       trunc_trailing_blanks_eol = 'X'
*       wk1_n_format            = SPACE
*       wk1_n_size              = SPACE
*       wk1_t_format            = SPACE
*       wk1_t_size              = SPACE
        show_transfer_status    = ' '
*       fieldnames              =
*       write_lf_after_last_line  = 'X'
*       virus_scan_profile      = '/SCET/GUI_DOWNLOAD'
*      IMPORTING
*       filelength              =
      CHANGING
        data_tab                = <lt_table>
      EXCEPTIONS
        file_write_error        = 1
        no_batch                = 2
        gui_refuse_filetransfer = 3
        invalid_type            = 4
        no_authority            = 5
        unknown_error           = 6
        header_not_allowed      = 7
        separator_not_allowed   = 8
        filesize_not_allowed    = 9
        header_too_long         = 10
        dp_error_create         = 11
        dp_error_send           = 12
        dp_error_write          = 13
        unknown_dp_error        = 14
        access_denied           = 15
        dp_out_of_memory        = 16
        disk_full               = 17
        dp_timeout              = 18
        file_not_found          = 19
        dataprovider_exception  = 20
        control_flush_error     = 21
        not_supported_by_gui    = 22
        error_no_gui            = 23
        OTHERS                  = 24.
    IF sy-subrc EQ 0.
      WRITE: ' - File written'.
      IF p_del EQ abap_true.
        DELETE FROM (lv_name) WHERE idno IN pt_idno.   "#EC CI_IMUD_NESTED

* Begin of Change IR5254174  Jitin 06.10.2016
* Maintain Change Document for Deleted data
        PERFORM maintain_change_document USING ps_table
                                               lv_name
                                               pt_idno
                                               <lt_table>.
* End of Change IR5254174  Jitin 06.10.2016

        IF sy-subrc EQ 0.
          WRITE: ' - Rows deleted', sy-dbcnt.
        ELSE.
          WRITE: ' - Error deleting from table', sy-subrc.
        ENDIF.
      ENDIF.
    ELSE.
      WRITE: ' - Error writing file', lv_filename(lv_filenamel).
    ENDIF.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  MAINTAIN_CHANGE_DOCUMENT
*&---------------------------------------------------------------------*
* Maintain Change Document for Deleted data
*----------------------------------------------------------------------*
FORM maintain_change_document USING fu_s_table TYPE zarn_table_mastr
                                    fu_v_name  TYPE ddobjname
                                    fu_t_idno  TYPE ztarn_idno_range
                                    fu_t_table TYPE table.

  DATA: lv_name              TYPE ddobjname,
        lt_idno              TYPE ztarn_idno_range,
        ls_idno              TYPE zsarn_idno,
        lt_table             TYPE REF TO data,
        lt_chgid             TYPE REF TO data,

        lv_chg_flag          TYPE cdchngindh      VALUE 'U',
        lv_update_flag       TYPE cdflag          VALUE 'U',
        lv_objectid          TYPE cdobjectv,
        lv_change_number     TYPE cdchangenr,
        lt_cdtxt             TYPE                    isu_cdtxt,
        lv_where_str         TYPE string,

        lt_xzarn_control     TYPE STANDARD TABLE OF yzarn_control,
        lt_yzarn_control     TYPE STANDARD TABLE OF yzarn_control,
        lt_xzarn_prd_version TYPE STANDARD TABLE OF yzarn_prd_version,
        lt_yzarn_prd_version TYPE STANDARD TABLE OF yzarn_prd_version,
        lt_xzarn_ver_status  TYPE STANDARD TABLE OF yzarn_ver_status,
        lt_yzarn_ver_status  TYPE STANDARD TABLE OF yzarn_ver_status,

        lt_xzarn_reg_dc_sell TYPE STANDARD TABLE OF yzarn_reg_dc_sell,
        lt_yzarn_reg_dc_sell TYPE STANDARD TABLE OF yzarn_reg_dc_sell,
        lt_xzarn_reg_lst_prc TYPE STANDARD TABLE OF yzarn_reg_lst_prc,
        lt_yzarn_reg_lst_prc TYPE STANDARD TABLE OF yzarn_reg_lst_prc,
        lt_xzarn_reg_rrp     TYPE STANDARD TABLE OF yzarn_reg_rrp,
        lt_yzarn_reg_rrp     TYPE STANDARD TABLE OF yzarn_reg_rrp,
        lt_xzarn_reg_std_ter TYPE STANDARD TABLE OF yzarn_reg_std_ter,
        lt_yzarn_reg_std_ter TYPE STANDARD TABLE OF yzarn_reg_std_ter,

        lt_xzarn_reg_banner  TYPE STANDARD TABLE OF yzarn_reg_banner,
        lt_yzarn_reg_banner  TYPE STANDARD TABLE OF yzarn_reg_banner,
        lt_xzarn_reg_ean     TYPE STANDARD TABLE OF yzarn_reg_ean,
        lt_yzarn_reg_ean     TYPE STANDARD TABLE OF yzarn_reg_ean,
        lt_xzarn_reg_hdr     TYPE STANDARD TABLE OF yzarn_reg_hdr,
        lt_yzarn_reg_hdr     TYPE STANDARD TABLE OF yzarn_reg_hdr,
        lt_xzarn_reg_hsno    TYPE STANDARD TABLE OF yzarn_reg_hsno,
        lt_yzarn_reg_hsno    TYPE STANDARD TABLE OF yzarn_reg_hsno,
        lt_xzarn_reg_pir     TYPE STANDARD TABLE OF yzarn_reg_pir,
        lt_yzarn_reg_pir     TYPE STANDARD TABLE OF yzarn_reg_pir,
        lt_xzarn_reg_prfam   TYPE STANDARD TABLE OF yzarn_reg_prfam,
        lt_yzarn_reg_prfam   TYPE STANDARD TABLE OF yzarn_reg_prfam,
        lt_xzarn_reg_txt     TYPE STANDARD TABLE OF yzarn_reg_txt,
        lt_yzarn_reg_txt     TYPE STANDARD TABLE OF yzarn_reg_txt,
        lt_xzarn_reg_uom     TYPE STANDARD TABLE OF yzarn_reg_uom,
        lt_yzarn_reg_uom     TYPE STANDARD TABLE OF yzarn_reg_uom,
        lt_xzarn_reg_artlink TYPE STANDARD TABLE OF yzarn_reg_artlink,
        lt_yzarn_reg_artlink TYPE STANDARD TABLE OF yzarn_reg_artlink,
        lt_xzarn_reg_allerg  TYPE STANDARD TABLE OF yzarn_reg_allerg,
        lt_yzarn_reg_allerg  TYPE STANDARD TABLE OF yzarn_reg_allerg,
        lt_xzarn_reg_onlcat  TYPE STANDARD TABLE OF yzarn_reg_onlcat,
        lt_yzarn_reg_onlcat  TYPE STANDARD TABLE OF yzarn_reg_onlcat,
        lt_xzarn_reg_str     TYPE yzarn_reg_strs,
        lt_yzarn_reg_str     TYPE yzarn_reg_strs,

        lt_xcc_hdr           TYPE STANDARD TABLE OF yzarn_cc_hdr,
        lt_ycc_hdr           TYPE STANDARD TABLE OF yzarn_cc_hdr,
        lt_xcc_det           TYPE STANDARD TABLE OF yzarn_cc_det,
        lt_ycc_det           TYPE STANDARD TABLE OF yzarn_cc_det,
        lt_xcc_team          TYPE STANDARD TABLE OF yzarn_cc_team,
        lt_ycc_team          TYPE STANDARD TABLE OF yzarn_cc_team.


  FIELD-SYMBOLS: <lt_table> TYPE table,
                 <ls_table> TYPE any,
                 <lt_chgid> TYPE table,
                 <ls_chgid> TYPE any,
                 <lv_chgid> TYPE zarn_chg_id.


  IF fu_s_table-arena_table_type = 'N'.
    EXIT.
  ENDIF.


  lv_name    = fu_v_name.
  lt_idno[]  = fu_t_idno[].


  CREATE DATA lt_table TYPE TABLE OF (lv_name).
  ASSIGN lt_table->* TO <lt_table>.

  CREATE DATA lt_chgid TYPE TABLE OF (lv_name).
  ASSIGN lt_chgid->* TO <lt_chgid>.


  LOOP AT lt_idno[] INTO ls_idno.

* When Regional or Control tables
    IF fu_s_table-arena_table_type = 'R' OR fu_s_table-arena_table_type = 'C'.

      CLEAR lv_where_str.
      lv_where_str = 'idno = ls_idno-low'.

      IF <lt_table> IS ASSIGNED.
        CLEAR: <lt_table>.
      ENDIF.

      IF <ls_table> IS ASSIGNED. UNASSIGN <ls_table>. ENDIF.
      LOOP AT fu_t_table[] ASSIGNING <ls_table> WHERE (lv_where_str).
        APPEND <ls_table> TO <lt_table>.
      ENDLOOP.



      CLEAR : lt_yzarn_reg_hdr[],
              lt_yzarn_reg_banner[],
              lt_yzarn_reg_dc_sell[],
              lt_yzarn_reg_ean[],
              lt_yzarn_reg_hdr[],
              lt_yzarn_reg_hsno[],
              lt_yzarn_reg_pir[],
              lt_yzarn_reg_prfam[],
              lt_yzarn_reg_rrp[],
              lt_yzarn_reg_std_ter[],
              lt_yzarn_reg_txt[],
              lt_yzarn_reg_uom[],
              lt_yzarn_prd_version[],
              lt_yzarn_ver_status[],
              lt_yzarn_reg_artlink[],
              lt_yzarn_reg_allerg[],
              lt_yzarn_reg_onlcat[],
              lt_yzarn_reg_str[].

      CASE lv_name.
        WHEN 'ZARN_REG_HDR'.          lt_yzarn_reg_hdr[]      = CORRESPONDING #( <lt_table> ).
        WHEN 'ZARN_REG_HSNO'.         lt_yzarn_reg_hsno[]     = CORRESPONDING #( <lt_table> ).
        WHEN 'ZARN_REG_BANNER'.       lt_yzarn_reg_banner[]   = CORRESPONDING #( <lt_table> ).
        WHEN 'ZARN_REG_DC_SELL'.      lt_yzarn_reg_dc_sell[]  = CORRESPONDING #( <lt_table> ).
        WHEN 'ZARN_REG_ONLCAT'.       lt_yzarn_reg_onlcat[]   = CORRESPONDING #( <lt_table> ).
        WHEN 'ZARN_REG_EAN'.          lt_yzarn_reg_ean[]      = CORRESPONDING #( <lt_table> ).
        WHEN 'ZARN_REG_HDR'.          lt_yzarn_reg_hdr[]      = CORRESPONDING #( <lt_table> ).
        WHEN 'ZARN_REG_PIR'.          lt_yzarn_reg_pir[]      = CORRESPONDING #( <lt_table> ).
        WHEN 'ZARN_REG_PRFAM'.        lt_yzarn_reg_prfam[]    = CORRESPONDING #( <lt_table> ).
        WHEN 'ZARN_REG_RRP'.          lt_yzarn_reg_rrp[]      = CORRESPONDING #( <lt_table> ).
        WHEN 'ZARN_REG_STD_TER'.      lt_yzarn_reg_std_ter[]  = CORRESPONDING #( <lt_table> ).
        WHEN 'ZARN_REG_STR'.          lt_yzarn_reg_str[]      = CORRESPONDING #( <lt_table> ).
        WHEN 'ZARN_REG_TXT'.          lt_yzarn_reg_txt[]      = CORRESPONDING #( <lt_table> ).
        WHEN 'ZARN_REG_UOM'.          lt_yzarn_reg_uom[]      = CORRESPONDING #( <lt_table> ).
        WHEN 'ZARN_REG_ARTLINK'.      lt_yzarn_reg_artlink[]  = CORRESPONDING #( <lt_table> ).
        when 'ZARN_REG_ALLERG'.       lt_yzarn_reg_allerg[]   = CORRESPONDING #( <lt_table> ).
        WHEN 'ZARN_PRD_VERSION'.      lt_yzarn_prd_version[]  = CORRESPONDING #( <lt_table> ).
        WHEN 'ZARN_VER_STATUS'.       lt_yzarn_ver_status[]   = CORRESPONDING #( <lt_table> ).
      ENDCASE.


      CLEAR lv_objectid.
      lv_objectid = ls_idno-low.

      CLEAR lv_change_number.
*   Change document
      CALL FUNCTION 'ZARN_CTRL_REGNL_WRITE_DOC_CUST'
        EXPORTING
          objectid                = lv_objectid
          tcode                   = sy-tcode
          utime                   = sy-uzeit
          udate                   = sy-datum
          username                = sy-uname
*         PLANNED_CHANGE_NUMBER   = ' '
          object_change_indicator = lv_chg_flag
*         PLANNED_OR_REAL_CHANGES = ' '
*         NO_CHANGE_POINTERS      = ' '
*         UPD_ICDTXT_ZARN_CTRL_REGNL = ' '
*         UPD_ZARN_CONTROL        = ' '
          upd_zarn_reg_allerg     = lv_update_flag
          upd_zarn_reg_artlink    = lv_update_flag
          upd_zarn_prd_version    = lv_update_flag
          upd_zarn_reg_banner     = lv_update_flag
          upd_zarn_reg_dc_sell    = lv_update_flag
          upd_zarn_reg_ean        = lv_update_flag
          upd_zarn_reg_hdr        = lv_update_flag
          upd_zarn_reg_hsno       = lv_update_flag
          upd_zarn_reg_lst_prc    = lv_update_flag
          upd_zarn_reg_onlcat     = lv_update_flag
          upd_zarn_reg_pir        = lv_update_flag
          upd_zarn_reg_prfam      = lv_update_flag
          upd_zarn_reg_rrp        = lv_update_flag
          upd_zarn_reg_std_ter    = lv_update_flag
          upd_zarn_reg_str        = lv_update_flag
          xzarn_reg_str           = lt_xzarn_reg_str
          yzarn_reg_str           = lt_yzarn_reg_str
          upd_zarn_reg_txt        = lv_update_flag
          upd_zarn_reg_uom        = lv_update_flag
          upd_zarn_ver_status     = lv_update_flag
        IMPORTING
          ev_changenumber         = lv_change_number
        TABLES
          icdtxt_zarn_ctrl_regnl  = lt_cdtxt
          xzarn_control           = lt_xzarn_control
          yzarn_control           = lt_yzarn_control
          xzarn_prd_version       = lt_xzarn_prd_version
          yzarn_prd_version       = lt_yzarn_prd_version
          xzarn_reg_allerg        = lt_xzarn_reg_allerg
          yzarn_reg_allerg        = lt_yzarn_reg_allerg
          xzarn_reg_artlink       = lt_xzarn_reg_artlink
          yzarn_reg_artlink       = lt_yzarn_reg_artlink
          xzarn_reg_banner        = lt_xzarn_reg_banner
          yzarn_reg_banner        = lt_yzarn_reg_banner
          xzarn_reg_dc_sell       = lt_xzarn_reg_dc_sell
          yzarn_reg_dc_sell       = lt_yzarn_reg_dc_sell
          xzarn_reg_ean           = lt_xzarn_reg_ean
          yzarn_reg_ean           = lt_yzarn_reg_ean
          xzarn_reg_hdr           = lt_xzarn_reg_hdr
          yzarn_reg_hdr           = lt_yzarn_reg_hdr
          xzarn_reg_hsno          = lt_xzarn_reg_hsno
          yzarn_reg_hsno          = lt_yzarn_reg_hsno
          xzarn_reg_lst_prc       = lt_xzarn_reg_lst_prc
          yzarn_reg_lst_prc       = lt_yzarn_reg_lst_prc
          xzarn_reg_onlcat        = lt_xzarn_reg_onlcat
          yzarn_reg_onlcat        = lt_yzarn_reg_onlcat
          xzarn_reg_pir           = lt_xzarn_reg_pir
          yzarn_reg_pir           = lt_yzarn_reg_pir
          xzarn_reg_prfam         = lt_xzarn_reg_prfam
          yzarn_reg_prfam         = lt_yzarn_reg_prfam
          xzarn_reg_rrp           = lt_xzarn_reg_rrp
          yzarn_reg_rrp           = lt_yzarn_reg_rrp
          xzarn_reg_std_ter       = lt_xzarn_reg_std_ter
          yzarn_reg_std_ter       = lt_yzarn_reg_std_ter
          xzarn_reg_txt           = lt_xzarn_reg_txt
          yzarn_reg_txt           = lt_yzarn_reg_txt
          xzarn_reg_uom           = lt_xzarn_reg_uom
          yzarn_reg_uom           = lt_yzarn_reg_uom
          xzarn_ver_status        = lt_xzarn_ver_status
          yzarn_ver_status        = lt_yzarn_ver_status.

    ENDIF.   " IF fu_s_table-arena_table_type = 'R' / 'C'



* When change category tables
    IF fu_s_table-arena_table_type = 'CC'.

      IF <lt_chgid> IS NOT ASSIGNED.
        CONTINUE.
      ENDIF.


      CLEAR lv_where_str.
      lv_where_str = 'idno NE ls_idno-low'.

      <lt_chgid> = fu_t_table[].
      DELETE <lt_chgid> WHERE (lv_where_str).


      CLEAR lv_where_str.
      lv_where_str = 'CHGID'.
      SORT <lt_chgid> BY (lv_where_str).
      DELETE ADJACENT DUPLICATES FROM <lt_chgid> COMPARING (lv_where_str).


      LOOP AT <lt_chgid> ASSIGNING <ls_chgid>.

        IF <lv_chgid> IS ASSIGNED. UNASSIGN <lv_chgid>. ENDIF.
        ASSIGN COMPONENT 'CHGID' OF STRUCTURE <ls_chgid> TO <lv_chgid>.

        IF <lv_chgid> IS NOT ASSIGNED.
          CONTINUE.
        ENDIF.

        CLEAR lv_where_str.
        lv_where_str = 'chgid = <lv_chgid> AND idno = ls_idno-low'.

        IF <lt_table> IS ASSIGNED.
          CLEAR: <lt_table>.
        ENDIF.

        IF <ls_table> IS ASSIGNED. UNASSIGN <ls_table>. ENDIF.
        LOOP AT fu_t_table[] ASSIGNING <ls_table> WHERE (lv_where_str).
          APPEND <ls_table> TO <lt_table>.
        ENDLOOP.

        CLEAR: lt_ycc_hdr[],
               lt_ycc_det[],
               lt_ycc_team[].


        CASE lv_name.
          WHEN 'ZARN_CC_HDR'.       lt_ycc_hdr[]   = <lt_table>.
          WHEN 'ZARN_CC_DET'.       lt_ycc_det[]   = <lt_table>.
          WHEN 'ZARN_CC_TEAM'.      lt_ycc_team[]  = <lt_table>.
        ENDCASE.


        CLEAR lv_objectid.
        lv_objectid = <lv_chgid>.

        CLEAR lv_change_number.
*   Change document
        CALL FUNCTION 'ZARN_CHG_CAT_WRITE_DOCUMENT'
          EXPORTING
            objectid                = lv_objectid
            tcode                   = sy-tcode
            utime                   = sy-uzeit
            udate                   = sy-datum
            username                = sy-uname
*           PLANNED_CHANGE_NUMBER   = ' '
            object_change_indicator = lv_chg_flag
*           PLANNED_OR_REAL_CHANGES = ' '
*           NO_CHANGE_POINTERS      = ' '
*           upd_icdtxt_zarn_chg_cat = ' '
            upd_zarn_cc_det         = lv_update_flag
            upd_zarn_cc_hdr         = lv_update_flag
            upd_zarn_cc_team        = lv_update_flag
          TABLES
            icdtxt_zarn_chg_cat     = lt_cdtxt
            xzarn_cc_det            = lt_xcc_det[]
            yzarn_cc_det            = lt_ycc_det[]
            xzarn_cc_hdr            = lt_xcc_hdr[]
            yzarn_cc_hdr            = lt_ycc_hdr[]
            xzarn_cc_team           = lt_xcc_team[]
            yzarn_cc_team           = lt_ycc_team[].

      ENDLOOP.  " LOOP AT <lt_chgid> ASSIGNING <ls_chgid>

    ENDIF.  " IF fu_s_table-arena_table_type = 'CC'





  ENDLOOP.  " LOOP AT lt_idno[] INTO ls_idno


ENDFORM.
