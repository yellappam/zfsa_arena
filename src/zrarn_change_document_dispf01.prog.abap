*----------------------------------------------------------------------*
***INCLUDE ZRARN_INBOUND_DATA_LOG_DISPF01 .
*----------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*&      Form  PREPARE_FCAT_9000
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
FORM prepare_fcat_9000 USING fu_v_reg TYPE xfeld
                             fu_v_chg TYPE xfeld
                    CHANGING fc_t_fieldcat TYPE lvc_t_fcat.

* Declaration
  DATA: lt_fieldcat TYPE lvc_t_fcat,
        ls_fieldcat TYPE lvc_s_fcat.

  FIELD-SYMBOLS : <ls_fieldcat>    TYPE lvc_s_fcat.

* Prepare field catalog for ALV display by merging structure
  REFRESH: lt_fieldcat[].
  CALL FUNCTION 'LVC_FIELDCATALOG_MERGE'
    EXPORTING
      i_structure_name       = 'ZSARN_CHANGE_DOCUMENT_DISPLAY'
    CHANGING
      ct_fieldcat            = lt_fieldcat[]
    EXCEPTIONS
      inconsistent_interface = 1
      program_error          = 2
      OTHERS                 = 3.
  IF sy-subrc NE 0.
    REFRESH: fc_t_fieldcat.
  ENDIF.



* Looping to make the output field
  LOOP AT lt_fieldcat ASSIGNING <ls_fieldcat>.


    <ls_fieldcat>-tooltip = <ls_fieldcat>-scrtext_l.
    <ls_fieldcat>-col_opt = abap_true.

    CASE <ls_fieldcat>-fieldname.
      WHEN 'IDNO'.
      WHEN 'CHGID'.
      WHEN 'CHANGE_IND'.
      WHEN 'TABNAME'.
      WHEN 'FIELDNAME'.
      WHEN 'FIELDDESC'.
      WHEN 'TABKEY'.
        <ls_fieldcat>-col_opt   = abap_false.
        <ls_fieldcat>-outputlen = '40'.
        <ls_fieldcat>-reptext   = <ls_fieldcat>-scrtext_m.
        <ls_fieldcat>-scrtext_l = <ls_fieldcat>-scrtext_m.

      WHEN 'REFERENCE_DATA'.
        IF fu_v_reg = abap_true.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

      WHEN 'CHG_CATEGORY'.
      WHEN 'CHANGENR'.
      WHEN 'VERSION'.
      WHEN 'VER_COMP_WITH'.
        IF fu_v_reg = abap_true.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

      WHEN 'BANNER'.
        IF fu_v_chg = abap_true.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

      WHEN 'PIM_UOM_CODE'.
        IF fu_v_chg = abap_true.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

      WHEN 'MEINH'.
        IF fu_v_chg = abap_true.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

      WHEN 'EAN11'.
        IF fu_v_chg = abap_true.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

      WHEN 'ORDER_UOM_PIM'.
        IF fu_v_chg = abap_true.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

      WHEN 'HYBRIS_INTERNAL_CODE'.
        IF fu_v_chg = abap_true.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

      WHEN 'PRFAM_UOM'.
        IF fu_v_chg = abap_true.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

      WHEN 'CONDITION_TYPE'.
        IF fu_v_chg = abap_true.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

      WHEN 'EKORG'.
        IF fu_v_chg = abap_true.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

      WHEN 'LIFNR'.
        IF fu_v_chg = abap_true.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

      WHEN 'COND_UNIT'.
        IF fu_v_chg = abap_true.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

      WHEN 'VKORG'.
        IF fu_v_chg = abap_true.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

      WHEN 'PLTYP'.
        IF fu_v_chg = abap_true.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

      WHEN 'BUNIT'.                               "++ONLD-835 JKH 30.05.2017
        IF fu_v_chg = abap_true.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

      WHEN 'CATALOG_TYPE'.                         "++ONLD-835 JKH 30.05.2017
        IF fu_v_chg = abap_true.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.


      WHEN 'CHG_TABLE'.
        IF fu_v_reg = abap_true.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

      WHEN 'CHG_FIELD'.
        IF fu_v_reg = abap_true.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

      WHEN 'TEAM_CODE'.
        IF fu_v_reg = abap_true.
          <ls_fieldcat>-no_out = abap_true.
        ENDIF.

      WHEN 'VALUE_NEW'.
        <ls_fieldcat>-col_opt   = abap_false.
        <ls_fieldcat>-outputlen = '30'.

      WHEN 'VALUE_OLD'.
        <ls_fieldcat>-col_opt   = abap_false.
        <ls_fieldcat>-outputlen = '30'.

      WHEN 'USERID'.
      WHEN 'USERNAME'.
      WHEN 'UDATE'.
      WHEN 'UTIME'.
      WHEN OTHERS.
        DELETE lt_fieldcat WHERE fieldname = <ls_fieldcat>-fieldname.

    ENDCASE.

  ENDLOOP.

  fc_t_fieldcat[] = lt_fieldcat[].

ENDFORM.                    " PREPARE_FCAT_8000
