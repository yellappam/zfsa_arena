*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*----------------------------------------------------------------------*
***INCLUDE ZIARN_APPROVALS_MASS_F02.
*----------------------------------------------------------------------*




*&---------------------------------------------------------------------*
*&      Form  SET_MASS_RELEVANCY
*&---------------------------------------------------------------------*
* Set Mass Relevancy
*----------------------------------------------------------------------*
FORM set_mass_relevancy USING fu_v_called_from  TYPE char3
                              fu_s_ver_data     TYPE ty_s_ver_data
                              fu_t_rel_matrix   TYPE ty_t_rel_matrix
                     CHANGING fc_s_outline_data TYPE zsarn_approvals_mass_tree.

  DATA : lv_process_type TYPE zarn_process_type,
         ls_rel_matrix   TYPE zarn_rel_matrix.


  IF fu_s_ver_data-article_ver IS INITIAL.
    lv_process_type = 'NEW'.
  ELSE.
    lv_process_type = 'CHG'.
  ENDIF.


  fc_s_outline_data-mass01 = abap_true.
  fc_s_outline_data-mass02 = abap_true.
  fc_s_outline_data-mass03 = abap_true.
  fc_s_outline_data-mass04 = abap_true.
  fc_s_outline_data-mass05 = abap_true.

* CT
  CLEAR ls_rel_matrix.
  TRY.
      ls_rel_matrix = fu_t_rel_matrix[ data_level      = fu_v_called_from
                                       process_type    = lv_process_type
                                       change_category = fc_s_outline_data-chg_category
                                       release_team    = 'CT'
                                       ].
      fc_s_outline_data-mass01 = ls_rel_matrix-mass_relevant.
    CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
  ENDTRY.



* BY
  CLEAR ls_rel_matrix.
  TRY.
      ls_rel_matrix = fu_t_rel_matrix[ data_level      = fu_v_called_from
                                       process_type    = lv_process_type
                                       change_category = fc_s_outline_data-chg_category
                                       release_team    = 'BY'
                                       ].
      fc_s_outline_data-mass02 = ls_rel_matrix-mass_relevant.
    CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
  ENDTRY.



* AM
  CLEAR ls_rel_matrix.
  TRY.
      ls_rel_matrix = fu_t_rel_matrix[ data_level      = fu_v_called_from
                                       process_type    = lv_process_type
                                       change_category = fc_s_outline_data-chg_category
                                       release_team    = 'AM'
                                       ].
      fc_s_outline_data-mass03 = ls_rel_matrix-mass_relevant.
    CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
  ENDTRY.



* PT
  TRY.
      ls_rel_matrix = fu_t_rel_matrix[ data_level      = fu_v_called_from
                                       process_type    = lv_process_type
                                       change_category = fc_s_outline_data-chg_category
                                       release_team    = 'PT'
                                       ].
      fc_s_outline_data-mass04 = ls_rel_matrix-mass_relevant.
    CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
  ENDTRY.



* MG
  CLEAR ls_rel_matrix.
  TRY.
      ls_rel_matrix = fu_t_rel_matrix[ data_level      = fu_v_called_from
                                       process_type    = lv_process_type
                                       change_category = fc_s_outline_data-chg_category
                                       release_team    = 'MG'
                                       ].
      fc_s_outline_data-mass05 = ls_rel_matrix-mass_relevant.
    CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
  ENDTRY.







ENDFORM.  " SET_MASS_RELEVANCY
*&---------------------------------------------------------------------*
*&      Form  APPROVALS_BUILD_GRID2
*&---------------------------------------------------------------------*
* Build Approvals Grid data
*----------------------------------------------------------------------*
FORM approvals_build_grid2 USING fu_v_called_from  TYPE char3
                                 fu_s_cc_hdr       TYPE zarn_cc_hdr
                                 fu_s_reg_hdr      TYPE zarn_reg_hdr
                                 fu_s_ver_data     TYPE ty_s_ver_data
                        CHANGING fc_t_reg_hdr      TYPE ty_t_reg_hdr
                                 fc_t_prd_version  TYPE ty_t_prd_version
                                 fc_t_fieldcat     TYPE lvc_t_fcat
                                 fc_s_outline_data TYPE zsarn_approvals_mass_tree
                                 fc_s_idno_appr    TYPE ty_s_idno_appr.



  DATA: lt_approvals      TYPE zarn_t_approval,
        lt_appr_output    TYPE ty_t_appr_output,
        lv_approval_area  TYPE zarn_e_appr_area,
        lt_fieldcat       TYPE lvc_t_fcat,
        lv_input_disabled TYPE xfeld,
        lv_exit           TYPE xfeld.


  IF go_approval IS NOT BOUND.
    go_approval = NEW #( iv_idno  = fu_s_ver_data-idno
                         iv_load_db_approvals = abap_true
                         iv_approval_event = zcl_arn_approval_backend=>gc_event_ui ).
  ENDIF.

  fc_s_idno_appr-go_approval = go_approval.

  fc_s_idno_appr-go_approval->load_recent_approval_data( ).



  "---initialize and determine editable / non editable
  PERFORM appr_initialize USING fu_v_called_from
                                fu_s_ver_data
                                fu_s_reg_hdr
                       CHANGING lv_exit
                                lv_input_disabled
                                fc_s_idno_appr.


  IF lv_exit = abap_true.
    EXIT.
  ENDIF.


  fc_s_idno_appr-gt_approval_teams = fc_s_idno_appr-go_approval->get_conf_teams( iv_display_only = abap_true ).

  lv_approval_area = fu_v_called_from.

  fc_s_idno_appr-go_approval->set_approval_area( lv_approval_area  ).

  lt_approvals[] = fc_s_idno_appr-go_approval->get_approvals( ).

  "do not display posted approvals
  DELETE lt_approvals[] WHERE status = zcl_arn_approval_backend=>gc_stat-posted.

  "ignore other change categories
  DELETE lt_approvals[] WHERE chg_category NE fu_s_cc_hdr-chg_category.


  "---Dynamically build fieldcatalog of the Approval
  PERFORM appr_build_fieldcat2 USING fu_v_called_from
                                     lv_input_disabled
                                     lt_approvals[]
                           CHANGING  lt_fieldcat[]
                                     fc_s_idno_appr.


  "---Build the output table (to be displayed in Tree)
  PERFORM appr_build_output2 USING fu_v_called_from
                                   lt_approvals[]
                          CHANGING lt_appr_output[]
                                   fc_s_idno_appr.


  "---Update header statuses accordingly in global tables
  PERFORM appr_update_hdr_status USING fu_v_called_from
                                       fu_s_ver_data
                              CHANGING fc_t_reg_hdr[]
                                       fc_t_prd_version[]
                                       fc_s_idno_appr.

*  "--Disable approve/reject all buttons if nothing to be done
*  PERFORM appr_disable_buttons USING fu_v_called_from.

  "---Adjust colours in ALV cells
  PERFORM appr_adjust_colours CHANGING lt_appr_output[]
                                       fc_s_idno_appr.


  SORT lt_appr_output[] BY sort_order chg_cat chg_catdesc.

  "---finalize

  CASE fu_v_called_from.

    WHEN co_appr_called_from_nat.

      PERFORM appr_finalize_and_display USING lt_appr_output[]
                                              fu_s_cc_hdr
                                              fu_v_called_from
                                     CHANGING fc_s_idno_appr-gt_appr_nat_approvals[]
                                              fc_s_outline_data.

    WHEN co_appr_called_from_reg.

      PERFORM appr_finalize_and_display USING lt_appr_output[]
                                              fu_s_cc_hdr
                                              fu_v_called_from
                                     CHANGING fc_s_idno_appr-gt_appr_reg_approvals[]
                                              fc_s_outline_data.
  ENDCASE.

  CLEAR: go_approval.
  FREE: go_approval.


ENDFORM.  " APPROVALS_BUILD_GRID2
*&---------------------------------------------------------------------*
*&      Form  APPR_INITIALIZE
*&---------------------------------------------------------------------*
* Build Approvals Grid data
*----------------------------------------------------------------------*
FORM appr_initialize  USING  pv_called_from     TYPE char3
                             ps_ver_data        TYPE ty_s_ver_data
                             ps_reg_hdr         TYPE zarn_reg_hdr
                    CHANGING pcv_exit           TYPE xfeld
                             pcv_input_disabled TYPE xfeld
                             fc_s_idno_appr     TYPE ty_s_idno_appr.


  "---refreshing some globals
  CLEAR pcv_exit.
  CLEAR pcv_input_disabled.

  REFRESH fc_s_idno_appr-gt_approval_comments.

  IF pv_called_from EQ co_appr_called_from_nat.
    REFRESH fc_s_idno_appr-gt_appr_nat_approvals.
  ENDIF.
  IF pv_called_from EQ co_appr_called_from_reg.
    REFRESH fc_s_idno_appr-gt_appr_reg_approvals.
  ENDIF.

  IF pv_called_from EQ co_appr_called_from_nat.
    fc_s_idno_appr-gv_appr_nat_approvals = abap_false.
  ENDIF.
  IF pv_called_from EQ co_appr_called_from_reg.
    fc_s_idno_appr-gv_appr_reg_approvals = abap_false.
  ENDIF.




  "---determine "complete exit
  IF ps_ver_data-idno IS INITIAL.
    "do not proceed and exit completele
    pcv_exit = abap_true.
    RETURN.
  ENDIF.




  "---determine "input disabled"
  IF ps_ver_data-fan_id IS INITIAL
  OR ps_ver_data-fsni_icare_value IS INITIAL.
    pcv_input_disabled = abap_true.
  ENDIF.


  IF pv_called_from EQ co_appr_called_from_nat
   AND ps_ver_data-version_status CS 'OBS'.
    pcv_input_disabled = abap_true.
  ENDIF.

  IF pv_called_from EQ co_appr_called_from_reg
    AND  ps_reg_hdr-status CS 'OBS'.
    pcv_input_disabled = abap_true.
  ENDIF.






  IF pv_called_from EQ co_appr_called_from_reg.

    fc_s_idno_appr-gv_appr_nat_is_approved =  fc_s_idno_appr-go_approval->is_fully_approved(
                                                iv_approval_area = zcl_arn_approval_backend=>gc_appr_area_national
                                                iv_no_records_as_approved = abap_true ).

    IF fc_s_idno_appr-gv_appr_nat_is_approved EQ abap_false.
      fc_s_idno_appr-gv_appr_national_not_approved = 'Regional cannot be approved until National is approved'.
      pcv_input_disabled = abap_true.
    ELSE.
      CLEAR fc_s_idno_appr-gv_appr_national_not_approved.
    ENDIF.
  ENDIF.


  IF gv_display = abap_true.
    pcv_input_disabled = abap_true.
  ENDIF.

*  "---disable buttons based on flag "input disabled"
*  IF pcv_input_disabled = abap_true.
*    LOOP AT SCREEN.
*      IF screen-name EQ 'BUT_APPR_APPROVE'
*      OR screen-name EQ 'BUT_APPR_REJECT'
*      OR screen-name EQ 'BUT_APPR_TOGGLE_EDIT'.
*        screen-input = 0.
*        MODIFY SCREEN.
*      ENDIF.
*    ENDLOOP.
*  ENDIF.


ENDFORM.  " APPR_INITIALIZE
*&---------------------------------------------------------------------*
*&      Form  APPR_BUILD_FIELDCAT2
*&---------------------------------------------------------------------*
* Dynamically build fieldcatalog of the Approval
*----------------------------------------------------------------------*
FORM appr_build_fieldcat2 USING pv_called_from         TYPE char3
                                piv_input_disabled     TYPE xfeld
                                pit_approvals          TYPE zarn_t_approval
                       CHANGING pct_fieldcat           TYPE lvc_t_fcat
                                fc_s_idno_appr         TYPE ty_s_idno_appr.

  CONSTANTS: co_no_team TYPE char3 VALUE 'N/A'.


  IF pv_called_from = co_appr_called_from_nat.
    IF gt_appr_nat_fieldcat[] IS NOT INITIAL.
      EXIT.
    ENDIF.
  ELSEIF pv_called_from = co_appr_called_from_reg.
    IF gt_appr_reg_fieldcat[] IS NOT INITIAL.
      EXIT.
    ENDIF.
  ENDIF.




  CALL FUNCTION 'LVC_FIELDCATALOG_MERGE'
    EXPORTING
*     I_BUFFER_ACTIVE        = I_BUFFER_ACTIVE
      i_structure_name       = 'ZARN_APPROVALS_FIELDS'
*     I_CLIENT_NEVER_DISPLAY = 'X'
*     I_BYPASSING_BUFFER     = I_BYPASSING_BUFFER
*     I_INTERNAL_TABNAME     = I_INTERNAL_TABNAME
    CHANGING
      ct_fieldcat            = pct_fieldcat
    EXCEPTIONS
      inconsistent_interface = 1
      program_error          = 2.

  LOOP AT pct_fieldcat ASSIGNING FIELD-SYMBOL(<ls_fieldcat>).
    CASE <ls_fieldcat>-fieldname.
      WHEN 'CHG_CAT'.
        <ls_fieldcat>-outputlen = 8.
        <ls_fieldcat>-scrtext_s = text-chc.
        <ls_fieldcat>-scrtext_m = text-chc.
        <ls_fieldcat>-scrtext_l = text-chc.
        <ls_fieldcat>-coltext   = text-chc.
        <ls_fieldcat>-reptext   = text-chc.
      WHEN 'CHG_CATDESC'.
        <ls_fieldcat>-outputlen = 20.
        <ls_fieldcat>-hotspot = abap_true.
      WHEN 'DATA_LEVEL'.
        <ls_fieldcat>-no_out = abap_true.
      WHEN 'SORT_ORDER'.
        <ls_fieldcat>-no_out = abap_true.
      WHEN OTHERS.
        IF <ls_fieldcat>-fieldname+4(2) CO '0123456789'.
          READ TABLE fc_s_idno_appr-gt_approval_teams ASSIGNING FIELD-SYMBOL(<ls_approval_team>)
           WITH KEY display_order = <ls_fieldcat>-fieldname+4(2).
          IF sy-subrc EQ 0.
            <ls_fieldcat>-icon      = abap_true.
            <ls_fieldcat>-scrtext_s = <ls_approval_team>-team_code.
            <ls_fieldcat>-scrtext_m = <ls_approval_team>-team_code.
            <ls_fieldcat>-scrtext_l = <ls_approval_team>-team_code.
            <ls_fieldcat>-coltext   = <ls_approval_team>-team_code.
            <ls_fieldcat>-reptext   = <ls_approval_team>-team_code.
            IF <ls_fieldcat>-fieldname(4) EQ co_appr_column_status.
              <ls_fieldcat>-outputlen = 6.
              <ls_fieldcat>-tooltip   = <ls_approval_team>-team_name.
            ELSEIF <ls_fieldcat>-fieldname(4) EQ co_appr_column_approve.
              <ls_fieldcat>-outputlen = 3.
              CONCATENATE
                <ls_approval_team>-team_name
                '- Approve'(ata)
                INTO <ls_fieldcat>-tooltip SEPARATED BY space.
            ELSEIF <ls_fieldcat>-fieldname(4) EQ co_appr_column_reject.
              <ls_fieldcat>-outputlen = 3.
              CONCATENATE
                <ls_approval_team>-team_name
                '- Reject'(atr)
                INTO <ls_fieldcat>-tooltip SEPARATED BY space.
            ELSEIF <ls_fieldcat>-fieldname(4) EQ co_appr_column_text.
              <ls_fieldcat>-outputlen = 3.
              CONCATENATE
                <ls_approval_team>-team_name
                '- Comment'(atc)
                INTO <ls_fieldcat>-tooltip SEPARATED BY space.
            ENDIF.

            <ls_fieldcat>-no_out    = abap_false.

            IF line_exists( pit_approvals[ team_code = <ls_approval_team>-team_code
                                           status = zcl_arn_approval_backend=>gc_stat-appr_ready ] ).

*             we have an approval waiting so need appr/reje/text columns
              IF <ls_fieldcat>-fieldname(4) EQ co_appr_column_status.
                <ls_fieldcat>-no_out    = abap_true.
              ELSEIF <ls_fieldcat>-fieldname(4) EQ co_appr_column_approve
              OR     <ls_fieldcat>-fieldname(4) EQ co_appr_column_reject
              OR     <ls_fieldcat>-fieldname(4) EQ co_appr_column_text.
                <ls_fieldcat>-hotspot = abap_true.
                <ls_fieldcat>-edit    = abap_true.
              ENDIF.

            ELSE.
*             we do not have an approval waiting so just need stat column
              IF <ls_fieldcat>-fieldname(4) EQ co_appr_column_status.
*               need to be able to undo approve/reject
                <ls_fieldcat>-hotspot = abap_true.
              ENDIF.

              IF <ls_fieldcat>-fieldname(4) EQ co_appr_column_approve
              OR <ls_fieldcat>-fieldname(4) EQ co_appr_column_reject.
                <ls_fieldcat>-no_out    = abap_true.
              ENDIF.

*             do we need the text column?
              IF <ls_fieldcat>-fieldname(4) EQ co_appr_column_text.

                "tbd this might be different if we want to allow "comments" at any time
                LOOP AT pit_approvals TRANSPORTING NO FIELDS
                                      WHERE team_code = <ls_approval_team>-team_code
                                        AND status_comment IS NOT INITIAL.
                  EXIT.
                ENDLOOP.
                IF sy-subrc NE 0.
                  <ls_fieldcat>-no_out = abap_true.
                ELSE.
                  <ls_fieldcat>-hotspot = abap_true.
                ENDIF.
              ENDIF.
            ENDIF.
          ELSE.
            <ls_fieldcat>-no_out    = abap_true.
            <ls_fieldcat>-scrtext_s = co_no_team.
            <ls_fieldcat>-scrtext_m = co_no_team.
            <ls_fieldcat>-scrtext_l = co_no_team.
            <ls_fieldcat>-coltext   = co_no_team.
            <ls_fieldcat>-reptext   = co_no_team.
            <ls_fieldcat>-tooltip   = co_no_team.
            <ls_fieldcat>-outputlen = 3.
          ENDIF.
        ENDIF.
        IF pv_called_from EQ co_appr_called_from_reg.
          IF fc_s_idno_appr-gv_appr_nat_is_approved EQ abap_false.
            <ls_fieldcat>-edit    = abap_false.
            <ls_fieldcat>-hotspot = abap_false.
          ENDIF.
        ENDIF.
        IF piv_input_disabled = abap_true.
          <ls_fieldcat>-edit    = abap_false.
          IF <ls_fieldcat>-fieldname(4) NE co_appr_column_text.
            "enable to display text even in Display mode
            <ls_fieldcat>-hotspot = abap_false.
          ENDIF.
        ENDIF.
    ENDCASE.
  ENDLOOP.
  DELETE pct_fieldcat WHERE reptext EQ co_no_team.

  IF pv_called_from EQ co_appr_called_from_nat.
    REFRESH gt_appr_nat_fieldcat.
    APPEND LINES OF pct_fieldcat TO gt_appr_nat_fieldcat.
  ENDIF.
  IF pv_called_from EQ co_appr_called_from_reg.
    REFRESH gt_appr_reg_fieldcat.
    APPEND LINES OF pct_fieldcat TO gt_appr_reg_fieldcat.
  ENDIF.

ENDFORM.  " APPR_BUILD_FIELDCAT2
*&---------------------------------------------------------------------*
*&      Form  APPR_BUILD_OUTPUT2
*&---------------------------------------------------------------------*
* Build the output table (to be displayed in Tree)
*----------------------------------------------------------------------*
FORM appr_build_output2 USING pv_called_from   TYPE char3
                              pit_approvals    TYPE zarn_t_approval
                     CHANGING pct_appr_output  TYPE ty_t_appr_output
                              fc_s_idno_appr   TYPE ty_s_idno_appr.

  DATA       lv_fieldname          TYPE char40.

  FIELD-SYMBOLS:   <ls_col_appr> TYPE zarn_approval_column,
                   <ls_col_stat> TYPE zarn_approval_column,
                   <ls_col_reje> TYPE zarn_approval_column,
                   <ls_col_text> TYPE zarn_approval_column.

* build the table
  REFRESH pct_appr_output.

  LOOP AT pit_approvals ASSIGNING FIELD-SYMBOL(<ls_approval>).

    READ TABLE pct_appr_output ASSIGNING FIELD-SYMBOL(<ls_appr_output>)
     WITH KEY chg_cat = <ls_approval>-chg_category.

    IF sy-subrc NE 0.
      APPEND INITIAL LINE TO pct_appr_output ASSIGNING <ls_appr_output>.
      <ls_appr_output>-data_level = pv_called_from .
      <ls_appr_output>-sort_order = 'B'.
      <ls_appr_output>-chg_cat    = <ls_approval>-chg_category.
      CALL FUNCTION 'Z_ARN_APPROVALS_GET_CHGCAT_DES'
        EXPORTING
          chg_category = <ls_appr_output>-chg_cat
        IMPORTING
          chg_cat_desc = <ls_appr_output>-chg_catdesc.
    ENDIF.
    CHECK <ls_appr_output> IS ASSIGNED.


    READ TABLE fc_s_idno_appr-gt_approval_teams ASSIGNING FIELD-SYMBOL(<ls_approval_team>)
     WITH KEY team_code = <ls_approval>-team_code.
    IF sy-subrc EQ 0.
      CONCATENATE
        '<ls_appr_output>-'
        co_appr_column_approve
        <ls_approval_team>-display_order
        INTO lv_fieldname.
      ASSIGN (lv_fieldname) TO <ls_col_appr>.
      CONCATENATE
        '<ls_appr_output>-'
        co_appr_column_status
        <ls_approval_team>-display_order
        INTO lv_fieldname.
      ASSIGN (lv_fieldname) TO <ls_col_stat>.
      CONCATENATE
        '<ls_appr_output>-'
        co_appr_column_reject
        <ls_approval_team>-display_order
        INTO lv_fieldname.
      ASSIGN (lv_fieldname) TO <ls_col_reje>.
      CONCATENATE
        '<ls_appr_output>-'
        co_appr_column_text
        <ls_approval_team>-display_order
        INTO lv_fieldname.
      ASSIGN (lv_fieldname) TO <ls_col_text>.
    ENDIF.
    IF  <ls_col_appr> IS ASSIGNED
    AND <ls_col_stat> IS ASSIGNED
    AND <ls_col_reje> IS ASSIGNED
    AND <ls_col_text> IS ASSIGNED.

      CLEAR: <ls_col_appr>,
             <ls_col_stat>,
             <ls_col_reje>,
             <ls_col_text>.
      IF <ls_approval>-status_comment IS NOT INITIAL.
        <ls_col_text> = icon_display_text.
      ENDIF.

      CASE <ls_approval>-status.


        WHEN zcl_arn_approval_backend=>gc_stat-appr_ready.
          "check authorisations
          IF fc_s_idno_appr-go_approval->is_authorised_for_team( <ls_approval>-team_code ).
            "yes this user is authorised for approvals

** for testing auth only.
*            IF fc_s_idno_appr-chg_category = 'BSC'.
*            <ls_col_appr> = icon_led_inactive.
*            <ls_col_reje> = icon_led_inactive.
*            <ls_col_stat> = icon_led_inactive.
*            <ls_col_text> = icon_led_inactive.
*              CONTINUE.
*            ENDIF.

            <ls_appr_output>-sort_order = 'A'.
            <ls_col_appr> = icon_okay.
            <ls_col_reje> = icon_cancel. " space.
            IF gv_display NE abap_true.
              IF <ls_col_text> EQ icon_display_text.
                <ls_col_text> = icon_change_text.
              ELSE.
                <ls_col_text> = icon_create_text.
              ENDIF.
            ENDIF.
            IF pv_called_from EQ co_appr_called_from_nat.
              fc_s_idno_appr-gv_appr_nat_approvals = abap_true.
            ENDIF.
            IF pv_called_from EQ co_appr_called_from_reg.
              fc_s_idno_appr-gv_appr_reg_approvals = abap_true.
            ENDIF.

          ELSE.
            <ls_col_appr> = icon_led_inactive.
            <ls_col_reje> = icon_led_inactive.
            <ls_col_stat> = icon_led_inactive.
            <ls_col_text> = icon_led_inactive.
          ENDIF.

        WHEN zcl_arn_approval_backend=>gc_stat-appr_in_queue.
          <ls_col_appr> = icon_led_yellow.
          <ls_col_stat> = icon_led_yellow.

        WHEN  zcl_arn_approval_backend=>gc_stat-approved.
          <ls_col_appr> = icon_led_green.
          <ls_col_stat> = icon_led_green.

        WHEN zcl_arn_approval_backend=>gc_stat-rejected.
          <ls_col_appr> = icon_led_red.
          <ls_col_stat> = icon_led_red.
      ENDCASE.


    ENDIF.
  ENDLOOP.

ENDFORM.  " APPR_BUILD_OUTPUT2
*&---------------------------------------------------------------------*
*&      Form  APPR_UPDATE_HDR_STATUS
*&---------------------------------------------------------------------*
* Update header statuses accordingly in global tables
*----------------------------------------------------------------------*
FORM appr_update_hdr_status USING pv_called_from   TYPE char3
                                  fu_s_ver_data    TYPE ty_s_ver_data
                         CHANGING fc_t_reg_hdr     TYPE ty_t_reg_hdr
                                  fc_t_prd_version TYPE ty_t_prd_version
                                  fc_s_idno_appr   TYPE ty_s_idno_appr.

  DATA: ls_prd_version TYPE zarn_prd_version,
        ls_reg_hdr     TYPE zarn_reg_hdr.

*   N A T I O N A L
  IF pv_called_from EQ co_appr_called_from_nat.
    READ TABLE fc_t_prd_version[] ASSIGNING FIELD-SYMBOL(<ls_prd_version>)
      WITH KEY idno = fu_s_ver_data-idno
            version = fu_s_ver_data-current_ver.
    IF sy-subrc EQ 0.
      CLEAR ls_prd_version.
      ls_prd_version = <ls_prd_version>.
      IF fc_s_idno_appr-gv_appr_nat_release_status NE space.
        <ls_prd_version>-release_status = fc_s_idno_appr-gv_appr_nat_release_status.
      ELSE.
        fc_s_idno_appr-gv_appr_nat_release_status = <ls_prd_version>-release_status.
      ENDIF.
      IF <ls_prd_version>-version_status EQ co_status_apr.
        IF fc_s_idno_appr-gv_appr_nat_approvals EQ abap_true.
*         we have approvals to do so switch from APR to WIP
          <ls_prd_version>-version_status = co_status_wip.
        ENDIF.
      ENDIF.
      IF <ls_prd_version> NE ls_prd_version.
* Update National Status
        PERFORM update_nat_db_status USING ls_prd_version <ls_prd_version>.
      ENDIF.
      fc_s_idno_appr-gv_appr_nat_status = <ls_prd_version>-version_status.
    ENDIF.
  ENDIF.



*   R E G I O N A L
  IF pv_called_from EQ co_appr_called_from_reg.
    READ TABLE fc_t_reg_hdr[] ASSIGNING FIELD-SYMBOL(<ls_reg_hdr>)
       WITH KEY idno = fu_s_ver_data-idno.
    IF sy-subrc EQ 0.
      ls_reg_hdr = <ls_reg_hdr>.
      IF fc_s_idno_appr-gv_appr_reg_release_status NE space.
        <ls_reg_hdr>-release_status = fc_s_idno_appr-gv_appr_reg_release_status.
      ELSE.
        fc_s_idno_appr-gv_appr_reg_release_status = <ls_reg_hdr>-release_status.
      ENDIF.
      IF ( <ls_reg_hdr>-status EQ co_status_apr
        OR <ls_reg_hdr>-status EQ co_status_cre )  "IS FIX - allow change to WIP also from CRE
        AND fc_s_idno_appr-gv_appr_reg_approvals EQ abap_true.
*           we have approvals to do so switch from APR/CRE to WIP
        <ls_reg_hdr>-status = co_status_wip.
      ENDIF.
      IF <ls_reg_hdr> NE ls_reg_hdr.
* Update Regional Status
        PERFORM update_reg_db_status USING ls_reg_hdr <ls_reg_hdr>.
      ENDIF.
      fc_s_idno_appr-gv_appr_reg_status = <ls_reg_hdr>-status.
    ENDIF.
  ENDIF.



  "update the texts
  CLEAR: fc_s_idno_appr-gv_appr_nat_release_statust,
         fc_s_idno_appr-gv_appr_reg_release_statust.
  SELECT SINGLE release_status_desc
    INTO fc_s_idno_appr-gv_appr_nat_release_statust
    FROM zarn_rel_statust                               "#EC CI_NOFIELD
    WHERE release_status EQ fc_s_idno_appr-gv_appr_nat_release_status.
  SELECT SINGLE release_status_desc
    INTO fc_s_idno_appr-gv_appr_reg_release_statust
    FROM zarn_rel_statust                               "#EC CI_NOFIELD
    WHERE release_status EQ fc_s_idno_appr-gv_appr_reg_release_status.


ENDFORM.  " APPR_UPDATE_HDR_STATUS
*&---------------------------------------------------------------------*
*&      Form  APPR_DISABLE_BUTTONS
*&---------------------------------------------------------------------*
* Disable approve/reject all buttons if nothing to be done
*----------------------------------------------------------------------*
FORM appr_disable_buttons USING pv_called_from TYPE char3.

*  IF ( pv_called_from EQ co_appr_called_from_nat AND fc_s_idno_appr-gv_appr_nat_approvals EQ abap_false )
*OR ( pv_called_from EQ co_appr_called_from_reg AND fc_s_idno_appr-gv_appr_reg_approvals EQ abap_false ).
**   Nothing to do so disable the buttons
*    LOOP AT SCREEN.
*      IF screen-name EQ 'BUT_APPR_APPROVE'
*      OR screen-name EQ 'BUT_APPR_REJECT'.
*        screen-input = 0.
*        MODIFY SCREEN.
*      ENDIF.
*    ENDLOOP.
*  ENDIF.


ENDFORM.  " APPR_DISABLE_BUTTONS
*&---------------------------------------------------------------------*
*&      Form  APPR_ADJUST_COLOURS
*&---------------------------------------------------------------------*
* Adjust colours in ALV cells
*----------------------------------------------------------------------*
FORM appr_adjust_colours CHANGING pct_appr_output TYPE ty_t_appr_output
                                  fc_s_idno_appr  TYPE ty_s_idno_appr.

  DATA : ls_cellcolour TYPE lvc_s_scol,
         lv_fieldname  TYPE char40.

  FIELD-SYMBOLS : <ls_col_appr> TYPE zarn_approval_column,
                  <ls_col_stat> TYPE zarn_approval_column,
                  <ls_col_reje> TYPE zarn_approval_column,
                  <ls_col_text> TYPE zarn_approval_column.

* need to recolor some of the cells
  LOOP AT pct_appr_output ASSIGNING FIELD-SYMBOL(<ls_appr_output>).
    LOOP AT fc_s_idno_appr-gt_approval_teams ASSIGNING FIELD-SYMBOL(<ls_approval_team>).
      UNASSIGN: <ls_col_appr>,
                <ls_col_reje>,
                <ls_col_text>.
      CONCATENATE
        '<ls_appr_output>-'
        co_appr_column_approve
        <ls_approval_team>-display_order
        INTO lv_fieldname.
      ASSIGN (lv_fieldname) TO <ls_col_appr>.
      CONCATENATE
        '<ls_appr_output>-'
        co_appr_column_reject
        <ls_approval_team>-display_order
        INTO lv_fieldname.
      ASSIGN (lv_fieldname) TO <ls_col_reje>.
      CONCATENATE
        '<ls_appr_output>-'
        co_appr_column_text
        <ls_approval_team>-display_order
        INTO lv_fieldname.
      ASSIGN (lv_fieldname) TO <ls_col_text>.

      IF <ls_col_appr> IS ASSIGNED.
        IF <ls_col_appr> IS INITIAL
        OR <ls_col_appr> NE icon_okay.
          CLEAR ls_cellcolour.
          ls_cellcolour-color-col = 2.
          CONCATENATE
            co_appr_column_approve
            <ls_approval_team>-display_order
            INTO ls_cellcolour-fname.
          APPEND ls_cellcolour TO <ls_appr_output>-cellcolour.
        ENDIF.
      ENDIF.
      IF <ls_col_reje> IS ASSIGNED.
        IF <ls_col_reje> IS INITIAL
        OR <ls_col_reje> NE icon_cancel.
          CLEAR ls_cellcolour.
          ls_cellcolour-color-col = 2.
          CONCATENATE
            co_appr_column_reject
            <ls_approval_team>-display_order
            INTO ls_cellcolour-fname.
          APPEND ls_cellcolour TO <ls_appr_output>-cellcolour.
        ENDIF.
      ENDIF.
      IF <ls_col_text> IS ASSIGNED.
        IF <ls_col_text> IS INITIAL
        OR <ls_col_text> EQ icon_display_text.
          CLEAR ls_cellcolour.
          ls_cellcolour-color-col = 2.
          CONCATENATE
            co_appr_column_text
            <ls_approval_team>-display_order
            INTO ls_cellcolour-fname.
          APPEND ls_cellcolour TO <ls_appr_output>-cellcolour.
        ENDIF.
      ENDIF.
    ENDLOOP.
  ENDLOOP.


ENDFORM.  " APPR_ADJUST_COLOURS
*&---------------------------------------------------------------------*
*&      Form  UPDATE_NAT_DB_STATUS
*&---------------------------------------------------------------------*
FORM update_nat_db_status USING ps_prd_version_old TYPE zarn_prd_version
                                ps_prd_version_new TYPE zarn_prd_version.

  DATA: lv_objectid               TYPE cdhdr-objectid,
        lv_tcode                  TYPE cdhdr-tcode,
        lv_utime                  TYPE cdhdr-utime,
        lv_udate                  TYPE cdhdr-udate,
        lv_username               TYPE cdhdr-username,
        lt_icdtxt_zarn_ctrl_regnl TYPE STANDARD TABLE OF cdtxt,
        lt_xzarn_control          TYPE STANDARD TABLE OF yzarn_control,
        lt_yzarn_control          TYPE STANDARD TABLE OF yzarn_control,
        lt_xzarn_prd_version      TYPE STANDARD TABLE OF yzarn_prd_version,
        lt_yzarn_prd_version      TYPE STANDARD TABLE OF yzarn_prd_version,
        lt_xzarn_reg_banner       TYPE STANDARD TABLE OF yzarn_reg_banner,
        lt_yzarn_reg_banner       TYPE STANDARD TABLE OF yzarn_reg_banner,
        lt_xzarn_reg_ean          TYPE STANDARD TABLE OF yzarn_reg_ean,
        lt_yzarn_reg_ean          TYPE STANDARD TABLE OF yzarn_reg_ean,
        lt_xzarn_reg_hdr          TYPE STANDARD TABLE OF yzarn_reg_hdr,
        lt_yzarn_reg_hdr          TYPE STANDARD TABLE OF yzarn_reg_hdr,
        lt_xzarn_reg_hsno         TYPE STANDARD TABLE OF yzarn_reg_hsno,
        lt_yzarn_reg_hsno         TYPE STANDARD TABLE OF yzarn_reg_hsno,
        lt_xzarn_reg_pir          TYPE STANDARD TABLE OF yzarn_reg_pir,
        lt_yzarn_reg_pir          TYPE STANDARD TABLE OF yzarn_reg_pir,
        lt_xzarn_reg_prfam        TYPE STANDARD TABLE OF yzarn_reg_prfam,
        lt_yzarn_reg_prfam        TYPE STANDARD TABLE OF yzarn_reg_prfam,
        lt_xzarn_reg_txt          TYPE STANDARD TABLE OF yzarn_reg_txt,
        lt_yzarn_reg_txt          TYPE STANDARD TABLE OF yzarn_reg_txt,
        lt_xzarn_reg_uom          TYPE STANDARD TABLE OF yzarn_reg_uom,
        lt_yzarn_reg_uom          TYPE STANDARD TABLE OF yzarn_reg_uom,
        lt_xzarn_ver_status       TYPE STANDARD TABLE OF yzarn_ver_status,
        lt_yzarn_ver_status       TYPE STANDARD TABLE OF yzarn_ver_status,
        lt_xzarn_reg_dc_sell      TYPE STANDARD TABLE OF yzarn_reg_dc_sell,
        lt_yzarn_reg_dc_sell      TYPE STANDARD TABLE OF yzarn_reg_dc_sell,
        lt_xzarn_reg_lst_prc      TYPE STANDARD TABLE OF yzarn_reg_lst_prc,
        lt_yzarn_reg_lst_prc      TYPE STANDARD TABLE OF yzarn_reg_lst_prc,
        lt_xzarn_reg_rrp          TYPE STANDARD TABLE OF yzarn_reg_rrp,
        lt_yzarn_reg_rrp          TYPE STANDARD TABLE OF yzarn_reg_rrp,
        lt_xzarn_reg_std_ter      TYPE STANDARD TABLE OF yzarn_reg_std_ter,
        lt_yzarn_reg_std_ter      TYPE STANDARD TABLE OF yzarn_reg_std_ter,
        lt_xzarn_reg_artlink      TYPE STANDARD TABLE OF yzarn_reg_artlink,
        lt_yzarn_reg_artlink      TYPE STANDARD TABLE OF yzarn_reg_artlink,
        lt_xzarn_reg_onlcat       TYPE STANDARD TABLE OF yzarn_reg_onlcat,
        lt_yzarn_reg_onlcat       TYPE STANDARD TABLE OF yzarn_reg_onlcat,
        lt_xzarn_reg_str          TYPE yzarn_reg_strs,
        lt_yzarn_reg_str          TYPE yzarn_reg_strs.



  REFRESH: lt_icdtxt_zarn_ctrl_regnl,
           lt_xzarn_control,
           lt_yzarn_control,
           lt_xzarn_prd_version,
           lt_yzarn_prd_version,
           lt_xzarn_reg_banner,
           lt_yzarn_reg_banner,
           lt_xzarn_reg_ean,
           lt_yzarn_reg_ean,
           lt_xzarn_reg_hdr,
           lt_yzarn_reg_hdr,
           lt_xzarn_reg_hsno,
           lt_yzarn_reg_hsno,
           lt_xzarn_reg_pir,
           lt_yzarn_reg_pir,
           lt_xzarn_reg_prfam,
           lt_yzarn_reg_prfam,
           lt_xzarn_reg_txt,
           lt_yzarn_reg_txt,
           lt_xzarn_reg_uom,
           lt_yzarn_reg_uom,
           lt_xzarn_ver_status,
           lt_yzarn_ver_status,
           lt_xzarn_reg_dc_sell,
           lt_yzarn_reg_dc_sell,
           lt_xzarn_reg_lst_prc,
           lt_yzarn_reg_lst_prc,
           lt_xzarn_reg_rrp,
           lt_yzarn_reg_rrp,
           lt_xzarn_reg_std_ter,
           lt_yzarn_reg_std_ter,
           lt_yzarn_reg_artlink,
           lt_xzarn_reg_artlink,
           lt_yzarn_reg_onlcat,
           lt_xzarn_reg_onlcat,
           lt_yzarn_reg_str,
           lt_xzarn_reg_str.

  "IS1608 fix - Y is for OLD, X is for NEW... was the other way

  APPEND ps_prd_version_old TO lt_yzarn_prd_version.

  ps_prd_version_new-changed_on = sy-datum.
  ps_prd_version_new-changed_at = sy-uzeit.
  ps_prd_version_new-changed_by = sy-uname.
  APPEND ps_prd_version_new TO lt_xzarn_prd_version.

  lv_objectid = ps_prd_version_old-idno.
  lv_tcode    = sy-tcode.
  lv_utime    = sy-uzeit.
  lv_udate    = sy-datum.
  lv_username = sy-uname.

  CALL FUNCTION 'ZARN_CTRL_REGNL_WRITE_DOC_CUST'
    EXPORTING
      objectid               = lv_objectid
      tcode                  = lv_tcode
      utime                  = lv_utime
      udate                  = lv_udate
      username               = lv_username
      upd_zarn_prd_version   = 'U'    "IS1608 fix - needs to be U for update, was X
      xzarn_reg_str          = lt_xzarn_reg_str
      yzarn_reg_str          = lt_yzarn_reg_str
    TABLES
      icdtxt_zarn_ctrl_regnl = lt_icdtxt_zarn_ctrl_regnl
      xzarn_control          = lt_xzarn_control
      yzarn_control          = lt_yzarn_control
      xzarn_prd_version      = lt_xzarn_prd_version
      yzarn_prd_version      = lt_yzarn_prd_version
      xzarn_reg_artlink      = lt_xzarn_reg_artlink
      yzarn_reg_artlink      = lt_yzarn_reg_artlink
      xzarn_reg_banner       = lt_xzarn_reg_banner
      yzarn_reg_banner       = lt_yzarn_reg_banner
      xzarn_reg_dc_sell      = lt_xzarn_reg_dc_sell
      yzarn_reg_dc_sell      = lt_yzarn_reg_dc_sell
      xzarn_reg_ean          = lt_xzarn_reg_ean
      yzarn_reg_ean          = lt_yzarn_reg_ean
      xzarn_reg_hdr          = lt_xzarn_reg_hdr
      yzarn_reg_hdr          = lt_yzarn_reg_hdr
      xzarn_reg_hsno         = lt_xzarn_reg_hsno
      yzarn_reg_hsno         = lt_yzarn_reg_hsno
      xzarn_reg_lst_prc      = lt_xzarn_reg_lst_prc
      yzarn_reg_lst_prc      = lt_yzarn_reg_lst_prc
      xzarn_reg_onlcat       = lt_xzarn_reg_onlcat
      yzarn_reg_onlcat       = lt_yzarn_reg_onlcat
      xzarn_reg_pir          = lt_xzarn_reg_pir
      yzarn_reg_pir          = lt_yzarn_reg_pir
      xzarn_reg_prfam        = lt_xzarn_reg_prfam
      yzarn_reg_prfam        = lt_yzarn_reg_prfam
      xzarn_reg_rrp          = lt_xzarn_reg_rrp
      yzarn_reg_rrp          = lt_yzarn_reg_rrp
      xzarn_reg_std_ter      = lt_xzarn_reg_std_ter
      yzarn_reg_std_ter      = lt_yzarn_reg_std_ter
      xzarn_reg_txt          = lt_xzarn_reg_txt
      yzarn_reg_txt          = lt_yzarn_reg_txt
      xzarn_reg_uom          = lt_xzarn_reg_uom
      yzarn_reg_uom          = lt_yzarn_reg_uom
      xzarn_ver_status       = lt_xzarn_ver_status
      yzarn_ver_status       = lt_yzarn_ver_status.

  UPDATE zarn_prd_version
    SET version_status = ps_prd_version_new-version_status
        release_status = ps_prd_version_new-release_status
    WHERE idno  EQ ps_prd_version_new-idno
    AND version EQ ps_prd_version_new-version.

ENDFORM.  " UPDATE_NAT_DB_STATUS

*&---------------------------------------------------------------------*
*&      Form  UPDATE_REG_DB_STATUS
*&---------------------------------------------------------------------*
FORM update_reg_db_status USING ps_reg_hdr_old TYPE zarn_reg_hdr
                                ps_reg_hdr_new TYPE zarn_reg_hdr.

  DATA: lv_objectid               TYPE cdhdr-objectid,
        lv_tcode                  TYPE cdhdr-tcode,
        lv_utime                  TYPE cdhdr-utime,
        lv_udate                  TYPE cdhdr-udate,
        lv_username               TYPE cdhdr-username,
        lt_icdtxt_zarn_ctrl_regnl TYPE STANDARD TABLE OF cdtxt,
        lt_xzarn_control          TYPE STANDARD TABLE OF yzarn_control,
        lt_yzarn_control          TYPE STANDARD TABLE OF yzarn_control,
        lt_xzarn_prd_version      TYPE STANDARD TABLE OF yzarn_prd_version,
        lt_yzarn_prd_version      TYPE STANDARD TABLE OF yzarn_prd_version,
        lt_xzarn_reg_banner       TYPE STANDARD TABLE OF yzarn_reg_banner,
        lt_yzarn_reg_banner       TYPE STANDARD TABLE OF yzarn_reg_banner,
        lt_xzarn_reg_ean          TYPE STANDARD TABLE OF yzarn_reg_ean,
        lt_yzarn_reg_ean          TYPE STANDARD TABLE OF yzarn_reg_ean,
        lt_xzarn_reg_hdr          TYPE STANDARD TABLE OF yzarn_reg_hdr,
        lt_yzarn_reg_hdr          TYPE STANDARD TABLE OF yzarn_reg_hdr,
        lt_xzarn_reg_hsno         TYPE STANDARD TABLE OF yzarn_reg_hsno,
        lt_yzarn_reg_hsno         TYPE STANDARD TABLE OF yzarn_reg_hsno,
        lt_xzarn_reg_pir          TYPE STANDARD TABLE OF yzarn_reg_pir,
        lt_yzarn_reg_pir          TYPE STANDARD TABLE OF yzarn_reg_pir,
        lt_xzarn_reg_prfam        TYPE STANDARD TABLE OF yzarn_reg_prfam,
        lt_yzarn_reg_prfam        TYPE STANDARD TABLE OF yzarn_reg_prfam,
        lt_xzarn_reg_txt          TYPE STANDARD TABLE OF yzarn_reg_txt,
        lt_yzarn_reg_txt          TYPE STANDARD TABLE OF yzarn_reg_txt,
        lt_xzarn_reg_uom          TYPE STANDARD TABLE OF yzarn_reg_uom,
        lt_yzarn_reg_uom          TYPE STANDARD TABLE OF yzarn_reg_uom,
        lt_xzarn_ver_status       TYPE STANDARD TABLE OF yzarn_ver_status,
        lt_yzarn_ver_status       TYPE STANDARD TABLE OF yzarn_ver_status,
        lt_xzarn_reg_dc_sell      TYPE STANDARD TABLE OF yzarn_reg_dc_sell,
        lt_yzarn_reg_dc_sell      TYPE STANDARD TABLE OF yzarn_reg_dc_sell,
        lt_xzarn_reg_lst_prc      TYPE STANDARD TABLE OF yzarn_reg_lst_prc,
        lt_yzarn_reg_lst_prc      TYPE STANDARD TABLE OF yzarn_reg_lst_prc,
        lt_xzarn_reg_rrp          TYPE STANDARD TABLE OF yzarn_reg_rrp,
        lt_yzarn_reg_rrp          TYPE STANDARD TABLE OF yzarn_reg_rrp,
        lt_xzarn_reg_std_ter      TYPE STANDARD TABLE OF yzarn_reg_std_ter,
        lt_yzarn_reg_std_ter      TYPE STANDARD TABLE OF yzarn_reg_std_ter,
        lt_xzarn_reg_artlink      TYPE STANDARD TABLE OF yzarn_reg_artlink,
        lt_yzarn_reg_artlink      TYPE STANDARD TABLE OF yzarn_reg_artlink,
        lt_xzarn_reg_onlcat       TYPE STANDARD TABLE OF yzarn_reg_onlcat,
        lt_yzarn_reg_onlcat       TYPE STANDARD TABLE OF yzarn_reg_onlcat,
        lt_xzarn_reg_str          TYPE yzarn_reg_strs,
        lt_yzarn_reg_str          TYPE yzarn_reg_strs.
  "IS1608 fix - Y is for OLD, X is for NEW... was the other way

  APPEND ps_reg_hdr_old TO lt_yzarn_reg_hdr.
  ps_reg_hdr_new-laeda = sy-datum.
  ps_reg_hdr_new-eruet = sy-uzeit.
  ps_reg_hdr_new-aenam = sy-uname.
  APPEND ps_reg_hdr_new TO lt_xzarn_reg_hdr.

  lv_objectid = ps_reg_hdr_old-idno.
  lv_tcode    = sy-tcode.
  lv_utime    = sy-uzeit.
  lv_udate    = sy-datum.
  lv_username = sy-uname.

  CALL FUNCTION 'ZARN_CTRL_REGNL_WRITE_DOC_CUST'
    EXPORTING
      objectid               = lv_objectid
      tcode                  = lv_tcode
      utime                  = lv_utime
      udate                  = lv_udate
      username               = lv_username
      upd_zarn_reg_hdr       = 'U'  "IS1608 fix - needs to be U for update, was X
      xzarn_reg_str          = lt_xzarn_reg_str
      yzarn_reg_str          = lt_yzarn_reg_str
    TABLES
      icdtxt_zarn_ctrl_regnl = lt_icdtxt_zarn_ctrl_regnl
      xzarn_control          = lt_xzarn_control
      yzarn_control          = lt_yzarn_control
      xzarn_prd_version      = lt_xzarn_prd_version
      yzarn_prd_version      = lt_yzarn_prd_version
      xzarn_reg_artlink      = lt_xzarn_reg_artlink
      yzarn_reg_artlink      = lt_yzarn_reg_artlink
      xzarn_reg_banner       = lt_xzarn_reg_banner
      yzarn_reg_banner       = lt_yzarn_reg_banner
      xzarn_reg_dc_sell      = lt_xzarn_reg_dc_sell
      yzarn_reg_dc_sell      = lt_yzarn_reg_dc_sell
      xzarn_reg_ean          = lt_xzarn_reg_ean
      yzarn_reg_ean          = lt_yzarn_reg_ean
      xzarn_reg_hdr          = lt_xzarn_reg_hdr
      yzarn_reg_hdr          = lt_yzarn_reg_hdr
      xzarn_reg_hsno         = lt_xzarn_reg_hsno
      yzarn_reg_hsno         = lt_yzarn_reg_hsno
      xzarn_reg_lst_prc      = lt_xzarn_reg_lst_prc
      yzarn_reg_lst_prc      = lt_yzarn_reg_lst_prc
      xzarn_reg_onlcat       = lt_xzarn_reg_onlcat
      yzarn_reg_onlcat       = lt_yzarn_reg_onlcat
      xzarn_reg_pir          = lt_xzarn_reg_pir
      yzarn_reg_pir          = lt_yzarn_reg_pir
      xzarn_reg_prfam        = lt_xzarn_reg_prfam
      yzarn_reg_prfam        = lt_yzarn_reg_prfam
      xzarn_reg_rrp          = lt_xzarn_reg_rrp
      yzarn_reg_rrp          = lt_yzarn_reg_rrp
      xzarn_reg_std_ter      = lt_xzarn_reg_std_ter
      yzarn_reg_std_ter      = lt_yzarn_reg_std_ter
      xzarn_reg_txt          = lt_xzarn_reg_txt
      yzarn_reg_txt          = lt_yzarn_reg_txt
      xzarn_reg_uom          = lt_xzarn_reg_uom
      yzarn_reg_uom          = lt_yzarn_reg_uom
      xzarn_ver_status       = lt_xzarn_ver_status
      yzarn_ver_status       = lt_yzarn_ver_status.

  UPDATE zarn_reg_hdr
    SET status         = ps_reg_hdr_new-status
        release_status = ps_reg_hdr_new-release_status
    WHERE idno  EQ ps_reg_hdr_old-idno.

ENDFORM.  " UPDATE_REG_DB_STATUS
*&---------------------------------------------------------------------*
*&      Form  APPR_FINALIZE_AND_DISPLAY
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->P_LT_APPR_OUTPUT[]  text
*      <--P_GT_APPR_NAT_APPROVALS[]  text
*----------------------------------------------------------------------*
FORM appr_finalize_and_display USING pit_appr_output   TYPE ty_t_appr_output
                                     fu_s_cc_hdr       TYPE zarn_cc_hdr
                                     fu_v_called_from  TYPE char3
                            CHANGING pct_gt_appr       TYPE ty_t_appr_output
                                     fc_s_outline_data TYPE zsarn_approvals_mass_tree.

  DATA: ls_gt_apr TYPE zarn_approvals_fields.

  REFRESH pct_gt_appr[] .
  APPEND LINES OF pit_appr_output[] TO pct_gt_appr[].


  CLEAR ls_gt_apr.
  READ TABLE pct_gt_appr[] INTO ls_gt_apr
  WITH KEY data_level = fu_v_called_from
           chg_cat    = fu_s_cc_hdr-chg_category.
  IF sy-subrc = 0.
    MOVE-CORRESPONDING ls_gt_apr TO fc_s_outline_data.
  ENDIF.




ENDFORM.  " APPR_FINALIZE_AND_DISPLAY
*&---------------------------------------------------------------------*
*&      Form  APPR_HANDLE_LINK_CLICK
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
FORM appr_handle_link_click USING fu_v_fieldname TYPE lvc_fname
                                  fu_v_node_key  TYPE lvc_nkey.

  DATA: lv_change_category TYPE zarn_chg_category,
        lt_appr_approvals  TYPE STANDARD TABLE OF zarn_approvals_fields,
        lv_status          TYPE zarn_approval_status,
        lv_fieldname       TYPE char40,
        lv_col_number      TYPE numc2,
        lv_called_from     TYPE char3,

        ls_ver_data        TYPE ty_s_ver_data,
        ls_reg_hdr         TYPE zarn_reg_hdr,
        ls_reg_data        TYPE zsarn_reg_data,
        ls_prod_data       TYPE zsarn_prod_data.



  FIELD-SYMBOLS: <ls_column>        TYPE zarn_approval_column,
                 <ls_approval_team> TYPE zarn_teams,
                 <ls_appr_approval> TYPE zarn_approvals_fields,

                 <ls_output>        TYPE zsarn_approvals_mass_tree,
                 <ls_idno_appr>     TYPE ty_s_idno_appr.


  READ TABLE gt_output[] ASSIGNING <ls_output>
  WITH KEY node_key  = fu_v_node_key
           line_type = 'N'.
  IF sy-subrc NE 0.
    EXIT.
  ENDIF.

  READ TABLE gt_idno_appr[] ASSIGNING <ls_idno_appr>
  WITH KEY idno         = <ls_output>-idno
           chg_area     = <ls_output>-chg_area
           chg_category = <ls_output>-chg_category.
  IF sy-subrc NE 0.
    EXIT.
  ENDIF.

  CLEAR: ls_ver_data, ls_reg_hdr, ls_reg_data, ls_prod_data.
  TRY.
      ls_ver_data = gt_ver_data[ idno = <ls_output>-idno ].
      ls_reg_hdr  = gt_reg_hdr[ idno = <ls_output>-idno ].

      ls_reg_data  = gt_reg_data[ idno = <ls_output>-idno ].

      ls_prod_data = gt_prod_data[ idno    = <ls_output>-idno
                                   version = ls_ver_data-current_ver ].

    CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
  ENDTRY.

* Maintain IDNO_APPR info as global so as to use the clicked IDNO_APPR
* details where not accessible
  ASSIGN <ls_idno_appr> TO <gs_idno_appr>.

  IF <ls_idno_appr>-chg_area = 'N'.
    lv_called_from = co_appr_called_from_nat.
  ELSEIF <ls_idno_appr>-chg_area = 'R'.
    lv_called_from = co_appr_called_from_reg.
  ENDIF.


  REFRESH lt_appr_approvals[].
  IF lv_called_from EQ co_appr_called_from_nat.
    lt_appr_approvals[] = <ls_idno_appr>-gt_appr_nat_approvals[].
  ELSEIF lv_called_from EQ co_appr_called_from_reg.
    lt_appr_approvals[] = <ls_idno_appr>-gt_appr_reg_approvals[].
  ENDIF.







  IF fu_v_fieldname EQ 'CHG_CATDESC'.

  ELSEIF fu_v_fieldname+4(2) CO '0123456789'.

    lv_col_number = fu_v_fieldname+4(2).

    IF <ls_appr_approval> IS ASSIGNED. UNASSIGN <ls_appr_approval>. ENDIF.
    READ TABLE lt_appr_approvals ASSIGNING <ls_appr_approval>
    WITH KEY data_level = lv_called_from
             chg_cat    = <ls_output>-chg_category.
    IF sy-subrc = 0.

      CLEAR lv_fieldname.
      IF <ls_column> IS ASSIGNED. UNASSIGN <ls_column>. ENDIF.
      CONCATENATE '<ls_appr_approval>-' fu_v_fieldname
             INTO lv_fieldname.
      ASSIGN (lv_fieldname) TO <ls_column>.

      IF <ls_column> IS ASSIGNED.
        IF NOT <ls_column> IS INITIAL.

          CLEAR lv_change_category.
          IF <ls_approval_team> IS ASSIGNED. UNASSIGN <ls_approval_team>. ENDIF.
          READ TABLE <ls_idno_appr>-gt_approval_teams ASSIGNING <ls_approval_team>
            WITH KEY display_order = lv_col_number.
          IF sy-subrc EQ 0.
            lv_change_category = <ls_appr_approval>-chg_cat.

            READ TABLE lt_appr_approvals ASSIGNING <ls_appr_approval>
              WITH KEY data_level = lv_called_from
                       chg_cat    = <ls_output>-chg_category.
            IF sy-subrc EQ 0.
              IF fu_v_fieldname(4) EQ co_appr_column_approve.

                IF <ls_column> EQ icon_led_green.
* Reset/Undo Approval
                  PERFORM approvals_reset   USING ls_ver_data
                                                  ls_reg_hdr
                                                  ls_reg_data
                                                  ls_prod_data
                                                  lv_called_from
                                                  lv_change_category
                                                  <ls_approval_team>-team_code
                                                  zif_arn_approval_status=>gc_arn_appr_status_reset
                                         CHANGING <ls_idno_appr>
                                                  gt_log[]
                                                  gt_prd_version[]
                                                  gt_reg_data[]
                                                  gt_prod_data[]
                                                  gt_reg_data_o[]
                                                  gt_prod_data_o[].


                ELSEIF <ls_column> EQ icon_led_red.
* Reset/Undo Rejection
                  PERFORM approvals_reset   USING ls_ver_data
                                                  ls_reg_hdr
                                                  ls_reg_data
                                                  ls_prod_data
                                                  lv_called_from
                                                  lv_change_category
                                                  <ls_approval_team>-team_code
                                                  zif_arn_approval_status=>gc_arn_appr_status_reset
                                         CHANGING <ls_idno_appr>
                                                  gt_log[]
                                                  gt_prd_version[]
                                                  gt_reg_data[]
                                                  gt_prod_data[]
                                                  gt_reg_data_o[]
                                                  gt_prod_data_o[].


                ELSE.
* Approve
                  PERFORM approvals_update2 USING ls_ver_data
                                                  ls_reg_hdr
                                                  ls_reg_data
                                                  ls_prod_data
                                                  lv_called_from
                                                  lv_change_category
                                                  <ls_approval_team>-team_code
                                                  zif_arn_approval_status=>gc_arn_appr_status_approved
                                         CHANGING <ls_idno_appr>
                                                  gt_log[]
                                                  gt_prd_version[]
                                                  gt_reg_data[]
                                                  gt_prod_data[]
                                                  gt_reg_data_o[]
                                                  gt_prod_data_o[].

                ENDIF.

              ELSEIF fu_v_fieldname(4) EQ co_appr_column_reject.
* Reject
                PERFORM approvals_update2 USING ls_ver_data
                                                ls_reg_hdr
                                                ls_reg_data
                                                ls_prod_data
                                                lv_called_from
                                                lv_change_category
                                                <ls_approval_team>-team_code
                                                zif_arn_approval_status=>gc_arn_appr_status_rejected
                                       CHANGING <ls_idno_appr>
                                                gt_log[]
                                                gt_prd_version[]
                                                gt_reg_data[]
                                                gt_prod_data[]
                                                gt_reg_data_o[]
                                                gt_prod_data_o[].


              ELSEIF fu_v_fieldname(4) EQ co_appr_column_status.
*** Status
**                IF <ls_column> EQ icon_led_green.
**                  PERFORM approvals_reset USING pv_called_from <ls_appr_approval> lv_col_number lv_change_category <ls_approval_team>-team_code zif_arn_approval_status=>gc_arn_appr_status_approved.
**                ELSEIF <ls_column> EQ icon_led_red.
**                  PERFORM approvals_reset USING pv_called_from <ls_appr_approval> lv_col_number lv_change_category <ls_approval_team>-team_code zif_arn_approval_status=>gc_arn_appr_status_rejected.
**                ENDIF.

              ELSEIF fu_v_fieldname(4) EQ co_appr_column_text.
*** Text
                IF <ls_column> EQ icon_display_text.
                  " Display Text
                  PERFORM approvals_display_text2 USING lv_called_from
                                                        lv_change_category
                                                        <ls_approval_team>-team_code
                                               CHANGING <ls_idno_appr>.
                ELSE.
                  " Add Text
                  PERFORM approvals_add_text2 USING lv_called_from
                                                    lv_change_category
                                                    <ls_approval_team>-team_code
                                           CHANGING <ls_column>
                                                    <ls_idno_appr>.

                ENDIF.

                IF lv_called_from EQ co_appr_called_from_nat.
                  <ls_idno_appr>-gt_appr_nat_approvals[] = lt_appr_approvals[].
                ELSEIF lv_called_from EQ co_appr_called_from_reg.
                  <ls_idno_appr>-gt_appr_reg_approvals[] = lt_appr_approvals[].
                ENDIF.


              ENDIF.  " IF fieldname(4) = APPR/REJE/STAT/TEXT
            ENDIF.  " READ TABLE lt_appr_approvals ASSIGNING <ls_appr_approval>
          ENDIF.  " READ TABLE ls_idno_appr-gt_approval_teams ASSIGNING <ls_approval_team>
        ENDIF.  " IF <ls_column> IS ASSIGNED
      ENDIF.  " IF NOT <ls_column> IS INITIAL
    ENDIF.  " READ TABLE lt_appr_approvals ASSIGNING <ls_appr_approval>
  ENDIF.  " ELSEIF fu_v_fieldname+4(2) CO '0123456789'

ENDFORM.  " APPR_HANDLE_LINK_CLICK
*&---------------------------------------------------------------------*
*&      Form  APPROVALS_UPDATE2
*&---------------------------------------------------------------------*
*
*----------------------------------------------------------------------*
FORM approvals_update2 USING ps_ver_data        TYPE ty_s_ver_data
                             ps_reg_hdr         TYPE zarn_reg_hdr
                             ps_reg_data        TYPE zsarn_reg_data
                             ps_prod_data       TYPE zsarn_prod_data
                             pv_called_from     TYPE char3
                             pv_change_category TYPE zarn_chg_category
                             pv_team_code       TYPE zarn_team_code
                             pv_status          TYPE zarn_approval_status
                    CHANGING pcs_idno_appr      TYPE ty_s_idno_appr
                             pct_log            TYPE ztarn_approvals_mass_log
                             pct_prd_version    TYPE ty_t_prd_version
                             pct_reg_data       TYPE ty_t_reg_data
                             pct_prod_data      TYPE ty_t_prod_data
                             pct_reg_data_o     TYPE ty_t_reg_data
                             pct_prod_data_o    TYPE ty_t_prod_data.

  DATA: lt_appr_approvals  TYPE ty_t_appr_output,
        lt_approval_teams  TYPE zarn_t_teams,
        lv_rej_comment     TYPE string,
        lv_exit            TYPE xfeld,
        lv_approval_area   TYPE zarn_e_appr_area,
        lv_processing_team TYPE zarn_team_code,
        ls_log             TYPE zsarn_approvals_mass_log,

        lo_approval        TYPE REF TO zcl_arn_approval_backend.


  lo_approval = pcs_idno_appr-go_approval.

  IF lo_approval IS INITIAL.
    EXIT.
  ENDIF.

  lv_approval_area = pv_called_from.



  lo_approval->set_approval_area( lv_approval_area ).

  "---initial checks whether action is allowed
  PERFORM appr_action_check_before USING pcs_idno_appr
                                         ps_ver_data
                                         ps_reg_hdr
                                         ps_reg_data
                                         ps_prod_data
                                         pv_called_from
                                         pv_status
                                CHANGING lv_exit
                                         pct_log[].
  IF lv_exit = abap_true.
    "action not relevant for further processing here
    RETURN.
  ENDIF.

* Prepare data required to perform the action
  PERFORM appr_action_data_prepare2   USING pv_called_from
                                            pv_team_code
                                            pv_status
                                   CHANGING lv_processing_team
                                            lt_appr_approvals
                                            lt_approval_teams
                                            pcs_idno_appr
                                            lv_rej_comment
                                            lv_exit.

  IF lv_exit = abap_true.
    "action not relevant for further processing here
    RETURN.
  ENDIF.


* All the determination logic and change ZARN_APPROVAL
  PERFORM appr_action_post2 USING  pv_called_from
                                   ps_ver_data
                                   ps_reg_hdr
                                   lt_appr_approvals
                                   pv_change_category
                                   lt_approval_teams
                                   lv_rej_comment
                                   lv_processing_team
                          CHANGING pcs_idno_appr
                                   lv_exit
                                   pct_log[].

  IF lv_exit = abap_true.
    "action not relevant for further processing here
    RETURN.
  ENDIF.


* Update statuses on header (regional/national)
* this is only for WIP / REJ status (work in progress)
  PERFORM appr_action_stat_update USING pv_called_from
                                        pv_status
                                        ps_ver_data
                               CHANGING pct_prd_version[]
                                        pct_reg_data[].


  COMMIT WORK AND WAIT.

  TRY.
      "action posted, continue with optimistic lock
      pcs_idno_appr-go_approval->enqueue_appr_idno_or_wait( zif_enq_mode_c=>optimistic ).

    CATCH zcx_excep_cls.
      " IDNO is locked by external process. Please restart transaction
      PERFORM build_message USING pcs_idno_appr
                                  ps_ver_data
                                  ps_reg_hdr
                                  'MSG'
                                  'ZARENA_MSG'
                                  'E'
                                  '099'
                                  ''
                                  ''
                                  ''
                                  ''
                         CHANGING ls_log.
      IF 1 = 2. MESSAGE e099. ENDIF.
      APPEND ls_log TO pct_log[].

      "action not relevant for further processing here
      RETURN.
  ENDTRY.


  "---do additional actions upon approvals
  IF pv_status EQ zif_arn_approval_status=>gc_arn_appr_status_approved.

* This can trigger defaulting (if national fully approved)
* or article posting (if everything is approved)
    PERFORM appr_after_approval2 USING pv_called_from
                                       ps_ver_data
                                       ps_reg_hdr
                              CHANGING pcs_idno_appr
                                       pct_prd_version[]
                                       pct_reg_data[]
                                       pct_prod_data[]
                                       pct_reg_data_o[]
                                       pct_prod_data_o[]
                                       lv_exit
                                       pct_log[].

    IF lv_exit = abap_true.
      "action not relevant for further processing here
      RETURN.
    ENDIF.

  ENDIF.


  IF pv_called_from EQ co_appr_called_from_nat.
    REFRESH pcs_idno_appr-gt_appr_nat_approvals.
  ENDIF.
  IF pv_called_from EQ co_appr_called_from_reg.
    REFRESH pcs_idno_appr-gt_appr_reg_approvals.
  ENDIF.




ENDFORM.  " APPROVALS_UPDATE2
*&---------------------------------------------------------------------*
*&      Form  APPR_ACTION_CHECK_BEFORE
*&---------------------------------------------------------------------*
*
*----------------------------------------------------------------------*
FORM appr_action_check_before USING ps_idno_appr     TYPE ty_s_idno_appr
                                    ps_ver_data      TYPE ty_s_ver_data
                                    ps_reg_hdr       TYPE zarn_reg_hdr
                                    ps_reg_data      TYPE zsarn_reg_data
                                    ps_prod_data     TYPE zsarn_prod_data
                                    pv_called_from   TYPE char3
                                    pv_status        TYPE zarn_approval_status
                           CHANGING pcv_exit         TYPE xfeld
                                    pct_log          TYPE ztarn_approvals_mass_log.

  DATA: lv_new_version       TYPE flag,
        ls_log               TYPE zsarn_approvals_mass_log,
        lv_data_change       TYPE xfeld,
        lv_validation_errors TYPE flag,
        lt_val_output        TYPE zarn_t_rl_output,
        ls_val_output        TYPE zarn_s_rl_output,
        lt_idno_err          TYPE ztarn_idno_key.



  CLEAR pcv_exit.

  PERFORM check_for_new_version USING ps_idno_appr
                                      ps_ver_data
                             CHANGING lv_new_version.

  IF lv_new_version EQ abap_true.

    " A new version now exists so cannot approve
    PERFORM build_message USING ps_idno_appr
                                ps_ver_data
                                ps_reg_hdr
                                'MSG'
                                'ZARENA_MSG'
                                'E'
                                '046'
                                ''
                                ''
                                ''
                                ''
                       CHANGING ls_log.
    IF 1 = 2. MESSAGE e046. ENDIF.
    APPEND ls_log TO pct_log[].

    pcv_exit = abap_true.
    EXIT.
  ENDIF.

  IF ( pv_called_from EQ co_appr_called_from_nat
    AND ps_idno_appr-gv_appr_nat_approvals EQ abap_false )
    OR ( pv_called_from EQ co_appr_called_from_reg
    AND ps_idno_appr-gv_appr_reg_approvals EQ abap_false ).

    CASE pv_status.
      WHEN zif_arn_approval_status=>gc_arn_appr_status_approved.
        " No approvals to approve
        PERFORM build_message USING ps_idno_appr
                                    ps_ver_data
                                    ps_reg_hdr
                                    'MSG'
                                    'ZARENA_MSG'
                                    'E'
                                    '041'
                                    ''
                                    ''
                                    ''
                                    ''
                           CHANGING ls_log.
        IF 1 = 2. MESSAGE e041. ENDIF.
        APPEND ls_log TO pct_log[].

        pcv_exit = abap_true.
        EXIT.
      WHEN zif_arn_approval_status=>gc_arn_appr_status_rejected.
        " No approvals to reject
        PERFORM build_message USING ps_idno_appr
                                    ps_ver_data
                                    ps_reg_hdr
                                    'MSG'
                                    'ZARENA_MSG'
                                    'E'
                                    '042'
                                    ''
                                    ''
                                    ''
                                    ''
                           CHANGING ls_log.
        IF 1 = 2. MESSAGE e042. ENDIF.
        APPEND ls_log TO pct_log[].

        pcv_exit = abap_true.
        EXIT.
    ENDCASE.
  ENDIF.


*  IF pv_status EQ zif_arn_approval_status=>gc_arn_appr_status_approved
*  OR pv_status EQ zif_arn_approval_status=>gc_arn_appr_status_rejected.
*
*    CLEAR lv_data_change.
**    PERFORM check_reg_changes CHANGING Lv_data_change.
*    IF lv_data_change EQ abap_true.
*      " Data has been changed. Please save first before doing approvals.
*      PERFORM build_message USING ps_idno_appr
*                                  ps_ver_data
*                                  ps_reg_hdr
*                                  'MSG'
*                                  'ZARENA_MSG'
*                                  'E'
*                                  '049'
*                                  ''
*                                  ''
*                                  ''
*                                  ''
*                         CHANGING ls_log.
*      IF 1 = 2. MESSAGE e049. ENDIF.
*      APPEND ls_log TO pct_log[].
*
*      pcv_exit = abap_true.
*      EXIT.
*    ENDIF.


  IF pv_called_from EQ co_appr_called_from_reg
    AND  pv_status  EQ zif_arn_approval_status=>gc_arn_appr_status_approved.

    CLEAR: lv_validation_errors, lt_val_output[].
* Check Validation Rule Engine Messages
    PERFORM validation_check_report USING ps_reg_data
                                          ps_prod_data
                                 CHANGING lv_validation_errors
                                          lt_val_output[]
                                          lt_idno_err[].


    LOOP AT lt_val_output[] INTO ls_val_output.
      CLEAR ls_log.
      ls_log-node = ps_idno_appr-idno     && '/' &&
                    ps_idno_appr-chg_area && '/' &&
                    ps_idno_appr-chg_category.

      READ TABLE pct_log[] TRANSPORTING NO FIELDS
      WITH KEY validation_id = ls_val_output-validation_id
               table_name    = ls_val_output-table_name
               field_name    = ls_val_output-field_name
               idno          = ls_val_output-idno
               version       = ls_val_output-version
               key1          = ls_val_output-key1
               key2          = ls_val_output-key2
               key3          = ls_val_output-key3
               msgno         = ls_val_output-msgno
               msgty         = ls_val_output-msgty
               msgid         = ls_val_output-msgid.
      IF sy-subrc NE 0.
        MOVE-CORRESPONDING ls_val_output TO ls_log.
        APPEND ls_log TO pct_log[].
      ENDIF.
    ENDLOOP.

    IF lv_validation_errors NE abap_false.
*      " Validation errors occured so approval not saved
*      PERFORM build_message USING ps_idno_appr
*                                  ps_ver_data
*                                  ps_reg_hdr
*                                  'MSG'
*                                  'ZARENA_MSG'
*                                  'E'
*                                  '047'
*                                  ''
*                                  ''
*                                  ''
*                                  ''
*                         CHANGING ls_log.
*      IF 1 = 2. MESSAGE e047. ENDIF.
*      APPEND ls_log TO pct_log[].

      pcv_exit = abap_true.
      EXIT.
    ENDIF.

  ENDIF.

*  ENDIF.


ENDFORM.  " APPR_ACTION_CHECK_BEFORE
*&---------------------------------------------------------------------*
*&      Form  CHECK_FOR_NEW_VERSION
*&---------------------------------------------------------------------*
*
*----------------------------------------------------------------------*
FORM check_for_new_version USING ps_idno_appr          TYPE ty_s_idno_appr
                                 ps_ver_data           TYPE ty_s_ver_data
                        CHANGING pv_new_version_exists TYPE flag.


  DATA: ls_ver_status TYPE zarn_ver_status.

  pv_new_version_exists = abap_false.

  SELECT SINGLE *
    INTO ls_ver_status
    FROM zarn_ver_status
    WHERE idno        EQ ps_ver_data-idno
    AND   current_ver GT ps_ver_data-current_ver.
  IF sy-subrc EQ 0.
    pv_new_version_exists = abap_true.
  ENDIF.

ENDFORM.  " CHECK_FOR_NEW_VERSION
*&---------------------------------------------------------------------*
*&      Form  VALIDATION_CHECK_REPORT
*&---------------------------------------------------------------------*
* Check Validation Rule Engine Messages
*----------------------------------------------------------------------*
FORM validation_check_report USING ps_reg_data           TYPE zsarn_reg_data
                                   ps_prod_data          TYPE zsarn_prod_data
                          CHANGING pcv_validation_errors TYPE flag
                                   pct_val_output        TYPE zarn_t_rl_output
                                   pct_idno_err          TYPE ztarn_idno_key.

  DATA: lo_validation_engine TYPE REF TO zcl_arn_validation_engine,
        lo_excep             TYPE REF TO zcx_arn_validation_err.

  CLEAR: pcv_validation_errors, pct_val_output[], pct_idno_err[].

  TRY.

      CREATE OBJECT lo_validation_engine
        EXPORTING
          iv_event          = zcl_arn_validation_engine=>gc_event_arena_ui
          iv_bapiret_output = abap_true.

    CATCH zcx_arn_validation_err INTO lo_excep.
*   So approvals know that an error happened
      pcv_validation_errors = abap_true.

      MESSAGE i000(zarena_msg) WITH lo_excep->msgv1 lo_excep->msgv2
                                    lo_excep->msgv3 lo_excep->msgv4.
      EXIT.
  ENDTRY.


  TRY.
      CALL METHOD lo_validation_engine->validate_arn_single
        EXPORTING
          is_zsarn_prod_data          = ps_prod_data
          is_zsarn_reg_data           = ps_reg_data
          iv_refresh_mstrdata_buffers = abap_true
        IMPORTING
          et_output                   = pct_val_output[].
    CATCH zcx_arn_validation_err INTO lo_excep.
*   So approvals know that an error happened
      pcv_validation_errors = abap_true.

      MESSAGE i000(zarena_msg) WITH lo_excep->msgv1 lo_excep->msgv2
                                    lo_excep->msgv3 lo_excep->msgv4.
      EXIT.
  ENDTRY.


  LOOP AT pct_val_output TRANSPORTING NO FIELDS WHERE msgty EQ 'A' OR msgty EQ 'E'.
*   So approvals know that an error happened
    pcv_validation_errors = abap_true.
    APPEND ps_reg_data-idno TO pct_idno_err.
    EXIT.
  ENDLOOP.

ENDFORM.  " VALIDATION_CHECK_REPORT
*&---------------------------------------------------------------------*
*&      Form  APPR_ACTION_DATA_PREPARE2
*&---------------------------------------------------------------------*
* Prepare data required to perform the action
*----------------------------------------------------------------------*
FORM appr_action_data_prepare2 USING pv_called_from   TYPE char3
                                     pv_team_code     TYPE zarn_team_code
                                     pv_status        TYPE zarn_approval_status
                            CHANGING pcv_proc_team    TYPE zarn_team_code
                                     pct_approvals    TYPE ty_t_appr_output
                                     pct_appr_teams   TYPE zarn_t_teams
                                     pcs_idno_appr    TYPE ty_s_idno_appr
                                     pcv_rej_comment  TYPE string
                                     pcv_exit         TYPE xfeld.


  DATA: lv_authorized  TYPE flag.

  CLEAR pcv_exit.

  REFRESH pct_approvals.
  IF pv_called_from EQ co_appr_called_from_nat.
    pct_approvals[] = pcs_idno_appr-gt_appr_nat_approvals.
  ELSEIF pv_called_from EQ co_appr_called_from_reg.
    pct_approvals[] = pcs_idno_appr-gt_appr_reg_approvals.
  ENDIF.

  "check which team is ready for approvals
  DATA(lv_team_ready) = pcs_idno_appr-go_approval->get_team_ready_for_approval( ).
  IF lv_team_ready IS INITIAL.
    "there is no team ready for this matrix.
    pcv_exit = abap_true.
    RETURN.
  ENDIF.

  IF  pv_team_code IS NOT INITIAL.
    "if clicking on particular cell then take
    "processing team from there
    pcv_proc_team = pv_team_code.
  ELSE.
    "PV_TEAM_CODE will be initial in case of pressing button APPROVE / REJECT ALL
    "if clicking on "approve/reject all"
    "take processing team as the team which is currently ready
    pcv_proc_team = lv_team_ready.
  ENDIF.



  CASE  pv_status.
    WHEN zif_arn_approval_status=>gc_arn_appr_status_approved
      OR zif_arn_approval_status=>gc_arn_appr_status_rejected.

      "in case of approving or rejecting only team which is currently ready
      "for approving can change something


      IF pv_team_code IS NOT INITIAL
        AND pv_team_code NE lv_team_ready.

        "trying to approve a team column which is not ready for approval yet
        pcv_exit = abap_true.
        RETURN.
      ENDIF.


    WHEN zif_arn_approval_status=>gc_arn_appr_status_reset.

      "in case of reset it is possible to reset
      "a) current team
      "b) previous team if current team hasn't started yet

      IF pv_team_code IS NOT INITIAL
        AND pv_team_code NE lv_team_ready.

        "if trying to reset something for different team than the current one
        "it is possible only for previous team and
        "if current team hasn't started working yet

        IF pcs_idno_appr-go_approval->has_team_started_approving( lv_team_ready ) = abap_true
          OR pv_team_code NE pcs_idno_appr-go_approval->get_previous_team( lv_team_ready ).

          "team already started approving, can't reset anything that doesn't belong
          "to this team
          pcv_exit = abap_true.
          RETURN.

        ENDIF.
      ENDIF.
  ENDCASE.

  IF pcs_idno_appr-go_approval->is_authorised_for_team( pcv_proc_team )
    NE abap_true.

    "trying to change something I am not authorised to
    pcv_exit = abap_true.
    RETURN.
  ENDIF.


  CLEAR pcs_idno_appr-gv_appr_result.
  IF pv_status EQ zif_arn_approval_status=>gc_arn_appr_status_approved.

*   Do not want mandatory comment on approval
*   PERFORM appr_approve.
    pcs_idno_appr-gv_appr_result = co_appr_result_approved.

  ELSEIF pv_status EQ zif_arn_approval_status=>gc_arn_appr_status_rejected.

    CLEAR pcs_idno_appr-gv_appr_comment.
* REJECTION COMMENT
    PERFORM appr_reject CHANGING pcs_idno_appr.
    pcv_rej_comment = pcs_idno_appr-gv_appr_comment.

  ELSEIF pv_status EQ zif_arn_approval_status=>gc_arn_appr_status_reset.

    CLEAR pcs_idno_appr-gv_appr_comment.
    pcs_idno_appr-gv_appr_result = co_appr_result_reset.

  ENDIF.


  CASE pcs_idno_appr-gv_appr_result.
    WHEN co_appr_result_approved OR co_appr_result_rejected
      OR co_appr_result_reset.
      "if one of those actions- OK, proceed


    WHEN OTHERS.
      "if other actions - don't do anything
      pcv_exit = abap_true.
      RETURN.
  ENDCASE.


ENDFORM.  " APPR_ACTION_DATA_PREPARE2
*&---------------------------------------------------------------------*
*&      Form  APPR_ACTION_POST2
*&---------------------------------------------------------------------*
* All the determination logic and change ZARN_APPROVAL
*----------------------------------------------------------------------*
FORM appr_action_post2 USING pv_called_from     TYPE char3
                             ps_ver_data        TYPE ty_s_ver_data
                             ps_reg_hdr         TYPE zarn_reg_hdr
                             pit_approvals      TYPE ty_t_appr_output
                             pv_change_category TYPE zarn_chg_category
                             pit_approval_teams TYPE zarn_t_teams
                             piv_rej_comment    TYPE string
                             piv_team           TYPE zarn_team_code
                    CHANGING pcs_idno_appr      TYPE ty_s_idno_appr
                             pcv_exit           TYPE xfeld
                             pct_log            TYPE ztarn_approvals_mass_log.



  DATA : lt_approval_new   TYPE zarn_t_approval,
         lt_appr_hist      TYPE zarn_t_appr_hist,
         lv_status_reason  TYPE zarn_e_appr_status_reason,
         lt_after_appr_act TYPE zarn_t_after_appr_action,
         ls_log            TYPE zsarn_approvals_mass_log.

  "action on approval automatically determines
  "statuses on following approvals

  TRY.

      "action posted, continue with optimistic lock
      pcs_idno_appr-go_approval->enqueue_appr_idno_or_wait(
                                    iv_idno = pcs_idno_appr-idno
                                    iv_mode = zif_enq_mode_c=>optimistic ).


      CASE pcs_idno_appr-gv_appr_result.

        WHEN co_appr_result_approved.

          pcs_idno_appr-go_approval->action_approve(
                            EXPORTING
                              iv_team_code    = piv_team
                              iv_chg_category = pv_change_category
                            IMPORTING
                              et_zarn_approval  = lt_approval_new
                              et_zarn_appr_hist = lt_appr_hist ).
          "et_after_appr_actions = lt_after_appr_act ).

        WHEN co_appr_result_rejected.

          lv_status_reason = pcs_idno_appr-gv_appr_rejection_reason.

          pcs_idno_appr-go_approval->action_reject(
                             EXPORTING
                               iv_team_code     = piv_team
                               iv_chg_category  = pv_change_category
                               iv_comment       = piv_rej_comment
                               iv_status_reason = lv_status_reason
                               iv_approval_area = pv_called_from
                             IMPORTING
                               et_zarn_approval  = lt_approval_new
                               et_zarn_appr_hist = lt_appr_hist ).

        WHEN co_appr_result_reset.

          pcs_idno_appr-go_approval->action_reset(
                            EXPORTING
                              iv_team_code    = piv_team
                              iv_chg_category = pv_change_category
                            IMPORTING
                              et_zarn_approval  = lt_approval_new
                              et_zarn_appr_hist = lt_appr_hist ).

      ENDCASE.

* Notifcation exceptions. We're fine to continue after this. Just notify
    CATCH zcx_arn_notif_err INTO DATA(lo_ex).
      MESSAGE lo_ex TYPE 'I' DISPLAY LIKE 'W'.

    CATCH zcx_excep_cls.
      " IDNO is locked by external process. Please restart transaction
      PERFORM build_message USING pcs_idno_appr
                                  ps_ver_data
                                  ps_reg_hdr
                                  'MSG'
                                  'ZARENA_MSG'
                                  'E'
                                  '099'
                                  ''
                                  ''
                                  ''
                                  ''
                         CHANGING ls_log.
      IF 1 = 2. MESSAGE e099. ENDIF.
      APPEND ls_log TO pct_log[].

      pcv_exit = abap_true.
  ENDTRY.

  pcs_idno_appr-go_approval->post_approvals( EXPORTING it_approval  = lt_approval_new
                                                       it_appr_hist = lt_appr_hist ).







ENDFORM.  " APPR_ACTION_POST2
*&---------------------------------------------------------------------*
*&      Form  APPR_ACTION_STAT_UPDATE
*&---------------------------------------------------------------------*
* Update statuses on header (regional/national)
* this is only for WIP / REJ status (work in progress)
*----------------------------------------------------------------------*
FORM appr_action_stat_update USING pv_called_from   TYPE char3
                                   pv_status        TYPE zarn_approval_status
                                   ps_ver_data      TYPE ty_s_ver_data
                          CHANGING pct_prd_version  TYPE ty_t_prd_version
                                   pct_reg_data     TYPE ty_t_reg_data.


  CASE pv_called_from .
    WHEN co_appr_called_from_nat.


      CASE pv_status.

        WHEN zif_arn_approval_status=>gc_arn_appr_status_approved.
          PERFORM update_nat_status USING ps_ver_data-idno
                                          ps_ver_data-current_ver
                                          co_status_wip
                                 CHANGING pct_prd_version[].

        WHEN zif_arn_approval_status=>gc_arn_appr_status_rejected.
          PERFORM update_nat_status USING ps_ver_data-idno
                                          ps_ver_data-current_ver
                                          co_status_rej
                                 CHANGING pct_prd_version[].

        WHEN zif_arn_approval_status=>gc_arn_appr_status_reset.
          PERFORM update_nat_status USING ps_ver_data-idno
                                          ps_ver_data-current_ver
                                          co_status_wip
                                 CHANGING pct_prd_version[].

      ENDCASE.

    WHEN co_appr_called_from_reg.


      CASE pv_status.

        WHEN zif_arn_approval_status=>gc_arn_appr_status_approved.
          PERFORM update_reg_status USING ps_ver_data-idno
                                          co_status_wip
                                          CHANGING pct_reg_data[].

        WHEN zif_arn_approval_status=>gc_arn_appr_status_rejected.
          PERFORM update_reg_status USING ps_ver_data-idno
                                          co_status_rej
                                 CHANGING pct_reg_data[].

        WHEN zif_arn_approval_status=>gc_arn_appr_status_reset.
          PERFORM update_reg_status USING ps_ver_data-idno
                                          co_status_wip
                                 CHANGING pct_reg_data[].

      ENDCASE.

  ENDCASE.



ENDFORM.

*&---------------------------------------------------------------------*
*&      Form  UPDATE_NAT_STATUS
*&---------------------------------------------------------------------*
FORM update_nat_status USING pv_idno TYPE zarn_idno
                             pv_version TYPE zarn_version
                             pv_status TYPE char3
                    CHANGING pct_prd_version  TYPE ty_t_prd_version.

  DATA: ls_prd_version LIKE LINE OF gt_prd_version.

  FIELD-SYMBOLS: <ls_prd_version> LIKE LINE OF gt_prd_version.

  READ TABLE pct_prd_version ASSIGNING <ls_prd_version>
  WITH KEY idno = pv_idno
           version = pv_version.
  IF sy-subrc EQ 0.
    IF <ls_prd_version>-version_status NE pv_status.

      ls_prd_version = <ls_prd_version>.
      <ls_prd_version>-version_status = pv_status.

      PERFORM update_nat_db_status USING ls_prd_version <ls_prd_version>.

*      gs_worklist-version_status      = <ls_prd_version>-version_status.
*      zarn_prd_version-version_status = <ls_prd_version>-version_status.
    ENDIF.
  ENDIF.

ENDFORM.  " UPDATE_NAT_STATUS
*&---------------------------------------------------------------------*
*&      Form  UPDATE_REG_STATUS
*&---------------------------------------------------------------------*
FORM update_reg_status USING pv_idno      TYPE zarn_idno
                             pv_status    TYPE char3
                    CHANGING pct_reg_data TYPE ty_t_reg_data.

  DATA: ls_reg_hdr TYPE zarn_reg_hdr.

  FIELD-SYMBOLS: <ls_reg_data> LIKE LINE OF gt_reg_data,
                 <ls_reg_hdr>  LIKE LINE OF <ls_reg_data>-zarn_reg_hdr.

  READ TABLE pct_reg_data ASSIGNING <ls_reg_data> WITH KEY idno = pv_idno.
  IF sy-subrc EQ 0.
    READ TABLE <ls_reg_data>-zarn_reg_hdr ASSIGNING <ls_reg_hdr> WITH KEY idno = pv_idno.
    IF sy-subrc EQ 0.
      IF <ls_reg_hdr>-status NE pv_status.
        ls_reg_hdr = <ls_reg_hdr>.
        <ls_reg_hdr>-status = pv_status.

        PERFORM update_reg_db_status USING ls_reg_hdr <ls_reg_hdr>.

*        zarn_reg_hdr-status = pv_status.
      ENDIF.
    ENDIF.
  ENDIF.

ENDFORM.  " UPDATE_REG_STATUS
*&---------------------------------------------------------------------*
*&      Form  APPR_AFTER_APPROVAL2
*&---------------------------------------------------------------------*
* This can trigger defaulting (if national fully approved)
* or article posting (if everything is approved)
*----------------------------------------------------------------------*
FORM appr_after_approval2 USING pv_called_from  TYPE char3
                                ps_ver_data     TYPE ty_s_ver_data
                                ps_reg_hdr      TYPE zarn_reg_hdr
                       CHANGING pcs_idno_appr   TYPE ty_s_idno_appr
                                pct_prd_version TYPE ty_t_prd_version
                                pct_reg_data    TYPE ty_t_reg_data
                                pct_prod_data   TYPE ty_t_prod_data
                                pct_reg_data_o  TYPE ty_t_reg_data
                                pct_prod_data_o TYPE ty_t_prod_data
                                pcv_exit        TYPE xfeld
                                pct_log         TYPE ztarn_approvals_mass_log.


  DATA :  lv_nat_approved        TYPE flag,
          lv_reg_approved        TYPE flag,
          lv_carry_over_happened TYPE flag,
          ls_log                 TYPE zsarn_approvals_mass_log.

  CLEAR lv_carry_over_happened.

  lv_nat_approved = pcs_idno_appr-go_approval->is_fully_approved(
                 iv_approval_area = zcl_arn_approval_backend=>gc_appr_area_national
                 iv_no_records_as_approved = abap_true ).

  lv_reg_approved = pcs_idno_appr-go_approval->is_fully_approved(
                        zcl_arn_approval_backend=>gc_appr_area_regional ).


  "if national fully approved, do the regional defaulting
  IF  lv_nat_approved EQ abap_true
  AND pv_called_from  EQ co_appr_called_from_nat.

* Do the defaulting ( = national to regional carry over)
    PERFORM appr_do_reg_defaulting USING ps_ver_data
                                         ps_reg_hdr
                                CHANGING pcs_idno_appr
                                         pct_reg_data[]
                                         pct_prod_data[]
                                         pct_log[].

* Automatically save data
* this can create additional regional approvals
    PERFORM process_regional_data USING space
                                        abap_true
                                        ps_ver_data
                                        ps_reg_hdr
                               CHANGING pcs_idno_appr
                                        pct_reg_data[]
                                        pct_prod_data[]
                                        pct_reg_data_o[]
                                        pct_prod_data_o[]
                                        pcv_exit
                                        pct_log[].

    IF pcv_exit = abap_true.
      "action not relevant for further processing here
      EXIT.
    ENDIF.

    lv_carry_over_happened = abap_true.

    " National data has been carried over to Regional and saved
    PERFORM build_message USING pcs_idno_appr
                                ps_ver_data
                                ps_reg_hdr
                                'MSG'
                                'ZARENA_MSG'
                                'S'
                                '079'
                                ''
                                ''
                                ''
                                ''
                       CHANGING ls_log.
    IF 1 = 2. MESSAGE s079. ENDIF.
    APPEND ls_log TO pct_log[].

  ENDIF.

  "if fully approved, update header statuses to approved
  IF  lv_nat_approved EQ abap_true.
    PERFORM update_nat_status USING ps_ver_data-idno
                                    ps_ver_data-current_ver
                                    co_status_apr
                           CHANGING pct_prd_version[].
  ENDIF.
  IF lv_reg_approved EQ abap_true.
    PERFORM update_reg_status USING ps_ver_data-idno
                                    co_status_apr
                           CHANGING pct_reg_data[].
  ENDIF.

  COMMIT WORK AND WAIT.
  TRY.
      pcs_idno_appr-go_approval->enqueue_appr_idno_or_wait( zif_enq_mode_c=>optimistic ).
    CATCH zcx_excep_cls.
      " IDNO is locked by external process. Please restart transaction
      PERFORM build_message USING pcs_idno_appr
                                  ps_ver_data
                                  ps_reg_hdr
                                  'MSG'
                                  'ZARENA_MSG'
                                  'E'
                                  '099'
                                  ''
                                  ''
                                  ''
                                  ''
                         CHANGING ls_log.
      IF 1 = 2. MESSAGE e099. ENDIF.
      APPEND ls_log TO pct_log[].

      pcv_exit = abap_true.
      EXIT.
  ENDTRY.




  IF lv_carry_over_happened = abap_true.

    "if carryover happened we need to check the recent approval data
    "before we continue to posting - carryover could have created
    "a new regional change categories which are not approved yet
    pcs_idno_appr-go_approval->load_recent_approval_data( ).

    lv_nat_approved = pcs_idno_appr-go_approval->is_fully_approved( iv_approval_area = zcl_arn_approval_backend=>gc_appr_area_national
                                                      iv_no_records_as_approved = abap_true ).
    lv_reg_approved = pcs_idno_appr-go_approval->is_fully_approved( zcl_arn_approval_backend=>gc_appr_area_regional ).

  ENDIF.

  IF  lv_nat_approved EQ abap_true
  AND lv_reg_approved EQ abap_true.
    "if both national and regional is fully approved
    "call the article posting

* Post Article
    PERFORM appr_post_article USING ps_ver_data
                                    ps_reg_hdr
                           CHANGING pcs_idno_appr
                                    pct_log[].

    TRY.
        pcs_idno_appr-go_approval->enqueue_appr_idno_or_wait( zif_enq_mode_c=>optimistic ).
      CATCH zcx_excep_cls.
        " IDNO is locked by external process. Please restart transaction
        PERFORM build_message USING pcs_idno_appr
                                    ps_ver_data
                                    ps_reg_hdr
                                    'MSG'
                                    'ZARENA_MSG'
                                    'E'
                                    '099'
                                    ''
                                    ''
                                    ''
                                    ''
                           CHANGING ls_log.
        IF 1 = 2. MESSAGE e099. ENDIF.
        APPEND ls_log TO pct_log[].

        pcv_exit = abap_true.
        EXIT.
    ENDTRY.
  ENDIF.

ENDFORM.  " APPR_AFTER_APPROVAL2
*&---------------------------------------------------------------------*
*&      Form  APPR_DO_REG_DEFAULTING
*&---------------------------------------------------------------------*
* Do the defaulting ( = national to regional carry over)
*----------------------------------------------------------------------*
FORM appr_do_reg_defaulting USING ps_ver_data   TYPE ty_s_ver_data
                                  ps_reg_hdr     TYPE zarn_reg_hdr
                         CHANGING pcs_idno_appr TYPE ty_s_idno_appr
                                  pct_reg_data  TYPE ty_t_reg_data
                                  pct_prod_data TYPE ty_t_prod_data
                                  pct_log       TYPE ztarn_approvals_mass_log.


  DATA: lr_gui_load           TYPE REF TO zcl_arn_gui_load,
        ls_reg_data_def       TYPE zsarn_reg_data,
        ls_log                TYPE zsarn_approvals_mass_log,

        lv_no_uom_avail       TYPE flag,
        lv_no_uom_avail_ean   TYPE flag,
        lv_dim_overflow_error TYPE flag.

  FIELD-SYMBOLS: <ls_reg_data>  TYPE zsarn_reg_data,
                 <ls_prod_data> TYPE zsarn_prod_data.

  CLEAR: ls_reg_data_def.

  READ TABLE pct_prod_data ASSIGNING <ls_prod_data>
      WITH KEY idno    =  ps_ver_data-idno
               version =  ps_ver_data-current_ver.
  IF sy-subrc EQ 0.
    READ TABLE pct_reg_data ASSIGNING <ls_reg_data>
         WITH KEY idno =  ps_ver_data-idno.
    IF sy-subrc EQ 0.

      CREATE OBJECT lr_gui_load.

      CLEAR: lv_no_uom_avail, lv_no_uom_avail_ean, lv_dim_overflow_error.
      CALL METHOD lr_gui_load->set_regional_default
        EXPORTING
          is_reg_data           = <ls_reg_data>
          is_prod_data          = <ls_prod_data>
          iv_reg_only           = space
        IMPORTING
          es_reg_data_def       = ls_reg_data_def
          ev_no_uom_avail       = lv_no_uom_avail
          ev_no_uom_avail_ean   = lv_no_uom_avail_ean
          ev_dim_overflow_error = lv_dim_overflow_error.

      <ls_reg_data> = ls_reg_data_def.


      IF lv_no_uom_avail = abap_true OR lv_no_uom_avail_ean = abap_true.
        " No more UOMs available to be defaulted. Kindly Contact IMS
        PERFORM build_message USING pcs_idno_appr
                                    ps_ver_data
                                    ps_reg_hdr
                                    'MSG'
                                    'ZARENA_MSG'
                                    'E'
                                    '085'
                                    ''
                                    ''
                                    ''
                                    ''
                           CHANGING ls_log.
        IF 1 = 2. MESSAGE e085. ENDIF.
        APPEND ls_log TO pct_log[].
      ENDIF.

      IF lv_dim_overflow_error = abap_true.
        " Exceeds the maximum length for Dim. and Wt. values, proceed manually
        PERFORM build_message USING pcs_idno_appr
                                    ps_ver_data
                                    ps_reg_hdr
                                    'MSG'
                                    'ZARENA_MSG'
                                    'E'
                                    '087'
                                    ''
                                    ''
                                    ''
                                    ''
                           CHANGING ls_log.
        IF 1 = 2. MESSAGE e087. ENDIF.
        APPEND ls_log TO pct_log[].
      ENDIF.

    ENDIF.
  ENDIF.



ENDFORM.  " APPR_DO_REG_DEFAULTING
*&---------------------------------------------------------------------*
*&      Form  PROCESS_REGIONAL_DATA
*&---------------------------------------------------------------------*
FORM process_regional_data USING pv_display      TYPE flag
                                 pv_save         TYPE flag
                                 ps_ver_data     TYPE ty_s_ver_data
                                 ps_reg_hdr      TYPE zarn_reg_hdr
                        CHANGING pcs_idno_appr   TYPE ty_s_idno_appr
                                 pct_reg_data    TYPE ty_t_reg_data
                                 pct_prod_data   TYPE ty_t_prod_data
                                 pct_reg_data_o  TYPE ty_t_reg_data
                                 pct_prod_data_o TYPE ty_t_prod_data
                                 pcv_exit        TYPE xfeld
                                 pct_log         TYPE ztarn_approvals_mass_log.


  DATA:
    ls_reg_data          TYPE zsarn_reg_data,
    lt_reg_data          TYPE ztarn_reg_data,
    lt_reg_data_del      TYPE ztarn_reg_data,
    lt_val_output        TYPE zarn_t_rl_output,
    ls_val_output        TYPE zarn_s_rl_output,
    lt_idno_err          TYPE ztarn_idno_key,
    ls_log               TYPE zsarn_approvals_mass_log,

    lv_validation_errors TYPE flag.



  FIELD-SYMBOLS: <ls_reg_data>    TYPE zsarn_reg_data,
                 <ls_prod_data>   TYPE zsarn_prod_data,
                 <ls_reg_data_o>  TYPE zsarn_reg_data,
                 <ls_prod_data_o> TYPE zsarn_prod_data.

  READ TABLE pct_prod_data[] ASSIGNING <ls_prod_data>
      WITH KEY idno    =  ps_ver_data-idno
               version =  ps_ver_data-current_ver.
  IF sy-subrc NE 0.
    EXIT.
  ENDIF.

  READ TABLE pct_reg_data[] ASSIGNING <ls_reg_data>
       WITH KEY idno =  ps_ver_data-idno.
  IF sy-subrc NE 0.
    EXIT.
  ENDIF.

* old data
  READ TABLE pct_prod_data_o[] ASSIGNING <ls_prod_data_o>
      WITH KEY idno    =  ps_ver_data-idno
               version =  ps_ver_data-current_ver.
  IF sy-subrc NE 0.
    EXIT.
  ENDIF.

* old data
  READ TABLE pct_reg_data_o[] ASSIGNING <ls_reg_data_o>
       WITH KEY idno =  ps_ver_data-idno.
  IF sy-subrc NE 0.
    EXIT.
  ENDIF.


  IF pv_display IS INITIAL.
* Default NON SAP fields
    PERFORM default_non_sap_fields CHANGING <ls_reg_data>.

    PERFORM default_brand_id_111 CHANGING <ls_reg_data>.
  ENDIF.


  APPEND <ls_reg_data>   TO lt_reg_data[].
  APPEND <ls_reg_data_o> TO lt_reg_data_del[].

  CLEAR: lv_validation_errors, lt_val_output[], lt_idno_err[].
* Check Validation Rule Engine Messages
  PERFORM validation_check_report USING <ls_reg_data>
                                        <ls_prod_data_o>
                               CHANGING lv_validation_errors
                                        lt_val_output[]
                                        lt_idno_err[].

  LOOP AT lt_val_output[] INTO ls_val_output.
    CLEAR ls_log.
    ls_log-node = pcs_idno_appr-idno     && '/' &&
                  pcs_idno_appr-chg_area && '/' &&
                  pcs_idno_appr-chg_category.

    READ TABLE pct_log[] TRANSPORTING NO FIELDS
    WITH KEY validation_id = ls_val_output-validation_id
             table_name    = ls_val_output-table_name
             field_name    = ls_val_output-field_name
             idno          = ls_val_output-idno
             version       = ls_val_output-version
             key1          = ls_val_output-key1
             key2          = ls_val_output-key2
             key3          = ls_val_output-key3
             msgno         = ls_val_output-msgno
             msgty         = ls_val_output-msgty
             msgid         = ls_val_output-msgid.
    IF sy-subrc NE 0.
      MOVE-CORRESPONDING ls_val_output TO ls_log.
      APPEND ls_log TO pct_log[].
    ENDIF.
  ENDLOOP.





  IF pv_save IS NOT INITIAL.
    CALL FUNCTION 'ZARN_REGIONAL_DB_UPDATE'   " Update Module
      EXPORTING
        it_reg_data       = lt_reg_data[]
        it_reg_data_old   = lt_reg_data_del[]
        it_idno_val_error = lt_idno_err[].

    "we want explicit commit to release exclusive approval locks
    COMMIT WORK AND WAIT.

    "and continue with optimistic lock again
    IF pcs_idno_appr-go_approval IS BOUND.
      TRY.
          pcs_idno_appr-go_approval->enqueue_appr_idno_or_wait( zif_enq_mode_c=>optimistic ).
        CATCH zcx_excep_cls.
          " IDNO is locked by external process. Please restart transaction
          PERFORM build_message USING pcs_idno_appr
                                      ps_ver_data
                                      ps_reg_hdr
                                      'MSG'
                                      'ZARENA_MSG'
                                      'E'
                                      '099'
                                      ''
                                      ''
                                      ''
                                      ''
                             CHANGING ls_log.
          IF 1 = 2. MESSAGE e099. ENDIF.
          APPEND ls_log TO pct_log[].

          pcv_exit = abap_true.
          EXIT.
      ENDTRY.
    ENDIF.


*   assign new data as old for next change if any
    <ls_reg_data_o> =  <ls_reg_data>.

  ENDIF.





ENDFORM.  " PROCESS_REGIONAL_DATA
*&---------------------------------------------------------------------*
*&      Form  DEFAULT_NON_SAP_FIELDS
*&---------------------------------------------------------------------*
* Default NON SAP fields
*----------------------------------------------------------------------*
FORM default_non_sap_fields CHANGING pcs_reg_data  TYPE ty_s_reg_data.

  FIELD-SYMBOLS: <ls_reg_hdr> TYPE zarn_reg_hdr.

  READ TABLE pcs_reg_data-zarn_reg_hdr ASSIGNING <ls_reg_hdr>
  WITH KEY idno = pcs_reg_data-idno.
  IF sy-subrc NE 0.
    EXIT.
  ENDIF.

* Default Legacy fields
  PERFORM default_legacy_uoms_113 CHANGING pcs_reg_data
                                           <ls_reg_hdr>.

* Default Host Product Indicator
  PERFORM default_host_prod_ind_113 USING pcs_reg_data-zarn_reg_banner[]
                                 CHANGING <ls_reg_hdr>.


ENDFORM.  " DEFAULT_NON_SAP_FIELDS
*&---------------------------------------------------------------------*
*&      Form  DEFAULT_LEGACY_UOMS_113
*&---------------------------------------------------------------------*
* Default Legacy fields
*----------------------------------------------------------------------*
FORM default_legacy_uoms_113 CHANGING pcs_reg_data TYPE ty_s_reg_data
                                      pcs_reg_hdr  TYPE zarn_reg_hdr.

* Default Legacy fields
  CALL FUNCTION 'ZARN_DETERMINE_NONSAP_UOMS'
    EXPORTING
      iv_idno    = pcs_reg_data-idno
      is_reg_hdr = pcs_reg_hdr
*     it_nsap_leg_uom =
      it_reg_uom = pcs_reg_data-zarn_reg_uom[]
    IMPORTING
      es_reg_hdr = pcs_reg_hdr
    EXCEPTIONS
      input_reqd = 1
      OTHERS     = 2.




ENDFORM.  " DEFAULT_LEGACY_UOMS_113
*&---------------------------------------------------------------------*
*&      Form  DEFAULT_HOST_PROD_IND_113
*&---------------------------------------------------------------------*
* Default Host Product Indicator
*----------------------------------------------------------------------*
FORM default_host_prod_ind_113 USING pt_reg_banner TYPE ztarn_reg_banner
                            CHANGING pcs_reg_hdr   TYPE zarn_reg_hdr.


  DATA: lt_zmd_host_sap_itab TYPE ztmd_host_sap,
        ls_reg_banner        TYPE zarn_reg_banner,
        lv_host_product      TYPE zarn_host_product.

* If any Host Product exist then set host indicator as true
  IF pcs_reg_hdr-matnr IS NOT INITIAL.
    SELECT * FROM zmd_host_sap INTO TABLE lt_zmd_host_sap_itab[]
      WHERE matnr = pcs_reg_hdr-matnr.
  ENDIF.

  IF lt_zmd_host_sap_itab[] IS NOT INITIAL.
    pcs_reg_hdr-host_product = abap_true.
    EXIT.
  ENDIF.


  CLEAR lv_host_product.

  "process host data only IF
  "a) 1000 with Category manager exists OR
  "b) 4000/5000/6000 with Category manager and Assort. Grade exist
  CLEAR ls_reg_banner.
  LOOP AT pt_reg_banner[] INTO ls_reg_banner
    WHERE idno = pcs_reg_hdr-idno.

    CASE ls_reg_banner-banner.
      WHEN zcl_constants=>gc_banner_1000.
        IF ls_reg_banner-zzcatman IS NOT INITIAL AND pcs_reg_hdr-zz_uni_dc EQ abap_true.
          "if exists 1000 with Catman and UNI DC flag = X, proceed
          lv_host_product = abap_true.
          EXIT.
        ENDIF.
      WHEN  zcl_constants=>gc_banner_4000
         OR zcl_constants=>gc_banner_5000
         OR zcl_constants=>gc_banner_6000.
        "if exists Retail Banner with Catman and Assortment (other than 'OR'), proceed
        IF ls_reg_banner-zzcatman IS NOT INITIAL     AND
           ls_reg_banner-sstuf IS NOT INITIAL AND
           ls_reg_banner-sstuf NE 'OR'.
          lv_host_product = abap_true.
          EXIT.
        ENDIF.
    ENDCASE.

  ENDLOOP.  " LOOP AT gt_zarn_reg_banner[] INTO ls_reg_banner



  pcs_reg_hdr-host_product = lv_host_product.


ENDFORM.  " DEFAULT_HOST_PROD_IND_113
*&---------------------------------------------------------------------*
*&      Form  DEFAULT_BRAND_ID_111
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
FORM default_brand_id_111 CHANGING pcs_reg_data  TYPE ty_s_reg_data.


  FIELD-SYMBOLS: <ls_reg_hdr> TYPE zarn_reg_hdr.

  READ TABLE pcs_reg_data-zarn_reg_hdr ASSIGNING <ls_reg_hdr>
  WITH KEY idno = pcs_reg_data-idno.
  IF sy-subrc NE 0.
    EXIT.
  ENDIF.

  IF <ls_reg_hdr>-idno     IS NOT INITIAL AND
     <ls_reg_hdr>-brand_id IS INITIAL.
    <ls_reg_hdr>-brand_id = zcl_constants=>gc_brand_id_default.    " 'OTHM'.
  ENDIF.


ENDFORM.  " DEFAULT_BRAND_ID_111
*&---------------------------------------------------------------------*
*&      Form  APPR_POST_ARTICLE
*&---------------------------------------------------------------------*
* Post Article
*----------------------------------------------------------------------*
FORM appr_post_article  USING    ps_ver_data   TYPE ty_s_ver_data
                                 ps_reg_hdr    TYPE zarn_reg_hdr
                        CHANGING pcs_idno_appr TYPE ty_s_idno_appr
                                 pct_log       TYPE ztarn_approvals_mass_log.


  DATA: lt_key        TYPE ztarn_key,
        ls_key        LIKE LINE OF lt_key,
        lr_status     TYPE ztarn_status_range,
        lrs_status    LIKE LINE OF lr_status,
        lt_message    TYPE bal_t_msg,
        ls_message    TYPE bal_s_msg,
        lv_post_dest  TYPE rfcdes-rfcdest,
        lv_own_logsys TYPE tbdls-logsys,
        ls_tvarvc     TYPE tvarvc,
        lv_rfcchk     TYPE boolean,
        ls_log        TYPE zsarn_approvals_mass_log.

  REFRESH: lt_key,
           lr_status,
           lt_message.
  CLEAR ls_key.
  ls_key-idno    = ps_ver_data-idno.
  ls_key-version = ps_ver_data-current_ver.
  APPEND ls_key TO lt_key.

  REFRESH lr_status.
  SELECT sign opti AS option low high
    INTO TABLE lr_status
    FROM tvarvc
    WHERE name EQ 'ZARN_ARTICLE_UPDATE_STATUS'.
  IF lr_status[] IS INITIAL.
    CLEAR lrs_status.
    lrs_status-sign   = 'I'.
    lrs_status-option = 'EQ'.
    lrs_status-low    = 'APR'.
    APPEND lrs_status TO lr_status.
  ENDIF.

  CLEAR: lv_post_dest,
         ls_tvarvc.
  SELECT SINGLE *
    INTO ls_tvarvc
    FROM tvarvc
    WHERE name EQ 'ZARN_ARTICLE_POST_LOGSYS'
    AND   type EQ 'P'.
  IF sy-subrc EQ 0 AND ls_tvarvc-low IS NOT INITIAL.
    lv_post_dest = ls_tvarvc-low.

    CALL FUNCTION 'CHECK_RFC_DESTINATION'
      EXPORTING
        i_destination              = lv_post_dest
      IMPORTING
        e_user_password_incomplete = lv_rfcchk.
    IF lv_rfcchk NE space.
      CLEAR lv_post_dest.
    ENDIF.
  ENDIF.

  IF lv_post_dest IS INITIAL.
    CALL FUNCTION 'OWN_LOGICAL_SYSTEM_GET'
      IMPORTING
        own_logical_system             = lv_own_logsys
      EXCEPTIONS
        own_logical_system_not_defined = 1
        OTHERS                         = 2.
    lv_post_dest = lv_own_logsys.
  ENDIF.

  CALL FUNCTION 'ZARN_ARTICLE_POSTING'
    DESTINATION lv_post_dest
    EXPORTING
      it_key           = lt_key
      ir_status        = lr_status
      iv_batch         = ' '
      iv_display_log   = ' '
      iv_no_change_doc = ' '
    IMPORTING
      et_message       = lt_message.

  LOOP AT lt_message[] INTO ls_message.

    PERFORM build_message USING pcs_idno_appr
                                ps_ver_data
                                ps_reg_hdr
                                'POST'
                                ls_message-msgid
                                ls_message-msgty
                                ls_message-msgno
                                ls_message-msgv1
                                ls_message-msgv2
                                ls_message-msgv3
                                ls_message-msgv4
                       CHANGING ls_log.

    ls_log-node = pcs_idno_appr-idno     && '/' &&
                  pcs_idno_appr-chg_area && '/' &&
                  pcs_idno_appr-chg_category.

    APPEND ls_log TO pct_log[].
  ENDLOOP.


ENDFORM.  " APPR_POST_ARTICLE
*&---------------------------------------------------------------------*
*&      Form  ACT_MASS_APPROVAL
*&---------------------------------------------------------------------*
* Action - Mass approval
*----------------------------------------------------------------------*
FORM act_mass_approval USING po_alv_tree      TYPE REF TO cl_gui_alv_tree
                             pt_output        TYPE ztarn_approvals_mass_tree
                             pt_ver_data      TYPE ty_t_ver_data
                             pt_reg_hdr       TYPE ty_t_reg_hdr
                    CHANGING pct_idno_appr    TYPE ty_t_idno_appr
                             pct_log          TYPE ztarn_approvals_mass_log
                             pct_prd_version  TYPE ty_t_prd_version
                             pct_reg_data     TYPE ty_t_reg_data
                             pct_prod_data    TYPE ty_t_prod_data
                             pct_reg_data_o   TYPE ty_t_reg_data
                             pct_prod_data_o  TYPE ty_t_prod_data
                             pct_output_nodes TYPE ztarn_approvals_mass_tree.

  DATA: lt_selected_nodes TYPE lvc_t_nkey,
        ls_selected_nodes TYPE lvc_s_nkey,
        lt_output_nodes   TYPE ztarn_approvals_mass_tree,
        ls_output_nodes   TYPE zsarn_approvals_mass_tree,

        lr_nkey           TYPE RANGE OF lvc_nkey,
        ls_nkey           LIKE LINE OF lr_nkey.


  DATA: lv_called_from TYPE char3,
        ls_ver_data    TYPE ty_s_ver_data,
        ls_reg_hdr     TYPE zarn_reg_hdr,
        ls_reg_data    TYPE zsarn_reg_data,
        ls_prod_data   TYPE zsarn_prod_data.


  FIELD-SYMBOLS: <ls_idno_appr> TYPE ty_s_idno_appr.


* Get only Nodes from output table
  TRY.
      lt_output_nodes[] = VALUE #( FOR output IN pt_output[]
                                WHERE ( line_type = 'N' ) ( output ) ).
    CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
  ENDTRY.

* Get selected nodes to act
  CLEAR: lt_selected_nodes[].
  CALL METHOD po_alv_tree->get_selected_nodes
    CHANGING
      ct_selected_nodes = lt_selected_nodes[]
    EXCEPTIONS
      cntl_system_error = 1
      dp_error          = 2
      failed            = 3
      OTHERS            = 4.
  IF sy-subrc <> 0.
    RETURN.
  ENDIF.

  CLEAR pct_output_nodes[].

* Ignore unselected nodes,
* if no node is Selected, then all nodes will be approved
  IF lt_selected_nodes[] IS NOT INITIAL.
    CLEAR ls_nkey.
    ls_nkey-sign = 'I'.
    ls_nkey-option = 'EQ'.

    LOOP AT lt_selected_nodes[] INTO ls_selected_nodes.
      ls_nkey-low = ls_selected_nodes.
      APPEND ls_nkey TO lr_nkey[].
    ENDLOOP.

    DELETE lt_output_nodes[] WHERE node_key NOT IN lr_nkey[].

    pct_output_nodes[] = lt_output_nodes[].
  ENDIF.


* Filter records - Ignore not relevant for Mass Approvals
  PERFORM filter_for_mass_appr CHANGING lt_output_nodes[].


* Sort by IDNO and chg_area as National needs to be approved before regional
  IF lt_output_nodes[] IS NOT INITIAL.
    SORT lt_output_nodes[] BY idno chg_area.
  ELSE.
    RETURN.
  ENDIF.


* For all Nodes to be approved - All/Selected
  LOOP AT lt_output_nodes[] INTO ls_output_nodes.


* Get IDNO Approval data
    READ TABLE pct_idno_appr[] ASSIGNING <ls_idno_appr>
    WITH TABLE KEY idno = ls_output_nodes-idno
               chg_area = ls_output_nodes-chg_area
           chg_category = ls_output_nodes-chg_category.
    IF sy-subrc NE 0.
      CONTINUE.
    ENDIF.


    AT END OF chg_category.

      CLEAR: ls_ver_data, ls_reg_hdr, ls_reg_data, ls_prod_data.
      TRY.
          ls_ver_data = pt_ver_data[ idno = ls_output_nodes-idno ].
          ls_reg_hdr  = pt_reg_hdr[ idno = ls_output_nodes-idno ].

          ls_reg_data  = pct_reg_data[ idno = ls_output_nodes-idno ].

          ls_prod_data = pct_prod_data[ idno    = ls_output_nodes-idno
                                        version = ls_ver_data-current_ver ].

        CATCH cx_sy_itab_line_not_found ##NO_HANDLER.
      ENDTRY.


      CLEAR lv_called_from.
      IF ls_output_nodes-chg_area = 'R'.
        lv_called_from = 'REG'.
      ELSEIF ls_output_nodes-chg_area = 'N'.
        lv_called_from = 'NAT'.
      ENDIF.




      " National must be approved first before approving regional
      IF <ls_idno_appr>-gv_appr_nat_is_approved = space AND
         <ls_idno_appr>-chg_area = 'R'.
        CONTINUE.
      ENDIF.


* Generate new instance for approvals as for mass approvals, mt_approval data in instance is not updated
* resulting in not updating ZARN_APPROVAL table correctly
      <ls_idno_appr>-go_approval = NEW #( iv_idno              = ls_output_nodes-idno
                                          iv_load_db_approvals = abap_true
                                          iv_approval_event    = zcl_arn_approval_backend=>gc_event_ui ).

** Approve
      PERFORM approvals_update2 USING ls_ver_data
                                      ls_reg_hdr
                                      ls_reg_data
                                      ls_prod_data
                                      lv_called_from
                                      ls_output_nodes-chg_category
                                      space
                                      zif_arn_approval_status=>gc_arn_appr_status_approved
                             CHANGING <ls_idno_appr>
                                      pct_log[]
                                      pct_prd_version[]
                                      pct_reg_data[]
                                      pct_prod_data[]
                                      pct_reg_data_o[]
                                      pct_prod_data_o[].


    ENDAT.  " AT END OF chg_area

  ENDLOOP.  " LOOP AT lt_output_nodes[] INTO ls_output_nodes

ENDFORM.  " ACT_MASS_APPROVAL
*&---------------------------------------------------------------------*
*&      Form  APPROVALS_DISPLAY_TEXT2
*&---------------------------------------------------------------------*
* Display Text
*----------------------------------------------------------------------*
FORM approvals_display_text2 USING pv_called_from     TYPE char3
                                   pv_change_category TYPE zarn_chg_category
                                   pv_team_code       TYPE zarn_team_code
                          CHANGING pcs_idno_appr      TYPE ty_s_idno_appr.


  DATA lv_appr_area TYPE zarn_e_appr_area.

  lv_appr_area = pv_called_from.
  pcs_idno_appr-go_approval->set_approval_area( lv_appr_area ).

  "get the approval record
  DATA(ls_approval) = pcs_idno_appr-go_approval->get_approval_record(
       EXPORTING iv_chg_category = pv_change_category
                 iv_team_code    = pv_team_code  ).

  "and display screen with text
  pcs_idno_appr-gv_appr_comment = ls_approval-status_comment.

  IF ls_approval-status = zcl_arn_approval_backend=>gc_stat-rejected.
    pcs_idno_appr-gv_appr_rejection_reason = ls_approval-status_reason.
  ELSE.
    CLEAR pcs_idno_appr-gv_appr_rejection_reason.
  ENDIF.

  PERFORM appr_display_text CHANGING pcs_idno_appr.

ENDFORM.  " APPROVALS_DISPLAY_TEXT2
*&---------------------------------------------------------------------*
*&      Form  APPR_DISPLAY_TEXT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
FORM appr_display_text CHANGING pcs_idno_appr TYPE ty_s_idno_appr.

  pcs_idno_appr-gv_appr_mode = co_appr_mode_display_text.

  CALL SCREEN 0102 STARTING AT 5 5.

ENDFORM.  " APPR_DISPLAY_TEXT
*&---------------------------------------------------------------------*
*&      Form  APPR_SET_STATUS_0102
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
FORM appr_set_status_0102 CHANGING pcs_idno_appr TYPE ty_s_idno_appr.


  CLEAR: pcs_idno_appr-gv_appr_result.

  IF pcs_idno_appr-gv_appr_mode EQ co_appr_mode_display_text.
    SET PF-STATUS 'STATUS_0102_DISP'.
  ELSE.
    SET PF-STATUS 'STATUS_0102'.
  ENDIF.

  IF pcs_idno_appr-gv_appr_mode EQ co_appr_mode_approve.
    SET TITLEBAR '102A'.
    LOOP AT SCREEN.
      IF screen-name CS 'REJECTION_REASON'.
        screen-input = 0.
        screen-output = 0.
        screen-invisible = 1.
        MODIFY SCREEN.
      ENDIF.
    ENDLOOP.
  ELSEIF pcs_idno_appr-gv_appr_mode EQ co_appr_mode_reject.
    SET TITLEBAR '102R'.
  ELSEIF pcs_idno_appr-gv_appr_mode EQ co_appr_mode_add_text.
    SET TITLEBAR '102T'.
    LOOP AT SCREEN.
      IF screen-name CS 'REJECTION_REASON'.
        screen-input = 0.
        screen-output = 0.
        screen-invisible = 1.
        MODIFY SCREEN.
      ENDIF.
    ENDLOOP.
  ELSEIF pcs_idno_appr-gv_appr_mode EQ co_appr_mode_display_text.
    SET TITLEBAR '102D'.
    LOOP AT SCREEN.
      IF screen-name CS 'REJECTION_REASON'.
        IF pcs_idno_appr-gv_appr_rejection_reason IS INITIAL.
          screen-input = 0.
          screen-output = 0.
          screen-invisible = 1.
          MODIFY SCREEN.
        ELSE.
          screen-input = 0.
          MODIFY SCREEN.
        ENDIF.
      ENDIF.
    ENDLOOP.
  ELSE.
    LEAVE.
  ENDIF.

ENDFORM.  " APPR_SET_STATUS_0102
*&---------------------------------------------------------------------*
*&      Form  APPR_POPULATE_DROP_DOWN_0102
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
FORM appr_populate_drop_down_0102 CHANGING pcs_idno_appr TYPE ty_s_idno_appr.


  STATICS: lt_rej_reason TYPE STANDARD TABLE OF zarn_rej_reason.

  DATA: lt_values TYPE vrm_values,
        ls_value  LIKE LINE OF lt_values.

  FIELD-SYMBOLS: <ls_rej_reason> LIKE LINE OF lt_rej_reason.

  IF lt_rej_reason[] IS INITIAL.
    SELECT *
      INTO TABLE lt_rej_reason
      FROM zarn_rej_reason.                             "#EC CI_NOWHERE
  ENDIF.

  IF pcs_idno_appr-gv_appr_mode EQ co_appr_mode_reject.

    REFRESH lt_values.
    LOOP AT lt_rej_reason ASSIGNING <ls_rej_reason>.
      CLEAR ls_value.
      ls_value-key  = <ls_rej_reason>-rejection_reason.
      ls_value-text = <ls_rej_reason>-rejection_reason_desc.
      APPEND ls_value TO lt_values.
    ENDLOOP.

    CALL FUNCTION 'VRM_SET_VALUES'
      EXPORTING
        id              = '<GS_IDNO_APPR>-GV_APPR_REJECTION_REASON'
        values          = lt_values
      EXCEPTIONS
        id_illegal_name = 1.
  ELSEIF pcs_idno_appr-gv_appr_mode EQ co_appr_mode_display_text.

    REFRESH lt_values.
    LOOP AT lt_rej_reason ASSIGNING <ls_rej_reason>
       WHERE rejection_reason EQ pcs_idno_appr-gv_appr_rejection_reason.
      CLEAR ls_value.
      ls_value-key  = <ls_rej_reason>-rejection_reason.
      ls_value-text = <ls_rej_reason>-rejection_reason_desc.
      APPEND ls_value TO lt_values.
    ENDLOOP.

    CALL FUNCTION 'VRM_SET_VALUES'
      EXPORTING
        id              = '<GS_IDNO_APPR>-GV_APPR_REJECTION_REASON'
        values          = lt_values
      EXCEPTIONS
        id_illegal_name = 1.
  ENDIF.

ENDFORM.  " APPR_POPULATE_DROP_DOWN_0102
*&---------------------------------------------------------------------*
*&      Form  APPR_CREATE_TEXT_EDIT_0102
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
FORM appr_create_text_edit_0102 CHANGING pcs_idno_appr TYPE ty_s_idno_appr.

  IF go_appr_editor_container IS INITIAL.
    CREATE OBJECT go_appr_editor_container
      EXPORTING
        container_name              = 'APPR_COMMENTS_CTRL'
      EXCEPTIONS
        cntl_error                  = 1
        cntl_system_error           = 2
        create_error                = 3
        lifetime_error              = 4
        lifetime_dynpro_dynpro_link = 5
        OTHERS                      = 6.
  ENDIF.
  IF NOT go_appr_editor_container IS INITIAL.
    CREATE OBJECT go_appr_text_editor
      EXPORTING
        max_number_chars           = 4000
*       style                      = 0
        wordwrap_mode              = cl_gui_textedit=>wordwrap_at_windowborder
        wordwrap_to_linebreak_mode = cl_gui_textedit=>false
        parent                     = go_appr_editor_container
*       lifetime                   =
*       name                       =
      EXCEPTIONS
        error_cntl_create          = 1
        error_cntl_init            = 2
        error_cntl_link            = 3
        error_dp_create            = 4
        gui_type_not_supported     = 5
        OTHERS                     = 6.
    IF NOT go_appr_text_editor IS INITIAL.
      CALL METHOD go_appr_text_editor->set_toolbar_mode
        EXPORTING
          toolbar_mode           = cl_gui_textedit=>false
        EXCEPTIONS
          error_cntl_call_method = 1
          invalid_parameter      = 2
          OTHERS                 = 3.
      CALL METHOD go_appr_text_editor->set_statusbar_mode
        EXPORTING
          statusbar_mode         = cl_gui_textedit=>false
        EXCEPTIONS
          error_cntl_call_method = 1
          invalid_parameter      = 2
          OTHERS                 = 3.
    ENDIF.
  ENDIF.

  IF NOT go_appr_text_editor IS INITIAL.
    CALL METHOD go_appr_text_editor->set_textstream
      EXPORTING
        text                   = pcs_idno_appr-gv_appr_comment
      EXCEPTIONS
        error_cntl_call_method = 1
        not_supported_by_gui   = 2
        OTHERS                 = 3.
    CALL METHOD cl_gui_textedit=>set_focus
      EXPORTING
        control           = go_appr_text_editor
      EXCEPTIONS
        cntl_error        = 1
        cntl_system_error = 2
        OTHERS            = 3.
    IF pcs_idno_appr-gv_appr_mode EQ co_appr_mode_display_text.
      CALL METHOD go_appr_text_editor->set_readonly_mode
        EXPORTING
          readonly_mode          = cl_gui_textedit=>true
        EXCEPTIONS
          error_cntl_call_method = 1
          invalid_parameter      = 2
          OTHERS                 = 3.
    ENDIF.
  ENDIF.

ENDFORM.  " APPR_CREATE_TEXT_EDIT_0102
*&---------------------------------------------------------------------*
*&      Form  APPR_USER_COMMAND_POPUP_102
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
FORM appr_user_command_popup_102 CHANGING pcs_idno_appr TYPE ty_s_idno_appr.


  DATA: lv_ucomm TYPE sy-ucomm.

  CLEAR: pcs_idno_appr-gv_appr_comment.
  CALL METHOD go_appr_text_editor->get_textstream
    EXPORTING
      only_when_modified     = cl_gui_textedit=>false
    IMPORTING
      text                   = pcs_idno_appr-gv_appr_comment
*     is_modified            =
    EXCEPTIONS
      error_cntl_call_method = 1
      not_supported_by_gui   = 2
      OTHERS                 = 3.

  CALL METHOD cl_gui_cfw=>flush
    EXCEPTIONS
      cntl_system_error = 1
      cntl_error        = 2
      OTHERS            = 3.

  lv_ucomm = sy-ucomm.
  CLEAR sy-ucomm.
  CASE lv_ucomm.
    WHEN 'APPROVE_CA'.
      pcs_idno_appr-gv_appr_result = co_appr_result_cancelled.
      LEAVE TO SCREEN 0.

    WHEN 'APPROVE_OK'.
      IF pcs_idno_appr-gv_appr_mode EQ co_appr_mode_reject.
        IF pcs_idno_appr-gv_appr_rejection_reason IS INITIAL.
          MESSAGE i000 WITH 'Rejection Reason is mandatory'.
          RETURN.
        ENDIF.
      ENDIF.

      IF pcs_idno_appr-gv_appr_mode EQ co_appr_mode_approve.
        pcs_idno_appr-gv_appr_result = co_appr_result_approved.
      ELSEIF pcs_idno_appr-gv_appr_mode EQ co_appr_mode_reject.
        pcs_idno_appr-gv_appr_result = co_appr_result_rejected.
      ELSEIF pcs_idno_appr-gv_appr_mode EQ co_appr_mode_add_text.
        pcs_idno_appr-gv_appr_result = co_appr_result_text_added.
      ENDIF.
      LEAVE TO SCREEN 0.

    WHEN 'BACK'.
      LEAVE TO SCREEN 0.
    WHEN 'EXIT'.
      LEAVE TO SCREEN 0.
    WHEN 'CANC'.
      LEAVE TO SCREEN 0.
  ENDCASE.


ENDFORM.  " APPR_USER_COMMAND_POPUP_102
*&---------------------------------------------------------------------*
*&      Form  APPROVALS_ADD_TEXT2
*&---------------------------------------------------------------------*
* Add Text
*----------------------------------------------------------------------*
FORM approvals_add_text2 USING pv_called_from     TYPE char3
                               pv_change_category TYPE zarn_chg_category
                               pv_team_code       TYPE zarn_team_code
                      CHANGING pcv_column         TYPE zarn_approval_column
                               pcs_idno_appr      TYPE ty_s_idno_appr.



  DATA: lv_appr_area    TYPE zarn_e_appr_area,
        lt_approval_new TYPE zarn_t_approval,
        lt_appr_hist    TYPE zarn_t_appr_hist.

  lv_appr_area = pv_called_from.

  pcs_idno_appr-go_approval->set_approval_area( lv_appr_area ).

  DATA(ls_approval) = pcs_idno_appr-go_approval->get_approval_record(
                      EXPORTING iv_chg_category = pv_change_category
                                iv_team_code    = pv_team_code  ).

  pcs_idno_appr-gv_appr_comment = ls_approval-status_comment.

* Add Text
  PERFORM appr_add_text CHANGING pcs_idno_appr.

  TRY.


      pcs_idno_appr-go_approval->enqueue_appr_idno_or_wait( iv_mode = zif_enq_mode_c=>optimistic ).

      pcs_idno_appr-go_approval->action_change_comment(
                    EXPORTING
                      iv_team_code     = pv_team_code
                      iv_chg_category  = pv_change_category
                      iv_comment       = pcs_idno_appr-gv_appr_comment
                    IMPORTING
                      et_zarn_approval  = lt_approval_new
                      et_zarn_appr_hist = lt_appr_hist ).

      pcs_idno_appr-go_approval->post_approvals( EXPORTING it_approval  = lt_approval_new
                                                           it_appr_hist = lt_appr_hist ).


      COMMIT WORK AND WAIT.

    CATCH zcx_excep_cls.
      MESSAGE i099 DISPLAY LIKE 'E' .
      RETURN.
  ENDTRY.


  TRY.
      pcs_idno_appr-go_approval->enqueue_appr_idno_or_wait( zif_enq_mode_c=>optimistic ).
    CATCH zcx_excep_cls.
      MESSAGE a099.
  ENDTRY.

  IF pcs_idno_appr-gv_appr_comment IS INITIAL.
    pcv_column = icon_create_text.
  ELSE.
    pcv_column = icon_change_text.
*    gv_change = abap_true.
  ENDIF.


ENDFORM.  " APPROVALS_ADD_TEXT2
*&---------------------------------------------------------------------*
*&      Form  APPR_ADD_TEXT
*&---------------------------------------------------------------------*
* Add Text
*----------------------------------------------------------------------*
FORM appr_add_text CHANGING pcs_idno_appr TYPE ty_s_idno_appr.


  pcs_idno_appr-gv_appr_mode = co_appr_mode_add_text.

  CALL SCREEN 0102 STARTING AT 5 5.

ENDFORM.  " APPR_ADD_TEXT
*&---------------------------------------------------------------------*
*&      Form  APPR_REJECT
*&---------------------------------------------------------------------*
*  Rejection Comment
*----------------------------------------------------------------------*
FORM appr_reject CHANGING pcs_idno_appr TYPE ty_s_idno_appr.

  pcs_idno_appr-gv_appr_mode = co_appr_mode_reject.
  CLEAR: pcs_idno_appr-gv_appr_comment,
         pcs_idno_appr-gv_appr_rejection_reason.

  CALL SCREEN 0102 STARTING AT 5 5.

ENDFORM. " APPR_REJECT
*&---------------------------------------------------------------------*
*&      Form  APPROVALS_RESET
*&---------------------------------------------------------------------*
* Approval RESET
*----------------------------------------------------------------------*
FORM approvals_reset USING ps_ver_data        TYPE ty_s_ver_data
                           ps_reg_hdr         TYPE zarn_reg_hdr
                           ps_reg_data        TYPE zsarn_reg_data
                           ps_prod_data       TYPE zsarn_prod_data
                           pv_called_from     TYPE char3
                           pv_change_category TYPE zarn_chg_category
                           pv_team_code       TYPE zarn_team_code
                           pv_status          TYPE zarn_approval_status
                  CHANGING pcs_idno_appr      TYPE ty_s_idno_appr
                           pct_log            TYPE ztarn_approvals_mass_log
                           pct_prd_version    TYPE ty_t_prd_version
                           pct_reg_data       TYPE ty_t_reg_data
                           pct_prod_data      TYPE ty_t_prod_data
                           pct_reg_data_o     TYPE ty_t_reg_data
                           pct_prod_data_o    TYPE ty_t_prod_data.



  PERFORM approvals_update2 USING ps_ver_data
                                  ps_reg_hdr
                                  ps_reg_data
                                  ps_prod_data
                                  pv_called_from
                                  pv_change_category
                                  pv_team_code
                                  pv_status
                         CHANGING pcs_idno_appr
                                  pct_log[]
                                  pct_prd_version[]
                                  pct_reg_data[]
                                  pct_prod_data[]
                                  pct_reg_data_o[]
                                  pct_prod_data_o[].


ENDFORM.  " APPROVALS_RESET
*&---------------------------------------------------------------------*
*&      Form  FILTER_FOR_MASS_APPR
*&---------------------------------------------------------------------*
* Filter records - Ignore not relevant for Mass Approvals
*----------------------------------------------------------------------*
FORM filter_for_mass_appr  CHANGING fc_t_output_nodes TYPE ztarn_approvals_mass_tree.

  DATA: ls_output_nodes TYPE zsarn_approvals_mass_tree,
        lv_tabix        TYPE sy-tabix.



* 1) Ignore rows where nothing needs an approval
* 2) If a user is not authorized for approval, then only authorized records will be approved
*    as un-authorized approvals will be icon_led_inactive, thus will get deleted
  DELETE fc_t_output_nodes[] WHERE appr01 NE icon_okay
                               AND appr02 NE icon_okay
                               AND appr03 NE icon_okay
                               AND appr04 NE icon_okay
                               AND appr05 NE icon_okay.


* 3) Ignore Locked IDNOs
  DELETE fc_t_output_nodes[] WHERE lock_ind EQ icon_locked.

* 4) Ignore records which are not Mass Relevant for appprovals
  LOOP AT fc_t_output_nodes INTO ls_output_nodes.

    lv_tabix = sy-tabix.

    IF ls_output_nodes-appr01 EQ icon_okay AND ls_output_nodes-mass01 IS INITIAL.
      DELETE fc_t_output_nodes INDEX lv_tabix.
      CONTINUE.
    ENDIF.


    IF ls_output_nodes-appr02 EQ icon_okay AND ls_output_nodes-mass02 IS INITIAL.
      DELETE fc_t_output_nodes INDEX lv_tabix.
      CONTINUE.
    ENDIF.


    IF ls_output_nodes-appr03 EQ icon_okay AND ls_output_nodes-mass03 IS INITIAL.
      DELETE fc_t_output_nodes INDEX lv_tabix.
      CONTINUE.
    ENDIF.


    IF ls_output_nodes-appr04 EQ icon_okay AND ls_output_nodes-mass04 IS INITIAL.
      DELETE fc_t_output_nodes INDEX lv_tabix.
      CONTINUE.
    ENDIF.


    IF ls_output_nodes-appr05 EQ icon_okay AND ls_output_nodes-mass05 IS INITIAL.
      DELETE fc_t_output_nodes INDEX lv_tabix.
      CONTINUE.
    ENDIF.

  ENDLOOP.  " LOOP AT fc_t_output_nodes INTO ls_output_nodes




ENDFORM.
