*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_DATA_RECONCILIATION_MAIN
*&---------------------------------------------------------------------*



START-OF-SELECTION.


* Initialize
  PERFORM initialize.

* Set Comparision Mode
  PERFORM set_comparision_mode USING p_ecc
                                     p_dsp
                           CHANGING gv_mode.

* Get main FAN ID List
  PERFORM get_main_fan_list USING s_fan[]
                         CHANGING gt_fan[]
                                  gv_error.

  CHECK gv_error IS INITIAL.

* Build IDNO Data
  PERFORM build_idno_data USING gt_fan[]
                       CHANGING gt_idno_data[]
                                gt_mara[].

* Get National and Regional Data
  PERFORM get_nat_reg_data USING gt_idno_data[]
                        CHANGING gt_prod_data[]
                                 gt_reg_data[]
                                 gs_prod_data_all
                                 gs_reg_data_all.


* Build FAN Exist Data
  PERFORM build_fan_exist_data USING gt_fan[]
                                     gt_idno_data[]
                                     gs_prod_data_all
                                     gs_reg_data_all
                                     gt_mara[]
                            CHANGING gt_fan_exist[].


* Get SAP data from DB
  PERFORM get_sap_data USING gt_idno_data[]
                             gt_mara[]
                             gs_prod_data_all
                             gs_reg_data_all
                    CHANGING gt_makt[]
                             gt_maw1[]
                             gt_marm[]
                             gt_marm_idno[]
                             gt_mean[]
                             gt_marc[]
                             gt_eina[]
                             gt_eine[]
                             gt_mvke[]
                             gt_wlk2[]
                             gt_t006[]
                             gt_t005[]
                             gt_tcurc[]
                             gt_coo_t[]
                             gt_gen_uom_t[]
                             gt_t134[]
                             gr_tvarvc_ekorg[]
                             gr_tvarvc_vkorg[]
                             gt_tvarvc_p_org[]
                             gt_tvarvc_uni_lni[]
                             gt_tvarvc_hyb_code[]
                             gt_tvta[]
                             gt_lfm1[]
                             gt_fanid[]
                             gt_prod_uom_t[]
                             gt_uom_cat[]
                             gt_uomcategory[]
                             gt_ref_article[]
                             gt_pir_vendor[]
                             gt_tvarv_ean_cate[]
                             gt_host_prdtyp[]
                             gt_mc_subdep[]
                             gv_pir_ekorg.




* Build ARENA Data
  PERFORM build_arena_data USING gt_fan[]
                                 gt_idno_data[]
                                 gt_prod_data[]
                                 gt_reg_data[]
                                 gt_mara[]
                                 gt_makt[]
                                 gt_maw1[]
                                 gt_marm[]
                                 gt_marm_idno[]
                                 gt_mean[]
                                 gt_eina[]
                                 gt_eine[]
                                 gt_mvke[]
                                 gt_wlk2[]
                                 gt_t006[]
                                 gt_t005[]
                                 gt_tcurc[]
                                 gt_coo_t[]
                                 gt_gen_uom_t[]
                                 gt_t134[]
                                 gr_tvarvc_ekorg[]
                                 gr_tvarvc_vkorg[]
                                 gt_tvarvc_p_org[]
                                 gt_tvarvc_uni_lni[]
                                 gt_tvarvc_hyb_code[]
                                 gt_tvta[]
                                 gt_lfm1[]
                                 gt_fanid[]
                                 gt_prod_uom_t[]
                                 gt_uom_cat[]
                                 gt_uomcategory[]
                                 gt_ref_article[]
                                 gt_pir_vendor[]
                                 gt_tvarv_ean_cate[]
                                 gt_host_prdtyp[]
                                 gt_mc_subdep[]
                                 gv_pir_ekorg
                                 go_gui_load
                       CHANGING  gt_reg_data_def[]
                                 gt_headdata_arn[]
                                 gt_clientdata_arn[]
                                 gt_clientext_arn[]
                                 gt_addnlclientdata_arn[]
                                 gt_materialdescription_arn[]
                                 gt_unitsofmeasure_arn[]
                                 gt_internationalartno_arn[]
                                 gt_inforecord_general_arn[]
                                 gt_inforecord_purchorg_arn[]
                                 gt_salesdata_arn[]
                                 gt_salesext_arn[]
                                 gt_posdata_arn[]
                                 gt_plantdata_arn[]
                                 gt_plantext_arn[]                                  "++ONLD-822 JKH 17.02.2017
                                 gt_bapi_clientext_arn[]
                                 gt_bapi_salesext_arn[]
                                 gt_bapi_plantext_arn[].                            "++ONLD-822 JKH 17.02.2017


  IF gv_mode = gc_mode-ecc.
* Build Comparision from ECC Data
    PERFORM build_compare_ecc_data USING gt_fan[]
                                         gt_idno_data[]
                                         gt_mara[]
                                         gt_makt[]
                                         gt_maw1[]
                                         gt_marm[]
                                         gt_mean[]
                                         gt_marc[]
                                         gt_eina[]
                                         gt_eine[]
                                         gt_mvke[]
                                         gt_wlk2[]
                                CHANGING gt_headdata[]
                                         gt_clientdata[]
                                         gt_clientext[]
                                         gt_addnlclientdata[]
                                         gt_materialdescription[]
                                         gt_unitsofmeasure[]
                                         gt_internationalartno[]
                                         gt_inforecord_general[]
                                         gt_inforecord_purchorg[]
                                         gt_salesdata[]
                                         gt_salesext[]
                                         gt_posdata[]
                                         gt_plantdata[]
                                         gt_plantext[]                              "++ONLD-822 JKH 17.02.2017
                                         gt_bapi_clientext[]
                                         gt_bapi_salesext[]
                                         gt_bapi_plantext[].                        "++ONLD-822 JKH 17.02.2017

  ENDIF.



  IF gv_mode = gc_mode-dsp.
    CLEAR : gt_clientdata[],
            gt_clientext[],
            gt_addnlclientdata[],
            gt_materialdescription[],
            gt_unitsofmeasure[],
            gt_internationalartno[],
            gt_inforecord_general[],
            gt_inforecord_purchorg[],
            gt_salesdata[],
            gt_salesext[],
            gt_posdata[],
            gt_plantdata[],
            gt_plantext[],                                                          "++ONLD-822 JKH 17.02.2017
            gt_bapi_clientext[],
            gt_bapi_salesext[],
            gt_bapi_plantext[].                                                     "++ONLD-822 JKH 17.02.2017
  ENDIF.


* Build Comparison data to be displayed
  PERFORM build_display_data USING gt_fan[]
                                   gt_idno_data[]
                                   gt_prod_data[]
                                   gt_reg_data_def[]
                                   gt_fanid[]
                                   gt_tvarvc_uni_lni[]
                                   gt_mean[]
                                   gt_headdata_arn[]
                                   gt_clientdata_arn[]
                                   gt_clientext_arn[]
                                   gt_addnlclientdata_arn[]
                                   gt_materialdescription_arn[]
                                   gt_unitsofmeasure_arn[]
                                   gt_internationalartno_arn[]
                                   gt_inforecord_general_arn[]
                                   gt_inforecord_purchorg_arn[]
                                   gt_salesdata_arn[]
                                   gt_salesext_arn[]
                                   gt_posdata_arn[]
                                   gt_plantdata_arn[]
                                   gt_plantext_arn[]                                "++ONLD-822 JKH 17.02.2017
                                   gt_bapi_clientext_arn[]
                                   gt_bapi_salesext_arn[]
                                   gt_bapi_plantext_arn[]                           "++ONLD-822 JKH 17.02.2017
                                   gt_headdata[]
                                   gt_clientdata[]
                                   gt_clientext[]
                                   gt_addnlclientdata[]
                                   gt_materialdescription[]
                                   gt_unitsofmeasure[]
                                   gt_internationalartno[]
                                   gt_inforecord_general[]
                                   gt_inforecord_purchorg[]
                                   gt_salesdata[]
                                   gt_salesext[]
                                   gt_posdata[]
                                   gt_plantdata[]
                                   gt_plantext[]                                    "++ONLD-822 JKH 17.02.2017
                                   gt_bapi_clientext[]
                                   gt_bapi_salesext[]
                                   gt_bapi_plantext[]                               "++ONLD-822 JKH 17.02.2017
                          CHANGING gt_fan_exist[]
                                   gt_reco_basic1[]
                                   gt_reco_basic2[]
                                   gt_reco_attr[]
                                   gt_reco_uom[]
                                   gt_reco_ean[]
                                   gt_reco_pir[]
                                   gt_reco_banner[]
                                   gv_count_9002
                                   gv_count_9003
                                   gv_count_9004
                                   gv_count_9005
                                   gv_count_9006
                                   gv_count_9007
                                   gv_count_9008.





END-OF-SELECTION.

* Free Memory
  PERFORM free_memory.


  IF gv_error IS INITIAL.
    CALL SCREEN 9000.
  ENDIF.
