*&---------------------------------------------------------------------*
*&  Include           ZIARN_GUI_TS01
*&---------------------------------------------------------------------*
MODULE ts_main_active_tab_set OUTPUT.
  ts_main-activetab = g_ts_main-pressed_tab.
  CASE g_ts_main-pressed_tab.
    WHEN c_ts_main-tab1.
      g_ts_main-subscreen = '0101'.
    WHEN c_ts_main-tab2.
      g_ts_main-subscreen = '0102'.
    WHEN OTHERS.
  ENDCASE.
ENDMODULE.                    "TS_MAIN_ACTIVE_TAB_SET OUTPUT


* GETS ACTIVE TAB
MODULE ts_main_active_tab_get INPUT.

  CASE gv_okcode.
    WHEN c_ts_main-tab1.
      g_ts_main-pressed_tab = c_ts_main-tab1.
    WHEN c_ts_main-tab2.
*     user is not allowed into the regional screen when navigating from I Care selection TAB
      IF gv_sel_tab EQ gc_icare_tab.
        MESSAGE i000 WITH text-206.
      ELSE.
        g_ts_main-pressed_tab = c_ts_main-tab2.
      ENDIF.

*    following logic may need to be reinstated !!!

*     check I care value
*      IF gs_worklist-fsni_icare_value IS INITIAL.
**        MESSAGE i000 WITH text-206.
*        g_ts_main-pressed_tab = c_ts_main-tab2.
*     article created and a change is not national approved ?
*      ELSEIF  zarn_ver_status-article_ver      IS NOT INITIAL  AND
*      IF  zarn_ver_status-article_ver      IS NOT INITIAL  AND
*          zarn_prd_version-version_status  NE gc_status_apr.
*        MESSAGE i000 WITH text-207 text-208.
*      ELSE.
*        g_ts_main-pressed_tab = c_ts_main-tab2.
*      ENDIF.

*      IF zarn_products-fsni_icare_value IS NOT INITIAL.
**     check if national is approved to proceed with enrichment
**      PERFORM approvals_is_nat_approved CHANGING gv_nat_approved.
**      IF  gv_nat_approved IS NOT INITIAL.
*        g_ts_main-pressed_tab = c_ts_main-tab2.
*      ELSE.
*        MESSAGE i000 WITH text-206.
*        g_ts_main-pressed_tab = c_ts_main-tab1.
*      ENDIF.
*
**     Also check for
*      IF zarn_ver_status-article_ver      IS NOT INITIAL  AND
*         zarn_prd_version-version_status  NE gc_status_apr. "'APR'.
**       cannot access  Regional tab for enrichment
*        MESSAGE i000 WITH text-207 text-208.
*        g_ts_main-pressed_tab = c_ts_main-tab1.
*      ELSE.
*        g_ts_main-pressed_tab = c_ts_main-tab2.
*      ENDIF.

    WHEN OTHERS.

  ENDCASE.
ENDMODULE.                    "TS_MAIN_ACTIVE_TAB_GET INPUT
