* regenerated at 19.04.2016 16:14:45 by  C90002769
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_SLATOP.                      " Global Data
  INCLUDE LZARN_SLAUXX.                      " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_SLAF...                      " Subroutines
* INCLUDE LZARN_SLAO...                      " PBO-Modules
* INCLUDE LZARN_SLAI...                      " PAI-Modules
* INCLUDE LZARN_SLAE...                      " Events
* INCLUDE LZARN_SLAP...                      " Local class implement.
* INCLUDE LZARN_SLAT99.                      " ABAP Unit tests
  INCLUDE LZARN_SLAF00                            . " subprograms
  INCLUDE LZARN_SLAI00                            . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
