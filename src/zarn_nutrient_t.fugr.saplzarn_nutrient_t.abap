* regenerated at 05.05.2016 15:33:22 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_NUTRIENT_TTOP.               " Global Data
  INCLUDE LZARN_NUTRIENT_TUXX.               " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_NUTRIENT_TF...               " Subroutines
* INCLUDE LZARN_NUTRIENT_TO...               " PBO-Modules
* INCLUDE LZARN_NUTRIENT_TI...               " PAI-Modules
* INCLUDE LZARN_NUTRIENT_TE...               " Events
* INCLUDE LZARN_NUTRIENT_TP...               " Local class implement.
* INCLUDE LZARN_NUTRIENT_TT99.               " ABAP Unit tests
  INCLUDE LZARN_NUTRIENT_TF00                     . " subprograms
  INCLUDE LZARN_NUTRIENT_TI00                     . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
