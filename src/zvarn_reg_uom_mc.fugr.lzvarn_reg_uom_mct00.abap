*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 19.05.2020 at 10:08:56
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZVARN_REG_UOM_MC................................*
TABLES: ZVARN_REG_UOM_MC, *ZVARN_REG_UOM_MC. "view work areas
CONTROLS: TCTRL_ZVARN_REG_UOM_MC
TYPE TABLEVIEW USING SCREEN '9000'.
DATA: BEGIN OF STATUS_ZVARN_REG_UOM_MC. "state vector
          INCLUDE STRUCTURE VIMSTATUS.
DATA: END OF STATUS_ZVARN_REG_UOM_MC.
* Table for entries selected to show on screen
DATA: BEGIN OF ZVARN_REG_UOM_MC_EXTRACT OCCURS 0010.
INCLUDE STRUCTURE ZVARN_REG_UOM_MC.
          INCLUDE STRUCTURE VIMFLAGTAB.
DATA: END OF ZVARN_REG_UOM_MC_EXTRACT.
* Table for all entries loaded from database
DATA: BEGIN OF ZVARN_REG_UOM_MC_TOTAL OCCURS 0010.
INCLUDE STRUCTURE ZVARN_REG_UOM_MC.
          INCLUDE STRUCTURE VIMFLAGTAB.
DATA: END OF ZVARN_REG_UOM_MC_TOTAL.

*.........table declarations:.................................*
TABLES: ZARN_REG_UOM_MC                .
