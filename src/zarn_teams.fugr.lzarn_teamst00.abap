*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 23.02.2016 at 08:06:06 by user C90001448
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_TEAMS......................................*
DATA:  BEGIN OF STATUS_ZARN_TEAMS                    .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_TEAMS                    .
CONTROLS: TCTRL_ZARN_TEAMS
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_TEAMS                    .
TABLES: ZARN_TEAMS                     .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
