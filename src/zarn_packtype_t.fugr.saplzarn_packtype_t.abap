* regenerated at 05.05.2016 15:39:57 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_PACKTYPE_TTOP.               " Global Data
  INCLUDE LZARN_PACKTYPE_TUXX.               " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_PACKTYPE_TF...               " Subroutines
* INCLUDE LZARN_PACKTYPE_TO...               " PBO-Modules
* INCLUDE LZARN_PACKTYPE_TI...               " PAI-Modules
* INCLUDE LZARN_PACKTYPE_TE...               " Events
* INCLUDE LZARN_PACKTYPE_TP...               " Local class implement.
* INCLUDE LZARN_PACKTYPE_TT99.               " ABAP Unit tests
  INCLUDE LZARN_PACKTYPE_TF00                     . " subprograms
  INCLUDE LZARN_PACKTYPE_TI00                     . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
