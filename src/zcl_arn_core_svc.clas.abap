CLASS zcl_arn_core_svc DEFINITION
  PUBLIC
  CREATE PUBLIC .

  PUBLIC SECTION.

    CLASS-METHODS get_current_version
      IMPORTING
        !iv_idno          TYPE zarn_idno
      RETURNING
        VALUE(rv_version) TYPE zarn_version .

    CLASS-METHODS get_latest_nat_vers
      IMPORTING
        !iv_idno          TYPE zarn_idno
      RETURNING
        VALUE(rv_version) TYPE zarn_version .

    CLASS-METHODS get_latest_approved_nat_vers
      IMPORTING
        !iv_idno          TYPE zarn_idno
      RETURNING
        VALUE(rv_version) TYPE zarn_version .
  PROTECTED SECTION.
  PRIVATE SECTION.
ENDCLASS.



CLASS ZCL_ARN_CORE_SVC IMPLEMENTATION.


  METHOD get_current_version.
* Returns the current arena version on the DB

    rv_version = zcl_arn_read_db=>read_version_status( iv_idno )-current_ver.

  ENDMETHOD.


  METHOD get_latest_approved_nat_vers.

    DATA lt_rng_approved_status TYPE RANGE OF zarn_version_status.

    "latest approved national version can be "older" than ZARN_VER_STATUS-CURRENT_VER
    "(in case the new data arrived but not yet approved)

    lt_rng_approved_status = VALUE #( sign = 'I' option = 'EQ' ( low = zif_arn_nat_version_status_c=>gc_approved )
                                                               ( low = zif_arn_nat_version_status_c=>gc_article_posted )
                                                               ( low = zif_arn_nat_version_status_c=>gc_migrated )  ).

    SELECT MAX( version ) FROM zarn_prd_version INTO rv_version
      WHERE idno = iv_idno
        AND version_status IN lt_rng_approved_status.

  ENDMETHOD.


  METHOD get_latest_nat_vers.

    SELECT MAX( version ) FROM zarn_prd_version INTO rv_version
      WHERE idno = iv_idno.

  ENDMETHOD.
ENDCLASS.
