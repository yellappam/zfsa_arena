class ZCL_ARN_PARAM_PROV definition
  public
  create public .

public section.

  types:
    BEGIN OF ts_param,
        name  TYPE zarn_e_param_name,
        value TYPE zarn_e_param_value,
      END OF ts_param .
  types:
    tt_param TYPE SORTED TABLE OF ts_param WITH NON-UNIQUE KEY name .

  class-methods GET
    importing
      !IV_PARAM type ZARN_E_PARAM_NAME
    returning
      value(RV_VALUE) type ZARN_E_PARAM_VALUE
    raising
      ZCX_ARN_PARAM_ERR .
  class-methods GET_RANGE
    importing
      !IV_PARAM type ZARN_E_PARAM_NAME
      !IV_SEP type CHAR1 default ','
    returning
      value(RT_PARAM_RANGE) type ZTARN_PARAM_RANGE
    raising
      ZCX_ARN_PARAM_ERR .
protected section.
private section.

  class-data ST_PARAM type TT_PARAM .
ENDCLASS.



CLASS ZCL_ARN_PARAM_PROV IMPLEMENTATION.


  METHOD get.
* Return the parameter

    TRY.
        rv_value = st_param[ name = iv_param ]-value.
      CATCH cx_sy_itab_line_not_found.
        SELECT SINGLE name, value
          FROM zarn_param
          INTO @DATA(ls_param)
          WHERE name = @iv_param.
        IF sy-subrc = 0.
          rv_value = ls_param-value.
          INSERT ls_param INTO TABLE st_param.
        ELSE.
          RAISE EXCEPTION TYPE zcx_arn_param_err
            EXPORTING
              textid   = zcx_arn_param_err=>not_found
              mv_param = iv_param.
        ENDIF.
    ENDTRY.

  ENDMETHOD.


  METHOD get_range.
* Return as range
    DATA: ls_param_range TYPE zsarn_param_range.

    DATA(lv_value) = get( iv_param ).
    CHECK lv_value IS NOT INITIAL.

    SPLIT lv_value AT iv_sep INTO TABLE DATA(lt_param).

    ls_param_range-sign = 'I'.
    ls_param_range-option = 'EQ'.
    LOOP AT lt_param ASSIGNING FIELD-SYMBOL(<lv_param>).
      ls_param_range-low = <lv_param>.
      APPEND ls_param_range TO rt_param_range.
    ENDLOOP.


  ENDMETHOD.
ENDCLASS.
