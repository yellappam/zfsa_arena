*---------------------------------------------------------------------*
*    view related data declarations
*   generation date: 16.05.2016 at 10:31:02 by user C90001363
*   view maintenance generator version: #001407#
*---------------------------------------------------------------------*
*...processing: ZARN_CC_DESC....................................*
DATA:  BEGIN OF STATUS_ZARN_CC_DESC                  .   "state vector
         INCLUDE STRUCTURE VIMSTATUS.
DATA:  END OF STATUS_ZARN_CC_DESC                  .
CONTROLS: TCTRL_ZARN_CC_DESC
            TYPE TABLEVIEW USING SCREEN '0001'.
*.........table declarations:.................................*
TABLES: *ZARN_CC_DESC                  .
TABLES: ZARN_CC_DESC                   .

* general table data declarations..............
  INCLUDE LSVIMTDT                                .
