*&---------------------------------------------------------------------*
*&  Include           ZIARN_APPROVALS_NOTIFY2CD1
*&---------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*&       Class lcl_approvals_notify
*&---------------------------------------------------------------------*
*        Text
*----------------------------------------------------------------------*
CLASS lcl_approvals_notify DEFINITION.


  PUBLIC SECTION.

    TYPES: ty_t_idno_range TYPE RANGE OF zarn_idno,
           ty_s_idno_range TYPE LINE OF ty_t_idno_range,
           ty_t_idno       TYPE TABLE OF zarn_idno.

    TYPES: BEGIN OF ty_notif,
             team_code    TYPE zarn_team_code,
             user         TYPE xubname,
             idno         TYPE zarn_idno,
             chg_area     TYPE zarn_chg_area,
             chg_category TYPE zarn_chg_category,
           END OF ty_notif,
           ty_t_notif TYPE SORTED TABLE OF ty_notif WITH UNIQUE KEY team_code user idno chg_area chg_category.


    TYPES: BEGIN OF ty_selections,
             idno       TYPE ztarn_idno_range,
             fanid      TYPE RANGE OF zarn_fan_id,
             updated_on TYPE RANGE OF zarn_e_appr_upd_tmstmp,
           END OF ty_selections.

    TYPES: BEGIN OF ty_control,
             consum TYPE abap_bool,
             test   TYPE abap_bool,
           END OF ty_control.

    TYPES: BEGIN OF ty_s_approval_guid,
             guid TYPE zarn_e_appr_guid,
           END OF ty_s_approval_guid.

    TYPES: ty_t_approval_guid TYPE STANDARD TABLE OF ty_s_approval_guid.

    TYPES: BEGIN OF ty_log,
             idno          TYPE zarn_idno,
             traffic_light TYPE zca_status_icon,
             type          TYPE bapi_mtype,
             id            TYPE symsgid,
             number        TYPE symsgno,
             message       TYPE bapi_msg,
*             row     TYPE fpm_row,
           END OF ty_log.

    TYPES: BEGIN OF ty_teamuser,
             team_code TYPE zarn_team_code,
             user      TYPE xubname,
           END OF ty_teamuser.



    TYPES: ty_t_team_users TYPE SORTED TABLE OF ty_teamuser WITH UNIQUE KEY team_code user.

    METHODS:
      constructor
        IMPORTING is_selections TYPE ty_selections
                  is_control    TYPE ty_control,
      run,
      display_log,
      write_log
        IMPORTING iv_type    TYPE bapi_mtype OPTIONAL
                  iv_id      TYPE symsgid OPTIONAL
                  iv_number  TYPE symsgno OPTIONAL
                  iv_message TYPE bapi_msg OPTIONAL.


  PRIVATE SECTION.
    DATA: ms_selections TYPE ty_selections,
          ms_control    TYPE ty_control.

    DATA: mo_alv_table  TYPE REF TO cl_salv_table.

    DATA: mt_log               TYPE STANDARD TABLE OF ty_log,
          mt_pending_approvals TYPE SORTED TABLE OF zarn_approval WITH NON-UNIQUE KEY idno,
          mt_chg_hdr           TYPE ztarn_appr_chg_hdr,
          mt_chg_det           TYPE ztarn_cc_det,
          mt_reg_hdr           TYPE SORTED TABLE OF zarn_reg_hdr WITH NON-UNIQUE KEY idno,
          mt_reg_banner        TYPE ztarn_reg_banner.
*          mt_appr_chg_hdr      TYPE SORTED TABLE OF zsarn_appr_chg_hdr WITH NON-UNIQUE KEY idno chg_area.

    DATA: mt_category TYPE SORTED TABLE OF zmd_category WITH UNIQUE KEY role_id,
          mt_dc_by_cm TYPE SORTED TABLE OF zmd_dc_by_cm_ass WITH NON-UNIQUE KEY cm_code,
          mt_dc_buyer TYPE SORTED TABLE OF zmd_dc_buyer WITH UNIQUE KEY dc_buyer_code,
          mt_teams    TYPE SORTED TABLE OF zarn_teams WITH UNIQUE KEY team_code.

    DATA: mv_send_cnt     TYPE i,
          mv_send_err_cnt TYPE i.

    METHODS:
      query
        RETURNING VALUE(rv_success) TYPE abap_bool,
      read_cust,
      get_team_recipient
        IMPORTING iv_team_code        TYPE zarn_team_code
                  iv_catman           TYPE zarn_catman
        RETURNING VALUE(rs_team_user) TYPE ty_teamuser,
      read_approvals
        RETURNING VALUE(rv_loaded) TYPE abap_bool,
      read_changes,
      read_regional,
      get_idno_from_fanid,
      adjust,
      determine_catman
        IMPORTING is_approval      TYPE zarn_approval
                  is_reg_hdr       TYPE zarn_reg_hdr
        RETURNING VALUE(rt_catman) TYPE ztcatman,
      catman_by_change
        IMPORTING is_approval      TYPE zarn_approval
                  is_reg_hdr       TYPE zarn_reg_hdr
        RETURNING VALUE(rt_catman) TYPE ztcatman,
      catman_by_host
        IMPORTING is_approval      TYPE zarn_approval
                  is_reg_hdr       TYPE zarn_reg_hdr
        RETURNING VALUE(rt_catman) TYPE ztcatman,
      catman_by_default
        IMPORTING is_approval      TYPE zarn_approval
                  is_reg_hdr       TYPE zarn_reg_hdr
        RETURNING VALUE(rt_catman) TYPE ztcatman,
      package
        EXPORTING et_recipient_approvals TYPE ztarn_user_approvals,
      validate
        IMPORTING is_approval     TYPE zarn_approval
        RETURNING VALUE(rv_valid) TYPE abap_bool  ,
      process
        IMPORTING it_recipient_approvals TYPE ztarn_user_approvals,
      finalise,
      send
        IMPORTING is_recipient_approval TYPE zsarn_user_approvals
        RETURNING VALUE(rv_sent)        TYPE abap_bool  ,
      update
        IMPORTING it_approvals TYPE zarn_t_approval,
      setup_alv,
      adjust_log,
      build_alv_header
        RETURNING VALUE(ro_header) TYPE REF TO cl_salv_form_layout_grid.


ENDCLASS.
