* regenerated at 02.05.2016 17:08:45 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_IRRADIATE_TTOP.              " Global Data
  INCLUDE LZARN_IRRADIATE_TUXX.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_IRRADIATE_TF...              " Subroutines
* INCLUDE LZARN_IRRADIATE_TO...              " PBO-Modules
* INCLUDE LZARN_IRRADIATE_TI...              " PAI-Modules
* INCLUDE LZARN_IRRADIATE_TE...              " Events
* INCLUDE LZARN_IRRADIATE_TP...              " Local class implement.
* INCLUDE LZARN_IRRADIATE_TT99.              " ABAP Unit tests
  INCLUDE LZARN_IRRADIATE_TF00                    . " subprograms
  INCLUDE LZARN_IRRADIATE_TI00                    . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
