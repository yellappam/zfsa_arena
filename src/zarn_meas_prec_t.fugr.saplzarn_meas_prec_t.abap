* regenerated at 05.05.2016 15:00:58 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_MEAS_PREC_TTOP.              " Global Data
  INCLUDE LZARN_MEAS_PREC_TUXX.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_MEAS_PREC_TF...              " Subroutines
* INCLUDE LZARN_MEAS_PREC_TO...              " PBO-Modules
* INCLUDE LZARN_MEAS_PREC_TI...              " PAI-Modules
* INCLUDE LZARN_MEAS_PREC_TE...              " Events
* INCLUDE LZARN_MEAS_PREC_TP...              " Local class implement.
* INCLUDE LZARN_MEAS_PREC_TT99.              " ABAP Unit tests
  INCLUDE LZARN_MEAS_PREC_TF00                    . " subprograms
  INCLUDE LZARN_MEAS_PREC_TI00                    . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
