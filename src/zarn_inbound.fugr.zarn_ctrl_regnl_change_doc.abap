*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Functional Specification 3113
* DATE WRITTEN     # 26022016
* SAP VERSION      # 731
* TYPE             # Function Module
* AUTHOR           # C90001363, Jitin Kharbanda
*-----------------------------------------------------------------------
* TITLE            # Change document create
* PURPOSE          # Maintain Change documents for Control and
*                  # Regional Tables
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 14.05.2019
* CHANGE No.       # CIP-540 AReNa NAT-REG online char change
* DESCRIPTION      # Added parameter to update regional table change log. Currently
*                  # only ZARN_REG_STR table has been updated. This can be extended
*                  # to other tables when needed
* WHO              # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
FUNCTION zarn_ctrl_regnl_change_doc .
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(IT_CONTROL) TYPE  ZTARN_CONTROL OPTIONAL
*"     VALUE(IT_PRD_VERSION) TYPE  ZTARN_PRD_VERSION
*"     VALUE(IT_VER_STATUS) TYPE  ZTARN_VER_STATUS
*"     REFERENCE(IT_REG_DATA) TYPE  ZTARN_REG_DATA OPTIONAL
*"     REFERENCE(IT_PROD_DATA) TYPE  ZTARN_PROD_DATA
*"     VALUE(IT_CONTROL_OLD) TYPE  ZTARN_CONTROL OPTIONAL
*"     VALUE(IT_PRD_VERSION_OLD) TYPE  ZTARN_PRD_VERSION OPTIONAL
*"     VALUE(IT_VER_STATUS_OLD) TYPE  ZTARN_VER_STATUS OPTIONAL
*"     REFERENCE(IT_REG_DATA_OLD) TYPE  ZTARN_REG_DATA OPTIONAL
*"     VALUE(IV_CI_CONTROL) TYPE  FLAG DEFAULT SPACE
*"     VALUE(IV_CI_PRD_VERSION) TYPE  FLAG DEFAULT SPACE
*"     VALUE(IV_CI_VER_STATUS) TYPE  FLAG DEFAULT SPACE
*"     VALUE(IV_CI_REG_HDR) TYPE  FLAG DEFAULT SPACE
*"     VALUE(IV_CI_REG_DATA) TYPE  FLAG DEFAULT ABAP_FALSE
*"----------------------------------------------------------------------

  DATA: lt_prod_data       TYPE ztarn_prod_data,
        ls_prod_data       TYPE zsarn_prod_data,

        lt_control         TYPE ztarn_control,
        ls_control         TYPE zarn_control,
        lt_control_old     TYPE ztarn_control,

        lt_prd_version     TYPE ztarn_prd_version,
        ls_prd_version     TYPE zarn_prd_version,
        lt_prd_version_old TYPE ztarn_prd_version,

        ls_reg_data        TYPE zsarn_reg_data,
        lt_reg_hdr         TYPE ztarn_reg_hdr,
        ls_reg_hdr         TYPE zarn_reg_hdr,
        lt_reg_hdr_old     TYPE ztarn_reg_hdr,

        lt_ver_status      TYPE ztarn_ver_status,
        ls_ver_status      TYPE zarn_ver_status,
        lt_ver_status_old  TYPE ztarn_ver_status,

        lt_cdtxt           TYPE isu_cdtxt,

        lv_objectid        TYPE cdhdr-objectid,
        lv_chg_flag        TYPE cdchngindh,
        lv_ci_control      TYPE cdflag,
        lv_ci_prd_version  TYPE cdflag,
        lv_ci_ver_status   TYPE cdflag,
        lv_ci_reg_hdr      TYPE cdflag,
        lv_ci_zarn_reg_str TYPE cdflag.



  DATA: lt_xcontrol          TYPE STANDARD TABLE OF yzarn_control,
        lt_ycontrol          TYPE STANDARD TABLE OF yzarn_control,

        lt_xprd_version      TYPE STANDARD TABLE OF yzarn_prd_version,
        lt_yprd_version      TYPE STANDARD TABLE OF yzarn_prd_version,

        lt_xver_status       TYPE STANDARD TABLE OF yzarn_ver_status,
        lt_yver_status       TYPE STANDARD TABLE OF yzarn_ver_status,

        lt_xreg_hdr          TYPE STANDARD TABLE OF yzarn_reg_hdr,
        lt_yreg_hdr          TYPE STANDARD TABLE OF yzarn_reg_hdr,

        lt_xreg_hsno         TYPE STANDARD TABLE OF yzarn_reg_hsno,
        lt_yreg_hsno         TYPE STANDARD TABLE OF yzarn_reg_hsno,
        lt_xreg_banner       TYPE STANDARD TABLE OF yzarn_reg_banner,
        lt_yreg_banner       TYPE STANDARD TABLE OF yzarn_reg_banner,
        lt_xreg_cluster      TYPE STANDARD TABLE OF yzarn_reg_cluster,
        lt_yreg_cluster      TYPE STANDARD TABLE OF yzarn_reg_cluster,
        lt_xreg_ean          TYPE STANDARD TABLE OF yzarn_reg_ean,
        lt_yreg_ean          TYPE STANDARD TABLE OF yzarn_reg_ean,
        lt_xreg_pir          TYPE STANDARD TABLE OF yzarn_reg_pir,
        lt_yreg_pir          TYPE STANDARD TABLE OF yzarn_reg_pir,
        lt_xreg_prfam        TYPE STANDARD TABLE OF yzarn_reg_prfam,
        lt_yreg_prfam        TYPE STANDARD TABLE OF yzarn_reg_prfam,
        lt_xreg_txt          TYPE STANDARD TABLE OF yzarn_reg_txt,
        lt_yreg_txt          TYPE STANDARD TABLE OF yzarn_reg_txt,
        lt_xreg_uom          TYPE STANDARD TABLE OF yzarn_reg_uom,
        lt_yreg_uom          TYPE STANDARD TABLE OF yzarn_reg_uom,
        lt_xzarn_reg_artlink TYPE STANDARD TABLE OF yzarn_reg_artlink,
        lt_yzarn_reg_artlink TYPE STANDARD TABLE OF yzarn_reg_artlink,
        lt_xzarn_reg_onlcat  TYPE STANDARD TABLE OF yzarn_reg_onlcat,
        lt_yzarn_reg_onlcat  TYPE STANDARD TABLE OF yzarn_reg_onlcat,
        lt_xzarn_reg_str     TYPE yzarn_reg_strs,
        lt_yzarn_reg_str     TYPE yzarn_reg_strs,

        lt_xreg_lst_prc      TYPE STANDARD TABLE OF yzarn_reg_lst_prc,
        lt_yreg_lst_prc      TYPE STANDARD TABLE OF yzarn_reg_lst_prc,
        lt_xreg_std_ter      TYPE STANDARD TABLE OF yzarn_reg_std_ter,
        lt_yreg_std_ter      TYPE STANDARD TABLE OF yzarn_reg_std_ter,
        lt_xreg_dc_sell      TYPE STANDARD TABLE OF yzarn_reg_dc_sell,
        lt_yreg_dc_sell      TYPE STANDARD TABLE OF yzarn_reg_dc_sell,
        lt_xreg_rrp          TYPE STANDARD TABLE OF yzarn_reg_rrp,
        lt_yreg_rrp          TYPE STANDARD TABLE OF yzarn_reg_rrp.


  IF iv_ci_control     IS INITIAL AND
     iv_ci_prd_version IS INITIAL AND
     iv_ci_ver_status  IS INITIAL AND
     iv_ci_reg_hdr     IS INITIAL AND
     iv_ci_reg_data    IS INITIAL.
    RETURN.
  ENDIF.

  LOOP AT it_prod_data INTO ls_prod_data.


    CLEAR: lt_control[],     lt_control_old[],
           lt_prd_version[], lt_prd_version_old[],
           lt_ver_status[],  lt_ver_status_old[],
           lt_reg_hdr[],     lt_reg_hdr_old[].

    lt_prd_version     = it_prd_version[].
    lt_prd_version_old = it_prd_version_old[].
    lt_ver_status      = it_ver_status[].
    lt_ver_status_old  = it_ver_status_old[].


    DELETE lt_prd_version[]       WHERE idno NE ls_prod_data-idno.
    DELETE lt_prd_version_old[]   WHERE idno NE ls_prod_data-idno.
    DELETE lt_ver_status[]        WHERE idno NE ls_prod_data-idno.
    DELETE lt_ver_status_old[]    WHERE idno NE ls_prod_data-idno.
    DELETE lt_reg_hdr[]           WHERE idno NE ls_prod_data-idno.
    DELETE lt_reg_hdr_old[]       WHERE idno NE ls_prod_data-idno.

    CLEAR: lt_xcontrol[], lt_ycontrol[], lv_ci_control.
* To maintain Control Table, Prd_Version table must be maintained
    IF iv_ci_control = abap_true AND iv_ci_prd_version = abap_true.

      LOOP AT lt_prd_version[] INTO ls_prd_version.
        CLEAR ls_control.
        READ TABLE it_control[] INTO ls_control WITH KEY guid = ls_prd_version-guid.
        IF sy-subrc = 0.
          APPEND ls_control TO lt_control[].
        ENDIF.
      ENDLOOP.

      LOOP AT lt_prd_version_old[] INTO ls_prd_version.
        CLEAR ls_control.
        READ TABLE it_control_old[] INTO ls_control WITH KEY guid = ls_prd_version-guid.
        IF sy-subrc = 0.
          APPEND ls_control TO lt_control_old[].
        ENDIF.
      ENDLOOP.

* Old Control Table
      lt_ycontrol[] = lt_control_old[].

* New Control Table
      lt_xcontrol[] = lt_control[].

      lv_ci_control = 'U'.

    ENDIF.


    CLEAR: lt_xprd_version[], lt_yprd_version[], lv_ci_prd_version.
    IF iv_ci_prd_version = abap_true.

* Old Product Version Table
      lt_yprd_version[] = lt_prd_version_old[].

* New Product Version Table
      lt_xprd_version[] = lt_prd_version[].

      lv_ci_prd_version = 'U'.
    ENDIF.



    CLEAR: lt_xver_status[], lt_yver_status[], lv_ci_ver_status.
    IF iv_ci_ver_status = abap_true.

* Old Version Status Table
      lt_yver_status[] = lt_ver_status_old[].

* New Version Status Table
      lt_xver_status[] = lt_ver_status[].

      lv_ci_ver_status = 'U'.
    ENDIF.


    CLEAR: lt_xreg_hdr[], lt_yreg_hdr[], lv_ci_reg_hdr.
    IF iv_ci_reg_hdr = abap_true.

      CLEAR ls_reg_data.
      READ TABLE it_reg_data_old INTO ls_reg_data
      WITH KEY idno = ls_prod_data-idno.
      IF sy-subrc = 0.
        lt_reg_hdr_old[] = ls_reg_data-zarn_reg_hdr[].
      ENDIF.

      CLEAR ls_reg_data.
      READ TABLE it_reg_data INTO ls_reg_data
      WITH KEY idno = ls_prod_data-idno.
      IF sy-subrc = 0.
        lt_reg_hdr[] = ls_reg_data-zarn_reg_hdr[].
      ENDIF.

* Old Version Status Table
      lt_yreg_hdr[] = lt_reg_hdr_old[].

* New Version Status Table
      lt_xreg_hdr[] = lt_reg_hdr[].

      lv_ci_reg_hdr = 'U'.
    ENDIF.

    IF iv_ci_reg_data = abap_true.
      PERFORM update_regional_data_chglog USING     ls_prod_data-idno
                                                    it_reg_data_old
                                                    it_reg_data
                                           CHANGING lt_yzarn_reg_str
                                                    lt_xzarn_reg_str
                                                    lv_ci_zarn_reg_str.
    ENDIF.

    CLEAR lv_objectid.
    lv_objectid = ls_prod_data-idno.

    lv_chg_flag = 'U'.

* Create Change Document
    CALL FUNCTION 'ZARN_CTRL_REGNL_WRITE_DOC_CUST'
      EXPORTING
        objectid                   = lv_objectid
        tcode                      = sy-tcode
        utime                      = sy-uzeit
        udate                      = sy-datum
        username                   = sy-uname
**         PLANNED_CHANGE_NUMBER      = ' '
        object_change_indicator    = lv_chg_flag
**         PLANNED_OR_REAL_CHANGES    = ' '
**         NO_CHANGE_POINTERS         = ' '
        upd_icdtxt_zarn_ctrl_regnl = ' '
        upd_zarn_control           = lv_ci_control
        upd_zarn_prd_version       = lv_ci_prd_version
        upd_zarn_reg_artlink       = ' '
        upd_zarn_reg_banner        = ' '
        upd_zarn_reg_dc_sell       = ' '
        upd_zarn_reg_ean           = ' '
        upd_zarn_reg_hdr           = lv_ci_reg_hdr
        upd_zarn_reg_hsno          = ' '
        upd_zarn_reg_lst_prc       = ' '
        upd_zarn_reg_onlcat        = ' '
        upd_zarn_reg_pir           = ' '
        upd_zarn_reg_prfam         = ' '
        upd_zarn_reg_rrp           = ' '
        upd_zarn_reg_std_ter       = ' '
        upd_zarn_reg_str           = lv_ci_zarn_reg_str
        xzarn_reg_str              = lt_xzarn_reg_str
        yzarn_reg_str              = lt_yzarn_reg_str
        upd_zarn_reg_txt           = ' '
        upd_zarn_reg_uom           = ' '
        upd_zarn_ver_status        = lv_ci_ver_status
      TABLES
        icdtxt_zarn_ctrl_regnl     = lt_cdtxt[]
        xzarn_control              = lt_xcontrol[]           " lt_control[]
        yzarn_control              = lt_ycontrol[]          " lt_control_old[]
        xzarn_prd_version          = lt_xprd_version[]       " lt_prd_version[]
        yzarn_prd_version          = lt_yprd_version[]       " lt_prd_version_old[]
        xzarn_reg_artlink          = lt_xzarn_reg_artlink[]
        yzarn_reg_artlink          = lt_yzarn_reg_artlink[]
        xzarn_reg_banner           = lt_xreg_banner[]
        yzarn_reg_banner           = lt_yreg_banner[]
        xzarn_reg_cluster          = lt_xreg_cluster
        yzarn_reg_cluster          = lt_yreg_cluster
        xzarn_reg_dc_sell          = lt_xreg_dc_sell[]
        yzarn_reg_dc_sell          = lt_yreg_dc_sell[]
        xzarn_reg_ean              = lt_xreg_ean[]
        yzarn_reg_ean              = lt_yreg_ean[]
        xzarn_reg_hdr              = lt_xreg_hdr[]
        yzarn_reg_hdr              = lt_yreg_hdr[]
        xzarn_reg_hsno             = lt_xreg_hsno[]
        yzarn_reg_hsno             = lt_yreg_hsno[]
        xzarn_reg_lst_prc          = lt_xreg_lst_prc[]
        yzarn_reg_lst_prc          = lt_yreg_lst_prc[]
        xzarn_reg_onlcat           = lt_xzarn_reg_onlcat[]
        yzarn_reg_onlcat           = lt_yzarn_reg_onlcat[]
        xzarn_reg_pir              = lt_xreg_pir[]
        yzarn_reg_pir              = lt_yreg_pir[]
        xzarn_reg_prfam            = lt_xreg_prfam[]
        yzarn_reg_prfam            = lt_yreg_prfam[]
        xzarn_reg_rrp              = lt_xreg_rrp[]
        yzarn_reg_rrp              = lt_yreg_rrp[]
        xzarn_reg_std_ter          = lt_xreg_std_ter[]
        yzarn_reg_std_ter          = lt_yreg_std_ter[]
        xzarn_reg_txt              = lt_xreg_txt[]
        yzarn_reg_txt              = lt_yreg_txt[]
        xzarn_reg_uom              = lt_xreg_uom[]
        yzarn_reg_uom              = lt_yreg_uom[]
        xzarn_ver_status           = lt_xver_status[]       " lt_ver_status[]
        yzarn_ver_status           = lt_yver_status[].       " lt_ver_status_old[].

  ENDLOOP.  " LOOP AT it_prod_data INTO ls_prod_data


ENDFUNCTION.

FORM update_regional_data_chglog USING iv_idno              TYPE zarn_idno
                                       it_reg_data_old      TYPE ztarn_reg_data
                                       it_reg_data_new      TYPE ztarn_reg_data
                              CHANGING ct_yzarn_reg_str     TYPE yzarn_reg_strs
                                       ct_xzarn_reg_str     TYPE yzarn_reg_strs
                                       cv_ci_zarn_reg_str   TYPE cdflag.

  CONSTANTS: lc_old_prefix  TYPE string VALUE 'CT_Y',
             lc_new_prefix  TYPE string VALUE 'CT_X',
             lc_flag_prefix TYPE string VALUE 'CV_CI_'.

  DATA: lo_struct_def     TYPE REF TO cl_abap_structdescr,
        ls_reg_tables     TYPE zsarn_reg_table_data,
        lv_old_data_table TYPE string,
        lv_new_data_table TYPE string,
        lv_ci_flag_field  TYPE string.

  FIELD-SYMBOLS: <lv_ci_flag>        TYPE cdflag,
                 <lt_itab_reg_table> TYPE ANY TABLE,
                 <lt_reg_table>      TYPE ANY TABLE.

  CLEAR: cv_ci_zarn_reg_str.

  lo_struct_def ?= cl_abap_structdescr=>describe_by_data( ls_reg_tables ).
  DATA(lt_components) = lo_struct_def->get_components( ).

  LOOP AT it_reg_data_old INTO DATA(ls_reg_data) WHERE idno = iv_idno.

    " Each component of the structure is a table. Process only given tables, in changing parameters,
    " using the naming convention
    LOOP AT lt_components INTO DATA(ls_component).

      lv_old_data_table = |{ lc_old_prefix }{ ls_component-name }|.
      lv_ci_flag_field  = |{ lc_flag_prefix }{ ls_component-name }|.

      ASSIGN (lv_ci_flag_field)  TO <lv_ci_flag>.
      IF sy-subrc <> 0. " Change Flag not passed in changing parameter
        CONTINUE.
      ENDIF.

      ASSIGN (lv_old_data_table) TO <lt_reg_table>.
      IF sy-subrc <> 0. " Internal table not passed using changing
        CONTINUE.
      ENDIF.

      ASSIGN COMPONENT ls_component-name OF STRUCTURE ls_reg_data-reg_tables TO <lt_itab_reg_table>.
      MOVE-CORRESPONDING <lt_itab_reg_table> TO <lt_reg_table>.
      <lv_ci_flag> = 'U'.
    ENDLOOP.

  ENDLOOP.

  LOOP AT it_reg_data_new INTO ls_reg_data WHERE idno = iv_idno.

    " Each component of the structure is a table. Process only given tables, in changing parameters,
    " using the naming convention
    LOOP AT lt_components INTO ls_component.

      lv_new_data_table = |{ lc_new_prefix }{ ls_component-name }|.
      lv_ci_flag_field  = |{ lc_flag_prefix }{ ls_component-name }|.

      ASSIGN (lv_ci_flag_field) TO <lv_ci_flag>.
      IF sy-subrc <> 0. " Change Flag not passed in changing parameter
        CONTINUE.
      ENDIF.

      ASSIGN (lv_new_data_table) TO <lt_reg_table>.
      IF sy-subrc <> 0. " Internal table not passed using changing
        CONTINUE.
      ENDIF.

      ASSIGN COMPONENT ls_component-name OF STRUCTURE ls_reg_data-reg_tables TO <lt_itab_reg_table>.
      MOVE-CORRESPONDING <lt_itab_reg_table> TO <lt_reg_table>.
      <lv_ci_flag> = 'U'.
    ENDLOOP.

  ENDLOOP.

ENDFORM.
