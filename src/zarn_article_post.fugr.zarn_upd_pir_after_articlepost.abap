FUNCTION zarn_upd_pir_after_articlepost.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(IS_EINA_NEW) TYPE  EINA OPTIONAL
*"     REFERENCE(IS_EINE_NEW) TYPE  EINE OPTIONAL
*"     REFERENCE(IS_EINA_OLD) TYPE  EINA OPTIONAL
*"     REFERENCE(IS_EINE_OLD) TYPE  EINE OPTIONAL
*"     REFERENCE(IV_INFNR) TYPE  INFNR OPTIONAL
*"  CHANGING
*"     REFERENCE(CT_RETURN) TYPE  BAPIRETURN1_TABTYPE OPTIONAL
*"----------------------------------------------------------------------


  DATA : ls_eina_new     TYPE eina,
         ls_eine_new     TYPE eine,
         ls_eina_old     TYPE eina,
         ls_eine_old     TYPE eine,
         ls_eina         TYPE eina,

         lv_arena        TYPE char1,
         lv_cond_unit    TYPE pricondunit,
         lv_msehi        TYPE msehi,
         lv_menge        TYPE zdequan,
         lv_menge_f      TYPE f,
         lv_output       TYPE i,
         lv_nomi         TYPE dzaehl,
         lv_deno         TYPE nennr,

         ls_weine        TYPE weine,
         lt_cdtxt        TYPE TABLE OF cdtxt,
         lt_xlfei        TYPE TABLE OF ulfei,
         lt_ylfei        TYPE TABLE OF ulfei,
         lv_objectid     TYPE cdhdr-objectid,
         lv_date         TYPE cdhdr-udate,
         lv_time         TYPE cdhdr-utime,
         lv_upd_pir      TYPE flag,
         lv_spl_upd      TYPE flag,
         lv_tabix        TYPE sy-tabix,

         ls_pir_key      TYPE zsarn_key,
         lt_pir_general  TYPE wrf_bapieina_tty,
         ls_pir_general  TYPE bapieina,
         lt_pir_purchorg TYPE wrf_bapieine_tty,
         ls_pir_purchorg TYPE wrfbapieine,
         ls_return       TYPE bapireturn1,

         lt_eina_new     TYPE mmpur_t_eina,
         ls_eina_infnr   TYPE eina,
         lt_eine_new     TYPE mmpur_t_eine,
         lt_eina_old     TYPE mmpur_t_eina,
         lt_eine_old     TYPE mmpur_t_eine,
         lv_infnr        TYPE infnr,

         ls_mt06e        TYPE mt06e,
         lt_eina_db      TYPE TABLE OF eina.

* Get Flag for Article Posting for AReNa
  CLEAR: lv_arena, ls_pir_key, lt_pir_general[], lt_pir_purchorg[].
  CALL FUNCTION 'ZARN_SET_GET_ARTICLE_POST'
    EXPORTING
      iv_mode                = 'R'
    IMPORTING
      ev_arena               = lv_arena
      es_pir_key             = ls_pir_key
      et_inforecord_general  = lt_pir_general[]
      et_inforecord_purchorg = lt_pir_purchorg[]
      et_eina_new            = lt_eina_new[]
      et_eine_new            = lt_eine_new[]
      et_eina_old            = lt_eina_old[]
      et_eine_old            = lt_eine_old[].



  " If FM is called in AReNA Article Posting execution only then continue
  CHECK lv_arena IS NOT INITIAL.





  LOOP AT lt_eina_new[] INTO ls_eina_infnr.

    lv_infnr = ls_eina_infnr-infnr.



*    IF lv_infnr IS INITIAL. "is_eina_new-infnr IS INITIAL.
*      "should not happen
*      RETURN.
*    ENDIF.

    CLEAR: lv_upd_pir, lv_spl_upd.

    CLEAR: ls_eina_old, ls_eina_new.
    IF lv_infnr IS INITIAL.
      "read the values for OLD from DB
      SELECT SINGLE * FROM eina INTO ls_eina_old  "#EC CI_SEL_NESTED
        WHERE matnr = ls_eina_infnr-matnr
          AND lifnr = ls_eina_infnr-lifnr.
*      infnr EQ lv_infnr. "is_eina_new-infnr.
    ELSE.
      "read the values for OLD from DB
      SELECT SINGLE * FROM eina INTO ls_eina_old   "#EC CI_SEL_NESTED
        WHERE infnr EQ lv_infnr. "is_eina_new-infnr.
    ENDIF.

*    "read the values for OLD from DB
*    SELECT SINGLE * FROM eina INTO ls_eina_old
*      WHERE matnr = ls_eina_infnr-matnr
*        and lifnr = ls_eina_infnr-lifnr.
**      infnr EQ lv_infnr. "is_eina_new-infnr.
    IF ls_eina_old IS INITIAL.
      "should not happen
      CONTINUE.
    ENDIF.

    ls_eina_new = ls_eina_old.

    lv_infnr = ls_eina_old-infnr.


    CLEAR: ls_eine_old, ls_eine_new.
    SELECT SINGLE * FROM eine INTO ls_eine_old   "#EC CI_SEL_NESTED
      WHERE infnr EQ ls_eina_old-infnr "is_eina_new-infnr
        AND ekorg EQ '9999' "is_eine_new-ekorg
        AND esokz EQ '0' "is_eine_new-esokz
        AND werks EQ space.
    IF ls_eine_old IS INITIAL.
      "should not happen
      CONTINUE.
    ENDIF.
    ls_eine_new = ls_eine_old.




    CLEAR ls_return.
    READ TABLE ct_return[] INTO ls_return
    WITH KEY type   = 'E'
             id     = '06'
             number = '765'.
    IF ls_pir_key IS NOT INITIAL AND ls_return IS NOT INITIAL.

      lv_tabix = sy-tabix.

      CLEAR: ls_pir_general, ls_pir_purchorg.
      READ TABLE lt_pir_general[]  INTO ls_pir_general
        WITH KEY material = ls_eina_infnr-matnr
                 vendor   = ls_eina_infnr-lifnr.
      READ TABLE lt_pir_purchorg[] INTO ls_pir_purchorg
        WITH KEY material = ls_eina_infnr-matnr
                 vendor   = ls_eina_infnr-lifnr.

*      READ TABLE lt_pir_general[]  INTO ls_pir_general  INDEX 1.
*      READ TABLE lt_pir_purchorg[] INTO ls_pir_purchorg INDEX 1.


* Build EINA
      ls_eina_new-matnr = ls_pir_general-material.
      ls_eina_new-lifnr = ls_pir_general-vendor.
      ls_eina_new-erdat = ls_pir_general-created_at.
      ls_eina_new-ernam = ls_pir_general-created_by.
      ls_eina_new-meins = ls_pir_general-po_unit.
      ls_eina_new-vabme = ls_pir_general-var_ord_un.
      ls_eina_new-relif = ls_pir_general-norm_vend.
      ls_eina_new-umrez = ls_pir_general-conv_num1.
      ls_eina_new-umren = ls_pir_general-conv_den1.
      ls_eina_new-idnlf = ls_pir_general-vend_mat.
      ls_eina_new-lifab = ls_pir_general-suppl_from.
      ls_eina_new-lifbi = ls_pir_general-suppl_to.
      ls_eina_new-lmein = ls_pir_general-base_uom.

*    IF is_eina_old IS NOT INITIAL.
*      ls_eina_new-erdat = is_eina_old-erdat.
*      ls_eina_new-ernam = is_eina_old-ernam.
*    ENDIF.



* Build EINe
      ls_eine_new-erdat = ls_pir_purchorg-created_at.
      ls_eine_new-ernam = ls_pir_purchorg-created_by.
      ls_eine_new-esokz = ls_pir_purchorg-info_type.
      ls_eine_new-loekz = ls_pir_purchorg-delete_ind.
      ls_eine_new-ekgrp = ls_pir_purchorg-pur_group.
      ls_eine_new-waers = ls_pir_purchorg-currency.
      ls_eine_new-minbm = ls_pir_purchorg-min_po_qty.
      ls_eine_new-norbm = ls_pir_purchorg-nrm_po_qty.
      ls_eine_new-aplfz = ls_pir_purchorg-plnd_delry.
      ls_eine_new-netpr = ls_pir_purchorg-net_price.
      ls_eine_new-peinh = ls_pir_purchorg-price_unit.
      ls_eine_new-effpr = ls_pir_purchorg-eff_price.
      ls_eine_new-bprme = ls_pir_purchorg-orderpr_un.
      ls_eine_new-ekkol = ls_pir_purchorg-cond_group.
      ls_eine_new-mwskz = ls_pir_purchorg-tax_code.
*    ls_eine_new-datlb = ls_pir_purchorg-last_po.
      ls_eine_new-prdat = ls_pir_purchorg-price_date.
      ls_eine_new-uebto = ls_pir_purchorg-overdeltol.
      ls_eine_new-untto = ls_pir_purchorg-under_tol.
      ls_eine_new-bpumz = ls_pir_purchorg-conv_num1.
      ls_eine_new-bpumn = ls_pir_purchorg-conv_den1.

*    IF is_eine_old IS NOT INITIAL.
*      ls_eine_new-erdat = is_eine_old-erdat.
*      ls_eine_new-ernam = is_eine_old-ernam.
*    ENDIF.

      lv_spl_upd = lv_upd_pir = abap_true.


    ELSE.

      lv_msehi = ls_eine_new-bprme.

      IF lv_msehi IS NOT INITIAL.

        CLEAR: lv_menge, lv_menge_f, lv_nomi, lv_deno.

        CALL FUNCTION 'ZCMD_CONVERT_MATERIAL_UNIT'
          EXPORTING
            i_matnr  = ls_eina_new-matnr
            i_in_me  = ls_eina_new-meins
            i_out_me = lv_msehi
            i_menge  = '1'
          IMPORTING
            e_menge  = lv_menge.

        lv_menge_f = lv_menge.

        CALL FUNCTION 'CONVERT_TO_FRACTION'
          EXPORTING
            input       = lv_menge_f
          IMPORTING
            nominator   = lv_nomi
            denominator = lv_deno.


        ls_eine_new-bprme = lv_msehi.
        ls_eine_new-bpumz = lv_nomi.
        ls_eine_new-bpumn = lv_deno.

        "TBD - Net Price is not going to be populated when creating PIR
        "standard clears the field
        "check the possibility to update in MODIFY_INFORECORD BADi first
        "if not possible then implement the update here


        lv_upd_pir = abap_true.
      ENDIF. " IF lv_msehi IS NOT INITIAL

    ENDIF.


    IF lv_upd_pir = abap_true.
* Update Inforecord
      CLEAR ls_eina.
      CALL FUNCTION 'ME_UPDATE_INFORECORD'
        EXPORTING
          xeina         = ls_eina_new
          xeine         = ls_eine_new
          yeina         = ls_eina_old
          yeine         = ls_eine_old
          reg_eina      = ls_eina
        EXCEPTIONS
          error_message = 1.
      IF sy-subrc = 0.

* INS Begin of Change INC5272626 JKH 23.01.2017
* correct KONP - kumza, kumne with correct values as of eine-bpumn and bpumz
* if konp-meins is filled.

        COMMIT WORK AND WAIT.

***   use MEII function modules to create/change inforecords
**        PERFORM read_mt06e USING ls_infrec_keys-material
**                        CHANGING ls_mt06e
**                                 ls_eina_new.

        CALL FUNCTION 'ME_MAINTAIN_INFORECORD'
          EXPORTING
            activity           = 'V'
            i_eina             = ls_eina_new
            i_eine             = ls_eine_new
            o_eina             = ls_eina_old
            o_eine             = ls_eine_old
            i_no_suppose       = 'X'
            i_no_material_read = 'X'
            i_mt06e            = ls_mt06e
            i_vorga            = 'A'
          IMPORTING
            e_eina             = ls_eina_new
          EXCEPTIONS
            error_message      = 1
            OTHERS             = 2.
        IF sy-subrc = 0.

          CLEAR: lt_eina_db[].
          CALL FUNCTION 'ME_POST_INFORECORD'
            TABLES
              t_eina_i      = lt_eina_db
            EXCEPTIONS
              error_message = 1
              OTHERS        = 2.
          IF sy-subrc = 0.

            CALL FUNCTION 'BAPI_TRANSACTION_COMMIT'
              EXPORTING
                wait = abap_true.
* INS End of Change INC5272626 JKH 23.01.2017


            IF lv_spl_upd = abap_true.
              DELETE ct_return[] INDEX lv_tabix.

              CLEAR ls_return.
              ls_return-type       = 'S'.
              ls_return-id         = 'WRF_MATERIAL'.
              ls_return-number     = '035'.

              MESSAGE ID ls_return-id TYPE ls_return-type NUMBER ls_return-number
              WITH ls_eine_new-infnr ls_eine_new-ekorg ls_eine_new-esokz ls_eine_new-werks
              INTO ls_return-message.

*ls_return-LOG_NO     =
*ls_return-LOG_MSG_NO =
              ls_return-message_v1 = ls_eine_new-infnr.
              ls_return-message_v2 = ls_eine_new-ekorg.
              ls_return-message_v3 = ls_eine_new-esokz.
              ls_return-message_v4 = ls_eine_new-werks.
              APPEND ls_return TO ct_return[].
            ENDIF.  " IF lv_spl_upd = abap_true


            lv_objectid = ls_eina_new-infnr. " Inforecord number
            lv_date     = sy-datum.
            lv_time     = sy-uzeit.

* Maintain Change Document
            CALL FUNCTION 'INFOSATZ_WRITE_DOCUMENT'
              EXPORTING
                objectid            = lv_objectid
                tcode               = sy-tcode
                utime               = lv_time
                udate               = lv_date
                username            = sy-uname
                upd_icdtxt_infosatz = space
                n_eina              = ls_eina_new
                o_yeina             = ls_eina_old
                upd_eina            = 'U'
                n_eine              = ls_eine_new
                o_yeine             = ls_eine_old
                o_weine             = ls_weine
                n_weine             = ls_weine
                upd_eine            = 'U'
                upd_lfei            = space
              TABLES
                icdtxt_infosatz     = lt_cdtxt
                xlfei               = lt_xlfei
                ylfei               = lt_ylfei.

          ENDIF.  " IF sy-subrc = 0 - ME_MAINTAIN_INFORECORD  ++INC5272626 JKH 23.01.2017
        ENDIF.  " IF sy-subrc = 0 - ME_POST_INFORECORD        ++INC5272626 JKH 23.01.2017
      ENDIF.  " IF sy-subrc = 0 - ME_UPDATE_INFORECORD
    ENDIF.  " IF lv_upd_pir = abap_true


  ENDLOOP.  " LOOP AT lt_eina_new[] INTO ls_eina_infnr

ENDFUNCTION.
