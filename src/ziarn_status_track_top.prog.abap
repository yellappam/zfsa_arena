*&---------------------------------------------------------------------*
*& Include ZIARN_STATUS_TRACK_TOP                            Report ZRARN_STATUS_TRACK
*&
*&---------------------------------------------------------------------*

REPORT zrarn_status_track MESSAGE-ID  zarena_msg.


TABLES:
  zarn_gtin_var,
  zarn_products,
  zarn_reg_hdr,
  zarn_control,
  zarn_prd_version.

CONSTANTS:
  gc_save             TYPE char01    VALUE 'A'. "The user may save all types of a layout

DATA:
      gs_variant          TYPE disvariant.
