* regenerated at 25.05.2016 11:50:09 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_PACKMAT_TTOP.                " Global Data
  INCLUDE LZARN_PACKMAT_TUXX.                " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_PACKMAT_TF...                " Subroutines
* INCLUDE LZARN_PACKMAT_TO...                " PBO-Modules
* INCLUDE LZARN_PACKMAT_TI...                " PAI-Modules
* INCLUDE LZARN_PACKMAT_TE...                " Events
* INCLUDE LZARN_PACKMAT_TP...                " Local class implement.
* INCLUDE LZARN_PACKMAT_TT99.                " ABAP Unit tests
  INCLUDE LZARN_PACKMAT_TF00                      . " subprograms
  INCLUDE LZARN_PACKMAT_TI00                      . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
