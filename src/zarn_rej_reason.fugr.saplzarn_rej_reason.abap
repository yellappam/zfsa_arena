* regenerated at 23.02.2016 11:39:19 by  C90001448
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_REJ_REASONTOP.               " Global Data
  INCLUDE LZARN_REJ_REASONUXX.               " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_REJ_REASONF...               " Subroutines
* INCLUDE LZARN_REJ_REASONO...               " PBO-Modules
* INCLUDE LZARN_REJ_REASONI...               " PAI-Modules
* INCLUDE LZARN_REJ_REASONE...               " Events
* INCLUDE LZARN_REJ_REASONP...               " Local class implement.
* INCLUDE LZARN_REJ_REASONT99.               " ABAP Unit tests
  INCLUDE LZARN_REJ_REASONF00                     . " subprograms
  INCLUDE LZARN_REJ_REASONI00                     . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
