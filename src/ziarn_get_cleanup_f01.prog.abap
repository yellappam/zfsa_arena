*&---------------------------------------------------------------------*
*&  Include           ZIARN_GET_CLEANUP_F01
*&---------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*&      Form  GET_CLEANUP
*&---------------------------------------------------------------------*
FORM get_cleanup.

  DATA: lt_idno  TYPE STANDARD TABLE OF zarn_idno,
        lv_count TYPE sy-dbcnt.

  FIELD-SYMBOLS: <ls_idno> LIKE LINE OF lt_idno.

  SELECT idno
    INTO TABLE lt_idno
    FROM zarn_get_cleanup
    WHERE idno  IN s_idno
    AND   uname IN s_uname
    GROUP BY idno.

  FORMAT RESET.
  IF lt_idno[] IS INITIAL.
    WRITE:/ 'No entries to remove'.
  ELSE.
    DESCRIBE TABLE lt_idno LINES lv_count.
    WRITE:/ lv_count, 'entries to remove'.
    LOOP AT lt_idno ASSIGNING <ls_idno>.
      PERFORM clean_tables USING <ls_idno>.
    ENDLOOP.
  ENDIF.

ENDFORM.                    " GET_CLEANUP

*&---------------------------------------------------------------------*
*&      Form  clean_tables
*&---------------------------------------------------------------------*
FORM clean_tables USING pv_idno TYPE zarn_idno.

  STATICS: lt_tables TYPE STANDARD TABLE OF zarn_table_mastr.

  DATA: lv_subrc   TYPE sy-subrc,
        lv_count   TYPE sy-dbcnt,
        ls_product TYPE zarn_products.

  FIELD-SYMBOLS: <ls_table> LIKE LINE OF lt_tables.

  IF lt_tables[] IS INITIAL.
    SELECT *
      INTO TABLE lt_tables
      FROM zarn_table_mastr
      WHERE arena_table_type EQ 'N'.

*   Control tables that can be deleted
    APPEND INITIAL LINE TO lt_tables ASSIGNING <ls_table>.
    <ls_table>-arena_table = 'ZARN_PRD_VERSION'.
    APPEND INITIAL LINE TO lt_tables ASSIGNING <ls_table>.
    <ls_table>-arena_table = 'ZARN_VER_STATUS'.
  ENDIF.

  ULINE.
  FORMAT RESET.
  FORMAT COLOR COL_HEADING.
  WRITE:/ 'Removing IDNO', pv_idno.

  SELECT SINGLE *
    INTO ls_product
    FROM zarn_products
    WHERE idno             EQ pv_idno
    AND   fsni_icare_value NE space.
  IF sy-subrc EQ 0.
    FORMAT COLOR COL_GROUP.
    WRITE:/ 'IDNO', pv_idno, 'has been I-Care so will not be removed'.
  ELSE.
    lv_subrc = 0.
    LOOP AT lt_tables ASSIGNING <ls_table>.
      FORMAT RESET.
      CLEAR lv_count.
      SELECT COUNT(*) FROM (<ls_table>-arena_table)
        INTO lv_count
        WHERE idno EQ pv_idno.
      IF p_test NE space.
        IF lv_count GT 0.
          WRITE:/ 'Would delete', lv_count, 'rows from', <ls_table>-arena_table.
        ELSE.
          WRITE:/ 'No rows found in', <ls_table>-arena_table.
        ENDIF.
      ELSE.
        IF lv_count GT 0.
          DELETE FROM (<ls_table>-arena_table)
            WHERE idno EQ pv_idno.
          IF sy-subrc NE 0.
            lv_subrc = 4.
            FORMAT COLOR COL_NEGATIVE.
            WRITE:/ 'Could not delete from table', <ls_table>-arena_table.
          ENDIF.
        ENDIF.
      ENDIF.
    ENDLOOP.
    COMMIT WORK AND WAIT.
  ENDIF.
  IF p_test EQ space.
    IF lv_subrc EQ 0.
      DELETE FROM zarn_get_cleanup
        WHERE idno EQ pv_idno.
      COMMIT WORK AND WAIT.
    ENDIF.
  ENDIF.

ENDFORM.                    "clean_tables
