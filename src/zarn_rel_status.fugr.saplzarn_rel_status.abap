* regenerated at 23.02.2016 08:13:04 by  C90001448
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_REL_STATUSTOP.               " Global Data
  INCLUDE LZARN_REL_STATUSUXX.               " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_REL_STATUSF...               " Subroutines
* INCLUDE LZARN_REL_STATUSO...               " PBO-Modules
* INCLUDE LZARN_REL_STATUSI...               " PAI-Modules
* INCLUDE LZARN_REL_STATUSE...               " Events
* INCLUDE LZARN_REL_STATUSP...               " Local class implement.
* INCLUDE LZARN_REL_STATUST99.               " ABAP Unit tests
  INCLUDE LZARN_REL_STATUSF00                     . " subprograms
  INCLUDE LZARN_REL_STATUSI00                     . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
