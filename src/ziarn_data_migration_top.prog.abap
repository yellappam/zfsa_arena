*&---------------------------------------------------------------------*
*&  Include           ZIARN_GET_CLEANUP_TOP
*&---------------------------------------------------------------------*

TYPES: BEGIN OF ty_s_log,
         msgty   TYPE char10,
         message TYPE char100,
         color   TYPE char1,
       END OF ty_s_log,
       ty_t_log TYPE STANDARD TABLE OF ty_s_log.

DATA: "ZARN_PRODUCTS -> ZARN_NET_QTY
  lt_zarn_products        TYPE STANDARD TABLE OF zarn_products,
  ls_zarn_products        TYPE zarn_products,
  lt_zarn_net_qty         TYPE STANDARD TABLE OF zarn_net_qty,
  lt_zarn_net_qty_tmp     TYPE STANDARD TABLE OF zarn_net_qty,
  ls_zarn_net_qty         TYPE zarn_net_qty,
  "ZARN_NUTRIENTS -> ZARN_SERVE_SIZE
  lt_zarn_nutrients       TYPE STANDARD TABLE OF zarn_nutrients,
  ls_zarn_nutrients       TYPE zarn_nutrients,
  lt_zarn_serve_size      TYPE STANDARD TABLE OF zarn_serve_size,
  lt_zarn_serve_size_tmp  TYPE STANDARD TABLE OF zarn_serve_size,
  ls_zarn_serve_size      TYPE zarn_serve_size,
  "ZARN_ORGANISM -> ZARN_BIOORGANISM
  lt_zarn_organism        TYPE STANDARD TABLE OF zarn_organism,
  ls_zarn_organism        TYPE zarn_organism,
  lt_zarn_bioorganism     TYPE STANDARD TABLE OF zarn_bioorganism,
  lt_zarn_bioorganism_tmp TYPE STANDARD TABLE OF zarn_bioorganism,
  ls_zarn_bioorganism     TYPE zarn_bioorganism,
  "Database update count
  lv_updatecount          TYPE sy-dbcnt,
  lv_failcount            TYPE sy-dbcnt,
  lv_existcount           TYPE sy-dbcnt,
  lv_index                TYPE sy-tabix,
  lv_idno_char            TYPE zarn_net_qty-idno,
  lv_subrc                TYPE i,
  lt_log                  TYPE ty_t_log,
  ls_log                  TYPE ty_s_log,
  lt_file_tab             TYPE TABLE OF file_table,
  ls_file_tab             TYPE file_table,
  lv_packsize             TYPE i,
  lv_idno                 TYPE zarn_products-idno,
  lv_version              TYPE zarn_products-version,
  lv_line_counter         TYPE i.
