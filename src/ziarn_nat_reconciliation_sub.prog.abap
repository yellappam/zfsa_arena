*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_NAT_RECONCILIATION_SUB
*&---------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*&      Form  RESTRICT_SELECT_OPTIONS
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
FORM restrict_select_options .

  DATA: ls_restrict TYPE sscr_restrict,
        ls_opt_list TYPE sscr_opt_list,
        ls_ass_tab  TYPE sscr_ass.


  ls_opt_list-name       = 'OBJ1'.
  ls_opt_list-options-eq = 'X'.
  APPEND ls_opt_list TO ls_restrict-opt_list_tab[].

  ls_ass_tab-kind    = 'S'.
  ls_ass_tab-sg_main = 'I'.
  ls_ass_tab-sg_addy = ''.
  ls_ass_tab-op_main = 'OBJ1'.
  ls_ass_tab-op_addy = ''.

  ls_ass_tab-name    = 'S_FANID'.  APPEND ls_ass_tab TO ls_restrict-ass_tab.







  CALL FUNCTION 'SELECT_OPTIONS_RESTRICT'
    EXPORTING
*     PROGRAM                =
      restriction            = ls_restrict
*     DB                     = ' '
    EXCEPTIONS
      too_late               = 1
      repeated               = 2
      selopt_without_options = 3
      selopt_without_signs   = 4
      invalid_sign           = 5
      empty_option_list      = 6
      invalid_kind           = 7
      repeated_kind_a        = 8
      OTHERS                 = 9.
  IF sy-subrc <> 0.
* Implement suitable error handling here
  ENDIF.


ENDFORM.  " RESTRICT_SELECT_OPTIONS
*&---------------------------------------------------------------------*
*&      Form  GET_DATA_FROM_HYBRIS
*&---------------------------------------------------------------------*
* Get data from Hybris
*----------------------------------------------------------------------*
FORM get_data_from_hybris CHANGING pct_prd_version TYPE ztarn_prd_version
                                   pct_ver_status  TYPE ztarn_ver_status
                                   pct_cc_det      TYPE ztarn_cc_det
                                   pct_prod_data   TYPE ztarn_prod_data.


  DATA: ls_search      TYPE zmaster_product_search1,
        ls_results     TYPE zmaster_product_with_price_cre,

        lo_hybris      TYPE REF TO zmaster_co_product_search_requ,
        lo_root        TYPE REF TO cx_root,
        lv_msg         TYPE string,
        lt_message_all TYPE bal_t_msg,
        lt_message     TYPE bal_t_msg,
        ls_message     TYPE bal_s_msg,
        lv_log_no      TYPE balognr.



  CHECK s_fanid[] IS NOT INITIAL.

  CLEAR: pct_prd_version[], pct_ver_status[], pct_cc_det[], pct_prod_data[].

  CLEAR ls_search.

  LOOP AT s_fanid.
    APPEND s_fanid-low TO ls_search-product_search-fan[].
  ENDLOOP.

  SORT ls_search-product_search-fan[].
  DELETE ADJACENT DUPLICATES FROM ls_search-product_search-fan[] COMPARING ALL FIELDS.



  TRY.
      CREATE OBJECT lo_hybris.
      IF lo_hybris IS BOUND.

        CLEAR ls_results.
        CALL METHOD lo_hybris->product_search_request_out
          EXPORTING
            output = ls_search
          IMPORTING
            input  = ls_results.

        IF ls_results-master_product_with_price_crea-product[] IS NOT INITIAL.

          CALL FUNCTION 'ZARN_INBOUND_DATA_PROCESSING'
            EXPORTING
              it_product     = ls_results-master_product_with_price_crea-product
              is_admin       = ls_results-master_product_with_price_crea-admin
              iv_id_type     = 'C'
            IMPORTING
              et_return      = lt_message[]
              ev_slg1_log_no = lv_log_no
              et_prod_data   = pct_prod_data[]
              et_ver_status  = pct_ver_status[]
              et_prd_version = pct_prd_version[]
              et_cc_det      = pct_cc_det[].

        ENDIF.  " ls_results-product[] IS NOT INITIAL


      ENDIF.  " IF lo_hybris IS BOUND

    CATCH cx_root INTO lo_root.
      lv_msg = lo_root->if_message~get_text( ).

      " Error reading data from Hybris system
      MESSAGE e138(zarena_msg) INTO zcl_message_services=>sv_msg_dummy.
      PERFORM append_message CHANGING lt_message_all.

      CLEAR ls_message.
      ls_message-msgid = 'ZARENA_MSG'.
      ls_message-msgty = 'E'.
      ls_message-msgno = '000'.
      PERFORM split_msg USING lv_msg CHANGING ls_message-msgv1 ls_message-msgv2 ls_message-msgv3 ls_message-msgv4.
      APPEND ls_message TO lt_message_all[].
      PERFORM log_message USING lv_msg.
      WHILE NOT lo_root->previous IS INITIAL.
        lo_root = lo_root->previous.
        lv_msg = lo_root->if_message~get_text( ).
        CLEAR ls_message.
        ls_message-msgid = 'ZARENA_MSG'.
        ls_message-msgty = 'E'.
        ls_message-msgno = '000'.
        PERFORM split_msg USING lv_msg CHANGING ls_message-msgv1 ls_message-msgv2 ls_message-msgv3 ls_message-msgv4.
        APPEND ls_message TO lt_message_all[].
        PERFORM log_message USING lv_msg.
      ENDWHILE.
  ENDTRY.

  IF lt_message_all[] IS NOT INITIAL.
    PERFORM popup_appl_log USING lt_message_all[].
  ENDIF.

ENDFORM.  " GET_DATA_FROM_HYBRIS

FORM append_message CHANGING ct_message TYPE bal_t_msg.

  APPEND INITIAL LINE TO ct_message ASSIGNING FIELD-SYMBOL(<ls_message>).
  <ls_message>-msgid = sy-msgid.
  <ls_message>-msgno = sy-msgno.
  <ls_message>-msgty = sy-msgty.
  <ls_message>-msgv1 = sy-msgv1.
  <ls_message>-msgv2 = sy-msgv2.
  <ls_message>-msgv3 = sy-msgv3.
  <ls_message>-msgv4 = sy-msgv4.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  BUILD_PRDVER_ALV_DATA
*&---------------------------------------------------------------------*
* Build Product Version ALV data
*----------------------------------------------------------------------*
FORM build_prdver_alv_data  USING    pt_prd_version TYPE ztarn_prd_version
                                     pt_ver_status  TYPE ztarn_ver_status
                                     pt_cc_det      TYPE ztarn_cc_det
                                     pt_prod_data   TYPE ztarn_prod_data
                            CHANGING pct_prdver_alv TYPE ztarn_nat_reco_prdver_alv
                                     pct_idno_data  TYPE ztarn_idno_data.

  DATA: ls_prd_version TYPE zarn_prd_version,
        ls_ver_status  TYPE zarn_ver_status,
        ls_cc_det      TYPE zarn_cc_det,
        ls_prod_data   TYPE zsarn_prod_data,
        ls_products    TYPE zarn_products,
        ls_prdver_alv  TYPE zsarn_nat_reco_prdver_alv,
        ls_idno_data   TYPE zsarn_idno_data.



  CLEAR: pct_idno_data[], pct_prdver_alv.

* Get Product Version data
  SELECT r~idno r~version_status AS nat_status r~id_type
         v~current_ver v~previous_ver v~latest_ver v~article_ver
         p~fan_id p~fs_short_descr
    INTO CORRESPONDING FIELDS OF TABLE pct_idno_data[]
    FROM zarn_prd_version AS r
    JOIN zarn_ver_status AS v
      ON r~idno    = v~idno
     AND r~version = v~current_ver
    JOIN zarn_products AS p
      ON p~idno    = v~idno
     AND p~version = v~current_ver
    WHERE p~fan_id IN s_fanid[].



  LOOP AT pct_idno_data[] INTO ls_idno_data.
    CLEAR ls_prdver_alv.

    ls_prdver_alv-idno = ls_idno_data-idno.

* Read National Data from Hybris Get - Inbound return
    CLEAR: ls_prod_data, ls_products.
    READ TABLE pt_prod_data[] INTO ls_prod_data
    WITH KEY idno    = ls_idno_data-idno.
    IF sy-subrc = 0.
      READ TABLE ls_prod_data-zarn_products[] INTO ls_products
      WITH KEY idno    = ls_prod_data-idno
               version = ls_prod_data-version.
      IF sy-subrc = 0.
        ls_prdver_alv-fan_id         = ls_products-fan_id.
        ls_prdver_alv-fs_short_descr = ls_products-fs_short_descr.
        ls_prdver_alv-status         = icon_led_green.
      ENDIF.  " READ TABLE ls_prod_data-zarn_products[] INTO ls_products
    ELSE.
      ls_prdver_alv-c_version        = ls_idno_data-current_ver.
      ls_prdver_alv-c_id_type        = ls_idno_data-id_type.
      ls_prdver_alv-c_version_status = ls_idno_data-nat_status.
      ls_prdver_alv-fan_id           = ls_idno_data-fan_id.
      ls_prdver_alv-fs_short_descr   = ls_idno_data-fs_short_descr.   "'ERROR - Check Inbound Log'.
      ls_prdver_alv-status           = icon_led_red.
    ENDIF.  " READ TABLE pt_prod_data[] INTO ls_prod_data

* Read CURRENT version data
    CLEAR ls_prd_version.
    READ TABLE pt_prd_version INTO ls_prd_version
    WITH KEY idno    = ls_idno_data-idno
             version = ls_idno_data-current_ver.
    IF sy-subrc = 0.
      ls_prdver_alv-c_version        = ls_prd_version-version.
      ls_prdver_alv-c_id_type        = ls_prd_version-id_type.
      ls_prdver_alv-c_version_status = ls_prd_version-version_status.
    ENDIF.


* Read Change Detail data - read any record to get new and comp with versions
    CLEAR ls_cc_det.
    READ TABLE pt_cc_det[] INTO ls_cc_det
    WITH KEY idno = ls_idno_data-idno.
    IF sy-subrc = 0.

      IF ls_cc_det-national_ver_id IS NOT INITIAL.
* Read NEW version data
        CLEAR ls_prd_version.
        READ TABLE pt_prd_version INTO ls_prd_version
        WITH KEY idno    = ls_cc_det-idno
                 version = ls_cc_det-national_ver_id.
        IF sy-subrc = 0.
          ls_prdver_alv-n_version        = ls_prd_version-version.
          ls_prdver_alv-n_id_type        = ls_prd_version-id_type.
          ls_prdver_alv-n_version_status = ls_prd_version-version_status.
        ENDIF.
      ENDIF.  " IF ls_cc_det-national_ver_id IS NOT INITIAL


      IF ls_cc_det-ver_comp_with IS NOT INITIAL.
* Read COMP WITH version data
        CLEAR ls_prd_version.
        READ TABLE pt_prd_version INTO ls_prd_version
        WITH KEY idno    = ls_cc_det-idno
                 version = ls_cc_det-ver_comp_with.
        IF sy-subrc = 0.
          ls_prdver_alv-cw_version        = ls_prd_version-version.
          ls_prdver_alv-cw_id_type        = ls_prd_version-id_type.
          ls_prdver_alv-cw_version_status = ls_prd_version-version_status.
        ENDIF.
      ENDIF.  " IF ls_cc_det-ver_comp_with IS NOT INITIAL


    ENDIF.  " READ TABLE pt_cc_det[] INTO ls_cc_det


** If Latest Version is NCH then it is success
* - N version as latest version
* - CW ar artuicle version, if no article version then Current if it is approved.
    CLEAR ls_ver_status.
    READ TABLE pt_ver_status[] INTO ls_ver_status
    WITH KEY idno = ls_idno_data-idno.
    IF sy-subrc = 0.
* Read LATEST version data
      CLEAR ls_prd_version.
      READ TABLE pt_prd_version INTO ls_prd_version
      WITH KEY idno    = ls_ver_status-idno
               version = ls_ver_status-latest_ver.
      IF sy-subrc = 0 AND ls_prd_version-version_status = 'NCH'.

        ls_prdver_alv-status           = icon_led_green.

        ls_prdver_alv-n_version        = ls_prd_version-version.
        ls_prdver_alv-n_id_type        = ls_prd_version-id_type.
        ls_prdver_alv-n_version_status = ls_prd_version-version_status.

        IF ls_ver_status-article_ver IS NOT INITIAL.
* Read article version
          CLEAR ls_prd_version.
          READ TABLE pt_prd_version INTO ls_prd_version
          WITH KEY idno    = ls_ver_status-idno
                   version = ls_ver_status-article_ver.
          IF sy-subrc = 0.
            ls_prdver_alv-cw_version        = ls_prd_version-version.
            ls_prdver_alv-cw_id_type        = ls_prd_version-id_type.
            ls_prdver_alv-cw_version_status = ls_prd_version-version_status.
          ENDIF.
        ELSE.
* Current version if approved
          IF ls_prdver_alv-c_version_status = 'APR'.
            ls_prdver_alv-cw_version        = ls_prdver_alv-c_version.
            ls_prdver_alv-cw_id_type        = ls_prdver_alv-c_id_type.
            ls_prdver_alv-cw_version_status = ls_prdver_alv-c_version_status.
          ENDIF.
        ENDIF.
      ENDIF.
    ENDIF.

    APPEND ls_prdver_alv TO pct_prdver_alv[].
  ENDLOOP.

ENDFORM.  " BUILD_PRDVER_ALV_DATA
*&---------------------------------------------------------------------*
*&      Form  SPLIT_MSG
*&---------------------------------------------------------------------*
FORM split_msg  USING    pv_msg   TYPE string
                CHANGING pc_msgv1 TYPE symsgv
                         pc_msgv2 TYPE symsgv
                         pc_msgv3 TYPE symsgv
                         pc_msgv4 TYPE symsgv.

  DATA: lv_msg TYPE string.
  lv_msg = pv_msg.

  CLEAR: pc_msgv1,
         pc_msgv2,
         pc_msgv3,
         pc_msgv4.
  IF strlen( pv_msg ) GT 50.
    pc_msgv1 = lv_msg(50).
    SHIFT lv_msg LEFT BY 50 PLACES.
    IF strlen( lv_msg ) GT 50.
      pc_msgv2 = lv_msg(50).
      SHIFT lv_msg LEFT BY 50 PLACES.
      IF strlen( lv_msg ) GT 50.
        pc_msgv3 = lv_msg(50).
        SHIFT lv_msg LEFT BY 50 PLACES.
        IF strlen( lv_msg ) GT 50.
          pc_msgv4 = lv_msg(50).
          SHIFT lv_msg LEFT BY 50 PLACES.
        ELSE.
          pc_msgv4 = lv_msg.
        ENDIF.
      ELSE.
        pc_msgv3 = lv_msg.
      ENDIF.
    ELSE.
      pc_msgv2 = lv_msg.
    ENDIF.
  ELSE.
    pc_msgv1 = lv_msg.
  ENDIF.

ENDFORM.  " SPLIT_MSG
*&---------------------------------------------------------------------*
*&      Form  LOG_MESSAGE
*&---------------------------------------------------------------------*
FORM log_message USING pv_message TYPE string.

  DATA: ls_log        TYPE bal_s_log,
        lv_log_handle TYPE balloghndl,
        lt_log_handle TYPE bal_t_logh,
        lv_text       TYPE text255.

  CLEAR: ls_log.
  ls_log-object    = 'ZARN'.
  ls_log-subobject = 'ZARN_PROXY'.

  lv_text = pv_message.

  CALL FUNCTION 'BAL_LOG_CREATE'
    EXPORTING
      i_s_log                 = ls_log
    IMPORTING
      e_log_handle            = lv_log_handle
    EXCEPTIONS
      log_header_inconsistent = 1.

  CALL FUNCTION 'BAL_LOG_MSG_ADD_FREE_TEXT'
    EXPORTING
      i_log_handle     = lv_log_handle
      i_msgty          = 'E'
      i_probclass      = '4'
      i_text           = lv_text
    EXCEPTIONS
      log_not_found    = 1
      msg_inconsistent = 2
      log_is_full      = 3.
  APPEND lv_log_handle TO lt_log_handle.
  CALL FUNCTION 'BAL_DB_SAVE'
    EXPORTING
      i_t_log_handle   = lt_log_handle
    EXCEPTIONS
      log_not_found    = 1
      save_not_allowed = 2
      numbering_error  = 3.
  CALL FUNCTION 'BAL_LOG_REFRESH'
    EXPORTING
      i_log_handle  = lv_log_handle
    EXCEPTIONS
      log_not_found = 1
      OTHERS        = 2.

ENDFORM. " LOG_MESSAGE
*&---------------------------------------------------------------------*
*&      Form  DISPLAY_ALV_9001
*&---------------------------------------------------------------------*
* Display ALV 9001
*----------------------------------------------------------------------*
FORM display_alv_9001 .


  CONSTANTS: lc_a        TYPE char01 VALUE 'A'.

* Declaration
  DATA: lo_container  TYPE REF TO cl_gui_custom_container,    "Custom container instance reference
        lt_fieldcat   TYPE        lvc_t_fcat,                 "Field catalog
        ls_layout     TYPE        lvc_s_layo ,                "Layout structure
        ls_disvariant TYPE        disvariant,                 "Variant
        lo_alvgrid    TYPE REF TO cl_gui_alv_grid.


  IF go_alvgrid_9001 IS INITIAL .


*   Create custom container
    CREATE OBJECT go_container_9001
      EXPORTING
        container_name              = 'CC_9001'
      EXCEPTIONS
        cntl_error                  = 1
        cntl_system_error           = 2
        create_error                = 3
        lifetime_error              = 4
        lifetime_dynpro_dynpro_link = 5
        OTHERS                      = 6.
    IF sy-subrc NE 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.


* Create ALV grid
    CREATE OBJECT go_alvgrid_9001
      EXPORTING
        i_parent = go_container_9001
      EXCEPTIONS
        OTHERS   = 5.
    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.


*   Preparing field catalog
    PERFORM prepare_fcat_9001 CHANGING lt_fieldcat[].

*   Preparing layout structure
    PERFORM prepare_layout_9001 CHANGING ls_layout.

* Prepare Variant
    ls_disvariant-report    = sy-repid.
    ls_disvariant-username  = sy-uname.

*   ALV Display
    CALL METHOD go_alvgrid_9001->set_table_for_first_display
      EXPORTING
        i_bypassing_buffer            = abap_true
        is_variant                    = ls_disvariant
        i_save                        = lc_a
        is_layout                     = ls_layout
      CHANGING
        it_outtab                     = gt_prdver_alv[]
        it_fieldcatalog               = lt_fieldcat[]
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.


  ELSE.

*   ALV Refresh Display
    CALL METHOD go_alvgrid_9001->refresh_table_display
      EXPORTING
*       is_stable      = ls_stable
        i_soft_refresh = abap_true
      EXCEPTIONS
        finished       = 1
        OTHERS         = 2.
  ENDIF.




ENDFORM.  " DISPLAY_ALV_9001
*&---------------------------------------------------------------------*
*&      Form  PREPARE_FCAT_9001
*&---------------------------------------------------------------------*
*   Preparing field catalog
*----------------------------------------------------------------------*
FORM prepare_fcat_9001  CHANGING fc_t_fieldcat TYPE lvc_t_fcat.


* Declaration
  DATA: lt_fieldcat TYPE lvc_t_fcat,
        lv_desc     TYPE char40.


  FIELD-SYMBOLS : <ls_fieldcat>    TYPE lvc_s_fcat.

* Prepare field catalog for ALV display by merging structure
  REFRESH: lt_fieldcat[].
  CALL FUNCTION 'LVC_FIELDCATALOG_MERGE'
    EXPORTING
      i_structure_name       = 'ZSARN_NAT_RECO_PRDVER_ALV'
    CHANGING
      ct_fieldcat            = lt_fieldcat[]
    EXCEPTIONS
      inconsistent_interface = 1
      program_error          = 2
      OTHERS                 = 3.
  IF sy-subrc NE 0.
    REFRESH: fc_t_fieldcat.
  ENDIF.




* Looping to make the output field
  LOOP AT lt_fieldcat ASSIGNING <ls_fieldcat>.


    <ls_fieldcat>-tooltip = <ls_fieldcat>-scrtext_l.
    <ls_fieldcat>-col_opt = abap_true.

    lv_desc = <ls_fieldcat>-scrtext_s.


    IF <ls_fieldcat>-fieldname+0(2) EQ 'C_'.
      <ls_fieldcat>-coltext = 'C:' && lv_desc.
      <ls_fieldcat>-tooltip = 'Current:' && <ls_fieldcat>-scrtext_l.
    ELSEIF <ls_fieldcat>-fieldname+0(2) EQ 'N_'.
      <ls_fieldcat>-coltext = 'N:' && lv_desc.
      <ls_fieldcat>-tooltip = 'New:' && <ls_fieldcat>-scrtext_l.
    ELSEIF <ls_fieldcat>-fieldname+0(3) EQ 'CW_'.
      <ls_fieldcat>-coltext = 'CW:' && lv_desc.
      <ls_fieldcat>-tooltip = 'Comp with:' && <ls_fieldcat>-scrtext_l.
    ENDIF.


    CASE <ls_fieldcat>-fieldname.
      WHEN 'IDNO'.
      WHEN 'C_VERSION'.
      WHEN 'C_ID_TYPE'.
      WHEN 'C_VERSION_STATUS'.
      WHEN 'N_VERSION'.
      WHEN 'N_ID_TYPE'.
      WHEN 'N_VERSION_STATUS'.
      WHEN 'CW_VERSION'.
      WHEN 'CW_ID_TYPE'.
      WHEN 'CW_VERSION_STATUS'.
      WHEN 'FAN_ID'.
      WHEN 'FS_SHORT_DESCR'.
      WHEN 'STATUS'.
      WHEN OTHERS.
        DELETE lt_fieldcat WHERE fieldname = <ls_fieldcat>-fieldname.
    ENDCASE.

  ENDLOOP.

  fc_t_fieldcat[] = lt_fieldcat[].


ENDFORM.  " PREPARE_FCAT_9001
*&---------------------------------------------------------------------*
*&      Form  PREPARE_LAYOUT_9001
*&---------------------------------------------------------------------*
*   Preparing layout structure
*----------------------------------------------------------------------*
FORM prepare_layout_9001 CHANGING fc_s_layout TYPE lvc_s_layo.

  fc_s_layout-sel_mode   = 'A'.

ENDFORM.  " PREPARE_LAYOUT_9001
*&---------------------------------------------------------------------*
*&      Form  DISPLAY_ALV_9002
*&---------------------------------------------------------------------*
* Display ALV 9002
*----------------------------------------------------------------------*
FORM display_alv_9002 .



  CONSTANTS: lc_a        TYPE char01 VALUE 'A'.

* Declaration
  DATA: lo_container  TYPE REF TO cl_gui_custom_container,    "Custom container instance reference
        lt_fieldcat   TYPE        lvc_t_fcat,                 "Field catalog
        ls_layout     TYPE        lvc_s_layo ,                "Layout structure
        ls_disvariant TYPE        disvariant,                 "Variant
        lo_alvgrid    TYPE REF TO cl_gui_alv_grid.


  IF go_alvgrid_9002 IS INITIAL .


*   Create custom container
    CREATE OBJECT go_container_9002
      EXPORTING
        container_name              = 'CC_9002'
      EXCEPTIONS
        cntl_error                  = 1
        cntl_system_error           = 2
        create_error                = 3
        lifetime_error              = 4
        lifetime_dynpro_dynpro_link = 5
        OTHERS                      = 6.
    IF sy-subrc NE 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.


* Create ALV grid
    CREATE OBJECT go_alvgrid_9002
      EXPORTING
        i_parent = go_container_9002
      EXCEPTIONS
        OTHERS   = 5.
    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.


*   Preparing field catalog
    PERFORM prepare_fcat_9002 CHANGING lt_fieldcat[].

*   Preparing layout structure
    PERFORM prepare_layout_9002 CHANGING ls_layout.

* Prepare Variant
    ls_disvariant-report    = sy-repid.
    ls_disvariant-username  = sy-uname.

*   ALV Display
    CALL METHOD go_alvgrid_9002->set_table_for_first_display
      EXPORTING
        i_bypassing_buffer            = abap_true
        is_variant                    = ls_disvariant
        i_save                        = lc_a
        is_layout                     = ls_layout
      CHANGING
        it_outtab                     = gt_cc_det[]
        it_fieldcatalog               = lt_fieldcat[]
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.


  ELSE.

*   ALV Refresh Display
    CALL METHOD go_alvgrid_9002->refresh_table_display
      EXPORTING
*       is_stable      = ls_stable
        i_soft_refresh = abap_true
      EXCEPTIONS
        finished       = 1
        OTHERS         = 2.
  ENDIF.



ENDFORM.  " DISPLAY_ALV_9002
*&---------------------------------------------------------------------*
*&      Form  PREPARE_FCAT_9002
*&---------------------------------------------------------------------*
*   Preparing field catalog
*----------------------------------------------------------------------*
FORM prepare_fcat_9002 CHANGING fc_t_fieldcat TYPE lvc_t_fcat.


* Declaration
  DATA: lt_fieldcat TYPE lvc_t_fcat,
        lv_desc     TYPE char40.


  FIELD-SYMBOLS : <ls_fieldcat>    TYPE lvc_s_fcat.

* Prepare field catalog for ALV display by merging structure
  REFRESH: lt_fieldcat[].
  CALL FUNCTION 'LVC_FIELDCATALOG_MERGE'
    EXPORTING
      i_structure_name       = 'ZARN_CC_DET'
    CHANGING
      ct_fieldcat            = lt_fieldcat[]
    EXCEPTIONS
      inconsistent_interface = 1
      program_error          = 2
      OTHERS                 = 3.
  IF sy-subrc NE 0.
    REFRESH: fc_t_fieldcat.
  ENDIF.


* Looping to make the output field
  LOOP AT lt_fieldcat ASSIGNING <ls_fieldcat>.

    <ls_fieldcat>-tooltip = <ls_fieldcat>-scrtext_l.
    <ls_fieldcat>-col_opt = abap_true.

    CASE <ls_fieldcat>-fieldname.
      WHEN 'MANDT'. <ls_fieldcat>-tech = abap_true.
      WHEN 'CHGID'.
      WHEN 'IDNO'.
      WHEN 'CHG_CATEGORY'.
      WHEN 'CHG_TABLE'.
      WHEN 'CHG_FIELD'.
      WHEN 'REFERENCE_DATA'.
      WHEN 'CHG_AREA'.
      WHEN 'NATIONAL_VER_ID'.
      WHEN 'VER_COMP_WITH'.
      WHEN 'CHG_IND'.
      WHEN 'VALUE_NEW'.
      WHEN 'VALUE_OLD'.
      WHEN 'REG_CDHDR'. <ls_fieldcat>-tech = abap_true.
      WHEN OTHERS.
        DELETE lt_fieldcat WHERE fieldname = <ls_fieldcat>-fieldname.
    ENDCASE.

  ENDLOOP.

  fc_t_fieldcat[] = lt_fieldcat[].


ENDFORM.  " PREPARE_FCAT_9002
*&---------------------------------------------------------------------*
*&      Form  PREPARE_LAYOUT_9002
*&---------------------------------------------------------------------*
*   Preparing layout structure
*----------------------------------------------------------------------*
FORM prepare_layout_9002 CHANGING fc_s_layout TYPE lvc_s_layo.

  fc_s_layout-sel_mode   = 'A'.

ENDFORM.  " PREPARE_LAYOUT_9002
*&---------------------------------------------------------------------*
*&      Form  POPUP_APPL_LOG
*&---------------------------------------------------------------------*
FORM popup_appl_log USING pt_message TYPE bal_t_msg.


  DATA: ls_log             TYPE bal_s_log,
        ls_display_profile TYPE bal_s_prof,
        lt_log_handle      TYPE bal_t_logh,
        lv_log_handle      TYPE balloghndl,
        lv_text            TYPE text255.

  FIELD-SYMBOLS: <ls_message> LIKE LINE OF pt_message.




  CHECK NOT pt_message[] IS INITIAL.

  ls_log-object    = 'ZARN'.
  ls_log-subobject = 'ZARN_POPUP'.

  CALL FUNCTION 'BAL_LOG_CREATE'
    EXPORTING
      i_s_log                 = ls_log
    IMPORTING
      e_log_handle            = lv_log_handle
    EXCEPTIONS
      log_header_inconsistent = 1.

  IF sy-subrc EQ 0.
    LOOP AT pt_message ASSIGNING <ls_message>.
      IF  <ls_message>-msgid EQ 'ZARENA_MSG'
      AND <ls_message>-msgno EQ '000'.
        CONCATENATE
          <ls_message>-msgv1
          <ls_message>-msgv2
          <ls_message>-msgv3
          <ls_message>-msgv4
          INTO lv_text.
        CALL FUNCTION 'BAL_LOG_MSG_ADD_FREE_TEXT'
          EXPORTING
            i_log_handle     = lv_log_handle
            i_msgty          = <ls_message>-msgty
            i_text           = lv_text
          EXCEPTIONS
            log_not_found    = 1
            msg_inconsistent = 2
            log_is_full      = 3
            OTHERS           = 4.
      ELSE.
        CALL FUNCTION 'BAL_LOG_MSG_ADD'
          EXPORTING
            i_log_handle     = lv_log_handle
            i_s_msg          = <ls_message>
          EXCEPTIONS
            log_not_found    = 1
            msg_inconsistent = 2
            log_is_full      = 3.
      ENDIF.
    ENDLOOP.

    CALL FUNCTION 'BAL_DSP_PROFILE_POPUP_GET'
*     EXPORTING
*       START_COL                 = 5
*       START_ROW                 = 5
*       END_COL                   = 87
*       END_ROW                   = 25
      IMPORTING
        e_s_display_profile = ls_display_profile.

    APPEND lv_log_handle TO lt_log_handle.
    CALL FUNCTION 'BAL_DSP_LOG_DISPLAY'
      EXPORTING
        i_s_display_profile  = ls_display_profile
        i_t_log_handle       = lt_log_handle
*       I_AMODAL             = ' '
      EXCEPTIONS
        profile_inconsistent = 1
        internal_error       = 2
        no_data_available    = 3
        no_authority         = 4.
  ENDIF.

ENDFORM.  " POPUP_APPL_LOG
