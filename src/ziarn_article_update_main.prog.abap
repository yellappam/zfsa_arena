*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_ARTICLE_UPDATE_MAIN
*&---------------------------------------------------------------------*

START-OF-SELECTION.

  CLEAR gt_key[].
  IF p_idno IS NOT INITIAL AND p_ver IS NOT INITIAL.
* Build Key table
    PERFORM build_key USING p_idno
                            p_ver
                   CHANGING gt_key[].
  ENDIF.

* Article Posting as Batch Program
  PERFORM article_posting USING gt_key[]
                                s_status[]
                                s_frmdat[]
                                abap_true   " 'X' for  Batch Processing
                       CHANGING gt_message[].




END-OF-SELECTION.

  IF gt_message[] IS NOT INITIAL.
* Maintain SLG1 Log
    CLEAR gv_log_no.
    PERFORM maintain_slg1_log USING gt_message[]
                                    p_log
                           CHANGING gv_log_no.
  ENDIF.
