class ZCL_ARN_NOTIF definition
  public
  abstract
  create public .

public section.

  interfaces ZIF_ARN_NOTIF .

  data MV_IDNO type ZARN_IDNO read-only .

  methods CONSTRUCTOR
    importing
      !IV_IDNO type ZARN_IDNO
      !IV_NOTIF_TEXT type ZARN_E_APPR_COMMENT optional
      !IT_APPROVALS type ZARN_T_APPROVAL optional
      !IS_NOTIF_PARAM type ZSARN_NOTIF_PARAM optional .
protected section.

  data MV_NOTIF_TEXT type ZARN_E_APPR_COMMENT .
  data MT_APPROVALS type ZARN_T_APPROVAL .
  data MO_SEND_REQUEST type ref to CL_BCS .
  data MS_NOTIF_PARAM type ZSARN_NOTIF_PARAM .

  methods RECIPIENT_DETERMINATION
    returning
      value(RT_RECIPIENTS) type RMPS_RECIPIENT_BCS
    raising
      CX_ADDRESS_BCS .
  methods CREATE_ATTACH
  abstract
    exporting
      !ET_ATTACHMENTS type ZTARN_BCS_ATTACH .
  methods CREATE_DOCUMENT
    returning
      value(RO_DOCUMENT) type ref to CL_DOCUMENT_BCS .
  methods CREATE_SEND_REQUEST
    returning
      value(RO_SEND_REQUEST) type ref to CL_BCS
    raising
      CX_SEND_REQ_BCS .
  methods GET_SUBJECT
  abstract
    returning
      value(RV_SUBJECT) type SO_OBJ_DES .
  methods GET_BODY
  abstract
    returning
      value(RT_BODY) type SOLI_TAB .
  methods REPLACE_CTL_CHAR
    importing
      !IV_TEXT type STRING
    returning
      value(RV_TEXT) type STRING .
private section.
ENDCLASS.



CLASS ZCL_ARN_NOTIF IMPLEMENTATION.


  method CONSTRUCTOR.

    me->mv_idno = iv_idno.
    me->mv_notif_text = iv_notif_text.
    me->mt_approvals = it_approvals.
    me->ms_notif_param = is_notif_param.

  endmethod.


  METHOD create_document.


    TRY.
        ro_document = cl_document_bcs=>create_document(
          EXPORTING
            i_type          = 'RAW'
            i_subject       = me->get_subject( )
            i_text          = me->get_body( )
         ).
      CATCH cx_document_bcs ##NO_HANDLER.
    ENDTRY.


  ENDMETHOD.


  METHOD CREATE_SEND_REQUEST.
* Create the send request

    ro_send_request = cl_bcs=>create_persistent( ).
    ro_send_request->set_send_immediately( abap_true ).

  ENDMETHOD.


  METHOD recipient_determination.

* Include self as a recipient?
    IF me->ms_notif_param-sender_as_receiver = abap_true.
      DATA(lv_user) = sy-uname.
      TRY.
          APPEND
            cl_cam_address_bcs=>create_user_home_address(
                i_user         = lv_user
                i_commtype     = cl_cam_address_bcs=>c_address_type_smtp
            ) TO rt_recipients.
        CATCH cx_address_bcs ##NO_HANDLER.
      ENDTRY.
    ENDIF.

  ENDMETHOD.


  METHOD replace_ctl_char.
* Get rid of control characters so we can jam a multi-line comment in a column
    rv_text = iv_text.

    REPLACE ALL OCCURRENCES OF cl_abap_char_utilities=>cr_lf IN rv_text WITH space.
    REPLACE ALL OCCURRENCES OF cl_abap_char_utilities=>horizontal_tab IN rv_text WITH space.

  ENDMETHOD.


  METHOD zif_arn_notif~generate.
* Generate the notification

* Create the send requst
    TRY.
        me->mo_send_request =  me->create_send_request( ).

* Recipient determination
        LOOP AT me->recipient_determination( ) INTO DATA(lo_recipient).
          me->mo_send_request->add_recipient( lo_recipient ).
        ENDLOOP.

* Create the document
        DATA(lo_document) = me->create_document( ).
* Set the document
        me->mo_send_request->set_document( lo_document ).

* Create attachment(s)
        me->create_attach(
          IMPORTING
            et_attachments = DATA(lt_attachments)
        ).
        LOOP AT lt_attachments ASSIGNING FIELD-SYMBOL(<ls_attachment>).
* Add the attachment to the document
          lo_document->add_attachment(
            EXPORTING
              i_attachment_type     = <ls_attachment>-type
              i_attachment_subject  = <ls_attachment>-name
              i_attachment_size     = <ls_attachment>-size
              i_att_content_hex     = <ls_attachment>-content
          ).
        ENDLOOP.

        rv_generated = abap_true.

      CATCH cx_document_bcs cx_send_req_bcs cx_address_bcs INTO DATA(lo_ex).
        RAISE EXCEPTION TYPE zcx_arn_notif_err
          EXPORTING
            textid        = zcx_arn_notif_err=>generation_error
            mv_error_text = lo_ex->get_text( )
            previous      = lo_ex.
    ENDTRY.

  ENDMETHOD.


  METHOD zif_arn_notif~get_approvals.

    rt_approvals = me->mt_approvals.

  ENDMETHOD.


  method ZIF_ARN_NOTIF~GET_NOTIF_PARAM.

    rs_param = me->ms_notif_param.

  endmethod.


  method ZIF_ARN_NOTIF~GET_NOTIF_TEXT.

    rv_text = me->mv_notif_text.

  endmethod.


  METHOD zif_arn_notif~transmit.
* Send the notification

    TRY.
        me->mo_send_request->send( ).
        IF iv_commit = abap_true.
          COMMIT WORK.
        ENDIF.
      CATCH cx_send_req_bcs.
    ENDTRY.

  ENDMETHOD.
ENDCLASS.
