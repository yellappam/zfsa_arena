*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZHYBRISTOP.                       " Global Data
  INCLUDE LZHYBRISUXX.                       " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZHYBRISF...                       " Subroutines
* INCLUDE LZHYBRISO...                       " PBO-Modules
* INCLUDE LZHYBRISI...                       " PAI-Modules
* INCLUDE LZHYBRISE...                       " Events
* INCLUDE LZHYBRISP...                       " Local class implement.
* INCLUDE LZHYBRIST99.                       " ABAP Unit tests

INCLUDE LZHYBRISF01.
