*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_SET_ACTIVE_LP_SEL
*&---------------------------------------------------------------------*


TABLES: zarn_ver_status.

SELECTION-SCREEN: BEGIN OF BLOCK b1 WITH FRAME TITLE text-001.


SELECT-OPTIONS: s_idno FOR zarn_ver_status-idno.

SELECTION-SCREEN: END OF BLOCK b1.
