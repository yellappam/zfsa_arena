*----------------------------------------------------------------------*
***INCLUDE ZARN_NATIONAL_UPDATES_GET_DF01.
*----------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*&      Form  GET_DATA_9001
*&---------------------------------------------------------------------*
*       Get required data records
*----------------------------------------------------------------------*
FORM get_data_9001.

*-- Get all products that have changed in the time period
  SELECT v~version_status, p~*
         INTO CORRESPONDING FIELDS OF TABLE @gt_products
         FROM zarn_products AS p
         INNER JOIN zarn_prd_version AS v
            ON p~idno = v~idno
            AND p~version = v~version
         WHERE  fsni_last_trans_date  IN @lr_date
         AND    fan_id                IN @s_fan
         AND    v~version_status      IN @s_vers.

ENDFORM.

*&---------------------------------------------------------------------*
*&      Form  INIT
*&---------------------------------------------------------------------*
*       Initialise data
*----------------------------------------------------------------------*
FORM init .

  DATA itab TYPE TABLE OF spfli.
  FIELD-SYMBOLS <ls_date> LIKE LINE OF lr_date.

*-- Move date selection screen into a Date/Time range
  LOOP AT s_date INTO DATA(ls_sdate).

    APPEND INITIAL LINE TO lr_date ASSIGNING <ls_date>.
    MOVE-CORRESPONDING ls_sdate TO <ls_date>.
*-- Move date into date/time field
    IF <ls_date>-low IS NOT INITIAL.
      OVERLAY <ls_date>-low  WITH '00000000000000'.
    ENDIF.
    OVERLAY <ls_date>-high WITH '00000000000000'.
    IF <ls_date>-option EQ 'EQ'."convert to range
      ls_sdate-high = ls_sdate-low + 1.
      <ls_date>-high = ls_sdate-high.
      <ls_date>-option = 'BT'.
      OVERLAY <ls_date>-high WITH '00000000000000'.
    ENDIF.

  ENDLOOP.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  DISPLAY_ALV_9001
*&---------------------------------------------------------------------*
FORM display_alv_9001.

  CONSTANTS: lc_a        TYPE char01 VALUE 'A'.

* Declaration
  DATA:
    lo_container  TYPE REF TO cl_gui_custom_container,    "Custom container instance reference
    lt_fieldcat   TYPE        lvc_t_fcat,                 "Field catalog
    lt_sort       TYPE        lvc_t_sort,                 "Sort by
    ls_layout     TYPE        lvc_s_layo ,                "Layout structure
    ls_disvariant TYPE        disvariant,                 "Variant
    lo_alvgrid    TYPE REF TO cl_gui_alv_grid.


  IF go_alvgrid_9001 IS INITIAL .

*   Create custom container
    CREATE OBJECT go_container_9001
      EXPORTING
        container_name              = 'CC_9001'
      EXCEPTIONS
        cntl_error                  = 1
        cntl_system_error           = 2
        create_error                = 3
        lifetime_error              = 4
        lifetime_dynpro_dynpro_link = 5
        OTHERS                      = 6.
    IF sy-subrc NE 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

* Create ALV grid
    CREATE OBJECT go_alvgrid_9001
      EXPORTING
        i_parent = go_container_9001
      EXCEPTIONS
        OTHERS   = 5.
    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.


*   Preparing field catalog
    PERFORM prepare_fcat_9001 CHANGING lt_fieldcat[].

*   Preparing layout structure
    PERFORM prepare_layout_9001 CHANGING ls_layout.

    PERFORM prepare_sort_9001 CHANGING lt_sort.

* Prepare Variant
    ls_disvariant-report    = sy-repid.
    ls_disvariant-username  = sy-uname.

*   ALV Display
    CALL METHOD go_alvgrid_9001->set_table_for_first_display
      EXPORTING
        i_bypassing_buffer            = abap_true
        i_save                        = lc_a
        is_layout                     = ls_layout
      CHANGING
        it_outtab                     = gt_products[]
        it_fieldcatalog               = lt_fieldcat[]
        it_sort                       = lt_sort[]
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.


  ELSE.

*   ALV Refresh Display
    CALL METHOD go_alvgrid_9001->refresh_table_display
      EXPORTING
        i_soft_refresh = abap_true
      EXCEPTIONS
        finished       = 1
        OTHERS         = 2.
  ENDIF.


ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  PREPARE_FCAT_9001
*&---------------------------------------------------------------------*
FORM prepare_fcat_9001  CHANGING fc_t_fieldcat TYPE lvc_t_fcat.

* Declaration
  DATA: lt_fieldcat TYPE lvc_t_fcat,
        lv_desc     TYPE char40.


  FIELD-SYMBOLS : <ls_fieldcat>    TYPE lvc_s_fcat.

* Prepare field catalog for ALV display by merging structure
  REFRESH: lt_fieldcat[].
  CALL FUNCTION 'LVC_FIELDCATALOG_MERGE'
    EXPORTING
      i_structure_name       = 'zsarn_prod_upd'
    CHANGING
      ct_fieldcat            = lt_fieldcat[]
    EXCEPTIONS
      inconsistent_interface = 1
      program_error          = 2
      OTHERS                 = 3.
  IF sy-subrc NE 0.
    REFRESH: fc_t_fieldcat.
  ENDIF.


* Looping to make the output field
  LOOP AT lt_fieldcat ASSIGNING <ls_fieldcat>.


    <ls_fieldcat>-tooltip = <ls_fieldcat>-scrtext_l.
    <ls_fieldcat>-col_opt = abap_true.

    lv_desc = <ls_fieldcat>-scrtext_s.

  ENDLOOP.

  fc_t_fieldcat[] = lt_fieldcat[].

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  PREPARE_LAYOUT_9001
*&---------------------------------------------------------------------*
FORM prepare_layout_9001  CHANGING fc_s_layout    TYPE lvc_s_layo.

  DATA: lv_count TYPE char15,
        lv_lines TYPE i.

*  fc_s_layout-ctab_fname = 'COLOR'.
  fc_s_layout-sel_mode   = 'A'.
  fc_s_layout-cwidth_opt  = abap_true.

  CLEAR lv_lines.
  DESCRIBE TABLE gt_products[] LINES lv_lines.

  lv_count = lv_lines.
  CONDENSE lv_count NO-GAPS.

  CONCATENATE TEXT-003 lv_count
         INTO fc_s_layout-grid_title
 SEPARATED BY space.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  display_alv_9002
*&---------------------------------------------------------------------*
FORM display_alv_9002.
  CONSTANTS: lc_a        TYPE char01 VALUE 'A'.

* Declaration
  DATA:
    lo_container  TYPE REF TO cl_gui_custom_container,    "Custom container instance reference
    lt_fieldcat   TYPE        lvc_t_fcat,                 "Field catalog
    lt_sort       TYPE        lvc_t_sort,                 "Sort by
    ls_layout     TYPE        lvc_s_layo ,                "Layout structure
    ls_disvariant TYPE        disvariant,                 "Variant
    lo_alvgrid    TYPE REF TO cl_gui_alv_grid.


  IF go_alvgrid_9002 IS INITIAL .

*   Create custom container
    CREATE OBJECT go_container_9002
      EXPORTING
        container_name              = 'CC_9002'
      EXCEPTIONS
        cntl_error                  = 1
        cntl_system_error           = 2
        create_error                = 3
        lifetime_error              = 4
        lifetime_dynpro_dynpro_link = 5
        OTHERS                      = 6.
    IF sy-subrc NE 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

* Create ALV grid
    CREATE OBJECT go_alvgrid_9002
      EXPORTING
        i_parent = go_container_9002
      EXCEPTIONS
        OTHERS   = 5.
    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.


*   Preparing field catalog
    PERFORM prepare_fcat_9002 CHANGING lt_fieldcat[].

*   Preparing layout structure
    PERFORM prepare_layout_9002 CHANGING ls_layout.

    PERFORM prepare_sort_9002 CHANGING lt_sort.

* Prepare Variant
    ls_disvariant-report    = sy-repid.
    ls_disvariant-username  = sy-uname.

*   ALV Display
    CALL METHOD go_alvgrid_9002->set_table_for_first_display
      EXPORTING
        i_bypassing_buffer            = abap_true
        i_save                        = lc_a
        is_layout                     = ls_layout
      CHANGING
        it_outtab                     = gt_prod_attr[]
        it_fieldcatalog               = lt_fieldcat[]
        it_sort                       = lt_sort[]
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.


  ELSE.

*   ALV Refresh Display
    CALL METHOD go_alvgrid_9002->refresh_table_display
      EXPORTING
*       is_stable      = ls_stable
        i_soft_refresh = abap_true
      EXCEPTIONS
        finished       = 1
        OTHERS         = 2.
  ENDIF.


ENDFORM.

*&---------------------------------------------------------------------*
*&      Form  PREPARE_FCAT_9002
*&---------------------------------------------------------------------*
FORM prepare_fcat_9002  CHANGING fc_t_fieldcat TYPE lvc_t_fcat.

* Declaration
  DATA: lt_fieldcat TYPE lvc_t_fcat,
        lv_desc     TYPE char40.


  FIELD-SYMBOLS : <ls_fieldcat>    TYPE lvc_s_fcat.

* Prepare field catalog for ALV display by merging structure
  REFRESH: lt_fieldcat[].
  CALL FUNCTION 'LVC_FIELDCATALOG_MERGE'
    EXPORTING
      i_structure_name       = 'zsarn_attr_comp'
    CHANGING
      ct_fieldcat            = lt_fieldcat[]
    EXCEPTIONS
      inconsistent_interface = 1
      program_error          = 2
      OTHERS                 = 3.
  IF sy-subrc NE 0.
    REFRESH: fc_t_fieldcat.
  ENDIF.


* Looping to make the output field
  LOOP AT lt_fieldcat ASSIGNING <ls_fieldcat>.


    <ls_fieldcat>-tooltip = <ls_fieldcat>-scrtext_l.
    <ls_fieldcat>-col_opt = abap_true.

    lv_desc = <ls_fieldcat>-scrtext_s.

    CASE <ls_fieldcat>-fieldname.
      WHEN 'ATTR_NAT_VAL'.  <ls_fieldcat>-coltext = <ls_fieldcat>-tooltip = 'National Value'.
      WHEN 'ATTR_REG_VAL'.  <ls_fieldcat>-coltext = <ls_fieldcat>-tooltip = 'Regional Value'.
    ENDCASE.

  ENDLOOP.

  fc_t_fieldcat[] = lt_fieldcat[].

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  PREPARE_LAYOUT_9002
*&---------------------------------------------------------------------*
FORM prepare_layout_9002  CHANGING fc_s_layout    TYPE lvc_s_layo.

  DATA: lv_count TYPE char15,
        lv_lines TYPE i.

*  fc_s_layout-ctab_fname = 'COLOR'.
  fc_s_layout-sel_mode   = 'A'.
  fc_s_layout-cwidth_opt  = abap_true.

  CLEAR lv_lines.
  DESCRIBE TABLE gt_products[] LINES lv_lines.

  lv_count = lv_lines.
  CONDENSE lv_count NO-GAPS.

  IF p_diff EQ abap_true.
    CONCATENATE TEXT-002 ': Regional Overrides only'
           INTO fc_s_layout-grid_title
    SEPARATED BY space.
  ELSE.
    fc_s_layout-grid_title = TEXT-002.
  ENDIF.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  GET_DATA_9002
*&---------------------------------------------------------------------*
FORM get_data_9002 CHANGING p_nodata.
  DATA lt_chars TYPE zonl_characteristic_tab.
  DATA lv_store TYPE werks_d.

  REFRESH gt_prod_attr.

  IF gt_rows_9001[] IS INITIAL.
    MESSAGE i133."No rows were selected
    p_nodata = abap_true.
    EXIT.
  ENDIF.

  LOOP AT gt_products ASSIGNING FIELD-SYMBOL(<ls_products>).

*-- Check if this specific line was selected - only show data for selected lines
    READ TABLE gt_rows_9001 TRANSPORTING NO FIELDS WITH KEY index = sy-tabix.
    IF sy-subrc NE 0."Line not selected.
      CONTINUE.
    ENDIF.
*--------------------------------------------------------------------*
*-- Convert the article number
    CALL FUNCTION 'CONVERSION_EXIT_ALPHA_INPUT'
      EXPORTING
        input  = <ls_products>-matnr_ni
      IMPORTING
        output = <ls_products>-matnr_ni.

*--------------------------------------------------------------------*
*-- Get the Regional attributes for the product

    CLEAR lt_chars[].
*-- Get the article attributes that come from Directly from Arena table fields
    zcl_onl_arena=>get_article_attr_direct(
                      EXPORTING iv_article    = <ls_products>-matnr_ni
                                iv_store      = lv_store
                      CHANGING ct_attributes  = lt_chars ).

**-- Get the article attributes that still come from Article Master Characteristics
*        me->get_attributes_from_art_mast(
*                          EXPORTING iv_article    = iv_article
*                          CHANGING ct_attributes  = <ls_class>-characteristic ).

*-- Get the article attributes that come from Indirectly from Arena
    zcl_onl_arena=>get_article_attr_indirect(
                      EXPORTING iv_article    = <ls_products>-matnr_ni
                      CHANGING ct_attributes  = lt_chars ).

    SORT lt_chars BY characteristic_code.

*--------------------------------------------------------------------*
*-- Loop through the Regional Attributes and put into list format

    LOOP AT lt_chars ASSIGNING FIELD-SYMBOL(<ls_chars>).
      APPEND INITIAL LINE TO gt_prod_attr ASSIGNING FIELD-SYMBOL(<ls_prod_attr>).
*-- product details
      <ls_prod_attr>-idno           = <ls_products>-idno.
      <ls_prod_attr>-version        = <ls_products>-version.
      <ls_prod_attr>-version_status = <ls_products>-version_status.
      <ls_prod_attr>-matnr          = <ls_products>-matnr_ni.
      <ls_prod_attr>-fan_id         = <ls_products>-fan_id.
*-- Attribute name and desc
      <ls_prod_attr>-attr_name = <ls_chars>-characteristic_code.
      <ls_prod_attr>-attr_desc = <ls_chars>-characteristic_description-content.
*-- Attribute value
      IF <ls_chars>-characteristic_value-text_value IS NOT INITIAL.
        <ls_prod_attr>-attr_reg_val = <ls_chars>-characteristic_value-text_value.
      ELSEIF <ls_chars>-characteristic_value-numeric_value IS NOT INITIAL.
        <ls_prod_attr>-attr_reg_val = <ls_chars>-characteristic_value-numeric_value.
      ELSEIF <ls_chars>-characteristic_value-boolean_value IS NOT INITIAL.
        <ls_prod_attr>-attr_reg_val = <ls_chars>-characteristic_value-boolean_value.
      ENDIF.
    ENDLOOP.

*--------------------------------------------------------------------*
*-- Get the National attributes for the product
    SELECT SINGLE prod_str_fs_cust_medium_descr, prod_ex_fs_cust_short_desc
           FROM  zvw_prod_desc
           INTO @DATA(ls_prod_desc)
           WHERE  idno  = @<ls_products>-idno.

*--------------------------------------------------------------------*
*-- Some attributes may not have regional values, so re-read attributes for national

    SELECT attr_name, attr_desc INTO @DATA(ls_attr_onl)
           FROM zarn_attr_onl.

*-- Look for existing attribute with a regional value
      READ TABLE gt_prod_attr ASSIGNING <ls_prod_attr> WITH KEY
                 idno      = <ls_products>-idno
                 attr_name = ls_attr_onl-attr_name.
      IF sy-subrc NE 0."Not Found
*-- Add new one
        APPEND INITIAL LINE TO gt_prod_attr ASSIGNING <ls_prod_attr>.
*-- product details
        <ls_prod_attr>-idno           = <ls_products>-idno.
        <ls_prod_attr>-version        = <ls_products>-version.
        <ls_prod_attr>-version_status = <ls_products>-version_status.
        <ls_prod_attr>-matnr          = <ls_products>-matnr_ni.
        <ls_prod_attr>-fan_id         = <ls_products>-fan_id.
*-- Attribute name and desc
        <ls_prod_attr>-attr_name = ls_attr_onl-attr_name.
        <ls_prod_attr>-attr_desc = ls_attr_onl-attr_desc.
      ENDIF.


*--------------------------------------------------------------------*
*-- Add the National attribute values to the list format
      CASE ls_attr_onl-attr_name.

        WHEN 'PIM_ORIDE_BRAND'.
          <ls_prod_attr>-attr_nat_val = <ls_products>-fs_brand_desc.
        WHEN 'PIM_ORIDE_SHORT_TEXT'.
          <ls_prod_attr>-attr_nat_val = ls_prod_desc-prod_ex_fs_cust_short_desc.
        WHEN 'PIM_ORIDE_MEDIUM_TEXT'.
          <ls_prod_attr>-attr_nat_val = ls_prod_desc-prod_str_fs_cust_medium_descr.
        WHEN 'PIM_ORIDE_LONG_TEXT'.
          SELECT SINGLE fs_cust_long_descr FROM  zarn_prod_str "#EC CI_SEL_NESTED
                 INTO @DATA(ls_long_descr)
                 WHERE  idno  = @<ls_products>-idno.
          IF sy-subrc EQ 0.
            <ls_prod_attr>-attr_nat_val = ls_long_descr.
          ENDIF.
        WHEN 'PIM_ORIDE_NETCONT'.
          <ls_prod_attr>-attr_nat_val = <ls_products>-net_quantity.
          SHIFT <ls_prod_attr>-attr_nat_val LEFT DELETING LEADING space.
        WHEN 'PIM_ORIDE_NETCONT_UOM'.
          <ls_prod_attr>-attr_nat_val = <ls_products>-net_quantity_uom.

        WHEN OTHERS.
          <ls_prod_attr>-attr_nat_val = 'N/A'.

      ENDCASE.

*-- Show differences only?
      IF p_diff EQ abap_true.
        IF ( <ls_prod_attr>-attr_reg_val EQ space ). "delete if no regional override
          DELETE gt_prod_attr INDEX sy-tabix.
        ENDIF.
      ENDIF.

    ENDSELECT.

  ENDLOOP.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  PREPARE_SORT_9002
*&---------------------------------------------------------------------*
FORM prepare_sort_9002  CHANGING pt_sort TYPE lvc_t_sort.

  APPEND INITIAL LINE TO pt_sort ASSIGNING FIELD-SYMBOL(<ls_sort>).
  <ls_sort>-spos = 1.
  <ls_sort>-fieldname = 'IDNO'.
  APPEND INITIAL LINE TO pt_sort ASSIGNING <ls_sort>.
  <ls_sort>-spos = 2.
  <ls_sort>-fieldname = 'VERSION'.
  <ls_sort>-down      = abap_true.
  APPEND INITIAL LINE TO pt_sort ASSIGNING <ls_sort>.
  <ls_sort>-spos = 3.
  <ls_sort>-fieldname = 'VERSION_STATUS'.
  APPEND INITIAL LINE TO pt_sort ASSIGNING <ls_sort>.
  <ls_sort>-spos = 4.
  <ls_sort>-fieldname = 'FAN_ID'.
  APPEND INITIAL LINE TO pt_sort ASSIGNING <ls_sort>.
  <ls_sort>-spos = 5.
  <ls_sort>-fieldname = 'MATNR'.
  APPEND INITIAL LINE TO pt_sort ASSIGNING <ls_sort>.
  <ls_sort>-spos = 6.
  <ls_sort>-fieldname = 'ATTR_NAME'.

ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  PREPARE_SORT_9001
*&---------------------------------------------------------------------*
FORM prepare_sort_9001  CHANGING pt_sort TYPE lvc_t_sort.

  APPEND INITIAL LINE TO pt_sort ASSIGNING FIELD-SYMBOL(<ls_sort>).
  <ls_sort>-spos = 1.
  <ls_sort>-fieldname = 'IDNO'.
  APPEND INITIAL LINE TO pt_sort ASSIGNING <ls_sort>.
  <ls_sort>-spos = 2.
  <ls_sort>-fieldname = 'VERSION'.
  <ls_sort>-down      = abap_true.

ENDFORM.
