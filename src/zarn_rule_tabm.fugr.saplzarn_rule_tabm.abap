* regenerated at 02.02.2016 16:01:44 by  C90001587
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_RULE_TABMTOP.                " Global Data
  INCLUDE LZARN_RULE_TABMUXX.                " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_RULE_TABMF...                " Subroutines
* INCLUDE LZARN_RULE_TABMO...                " PBO-Modules
* INCLUDE LZARN_RULE_TABMI...                " PAI-Modules
* INCLUDE LZARN_RULE_TABME...                " Events
* INCLUDE LZARN_RULE_TABMP...                " Local class implement.
* INCLUDE LZARN_RULE_TABMT99.                " ABAP Unit tests
  INCLUDE LZARN_RULE_TABMF00                      . " subprograms
  INCLUDE LZARN_RULE_TABMI00                      . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules

INCLUDE LZARN_RULE_TABMF01.
