*&---------------------------------------------------------------------*
*& Report  ZRARN_NAT_REG_DATA_UPLOAD
*&
*&---------------------------------------------------------------------*
*&---------------------------------------------------------------------*
* PROJECT          # FSA
* SPECIFICATION    # 3113
* DATE WRITTEN     # 13.11.2015
* SYSTEM           # DE1
* SAP VERSION      # ECC6.0
* TYPE             # Report
* AUTHOR           # C90001363
*-----------------------------------------------------------------------
* TITLE            # National and Regional Data Upload
* PURPOSE          # National and Regional Data Upload into DB from files
*                  # for testing purpose
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      #
*-----------------------------------------------------------------------
* CALLS TO         #
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

REPORT zrarn_nat_reg_data_upload.

*----------------------------------------------------------------------*
*       TYPE DECLARATION
*----------------------------------------------------------------------*
TYPES: BEGIN OF ty_s_log,
        tabname TYPE tabname,
        msgty   TYPE char10,
        message TYPE char50,
        color   TYPE char1,
  END OF ty_s_log,
ty_t_log TYPE STANDARD TABLE OF ty_s_log.

*----------------------------------------------------------------------*
*       DATA DECLARATION
*----------------------------------------------------------------------*
DATA: lt_files          TYPE STANDARD TABLE OF file_info,
      ls_files          TYPE file_info,
      lt_intern         TYPE TABLE OF alsmex_tabline,
      ls_intern         TYPE alsmex_tabline,
      lt_log            TYPE ty_t_log,
      ls_log            TYPE ty_s_log,
      lt_table_mastr    TYPE STANDARD TABLE OF zarn_table_mastr,
      ls_table_mastr    TYPE zarn_table_mastr,

      lr_oref           TYPE REF TO cx_root,

      lv_dir_exist      TYPE flag,
      lv_file_filter    TYPE string,
      lv_files_cnt      TYPE i,
      lv_filename       TYPE string,
      lv_tabname        TYPE string,
      lv_localfile      TYPE localfile,
      lv_pc_path        TYPE string,
      lv_index          TYPE i,
      lv_row            TYPE i VALUE 0,
      lv_filetype       TYPE char10.



FIELD-SYMBOLS : <lv_val> TYPE any.


FIELD-SYMBOLS: <fs_table>      TYPE table,
               <wa_table>      TYPE any.
DATA: xtable  TYPE REF TO data,
      xstable TYPE REF TO data.

*SELECTION-SCREEN COMMENT /1(50) text-001.

PARAMETERS: p_dir     TYPE string OBLIGATORY,
            r_txt     RADIOBUTTON GROUP rad1,
            "r_xls     RADIOBUTTON GROUP rad1,
            r_xlsx    RADIOBUTTON GROUP rad1,
            c_hdr     AS CHECKBOX DEFAULT 'X'.

DATA: "r_txt  TYPE char1 VALUE 'X',
      r_xls  TYPE char1.
"r_xlsx TYPE char1.

*----------------------------------------------------------------------*
*       VALUE-REQUEST
*----------------------------------------------------------------------*
AT SELECTION-SCREEN ON VALUE-REQUEST FOR p_dir.

* Browse the Directories
  CALL METHOD cl_gui_frontend_services=>directory_browse
    CHANGING
      selected_folder      = p_dir
    EXCEPTIONS
      cntl_error           = 1
      error_no_gui         = 2
      not_supported_by_gui = 3
      OTHERS               = 4.


*----------------------------------------------------------------------*
*       VALIDATION
*----------------------------------------------------------------------*
AT SELECTION-SCREEN ON p_dir.

* Validate if Directory exists or not
  IF p_dir IS NOT INITIAL.
    CLEAR lv_dir_exist.
    CALL METHOD cl_gui_frontend_services=>directory_exist
      EXPORTING
        directory            = p_dir
      RECEIVING
        result               = lv_dir_exist
      EXCEPTIONS
        cntl_error           = 1
        error_no_gui         = 2
        wrong_parameter      = 3
        not_supported_by_gui = 4
        OTHERS               = 5.
    IF sy-subrc EQ 0.
      IF lv_dir_exist IS INITIAL.
        MESSAGE 'Directory not found' TYPE 'E'.
        EXIT.
      ENDIF.
    ENDIF.
  ENDIF.



*----------------------------------------------------------------------*
*       START-OF-SELECTION
*----------------------------------------------------------------------*
START-OF-SELECTION.

  IF r_txt EQ abap_true.
    lv_file_filter = '*.TXT'.
  ELSEIF r_xls EQ abap_true.
    lv_file_filter = '*.XLS'.
  ELSEIF r_xlsx EQ abap_true.
    lv_file_filter = '*.XLSX'.
  ENDIF.

* Get file info from directory
  CLEAR: lt_files[], lv_files_cnt.
  CALL METHOD cl_gui_frontend_services=>directory_list_files
    EXPORTING
      directory                   = p_dir
      filter                      = lv_file_filter
      files_only                  = abap_true
*     directories_only            =
    CHANGING
      file_table                  = lt_files
      count                       = lv_files_cnt
    EXCEPTIONS
      cntl_error                  = 1
      directory_list_files_failed = 2
      wrong_parameter             = 3
      error_no_gui                = 4
      not_supported_by_gui        = 5
      OTHERS                      = 6.

  IF lv_files_cnt EQ 0.
    MESSAGE 'No file to Upload' TYPE 'S' DISPLAY LIKE 'E'.
    EXIT.
  ENDIF.


  CLEAR: lt_table_mastr[].
  SELECT * FROM zarn_table_mastr
    INTO CORRESPONDING FIELDS OF TABLE lt_table_mastr[].


  REFRESH: lt_log[].

  LOOP AT lt_files INTO ls_files.

    TRANSLATE ls_files-filename TO UPPER CASE.

    IF lv_file_filter = '*.TXT'.
      IF NOT ls_files-filename CS '.TXT'.
        CONTINUE.
      ENDIF.
    ELSEIF lv_file_filter = '*.XLS'.
      IF NOT ls_files-filename CS '.XLS'.
        CONTINUE.
      ENDIF.
      IF ls_files-filename CS '.XLSX'.
        CONTINUE.
      ENDIF.
    ELSEIF lv_file_filter = '*.XLSX'.
      IF NOT ls_files-filename CS '.XLSX'.
        CONTINUE.
      ENDIF.
    ENDIF.





* Get file name without extension as this will be the table name
    lv_pc_path = ls_files-filename.
    CLEAR lv_filename.

    CALL FUNCTION 'CH_SPLIT_FILENAME'
      EXPORTING
        complete_filename = lv_pc_path
*       CHECK_DOS_FORMAT  =
      IMPORTING
*       DRIVE             =
*       EXTENSION         =
        name              = lv_filename
*       NAME_WITH_EXT     =
*       PATH              =
      EXCEPTIONS
        invalid_drive     = 1
        invalid_path      = 2
        OTHERS            = 3.

    CONDENSE lv_filename NO-GAPS.

    lv_tabname = lv_filename.

    TRANSLATE lv_tabname TO UPPER CASE.

    CLEAR ls_table_mastr.
    READ TABLE lt_table_mastr[] INTO ls_table_mastr
    WITH KEY arena_table = lv_tabname.
    IF ls_table_mastr-upload_relevant IS INITIAL.
      CLEAR ls_log.
      ls_log-tabname = lv_tabname.
      ls_log-msgty   = 'Error'.
      ls_log-message = 'Table is not Upload Relevant'.
      APPEND ls_log TO lt_log[].
      CONTINUE.
    ENDIF.




    TRY.
* Create Dynamic internal table for table to be uploaded
        CREATE DATA xtable TYPE TABLE OF (lv_filename).
        ASSIGN xtable->* TO <fs_table>.

        CREATE DATA xstable TYPE (lv_filename).
        ASSIGN xstable->* TO <wa_table>.

        CLEAR lv_filename.
        CONCATENATE p_dir ls_files-filename INTO lv_filename
        SEPARATED BY '\'.



        lv_filetype = 'ASC'.

        IF lv_file_filter = '*.TXT'.

* Get data from PC
*        CALL FUNCTION 'GUI_UPLOAD'
          CALL METHOD cl_gui_frontend_services=>gui_upload
            EXPORTING
              filename                = lv_filename
              filetype                = lv_filetype
              has_field_separator     = abap_true
*          TABLES
               CHANGING
              data_tab                = <fs_table>
            EXCEPTIONS
              file_open_error         = 1
              file_read_error         = 2
              no_batch                = 3
              gui_refuse_filetransfer = 4
              invalid_type            = 5
              no_authority            = 6
              unknown_error           = 7
              bad_data_format         = 8
              header_not_allowed      = 9
              separator_not_allowed   = 10
              header_too_long         = 11
              unknown_dp_error        = 12
              access_denied           = 13
              dp_out_of_memory        = 14
              disk_full               = 15
              dp_timeout              = 16
              not_supported_by_gui    = 17
              error_no_gui            = 18
              OTHERS                  = 19.
          IF sy-subrc <> 0.
            CLEAR ls_log.
            ls_log-tabname = lv_tabname.
            ls_log-msgty   = 'Error'.
            ls_log-message = 'Unable to read data from file'.
            APPEND ls_log TO lt_log[].
            CONTINUE.
          ENDIF.

        ELSEIF lv_file_filter = '*.XLS'.


          DATA  : ls_raw_data   TYPE truxs_t_text_data.

          lv_localfile = lv_filename.
          CALL FUNCTION 'TEXT_CONVERT_XLS_TO_SAP'
            EXPORTING
              i_line_header        = zcl_constants=>gc_true
              i_tab_raw_data       = ls_raw_data
              i_filename           = lv_localfile
            TABLES
              i_tab_converted_data = <fs_table>
            EXCEPTIONS
              conversion_failed    = 1
              OTHERS               = 2.
          IF sy-subrc <> 0.
            CLEAR ls_log.
            ls_log-tabname = lv_tabname.
            ls_log-msgty   = 'Error'.
            ls_log-message = 'Unable to read data from file'.
            APPEND ls_log TO lt_log[].
            CONTINUE.
          ENDIF.

        ELSEIF lv_file_filter = '*.XLSX'.

          lv_localfile = lv_filename.
          REFRESH: lt_intern[].
          CALL FUNCTION 'ALSM_EXCEL_TO_INTERNAL_TABLE'
            EXPORTING
              filename                = lv_localfile
              i_begin_col             = 1
              i_begin_row             = 1
              i_end_col               = 350
              i_end_row               = 9999
            TABLES
              intern                  = lt_intern
            EXCEPTIONS
              inconsistent_parameters = 1
              upload_ole              = 2
              OTHERS                  = 3.
          IF sy-subrc <> 0.
            CLEAR ls_log.
            ls_log-tabname = lv_tabname.
            ls_log-msgty   = 'Error'.
            ls_log-message = 'Unable to read data from file'.
            APPEND ls_log TO lt_log[].
            CONTINUE.
          ENDIF.

          SORT lt_intern BY row col.

          LOOP AT lt_intern INTO ls_intern.
            MOVE ls_intern-col TO lv_index.
            ASSIGN COMPONENT lv_index OF STRUCTURE <wa_table> TO <lv_val>.
            IF sy-subrc IS INITIAL.
              TRY .
                  MOVE ls_intern-value TO <lv_val>.
                CATCH cx_sy_conversion_no_number.
                CATCH cx_sy_conversion_overflow.
                CATCH cx_sy_move_cast_error.
              ENDTRY.
            ENDIF.

            AT END OF row.
              APPEND  <wa_table> TO <fs_table>.
              CLEAR <wa_table>.
            ENDAT.

          ENDLOOP.  " LOOP AT lt_intern INTO ls_intern
        ENDIF.  " IF lv_file_filter = '*.TXT'


        IF c_hdr EQ abap_true AND <fs_table> IS NOT INITIAL.
          DELETE <fs_table> INDEX 1.
        ENDIF.
        LOOP AT <fs_table> ASSIGNING <wa_table>.
          IF <wa_table> IS INITIAL.
            DELETE <fs_table> INDEX sy-tabix.
          ENDIF.
        ENDLOOP.

        IF <fs_table> IS INITIAL.
          CLEAR ls_log.
          ls_log-tabname = lv_tabname.
          ls_log-msgty   = 'Error'.
          ls_log-message = 'No Data found to Upload'.
          APPEND ls_log TO lt_log[].
          CONTINUE.
        ENDIF.



        TRY.
* Update DB Table
            MODIFY (lv_tabname) FROM TABLE <fs_table>.
          CATCH cx_root INTO lr_oref.
            CLEAR ls_log.
            ls_log-tabname = lv_tabname.
            ls_log-msgty   = 'Error'.
            ls_log-message = 'Error occured while updation, check data'.
            APPEND ls_log TO lt_log[].
        ENDTRY.

        CLEAR ls_log.
        ls_log-tabname = lv_tabname.
        ls_log-msgty   = 'Success'.
        ls_log-message = 'Data Uploaded'.
        APPEND ls_log TO lt_log[].

      CATCH cx_root INTO lr_oref.
        CLEAR ls_log.
        ls_log-tabname = lv_tabname.
        ls_log-msgty   = 'Error'.
        ls_log-message = 'Table not found/Error occured while updation'.
        APPEND ls_log TO lt_log[].
    ENDTRY.

  ENDLOOP.



*----------------------------------------------------------------------*
*       END-OF-SELECTION
*----------------------------------------------------------------------*
END-OF-SELECTION.

  WRITE: 5 'Table Name', 40 'Message Type', 55 'Message'.
  WRITE: / , sy-uline+1(110).

  LOOP AT lt_log[] INTO ls_log.

    IF ls_log-msgty = 'Error'.
      WRITE: /5 ls_log-tabname, 40 ls_log-msgty COLOR 6, 55 ls_log-message.
    ELSE.
      WRITE: /5 ls_log-tabname, 40 ls_log-msgty, 55 ls_log-message.
    ENDIF.

  ENDLOOP.
