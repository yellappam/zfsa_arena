*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Functional Specification 3174
* DATE WRITTEN     # 17.03.2017
* SAP VERSION      # 740
* TYPE             # Function
* AUTHOR           # C90001363, Jitin Kharbanda
*-----------------------------------------------------------------------
* TITLE            # AReNa: Send I-Care flag to Hybris (R3)
* PURPOSE          # AReNa: Send I-Care flag to Hybris (R3)
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------
FUNCTION z_hybris_set_r3.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(REQUEST) TYPE  ZPRODUCT_STAGING_ICARE_REQUES4
*"  EXPORTING
*"     REFERENCE(RESPONSE) TYPE  ZPRODUCT_STAGING_ICARE_RESPON4
*"     REFERENCE(ES_CONTROL) TYPE  ZARN_CONTROL
*"     REFERENCE(ES_PRD_VERSION) TYPE  ZARN_PRD_VERSION
*"     REFERENCE(ES_VER_STATUS) TYPE  ZARN_VER_STATUS
*"     REFERENCE(MESSAGES) TYPE  BAL_T_MSG
*"  EXCEPTIONS
*"      HYBRIS_NOT_AVAILABLE
*"----------------------------------------------------------------------

  DATA: ls_request     TYPE zproduct_staging_icare_reques4,
        ls_icare       TYPE zproduct_staging_icare_reques1,
        ls_response    TYPE zproduct_staging_icare_respon4,
        ls_control     TYPE zarn_control,
        ls_prd_version TYPE zarn_prd_version,
        ls_ver_status  TYPE zarn_ver_status,

        ls_tvarvc      TYPE tvarvc.


  ls_request = request.
  ls_icare   = request-product_staging_icare_request-product_enrichment_priority_re.

  IF ls_icare-product_code IS INITIAL.
    RETURN.
  ENDIF.


  CLEAR: ls_ver_status, ls_prd_version, ls_control.

* Get Version Status
  SELECT SINGLE * FROM zarn_ver_status
    INTO CORRESPONDING FIELDS OF ls_ver_status
    WHERE idno = ls_icare-product_code-base_product_code.



  CLEAR ls_tvarvc.
  ls_tvarvc-name = 'ZHYBRIS_SET_R3_AYSNC'.
  SELECT SINGLE *
    INTO ls_tvarvc
    FROM tvarvc
    WHERE name EQ ls_tvarvc-name.

  IF ls_tvarvc-low EQ 'X'.
    CALL FUNCTION 'Z_HYBRIS_SET_R3_ASYNC'
      STARTING NEW TASK 'ZHYBRIS'
      EXPORTING
        request = ls_request.
  ELSE.
* Call Proxy and build AReNa control data
    PERFORM hybris_set_r3 USING ls_request
                       CHANGING ls_response
                                ls_control
                                ls_prd_version
                                ls_ver_status
                                messages[].

    response       = ls_response.
    es_control     = ls_control.
    es_prd_version = ls_prd_version.
    es_ver_status  = ls_ver_status.
  ENDIF.  " IF ls_tvarvc-low EQ 'X'





ENDFUNCTION.
