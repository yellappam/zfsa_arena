* regenerated at 16.03.2016 15:04:55 by  C89991021
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_SYNCTOP.                     " Global Data
  INCLUDE LZARN_SYNCUXX.                     " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_SYNCF...                     " Subroutines
* INCLUDE LZARN_SYNCO...                     " PBO-Modules
* INCLUDE LZARN_SYNCI...                     " PAI-Modules
* INCLUDE LZARN_SYNCE...                     " Events
* INCLUDE LZARN_SYNCP...                     " Local class implement.
* INCLUDE LZARN_SYNCT99.                     " ABAP Unit tests
  INCLUDE LZARN_SYNCF00                           . " subprograms
  INCLUDE LZARN_SYNCI00                           . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules

INCLUDE LZARN_SYNCF01.
