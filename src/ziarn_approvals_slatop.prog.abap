*&---------------------------------------------------------------------*
*&  Include           ZIARN_APPROVALS_SLATOP
*&---------------------------------------------------------------------*
REPORT zrarn_approvals_sla MESSAGE-ID zarena_msg.

INCLUDE ziarn_approvals_slacd1.

TABLES: zarn_products,
        zarn_s_approval_sel,
        zarn_reg_hdr,
        zarn_reg_pir,
        zmd_category.

CONSTANTS: BEGIN OF gc_ucomm,
             tab_itm TYPE string VALUE 'TAB_ITM',
             tab_pov TYPE string VALUE 'TAB_POV',
             tab_vov TYPE string VALUE 'TAB_VOV',
           END OF gc_ucomm.

CONSTANTS: BEGIN OF gc_tab,
             tab_itm TYPE string VALUE 'TAB_ITM',
             tab_pov TYPE string VALUE 'TAB_POV',
             tab_vov TYPE string VALUE 'TAB_VOV',
           END OF gc_tab.

DATA: go_approval_sla TYPE REF TO lcl_approvals_sla ##NEEDED,
      gv_sel_scr_tab  TYPE char70,
      gr_lifnr        TYPE RANGE OF elifn,
      gs_lifnr        LIKE LINE OF gr_lifnr.
