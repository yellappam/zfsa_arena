* regenerated at 24.08.2018 13:15:25
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_DSPRDYPKG_MTOP.              " Global Declarations
  INCLUDE LZARN_DSPRDYPKG_MUXX.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_DSPRDYPKG_MF...              " Subroutines
* INCLUDE LZARN_DSPRDYPKG_MO...              " PBO-Modules
* INCLUDE LZARN_DSPRDYPKG_MI...              " PAI-Modules
* INCLUDE LZARN_DSPRDYPKG_ME...              " Events
* INCLUDE LZARN_DSPRDYPKG_MP...              " Local class implement.
* INCLUDE LZARN_DSPRDYPKG_MT99.              " ABAP Unit tests
  INCLUDE LZARN_DSPRDYPKG_MF00                    . " subprograms
  INCLUDE LZARN_DSPRDYPKG_MI00                    . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
