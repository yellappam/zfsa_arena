class ZCO_ADCITEM_ATTRIBUTES_IMPORT definition
  public
  inheriting from CL_PROXY_CLIENT
  create public .

public section.

  methods ADCITEM_ATTRIBUTES_IMPORT_REQU
    importing
      !OUTPUT type ZADCITEM_ATTRIBUTES_IMPORT_RE3
    exporting
      !INPUT type ZADCITEMT_ATRIBUTES_IMPORT_R18
    raising
      CX_AI_SYSTEM_FAULT
      ZCX_ADCITEM_ATTRIBUTES_IMPORT .
  methods CONSTRUCTOR
    importing
      !LOGICAL_PORT_NAME type PRX_LOGICAL_PORT_NAME optional
    raising
      CX_AI_SYSTEM_FAULT .
protected section.
private section.
ENDCLASS.



CLASS ZCO_ADCITEM_ATTRIBUTES_IMPORT IMPLEMENTATION.


  method ADCITEM_ATTRIBUTES_IMPORT_REQU.

  data:
    ls_parmbind type abap_parmbind,
    lt_parmbind type abap_parmbind_tab.

  ls_parmbind-name = 'OUTPUT'.
  ls_parmbind-kind = cl_abap_objectdescr=>importing.
  get reference of OUTPUT into ls_parmbind-value.
  insert ls_parmbind into table lt_parmbind.

  ls_parmbind-name = 'INPUT'.
  ls_parmbind-kind = cl_abap_objectdescr=>exporting.
  get reference of INPUT into ls_parmbind-value.
  insert ls_parmbind into table lt_parmbind.

  if_proxy_client~execute(
    exporting
      method_name = 'ADCITEM_ATTRIBUTES_IMPORT_REQU'
    changing
      parmbind_tab = lt_parmbind
  ).

  endmethod.


  method CONSTRUCTOR.

  super->constructor(
    class_name          = 'ZCO_ADCITEM_ATTRIBUTES_IMPORT'
    logical_port_name   = logical_port_name
  ).

  endmethod.
ENDCLASS.
