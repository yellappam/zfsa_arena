interface ZIF_ARN_REQUESTOR_C
  public .


  constants GC_NONE type ZARN_REQUESTOR value '' ##NO_TEXT.
  constants GC_CATEGORY_MANAGER type ZARN_REQUESTOR value 'CATMG' ##NO_TEXT.
  constants GC_TRENTS type ZARN_REQUESTOR value 'TRENTS' ##NO_TEXT.
  constants GC_FRESH type ZARN_REQUESTOR value 'FRESH' ##NO_TEXT.
  constants GC_IN_STORE type ZARN_REQUESTOR value 'INSTORE' ##NO_TEXT.
  constants GC_LOCAL_RANGE type ZARN_REQUESTOR value 'LRA' ##NO_TEXT.
endinterface.
