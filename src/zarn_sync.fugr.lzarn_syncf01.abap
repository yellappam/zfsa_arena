*----------------------------------------------------------------------*
***INCLUDE LZARN_SYNCF01 .
*----------------------------------------------------------------------*
FORM fill_field_desc.

  DATA lv_label TYPE string.

  IF zarn_sync-tabname IS NOT INITIAL AND zarn_sync-fldname IS NOT INITIAL.
    " Select and display field descriptions

    CALL FUNCTION 'DDIF_FIELDLABEL_GET'
      EXPORTING
        tabname        = zarn_sync-tabname
        fieldname      = zarn_sync-fldname
*       LANGU          = SY-LANGU
*       LFIELDNAME     = ' '
      IMPORTING
        label          = lv_label
      EXCEPTIONS
        not_found      = 1
        internal_error = 2
        OTHERS         = 3.
    IF sy-subrc <> 0.
* Implement suitable error handling here
      CLEAR zarn_sync-fielddesc.
    ELSE.
      zarn_sync-fielddesc = lv_label.
    ENDIF.

  ELSE.
    MESSAGE 'Enter both Table and Field name' TYPE 'E'.
  ENDIF.

ENDFORM.                    "fill_site_name
