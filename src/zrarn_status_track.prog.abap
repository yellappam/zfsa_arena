*&---------------------------------------------------------------------*
*& Report  ZRARN_STATUS_TRACK
*&
*&---------------------------------------------------------------------*
*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Functional Specification 3142
* DATE WRITTEN     # 20 04 2016
* SAP VERSION      # 731
* TYPE             # Report Program
* AUTHOR           # C90002769, Rajesh Anumolu
*-----------------------------------------------------------------------
* TITLE            # AReNa: Status Tracking Report
* PURPOSE          # AReNa: Report to track SLA
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 12.06.2018
* CHANGE No.       # S/4 HANA Upgrade
* DESCRIPTION      # Explicit sort added before FAE statemente
*                  # Search for S/4 HANA Upgrade
* WHO              # Brad Gorlicki (C90012814)
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

INCLUDE ziarn_status_track_top.   " global Data

INCLUDE ziarn_status_track_sel.   " Selection screen
* INCLUDE ZIARN_STATUS_TRACK_O01                  .  " PBO-Modules
* INCLUDE ZIARN_STATUS_TRACK_I01                  .  " PAI-Modules
INCLUDE ziarn_status_track_f01                  .  " FORM-Routines


*>>>DELETED - Ivo Skolek, 3162 approval stabilisation clean up - code not USED anymore *<<<


*
*START-OF-SELECTION.
** Use single selection option on the screen
** for Retail Cat manager
*  s_rcman    = s_ccman.
*  s_rcman[]  = s_ccman[].
*
*  PERFORM get_selection_screen_data.
