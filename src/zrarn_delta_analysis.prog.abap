*&---------------------------------------------------------------------*
*& Report  ZARN_ARTICLE_UPDATE
*&
*&---------------------------------------------------------------------*
*&---------------------------------------------------------------------*
* PROJECT          # FSA
* SPECIFICATION    # 3133
* DATE WRITTEN     # 01.04.2016
* SYSTEM           # DE1
* SAP VERSION      # ECC6.0
* TYPE             # Report
* AUTHOR           # C90001363
*-----------------------------------------------------------------------
* TITLE            # Delta Analysis
* PURPOSE          # Business requirement is to get the list of all the
*                  # changes supplied in the national data. Also there
*                  # can be some differences in the regional data and
*                  # article master. Hence the requirement is to have a
*                  # central place where we can get the list of all the
*                  # changes supplied by vendor(HYBRIS) or the differences
*                  # between regional data and article master.
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      #
*-----------------------------------------------------------------------
* CALLS TO         #
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 24.04.2018
* CHANGE No.       # ChaRM DE0K917906; JIRA CI18-51 Pay By Scan Changes
* DESCRIPTION      # Pay by Scan should be set not only at the banner
*                  # level but also at the individual store level. The
*                  # previous implementation was to allow it to be set
*                  # only at the banner level on MVKE-MVGR3. The new
*                  # implementation is to add custom field ZZ_PBS to MARC.
*                  # 1. Added logic for *ZZ_PBS.
* WHO              # I00600343, Fengyu Li
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

REPORT zrarn_delta_analysis MESSAGE-ID zarena_msg.


* Constant declarations
INCLUDE ziarn_delta_analysis_constants.

* Top Include for Declarations
INCLUDE ziarn_delta_analysis_top.

* Selection Screen
INCLUDE ziarn_delta_analysis_sel.

* Main Processing Logic
INCLUDE ziarn_delta_analysis_main.

* Subroutines
INCLUDE ziarn_delta_analysis_sub.

* PBO
INCLUDE ziarn_delta_analysis_pbo.

* PAI
INCLUDE ziarn_delta_analysis_pai.
