*&---------------------------------------------------------------------*
* PROJECT          # FSA
* SPECIFICATION    # 3146
* DATE WRITTEN     # 13.11.2015
* SYSTEM           # DE1
* SAP VERSION      # ECC6.0
* TYPE             # Report
* AUTHOR           # C90001448, Greg van der Loeff
*-----------------------------------------------------------------------
* TITLE            # National and Regional Data Cleanup
* PURPOSE          # National and Regional Data Cleanup download from DB
*                  # to files and deletion for testing purpose
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      #
*-----------------------------------------------------------------------
* CALLS TO         #
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------
REPORT zrarn_cleanup
       LINE-SIZE 1023.

INCLUDE ziarn_cleanup_def.
INCLUDE ziarn_cleanup_scr.
INCLUDE ziarn_cleanup_frm.

END-OF-SELECTION.
  PERFORM process_tables.
