interface ZIF_ARENA_STATUS_C
  public .


  constants GC_COMPLETE type ZARN_E_ARENA_STATUS value 'C' ##NO_TEXT.
  constants GC_IN_PROGRESS type ZARN_E_ARENA_STATUS value 'I' ##NO_TEXT.
  constants GC_FAILED type ZARN_E_ARENA_STATUS value 'F' ##NO_TEXT.
endinterface.
