* regenerated at 02.05.2016 14:29:42 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_ADDITTYPE_TTOP.              " Global Data
  INCLUDE LZARN_ADDITTYPE_TUXX.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_ADDITTYPE_TF...              " Subroutines
* INCLUDE LZARN_ADDITTYPE_TO...              " PBO-Modules
* INCLUDE LZARN_ADDITTYPE_TI...              " PAI-Modules
* INCLUDE LZARN_ADDITTYPE_TE...              " Events
* INCLUDE LZARN_ADDITTYPE_TP...              " Local class implement.
* INCLUDE LZARN_ADDITTYPE_TT99.              " ABAP Unit tests
  INCLUDE LZARN_ADDITTYPE_TF00                    . " subprograms
  INCLUDE LZARN_ADDITTYPE_TI00                    . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
