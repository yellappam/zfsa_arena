*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_DELTA_ANALYSIS_TOP
*&---------------------------------------------------------------------*


CONSTANTS:

* AReNa Tab Typ
  BEGIN OF gc_arn_tab_typ,
    nat TYPE c               VALUE 'N', "  National Table
    reg TYPE c               VALUE 'R', "  Regional Table
  END OF gc_arn_tab_typ.

* Versions to compare
TYPES : BEGIN OF ty_s_ver_screen,
          fan         TYPE zarn_fan_id,
          vno         TYPE i,
          version     TYPE zarn_version,
          status      TYPE zarn_version_status,
          idno        TYPE zarn_idno,
          matnr       TYPE matnr,
          prod_data_b TYPE zsarn_prod_data,
          prod_data_v TYPE zsarn_prod_data,
          reg_data    TYPE zsarn_reg_data,
          cc_det      TYPE ztarn_cc_det,
        END OF ty_s_ver_screen,
        ty_t_ver_screen TYPE SORTED TABLE OF ty_s_ver_screen WITH UNIQUE KEY fan vno,


* FAN and Version Status
        BEGIN OF ty_s_fan_ver_stat,
          fan          TYPE zarn_fan_id,
          idno         TYPE zarn_idno,
          previous_ver TYPE zarn_previous_ver,
          current_ver  TYPE zarn_current_ver,
          latest_ver   TYPE zarn_latest_ver,
          article_ver  TYPE zarn_article_ver,
          changed_on   TYPE aedat,
          changed_at   TYPE aenzt,
          changed_by   TYPE aenam,
        END OF ty_s_fan_ver_stat,
        ty_t_fan_ver_stat TYPE STANDARD TABLE OF ty_s_fan_ver_stat,

* CC Fields
        BEGIN OF ty_s_cc_fields,
          chgid        TYPE zarn_chg_id,
          idno         TYPE zarn_idno,
          chg_category TYPE zarn_chg_category,
          chg_table    TYPE zarn_chg_table,
          chg_field    TYPE zarn_chg_field,
          version      TYPE zarn_version,
          chg_area     TYPE zarn_chg_area,
          chg_ind      TYPE zarn_chg_ind,
          value        TYPE cdfldvaln,
          ref_data     TYPE zarn_reference_data,
        END OF ty_s_cc_fields,
        ty_t_cc_fields TYPE STANDARD TABLE OF ty_s_cc_fields,

* FAN ID Help
        BEGIN OF ty_s_fan_help,
          fan_id      TYPE zarn_fan_id,
          description TYPE zarn_description,
        END OF ty_s_fan_help,
        ty_t_fan_help TYPE STANDARD TABLE OF ty_s_fan_help,

* Version Help
        BEGIN OF ty_s_ver_help,
          fan_id  TYPE zarn_fan_id,
          version TYPE zarn_version,
        END OF ty_s_ver_help,
        ty_t_ver_help TYPE STANDARD TABLE OF ty_s_ver_help,

* Change Area Type
        BEGIN OF ty_s_chg_area_help,
          chg_area TYPE zarn_chg_area,
          desc     TYPE zarn_description,
        END OF ty_s_chg_area_help,
        ty_t_chg_area_help TYPE STANDARD TABLE OF ty_s_chg_area_help,

* Change Table Type
        BEGIN OF ty_s_chg_tab_help,
          chg_tab  TYPE zarn_arena_table,
          chg_area TYPE zarn_chg_area,
        END OF ty_s_chg_tab_help,
        ty_t_chg_tab_help TYPE STANDARD TABLE OF ty_s_chg_tab_help,


* Article Master - MARA
        ty_s_mara         TYPE mara,
        ty_t_mara         TYPE STANDARD TABLE OF ty_s_mara,

* MARM
        ty_s_marm         TYPE marm,
        ty_t_marm         TYPE STANDARD TABLE OF ty_s_marm,

* Table Master
        ty_s_table_mastr  TYPE zarn_table_mastr,
        ty_t_table_mastr  TYPE STANDARD TABLE OF zarn_table_mastr,

* Field Config
        ty_s_field_confg  TYPE zarn_field_confg,
        ty_t_field_confg  TYPE STANDARD TABLE OF zarn_field_confg,






        BEGIN OF ty_s_zzcatman,
          matnr    TYPE matnr,
          zzcatman TYPE zmd_e_catman,
        END OF ty_s_zzcatman,

        BEGIN OF ty_s_zzcatman_ret,
          matnr    TYPE matnr,
          vkorg    TYPE vkorg,zzcatman TYPE zmd_e_catman,
        END OF ty_s_zzcatman_ret,

        BEGIN OF ty_s_marc_sos,
          matnr           TYPE matnr,
          werks           TYPE werks_d,
          bwscl           TYPE bwscl,
          zzonline_status TYPE zmd_online_status,                      "++ONLD-822 JKH 17.02.2017
          zz_pbs          TYPE zmd_e_pbs,
        END OF ty_s_marc_sos,

        BEGIN OF ty_s_wrf_matgrp_prod,
          matnr   TYPE matnr,
          node    TYPE wrf_struc_node,
          mainflg TYPE wrf_main_flag,
        END OF ty_s_wrf_matgrp_prod,

        BEGIN OF ty_s_maw1,
          matnr TYPE matnr,
          wvrkm  TYPE vrkme,
          wausm  TYPE ausme,
        END OF ty_s_maw1,

        BEGIN OF ty_s_reg_banner,
          matnr    TYPE matnr,
          vkorg    TYPE vkorg,
          vtweg    TYPE vtweg,
          zzcatman TYPE zmd_e_catman,
          sstuf    TYPE sstuf,
          vrkme    TYPE vrkme,
          prodh    TYPE prodh_d,
          versg    TYPE stgma,
          ktgrm    TYPE ktgrm,
          vmsta    TYPE vmsta,
          vmstd    TYPE vmstd,
          aumng    TYPE aumng,
        END OF ty_s_reg_banner.




DATA : gt_version             TYPE ty_t_ver_screen,
       gt_fan_ver_stat        TYPE ty_t_fan_ver_stat,
       gt_prd_version         TYPE ztarn_prd_version,
       gt_mara                TYPE ty_t_mara,
       gt_marm                TYPE ty_t_marm,
       gt_table_mastr         TYPE ty_t_table_mastr,
       gt_field_confg         TYPE ty_t_field_confg,

       gt_fan_help            TYPE ty_t_fan_help,
       gt_ver_help            TYPE ty_t_ver_help,
       gt_chg_area_help       TYPE ty_t_chg_area_help,
       gt_chg_tab_help        TYPE ty_t_chg_tab_help,
       gt_return_help         TYPE STANDARD TABLE OF ddshretval,
       gs_return_help         TYPE ddshretval,


       gt_prod_data           TYPE ztarn_prod_data,
       gt_cc_fields           TYPE ty_t_cc_fields,
       gt_cc_det_nat          TYPE ztarn_cc_det,



       gt_reg_data            TYPE ztarn_reg_data,
       gt_reg_data_ecc        TYPE ztarn_reg_data,
       gt_cc_det_reg          TYPE ztarn_cc_det,

       gt_makt                TYPE SORTED TABLE OF makt WITH NON-UNIQUE KEY matnr,
       gt_gil_zzcatman        TYPE SORTED TABLE OF ty_s_zzcatman WITH NON-UNIQUE KEY matnr,
       gt_ret_zzcatman        TYPE SORTED TABLE OF ty_s_zzcatman_ret WITH NON-UNIQUE KEY matnr vkorg,
       gt_retdc_zzcatman      TYPE SORTED TABLE OF ty_s_zzcatman WITH NON-UNIQUE KEY matnr,
       gt_host_sap            TYPE SORTED TABLE OF zmd_host_sap WITH NON-UNIQUE KEY matnr,
       gt_wlk2                TYPE SORTED TABLE OF wlk2 WITH NON-UNIQUE KEY matnr vkorg vtweg werks,
       gt_marc_sos            TYPE SORTED TABLE OF ty_s_marc_sos WITH NON-UNIQUE KEY matnr werks,
       gt_mean                TYPE TABLE OF mean,
       gt_wrf_matgrp_prod     TYPE SORTED TABLE OF ty_s_wrf_matgrp_prod WITH NON-UNIQUE KEY matnr,
       gt_reg_banner          TYPE SORTED TABLE OF ty_s_reg_banner WITH NON-UNIQUE KEY matnr,
       gt_tvarv_ref_sites_uni TYPE tvarvc_t,
       gt_tvarv_ref_sites_lni TYPE tvarvc_t,
       gtr_layer_uoms         TYPE RANGE OF meins,
       gtr_pallets_uoms       TYPE RANGE OF meins,
       gtr_inner_uoms         TYPE RANGE OF meins,
       gtr_retail_uoms        TYPE RANGE OF meins,
       gtr_shipper_uoms       TYPE RANGE OF meins,
       gtr_numtp              TYPE RANGE OF numtp,
       gt_maw1                TYPE SORTED TABLE OF ty_s_maw1 WITH NON-UNIQUE KEY matnr,
       gt_eina                TYPE SORTED TABLE OF eina WITH NON-UNIQUE KEY matnr lifnr,
       gt_eine                TYPE SORTED TABLE OF eine WITH NON-UNIQUE KEY infnr,



       gt_delta_alv           TYPE ztarn_delta_analysis_alv,

       gv_old_fan             TYPE zarn_fan_id,
       gv_user_input          TYPE flag,
       gv_national            TYPE flag,
       gv_regional            TYPE flag,
       gv_error               TYPE flag.
