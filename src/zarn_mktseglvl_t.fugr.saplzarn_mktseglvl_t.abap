* regenerated at 02.05.2016 15:27:34 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_MKTSEGLVL_TTOP.              " Global Data
  INCLUDE LZARN_MKTSEGLVL_TUXX.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_MKTSEGLVL_TF...              " Subroutines
* INCLUDE LZARN_MKTSEGLVL_TO...              " PBO-Modules
* INCLUDE LZARN_MKTSEGLVL_TI...              " PAI-Modules
* INCLUDE LZARN_MKTSEGLVL_TE...              " Events
* INCLUDE LZARN_MKTSEGLVL_TP...              " Local class implement.
* INCLUDE LZARN_MKTSEGLVL_TT99.              " ABAP Unit tests
  INCLUDE LZARN_MKTSEGLVL_TF00                    . " subprograms
  INCLUDE LZARN_MKTSEGLVL_TI00                    . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
