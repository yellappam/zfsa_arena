*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_APPROVALS_MASS_CD1
*&---------------------------------------------------------------------*

*&---------------------------------------------------------------------*
*&       Class lcl_controller
*&---------------------------------------------------------------------*
*        Text
*----------------------------------------------------------------------*
CLASS lcl_controller DEFINITION.

  PUBLIC SECTION.

    DATA: mo_view TYPE REF TO lcl_view.

    CLASS-METHODS initialize.

    CLASS-METHODS get
      RETURNING VALUE(ro_instance) TYPE REF TO lcl_controller.

    METHODS main.

    METHODS handle_event.

    METHODS get_data.

    METHODS save_data.

    METHODS call_screen_0100 IMPORTING call_screen TYPE flag.

    METHODS user_command_0100.

  PRIVATE SECTION.

    CLASS-DATA: so_singleton TYPE REF TO lcl_controller.

    DATA: mo_data TYPE REF TO lcl_model.

ENDCLASS.
