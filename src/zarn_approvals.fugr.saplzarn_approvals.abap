*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_APPROVALSTOP.                " Global Data
  INCLUDE LZARN_APPROVALSUXX.                " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_APPROVALSF...                " Subroutines
* INCLUDE LZARN_APPROVALSO...                " PBO-Modules
* INCLUDE LZARN_APPROVALSI...                " PAI-Modules
* INCLUDE LZARN_APPROVALSE...                " Events
* INCLUDE LZARN_APPROVALSP...                " Local class implement.
* INCLUDE LZARN_APPROVALST99.                " ABAP Unit tests

INCLUDE ZIARN_GUI_F03.
INCLUDE LZARN_APPROVALSF01.
