*&---------------------------------------------------------------------*
*&  Include           ZIARN_GUI_SEL
*&---------------------------------------------------------------------*


SELECTION-SCREEN BEGIN OF SCREEN 1100 AS SUBSCREEN.
SELECTION-SCREEN BEGIN OF BLOCK b2 WITH FRAME TITLE TEXT-020.
*SELECTION-SCREEN: BEGIN OF LINE,
*                  POSITION 33.
*PARAMETERS:  p_disp TYPE xfeld.
*SELECTION-SCREEN: COMMENT 23(7) text-029,
*                  END OF LINE.

* AReNa Create/Change

SELECT-OPTIONS:
     s_gtin1 FOR zarn_gtin_var-gtin_code,
     s_plu   FOR zarn_products-plu,
     s_matnr FOR zarn_products-matnr_ni MATCHCODE OBJECT mat1,
     s_fan   FOR zarn_products-fan_id MATCHCODE OBJECT zmd_zzfan,
     s_hid   FOR zarn_products-idno,
     s_desc1 FOR zarn_products-fs_short_descr_upper NO-EXTENSION   NO INTERVALS,
     s_bran1 FOR zarn_products-fs_brand_id NO-EXTENSION   NO INTERVALS.
SELECTION-SCREEN END OF BLOCK b2.

SELECTION-SCREEN BEGIN OF BLOCK b8 WITH FRAME TITLE TEXT-100.
* *Category TEAM  id
SELECT-OPTIONS:
    s_rcman  FOR zarn_reg_hdr-ret_zzcatman NO INTERVALS NO-DISPLAY,
    s_ccman  FOR zarn_reg_hdr-gil_zzcatman NO INTERVALS.
SELECTION-SCREEN END OF BLOCK b8.

SELECTION-SCREEN BEGIN OF BLOCK b3 WITH FRAME TITLE TEXT-030.

SELECT-OPTIONS:
     s_stat FOR zarn_prd_version-version_status, " zarn_control-status,
*    Available to purchase
     s_atp    FOR zarn_pir-avail_start_date MATCHCODE OBJECT cpe_timestamp,
*    created on
     s_credat FOR zarn_control-created_on,
     s_creby  FOR zarn_control-created_by MATCHCODE OBJECT kw_f4_username,
     s_chgdat FOR zarn_prd_version-changed_on,
     s_chgby  FOR zarn_prd_version-changed_by MATCHCODE OBJECT kw_f4_username,
*    presented date TBC
     s_datein FOR zarn_control-date_in.

SELECTION-SCREEN END OF BLOCK b3.

SELECTION-SCREEN BEGIN OF BLOCK b4 WITH FRAME TITLE TEXT-040.

* SAP Vendor number
SELECT-OPTIONS:
     s_gln1   FOR zarn_products-gln NO-EXTENSION NO INTERVALS,
     s_glname FOR zarn_products-gln_descr NO-EXTENSION NO INTERVALS NO-DISPLAY,
     s_van    FOR zarn_pir-vendor_article_number.
SELECTION-SCREEN END OF BLOCK b4.


* INS Begin of Change 3174 JKH 23.03.2017
SELECTION-SCREEN BEGIN OF BLOCK b5 WITH FRAME TITLE TEXT-061.
PARAMETERS: p_rmode TYPE xfeld.
SELECT-OPTIONS: s_rmhid  FOR zarn_products-idno,
                s_rmdesc FOR zarn_reg_hdr-fs_short_descr_upper NO-EXTENSION   NO INTERVALS,
                s_rmbran FOR zarn_reg_hdr-fs_brand_desc_upper  NO-EXTENSION   NO INTERVALS,
                s_sla    FOR zsarn_icare_alv-enrich_sla.
SELECTION-SCREEN END OF BLOCK b5.
* INS End of Change 3174 JKH 23.03.2017



SELECTION-SCREEN END OF SCREEN 1100.



SELECTION-SCREEN BEGIN OF SCREEN 1200 AS SUBSCREEN.
SELECTION-SCREEN BEGIN OF BLOCK b1 WITH FRAME TITLE TEXT-010.
* I Care
SELECT-OPTIONS:
     s_gtin   FOR zarn_gtin_var-gtin_code               NO-EXTENSION   NO INTERVALS,
     s_descr  FOR zarn_products-fs_short_descr_upper    NO-EXTENSION   NO INTERVALS,
     s_fbrand FOR zarn_products_ex-fs_brand_desc_upper  NO-EXTENSION   NO INTERVALS,
     s_brand  FOR zarn_products-brand_id_upper          NO-EXTENSION   NO INTERVALS.
SELECTION-SCREEN END OF BLOCK b1.

SELECTION-SCREEN BEGIN OF BLOCK b10 WITH FRAME TITLE TEXT-040.
SELECT-OPTIONS:
     s_glnd  FOR zarn_pir-gln_description       NO-EXTENSION NO INTERVALS NO-DISPLAY,
     s_gln   FOR zarn_pir-gln                   NO-EXTENSION NO INTERVALS,
     s_vartn FOR zarn_pir-vendor_article_number NO-EXTENSION NO INTERVALS.
SELECTION-SCREEN END OF BLOCK b10.
SELECTION-SCREEN END OF SCREEN 1200.



* INS Begin of Change 3174 JKH 06.03.2017
SELECTION-SCREEN BEGIN OF SCREEN 1400 AS SUBSCREEN.
SELECTION-SCREEN BEGIN OF BLOCK b11 WITH FRAME TITLE TEXT-010.

* Ascending and Descending Icons
SELECTION-SCREEN BEGIN OF LINE.
SELECTION-SCREEN COMMENT 67(5) icon_des.
SELECTION-SCREEN COMMENT 73(5) icon_asc.
SELECTION-SCREEN END OF LINE.

* GTIN Code
SELECT-OPTIONS: s_gtin5 FOR zarn_gtin_var-gtin_code NO INTERVALS.

* Foodstuffs Article Number(FAN)
SELECTION-SCREEN BEGIN OF LINE.
SELECTION-SCREEN COMMENT 1(30) TEXT-s04 FOR FIELD s_fan5.
SELECTION-SCREEN POSITION 30.
SELECT-OPTIONS: s_fan5 FOR zarn_products-fan_id MATCHCODE OBJECT zmd_zzfan NO INTERVALS.
SELECTION-SCREEN COMMENT 60(1) TEXT-s18.
PARAMETERS: p_seq4 TYPE n LENGTH 1.
SELECTION-SCREEN COMMENT 65(1) TEXT-s18.
PARAMETERS: p_rs4a TYPE xfeld RADIOBUTTON GROUP rs4.
SELECTION-SCREEN COMMENT 71(1) TEXT-s18.
PARAMETERS: p_rs4d TYPE xfeld RADIOBUTTON GROUP rs4.
SELECTION-SCREEN END OF LINE.

* Short Description
SELECT-OPTIONS: s_shdesc FOR zarn_products_2-description_upper NO INTERVALS.

* FS Short Description
SELECTION-SCREEN BEGIN OF LINE.
SELECTION-SCREEN COMMENT 1(30) TEXT-s06 FOR FIELD s_descr5.
SELECTION-SCREEN POSITION 30.
SELECT-OPTIONS: s_descr5 FOR zarn_products-fs_short_descr_upper MATCHCODE OBJECT zmd_zzfan NO INTERVALS.
SELECTION-SCREEN COMMENT 60(1) TEXT-s18.
PARAMETERS: p_seq6 TYPE n LENGTH 1.
SELECTION-SCREEN COMMENT 65(1) TEXT-s18.
PARAMETERS: p_rs6a TYPE xfeld RADIOBUTTON GROUP rs6.
SELECTION-SCREEN COMMENT 71(1) TEXT-s18.
PARAMETERS: p_rs6d TYPE xfeld RADIOBUTTON GROUP rs6.
SELECTION-SCREEN END OF LINE.

* FS Brand
SELECTION-SCREEN BEGIN OF LINE.
SELECTION-SCREEN COMMENT 1(30) TEXT-s07 FOR FIELD s_fbran5.
SELECTION-SCREEN POSITION 30.
SELECT-OPTIONS: s_fbran5 FOR zarn_products_2-fs_brand_id_upper MATCHCODE OBJECT zmd_zzfan NO INTERVALS.
SELECTION-SCREEN COMMENT 60(1) TEXT-s18.
PARAMETERS: p_seq7 TYPE n LENGTH 1.
SELECTION-SCREEN COMMENT 65(1) TEXT-s18.
PARAMETERS: p_rs7a TYPE xfeld RADIOBUTTON GROUP rs7.
SELECTION-SCREEN COMMENT 71(1) TEXT-s18.
PARAMETERS: p_rs7d TYPE xfeld RADIOBUTTON GROUP rs7.
SELECTION-SCREEN END OF LINE.

* National Brand
SELECT-OPTIONS: s_brand5 FOR zarn_products-brand_id_upper NO INTERVALS.

*SELECTION-SCREEN SKIP.


* Supplier Submitted Start Date
SELECTION-SCREEN BEGIN OF LINE.
SELECTION-SCREEN COMMENT 1(30) TEXT-s08 FOR FIELD s_sbdtst.
SELECTION-SCREEN POSITION 30.
SELECT-OPTIONS: s_sbdtst FOR zarn_prd_version-changed_on NO-EXTENSION NO INTERVALS.
SELECTION-SCREEN COMMENT 44(2) TEXT-s20.
SELECTION-SCREEN POSITION 46.
SELECT-OPTIONS: s_sbdten FOR zarn_prd_version-changed_on NO-EXTENSION NO INTERVALS.
SELECTION-SCREEN COMMENT 60(1) TEXT-s18.
PARAMETERS: p_seq8 TYPE n LENGTH 1 DEFAULT '2'.
SELECTION-SCREEN COMMENT 65(1) TEXT-s18.
PARAMETERS: p_rs8a TYPE xfeld RADIOBUTTON GROUP rs8.
SELECTION-SCREEN COMMENT 71(1) TEXT-s18.
PARAMETERS: p_rs8d TYPE xfeld RADIOBUTTON GROUP rs8 DEFAULT 'X'.
SELECTION-SCREEN END OF LINE.

** Supplier Submitted End Date
*SELECTION-SCREEN BEGIN OF LINE.
*SELECTION-SCREEN COMMENT 1(28) text-s09 FOR FIELD s_sbdten.
*SELECT-OPTIONS: s_sbdten FOR zarn_prd_version-changed_on NO-EXTENSION NO INTERVALS.
*SELECTION-SCREEN COMMENT 58(1) text-s18.
*PARAMETERS: p_seq9 TYPE n LENGTH 1.
*SELECTION-SCREEN COMMENT 63(1) text-s18.
*PARAMETERS: p_rs9a TYPE xfeld RADIOBUTTON GROUP rs9.
*SELECTION-SCREEN COMMENT 68(1) text-s18.
*PARAMETERS: p_rs9d TYPE xfeld RADIOBUTTON GROUP rs9.
*SELECTION-SCREEN END OF LINE.

* Hybris Code
SELECTION-SCREEN BEGIN OF LINE.
SELECTION-SCREEN COMMENT 1(30) TEXT-s19.   " FOR FIELD s_hid5.
*SELECT-OPTIONS: s_hid5 FOR zarn_products-fsni_gilmours_value NO-DISPLAY.
SELECTION-SCREEN COMMENT 60(1) TEXT-s18.
PARAMETERS: p_seq19 TYPE n LENGTH 1.
SELECTION-SCREEN COMMENT 65(1) TEXT-s18.
PARAMETERS: p_rs19a TYPE xfeld RADIOBUTTON GROUP rs19.
SELECTION-SCREEN COMMENT 71(1) TEXT-s18.
PARAMETERS: p_rs19d TYPE xfeld RADIOBUTTON GROUP rs19.
SELECTION-SCREEN END OF LINE.



SELECTION-SCREEN SKIP.

* I-Care Flag
SELECTION-SCREEN BEGIN OF LINE.
SELECTION-SCREEN COMMENT 1(30) TEXT-s10 FOR FIELD s_icflag.
SELECTION-SCREEN POSITION 30.
SELECT-OPTIONS: s_icflag FOR zarn_products-fsni_icare_value NO-EXTENSION NO INTERVALS.
SELECTION-SCREEN COMMENT 60(1) TEXT-s18.
PARAMETERS: p_seq10 TYPE n LENGTH 1 DEFAULT '1'.
SELECTION-SCREEN COMMENT 65(1) TEXT-s18.
PARAMETERS: p_rs10a TYPE xfeld RADIOBUTTON GROUP rs10.
SELECTION-SCREEN COMMENT 71(1) TEXT-s18.
PARAMETERS: p_rs10d TYPE xfeld RADIOBUTTON GROUP rs10.
SELECTION-SCREEN END OF LINE.

* In-Store Flag
SELECTION-SCREEN BEGIN OF LINE.
SELECTION-SCREEN COMMENT 1(30) TEXT-s11 FOR FIELD s_instor.
SELECTION-SCREEN POSITION 30.
SELECT-OPTIONS: s_instor FOR zarn_products-fsni_instore_value NO-EXTENSION NO INTERVALS.
*SELECTION-SCREEN COMMENT 58(1) text-s18.
*PARAMETERS: p_seq11 TYPE n LENGTH 1.
*SELECTION-SCREEN COMMENT 63(1) text-s18.
*PARAMETERS: p_rs11a TYPE xfeld RADIOBUTTON GROUP rs11.
*SELECTION-SCREEN COMMENT 68(1) text-s18.
*PARAMETERS: p_rs11d TYPE xfeld RADIOBUTTON GROUP rs11.
SELECTION-SCREEN END OF LINE.

* Gilmours Flag
SELECTION-SCREEN BEGIN OF LINE.
SELECTION-SCREEN COMMENT 1(30) TEXT-s12 FOR FIELD s_gilmrs.
SELECTION-SCREEN POSITION 30.
SELECT-OPTIONS: s_gilmrs FOR zarn_products-fsni_gilmours_value NO-EXTENSION NO INTERVALS.
*SELECTION-SCREEN COMMENT 58(1) text-s18.
*PARAMETERS: p_seq12 TYPE n LENGTH 1.
*SELECTION-SCREEN COMMENT 63(1) text-s18.
*PARAMETERS: p_rs12a TYPE xfeld RADIOBUTTON GROUP rs12.
*SELECTION-SCREEN COMMENT 68(1) text-s18.
*PARAMETERS: p_rs12d TYPE xfeld RADIOBUTTON GROUP rs12.
SELECTION-SCREEN END OF LINE.

PARAMETERS: p_flagop TYPE zarn_flag_operator DEFAULT 'AND' OBLIGATORY.


SELECTION-SCREEN END OF BLOCK b11.

SELECTION-SCREEN BEGIN OF BLOCK b12 WITH FRAME TITLE TEXT-040.
SELECT-OPTIONS:
     s_gln5   FOR zarn_pir-gln                          NO INTERVALS,
     s_vartn5 FOR zarn_pir-vendor_article_number        NO INTERVALS,
     s_glnd5  FOR zarn_pir-gln_description_upper        NO INTERVALS,
     s_mgln5  FOR zarn_pir-manufacturer_gln             NO INTERVALS,
     s_mglnd5 FOR zarn_pir-manufacturer_gln_descr_upper NO INTERVALS.

SELECTION-SCREEN END OF BLOCK b12.

SELECTION-SCREEN BEGIN OF BLOCK b13 WITH FRAME TITLE TEXT-043.
PARAMETERS: p_stindx TYPE int4,
            p_pagesz TYPE int4  DEFAULT '30'  OBLIGATORY,
            p_itempp TYPE n LENGTH 5 DEFAULT '99999' OBLIGATORY.
SELECTION-SCREEN END OF BLOCK b13.

SELECTION-SCREEN END OF SCREEN 1400.
* INS End of Change 3174 JKH 06.03.2017








SELECTION-SCREEN BEGIN OF SCREEN 1300 AS SUBSCREEN.
*SELECTION-SCREEN BEGIN OF BLOCK b5 WITH FRAME TITLE text-050.
* Approvals
SELECTION-SCREEN BEGIN OF BLOCK ap1 WITH FRAME TITLE TEXT-020.
SELECT-OPTIONS: s_aprgt   FOR zarn_gtin_var-gtin_code,
                s_aprplu  FOR zarn_products-plu,
                s_aprmt   FOR zarn_products-matnr_ni MATCHCODE OBJECT mat1,
                s_aprfi   FOR zarn_products-fan_id MATCHCODE OBJECT zmd_zzfan,
                s_aprhid  FOR zarn_products-idno,
                s_aprdes  FOR zarn_products-fs_short_descr_upper NO-EXTENSION   NO INTERVALS,
                s_aprbrn  FOR zarn_products-fs_brand_id NO-EXTENSION   NO INTERVALS.
SELECTION-SCREEN END OF BLOCK ap1.

SELECTION-SCREEN BEGIN OF BLOCK b9 WITH FRAME TITLE TEXT-100.
* *Category TEAM  id
SELECT-OPTIONS: s_aprrcm  FOR zarn_reg_hdr-ret_zzcatman NO INTERVALS NO-DISPLAY,
                s_aprcmn  FOR zarn_reg_hdr-gil_zzcatman NO INTERVALS.
SELECTION-SCREEN END OF BLOCK b9.

*{ +3162 - Revised approval selection screen
*SELECTION-SCREEN BEGIN OF BLOCK ap2 WITH FRAME TITLE text-ap2.
*SELECT-OPTIONS: s_aprhs FOR zarn_prd_version-version_status,
*                s_aprhr FOR zarn_prd_version-release_status,
*                s_aprrs FOR zarn_reg_hdr-status,
*                s_aprrr FOR zarn_reg_hdr-release_status,
*                s_aprcc FOR zarn_cc_hdr-chg_category,
*                s_aprcr FOR zarn_reg_hdr-ersda, "zarn_prd_version-changed_on,
*                s_aprco FOR zarn_reg_hdr-laeda. "zarn_prd_version-changed_on.
*SELECTION-SCREEN END OF BLOCK ap2.

* Approvals selection criteria
SELECTION-SCREEN BEGIN OF BLOCK ar1 WITH FRAME TITLE TEXT-ar1.
* New or Change article
SELECTION-SCREEN BEGIN OF LINE.
PARAMETERS: p_apnew TYPE xfeld RADIOBUTTON GROUP ag1.
SELECTION-SCREEN COMMENT 3(15) TEXT-ar4.
PARAMETERS: p_apchg TYPE xfeld RADIOBUTTON GROUP ag1.
SELECTION-SCREEN COMMENT 21(15) TEXT-ar5.
PARAMETERS: p_apncb TYPE xfeld RADIOBUTTON GROUP ag1 DEFAULT 'X'.
SELECTION-SCREEN COMMENT 39(10) TEXT-ar8.
SELECTION-SCREEN END OF LINE.

* National or regional change
SELECTION-SCREEN BEGIN OF LINE.
PARAMETERS: p_apnat TYPE xfeld RADIOBUTTON GROUP ag2.
SELECTION-SCREEN COMMENT 3(15) TEXT-ar6.
PARAMETERS: p_apreg TYPE xfeld RADIOBUTTON GROUP ag2.
SELECTION-SCREEN COMMENT 21(15) TEXT-ar7.
PARAMETERS: p_apnrb TYPE xfeld RADIOBUTTON GROUP ag2 DEFAULT 'X'.
SELECTION-SCREEN COMMENT 39(10) TEXT-ar8.
SELECTION-SCREEN END OF LINE.

*Others(Team, Status, Category)
SELECT-OPTIONS: s_aptem FOR zarn_s_approval_sel-team_code,
                s_apsta FOR zarn_s_approval_sel-status DEFAULT zcl_arn_approval_backend=>gc_stat-appr_ready,
                s_apcat FOR zarn_s_approval_sel-chg_category.
SELECTION-SCREEN END OF BLOCK ar1.
*} +3162 - Revised approval selection screen

SELECTION-SCREEN BEGIN OF BLOCK ap3 WITH FRAME TITLE TEXT-040.
SELECT-OPTIONS: s_aprgl FOR zarn_pir-gln   NO-EXTENSION NO INTERVALS,
                s_aprgn FOR zarn_pir-gln_description NO-DISPLAY,
                s_aprgv FOR zarn_pir-vendor_article_number.
SELECTION-SCREEN END OF BLOCK ap3.

*SELECTION-SCREEN END OF BLOCK b5.
SELECTION-SCREEN END OF SCREEN 1300.


SELECTION-SCREEN: BEGIN OF TABBED BLOCK mytab FOR 32 LINES,
                  TAB (20) button1 USER-COMMAND push1,
                  TAB (20) button2 USER-COMMAND push2,
                  TAB (20) button3 USER-COMMAND push3
                                   DEFAULT SCREEN 1300,
                  END OF BLOCK mytab.

SELECTION-SCREEN BEGIN OF BLOCK b7 WITH FRAME TITLE TEXT-071.
PARAMETERS:
  p_ratio TYPE ajhranp            DEFAULT 90 OBLIGATORY,
  p_vari  TYPE disvariant-variant.
SELECTION-SCREEN END OF BLOCK b7.

AT SELECTION-SCREEN ON VALUE-REQUEST FOR s_gln1-low.
  PERFORM get_lifnr_gln CHANGING s_gln1-low.

AT SELECTION-SCREEN ON VALUE-REQUEST FOR s_gln-low.
  PERFORM get_lifnr_gln CHANGING s_gln-low.

AT SELECTION-SCREEN ON VALUE-REQUEST FOR s_aprgl-low.
  PERFORM get_lifnr_gln CHANGING s_aprgl-low.


*---------------------------------------------------------------------*
*
*---------------------------------------------------------------------*
INITIALIZATION.

  CLEAR gv_cr_r1_switch.
  SELECT SINGLE low FROM tvarvc INTO gv_cr_r1_switch    "++3174 JKH 06.03.2017
    WHERE name = gc_cr_r1_switch
      AND type = 'P'.

*  IF sy-uname  = 'C90001363'.
*    gv_cr_r1_switch = abap_true.
*  ENDIF.

  PERFORM restrict_select_options_icare.       "++3174 JKH 06.03.2017

  button1 = TEXT-080.  " AReNa Create/Change
  button2 = TEXT-070.  " I Care
  button3 = TEXT-090.  " Approvals

* get the selection screen tab from memory
  mytab-prog      = sy-repid.
  GET PARAMETER ID 'ZARN_GUI_TAB' FIELD gv_sel_scr_tab.
  IF   gv_sel_scr_tab IS INITIAL.
*   default
    mytab-dynnr     = 1100.
    mytab-activetab = gc_arena_tab.
  ELSE.
    mytab-activetab = gv_sel_scr_tab.
    CASE mytab-activetab.
      WHEN gc_icare_tab.
        IF gv_cr_r1_switch = abap_true.           "++3174 JKH 06.03.2017
          mytab-dynnr = 1400.
        ELSE.
          mytab-dynnr = 1200.
        ENDIF.
      WHEN gc_arena_tab.
        mytab-dynnr = 1100.
      WHEN gc_appro_tab.
        mytab-dynnr = 1300.
    ENDCASE.
  ENDIF.


* use authorisation object to read the value assiged to the user
* to determine field reference value
  CALL FUNCTION 'SUSR_USER_AUTH_FOR_OBJ_GET'
    EXPORTING
      user_name           = sy-uname
      sel_object          = 'ZARN_FREF'
    TABLES
      values              = gt_values
    EXCEPTIONS
      user_name_not_exist = 1
      not_authorized      = 2
      internal_error      = 3
      OTHERS              = 4.

  IF sy-subrc IS NOT INITIAL.
    MESSAGE e000 WITH TEXT-210.
    RETURN.
  ENDIF.

  "IS - cater for display only auths
  IF line_exists( gt_values[ objct = 'ZARN_FREF' field = 'FIELD_REF' von = gc_auth_fref_change  ] ).
    CLEAR gv_auth_display_only.
  ELSE.
    gv_auth_display_only = abap_true.
  ENDIF.

  " sort in case user has multiple auth. (both change and display)
  SORT gt_values BY von.

* check if there is a value
  READ TABLE gt_values INTO gs_values
       WITH KEY objct = 'ZARN_FREF'
                field = 'FIELD_REF'.
  IF sy-subrc IS NOT INITIAL OR
     gs_values-von IS INITIAL.
* Implement suitable error handling here
    MESSAGE e000 WITH TEXT-210.
    RETURN.
  ENDIF.

* assign value to check in database
  gv_field_ref = gs_values-von.

* check if there is a value that is valid ?
* Get GUI Relevant settings
  SELECT SINGLE field_ref INTO gv_field_ref
         FROM zarn_gui_sel
       WHERE field_ref = gv_field_ref.
  IF sy-subrc IS NOT INITIAL.
*   Implement suitable error handling here
    MESSAGE e000 WITH TEXT-210.
    RETURN.
  ENDIF.

* is saved in 'def_layout'.
  IF gs_variant-report IS INITIAL.

    CLEAR gs_variant.
    gs_variant-report   = sy-repid.
    gs_variant-username = sy-uname.

    CALL FUNCTION 'LVC_VARIANT_DEFAULT_GET'
      EXPORTING
        i_save     = gc_save
      CHANGING
        cs_variant = gs_variant
      EXCEPTIONS
        not_found  = 2.
    IF sy-subrc = 2.
      RETURN.
    ELSE.
*     set name of layout on selection screen
      p_vari = gs_variant-variant.
    ENDIF.
  ENDIF.                             "default IS INITIAL
*---------------------------------------------------------------------*
*
*---------------------------------------------------------------------*
AT SELECTION-SCREEN.
*  ^.

  CLEAR gv_cr_r1_switch.
  SELECT SINGLE low FROM tvarvc INTO gv_cr_r1_switch    "++3174 JKH 06.03.2017
    WHERE name = gc_cr_r1_switch
      AND type = 'P'.

*  IF sy-uname  = 'C90001363'.
*    gv_cr_r1_switch = abap_true.
*  ENDIF.

  CASE sy-dynnr.
    WHEN 1000.
      CASE sy-ucomm.
        WHEN gc_icare_tab.  "'PUSH2'. ONLI
          IF gv_cr_r1_switch = abap_true.           "++3174 JKH 06.03.2017
            mytab-dynnr = 1400.
          ELSE.
            mytab-dynnr = 1200.
          ENDIF.

          mytab-activetab = gc_icare_tab.

        WHEN gc_arena_tab.   "'PUSH1'. ONLI
          mytab-dynnr     = 1100.
          mytab-activetab = gc_arena_tab.

        WHEN gc_appro_tab.   "'PUSH3'. ONLI
          mytab-dynnr     = 1300.
          mytab-activetab = gc_appro_tab.

      ENDCASE.

*   Selection screen TAB check - the user must have a selection criteria
*                                before execute
    WHEN 1100.
      IF sy-ucomm EQ 'ONLI'.
        IF s_gtin1[]   IS INITIAL  AND
           s_plu[]     IS INITIAL  AND
           s_matnr[]   IS INITIAL  AND
           s_fan[]     IS INITIAL  AND
           s_hid[]     IS INITIAL  AND
           s_desc1[]   IS INITIAL  AND
           s_bran1[]   IS INITIAL  AND
           s_rcman[]   IS INITIAL  AND
           s_ccman[]   IS INITIAL  AND
           s_stat[]    IS INITIAL  AND
           s_atp[]     IS INITIAL  AND
           s_credat[]  IS INITIAL  AND
           s_creby[]   IS INITIAL  AND
           s_chgdat[]  IS INITIAL  AND
           s_chgby[]   IS INITIAL  AND
           s_datein[]  IS INITIAL  AND
           s_gln1[]    IS INITIAL  AND
           s_glname[]  IS INITIAL  AND
           s_van[]     IS INITIAL.

          MESSAGE e000 WITH TEXT-205.

        ENDIF.
      ENDIF.

    WHEN 1200.
      IF sy-ucomm EQ 'ONLI'.
        IF s_gtin[]    IS INITIAL  AND
           s_descr[]   IS INITIAL  AND
           s_brand[]   IS INITIAL  AND
           s_fbrand[]  IS INITIAL  AND
           s_glnd[]    IS INITIAL  AND
           s_gln[]     IS INITIAL  AND
           s_vartn[]   IS INITIAL.
          MESSAGE e000 WITH TEXT-205.
        ENDIF.
      ENDIF.

* INS Begin of Change IR5173046 JKH 14.09.2016
* GTIN code must be numeric
      IF s_gtin-low IS NOT INITIAL AND s_gtin-low CN '1234567890 '.
        MESSAGE e075.
      ENDIF.
* INS End of Change IR5173046 JKH 14.09.2016


* INS Begin of Change 3174 JKH 06.03.2017
    WHEN 1400.
      IF sy-ucomm EQ 'ONLI'.
        IF s_gtin5[]    IS INITIAL  AND
           s_fan5[]     IS INITIAL  AND
           s_shdesc[]   IS INITIAL  AND
           s_descr5[]   IS INITIAL  AND
           s_fbran5[]   IS INITIAL  AND
           s_brand5[]   IS INITIAL  AND
           s_gln5[]     IS INITIAL  AND
           s_vartn5[]   IS INITIAL  AND
           s_glnd5[]    IS INITIAL  AND
           s_mgln5[]    IS INITIAL  AND
           s_mglnd5[]   IS INITIAL.
          MESSAGE e000 WITH TEXT-205.
        ENDIF.
      ENDIF.

* GTIN code must be numeric
      LOOP AT s_gtin5[] INTO s_gtin5 WHERE low CN '1234567890 '.
        EXIT.
      ENDLOOP.
      IF sy-subrc = 0.
        MESSAGE e075.
      ENDIF.
* INS End of Change 3174 JKH 06.03.2017



    WHEN 1300.
      IF sy-ucomm EQ 'ONLI'.
        IF s_aprgt[]   IS INITIAL  AND
           s_aprplu[]  IS INITIAL  AND
           s_aprmt[]   IS INITIAL  AND
           s_aprfi[]   IS INITIAL  AND
           s_aprhid[]  IS INITIAL  AND
           s_aprdes[]  IS INITIAL  AND
           s_aprbrn[]  IS INITIAL  AND
           s_aprcmn[]  IS INITIAL  AND
*{ +3162
*           s_aprhs[]   IS INITIAL  AND
*           s_aprhr[]   IS INITIAL  AND
*           s_aprrs[]   IS INITIAL  AND
*           s_aprrr[]   IS INITIAL  AND
*           s_aprcc[]   IS INITIAL  AND
*           s_aprcr[]   IS INITIAL  AND
*           s_aprco[]   IS INITIAL  AND
          p_apnew IS INITIAL AND
          p_apchg IS INITIAL AND
          p_apnat IS INITIAL AND
          p_apreg IS INITIAL AND
          s_apsta IS INITIAL AND
          s_aptem IS INITIAL AND
          s_apcat IS INITIAL AND
*} +3162

           s_aprgl[]   IS INITIAL  AND
           s_aprgn[]   IS INITIAL  AND
           s_aprgv[]   IS INITIAL .

          MESSAGE e000 WITH TEXT-205.

        ENDIF.
      ENDIF.

  ENDCASE.

AT SELECTION-SCREEN ON p_ratio.
  IF p_ratio GT 90 OR p_ratio LT 5.
    MESSAGE e000 WITH TEXT-016.
  ENDIF.

*---------------------------------------------------------------------*
*
*---------------------------------------------------------------------*
AT SELECTION-SCREEN ON VALUE-REQUEST FOR p_vari.
* popup F4 help to select a layout
  PERFORM alv_worklist_variant.


* INS Begin of Change 3174 JKH 15.03.2017
AT SELECTION-SCREEN ON p_flagop.
  IF mytab-activetab = gc_icare_tab AND gv_cr_r1_switch = abap_true.
    IF p_flagop IS NOT INITIAL.
      IF p_flagop NE 'AND' AND p_flagop NE 'OR'.
* Enter a valid value
        MESSAGE e088.
      ENDIF.
    ENDIF.
  ENDIF.


AT SELECTION-SCREEN ON s_icflag.
  IF mytab-activetab = gc_icare_tab AND gv_cr_r1_switch = abap_true.
    IF s_icflag-low IS NOT INITIAL.
      IF s_icflag-low NE abap_true.
* Enter a valid value
        MESSAGE e088.
      ENDIF.
    ENDIF.
  ENDIF.


AT SELECTION-SCREEN ON s_instor.
  IF mytab-activetab = gc_icare_tab AND gv_cr_r1_switch = abap_true.
    IF s_instor-low IS NOT INITIAL.
      IF s_instor-low NE abap_true.
* Enter a valid value
        MESSAGE e088.
      ENDIF.
    ENDIF.
  ENDIF.

AT SELECTION-SCREEN ON s_gilmrs.
  IF mytab-activetab = gc_icare_tab AND gv_cr_r1_switch = abap_true.
    IF s_gilmrs-low IS NOT INITIAL.
      IF s_gilmrs-low NE abap_true.
* Enter a valid value
        MESSAGE e088.
      ENDIF.
    ENDIF.
  ENDIF.

* INS End of Change 3174 JKH 15.03.2017


AT SELECTION-SCREEN OUTPUT.
  icon_des = icon_sort_up.
  icon_asc = icon_sort_down.
*  blank1    = icon_space.



*&---------------------------------------------------------------------*
*&      Form  ALV_WORKLIST_VARIANT
*&---------------------------------------------------------------------*
FORM alv_worklist_variant .

  DATA: lv_save        TYPE char01,  "I_SAVE: modus for saving a layout
        lv_layout      TYPE disvariant,
        lv_spec_layout TYPE disvariant,     "specific layout
        lv_exit        TYPE c.  "is set if the user has aborted a layout popup

  CLEAR lv_layout.
  MOVE sy-repid TO lv_layout-report.

  CALL FUNCTION 'LVC_VARIANT_F4'
    EXPORTING
      is_variant = lv_layout
      i_save     = gc_save
    IMPORTING
      e_exit     = lv_exit
      es_variant = lv_spec_layout
    EXCEPTIONS
      not_found  = 1
      OTHERS     = 2.
  IF sy-subrc NE 0.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
            WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ELSE.
    IF lv_exit IS INITIAL.
*     set name of layout on selection screen
      p_vari    = lv_spec_layout-variant.
    ENDIF.
  ENDIF.
ENDFORM.                    " ALV_WORKLIST_VARIANT
