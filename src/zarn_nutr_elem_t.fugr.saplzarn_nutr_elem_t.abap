* regenerated at 25.05.2016 11:45:23 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_NUTR_ELEM_TTOP.              " Global Data
  INCLUDE LZARN_NUTR_ELEM_TUXX.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_NUTR_ELEM_TF...              " Subroutines
* INCLUDE LZARN_NUTR_ELEM_TO...              " PBO-Modules
* INCLUDE LZARN_NUTR_ELEM_TI...              " PAI-Modules
* INCLUDE LZARN_NUTR_ELEM_TE...              " Events
* INCLUDE LZARN_NUTR_ELEM_TP...              " Local class implement.
* INCLUDE LZARN_NUTR_ELEM_TT99.              " ABAP Unit tests
  INCLUDE LZARN_NUTR_ELEM_TF00                    . " subprograms
  INCLUDE LZARN_NUTR_ELEM_TI00                    . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
