* regenerated at 12.05.2016 16:32:50 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_NUTR_BQTY_TTOP.              " Global Data
  INCLUDE LZARN_NUTR_BQTY_TUXX.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_NUTR_BQTY_TF...              " Subroutines
* INCLUDE LZARN_NUTR_BQTY_TO...              " PBO-Modules
* INCLUDE LZARN_NUTR_BQTY_TI...              " PAI-Modules
* INCLUDE LZARN_NUTR_BQTY_TE...              " Events
* INCLUDE LZARN_NUTR_BQTY_TP...              " Local class implement.
* INCLUDE LZARN_NUTR_BQTY_TT99.              " ABAP Unit tests
  INCLUDE LZARN_NUTR_BQTY_TF00                    . " subprograms
  INCLUDE LZARN_NUTR_BQTY_TI00                    . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
