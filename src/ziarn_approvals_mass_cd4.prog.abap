*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_APPROVALS_MASS_CD4
*&---------------------------------------------------------------------*

*&---------------------------------------------------------------------*
*&  Class Definition - Event handler class definition for Tree
*&---------------------------------------------------------------------*
CLASS lcl_appr_mass_event_handler DEFINITION.
  PUBLIC SECTION.
    METHODS :

* Link Click on Tree items
      appr_mass_handle_link_click FOR EVENT link_click OF cl_gui_alv_tree
        IMPORTING fieldname node_key sender,

      on_function_selected FOR EVENT function_selected OF cl_gui_toolbar
        IMPORTING fcode sender,

      dialog_close FOR EVENT close OF cl_gui_dialogbox_container
        IMPORTING sender.

ENDCLASS.                    "lcl_appr_nat_event_handler DEFINITION
