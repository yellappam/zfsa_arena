*&---------------------------------------------------------------------*
*& Report YM_MC_DEFAULT
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
*-----------------------------------------------------------------------
* DATE WRITTEN     # 20.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # This is a temporary program and shouldn't be transported
*                  # to Production. It is for testing purpose in QA as DEV
*                  # data is not up-to date
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

REPORT zrmm_mc_default.

PARAMETERS: p_gln   TYPE char30 OBLIGATORY DEFAULT '94290008068',
            p_brand TYPE wrf_brand_descr DEFAULT 'DARRELL',
            p_maktx TYPE maktx DEFAULT 'DARRELL LEA LICORICE SOFT BLACK 150G'.

START-OF-SELECTION.
  PERFORM execute.

FORM execute .

  TYPES: BEGIN OF ts_output,
           matnr       TYPE matnr,
           matkl       TYPE matkl,
           matkl_desc  TYPE wgbez,
           gln_count   TYPE i,
           brand_descr TYPE wrf_brand_descr,
           brand_count TYPE i,
           maktx       TYPE  maktx,
           maktx_count TYPE i,
           total       TYPE i,
         END  OF ts_output,
         tt_output TYPE STANDARD TABLE OF ts_output WITH DEFAULT KEY.

  DATA: lv_location1   TYPE bbbnr,
        lv_location2   TYPE bbsnr,
        lv_check_digit TYPE bubkz,
        ls_layout_key  TYPE salv_s_layout_key,
        lt_output      TYPE tt_output,
        gt_word        TYPE STANDARD TABLE OF string.

  DATA(lv_gln) = p_gln.
  lv_location1 = lv_gln+0(7).
  SHIFT lv_gln LEFT BY 7 PLACES.

  DATA(lv_length) = strlen( lv_gln ).
  IF lv_length > 0.
    lv_length = lv_length - 1.
    lv_location2 = lv_gln+0(lv_length).
    lv_check_digit = lv_gln+lv_length(1).
  ENDIF.

  SELECT lifnr
    FROM lfa1
    INTO TABLE @DATA(lt_vendor)
    WHERE bbbnr = @lv_location1
      AND bbsnr = @lv_location2
      AND bubkz = @lv_check_digit.
  IF sy-subrc <> 0.
    RETURN.
  ENDIF.

  SELECT DISTINCT matnr
    FROM eina
    INTO TABLE @DATA(lt_article)
    FOR ALL ENTRIES IN @lt_vendor
    WHERE lifnr = @lt_vendor-lifnr.
  IF sy-subrc <> 0.
    RETURN.
  ENDIF.

  SELECT a~matnr,
         maktx,
         matkl,
         a~brand_id,
         brand_descr
    FROM mara AS a
    INNER JOIN makt AS t ON t~matnr = a~matnr AND
                            t~spras = @sy-langu
    LEFT OUTER JOIN wrf_brands_t AS b ON b~brand_id = a~brand_id AND
                                         b~language = @sy-langu
    INTO TABLE @DATA(lt_mara)
    FOR ALL ENTRIES IN @lt_article
    WHERE a~matnr = @lt_article-matnr.
  IF sy-subrc <> 0.
    RETURN.
  ENDIF.

  LOOP AT lt_mara INTO DATA(ls_mara).
    APPEND INITIAL LINE TO lt_output ASSIGNING FIELD-SYMBOL(<ls_output>).
    MOVE-CORRESPONDING ls_mara TO <ls_output>.

    DATA(ls_merch_cat) = zcl_merchandise_cat_services=>get_instance( )->get_merchandise_cat( ls_mara-matkl ).
    <ls_output>-matkl_desc = ls_merch_cat-merchandise_cat_desc.
    <ls_output>-gln_count = 16000.

    " Brand points
    IF p_brand IS NOT INITIAL.
      FIND FIRST OCCURRENCE OF p_brand IN ls_mara-brand_descr IGNORING CASE.
      IF sy-subrc = 0.
        <ls_output>-brand_count = 6000.
      ENDIF.
    ENDIF.

    " Merchandise Category Points
    IF p_maktx IS NOT INITIAL.
      CLEAR: gt_word.
      SPLIT p_maktx AT '' INTO TABLE gt_word.

      LOOP AT gt_word INTO DATA(lv_word).

        FIND FIRST OCCURRENCE OF  lv_word IN <ls_output>-maktx IGNORING CASE.
        IF sy-subrc = 0.
          IF <ls_output>-maktx_count IS INITIAL.
            <ls_output>-maktx_count = 5000.
          ELSE.
            ADD 1 TO <ls_output>-maktx_count.
          ENDIF.
        ENDIF.

      ENDLOOP.
    ENDIF.

    <ls_output>-total = <ls_output>-gln_count + <ls_output>-brand_count + <ls_output>-maktx_count.
  ENDLOOP.

  SORT lt_output BY total DESCENDING.
  TRY.
      cl_salv_table=>factory( IMPORTING r_salv_table = DATA(lo_alv)
                              CHANGING t_table = lt_output ).
      lo_alv->get_columns( )->set_optimize( abap_true ).

      lo_alv->get_columns( )->get_column( 'GLN_COUNT')->set_short_text( 'GLN').
      lo_alv->get_columns( )->get_column( 'BRAND_COUNT')->set_short_text( 'Brand').
      lo_alv->get_columns( )->get_column( 'MAKTX_COUNT')->set_short_text( 'ArtDesc').
      lo_alv->get_columns( )->get_column( 'TOTAL')->set_short_text( 'Total').

      lo_alv->get_functions( )->set_all( abap_true ).

      DATA(lo_layout) = lo_alv->get_layout( ).
      ls_layout_key-report = sy-repid.
      lo_layout->set_key( ls_layout_key ).
      lo_layout->set_save_restriction( if_salv_c_layout=>restrict_none ).
      lo_layout->set_default( abap_true ).

      lo_alv->display( ).
    CATCH cx_salv_msg cx_salv_not_found.
      RETURN.
  ENDTRY.

ENDFORM.
