class ZCX_HYBRIS_SERVICES definition
  public
  inheriting from ZCX_BAPI_EXCEPTION
  create public .

public section.

  methods CONSTRUCTOR
    importing
      !TEXTID like IF_T100_MESSAGE=>T100KEY optional
      !PREVIOUS like PREVIOUS optional
      !MT_BAPI_RETURN type BAPIRET2_T optional .
protected section.
private section.
ENDCLASS.



CLASS ZCX_HYBRIS_SERVICES IMPLEMENTATION.


  method CONSTRUCTOR.
CALL METHOD SUPER->CONSTRUCTOR
EXPORTING
PREVIOUS = PREVIOUS
MT_BAPI_RETURN = MT_BAPI_RETURN
.
clear me->textid.
if textid is initial.
  IF_T100_MESSAGE~T100KEY = IF_T100_MESSAGE=>DEFAULT_TEXTID.
else.
  IF_T100_MESSAGE~T100KEY = TEXTID.
endif.
  endmethod.
ENDCLASS.
