REPORT  zprog_tree_report.

TABLES: zarn_products.

DATA: gv_gtin   TYPE zarn_gtin_code,
      gv_vendor TYPE zarn_gln_descr_upper.

PARAMETERS: p_short TYPE zarn_description_upper DEFAULT '*CADB*'.
SELECT-OPTIONS: s_vend FOR gv_vendor,
                s_fan  FOR zarn_products-fan_id,
                s_gtin FOR gv_gtin.
PARAMETERS: p_skip TYPE zca_e_skip DEFAULT 0,
            p_top  TYPE zca_e_top DEFAULT 30.

TYPES: BEGIN OF ts_output,
         product_id           TYPE  zarn_idno,
         catalog_version      TYPE  zarn_e_catalog_version,
         catalog_version_desc TYPE  val_text,
         short_desc           TYPE  zarn_fs_short_descr_upper,
         main_gtin            TYPE  zarn_gtin_code,
         main_gln             TYPE zarn_gln,
         main_gln_desc        TYPE zarn_gln_descr,
         has_vendor           TYPE flag,
         fan                  TYPE  zarn_fan_id,
         article              TYPE  matnr,
         icare_flag           TYPE  zarn_fsni_icare_value,
         icare_value          TYPE  zarn_fsni_icare_value,
         arena_status         TYPE  zarn_e_arena_status,
         has_header           TYPE flag,
       END OF ts_output,
       tt_output TYPE STANDARD TABLE OF ts_output WITH DEFAULT KEY.

START-OF-SELECTION.
  PERFORM execute.

FORM execute .

  DATA: ls_input TYPE zsmm_hybris_lean_search_in.

  APPEND p_short TO ls_input-short_desc.

  LOOP AT s_fan[] INTO s_fan.
    APPEND INITIAL LINE TO ls_input-fan ASSIGNING FIELD-SYMBOL(<lv_fan>).
    <lv_fan> = s_fan-low.
  ENDLOOP.

  LOOP AT s_gtin[] INTO s_gtin.
    APPEND INITIAL LINE TO ls_input-gtin ASSIGNING FIELD-SYMBOL(<lv_gtin>).
    <lv_gtin> = s_gtin-low.
  ENDLOOP.

  LOOP AT s_vend[] INTO s_vend.
    APPEND INITIAL LINE TO ls_input-vendor_gln_desc ASSIGNING FIELD-SYMBOL(<lv_vendor>).
    <lv_vendor> = s_vend-low.
  ENDLOOP.

  APPEND INITIAL LINE TO ls_input-sorting ASSIGNING FIELD-SYMBOL(<ls_sorting>).
  <ls_sorting>-attribute = 'ICARE_FLAG'.

  APPEND INITIAL LINE TO ls_input-sorting ASSIGNING <ls_sorting>.
  <ls_sorting>-attribute = 'CREATED_ON'.
  <ls_sorting>-sort_order = abap_true.

  TRY.
      GET TIME STAMP FIELD DATA(lv_start).
      DATA(ls_output) = zcl_hybris_services=>get_instance( )->search_lean( is_input = ls_input
                                                                           iv_skip  = p_skip
                                                                           iv_top   = p_top ).
      GET TIME STAMP FIELD DATA(lv_end).
      DATA: lv_diff TYPE i.
      lv_diff = lv_end - lv_start.
      WRITE: 'Time taken: ', lv_diff.
      PERFORM show_data USING ls_output-products.

    CATCH zcx_hybris_services INTO DATA(lo_x_error).
      zcl_message_services=>show_as_log( it_bapi_return = lo_x_error->mt_bapi_return ).
  ENDTRY.
ENDFORM.
*&---------------------------------------------------------------------*
*&      Form  SHOW_DATA
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->P_LS_OUTPUT_PRODUCTS  text
*----------------------------------------------------------------------*
FORM show_data  USING  it_output_products TYPE ANY TABLE.

  DATA: lt_output TYPE tt_output.

  LOOP AT it_output_products ASSIGNING FIELD-SYMBOL(<ls_data>).
    APPEND INITIAL LINE TO lt_output ASSIGNING FIELD-SYMBOL(<ls_output>).
    MOVE-CORRESPONDING <ls_data> TO <ls_output>.
  ENDLOOP.

  IF lt_output IS NOT INITIAL.
    SELECT idno
      FROM zarn_reg_hdr
      INTO TABLE @DATA(lt_header)
      FOR ALL ENTRIES IN @lt_output
      WHERE idno = @lt_output-product_id.
    LOOP AT lt_output ASSIGNING <ls_output>.
      READ TABLE lt_header WITH KEY idno = <ls_output>-product_id TRANSPORTING NO FIELDS.
      IF sy-subrc = 0.
        <ls_output>-has_header = abap_true.
      ENDIF.
    ENDLOOP.
  ENDIF.

  PERFORM enhance_vendor_info CHANGING lt_output.

  TRY.
      cl_salv_table=>factory( IMPORTING r_salv_table = DATA(lo_alv)
                              CHANGING t_table = lt_output
                             ).

      lo_alv->get_functions( )->set_all( abap_true ).
      PERFORM set_columns USING lo_alv.
      lo_alv->display( ).
    CATCH cx_salv_msg.
      RETURN.
  ENDTRY.

ENDFORM.

FORM enhance_vendor_info CHANGING ct_output TYPE tt_output.

  TYPES: BEGIN OF ts_gln,
           gln         TYPE zarn_gln,
           location1   TYPE bbbnr,
           location2   TYPE bbsnr,
           check_digit TYPE bubkz,
         END   OF ts_gln,
         tt_gln TYPE STANDARD TABLE OF ts_gln WITH DEFAULT KEY.

  DATA: lt_gln  TYPE tt_gln,
        gt_word TYPE STANDARD TABLE OF string.

  CHECK ct_output IS NOT INITIAL.

  LOOP AT ct_output ASSIGNING FIELD-SYMBOL(<ls_output>) WHERE main_gln IS NOT INITIAL.
    APPEND INITIAL LINE TO lt_gln ASSIGNING FIELD-SYMBOL(<ls_gln>).
    <ls_gln>-gln = <ls_output>-main_gln.
    DATA(lv_gln) = <ls_gln>-gln.
    <ls_gln>-location1 = lv_gln+0(7).
    SHIFT lv_gln LEFT BY 7 PLACES.

    DATA(lv_length) = strlen( lv_gln ).
    IF lv_length > 0.
      lv_length = lv_length - 1.
      <ls_gln>-location2 = lv_gln+0(lv_length).
      <ls_gln>-check_digit = lv_gln+lv_length(1).
    ENDIF.
  ENDLOOP.

  IF lt_gln IS INITIAL.
    RETURN.
  ENDIF.
  SELECT lifnr,
         bbbnr,
         bbsnr,
         bubkz
    FROM lfa1
    INTO TABLE @DATA(lt_vendor)
    FOR ALL ENTRIES IN @lt_gln
    WHERE bbbnr = @lt_gln-location1
      AND bbsnr = @lt_gln-location2
      AND bubkz = @lt_gln-check_digit.
  IF sy-subrc <> 0.
    RETURN.
  ENDIF.

  LOOP AT ct_output ASSIGNING <ls_output> WHERE main_gln IS NOT INITIAL.
    READ TABLE lt_gln INTO DATA(ls_gln) WITH KEY gln = <ls_output>-main_gln.
    IF sy-subrc = 0.
      IF line_exists( lt_vendor[ bbbnr = ls_gln-location1 bbsnr = ls_gln-location2 bubkz = ls_gln-check_digit ] ).
        <ls_output>-has_vendor = abap_true.
      ENDIF.
    ENDIF.
  ENDLOOP.

ENDFORM.

FORM set_columns USING io_alv TYPE REF TO cl_salv_table.

  DATA(lo_columns) = io_alv->get_columns( ).
  lo_columns->set_optimize( abap_true ).

  TRY.
      lo_columns->get_column( 'HAS_VENDOR' )->set_short_text( 'Has Vendor'(004) ).
      lo_columns->get_column( 'HAS_HEADER' )->set_short_text( 'Has Header'(005) ).
      lo_columns->get_column( 'ICARE_FLAG' )->set_short_text( 'ICare Flag'(006) ).
    CATCH cx_salv_not_found ##no_handler.
  ENDTRY.

ENDFORM.
