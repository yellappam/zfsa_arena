class ZCL_HYBRIS_SERVICES definition
  public
  final
  create private .

public section.

  class-methods GET_INSTANCE
    returning
      value(RO_INSTANCE) type ref to ZCL_HYBRIS_SERVICES .
  methods SEARCH_LEAN
    importing
      !IS_INPUT type ZSMM_HYBRIS_LEAN_SEARCH_IN
      !IV_SKIP type ZCA_E_SKIP default 0
      !IV_TOP type ZCA_E_TOP default 30
    returning
      value(RS_OUTPUT) type ZSMM_HYBRIS_LEAN_SEARCH_OUT
    raising
      ZCX_HYBRIS_SERVICES .
  methods GET_DEFAULT_MERCH_CAT
    importing
      !IV_GLN type ZARN_GLN
      !IV_BRAND_NAME type WRF_BRAND_DESCR optional
      !IV_DESCRIPTION type MAKTX
    returning
      value(RV_MERCH_CAT) type MATKL .
protected section.
PRIVATE SECTION.

  TYPES:
    BEGIN OF ts_attribute_map,
      sap_attribute   TYPE string,
      proxy_attribute TYPE string,
    END   OF ts_attribute_map .
  TYPES:
    tt_attribute_map TYPE SORTED TABLE OF ts_attribute_map WITH UNIQUE KEY sap_attribute .
  TYPES:
    BEGIN OF ts_regional_header,
      id                   TYPE zarn_idno,
      version              TYPE zarn_version,
      article              TYPE matnr,
      requested_by         TYPE zarn_requested_by,
      requested_by_date    TYPE zarn_requested_by_date,
      retail_category_mngr TYPE zarn_catman,
      initiation           TYPE zarn_initiation,
      requestor            TYPE zarn_requestor,
      requested_mc         TYPE zarn_requested_mc,
      icare_value          TYPE zarn_fsni_icare_value,
      id_type              TYPE zarn_id_type,
    END OF ts_regional_header .
  TYPES:
    tt_regional_header TYPE HASHED TABLE OF ts_regional_header WITH UNIQUE KEY id .

  CLASS-DATA so_singleton TYPE REF TO zcl_hybris_services .
  DATA mt_attribute_map TYPE tt_attribute_map .
  DATA mt_catalog_domain TYPE ztt_ca_domain_value .

  METHODS enrich_search_lean_output
    CHANGING
      !cs_output TYPE zsmm_hybris_lean_search_out .
  METHODS map_merch_category
    IMPORTING
      !is_nat_merch_hier  TYPE zproduct_lean_search_response6
    RETURNING
      VALUE(rv_merch_cat) TYPE matkl .
  METHODS map_search_sorting
    IMPORTING
      !it_input        TYPE ztmm_hybris_lean_search_sort
    RETURNING
      VALUE(rs_output) TYPE zproduct_lean_search_request_s .
  METHODS map_fsni_ranging
    IMPORTING
      !is_fsni_ranging TYPE zproduct_lean_search_respons19
    RETURNING
      VALUE(rs_output) TYPE zsmm_hybris_search_fsni_range .
  METHODS map_uom_variants
    IMPORTING
      !is_uom_variant   TYPE zproduct_lean_search_response
    EXPORTING
      !ev_main_gtin     TYPE zarn_gtin_code
      !ev_main_gln      TYPE zarn_gln
      !ev_main_gln_desc TYPE zarn_gln_descr
    RETURNING
      VALUE(rt_output)  TYPE ztmm_hybris_lean_search_uomv .
  METHODS map_search_lean_output
    IMPORTING
      !is_input        TYPE zproduct_lean_search_respons24
    RETURNING
      VALUE(rs_output) TYPE zsmm_hybris_lean_search_out .
  METHODS map_search_lean_input
    IMPORTING
      !is_input        TYPE zsmm_hybris_lean_search_in
      !iv_skip         TYPE zca_e_skip
      !iv_top          TYPE zca_e_top
    RETURNING
      VALUE(rs_output) TYPE zproduct_lean_search_request1 .
  METHODS raise_exception
    IMPORTING
      !it_message TYPE bapiret2_t OPTIONAL
    RAISING
      zcx_hybris_services .
  METHODS search_lean_int
    IMPORTING
      !is_input        TYPE zproduct_lean_search_request1
    RETURNING
      VALUE(rs_output) TYPE zproduct_lean_search_respons24
    RAISING
      zcx_hybris_services .
  METHODS read_attribute_map .
  METHODS map_catalog_version
    IMPORTING
      !iv_catalog      TYPE string
    EXPORTING
      !ev_catalog      TYPE zarn_e_catalog_version
      !ev_catalog_desc TYPE val_text .
ENDCLASS.



CLASS ZCL_HYBRIS_SERVICES IMPLEMENTATION.


METHOD enrich_search_lean_output.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 12.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Enrich output data
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  DATA: lt_regional_header TYPE tt_regional_header.

  CHECK cs_output-products IS NOT INITIAL.

  SELECT hdr~idno,
         hdr~version,
         hdr~matnr,
         hdr~requested_by,
         hdr~requested_by_date,
         hdr~ret_zzcatman,
         hdr~initiation,
         hdr~requestor,
         hdr~requested_mc,
         prod~fsni_icare_value,
         ver~id_type
    FROM zarn_reg_hdr AS hdr
    LEFT OUTER JOIN zarn_products AS prod ON prod~idno    = hdr~idno AND
                                             prod~version = hdr~version
    LEFT OUTER JOIN zarn_prd_version AS ver ON ver~idno    = hdr~idno AND
                                               ver~version = hdr~version
    INTO TABLE @lt_regional_header
    FOR ALL ENTRIES IN @cs_output-products
    WHERE hdr~idno = @cs_output-products-product_id.

  LOOP AT cs_output-products ASSIGNING FIELD-SYMBOL(<ls_product>).
    READ TABLE lt_regional_header INTO DATA(ls_regional_header) WITH KEY id = <ls_product>-product_id.
    IF sy-subrc = 0.
      MOVE-CORRESPONDING ls_regional_header TO <ls_product>.
      <ls_product>-icare_flag        = abap_true.
      IF <ls_product>-article IS NOT INITIAL.
        <ls_product>-arena_status = zif_arena_status_c=>gc_complete.
      ELSEIF ls_regional_header-id_type = zif_arn_id_type=>gc_icare OR
             ls_regional_header-id_type = zif_arn_id_type=>gc_requested.
        <ls_product>-arena_status = zif_arena_status_c=>gc_in_progress.
      ENDIF.
    ELSE.
      <ls_product>-icare_flag = <ls_product>-fsni_ranging-fsnitake_up.
    ENDIF.
  ENDLOOP.

ENDMETHOD.


METHOD get_default_merch_cat.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 26.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # This determines the default merchandise category
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  " Note: When requesting i-Care functionality, user needs to provide default merchandise
  " category. To speed up ranging, it has been decided to determine this based on available
  " information.

  TYPES: BEGIN OF ts_output,
           matnr       TYPE matnr,
           matkl       TYPE matkl,
           matkl_desc  TYPE wgbez,
           gln_count   TYPE i,
           brand_descr TYPE wrf_brand_descr,
           brand_count TYPE i,
           maktx       TYPE  maktx,
           maktx_count TYPE i,
           total       TYPE i,
         END  OF ts_output,
         tt_output TYPE STANDARD TABLE OF ts_output WITH DEFAULT KEY.

  DATA: lv_location1   TYPE bbbnr,
        lv_location2   TYPE bbsnr,
        lv_check_digit TYPE bubkz,
        lt_output      TYPE tt_output,
        lt_word        TYPE STANDARD TABLE OF string.

  DATA(lv_gln) = iv_gln.
  lv_location1 = lv_gln+0(7).
  SHIFT lv_gln LEFT BY 7 PLACES.

  DATA(lv_length) = strlen( lv_gln ).
  IF lv_length > 0.
    lv_length      = lv_length - 1.
    lv_location2   = lv_gln+0(lv_length).
    lv_check_digit = lv_gln+lv_length(1).
  ENDIF.

  " Step 1: Determine vendors with given GLN
  SELECT lifnr
    FROM lfa1
    INTO TABLE @DATA(lt_vendor)
    WHERE bbbnr = @lv_location1
      AND bbsnr = @lv_location2
      AND bubkz = @lv_check_digit.
  IF sy-subrc <> 0.
    RETURN.
  ENDIF.

  " Step 2: Determine all articles supplied by the vendors
  SELECT DISTINCT matnr
    FROM eina
    INTO TABLE @DATA(lt_article)
    FOR ALL ENTRIES IN @lt_vendor
    WHERE lifnr = @lt_vendor-lifnr.
  IF sy-subrc <> 0.
    RETURN.
  ENDIF.

  " Step 3: Get additional information about the articles
  SELECT a~matnr,
         maktx,
         matkl,
         a~brand_id,
         brand_descr
    FROM mara AS a
    INNER JOIN makt AS t ON t~matnr = a~matnr AND
                            t~spras = @sy-langu
    LEFT OUTER JOIN wrf_brands_t AS b ON b~brand_id = a~brand_id AND
                                         b~language = @sy-langu
    INTO TABLE @DATA(lt_mara)
    FOR ALL ENTRIES IN @lt_article
    WHERE a~matnr = @lt_article-matnr.
  IF sy-subrc <> 0.
    RETURN.
  ENDIF.

  " Step 4: Allocate points and determine the top most merchandise category as default
  LOOP AT lt_mara INTO DATA(ls_mara).
    APPEND INITIAL LINE TO lt_output ASSIGNING FIELD-SYMBOL(<ls_output>).
    MOVE-CORRESPONDING ls_mara TO <ls_output>.

    DATA(ls_merch_cat) = zcl_merchandise_cat_services=>get_instance( )->get_merchandise_cat( ls_mara-matkl ).
    <ls_output>-matkl_desc = ls_merch_cat-merchandise_cat_desc.
    <ls_output>-gln_count = 16000.

    " Brand points
    IF iv_brand_name IS NOT INITIAL.
      FIND FIRST OCCURRENCE OF iv_brand_name IN ls_mara-brand_descr IGNORING CASE.
      IF sy-subrc = 0.
        <ls_output>-brand_count = 6000.
      ENDIF.
    ENDIF.

    " Article Description points
    IF iv_description IS NOT INITIAL.
      CLEAR: lt_word.
      SPLIT iv_description AT '' INTO TABLE lt_word.

      LOOP AT lt_word INTO DATA(lv_word).
        FIND FIRST OCCURRENCE OF  lv_word IN <ls_output>-maktx IGNORING CASE.
        IF sy-subrc = 0.
          IF <ls_output>-maktx_count IS INITIAL.
            <ls_output>-maktx_count = 5000.
          ELSE.
            ADD 1 TO <ls_output>-maktx_count.
          ENDIF.
        ENDIF.
      ENDLOOP.
    ENDIF.

    <ls_output>-total = <ls_output>-gln_count + <ls_output>-brand_count + <ls_output>-maktx_count.
  ENDLOOP.

  SORT lt_output BY total DESCENDING.
  rv_merch_cat = VALUE #( lt_output[ 1 ]-matkl OPTIONAL ).

ENDMETHOD.


METHOD get_instance.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 07.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Get class instance
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  IF so_singleton IS NOT BOUND.
    so_singleton = NEW #( ).
  ENDIF.
  ro_instance = so_singleton.

ENDMETHOD.


METHOD map_catalog_version.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 13.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Maps Catalog Version
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  CLEAR: ev_catalog,
         ev_catalog_desc.

  IF mt_catalog_domain IS INITIAL.
    TRY.
        mt_catalog_domain = zcl_ddic_services=>get_domain_values( 'ZARN_D_CATALOG_VERSION' ).
      CATCH zcx_ddic_services_error ##no_handler.
    ENDTRY.
  ENDIF.

  ev_catalog_desc = iv_catalog.
  READ TABLE mt_catalog_domain INTO DATA(ls_catalog_domain) WITH KEY text = iv_catalog.
  IF sy-subrc = 0.
    ev_catalog = ls_catalog_domain-value.
  ENDIF.

ENDMETHOD.


METHOD map_fsni_ranging.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 17.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Map FSNI Ranging from proxy to sap structure
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  rs_output-fsnitake_up = is_fsni_ranging-fsnitake_up-value.

ENDMETHOD.


METHOD map_merch_category.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 11.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Map merchandise category
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  LOOP AT is_nat_merch_hier-national_merchandise_hierarchy INTO DATA(ls_nat_merch_hier).
    rv_merch_cat = ls_nat_merch_hier-code.
    EXIT.
  ENDLOOP.

  IF rv_merch_cat CO '0'.
    CLEAR: rv_merch_cat.
  ENDIF.

ENDMETHOD.


METHOD map_search_lean_input.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 07.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Map Search Lean Input to proxy structure
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  ASSIGN rs_output-product_lean_search_request-fsvendor_data_req TO FIELD-SYMBOL(<ls_search_req>).

  <ls_search_req>-short_description = is_input-short_desc.
  <ls_search_req>-fan               = is_input-fan.
  <ls_search_req>-gln_description   = is_input-vendor_gln_desc.
  <ls_search_req>-gtin              = is_input-gtin.
  <ls_search_req>-flag_operator     = 'AND'.
  <ls_search_req>-start_index       = iv_skip.
  <ls_search_req>-page_size         = iv_top.
  <ls_search_req>-sorting = map_search_sorting( is_input-sorting ).

ENDMETHOD.


METHOD map_search_lean_output.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 07.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Maps proxy output to ABAP output
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  DATA(lt_product) = is_input-product_lean_search_response-fsvendor_data_res-basic_product.
  LOOP AT lt_product INTO DATA(ls_product).
    APPEND INITIAL LINE TO rs_output-products ASSIGNING FIELD-SYMBOL(<ls_product>).
    MOVE-CORRESPONDING ls_product TO <ls_product>.
    <ls_product>-product_id      = ls_product-code.
    <ls_product>-short_desc      = ls_product-fsshort_description.
    <ls_product>-brand_desc      = ls_product-fsbrand-description.
    <ls_product>-uom_variants    = map_uom_variants( EXPORTING is_uom_variant   = ls_product-uomvariants
                                                     IMPORTING ev_main_gtin     = <ls_product>-main_gtin
                                                               ev_main_gln      = <ls_product>-main_gln
                                                               ev_main_gln_desc = <ls_product>-main_gln_desc ).
    <ls_product>-merchandise_cat = map_merch_category( ls_product-national_merchandise_hierarchi ).
    <ls_product>-fsni_ranging    = map_fsni_ranging( ls_product-fsniranging ).
    map_catalog_version( EXPORTING iv_catalog      = ls_product-catalog_version
                         IMPORTING ev_catalog      = <ls_product>-catalog_version
                                   ev_catalog_desc = <ls_product>-catalog_version_desc ).
  ENDLOOP.

  rs_output-skip          = is_input-product_lean_search_response-fsvendor_data_res-start_index.
  rs_output-top           = is_input-product_lean_search_response-fsvendor_data_res-page_size.
  rs_output-total_records = is_input-product_lean_search_response-fsvendor_data_res-total_records_found.
  enrich_search_lean_output( CHANGING cs_output = rs_output ).

ENDMETHOD.


METHOD map_search_sorting.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 08.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Map sorting
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  read_attribute_map( ).
  LOOP AT it_input INTO DATA(ls_input).
    READ TABLE mt_attribute_map INTO DATA(ls_attribute_map) WITH KEY sap_attribute = ls_input-attribute.
    IF sy-subrc = 0.
      APPEND INITIAL LINE TO rs_output-entry ASSIGNING FIELD-SYMBOL(<ls_entry>).
      <ls_entry>-sort_by_attribute = ls_attribute_map-proxy_attribute.
      IF ls_input-sort_order = abap_false.
        <ls_entry>-sort_direction = 'ASC'.
      ELSE.
        <ls_entry>-sort_direction = 'DESC'.
      ENDIF.
    ENDIF.
  ENDLOOP.

ENDMETHOD.


METHOD map_uom_variants.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 09.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Map UOM variant from proxy to sap structure
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  CLEAR: ev_main_gtin,
         ev_main_gln,
         ev_main_gln_desc.

  LOOP AT is_uom_variant-uomvariant INTO DATA(ls_variant).
    APPEND INITIAL LINE TO rt_output ASSIGNING FIELD-SYMBOL(<ls_output>).
    <ls_output>-uom_code  = ls_variant-code.
    <ls_output>-base_unit = ls_variant-base_unit.

    " GTIN Variants
    LOOP AT ls_variant-gtinvariants-gtinvariant INTO DATA(ls_gtin_variant).
      APPEND INITIAL LINE TO <ls_output>-gtin_variants ASSIGNING FIELD-SYMBOL(<ls_gtin_variant>).
      <ls_gtin_variant>-gtin_code = ls_gtin_variant-code.
      <ls_gtin_variant>-current   = ls_gtin_variant-current.

      IF <ls_output>-base_unit = abap_true AND
         <ls_gtin_variant>-current = abap_true.
        ev_main_gtin = <ls_gtin_variant>-gtin_code.
      ENDIF.
    ENDLOOP.

    " Purchase Info Record
    LOOP AT ls_variant-purchasing_infor_records-purchasing_info_record INTO DATA(ls_pir).
      APPEND INITIAL LINE TO <ls_output>-purchase_info_rec ASSIGNING FIELD-SYMBOL(<ls_pir>).
      <ls_pir>-hybris_internal_code = ls_pir-hybris_internal_code.
      <ls_pir>-gln_code             = ls_pir-gln-gln.
      <ls_pir>-gln_desc             = ls_pir-gln-description.

      IF <ls_output>-base_unit = abap_true.
        ev_main_gln      = <ls_pir>-gln_code.
        ev_main_gln_desc = <ls_pir>-gln_desc.
      ENDIF.

    ENDLOOP.

  ENDLOOP.

ENDMETHOD.


METHOD raise_exception.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 07.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Raise exception
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  DATA: lt_message TYPE bapiret2_t.

  IF it_message IS INITIAL.
    zcl_message_services=>add_message_to_bapiret_tab( CHANGING ct_return = lt_message ).
  ELSE.
    lt_message = it_message.
  ENDIF.

  RAISE EXCEPTION TYPE zcx_hybris_services
    EXPORTING
      mt_bapi_return = lt_message.

ENDMETHOD.


METHOD read_attribute_map.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 08.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Read Attribute Map
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  CHECK mt_attribute_map IS INITIAL.

  mt_attribute_map = VALUE #( (  sap_attribute = 'FAN'        proxy_attribute = 'FAN' )
                              (  sap_attribute = 'SHORT_DESC' proxy_attribute = 'FS_SHORT_DESCRIPTION' )
                              (  sap_attribute = 'ICARE_FLAG' proxy_attribute = 'NI_TAKEUP' )
                              (  sap_attribute = 'CREATED_ON' proxy_attribute = 'CREATION_DATE' )
                            ).

ENDMETHOD.


METHOD search_lean.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 07.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Searches Hybris system with lean functionality
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  DATA(ls_proxy_input)  = map_search_lean_input( is_input = is_input
                                                 iv_skip  = iv_skip
                                                 iv_top   = iv_top ).
  DATA(ls_proxy_output) = search_lean_int( ls_proxy_input ).
  rs_output = map_search_lean_output( ls_proxy_output ).

ENDMETHOD.


METHOD search_lean_int.
*-----------------------------------------------------------------------
* DATE WRITTEN     # 07.02.2020
* SYSTEM           # DE0
* SPECIFICATION    # CIP-1006 - Arena iCare App
* SAP VERSION      # SAP_BASIS  750 0015
* AUTHOR           # I90003973, Yellappa Madigonde
*-----------------------------------------------------------------------
* PURPOSE          # Searches Hybris system with lean functionality
*-----------------------------------------------------------------------
* CHANGE HISTORY
*-----------------------------------------------------------------------
* DATE             #
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

  DATA: lo_hybris  TYPE REF TO zco_product_search_basic_out,
        lo_root    TYPE REF TO cx_root,
        lv_msg     TYPE string,
        ls_message TYPE bapiret2,
        lt_message TYPE bapiret2_tty.

  TRY.

      CREATE OBJECT lo_hybris.
      CALL METHOD lo_hybris->product_search_basic_out
        EXPORTING
          output = is_input
        IMPORTING
          input  = rs_output.

    CATCH cx_root INTO lo_root.
      lv_msg = lo_root->if_message~get_text( ).
      TRY.
          MESSAGE e000 INTO zcl_message_services=>sv_msg_dummy.
          zcl_message_services=>convert_text_to_msg_variables( EXPORTING iv_text       = lv_msg
                                                                         iv_message_id = 'ZARENA_MSG'
                                                               IMPORTING es_return     = ls_message ).
          APPEND ls_message TO lt_message.
        CATCH zcx_message_exception.
      ENDTRY.

      MESSAGE e138 INTO zcl_message_services=>sv_msg_dummy.
      zcl_message_services=>add_message_to_bapiret_tab( CHANGING ct_return = lt_message ).
      raise_exception( lt_message ).

  ENDTRY.

ENDMETHOD.
ENDCLASS.
