*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------

*&---------------------------------------------------------------------*
*&  Include           ZIARN_APPROVALS_MASS_CI1
*&---------------------------------------------------------------------*

*&---------------------------------------------------------------------*
*&       Class (Implementation)  lcl_controller
*&---------------------------------------------------------------------*
*        Text
*----------------------------------------------------------------------*
CLASS lcl_controller IMPLEMENTATION.

  METHOD initialize.
    FREE: so_singleton.

    CLEAR: so_singleton.

  ENDMETHOD.

  METHOD get.

    IF so_singleton IS NOT BOUND.
      so_singleton = NEW #( ).
    ENDIF.

    ro_instance = so_singleton.

  ENDMETHOD.



  METHOD call_screen_0100.



    IF mo_view IS NOT BOUND.
      mo_view = NEW #( ).
    ENDIF.

    mo_view->initialize( ).


    mo_view->set_data( it_cc_hdr      = mo_data->mt_cc_hdr[]
                       it_cc_desc     = mo_data->mt_cc_desc[]
                       it_reg_hdr     = mo_data->mt_reg_hdr[]
                       it_prd_version = mo_data->mt_prd_version[]
                       it_ver_data    = mo_data->mt_ver_data[]
                       it_approval    = mo_data->mt_approval[]
                       it_appr_ch     = mo_data->mt_appr_ch[]
                       it_rel_matrix  = mo_data->mt_rel_matrix[]
                       it_table_mastr = mo_data->mt_table_mastr[]
                       it_field_confg = mo_data->mt_field_confg[]
                       ir_idno        = mo_data->mr_idno[]
                       ir_chgcat      = mo_data->mr_chgcat[]
                       ir_chgare      = mo_data->mr_chgare[]
                       ir_nstat       = mo_data->mr_nstat[]
                       ir_rstat       = mo_data->mr_rstat[]
                        ).

    IF call_screen = abap_true.
      CALL SCREEN 0100.
    ENDIF.

  ENDMETHOD.


  METHOD main.

    IF mo_data IS NOT BOUND.
      mo_data = NEW #( ir_idno   = s_idno[]
                       ir_chgcat = s_chgcat[]
                       ir_chgare = s_chgare[]
                       ir_nstat  = s_nstat[]
                       ir_rstat  = s_rstat[] ).
    ENDIF.

    me->get_data( ).


    gt_reg_hdr[]     = mo_data->mt_reg_hdr[].
    gt_prd_version[] = mo_data->mt_prd_version[].
    gt_ver_data[]    = mo_data->mt_ver_data[].

  ENDMETHOD.

  METHOD handle_event.
  ENDMETHOD.


  METHOD get_data.

    mo_data->initialize( ).

    mo_data->get_data( ).

  ENDMETHOD.

  METHOD save_data.
  ENDMETHOD.

  METHOD user_command_0100.

    TRY.
        CASE sy-ucomm.
          WHEN 'SAVE'.
            save_data( ).

        ENDCASE.

      CATCH zcx_bapi_exception INTO DATA(lo_x_exception).
        lo_x_exception->show_message_as_log(
          iv_title      = text-t07
          iv_start_col  = 5
          iv_start_row  = 5
          iv_end_col    = 105
          iv_end_row    = 10 ).
    ENDTRY.



  ENDMETHOD.

ENDCLASS.               "lcl_controller
