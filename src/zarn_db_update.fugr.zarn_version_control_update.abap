*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Functional Specification 3113
* DATE WRITTEN     # 17112015
* SAP VERSION      # 731
* TYPE             # Function Module
* AUTHOR           # C90001363, Jitin Kharbanda
*-----------------------------------------------------------------------
* TITLE            # Update data into DB
* PURPOSE          # Update Version and Control tables into DB
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             #             (repeat block for amendment)
* CHANGE No.       #
* DESCRIPTION      #
* WHO              #
*-----------------------------------------------------------------------
FUNCTION zarn_version_control_update.
*"----------------------------------------------------------------------
*"*"Update Function Module:
*"
*"*"Local Interface:
*"  IMPORTING
*"     VALUE(IT_CONTROL) TYPE  ZTARN_CONTROL OPTIONAL
*"     VALUE(IT_PRD_VERSION) TYPE  ZTARN_PRD_VERSION OPTIONAL
*"     VALUE(IT_VER_STATUS) TYPE  ZTARN_VER_STATUS OPTIONAL
*"----------------------------------------------------------------------



* AReNa: Control Records
  IF it_control[] IS NOT INITIAL.
    MODIFY zarn_control FROM TABLE it_control[].
  ENDIF.


* AReNa: Product Version Master
  IF it_prd_version[] IS NOT INITIAL.
    MODIFY zarn_prd_version FROM TABLE it_prd_version[].
  ENDIF.


* AReNa: Version Status
  IF it_ver_status[] IS NOT INITIAL.
    MODIFY zarn_ver_status FROM TABLE it_ver_status[].
  ENDIF.

ENDFUNCTION.
