* regenerated at 02.05.2016 11:18:25 by  C90001363
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZARN_CURRENCY_TTOP.               " Global Data
  INCLUDE LZARN_CURRENCY_TUXX.               " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZARN_CURRENCY_TF...               " Subroutines
* INCLUDE LZARN_CURRENCY_TO...               " PBO-Modules
* INCLUDE LZARN_CURRENCY_TI...               " PAI-Modules
* INCLUDE LZARN_CURRENCY_TE...               " Events
* INCLUDE LZARN_CURRENCY_TP...               " Local class implement.
* INCLUDE LZARN_CURRENCY_TT99.               " ABAP Unit tests
  INCLUDE LZARN_CURRENCY_TF00                     . " subprograms
  INCLUDE LZARN_CURRENCY_TI00                     . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
