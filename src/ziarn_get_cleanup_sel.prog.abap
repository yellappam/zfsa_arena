*&---------------------------------------------------------------------*
*&  Include           ZIARN_GET_CLEANUP_SEL
*&---------------------------------------------------------------------*
SELECTION-SCREEN BEGIN OF BLOCK sel WITH FRAME TITLE text-sel.
SELECT-OPTIONS: s_idno  FOR zarn_get_cleanup-idno,
                s_uname FOR zarn_get_cleanup-uname.
SELECTION-SCREEN END OF BLOCK sel.
SELECTION-SCREEN BEGIN OF BLOCK opt WITH FRAME TITLE text-opt.
PARAMETERS: p_test TYPE flag DEFAULT 'X'.
SELECTION-SCREEN END OF BLOCK opt.
