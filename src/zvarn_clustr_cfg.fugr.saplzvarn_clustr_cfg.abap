* regenerated at 22.06.2020 15:50:23
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE LZVARN_CLUSTR_CFGTOP.              " Global Declarations
  INCLUDE LZVARN_CLUSTR_CFGUXX.              " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE LZVARN_CLUSTR_CFGF...              " Subroutines
* INCLUDE LZVARN_CLUSTR_CFGO...              " PBO-Modules
* INCLUDE LZVARN_CLUSTR_CFGI...              " PAI-Modules
* INCLUDE LZVARN_CLUSTR_CFGE...              " Events
* INCLUDE LZVARN_CLUSTR_CFGP...              " Local class implement.
* INCLUDE LZVARN_CLUSTR_CFGT99.              " ABAP Unit tests
  INCLUDE LZVARN_CLUSTR_CFGF00                    . " subprograms
  INCLUDE LZVARN_CLUSTR_CFGI00                    . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules

INCLUDE lzvarn_clustr_cfgf01.
