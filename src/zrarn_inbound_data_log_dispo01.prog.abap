*----------------------------------------------------------------------*
***INCLUDE ZRARN_INBOUND_DATA_LOG_DISPO01 .
*----------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*&      Module  STATUS_9000  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE status_9000 OUTPUT.
  CONSTANTS : lc_pf TYPE char20 VALUE 'ZPF_ARN_LOG',
              lc_tb TYPE char20 VALUE 'ZTB_ARN_LOG'.
* Set PF status and title bar
  SET PF-STATUS lc_pf.
  SET TITLEBAR  lc_tb.

ENDMODULE.                 " STATUS_9000  OUTPUT
*&---------------------------------------------------------------------*
*&      Module  DISPLAY_ALV_9000  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE display_alv_9000 OUTPUT.

  CONSTANTS: lc_a        TYPE char01 VALUE 'A'.

* Declaration
  DATA: lo_docking       TYPE REF TO cl_gui_docking_container,  "Custom container instance reference
        lt_fieldcat      TYPE        lvc_t_fcat,                "Field catalog
*        ls_layout        TYPE        lvc_s_layo ,               "Layout structure
*        lt_toolbar       TYPE        ui_functions,              "Toolbar Function
*        lo_event_handler TYPE REF TO lcl_event_handler,         "Event handling
*        ls_disvariant    TYPE        disvariant,                "Variant
        lo_alvgrid       TYPE REF TO cl_gui_alv_grid.


  IF lo_alvgrid IS INITIAL .
*   Create docking container
    CREATE OBJECT lo_docking
      EXPORTING
        parent = cl_gui_container=>screen0
        ratio  = 90  " 90% yet not full-screen size
      EXCEPTIONS
        others = 6.
    IF sy-subrc NE 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

*   Full size screen for ALV
    CALL METHOD lo_docking->set_extension
      EXPORTING
        extension  = 99999  " full-screen size !!!
      EXCEPTIONS
        cntl_error = 1
        OTHERS     = 2.
    IF sy-subrc NE 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

* Create ALV grid
    CREATE OBJECT lo_alvgrid
      EXPORTING
        i_parent = lo_docking
      EXCEPTIONS
        others   = 5.
    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                 WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.


*   Preparing field catalog
    PERFORM prepare_fcat_9000 CHANGING lt_fieldcat
                                       lv_sub_obj.


*   ALV Display
    CALL METHOD lo_alvgrid->set_table_for_first_display
      EXPORTING
*       i_structure_name              = gc_alv_8000-struct
*       is_variant                    = ls_disvariant
        i_save                        = lc_a
*       is_layout                     = ls_layout
*       it_toolbar_excluding          = lt_toolbar
      CHANGING
        it_outtab                     = lt_alv
        it_fieldcatalog               = lt_fieldcat
      EXCEPTIONS
        invalid_parameter_combination = 1
        program_error                 = 2
        too_many_lines                = 3
        OTHERS                        = 4.


  ELSE.

*   ALV Refresh Display
    CALL METHOD lo_alvgrid->refresh_table_display
      EXPORTING
*       is_stable      = ls_stable
        i_soft_refresh = abap_true
      EXCEPTIONS
        finished       = 1
        OTHERS         = 2.
  ENDIF.


ENDMODULE.                 " DISPLAY_ALV_9000  OUTPUT
