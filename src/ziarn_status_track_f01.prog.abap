*&---------------------------------------------------------------------*
*&  Include           ZIARN_STATUS_TRACK_F01
*&---------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*&      Form  GET_SELECTION_SCREEN_DATA
*&---------------------------------------------------------------------*

FORM get_selection_screen_data .

  TYPES:
    BEGIN OF ty_s_chg_doc,
*        objectclas   TYPE cdobjectcl,
      objectid  TYPE cdobjectv,
      changenr  TYPE cdchangenr,
      tabname   TYPE tabname,
      tabkey    TYPE cdtabkey,
      fname     TYPE fieldname,
      chngind   TYPE cdchngind,
      username  TYPE cdusername,
      udate     TYPE cddatum,
      utime     TYPE cduzeit,
      tcode     TYPE cdtcode,
      value_new TYPE cdfldvaln,
      value_old TYPE cdfldvalo,
    END OF ty_s_chg_doc,

    ty_t_chg_doc TYPE HASHED TABLE OF ty_s_chg_doc
                 WITH UNIQUE KEY objectid changenr
                                 tabname tabkey fname chngind,
    BEGIN OF ty_key,
      idno TYPE cdobjectv,
    END OF ty_key,

    BEGIN OF ty_sel,
*        idno             TYPE cdobjectv, "zarn_products-idno,     " zarn_products
      idno             TYPE zarn_products-idno,     " zarn_products
      version          TYPE zarn_products-version,
      chg_area         TYPE zarn_chg_area,
      fan_id           TYPE zarn_fan_id,
      matnr_ni         TYPE zarn_matnr_ni,
      plu              TYPE zarn_plu,
      description      TYPE zarn_products-description,
      gln              TYPE zarn_products-gln,
      gln_descr        TYPE zarn_products-gln_descr,
      avail_start_date TYPE zarn_avail_start_date, " zarn_products
      version_status   TYPE zarn_version_status,   " zarn_prd_version
      vstat_date       TYPE zarn_status_date,
      release_status   TYPE zarn_rel_status,
      rstat_date       TYPE zarn_rel_date,
      article_ver      TYPE zarn_ver_status-article_ver,
      approved_on      TYPE zarn_approved_on,      "ZARN_VER_STATUS
      approved_at      TYPE zarn_approved_at,
      approved_by      TYPE zarn_approved_by,
      change_doc_no    TYPE cdchangeid,
      date_in          TYPE zarn_date_in,          " Control
      uni_dc           TYPE zarn_reg_hdr-zz_uni_dc,   "ZARN_REG_HDR
      lni_dc           TYPE zarn_reg_hdr-zz_lni_dc,

    END OF ty_sel.

  CONSTANTS:
    lc_reg_hdr    TYPE cdpos-tabname      VALUE 'ZARN_REG_HDR',
    lc_prd_ver    TYPE cdpos-tabname      VALUE 'ZARN_PRD_VERSION',
    lc_status     TYPE cdpos-fname        VALUE 'STATUS',
    lc_v_status   TYPE cdpos-fname        VALUE 'VERSION_STATUS',
    lc_r_status   TYPE cdpos-fname        VALUE 'RELEASE_STATUS',
    lc_objectclas TYPE cdhdr-objectclas VALUE 'ZARN_CTRL_REGNL'.


  DATA:
    lt_output  TYPE STANDARD TABLE OF ty_sel,
    lt_sel     TYPE STANDARD TABLE OF ty_sel,
    ls_sel     LIKE LINE OF lt_sel,
    lt_reg_hdr TYPE STANDARD TABLE OF zarn_reg_hdr,
    ls_reg_hdr LIKE LINE OF lt_reg_hdr,
*    lt_key       TYPE TABLE OF TY_key,
    lt_key     TYPE ztarn_key,
    ls_key     LIKE LINE OF lt_key,
    lt_key_sel TYPE TABLE OF ty_key,
    ls_key_sel LIKE LINE OF lt_key_sel.
*    lt_key_sel   TYPE ztarn_key.

  DATA:
*    lt_val_output TYPE zarn_t_rl_output,
    lr_table   TYPE REF TO cl_salv_table,
    lr_layout  TYPE REF TO cl_salv_layout,
    ls_lay_key TYPE salv_s_layout_key,
    lv_header  TYPE lvc_title,
    lr_excep   TYPE REF TO zcx_arn_validation_err.

  DATA: lt_cdhdr   TYPE SORTED TABLE OF cdhdr
                                WITH UNIQUE KEY objectclas objectid changenr,
        ls_cdhdr   TYPE cdhdr,
        lt_cdpos   TYPE STANDARD TABLE OF cdpos,
*                               SORTED TABLE OF cdpos
*                               WITH UNIQUE KEY objectclas objectid, " changenr tabname tabkey fname chngind,
        ls_cdpos   TYPE cdpos,
        lt_chg_doc TYPE ty_t_chg_doc,
        ls_chg_doc TYPE ty_s_chg_doc.


*  IF    s_datein[] IS NOT INITIAL OR
*        s_adate[]  IS NOT INITIAL OR
*        s_rstat[]  IS NOT INITIAL OR
*        s_vstat[]  IS NOT INITIAL OR
*        s_fan[]    IS NOT INITIAL OR
*        s_matnr[]  IS NOT INITIAL OR
*        s_plu[]    IS NOT INITIAL.

*   Control data selection
  SELECT
        prod~idno
        prod~version
        prod~fan_id
        prod~matnr_ni
        prod~plu
        prod~description
        prod~gln
        prod~gln_descr
        prod~avail_start_date
        prd_ver~version_status
        prd_ver~release_status
        ver_sts~article_ver
        ver_sts~approved_on
        ver_sts~approved_at
        ver_sts~approved_by
        ver_sts~change_doc_no
        control~date_in
        reg_hdr~zz_uni_dc
        reg_hdr~zz_lni_dc
        INTO CORRESPONDING FIELDS OF TABLE lt_sel
        FROM zarn_prd_version        AS prd_ver
        INNER JOIN zarn_control      AS control
          ON control~guid            EQ prd_ver~guid
        INNER JOIN zarn_ver_status   AS ver_sts
          ON  ver_sts~idno           EQ prd_ver~idno
          AND ver_sts~current_ver    EQ prd_ver~version
*       INNER JOIN zarn_pir        AS v
*         ON  v~idno     EQ prd_ver~idno
*         AND v~version  EQ prd_ver~version
        INNER JOIN zarn_products     AS prod
          ON  prod~idno              EQ prd_ver~idno
*         AND prod~version EQ prd_ver~version
        INNER JOIN zarn_gtin_var     AS gtin
            ON  gtin~idno            EQ prd_ver~idno
            AND gtin~version         EQ prd_ver~version
        INNER JOIN zarn_reg_hdr      AS reg_hdr
          ON  reg_hdr~idno           EQ prd_ver~idno
        WHERE control~date_in        IN s_datein
          AND prd_ver~version_status IN s_vstat
          AND prd_ver~release_status IN s_rstat
          AND prod~fan_id            IN s_fan
          AND prod~matnr_ni          IN s_matnr
          AND prod~plu               IN s_plu
          AND prod~avail_start_date  IN s_adate
          AND gtin~gtin_code         IN s_gtin1
          AND ( reg_hdr~ret_zzcatman IN s_rcman OR reg_hdr~gil_zzcatman IN s_ccman ).

* for regional header records
  lt_key[] =  lt_sel[].
  SORT lt_key BY idno.
  DELETE ADJACENT DUPLICATES FROM lt_key COMPARING idno.

* for normal selection - remove duplicates introduced by ZARN_GTIN_VAR
  SORT lt_sel BY idno version.
  DELETE ADJACENT DUPLICATES FROM lt_sel COMPARING idno version.

* for change documents
  LOOP AT lt_key INTO ls_key.
    ls_key_sel-idno = ls_key-idno.
    APPEND ls_key_sel TO lt_key_sel.
  ENDLOOP.


*  SORT lt_key BY idno.
*  DELETE ADJACENT DUPLICATES FROM lt_key COMPARING idno.

*Begin of changes - S/4 HANA Upgrade, Initial check before FAE - Brad Gorlicki
* get Regional records for addition
  IF lt_key[] IS NOT INITIAL.
    SELECT * FROM zarn_reg_hdr INTO TABLE
             lt_reg_hdr FOR ALL ENTRIES IN lt_key
                        WHERE idno = lt_key-idno.
  ENDIF.
*End of changes - S/4 HANA Upgrade, Initial check before FAE - Brad Gorlicki

*  lt_output =

*CDHDR
*  OBJECTCLAS	  ZARN_CTRL_REGNL
*  OBJECTID	    BP0000007
*  CHANGENR
*  USERNAME
*  UDATE    date when the RECORD WAS CREATED
*  UTIME
*  TCODE
*  PLANCHNGNR
*  ACT_CHNGNO
*  WAS_PLANND
*  CHANGE_IND	 = U


*CDPOS
*  OBJECTCLAS = ZARN_CTRL_REGNL
*  OBJECTID   = BP0000007 IDNO
*  CHANGENR   =
*  TABNAME    = ZARN_REG_HDR and ZARN_PRD_VERSION
*  TABKEY     =
*  FNAME      = STATUS  and VERSION_STATUS and RELEASE_STATUS
*  CHNGIND    = U
*  VALUE_NEW
*  VALUE_OLD


  REFRESH: lt_cdhdr[].
  SELECT   objectclas objectid changenr username udate utime tcode
    INTO CORRESPONDING FIELDS OF TABLE lt_cdhdr[]
    FROM cdhdr
    FOR ALL ENTRIES IN lt_key_sel
    WHERE objectclas EQ lc_objectclas
      AND objectid   EQ lt_key_sel-idno
      AND udate      IN s_datein[].

  IF lt_cdhdr[] IS NOT INITIAL.
* Get Change Document Details
    REFRESH: lt_cdpos[].
    SELECT objectclas objectid changenr tabname tabkey fname chngind
           value_new value_old
      INTO CORRESPONDING FIELDS OF TABLE lt_cdpos[]
    FROM cdpos
      FOR ALL ENTRIES IN lt_cdhdr[]
      WHERE objectclas EQ lc_objectclas
        AND objectid   EQ lt_cdhdr-objectid
        AND changenr   EQ lt_cdhdr-changenr
        AND ( tabname  EQ lc_reg_hdr OR
              tabname EQ lc_prd_ver )
        AND ( fname EQ lc_status   OR
              fname EQ lc_v_status OR
              fname EQ lc_r_status )
        AND chngind   EQ 'U'.
    IF sy-subrc IS INITIAL.


      LOOP AT lt_cdpos INTO ls_cdpos.
        CLEAR ls_chg_doc.
*        ls_chg_doc-objectclas = ls_cdpos-objectclas.
        ls_chg_doc-objectid   = ls_cdpos-objectid.
        ls_chg_doc-changenr   = ls_cdpos-changenr.
        ls_chg_doc-tabname    = ls_cdpos-tabname.
        ls_chg_doc-tabkey     = ls_cdpos-tabkey.
        ls_chg_doc-fname      = ls_cdpos-fname.
        ls_chg_doc-chngind    = ls_cdpos-chngind.
        ls_chg_doc-value_new  = ls_cdpos-value_new.
        ls_chg_doc-value_old  = ls_cdpos-value_old.

        CLEAR ls_cdhdr.
        READ TABLE lt_cdhdr INTO ls_cdhdr
        WITH TABLE KEY objectclas = ls_cdpos-objectclas
                       objectid   = ls_cdpos-objectid
                       changenr   = ls_cdpos-changenr.
        IF sy-subrc = 0.
          ls_chg_doc-username   = ls_cdhdr-username.
          ls_chg_doc-udate      = ls_cdhdr-udate.
          ls_chg_doc-utime      = ls_cdhdr-utime.
          ls_chg_doc-tcode      = ls_cdhdr-tcode.
        ENDIF.

        INSERT ls_chg_doc INTO TABLE lt_chg_doc[].
      ENDLOOP.  " LOOP AT lt_cdpos INTO ls_cdpos


    ENDIF.  " IF sy-subrc = 0  - CDPOS
  ENDIF.

  BREAK c90002769.
*  SORT: lt_cdpos.

  LOOP AT lt_reg_hdr INTO ls_reg_hdr.

    LOOP AT lt_sel INTO ls_sel
         WHERE idno = ls_reg_hdr-idno.

      IF ls_sel-article_ver NE  ls_sel-version.
*       if the version is different to article version
        CLEAR:
           ls_sel-approved_on,
           ls_sel-approved_at,
           ls_sel-approved_by.

*       get Version status
        IF ls_sel-version_status IS NOT INITIAL.
          READ TABLE lt_chg_doc INTO ls_chg_doc
               WITH KEY  objectid  = ls_reg_hdr-idno
                         tabname   = lc_prd_ver
                         fname     = lc_v_status
                         value_new = ls_sel-version_status.
          IF sy-subrc IS INITIAL.
            ls_sel-vstat_date = ls_chg_doc-udate.
*          ls_sel-approved_at = ls_chg_doc-utime.
*          ls_sel-approved_by = ls_chg_doc-username.
          ENDIF.
        ENDIF.

*       get release status
        IF ls_sel-release_status IS NOT INITIAL.
          READ TABLE lt_chg_doc INTO ls_chg_doc
               WITH KEY  objectid  = ls_reg_hdr-idno
                         tabname   = lc_prd_ver
                         fname     = lc_r_status
                         value_new = ls_sel-release_status.
          IF sy-subrc IS INITIAL.
            ls_sel-rstat_date = ls_chg_doc-udate.
*          ls_sel-approved_at = ls_chg_doc-utime.
*          ls_sel-approved_by = ls_chg_doc-username.
          ENDIF.
        ENDIF.

*      LOOP AT lt_cdpos INTO ls_cdpos
*              WHERE objectclas EQ lc_objectclas
*                AND objectid   EQ ls_cdpos-objectid
**                and changenr   eq
*                AND tabname    EQ  lc_prd_ver.
***                and tabkey     eq
**                and fname      eq
***                and chngind    eq 'U'
**                and value_new  eq
**                and
*
*        CASE ls_cdpos-fname.
*          WHEN
*                lc_v_status .
*
*          WHEN  lc_r_status .
*        ENDCASE.
**                       changenr   = ls_cdpos-changenr
*      ENDLOOP.


*         ls_sel-approved_on =
*         ls_sel-approved_at =
*         ls_sel-approved_by =
      ENDIF.

      ls_sel-chg_area = 'N'.
      APPEND ls_sel TO lt_output.
    ENDLOOP.

*idno
*version
*fan_id
*matnr_ni
*plu
*description
*gln
*gln_descr
*avail_start_date

    ls_sel-version_status = ls_reg_hdr-status.
    ls_sel-release_status = ls_reg_hdr-release_status.
    CLEAR: ls_sel-approved_on,
           ls_sel-approved_at,
           ls_sel-approved_by,
           ls_sel-vstat_date,
           ls_sel-rstat_date.

*       get  status
    IF ls_sel-version_status IS NOT INITIAL.
      READ TABLE lt_chg_doc INTO ls_chg_doc
           WITH KEY  objectid  = ls_reg_hdr-idno
                     tabname   = lc_reg_hdr
                     fname     = lc_status
                     value_new = ls_sel-version_status.
      IF sy-subrc IS INITIAL.
        ls_sel-vstat_date = ls_chg_doc-udate.
*      ls_sel-approved_at = ls_chg_doc-utime.
*      ls_sel-approved_by = ls_chg_doc-username.
      ENDIF.
    ENDIF.
*       get release status
    IF ls_sel-release_status IS NOT INITIAL.
      READ TABLE lt_chg_doc INTO ls_chg_doc
           WITH KEY  objectid  = ls_reg_hdr-idno
                     tabname   = lc_reg_hdr
                     fname     = lc_r_status
                     value_new = ls_sel-release_status.
      IF sy-subrc IS INITIAL.
        ls_sel-rstat_date = ls_chg_doc-udate.
*      ls_sel-approved_at = ls_chg_doc-utime.
*      ls_sel-approved_by = ls_chg_doc-username.
      ENDIF.
    ENDIF.

*   change_doc_no
    ls_sel-date_in =  ls_reg_hdr-ersda.
    ls_sel-uni_dc  =  ls_reg_hdr-zz_uni_dc.
    ls_sel-lni_dc  =  ls_reg_hdr-zz_lni_dc.
    ls_sel-chg_area = 'R'.
    APPEND ls_sel TO lt_output.
    CLEAR ls_sel.


  ENDLOOP.



**    IF lt_key IS INITIAL.
*    IF lt_sel IS INITIAL.
*      RETURN.
*    ENDIF.
*
*    SORT lt_key BY idno version.
*    DELETE ADJACENT DUPLICATES FROM lt_key COMPARING idno.
*
**   use the filtered key from other selection to filter further if any
*    PERFORM get_gtin_selection CHANGING lt_key.
*
*    PERFORM get_reg_hdr_selection CHANGING lt_key.
*
**____________________________________________________________________
*  ELSEIF  s_gtin1[] IS NOT INITIAL.
**     start the selects from GTIN table !
*    SELECT g~idno g~version
*      FROM zarn_gtin_var AS g
*      INNER JOIN zarn_ver_status AS v
*         ON v~idno        EQ g~idno
*        AND v~current_ver EQ g~version
*      INTO TABLE lt_key
*      WHERE  gtin_code IN  s_gtin1.
*
*    SORT lt_key BY idno version.
*    DELETE ADJACENT DUPLICATES FROM lt_key COMPARING idno.
**   use the filtered key from GTIN  to filter further if any
*    PERFORM get_reg_hdr_selection CHANGING lt_key.
*    PERFORM get_other_input_selection CHANGING lt_key.
**____________________________________________________________________
*  ELSEIF  s_rcman[] IS NOT INITIAL OR
*          s_ccman[] IS NOT INITIAL.
*
**   Regional header selection
*    SELECT idno version
*       FROM zarn_reg_hdr
*          INTO TABLE lt_key
*       WHERE  ( ret_zzcatman IN s_rcman
*           OR   gil_zzcatman IN s_ccman ).
*
*    SORT lt_key BY idno version.
*    DELETE ADJACENT DUPLICATES FROM lt_key COMPARING idno.
**   use the filtered key from Regional header to filter further if any
*    PERFORM get_gtin_selection CHANGING lt_key.
*    PERFORM get_other_input_selection CHANGING lt_key.
*
*  ENDIF.




  TRY.

      cl_salv_table=>factory( "EXPORTING r_container = go_dialogbox_container
                              IMPORTING r_salv_table = lr_table
                              CHANGING t_table = lt_output ).

* CATCH cx_salv_msg .
*ENDTRY.

      lr_table->get_columns( )->set_optimize( 'X' ).

      lr_layout = lr_table->get_layout( ).
      lr_layout->set_default( 'X' ).
      ls_lay_key-report = sy-repid.
      lr_layout->set_key( ls_lay_key ).
      lr_layout->set_save_restriction( if_salv_c_layout=>restrict_none ).

      lv_header = |AReNa Status Report - Number of records { lines( lt_output ) } |.
      lr_table->get_display_settings( )->set_list_header( lv_header ).
      lr_table->get_functions( )->set_all( ).

      lr_table->display( ).
*    lr_table->refresh_table_display( ).
    CATCH cx_salv_msg.
      "TBD
  ENDTRY.




ENDFORM.                    " GET_SELECTION_SCREEN_DATA
*&---------------------------------------------------------------------*
*&      Form  GET_GTIN_SELECTION
*&---------------------------------------------------------------------*
FORM get_gtin_selection  CHANGING pt_key      TYPE ztarn_key.

  DATA:
    lt_key_sel  TYPE ztarn_key.

  IF s_gtin1[] IS NOT INITIAL.
*   GTIN
    SELECT idno version
       FROM zarn_gtin_var
          INTO TABLE lt_key_sel
      FOR ALL ENTRIES IN pt_key
       WHERE  idno      EQ pt_key-idno
         AND  version   EQ pt_key-version
         AND  gtin_code IN  s_gtin1 .

*   use the filtered key from GTIN
    CLEAR pt_key.
    pt_key = lt_key_sel.
    SORT pt_key BY idno version.
    DELETE ADJACENT DUPLICATES FROM pt_key COMPARING idno.
  ENDIF.
ENDFORM.                    " GET_GTIN_SELECTION
*&---------------------------------------------------------------------*
*&      Form  GET_REG_HDR_SELECTION
*&---------------------------------------------------------------------*
FORM get_reg_hdr_selection  CHANGING pt_key  TYPE ztarn_key.

  DATA:
    lt_key_sel  TYPE ztarn_key.

  IF s_rcman[] IS NOT INITIAL OR
     s_ccman[] IS NOT INITIAL.
*     Regional Header
    SELECT idno version
       FROM zarn_reg_hdr
          INTO TABLE lt_key_sel
      FOR ALL ENTRIES IN pt_key
       WHERE  idno    EQ pt_key-idno
         AND  version EQ pt_key-version
         AND ( ret_zzcatman IN s_rcman
           OR  gil_zzcatman IN s_ccman ).
*   keep it consistent
    CLEAR pt_key.
    pt_key = lt_key_sel.
    SORT pt_key BY idno version.
    DELETE ADJACENT DUPLICATES FROM pt_key COMPARING idno.
  ENDIF.

ENDFORM.                    " GET_REG_HDR_SELECTION
*&---------------------------------------------------------------------*
*&      Form  GET_OTHER_INPUT_SELECTION
*&---------------------------------------------------------------------*
FORM get_other_input_selection  CHANGING pt_key   TYPE ztarn_key.

  DATA:
    lt_key_sel  TYPE ztarn_key.

* select on other input and filter
  IF    s_datein[] IS NOT INITIAL OR
        s_adate[] IS NOT INITIAL OR
        s_rstat[] IS NOT INITIAL OR
        s_vstat[] IS NOT INITIAL OR
        s_fan[]   IS NOT INITIAL OR
        s_matnr[] IS NOT INITIAL OR
        s_plu[]   IS NOT INITIAL.

*   Control data selection
    SELECT p~idno p~version
          INTO TABLE lt_key_sel
          FROM zarn_prd_version      AS p
          INNER JOIN zarn_control    AS c
             ON c~guid        EQ p~guid
          INNER JOIN zarn_ver_status AS s
            ON  s~idno        EQ p~idno
            AND s~current_ver EQ p~version
          INNER JOIN zarn_pir        AS v
            ON  v~idno        EQ p~idno
            AND v~version     EQ p~version
         INNER JOIN zarn_products    AS a
              ON  a~idno    EQ p~idno
              AND a~version EQ p~version
         INNER JOIN zarn_reg_hdr AS r
              ON  r~idno    EQ p~idno
          FOR ALL ENTRIES IN pt_key
          WHERE  p~idno           EQ pt_key-idno
            AND p~version        EQ pt_key-version
            AND p~version_status        IN s_vstat
            AND p~release_status        IN s_rstat
            AND c~date_in               IN s_datein
            AND a~fan_id                IN s_fan
            AND a~matnr_ni              IN s_matnr
            AND a~plu                   IN s_plu
            AND a~avail_start_date      IN s_adate.


*     keep it consistent
    CLEAR pt_key.
    pt_key = lt_key_sel.
    SORT pt_key BY idno version.
    DELETE ADJACENT DUPLICATES FROM pt_key COMPARING idno version.

  ENDIF.
ENDFORM.                    " GET_OTHER_INPUT_SELECTION
