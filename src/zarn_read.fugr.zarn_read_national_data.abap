FUNCTION zarn_read_national_data.
*"----------------------------------------------------------------------
*"*"Local Interface:
*"  IMPORTING
*"     REFERENCE(IT_KEY) TYPE  ZTARN_KEY
*"  EXPORTING
*"     REFERENCE(ET_DATA) TYPE  ZTARN_PROD_DATA
*"     REFERENCE(ES_DATA_ALL) TYPE  ZSARN_PROD_DATA
*"----------------------------------------------------------------------
*-----------------------------------------------------------------------
* PROJECT          # FSA
* SPECIFICATION    # Functional Specification 3125
* DATE WRITTEN     # 01 03 2016
* SAP VERSION      # 731
* TYPE             # Function Module
* AUTHOR           # C90002769, Rajesh Anumolu
*-----------------------------------------------------------------------
* TITLE            # AReNa: Read all National DB tables
* PURPOSE          # AReNa: provide data for all NAtional DB tables
* COPIED FROM      #
*-----------------------------------------------------------------------
* CALLED FROM      # <list>
*                  #
*-----------------------------------------------------------------------
* CALLS TO         # <custom programs, function modules, class methods>
*-----------------------------------------------------------------------
* RESTRICTIONS     #
*                  #
*-----------------------------------------------------------------------
*                  --------- AMENDMENTS ----------
*-----------------------------------------------------------------------
* DATE             # 30.05.2018
* CHANGE No.       # S/4 HANA Upgrade
* DESCRIPTION      # Explicit sort added before delete adjacent duplicates
*                  # Search for S/4 HANA Upgrade
* WHO              # Brad Gorlicki (C90012814)
*-----------------------------------------------------------------------
* DATE             # 05.05.2020
* CHANGE No.       # Agile
* DESCRIPTION      # Moving FEATURE_BENEFIT field from ZARN_PRODUCTS_EX to new table
*                  #
* WHO              # Greg van der Loeff/C90001448
*-----------------------------------------------------------------------

  DATA:
    lv_where_str   TYPE string,
    lt_key         TYPE ztarn_key,
    ls_key         LIKE LINE OF lt_key,
    lv_itabname    TYPE char50,
    lv_datatabname TYPE char50,
    lt_tabname     TYPE ty_t_tabname,
    ls_tabname     TYPE ty_s_tabname,
    ls_data        LIKE LINE OF et_data,
    lv_success     TYPE char01,
    ls_prod_data   TYPE zsarn_prod_data,
    ls_zarn_benefit TYPE zarn_benefits.


  FIELD-SYMBOLS:
    <tabname>      TYPE tabname,
    <itabname>     TYPE table,
    <table>        TYPE table,
    <dataitabname> TYPE table,
    <table_str>    TYPE any.


  lt_key = it_key.
*Begin of changes - S/4 HANA Upgrade, Explicit sort before Delete Adjacent Duplicates - Brad Gorlicki
  SORT lt_key BY idno version.
*End of changes - S/4 HANA Upgrade, Explicit sort before Delete Adjacent Duplicates - Brad Gorlicki
  DELETE ADJACENT DUPLICATES FROM lt_key COMPARING idno version.

  IF lt_key IS INITIAL.
    RETURN.
  ENDIF.


* Get national Tables to be checked
  SELECT arena_table
    INTO TABLE lt_tabname[]
    FROM zarn_table_mastr
    WHERE arena_table_type = gc_nat.


* Read Database
  LOOP AT lt_tabname INTO ls_tabname.

    IF <tabname>  IS ASSIGNED. UNASSIGN <tabname>.  ENDIF.
    IF <itabname> IS ASSIGNED. UNASSIGN <itabname>. ENDIF.

    ASSIGN ls_tabname-tabname TO <tabname>.


** Select Data for all the Product tables
    CLEAR: lv_itabname.
    CONCATENATE 'LS_PROD_DATA-' ls_tabname-tabname INTO lv_itabname.
    ASSIGN (lv_itabname) TO <itabname>.

    SELECT * FROM (<tabname>)   "#EC CI_SEL_NESTED
      INTO CORRESPONDING FIELDS OF TABLE <itabname>
        FOR ALL ENTRIES IN lt_key[]
        WHERE idno    EQ lt_key-idno
          AND version EQ lt_key-version.

  ENDLOOP.

*  export single layer
  es_data_all = ls_prod_data.

* now split the tables into ZTARN_PROD_DATA Format
  LOOP AT lt_key INTO ls_key.
    ls_data-idno    =  ls_key-idno.
    ls_data-version =  ls_key-version.

    CLEAR lv_where_str.
*   Dynnamic where clause for Internal Table
    CONCATENATE 'IDNO EQ ' '''' INTO lv_where_str SEPARATED BY space.
    CONCATENATE lv_where_str ls_data-idno '''' INTO lv_where_str.

    CONCATENATE lv_where_str 'AND VERSION EQ ' '''' INTO lv_where_str SEPARATED BY space.
    CONCATENATE lv_where_str ls_data-version '''' INTO lv_where_str.

*   loop at seperate tables
    LOOP AT lt_tabname INTO ls_tabname.

      IF <itabname> IS ASSIGNED. UNASSIGN <itabname>. ENDIF.

*     Select Data for all the Product tables
      CLEAR: lv_itabname.
      CONCATENATE 'LS_PROD_DATA-' ls_tabname-tabname INTO lv_itabname.
      ASSIGN (lv_itabname) TO <itabname>.

*     Select Data for all the Product tables
      CLEAR: lv_datatabname.
      CONCATENATE 'LS_DATA-' ls_tabname-tabname INTO lv_datatabname.
      ASSIGN (lv_datatabname) TO <dataitabname>.

*     loop at tables for KEY and append
      LOOP AT <itabname> ASSIGNING <table_str> WHERE (lv_where_str).
*       append data to LS_DATA structure
        APPEND <table_str> TO <dataitabname>.
      ENDLOOP.
*
      IF sy-subrc IS INITIAL.
        lv_success = abap_true.
      ENDIF.
    ENDLOOP.

*   append to data deep structure TABLE
    IF lv_success IS NOT INITIAL.

*     Check for existing feature benefit
      IF ls_data-zarn_benefits[] IS INITIAL.
        READ TABLE ls_data-zarn_products_ex ASSIGNING FIELD-SYMBOL(<ls_products_ex>) INDEX 1.
        IF sy-subrc EQ 0.
          IF <ls_products_ex>-feature_benefit IS NOT INITIAL.
            CLEAR ls_zarn_benefit.
            ls_zarn_benefit-idno            = <ls_products_ex>-idno.
            ls_zarn_benefit-version         = <ls_products_ex>-version.
            ls_zarn_benefit-benefit_no      = 1.
            ls_zarn_benefit-feature_benefit = <ls_products_ex>-feature_benefit.
            INSERT ls_zarn_benefit INTO TABLE ls_data-zarn_benefits.
          ENDIF.
        ENDIF.
      ENDIF.


      APPEND ls_data TO et_data.
      CLEAR: ls_data,
             lv_success.
    ENDIF.
  ENDLOOP.

ENDFUNCTION.
